var opportunityLogCollection = require('../databaseSchema/oppLogSchema').opportunityLog
var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston();
var _ = require("lodash");

function OppLog() {

}

OppLog.prototype.createLogForOppTransfer = function (data,callback) {

    var logs = [];
    
    _.each(data,function (op) {
        logs.push(buildLogsObj(op.fromUserId,op.from,op.to,op.emailId,op.opps,op.transferredBy,"transfer",op.note))
    })

    insertLog(logs);
}

OppLog.prototype.createLogForOppStateChange = function (userId,oldStateOpp,newStateOpp,callback) {

    var logs = [];
    var checkForTheseProperties = ["stageName","closeDate","contactEmailId",
        "netGrossMargin","amount","influencers","decisionMakers",
        "partners","userEmailId","createdByEmailId","usersWithAccess",
        "opportunityName","productType","sourceType","currency","businessUnit"
        ,"solution","vertical","type","geoLocation"]

    for(var key1 in oldStateOpp){
        for(var key2 in newStateOpp){
            if(key1 == key2 && _.includes(checkForTheseProperties,key1)){

                if(key1 == "influencers" || key1 == "decisionMakers" || key1 == "partners"){
                    if(oldStateOpp[key1].length != newStateOpp[key2].length){
                        logs.push(buildLogForStateChange(oldStateOpp.opportunityId,userId,oldStateOpp[key1],newStateOpp[key2],"propertyChange",key1))
                    }
                } else if(key1 == "usersWithAccess"){

                    if(!oldStateOpp[key1]){
                        oldStateOpp[key1] = [];
                    }

                    if(oldStateOpp[key1].length != newStateOpp[key2].length){
                        var usersAccess = buildLogForStateChangeUsersWithAccess(oldStateOpp.opportunityId,userId,oldStateOpp[key1],newStateOpp[key2],"propertyChange",key1);
                        _.each(usersAccess,function (el) {
                            logs.push(el)
                        });
                    }
                } else if(oldStateOpp[key1] != newStateOpp[key2] && key1 !="closeDate" && key1 !="geoLocation" && key1 !="usersWithAccess"){
                    logs.push(buildLogForStateChange(oldStateOpp.opportunityId,userId,oldStateOpp[key1],newStateOpp[key2],"propertyChange",key1))
                } else if(key1 == "geoLocation" && (oldStateOpp[key1].zone != newStateOpp[key2].zone || oldStateOpp[key1].town != newStateOpp[key2].town)){

                    var oldV = oldStateOpp[key1].town +","+oldStateOpp[key2].zone
                    var newV = newStateOpp[key1].town +","+newStateOpp[key2].zone

                    logs.push(buildLogForStateChange(oldStateOpp.opportunityId,userId,oldV,newV,"propertyChange",key1))
                } else if(key1 =="closeDate"){
                    if(new Date(oldStateOpp[key1]) == new Date(newStateOpp[key2])){

                    } else {

                        var timeDiff = Math.abs(new Date(oldStateOpp[key1]).getTime() - new Date(newStateOpp[key2]).getTime());

                        if(timeDiff>0){
                            logs.push(buildLogForStateChange(oldStateOpp.opportunityId,userId,oldStateOpp[key1],newStateOpp[key2],"propertyChange",key1))
                        }
                    }

                }
            }
        }
    }

    _.each(logs,function (log) {
        if(_.includes(log.newValue,"Close")){
            log.action = "close";
            log.date = new Date();
        }
    });

    var newUsersAdded = [],
        existingUsers = [],
        newUsers = [];

    if(oldStateOpp.usersWithAccess && oldStateOpp.usersWithAccess.length>0){
        existingUsers = _.pluck(oldStateOpp.usersWithAccess,"emailId")
    }

    if(newStateOpp.usersWithAccess && newStateOpp.usersWithAccess.length>0){
        newUsers = _.pluck(newStateOpp.usersWithAccess,"emailId")
    }

    newUsersAdded = _.difference(newUsers,existingUsers);

    insertLog(logs,function () {
        callback(newUsersAdded)
    });
}

OppLog.prototype.createLogForPeopleAdded = function (userId,opportunityId,oldState,newState,type,action,callback) {
    var logs = [];
    logs.push(buildLogForStateChange(opportunityId,userId,oldState,newState,action,type))

    insertLog(logs);
}

OppLog.prototype.createLogForCreation = function (userId,userEmailId,opportunityId,action,callback) {
    var logs = [];
    logs.push(buildLogsObj(userId,userEmailId,userId,userEmailId,opportunityId,userEmailId,action))

    insertLog(logs);
}

function buildLogForStateChange(oppId,fromUserId,oldValue,newValue,action,type){

    return {
        opportunityId: oppId,
        action: action,
        oldValue:oldValue,
        newValue:newValue,
        date: new Date(),
        type:type,
        fromUserId: fromUserId,
        fromEmailId: null,
        toUserId: null,
        toEmailId: null,
        transferredBy: null,
        notes:null
    }
}

function buildLogForStateChangeUsersWithAccess(oppId,fromUserId,oldValue,newValue,action,type){

    var oldValGrp = _.groupBy(oldValue,'accessGroup')
    var newValGrp = _.groupBy(newValue,'accessGroup');
    var logsToCreate = [];

    if(!_.isEmpty(oldValGrp)){
        for(var oldKey in oldValGrp){
            for(var newKey in newValGrp){
                if(oldKey === newKey){
                    if(oldValGrp[oldKey].length != newValGrp[newKey].length){
                        var existingAccess = _.pluck(oldValGrp[oldKey],"emailId");
                        var newAccess = _.pluck(newValGrp[newKey],"emailId");

                        if(existingAccess.length>newAccess.length){
                            action = "removed"
                        } else {
                            action = "added"
                        }

                        logsToCreate.push({
                            opportunityId: oppId,
                            action: action,
                            oldValue:existingAccess,
                            newValue:newAccess,
                            date: new Date(),
                            type:oldKey == "Others"?"Internal Team":oldKey,
                            fromUserId: fromUserId,
                            fromEmailId: null,
                            toUserId: null,
                            toEmailId: null,
                            transferredBy: null,
                            notes:null
                        })
                    } else {
                    }
                } else {

                }
            }
        }
    } else {

        for(var key in newValGrp){

            var newAccess = _.pluck(newValGrp[key],"emailId");
            action = "added"

            logsToCreate.push({
                opportunityId: oppId,
                action: action,
                oldValue:[],
                newValue:newAccess,
                date: new Date(),
                type:key == "Others"?"Internal Team":key,
                fromUserId: fromUserId,
                fromEmailId: null,
                toUserId: null,
                toEmailId: null,
                transferredBy: null,
                notes:null
            })
        }
    }

    if(!_.isEmpty(oldValGrp) && _.isEmpty(newValGrp)){

        for(var key in oldValGrp){

            var newAccess = _.pluck(oldValGrp[key],"emailId");
            action = "removed"

            logsToCreate.push({
                opportunityId: oppId,
                action: action,
                oldValue:newAccess,
                newValue:[],
                date: new Date(),
                type:key,
                fromUserId: fromUserId,
                fromEmailId: null,
                toUserId: null,
                toEmailId: null,
                transferredBy: null,
                notes:null
            })
        }
    }

    return logsToCreate;
}

function buildLogForStateChangeCustomers(oppId,fromUserId,oldValue,newValue,action,type){

    var oldValGrp = _.groupBy(oldValue,'accessGroup')
    var newValGrp = _.groupBy(newValue,'accessGroup');
    var logsToCreate = [];

    if(!_.isEmpty(oldValGrp)){
        for(var oldKey in oldValGrp){
            for(var newKey in newValGrp){
                if(oldKey === newKey){
                    if(oldValGrp[oldKey].length != newValGrp[newKey].length){
                        var existingAccess = _.pluck(oldValGrp[oldKey],"emailId");
                        var newAccess = _.pluck(newValGrp[newKey],"emailId");

                        if(existingAccess.length>newAccess.length){
                            action = "removed"
                        } else {
                            action = "added"
                        }

                        logsToCreate.push({
                            opportunityId: oppId,
                            action: action,
                            oldValue:existingAccess,
                            newValue:newAccess,
                            date: new Date(),
                            type:oldKey,
                            fromUserId: fromUserId,
                            fromEmailId: null,
                            toUserId: null,
                            toEmailId: null,
                            transferredBy: null,
                            notes:null
                        })
                    } else {
                    }
                } else {

                }
            }
        }
    } else {

        for(var key in newValGrp){

            var newAccess = _.pluck(newValGrp[key],"emailId");
            action = "added"

            logsToCreate.push({
                opportunityId: oppId,
                action: action,
                oldValue:[],
                newValue:newAccess,
                date: new Date(),
                type:key,
                fromUserId: fromUserId,
                fromEmailId: null,
                toUserId: null,
                toEmailId: null,
                transferredBy: null,
                notes:null
            })
        }
    }

    if(!_.isEmpty(oldValGrp) && _.isEmpty(newValGrp)){

        for(var key in oldValGrp){

            var newAccess = _.pluck(oldValGrp[key],"emailId");
            action = "removed"

            logsToCreate.push({
                opportunityId: oppId,
                action: action,
                oldValue:newAccess,
                newValue:[],
                date: new Date(),
                type:key,
                fromUserId: fromUserId,
                fromEmailId: null,
                toUserId: null,
                toEmailId: null,
                transferredBy: null,
                notes:null
            })
        }
    }

    return logsToCreate;
}

function buildLogsObj(fromUserId,fromEmailId,toUserId,toEmailId,oppId,transferredBy,action,note) {
    return {
        opportunityId: oppId,
        action: action,
        date: new Date(),
        fromUserId: fromUserId,
        fromEmailId: fromEmailId,
        toUserId: toUserId,
        toEmailId: toEmailId,
        transferredBy: transferredBy,
        notes:note?note:null
    }
}

function insertLog(logs,callback){

    if(logs && logs.length>0){
        opportunityLogCollection.collection.insert(logs,function (err,result) {
            if(callback){
                callback()
            }
        })
    } else {
        if(callback){
            callback()
        }
    }
}

module.exports = OppLog;