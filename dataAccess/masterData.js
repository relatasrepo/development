var masterDataCollection = require('../databaseSchema/masterDataSchema').masterData;
var customObjectId = require('mongodb').ObjectID;

var mongoose = require('mongoose');
var _ = require("lodash")

function MasterData(){}

MasterData.prototype.saveAcc = function(companyId,masterList,accName,callback){

    masterList.forEach(function (el) {
        el._id = new customObjectId()
    });

    var importantHeaders = [];

    for(var key in masterList[0]){
        importantHeaders.push({
            name:key,
            isImportant:false
        })
    }

    masterDataCollection.update({
        companyId:companyId,
        name:accName
    },{
        $set:{
            createdDate: new Date(),
            name:accName,
            data:masterList,
            importantHeaders:importantHeaders
        }},{upsert:true},function (err,result) {
        callback()
    })
};

MasterData.prototype.linkToRelatasAcc = function(companyId,accName,_id,domain,callback){

    var query = {
        companyId:companyId,
        name:accName,
        "data._id": mongoose.Types.ObjectId(_id)
    }

    var update = { $set: { "data": {relatasAccount:domain }} }

    masterDataCollection.update(query,update).exec(function (err,result) {
        callback(err,result)
    });

};

MasterData.prototype.getAccData = function(companyId,accName,search,skip,callback){

    var query = {}

    if(search){
        query = {
            companyId:companyId,
            name:accName
        }

        masterDataCollection.find(query).lean().exec(function (err,result) {
            callback(err,result)
        });
    } else if(skip === 0 || skip>0) {
        query = {
            companyId:companyId,
            name:accName
        }
        var skip_limit = {skip:skip,limit:50};

        masterDataCollection.find(query,{data:{$slice:[skip, 50]}}).lean().exec(function (err,result) {
            callback(err,result)
        });
    }
}

MasterData.prototype.getMasterDataRow = function(companyId,name,accName,callback){

    var query = {
        companyId:companyId,
        "data.relatasAccount": accName
    }

    masterDataCollection.aggregate([
        {
            $match: query
        },
        {
            $unwind: "$data"
        },
        {
            $match:{
                "data.relatasAccount": accName
            }
        },
        {
            $group:{
                _id:"$name",
                data:{
                    $push:"$data"
                },
                importantHeaders:{
                    $first:"$importantHeaders"
                }
            }
        }
    ]).exec(function (err,result) {

        var data = null;
        if(!err && result && result.length > 0){
            data = result;
        }
        callback(err,data)
    });
}

MasterData.prototype.deleteAccount = function(companyId,accName,callback){

    var query = {
        companyId:companyId,
        name:accName
    }

    masterDataCollection.remove(query).exec(function (err,result) {
       callback(err,result)
    });
}

MasterData.prototype.deleteAccountData = function(companyId,doc,callback){

    var query = {
        companyId:companyId,
        name:doc.acc_identifier_rel,
        "data._id": mongoose.Types.ObjectId(doc._id)
    }

    var update = { $pull: { "data": {_id:mongoose.Types.ObjectId(doc._id) }} }

    masterDataCollection.update(query,update).exec(function (err,result) {
       callback(err,result)
    });
}

MasterData.prototype.updateAccOppLink = function(companyId,doc,callback){

    var query = {
        companyId:companyId,
        name:doc.name
    }

    var update = { $set: { oppLinkMandatory:doc.oppLinkMandatory}};

    masterDataCollection.update(query,update).exec(function (err,result) {
       callback(err,result)
    });
}

MasterData.prototype.updateAccountData = function(companyId,doc,common,callback){
    var query = {};
    var update = {};

    if(common.checkRequired(doc._id)){

        query = {
            companyId:companyId,
            name:doc.acc_identifier_rel,
            "data._id": mongoose.Types.ObjectId(doc._id)
        }

        for(var key in doc){
            if(key != "_id" && key != "acc_identifier_rel"){
                update["data.$."+key] = doc[key]
            }
        }

        masterDataCollection.update(query,{$set:update}).exec(function (err,result) {
            callback(err,result)
        });
    } else {

        query = {
            companyId:companyId,
            name:doc.acc_identifier_rel
        }

        update = {};

        for(var key in doc){
            if(key != "_id" && key != "acc_identifier_rel"){
                update[key] = doc[key]
            }
        }

        update._id = new customObjectId();

        masterDataCollection.update(query,{$push:{data:update}}).exec(function (err,result) {
            callback(err,result)
        });
    }
}

MasterData.prototype.updateImportantHeaders = function(companyId,doc,callback){

    var query = {
        companyId:companyId,
        name:doc.acc_identifier_rel
    }

    masterDataCollection.update(query,{$set:{importantHeaders:doc.importantHeaders}}).exec(function (err,result) {
       callback(err,result)
    });
}

MasterData.prototype.getAccTypes = function(companyId,callback){

    masterDataCollection.aggregate([
        {
            $match:{
                companyId:companyId
            }
        },
        {
            $project:{
                name:1,
                oppLinkMandatory:1,
                size:{$size:"$data"}
            }
        },
        {
            $group:{
                _id:null,
                types:{
                    $push:{
                        name:"$name",
                        size:"$size",
                        oppLinkMandatory:"$oppLinkMandatory"
                    }
                }
            }
        }
    ],function (err,result) {
        callback(err,result)
    })
};

MasterData.prototype.getAccTypesAndData = function(companyId,callback){

    masterDataCollection.aggregate([
        {
            $match:{
                companyId:companyId
            }
        },
        {
            $project:{
                name:1,
                importantHeaders:1
            }
        },
        {
            $group:{
                _id:null,
                masterData:{
                    $push:{
                        name:"$name",
                        data:"$importantHeaders",
                    }
                }
            }
        }
    ],function (err,result) {
        callback(err,result)
    })
};

MasterData.prototype.getAccountDataDifference = function(dataTypeArr, dataTypeColsArr, companyId, callback){
    
    var query = {
        companyId:companyId,
        name:{$in:dataTypeArr},
        "data._id": {$in:dataTypeColsArr}
    }

    masterDataCollection.find(query, {name:1, data:1}, function(err, result) {
        return(callback(err, result));
    })
};


module.exports = MasterData;