var InteractionsCollection = require('../databaseSchema/userManagementSchema').interactions
var newInteractionsCollection = require('../databaseSchema/interactionSchema').interactionsV2
var AccInteractionCollection = require('../databaseSchema/accInteractionSchema').accInteraction
var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var scheduleInvitation = require('../databaseSchema/userManagementSchema').scheduleInvitation;
var taskCollection = require('../databaseSchema/userManagementSchema').tasks;
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var invalidEmailListCollection = require('../databaseSchema/userManagementSchema').invalidEmailList;

var contactClass = require('../dataAccess/contactsManagementClass');
var contactManagementClass = require('../dataAccess/contactManagement');
var AccManagementClass = require('../dataAccess/accountManagement');

var async = require("async");

var mongoose = require('mongoose');
//var castToObjectId = require('mongodb').ObjectID;
var customObjectId = require('mongodb').ObjectID;

var contactObj = new contactClass();
var contactManagementObj = new contactManagementClass();
var accManagementObj = new AccManagementClass();
var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getInteractionsErrorLogger();
var _ = require("lodash")
    , moment = require("moment")

var momentTZ = require('moment-timezone');

function interactionsManagement()
{
    /******************* New Interaction Collection API's *************************/
    
    this.updateEmailInteractions = function(user,interactionsArr,refIdArr,callback){
        checkInteractionsExists(user,interactionsArr,refIdArr,callback)
    };

    this.getListOfCompanies = function(callback){
        companyCollection.find({},{companyName:1}).exec(function(err,result){
            if(err) throw err;
            callback(result);
        });
    };

    this.getAllUserInteractions = function(fromDate, toDate, category, callback) {
        var dateMin = momentTZ(fromDate);
        dateMin.hour(0)
        dateMin.minute(0)
        dateMin.second(0)
        dateMin.millisecond(0)
        var dateMax = momentTZ(toDate);
        dateMax.hour(23)
        dateMax.minute(59)
        dateMax.second(59)
        dateMax.millisecond(0)

        var interactionsFiltred = [];
        var removeUserHavingZero = [];
        var q;
        if(category === 'all')
            q = {};
        else if(category === 'corperate')
            q = {corporateUser:true};
        else if(category === 'retail')
            q = {$or:[
                {corporateUser:{'$exists':false}},
                {$and:[
                    {corporateUser:{'$exists':true}},
                    {corporateUser:false}
                ]
                }
            ]};
        else
            q = { companyId: castToObjectId(category) }

        async.series([
            function(callback) {
                myUserCollection.find(q, { emailId: 1, corporateUser:1,companyName:1, _id: 1,serviceLogin:1 }, function(e, r) {
                    for (var i = 0; i < r.length; i++){
                        var uType = 'R';
                        if(r[i].corporateUser === true)
                            uType = 'C';
                        interactionsFiltred.push({
                            _id: r[i]._id,
                            email: r[i].emailId,
                            userType: uType,
                            corporate:r[i].companyName,
                            grandTotal: 0,
                            serviceLogin:r[i].serviceLogin,
                            google: { email: 0, meeting: 0, total: 0 },
                            linkedin: { share: 0, like: 0, comment: 0, total: 0 },
                            mobile:{sms:0, call:0, total:0},
                            facebook: 0,
                            twitter: 0,
                            relatas: {
                                calander: 0,
                                documentShare: 0,
                                email: 0,
                                call: 0,
                                inPerson: 0,
                                phone: 0,
                                skype: 0,
                                meeting: 0,
                                message: 0,
                                total: 0
                            }
                        });
                    }
                    callback();
                });
            },
            function(callback) {
                async.forEach(interactionsFiltred, function(interactionsFiltred, callback) {
                    InteractionsCollection.aggregate([
                        { $match: { 'ownerId': interactionsFiltred._id, userId:{$ne:interactionsFiltred._id} } },
                        { $match: { "interactionDate": { $gt: new Date(dateMin), $lt: new Date(dateMax) } } },
                        //{
                        //    $group: {
                        //        _id: {
                        //            ne: { $ne: ["$userId", "$ownerId"] },
                        //            // emailId: "$emailId",
                        //            uid: "$ownerId",
                        //        },
                        //        count: { $sum: 1 },
                        //        interactions: { $push: { source: "$source", interactionType: "$interactionType", subType: "$subType" } }
                        //    }
                        //},
                        //{ $match: { "_id.ne": true } }, {
                        //    $project: {
                        //        "_id": 0,
                        //        // emailId: "$_id.emailId",
                        //        userId: "$_id.uid",
                        //        interactions: "$interactions"
                        //    }
                        //},
                        //{ $unwind: "$interactions" },
                        {
                            $group: {
                                _id: {
                                    // email: "$emailId",

                                    //userId: "$userId",
                                    //source: "$interactions.source",
                                    //Itype: "$interactions.interactionType",
                                    //Stype: "$interactions.subType"

                                    userId: "$ownerId",
                                    source: "$source",
                                    Itype: "$interactionType",
                                    Stype: "$subType"
                                },
                                count: { $sum: 1 }
                            }
                        }, {
                            $group: {
                                // _id: { email: "$_id.email" },
                                _id: { userId: "$_id.userId" },
                                mainsource: {
                                    $push: {
                                        source: "$_id.source",
                                        itype: "$_id.Itype",
                                        stype: "$_id.Stype",
                                        count: "$count"
                                    }
                                },
                                Tcount: { $push: "$count" }
                            }
                        }, {
                            $project: {
                                _id: 0,
                                // email: "$_id.email",
                                userId: "$_id.userId",
                                mainsource: 1,
                                // total: { $sum: "$Tcount" }
                            }
                        }
                    ], function(err, result) {
                        if (err) throw err;
                        if (result[0] !== undefined) {
                            var i = 0;
                            for (var j = 0; j < result[0].mainsource.length; j++) {
                                switch (result[i].mainsource[j].source) {
                                    case 'google':
                                        switch (result[i].mainsource[j].itype) {
                                            case 'email':
                                                interactionsFiltred.google.email += result[i].mainsource[j].count;
                                                interactionsFiltred.google.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'meeting':
                                                interactionsFiltred.google.meeting += result[i].mainsource[j].count;
                                                interactionsFiltred.google.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                        };
                                        break;
                                    case 'mobile':
                                        switch (result[i].mainsource[j].itype) {
                                            case 'call':
                                                interactionsFiltred.mobile.call += result[i].mainsource[j].count;
                                                interactionsFiltred.mobile.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'sms':
                                                interactionsFiltred.mobile.sms += result[i].mainsource[j].count;
                                                interactionsFiltred.mobile.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                        };
                                        break;
                                    case 'linkedin':
                                        switch (result[i].mainsource[j].stype) {
                                            case 'like':
                                                interactionsFiltred.linkedin.like = result[i].mainsource[j].count;
                                                interactionsFiltred.linkedin.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'share':
                                                interactionsFiltred.linkedin.share = result[i].mainsource[j].count;
                                                interactionsFiltred.linkedin.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'comment':
                                                interactionsFiltred.linkedin.comment = result[i].mainsource[j].count;
                                                interactionsFiltred.linkedin.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                        }
                                        break;
                                    case 'facebook':
                                        interactionsFiltred.facebook += result[i].mainsource[j].count;
                                        interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                        break;
                                    case 'twitter':
                                        interactionsFiltred.twitter += result[i].mainsource[j].count;
                                        interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                        break;
                                    case 'relatas':
                                        switch (result[i].mainsource[j].itype) {
                                            case 'email':
                                                interactionsFiltred.relatas.email += result[i].mainsource[j].count;
                                                interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'calendar-password':
                                                interactionsFiltred.relatas.calander += result[i].mainsource[j].count;
                                                interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'meeting-comment':
                                                interactionsFiltred.relatas.meeting += result[i].mainsource[j].count;
                                                interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'document-share':
                                                interactionsFiltred.relatas.documentShare += result[i].mainsource[j].count;
                                                interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'message':
                                                interactionsFiltred.relatas.message += result[i].mainsource[j].count;
                                                interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'meeting':
                                                switch (result[i].mainsource[j].stype) {
                                                    case 'Phone':
                                                        interactionsFiltred.relatas.phone = result[i].mainsource[j].count;
                                                        interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                        interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                        break;
                                                    case 'Skype':
                                                        interactionsFiltred.relatas.skype = result[i].mainsource[j].count;
                                                        interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                        interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                        break;
                                                    case 'Conference Call':
                                                        interactionsFiltred.relatas.call = result[i].mainsource[j].count;
                                                        interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                        interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                        break;
                                                    case 'In-Person':
                                                        interactionsFiltred.relatas.inPerson = result[i].mainsource[j].count;
                                                        interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                        interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                        break;
                                                };
                                                break;
                                        };
                                        break;
                                    case null:
                                        switch (result[i].mainsource[j].itype) {
                                            case 'meeting':
                                                interactionsFiltred.google.meeting += result[i].mainsource[j].count;
                                                interactionsFiltred.google.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'google-meeting':
                                                interactionsFiltred.google.meeting += result[i].mainsource[j].count;
                                                interactionsFiltred.google.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'calendar-password':
                                                interactionsFiltred.relatas.calander += result[i].mainsource[j].count;
                                                interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'message':
                                                interactionsFiltred.relatas.message += result[i].mainsource[j].count;
                                                interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                            case 'document-share':
                                                interactionsFiltred.relatas.documentShare += result[i].mainsource[j].count;
                                                interactionsFiltred.relatas.total += result[i].mainsource[j].count;
                                                interactionsFiltred.grandTotal += result[i].mainsource[j].count;
                                                break;
                                        };
                                        break;
                                }
                            }
                        }
                        callback();

                    });

                }, function(err) {
                    if (err) return next(err);
                    callback();
                });
            }
        ], function(err) {
            if (err) return next(err);
            removeUserHavingZero = interactionsFiltred.filter(function(el) {
                if (el.grandTotal > 0)
                    return el;
            });
            callback({array:removeUserHavingZero,users:removeUserHavingZero.length});
        });
    };

    this.getUniqueContactsInteractedByDate = function(ownerId, minDate, maxDate, callback){
        InteractionsCollection.aggregate([
            {$match:{
                "ownerId" : ownerId, 
                userId:{$ne: ownerId},
                interactionDate:{$gt:new Date(minDate), $lt:new Date(maxDate)} 
                }
            },
            {$project:{
                    emailOrMobile:{$cond:{ if:"$emailId", then:"$emailId", else:"$mobileNumber" }},
                    firstName: "$firstName",
                    lastName: "$lastName",
                }
            },
            {$group:{
                    _id:"$emailOrMobile",
                    firstName:{$first: "$firstName"},
                    lastName: {$first: "$lastName"},
                }
            }
        ]).exec(function(error, uniqueContacts){
            if(error)
                callback(error, []);
            else
                callback(null, uniqueContacts)
        })
    }

    this.getContactsInteractedForFirstTimeByDate = function(ownerId, minDate, maxDate, callback){
        InteractionsCollection.aggregate([
            {$match: {ownerId:ownerId, userId:{$ne: ownerId} } },
            {$project:{
                emailOrMobile:{$cond:{ if:"$emailId", then:"$emailId", else:"$mobileNumber" }},
                firstName: "$firstName",
                lastName: "$lastName",
                interactionDate:"$interactionDate"
                }
            },
            {$group:{
                _id:"$emailOrMobile",
                firstName:{$first: "$firstName"},
                lastName: {$first: "$lastName"},
                interactionDate:{$min:"$interactionDate"},
                }
            },
            {$match:{ interactionDate:{$gt: new Date(minDate), $lt: new Date(maxDate)}}}
        ]).exec(function(error, uniqueContacts){
            if(error)
                callback(error, []);
            else
                callback(null, uniqueContacts)
        })
    }

    this.topCompaniesInteractedByDate = function(ownerId, minDate, maxDate, callback){
        InteractionsCollection.aggregate([
            {$match: {ownerId:ownerId, userId:{$ne: ownerId} } },
            {$match:{ interactionDate:{$gt: new Date(minDate), $lt: new Date(maxDate)}}},
            {$group:{
                _id:"$companyName",
                interactions:{$sum:1}
            }
            },
            {$match:{_id:{$nin:[null, '']}}},
            {$sort:{interactions:-1}},
            {$limit:30},
            {$project:{
                _id:0,
                company:"$_id",
                interactions:"$interactions"
            }}]
        ).exec(function(error, companies){
            if(error)
                callback(error, []);
            else
                callback(null, companies)
        })
    }

    this.addInteractionsMeeting = function(userId,interactionsArr,callback){

        var refIds = [];
        for(var i=0; i<interactionsArr.length; i++){
            refIds.push(interactionsArr[i].refId);
        }


        InteractionsCollection.aggregate([
          {$match:{ownerId: userId}}
            ,{$match:{"refId":{$in:refIds}}}
            ,{$group:{_id:null,list:{$push:"$_id"}}}
        ]).exec(function(error,result){
            if(error){
                logger.info('Error in addInteractionsMeeting():interactionsManagement ',error);
                callback(false);
            }
            else if(result && result.length > 0 && result[0] && result[0].list && result[0].list.length > 0){
                var nonExistsInteractions = [];
                for(var i=0; i<interactionsArr.length; i++){
                    var exists = false;
                    for(var j=0; j<result[0].list.length; j++){
                        if(interactionsArr[i].emailId == result[0].list[j].ownerEmailId){
                            exists = true;
                        }
                    }

                    if(!exists){
                        nonExistsInteractions.push(interactionsArr[i]);
                    }
                }
                
                if(nonExistsInteractions.length > 0){
                    addAllInteractions(userId,nonExistsInteractions,callback)
                }
                else{
                    callback(true);
                }
            }
            else{
                addAllInteractions(userId,interactionsArr,callback);
            }
        })
    };

    // Interactions for given number of days
    this.losingTouchByDate = function(userId,emailIdArr,minDate,back){
        var date;
        if(!minDate){
            date = new Date();
            date.setHours(0,0,0,0);
            date.setDate(date.getDate() - 15);
        }
        else date = new Date(minDate);

        interaction.aggregate([
            {
                $match:{
                    userId:userId
                }
            },
            {
                $unwind:"$interactions"
            },
            {
                $match:{
                    "interactions.userId":{ $ne:userId },
                    "interactions.emailId":{$in:emailIdArr}
                }
            },
            {
                $group: {
                    _id: "$interactions.emailId",
                    lastInteracted:{$max:"$interactions.interactionDate"},
                    count:{$sum:1},
                    interactions:{
                        $push:{
                            "_id" : "$interactions._id",
                            "refId" : "$interactions.refId",
                            "userId" : "$interactions.userId",
                            "emailId" : "$interactions.emailId",
                            "interactionType" : "$interactions.interactionType",
                            "subType" : "$interactions.subType",
                            "action" : "$interactions.action",
                            "interactionDate" : "$interactions.interactionDate",
                            "createdDate" : "$interactions.createdDate",
                            "source" : "$interactions.source",
                            "title" : "$interactions.title",
                            "description" : "$interactions.dlastInteractedescription",
                            "endDate" : "$interactions.endDate",
                            "firstName" : "$interactions.firstName",
                            "lastName" : "$interactions.lastName",
                            "publicProfileUrl" : "$interactions.publicProfileUrl",
                            "profilePicUrl" : "$interactions.profilePicUrl",
                            "mobileNumber" : "$interactions.mobileNumber",
                            "companyName" : "$interactions.companyName",
                            "designation" : "$interactions.designation"
                        }
                    }
                }
            },
            {
                $match:{
                    lastInteracted:{$lte:date}
                }
            },
            {
                $sort:{lastInteracted:1,"interactions.interactionDate":-1}
            },
            {
                $unwind:"$interactions"
            },
            {
                $project:{
                    _id:1,
                    lastInteracted:1,
                    interaction:{
                        $cond:{if:{$eq:["$lastInteracted","$interactions.interactionDate"]},then:"$interactions",else:false}
                    }
                }
            },
            {
                $match:{
                    interaction:{$ne:false}
                }
            },
            {
                $group:{
                    _id:null,
                    losingTouch:{
                        $push:{
                            _id: "$interaction._id",
                            "userId" : "$interaction.userId",
                            "emailId" : "$interaction.emailId",
                            "type" : "$interaction.type",
                            "interactionDate" : "$interaction.interactionDate",
                            "firstName" : "$interaction.firstName",
                            "lastName" : "$interaction.lastName",
                            "profilePicUrl" : "$interaction.profilePicUrl",
                            "companyName" : "$interaction.companyName",
                            "designation" : "$interaction.designation",
                            "mobileNumber":"$interaction.mobileNumber"
                        }
                    }
                }
            }
        ]).exec(function(error,result){
            if(!error && checkRequired(result) && result.length > 0){
                back(result[0].losingTouch);
            }
            else back([])
        });
    };

    this.losingTouchCountByDate = function(userId,emailIdArr,minDate,back){
        var date;
        if(!minDate){
            date = new Date();
            date.setHours(0,0,0,0);
            date.setDate(date.getDate() - 15);
        }
        else date = new Date(minDate);

        interaction.aggregate([
            {
                $match:{userId:userId}
            },
            {
                $unwind:"$interactions"
            },
            {
                $match:{
                    "interactions.userId":{$ne:userId},
                    "interactions.emailId":{$in:emailIdArr}
                }
            },
            {
                $group: {
                    _id: {mobileNumber:"",emailId:"$interactions.emailId"},
                    firstName:{$addToSet:"$interactions.firstName"},
                    lastName:{$addToSet:"$interactions.lastName"},
                    userId:{$addToSet:"$interactions.userId"},
                    publicProfileUrl:{$addToSet:"$interactions.publicProfileUrl"},
                    lastInteracted:{$max:"$interactions.interactionDate"},
                    count: {
                        $sum: 1
                    }
                }
            },
            {
                $match:{
                    lastInteracted:{$lte:date}
                }
            },
            {
                $group:{
                    _id:null,
                    losingTouchCount:{$sum:1}
                }
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                back(interactions[0].losingTouchCount);
            }
            else back([])
        });
    };

    this.pastSevenDayInteractionStatus = function(userId,emailId,dateObj,allDetails,back){
        var date = new Date(dateObj.start);
        var today = new Date(dateObj.end);

        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId
                }
            },
            {
                $match:{
                    "userId":{ $ne:userId },
                    "emailId":{ $ne:emailId },
                    "interactionType":"email"
                }
            },
            {
                $group: {
                    _id: "$emailId",
                    interactions:{
                        $push:{
                            _id: "$_id",
                            refId: "$refId",
                            userId: "$userId",
                            emailId: "$emailId",
                            type: "$interactionType",
                            subType: "$subType",
                            action: "$action",
                            interactionDate: "$interactionDate",
                            createdDate: "$createdDate",
                            source: "$source",
                            title: "$title",
                            description: "$description",
                            endDate: "$endDate"
                        }
                    },
                    count: {
                        $sum: 1
                    }
                }
            },
            {
                $sort:{
                    "interactions.interactionDate":-1
                }
            }
        ]).exec(function(err,interactions){

            if(!err && checkRequired(interactions) && interactions.length > 0){

                var youNotReplied = [];
                var youNotReplied2 = [];
                var othersNotReplied = [];
                var othersNotReplied2 = [];
                for(var i=0; i<interactions.length; i++){

                    if(interactions[i].interactions.length == 1){
                        if(interactions[i].interactions[0].action == 'receiver'){
                            if(checkRequired(interactions[i].interactions[0].userId)){
                                othersNotReplied2.push(interactions[i].interactions[0])
                            }else othersNotReplied.push(interactions[i].interactions[0]);
                        }else{
                            if(checkRequired(interactions[i].interactions[0].userId)){
                                youNotReplied2.push(interactions[i].interactions[0]);
                            }else youNotReplied.push(interactions[i].interactions[0])
                        }
                    }
                    else if(interactions[i].interactions.length > 1){

                        var top = interactions[i].interactions[interactions[i].interactions.length - 1];
                        if(top.action == 'receiver'){
                            var flag1 = false;
                            var j1 = 0;
                            for(var j=0; j<interactions[i].interactions.length; j++){
                                if(interactions[i].interactions[j].action == 'sender'){
                                    flag1 = true;
                                    j1 = j;
                                }
                            }
                            if(!flag1){
                                if(checkRequired(interactions[i].interactions[j1].userId)){
                                    othersNotReplied2.push(interactions[i].interactions[j1])
                                }else
                                    othersNotReplied.push(interactions[i].interactions[j1]);
                            }
                        }
                        else{

                            var flag2 = false;
                            var k2 = 0;
                            for(var k=0; k<interactions[i].interactions.length; k++){
                                if(interactions[i].interactions[k].action == 'receiver'){
                                    flag2 = true;
                                    k2 = j;
                                }
                            }
                            if(!flag2){
                                if(checkRequired(interactions[i].interactions[k2].userId)){
                                    youNotReplied2.push(interactions[i].interactions[k2]);
                                }else
                                    youNotReplied.push(interactions[i].interactions[k2]);
                            }
                        }
                    }
                }
                if(allDetails){
                    if(youNotReplied2.length > 0){
                        myUserCollection.populate(youNotReplied2,{path:'userId',select:'firstName lastName emailId'},function(err,arr){
                            if(err){
                                nextPopulateUser(othersNotReplied2,function(err2,arr2){
                                    if(err2){
                                        youNotReplied = youNotReplied.concat(youNotReplied2);
                                        othersNotReplied = othersNotReplied.concat(othersNotReplied2)
                                        back({youNotReplied:youNotReplied,othersNotReplied:othersNotReplied});
                                    }
                                    else{
                                        youNotReplied = youNotReplied.concat(youNotReplied2);
                                        othersNotReplied = othersNotReplied.concat(arr2)
                                        back({youNotReplied:youNotReplied,othersNotReplied:othersNotReplied});
                                    }
                                })
                            }
                            else{
                                nextPopulateUser(othersNotReplied2,function(err2,arr2){
                                    if(err2){
                                        youNotReplied = youNotReplied.concat(arr);
                                        othersNotReplied = othersNotReplied.concat(othersNotReplied2)
                                        back({youNotReplied:youNotReplied,othersNotReplied:othersNotReplied});
                                    }
                                    else{
                                        youNotReplied = youNotReplied.concat(arr);
                                        othersNotReplied = othersNotReplied.concat(arr2)
                                        back({youNotReplied:youNotReplied,othersNotReplied:othersNotReplied});
                                    }
                                })
                                //nextPopulateUser(arr,othersNotReplied2)
                            }
                        })
                    }
                    else{
                        youNotReplied = youNotReplied.concat(youNotReplied2);
                        nextPopulateUser(othersNotReplied2,function(err2,arr2){
                            if(err2){
                                othersNotReplied = othersNotReplied.concat(othersNotReplied2);
                                back({youNotReplied:youNotReplied,othersNotReplied:othersNotReplied});
                            }
                            else{
                                othersNotReplied = othersNotReplied.concat(arr2);
                                back({youNotReplied:youNotReplied,othersNotReplied:othersNotReplied});
                            }
                        })
                    }

                }else{
                    youNotReplied = youNotReplied.concat(youNotReplied2);
                    othersNotReplied = othersNotReplied.concat(othersNotReplied2);
                    back({youNotReplied:youNotReplied,othersNotReplied:othersNotReplied});
                }

            }
            else{
                back({youNotReplied:[],othersNotReplied:[]})
            }
        });
    };

    this.interactionIndividualCountByLocation = function(userId,emailId,emailIdArr,location,designation,dateMax,sortByDate,back){

        var search = new RegExp(location,'i');

        var match = {
            // "interactions.location":search,
            "emailId":{$ne:emailId},
            "userId":{$ne:userId},
            "interactionDate":{$lte:new Date(dateMax)}
        };

        if(emailIdArr && emailIdArr.length > 0){
            match["emailId"] = {$in:emailIdArr}
        }

        if(checkRequired(designation)){
            match["designation"] = new RegExp(designation,'i');
        }

        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId
                }
            },
            {
                $match:match
            },
            {
                $group: {
                    _id: {mobileNumber:"$mobileNumber",emailId:"$emailId"},
                    publicProfileUrl:{$addToSet:"$publicProfileUrl"},
                    lastInteracted:{$max:"$interactionDate"},
                    obj:{$addToSet:{userId:"$userId",firstName:"$firstName",lastName:"$lastName",date:"$interactionDate"}},
                    count: {
                        $sum: 1
                    }
                }
            },
            {
                $sort:{
                    lastInteracted:-1
                }
            }
        ]).exec(function(err,interactions){

            if(!err && checkRequired(interactions) && interactions.length > 0){
                getContactsMatched(userId,emailIdArr,interactions,sortByDate,true,false,true,back);
            }
            else{
                getContactsMatched(userId,emailIdArr,[],sortByDate,true,false,true,back);
            }
        });
    };

    this.losingTouchByDateORDaysGetList_location_designation = function(userId,emailIdArr,minDate,location,designation,sortByDate,back){
        var date;
        if(!minDate){
            date = new Date();
            date.setHours(0,0,0,0);
            date.setDate(date.getDate() - 15);
        }
        else date = new Date(minDate);

        var search = new RegExp(location,'i');
        var match = {
            "interactions.location":search,
            "interactions.userId":{$ne:userId}
        };

        if(checkRequired(designation)){
            match["interactions.designation"] = new RegExp(designation,'i');
        }
        if(checkRequired(emailIdArr) && emailIdArr.length > 0){
            match["interactions.emailId"] = {$in:emailIdArr};
        }

        interaction.aggregate([
            {
                $match:{userId:userId}
            },
            {
                $unwind:"$interactions"
            },
            {
                $match:match
            },
            {
                $group: {
                    _id: {mobileNumber:"",emailId:"$interactions.emailId"},
                    publicProfileUrl:{$addToSet:"$interactions.publicProfileUrl"},
                    lastInteracted:{$max:"$interactions.interactionDate"},
                    obj:{$addToSet:{userId:"$interactions.userId",firstName:"$interactions.firstName",lastName:"$interactions.lastName",date:"$interactions.interactionDate"}},
                    count: {
                        $sum: 1
                    }
                }
            },
            {
                $match:{
                    lastInteracted:{$lte:date}
                }
            },
            {
                $sort:{lastInteracted:1}
            },
            {
                $project:{
                    _id:1,
                    lastInteracted:1,
                    count:1,
                    publicProfileUrl:1,
                    obj:1
                }
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                back(getFinalArr(interactions,sortByDate,false,false,true));
            }
            else back([])
        });
    };

    this.losingTouchByDateORDaysGetList = function(userId,emailIdArr,minDate,sortByDate,back){
        var date;
        if(!minDate){
            date = new Date();
            date.setHours(0,0,0,0);
            date.setDate(date.getDate() - 30);
        }
        else date = new Date(minDate);

        var match = {
            "userId":{$ne:userId}
        };

        if(checkRequired(emailIdArr) && emailIdArr.length > 0){
            match["emailId"] = {$in:emailIdArr};
        }

        InteractionsCollection.aggregate([
            {
                $match:{ownerId:userId}
            },
            {
                $match:match
            },
            {
                $group: {
                    _id: {mobileNumber:"",emailId:"$emailId"},
                    publicProfileUrl:{$addToSet:"$publicProfileUrl"},
                    lastInteracted:{$max:"$interactionDate"},
                    obj:{$addToSet:{userId:"$userId",firstName:"$firstName",lastName:"$lastName",date:"$interactionDate"}},
                    count: {
                        $sum: 1
                    }
                }
            },
            {
                $match:{
                    lastInteracted:{$lte:date}
                }
            },
            {
                $sort:{lastInteracted:1}
            },
            {
                $project:{
                    _id:1,
                    lastInteracted:1,
                    count:1,
                    publicProfileUrl:1,
                    obj:1
                }
            }
        ]).exec(function(err,interactions){

            if(!err && checkRequired(interactions) && interactions.length > 0){
                back(getFinalArr(interactions,sortByDate,false,true,true));
            }
            else back([])
        });
    };

    this.getMeetingsBeforeAndNext = function(userId,dateMin,dateMax,refId,meetingDate,callback){

        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId
                }
            },
            {
                $match:{
                    "userId":{$ne:userId},
                    "refId":{$ne:refId},
                    "interactionType":'meeting',
                    "endDate":{$exists:true,$ne:null},
                    "interactionDate":{$gte:new Date(dateMin),$lte:new Date(dateMax)}
                }
            },
            {
                $project:{
                    "_id":"$_id",
                    "profilePicUrl":"$profilePicUrl",
                    "userId":"$userId",
                    firstName:"$firstName",
                    emailId:"$emailId",
                    startDate:"$interactionDate",
                    endDate:"$endDate",
                    locationType:"$subType",
                    meeting:"$refId",
                    position:{$cond:{if:{$lt:[new Date(meetingDate),"$interactionDate"]},then:"after",else:"before"}}
                }
            },
            {
                $group:{
                    _id:"$position",
                    meetings:{
                        $push:{
                            _id:"$_id",
                            profilePicUrl:"$profilePicUrl",
                            userId:"$userId",
                            startDate:"$startDate",
                            endDate:"$endDate",
                            locationType:"$locationType",
                            meeting:"$meeting",
                            position:"$position",
                            firstName:"$firstName",
                            emailId:"$emailId"
                        }
                    }
                }
            }
        ]).exec(function(err,interactions){

            if(!err && checkRequired(interactions) && interactions.length > 0){
                var invitationIds = [];
                var finalObj = [];
                for(var i=0; i<interactions.length; i++){
                    if(interactions[i].meetings && interactions[i].meetings.length > 0){
                        if(interactions[i]._id == 'after'){
                            interactions[i].meetings.sort(function (o1, o2) {
                                return new Date(o1.startDate) < new Date(o2.startDate) ? -1 : new Date(o1.startDate) > new Date(o2.startDate) ? 1 : 0;
                            });
                        }
                        else{
                            interactions[i].meetings.sort(function (o1, o2) {
                                return new Date(o1.startDate) > new Date(o2.startDate) ? -1 : new Date(o1.startDate) < new Date(o2.startDate) ? 1 : 0;
                            });
                        }

                        invitationIds.push(interactions[i].meetings[0].meeting);

                        if(interactions[i].meetings.length > 1)
                            interactions[i].meetings = interactions[i].meetings.slice(0,1);

                        var obj = {
                            position:interactions[i]._id,
                            _id:interactions[i].meetings[0]._id,
                            profilePicUrl:interactions[i].meetings[0].profilePicUrl,
                            userId:interactions[i].meetings[0].userId,
                            firstName:interactions[i].meetings[0].firstName,
                            emailId:interactions[i].meetings[0].emailId,
                            invitationId:interactions[i].meetings[0].meeting,
                            startDate:interactions[i].meetings[0].startDate,
                            endDate:interactions[i].meetings[0].endDate,
                            locationType:interactions[i].meetings[0].locationType
                        };
                        finalObj.push(obj)
                    }
                }
                callback(finalObj)
            }
            else callback([])
        })
    };

    this.interactionsByDateMinMaxDashboardWithoutTasks = function(userId,dateMin,dateMax,projection,back){
        interaction.aggregate([
            {
                $match:{
                    userId:userId
                }
            },
            {
                $unwind:"$interactions"
            },
            {
                $match:{
                    "interactions.userId":{$ne:userId},
                    "interactions.interactionType":'meeting',
                    "interactions.interactionDate":{$gte:new Date(dateMin),$lte:new Date(dateMax)}
                }
            },
            {
                $group:{
                    _id:null,
                    refArr:{ $addToSet: "$interactions.refId" }
                }
            }
        ]).exec(function ( e, d ) {

            if(e){
                back([]);
            }
            else{
                if(checkRequired(d) && d.length > 0 && d[0] && d[0].refArr && d[0].refArr.length > 0){
                    var projectionNew = {invitationId:1,senderId:1,senderName:1,senderEmailId:1,scheduleTimeSlots:1,suggested:1,suggestedBy:1,to:1,toList:1,selfCalendar:1,isGoogleMeeting:1,senderPicUrl:1};
                    if(checkRequired(projection))
                        projectionNew = projection

                    scheduleInvitation.find({invitationId:{$in:d[0].refArr}},projectionNew).exec(function(error,meetings){
                        if(checkRequired(meetings) && meetings.length > 0){
                            back(meetings)
                        }else back([])
                    })
                }else back([]);
            }
        })
    };

    this.interactionsByDateMinMaxDashboardWithoutTasksFuture = function(userId,dateMin,back){

        interaction.aggregate([
            {
                $match:{
                    userId:userId
                }
            },
            {
                $unwind:"$interactions"
            },
            {
                $match:{
                    "interactions.interactionType":'meeting',
                    "interactions.interactionDate":{$gte:new Date(dateMin)}
                }
            },
            {
                $group:{
                    _id:null,
                    refArr:{ $addToSet: "$interactions.refId" }
                }
            }
        ]).exec(function ( e, d ) {
            if(e){
                back([]);
            }
            else{
                if(checkRequired(d) && d.length > 0 && d[0] && d[0].refArr && d[0].refArr.length > 0){
                    var projection = {invitationId:1,senderId:1,senderName:1,senderEmailId:1,scheduleTimeSlots:1,suggested:1,suggestedBy:1,to:1,toList:1,selfCalendar:1,isGoogleMeeting:1,senderPicUrl:1}
                    scheduleInvitation.find({invitationId:{$in:d[0].refArr}},projection).exec(function(error,meetings){
                        if(checkRequired(meetings) && meetings.length > 0){
                            back(meetings)
                        }else back([])
                    })
                }else back([]);
            }
        })
    };

    this.getInteractionInitiationsCount = function(userId,emailId,contactEmailId,isArray,dateMin,dateMax,mobileNumber,source,callback){
        var isEmail = true
        if(!validateEmail(contactEmailId)) {
            isEmail = false
        }

        var q = {
            "emailId":contactEmailId,
            "interactionDate":{
                $gte:new Date(dateMin),$lte:new Date(dateMax)
            },
            "userId": {$ne: userId}
        };

        if(checkRequired(contactEmailId) && checkRequired(mobileNumber)){

            var mobileNumberRegEx = mobileNumber.length <= 10 ? mobileNumber : mobileNumber.substr(mobileNumber.length - 10);

            q = {
                "interactionDate":{
                    $gte:new Date(dateMin),$lte:new Date(dateMax)
                },
                "userId": {$ne: userId}
            }

            q.$or = [{"emailId":contactEmailId},{"mobileNumber":new RegExp('\d{0,4}'+mobileNumberRegEx+'$')}]
        }


        if(isArray){
            q = {
                "emailId":{$in:contactEmailId},
                "interactionDate":{
                    $gte:new Date(dateMin),$lte:new Date(dateMax)
                },
                "userId": {$ne: userId}
            };
        }

        if(isNumber(contactEmailId) && !isEmail){
            var mobile10Digit = contactEmailId.length <= 10 ? contactEmailId : contactEmailId.substr(contactEmailId.length - 10);
            q = {
                //"mobileNumber": contactEmailId,
                "mobileNumber": new RegExp('\d{0,4}'+mobile10Digit+'$'),
                "interactionDate":{
                    $gte:new Date(dateMin),$lte:new Date(dateMax)
                },
                "userId": {$ne: userId}
            }
        }

        if(_.includes(source, 'outlook')){
            q["source"] = {$ne:"google"}
        } else {
            q["source"] = {$ne:"outlook"}
        }

        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId
                }
            },
            {
                $match:q
            },
            {
                $group: {
                    _id:"$refId",
                    interactionType: { "$first": "$interactionType" },
                    action: { "$first": "$action" }
                }
            },
            {
                $group: {
                    _id: "$action",
                    connectedVia:{$addToSet:"$interactionType"},
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                callback(interactions);
            }
            else callback([])
        });
    };

    this.interactionWithContactByTypes = function(userId,contactEmailId,isArray,dateMin,dateMax,mobileNumber,source,back){

        var isEmail = true
        if(!validateEmail(contactEmailId)) {
            isEmail = false
        }

        var q = {"emailId":contactEmailId,"userId": {$ne: userId}};
        if(isArray){
            q = {"emailId":{$in:contactEmailId}};
        }

        if(isEmail && checkRequired(contactEmailId) && checkRequired(mobileNumber)){
            q = {"userId": {$ne: userId}};
            var mobile10Digit = mobileNumber.length <= 10 ? mobileNumber : mobileNumber.substr(mobileNumber.length - 10);
            q.$or = [{"emailId":contactEmailId},{"mobileNumber":new RegExp('\d{0,4}'+mobile10Digit+'$')}]
        }

        if(isNumber(contactEmailId) && !isEmail){
            var mobile10Digit = contactEmailId.length <= 10 ? contactEmailId : contactEmailId.substr(contactEmailId.length - 10);
            q = {
                "mobileNumber": new RegExp('\d{0,4}'+mobile10Digit+'$'),
                //"mobileNumber": contactEmailId,
                "userId": {$ne: userId}
            }
        }

        if(dateMin != null && dateMax != null){
            q["interactionDate"] = {$gte:new Date(dateMin),$lte:new Date(dateMax)}
        }
        else if(dateMax != null && dateMin == null){
            q["interactionDate"] = {$lte:new Date(dateMax)}
        }
        
        if(_.includes(source, 'outlook')){
            q["source"] = {$ne:"google"}
        } else {
            q["source"] = {$ne:"outlook"}
        }

        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId
                }
            },
            {
                $match:q
            },
            {
                $group:{
                    _id:"$interactionType",
                    count:{$sum:1}
                }
            },
            {
                $group:{
                    _id:null,
                    totalCount:{$sum:"$count"},
                    maxCount:{$max:"$count"},
                    typeCounts:{$push:{_id:"$_id",count:"$count"}}
                }
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                back(interactions);
            }
            else back([])
        });
    };

    this.getInteractedWithCountList_top_limit = function(userId,emailIdArr,dateMin,dateMax,limit,callback){

        var match = {
            "interactions.emailId":{$in:emailIdArr},
            "interactions.userId":{$ne:userId},
            "interactions.interactionDate":{
                $gte:new Date(dateMin),$lte:new Date(dateMax)
            }
        };

        interaction.aggregate([
            {
                $match:{userId:userId}
            },
            {
                $unwind:"$interactions"
            },
            {
                $match:match
            },
            {
                $group: {
                    _id:"$interactions.emailId",
                    lastInteracted:{$max:"$interactions.interactionDate"},
                    count: {
                        $sum: 1
                    }
                }
            },
            {
                $sort:{count:-1}
            },
            {
                $limit:limit
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                callback(interactions);
            }
            else callback([])
        });
    };

    this.getInteractedLastInteractedType = function(userId,emailIdArr,dateMin,dateMax,limit,callback){

        var match = {
            "interactions.emailId":{$in:emailIdArr},
            "interactions.userId":{$ne:userId},
            "interactions.interactionDate":{
                $gte:new Date(dateMin),$lte:new Date(dateMax)
            }
        };

        interaction.aggregate([
            {
                $match:{userId:userId}
            },
            {
                $unwind:"$interactions"
            },
            {
                $match:match
            },
            {
                $group: {
                    _id:"$interactions.emailId",
                    lastInteracted:{$max:"$interactions.interactionDate"},
                    count: {
                        $sum: 1
                    }
                }
            },
            {
                $sort:{count:-1}
            },
            {
                $limit:limit
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                callback(interactions);
            }
            else callback([])
        });
    };

    this.calculateRelationshipStrength = function(userId,contactEmailId,dateMin,dateMax,mobileNumber,callback){

        var isEmail = true
        if(!validateEmail(contactEmailId)) {
            isEmail = false
        }

        var q = {"userList.emailId":contactEmailId,"userId": {$ne: userId}};
        var unwindQuery = "$userList.emailId";
        var unwindQuery2 = "$emailId";

        if(isNumber(contactEmailId) && !isEmail){
            var mobile10Digit = contactEmailId.length <= 10 ? contactEmailId : contactEmailId.substr(contactEmailId.length - 10);
            q = {
                "mobileNumber": new RegExp('\d{0,4}'+mobile10Digit+'$'),
                "userId": {$ne: userId}
            }
            unwindQuery = "$userList.mobileNumber";
            unwindQuery2 = "$mobileNumber";
        }

        if(isEmail && checkRequired(contactEmailId) && checkRequired(mobileNumber)){
            q = {"userId": {$ne: userId}};
            var mobile10Digit = mobileNumber.length <= 10 ? mobileNumber : mobileNumber.substr(mobileNumber.length - 10);
            q.$or = [{"userList.emailId":contactEmailId},{"userList.mobileNumber":new RegExp('\d{0,4}'+mobile10Digit+'$')}]
        }

        var aggregateQuery = [
            {
                $match:{ownerId:userId}
            },
            {
                $match:{
                    "interactionDate":{$gte:new Date(dateMin),$lte:new Date(dateMax)},
                    "userId":{$ne:userId}
                }
            },
            {
                $group:{
                    _id: {mobileNumber:"",emailId:"$emailId"},
                    userId:{$addToSet:"$userId"},
                    emailId:{$addToSet:"$emailId"},
                    mobileNumber:{$addToSet:"$mobileNumber"},
                    count: {
                        $sum: 1
                    }
                }
            },
            {
                $group:{
                    _id:null,
                    userList:{$push:{userId:"$userId",count:"$count",emailId:"$emailId",mobileNumber:"$mobileNumber"}},
                    maxCount:{$max:"$count"},
                    minCount:{$min:"$count"}
                }
            },
            {
                $unwind:"$userList"
            },
            {
                $unwind:unwindQuery
            },
            {
                $match:q
            },
            {
                $group:{
                    _id:null,
                    count:{$sum:"$userList.count"},
                    emailId:{$addToSet:"$userList.emailId"},
                    mobileNumber:{$addToSet:"$userList.mobileNumber"},
                    maxCount:{$max:"$maxCount"},
                    minCount:{$min:"$minCount"}
                }
            },
            {
                $unwind:unwindQuery2
            }
        ];

        if(isEmail && checkRequired(contactEmailId) && checkRequired(mobileNumber)){

            aggregateQuery = [
                {
                    $match:{ownerId:userId}
                },
                {
                    $match:{
                        "interactionDate":{$gte:new Date(dateMin),$lte:new Date(dateMax)},
                        "userId":{$ne:userId}
                    }
                },
                {
                    $group:{
                        _id: {mobileNumber:"",emailId:"$emailId"},
                        userId:{$addToSet:"$userId"},
                        emailId:{$addToSet:"$emailId"},
                        mobileNumber:{$addToSet:"$mobileNumber"},
                        count: {
                            $sum: 1
                        }
                    }
                },
                {
                    $group:{
                        _id:null,
                        userList:{$push:{userId:"$userId",count:"$count",emailId:"$emailId",mobileNumber:"$mobileNumber"}},
                        maxCount:{$max:"$count"},
                        minCount:{$min:"$count"}
                    }
                },
                {
                    $unwind:"$userList"
                },
                {
                    $unwind:"$userList.userId"
                },
                {
                    $unwind:unwindQuery
                },
                {
                    $unwind:"$userList.mobileNumber"
                },
                {
                    $match:q
                },
                {
                    $group:{
                        _id:null,
                        count:{$sum:"$userList.count"},
                        emailId:{$addToSet:"$userList.emailId"},
                        mobileNumber:{$addToSet:"$userList.mobileNumber"},
                        maxCount:{$max:"$maxCount"},
                        minCount:{$min:"$minCount"}
                    }
                },
                {
                    $unwind:unwindQuery2
                },
                {
                    $unwind:'$mobileNumber'
                }
            ];
        }

        InteractionsCollection.aggregate(aggregateQuery).exec(function(error,result){
            if(!error){
                callback(result)
            } else {
                callback([])
            }
        })
    };

    this.getInteractionsByDateTimeline = function(userId,cUserId,isEmailId,limit,dateMin,mobileNumber,source,back){

        var isEmail = true
        if(!validateEmail(cUserId)) {
            isEmail = false
        }

        var q = {

            "interactionDate":{
                $lte:new Date(dateMin)
            },
            "userId": {$ne: userId}
        };

        if(isEmailId){
            q["emailId"] = cUserId;
        }
        else q["userId"] = cUserId;

        if(isEmailId && checkRequired(cUserId) && checkRequired(mobileNumber)){

            var mobileNumberRegEx = mobileNumber.length <= 10 ? mobileNumber : mobileNumber.substr(mobileNumber.length - 10);

            q = {
                "interactionDate":{
                    $lte:new Date(dateMin)
                },
                "userId": {$ne: userId}
            }

            q.$or = [{"emailId":cUserId},{"mobileNumber":new RegExp('\d{0,4}'+mobileNumberRegEx+'$')}]
            // q.$or = [{"emailId":cUserId},{"mobileNumber":mobileNumber}]
        }

        if(_.includes(source, 'outlook')){
            q["source"] = {$ne:"google"}
        } else {
            q["source"] = {$ne:"outlook"}
        }

        if(isNumber(cUserId) && !isEmail){
            var mobile10Digit = cUserId.length <= 10 ? cUserId : cUserId.substr(cUserId.length - 10);
            q = {
                "mobileNumber": new RegExp('\d{0,4}'+mobile10Digit+'$'),
                "userId": {$ne: userId}
            }
        }

        if(!cUserId && mobileNumber){
            mobile10Digit = mobileNumber.length <= 10 ? mobileNumber : mobileNumber.substr(mobileNumber.length - 10);
            q = {
                "mobileNumber": new RegExp('\d{0,4}'+mobile10Digit+'$'),
                "userId": {$ne: userId}
            }
        }

        InteractionsCollection.aggregate([
            {
                $match:{ownerId:userId}
            },
            {
                $match:q
            },
            {
                $project: {
                    _id: "$_id",
                    emailId: "$emailId",
                    firstName: "$firstName",
                    userId: "$userId",
                    publicProfileUrl: "$publicProfileUrl",
                    interactionDate: "$interactionDate",
                    interactionType: "$interactionType",
                    subType: "$subType",
                    action: "$action",
                    title: "$title",
                    refId: "$refId",
                    description: "$description",
                    trackInfo: "$trackInfo",
                    trackId: "$trackId",
                    ignore: "$ignore",
                    createdDate: "$createdDate",
                    emailContentId: "$emailContentId",
                    googleAccountEmailId: "$googleAccountEmailId",
                    source: "$source",
                    duration: "$duration"
                }
            },
            {
                $group:{
                    _id:"$refId",
                    firstName:{$first:"$firstName"},
                    userId:{$first:"$userId"},
                    publicProfileUrl:{$first:"$publicProfileUrl"},
                    interactionDate:{ "$first": "$interactionDate"},
                    interactionType:{ "$first": "$interactionType"},
                    subType:{ "$first": "$subType"},
                    action:{ "$first": "$action"},
                    emailId:{ "$first": "$emailId"},
                    title:{ "$first": "$title"},
                    refId:{ "$first": "$refId"},
                    description:{ "$first": "$description"},
                    trackInfo:{ "$first": "$trackInfo"},
                    trackId:{ "$first": "$trackId"},
                    ignore:{ "$first": "$ignore"},
                    createdDate:{ "$first": "$createdDate"},
                    emailContentId:{ "$first": "$emailContentId"},
                    googleAccountEmailId:{ "$first": "$googleAccountEmailId"},
                    source:{ "$first": "$source"},
                    duration:{ "$first": "$duration"}
                }
            },
            {
                $sort:{interactionDate:-1}
            },
            {
                $limit:limit
            },
            {
                $group:{
                    _id:null,
                    firstName:{$addToSet:"$firstName"},
                    emailId:{$addToSet:"$emailId"},
                    userId:{$addToSet:"$userId"},
                    publicProfileUrl:{$addToSet:"$publicProfileUrl"},
                    interactions:{$push:{
                        _id: "$_id",
                        interactionDate: "$interactionDate",
                        interactionType: "$interactionType",
                        subType: "$subType",
                        action: "$action",
                        emailId: "$emailId",
                        title: "$title",
                        refId: "$refId",
                        description: "$description",
                        trackInfo: "$trackInfo",
                        trackId: "$trackId",
                        ignore: "$ignore",
                        createdDate: "$createdDate",
                        emailContentId: "$emailContentId",
                        googleAccountEmailId: "$googleAccountEmailId",
                        source: "$source",
                        duration: "$duration"
                    }}
                }
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                back(interactions)
            }
            else back([])
        })
    };

    this.getInteractionsByDateTimeLineTasks = function(userId,cUserId,isEmailId,limit,dateMin,back){

        var q = {
            "interactions.interactionType": "task"
        };

        if(isEmailId){
            q["interactions.emailId"] = cUserId;
        }
        else q["interactions.userId"] = cUserId;

        interaction.aggregate([
            {
                $match:{userId:userId}
            },
            {
                $unwind:"$interactions"
            },
            {
                $match:q
            },
            {
                $project: {
                    _id: "$interactions._id",
                    emailId: "$interactions.emailId",
                    firstName: "$interactions.firstName",
                    interactionDate: "$interactions.interactionDate",
                    interactionType: "$interactions.interactionType",
                    subType: "$interactions.subType",
                    action: "$interactions.action",
                    title: "$interactions.title",
                    description: "$interactions.description",
                    refId: "$interactions.refId",
                    createdDate: "$interactions.createdDate"
                }
            },
            {
                $sort:{interactionDate:-1}
            },
            {
                $limit:limit
            },
            {
                $group:{
                    _id:null,
                    firstName:{$addToSet:"$firstName"},
                    emailId:{$addToSet:"$emailId"},
                    userId:{$addToSet:"$userId"},
                    publicProfileUrl:{$addToSet:"$publicProfileUrl"},
                    interactions:{$push:{
                        _id: "$_id",
                        interactionDate: "$interactionDate",
                        interactionType: "$interactionType",
                        subType: "$subType",
                        action: "$action",
                        title: "$title",
                        description: "$description",
                        refId: "$refId",
                        emailId: "$emailId",
                        createdDate: "$createdDate"
                    }}
                }
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                // populate tasks
                taskCollection.populate(interactions,{path:'interactions.refId',select:'_id status'},function(error,tasks){
                    back(tasks)
                });
            }
            else back([])
        })
    };

    this.getInteractionsByDateTimeLineSocialUpdates = function(userId,cUserId,isEmailId,limit,dateMin,back){

        var q = {
            "interactionType": {$in:["twitter", "linkedin", "facebook"]},
            "interactionDate":{
                $lte:new Date(dateMin)
            }
        };

        if(isEmailId){
            q["emailId"] = cUserId;
        }
        else q["userId"] = cUserId;

        InteractionsCollection.aggregate([
            {
                $match:{ownerId:userId}
            },
            {
                $match:q
            },
            {
                $project: {
                    _id: "$_id",
                    emailId: "$emailId",
                    firstName: "$firstName",
                    interactionDate: "$interactionDate",
                    interactionType: "$interactionType",
                    subType: "$subType",
                    action: "$action",
                    title: "$title",
                    refId: "$refId",
                    createdDate: "$createdDate"
                }
            },
            {
                $sort:{interactionDate:-1}
            },
            {
                $limit:limit
            },
            {
                $group:{
                    _id:null,
                    firstName:{$addToSet:"$firstName"},
                    emailId:{$addToSet:"$emailId"},
                    userId:{$addToSet:"$userId"},
                    publicProfileUrl:{$addToSet:"$publicProfileUrl"},
                    interactions:{$push:{
                        _id: "$_id",
                        interactionDate: "$interactionDate",
                        interactionType: "$interactionType",
                        subType: "$subType",
                        action: "$action",
                        title: "$title",
                        refId: "$refId",
                        createdDate: "$createdDate"
                    }}
                }
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                back(interactions)
            }
            else back([])
        })
    };

    this.getInteractionsByDateSocialUpdatesCount = function(userId,dateMin,back){
        var q = {
            "interactionType": {$in:["twitter", "linkedin", "facebook"]},
            "interactionDate":{$gte:new Date(dateMin)},
            "action": 'receiver',
            "userId": userId
        };

        InteractionsCollection.aggregate([
            {
                $match:{ownerId:userId}
            },
            {
                $match:q
            },
            {
                $group:{
                    _id:null,
                    refIds:{$addToSet:"$refId"}
                }
            },
            {
                $project:{
                    socialUpdates:{$size:"$refIds"}
                }
            }
        ]).exec(function(err,interactions){

            if(!err && checkRequired(interactions) && interactions.length > 0){
                back(interactions[0].socialUpdates);
            }
            else back(0);
        })
    };

    this.documentsSharedWithContact = function(userId,contactEmailId,limit,dateMin,back){

        var q = {
            "emailId": contactEmailId,
            "interactionType": "document-share",
            "interactionDate":{
                $lte:new Date(dateMin)
            }
        };

        InteractionsCollection.aggregate([
            {
                $match:{ownerId:userId}
            },
            {
                $match:q
            },
            {
                $project: {
                    _id: "$_id",
                    emailId: "$emailId",
                    firstName: "$firstName",
                    interactionDate: "$interactionDate",
                    interactionType: "$interactionType",
                    subType: "$subType",
                    action: "$action",
                    title: "$title",
                    refId: "$refId",
                    createdDate: "$createdDate"
                }
            },
            {
                $sort:{interactionDate:-1}
            },
            {
                $limit:limit
            },
            {
                $group:{
                    _id:null,
                    firstName:{$addToSet:"$firstName"},
                    emailId:{$addToSet:"$emailId"},
                    userId:{$addToSet:"$userId"},
                    publicProfileUrl:{$addToSet:"$publicProfileUrl"},
                    interactions:{$push:{
                        _id: "$_id",
                        interactionDate: "$interactionDate",
                        interactionType: "$interactionType",
                        subType: "$subType",
                        action: "$action",
                        title: "$title",
                        refId: "$refId",
                        createdDate: "$createdDate"
                    }}
                }
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                back(interactions)
            }
            else back([])
        });
    };

    this.createInteraction_new = function(userId,interactionDetailsArr){
        var interactionsObjArr = [];
        for(var i=0; i<interactionDetailsArr.length; i++){
            if(checkRequired(interactionDetailsArr[i].refId))
                interactionsObjArr.push(constructInteractionManagementObject_new(interactionDetailsArr[i]))
        }

        if(interactionsObjArr.length > 0){

            interaction.update({userId:userId},{$push:{interactions:{$each:interactionsObjArr}}},function(error,result){
                if(error)
                    logger.info('Error in findOneAndUpdate INTERACTIONS ',error);

            });
        }
        else{
            //logger.info('No refId while storing interaction InteractionType:'+interaction.type+' Source: '+interaction.source+' Subtype: '+interaction.subType+' emailId: '+interaction.emailId);
        }
    };

    this.removeInteractions_new = function(userIdList,refId,callback){

        InteractionsCollection.remove({refId:refId},function(error,result){
            if(error){
                logger.info('Error in removeInteractions():interactionsManagement ',error);
            }
            if(callback){
                callback();
            }
        })
    };

    this.removeUserCanceledInteractions = function(userIdList,refId,userId){
        interaction.update({userId:{$in:userIdList}},{$pull:{interactions:{refId:{$in:[refId]},userId:userId}}},{multi:true},function(error,result){
            if(error){
                logger.info('Error in removeUserCanceledInteractions():interactionsManagement ',error);
            }
        })
    };

    this.interactionIndividualTopFive_new_dashboard = function(userId,emailId,mobileNumber,dateMin,dateMax,back){

        mobileNumber = mobileNumber.replace(/[^a-zA-Z0-9]/g,'');

        interaction.aggregate([
            {
                $match: {userId: userId}
            },
            {
                $unwind: "$interactions"
            },
            {
                $match: {
                    "interactions.userId": {$ne: userId},
                    "interactions.emailId": {$ne: emailId},
                    "interactions.mobileNumber": {$ne: mobileNumber},
                    "interactions.action": {$in: ['sender', 'receiver']},
                    "interactions.interactionDate": {$gte: new Date(dateMin), $lte: new Date(dateMax)}
                }
            },
            {
                $group: {
                    _id: {mobileNumber:"$interactions.mobileNumber",emailId:"$interactions.emailId"},
                    //_id: {mobileNumber:"",emailId:"$interactions.emailId"},
                    publicProfileUrl:{$addToSet:"$interactions.publicProfileUrl"},
                    lastInteracted:{$max:"$interactions.interactionDate"},
                    obj:{$addToSet:{userId:"$interactions.userId",firstName:"$interactions.firstName",lastName:"$interactions.lastName",date:"$interactions.interactionDate"}},
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function (error,topFive) {

            if(error){
                logger.info("Error in interactionIndividualTopFive_new_dashboard():interactionsManagement ",error)
            }
            if(topFive && topFive.length > 0){

                var result = getFinalArr(topFive,false,false,false,true);

                var topFiveClean = result.slice(0,5);
                var emailArr = [];
                var contactExists = [];

                for (var i = 0; i < topFiveClean.length; ++i) {
                    emailArr.push(topFiveClean[i]._id.emailId);
                    var obj = {personEmailId:topFiveClean[i]._id.emailId,personName:topFiveClean[i]._id.emailId}

                    contactObj.addSingleContactNotExist(userId,obj);

                }//end for loop
                back(result.slice(0,5));
            }else back([]);
        });
    };

    //this.topTenSocialMediaToday = function(userId,emailId,emailIdArr,dateMin,dateMax,back){
    //    emailIdArr.push(emailId);
    //    var q = {
    //        // "userId": {$ne: userId},
    //        // "interactions.emailId": {$ne: emailId},
    //        "interactions.action": {$in: ['sender']},
    //        "interactions.interactionType": {$in: ['facebook', 'linkedin', 'twitter']},
    //        "interactions.interactionDate": {$gte: new Date(dateMin), $lte: new Date(dateMax)}
    //    }
    //    if(emailIdArr && emailIdArr.length > 0){
    //        q["interactions.emailId"] = {$in:emailIdArr}
    //    }
    //
    //
    //    interaction.aggregate([
    //        {
    //            $match:{interactions:{$elemMatch:{interactionDate:{$gte: new Date(dateMin), $lte: new Date(dateMax)}}}}
    //        },
    //        {
    //            $unwind: "$interactions"
    //        },
    //        {
    //            $match: q
    //        },
    //        {
    //            $group: {
    //                //_id: {mobileNumber:"$interactions.mobileNumber",emailId:"$interactions.emailId",firstName:"$interactions.firstName"},
    //                _id: {emailId:"$interactions.emailId"},
    //                firstName:{$addToSet:"$interactions.firstName"},
    //                lastName:{$addToSet:"$interactions.lastName"},
    //                userId:{$addToSet:"$interactions.userId"},
    //                publicProfileUrl:{$addToSet:"$interactions.publicProfileUrl"},
    //                lastInteracted:{$max:"$interactions.interactionDate"},
    //                facebook:{$sum:{$cond:{if:{$eq:["$interactions.interactionType","facebook"]},then:1,else:0}}},
    //                linkedin:{$sum:{$cond:{if:{$eq:["$interactions.interactionType","linkedin"]},then:1,else:0}}},
    //                twitter:{$sum:{$cond:{if:{$eq:["$interactions.interactionType","twitter"]},then:1,else:0}}},
    //                count:{$sum:1}
    //            }
    //        },
    //        {
    //            $match:{
    //                $or:[
    //                    //{"_id.mobileNumber":{$ne:null}},
    //                    {"_id.emailId":{$ne:null}}
    //                    //{"_id.firstName":{$ne:null}}
    //                ]
    //            }
    //        },
    //        {
    //            $sort:{count:1}
    //        },
    //        {
    //            $limit:10
    //        },
    //        {
    //            $group:{
    //                _id:null,
    //                facebookMaxValue:{$max:"$facebook"},
    //                linkedinMaxValue:{$max:"$linkedin"},
    //                twitterMaxValue:{$max:"$twitter"},
    //                values:{$push:{
    //                    _id: "$_id",
    //                    firstName:"$firstName",
    //                    lastName:"$lastName",
    //                    userId:"$userId",
    //                    publicProfileUrl:"$publicProfileUrl",
    //                    lastInteracted:"$lastInteracted",
    //                    facebook:"$facebook",
    //                    linkedin:"$linkedin",
    //                    twitter:"$twitter",
    //                    count:"$count"
    //                }}
    //            }
    //        }
    //    ],function (error,topFive) {
    //        if(error){
    //            logger.info("Error in topTenSocialMediaToday():interactionsManagement ",error)
    //        }
    //        if(topFive && topFive.length > 0){
    //            var a=topFive[0];
    //            for(var i=0;i<a.values.length;i++){
    //                a.values[i].facebook/=2;
    //                a.values[i].count-=a.values[i].facebook;
    //                if(a.values[i].userId[0].toString()===userId.toString())
    //                    a.values.splice(i, 1);
    //            }
    //            //for(var i=0;i<a.values.length;i++){
    //            //    a.values[i].facebook/=2;
    //            //    a.values[i].count-=a.values[i].facebook;
    //            //    if(a.values[i].userId[0]==userId)
    //            //        a.values.splice(i, 1);
    //            //}
    //            back(a)
    //        }else back(null);
    //    });
    //};

    this.topTenSocialMediaToday = function(userId,emailId,emailIdArr,dateMin,dateMax,back){
        emailIdArr.push(emailId);
        var q = {
            // "userId": {$ne: userId},
            // "interactions.emailId": {$ne: emailId},
            "interactions.action": {$in: ['sender']},
            "interactions.interactionType": {$in: ['facebook', 'linkedin', 'twitter']},
            "interactions.interactionDate": {$gte: new Date(dateMin), $lte: new Date(dateMax)}
        }
        if(emailIdArr && emailIdArr.length > 0){
            q["interactions.emailId"] = {$in:emailIdArr}
        }

        interaction.aggregate([
            {
                $match:{interactions:{$elemMatch:{interactionDate:{$gte: new Date(dateMin), $lte: new Date(dateMax)}}}}
            },
            {
                $unwind: "$interactions"
            },
            {
                $match: q
            },
            {
                $group: {
                    //_id: {mobileNumber:"$interactions.mobileNumber",emailId:"$interactions.emailId",firstName:"$interactions.firstName"},
                    _id: {emailId:"$interactions.emailId"},
                    firstName:{$addToSet:"$interactions.firstName"},
                    lastName:{$addToSet:"$interactions.lastName"},
                    userId:{$addToSet:"$interactions.userId"},
                    publicProfileUrl:{$addToSet:"$interactions.publicProfileUrl"},
                    lastInteracted:{$max:"$interactions.interactionDate"},
                    facebook:{$sum:{$cond:{if:{$eq:["$interactions.interactionType","facebook"]},then:1,else:0}}},
                    linkedin:{$sum:{$cond:{if:{$eq:["$interactions.interactionType","linkedin"]},then:1,else:0}}},
                    twitter:{$sum:{$cond:{if:{$eq:["$interactions.interactionType","twitter"]},then:1,else:0}}},
                    count:{$sum:1}
                }
            },
            {
                $match:{
                    $or:[
                        //{"_id.mobileNumber":{$ne:null}},
                        {"_id.emailId":{$ne:null}}
                        //{"_id.firstName":{$ne:null}}
                    ]
                }
            },
            {
                $sort:{count:1}
            },
            {
                $limit:10
            },
            {
                $group:{
                    _id:null,
                    facebookMaxValue:{$max:"$facebook"},
                    linkedinMaxValue:{$max:"$linkedin"},
                    twitterMaxValue:{$max:"$twitter"},
                    values:{$push:{
                        _id: "$_id",
                        firstName:"$firstName",
                        lastName:"$lastName",
                        userId:"$userId",
                        publicProfileUrl:"$publicProfileUrl",
                        lastInteracted:"$lastInteracted",
                        facebook:"$facebook",
                        linkedin:"$linkedin",
                        twitter:"$twitter",
                        count:"$count"
                    }}
                }
            }
        ],function (error,topFive) {
            if(error){
                logger.info("Error in topTenSocialMediaToday():interactionsManagement ",error)
            }
            if(topFive && topFive.length > 0){
                var a=topFive[0];
                for(var i=0;i<a.values.length;i++){
                    a.values[i].facebook/=2;
                    a.values[i].linkedin/=2;
                    a.values[i].count-=(a.values[i].facebook+a.values[i].linkedin);
                    if(a.values[i].userId[0].toString()===userId.toString())
                        a.values.splice(i, 1);
                }
                //for(var i=0;i<a.values.length;i++){
                //    a.values[i].facebook/=2;
                //    a.values[i].count-=a.values[i].facebook;
                //    if(a.values[i].userId[0]==userId)
                //        a.values.splice(i, 1);
                //}
                back(a)
            }else back(null);
        });
    };

    this.getCompaniesUserInteractedWithDateAndGetEmailIdsInteracted_dashboard = function(userId,isArray,emailId,dateMin,mobileNumber,back){

        mobileNumber = mobileNumber.replace(/[^a-zA-Z0-9]/g,'');

        var userMatch = {userId: userId};
        var q = {
            "interactions.userId": {$ne: userId},
            "interactions.emailId": {$ne: emailId},
            "interactions.action": {$in: ['sender', 'receiver']},
            "interactions.interactionDate": {$gte: new Date(dateMin)}
        }
        if(isArray){
            userMatch = {userId:{$in:userId}}
            q = {
                "interactions.userId": {$nin: userId},
                "interactions.emailId": {$nin: emailId},
                "interactions.action": {$in: ['sender', 'receiver']},
                "interactions.interactionDate": {$gte: new Date(dateMin)}
            }
        }

        interaction.aggregate([
            {
                $match: userMatch
            },
            {
                $unwind: "$interactions"
            },
            {
                $match: q
            },
            {
                $group: {
                    _id: null,
                    companies:{$addToSet:"$interactions.companyName"},
                    emailIdArr:{$addToSet:"$interactions.emailId"},
                    mobileNumberArr:{$addToSet:"$interactions.mobileNumber"}
                }
            },
            {
                $project:{
                    _id:1,
                    emailIdArr:1,
                    mobileNumberArr:1,
                    interactionsCompanyCount:"$companies"
                }
            }
        ]).exec(function(error, result){

            if(result.length>0){
                var emailArr = result[0].emailIdArr;
            }
            myUserCollection.aggregate([
                {
                    $match: {_id: userId}
                },{
                    $unwind: "$contacts"
                },{
                    $match: {"contacts.personEmailId": {$in: emailArr}
                    }
                },{
                    $project: {
                        _id: 0,
                        email: "$contacts.personEmailId",
                        companyName: "$contacts.account.name",
                        showToReportingManager:"$contacts.account.showToReportingManager"
                    }
                }
            ]).exec(function(err, userCompanyFromContactsArray){

                var userCompanyObject = {}

                _.each(userCompanyFromContactsArray, function(u){
                    if(u.companyName){
                        userCompanyObject[u.email] = u.companyName
                    }
                })

                var numberOfCompanies = Object.keys(userCompanyObject).length

                if(error){
                    logger.info('Error in getCompaniesUserInteractedWithDateAndGetEmailIdsInteracted_dashboard():interactionsManagement dashboard ',err, userId);
                }
                var obj = {
                    interactionsWithCompany_Count:0,
                    interactedWithEmails:[],
                    interactedWithMobileNumber:[]
                };
                if(result && result.length > 0 && result[0]){

                    if(checkRequired(result[0].emailIdArr) && result[0].emailIdArr.length > 0){
                        obj.interactedWithEmails = removeInvalidContentFromArray(result[0].emailIdArr,[null,'']);
                    }
                    if(checkRequired(result[0].mobileNumberArr) && result[0].mobileNumberArr.length > 0){
                        obj.interactedWithMobileNumber = removeInvalidContentFromArray(result[0].mobileNumberArr,[null,'']);
                    }
                    if(numberOfCompanies > 0){
                        obj.interactionsWithCompany_Count = numberOfCompanies;
                    }
                }
                back(obj);

            })// End my user collection
        });
    };

    this.getInteractionsCountByCompanyLocationTypeDate = function(userId,dateMin,dateMax,emailIdArr,mobileNumberArr,back){

        myUserCollection.aggregate([
            {
                $match: {_id: userId}
            },{
                $unwind: "$contacts"
            },
            {
                $match: {
                    "contacts.account": {$exists: true}
                }
            },{
                $project: {
                    _id: 0,
                    email: "$contacts.personEmailId",
                    mobileNumber: "$contacts.mobileNumber",
                    companyName: "$contacts.account.name",
                    showToReportingManager:"$contacts.account.showToReportingManager"
                }
            }
        ]).exec(function(err, userCompanyFromContactsArray) {

            var usersEmail = _.pluck(userCompanyFromContactsArray, "email")
            usersEmail = _.compact(usersEmail)
            var usersMobileNumber = _.pluck(userCompanyFromContactsArray, "mobileNumber")
            usersMobileNumber = _.compact(usersMobileNumber)

            var q = {
                "interactions.userId": {$ne: userId},
                "interactions.action": {$in: ['sender', 'receiver']},
                "interactions.interactionDate": {$gte: new Date(dateMin)}
            };

            if(checkRequired(usersEmail) && usersEmail.length > 0 && mobileNumberArr.length > 0){
                q['$or'] = [{"interactions.emailId":{$in:usersEmail}},{"interactions.mobileNumber":{$in:mobileNumberArr}}]
            }
            else if(usersEmail.length > 0){
                q["interactions.emailId"] = {$in:usersEmail}
            }
            else if(mobileNumberArr.length > 0){
                q["interactions.mobileNumber"] = {$in:mobileNumberArr}
            }

            if(checkRequired(usersEmail) && usersEmail.length > 0 && usersMobileNumber.length > 0){
                q = {
                    "interactions.userId": {$ne: userId},
                    "interactions.action": {$in: ['sender', 'receiver']},
                    "interactions.interactionDate": {$gte: new Date(dateMin)}
                };

                q['$or'] = [{"interactions.emailId":{$in:usersEmail}},{"interactions.mobileNumber":{$in:usersMobileNumber}}]
            }

            interaction.aggregate([
                {
                    $match: {userId: userId}
                },
                {
                    $unwind: "$interactions"
                },
                {
                    $match: q
                },
                {
                    $group:{
                        _id:"$interactions.emailId",
                        count:{$sum:1}
                    }
                },
                {
                    $group:{
                        _id:null,
                        totalCount:{$sum:"$count"},
                        maxCount:{$max:"$count"},
                        typeCounts:{$push:{_id:"$_id",count:"$count"}}
                    }
                },
                {
                    $unwind:"$typeCounts"
                },
                {
                    $sort:{"typeCounts.count":-1}
                },
                {
                    $group:{
                        _id:null,
                        totalCount:{$max:"$totalCount"},
                        maxCount:{$max:"$maxCount"},
                        typeCounts:{$push:"$typeCounts"}
                    }
                }
            ]).exec(function(errorCompanyName,resultCompanyName){

                var userCompanyObject = {}

                _.each(userCompanyFromContactsArray, function (u) {
                    userCompanyObject[u.email] = u.companyName
                })

                var companyNameArr = _.map(resultCompanyName[0].typeCounts, function (r) {
                    r.companyName = userCompanyObject[r._id] || r.companyName || "Others"
                    return r;
                })

                var groupTypeCountArr = _.map(_.groupBy(resultCompanyName[0].typeCounts, "companyName"), function (vals, companyName) {
                    return _.reduce(vals, function (m, o) {
                        for (var p in o)
                            if (p != "companyName")
                                m[p] = (m[p] || 0) + o[p];
                        return m;
                    }, {companyName: companyName});
                });

                var objCompany = {};
                objCompany._id = null;
                objCompany.totalCount = resultCompanyName[0].totalCount;
                objCompany.maxCount = resultCompanyName[0].maxCount;
                objCompany.typeCounts = groupTypeCountArr;
                var objCompanyArr = [];
                objCompanyArr.push(objCompany)

                if(errorCompanyName){
                    logger.info("Error in getInteractionsCountByCompanyLocationTypeDate():interactionsManagement errorCompanyName ",errorCompanyName)
                }
                interaction.aggregate([
                    {
                        $match: {userId: userId}
                    },
                    {
                        $unwind: "$interactions"
                    },
                    {
                        $match: q
                    },
                    {
                        $match:{
                            "interactions.location":{$nin:[null,'']}
                        }
                    },
                    {
                        $group:{
                            _id:"$interactions.location",
                            count:{$sum:1}
                        }
                    },
                    {
                        $group:{
                            _id:null,
                            totalCount:{$sum:"$count"},
                            typeCounts:{$push:{_id:"$_id",count:"$count"}}
                        }
                    },
                    {
                        $unwind:"$typeCounts"
                    },
                    {
                        $sort:{"typeCounts.count":-1}
                    },
                    {
                        $limit:5
                    },
                    {
                        $group:{
                            _id:null,
                            totalCount:{$max:"$totalCount"},
                            typeCounts:{$push:"$typeCounts"}
                        }
                    }
                ]).exec(function(errorLocation,resultLocation){
                    if(errorCompanyName){
                        logger.info("Error in getInteractionsCountByCompanyLocationTypeDate():interactionsManagement errorLocation ",errorLocation)
                    }
                    interaction.aggregate([
                        {
                            $match: {userId: userId}
                        },
                        {
                            $unwind: "$interactions"
                        },
                        {
                            $match: q
                        },
                        {
                            $match:{
                                "interactions.interactionType":{$ne:null}
                            }
                        },
                        {
                            $group:{
                                _id:"$interactions.interactionType",
                                count:{$sum:1}
                            }
                        },
                        {
                            $group:{
                                _id:null,
                                totalCount:{$sum:"$count"},
                                maxCount:{$max:"$count"},
                                typeCounts:{$push:{_id:"$_id",count:"$count"}}
                            }
                        }
                    ]).exec(function(errorInteractionType,resultInteractionType){
                        if(errorInteractionType){
                            logger.info("Error in getInteractionsCountByCompanyLocationTypeDate():interactionsManagement errorInteractionType ",errorInteractionType)
                        }
                        var data = {};
                        if(checkRequired(resultInteractionType) && resultInteractionType.length > 0){
                            data.interactionsByType = resultInteractionType;
                        }
                        else data.interactionsByType = [];

                        if(checkRequired(resultLocation) && resultLocation.length > 0){
                            data.interactionsByLocation = resultLocation;
                        }
                        else data.interactionsByLocation = [];

                        if(checkRequired(objCompanyArr) && objCompanyArr.length > 0){
                            data.interactionsByCompany = objCompanyArr;
                        }
                        else data.interactionsByCompany = [];

                        back(data)
                    })
                })
            }); // End Interactions
        })//End My user collections

    };

    this.totalInteractionsIndividual = function(userId,dateMin,dateMax,back){

        InteractionsCollection.aggregate([
            {
                $match: {ownerId: userId}
            },
            {
                $match: {
                    "userId": {$ne: userId}
                    //"interactionDate": {$gte: new Date(dateMin), $lte:new Date(dateMax)}
                }
            },
            {
                $group: {
                    _id: "$userId",
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function(err,interactions){

            if(!err && checkRequired(interactions) && interactions.length > 0){
                myUserCollection.populate(interactions,{ path: '_id' ,select: 'firstName lastName publicProfileUrl location locationLatLang currentLocation emailId'},function(eror,users){
                    back(users);
                });
            }
            else back([])
        });
    };

    this.totalInteractionsIndividualSummaryDate = function(userId,withUserId,isEmailId,mobileNumber,dateMin,dateMax,orMobileNumber,source,back){
      var q = {"interactionDate":{ $lte:new Date(dateMax)}};
      if(dateMin != null && dateMax != null){
        q = {"interactionDate": {$gte: new Date(dateMin), $lte:new Date(dateMax)}};
      }

      if(isEmailId){
        if(checkRequired(withUserId) && checkRequired(orMobileNumber)){
            var mobile10Digit = orMobileNumber.length <= 10 ? orMobileNumber : orMobileNumber.substr(orMobileNumber.length - 10);
          q.$or = [{"emailId":withUserId},{"mobileNumber":new RegExp('\d{0,4}'+mobile10Digit+'$')}]
        }else
          q["emailId"] = withUserId;
      }

        if(isNumber(withUserId) && !isEmailId){
            mobile10Digit = withUserId.length <= 10 ? withUserId : withUserId.substr(withUserId.length - 10);
            q["mobileNumber"] = new RegExp('\d{0,4}'+mobile10Digit+'$')
        }

        q["userId"] = {$ne:userId}

        if(_.includes(source, 'outlook')){
            q["source"] = {$ne:"google"}
        } else {
            q["source"] = {$ne:"outlook"}
        }

        InteractionsCollection.aggregate([
          {
            $match: {ownerId: userId}
          },
          {
            $match:q
          },
            {
                $group:{
                    _id:"$refId",
                    recorIdId:{$first:"$_id"},
                    userId:{$first:"$userId"},
                    publicProfileUrl:{$first:"$publicProfileUrl"},
                    interactionDate:{ "$first": "$interactionDate"},
                    interactionType:{ "$first": "$interactionType"},
                    subType:{ "$first": "$subType"},
                    action:{ "$first": "$action"},
                    emailId:{ "$first": "$emailId"},
                    title:{ "$first": "$title"},
                    refId:{ "$first": "$refId"},
                    description:{ "$first": "$description"},
                    postURL:{ "$first": "$postURL"},
                    trackInfo:{ "$first": "$trackInfo"},
                    trackId:{ "$first": "$trackId"},
                    ignore:{ "$first": "$ignore"},
                    createdDate:{ "$first": "$createdDate"},
                    emailContentId:{ "$first": "$emailContentId"},
                    googleAccountEmailId:{ "$first": "$googleAccountEmailId"},
                    source:{ "$first": "$source"},
                    profilePicUrl:{ "$first": "$profilePicUrl"},
                    mobileNumber:{ "$first": "$mobileNumber"},
                    firstName:{ "$first": "$firstName"},
                    lastName:{ "$first": "$lastName"},
                    duration:{ "$first": "$duration"}
                }
            },
          {
            $project:{
              "_id" : "$recorIdId",
              "refId" : "$refId",
              "userId" : "$userId",
              "emailId" : "$emailId",
              "interactionType" : "$interactionType",
              "subType" : "$subType",
              "action" : "$action",
              "interactionDate" : "$interactionDate",
              "createdDate" : "$createdDate",
              "source" : "$source",
              "title" : "$title",
              "description" : "$description",
              "publicProfileUrl" : "$publicProfileUrl",
              "profilePicUrl" : "$profilePicUrl",
              "mobileNumber" : "$mobileNumber",
              "trackInfo":"$trackInfo",
              "trackId":"$trackId",
              "ignore":"$ignore",
              "googleAccountEmailId":"$googleAccountEmailId",
              "postURL":"$postURL",
              "emailContentId":"$emailContentId",
                "fullName":{ $concat: ["$firstName", " ", "$lastName"]}
                }
          },
          {
            $sort:{"interactionDate":-1}
          }
        ]).exec(function(err,interactions){

            if(!err && checkRequired(interactions) && interactions.length > 0){
                back(interactions);
            }
            else back([])
        });
    };

    this.updateInteractionToIgnore = function(userId,interactionId,updateLastOpenedDate,back){

        var updateObj = {$set:{"ignore":true}};
        if(updateLastOpenedDate){
            updateObj = {$set:{"ignore":true,"trackInfo.lastOpenedOn":new Date()}};
        }

        InteractionsCollection.update({ownerId:userId,"_id":interactionId},updateObj, function(error,result){
            if(error){
                logger.info("Error in updateInteractionToIgnore():interactionsManagement ",error);
                back(false)
            }
            else if(result && result.ok){
                back(true)
            }
            else back(false)
        })
    };

    this.updateInteractionOpened = function(userId,receiverEmailId,trackId,back){

        // InteractionsCollection.update({ownerId:userId,emailId:receiverEmailId,trackId:trackId,"trackInfo.isRed":{$ne:true}},{$set:{"trackInfo.lastOpenedOn":new Date(),"trackInfo.isRed":true,"trackInfo.emailOpens":1}}, function(error,result){
        InteractionsCollection.update({ownerId:userId,trackId:trackId,"trackInfo.isRed":{$ne:true}},{$set:{"trackInfo.lastOpenedOn":new Date(),"trackInfo.isRed":true,"trackInfo.emailOpens":1}}, function(error,result){
            if(error){
                logger.info("Error in updateInteractionOpened():interactionsManagement ",error);
                back(false)
            }
            else if(result){
                if(result.nModified == 0){
                    // InteractionsCollection.update({ownerId:userId,emailId:receiverEmailId,trackId:trackId,"trackInfo.isRed":true},{ $inc:{"trackInfo.emailOpens":1}}, function(err,resul) {
                    InteractionsCollection.update({ownerId:userId,trackId:trackId,"trackInfo.isRed":true},{ $inc:{"trackInfo.emailOpens":1}}, function(err,resul) {
                        if (err) {
                            logger.info("Error in updateInteractionOpened():interactionsManagement ", error);
                            back(false)
                        }
                        if(resul && resul.ok)
                            back(true)
                        else
                            back(false)
                    });
                }
                else if(result.ok) {
                    back(true)
                }
            }
            else back(false)
        })
    };

    this.updateInteractionOpenedTaskBulk = function(refId,isEmailId,lEmailId,cEmailId,status,back){

        var bulk = InteractionsCollection.collection.initializeUnorderedBulkOp();
        if(isEmailId){
            bulk.find({ownerEmailId:{$in:[lEmailId,cEmailId]},emailId:lEmailId,refId:refId}).update({$set:{"trackInfo.lastOpenedOn":new Date(),"trackInfo.status":status}});
            bulk.find({ownerEmailId:{$in:[lEmailId,cEmailId]},emailId:cEmailId,refId:refId}).update({$set:{"trackInfo.lastOpenedOn":new Date(),"trackInfo.status":status}});
        }
        else{
            bulk.find({ownerId:{$in:[lEmailId,cEmailId]},userId:lEmailId,refId:refId}).update({$set:{"trackInfo.lastOpenedOn":new Date(),"trackInfo.status":status}});
            bulk.find({ownerId:{$in:[lEmailId,cEmailId]},userId:cEmailId,refId:refId}).update({$set:{"trackInfo.lastOpenedOn":new Date(),"trackInfo.status":status}});
        }

        bulk.execute(function(err, result) {
            if(err){
                logger.info('Error in updateInteractionOpenedTaskBulk():InteractionsManagement ',err )
            }
            else{
                result = result.toJSON();
                logger.info('updateInteractionOpenedTaskBulk() results ',result);
            }
            if(back) back(true)
        });
    };
    
    this.addTwitterInteractions = function (userId,interactions,callback) {
        addAllInteractions(userId,interactions,callback)
    }
    
    this.updateInteractionMeetingAcceptedBulk = function(refId,sUserId,sEmailId,rUserId,rEmailId,startDate,endDate,lastOpenedOn,status,back){

        var bulk = interaction.collection.initializeUnorderedBulkOp();
        var updateObj = {$set:{"interactions.$.interactionDate":startDate,"interactions.$.trackInfo.lastOpenedOn":lastOpenedOn,"interactions.$.trackInfo.status":status,"interactions.$.trackInfo.action":"confirmed"}};
        if(checkRequired(endDate)){
            updateObj = {$set:{"interactions.$.interactionDate":startDate,"interactions.$.endDate":endDate,"interactions.$.trackInfo.lastOpenedOn":lastOpenedOn,"interactions.$.trackInfo.status":status,"interactions.$.trackInfo.action":"confirmed"}};
        }

        if(checkRequired(sUserId) && checkRequired(rUserId)){
            bulk.find({userId:{$in:[sUserId,rUserId]},interactions:{$elemMatch:{emailId:sEmailId,refId:refId}}}).update(updateObj);
            bulk.find({userId:{$in:[sUserId,rUserId]},interactions:{$elemMatch:{emailId:rEmailId,refId:refId}}}).update(updateObj);
        }
        else if(checkRequired(sUserId)){
            bulk.find({userId:sUserId,interactions:{$elemMatch:{emailId:sEmailId,refId:refId}}}).update(updateObj);
            bulk.find({userId:sUserId,interactions:{$elemMatch:{emailId:rEmailId,refId:refId}}}).update(updateObj);
        }
        else if(checkRequired(rUserId)){
            bulk.find({userId:rUserId,interactions:{$elemMatch:{emailId:sEmailId,refId:refId}}}).update(updateObj);
            bulk.find({userId:rUserId,interactions:{$elemMatch:{emailId:rEmailId,refId:refId}}}).update(updateObj);
        }

        bulk.execute(function(err, result) {
            if(err){
                logger.info('Error in updateInteractionMeetingAcceptedBulk():InteractionsManagement ',err )
            }
            else{
                result = result.toJSON();
                logger.info('updateInteractionMeetingAcceptedBulk() results ',result);
            }
            if(back) back(true)
        });
    };

    this.updateInteractionMeetingAccepted = function(refId,sUserId,sEmailId,rUserId,rEmailId,startDate,endDate,lastOpenedOn,status,back){

        var updateObj = {$set:{"interactionDate":startDate,"trackInfo.lastOpenedOn":lastOpenedOn,"trackInfo.status":status,"trackInfo.action":"confirmed"}};
        if(checkRequired(endDate)){
            updateObj = {$set:{"interactionDate":startDate,"endDate":endDate,"trackInfo.lastOpenedOn":lastOpenedOn,"trackInfo.status":status,"trackInfo.action":"confirmed"}};
        }

        InteractionsCollection.update({ownerId:{$in:[sUserId,rUserId]},"refId":refId},updateObj,{multi:true}, function(error,result){
            if(error){
                logger.info("Error in updateInteractionToIgnore():interactionsManagement ",error);
                back(false)
            }
            else if(result && result.ok){
                back(true)
            }
            else back(false)
        })

    };

    this.bestPathToConnection = function(userId,userIdArr,notInEmailIdArr,callback){
        var q = {
            "userId": {$in:userIdArr}
        }
        if(notInEmailIdArr && notInEmailIdArr.length > 0){
            q["emailId"] = {$nin:notInEmailIdArr}
        }
        InteractionsCollection.aggregate([
            {
                $match: {ownerId: userId}
            },
            {
                $match: q
            },
            {
                $group: {
                    _id: "$userId",
                    lastInteracted:{$max:"$interactionDate"},
                    count: {
                        $sum: 1
                    }
                }
            },

            {
                $sort:{count:-1}
            },
            {
                $limit:3
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                myUserCollection.populate(interactions,{ path: '_id' ,select: 'firstName lastName publicProfileUrl emailId'},function(eror,users){
                    callback(users);
                });
            }
            else callback([])
        });
    };

    this.lastInteractionWithContact = function(userId,cUserId,idType,dateMin,limit,back){

        var q = {
            "interactionDate": { $lte:new Date(dateMin)}
        };
        if(idType == 'emailId'){
            if(checkArray(cUserId)){
                q[ "emailId"] = {$in:cUserId}
            } else {
                q[ "emailId"] = cUserId
            }
        }
        else if(idType == 'mobileNumber'){
            var search = new RegExp(cUserId.trim(),'i');
            q[ "mobileNumber"] = search
        }
        else{
            q[ "userId"] = castToObjectId(cUserId)
        }

        InteractionsCollection.aggregate([
            {
                $match: {
                    ownerId: userId,
                    userId: {$ne: userId}
                }
            },
            {
                $match: q
            },
            {
                $sort:{
                    "interactionDate":-1
                }
            },
            {
                $limit:limit
            },
            {
                $project:{
                    _id:"$_id",
                    type:"$interactionType",
                    interactionDate:"$interactionDate",
                    title:"$title",
                    refId:"$refId",
                    mobileNumber:"$mobileNumber",
                    emailId:"$emailId",
                    firstName:"$firstName",
                    lastName:"$lastName"
                }
            }
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions.length > 0){
                back(interactions);
            }
            else back([])
        });
    };

    //modified version of lastInteractionWithContact for web -- Rajiv, 04032016
    //modified to return total interactions number with that user also
    this.lastInteractionWithContactMobile = function(userId,emailId,cUserId,idType,dateMin,limit,back){


        var q
        if(idType == 'userId'){
            q = {$and:[{ownerId:userId},{"userId":cUserId},{"interactionDate": {$lte:new Date(dateMin)}},{"interactionType":{$ne:"task"}}]}
        }
        else if(idType == 'mobileNumber'){

            q ={$and:[{ownerId:userId}, {"mobileNumber":{$eq:cUserId}},{"interactionDate": {$lte:new Date(dateMin)}},{"userId":{$ne:userId}},
                {"interactionType":{$ne:"task"}}]}
        }
        else{
            q = {$and:[{ownerId:userId},{"userId":cUserId,"interactionDate": {$lte:new Date(dateMin)},"interactionType":{$ne:"task"}}]}
        }

        InteractionsCollection.aggregate([
            {
                $match: q
            },
            {
                $sort:{"interactionDate":-1}
            },
            {
                $limit:limit
            },
            {$group:{_id:null,
                interactions:{$push: {
                    _id: "$_id",
                    type: "$interactionType",
                    interactionDate: "$interactionDate",
                    title: "$title",
                    refId: "$refId",
                    action: "$action",
                    userId: "$userId",
                    mobileNumber: "$mobileNumber"
                }
            }
            }}
        ]).exec(function(err,interactions){
            if(!err && checkRequired(interactions) && interactions[0].interactions.length > 0){
                var countInteractionsTotal = interactions[0].interactions.length
                back(interactions[0].interactions.splice(0,limit),countInteractionsTotal);
            }
            else back([],0)
        });
    };
    
    /* total interactions with one user and last interacted date*/
    this.totalInteractionsWithOneUserCountAndLastInteracted = function(userIdN,withEmailId,castToObjectId,dateMax,back){
        var userId = null;

        if(checkRequired(userIdN)){
            userId = castToObjectId(userIdN)
        }

        if(checkRequired(userId) && checkRequired(withEmailId)){
            InteractionsCollection.aggregate([
                {
                    $match: {ownerId: userId}
                },
                {
                    $match: {
                        "emailId":withEmailId,
                        "interactionDate":{$lte:new Date(dateMax)}
                    }
                },
                {
                    $group: {
                        _id: null,
                        lastInteracted:{$max:"$interactionDate"},
                        arr:{$addToSet:"$interactionDate"},
                        count: {
                            $sum: 1
                        }
                    }
                }
            ]).exec(function(err,interactions){

                if(!err && checkRequired(interactions) && interactions.length > 0){
                    if(checkRequired(interactions[0].arr) && interactions[0].arr.length > 0){

                        interactions[0].arr.sort(function(a, b){return new Date(b)-new Date(a)});
                        for(var i=0; i<interactions[0].arr.length; i++){
                            if(new Date(interactions[0].arr[i]) <= new Date()){
                                interactions[0].lastInteracted = interactions[0].arr[i];
                                break;
                            }
                        }
                        back(interactions);
                    }else
                        back(interactions);
                }
                else back([])
            });
        }
        else back([])
    };

    this.removeInvalidInteractions = function(match,invalidEmailsArr){
        InteractionsCollection.remove(match,{emailId:{$in:invalidEmailsArr}},function(error,result){
            if(error){
                logger.info("Error removing invalid content removeInvalidInteractions():InteractionManageMent ",error)
            }
            else{
                logger.info("invalid email remove interactions result ",result);
            }
        })
    };
    
    this.removeInvalidInteractionsByUser = function(query,callback){
        InteractionsCollection.remove(query,function(error,result){
            
            if(error){
                logger.info("Error removing invalid content removeInvalidInteractions():InteractionManageMent ",error)
                if(callback){
                    callback(error,false)
                }
            }
            else{
                if(callback){
                    callback(error,true)
                }
                logger.info("invalid email remove interactions result ",true);
            }
        })
    };

    this.getInteractionsCountByCompanyLocationTypeDate_companyLanding = function(userIdList,dateMin,dateMax,back){

        var q = {
            "interactions.action": {$in: ['sender', 'receiver']},
            "interactions.interactionDate": {$gte: new Date(dateMin),$lte:new Date(dateMax)}
        };

        interaction.aggregate([
            {
                $match: {userId: {$in:userIdList}}
            },
            {
                $unwind: "$interactions"
            },
            {
                $match: q
            },
            {
                $project:{
                    //"interactions":{$cond: { if:{$eq:["$emailId","$interactions.emailId"]}, then: null, else: "$interactions"}}
                    "interactions":"$interactions"
                }
            },
            {
                $match:{"interactions":{$ne:null}}
            },
            {
                $match:{
                    "interactions.companyName":{$nin:[null,'']}
                }
            },
            {
                $group:{
                    _id:"$interactions.companyName",
                    //_id:{companyName:"$interactions.companyName",location:"$interactions.location",interactionType:"$interactions.interactionType"},
                    count:{$sum:1}
                }
            },
            {
                $group:{
                    _id:null,
                    totalCount:{$sum:"$count"},
                    maxCount:{$max:"$count"},
                    typeCounts:{$push:{_id:"$_id",count:"$count"}}
                }
            },
            {
                $unwind:"$typeCounts"
            },
            {
                $sort:{"typeCounts.count":-1}
            },
            {
                $limit:5
            },
            {
                $group:{
                    _id:null,
                    totalCount:{$max:"$totalCount"},
                    maxCount:{$max:"$maxCount"},
                    typeCounts:{$push:"$typeCounts"}
                }
            }
        ]).exec(function(errorCompanyName,resultCompanyName){
            if(errorCompanyName){
                logger.info("Error in getInteractionsCountByCompanyLocationTypeDate():interactionsManagement errorCompanyName ",errorCompanyName)
            }
            interaction.aggregate([
                {
                    $match: {userId: {$in:userIdList}}
                },
                {
                    $unwind: "$interactions"
                },
                {
                    $match: q
                },
                {
                    $project:{
                        "interactions":"$interactions"
                    }
                },
                {
                    $match:{
                        "interactions.location":{$nin:[null,'']}
                    }
                },
                {
                    $group:{
                        _id:"$interactions.location",
                        //_id:{companyName:"$interactions.companyName",location:"$interactions.location",interactionType:"$interactions.interactionType"},
                        count:{$sum:1}
                    }
                },
                {
                    $group:{
                        _id:null,
                        totalCount:{$sum:"$count"},
                        typeCounts:{$push:{_id:"$_id",count:"$count"}}
                    }
                },
                {
                    $unwind:"$typeCounts"
                },
                {
                    $sort:{"typeCounts.count":-1}
                },
                {
                    $limit:5
                },
                {
                    $group:{
                        _id:null,
                        totalCount:{$max:"$totalCount"},
                        typeCounts:{$push:"$typeCounts"}
                    }
                }
            ]).exec(function(errorLocation,resultLocation){
                if(errorCompanyName){
                    logger.info("Error in getInteractionsCountByCompanyLocationTypeDate():interactionsManagement errorLocation ",errorLocation)
                }
                interaction.aggregate([
                    {
                        $match: {userId: {$in:userIdList}}
                    },
                    {
                        $unwind: "$interactions"
                    },
                    {
                        $match: q
                    },
                    {
                        $project:{
                            "interactions":"$interactions"
                        }
                    },
                    {
                        $match:{
                            "interactions.interactionType":{$ne:null}
                        }
                    },
                    {
                        $group:{
                            _id:"$interactions.interactionType",
                            count:{$sum:1}
                        }
                    },
                    {
                        $group:{
                            _id:null,
                            totalCount:{$sum:"$count"},
                            maxCount:{$max:"$count"},
                            typeCounts:{$push:{_id:"$_id",count:"$count"}}
                        }
                    }
                ]).exec(function(errorInteractionType,resultInteractionType){
                    if(errorInteractionType){
                        logger.info("Error in getInteractionsCountByCompanyLocationTypeDate():interactionsManagement errorInteractionType ",errorInteractionType)
                    }
                    var data = {};
                    if(checkRequired(resultInteractionType) && resultInteractionType.length > 0){
                        data.interactionsByType = resultInteractionType;
                    }
                    else data.interactionsByType = [];

                    if(checkRequired(resultLocation) && resultLocation.length > 0){
                        data.interactionsByLocation = resultLocation;
                    }
                    else data.interactionsByLocation = [];

                    if(checkRequired(resultCompanyName) && resultCompanyName.length > 0){
                        data.interactionsByCompany = resultCompanyName;
                    }
                    else data.interactionsByCompany = [];

                    back(data)
                })
            })
        });
    };

    this.trackResponseEmails = function(userId,emailId){
        var classObj = this;
        InteractionsCollection.aggregate([
            {
                $match: {ownerId: userId}
            },
            {
                $match:{
                    "userId":userId,
                    "action":"sender",
                    "interactionType":"email",
                    "title":{$exists:true,$nin:[null,undefined,'',' ']},
                    "refId":{$exists:true,$nin:[null,undefined,'',' ']},
                    "trackInfo.trackResponse":true,
                    "trackInfo.gotResponse":false
                }
            },
            {
                $group:{
                    _id:null,
                    interactions:{$push:{_id:"$_id",title:"$title",refId:"$refId"}},
                    titleList:{$addToSet:"$title"},
                    refIdList:{$addToSet:"$refId"}
                }
            }
        ]).exec(function(error,result){
            
            if(error){
                logger.info("Error in trackResponseEmails():interactionsManagement ",error)
            }
            else if(result && result.length > 0 && result[0] && result[0].interactions && result[0].interactions.length > 0){
                // classObj.trackResponseEmails_next(userId,emailId,result[0].interactions,result[0].titleList,result[0].refIdList)
            }
        })
    };

    this.trackResponseEmails_next = function(userId,emailId,interactions,titleList,refIdList){
        var newTitleList = [];

        titleList.forEach(function(title){
            newTitleList.push(new RegExp(title))
        });
        InteractionsCollection.aggregate([
            {
                $match: {ownerId: userId}
            },
            {
                $match:{
                    "emailId":{$ne:emailId},
                    "action":"sender",
                    "interactionType":"email",
                    "title":{$in:newTitleList},
                    "refId":{$nin:refIdList}
                }
            },
            {
                $group:{
                    _id:null,
                    interactions:{$push:{_id:"$_id",title:"$title",refId:"$refId",emailId:"$emailId",interactionDate:"$interactionDate"}}
                }
            }
        ]).exec(function(error,result){
            if(error){
                logger.info("Error in trackResponseEmails():interactionsManagement ",error)
            }
            else if(result && result.length > 0 && result[0] && result[0].interactions && result[0].interactions.length > 0){
                var gotResponsesTo = [];
                for(var i=0; i<interactions.length; i++){
                    var title = new RegExp(interactions[i].title);
                    result[0].interactions.forEach(function(inte){
                        if(inte.title.match(title)){
                            gotResponsesTo.push({userId:userId,emailId1:emailId,emailId2:inte.emailId,refId:interactions[i].refId,interactionDate:inte.interactionDate,title:interactions[i].title,title2:inte.title});
                        }
                    })
                }

                if(gotResponsesTo.length >0){
                    var bulk = InteractionsCollection.collection.initializeUnorderedBulkOp();
                    gotResponsesTo.forEach(function(obj){
                        bulk.find({$or:[{emailId:obj.emailId1},{emailId:obj.emailId2}],interactions:{$elemMatch:{refId:obj.refId,emailId:obj.emailId1}}}).update({$set:{"interactions.$.trackInfo.gotResponse":true,"interactions.$.trackInfo.lastOpenedOn":new Date(obj.interactionDate)}});
                        bulk.find({$or:[{emailId:obj.emailId1},{emailId:obj.emailId2}],interactions:{$elemMatch:{refId:obj.refId,emailId:obj.emailId2}}}).update({$set:{"interactions.$.trackInfo.gotResponse":true,"interactions.$.trackInfo.lastOpenedOn":new Date(obj.interactionDate)}});
                    });

                    bulk.execute(function(err, result) {
                        if(err){
                            logger.info('Error in trackResponseEmails_next():InteractionsManagement Bulk ',err )
                        }
                        else{
                            result = result.toJSON();
                            logger.info('trackResponseEmails_next() results Bulk ',result);
                        }
                    });
                }
            }
        })
    };

    this.updateInteractionReplied = function(obj){
        // var bulk = interaction.collection.initializeUnorderedBulkOp();
        // bulk.find({$or:[{emailId:obj.emailId1},{emailId:obj.emailId2}],interactions:{$elemMatch:{refId:obj.refId,emailId:obj.emailId1}}}).update({$set:{"interactions.$.trackInfo.gotResponse":true,"interactions.$.trackInfo.lastOpenedOn":new Date(obj.interactionDate)}});
        // bulk.find({$or:[{emailId:obj.emailId1},{emailId:obj.emailId2}],interactions:{$elemMatch:{refId:obj.refId,emailId:obj.emailId2}}}).update({$set:{"interactions.$.trackInfo.gotResponse":true,"interactions.$.trackInfo.lastOpenedOn":new Date(obj.interactionDate)}});

        var bulk = InteractionsCollection.collection.initializeUnorderedBulkOp();
        bulk.find({$or:[{ownerEmailId:obj.emailId1},{ownerEmailId:obj.emailId2}],refId:obj.refId,emailId:obj.emailId1}).update({$set:{"trackInfo.gotResponse":true,"trackInfo.lastOpenedOn":new Date(obj.interactionDate)}});
        bulk.find({$or:[{ownerEmailId:obj.emailId1},{ownerEmailId:obj.emailId2}],refId:obj.refId,emailId:obj.emailId2}).update({$set:{"trackInfo.gotResponse":true,"trackInfo.lastOpenedOn":new Date(obj.interactionDate)}});

        bulk.execute(function(err, result) {
            if(err){
                logger.info('Error in trackResponseEmails_next():InteractionsManagement Bulk ',err )
            }
            else{
                result = result.toJSON();
                logger.info('trackResponseEmails_next() results Bulk ',result);
            }
        });
    }

    this.linkedinConnectedVia = function(userId,cEmailId,callback){
        interaction.aggregate([
            {
                $match: {userId: userId}
            },
            {
                $unwind: "$interactions"
            },
            {
                $match:{
                    "interactions.interactionType":"linkedin",
                    "interactions.emailId":cEmailId
                }
            },
            {
                $group:{
                    _id:null,
                    types:{$addToSet:"$interactions.interactionType"}
                }
            }
        ]).exec(function(error,result){
            if(error){
                logger.info("Error in linkedinConnectedVia():interactionsManagement ",error);
            }
            callback(error,result)
        })
    };

    this.removeInteractions_user = function(userId,emailId,callback){
        InteractionsCollection.remove({$or:[{ownerId:userId},{ownerEmailId:emailId}]},function(error,result){
            if(error){
                callback(false,'onDeleteInteractions',error,result);
            }
            else callback(true,'onDeleteInteractions',error,result);
        });
    };
    /******************* New Interaction Collection API's *************************/

    this.interactionIndividualTopFive_new = function(userId,emailId,start,end,back){
        var date = new Date(start);
        var today = new Date(end);

        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:date,
                        $lte:today
                    }
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){
                getInvalidEmailListFromDb(function(invalidEmailList) {
                    var queryOr = [];
                    if (checkRequired(d[0]) && checkRequired(d[0]._id)) {
                        queryOr.push({
                            userId: {$ne: userId},
                            emailId: {$nin: ['', emailId]},
                            action: d[0]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[0].refArr}
                        })
                    }
                    if (checkRequired(d[1]) && checkRequired(d[1]._id)) {
                        queryOr.push({
                            userId: {$ne: userId},
                            emailId: {$nin: ['', emailId]},
                            action: d[1]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[1].refArr}
                        })
                    }

                    InteractionsCollection.aggregate([
                        {
                            $match: {
                                $or: queryOr,
                                emailId: {$nin: invalidEmailList}
                            }
                        },
                        {
                            $group: {
                                _id: "$emailId",
                                lastInteracted: {$max: "$interactionDate"},
                                count: {
                                    $sum: 1
                                },
                                interactions: {
                                    $push: {
                                        "_id": "$_id",
                                        "refId": "$refId",
                                        "userId": "$userId",
                                        "emailId": "$emailId",
                                        "type": "$type",
                                        "subType": "$subType",
                                        "action": "$action",
                                        "interactionDate": "$interactionDate",
                                        "createdDate": "$createdDate",
                                        "source": "$source",
                                        "title": "$title",
                                        "description": "$description",
                                        "endDate": "$endDate",
                                        "firstName": "$firstName",
                                        "lastName": "$lastName",
                                        "publicProfileUrl": "$publicProfileUrl",
                                        "profilePicUrl": "$profilePicUrl",
                                        "mobileNumber": "$mobileNumber",
                                        "companyName": "$companyName",
                                        "designation": "$designation"
                                    }
                                }
                            }
                        },
                        {
                            $unwind: "$interactions"
                        },
                        {
                            $project: {
                                _id: 1,
                                lastInteracted: 1,
                                count: 1,
                                interaction: {
                                    $cond: {
                                        if: {$eq: ["$lastInteracted", "$interactions.interactionDate"]},
                                        then: "$interactions",
                                        else: false
                                    }
                                }
                            }
                        },
                        {
                            $match: {
                                interaction: {$ne: false}
                            }
                        },
                        {
                            $group: {
                                _id: null,
                                topInteractions: {
                                    $push: {
                                        "_id": "$interaction._id",
                                        //"refId" : "$interaction.refId",
                                        "userId": "$interaction.userId",
                                        "emailId": "$interaction.emailId",
                                        //"type" : "$interaction.type",
                                        //"subType" : "$interaction.subType",
                                        //"action" : "$interaction.action",
                                        "interactionDate": "$interaction.interactionDate",
                                        //"createdDate" : "$interaction.createdDate",
                                        //"source" : "$interaction.source",
                                        //"title" : "$interaction.title",
                                        //"description" : "$interaction.description",
                                        //"endDate" : "$interaction.endDate",
                                        "firstName": "$interaction.firstName",
                                        "lastName": "$interaction.lastName",
                                        //"publicProfileUrl" : "$interaction.publicProfileUrl",
                                        "profilePicUrl": "$interaction.profilePicUrl",
                                        "mobileNumber": "$interaction.mobileNumber",
                                        //"companyName" : "$interaction.companyName",
                                        //"designation" : "$interaction.designation",
                                        "count": "$count"
                                    }
                                }
                            }
                        }
                    ]).exec(function (err, interactions) {

                        if (!err && checkRequired(interactions) && interactions.length > 0 && interactions[0] && interactions[0].topInteractions) {
                            interactions[0].topInteractions.sort(function (a, b) {
                                if (a.count < b.count) return 1;
                                if (a.count > b.count) return -1;
                                return 0;
                            });
                            back(interactions[0].topInteractions);//
                        }
                        else back([])
                    });
                });
            } else back([])
        });
    };

    this.createInteraction = function(interactionDetails){

        var interaction = constructInteractionManagementObject(interactionDetails);

        if(checkRequired(interaction.refId)){
            interactions.update({userId:interactionDetails.userId,refId:interactionDetails.refId},{$set:interaction},{upsert: true},function(error,i){
                if(error)
                    logger.info('Error in findOneAndUpdate INTERACTIONS '+error);

            });
        }
        else{
            logger.info('No refId while storing interaction InteractionType:'+interaction.type+' Source: '+interaction.source+' Subtype: '+interaction.subType+' emailId: '+interaction.emailId);
        }
    };

    this.updateInteractionWithProfile = function(userId,emailId,mobileNumber,updateObj){
        // var query = {$or:[{userId:userId},{emailId:emailId}]};
        // if(checkRequired(mobileNumber)){
        //     query.$or.push({mobileNumber:mobileNumber});
        // }
        //
        // interactions.update(query,{$set:updateObj},{multi:true},function(error,updateResult){
        //     if(error){
        //         logger.info('Error Interactions updateInteractionWithProfile() '+JSON.stringify(error));
        //     }
        //
        // })
    };

    this.findInteractionById = function(id,callback){
        interactions.findOne({_id:id},function(error,interaction){
            if(checkRequired(interaction)){
                if(checkRequired(interaction.userId)){
                    myUserCollection.populate(interaction,{path:'userId', select:'firstName lastName emailId'},function(err,obj){
                        if(!err && obj){
                            callback(obj)
                        }
                        else callback(interaction)
                    })
                }else callback(interaction)
            }else callback(false)
        })
    };

    this.upcomingMeetingsNextSevenDays = function(userId,future,callback){
        var date = new Date();
        var today = new Date();

        if(future){
            today.setDate(today.getDate() + 7);
        }
        else{
            date.setDate(date.getDate() - 7);
        }
        date.setHours(0,0,0,0);
        today.setHours(0,0,0,0);

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    type:'meeting',
                    interactionDate:{
                        $gte:date,
                        $lte:today
                    }
                }
            },
            {
                $group: {
                    _id: null,
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {

            if(!e && checkRequired(d) && d.length > 0 && checkRequired(d[0].count)){
                callback(d);
            }
            else{
                callback([]);
            }
        })
    };

    this.insertInteraction = function(interaction,callback){
        interactions.findOne({userId:interaction.userId,refId:interaction.refId},function(error,i){
            if(checkRequired(i)){
                //logger.info('Interaction already exists with id');
                callback('exist with ref id')
            }else{
                insertNewInteraction(interaction,callback);
            }
        })
    };

    function insertNewInteraction(interaction,callback){
        var interactionObj = constructInteractionManagementObject(interaction);
        interactionObj.save(function (error, insertedDocument) {
            if (!error){
                //logger.info('interaction inserted successfully');
                callback('inserted');
            }
            else{
                // logger.info('error in inserting interaction');
                callback('error not inserted');
            }
        })
    }

    this.interactionEmailsByDate = function(userId,back){
        var date = new Date();
        var today = new Date();

        date.setHours(0,0,0,0);
        today.setHours(0,0,0,0);
        today.setDate(today.getDate() + 1);
        date.setDate(date.getDate() - 90);

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:date,
                        $lte:today
                    }
                }
            },
            {
                $group: {
                    _id: null,
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {

            if(e || d.length == 0){
                var output = {
                    refIdArr:[],
                    totalInteractions:0,
                    interactedEmails:[],
                    emailsCount: 0
                };
                back(output);
            }else{

                if(d.length > 0){
                    totalInteractedPersons(d[0].refArr,function(result){
                        var output = {
                            refIdArr:d[0].refArr,
                            totalInteractions:d[0].count,
                            interactedEmails:result.length >0 ? result[0].emailIdArr : [],
                            emailsCount:result.length >0 ? result[0].count : 0
                        };
                        back(output);
                    });
                }
            }
        });
    };

    function totalInteractedPersons(result,back){

        interactions.aggregate([
            {
                $match:{
                    refId:{
                        $in:result
                    },
                    action:'receiver'
                }
            },
            {
                $group: {
                    _id: null,
                    emailIdArr:{ $addToSet: "$emailId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            back(d);
        });
    }

    this.getInteractionsByDatesArr = function(userId,dateStart,dateEnd,callback){

        var mainQuery = [];

        for(var dates=0; dates < dateStart.length; dates++){
            var query = {
                "interactionDate": {
                    '$gte':dateStart[dates],'$lte':dateEnd[dates]
                }
            };
            mainQuery.push(query);
        }

        interactions.find(
            {
                $and:[{userId:userId},{type:'meeting'},{source:'google'}],
                $or:mainQuery
            },
            function(error,result){
                if(error){
                    callback([]);
                }else
                    callback(result);
            }
        )
    };

    /*  Top 5 people interactions */
    this.interactionIndividualByDate = function(userId,emailId,back){
        var date = new Date();
        var today = new Date();

        date.setHours(0,0,0,0);
        today.setHours(0,0,0,0);
        today.setDate(today.getDate() + 1);
        date.setDate(date.getDate() - 90);

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:date,
                        $lte:today
                    }
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:{ $ne:userId },
                        emailId:{$nin:['',emailId]},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:{ $ne:userId },
                        emailId:{$nin:['',emailId]},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group: {
                            _id: "$emailId",
                            count: {
                                $sum: 1
                            }
                        }
                    },
                    {
                        $sort:{
                            count:-1
                        }
                    },
                    {
                        $limit:5
                    }
                ]).exec(function(err,interactions){
                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    this.interactionIndividualByMonth = function(userId,back){
        var date = new Date();
        var today = new Date();

        date.setHours(0,0,0,0);
        today.setHours(0,0,0,0);
        today.setDate(today.getDate() + 1);
        date.setDate(date.getDate() - 90);

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    //$or:[{userId:userId},{emailId:{$in:[]}}],
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:date,
                        $lte:today
                    }
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {

            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:{ $nin: [userId] },
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:{ $nin: [userId] },
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group: {
                            _id: { $month: "$interactionDate" },
                            year: {$addToSet:{ $year: "$interactionDate" }},
                            users:{$addToSet:"$emailId"},
                            count: {
                                $sum: 1
                            }
                        }
                    },
                    {
                        $sort:{
                            _id:1
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){

                        back(interactions)

                    }
                    else back([])
                });
            } else back([])
        });
    };


    function getAllMonths(arr){
        for(var i=0; i<12; i++){
            var exist = false;
            for(var j=0; j<arr.length; j++){
                if(arr[j]._id == i+1){
                    exist = true;
                    break;
                }
            }
            if(!exist){
                // var year = (i+1 == 12) ?
                arr.push({_id:i+1,users:[],count:0,year:[new Date().getFullYear()]})
            }
        }
        arr.sort(function (o1, o2) {
            return o1._id < o2._id ? -1 : o1._id > o2._id ? 1 : 0;
        });
        return arr;
    }

    /* interactions summary from interactions */

    this.totalInteractionsIndividualSummary = function(userId,withUserId,isEmailId,needProjection,mobileNumber,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {

            if(!e && checkRequired(d) && d.length > 0){
                getInvalidEmailListFromDb(function(invalidEmailList) {
                    var queryOr = [];
                    if (checkRequired(d[0]) && checkRequired(d[0]._id)) {
                        queryOr.push({
                            action: d[0]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[0].refArr}
                        })
                        if (isEmailId) {

                            if (checkRequired(mobileNumber) && mobileNumber != 'null') {
                                queryOr[0].$or = [{emailId: withUserId}, {mobileNumber: mobileNumber}]
                            } else
                                queryOr[0].emailId = withUserId;
                        }
                        else {
                            if (checkRequired(mobileNumber) && mobileNumber != 'null') {
                                queryOr[0].$or = [{userId: withUserId}, {mobileNumber: mobileNumber}]
                            } else
                                queryOr[0].userId = withUserId;
                        }
                    }
                    if (checkRequired(d[1]) && checkRequired(d[1]._id)) {
                        queryOr.push({
                            action: d[1]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[1].refArr}
                        })
                        if (isEmailId) {
                            if (checkRequired(mobileNumber) && mobileNumber != 'null') {
                                queryOr[1].$or = [{emailId: withUserId}, {mobileNumber: mobileNumber}]
                            } else
                                queryOr[1].emailId = withUserId;
                        }
                        else {
                            if (checkRequired(mobileNumber) && mobileNumber != 'null') {
                                queryOr[1].$or = [{userId: withUserId}, {mobileNumber: mobileNumber}]
                            } else
                                queryOr[1].userId = withUserId;
                        }
                    }

                    var aggregateQuery;
                    if (needProjection) {
                        aggregateQuery = [
                            {
                                $match: {
                                    $or: queryOr,
                                    emailId: {$nin: invalidEmailList}
                                }
                            },
                            {
                                $project: {
                                    _id: 1,
                                    emailId: 1,
                                    title: 1,
                                    interactionDate: 1,
                                    type: 1,
                                    subType: 1,
                                    createdDate: 1,
                                    refId: 1,
                                    userId: 1,
                                    trackInfo: 1
                                }
                            },
                            {
                                $sort: {interactionDate: -1}
                            }
                        ]
                    }
                    else {
                        aggregateQuery = [
                            {
                                $match: {
                                    $or: queryOr,
                                    emailId: {$nin: invalidEmailList}
                                }
                            },
                            {
                                $sort: {interactionDate: -1}
                            }
                        ]
                    }

                    interactions.aggregate(aggregateQuery).exec(function (err, interactions) {

                        if (!err && checkRequired(interactions) && interactions.length > 0) {
                            back(interactions);
                        }
                        else back([])
                    });
                });
            } else back([])
        });
    };

    this.totalUsersInteractionsByEmail = function(emails, date,callback){
        
        var interactions = 0;
        async.forEach(emails, function(email, asynccallback) {
            InteractionsCollection.aggregate([{$match:{ownerEmailId:email, emailId:{$ne:email}, $or:[ {createdDate:{$lt:new Date(date.max)}}, {createdDate:null}] } }]).exec(function (error, interaction) {
                if(error) {
                    logger.info('Error in totalUsersInteractionsByEmail():interactionsManagement ',error);
                    asynccallback()
                }
                else {
                    interactions += interaction.length;
                    asynccallback()
                }
            });
        }, function(err) {
            if (err) {
                logger.info('Error in totalUsersInteractionsByEmail():interactionsManagement ',error);
                callback(interactions);
            }
            else
                callback(interactions);

        });
    };

    /* total interactions with one user */

    this.totalInteractionsWithOneUserCount = function(userId,withUserId,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:withUserId,
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:withUserId,
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            count: {
                                $sum: 1
                            }
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    this.totalInteractionsSelfCountAndLastInteracted = function(userId,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {

            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        refId:{$in:d[0].refArr},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver'
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        refId:{$in:d[1].refArr},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver'
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            lastInteracted:{$max:"$interactionDate"},
                            arr:{$addToSet:"$interactionDate"},
                            count: {
                                $sum: 1
                            }
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        if(checkRequired(interactions[0].arr) && interactions[0].arr.length > 0){

                            interactions[0].arr.sort(function(a, b){return new Date(b)-new Date(a)});
                            for(var i=0; i<interactions[0].arr.length; i++){
                                if(new Date(interactions[0].arr[i]) <= new Date()){
                                    interactions[0].lastInteracted = interactions[0].arr[i];
                                    break;
                                }
                            }
                            back(interactions);
                        }else
                            back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };


    this.totalInteractionsSelfLastFiveInteracted = function(userId,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {

            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {

                        refId:{$in:d[0].refArr},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver'
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        refId:{$in:d[1].refArr},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver'
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $match:{
                            interactionDate:{$lte:new Date()}
                        }
                    },
                    {
                        $sort : { interactionDate : -1 }
                    },
                    { $limit : 5 }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){

                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    /* Interactions by sub type  */

    this.totalInteractionsBySubType = function(userId,back){
        var date = new Date();
        var today = new Date();

        date.setHours(0,0,0,0);
        today.setHours(0,0,0,0);
        today.setDate(today.getDate() + 1);
        date.setDate(date.getDate() - 90);
        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:date,
                        $lte:today
                    }
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:{ $nin: [userId] },
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:{ $nin: [userId] },
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group: {
                            _id: "$subType",
                            count: {
                                $sum: 1
                            }
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    /* interactions summary from interactions */

    this.interactionsByDateDashboard = function(userId,back){
        var date = new Date();
        var today = new Date();

        date.setHours(0,0,0,0);
        today.setHours(0,0,0,0);
        today.setDate(today.getDate() + 2);
        date.setDate(date.getDate() - 4);

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:date,
                        $lte:today
                    },
                    type:{$in:['meeting','google-meeting']}
                }
            }
        ]).exec(function ( e, d ) {

            if(e){
                back([]);
            }else{

                if(checkRequired(d) && d.length > 0){
                    back(d)
                }else back([]);
            }
        });
    };

    this.interactionsByDateMinMaxDashboard = function(userId,dateMin,dateMax,back){

        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:new Date(dateMin),
                        $lte:new Date(dateMax)
                    },
                    type:{$in:['meeting']}
                }
            },
            {
                $group:{
                    _id:null,
                    refArr:{ $addToSet: "$refId" }
                }
            }
        ]).exec(function ( e, d ) {
            if(e){
                back([]);
            }else{
                if(checkRequired(d) && d.length > 0 && d[0] && d[0].refArr && d[0].refArr.length > 0){
                    scheduleInvitation.find({invitationId:{$in:d[0].refArr}}).populate('tasks',null,null,{ sort: { 'dueDate': 1 }}).exec(function(error,meetings){
                        if(checkRequired(meetings) && meetings.length > 0){
                            back(meetings)
                        }else back([])
                    })
                }else back([]);
            }
        })
    };

    this.removeInteractions = function(refId){
        InteractionsCollection.remove({refId:refId},function(error,result){

        })
    };

    this.removeUserInteractionRefIdEmailId = function(refId,emailId){
        InteractionsCollection.remove({refId:refId,ownerEmailId:emailId},function(error,result){

        })
    };

    this.removeUserInteractions = function(userId,callback){
        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:'sender'
                }
            },
            {
                $group: {
                    _id: null,
                    refArr:{ $addToSet: "$refId" },
                    count: { $sum: 1 }
                }
            }
        ]).exec(function(error,data){
            interactions.remove({userId:userId},function(error,result){
                if(checkRequired(data) && data.length >0 && checkRequired(data[0]) && checkRequired(data[0].refArr) && data[0].refArr.length > 0){
                    interactions.remove({refId:{$in:data[0].refArr}},function(error,result){

                    })
                }

                if(error){
                    callback(false,'onDeleteInteractions',error,result);
                }
                else callback(true,'onDeleteInteractions',error,result);
            })
        })
    };

    function nextPopulateUser(youNotReplied2,callback){
        if(youNotReplied2.length > 0){
            myUserCollection.populate(youNotReplied2,{path:'userId',select:'firstName lastName emailId'},function(err,arr){
                callback(err,arr)
            })
        }
        else callback({error:'error'},null);
    }

    this.totalInteractionCountByRange = function(userId,back){
        InteractionsCollection.aggregate([
            { $match:{
                ownerId:userId,
                interactionDate:{$gte: moment().subtract(90, "days").toDate()}
            }},
            { $group: { _id: null, count: { $sum: 1 } } }
        ]).exec(function ( e, d ) {
            if(e){
                back(0);
            }else{
                back(d);
            }
        });
    };

    /*  Top 5 people interactions */
    this.interactionIndividualTopFive = function(userId,emailId,start,end,back){
        var date = new Date(start);
        var today = new Date(end);

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:date,
                        $lte:today
                    }
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:{ $ne:userId },
                        emailId:{$nin:['',emailId]},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:{ $ne:userId },
                        emailId:{$nin:['',emailId]},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group: {
                            _id: "$emailId",
                            lastInteracted:{$max:"$interactionDate"},
                            count: {
                                $sum: 1
                            }
                        }
                    },
                    {
                        $sort:{
                            count:-1
                        }
                    }
                ]).exec(function(err,interactions){
                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    // Interactions for given number of days /* IN-PROGRESS */
    this.losingTouchByDateORDays = function(userId,emailIdArr,minDate,mobileNumberArr,back){
        var date;
        if(!minDate){
            date = new Date();
            date.setHours(0,0,0,0);
            date.setDate(date.getDate() - 15);
        }
        else date = new Date(minDate);

        InteractionsCollection.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {

            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:{ $ne: userId },
                        emailId:{$in:emailIdArr},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:{ $ne: userId },
                        emailId:{$in:emailIdArr},
                        mobileNumber:{$in:mobileNumberArr},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                InteractionsCollection.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group: {
                            _id: "$emailId",
                            lastInteracted:{$max:"$interactionDate"},
                            count:{$sum:1},
                            interactions:{
                                $push:{
                                    "_id" : "$_id",
                                    "refId" : "$refId",
                                    "userId" : "$userId",
                                    "emailId" : "$emailId",
                                    "type" : "$type",
                                    "subType" : "$subType",
                                    "action" : "$action",
                                    "interactionDate" : "$interactionDate",
                                    "createdDate" : "$createdDate",
                                    "source" : "$source",
                                    "title" : "$title",
                                    "description" : "$description",
                                    "endDate" : "$endDate",
                                    "firstName" : "$firstName",
                                    "lastName" : "$lastName",
                                    "publicProfileUrl" : "$publicProfileUrl",
                                    "profilePicUrl" : "$profilePicUrl",
                                    "mobileNumber" : "$mobileNumber",
                                    "companyName" : "$companyName",
                                    "designation" : "$designation"
                                }
                            }
                        }
                    },
                    {
                        $match:{
                            lastInteracted:{$lte:date}
                        }
                    },
                    {
                        $sort:{lastInteracted:1,"interactions.interactionDate":-1}
                    },
                    {
                        $unwind:"$interactions"
                    },
                    {
                        $project:{
                            _id:1,
                            lastInteracted:1,
                            interaction:{
                                $cond:{if:{$eq:["$lastInteracted","$interactions.interactionDate"]},then:"$interactions",else:false}
                            }
                        }
                    },
                    {
                        $match:{
                            interaction:{$ne:false}
                        }
                    },
                    {
                        $group:{
                            _id:null,
                            losingTouch:{
                                $push:{
                                    _id: "$interaction._id",
                                    "userId" : "$interaction.userId",
                                    "emailId" : "$interaction.emailId",
                                    "type" : "$interaction.type",
                                    "interactionDate" : "$interaction.interactionDate",
                                    "firstName" : "$interaction.firstName",
                                    "lastName" : "$interaction.lastName",
                                    "profilePicUrl" : "$interaction.profilePicUrl",
                                    "companyName" : "$interaction.companyName",
                                    "designation" : "$interaction.designation",
                                    "mobileNumber":"$interaction.mobileNumber"
                                }
                            }
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions[0].losingTouch);
                    }
                    else back([])
                });
            } else back([])
        });
    };
    
    this.getRecentlyInteractedContacts = function (userId,callback) {

        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId,
                    userId:{$ne:userId},
                    interactionDate:{$gte: moment().subtract(15, "days").toDate(),$lte: new Date()}
                }
            },
            {
                $project:{
                    emailOrMobile:{$cond:{ if:"$emailId", then:"$emailId", else:"$mobileNumber" }},
                    interactionDate:"$interactionDate",
                    interactionType:"$interactionType"
                }
            },
            { $sort: { 'interactionDate': 1 } },
            {
                $group:
                {
                    _id: "$emailOrMobile",
                    lastInteractedDate: { $max: "$interactionDate" },
                    interactionType: { $last: "$interactionType" },
                }
            }
        ]).exec(function (err,contacts) {
            
            if(!err){
                callback(contacts)
            } else {
                callback([])
            }
        });
    };

    this.getRecentlyInteractedContactsByEmailId = function (userId,emailIds,callback) {

        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId,
                    userId:{$ne:userId},
                    interactionDate:{$gte: moment().subtract(90, "days").toDate(),$lte: new Date()},
                    emailId:{$in:emailIds}
                }
            },
            {
                $project:{
                    emailOrMobile:{$cond:{ if:"$emailId", then:"$emailId", else:"$mobileNumber" }},
                    interactionDate:"$interactionDate",
                    interactionType:"$interactionType",
                    action:"$action"
                }
            },
            { $sort: { 'interactionDate': 1 } },
            {
                $group:
                {
                    _id: "$emailOrMobile",
                    lastInteractedDate: { $max: "$interactionDate" },
                    interactionType: { $last: "$interactionType" },
                    interactionAction: { $last: "$action" }
                }
            }
        ]).exec(function (err,contacts) {
            if(!err){
                callback(contacts)
            } else {
                callback([])
            }
        });
    };

    this.losingTouchByDateORDaysGetEmailIdArray = function(userId,emailIdArr,minDate,back){
        var date;
        if(!minDate){
            date = new Date();
            date.setHours(0,0,0,0);
            date.setDate(date.getDate() - 15);
        }
        else date = new Date(minDate);

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {

            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:{ $ne: userId },
                        emailId:{$in:emailIdArr},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:{ $ne: userId },
                        emailId:{$in:emailIdArr},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group: {
                            _id: "$emailId",
                            lastInteracted:{$max:"$interactionDate"},
                            count:{$sum:1},
                            interactions:{
                                $push:{
                                    "_id" : "$_id",
                                    "emailId" : "$emailId",
                                    "interactionDate" : "$interactionDate"
                                }
                            }
                        }
                    },
                    {
                        $match:{
                            lastInteracted:{$lte:date}
                        }
                    },
                    {
                        $sort:{lastInteracted:1,"interactions.interactionDate":-1}
                    },
                    {
                        $unwind:"$interactions"
                    },
                    {
                        $project:{
                            _id:1,
                            lastInteracted:1,
                            interaction:{
                                $cond:{if:{$eq:["$lastInteracted","$interactions.interactionDate"]},then:"$interactions",else:false}
                            }
                        }
                    },
                    {
                        $match:{
                            interaction:{$ne:false}
                        }
                    },
                    {
                        $group:{
                            _id:null,
                            losingTouch:{
                                $addToSet:"$interaction.emailId"
                            }
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions[0].losingTouch);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    this.interactionWithContactBySubtypes = function(userId,cUserId,dateMin,dateMax,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{$gte:new Date(dateMin),$lte:new Date(dateMax)}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:cUserId,
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:cUserId,
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }
                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group:{
                            _id:"$subType",
                            count:{$sum:1}
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    this.nextInteractionWithContact = function(userId,cUserId,isEmailId,dateMin,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{$gt:new Date(dateMin)}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                    if(isEmailId){
                        queryOr[0].emailId = cUserId;
                    }
                    else queryOr[0].userId = cUserId;
                }

                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                    if(isEmailId){
                        queryOr[1].emailId = cUserId;
                    }
                    else queryOr[1].userId = cUserId;
                }
                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $sort:{
                            interactionDate:1
                        }
                    },
                    {
                        $limit:1
                    },
                    {
                        $project:{
                            _id: 1,
                            type:1,
                            title:1,
                            interactionDate:1,
                            mobileNumber:1
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    this.lastCallInteractionWithContact = function(userId,cUserId,dateMin,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    type:'call'
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:cUserId,
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr},
                        interactionDate:{$lte:new Date(dateMin)}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:cUserId,
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr},
                        interactionDate:{$lte:new Date(dateMin)}
                    })
                }
                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $sort:{
                            interactionDate:-1
                        }
                    },
                    {
                        $limit:1
                    },
                    {
                        $project:{
                            _id:1,
                            type:1,
                            title:1,
                            interactionDate:1
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    this.getInteractionsToAddFavoriteContacts = function(userId,callback){
        InteractionsCollection.aggregate([
            {$match:{ownerId:userId,userId:{$ne:userId}}},
            {$match:{interactionDate:{$gt:moment().subtract(180, "days").toDate()}}},
            {$group:{_id:"$emailId",count:{$sum:1}}},
            {$sort:{count:-1}},
            {$match:{_id:{$ne:null}}},
            {$limit:10}
        ],function(error, result){
            if(error){
                callback([])
            }
            else{
                callback(result)
            }
        })
    }

    this.getMoreThanFiveInteractedEmails = function(userId,dateMin,back){
        
        InteractionsCollection.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{$gte:new Date(dateMin)}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:{$ne:userId},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:{$ne:userId},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }
                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr,
                            emailId:{ $nin: [ '',/info/i, /support/i, /admin/i, /hello/i, /noreply/i, /no-reply/i, /help/i, /mailer-daemon/i, /mail-noreply/i, /alert/i, /calendar-notification/i, /eBay/i, /flipkartletters/i, /pinterest/i ] }
                        }
                    },
                    {
                        $group:{
                            _id:"$emailId",
                            count:{$sum:1}
                        }
                    },
                    {
                        $match:{
                            count:{$gt:5}
                        }
                    },
                    {
                        $group:{
                            _id:null,
                            emails:{$addToSet:"$_id"}
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0 && interactions[0] && interactions[0].emails && interactions[0].emails.length > 0){
                        back(interactions[0].emails);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    this.interactionIndividualTopFive_new_withTypeCount = function(userId,emailId,start,end,back){
        var date = new Date(start);
        var today = new Date(end);

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:date,
                        $lte:today
                    }
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){
                getInvalidEmailListFromDb(function(invalidEmailList) {
                    var queryOr = [];
                    if (checkRequired(d[0]) && checkRequired(d[0]._id)) {
                        queryOr.push({
                            userId: {$ne: userId},
                            emailId: {$nin: ['', emailId]},
                            action: d[0]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[0].refArr}
                        })
                    }
                    if (checkRequired(d[1]) && checkRequired(d[1]._id)) {
                        queryOr.push({
                            userId: {$ne: userId},
                            emailId: {$nin: ['', emailId]},
                            action: d[1]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[1].refArr}
                        })
                    }

                    interactions.aggregate([
                        {
                            $match: {
                                $or: queryOr,
                                emailId: {$nin: invalidEmailList}
                            }
                        },
                        {
                            $group: {
                                _id: {emailId: "$emailId", type: "$type"},
                                lastInteracted: {$max: "$interactionDate"},
                                count: {
                                    $sum: 1
                                },
                                interactions: {
                                    $push: {
                                        "_id": "$_id",
                                        "refId": "$refId",
                                        "userId": "$userId",
                                        "emailId": "$emailId",
                                        "type": "$type",
                                        "subType": "$subType",
                                        "action": "$action",
                                        "interactionDate": "$interactionDate",
                                        "createdDate": "$createdDate",
                                        "source": "$source",
                                        "title": "$title",
                                        "description": "$description",
                                        "endDate": "$endDate",
                                        "firstName": "$firstName",
                                        "lastName": "$lastName",
                                        "publicProfileUrl": "$publicProfileUrl",
                                        "profilePicUrl": "$profilePicUrl",
                                        "mobileNumber": "$mobileNumber",
                                        "companyName": "$companyName",
                                        "designation": "$designation"
                                    }
                                }
                            }
                        },
                        {
                            $group: {
                                _id: "$_id.emailId",
                                types: {
                                    $push: {
                                        "type": "$_id.type",
                                        "count": "$count"
                                    }
                                },
                                lastInteracted: {$max: "$lastInteracted"},
                                total: {$sum: "$count"},
                                interactions: {
                                    $push: "$interactions"
                                }
                            }
                        },
                        {
                            $unwind: "$interactions"
                        },
                        {
                            $unwind: "$interactions"
                        },
                        {
                            $project: {
                                _id: 1,
                                lastInteracted: 1,
                                total: 1,
                                types: 1,
                                interaction: {
                                    $cond: {
                                        if: {$eq: ["$lastInteracted", "$interactions.interactionDate"]},
                                        then: "$interactions",
                                        else: false
                                    }
                                }
                            }
                        },
                        {
                            $match: {
                                interaction: {$ne: false}
                            }
                        },
                        {
                            $group: {
                                _id: null,
                                topInteractions: {
                                    $push: {
                                        "_id": "$interaction._id",
                                        "userId": "$interaction.userId",
                                        "emailId": "$interaction.emailId",
                                        "interactionDate": "$interaction.interactionDate",
                                        "firstName": "$interaction.firstName",
                                        "lastName": "$interaction.lastName",
                                        "profilePicUrl": "$interaction.profilePicUrl",
                                        "mobileNumber": "$interaction.mobileNumber",
                                        "count": "$total",
                                        "types": "$types"
                                    }
                                }
                            }
                        }
                    ]).exec(function (err, interactions) {
                        if (!err && checkRequired(interactions) && interactions.length > 0 && interactions[0] && interactions[0].topInteractions) {
                            interactions[0].topInteractions.sort(function (a, b) {
                                if (a.total < b.total) return 1;
                                if (a.total > b.total) return -1;
                                return 0;
                            });
                            back(interactions[0].topInteractions);
                        }
                        else back([])
                    });
                });
            } else back([])
        });
    };
// added By Rajiv - 05042016
    this.getInteractionsForContact = function(user,req,dateMin,dateMax,idType,back){
        var query1
        if(idType == "mobileNumber"){
            query1 = {
                "ownerId":user._id,
                "userId":{$ne:user._id},
                "mobileNumber": req.query.mobileNumber,
                "interactionDate": dateMin?{$gte: new Date(dateMin), $lte: new Date(dateMax)}:{$lte: new Date(dateMax)},
                "interactionType": {$ne: "task"}
            }
        }if(idType == "userId") {
            query1 = {
                "ownerId":user._id,
                "userId": castToObjectId(req.query.id),
                "interactionDate": dateMin?{$gte: new Date(dateMin), $lte: new Date(dateMax)}:{$lte: new Date(dateMax)},
                "interactionType": {$ne: "task"}
            }
        } if(idType == "emailId") {

            if(req.query.mobileNumber){

                var mobileNumberRegEx = req.query.mobileNumber.length <= 10 ? req.query.mobileNumber : req.query.mobileNumber.substr(req.query.mobileNumber.length - 10);
                query1 = {
                    "ownerId":user._id,
                    "userId":{$ne:user._id},
                    "interactionDate": dateMin?{$gte: new Date(dateMin), $lte: new Date(dateMax)}:{$lte: new Date(dateMax)},
                    "interactionType": {$ne: "task"}
                }
                query1.$or = [{"emailId":req.query.id},{"mobileNumber":new RegExp('\d{0,4}'+mobileNumberRegEx+'$')}]
            } else {
                query1 = {
                    "ownerId":user._id,
                    "userId":{$ne:user._id},
                    "emailId": req.query.id,
                    "interactionDate": dateMin?{$gte: new Date(dateMin), $lte: new Date(dateMax)}:{$lte: new Date(dateMax)},
                    "interactionType": {$ne: "task"}
                }
            }
        }

        var mIntObj = {

            _id:"$_id",
            refId:"$refId",
            userId:"$userId",
            emailId:"$emailId",
            action:"$action",
            interactionDate:"$interactionDate",
            createdDate:"$createdDate",
            source:"$source",
            title:"$title",
            description:"$description",
            mobileNumber:"$mobileNumber",
            endDate:"$endDate",
            interactionType:"$interactionType",
            subType:"$subType",
            trackInfo: "$trackInfo",
            trackId: "$trackId",
            duration: "$duration"
        }

        InteractionsCollection.aggregate([
            {
                $match:query1
            },
            {
                $sort:{"interactionDate":-1}
            },
            // {
            //     $limit:5
            // },
            {
                $group:{
                    _id:null,
                    meetingArray:{
                        "$push": { "$cond" :{
                            if:{$eq:["$interactionType","meeting"]},
                            then:mIntObj,
                            else: "$noval"
                        }
                        }
                    },
                    emailArray:{
                        "$push": { "$cond" :{
                            if:{$eq:["$interactionType","email"]},
                            then:mIntObj,
                            else: "$noval"
                        }
                        }
                    },
                    callArray:{
                        "$push": { "$cond" :{
                            if:{$eq:["$interactionType","call"]},
                            then:mIntObj,
                            else: "$noval"
                        }
                        }
                    },
                    smsArray:{
                        "$push": { "$cond" :{
                            if:{$eq:["$interactionType","sms"]},
                            then:mIntObj,
                            else: "$noval"
                        }
                        }
                    },
                    totalCount:{$sum:1}
                }
            }],function(err,res){
            back(err,res)
        })
    }


    this.lastInteractionWithUserMobile = function(userId,cId,type,req,back){

        var query1
        var idType = type

        var limit = req.query.limit?req.query.limitBy:1

        if(idType == "mobileNumber"){
            query1 = {
                "ownerId":userId,
                "userId":{$ne:userId},
                "mobileNumber": req.query.mobileNumber,
                "interactionDate": {$lte: new Date()},
                "interactionType": {$ne: "task"}
            }
        }if(idType == "userId") {
            query1 = {
                "ownerId":user._id,
                "userId": castToObjectId(req.query.id),
                "interactionDate": {$lte: new Date()},
                "interactionType": {$ne: "task"}
            }
        } if(idType == "emailId") {
            query1 = {
                "ownerId":userId,
                "emailId": req.query.id,
                "interactionDate": {$lte: new Date()},
                "interactionType": {$ne: "task"}
            }
        }

        var project = {
            _id: 1,
            type: 1,
            interactionDate: 1,
            title: 1,
            refId: 1,
            action: 1,
            userId: 1,
            mobileNumber: 1
        }

        InteractionsCollection.find(query1,project).sort({"interactionDate":-1}).limit(limit).exec(function(err,res){
            if(err) {
                back([])
            }
            back(res[0])
        })

    }


    this.lastInteractionsWithSubTypeCountOneUser = function(userId,cUserId,isEmailId,dateMin,dateMax,back){

        interactions.aggregate([
            {
                $match:{userId:castToObjectId(userId)}
            },
            {
                $unwind:"$interactions"
            },
            {
                $match:{
                    "interactions.emailId":cUserId,
                    "interactions.action":{$in:['sender','receiver']},
                    "interactions.interactionDate":{$gte:new Date(dateMin),$lte:new Date(dateMax)}
                }
            },
            {
                $group: {
                    _id: "$interactions.action",
                    refArr:{ $addToSet: "$interactions.refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){
                getInvalidEmailListFromDb(function(invalidEmailList) {
                    var queryOr = [];
                    if (checkRequired(d[0]) && checkRequired(d[0]._id)) {
                        queryOr.push({
                            "interactions.action": d[0]._id == 'receiver' ? 'sender' : 'receiver',
                            "interactions.refId": {$in: d[0].refArr},
                            "interactions.emailId": cUserId
                        })
                        /*if(isEmailId){
                         queryOr[0].emailId = cUserId;
                         }
                         else queryOr[0].interactions.userId = cUserId;*/
                    }
                    if (checkRequired(d[1]) && checkRequired(d[1]._id)) {
                        queryOr.push({
                            "interactions.action": d[1]._id == 'receiver' ? 'sender' : 'receiver',
                            "interactions.refId": {$in: d[1].refArr},
                            "interactions.emailId": cUserId
                        })
                        /*if(isEmailId){
                         queryOr[1].interactions.emailId = cUserId;
                         }
                         else queryOr[1].interactions.userId = cUserId;*/
                    }

                    interactions.aggregate([
                        {
                            $match: {
                                $or: queryOr,
                                emailId: {$nin: invalidEmailList}
                            }
                        },
                        {
                            $group: {
                                _id: "$interactions.interactionType",
                                count: {
                                    $sum: 1
                                },
                                interactions: {
                                    $push: {
                                        "_id": "$interactions._id",
                                        "refId": "$interactions.refId",
                                        "userId": "$interactions.userId",
                                        "emailId": "$interactions.emailId",
                                        "type": "$interactions.interactionType",
                                        "subType": "$interactions.subType",
                                        "action": "$interactions.action",
                                        "interactionDate": "$interactions.interactionDate",
                                        "createdDate": "$interactions.createdDate",
                                        "source": "$interactions.source",
                                        "title": "$interactions.title",
                                        "description": "$interactions.description",
                                        "endDate": "$interactions.endDate",
                                        "firstName": "$interactions.firstName",
                                        "lastName": "$interactions.lastName",
                                        "publicProfileUrl": "$interactions.publicProfileUrl",
                                        "profilePicUrl": "$interactions.profilePicUrl",
                                        "mobileNumber": "$interactions.mobileNumber",
                                        "companyName": "$interactions.companyName",
                                        "designation": "$interactions.designation"
                                    }
                                }
                            }
                        },
                        {
                            $group: {
                                _id: null,
                                types: {
                                    $push: {
                                        "type": "$interactions._id",
                                        "count": "$count"
                                    }
                                },
                                total: {$sum: "$count"},
                                interactions: {
                                    $push: "$interactions"
                                }
                            }
                        },
                        {
                            $unwind: "$interactions"
                        },
                        {
                            $unwind: "$interactions"
                        },
                        {
                            $sort: {
                                "interactions.interactionDate": -1
                            }
                        },
                        {
                            $limit: 3
                        },
                        {
                            $project: {
                                types: 1,
                                total: 1,
                                interactions: {
                                    "_id": 1,
                                    "type": 1,
                                    "interactionDate": 1,
                                    "title": 1
                                }
                            }
                        }
                    ]).exec(function (err, interactions) {

                        if (!err && checkRequired(interactions) && interactions.length > 0 && interactions[0]) {
                            var obj = {
                                interactionsCountByType: interactions[0].types,
                                totalInteractions: interactions[0].total,
                                lastInteractions: []
                            };

                            for (var i = 0; i < interactions.length; i++) {
                                obj.lastInteractions.push(interactions[i].interactions);
                            }

                            back(obj);
                        }
                        else back({
                            interactionsCountByType: [],
                            totalInteractions: 0,
                            lastInteractions: []
                        })
                    });
                });
            } else back({
                interactionsCountByType:[],
                totalInteractions:0,
                lastInteractions:[]
            })
        });
    };


// New Web API's
    this.interactionCountAndCompanyCount = function(userId,emailId,minDate,days,back){
        var date;
        if(!minDate){
            date = new Date();
            date.setHours(0,0,0,0);
            date.setDate(date.getDate() - days);
        }
        else date = new Date(minDate);

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:date
                    }
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {

            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:{ $nin: [userId] },
                        emailId:{$ne:emailId},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:{ $nin: [userId] },
                        emailId:{$ne:emailId},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            companies:{$addToSet:"$companyName"},
                            interactionsCount:{$sum:1}
                        }
                    },
                    {
                        $project:{
                            _id:0,
                            companiesCount:{$size:"$companies"},
                            interactionsCount:1
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    // Emails sent vs received
    this.getEmailInteractionInitiationsCount = function(userId,dateMin,dateMax,callback){
        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    type:'email',
                    interactionDate:{
                        $gte:new Date(dateMin),$lte:new Date(dateMax)
                    }
                }
            },
            {
                $group: {
                    _id: "$action",
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){
                callback(d)
            } else callback([])
        });
    };

    this.documentsSharedWithContactEmailArr = function(userId,contactEmailIdArr,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    type:"document-share"
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        emailId:{$in:contactEmailIdArr},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        emailId:{$in:contactEmailIdArr},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }
                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $project: {
                            _id: "$_id",
                            emailId: "$emailId",
                            firstName: "$firstName",
                            interactionDate: "$interactionDate",
                            type: "$type",
                            subType: "$subType",
                            action: "$action",
                            title: "$title",
                            refId: "$refId",
                            createdDate: "$createdDate"
                        }
                    },
                    {
                        $sort:{interactionDate:-1}
                    },
                    {
                        $limit:5
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    this.getInteractionsByDateEmailArray = function(userId,emails,dateMin,dateMax,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{$gte:new Date(dateMin),$lte:new Date(dateMax)}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0) {
                getInvalidEmailListFromDb(function(invalidEmailList) {
                    var queryOr = [];
                    if (checkRequired(d[0]) && checkRequired(d[0]._id)) {
                        queryOr.push({
                            emailId: {$in: emails},
                            action: d[0]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[0].refArr}
                        })

                    }
                    if (checkRequired(d[1]) && checkRequired(d[1]._id)) {
                        queryOr.push({
                            emailId: {$in: emails},
                            action: d[1]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[1].refArr}
                        })

                    }

                    interactions.aggregate([
                            {
                                $match: {
                                    $or: queryOr,
                                    emailId: {$nin: invalidEmailList}
                                }
                            },
                            {
                                $project: {
                                    _id: "$_id",
                                    emailId: "$emailId",
                                    userId: "$userId",
                                    firstName: "$firstName",
                                    interactionDate: "$interactionDate",
                                    type: "$type",
                                    subType: "$subType",
                                    action: "$action",
                                    title: "$title",
                                    refId: "$refId",
                                    createdDate: "$createdDate",
                                    trackInfo: "$trackInfo"
                                }
                            },
                            {
                                $sort: {interactionDate: -1}
                            },
                            {
                                $limit: 5
                            }
                        ]
                    ).exec(function (err, interactions) {
                        if (!err && checkRequired(interactions) && interactions.length > 0) {
                            back(interactions)
                        }
                        else back([])
                    })
                });
            }
        })
    };

    this.getInteractedEmailsDate = function(userId,dateMin,back){
        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{$gte:new Date(dateMin)}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0) {
                getInvalidEmailListFromDb(function(invalidEmailList) {
                    var queryOr = [];
                    if (checkRequired(d[0]) && checkRequired(d[0]._id)) {
                        queryOr.push({
                            userId: {$ne: userId},
                            action: d[0]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[0].refArr}
                        })

                    }
                    if (checkRequired(d[1]) && checkRequired(d[1]._id)) {
                        queryOr.push({
                            userId: {$ne: userId},
                            action: d[1]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[1].refArr}
                        })

                    }

                    interactions.aggregate([
                            {
                                $match: {
                                    $or: queryOr,
                                    emailId: {$nin: invalidEmailList}
                                }
                            },
                            {
                                $group: {
                                    _id: null,
                                    emailIdArr: {$addToSet: "$emailId"}
                                }
                            }
                        ]
                    ).exec(function (err, interactions) {
                        if (!err && checkRequired(interactions) && interactions.length > 0 && interactions[0] && checkRequired(interactions[0].emailIdArr) && interactions[0].emailIdArr.length > 0) {
                            back(interactions[0].emailIdArr)
                        }
                        else back([])
                    })
                });
            }
        })
    };

    this.getInteractionsCountByLocationDate = function(userId,dateMin,back){
        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{$gte:new Date(dateMin)}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0) {
                getInvalidEmailListFromDb(function(invalidEmailList) {
                    var queryOr = [];
                    if (checkRequired(d[0]) && checkRequired(d[0]._id)) {
                        queryOr.push({
                            userId: {$ne: userId},
                            action: d[0]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[0].refArr}
                        })

                    }
                    if (checkRequired(d[1]) && checkRequired(d[1]._id)) {
                        queryOr.push({
                            userId: {$ne: userId},
                            action: d[1]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[1].refArr}
                        })
                    }

                    interactions.aggregate([
                            {
                                $match: {
                                    $or: queryOr,
                                    emailId: {$nin: invalidEmailList},
                                    location: {$exists: true, $ne: ''}
                                }
                            },
                            {
                                $group: {
                                    _id: "$location",
                                    emailIdArr: {$addToSet: "$emailId"},
                                    count: {$sum: 1}
                                }
                            }
                        ]
                    ).exec(function (err, interactions) {
                        if (!err && checkRequired(interactions) && interactions.length > 0) {
                            back(interactions)
                        }
                        else back([])
                    })
                });
            }
        })
    };

    this.totalInteractionsByTypeDate = function(userId,dateMin,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{
                        $gte:new Date(dateMin)
                    }
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        userId:{ $nin: [userId] },
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        userId:{ $nin: [userId] },
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr
                        }
                    },
                    {
                        $group: {
                            _id: "$type",
                            count: {
                                $sum: 1
                            }
                        }
                    }
                ]).exec(function(err,interactions){

                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    this.lastInteractedWithEmails = function(userId,emailIdArr,start,end,back){
        var date;
        var today;
        var match;
        if(date !=null && today != null){
            date = new Date(start);
            today = new Date(end);
            match = {
                userId:userId,
                action:{$in:['sender','receiver']},
                interactionDate:{
                    $gte:date,
                    $lte:today
                }
            }
        }
        else{
            match = {
                userId:userId,
                action:{$in:['sender','receiver']}
            }
        }

        interactions.aggregate([
            {
                $match:match
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(!e && checkRequired(d) && d.length > 0){
                getInvalidEmailListFromDb(function(invalidEmailList) {
                    var queryOr = [];
                    if (checkRequired(d[0]) && checkRequired(d[0]._id)) {
                        queryOr.push({
                            emailId: {$in: emailIdArr},
                            action: d[0]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[0].refArr}
                        })
                    }
                    if (checkRequired(d[1]) && checkRequired(d[1]._id)) {
                        queryOr.push({
                            emailId: {$in: emailIdArr},
                            action: d[1]._id == 'receiver' ? 'sender' : 'receiver',
                            refId: {$in: d[1].refArr}
                        })
                    }

                    interactions.aggregate([
                        {
                            $match: {
                                $or: queryOr,
                                emailId: {$nin: invalidEmailList}
                            }
                        },
                        {
                            $group: {
                                _id: "$emailId",
                                lastInteracted: {$max: "$interactionDate"},
                                count: {
                                    $sum: 1
                                },
                                interactions: {
                                    $push: {
                                        "_id": "$_id",
                                        "refId": "$refId",
                                        "userId": "$userId",
                                        "emailId": "$emailId",
                                        "type": "$type",
                                        "subType": "$subType",
                                        "action": "$action",
                                        "interactionDate": "$interactionDate",
                                        "createdDate": "$createdDate",
                                        "source": "$source",
                                        "title": "$title",
                                        "description": "$description",
                                        "endDate": "$endDate",
                                        "firstName": "$firstName",
                                        "lastName": "$lastName",
                                        "publicProfileUrl": "$publicProfileUrl",
                                        "profilePicUrl": "$profilePicUrl",
                                        "mobileNumber": "$mobileNumber",
                                        "companyName": "$companyName",
                                        "designation": "$designation"
                                    }
                                }
                            }
                        },
                        {
                            $unwind: "$interactions"
                        },
                        {
                            $project: {
                                _id: 1,
                                lastInteracted: 1,
                                count: 1,
                                interaction: {
                                    $cond: {
                                        if: {$eq: ["$lastInteracted", "$interactions.interactionDate"]},
                                        then: "$interactions",
                                        else: false
                                    }
                                }
                            }
                        },
                        {
                            $match: {
                                interaction: {$ne: false}
                            }
                        },
                        {
                            $group: {
                                _id: null,
                                topInteractions: {
                                    $push: {
                                        "_id": "$interaction._id",
                                        "userId": "$interaction.userId",
                                        "emailId": "$interaction.emailId",
                                        "interactionDate": "$interaction.interactionDate",
                                        "title": "$interaction.title",
                                        "count": "$count"
                                    }
                                }
                            }
                        }
                    ]).exec(function (err, interactions) {
                        if (!err && checkRequired(interactions) && interactions.length > 0 && checkRequired(interactions[0]) && checkRequired(interactions[0].topInteractions) && interactions[0].topInteractions.length > 0) {
                            back(interactions[0].topInteractions);
                        }
                        else back([])
                    });
                });
            } else back([])
        });
    };

    this.getCompaniesUserInteracted = function(userId,emailId,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {

            if(!e && checkRequired(d) && d.length > 0){

                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        emailId:{$ne:emailId},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        emailId:{$ne:emailId},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr,
                            companyName:{$exists:true,$ne:''}
                        }
                    },
                    {
                        $group: {
                            _id: "$companyName",
                            lastInteractionDate:{$max:"$interactionDate"},
                            emails:{$addToSet:"$emailId"}
                        }
                    },
                    {
                        $project:{
                            _id:0,
                            companyName:"$_id",
                            contactsCount:{$size:"$emails"},
                            emails:1,
                            lastInteractionDate:1
                        }
                    }
                ]).exec(function(err,interactions){
                    if(!err && checkRequired(interactions) && interactions.length > 0){
                        back(interactions);
                    }
                    else back([])
                });
            } else back([])
        });
    };

    this.getCompaniesUserInteractedWithDateAndGetEmailIdsInteracted = function(userId,emailId,dateMin,back){

        interactions.aggregate([
            {
                $match:{
                    userId:userId,
                    action:{$in:['sender','receiver']},
                    interactionDate:{$gte:new Date(dateMin)}
                }
            },
            {
                $group: {
                    _id: "$action",
                    refArr:{ $addToSet: "$refId" },
                    count: {
                        $sum: 1
                    }
                }
            }
        ]).exec(function ( e, d ) {
            if(e){
                logger.info('Error in getCompaniesUserInteractedWithDateAndGetEmailIdsInteracted():interactionsManagement 1 ',e, userId);
            }
            if(!e && checkRequired(d) && d.length > 0){
                // Query for company count
                var queryOr = [];
                if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                    queryOr.push( {
                        emailId:{$ne:emailId},
                        action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[0].refArr}
                    })
                }
                if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                    queryOr.push( {
                        emailId:{$ne:emailId},
                        action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                        refId:{$in:d[1].refArr}
                    })
                }

                interactions.aggregate([
                    {
                        $match:{
                            $or:queryOr,
                            companyName:{$exists:true,$ne:''}
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            companies:{$addToSet:"$companyName"}
                        }
                    },
                    {
                        $project:{
                            _id:0,
                            interactionsCompanyCount:{$size:"$companies"}
                        }
                    }
                ]).exec(function(err,interactionsCompanyCount){
                    if(err){
                        logger.info('Error in getCompaniesUserInteractedWithDateAndGetEmailIdsInteracted():interactionsManagement 2 ',err, userId);
                    }
                    var interactionsCompany = 0;
                    if(!err && checkRequired(interactionsCompanyCount) && interactionsCompanyCount.length > 0){
                        interactionsCompany = interactionsCompanyCount[0].interactionsCompanyCount || 0;
                    }

                    var queryOr = [];
                    if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                        queryOr.push( {
                            emailId:{$ne:emailId},
                            action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                            refId:{$in:d[0].refArr}
                        })
                    }
                    if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                        queryOr.push( {
                            emailId:{$ne:emailId},
                            action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                            refId:{$in:d[1].refArr}
                        })
                    }
                    getInvalidEmailListFromDb(function(invalidEmailList) {
                        interactions.aggregate([
                            {
                                $match: {
                                    $or: queryOr,
                                    emailId: {$nin: invalidEmailList}
                                }
                            },
                            {
                                $match: {
                                    $or: [{emailId: {$exists: true, $ne: ""}}, {
                                        mobileNumber: {
                                            $exists: true,
                                            $ne: ""
                                        }
                                    }]
                                }
                            },
                            {
                                $group: {
                                    _id: null,
                                    emailIdArr: {$addToSet: "$emailId"},
                                    mobileNumberArr: {$addToSet: "$mobileNumber"}
                                }
                            }
                        ]).exec(function (err, interactedUserInfo) {
                            if (err) {
                                logger.info('Error in getCompaniesUserInteractedWithDateAndGetEmailIdsInteracted():interactionsManagement 3 ', err, userId);
                            }
                            var obj = {
                                interactionsWithCompany_Count: interactionsCompany,
                                interactedWithEmails: [],
                                interactedWithMobileNumber: []
                            };
                            if (checkRequired(interactedUserInfo) && interactedUserInfo.length > 0) {
                                obj.interactedWithEmails = removeInvalidContentFromArray(interactedUserInfo[0].emailIdArr, [null, '']);
                                obj.interactedWithMobileNumber = removeInvalidContentFromArray(interactedUserInfo[0].mobileNumberArr, [null, '']);
                            }
                            back(obj);
                        })
                    });
                });
            } else back(null)
        });
    };

    this.updateOpenTrack = function(trackId,emailId){
        interactions.update({trackId:trackId,emailId:emailId},{$set:{"trackInfo.lastOpenedOn":new Date()}},function(error,result){
            if(error){
                logger.info('Error in updateOpenTrack():InteractionManagement ',error);
            }
        })
    };

    this.updateOpenTrackWithReferenceId = function(refId){
        interactions.update({refId:refId},{$set:{"trackInfo.lastOpenedOn":new Date()}},{multi:true},function(error,result){
            if(error){
                logger.info('Error in updateOpenTrackWithReferenceId():InteractionManagement ',error);
            }
        })
    };

    this.updateOpenTrackWithUserIdAndReferenceId = function(refId,userId){
        interactions.update({refId:refId},{$set:{"trackInfo.lastOpenedOn":new Date()}},{multi:true},function(error,result){
            if(error){
                logger.info('Error in updateOpenTrackWithUserIdAndReferenceId():InteractionManagement ',error);
            }
        })
    };

    this.updateOpenTrackWithEmailIdAndReferenceId = function(refId,emailId){
        interactions.update({refId:refId,emailId:emailId},{$set:{"trackInfo.lastOpenedOn":new Date()}},function(error,result){
            if(error){
                logger.info('Error in updateOpenTrackWithEmailIdAndReferenceId():InteractionManagement ',error);
            }
        })
    };

    this.updateOpenTrackDoc = function(docId,emailId){
        interactions.update({refId:docId,emailId:emailId},{$set:{"trackInfo.lastOpenedOn":new Date()}},function(error,result){
            if(error){
                logger.info('Error in updateOpenTrackDoc():InteractionManagement ',error);
            }
        })
    };

    this.bulkUpdateInteractions = function(interactionsArr,userEmailId,source,operation,callback){
        var bulk = interactions.collection.initializeUnorderedBulkOp();
        for(var i=0; i<interactionsArr.length; i++){
            interactionsArr[i].userId = interactionsArr[i].userId.toString()
            bulk.find({userId:interactionsArr[i].userId,refId:interactionsArr[i].refId}).upsert().updateOne({$set: interactionsArr[i]});
        }
        bulk.execute(function(err, result) {

            if(err){
                logger.info('Error in bulkUpdateInteractions():InteractionsManagement ',err ,userEmailId, source)
            }
            else{
                result = result.toJSON();
                logger.info('bulkUpdateInteractions() results ',result, userEmailId, source);
            }
            if(callback) callback(true)
        })
    };

    //Scatter Plot + Top Five companies Interacted. Last modified
    this.getInteractionTimeline = function(userIdList, noOfDays, userId, userObjId,usersCompany,source,callback){
        var userSelectedArr = [];
        userSelectedArr = userIdList
        var userIdListSlice = []
        if((userId == userIdList[0])&& userIdList.length>1){
            userIdListSlice = _.filter(userIdList, function(x) {
                return x != userId
            });
        } else if(userId != userIdList[0]) {
            userIdListSlice = userIdList
        }

        myUserCollection.aggregate([
            {
                $match: {_id: {$in: userIdListSlice}}
            },{
                $unwind: "$contacts"
            },{
                $match: {
                    "contacts.account.showToReportingManager":true,
                    "contacts.account.name": {$nin:[null,usersCompany]}
                }
            },
            {
                $project: {
                    _id: "$_id",
                    email: "$contacts.personEmailId",
                    companyName: "$contacts.account.name"
                }
            }
        ]).exec(function(err, userCompanyFromContactsArray){
            var userGroup = _
                .chain(userCompanyFromContactsArray)
                .groupBy('_id')
                .map(function(value, key) {
                    return {
                        userId: castToObjectId(key),
                        emailArray: _.pluck(value, 'email')
                    }
                })
                .value();
            //build the query to fetch data
            var queryFinal = { "$or": userGroup.map(function(el) {
                el["ownerId"] = el.userId
                el["userId"] = {"$ne":el.userId}
                el["emailId"] = { "$in": el.emailArray };

                if(_.includes(source, 'outlook')){
                    el["source"] = {$ne:"google"}
                } else {
                    el["source"] = {$ne:"outlook"}
                }

                if(noOfDays > 0)
                    el["interactionDate"] = {
                        $gte: moment().subtract(noOfDays, "days").toDate(),
                        $lte: new Date()
                    }

                delete el.emailArray;
                return el;
            })};

            var aggregationPipelineOthers = getAggregationPipeline(userIdListSlice,queryFinal,true);

            //Start UserIDList except LIU
            InteractionsCollection.aggregate(aggregationPipelineOthers).exec(function(err, data){

                var userEmails = []
                    , emailWithCount = {}

                _.each(data, function(d){
                    userEmails.push(d.email)
                    d.lastInteractedDate = d.lastInteracted
                    d.lastInteracted = moment().diff(moment(d.lastInteracted), "days")
                    emailWithCount[d.email] = d

                })
                var userCompanyObject = {},
                    userCompanyFromContactsObject = {}
                var othersVisibility = false;
                _.each(userCompanyFromContactsArray, function(u){

                    if(u.companyName){
                    }
                    else {
                        othersVisibility = u.showToReportingManager
                    }
                    userCompanyFromContactsObject[u.email] = u.companyName
                })

                data = _.map(data, function(r){
                    r.company = userCompanyFromContactsObject[r.email] || r.companyName || "Others"
                    if (r.company == "Others"){
                        if(othersVisibility === false){
                            r.deleteThis = true
                        }
                    } return r;
                })

                var arr1 = _.pluck(data, "email");
                var arr2 = _.pluck(userCompanyFromContactsArray, "email");
                var diff = _.difference(arr1, arr2);
                var removeObj2 = _.filter(data, function(obj) { return diff.indexOf(obj.email) >= 0; });

                var cleanArr = data.filter( function( el ) {
                    return removeObj2.indexOf( el ) < 0;
                });

                var filteredArray = _.filter(cleanArr,function(r) {
                    if(!r.deleteThis){
                        return r;
                    }
                });

                var query = {
                    "userId": {$ne: userObjId},
                    "emailId":{$nin:[null,'']}
                }

                if(noOfDays > 0)
                    query["interactionDate"] = {
                        $gte: moment().subtract(noOfDays, "days").toDate(),
                        $lte: new Date()
                    }

                if(_.includes(source, 'outlook')){
                    query["source"] = {$ne:"google"}
                } else {
                    query["source"] = {$ne:"outlook"}
                }

                var aggregationPipeline = getAggregationPipeline(userObjId,query,false);

                if(userId == userSelectedArr[0]){
                    //LIU Start
                    InteractionsCollection.aggregate(aggregationPipeline).exec(function(err, data) {

                        var liuInteractions = data;

                        var userEmailsLIU = []
                            , emailWithCountLIU = {}

                        _.each(liuInteractions, function(d){
                            userEmailsLIU.push(d.email)
                            d.lastInteractedDate = d.lastInteracted
                            d.lastInteracted = moment().diff(moment(d.lastInteracted), "days")
                            emailWithCountLIU[d.email] = d
                        })

                        //Check for account name for the emails fetched from the Interactions

                        myUserCollection.aggregate([
                            {
                                $match: {_id: userObjId}
                            },{
                                $unwind: "$contacts"
                            },{
                                $match: {
                                    "contacts.personEmailId": {$in: userEmailsLIU},
                                    "contacts.account.name": {$nin:[null,usersCompany]}
                                }
                            },{
                                $project: {
                                    _id: 0,
                                    email: "$contacts.personEmailId",
                                    companyName: "$contacts.account.name"
                                }
                            }
                        ]).exec(function(err, userCompanyFromContactsArrayLIU) {

                            userCompanyFromContactsObject = {}
                            _.each(userCompanyFromContactsArrayLIU, function (u) {
                                userCompanyFromContactsObject[u.email] = u.companyName
                            })

                            var liuInteractionsMap = _.map(liuInteractions, function (r) {
                                r.company = userCompanyFromContactsObject[r.email] || r.companyName || "Others"
                                return r;
                            })

                            var arr1Liu = _.pluck(liuInteractionsMap, "email");
                            var arr2Liu = _.pluck(userCompanyFromContactsArrayLIU, "email");
                            var diff = _.difference(arr1Liu, arr2Liu);
                            var removeObjLiu = _.filter(data, function(obj) { return diff.indexOf(obj.email) >= 0; });

                            var cleanArrLiu = liuInteractionsMap.filter( function( el ) {
                                return removeObjLiu.indexOf( el ) < 0;
                            });

                            var allNodes = filteredArray.concat(cleanArrLiu)

                            var result = _.chain(allNodes)
                                .groupBy("company")
                                .pairs()
                                .map(function (currentCompany) {

                                    var sum = 0, lastDate = 1, lastInteractedDate, lastDay = 9999;

                                    _.each(currentCompany[1], function(d){

                                        if(lastDay<d.lastInteracted){
                                        }
                                        else {
                                            lastDay = d.lastInteracted
                                            lastDate = lastDay
                                            if(d.lastInteractedDate)
                                                lastInteractedDate = moment(d.lastInteractedDate).format("DD MMM YYYY")
                                        }
                                        sum += d.count
                                    })

                                    currentCompany.splice(1)
                                    currentCompany.push(sum)
                                    currentCompany.push(lastDate)
                                    currentCompany.push(lastInteractedDate)
                                    return _.object(_.zip(["company", "mailCount", "lastInteractedInDays", "lastInteractedDate"], currentCompany));
                                })
                                .value();
                            callback(result)
                        }) // End of LIU User Collection

                    })//End of UserId
                }
                else {
                    var result = _.chain(filteredArray)
                        .groupBy("company")
                        .pairs()
                        .map(function (currentCompany) {
                            var sum = 0, lastDate = 3650, lastInteractedDate, lastDay = 9999
                            _.each(currentCompany[1], function(d){
                                sum += d.count
                                //lastDate = d.lastInteracted < lastDate ? d.lastInteracted : lastDate

                                if(lastDay<d.lastInteracted){

                                } else {
                                    lastDay = d.lastInteracted
                                    lastDate = lastDay
                                    if(d.lastInteractedDate)
                                        lastInteractedDate = moment(d.lastInteractedDate).format("DD MMM YYYY")
                                }

                            })
                            currentCompany.splice(1)
                            currentCompany.push(sum)
                            currentCompany.push(lastDate)
                            currentCompany.push(lastInteractedDate)
                            return _.object(_.zip(["company", "mailCount", "lastInteractedInDays", "lastInteractedDate"], currentCompany));
                        })
                        .value();
                    callback(result)
                }
            }) //End of myUserCollection Query for userIDList
        })
    }

    //this.getInteractionInitiative = function(userId, companyName, userEmail, callBack){
    this.getInteractionInitiative = function(currentUser, userIds, companyName, noOfDays, callBack){
        companyName = companyName?companyName.toLowerCase():companyName
        
        var userSelectedArr = [];
        userSelectedArr = userIds

        var userIdListSlice = []

        var userIdStr = currentUser.toString();

        if((userIdStr == userSelectedArr[0]) && userIds.length>1){
            userIdListSlice = _.filter(userSelectedArr, function(x) {
                return x != userIdStr
            });
        } else userIdListSlice = userIds

        var allHierarchy = _.filter(userIds, function(x) {
            return x != userIdStr
        });

        if((userIdStr == userSelectedArr[0]) && (userIds.length == 1)) {

            var queryLiu = {"contacts.account.name":{$exists:true,$ne:null,$eq: companyName}}

            myUserCollection.aggregate([
                {
                    $match: {_id: currentUser}
                },{
                    $unwind: "$contacts"
                },{
                    $match: queryLiu
                },{
                    $project: {email: "$contacts.personEmailId", _id: 0}
                }
            ]).exec(function(err, liuData) {

                var liuEmails = _.pluck(liuData, "email")
                    , queryInteractions = {"emailId": {$in: liuEmails}}

                queryInteractions["userId"] = {$ne:currentUser}

                if(noOfDays > 0)
                    queryInteractions["interactionDate"] = {$gte: moment().subtract(noOfDays, "days").toDate(),  $lte: new Date()}

                var aggregationPipelineLiu = getInitiativeAggregationPipeline(currentUser,queryInteractions,false);

                InteractionsCollection.aggregate(aggregationPipelineLiu).exec(function(err, data){
                    callBack(null, data[0])
                })//End LIU interactions callback with data
            })//End for LIU
        }
        else {
            //Get for Others under the hierarchy

            var selectedQuery = {
                $and:[
                    {"contacts.account.name":{$exists:true,$ne:null,$eq: companyName}},
                    {"contacts.account.showToReportingManager": true}
                ]
            }

            myUserCollection.aggregate([

                {
                    $match: {_id: {$in:userIdListSlice}}
                },{
                    $unwind: "$contacts"
                },{
                    $match: selectedQuery
                },{
                    $project: {email: "$contacts.personEmailId", _id: "$_id"}
                }
            ]).exec(function(err, hierarchyData) {
                //Group each user and their respective email Ids

                var userGroup = _
                    .chain(hierarchyData)
                    .groupBy('_id')
                    .map(function(value, key) {
                        return {
                            userId: castToObjectId(key),
                            emailArray: _.pluck(value, 'email')
                        }
                    })
                    .value();

                //build the query to fetch data
                var queryFinal = { "$or": userGroup.map(function(el) {
                    el["ownerId"] = el.userId
                    el["emailId"] = { "$in": el.emailArray };
                    el["userId"] = {$ne:el.userId}

                    if(noOfDays > 0)
                        el["interactionDate"] = {
                            $gte: moment().subtract(noOfDays, "days").toDate(),
                            $lte: new Date()
                        }

                    delete el.emailArray;
                    return el;
                })};

                var aggregationPipelineOthers = getInitiativeAggregationPipeline(userIdListSlice,queryFinal,true);

                //Get interactions for the selected hierarchy
                InteractionsCollection.aggregate(aggregationPipelineOthers).exec(function(err, othersIntdata){
                    //Get for LIU and append to hierarchy if LIU is selected
                    var queryLiu = {"contacts.account.name":{$exists:true,$nin:[null,false],$eq: companyName}}

                    myUserCollection.aggregate([
                        {
                            $match: {_id: currentUser}
                        },{
                            $unwind: "$contacts"
                        },{
                            $match: queryLiu
                        },{
                            $project: {email: "$contacts.personEmailId", _id: 0}
                        }
                    ]).exec(function(err, liuData) {

                        var liuEmails = _.pluck(liuData, "email")
                            , queryInteractions = {"emailId": {$in: liuEmails}}
                        queryInteractions["userId"] = {$ne:currentUser}
                        if(noOfDays > 0)
                            queryInteractions["interactionDate"] = {$gte: moment().subtract(noOfDays, "days").toDate(),  $lte: new Date()}

                        var aggregationPipelineLiu = getInitiativeAggregationPipeline(currentUser,queryInteractions,false);

                        InteractionsCollection.aggregate(aggregationPipelineLiu).exec(function(err, liuData){

                            if((userIdStr == userSelectedArr[0]) && (userIds.length>1)){

                                var tempInitOthers2 = 0
                                var tempInitMe2 = 0
                                var tempInitOthers1 = 0
                                var tempInitMe1 = 0

                                if(liuData && liuData.length>0){
                                    tempInitOthers2 = liuData[0].initiatedByOthers
                                    tempInitMe2 = liuData[0].initiatedByMe
                                }

                                if(othersIntdata && othersIntdata.length>0){
                                    tempInitOthers1 = othersIntdata[0].initiatedByOthers
                                    tempInitMe1 = othersIntdata[0].initiatedByMe
                                }

                                var finalArr = [];
                                var tempObj = {}
                                tempObj._id = null;
                                tempObj.initiatedByOthers = tempInitOthers1 + tempInitOthers2
                                tempObj.initiatedByMe = tempInitMe1 + tempInitMe2

                                finalArr.push(tempObj)
                                callBack(null, finalArr[0])

                            } else {

                                if(!othersIntdata) {
                                    var noOthersIntdata = {}
                                    noOthersIntdata._id = null;
                                    noOthersIntdata.initiatedByOthers = 0
                                    noOthersIntdata.initiatedByMe = 0
                                    callBack(null, noOthersIntdata[0])
                                } else {
                                    callBack(null, othersIntdata[0])
                                }
                            }

                        })//End LIU
                    })
                })//End fetch interactions for others
            })//End Selected Hierarchy
        }//End Else
    }//End Initiations Function

    //Get top Five locations interacted based on the visibility settings of the hierarchy
    this.getInteractionsCountByLocation = function(currentUser, userIdList,dateMin,dateMax,usersCompany,source,callback){

        var userSelectedArr = [];
        userSelectedArr = userIdList

        var userIdListSlice = []

        var userIdStr = currentUser.toString();

        if((userIdStr == userSelectedArr[0]) && userIdList.length>1){
            userIdListSlice = _.filter(userSelectedArr, function(x) {
                return x != userIdStr
            });
        } else userIdListSlice = userIdList

        if((userIdStr == userSelectedArr[0]) && (userIdList.length == 1)) {

            myUserCollection.aggregate([
                {
                    $match: {_id: currentUser}
                }
                , {
                    $unwind: "$contacts"
                }
                , {
                    $match: {
                        "contacts.account": {$exists: true},
                        "contacts.account.name": {$nin:[null,usersCompany]}
                    }
                }
                , {
                    $project: {
                        "_id": 0,
                        "email": "$contacts.personEmailId"
                    }
                }


            ]).exec(function (usersError, companyUsers) {

                var companyUsersEmail = _.pluck(companyUsers, "email")

                var query = {
                    "emailId": {$in: companyUsersEmail},
                    "location": {$nin: [null, '']},
                    "userId": {$ne: currentUser}
                };

                if (dateMin != dateMax)
                    query["interactionDate"] = {$gte: new Date(dateMin),
                        $lte: new Date()}

                if(_.includes(source, 'outlook')){
                    query["source"] = {$ne:"google"}
                } else {
                    query["source"] = {$ne:"outlook"}
                }

                var locationAggregatePipeline = getLocationAggregationPipeline(currentUser,query,false);

                InteractionsCollection.aggregate(locationAggregatePipeline).exec(function (errorLocation, resultLocation) {

                    if (errorLocation) {
                        logger.info("Error in getInteractionsCountByLocation():interactionsManagement errorLocation ", errorLocation)
                    }

                    var data = {};

                    if (checkRequired(resultLocation) && resultLocation.length > 0) {
                        data.interactionsByLocation = resultLocation;
                    }
                    else data.interactionsByLocation = [];

                    callback(data)
                })
            })
        }// End LIU If
        else {
            myUserCollection.aggregate([
                {
                    $match: {_id: currentUser}
                }
                , {
                    $unwind: "$contacts"
                }
                , {
                    $match: {
                        "contacts.account": {$exists: true},
                        "contacts.account.name": {$nin:[null,usersCompany]}
                    }
                }
                , {
                    $project: {
                        "_id": 0,
                        "email": "$contacts.personEmailId"
                    }
                }

            ]).exec(function (usersError, companyUsers) {

                var liuEmailArray = _.pluck(companyUsers, "email")

                //Get Other users contacts except LIU
                myUserCollection.aggregate([
                    {
                        $match: {_id: {$in: userIdListSlice}}
                    }
                    ,{
                        $unwind: "$contacts"
                    }
                    ,{
                        $match: {
                            "contacts.account.showToReportingManager": true,
                            "contacts.account.name": {$nin:[null,usersCompany]}
                        }
                    }
                    ,{
                        $project: {
                            "_id": '$_id',
                            "email": "$contacts.personEmailId"
                        }
                    }

                ]).exec(function(usersError,otherCompanyUsers){

                    var query1 = {
                        "location": {$nin: [null, '']},
                        "userId": {$ne: currentUser},
                        "action": {$in: ['sender', 'receiver']},
                        "emailId":{$in:liuEmailArray}
                    };

                    if (dateMin != dateMax)
                        query1["interactionDate"] = {$gte: new Date(dateMin),
                            $lte: new Date()}

                    var locationAggregatePipelineSelf = getLocationAggregationPipeline(currentUser,query1,false);

                    InteractionsCollection.aggregate(locationAggregatePipelineSelf).exec(function (errorLocation, resultLocationLiu) {

                        //Group each user and their respective email Ids
                        var userGroup = _
                            .chain(otherCompanyUsers)
                            .groupBy('_id')
                            .map(function(value, key) {
                                return {
                                    userId: castToObjectId(key),
                                    emailArray: _.pluck(value, 'email')
                                }
                            })
                            .value();

                        //build the query to fetch data
                        var queryFinal = { "$or": userGroup.map(function(el) {
                            el["ownerId"] = el.userId;
                            el["userId"] = { "$ne": el.userId };
                            el["emailId"] = { "$in": el.emailArray };
                            el["location"] = {"$nin": [null, '']};

                            if(dateMin != dateMax)
                                el["interactionDate"] = {$gte: new Date(dateMin),
                                    $lte: new Date()}

                            delete el.emailArray;
                            return el;
                        })};

                        //Now get Other's Interactions

                        var locationAggregatePipelineOthers = getLocationAggregationPipeline(userIdListSlice,queryFinal,true);

                        InteractionsCollection.aggregate(locationAggregatePipelineOthers).exec(function (errorLocation, resultLocationOthers) {

                            if((userIdStr == userSelectedArr[0]) && (userIdList.length>1)){

                                var totalCountOthers = 0
                                var typeCountsOthers = []

                                var totalCountLiu = resultLocationLiu[0].totalCount

                                if(resultLocationOthers && resultLocationOthers.length>0) {
                                    typeCountsOthers = resultLocationOthers[0].typeCounts
                                    totalCountOthers = resultLocationOthers[0].totalCount
                                }

                                var typeCountsLiu =  resultLocationLiu[0].typeCounts
                                var typeCountsAll = typeCountsOthers.concat(typeCountsLiu)

                                var groupTypeCountArr = _.map(_.groupBy(typeCountsAll, "_id"), function(vals, _id) {
                                    return _.reduce(vals, function(m, o) {
                                        for (var p in o)
                                            if (p != "_id")
                                                m[p] = (m[p]||0)+o[p];
                                        return m;
                                    }, {_id: _id});
                                });

                                var sortTypeCount = _.sortBy(groupTypeCountArr, function(o) { return -o.count; })

                                var topFiveLocations = sortTypeCount.splice(0,5)

                                var newObj = {};
                                newObj._id = null;
                                newObj.totalCount = totalCountLiu + totalCountOthers;
                                newObj.typeCounts = topFiveLocations

                                var newResultLocations = [];
                                newResultLocations.push(newObj)

                                if (errorLocation) {
                                    logger.info("Error in getInteractionsCountByLocation():interactionsManagement errorLocation ", errorLocation)
                                }

                                var data = {};

                                if (checkRequired(newResultLocations) && newResultLocations.length > 0) {
                                    data.interactionsByLocation = newResultLocations;
                                }
                                else data.interactionsByLocation = [];

                                callback(data)

                            }
                            else {

                                if (errorLocation) {
                                    logger.info("Error in getInteractionsCountByLocation():interactionsManagement errorLocation ", errorLocation)
                                }

                                var data = {};

                                if (checkRequired(resultLocationOthers) && resultLocationOthers.length > 0) {
                                    data.interactionsByLocation = resultLocationOthers;
                                }
                                else data.interactionsByLocation = [];

                                callback(data)
                            }
                        })//Other's interactions end
                    })
                })// End Other user's contacts
            })//End My User Collection
        }
    };

    this.getInteractionsCountByTypeForCompany = function(currentUser, allUsers,dateMin,dateMax,companyName, callback){

        companyName = companyName?companyName.toLowerCase():companyName;

        var query = {"contacts.account.name":{$exists:true,$ne:null,$eq: companyName}
            , $or: [{"contacts.account.showToReportingManager": true},{ "_id": currentUser}]};

        //var query = {$and: [{"contacts.account.showToReportingManager": true},{ "_id": currentUser}]};

        var userSelectedArr = [];
        userSelectedArr = allUsers

        var userIdListSlice = []

        var userIdStr = currentUser.toString();

        if((userIdStr == userSelectedArr[0]) && allUsers.length>1){
            userIdListSlice = _.filter(userSelectedArr, function(x) {
                return x != userIdStr
            });
        } else userIdListSlice = allUsers

        if((userIdStr == userSelectedArr[0]) && (allUsers.length == 1)){
            myUserCollection.aggregate([
                {
                    $match: {_id: currentUser}
                }
                ,{
                    $unwind: "$contacts"
                }
                ,{
                    $match: {"contacts.account.name":{$exists:true,$ne:null,$eq: companyName}}
                }
                ,{
                    $project: {
                        "_id": 0,
                        "email": "$contacts.personEmailId"
                    }
                }

            ]).exec(function(usersError, companyUsers) {
                var companyUsersEmail = _.pluck(companyUsers, "email")

                var query = {
                    "emailId": {$in: companyUsersEmail},
                    "userId":{$ne:currentUser}
                };

                if(dateMin != dateMax)
                    query["interactionDate"] = {
                        $gte: new Date(dateMin),
                        $lte: new Date()
                    }

                var aggregationPipelineLiu = getInteractionByTypeAggregationPipeline(currentUser,query,false);

                InteractionsCollection.aggregate(aggregationPipelineLiu).exec(function(errorInteractionType,resultInteractionType){

                    if(errorInteractionType){
                        logger.info("Error in getInteractionsCountBySocialType():interactionsManagement errorInteractionType --1",errorInteractionType)
                    }
                    var data = {};
                    if(checkRequired(resultInteractionType) && resultInteractionType.length > 0){
                        data.interactionsByType = resultInteractionType;
                    }
                    else data.interactionsByType = [];

                    callback(data)
                })

            })//End of My User Collection
        } else {

            myUserCollection.aggregate([
                {
                    $match: {_id: currentUser}
                }
                ,{
                    $unwind: "$contacts"
                }
                ,{
                    $match: {"contacts.account.name":{$exists:true,$ne:null,$eq: companyName}}
                }
                ,{
                    $project: {
                        "_id": 0,
                        "email": "$contacts.personEmailId"
                    }
                }

            ]).exec(function(usersError, userLiuEmails) {

                var selectedQuery = {
                    $and:[
                        {"contacts.account.name":{$exists:true,$ne:null,$eq: companyName}},
                        {"contacts.account.showToReportingManager": true}
                    ]
                }

                myUserCollection.aggregate([
                    {
                        $match: {_id: {$in: userIdListSlice}}
                    }
                    ,{
                        $unwind: "$contacts"
                    }
                    ,{
                        $match: selectedQuery
                    }
                    ,{
                        $project: {
                            "_id": '$_id',
                            "email": "$contacts.personEmailId"
                        }
                    }

                ]).exec(function(usersError,companyUsers){

                    var liuEmail = _.pluck(userLiuEmails, "email")

                    var queryLiu = {
                        "emailId": {$in: liuEmail},
                        "userId":{$ne:currentUser}
                    };

                    if(dateMin != dateMax)
                        queryLiu["interactionDate"] = {$gte: new Date(dateMin),
                            $lte: new Date()}

                    var aggregationPipelineLiu = getInteractionByTypeAggregationPipeline(currentUser,queryLiu,false);

                    InteractionsCollection.aggregate(aggregationPipelineLiu).exec(function(errorInteractionType,resultInteractionTypeLiu){

                        //Group each user and their respective email Ids
                        var userGroup = _
                            .chain(companyUsers)
                            .groupBy('_id')
                            .map(function(value, key) {
                                return {
                                    userId: castToObjectId(key),
                                    emailArray: _.pluck(value, 'email')
                                }
                            })
                            .value();

                        //build the query to fetch data
                        var queryFinal = { "$or": userGroup.map(function(el) {
                            el["ownerId"] = el.userId;
                            el["userId"] = { "$ne": el.userId };
                            el["emailId"] = { "$in": el.emailArray };

                            if(dateMin != dateMax)
                                el["interactionDate"] = {$gte: new Date(dateMin)}

                            delete el.emailArray;
                            return el;
                        })};

                        var aggregationPipelineOthers = getInteractionByTypeAggregationPipeline(userIdListSlice,queryFinal,true);

                        InteractionsCollection.aggregate(aggregationPipelineOthers).exec(function(errorInteractionType,resultInteractionType){

                            if(errorInteractionType){
                                logger.info("Error in getInteractionsCountBySocialType():interactionsManagement errorInteractionType --2 ",errorInteractionType)
                            }

                            if((userIdStr == userSelectedArr[0]) && (allUsers.length>1)){

                                var newTotalCount1 = 0
                                var newTotalCount2 = 0
                                var newMaxCount1 = 0
                                var newMaxCount2 = 0
                                var newTypeCounts1 = []
                                var newTypeCounts2 = []

                                if(resultInteractionTypeLiu && resultInteractionTypeLiu.length>0){
                                    newTotalCount1 = resultInteractionTypeLiu[0].totalCount
                                    newMaxCount1 = resultInteractionTypeLiu[0].maxCount
                                    newTypeCounts1 = resultInteractionTypeLiu[0].typeCounts
                                }

                                if (resultInteractionType && resultInteractionType.length>0){
                                    newTotalCount2 = resultInteractionType[0].totalCount
                                    newMaxCount2 = resultInteractionType[0].maxCount
                                    newTypeCounts2 = resultInteractionType[0].typeCounts
                                }

                                var totalCount = newTotalCount1 + newTotalCount2
                                var totalMaxCount = newMaxCount1 + newMaxCount2
                                var totalTypeCount = newTypeCounts1.concat(newTypeCounts2)

                                var groupTypeCountArr = _.map(_.groupBy(totalTypeCount, "_id"), function(vals, _id) {
                                    return _.reduce(vals, function(m, o) {
                                        for (var p in o)
                                            if (p != "_id")
                                                m[p] = (m[p]||0)+o[p];
                                        return m;
                                    }, {_id: _id});
                                });

                                var newObj = {};
                                newObj._id = null;
                                newObj.totalCount = totalCount;
                                newObj.maxCount = totalMaxCount;
                                newObj.typeCounts = groupTypeCountArr

                                var newResultInteractions = [];
                                newResultInteractions.push(newObj)

                                var data = {};

                                if(checkRequired(newResultInteractions) && newResultInteractions.length > 0){
                                    data.interactionsByType = newResultInteractions;
                                }
                                else data.interactionsByType = [];

                                callback(data)
                            } else {

                                var data = {};

                                if(checkRequired(resultInteractionType) && resultInteractionType.length > 0){
                                    data.interactionsByType = resultInteractionType;
                                }
                                else data.interactionsByType = [];

                                callback(data)
                            }
                        })//End Non LIU users

                    })//End LIU interactions
                })
            })//End of My User Collection
        }
    };

    //Get Total Interactions Count by Type for Company Landing
    this.getInteractionsCountByType = function(currentUser, userIdList,dateMin,dateMax,usersCompany,source,callback){

        var userSelectedArr = [];
        userSelectedArr = userIdList

        var userIdListSlice = []

        var userIdStr = currentUser.toString();

        if((userIdStr == userSelectedArr[0]) && userIdList.length>1){
            userIdListSlice = _.filter(userSelectedArr, function(x) {
                return x != userIdStr
            });
        } else userIdListSlice = userIdList

        if((userIdStr == userSelectedArr[0]) && (userIdList.length == 1)){
            myUserCollection.aggregate([
                {
                    $match: {_id: currentUser}
                }
                ,{
                    $unwind: "$contacts"
                }
                ,{
                    $match: {
                        "contacts.account": {$exists: true},
                        "contacts.account.name": {$nin:[null,usersCompany]}
                    }
                }
                ,{
                    $project: {
                        "_id": 0,
                        "email": "$contacts.personEmailId"
                    }
                }

            ]).exec(function(usersError, companyUsers) {
                var companyUsersEmail = _.pluck(companyUsers, "email")

                var query = {
                    "emailId": {$in: companyUsersEmail},
                    "userId": {$ne: currentUser}
                };

                if(dateMin != dateMax)
                    query["interactionDate"] = {$gte: new Date(dateMin),
                        $lte: new Date()}

                if(_.includes(source, 'outlook')){
                    query["source"] = {$ne:"google"}
                } else {
                    query["source"] = {$ne:"outlook"}
                }

                var aggregationPipelineLiu = getInteractionByTypeAggregationPipeline(currentUser,query,false);

                InteractionsCollection.aggregate(aggregationPipelineLiu).exec(function(errorInteractionType,resultInteractionType){

                    if(errorInteractionType){
                        logger.info("Error in getInteractionsCountBySocialType():interactionsManagement errorInteractionType --4",errorInteractionType)
                    }
                    var data = {};
                    if(checkRequired(resultInteractionType) && resultInteractionType.length > 0){
                        data.interactionsByType = resultInteractionType;
                    }
                    else data.interactionsByType = [];

                    callback(data)
                })

            })//End of My User Collection
        } else {

            myUserCollection.aggregate([
                {
                    $match: {_id: currentUser}
                }
                ,{
                    $unwind: "$contacts"
                }
                ,{
                    $match: {
                        "contacts.account": {$exists: true},
                        "contacts.account.name": {$nin:[null,usersCompany]}
                    }
                }
                ,{
                    $project: {
                        "_id": 0,
                        "email": "$contacts.personEmailId"
                    }
                }

            ]).exec(function(usersError, userLiuEmails) {

                myUserCollection.aggregate([
                    {
                        $match: {_id: {$in: userIdListSlice}}
                    }
                    ,{
                        $unwind: "$contacts"
                    }
                    ,{
                        $match: {
                            "contacts.account.showToReportingManager": true,
                            "contacts.account.name": {$nin:[null,usersCompany]}
                        }
                    }
                    ,{
                        $project: {
                            "_id": '$_id',
                            "email": "$contacts.personEmailId"
                        }
                    }

                ]).exec(function(usersError,companyUsers){

                    var liuEmail = _.pluck(userLiuEmails, "email")

                    var queryLiu = {
                        "emailId": {$in: liuEmail},
                        "userId": {$ne: currentUser}
                    };

                    if(dateMin != dateMax)
                        queryLiu["interactionDate"] = {$gte: new Date(dateMin),
                            $lte: new Date()}

                    var aggregationPipelineLiu = getInteractionByTypeAggregationPipeline(currentUser,queryLiu,false);

                    InteractionsCollection.aggregate(aggregationPipelineLiu).exec(function(errorInteractionType,resultInteractionTypeLiu){

                        //Group each user and their respective email Ids
                        var userGroup = _
                            .chain(companyUsers)
                            .groupBy('_id')
                            .map(function(value, key) {
                                return {
                                    userId: castToObjectId(key),
                                    emailArray: _.pluck(value, 'email')
                                }
                            })
                            .value();

                        //build the query to fetch data
                        var queryFinal = { "$or": userGroup.map(function(el) {
                            el["ownerId"] = el.userId;
                            el["userId"] = { "$ne": el.userId };
                            el["emailId"] = { "$in": el.emailArray };

                            if(dateMin != dateMax)
                                el["interactionDate"] = {$gte: new Date(dateMin)}

                            delete el.emailArray;
                            return el;
                        })};

                        var aggregationPipelineOthers = getInteractionByTypeAggregationPipeline(userIdListSlice,queryFinal,true);

                        InteractionsCollection.aggregate(aggregationPipelineOthers).exec(function(errorInteractionType,resultInteractionType){

                            if((userIdStr == userSelectedArr[0]) && (userIdList.length>1)){

                                var newTotalCount2 = 0;
                                var newMaxCount2 = 0;
                                var newTypeCounts2 = [];

                                if(resultInteractionType && resultInteractionType.length>0){
                                    newTotalCount2 = resultInteractionType[0].totalCount
                                    newMaxCount2 = resultInteractionType[0].maxCount
                                    newTypeCounts2 = resultInteractionType[0].typeCounts
                                }

                                var newTotalCount1 = resultInteractionTypeLiu[0].totalCount
                                var newMaxCount1 = resultInteractionTypeLiu[0].maxCount
                                var newTypeCounts1 = resultInteractionTypeLiu[0].typeCounts

                                var totalCount = newTotalCount1 + newTotalCount2
                                var totalMaxCount = newMaxCount1 + newMaxCount2
                                var totalTypeCount = newTypeCounts1.concat(newTypeCounts2)

                                var groupTypeCountArr = _.map(_.groupBy(totalTypeCount, "_id"), function(vals, _id) {
                                    return _.reduce(vals, function(m, o) {
                                        for (var p in o)
                                            if (p != "_id")
                                                m[p] = (m[p]||0)+o[p];
                                        return m;
                                    }, {_id: _id});
                                });

                                var newObj = {};
                                newObj._id = null;
                                newObj.totalCount = totalCount;
                                newObj.maxCount = totalMaxCount;
                                newObj.typeCounts = groupTypeCountArr

                                var newResultInteractions = [];
                                newResultInteractions.push(newObj)

                                if(errorInteractionType){
                                    logger.info("Error in getInteractionsCountBySocialType():interactionsManagement errorInteractionType --5",errorInteractionType)
                                }

                                var data = {};

                                if(checkRequired(newResultInteractions) && newResultInteractions.length > 0){
                                    data.interactionsByType = newResultInteractions;
                                }
                                else data.interactionsByType = [];

                                callback(data)
                            } else {

                                if(resultInteractionType){
                                    logger.info("Error in getInteractionsCountBySocialType():interactionsManagement errorInteractionType --6",errorInteractionType)
                                }

                                var data = {};

                                if(checkRequired(resultInteractionType) && resultInteractionType.length > 0){
                                    data.interactionsByType = resultInteractionType;
                                }
                                else data.interactionsByType = [];

                                callback(data)
                            }
                        })//End Non LIU users

                    })//End LIU interactions
                })
            })//End of My User Collection
        }
    };

    //Fetch monthly interactions with company wide interactions for company select page
    this.fetchInteractionsCountByMonth = function(currentUser, selectedUsers, allUsers, companyName, startDate, endDate, callBack){
        companyName = companyName?companyName.toLowerCase():companyName
        var userSelectedArr = [];
        userSelectedArr = selectedUsers

        var userIdListSlice = []

        var userIdStr = currentUser.toString();

        if((userIdStr == userSelectedArr[0]) && selectedUsers.length>1){
            userIdListSlice = _.filter(userSelectedArr, function(x) {
                return x != userIdStr
            });
        } else userIdListSlice = selectedUsers

        var allHierarchy = _.filter(allUsers, function(x) {
            return x != userIdStr
        });

        //For LIU

        if((userIdStr == userSelectedArr[0]) && (selectedUsers.length == 1)) {

            var query = {}
                , userQuery = {"contacts.account.name":{$exists:true,$ne:null,$eq: companyName}};

            if(!startDate)
                query = {
                    "interactionDate": {$lte:new Date(endDate)}
                }
            else
                query = {
                    "interactionDate": {$gte: new Date(startDate),$lte:new Date(endDate)}
                }

            var queryCompany = {
                $and:[
                    {"contacts.account.name":{$exists:true,$ne:null,$eq: companyName}},
                    // {"contacts.account.showToReportingManager": true}
                ]
            }

            myUserCollection.aggregate([
                {
                    $match: {_id: {$in: allHierarchy}}
                },{
                    $unwind: "$contacts"
                },{
                    $match: queryCompany
                },{
                    $project: {email: "$contacts.personEmailId", _id: "$_id"}
                }
            ]).exec(function(err, companyData) {

                //Group each user and their respective email Ids



                var userGroup = _
                    .chain(companyData)
                    .groupBy('_id')
                    .map(function(value, key) {
                        return {
                            userId: castToObjectId(key),
                            emailArray: _.pluck(value, 'email')
                        }
                    })
                    .value();

                //build the query to fetch data
                var queryFinal = { "$or": userGroup.map(function(el) {
                    el["ownerId"] = el.userId
                    el["emailId"] = { "$in": el.emailArray };
                    el["userId"] = {$ne:el.userId}

                    if(!startDate)
                        el["interactionDate"] = {$lte:new Date(endDate)}
                    else {
                        el["interactionDate"] = {$gte: new Date(startDate),$lte:new Date(endDate)}
                    }
                    delete el.emailArray;
                    return el;
                })};

                var aggregationPipelineOthers = getMonthlyAggregationPipeline(allHierarchy,queryFinal,true)

                InteractionsCollection.aggregate(aggregationPipelineOthers).exec(function (err, companyWideData) {

                    //For only LIU
                    myUserCollection.aggregate([
                        {
                            $match: {_id: currentUser}
                        },{
                            $unwind: "$contacts"
                        },{
                            $match: userQuery
                        },{
                            $project: {email: "$contacts.personEmailId", _id: 0}
                        }
                    ]).exec(function(err, data){
                        var companyEmails = _.pluck(data,"email")
                        query["emailId"] = {$in: companyEmails}
                        query["userId"] = {$ne:currentUser}

                        var aggregationPipelineLiu = getMonthlyAggregationPipeline(currentUser,query,false);

                        InteractionsCollection.aggregate(aggregationPipelineLiu).exec(function(err, data){

                            if(err){
                                logger.info()
                            }

                            else{
                                var finalResult
                                if(companyWideData && data) {
                                    finalResult = function (array) {
                                        var o = {}, r = [];
                                        array.forEach(function (a) {
                                            var k = a._id.year + '|' + a._id.month;
                                            if (!(k in o)) {
                                                o[k] = {
                                                    _id: a._id,
                                                    count: 0,
                                                    sampleDate: a.sampleDate,
                                                    interactions: [],
                                                };
                                                r.push(o[k]);
                                            }
                                            o[k].count += a.count;
                                            o[k].interactions = o[k].interactions.concat(a.interactions);
                                        });
                                        return r;
                                    }(data.concat(companyWideData));
                                } else {
                                    finalResult = data
                                }

                                _.map(finalResult, function(interactionsByMonth){
                                    var interactionsBySelf = 0
                                        , interactionsByTeam = 0

                                    _.each(interactionsByMonth.interactions, function(interaction){

                                        if(interaction.userId){
                                            if(_.contains(userIdStr, interaction.userId.toString()))
                                                interactionsBySelf++
                                            interactionsByTeam ++
                                        }
                                    })
                                    interactionsByMonth.interactionsBySelf = interactionsBySelf
                                    interactionsByMonth.interactionsByTeam = interactionsByTeam
                                    return interactionsByMonth
                                })

                                if(data.length<1 && companyWideData && companyWideData.length>0) {
                                    companyWideData[0].interactionsBySelf=0,
                                        companyWideData[0].interactionsByTeam = companyWideData[0].count

                                    callBack(companyWideData)
                                } else {
                                    //Sometimes the dates get jumbled while concatenating. Sort them to resolve the bugs. LM 9M16,NP
                                    var sortResult = _.sortBy(finalResult, function(o) { return o.sampleDate; })

                                    callBack(sortResult)
                                }
                            }
                        })
                    })// End for LIU
                })
            })//End company Wide + LIU
        }
        else {
            //For LIU + Company Wide

            var intQueryLiu = {}
                , liuQuery = {"contacts.account.name":{$exists:true,$ne:null,$eq: companyName}};

            if(!startDate)
                intQueryLiu = {
                    "interactionDate": {$lte:new Date(endDate)}
                }
            else
                intQueryLiu = {
                    "interactionDate": {$gte: new Date(startDate),$lte:new Date(endDate)}
                }

            myUserCollection.aggregate([
                {
                    $match: {_id: currentUser}
                },{
                    $unwind: "$contacts"
                },{
                    $match: liuQuery
                },{
                    $project: {email: "$contacts.personEmailId", _id: 0}
                }
            ]).exec(function(err, companyData) {

                var liuEmails = _.pluck(companyData, "email")
                intQueryLiu["emailId"] = {$in: liuEmails}
                intQueryLiu["userId"] = {$ne:currentUser}

                var aggregationPipelineLiu = getMonthlyAggregationPipeline(currentUser,intQueryLiu,false);

                InteractionsCollection.aggregate(aggregationPipelineLiu).exec(function (err, liuData) {

                    var selectedQuery = {
                        $and:[
                            {"contacts.account.name":{$exists:true,$ne:null,$eq: companyName}},
                            {"contacts.account.showToReportingManager": true}
                        ]
                    }

                    //Get selected hierarchy emails
                    myUserCollection.aggregate([
                        {
                            $match: {_id: {$in: userIdListSlice}}
                        },{
                            $unwind: "$contacts"
                        },{
                            $match: selectedQuery
                        },{
                            $project: {email: "$contacts.personEmailId", _id: "$_id"}
                        }
                    ]).exec(function(err, selectedDataEmails) {

                        //Group each user and their respective email Ids
                        var userGroup = _
                            .chain(selectedDataEmails)
                            .groupBy('_id')
                            .map(function(value, key) {
                                return {
                                    userId: castToObjectId(key),
                                    emailArray: _.pluck(value, 'email')
                                }
                            })
                            .value();

                        //build the query to fetch data
                        var queryFinal = { "$or": userGroup.map(function(el) {
                            el["ownerId"] = el.userId
                            el["userId"] = {$ne:el.userId}
                            el["emailId"] = { "$in": el.emailArray };

                            if(!startDate)
                                el["interactionDate"] = {$lte:new Date(endDate)}
                            else {
                                el["interactionDate"] = {$gte: new Date(startDate),$lte:new Date(endDate)}
                            }
                            delete el.emailArray;
                            return el;
                        })};

                        var aggregationPipelineOthers = getMonthlyAggregationPipeline(userIdListSlice,queryFinal,true);

                        //Get interactions for the selected hierarchy
                        InteractionsCollection.aggregate(aggregationPipelineOthers).exec(function (err,selectedData){

                            //Get interactions for everyone under the hierarchy except LIU
                            
                            myUserCollection.aggregate([
                                {
                                    $match: {_id: {$in: allHierarchy}}
                                },{
                                    $unwind: "$contacts"
                                },{
                                    $match: selectedQuery
                                },{
                                    $project: {email: "$contacts.personEmailId", _id: "$_id"}
                                }
                            ]).exec(function(err, hierarchyData) {

                                //Group each user and their respective email Ids
                                var hierarchyGroup = _
                                    .chain(hierarchyData)
                                    .groupBy('_id')
                                    .map(function(value, key) {
                                        return {
                                            userId: castToObjectId(key),
                                            emailArray: _.pluck(value, 'email')
                                        }
                                    })
                                    .value();

                                //build the query to fetch data
                                var hierarchyQueryFinal = { "$or": hierarchyGroup.map(function(el) {
                                    el["ownerId"] = el.userId
                                    el["emailId"] = { "$in": el.emailArray };
                                    el["userId"] = {$ne:el.userId}

                                    if(!startDate)
                                        el["interactionDate"] = {$lte:new Date(endDate)}
                                    else {
                                        el["interactionDate"] = {$gte: new Date(startDate),$lte:new Date(endDate)}
                                    }
                                    delete el.emailArray;
                                    return el;
                                })};

                                var aggregationPipelineAllHierarchy = getMonthlyAggregationPipeline(allHierarchy,hierarchyQueryFinal,true);

                                //Get interactions for the whole hierarchy
                                InteractionsCollection.aggregate(aggregationPipelineAllHierarchy).exec(function (err,hierarchyFinalData){

                                    var finalResult
                                    //hierarchy has interactions
                                    if(hierarchyFinalData && liuData){

                                        var finalResult = function (array) {
                                            var o = {}, r = [];
                                            array.forEach(function (a) {
                                                var k = a._id.year + '|' + a._id.month;
                                                if (!(k in o)) {
                                                    o[k] = {
                                                        _id: a._id,
                                                        count: 0,
                                                        sampleDate: a.sampleDate,
                                                        interactions: [],
                                                    };
                                                    r.push(o[k]);
                                                }
                                                o[k].count += a.count;
                                                o[k].interactions = o[k].interactions.concat(a.interactions);
                                            });
                                            return r;
                                        }(liuData.concat(hierarchyFinalData));

                                        if((userIdStr == userSelectedArr[0]) && (selectedUsers.length > 0)){
                                            var arrUsers = [];
                                            arrUsers.push(currentUser);
                                            arrUsers = userIdListSlice.concat(arrUsers)

                                            var selectedUserList = _.map(arrUsers, function(selectedUser){ return selectedUser.toString()})
                                        } else {
                                            var selectedUserList = _.map(userIdListSlice, function(selectedUser){ return selectedUser.toString()})
                                        }

                                        _.map(finalResult, function(interactionsByMonth){
                                            var interactionsBySelf = 0
                                                , interactionsByTeam = 0

                                            _.each(interactionsByMonth.interactions, function(interaction){
                                                if(_.contains(selectedUserList, interaction.userId.toString()))
                                                    interactionsBySelf++
                                                interactionsByTeam ++
                                            })
                                            interactionsByMonth.interactionsBySelf = interactionsBySelf
                                            interactionsByMonth.interactionsByTeam = interactionsByTeam
                                            return interactionsByMonth
                                        })

                                    } else if(!selectedData && !hierarchyFinalData){

                                        var finalResult = liuData

                                        var selectedUserList = _.map(selectedUsers, function(selectedUser){ return selectedUser.toString()})
                                        _.map(finalResult, function(interactionsByMonth){
                                            var interactionsBySelf = 0
                                                , interactionsByTeam = 0

                                            _.each(interactionsByMonth.interactions, function(interaction){
                                                if(_.contains(selectedUserList, interaction.userId.toString()))
                                                    interactionsBySelf++
                                                interactionsByTeam ++
                                            })
                                            interactionsByMonth.interactionsBySelf = interactionsBySelf
                                            interactionsByMonth.interactionsByTeam = interactionsByTeam
                                            return interactionsByMonth
                                        })


                                    } else {

                                        finalResult = liuData
                                        _.map(finalResult, function(interactionsByMonth){
                                            var interactionsBySelf = 0
                                                , interactionsByTeam = 0

                                            _.each(interactionsByMonth.interactions, function(interaction){
                                                if(_.contains(userIdStr, interaction.userId.toString()))
                                                    interactionsBySelf++
                                                interactionsByTeam ++
                                            })
                                            interactionsByMonth.interactionsBySelf = interactionsBySelf
                                            interactionsByMonth.interactionsByTeam = interactionsByTeam
                                            return interactionsByMonth
                                        })

                                    }

                                    //Sometimes the dates get jumbled while concatenating. Sort them to resolve the bugs. LM 9M16,NP
                                    var sortResult = _.sortBy(finalResult, function(o) { return o.sampleDate; })

                                    callBack(sortResult)
                                })
                            })//All contacts for everyone under hierarchy
                        })//End selected hierarchy Interactions fetch
                    })//END Get selected hierarchy emails
                })//Interactions for LIU End
            })
        }//End Else
    }//End All Fetch

    this.getInteractionPercentage = function(userIds,currentUser, companyName, pastDuration, currentDuration, callBack){
        myUserCollection.aggregate([
            {
                $match: {_id: {$in: userIds}}
            },{
                $unwind: "$contacts"
            },{
                $match: {"contacts.account.name":{$exists:true,$ne:null, $eq: companyName}}
            },{
                $project: {email: "$contacts.personEmailId", _id: 0}
            }
        ]).exec(function(err, data){
            var companyEmails = _.pluck(data,"email")
            interaction.aggregate([
                {
                    $match: {userId : {$in: userIds}}
                },{
                    $unwind: "$interactions"
                },{
                    $match: { "interactions.emailId": {$in: companyEmails}}
                }
                ,{
                    $project: {
                        myInteractionPast: {$cond: [{$and: [{$gte: ["$interactions.interactionDate", pastDuration.startDate]},
                            {$lte: ["$interactions.interactionDate", pastDuration.endDate]}
                            ,{$eq: ["$userId", currentUser]}]},
                            1,
                            0
                        ]},
                        myInteractionPresent: {$cond: [{$and: [{$gte: ["$interactions.interactionDate", currentDuration.startDate]},
                            {$lte: ["$interactions.interactionDate", currentDuration.endDate]}
                            ,{$eq: ["$userId", currentUser]}]},
                            1,
                            0
                        ]},
                        companyInteractionPast: {$cond: [{$and: [{$gte: ["$interactions.interactionDate", pastDuration.startDate]},
                            {$lte: ["$interactions.interactionDate", pastDuration.endDate]}
                            ,{$ne: ["$userId", currentUser]}]},
                            1,
                            0
                        ]},
                        companyInteractionPresent: {$cond: [{$and: [{$gte: ["$interactions.interactionDate", currentDuration.startDate]},
                            {$lte: ["$interactions.interactionDate", currentDuration.endDate]}
                            ,{$ne: ["$userId", currentUser]}]},
                            1,
                            0
                        ]}
                    }
                }
                ,{
                    $group:{
                        _id: null,
                        countPast: {$sum: "$myInteractionPast"},
                        countPresent: {$sum: "$myInteractionPresent"},
                        countCompanyPast: {$sum: "$companyInteractionPast"},
                        countCompanyPresent: {$sum: "$companyInteractionPresent"}
                    }
                }
            ]).exec(function(err, data){
                data = data[0]
                data.countCompanyPast += data.countPast
                data.countCompanyPresent += data.countPresent
                delete data._id
                callBack(data)
            })
        })
    }

    this.getLastInteractedDate = function(userId,callback){
        InteractionsCollection.aggregate([
            {
                $match:{ownerId:userId}
            },
            { $sort: { 'interactionDate': 1 } },
            {
                $group:
                {
                    _id: '$emailId',
                    lastInteractedType:{$last:"$interactionType"},
                    lastInteractedDate: { $last: "$interactionDate" },
                    lastInteracted: { $last: "$interactionDate" },
                    action: { $last: "$action" }
                }
            }

        ]).exec(function(err,date){

            if(!err){
                callback(date)
            } else {
                callback([])
            }
        });
    }

    this.checkInteractedPastDay = function(userId,callback){
        
        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId,
                    interactionDate:{$gte: moment().subtract(1, "days").toDate()}

                }
            },
            {
                $limit:1
            }
        ]).exec(function(err,result){
            if(result.length>0){
                callback(true)
            } else {
                callback(false)
            }

        });
    }

    this.getLastInteractedDateByEmailList = function(userId,emailIds,callback){
        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId,
                    userId:{$ne:userId}
                }
            },
            {
                $match:{
                    "emailId":{$in:emailIds},
                    "interactionType":{$ne:null}
                }
            },
            { $sort: { 'interactionDate': 1 } },
            {
                $group:
                {
                    _id: '$emailId',
                    lastInteractedType:{$last:"$interactionType"},
                    lastInteractedDate: { $last: "$interactionDate" },
                    lastInteracted: { $last: "$interactionDate" },
                    action: { $last: "$action" },
                    title:{$last:"$title"},
                    description:{$last:"$description"},
                    emailContentId:{$last:"$emailContentId"},
                    refId:{$last:"$refId"},
                    firstName:{$last:"$firstName"},
                    lastName:{$last:"$lastName"}
                }
            }
        ]).exec(function(err,date){

            if(!err){
                callback(err,date)
            } else {
                callback(err,[])
            }
        });
    }

    this.getLastInteractedDateByEmailAndMobileList = function(userId,emailIds,mobileNumberArr,callback){
        
        var q = {
            userId: {$ne:userId},
            interactionDate:{$lte: new Date()}
        };

        q['$or'] = [{mobileNumber:{$in:mobileNumberArr}},{emailId:{$in:emailIds}}]
        
        InteractionsCollection.aggregate([
            {
                $match:{ownerId:userId}
            },
            {
                $match:q
            },
            { $sort: { 'interactionDate': 1 } },
            {
                $project:{
                    emailOrMobile:{$cond:{ if:"$emailId", then:"$emailId", else:"$mobileNumber" }},
                    interactionDate:"$interactionDate",
                    interactionType:"$interactionType",
                    action: "$action",
                    title: "$title",
                    description:"$description",
                    emailContentId:"$emailContentId",
                    mobileNumber:"$mobileNumber",
                    emailId:'$emailId',
                    refId:"$refId",
                    trackInfo: "$trackInfo",
                    trackId: "$trackId"
                }
            },
            {
                $group:
                {
                    _id: "$emailOrMobile",
                    lastInteractedType:{$last:"$interactionType"},
                    lastInteractedDate: { $max: "$interactionDate" },
                    lastInteracted: { $max: "$interactionDate" },
                    action: { $last: "$action" },
                    title:{$last:"$title"},
                    description:{$last:"$description"},
                    emailContentId:{$last:"$emailContentId"},
                    mobileNumber:{$last:"$mobileNumber"},
                    emailId:{$last:'$emailId'},
                    refId:{$last:"$refId"},
                    trackInfo: {$last:"$trackInfo"},
                    trackId: {$last:"$trackId"}
                }
            }
        ]).exec(function(err,date){

            if(!err){
                callback(date)
            } else {
                callback([])
            }
        });
    };

    this.getLastInteractedDateForAllContacts = function(userIdArr,emailIdsArr,callback){

        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:{$in:userIdArr},
                    userId:{$nin:userIdArr},
                    emailId:{$in:emailIdsArr}
                }
            },
            { $sort: { 'interactionDate': 1 } },
            {
                $group:
                {
                    _id: '$emailId',
                    lastInteractedType:{$last:"$interactionType"},
                    lastInteractedDate: { $last: "$interactionDate" },
                    lastInteracted: { $last: "$interactionDate" },
                    action: { $last: "$action" },
                    title:{$last:"$title"},
                    description:{$last:"$description"},
                    emailContentId:{$last:"$emailContentId"},
                    refId:{$last:"$refId"},
                    trackInfo: {$last:"$trackInfo"},
                    trackId: {$last:"$trackId"}
                }
            }
        ]).exec(function(err,date){

            if(!err){
                callback(date)
            } else {
                callback([])
            }
        });
    };

    this.updateMeetingsAsInteractions = function (userId,interactions,callback) {
        addAllInteractions(userId,interactions,callback);
    }

    this.addInteractionsBulk = function (user,interactionsArr,refIdArr,callback) {
        if(interactionsArr.length>0 && refIdArr.length>0){
            checkInteractionsExists(user,interactionsArr,refIdArr,callback);
        } else {
            callback(false)
        }
    }

    this.getLastInteractedDateArray = function(userIdArr,emailIds,callback){
        interaction.aggregate([
            {
                $match:{userId:{$in:userIdArr}}
            },
            {
                $unwind:'$interactions'
            },
            {
                $match:{"interactions.emailId":{$in:emailIds}}
            },
            { $sort: { 'interactions.interactionDate': 1 } },
            {
                $group:
                {
                    _id: '$interactions.emailId',
                    lastInteractedType:{$last:"$interactions.interactionType"},
                    lastInteractedDate: { $last: "$interactions.interactionDate" },
                    lastInteracted: { $last: "$interactions.interactionDate" },
                    action: { $last: "$interactions.action" }
                }
            }
        ]).exec(function(err,date){

            if(!err){
                callback(date)
            } else {
                callback([])
            }
        });
    }

    this.getEmailSentTimeToFindPreferredSlots = function(userId,callback){
        InteractionsCollection.aggregate([
            {
                $match:{
                    ownerId:userId,
                    userId:{$ne:userId},
                    interactionDate:{$gte: moment().subtract(90, "days").toDate()},
                    // interactionDate:{$gte: new Date('2016-06-02'), $lt:new Date('2016-09-16')},
                    interactionType:'email',
                    action:'receiver'
                }
            },
            { $sort: { 'interactionDate': 1 } },
            {
                $project:
                {
                    ownerEmailId:"$ownerEmailId",
                    _id:0,
                    // action:"$action",
                    interactionDate:"$interactionDate",
                    emailId:"$emailId",
                    // interactionType:"$interactionType",
                    date: { $dateToString: { format: "%Y-%m-%dT%H:00:00", date: "$interactionDate" }},
                    hour : {$hour:"$interactionDate"},
                    title:"$title",
                    refId:"$refId"
                }
            },
            {
                $group:{
                    _id:"$refId",
                    recipients:{
                        $push:{
                            emailId:"$emailId",
                        }
                    },
                    ownerEmailId:{$first:"$ownerEmailId"},
                    interactionDate:{$first:"$interactionDate"},
                    date: {$first:{ $dateToString: { format: "%Y-%m-%dT%H:00:00", date: "$interactionDate" }}},
                    hour : {$first:{$hour:"$interactionDate"}},
                    title:{$first:"$title"}
                }
            },
            {
                $project:
                {
                    ownerEmailId:"$ownerEmailId",
                    _id:0,
                    // action:"$action",
                    recipients:"$recipients",
                    interactionDate:"$interactionDate",
                    emailId:"$emailId",
                    // interactionType:"$interactionType",
                    date: "$date",
                    hour : "$hour",
                    title:"$title",
                    refId:"$_id"
                }
            }
            // {
            //     $group:
            //     {
            //         _id:"$hour",
            //         interaction:{
            //             $push:{
            //                 ownerEmailId:"$ownerEmailId",
            //                 action:"$action",
            //                 interactionDate:"$interactionDate",
            //                 emailId:"$emailId",
            //                 interactionType:"$interactionType",
            //                 title:"$title",
            //                 date:"$date",
            //                 hour:"$hour"
            //             }
            //         },
            //         count:{$sum:1}
            //     }
            // }
        ]).exec(function(err,data){
            if(!err){
                callback(null,data)
            } else {
                callback(err,[])
            }
        });
    }
}

function checkInteractionsExists(user,interactionsArr,refIdArr,callback){
    InteractionsCollection.aggregate([
        {
            $match:{ownerId:castToObjectId(user._id.toString())}
        },
        {
            $match:{"refId":{$in:refIdArr}}
        },
        {
            $group:{
                _id:null,
                refIdList:{$addToSet:"$refId"},
                titleList:{$addToSet:"$title"}
            }
        }
    ]).exec(function(error,result){
        if(error){
            logger.info('Error in checkInteractionsExists():interactionsManagement ',error);
            callback(false);
        }
        else if(result && result.length > 0 && result[0] && result[0].refIdList && result[0].refIdList.length > 0){
            var nonExistsInteractions = [];
            for(var i=0; i<interactionsArr.length; i++){
                if(result[0].refIdList.indexOf(interactionsArr[i].refId) == -1){
                    nonExistsInteractions.push(interactionsArr[i]);
                }
                else if(result[0].titleList.indexOf(interactionsArr[i].title) == -1){
                    nonExistsInteractions.push(interactionsArr[i]);
                }
            }

            if(nonExistsInteractions.length > 0){
                addAllInteractions(castToObjectId(user._id.toString()),nonExistsInteractions,callback)
            }
            else{
                callback(true);
            }
        }
        else{
            addAllInteractions(castToObjectId(user._id.toString()),interactionsArr,callback)
        }
    })
}

function addAllInteractions(userId,nonExistsInteractions,callback){

  var InteractionsCollection = require('../databaseSchema/userManagementSchema').interactions
  myUserCollection.findOne({_id: castToObjectId(userId.toString())}, {emailId: 1,companyId:1}, function(err, user){

    if(err){
      logger.error("Error in addAllInteractions - ",err)
      logger.error(err.stack)
      callback(false)
    }else if(user){
      nonExistsInteractions = _.map(nonExistsInteractions, function(interaction){
        interaction.ownerId = userId
        interaction.ownerEmailId = user.emailId
          interaction.companyId = user.companyId?castToObjectId(String(user.companyId)):null;
        return interaction
      })

        nonExistsInteractions.forEach(function(ele){ //since it was trying to create the duplicate _id
            ele.userId = ele.userId?castToObjectId(ele.userId):null;
            delete ele._id;
        });

        var interactionActivity = checkPrevMailHasBeenReplied(nonExistsInteractions,user.emailId,userId.toString());

        if(nonExistsInteractions.length>0){

            InteractionsCollection.collection.insert(nonExistsInteractions, function(error, result){

                contactManagementObj.updateInteractionActivity(castToObjectId(userId.toString()),interactionActivity);

                if(user.companyId){
                    accManagementObj.updateLastInteractedDate(castToObjectId(user.companyId.toString()),nonExistsInteractions);
                }

                if(error){
                    logger.info('Error in addAllInteractions(): interactionsManagement ',error);
                    callback(false)
                }
                else{
                    callback(true);
                }
            
            })
        } else {
            callback(false)
        }

    } else {
        callback(true);
    }
  })
}

interactionsManagement.prototype.updateNewInteractionsCollection = function (userId,user,interactions){

    var refIds = [],
    accInteractions = [];

    _.each(interactions,function (el) {
        refIds.push(el.refId);
    })

    var findQuery = {ownerId:userId,refId:{$in:refIds}}
    if(user && user.companyId) {
        findQuery = {companyId:user.companyId,ownerId:userId,refId:{$in:refIds}}
    }

    // newInteractionsCollection.find({ownerId:userId,refId:{$in:refIds}},function (error,existingInteractions) {
    newInteractionsCollection.find(findQuery).lean().exec(function (error,existingInteractions) {

        try {
            var existingRefIds = _.pluck(existingInteractions,"refId")
            var nonExistsInteractions = [];

            for(var i=0; i<interactions.length; i++){
                if(existingRefIds.indexOf(interactions[i].refId) == -1){
                    nonExistsInteractions.push(interactions[i]);
                    accInteractions.push({
                        companyId: interactions[i].companyId,
                        ownerId: interactions[i].ownerId,
                        date: moment(interactions[i].interactionDate).format("DDMMYYYY"),
                        accountName:fetchCompanyFromEmail(interactions[i].emailId),
                        ownerEmailId: interactions[i].ownerEmailId
                    });
                }
            }

            if(accInteractions.length>0){
                updateAccInteractions(accInteractions)
            }

            if(nonExistsInteractions.length){
                newInteractionsCollection.collection.insert(nonExistsInteractions, function(error, result){
                });
            }
        } catch (err){
            logger.info('updateNewInteractionsCollection() Error ',err);
        }

    })

}

function updateAccInteractions(interactions,callback) {

    AccInteractionCollection.collection.insert(interactions, function(error, result){
        if(callback){
            callback()
        }
    });
}

function checkPrevMailHasBeenReplied (messages,userEmailId,userId) {

    var filterMessages = messages.filter(function (message) {
        return message.userId != userId
    })

    var data = _
        .chain(filterMessages)
        .groupBy("emailId")
        .map(function(value, key) {
            var sent = 0,received = 0,hasReplied = false;

            value.sort(function (o2, o1) {
                return new Date(o1.interactionDate) > new Date(o2.interactionDate) ? 1 : new Date(o1.interactionDate) < new Date(o2.interactionDate) ? -1 : 0;
            });

            var lastButOneSender = null;

            if(value && value.length>1){
                lastButOneSender = value[1]?value[1]:null
            }

            if(value && value.length == 1){
                lastButOneSender = value[0]
            }

            _.each(value,function (val) {
                if((val.emailId == userEmailId && val.action == "sender") || val.emailId != userEmailId && val.action == "receiver"){
                    sent++
                } else {
                    received++
                }
            })

            if(lastButOneSender && lastButOneSender.emailId == userEmailId && lastButOneSender.action == "sender"){
                hasReplied = true
            }

            if(lastButOneSender && lastButOneSender.emailId != userEmailId && lastButOneSender.action == "receiver"){
                hasReplied = true
            }

            return {
                emailThreadId:key,
                hasReplied:hasReplied,
                lastButOneSender:lastButOneSender,
                cEmailIds:lastButOneSender.emailId,
                received:received,
                sent:sent
            };

        })
        .value();

    return data;
}

function constructInteractionManagementObject(obj){
    var interactionDate = new Date();
    var interaction = {
        userId:obj.userId,
        emailId:obj.emailId,
        type:obj.type,
        subType:obj.subType,
        action:obj.action,
        interactionDate:obj.interactionDate || interactionDate,
        refId:obj.refId,
        createdDate:interactionDate,
        source:obj.source,
        title:obj.title || '',
        description:obj.description || ''
    };

    if(checkRequired(obj.mobileNumber)){
        interaction.mobileNumber = obj.mobileNumber;
    }

    if(checkRequired(obj.endDate)){
        interaction.endDate = obj.endDate;
    }

    if(checkRequired(obj.firstName)){
        interaction.firstName = obj.firstName;
    }
    if(checkRequired(obj.lastName)){
        interaction.lastName = obj.lastName;
    }
    if(checkRequired(obj.companyName)){
        interaction.companyName = obj.companyName;
    }
    if(checkRequired(obj.designation)){
        interaction.designation = obj.designation;
    }
    if(checkRequired(obj.profilePicUrl)){
        interaction.profilePicUrl = obj.profilePicUrl;
    }
    if(checkRequired(obj.publicProfileUrl)){
        interaction.publicProfileUrl = obj.publicProfileUrl;
    }
    if(checkRequired(obj.location)){
        interaction.location = obj.location;
    }
    if(checkRequired(obj.trackId)){
        interaction.trackId = obj.trackId;
    }
    if(checkRequired(obj.user_twitter_id)){
        interaction.user_twitter_id = obj.user_twitter_id;
    }
    if(checkRequired(obj.user_twitter_name)){
        interaction.user_twitter_name = obj.user_twitter_name;
    }

    if(checkRequired(obj.trackInfo) && checkRequired(obj.trackInfo.lastOpenedOn)){
        interaction.trackInfo = obj.trackInfo;
    }

    return interaction;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

function removeInvalidContentFromArray(arr,invalidContent){
    if(checkRequired(arr) && arr.length > 0)
        return  arr.filter(function(n){ return invalidContent.indexOf(n) == -1 });
    else return [];
}

function removeDuplicates_id(arr){

    var end = arr.length;

    for(var i = 0; i < end; i++)
    {
        for(var j = i + 1; j < end; j++)
        {
            if (arr[i]._id == arr[j]._id)
            {
                var shiftLeft = j;
                for(var k = j+1; k < end; k++, shiftLeft++)
                {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

function constructInteractionManagementObject_new(obj){
    var interactionDate = new Date();
    var interaction = {
        userId:obj.userId,
        emailId:obj.emailId,
        interactionType:obj.type,
        subType:obj.subType,
        action:obj.action,
        interactionDate:obj.interactionDate || interactionDate,
        refId:obj.refId,
        createdDate:interactionDate,
        source:obj.source,
        title:obj.title || '',
        description:obj.description || ''
    };

    if(checkRequired(obj.mobileNumber)){
        interaction.mobileNumber = obj.mobileNumber;
    }

    if(checkRequired(obj.endDate)){
        interaction.endDate = obj.endDate;
    }

    if(checkRequired(obj.firstName)){
        interaction.firstName = obj.firstName;
    }
    if(checkRequired(obj.lastName)){
        interaction.lastName = obj.lastName;
    }
    if(checkRequired(obj.companyName)){
        interaction.companyName = obj.companyName;
    }
    if(checkRequired(obj.designation)){
        interaction.designation = obj.designation;
    }
    if(checkRequired(obj.profilePicUrl)){
        interaction.profilePicUrl = obj.profilePicUrl;
    }
    if(checkRequired(obj.publicProfileUrl)){
        interaction.publicProfileUrl = obj.publicProfileUrl;
    }
    if(checkRequired(obj.location)){
        interaction.location = obj.location;
    }
    if(checkRequired(obj.trackId)){
        interaction.trackId = obj.trackId;
    }
    if(checkRequired(obj.user_twitter_id)){
        interaction.user_twitter_id = obj.user_twitter_id;
    }
    if(checkRequired(obj.user_twitter_name)){
        interaction.user_twitter_name = obj.user_twitter_name;
    }

    if(checkRequired(obj.trackInfo) && checkRequired(obj.trackInfo.lastOpenedOn)){
        interaction.trackInfo = obj.trackInfo;
    }

    return interaction;
}

function getFinalArr(interactions,sortByDate,noSort,reverse,diffFormat){
    var finalArr = [];
    for(var i=0; i<interactions.length; i++){
        var firstName = [];
        var lastName = [];
        var userId = [];
        if(diffFormat && interactions[i].obj && interactions[i].obj.length > 0){
            interactions[i].obj.sort(function (o1, o2) {
                return new Date(o1.date) > new Date(o2.date) ? -1 : new Date(o1.date) < new Date(o2.date) ? 1 : 0;
            });

            interactions[i].obj.forEach(function(item){
                if(firstName.length ==0 && checkRequired(item.firstName)){
                    firstName.push(item.firstName)
                }
                if(lastName.length ==0 && checkRequired(item.lastName)){
                    lastName.push(item.lastName)
                }
                if(userId.length ==0 && checkRequired(item.userId)){
                    userId.push(item.userId)
                }
            });
            interactions[i].obj = [];
        }

        if(firstName.length == 0){
            firstName.push(interactions[i]._id.emailId)
        }

        interactions[i].firstName = firstName;
        interactions[i].lastName = lastName;
        interactions[i].userId = userId;
        var exist = false;
        for(var j=0; j<finalArr.length; j++){

            if((checkRequired(interactions[i]._id.emailId) && interactions[i]._id.emailId == finalArr[j]._id.emailId) || (checkRequired(interactions[i]._id.mobileNumber) && interactions[i]._id.mobileNumber == finalArr[j]._id.mobileNumber)){
                finalArr[j].count = finalArr[j].count+interactions[i].count;
                if(!checkRequired(finalArr[j].mobileNumber) && checkRequired(interactions[i]._id.mobileNumber)){
                    finalArr[j].mobileNumber = interactions[i]._id.mobileNumber;
                }
                if(new Date(finalArr[j].lastInteracted) < new Date(interactions[i].lastInteracted)){
                    finalArr[j].lastInteracted = interactions[i].lastInteracted;
                }
                if(finalArr[j].firstName.length == 0){
                    finalArr[j].firstName = interactions[i].firstName
                }
                if(finalArr[j].lastName.length == 0){
                    finalArr[j].lastName = interactions[i].lastName
                }

                finalArr[j].userId = finalArr[j].userId.concat(interactions[i].userId)
                finalArr[j].publicProfileUrl = finalArr[j].publicProfileUrl.concat(interactions[i].publicProfileUrl)

                exist = true;
            }
        }
        if(!exist){
            finalArr.push(interactions[i]);
        }
    }

    if(!noSort){
        if(sortByDate){
            if(reverse){
                finalArr.sort(function (o1, o2) {
                    return new Date(o1.lastInteracted) > new Date(o2.lastInteracted) ? 1 : new Date(o1.lastInteracted) < new Date(o2.lastInteracted) ? -1 : 0;
                });
            }
            else finalArr.sort(function (o1, o2) {
                return new Date(o1.lastInteracted) > new Date(o2.lastInteracted) ? -1 : new Date(o1.lastInteracted) < new Date(o2.lastInteracted) ? 1 : 0;
            });
        }
        else{
            finalArr.sort(function(a,b)
            {
                if (a.count < b.count) return 1;
                if (a.count > b.count) return -1;
                return 0;
            });
        }
    }

    return finalArr;
}

function getContactsMatched(userId,emailArr,interactions,sortByDate,noSort,reverse,diffFormat,callback){
    var finalArr = [];
    for(var i=0; i<interactions.length; i++){

        var firstName = [];
        var lastName = [];
        var userIdL = [];
        if(diffFormat && interactions[i].obj && interactions[i].obj.length > 0){
            interactions[i].obj.sort(function (o1, o2) {
                return new Date(o1.date) > new Date(o2.date) ? -1 : new Date(o1.date) < new Date(o2.date) ? 1 : 0;
            });

            interactions[i].obj.forEach(function(item){
                if(firstName.length ==0 && checkRequired(item.firstName)){
                    firstName.push(item.firstName)
                }
                if(lastName.length ==0 && checkRequired(item.lastName)){
                    lastName.push(item.lastName)
                }
                if(userIdL.length ==0 && checkRequired(item.userId)){
                    userIdL.push(item.userId)
                }
            });
            interactions[i].obj = [];
        }
        if(firstName.length == 0){
            firstName.push(interactions[i]._id.emailId)
        }
        interactions[i].firstName = firstName;
        interactions[i].lastName = lastName;
        interactions[i].userId = userIdL;


        var exist = false;
        for(var j=0; j<finalArr.length; j++){
            if((checkRequired(interactions[i]._id.emailId) && interactions[i]._id.emailId == finalArr[j]._id.emailId) || (checkRequired(interactions[i]._id.mobileNumber) && interactions[i]._id.mobileNumber == finalArr[j]._id.mobileNumber)){
                finalArr[j].count = finalArr[j].count+interactions[i].count;
                if(!checkRequired(finalArr[j].mobileNumber) && checkRequired(interactions[i]._id.mobileNumber)){
                    finalArr[j].mobileNumber = interactions[i]._id.mobileNumber;
                }
                if(new Date(finalArr[j].lastInteracted) < new Date(interactions[i].lastInteracted)){
                    finalArr[j].lastInteracted = interactions[i].lastInteracted;
                }
                finalArr[j].firstName = finalArr[j].firstName.concat(interactions[i].firstName)
                finalArr[j].lastName = finalArr[j].lastName.concat(interactions[i].lastName)
                finalArr[j].userId = finalArr[j].userId.concat(interactions[i].userId)
                finalArr[j].publicProfileUrl = finalArr[j].publicProfileUrl.concat(interactions[i].publicProfileUrl)

                exist = true;
            }
        }
        if(!exist){
            if(checkRequired(interactions[i]._id.emailId)){
                var index = emailArr.indexOf(interactions[i]._id.emailId);
                if(index > -1){
                    emailArr.splice(index,1);
                }
            }

            finalArr.push(interactions[i]);
        }
    }

    if(emailArr.length > 0){

        var q = {"contacts.personEmailId":{$in:emailArr}};

        myUserCollection.aggregate([
            {
                $match:{_id:userId}
            },
            {
                $unwind:"$contacts"
            },
            {
                $match:q
            },
            {
                $group:{
                    _id:null,
                    contacts: {
                        $push:"$contacts"
                    }
                }
            }
        ]).exec(function(error,data){

            if(checkRequired(data) && data.length > 0 && data[0] && data[0].contacts && data[0].contacts.length > 0){
                var listC = [];
                for(var i=0; i<data[0].contacts.length; i++){
                    if(!checkRequired(data[0].contacts[i].personName)){
                        data[0].contacts[i].personName = data[0].contacts[i].personEmailId;
                    }
                    var obj = {
                        _id:{mobileNumber:data[0].contacts[i].mobileNumber,emailId:data[0].contacts[i].personEmailId},
                        firstName:[data[0].contacts[i].personName],
                        lastName:[],
                        userId:[data[0].contacts[i].personId],
                        publicProfileUrl:[data[0].contacts[i].publicProfileUrl],
                        lastInteracted:null,
                        count:0
                    };
                    listC.push(obj);
                }

                finalArr = finalArr.concat(listC);
            }

            if(!noSort){
                if(sortByDate){
                    if(reverse){
                        finalArr.sort(function (o1, o2) {
                            return new Date(o1.lastInteracted) > new Date(o2.lastInteracted) ? 1 : new Date(o1.lastInteracted) < new Date(o2.lastInteracted) ? -1 : 0;
                        });
                    }
                    else finalArr.sort(function (o1, o2) {
                        return new Date(o1.lastInteracted) > new Date(o2.lastInteracted) ? -1 : new Date(o1.lastInteracted) < new Date(o2.lastInteracted) ? 1 : 0;
                    });
                }
                else{
                    finalArr.sort(function(a,b)
                    {
                        if (a.count < b.count) return 1;
                        if (a.count > b.count) return -1;
                        return 0;
                    });
                }
            }
            callback(finalArr)
        })
    }
    else{
        if(!noSort){
            if(sortByDate){
                if(reverse){
                    finalArr.sort(function (o1, o2) {
                        return new Date(o1.lastInteracted) > new Date(o2.lastInteracted) ? 1 : new Date(o1.lastInteracted) < new Date(o2.lastInteracted) ? -1 : 0;
                    });
                }
                else finalArr.sort(function (o1, o2) {
                    return new Date(o1.lastInteracted) > new Date(o2.lastInteracted) ? -1 : new Date(o1.lastInteracted) < new Date(o2.lastInteracted) ? 1 : 0;
                });
            }
            else{
                finalArr.sort(function(a,b)
                {
                    if (a.count < b.count) return 1;
                    if (a.count > b.count) return -1;
                    return 0;
                });
            }
        }
        callback(finalArr);
    }
}

function validateContactInfo(contactInfo,contact){
    if(checkRequired(contactInfo)){
        if(checkRequired(contactInfo.contacts[0])){
            return false;
        }
        else return true;
    } else return true;
}

function arrayContains(needle, arrhaystack)
{
    return (arrhaystack.indexOf(needle) > -1);
}

function castToObjectId(id){
    return mongoose.Types.ObjectId(id)
};

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function getAggregationPipeline(userId,query,isArray){

    var matchQuery = {ownerId: userId}
    if(isArray){
        matchQuery = {ownerId: {$in:userId}}
    }
    
    var pipeline = [
    {
        $match: matchQuery
    },
    {
        $match: query
    },
    {
        $group:{
            _id: "$ownerId",
                interactions: {
                $push: {
                    emailId:"$emailId",
                        companyName:"$companyName",
                        interactionDate:"$interactionDate"
                }
            }
        }
    },
    {
        $unwind: "$interactions"
    },
    {
        $group:{
            _id: "$interactions.emailId",
                count: { $sum: 1},
            company: {$first: "$interactions.companyName"},
            lastInteracted: {$max: "$interactions.interactionDate"}
        }
    },
    {
        $sort: {"count" : -1}
    },
    {$project:{
        email: "$_id",
            count: "$count",
            company: 1,
            lastInteracted: 1,
            _id: 0
    }}
    ]
    return pipeline;
}

function getLocationAggregationPipeline(userId,query,isArray){
    var matchQuery = {ownerId: userId}
    if(isArray){
        matchQuery = {ownerId: {$in:userId}}
    }

    var pipeline = [
        {
            $match: matchQuery
        },
        {
            $match: query
        },
        {
            $group: {
                _id: "$location",
                count: {$sum: 1}
            }
        },
        {
            $group: {
                _id: null,
                totalCount: {$sum: "$count"},
                typeCounts: {$push: {_id: "$_id", count: "$count"}}
            }
        },
        {
            $unwind: "$typeCounts"
        },
        {
            $sort: {"typeCounts.count": -1}
        },
        {
            $limit: 5
        },
        {
            $group: {
                _id: null,
                totalCount: {$max: "$totalCount"},
                typeCounts: {$push: "$typeCounts"}
            }
        }
    ]
    return pipeline;
}

function getInteractionByTypeAggregationPipeline(userId,query,isArray){

    var matchQuery = {ownerId: userId}
    if(isArray){
        matchQuery = {ownerId: {$in:userId}}
    }

    var pipeline = [
        {
            $match: matchQuery
        },
        {
            $match: query
        },
        {
            $group:{
                _id:"$interactionType",
                count:{$sum:1}
            }
        },
        {
            $group:{
                _id:null,
                totalCount:{$sum:"$count"},
                maxCount:{$max:"$count"},
                typeCounts:{$push:{_id:"$_id",count:"$count"}}
            }
        }
    ]
    return pipeline;
}

function getInitiativeAggregationPipeline(userId,query,isArray){

    var matchQuery = {ownerId: userId}
    if(isArray){
        matchQuery = {ownerId: {$in:userId}}
    }

    var pipeline = [
        {
            $match: matchQuery
        }
        ,{
            $match: query
        } ,
        {
            $project: {
                sender: {$cond: [{$eq: ["$action", "sender"]}, 1, 0]},
                receiver: {$cond: [{$eq: ["$action", "receiver"]}, 1, 0]}
            }
        }
        ,{
            $group: {
                _id: null,
                initiatedByOthers: {$sum: "$sender"},
                initiatedByMe: {$sum: "$receiver"}
            }
        }
    ]
    return pipeline;
}

function getMonthlyAggregationPipeline(userId,query,isArray){

    var matchQuery = {ownerId: userId}
    if(isArray){
        matchQuery = {ownerId: {$in:userId}}
    }

    var pipeline = [
        {
            $match: matchQuery
        },
        {
            $match: query
        },
        {
            $project: {
                month: {$month: "$interactionDate"},
                year: {$year: "$interactionDate"},
                interactionDate: "$interactionDate",
                ownerId: 1
            }
        },
        {
            $group: {
                _id: {month: "$month", year: "$year"},
                count: {$sum: 1},
                sampleDate: {$first: "$interactionDate"},
                interactions: {
                    $push: {
                        userId: "$ownerId"
                    }
                }
            }
        },
        {
            $sort: {sampleDate: 1}
        }
    ]
    return pipeline;
}

var getInvalidEmailListFromDb = function (callback) {
    invalidEmailListCollection.findOne({type:"invalidEmail"}, function (err, lists) {
        if(!err){
            callback(lists.invalid)
        } else {
            callback([])
        }
    });
}

interactionsManagement.prototype.getLastInteractiondDate = function (userId,personEmailIdArr,callback) {

    InteractionsCollection.aggregate([
        {
            $match:{
                ownerId:userId,
                userId:{$ne:userId},
                emailId:{$in:personEmailIdArr}
            }
        },
        {
            $project:{
                emailOrMobile:{$cond:{ if:"$emailId", then:"$emailId", else:"$mobileNumber" }},
                interactionDate:"$interactionDate",
                interactionType:"$interactionType"
            }
        },
        {
            $group:
            {
                _id: "$emailOrMobile",
                lastInteractedDate: { $max: "$interactionDate" },
                interactionType: { $last: "$interactionType" }
            }
        }
    ]).exec(function (err,contacts) {

        if(!err && contacts){
            callback(err, contacts)
        } else {
            callback(err,null)
        }
    });
};

interactionsManagement.prototype.getLastInteractiondDateGroupByUser = function (userId,emailIds,fetchWith,callback) {

    var queryFinal = {
        "$or": emailIds.map(function (el) {
            el["ownerId"] = el.userId;
            el["userId"] = {"$ne": el.userId};
            el["emailId"] = fetchWith;

            return el;
        })
    }

        InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $project:{
                // emailOrMobile:{$cond:{ if:"$emailId", then:"$emailId", else:"$mobileNumber" }},
                interactionDate:"$interactionDate",
                interactionType:"$interactionType",
                ownerEmailId:"$ownerEmailId"
            }
        },
        {
            $group:
            {
                _id: "$ownerEmailId",
                lastInteractedDate: { $max: "$interactionDate" },
                interactionType: { $last: "$interactionType" }
            }
        }
    ]).exec(function (err,contacts) {

        if(!err && contacts){
            callback(err, contacts)
        } else {
            callback(err,null)
        }
    });
};

interactionsManagement.prototype.getEmailClassificationByEmail = function(from,to,emailId,callback){
    var dateMin = momentTZ(from);
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)
    var dateMax = momentTZ(to);

    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(59)
    dateMax.millisecond(0)
    
    InteractionsCollection.aggregate([
        {
            $match:
            {
                ownerEmailId: emailId,
                interactionType: "email",
                interactionDate: { $gt: new Date(dateMin), $lt: new Date(dateMax) },
                action:"sender"
            }
        },
        {
            $group: {
                _id: {
                    ne: { $ne: ["$userId", "$ownerId"] },
                    emailId: "$ownerEmailId",
                    uid: "$ownerId",
                },
                count: { $sum: 1 },
                interactions: {
                    $push: {
                        email: "$emailId",
                        source: "$source",
                        interactionType: "$interactionType",
                        title: "$title",
                        date: "$interactionDate",
                        "eSentiment": "$eSentiment",
                        "eMoney": "$eMoney",
                        "eAction": "$eAction",
                        "emailClassified": "$emailClassified",
                        "action":"$action"
                    }

                }
            }
        },
        { $match: { "_id.ne": true } },
        { $unwind: "$interactions" },
        {
            $project: {
                "_id": 0,
                to: "$_id.emailId",
                from: "$interactions.email",
                source: "$interactions.source",
                interactionType: "$interactions.interactionType",
                title: "$interactions.title",
                date: { $dateToString: { format: "%Y-%m-%d", date: "$interactions.date" }},
                "eSentiment": "$interactions.eSentiment",
                "eMoney": "$interactions.eMoney",
                "eAction": "$interactions.eAction",
                "emailClassified": "$interactions.emailClassified",
                "action":"$interactions.action"
            }
        }],function(error, result){
        if(error){
            callback(error, null)
        }
        else{
            callback(null,result)
        }
    });
}

interactionsManagement.prototype.fetchYesterdaysMeeting = function (userId,dateMin,dateMax,project,callback) {

    var dateRange = {'$gte': dateMin, '$lt': dateMax}
    InteractionsCollection.find({ownerId:userId,userId:{$ne:userId},interactionType:'meeting',interactionDate:dateRange},project,function (err,interactions) {
        callback(err,interactions);
    });

}

interactionsManagement.prototype.removeInvalidContactInteractionByPattern = function (list,callback) {
    InteractionsCollection.remove({emailId:{$in:list.invalid,$nin:list.exception}},function (err,result) {
        callback(err,result);
    });
}

interactionsManagement.prototype.interactionsBeforeJoiningForMultiUsers = function (users,dataForQuery,callback) {

    var query = { "$or": dataForQuery.map(function(el) {
        var obj = {};

        obj["ownerId"] = el.userId;
        obj["emailId"] = el.contactEmailId;
        obj["userId"] = {$ne:el.userId};
        obj["interactionDate"] = {$lte: new Date(el.createdDate)}
        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id: {ownerEmailId:"$ownerEmailId",contactEmailId:"$emailId"},
                beforeCount: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });

}

interactionsManagement.prototype.interactionsBeforeJoining = function (userEmailId,date,callback) {

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerEmailId:userEmailId,
                userId:{$ne:"$ownerId"},
                interactionDate:{$lte: new Date(date)}
            }
        },
        {
            $group: {
                _id: {ownerEmailId:"$ownerEmailId"},
                beforeCount: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });

}

interactionsManagement.prototype.interactionsAfterJoining = function (userEmailId,date,callback) {

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerEmailId:userEmailId,
                userId:{$ne:"$ownerId"},
                interactionDate:{$gte: new Date(date)}
            }
        },
        {
            $group: {
                _id: {ownerEmailId:"$ownerEmailId"},
                afterCount: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });
}

interactionsManagement.prototype.interactionsPast30Days = function (userId,callback) {

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerId:userId,
                userId:{$ne:userId},
                interactionDate:{$gte: moment().subtract(30, "days").toDate(),$lte: new Date()}
            }
        },
        {
            $group: {
                _id: {ownerEmailId:"$ownerEmailId"},
                past30days: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });
}

interactionsManagement.prototype.interactionsOnRelatas = function (userEmailId,date,callback) {

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerEmailId:userEmailId,
                userId:{$ne:"$ownerId"},
                source:"relatas"
            }
        },
        {
            $group: {
                _id: {ownerEmailId:"$ownerEmailId"},
                relatasSource: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });

}

interactionsManagement.prototype.interactionsOnRelatasPast30Days = function (userEmailId,callback) {

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerEmailId:userEmailId,
                userId:{$ne:"$ownerId"},
                source:"relatas",
                interactionDate:{$gte: moment().subtract(30, "days").toDate(),$lte: new Date()}
            }
        },
        {
            $group: {
                _id: {ownerEmailId:"$ownerEmailId"},
                relatasSource: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });

}

interactionsManagement.prototype.interactionsByDate = function (userEmailId,emailIds,from,to,callback) {

    //By default 30 days.

    from = from?from:moment().subtract(30, "days").toDate();
    to = to?to:new Date();

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerEmailId:userEmailId,
                userId:{$ne:"$ownerId"},
                interactionDate:{$gte: from,$lte: to},
                emailId:{$in:emailIds}
            }
        },
        {
            $group: {
                _id: {ownerEmailId:"$ownerEmailId"},
                count: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });

}

interactionsManagement.prototype.interactionsAfterAfterForMultiUsers = function (users,dataForQuery,callback) {

    var query = { "$or": dataForQuery.map(function(el) {
        var obj = {};

        obj["ownerId"] = el.userId;
        obj["emailId"] = el.contactEmailId;
        obj["userId"] = {$ne:el.userId};
        obj["interactionDate"] = {$gte: new Date(el.createdDate)}
        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id: {ownerEmailId:"$ownerEmailId",contactEmailId:"$emailId"},
                afterCount: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });
}

interactionsManagement.prototype.actionsOnRelatasForMultiUsers = function (users,dataForQuery,callback) {

    var query = { "$or": dataForQuery.map(function(el) {
        var obj = {};

        obj["ownerId"] = el.userId;
        obj["emailId"] = el.contactEmailId;
        obj["userId"] = {$ne:el.userId};
        obj["source"] = "relatas"
        obj["interactionDate"] = {$gte: new Date(el.createdDate)}
        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id: {ownerEmailId:"$ownerEmailId",contactEmailId:"$emailId"},
                sourceRelatasCount: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });
}

interactionsManagement.prototype.getInteractionsCountByTypeForUsers = function (userIds,minDate,MaxDate,teamMembers,external,callback) {
    
    var query = { "$or": userIds.map(function(el) {
        var notIn = [];
        notIn.push(el._id)
        notIn.push(el._id.toString())

        var obj = {}
        obj["ownerId"] = el._id
        obj["userId"] = {$nin:notIn}
        obj["interactionType"] = {$in:["call","email","meeting","meeting-comment","sms","twitter"] } // We have only these many number of interactions.

        if(minDate){
            obj["interactionDate"] = {$gte: new Date(minDate),$lte: new Date(MaxDate)}
        }

        if(teamMembers.length>0 && external){
            obj["emailId"] = {$nin:teamMembers}
        } else if(teamMembers.length>0 && !external){
            obj["emailId"] = {$in:teamMembers}
        }

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group:{
                _id:{
                    "ownerEmailId":"$ownerEmailId",
                    "type":"$interactionType"
                },
                count:{$sum:1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });
}

interactionsManagement.prototype.interactionsForMultiUsersContacts = function (dataForQuery,dateMin,dateMax,common,callback) {

    // var groupByUser = _(dataForQuery).groupBy('userEmailId').transform(function(result, current) {
    //     if(current && current[0]){
    //         result.push({
    //             userEmailId: current[0].userEmailId,
    //             userId: current[0].userId,
    //             contactEmailId: _.map(current, 'contactEmailId')
    //         });
    //     }
    //
    // }, []).value();

    var query = { "$or": dataForQuery.map(function(el) {
        var obj = {};

        var notIn = [];
        notIn.push(el)
        notIn.push(el.toString())

        obj["ownerId"] = el;
        // obj["emailId"] = {$in:el.contactEmailId};
        obj["userId"] = {$nin:notIn};
        // obj["userId"] = {$ne:el};
        if(dateMin && dateMax){
            obj["interactionDate"] = {$gte: new Date(dateMax),$lte: new Date(dateMin)}
        }
        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $sort: { 'interactionDate': 1 }
        },
        {
            $group: {
                _id: {
                    ownerEmailId:"$ownerEmailId",
                    contactEmailId:"$emailId",
                    // type:"$interactionType",
                    $dayOfYear:"$interactionDate",
                },
                lastInteractedDate: { $max: "$interactionDate" },
                count: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });
}

interactionsManagement.prototype.interactionsForContacts = function (userId,dateMin,dateMax,contactEmailIds,callback) {

    var notIn = [];
    notIn.push(userId)
    notIn.push(userId.toString())

    var query = {
        ownerId:userId,
        // userId:{$ne:userId},
        userId:{$nin:notIn},
        interactionDate:{$gte: new Date(dateMax),$lte: new Date(dateMin)}
    }

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        // {
        //     $sort: { 'interactionDate': 1 }
        // },
        {
            $group: {
                _id: {
                    ownerEmailId:"$ownerEmailId",
                    contactEmailId:"$emailId"
                    // // type:"$interactionType",
                    // $dayOfYear:"$interactionDate",
                },
                // lastInteractedDate: { $max: "$interactionDate" },
                count: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });
}

interactionsManagement.prototype.getInteractionsByDateRange = function(userId,cUserId,isEmailId,limit,dateMin,dateMax,mobileNumber,callback){

    var isEmail = true
    if(!validateEmail(cUserId)) {
        isEmail = false
    }

    var q = {
        "interactionDate":{
            $lte:new Date(dateMin)
        },
        "userId": {$ne: userId}
    };

    if(isEmailId){
        q["emailId"] = cUserId;
    }
    else q["userId"] = cUserId;

    if(isNumber(cUserId) && !isEmail){
        var mobile10Digit = cUserId.length <= 10 ? cUserId : cUserId.substr(cUserId.length - 10);
        q = {
            "mobileNumber": new RegExp('\d{0,4}'+mobile10Digit+'$'),
            "userId": {$ne: userId}
        }
    }

    if(isEmailId && checkRequired(cUserId) && checkRequired(mobileNumber)){
        q = {
            "interactionDate":{
                $lte:new Date(dateMin)
            },
            "userId": {$ne: userId}
        }

        q.$or = [{"emailId":cUserId},{"mobileNumber":mobileNumber}]
    }

    InteractionsCollection.aggregate([
        {
            $match:{ownerId:userId}
        },
        {
            $match:q
        },
        {
            $project: {
                _id: "$_id",
                emailId: "$emailId",
                firstName: "$firstName",
                userId: "$userId",
                publicProfileUrl: "$publicProfileUrl",
                interactionDate: "$interactionDate",
                interactionType: "$interactionType",
                subType: "$subType",
                action: "$action",
                title: "$title",
                refId: "$refId",
                description: "$description",
                trackInfo: "$trackInfo",
                trackId: "$trackId",
                ignore: "$ignore",
                createdDate: "$createdDate",
                emailContentId: "$emailContentId",
                googleAccountEmailId: "$googleAccountEmailId",
                source: "$source"
            }
        },
        {
            $sort:{interactionDate:-1}
        },
        // {
        //     $limit:limit
        // },
        {
            $group:{
                _id:null,
                firstName:{$addToSet:"$firstName"},
                emailId:{$addToSet:"$emailId"},
                userId:{$addToSet:"$userId"},
                publicProfileUrl:{$addToSet:"$publicProfileUrl"},
                interactions:{$push:{
                    _id: "$_id",
                    interactionDate: "$interactionDate",
                    interactionType: "$interactionType",
                    subType: "$subType",
                    action: "$action",
                    emailId: "$emailId",
                    title: "$title",
                    refId: "$refId",
                    description: "$description",
                    trackInfo: "$trackInfo",
                    trackId: "$trackId",
                    ignore: "$ignore",
                    createdDate: "$createdDate",
                    emailContentId: "$emailContentId",
                    googleAccountEmailId: "$googleAccountEmailId",
                    source: "$source"
                }}
            }
        }
    ]).exec(function(err,interactions){
        if(!err && checkRequired(interactions) && interactions.length > 0){
            callback(interactions)
        }
        else callback([])
    })
};

interactionsManagement.prototype.insertInteractions = function(userId,interactions,user,callback) {
    
    var InteractionsCollection = require('../databaseSchema/userManagementSchema').interactions;

    InteractionsCollection.collection.insert(interactions, function(error, result){

        updateContactsForMeetingInteractions(userId,_.pluck(interactions,"emailId"),user,function () {
            if(callback){
                callback()
            }
        });
    })
}

function updateContactsForMeetingInteractions(userId,cleanArray,user,callback){

    var contactsArray = [];

    cleanArray = _.uniq(cleanArray);

    for(var i =0;i<cleanArray.length;i++){
        var contactObj2 = {
            _id:new customObjectId(),
            personId:null,
            personName:cleanArray[i],
            personEmailId:cleanArray[i].toLowerCase(),
            birthday:null,
            companyName:null,
            designation:null,
            addedDate:new Date(),
            verified:false,
            relatasUser:false,
            relatasContact:true,
            mobileNumber:null,
            location:null,
            source:'meeting-interactions',
            account:{
                name:fetchCompanyFromEmail(cleanArray[i]),
                selected:false,
                updatedOn:new Date()
            }
        };

        contactsArray.push(contactObj2)
    }

    // Creating new Contact Collection
    contactManagementObj.insertContacts(userId,user.emailId,cleanArray,contactsArray,user.lastLoginDate,function (cErr,contactsList) {
        if(callback){
            callback()
        }
    });

    contactObj.addContactNotExist(userId,user.emailId,contactsArray,[],cleanArray,'meetings',false);
}

interactionsManagement.prototype.checkIfInteractedWithContacts = function (queryFinal,callback) {

    InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $group:{
                _id:"$emailId",
                personEmailId:{$first:"$emailId"}
            }

        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}

interactionsManagement.prototype.updateInteractionDateById = function (list,usersEmailIdObj,back) {

    if(list.length>0){

        var bulk = InteractionsCollection.collection.initializeUnorderedBulkOp();
        _.each(list,function (el) {

            if(el.participants.length>0){
                var uList = [];
                _.each(el.participants,function (pa) {
                    if(usersEmailIdObj[pa.emailId]){
                        uList.push(pa.emailId);
                    }
                });

                if(uList.length>0){
                    bulk.find({ownerEmailId:{$in:uList},refId:el.invitationId}).update({$set:{"interactionDate":new Date(el.interactionDate)}});
                }
            }
        });

        bulk.execute(function(err, result) {
            if(err){
                logger.info('Error in updateInteractionDateById():InteractionsManagement ',err )
            }

            if(back) back(true)
        });   
    } else {
        if(back) back(true)
    }
}

interactionsManagement.prototype.getInteractionCountByUsersAndContacts = function (usersAndContacts,dateMin,dateMax,callback) {

    var queryFinal = { "$or": usersAndContacts.map(function(el) {

        var obj = {}
        obj["ownerEmailId"] = el.userEmailId
        obj["emailId"] = { "$in": el.contacts };

        if(el.minDate && el.maxDate){
            obj["interactionDate"] = {$gte:new Date(el.minDate),$lte:new Date(el.maxDate)}
        }

        if(!el.maxDate && el.minDate){
            obj["interactionDate"] = {$gte:new Date(el.minDate),$lte:new Date()}
        }

        if(dateMin && dateMax){
            obj["interactionDate"] = {$gte:new Date(dateMin),$lte:new Date(dateMax)}
        } else if(!el.minDate && !el.maxDate) {
            obj["interactionDate"] = {$lte:new Date()}
        }

        return obj;
    })};
    
    InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $group:{
                _id:"$ownerEmailId",
                count:{"$sum":1}
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}

interactionsManagement.prototype.getInteractionCountByGroupByUserContacts = function (usersAndContacts,dateMin,dateMax,callback) {

    var queryFinal = { "$or": usersAndContacts.map(function(el) {
        var obj = {}
        obj["ownerEmailId"] = el.userEmailId
        obj["emailId"] = { "$in": el.contacts };

        if(dateMin && dateMax){
            obj["interactionDate"] = {$gte:new Date(moment().subtract(1, "month")),$lte:new Date(dateMax)}
        } else {
            obj["interactionDate"] = {$lte:new Date()}
        }

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $group:{
                _id:{ownerEmailId:"$ownerEmailId",emailId:"$emailId",interactionType:"$interactionType"},
                count:{"$sum":1}
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}

interactionsManagement.prototype.getTwoWayInteractionsCount = function (usersAndContacts,dateMin,dateMax,callback) {

    var queryFinal = { "$or": usersAndContacts.map(function(el) {
        var obj = {}
        obj["ownerEmailId"] = el.userEmailId
        obj["emailId"] = { "$in": el.oppOwner };

        if(el.oppCreatedDates.length>0){
            obj["interactionDate"] = {$gte:new Date(_.min(el.oppCreatedDates)),$lte:new Date()}
        } else {
            obj["interactionDate"] = {$lte:new Date()}
        }

        return obj;
    })};
    
    InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $group: {
                _id: {action:"$action",ownerEmailId:"$ownerEmailId",contact:"$emailId"},
                // connectedVia:{$addToSet:"$interactionType"},
                count: {
                    $sum: 1
                }
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}

interactionsManagement.prototype.getInteractionCountByGroupByUserContactsMeetings = function (usersAndContacts,dateMin,dateMax,callback) {

    var queryFinal = { "$or": usersAndContacts.map(function(el) {
        var obj = {}
        obj["ownerEmailId"] = el.userEmailId
        obj["emailId"] = { "$in": el.uniqDmsInfls };
        obj["interactionType"] = "meeting";

        if(dateMin && dateMax){
            obj["interactionDate"] = {$gte:new Date(dateMin),$lte:new Date(dateMax)}
        } else {
            obj["interactionDate"] = {$lte:new Date()}
        }

        return obj;
    })};
    
    InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $group:{
                _id:{ownerEmailId:"$ownerEmailId",emailId:"$emailId",interactionType:"$interactionType"},
                count:{"$sum":1}
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}

interactionsManagement.prototype.getInteractionCountByCompany = function (usersAndContacts,dateMin,dateMax,common,callback) {

    var queryFinal = { "$or": usersAndContacts.map(function(el) {
        var obj = {}
        obj["ownerEmailId"] = el.userEmailId;

        var companies = el.contacts.map(function (co) {

            var company = common.fetchCompanyFromEmail(co);

            if(company){
                return new RegExp('@'+company, "i")
            }

        })

        obj["emailId"] = { "$in": _.uniq(_.compact(companies)) };

        if(dateMin && dateMax){
            // obj["interactionDate"] = {$gte:new Date(dateMin),$lte:new Date(dateMax)}
            obj["interactionDate"] = {$gte:new Date(moment().subtract(1, "month")),$lte:new Date()}
        } else {
            obj["interactionDate"] = {$lte:new Date()}
        }

        return obj;
    })};
    
    InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $group:{
                _id:{ownerEmailId:"$ownerEmailId",emailId:"$emailId"},
                count:{"$sum":1}
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}

interactionsManagement.prototype.ignoreMail = function (userId,emailId,refId,callback) {

    InteractionsCollection.update({
        ownerId:userId,
        emailId:emailId,
        refId:refId
    },{
        $set:{
            notImportant:true
        }
    },function (err,results) {
        callback(err,results)
    })

};

interactionsManagement.prototype.getMlAnalyzedMails = function (userId,skip,limit,from,filter,callback) {

    InteractionsCollection.aggregate([
        {
            $match: buildQueryForInsightsMailsFilter(userId,from,filter)
        }
        ,{$sort:{interactionDate:-1}}
        // ,{ $skip: skip },
        // { $limit: limit }
    ]).exec(function(error, interactions){

        callback(error,interactions)
    })
}

interactionsManagement.prototype.getMlAnalyzedMailsCounter = function (userId,from,filter,callback) {

    InteractionsCollection.aggregate([
        {
            $match: buildQueryForInsightsMailsFilter(userId,from,filter)
        },
        {
            $group:{
                _id:null,
                threads:{
                    $addToSet:"$emailThreadId"
                }
            }
        },
        {
            $project:{
                count:{$size:"$threads"}
            }
        }
    ]).exec(function(error, interactions){
        callback(error,interactions)
    })
}

interactionsManagement.prototype.checkLiuRespondedToLastEmail = function (userId,from,emailThreadIds,callback) {

    InteractionsCollection.aggregate(
        [{
            $match: {
                ownerId:userId,
                userId:{$ne:userId},
                emailThreadId:{$in:emailThreadIds},
                interactionDate:{$gte:new Date(from),$lte:new Date()},
                interactionType:"email"
            }
        },
        {
            $group:{
                _id:"$emailThreadId",
                interactions:{
                    $push:{
                        ownerEmailId:"$ownerEmailId",
                        emailId: "$emailId",
                        interactionDate: "$interactionDate",
                        emailThreadId: "$emailThreadId",
                        action: "$action",
                        title: "$title",
                        cc:"$cc",
                        to:"$to"
                    }
                }
            }
        }
    ]).exec(function(error, interactions){
        if(!error && interactions){
            getLatestMailInThread(interactions,callback)
        }
    })
}

function getLatestMailInThread(interactions,callback) {

    var latestReplies = [];
    _.each(interactions,function (intThread) {
        intThread.interactions.sort(function (o2, o1) {
            return new Date(o1.interactionDate) > new Date(o2.interactionDate) ? 1 : new Date(o1.interactionDate) < new Date(o2.interactionDate) ? -1 : 0;
        });

        if(intThread.interactions[0] && (intThread.interactions[0].to && intThread.interactions[0].to.length == 1 || intThread.interactions[0].cc && intThread.interactions[0].cc.length == 1)){
            latestReplies = latestReplies.concat(intThread.interactions[0])
        } else {
        }
    })

    callback(null,latestReplies)

}

function buildQueryForInsightsMailsFilter(userId,from,filter){

    var match = {
        ownerId:userId,
        // userId:"sender",
        userId:{$ne:userId},
        interactionDate:{$gte:new Date(from),$lte:new Date()},
        $or:[
            {toCcBcc: 'me'},
            {eAction: 'important'},
            {importance: {'$gt': 0}},
            {$and:[{"trackInfo.trackResponse":true},{createdDate:{$lte:new Date(moment().subtract(7, "days").toDate())}}]}
        ],
        notImportant:{$ne:true}
    }

    if(filter){

        match = {
            ownerId:userId,
            // action:"sender",
            userId:{$ne:userId},
            interactionDate:{$gte:new Date(from),$lte:new Date()},
            notImportant:{$ne:true} // Mails that are not marked as 'Not Imp.' by the user.
        }

        if(filter == "important"){
            match["$or"] = [{eAction: 'important'},{importance: {'$gt': 0}}]
        }

        if(filter == "onlyToMe"){
            match["toCcBcc"] = "me"
        }

        if(filter == "followUp"){
            // match["trackInfo.trackResponse"] = true
            match["$and"] = [{"trackInfo.trackResponse":true},{createdDate:{$lte:new Date(moment().subtract(7, "days").toDate())}}]
        }

        if(filter == "Positive"){
            match["sentiment"] = "Positive"
        }

        if(filter == "Negative"){
            match["sentiment"] = "Negative"
        }
    }

    match["hasUnsubscribe"] = {$ne:true};

    return match;

}

interactionsManagement.prototype.getNumberOfImportantMails = function (userId,from,callback) {

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerId:userId,
                userId:{$ne: userId},
                interactionDate:{$gte:new Date(from),$lte:new Date()},
                $or:[
                    {toCcBcc: 'me'},
                    {eAction: 'important'},
                    {importance: {'$gt': 0}},
                    // {"trackInfo.trackResponse":true}
                    {$and:[{"trackInfo.trackResponse":true},{createdDate:{$lte:new Date(moment().subtract(7, "days").toDate())}}]}
                ],
                notImportant:{$ne:true},
                hasUnsubscribe:{$ne:true}
            }
        },
        {
            $project:{
                importance:1,
                toCcBcc:1,
                sentiment:1,
                trackInfo:1,
                interactionDate:1,
                emailThreadId:1,
                title:1,
                emailContentId:1,
                action:1
            }
        }
    ]).exec(function(error, interactions){
        callback(error,interactions)
    })
}

interactionsManagement.prototype.getUsersAccountInteractionGrowth = function (userIds,date,callback) {

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerId:{$in:userIds},
                userId:{$nin:userIds},
                interactionDate:{$gte: new Date(date)},
                emailId:{$nin:[/@gmail/,/@hotmail/,/@outlook/,/@yahoo/,/@live/,/@aol/,/@rediffmail/]}
            }
        },
        {
          $sort:{interactionDate:1}
        },
        {
            $group: {
                _id:{emailId:'$ownerEmailId',account:"$emailId"},
                addedDate:{$first:"$interactionDate"}
            }
        },
        {
            $project:{
                account:"$_id.account",
                addedDate:1,
                emailId:"$_id"
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}

interactionsManagement.prototype.getInteractionsForUsersByDateRange = function(userIds,dateMin,dateMax,liuDomain,callback){

    if(new Date(dateMax)> new Date()){
        dateMax = new Date();
    }

    var query = { "$or": userIds.map(function(el) {
        var obj = {};
        
        obj["ownerId"] = el
        obj["userId"] = {$ne:el};

        if(liuDomain && liuDomain != "others" && liuDomain != "Others"){
            var emailIds = [new RegExp('@'+liuDomain, "i")];
            obj["emailId"] = {"$nin":emailIds}
        }

        obj["interactionDate"] = {
            $gte: new Date(dateMin),
            $lte: new Date(dateMax)
        }
        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id:{userEmailId:'$ownerEmailId',userId:"$ownerId"},
                count:{$sum:1 }
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}

interactionsManagement.prototype.getOneWayInteractionsForUsersByDateRange = function(userIds,dateMin,dateMax,liuDomain,callback){

    if(new Date(dateMax)> new Date()){
        dateMax = new Date();
    }

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el
        // obj["userId"] = {$ne:el};
        obj["action"] = "receiver";

        if(liuDomain && liuDomain != "others" && liuDomain != "Others"){
            var emailIds = [new RegExp('@'+liuDomain, "i")];
            obj["emailId"] = {"$nin":emailIds}
        }

        obj["interactionDate"] = {
            $gte: new Date(dateMin),
            $lte: new Date(dateMax)
        }

        obj["emailId"] = {$nin:[false,null,""," "]};
        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id:{userEmailId:'$ownerEmailId',userId:"$ownerId",emailId:"$emailId"},
                count:{$sum:1 }
            }
        }
    ]).exec(function (err,results) {

        var withoutLiuInt = [];

        if(!err && results && results.length){
            _.each(results,function (el) {
                if(el._id.userEmailId != el._id.emailId){
                    withoutLiuInt.push(el)
                }
            })
        }

        var sumUp = _
            .chain(withoutLiuInt)
            .groupBy(function (v) {
                var key = v._id.userEmailId+""+v._id.userId;
                return key;
            })
            .map(function(values, key) {

                var obj = {
                    _id:{
                        userEmailId:"",
                        userId:""
                    },
                    count:0
                }
                _.each(values,function (val) {

                    obj._id.userEmailId = val._id.userEmailId
                    obj._id.userId = val._id.userId
                    obj.count = obj.count+val.count;
                });

                return obj;
            })
            .value();

        callback(err,sumUp)
    });
}

interactionsManagement.prototype.interactionsByUsersAndContacts = function(userIds,contacts,dateMin,dateMax,liuDomain,callback){

    if(new Date(dateMax)> new Date()){
        dateMax = new Date();
    }

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el
        obj["emailId"] = { "$in": contacts };

        if(liuDomain && liuDomain != "others" && liuDomain != "Others"){
            var emailIds = [new RegExp('@'+liuDomain, "i")];
            obj["$and"] = [{emailId:{$in:contacts}},{emailId:{$nin:emailIds}}]
        }

        obj["userId"] = {$ne:el}

        obj["interactionDate"] = {
            $gte: new Date(dateMin),
            $lte: new Date()
        }

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id:{userId:'$ownerId',userEmailId:'$ownerEmailId',emailId:"$emailId"},
                count:{$sum:1 }
            }
        }
    ]).exec(function (err,results) {
        var withoutLiuInt = []

        if(!err && results && results.length){
            _.each(results,function (el) {
                if(el._id.userEmailId != el._id.emailId){
                    withoutLiuInt.push(el)
                }
            })
        }

        callback(err,withoutLiuInt)
    });
}

interactionsManagement.prototype.oneWayInteractionsByUsersAndContacts = function(userIds,contacts,dateMin,dateMax,liuDomain,callback){

    if(new Date(dateMax)> new Date()){
        dateMax = new Date();
    }

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = castToObjectId(el)
        obj["emailId"] = { "$in": contacts };
        obj["action"] = "receiver";

        if(liuDomain && liuDomain != "others" && liuDomain != "Others"){
            var emailIds = [new RegExp('@'+liuDomain, "i")];
            obj["$and"] = [{emailId:{$in:contacts}},{emailId:{$nin:emailIds}}]
        }

        obj["interactionDate"] = {
            $gte: new Date(dateMin),
            $lte: new Date(dateMax)
        }

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id:{userId:'$ownerId',userEmailId:'$ownerEmailId',emailId:"$emailId"},
                count:{$sum:1 }
            }
        }
    ]).exec(function (err,results) {
        var withoutLiuInt = [];
        
        if(!err && results && results.length){
            _.each(results,function (el) {
                if(el._id.userEmailId != el._id.emailId){
                    withoutLiuInt.push(el)
                }
            })
        }

        callback(err,withoutLiuInt)
    });
}

interactionsManagement.prototype.interactionsInitForAccount = function (userIds,account,from,to,contacts,serviceLogin,companyId,callback) {

    var match = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        // obj["companyId"] = companyId;
        obj["userId"] = {$ne: el}
        obj["$or"] = [
            {"emailId":{$in:contacts}},
            // {"emailId":new RegExp('@'+account, "i")}
        ]

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        match = {
            companyId:companyId,
            userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(from && to){
            match["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                match["source"] = {$ne:"google"};
            } else {
                match["source"] = {$ne:"outlook"};
            }
        }
    }

    InteractionsCollection.aggregate([
        {
            $match:match
        },
        {
            $group: {
                _id: "$action",
                connectedVia:{$addToSet:"$interactionType"},
                count: {
                    $sum: 1
                }
            }
        }
    ]).exec(function(err,interactions){

        callback(err,interactions)
    });
}

interactionsManagement.prototype.lastInteractionDateForUsersContacts = function (companyId,userEmailIds,contacts,callback) {

    newInteractionsCollection.aggregate([
        {
            $match:{
                companyId:companyId,
                ownerEmailId:{$in:userEmailIds},
                emailId:{$in:contacts},
                interactionDate:{$lte: new Date()}
            }
        },
        {
            $sort:{
                interactionDate:-1
            }
        },
        {
            $group: {
                _id: {owner:"$ownerEmailId",emailId:"$emailId"},
                lastInteractedDate:{$max:"$interactionDate"}
            }
        }
    ]).exec(function(err,interactions){
        callback(err,interactions)
    });
}

interactionsManagement.prototype.interactionsInitForAccount_new = function (userIds,account,from,to,contacts,serviceLogin,companyId,callback) {

    var match = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        // obj["companyId"] = companyId;
        obj["userId"] = {$ne: el}
        obj["$or"] = [
            {"emailId":{$in:contacts}},
            // {"emailId":new RegExp('@'+account, "i")}
        ]

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        match = {
            companyId:companyId,
            // userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(from && to){
            match["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                match["source"] = {$ne:"google"};
            } else {
                match["source"] = {$ne:"outlook"};
            }
        }
    }

    newInteractionsCollection.aggregate([
        {
            $match:match
        },
        {
            $group: {
                _id:"$refId",
                action: { "$first": "$action" },
                interactionType: { "$first": "$interactionType" },
            }
        },
        {
            $group: {
                _id: "$action",
                connectedVia:{$addToSet:"$interactionType"},
                count: {
                    $sum: 1
                }
            }
        }
    ]).exec(function(err,interactions){

        callback(err,interactions)
    });
}

interactionsManagement.prototype.interactionTypesForAccount = function (userIds,account,from,to,contacts,serviceLogin,companyId,callback) {

    var match = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne: el}

        obj["$or"] = [
            {"emailId":{$in:contacts}},
            // {"emailId":new RegExp('@'+account, "i")}
        ]

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }
        
        return obj;
    })};
    
    if(companyId){
        match = {
            companyId:companyId,
            userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(from && to){
            match["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                match["source"] = {$ne:"google"};
            } else {
                match["source"] = {$ne:"outlook"};
            }
        }
    }

    InteractionsCollection.aggregate([
        {
            $match:match
        },
        {
            $group:{
                _id:"$interactionType",
                count:{$sum:1}
            }
        },
        {
            $group:{
                _id:null,
                totalCount:{$sum:"$count"},
                maxCount:{$max:"$count"},
                typeCounts:{$push:{_id:"$_id",count:"$count"}}
            }
        }
    ]).exec(function(err,interactions){
       callback(err,interactions)
    });
}

interactionsManagement.prototype.interactionTypesForAccount_new = function (userIds,account,from,to,contacts,serviceLogin,companyId,callback) {

    var match = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne: el}

        obj["$or"] = [
            {"emailId":{$in:contacts}},
            // {"emailId":new RegExp('@'+account, "i")}
        ]

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        match = {
            companyId:companyId,
            // userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(from && to){
            match["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                match["source"] = {$ne:"google"};
            } else {
                match["source"] = {$ne:"outlook"};
            }
        }
    }

    newInteractionsCollection.aggregate([
        {
            $match:match
        },
        {
            $group: {
                _id:"$refId",
                interactionType: { "$first": "$interactionType" },
            }
        },
        {
            $group:{
                _id:"$interactionType",
                count:{$sum:1}
            }
        },
        {
            $group:{
                _id:null,
                totalCount:{$sum:"$count"},
                maxCount:{$max:"$count"},
                typeCounts:{$push:{_id:"$_id",count:"$count"}}
            }
        }
    ]).exec(function(err,interactions){
       callback(err,interactions)
    });
}

function checkArray(value) {
    return Array.isArray(value);
}

function fetchCompanyFromEmail(email){

    if(checkRequired(email)){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            word = word.toLowerCase();
            return removableTextList.indexOf(word.trim()) > -1
        })
        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
    } else {
        return null
    }
}

// export the User interactions Class
module.exports = interactionsManagement;
