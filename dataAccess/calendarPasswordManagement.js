
var calendarPasswordManagementCollection = require('../databaseSchema/userManagementSchema').calendarPasswordManagement;

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston()

function CalendarPasswordManagement()
{

}

function constructCalendarPasswordManagementObject(reqObj){
    var date = new Date();
    var calendarPasswordManagementObject = new calendarPasswordManagementCollection({
        readStatus:false,
        requestDate:date,
        requestFrom:{
            userId:reqObj.requestFrom.userId,
            emailId:reqObj.requestFrom.emailId
        },
        requestTo:{
            userId:reqObj.requestTo.userId,
            emailId:reqObj.requestTo.emailId
        }

    });
    return calendarPasswordManagementObject;
}

CalendarPasswordManagement.prototype.saveRequest = function(reqObj,callback){

   var calendarPasswordManagementObject = constructCalendarPasswordManagementObject(reqObj);
    calendarPasswordManagementObject.save(function(err,calendarPasswordReq){
        if(err){
            logger.info("mongo error in saving calendar password request "+err);
            callback(err,'error',null);
        }
        else{

            callback(err,'success',calendarPasswordReq);

        }
    })
}

CalendarPasswordManagement.prototype.getReceivedCalendarPasswordRequests = function(userId,callback){
    calendarPasswordManagementCollection.find({"requestTo.userId":userId,readStatus:false},function(error,calendarPasswordReq){
        if (error) {
            logger.info("Mongo error in searching received calendar password requests " + error);
            callback(error,null);
        }
        else{
            callback(error,calendarPasswordReq);
        }
    })
}


CalendarPasswordManagement.prototype.getRequestedCalendarRequests = function(userId,callback){
    calendarPasswordManagementCollection.find({"requestFrom.userId":userId},function(error,calendarPasswordReq){
        if (error) {
            logger.info("Mongo error in searching requested calendar password requests " + error);
            callback(error,null);
        }
        else{
            callback(error,calendarPasswordReq);
        }
    })
};

CalendarPasswordManagement.prototype.updateCalendarRequest = function(resObj,callback){
    var date = new Date();
    calendarPasswordManagementCollection.update({_id:resObj.requestId},{readStatus:true,isAccepted:resObj.accepted,actionDate:date},function(error,result){
        if (error) {
            callback(error,false);
        }
        else if (result && result.ok) {
            callback(error,true);
        }
        else{
            callback(error,false);
        }
    })
};

CalendarPasswordManagement.prototype.removeUserRequests = function(userId,callback){
    calendarPasswordManagementCollection.remove({"requestFrom.userId":userId},function(error,result){

        if(error){
            callback(false,'onDeleteCalPassReq',error,result);
        }
        else callback(true,'onDeleteCalPassReq',error,result);
    })
};

function checkRequired(data){
    if(data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

// export the User EventDataAccess Class
module.exports = CalendarPasswordManagement;