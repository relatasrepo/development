
var relationshipStrengthCollection = require('../databaseSchema/userManagementSchema').relationshipStrength;
function RelationshipStrength(){

    this.getRelationshipStrengthDoc = function(callback){
        relationshipStrengthCollection.findOne({},function(error,doc){
            if(error){
                callback(error,doc)
            }
            else if(doc){
                callback(error,doc)
            }
            else createRelationshipStrengthDoc(callback)
        })
    };

    this.updateRelationshipStrengthDoc = function(docId,update,callback){
        relationshipStrengthCollection.update({_id:docId},{$set:update},function(error,result){
            if(error){
                callback(false)
            }
            else if(result && result.ok){
                callback(true)
            }else callback(false)
        })
    }
}

function createRelationshipStrengthDoc(callback){
    var doc = new relationshipStrengthCollection({
        minRatio:50,
        maxRatio:99
    });
    doc.save(function(error,doc){
       callback(error,doc)
    })
}

module.exports = RelationshipStrength;