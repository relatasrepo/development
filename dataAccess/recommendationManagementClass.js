var recommendationCollection = require('../databaseSchema/recommendationSchema').rrsProducts;
var winstonLog = require('../common/winstonLog');
var _ = require("lodash");
var logLib = new winstonLog();

function RecommendationManagement(){

    this.getRecommendations = function(emailId,callback){
        recommendationCollection.find({recommendedUser:emailId,converted:{$ne:true},ignore:{$ne:true}}, function(err, recommendations){
            if(err) {
                console.log("Error in retrieving recommendations");
            }
            callback(err, recommendations);
        })
    };

    this.getAllMLAccountInsights = function (callback) {
        
        var match = {
        }

        var projectQuery = {
            _id:0,
            'Company Name':"$companyName",
            'Account Name':"$accountDomainName",
            'User Email':"$rlUserEmailId",
            'Insights Date':"$mlInsightsDate",
            'Interactions Count':"$interactionsCount",
            'Opps Won':"$oppsWonCount",
            'Opps Lost Count':"$oppsLostCount",
            'Opps Pipeline':"$oppsPipeLineCount",
            'Opps Total':"$oppsTotalCount",
            'Recommended Product':"$recommendedProduct",
            'Recommended User':"$recommendedUser",
            'Recommended Amount':"$recommendedAmount",
            'Positive Sentiments':"$oppsWonCount",
            'Signed Documents Sent':"$nSignedDocumentsSent",
            'Important Documents Requested':"$nImportantDocumentsRequested",
            'Unanswered Questions':"$nQuestionAskedAndNotAnswered"

        };

        recommendationCollection.aggregate([
            {
                $match:match
            },
            {
                $project:projectQuery
            }
        ],function(error, result){
            if(error){
                loggerError.info('Error in getAllMLAccountInsights():opportunitiesManagement ',error)
                callback(error,null);
            }
            else{
                callback(null,result);
            }
        });
    };

}

RecommendationManagement.prototype.ignoreRecommendation = function(emailId,id,callback){
    recommendationCollection.update({recommendedUser:emailId,_id:id},{$set:{ignore:true}}, function(err, results){
        callback(err,results)
    });
}

RecommendationManagement.prototype.convertRecommendationToOpp = function(emailId,id,callback){
    recommendationCollection.update({recommendedUser:emailId,_id:id},{$set:{converted:true}}, function(err, results){
        callback(err,results)
    });
}

module.exports = RecommendationManagement;