db.auth("ozymandias", "1pe07is0289972")
db.portfolioAccess.createIndex( { companyId: 1,type:1,name:1 }, { background: true } );
db.oppCommits.find({commitFor:"week"},{userEmailId:1}).count()
db.opportunities.find({userEmailId:/ivalue/,opportunityName:/Informatica_Arcserv/}).count()

db.opportunitiesTarget.find({salesTarget:{ $elemMatch: { date: {$lte:ISODate("2018-06-31T05:03:28.902Z")} }}}).count()
db.opportunitiesTarget.find({userId:ObjectId("5c0cca380cef763472f02b64")},{salesTarget:1,userEmailId:1}).pretty()

db.contacts.find({ownerEmailId:"anshil.nigam@merittrac.com",personEmailId:{$in:[null,""," "]}}).count()
db.user.update({'emailId': 'jimmytestacc@gmail.com'}, {$pull: {'contacts': {personEmailId: { $in: [null,""," "]}}}})


db.user.update({emailId:"ashish.sharma@e2enetworks.com"},{$set: {emailId:null}})


db.company.update({"_id" : ObjectId("5cad97990cf02535a641b4e1")},{$set:{"logo.url":"https://s3.amazonaws.com/relatas-live-company-logos/e2e.png"}})

db.user.aggregate([
    {
        $match: {
            emailId: "sudip@relatas.com"
        }
    },{
        $unwind: "$contacts"
    }, {
        $match: {"contacts.personEmailId":{$exists:true}}
    },{
        $project:{
            _id:"$contacts._id",
            "personEmailId":"$contacts.personEmailId",
            "personName":"$contacts.personName",
        }
    },{
        $out:"sudipContacts"
    }
])


db.document.find({documentCreatedBy:/meritt/}).count()

db.opportunities.find({userEmailId:"vinod.viswanathan@merittrac.com",createdDate:{$gte:ISODate("2019-03-17T05:03:28.902Z")}}).pretty()


//56,21 docs
db.losingTouch.find({userEmailId:"anshil.nigam@merittrac.com"}).pretty()
db.opportunities.find({userEmailId:/merittrac.com/,opportunityName:/LVB/}).count()

db.user.find({emailId:/relatas/},{lastMobileSyncDate:1}).pretty()

db.interactionsV2.find({"ownerEmailId": 'sudip@relatas.com',"mobileNumber":/5104589555/},{ownerEmailId:1,emailId:1}).count()

db.interactions.find({"ownerEmailId": /22by7/,"emailId":/abb/}).count()


db.insightsAdminReports.find({"dayString": "21112018","companyId": ObjectId("5a33a7f524493342232e24de"),"forTeam": false,"pipelineVelocity.shortfall":{$gte:1}},{dealsAtRisk:1,losingTouch:1,"pipelineVelocity.shortfall":1,ownerEmailId:1}).pretty()
db.insightsAdminReports.find({ownerEmailId:"siva@22by7.in","companyId": ObjectId("5a33a7f524493342232e24de"),"forTeam": false,"pipelineVelocity.shortfall":{$gte:1}}).pretty()

db.user.update({},{$set:{companyId:ObjectId("5c8227ad765a2420f218672c"),corporateUser:true,corporateAdmin:true}})

db.oppCommits.find({
    userEmailId:/22by/i,
},{opportunities:0}).sort({date: -1}).pretty()


db.opportunities.find({opportunityId: "5b3524c362905d6742e6cbd4"})
db.userlog.find({emailId:/@relatas/,loginDate:{$gte:ISODate("2019-03-02T04:49:24.635Z")}},{loginDate:1,emailId:1,loginPage:1}).pretty()

db.interactionsV2.find({"ownerEmailId": "taran.raina@ivalue.co.in","emailId":"anushaa-r@lntecc.com"}).count()
db.interactions.find({"ownerEmailId": "taran.raina@ivalue.co.in","emailId":"anushaa-r@lntecc.com"}).count()

db.opportunities.find({userEmailId:/sudip@relatas/i,relatasStage:"Close Won"},{interactionCount:1,createdDate:1,closeDate:1,amount:1,opportunityName:1}).pretty()

db.opportunities.aggregate([{$match: {}},{$out: "interactionsJune"}])
db.opportunities.aggregate([{$match: {userEmailId:/ivalue/i}},{$out: "interactionsJune"}])
mongoexport -d Relatas -u ozymandias -p 1pe07is0289972 -c contacts --type=csv --fields personEmailId,fullName -q '{ownerEmailId: "sudip@relatas.com"}' --out /home/ubuntu/exports/sudipContacts.csv


db.opportunities.find({userEmailId:"bharat.barhate@ivalue.co.in",closeDate:{$gte:ISODate("2018-09-25T05:03:28.902Z"),$lte:ISODate("2018-09-31T05:03:28.902Z")}}).pretty();


db.opportunities.find({netGrossMargin:0},{userEmailId:1}).pretty()

db.user.find({emailId:/Bharat/i},{emailId:1}).pretty()


db.interactions.find({ownerEmailId:"anshul.kawatra@ivalue.co.in",emailId:/swapnil/i}).count()

db.opportunities.find({"userEmailId" : /22by7/i,companyId:{$ne:ObjectId("5a33a7f524493342232e24de")}},{userEmailId:1,companyId:1,sourceOpportunityId:1}).count();

db.opportunities.find({"userEmailId" : /22by7/i,sourceOpportunityId:{$ne:null},"companyId" : ObjectId("5a33a7f524493342232e24de")}).count()
db.opportunities.find({companyId:"54eabe4929b9913521cefb64"},{userEmailId:1,companyId:1,sourceOpportunityId:1}).pretty()

db.opportunities.find({"companyId" : ObjectId("5a33a7f524493342232e24de"),netGrossMargin:11.66}).count()
db.opportunities.find({"companyId" : ObjectId("5a33a7f524493342232e24de"),userEmailId:/arun@22by7/i,"contactEmailId" : "venkatesh_r@aurigene.com"},{opportunityName:1,netGrossMargin:1,amount:1,closeDate:1}).pretty()

db.oppCommits.update({userEmailId:/@ivalue.co/i,"opportunities.stageName": "Proposal"}, { "$set": { "opportunities.$.stageName": "Commit","opportunities.$.relatasStage": "Commit" }},{multi:true})
db.oppCommits.find({userEmailId:/@ivalue.co/i}).count()




db.accounts.find({target:{ $elemMatch: { amount: {$gt:0} }}}).count()

db.opportunities.aggregate([
    {
        $match:{
            companyId:ObjectId("58382e54592160246f93e1f4")
        }
    },
    {
        $group: {
            _id: "$geoLocation.zone",
            count: { $sum: 1 }
        }
    }
]).pretty()


db.opportunities.find({"companyId" : ObjectId("58382e54592160246f93e1f4"),"geoLocation.zone":/one/i},{userEmailId:1,"geoLocation.zone":1}).pretty()

db.interactionsV2.find({
        "ownerEmailId": "anshil.nigam@merittrac.com",
    interactionType:"meeting"
    }
).sort({interactionDate:-1}).pretty()


db.user.find({_id:ObjectId("584a4863aa918867077eea32")},{emailId:1}).pretty();

db.user.find({hierarchyParent: { $regex: "5a8c25d68cf056754e5b3891", $options: 'i' }},{emailId:1,hierarchyPath:1,hierarchyParent:1}).count()
db.user.find({emailId:/Jitender.sharma@ivalue.co.in/i},{hierarchyPath:1,hierarchyParent:1}).pretty()
db.user.find({emailId:{$in:["santosh.sharma@ivalue.co.in","adarsh.sinha@ivalue.co.in","navnit.mishra@ivalue.co.in"]}},{hierarchyPath:1,hierarchyParent:1}).pretty()


db.user.find({hierarchyPath: { $regex: "584671e51ebb62226bc93071", $options: 'i' }},{ownerEmailId:1,hierarchyPath:1,hierarchyParent:1}).pretty()

db.revenueHierarchy.find({ownerEmailId:/ranvijay@ivalue.co.in/},{ownerEmailId:1,hierarchyPath:1,hierarchyParent:1}).pretty()
db.revenueHierarchy.find({hierarchyParent:"584a4092aa918867077ddc9f"},{ownerEmailId:1,hierarchyPath:1,hierarchyParent:1}).pretty()

db.revenueHierarchy.update({hierarchyParent:"584a4092aa918867077ddc9f"},{$set:{hierarchyPath:",5937e8d6d08a0f3b30af03a9,588c74b801a40775293bdc52,591aca9e3a1503aa40450e37,588efc0c5a696c34434e4f45,584a4092aa918867077ddc9f"}})

db.user.find({hierarchyPath: /584a4863aa918867077eea32/i },{emailId:1,hierarchyPath:1,hierarchyParent:1}).pretty()
db.user.find({hierarchyParent: ObjectId("58f9b7c5e7bfc4e064cb7d48") },{emailId:1,hierarchyPath:1,hierarchyParent:1}).pretty()

db.revenueHierarchy.find({companyId:ObjectId("58382e54592160246f93e1f4"),
    userId:{$in:[ObjectId("5937e8d6d08a0f3b30af03a9"),
            ObjectId("588c74b801a40775293bdc52"),
            ObjectId("588efc0c5a696c34434e4f45")]}}).pretty()


db.revenueHierarchy.update({companyId:ObjectId("58382e54592160246f93e1f4"), hierarchyParent:"584ea0467bab886134a9d9c7"}
,{$set:{hierarchyPath:",5937e8d6d08a0f3b30af03a9,588c74b801a40775293bdc52,591aca9e3a1503aa40450e37,588efc0c5a696c34434e4f45,584ea0467bab886134a9d9c7"}},{multi:true})

db.revenueHierarchy.find({companyId:ObjectId("58382e54592160246f93e1f4"), hierarchyParent:"584ea0467bab886134a9d9c7"}).pretty()

//Retail to corporate user Manipal


db.user.find({$and:[{emailId:{$ne:"ashish.sharma@e2enetworks.com"}},{emailId:/e2enetworks.com/}],companyId:null},{emailId:1}).pretty()

db.user.update({emailId:/e2enetworks.com/,companyId:null},{$set:{"companyId" : ObjectId("5cad97990cf02535a641b4e1"),corporateUser:true}},{multi:true})

db.user.update({emailId:/e2enetworks.com/,"companyId" : ObjectId("5cad97990cf02535a641b4e1")},{$set:{hierarchyParent:null,hierarchyPath:null,orgHead:false}},{multi:true})

db.company.update(
    {"_id": ObjectId("5abfd889da93582853628deb")},
    {
        $set: {
            "logo.url": "https://s3.amazonaws.com/relatas-dev-company-logos/anomkin.png"
        },
    }
)

db.user.aggregate({
    $match:{
        emailId:/22by7/i
    }
},{
    $group:{
        _id:null,
        ids:{$push:"_id"}
    }
}).pretty();

db.auth("ozymandias", "1pe07is0289972")


db.oppMetaData.find({userEmailId:/sudip@relatas/i},{data:0}).pretty();

db.oppMetaData.find({userEmailId:/anomkin/i},{data:0}).pretty();
db.oppMetaData.find({userEmailId:/venkat@22by7/i,month:3},{data:0}).pretty();

db.oppMetaData.find({userEmailId:/22by7/i,month:3,'data.0': {$exists: true}},{userEmailId:1,data:1}).pretty()
db.oppMetaData.find({userEmailId:/22by7/i,month:3,'data.0': {$exists: true}},{userEmailId:1,data:1}).count()

db.opportunities.find({closeDate:{$lte:ISODate("1971-04-01T05:03:28.902Z")}}).count();
db.opportunities.find({sourceType:/renewal/i,type:"New"},{type:1}).count();

mongodump -d db_name --collection 22byOppMeta

mongorestore -d Relatas -u ozymandias -p 1pe07is0289972 -c OppMetaCopy /dump/db_name/oppMetaData.bson

// 1. Delete oppmeta data for april month
// 2. db.oppMetaData.find({userEmailId:/22by7/i,month:3})

db.OppMetaCopy.find({userEmailId:/22by7/i,month:3}).forEach(function (doc) {
    db.oppMetaData.insert(doc);
});


db.user.find({emailId:"nitin.kapoor@ivalue.co.in"},{contacts:0}).pretty()

db.opportunities.find({
    userEmailId:"bharat.barhate@ivalue.co.in",
    productType:{$in:["Forcepoint","Check Point"]},
    closeDate:{$gte:ISODate("2018-09-25T05:03:28.902Z"),
        $lte:ISODate("2018-09-31T05:03:28.902Z")
    }},{opportunityName:1,productType:1,opportunityId:1}).pretty();

sudo scp -i /home/naveen/keys/showcase.pem ubuntu@35.194.89.210:opp_loc.xlsx data_dump

db.user.update({emailId:{$in:[]}}, {$set: {profileDeactivated:true}},{multi:true})
db.user.find({emailId:{$in:[]}}).count()

// 4883

db.opportunities.find({"companyId" : ObjectId("58382e54592160246f93e1f4"),userEmailId:{$nin:["",null," "]}}).count()

var copy = db.opportunities.findOne({"companyId" : ObjectId("58382e54592160246f93e1f4")},{_id:0});
for (var i = 0; i< 3000; i++){
    delete copy._id;
    delete copy.opportunityId;
    db.opportunities.insert(copy);
}


