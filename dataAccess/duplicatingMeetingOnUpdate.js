/**
 * Created by elavarasan on 27/10/15.
 */
var events=require('../databaseSchema/calendarSchema');
var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger = logLib.getWinston();

var duplicationFunction=function(){};
duplicationFunction.prototype.duplicateData=function(invitationId,data,callback){
    events.findOneAndUpdate({invitationId:invitationId},{$set:data},function(err,event){
        if(!err){
            logger.info('data duplicated')
        }else{
            logger.info(event);
        }
    });
};
module.exports=duplicationFunction;