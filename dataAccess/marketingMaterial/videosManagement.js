
var mmCollection = require('../../databaseSchema/marketingMaterialSchema').marketingMaterial;

var winstonLog = require('../../common/winstonLog');
var logLib = new winstonLog();
var _ = require("lodash");

function MarketingMaterialManagement(){}

MarketingMaterialManagement.prototype.getVideos = function (type,callback) {
    mmCollection.find({entityType:type},function (err,result) {
        callback(err,result)
    })
}

MarketingMaterialManagement.prototype.updateVideos = function (url,callback) {
    mmCollection.update({entityType:"videos"},{ $push: { urls: url } },{ upsert: true },function (err,result) {
        callback(err,result)
    })
}

MarketingMaterialManagement.prototype.deleteVideos = function (url,callback) {
    mmCollection.update({entityType:"videos"},{ $pull: { 'urls': {url:url} } },function (err,result) {
        callback(err,result)
    })
}

MarketingMaterialManagement.prototype.updateDefaultVideos = function (url,callback) {
    mmCollection.update({entityType:"videos",urls: { $elemMatch: { isDefault: true }}}, {$set: { "urls.$.isDefault": false }},function (err,result) {
        mmCollection.update({entityType:"videos",urls: { $elemMatch: { url: url }}}, {$set: { "urls.$.isDefault": true }},function (err,result) {
            callback(err,result)
        })
    })
}

MarketingMaterialManagement.prototype.setOrder = function (url,order,callback) {
    mmCollection.update({entityType:"videos",urls: { $elemMatch: { url: url }}}, {$set: { "urls.$.order": order }},function (err,result) {
        callback(err,result)
    })
}

module.exports = MarketingMaterialManagement;