var userLogCollection = require('../databaseSchema/userManagementSchema').userlog;

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston();

function UserLog()
{

    this.insertLog = function(logData){
       var log = createLog(logData);
       log.save(function(error,savedLog){

       })
    };

    this.removeUserLogs = function(userId,callback){
         userLogCollection.remove({userId:userId},function(error,result){
             if(error){
                 callback(false,'onDeleteLogs',error,result);
             }
             else callback(true,'onDeleteLogs',error,result);
         })
    }
}

function createLog(logData){
    var date = new Date();
    var log = new userLogCollection({
        userId: logData.userId,
        emailId:logData.emailId,
        loginDate:date,
        loginPage:logData.loginPage,
        userIp:logData.userIp,
        userAgent:logData.userAgent,
        adminActionTaken:null
    });
    return log;
}

function checkRequired(data){
    if(data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

// export the User EventDataAccess Class
module.exports = UserLog;