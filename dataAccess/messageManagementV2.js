/**
 * Created by naveen on 5/12/16.
 */

var messagesCollection = require('../databaseSchema/messageSchema').messages;

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getInteractionsErrorLogger();
var _ = require("lodash")

function MessageManagement(){};

MessageManagement.prototype.createMessage = function (message,callback) {
    messagesCollection.collection.insert(message,function (err,result) {
        callback(err,result)
    });
};

MessageManagement.prototype.getConversationByReferenceId = function (userId,messageReferenceId,messageReferenceType,callback) {
    messagesCollection.find({"messageOwner.userId":userId,messageReferenceId:messageReferenceId,messageReferenceType:messageReferenceType},function (err,result) {
        callback(err,result);
    });
};

MessageManagement.prototype.getConversationByConversationId = function (userId,conversationId,messageReferenceType,callback) {

    var query = {
        conversationId:conversationId
    }

    if(userId){
        query["messageOwner.userId"]= userId
    }

    if(messageReferenceType){
        query["messageReferenceType"]= messageReferenceType
    }

    messagesCollection.aggregate([
        {
            $match:query
        }
    ]).exec(function (err,mesages) {
        callback(err,mesages);
    })

};

MessageManagement.prototype.getConversationByEmailId = function (userId,emailId,messageReferenceType,callback) {
    messagesCollection.find({"messageOwner.userId":userId,emailId:emailId,messageReferenceType:messageReferenceType},function (err,result) {
        callback(err,result);
    });
};

MessageManagement.prototype.getNotesForContact = function (userId,emailId,callback) {

    if(userId && emailId){
        messagesCollection.find({"messageOwner.userId":userId,emailId:emailId},function (err,result) {
            callback(err,result);
        });
    } else {
        callback(null,[])
    }
};

MessageManagement.prototype.getNotesForAccount = function (userIds,from,to,account,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["messageOwner.userId"] = el;
        obj["emailId"] = new RegExp('@'+account, "i")
        obj["date"] = {
            $gte: new Date(from),
            $lte: new Date(to)
        }
        return obj;
    })};

    messagesCollection.find(query,function (err,result) {
        callback(err,result);
    });
};

module.exports = MessageManagement;