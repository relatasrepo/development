/**
 * Created by naveen on 13/10/17.
 */
var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var opportunitiesCollection = require('../databaseSchema/userManagementSchema').opportunities;
var oppCommitCollection = require('../databaseSchema/oppCommitSchema').oppCommits
var winstonLog = require('../common/winstonLog');

var mongoose = require('mongoose');
var moment = require('moment-timezone');

var _ = require("lodash");

var logLib = new winstonLog();
var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();

function OppCommitManagement() {

}

OppCommitManagement.prototype.updateCommitWeekly = function (userId,weekYear,commitStage,commitValues,qStart,qEnd,timezone,commitDate,callback) {

    getOppByStageSnapShot(userId,qStart,qEnd,commitStage,function (err,opps) {

        _.each(opps,function (op) {

            if(op.opps && op.opps.length>0){
                _.each(op.opps,function (el) {
                    if(!el.createdDate){
                        el.createdDate = el._id.getTimestamp()
                    }
                })
            }
        });

        var data = {
            opportunities: opps,
            week: {
                userCommitAmount: commitValues.weekRelatasCommitAmount,
                relatasCommitAmount: 0
            },
            month: {
                userCommitAmount: 0,
                relatasCommitAmount: 0
            },
            quarter: {
                userCommitAmount: 0,
                relatasCommitAmount: 0
            },
            commitForDate:commitDate,
            commitFor:"week"
        }

        var findQuery = {
            userId:userId,
            commitWeekYear:weekYear
        };

        oppCommitCollection.findOne(findQuery,{_id:1,userId:1},function (err,existingCommit) {

            if(!err && existingCommit){
                oppCommitCollection.collection.update(findQuery,{$set:data},function (err,result) {
                    callback(err,opps)
                })
            } else {
                insertCommit(userId,weekYear,data,callback)
            }
        })
    })
}

OppCommitManagement.prototype.updateCommitMonth = function (userId,userEmailId,monthYear,commitStage,commitValue,startOfMonth,endOfMonth,timezone,commitDate,netGrossMarginReq,calculateNGM,callback) {

    getOppByInCommitStageByDateRange(userId,commitStage,null,null,netGrossMarginReq,calculateNGM,function (err,opps) {

        var data = {
            opportunities: opps,
            date: new Date(),
            commitWeekYear: null,
            monthYear: monthYear,
            commitForDate: new Date(commitDate),
            week:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            month:{
                userCommitAmount:commitValue,
                relatasCommitAmount:0
            },
            quarter:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            userId: userId,
            commitFor:'month',
            userEmailId:userEmailId
        }

        var findQuery = {
            userId:userId,
            monthYear:monthYear
        };

        oppCommitCollection.findOne(findQuery,{_id:1,userId:1},function (err,commitMade) {

            if(!err && commitMade){
                oppCommitCollection.collection.update(findQuery,{$set:data},function (err,result) {
                    callback(err,data)
                })
            } else {
                oppCommitCollection.collection.insert([data],function (err,resultInsert) {
                    callback(err,data)
                })
            }
        })
    })
}

OppCommitManagement.prototype.updateCommitQuarter = function (userId,userEmailId,commitStage,commitValue,dateMin,dateMax,timezone,commitDate,netGrossMarginReq,calculateNGM,quarterYear,callback) {

    getOppInCommitStage(userId,commitStage,null,null,netGrossMarginReq,calculateNGM,function (err,opps) {

        var data = {
            opportunities: opps,
            date: new Date(),
            commitWeekYear: null,
            monthYear: null,
            quarterYear:quarterYear,
            commitForDate: new Date(dateMin),
            week:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            month:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            quarter:{
                userCommitAmount:commitValue,
                relatasCommitAmount:0
            },
            userId: userId,
            commitFor:'quarter',
            userEmailId:userEmailId
        }

        var findQuery = {
            userId:userId,
            quarterYear:quarterYear
        };

        oppCommitCollection.findOne(findQuery,{_id:1,userId:1},function (err,commitMade) {

            if(!err && commitMade){
                oppCommitCollection.collection.update(findQuery,{$set:data},function (err,result) {
                    callback(err,data)
                })
            } else {
                oppCommitCollection.collection.insert([data],function (err,resultInsert) {
                    callback(err,data)
                })
            }
        })
    })
}

function insertCommit(userId,weekYear,updateObj,callback) {

    updateObj.userId = userId;
    updateObj.commitWeekYear = weekYear;
    updateObj.date = new Date();

    oppCommitCollection.collection.insert(updateObj,function (err,resultInsert) {
        callback(err,resultInsert)
    })
}

function getOppByStageSnapShot(userId,qtrStart,qtrEnd,commitStage,callback){

    var match = {
        userId:userId,
        $or:[{relatasStage:commitStage},{stageName:commitStage}]
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $group:{
                _id:"$relatasStage",
                opps:{
                    $push:{
                        "_id": "$_id",
                        "contactEmailId": "$contactEmailId",
                        "opportunityName": "$opportunityName",
                        "stageName": "$relatasStage",
                        "relatasStage": "$relatasStage",
                        "amount": "$amount",
                        "opportunityId": "$opportunityId",
                        "closeDate": "$closeDate",
                        "geoLocation": "$geoLocation",
                        "productType": "$productType",
                        "vertical": "$vertical",
                        "netGrossMargin": "$netGrossMargin",
                        "userEmailId": "$userEmailId",
                        "createdDate": "$createdDate"
                    }
                },
                totalAmount:{$sum:"$amount"}
            }
        }
    ]).exec(function (err,opps) {
        callback(err,opps)
    })
}

function getOppByInCommitStageByDateRange(userId,commitStage,dateMin,dateMax,netGrossMarginReq,calculateNGM,callback){

    var match = {
        userId:userId,
        $or:[{relatasStage:commitStage},{stageName:commitStage}]
    }

    if(dateMin && dateMax){
        match = {
            userId:userId,
            $or:[{relatasStage:commitStage},{stageName:commitStage}],
            closeDate:{
                $gte:new Date(dateMin),
                $lte:new Date(dateMax)
            }
        }
    }

    opportunitiesCollection.aggregate([{
            $match:match
        }]).exec(function (err,opps) {
            var opportunities = []
            if(!err && opps){
                if(netGrossMarginReq){
                    opportunities  = calculateNGM(opps)
                } else {
                    opportunities = opps;
                }
            }
            callback(err,opportunities)
    })
}

function getOppInCommitStage(userId,commitStage,dateMin,dateMax,netGrossMarginReq,calculateNGM,callback){

    var match = {
        userId:userId,
        $or:[{relatasStage:commitStage},{stageName:commitStage}]
    }

    if(dateMin && dateMax){
        match = {
            userId:userId,
            $or:[{relatasStage:commitStage},{stageName:commitStage}],
            closeDate:{
                $gte:new Date(dateMin),
                $lte:new Date(dateMax)
            }
        }
    }

    opportunitiesCollection.aggregate([{
            $match:match
        }]).exec(function (err,opps) {
            callback(err,opps)
    })
}

OppCommitManagement.prototype.getCommitsByCustomRange = function (findQuery,callback) {

    oppCommitCollection.findOne(findQuery).lean().exec(function(err, data) {
        callback(err,data)
    })
}

OppCommitManagement.prototype.getCommitsByCustomQuery = function (findQuery,callback) {
    oppCommitCollection.find(findQuery,{opportunities:0}).lean().exec(function(err, data) {
        callback(err,data)
    })
}

OppCommitManagement.prototype.getCommitsByWeekRange = function (userId,weekYear,callback) {

    var findQuery = {
        userId:userId,
        commitWeekYear:weekYear
    };

    oppCommitCollection.findOne(findQuery).lean().exec(function(err, data) {

        if(!err && data && data.opportunities){
            var oppIds = _.pluck(_.flatten(_.pluck(data.opportunities,"opps")),"_id");

            opportunitiesCollection.find({_id:{$in:oppIds}},{userEmailId:1}).lean().exec(function (errOpps,opps) {
                var oppsObj = {};
                if(!errOpps && opps && opps.length>0){
                    _.each(opps,function (el) {
                        oppsObj[String(el._id)] = el;
                    });

                    _.each(data.opportunities,function (oppsGrp) {
                        _.each(oppsGrp.opps,function (op) {
                            if(String(op._id) && oppsObj[String(op._id)]){
                                op.userEmailId = oppsObj[String(op._id)].userEmailId;
                            }
                        })
                    })
                }

                callback(err,data)
            });
        } else {
            callback(err,data)
        }

    });
}

OppCommitManagement.prototype.getCommitsByMonthRange = function (userId,monthYear,callback) {

    var findQuery = {
        userId:userId,
        monthYear:monthYear,
        commitFor:"month"
    }

    oppCommitCollection.findOne(findQuery).lean().exec(function(err, data) {
        callback(err,data)
    });
}

OppCommitManagement.prototype.getCommitsByQuarterRange = function (userId,quarterYear,callback) {

    var findQuery = {
        userId:userId,
        quarterYear:quarterYear,
        commitFor:"quarter"
    };

    oppCommitCollection.findOne(findQuery).lean().exec(function(err, data) {
        callback(err,data)
    });
}

OppCommitManagement.prototype.getCommitsByWeekRangeMultiUsers = function (userIds,weekYear,callback) {

    var findQuery = {
        userId:{$in:userIds},
        commitWeekYear:weekYear
    };

    oppCommitCollection.find(findQuery).lean().exec(function(err, data) {
        if(!err && data){
            mergeMultiUserCommits(data,function (formattedData) {
                callback(err,formattedData)
            });
        }
    });
}

OppCommitManagement.prototype.getCommitsByMonthRangeMultiUsers = function (userIds,monthYear,callback) {

    var findQuery = {
        userId:{$in:userIds},
        monthYear:monthYear,
        commitFor:"month"
    };

    oppCommitCollection.find(findQuery).lean().exec(function(err, data) {

        if(!err && data){
            callback(err,mergeMultiUserCommitsMonth(data))
        }
    });
}

OppCommitManagement.prototype.getCommitsByMonthRangeMultiUsersWithProjection = function (userIds,monthYear,projection,callback) {

    var findQuery = {
        userId:{$in:userIds},
        monthYear:monthYear,
        commitFor:"month"
    };

    if(!projection){
        projection = {
            opportunities:0,
            quarter:0,
            week:0
        }
    }

    oppCommitCollection.find(findQuery,projection).lean().exec(function(err, data) {

        if(!err && data){
            callback(err,mergeMultiUserCommitsMonth(data))
        }
    });
}

OppCommitManagement.prototype.getCommitsByDateMultiUsersWithProjection = function (userIds,dateMin,dateMax,projection,callback) {

    var findQuery = {
        userId:{$in:userIds},
        // monthYear:{$in:monthYears},
        commitFor:"month",
        commitForDate: {$gte:new Date(dateMin),$lte:new Date(dateMax)}
    };

    if(!projection){
        projection = {
            opportunities:0,
            quarter:0,
            week:0
        }
    }

    oppCommitCollection.find(findQuery,projection).lean().exec(function(err, data) {
        if(!err && data){

            var dataObj = {}
            _.each(data,function (el) {
                if(dataObj[el.userEmailId]){
                    dataObj[el.userEmailId].push({
                        date:el.commitForDate,
                        monthYear:el.monthYear,
                        amount: parseFloat(el.month.userCommitAmount)
                    })
                } else {
                    dataObj[el.userEmailId] = [];
                    if(el.month && el.month.userCommitAmount){
                        dataObj[el.userEmailId].push({
                            date:el.commitForDate,
                            monthYear:el.monthYear,
                            amount: parseFloat(el.month.userCommitAmount)
                        })
                    }
                }
            });

            callback(err,dataObj)
        }
    });
}

OppCommitManagement.prototype.getCommitsByQuarterRangeMultiUsers = function (userIds,quarterYear,callback) {

    var findQuery = {
        userId:{$in:userIds},
        quarterYear:quarterYear,
        commitFor:"quarter"
    };

    oppCommitCollection.find(findQuery).lean().exec(function(err, data) {
        if(!err && data){
            callback(err,mergeMultiUserCommitsQuarter(data))
        }
    });
}

function mergeMultiUserCommits(data,callback) {

    var emptyObj = {
        "quarter": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "month": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "week": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "opportunities": []
    }

    var opps = [];
    var oppIds = [];

    if(data.length>0){
        _.each(data,function (el) {

            emptyObj.quarter.userCommitAmount = emptyObj.quarter.userCommitAmount+el.quarter.userCommitAmount
            emptyObj.month.userCommitAmount = emptyObj.month.userCommitAmount+el.month.userCommitAmount
            emptyObj.week.userCommitAmount = emptyObj.week.userCommitAmount+el.week.userCommitAmount

            emptyObj.quarter.relatasCommitAmount = emptyObj.quarter.relatasCommitAmount+el.quarter.relatasCommitAmount
            emptyObj.month.relatasCommitAmount = emptyObj.month.relatasCommitAmount+el.month.relatasCommitAmount
            emptyObj.week.relatasCommitAmount = emptyObj.week.relatasCommitAmount+el.week.relatasCommitAmount

            oppIds = oppIds.concat(_.pluck(_.flatten(_.pluck(el.opportunities,"opps")),"_id"))
            if(el.opportunities && el.opportunities[0] && el.opportunities[0].opps && el.opportunities[0].opps.length>0){
                opps = opps.concat(_.flatten(_.pluck(el.opportunities,"opps")))
            }
        })
    }

    oppIds = _.compact(_.flatten(oppIds));

    if(oppIds && oppIds.length>0){
        opportunitiesCollection.find({_id:{$in:oppIds}},{userEmailId:1}).lean().exec(function (errOpps,oppsList) {
            var oppsObj = {};
            if(!errOpps && oppsList && oppsList.length>0){
                _.each(oppsList,function (el) {
                    oppsObj[String(el._id)] = el;
                });

                _.each(data,function (el) {
                    _.each(el.opportunities,function (oppsGrp) {
                        _.each(oppsGrp.opps,function (op) {
                            if(String(op._id) && oppsObj[String(op._id)]){
                                op.userEmailId = oppsObj[String(op._id)].userEmailId;
                            }
                        })
                    })
                });

                _.each(opps,function (op) {
                    if(String(op._id) && oppsObj[String(op._id)]){
                        op.userEmailId = oppsObj[String(op._id)].userEmailId;
                    }
                })
            }

            emptyObj.opportunities.push({
                opps:opps
            });

            callback({
                flattenedData:emptyObj,
                rawData:data
            })
        });
    } else {

        callback({
            flattenedData:emptyObj,
            rawData:data
        })
    }

}

function mergeMultiUserCommitsMonth(data) {

    var emptyObj = {
        "quarter": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "month": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "week": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "opportunities": []
    }

    var opps = [];

    if(data.length>0){
        _.each(data,function (el) {

            if(el.week){
                emptyObj.week.userCommitAmount = emptyObj.week.userCommitAmount+parseFloat(el.week.userCommitAmount)
                emptyObj.week.relatasCommitAmount = emptyObj.week.relatasCommitAmount+parseFloat(el.week.relatasCommitAmount)
            }

            if(el.quarter){
                emptyObj.quarter.userCommitAmount = emptyObj.quarter.userCommitAmount+parseFloat(el.quarter.userCommitAmount)
                emptyObj.quarter.relatasCommitAmount = emptyObj.quarter.relatasCommitAmount+parseFloat(el.quarter.relatasCommitAmount)
            }

            emptyObj.month.userCommitAmount = emptyObj.month.userCommitAmount+parseFloat(el.month.userCommitAmount)
            emptyObj.month.relatasCommitAmount = emptyObj.month.relatasCommitAmount+parseFloat(el.month.relatasCommitAmount)

            if(el.opportunities){
                opps = opps.concat(el.opportunities)
            }
        })
    }

    emptyObj.opportunities = opps;

    return {
        flattenedData:emptyObj,
        rawData:data
    };

}

function mergeMultiUserCommitsQuarter(data) {

    var emptyObj = {
        "quarter": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "month": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "week": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "opportunities": []
    }

    var opps = [];

    if(data.length>0){
        _.each(data,function (el) {

            el.quarter.userCommitAmount = parseFloat(el.quarter.userCommitAmount)
            el.month.userCommitAmount = parseFloat(el.month.userCommitAmount)
            el.week.userCommitAmount = parseFloat(el.week.userCommitAmount)

            emptyObj.quarter.userCommitAmount = emptyObj.quarter.userCommitAmount+parseFloat(el.quarter.userCommitAmount)
            emptyObj.month.userCommitAmount = emptyObj.month.userCommitAmount+parseFloat(el.month.userCommitAmount)
            emptyObj.week.userCommitAmount = emptyObj.week.userCommitAmount+parseFloat(el.week.userCommitAmount)

            emptyObj.quarter.relatasCommitAmount = emptyObj.quarter.relatasCommitAmount+parseFloat(el.quarter.relatasCommitAmount)
            emptyObj.month.relatasCommitAmount = emptyObj.month.relatasCommitAmount+parseFloat(el.month.relatasCommitAmount)
            emptyObj.week.relatasCommitAmount = emptyObj.week.relatasCommitAmount+parseFloat(el.week.relatasCommitAmount)
            opps = opps.concat(el.opportunities)
        })
    }

    emptyObj.opportunities = opps;

    return {
        flattenedData:emptyObj,
        rawData:data
    };
}

OppCommitManagement.prototype.findLastUpdatedSnapShot = function (userIds,startDate,endDate,callback) {

    var findQuery = {
        userId:{$in:userIds},
        commitForDate: {$gte: new Date(startDate),$lte: new Date(endDate)},
        commitFor:"month"
    };

    oppCommitCollection.aggregate([
        {
            $match:findQuery
        },
        {
          $project:{
              day: {$dayOfMonth:"$commitForDate"},
              month_num: {$month:"$commitForDate"},
              year: {$year:"$commitForDate"},
              month:1,
              commitForDate:1,
              userId:1,
          }
        }
    ]).exec(function (err,commits) {
        if(!err && commits && commits.length>0){
            commits.forEach(function (co) {
                co.monthYear = co.day+""+co.month_num+""+co.year;
                co.commitValue = parseFloat(co.month.userCommitAmount)
            })
        }

        callback(err,commits)
    })

}

OppCommitManagement.prototype.getCommitsByDateRange = function (userId,dateMin,dateMax,callback) {

    var findQuery = {
        userId:userId,
        date:{
            $gte: new Date(dateMin),
            $lte: new Date(dateMax)
        },
        $and:[{commitFor:{$ne:"month"}},{commitFor:{$ne:"quarter"}}]
    };

    oppCommitCollection.find(findQuery).lean().exec(function(err, data) {
        callback(err,data)
    });
}

OppCommitManagement.prototype.getPastMonthlyCommits = function (userId,userEmailId,monthYear,oppsInCommitStageThisMonth,callback) {

    var findQuery = {
        userId:userId,
        commitFor:"month",
        commitForDate: {$gte:new Date(moment().subtract(11,"month")),$lte:new Date(moment().endOf("month"))}
    };

    oppCommitCollection.find(findQuery).sort({"commitForDate":1}).limit(13).exec(function(err, data) {
        callback(err,fillNonExistingMonthlyCommits(!err && data?data:[],userId,userEmailId,monthYear,oppsInCommitStageThisMonth))
    });
}

OppCommitManagement.prototype.getPastQuarterlyCommits = function (userId,userEmailId,allQuarters,oppsInCommitStageThisQtr,timezone,fyStartDt,callback) {

    var findQuery = {
        userId:userId,
        commitFor:"quarter",
        commitForDate: {
            $gte:new Date(moment().subtract(1,"year")),
            $lte:new Date(allQuarters[allQuarters.currentQuarter].end)
        }
    };

    oppCommitCollection.find(findQuery).sort({"commitForDate":1}).limit(4).lean().exec(function(err, data) {
        callback(err,fillNonExistingQtrlyCommits(!err && data?data:[],userId,userEmailId,allQuarters,oppsInCommitStageThisQtr,timezone,fyStartDt))
    });
}

function fillNonExistingMonthlyCommits(existingCommits,userId,userEmailId,monthYear,oppsInCommitStageThisMonth) {

    // var months = getMonthsBetweenDates(new Date(moment().subtract(11,"month")),new Date(moment().add(1,"month")));
    var months = getMonthsBetweenDates(new Date(moment().subtract(11,"month")),new Date(moment().endOf("month")));

    var monthsYears = [],
        monthsYearsObj = {};

    _.each(months,function (el) {
        monthsYears.push(el.monthYear);
        monthsYearsObj[el.monthYear] = el
    })

    var nonExistingCommits = _.difference(monthsYears,_.pluck(existingCommits,"monthYear"));

    _.each(nonExistingCommits,function (el) {
        existingCommits.push({
            opportunities: [],
            date: monthsYearsObj[el].date,
            commitWeekYear: null,
            monthYear: el,
            commitForDate: monthsYearsObj[el].date,
            week:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            month:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            quarter:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            userId: userId,
            commitFor:'month',
            userEmailId:userEmailId
        })
    })

    _.each(existingCommits,function (el) {

        if(el.monthYear == monthYear){
            el = JSON.parse(JSON.stringify(el))
            oppsInCommitStageThisMonth = JSON.parse(JSON.stringify(oppsInCommitStageThisMonth))
            el.opportunities = el.opportunities.concat(oppsInCommitStageThisMonth);
        }
    })

    existingCommits.sort(function (o1, o2) {
        return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
    });

    return existingCommits;

}

function fillNonExistingQtrlyCommits(existingCommits,userId,userEmailId,allQuarters,oppsInCommitStageThisMonth,timezone,fyStartDt) {

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    var qtrs = [];
    var qtrsYearsObj = {};

    for( var key in allQuarters){
        if(key != "currentQuarter"){
            var qtrYear = key+""+moment(moment(allQuarters[key].start)).year();
            var obj = {
                qtrYear:qtrYear,
                date:new Date(moment(allQuarters[key].start)),
                start:new Date(moment(allQuarters[key].start)),
                end:new Date(moment(allQuarters[key].end))
            }

            qtrsYearsObj[qtrYear] = obj;
            qtrs.push(obj);
        }
    }

    var prevQtrs = getPreviousQuarters(moment(fyStartDt).subtract(1,"year"),timezone);

    qtrs = prevQtrs.concat(qtrs);

    qtrs = qtrs.filter(function (el) {
        return new Date(el.date)<new Date()
    })

    qtrs.sort(function (o1, o2) {
        return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
    });

    _.each(qtrs,function (el) {
        qtrsYearsObj[String(el.qtrYear)] = el
    });

    qtrs = _.takeRight(qtrs, 4);

    var qtrsYear = [];
    _.each(qtrs,function (el) {
        qtrsYear.push(el.qtrYear);
    });

    var nonExistingCommits = _.difference(qtrsYear,_.pluck(existingCommits,"quarterYear"));

    _.each(nonExistingCommits,function (el) {
        existingCommits.push({
            opportunities: [],
            date: qtrsYearsObj[el].date,
            commitWeekYear: null,
            monthYear: null,
            quarterYear: el,
            commitForDate: qtrsYearsObj[el].date,
            week:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            month:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            quarter:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            userId: userId,
            commitFor:'quarter',
            userEmailId:userEmailId
        })
    })

    _.each(existingCommits,function (el) {
        el.target = 0;
        if(qtrsYearsObj[String(el.quarterYear)]){
            el.qtrStart = qtrsYearsObj[el.quarterYear].start;
            el.qtrEnd = qtrsYearsObj[el.quarterYear].end
        }
    });

    return existingCommits;

}

function getMonthsBetweenDates(from,to) {
    var startDate = moment(from);
    var endDate = moment(to);

    var result = [];

    if (endDate.isBefore(startDate)) {
        throw "End date must be greated than start date."
    }

    var currentDate = startDate.clone();

    while (currentDate.isBefore(endDate)) {
        result.push({
            date:new Date(currentDate),
            monthYear:moment(currentDate).month()+""+moment(currentDate).year()
        });
        currentDate.add(1, 'month');
    }

    return result;
}

function getPreviousQuarters(fyStartDate,timezone) {

    var quarters = [],
    twelve_months_for_loop = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November"],
        months = [],
        qtrYears = [];

    months.push(fyStartDate);

    _.each(twelve_months_for_loop,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    quarters.push({start:moment(months[0]).startOf('month').format(),end:moment(months[2]).endOf('month').format(),qtr:"quarter1"})
    quarters.push({start:moment(months[3]).startOf('month').format(),end:moment(months[5]).endOf('month').format(),qtr:"quarter2"})
    quarters.push({start:moment(months[6]).startOf('month').format(),end:moment(months[8]).endOf('month').format(),qtr:"quarter3"})
    quarters.push({start:moment(months[9]).startOf('month').format(),end:moment(months[11]).endOf('month').format(),qtr:"quarter4"})

    _.each(quarters,function (el) {
        qtrYears.push({
            qtrYear:el.qtr+""+moment(el.start).year(),
            date:new Date(el.start),
            start:new Date(el.start),
            end:new Date(el.end),
        })
    });

    return qtrYears;
}

module.exports = OppCommitManagement;
