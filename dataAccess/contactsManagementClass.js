var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var contactsCollection = require('../databaseSchema/contactSchema').contacts;
var dntContactsCollection = require('../databaseSchema/dntContactSchema').dntContacts;
var UserManagement = require('./userManagementDataAccess');
var userRelationCollection = require('../databaseSchema/userRelatonShipSchema').userRelation;

var UserManagementObj = new UserManagement();
var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger = logLib.getWinston();
var _ = require("lodash");
var interaction = require('../databaseSchema/userManagementSchema').interactions;
var interactionsTrash = require('../databaseSchema/interactionTrashSchema').interactionsTrash;
var interactionsV2 = require('../databaseSchema/interactionSchema').interactionsV2;
var moment = require("moment");

function Contact() {

    this.getNewContactsAdded = function(userId, dateMin, callback) {
        var date;
        if (!dateMin) {
            date = new Date();
            date.setHours(0)
            date.setMinutes(0)
            date.setSeconds(0)
            date.setDate(date.getDate() - 7);
        } else {
            date = new Date(dateMin);
        }

        myUserCollection.aggregate([{
            $match: { _id: userId }
        }, {
            $unwind: "$contacts"
        }, {
            $match: {
                "contacts.addedDate": { $gte: date }
            }
        }, {
            $group: {
                _id: null,
                contacts: {
                    $push: {
                        _id: "$contacts._id",
                        personId: "$contacts.personId",
                        personName: "$contacts.personName",
                        personEmailId: "$contacts.personEmailId",
                        companyName: "$contacts.companyName",
                        designation: "$contacts.designation",
                        addedDate: "$contacts.addedDate",
                        mobileNumber: "$contacts.mobileNumber",
                        contactRelation: "$contacts.contactRelation",
                        favorite: "$contacts.favorite",
                        twitterUserName: "$contacts.twitterUserName",
                        facebookUserName: "$contacts.twitterUserName",
                        linkedinUserName: "$contacts.twitterUserName",
                        hashtags: "$contacts.hashtag",
                        contactImageLink: "$contacts.contactImageLink"
                    }
                },
                count: {
                    $sum: 1
                }
            }
        }]).exec(function(error, data) {
            if (checkRequired(data) && data.length > 0) {
                callback(error, data)
            } else callback(error, [])
        })
    };

    this.getRecentlyInteractedContacts = function (userId,searchContent,callback) {

        var q = {};
        q['$or'] = [{ "contacts.personEmailId": {$in:searchContent} }, { "contacts.mobileNumber": {$in:searchContent} }];

        myUserCollection.aggregate([{
            $match: { _id: userId }
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }, {
            $group: {
                _id: "$contacts.relatasUser",
                contacts: {
                    $addToSet: {
                        _id: "$contacts._id",
                        personId: "$contacts.personId",
                        personName: "$contacts.personName",
                        personEmailId: "$contacts.personEmailId",
                        companyName: "$contacts.companyName",
                        designation: "$contacts.designation",
                        addedDate: "$contacts.addedDate",
                        mobileNumber: "$contacts.mobileNumber",
                        contactRelation: "$contacts.contactRelation",
                        favorite: "$contacts.favorite",
                        twitterUserName: "$contacts.twitterUserName",
                        facebookUserName: "$contacts.twitterUserName",
                        linkedinUserName: "$contacts.twitterUserName",
                        hashtags: "$contacts.hashtag",
                        contactImageLink: "$contacts.contactImageLink",
                        contactsId:"$contacts._id",
                        ownerEmailId:"$emailId"
                    }
                },
                count: {
                    $sum: 1
                }
            }
        }]).exec(function(error, data) {
            if (checkRequired(data) && data.length > 0) {
                var index = null;
                for (var i = 0; i < data.length; i++) {
                    if (data[i]._id == true) {
                        index = i;
                    }
                }

                if (index != null) {
                    myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'publicProfileUrl profilePicUrl' }, function(err, list) {
                        if (!err && list) {
                            callback(null, concatAllContacts(data, list, true))
                        } else
                            callback(null, concatAllContacts(data, [], true))
                    })
                } else callback(null, concatAllContacts(data, [], true))

                // callback(error, data)
            } else callback(error, [])
        })
    };

    this.getRecentlyInteractedAccounts = function (userId,searchContent,callback) {

        var q = { "contacts.account.name": searchContent}

        myUserCollection.aggregate([{
            $match: { _id: userId }
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }, {
            $group: {
                _id: "$emailId",
                contacts: {
                    $push: {
                        _id: "$contacts._id",
                        personEmailId: "$contacts.personEmailId",
                        account:"$contacts.account.name"
                    }
                },
                count: {
                    $sum: 1
                }
            }
        }]).exec(function(error, data) {
            if (checkRequired(data) && data.length > 0) {
                callback(error, data)
            } else callback(error, [])
        })
    };

    this.checkPipelineValuesExists = function (userId,callback) {
        myUserCollection.aggregate([
        {
            $match: { _id: userId }
        },{
            $unwind: "$contacts"
        },{
            $match: {
                "contacts.account.value.inProcess": { $gte: 1 }
            }
        },{
                $limit:1
            },{
                $project:{
                    _id:"$contacts"
                }
            }]).exec(function (err,result) {
            if(result.length>0){
                callback(true)
            } else {
                callback(false)
            }
        });
    };

    this.populateUserProfileContacts = function(contacts, callback) {
        var contactsId = [];
        var contactsNoId = [];
        for (var c = 0; c < contacts.length; c++) {
            if (checkRequired(contacts[c].personId)) {
                contactsId.push(contacts[c])
            } else contactsNoId.push(contacts[c])
        }
        if (contactsId.length > 0) {
            myUserCollection.populate(contactsId, { path: 'personId', select: 'firstName lastName companyName designation profilePicUrl mobileNumber' }, function(error, list) {
                if (!error && checkRequired(list) && list.length > 0) {
                    contacts = list.concat(contactsNoId);
                    callback(contacts, true)
                } else callback(contacts, false);
            })
        } else callback(contacts, false);
    };

    this.getContactsNotInEmailArr = function(userId, emailArr, emailId, isCount, callback) {
        if (emailArr) {
            emailArr.push(emailId);
        } else {
            emailArr = [emailId];
        }

        if (checkRequired(emailArr) && emailArr.length > 0) {
            if (isCount) {
                myUserCollection.aggregate([{
                    $match: { _id: userId }
                }, {
                    $unwind: "$contacts"
                }, {
                    $match: {
                        "contacts.personEmailId": { $nin: emailArr }
                    }
                }, {
                    $group: {
                        _id: null,
                        count: {
                            $sum: 1
                        }
                    }
                }]).exec(function(error, data) {

                    if (checkRequired(data) && data.length > 0) {
                        callback(data[0].count)
                    } else callback(error, 0)
                })
            } else {
                myUserCollection.aggregate([{
                    $match: { _id: userId }
                }, {
                    $unwind: "$contacts"
                }, {
                    $match: {
                        "contacts.personEmailId": { $nin: emailArr },
                        "contacts.personId": { $nin: ['', null] }
                    }
                }, {
                    $group: {
                        _id: null,
                        contacts: {
                            $push: {
                                _id: "$contacts._id",
                                personId: "$contacts.personId",
                                personEmailId: "$contacts.personEmailId",
                                personName: "$contacts.personName",
                                companyName: "$contacts.companyName",
                                designation: "$contacts.designation",
                                skypeId: "$contacts.skypeId",
                                mobileNumber: "$contacts.mobileNumber",
                                birthday: "$contacts.birthday",
                                source: "$contacts.source",
                                lastInteracted: "$contacts.lastInteracted",
                                addedDate: "$contacts.addedDate",
                                relatasUser: "$contacts.relatasUser",
                                relatasContact: "$contacts.relatasContact",
                                contactRelation: "$contacts.contactRelation",
                                favorite: "$contacts.favorite",
                                twitterUserName: "$contacts.twitterUserName",
                                facebookUserName: "$contacts.twitterUserName",
                                linkedinUserName: "$contacts.twitterUserName",
                                hashtags: "$contacts.hashtag"
                            }
                        },
                        count: {
                            $sum: 1
                        }
                    }
                }]).exec(function(error, data) {

                    if (checkRequired(data) && data.length > 0) {

                        myUserCollection.populate(data[0].contacts, {
                            path: 'personId',
                            select: 'firstName lastName companyName designation publicProfileUrl mobileNumber'
                        }, function(err, list) {

                            if (!err && list && list.length > 0) {

                                getContactsNotInEmailArrNoPersonId(userId, emailArr, function(info) {
                                    if (checkRequired(info) && info.length > 0) {
                                        list = list.concat(info[0].contacts)
                                    }
                                    callback(list)
                                })

                            } else {
                                getContactsNotInEmailArrNoPersonId(userId, emailArr, function(info) {
                                    list = [];
                                    if (checkRequired(info) && info.length > 0) {
                                        list = list.concat(info.contacts)
                                    }
                                    list = list.concat(data[0].contacts)
                                    callback(list)
                                })
                            }
                        });

                    } else {
                        getContactsNotInEmailArrNoPersonId(userId, emailArr, function(info) {
                            var list = [];
                            if (checkRequired(info) && info.length > 0) {
                                list = list.concat(info.contacts)
                            }
                            callback(list)
                        })

                    }
                })
            }
        }
    };

    this.getContactsByTypeCount = function(userId, emailArr, mobileNumberArr, callback) {

        var q = { "contacts.contactRelation": { $exists: true } };
        if (checkRequired(emailArr) && emailArr.length > 0 && mobileNumberArr.length > 0) {
            q["$or"] = [
                { "contacts.personEmailId": { $in: emailArr } },
                { "contacts.mobileNumber": { $in: mobileNumberArr } }
            ];
        } else if (emailArr.length > 0) {
            q["contacts.personEmailId"] = { $in: emailArr }
        } else if (mobileNumberArr.length > 0) {
            q["contacts.mobileNumber"] = { $in: mobileNumberArr }
        }

        myUserCollection.aggregate([{
            $match: { _id: userId }
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }, {
            $group: {
                _id: null,
                customer: { $sum: { $cond: { if: { $eq: ["$contacts.contactRelation.prospect_customer", "customer"] }, then: 1, else: 0 } } },
                prospect: { $sum: { $cond: { if: { $eq: ["$contacts.contactRelation.prospect_customer", "prospect"] }, then: 1, else: 0 } } },
                influencer: { $sum: { $cond: { if: { $eq: ["$contacts.contactRelation.decisionmaker_influencer", "influencer"] }, then: 1, else: 0 } } },
                decision_maker: { $sum: { $cond: { if: { $eq: ["$contacts.contactRelation.decisionmaker_influencer", "decision_maker"] }, then: 1, else: 0 } } },
                favorites: { $sum: { $cond: { if: { $eq: ["$contacts.favorite", true] }, then: 1, else: 0 } } }
            }
        }]).exec(function(error, data) {
            if (checkRequired(data) && data.length > 0) {
                callback(error, data)
            } else callback(error, [])
        })
    };

    this.getContactsFavoriteCount = function(userId, emailArr, mobileNumberArr, callback) {

        var q = { "contacts.favorite": true };
        if (checkRequired(emailArr) && emailArr.length > 0 && mobileNumberArr.length > 0) {
            q["$or"] = [
                { "contacts.personEmailId": { $in: emailArr } },
                { "contacts.mobileNumber": { $in: mobileNumberArr } }
            ];
        } else if (emailArr.length > 0) {
            q["contacts.personEmailId"] = { $in: emailArr }
        } else if (mobileNumberArr.length > 0) {
            q["contacts.mobileNumber"] = { $in: mobileNumberArr }
        }

        myUserCollection.aggregate([{
            $match: { _id: userId }
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }, {
            $group: {
                _id: null,
                count: {
                    $sum: 1
                }
            }
        }]).exec(function(error, data) {

            if (checkRequired(data) && data.length > 0 && data[0] && data[0].count) {
                callback(error, data[0].count)
            } else callback(error, 0)
        })
    };

    this.getContactsFavoriteCountMultipleUsers = function(userIds, callback) {

        var q = { "contacts.favorite": true };

        myUserCollection.aggregate([{
            $match: { _id: {$in:userIds} }
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        },{
            $project:{
                personEmailId:"$contacts.personEmailId",
                emailId:"$emailId",
                personId:"$contacts.personId",
                contactRelation:"$contacts.contactRelation",
                personName:"$contacts.personName"
            }
        },{
            $group: {
                _id: "$emailId",
                contacts:{
                    $addToSet: {
                        personEmailId:"$personEmailId",
                        personId:"$personId",
                        contactRelation:"$contactRelation",
                        personName:"$personName"
                    }
                }
            }
        }]).exec(function(error, data) {
            callback(error, data)
        })
    };

    this.getContactsByTypeCount_companyLanding = function(userIdList, callback) {

        myUserCollection.aggregate([{
            $match: { _id: { $in: userIdList } }
        }, {
            $unwind: "$contacts"
        }, {
            $group: {
                _id: null,
                customer: { $sum: { $cond: { if: { $eq: ["$contacts.contactRelation.prospect_customer", "customer"] }, then: 1, else: 0 } } },
                prospect: { $sum: { $cond: { if: { $eq: ["$contacts.contactRelation.prospect_customer", "prospect"] }, then: 1, else: 0 } } },
                influencer: { $sum: { $cond: { if: { $eq: ["$contacts.contactRelation.decisionmaker_influencer", "influencer"] }, then: 1, else: 0 } } },
                decision_maker: { $sum: { $cond: { if: { $eq: ["$contacts.contactRelation.decisionmaker_influencer", "decision_maker"] }, then: 1, else: 0 } } },
                favorite: { $sum: { $cond: { if: { $eq: ["$contacts.favorite", true] }, then: 1, else: 0 } } },
                totalCompanies: { $addToSet: "$contacts.account.name" }
            }
        }]).exec(function(error, data) {
            data[0].totalCompanies = (_.filter(data[0].totalCompanies, function(i) {
                return i != null })).length
            if (checkRequired(data) && data.length > 0) {
                callback(error, data)
            } else callback(error, [])
        })
    };

    this.getContactsByEmailId = function(userId, emailArr, callback){
        myUserCollection.aggregate([
            {$match:{_id:userId}},
            {$unwind:"$contacts"},
            {$match:{"contacts.personEmailId":{$in:emailArr}}},
            {$project:{
                _id:0,
                ownerEmailId:"$emailId",
                ownerId:"$_id",
                personEmailId:"$contacts.personEmailId",
                contactValue:"$contacts.account.value.inProcess",
                personName:"$contacts.personName",
                lastInteracted:"$contacts.lastInteracted",
                designation:"$contacts.designation",
                companyNamefromUser:"$contacts.companyName",
                contactId:"$contacts._id",
                personId:"$contacts.personId",
                relation:"$contacts.contactRelation.decisionmaker_influencer"
                //publicProfileUrl:"$"
            }}
        ]).exec(function(error, contacts){
            callback(error, contacts)
        })
    }

    this.getEmailContacts = function(userId, callback){
        myUserCollection.aggregate([
            {$match:{_id:userId}},
            {$unwind:"$contacts"},
            {$project:{
                personEmailId:"$contacts.personEmailId",
            }}
        ]).exec(function(error, contacts){
            callback(error, contacts)
        })
    }
}

Contact.prototype.getAllContactsUser_without_populate_emailId_id = function(userId, callback) {
    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $group: {
            _id: null,
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    account: "$contacts.account"
                }
            },
            count: {
                $sum: 1
            }
        }
    }]).exec(function(error, data) {

        var otherAccounts = ['gmail', 'yahoo', 'hotmail'];
        if (data && data.length > 0 && data[0].contacts && data[0].contacts.length > 0) {
            var aggrObj = {};
            var isOk = false;
            for (var i = 0; i < data[0].contacts.length; i++) {

                if (checkRequired(data[0].contacts[i].personEmailId) && data[0].contacts[i].personEmailId.indexOf("@") != -1 && data[0].contacts[i].personEmailId.indexOf(".") != -1) {
                    var domain = data[0].contacts[i].personEmailId.split('@')[1].split('.')[0];
                    if (checkRequired(data[0].contacts[i].account) && checkRequired(data[0].contacts[i].account.name)) {
                        domain = data[0].contacts[i].account.name;
                    }
                    if (otherAccounts.indexOf(domain) != -1) {
                        domain = 'other'
                    }
                    if (checkRequired(data[0].contacts[i].account) && checkRequired(data[0].contacts[i].account.name)) {
                        data[0].contacts[i].selected = data[0].contacts[i].account.selected;
                    } else data[0].contacts[i].selected = true;

                    if (aggrObj[domain]) {
                        //aggrObj[domain].push(data[0].contacts[i]);
                        aggrObj[domain].contacts.push(data[0].contacts[i])
                        aggrObj[domain].total++;
                    } else {
                        isOk = true;
                        aggrObj[domain] = { contacts: [], total: 0, account: domain, selectAll: domain + 'selectAll', showToReportingManager: data[0].contacts[i].account.showToReportingManager }
                        aggrObj[domain].contacts.push(data[0].contacts[i])
                        aggrObj[domain].total++
                    }

                    aggrObj[domain].selected = data[0].contacts[i].selected;

                    if (checkRequired(data[0].contacts[i].account) && checkRequired(data[0].contacts[i].account.name)) {
                        aggrObj[domain].showToReportingManager = data[0].contacts[i].account.showToReportingManager;
                    }
                    //else{
                    //    aggrObj[domain].showToReportingManager = false;
                    //}
                }

            }
            if (isOk) {
                var arr = [];
                var allAcc = [];
                for (var acc in aggrObj) {
                    allAcc.push(acc)
                    arr.push(aggrObj[acc]);
                }
                arr.sort(function(a, b) {
                    return a.account < b.account ? -1 : a.account > b.account ? 1 : 0;
                });

                callback(arr, allAcc)
            } else callback(null)
        } else callback(null)
    })
};

function getContactsNotInEmailArrNoPersonId(userId, emailArr, callback) {
    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: {
            "contacts.personEmailId": { $nin: emailArr },
            "contacts.personId": { $in: ['', null] }
        }
    }, {
        $group: {
            _id: null,
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personEmailId: "$contacts.personEmailId",
                    personName: "$contacts.personName",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    skypeId: "$contacts.skypeId",
                    mobileNumber: "$contacts.mobileNumber",
                    birthday: "$contacts.birthday",
                    source: "$contacts.source",
                    lastInteracted: "$contacts.lastInteracted",
                    addedDate: "$contacts.addedDate",
                    relatasUser: "$contacts.relatasUser",
                    relatasContact: "$contacts.relatasContact",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag"
                }
            },
            count: {
                $sum: 1
            }
        }
    }]).exec(function(error, data) {
        callback(data)
    })
}

Contact.prototype.getAllContactsWithProfile = function(userId, callback) {

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag",
                    contactImageLink: "$contacts.contactImageLink"
                }
            },
            count: {
                $sum: 1
            }
        }
    }]).exec(function(error, data) {

        if (checkRequired(data) && data.length > 0) {
            var index = null;
            for (var i = 0; i < data.length; i++) {
                if (data[i]._id == true) {
                    index = i;
                }
            }
            if (index != null) {
                myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'publicProfileUrl profilePicUrl' }, function(err, list) {

                    if (!err && list) {

                        callback(null, concatAllContacts(data, list, true))
                    } else
                        callback(null, concatAllContacts(data, [], true))
                })
            } else callback(null, concatAllContacts(data, [], true))

        } else
            callback(error, concatAllContacts([], [], true))
    })
};

function concatAllContacts(all, some, sortByName) {

    var allContacts = [];
    if (checkRequired(all) && all.length > 0) {
        for (var i = 0; i < all.length; i++) {
            if (all[i]._id != true) {
                for (var j = 0; j < all[i].contacts.length; j++) {
                    if (!checkRequired(all[i].contacts[j].personName)) {
                        all[i].contacts[j].personName = all[i].contacts[j].personEmailId
                    }
                }
                allContacts = allContacts.concat(all[i].contacts);
            }
        }
    }
    allContacts = allContacts.concat(some);

    if (sortByName) {
        allContacts.sort(function(a, b) {
            if (checkRequired(a.personName) && checkRequired(b.personName)) {
                if (a.personName.toLocaleLowerCase() < b.personName.toLocaleLowerCase()) return -1;
                if (a.personName.toLocaleLowerCase() > b.personName.toLocaleLowerCase()) return 1;
                return 0;
            } else return 1;
        })
    } else {
        allContacts.sort(function(a, b) {
            return new Date(b.addedDate) - new Date(a.addedDate);
        });
    }

    return allContacts;
}

/*Get by type hashtag*/
Contact.prototype.getAllFavoriteContactsWithProfile = function(userId, searchContent, callback) {

    var q = { "contacts.favorite": true };
    if (checkRequired(searchContent)) {
        var search = new RegExp(searchContent, 'i');

        q['$or'] = [{ "contacts.personName": search }, { "contacts.companyName": search }, { "contacts.designation": search }];
    }

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: q
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag",
                    contactImageLink: "$contacts.contactImageLink"

                }
            },
            userIds: {
                $addToSet: "$contacts.personId"
            },
            count: {
                $sum: 1
            }
        }
    }]).exec(function(error, data) {

        if (checkRequired(data) && data.length > 0) {
            var index = null;
            for (var i = 0; i < data.length; i++) {
                if (data[i]._id == true) {
                    index = i;
                }
            }
            if (index != null) {
                myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'profilePicUrl' }, function(err, list) {
                    if (!err && list) {
                        callback(null, concatAllContacts(data, list, true), data[index].userIds)
                    } else
                        callback(null, concatAllContacts(data, [], true), data[index].userIds)
                })
            } else callback(null, concatAllContacts(data, [], true), [])

        } else
            callback(error, concatAllContacts([], [], true), [])
    })
};

Contact.prototype.startTracking = function(userId,contacts,callback){
    dntContactsCollection.find({ownerId:userId,$or:[{personEmailId:{$in:contacts}},{mobileNumber:{$in:contacts}}]},function (err,result) {
        addUsers(userId,result,function () {
            dntContactsCollection.remove({ownerId:userId,$or:[{personEmailId:{$in:contacts}},{mobileNumber:{$in:contacts}}]},function (err_,res_r) {
                callback(err_,res_r);
            })
        })
    });
}

Contact.prototype.findDntContacts = function(userId,callback){
    dntContactsCollection.find({ownerId:userId},{personEmailId:1}).lean().exec(function (err,contacts) {
        var emailIds = [];
        if(!err && contacts && contacts.length>0){
            emailIds = _.pluck(contacts,"personEmailId");
        }

        callback(emailIds);
    });
}

var findDntContacts = function(userId,callback){
    dntContactsCollection.find({ownerId:userId},{personEmailId:1}).lean().exec(function (err,contacts) {
        var emailIds = [];
        if(!err && contacts && contacts.length>0){
            emailIds = _.pluck(contacts,"personEmailId");
        }

        callback(emailIds);
    });
}

Contact.prototype.getAllDntCotnacts = function(userId,callback){
    dntContactsCollection.find({ownerId:userId},{mobileNumber:1, personEmailId:1}).lean().exec(function (err,contacts) {
        var contactsCollection = {};

        var mobileNumbers = [];

        if(!err && contacts && contacts.length>0){
            contactsCollection.mobileNumbers = _.pluck(contacts,"mobileNumber");
            contactsCollection.personEmailIds = _.pluck(contacts,"personEmailId");

        }
        callback(contactsCollection);
    });
}


Contact.prototype.dntContactsRemove = function(userId,contacts,callback){

    if (contacts.length > 0) {

        myUserCollection.aggregate([
            {
                $match: {
                    _id: userId
                }
            },
            {
                $unwind: "$contacts"
            },
            {
                $match:{
                    "contacts._id": {$in:contacts}
                }
            },
            {
                $group:{
                    _id:"$emailId",
                    contacts:{
                        $push:"$contacts"
                    }
                }
            }], function(err, result) {

            if(!err && result && result.length>0){

                var personEmailIds = [],mobiles = [];
                _.each(result[0].contacts,function (co) {
                    if(co.personEmailId){
                        personEmailIds.push(co.personEmailId);
                    }

                    if(co.mobileNumber){
                        mobiles.push(co.mobileNumber);
                    }

                    co.ownerId = userId;
                    co.ownerEmailId = result[0]._id;
                });

                var findQ = {
                    ownerId:userId,
                    $or:[{emailId:{$in:personEmailIds}},{mobileNumber:{$in:mobiles}}]
                };

                var findQ2 = {
                    ownerId:userId,
                    userId:{$ne:userId},
                    $or:[{emailId:{$in:personEmailIds}},{mobileNumber:{$in:mobiles}}]
                }

                if(personEmailIds.length>0 && mobiles.length == 0){
                    findQ = {
                        ownerId:userId,
                        emailId:{$in:personEmailIds}
                    };

                    findQ2 = {
                        ownerId:userId,
                        userId:{$ne:userId},
                        emailId:{$in:personEmailIds}
                    }
                }

                if(personEmailIds.length == 0 && mobiles.length > 0){
                    findQ = {
                        ownerId:userId,
                        mobileNumber:{$in:mobiles}
                    };

                    findQ2 = {
                        ownerId:userId,
                        userId:{$ne:userId},
                        mobileNumber:{$in:mobiles}
                    }
                }

                dntContactsCollection.remove({ ownerId:userId,
                    $or:[{personEmailId:{$in:personEmailIds}},{mobileNumber:{$in:mobiles}}]},function () {
                    dntContactsCollection.collection.insert(result[0].contacts,function (errIn,resIn) {

                        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();

                        contacts.forEach(function(a){
                            bulk.find({_id: userId}).update({$pull: {'contacts': {_id: a}}});
                        });

                        bulk.execute(function(err, result) {

                            interactionsV2.find(findQ).exec(function (err_i,interactionsToMove) {
                                if(!err_i && interactionsToMove && interactionsToMove.length>0){
                                    interactionsTrash.collection.insert(interactionsToMove,function () {
                                        interactionsV2.remove(findQ).exec(function () {
                                            interaction.remove(findQ2).exec(function () {});
                                            if(callback){
                                                callback(err,result)
                                            }
                                        });
                                    })
                                } else {
                                    interactionsV2.remove(findQ).exec(function () {
                                        interaction.remove(findQ2).exec(function () {});
                                        if(callback){
                                            callback(err,result)
                                        }
                                    });
                                }
                            });
                        })
                    })
                })
            } else {
                callback(err,null)
            }
        });
    }
}

Contact.prototype.getDntContacts = function(userId,skip,limit,search,callback) {

    var query = {ownerId:userId};
    if(search){
        query = {
            ownerId:userId,
            $or:[{personEmailId:new RegExp(search, 'i')},{mobileNumber:new RegExp(search, 'i')},{personName:new RegExp(search, 'i')}]
        }
    }

    dntContactsCollection.find(query).lean().exec(function (err,contacts) {
        if(!err && contacts){
            var total = contacts.length;
            var results = contacts.splice(skip,limit);

            callback({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{
                    total:total,
                    skipped:skip,
                    limit:limit,
                    returned:results.length,
                    contacts:results
                }
            });
        } else {
            callback({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": {
                    total: 0,
                    skipped: skip,
                    limit: limit,
                    returned: 0,
                    contacts: []
                }});
        }
    })
}

Contact.prototype.getDntSearch = function(userId,skip,limit,search,callback) {

    var query = {ownerId:userId};
    if(search){
        query = {
            ownerId:userId,
            $or:[{personEmailId:new RegExp(search, 'i')},{mobileNumber:new RegExp(search, 'i')},{personName:new RegExp(search, 'i')}]
        }
    }

    dntContactsCollection.find(query).lean().exec(function (err,contacts) {
        if(!err && contacts){
            var total = contacts.length;
            var results = contacts.splice(skip,limit);

            callback({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{
                    total:total,
                    skipped:skip,
                    limit:limit,
                    returned:results.length,
                    contacts:results
                }
            });
        } else {
            callback({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": {
                    total: 0,
                    skipped: skip,
                    limit: limit,
                    returned: 0,
                    contacts: []
                }});
        }
    })
}

Contact.prototype.getAllHashtagContactsWithProfile = function(userId, searchContent, callback) {
    var q = {};
    if (checkRequired(searchContent)) {
        var search = new RegExp(searchContent, 'i');
        q['$or'] = [{ "contacts.personName": search }, { "contacts.companyName": search }, { "contacts.designation": search }, { "contacts.hashtag": search }];

    }

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: q
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag"

                }
            },
            userIds: {
                $addToSet: "$contacts.personId"
            },
            count: {
                $sum: 1
            }
        }
    }]).exec(function(error, data) {

        if (checkRequired(data) && data.length > 0) {
            var index = null;
            for (var i = 0; i < data.length; i++) {
                if (data[i]._id == true) {
                    index = i;
                }
            }
            if (index != null) {
                myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'firstName lastName companyName designation profilePicUrl' }, function(err, list) {
                    if (!err && list) {
                        callback(null, concatAllContacts(data, list, true), data[index].userIds)
                    } else
                        callback(null, concatAllContacts(data, [], true), data[index].userIds)
                })
            } else callback(null, concatAllContacts(data, [], true), [])

        } else
            callback(error, concatAllContacts([], [], true), [])
    })
};

Contact.prototype.getContactsByType = function(userId, type, emailIdArr, searchContent, callback) {

    var and = [];

    for (var i = 0; i < type.length; i++) {
        if (type[i] != 'favorite') {

            if(type[i] == "decision_maker") {
                var d1 = { $or: [{ "contacts.contactRelation.prospect_customer": type[i].trim() }, { "contacts.contactRelation.decision_maker": true }] };
            } else if(type[i] == "influencer") {
                d1 = { $or: [{ "contacts.contactRelation.prospect_customer": type[i].trim() }, { "contacts.contactRelation.influencer": true }] };
            } else if(type[i] == "partner") {
                d1 = { $or: [{ "contacts.contactRelation.prospect_customer": type[i].trim() }, { "contacts.contactRelation.partner": true }] };
            }else {
                d1 = { $or: [{ "contacts.contactRelation.prospect_customer": type[i].trim() }, { "contacts.contactRelation.decisionmaker_influencer": type[i].trim() }] };
            }

            and.push(d1)
        }
    }

    var q = {
        $and: [
            { $and: and }
        ]
    };

    if (checkRequired(searchContent)) {
        var search = new RegExp(searchContent, 'i');

        var a = { '$or': [{ "contacts.personName": search }, { "contacts.companyName": search }, { "contacts.designation": search }] }
        q['$and'].push(a);
    }
    if (checkRequired(emailIdArr) && emailIdArr.length > 0) {
        q["contacts.personEmailId"] = { $in: emailIdArr }
    }

    if (type.indexOf('favorite') != -1) {
        q['$and'].push({ '$or': [{ "contacts.favorite": true }] })
    }

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: q
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag",
                    contactImageLink: "$contacts.contactImageLink"
                }
            },
            userIds: {
                $addToSet: "$contacts.personId"
            },
            count: {
                $sum: 1
            }
        }
    }]).exec(function(error, data) {
        if (checkRequired(data) && data.length > 0) {
            var index = null;
            for (var i = 0; i < data.length; i++) {
                if (data[i]._id == true) {
                    index = i;
                }
            }
            if (index != null) {
                myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'profilePicUrl publicProfileUrl' }, function(err, list) {
                    if (!err && list) {

                        callback(null, concatAllContacts(data, list, true), data[index].userIds)
                    } else
                        callback(null, concatAllContacts(data, [], true), data[index].userIds)
                })
            } else callback(null, concatAllContacts(data, [], true), [])

        } else
            callback(error, concatAllContacts([], [], true), [])
    })
};

Contact.prototype.getCommonConnectionsNew = function(userEmail,cUserEmail,userContacts,cUserContacts,limit,callBack){

    var userContactsArr = userContacts
    var cUserContactsArr = cUserContacts
    var resultArr = []
    var count = 0;
    var limiter = limit

    if(limit>25 || limit==null || limit == "undefined" || limit == ""){
        limiter = 25
    }

    if(limit == 'web')
        limiter = 10000;

    for (var i = 0; i < userContactsArr.length; i++){
        for ( var j = 0; j<cUserContactsArr.length && resultArr.length<10000 ; j++){


            var u1_e = userContactsArr[i].personEmailId
            var u2_e = cUserContactsArr[j].personEmailId
            var u1_m = userContactsArr[i].mobileNumber
            var u2_m = cUserContactsArr[j].mobileNumber
            var isCommon = false;

            if(u1_e==userEmail || u1_e==cUserEmail){
                break;
            }


            if(u1_e == u2_e){
                if(u1_e!= null && u1_e!=""){
                    isCommon = true
                }
            }

            if(!isCommon)
            if( u1_e == null || u1_e == ""){
                if(u2_e == null || u2_e == ""){
                    if(u1_m == u2_m){
                        if(u1_m != null || u1_m != ""){ // This is a condition where email and mobileNumber are null or "". It should not be possible, but it is a safety
                            isCommon = true
                        }
                    }
                }
            }else{
                if (u2_e == null || u2_e == ""){
                    if(u1_m == u2_m){
                        if(u1_m!=null || u1_m!= ""){
                            isCommon = true
                        }
                    }
                }else if(u1_m == u2_m){
                    if(u1_m != null &&  u1_m !="") {
                        if(u2_m!=null && u2_m!="") {
                            isCommon = true
                        }
                    }
                }
            }

            //check if isCommon isn't already true to prevent a repeat addition of the common contact.
            if(!isCommon){
                if(u1_m == null || u1_m == ""){
                    if(u2_m == null || u2_m == ""){
                        if(u1_e == u2_e){
                            if(u1_e!=null || u1_e != ""){
                                isCommon = true
                            }
                        }
                    }
                }else{
                    if(u2_m != null && u2_m != ""){
                        if(u1_e == u2_e){
                            if(u1_e!= null && u1_e != ""){
                                isCommon = true
                            }
                        }
                    }else if(u1_e == u2_e){
                        isCommon = true
                    }
                }
            }

            if(isCommon){
                    resultArr = resultArr.concat(userContactsArr[i])
                    count++
                    break;
            }
        }
    }

    callBack(resultArr)
}

Contact.prototype.findProfileWithFieldsAsArray=function(user,fields,idType,callBack) {

    var query = {}

    if(idType == "emailId"){
        query = {emailId:user}
    }else if(idType == "userId"){
        query = {_id:user}
    }
    myUserCollection.find(query, fields, function (err, data) {
        if (err){
            callBack([])
        }
            //throw new err
        else {
            callBack(data)
        }
    })
}

Contact.prototype.getCommonContacts = function(userId, cUserId, project, callback) {

    var projectContact2 = {
        personEmailId: "$contacts.personEmailId",
        _id: "$contacts._id",
        mobileNumber: "$contacts.mobileNumber",
        designation: "$contacts.designation",
        personName: "$contacts.personName",
        personId: "$contacts.personId"
    };

    if (userId.toString() != cUserId.toString()) {

        myUserCollection.aggregate([{
            $match:{_id:userId}},
            {$unwind:"$contacts"},
            {$group:{_id:"$emailId",
                // contacts: { $push: projectContact2 }}},
                contacts: { $addToSet: "$contacts.personEmailId" }}}],
            function(error, userContacts1){

            if(!error && userContacts1.length>0){

                var userEmailContacts1 = [];
                if(userContacts1[0] && userContacts1[0].contacts && userContacts1[0].contacts.length>0){
                    userEmailContacts1 = userContacts1[0].contacts
                }
                var emailId1 = userContacts1[0]._id;

                if(userEmailContacts1.length>0){
                    myUserCollection.aggregate([{
                        $match:{_id:cUserId}},
                        {$unwind:"$contacts"},{
                            $match:{
                                "contacts.personEmailId": {$in:userEmailContacts1}
                            }
                        },
                        { "$skip": 0 },
                        { "$limit": 20 },
                        {$group:{_id:'$emailId',
                            contacts: { $push: projectContact2 }}}],
                        function(error2, userContacts2){

                        if(!error2 && userContacts2.length>0){

                            var commonContacts = [];
                            var emailId2 = userContacts2[0]._id;

                            _.each(userContacts2[0].contacts,function (contact) {
                                if(contact.personEmailId && contact.personEmailId != emailId2 && contact.personEmailId != emailId1){
                                    commonContacts.push(contact)
                                }
                            });

                            callback(commonContacts)

                        } else {
                            callback([])
                        }
                    });

                } else {
                    callback([])
                }

            } else {
                callback([])
            }
        });
    }
    else {
        callback([])
    }
};

Contact.prototype.getCommonContactsNonRelatas = function (userId,fetchWithId,castToObjectId, callback) {

    var matchWith;

    if(isNumber(fetchWithId)){
        matchWith = {"contacts.mobileNumber":fetchWithId};
    } else {
        matchWith = {"contacts.personEmailId":fetchWithId};
    }

    getRelatasUsersInUsersContactList(userId,function (relatasUsers) {

        var personIds = _.map(relatasUsers,function (c) {
            if(userId.toString() != c.contacts.personId.toString()){
                return castToObjectId(c.contacts.personId)
            }
        });

        personIds = _.compact(personIds)

        var queryFinal = { "$or": personIds.map(function(el) {
            if(el){
                var obj = {}
                obj["_id"] = el
                obj["contacts.personEmailId"] = fetchWithId
                return obj;
            }
        })};

        myUserCollection.aggregate([{
            $match: {_id:{$in:personIds}}
        },{
            $unwind:"$contacts"
        },{
            $match:queryFinal
        },{
            $project:{
                _id:"$_id",
                personId:"$_id",
                personEmailId:"$emailId",
                mobileNumber:"$mobileNumber",
                personName:{$concat: ["$firstName", " ", "$lastName"]}
            }
        }]).exec(function (err,results) {

            results = _.uniq(results, 'personEmailId');

            if(!err && results && results.length>0){
                callback(results)
            } else {
                callback([])
            }
        })
    });
}

Contact.prototype.getCommonContactsWithEmail = function(userId, emailId, contactEmailId, emailIdArr, callback) {
    var notIn = [contactEmailId, emailId, '',null].concat(emailIdArr)//emailID as null for a contact will not mess with common connections.

    myUserCollection.aggregate([{
        $match: {
            emailId: { $in: [emailId, contactEmailId] }
        }
    }, {
        $unwind: "$contacts"
    }, {
        $match: { "contacts.personEmailId": { $nin: notIn } }
    }, {
        $group: {
            _id: "$contacts.personEmailId",
            contacts: { $push: "$contacts" },
            count: { $sum: 1 }
        }
    }, {
        $match: {
            count: { $gt: 1 }
        }
    }, {
        $unwind: "$contacts"
    }, {
        $group: {
            _id: null,
            contacts: { $addToSet: "$contacts.personEmailId" }
        }
    }]).exec(function(err, data) {
        if (checkRequired(data) && data.length > 0 && data[0].contacts && data[0].contacts.length > 0) {
            getContactsInEmails(userId, data[0].contacts, false, function(err, contacts, emailIdArr) {
                callback(contacts, emailIdArr)
            });
        } else callback([], [])
    })
};

Contact.prototype.getCommonContactsEmailIdListQueryByUserIdAndCompanyName = function(userId, cUserId, companyName, callback) {
    var search = new RegExp(companyName, 'i');
    if (userId.toString() != cUserId.toString()) {
        myUserCollection.aggregate([{
            $match: {
                _id: { $in: [userId, cUserId] }
            }
        }, {
            $unwind: "$contacts"
        }, {
            $match: {
                "contacts.personId": { $nin: [userId.toString(), cUserId.toString()] },
                "contacts.personEmailId": { $nin: ["", " "] },
                "contacts.companyName": search
            }
        }, {
            $group: {
                _id: "$contacts.personEmailId",
                contacts: { $push: "$contacts" },
                count: { $sum: 1 }
            }
        }, {
            $match: {
                count: { $gt: 1 }
            }
        }, {
            $unwind: "$contacts"
        }, {
            $group: {
                _id: null,
                contacts: { $addToSet: "$contacts.personEmailId" }
            }
        }]).exec(function(err, data) {
            if (checkRequired(data) && data.length > 0 && data[0].contacts && data[0].contacts.length > 0) {
                callback(data[0].contacts)
            } else callback([])
        })
    } else callback([])
};

Contact.prototype.getCommonContactsWithEmailWithinCompany = function(userId, emailId, fetchWithId, companyName,liuCompanyEmailIdDomain, callback) {

    var search = new RegExp(liuCompanyEmailIdDomain, 'i');
    myUserCollection.aggregate([
        {
            $match: {
                emailId: search
            }
        },
        {
            $unwind: "$contacts"
        },
        {
            $match: { "contacts.personEmailId":  fetchWithId }
        },
        {
            $project: {
                "emailId": "$emailId",
                "company": "$companyName",
                "corporateUser": "$corporateUser"
            }
        }
    ]).exec(function(err, data) {

        var relatasUserCCs = _.pluck(data,'emailId');

        if (checkRequired(data) && data.length > 0 && relatasUserCCs && relatasUserCCs.length > 0) {
            getContactsInEmails(userId, relatasUserCCs, false, function(err, contacts, emailIdArr) {
                callback(contacts, emailIdArr)
            });
        } else callback([], []);
    })
};

Contact.prototype.getCommonContactsWithEmailWithinCompanyOld = function(userId, emailId, fetchWithId, companyName, callback) {

    var search = new RegExp(companyName, 'i');
    myUserCollection.aggregate([{
        $match: {
            emailId: { $in: [emailId, fetchWithId] }
        }
    }, {
        $unwind: "$contacts"
    }, {
        $match: { "contacts.personEmailId": { $nin: [emailId,fetchWithId]} }
    }, {
        $group: {
            _id: "$contacts.personEmailId",
            contacts: { $push: "$contacts" },
            count: { $sum: 1 }
        }
    }, {
        $match: {
            count: { $gt: 1 }
        }
    }, {
        $unwind: "$contacts"
    }, {
        $match: { "contacts.companyName": search }
    }, {
        $group: {
            _id: null,
            contacts: { $addToSet: "$contacts.personEmailId" }
        }
    }]).exec(function(err, data) {

        if (checkRequired(data) && data.length > 0 && data[0].contacts && data[0].contacts.length > 0) {
            getContactsInEmails(userId, data[0].contacts, false, function(err, contacts, emailIdArr) {
                callback(contacts, emailIdArr)
            });
        } else callback([], []);
    })
};

//Deprecated for Mobile -- Rajiv 050416
Contact.prototype.getPersonalNote = function(userId, cUserId, isEmailId, callback) {
    var q;
    if (isEmailId) {
        q = { "contacts.personEmailId": cUserId }
    } else {
        q = { "contacts.personId": cUserId }
    }

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: q
    }, {
        $project: {
            "contacts.favorite": 1,
            "contacts.relation": 1,
            "contacts.notes": 1
        }
    }]).exec(function(error, data) {
        callback(data)
    })
};

Contact.prototype.getNoteForUser = function(userId,req,idType,back){
    var query;
    if(idType == "emailId"){
        query = {"contacts.personEmailId" : req.query.id}
    }else if( idType == "userId"){
        query = {"contacts.personId":req.query.id}
    }else{
        query = {"contacts.mobileNumber" : req.query.mobileNumber}
    }

    myUserCollection.aggregate(
        [{
            $match:{_id:userId}
        },
        {
            $unwind:"$contacts"
        },
        {
            $match:query
        },
        {
            $project:{
                "contacts.favorite":1,
                "contacts.relation":1,
                "contacts.notes":1
            }
        }]
    ).exec(function(error,data){
            back(data)
        })
    }

Contact.prototype.getContactsInEmailIdArrUserId = function(userId, emailArr, callback) {
    getContactsInEmails(userId, emailArr, false, callback);
    };

function getContactsInEmails(userId, emailArr, project, callback) {
    var projectContact = {
        _id: "$contacts._id",
        personId: "$contacts.personId",
        personName: "$contacts.personName",
        personEmailId: "$contacts.personEmailId",
        companyName: "$contacts.companyName",
        designation: "$contacts.designation",
        mobileNumber: "$contacts.mobileNumber",
        contactRelation: "$contacts.contactRelation",
        favorite: "$contacts.favorite",
        twitterUserName: "$contacts.twitterUserName",
        facebookUserName: "$contacts.twitterUserName",
        linkedinUserName: "$contacts.twitterUserName",
        addedDate: "$contacts.addedDate",
        source: "$contacts.source",
        hashtag: "$contacts.hashtag",
        ownerId:"$_id"
    };
    if (project) {
        projectContact = project;
    }
    myUserCollection.aggregate([{
        $match: {
            _id: userId
        }
    }, {
        $unwind: "$contacts"
    }, {
        $match: { "contacts.personEmailId": { $in: emailArr } }
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: projectContact
            },
            count: { $sum: 1 }
        }
    }]).exec(function(err, data) {
        if (checkRequired(data) && data.length > 0) {
            var index = null;
            for (var i = 0; i < data.length; i++) {
                if (data[i]._id == true || data[i]._id == 'true') {
                    index = i;
                }
            }

            if (index != null) {
                myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'firstName lastName companyName designation profilePicUrl mobileNumber publicProfileUrl' }, function(err, list) {
                    if (!err && list) {
                        callback(null, concatAllContacts(data, list, true), emailArr)
                    } else
                        callback(null, concatAllContacts(data, [], true), emailArr)
                })
            } else callback(null, concatAllContacts(data, [], true), emailArr)

        } else
            callback(err, concatAllContacts([], [], true), emailArr)
    })
}

Contact.prototype.getContactsInEmailsWithEmailId = function(emailId, isArray, emailArr, callback) {
    getContactsInEmailsWithEmailId(emailId, isArray, emailArr, callback)
}

function getContactsInEmailsWithEmailId(emailId, isArr, emailArr, callback) {

    var q = {
        emailId: emailId
    };
    if (isArr) {
        q = {
            emailId: { $in: emailId }
        }
    }

    myUserCollection.aggregate([{
        $match: q
    }, {
        $unwind: "$contacts"
    }, {
        $match: { "contacts.personEmailId": { $in: emailArr } }
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    mobileNumber: "$contacts.mobileNumber",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag"
                }
            },
            count: { $sum: 1 }
        }
    }]).exec(function(err, data) {

        if (checkRequired(data) && data.length > 0) {
            var index = null;
            for (var i = 0; i < data.length; i++) {
                if (data[i]._id == true) {
                    index = i;
                }
            }

            if (index != null) {
                myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'firstName lastName companyName designation profilePicUrl mobileNumber' }, function(err, list) {
                    if (!err && list) {
                        callback(null, concatAllContacts2(data, list))
                    } else
                        callback(null, concatAllContacts2(data, []))
                })
            } else callback(null, concatAllContacts2(data, []))

        } else
            callback(err, concatAllContacts2([], []))
    })
}

Contact.prototype.getContactsEmailsExistInOther = function(userId, emailId, isArr, emailArr, callback) {

    var q = {
        emailId: emailId
    };
    if (isArr) {
        q = {
            emailId: { $in: emailId }
        }
    }

    myUserCollection.aggregate([{
        $match: q
    }, {
        $unwind: "$contacts"
    }, {
        $match: { "contacts.personEmailId": { $in: emailArr } }
    }, {
        $group: {
            _id: null,
            emails: {
                $addToSet: "$contacts.personEmailId"
            },
            count: { $sum: 1 }
        }
    }]).exec(function(err, data) {
        if (checkRequired(data) && data.length > 0 && data[0] && data[0].emails && data[0].emails.length > 0) {
            getContactsInEmails(userId, data[0].emails, callback)
        } else
            callback(null, [])
    })
}

/* UPDATE CONTACTS */
Contact.prototype.updateFavorite = function(userId, contactId, status, callback) {

    var updateObj = {
        'contacts.$.doNotTrackForFavorite':true,
        'contacts.$.favorite':status
    }

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: { _id: contactId } } }, { $set: updateObj }, function(err, result) {
        if (err) {
            callback(err, false);
        } else if (result && result.ok) {
            callback(err, true);
        } else callback(err, false)
    })
};

Contact.prototype.updateFavoriteByEmailId = function(userId, emailId, status, callback) {

    var updateObj = {
        'contacts.$.doNotTrackForFavorite':true,
        'contacts.$.favorite':status
    }

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: { personEmailId: emailId } } }, { $set: updateObj }, function(err, result) {
        if (err) {
            callback(err, false);
        } else if (result && result.ok) {
            callback(err, true);
        } else callback(err, false)
    })
};

Contact.prototype.updateFavoriteForMultiUsers = function(userGroup,callback) {

    if(userGroup.length>0){
        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();

        _.each(userGroup,function (el) {
            _.each(el.contacts,function (co) {
                bulk.find({ emailId: el.emailId, "contacts.personEmailId": co }).update({ $set: { "contacts.$.favorite": true } });
            })
        });

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updateFavoriteForMultiUsers():Contact Bulk', err)
            } else {
                result = result.toJSON();
                logger.info('updateFavoriteForMultiUsers() results ', result);
            }

            if (callback) { callback(err, result) }
        });
    }
};

Contact.prototype.updatePersonNameForMultiUsers = function(userGroup,callback) {

    if(userGroup.length>0){
        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();

        _.each(userGroup,function (el) {

            _.each(el.contacts,function (co) {
                bulk.find({ emailId: el._id, "contacts.personEmailId": co }).update({ $set: { "contacts.$.personName": co } });
            })
        });

        // bulk.execute(function(err, result) {
        //     if (err) {
        //         logger.info('Error in updatePersonNameForMultiUsers():Contact Bulk', err)
        //     } else {
        //         result = result.toJSON();
        //         logger.info('updatePersonNameForMultiUsers() results ', result);
        //     }
        //
        //     if (callback) { callback(err, result) }
        // });

        callback(true)
    } else {
        callback("Nothing to update")
    }
};

Contact.prototype.updateRelationshipForMultiUsers = function(userGroup, callback) {

    if(userGroup.length>0){
        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();

        _.each(userGroup,function (el) {

            if(el.contacts.partners.length>0){
                _.each(el.contacts.partners,function (co) {
                    bulk.find({ emailId: el.emailId, "contacts.personEmailId": co }).update({ $set: { "contacts.$.contactRelation.partner": true,"contacts.$.favorite": true } });
                })
            }

            if(el.contacts.dms.length>0){
                _.each(el.contacts.dms,function (co) {
                    bulk.find({ emailId: el.emailId, "contacts.personEmailId": co }).update({ $set: { "contacts.$.contactRelation.decision_maker": true,"contacts.$.favorite": true } });
                })
            }

            if(el.contacts.influencers.length>0){
                _.each(el.contacts.influencers,function (co) {
                    bulk.find({ emailId: el.emailId, "contacts.personEmailId": co }).update({ $set: { "contacts.$.contactRelation.influencer": true,"contacts.$.favorite": true } });
                })
            }

            if(el.contacts.customers.length>0){
                _.each(el.contacts.customers,function (co) {
                    bulk.find({ emailId: el.emailId, "contacts.personEmailId": co }).update({ $set: { "contacts.$.contactRelation.prospect_customer": 'customer',"contacts.$.favorite": true } });
                })
            }

            if(el.contacts.prospects.length>0){
                _.each(el.contacts.prospects,function (co) {
                    bulk.find({ emailId: el.emailId, "contacts.personEmailId": co }).update({ $set: { "contacts.$.contactRelation.prospect_customer": 'prospect',"contacts.$.favorite": true } });
                })
            }
        });

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updateRelationshipForMultiUsers():Contact Bulk', err)
            } else {
                result = result.toJSON();
                logger.info('updateRelationshipForMultiUsers() results ', result);
            }

            if (callback) { callback(err, result) }
        });
    }
};

/* UPDATE CONTACTS WATCHLIST*/
Contact.prototype.updateWatchlist = function(userId, contactId, status,type,callback) {

    var obj = { "contacts.$.watchThisContact": status }
    if(type === 'companies'){
        obj = { "contacts.$.account.watchThisAccount": status }
    }

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: { _id: contactId } } }, { $set: obj }, function(err, result) {
        if (err) {
            callback(err, false);
        } else if (result && result.ok) {
            callback(err, true);
        } else callback(err, false)
    })
};

/*Update the response time stamp for email sent*/
Contact.prototype.updateResponseTimeStamp = function(userId, receiverEmailId, respSentTime, isLeadTrack) {

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: { personEmailId: receiverEmailId } } }, { $set: { "contacts.$.respSentTime": respSentTime } }, function(err, result) {

        if (err) {
            console.log(err);
        } else if (result && result.ok) {

            //If Homelane, delete Hashtag 'open' and create a 'closed' hashtag
            if (isLeadTrack) {

                var open = 'open';
                deleteHashtag(userId, receiverEmailId, open, function(err, result) {
                    if (err) {
                        logger.info('Error in deleteHashtag(); ', err);
                    }
                });

                var close = 'closed';
                updateHashtag(userId, receiverEmailId, close, function(err, result) {
                    if (err) {
                        logger.info('Error in updateHashtag(); ', err);
                    }
                });

            }
        } else console.log(false)
    })
};

/*Hashtag Create*/

/*Creates a hashtag for the user if not already exists. Can't create two same hashtags*/
Contact.prototype.addHashtag = function(userId, contactId, hashtag, callback) {

    var match = { _id: contactId };

    if(validateEmail(contactId)){
        match = { personEmailId: contactId }
    }

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: match } }, { $addToSet: { "contacts.$.hashtag": hashtag } }, function(err, result) {
        if (err) {
            callback(err, false);
        } else if (result && result.ok) {
            callback(err, true);
        } else callback(err, false)
    })
};

/*Deletes a Hashtag*/

Contact.prototype.deleteHashtag = function(userId, contactId, hashtag, callback) {

    var match = { _id: contactId };

    if(validateEmail(contactId)){
        match = { personEmailId: contactId }
    }

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: match } }, { $pull: { "contacts.$.hashtag": hashtag } }, function(err, result) {
        if (err) {
            callback(err, false);
        } else if (result && result.ok) {
            callback(err, true);
        } else callback(err, false)
    })
};

Contact.prototype.updateRelationshipStrength = function(userId, contactId, relationshipStrength_updated, callback) {

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: { _id: contactId } } }, { $set: { "contacts.$.relationshipStrength_updated": relationshipStrength_updated } }, function(err, result) {
        if (err) {
            callback(err, false);
        } else if (result && result.ok) {
            callback(err, true);
        } else callback(err, false)
    })
};

Contact.prototype.updateRelationshipType = function(userId, contactId, relationKey, type,value, callback) {
    var updateObj;

    if (relationKey == 'prospect_customer') {

        if(type == 'lead'){
            updateObj = {
                "contacts.$.contactRelation.prospect_customer": type,
                "contacts.$.stageDate.lead": new Date()
            }

        } else if(type == 'prospect'){
            updateObj = {
                "contacts.$.contactRelation.prospect_customer": type,
                "contacts.$.stageDate.prospect": new Date()
            }
        } else if(type == 'customer'){
            updateObj = {
                "contacts.$.contactRelation.prospect_customer": type,
                "contacts.$.stageDate.customer": new Date()
            }
        } else {
            updateObj = {
                "contacts.$.contactRelation.prospect_customer": type
            }
        }
    } else if (relationKey == 'decisionmaker_influencer') {
        updateObj = { "contacts.$.contactRelation.decisionmaker_influencer": value }
    }


    var updateObj2 = {
        "contactRelation.prospect_customer":type
    }

    if(type == "partner") {
        updateObj = { "contacts.$.contactRelation.partner": value }
        updateObj2 = {
            "contactRelation.partner":value
        }
    }
    if(type == "decision_maker") {
        updateObj = { "contacts.$.contactRelation.decision_maker": value }
        updateObj2 = {
            "contactRelation.decision_maker":value
        }
    }
    if(type == "influencer") {
        updateObj = { "contacts.$.contactRelation.influencer": value }
        updateObj2 = {
            "contactRelation.influencer":value
        }
    }

    if(!callback){
        callback = value;
    }

    if (checkRequired(updateObj)) {
        myUserCollection.update({ _id: userId, contacts: { $elemMatch: { _id: contactId } } }, { $set: updateObj }, function(err, result) {
            myUserCollection.find({ _id: userId, contacts: { $elemMatch: { _id: contactId } } },{ "contacts.$": 1 },function (err1,contact) {

                if(contact && contact[0] && contact[0].contacts){
                    contactsCollection.update({ ownerId: userId, personEmailId: contact[0].contacts[0].personEmailId}, { $set: updateObj2 },{multi:true}, function(err, result) {

                    });
                }
            });

            if (err) {
                callback(err, false);
            } else if (result && result.ok) {
                callback(err, true);
            } else callback(err, false)
        })
    } else callback(null, false);
};

Contact.prototype.updateRelationshipPartner = function(userId, contactId, relationKey, type,value, callback) {

    var updateObj = { "contacts.$.contactRelation.decision_maker": value }
    var updateObj2 = {
        "contactRelation.decision_maker":value
    }

    if(type == "influencer"){
        updateObj = { "contacts.$.contactRelation.influencer": value }
        updateObj2 = {
            "contactRelation.influencer":value
        }

    } else if(type == "partner"){
        updateObj = { "contacts.$.contactRelation.partner": value }
        updateObj2 = {
            "contactRelation.partner":value
        }
    }

    if (checkRequired(updateObj)) {
        myUserCollection.update({ _id: userId, contacts: { $elemMatch: { _id: contactId } } }, { $set: updateObj }, function(err, result) {

            if (err) {
                callback(err, false);
            } else if (result && result.ok) {

                myUserCollection.find({ _id: userId, contacts: { $elemMatch: { _id: contactId } } },{ "contacts.$": 1 },function (err1,contact) {

                    if(contact && contact[0] && contact[0].contacts){
                        contactsCollection.update({ ownerId: userId, personEmailId: contact[0].contacts[0].personEmailId}, { $set: updateObj2 },{multi:true}, function(err, result) {

                        });
                    }
                });

                myUserCollection.findOne({ _id: userId },{ "contacts": 1 },function (err,contacts) {
                    if (!err && contacts) {
                        updateContactRelevance(userId,contacts.contacts,function (cErr,response) {

                        });
                    }
                });

                callback(err, true);
            } else callback(err, false)
        })
    } else {
        callback(null, false);
    }
};

Contact.prototype.updateCXO = function(userId,contactId,value,callback) {

    var updateObj = { "contacts.$.contactRelation.cxo": value }

    if (checkRequired(updateObj)) {
        myUserCollection.update({ _id: userId, contacts: { $elemMatch: { _id: contactId } } }, { $set: updateObj }, function(err, result) {

            myUserCollection.find({ _id: userId, contacts: { $elemMatch: { _id: contactId } } },{ "contacts.$": 1 },function (err1,contact) {

                if(contact && contact[0] && contact[0].contacts){
                    contactsCollection.update({ ownerId: userId, personEmailId: contact[0].contacts[0].personEmailId}, { $set: {"contactRelation.cxo":value} },{multi:true}, function(err, result) {

                    });
                }
            });

            if (err) {
                callback(err, false);
            } else if (result && result.ok) {
                callback(err, true);
            } else callback(err, false)
        })
    } else {
        callback(null, false);
    }
};

Contact.prototype.updateContactRelevance = updateContactRelevance

function getContactsGroupedByRelation(contacts,callback) {

    var allContacts = [];
    var obj = {
        important:[],
        partners:[],
        decisionMakers:[],
        influencers:[],
        others:[]
    };

    if(contacts.length>0){
        _.each(contacts,function (co) {

            if(co.personEmailId){

                allContacts.push(co.personEmailId)

                if(co.favorite){
                    obj.important.push(contactFormatSchema(co.personEmailId))
                }

                if(co.contactRelation && co.contactRelation.decision_maker){
                    obj.decisionMakers.push(contactFormatSchema(co.personEmailId))
                }

                if(co.contactRelation && co.contactRelation.influencer){
                    obj.influencers.push(contactFormatSchema(co.personEmailId))
                }

                if(co.contactRelation && co.contactRelation.partner){
                    obj.partners.push(contactFormatSchema(co.personEmailId))
                }

                if(!co.contactRelation || (!co.favorite && co.contactRelation && !co.contactRelation.decision_maker && !co.contactRelation.influencer && !co.contactRelation.partner)){
                    obj.others.push(contactFormatSchema(co.personEmailId))
                }
            }
        })
    }

    callback(allContacts,obj);
}

function contactFormatSchema(contact) {

    return{
        emailId:contact,
        interactionCounter:0
    }
}

function updateContactRelevance (userId,contacts,callback) {

    var startDate = moment(new Date()).startOf('month');
    getContactsGroupedByRelation(contacts,function (allContacts,contactsGrouped) {
        getInteractionCount(userId,allContacts,startDate,function (ierr,interactions,monthYear) {
            if(!ierr && interactions && interactions.length>0){
                formatDataForStorage(contactsGrouped,interactions,function (data) {

                    userRelationCollection.findOne({userId:userId,monthYear:monthYear},function (err,result) {

                        var toUpdateRr = null;

                        _.each(data,function (dt) {

                            if(dt.monthYear == monthYear){
                                dt.userId = userId;
                                dt.userEmailId = !err && result?result.userEmailId:null;
                                toUpdateRr = dt
                            }
                        });

                        userRelationCollection.remove({userId:userId,monthYear:monthYear},function (err,rmResult) {
                            if(!err){
                                userRelationCollection.collection.insert(toUpdateRr,function (err,insertResult) {
                                })
                            }
                        });
                    });
                });
            }
        })
    });

}

function getInteractionCount(userId,allContacts,startDate,callback) {

    interaction.aggregate([{
            $match: {
                ownerId:userId,
                userId:{$ne:userId},
                action:"receiver",
                emailId:{$in:allContacts},
                interactionDate:{$gte:new Date(startDate),$lte: new Date()}
            }
        },
        {
            $project:{
                emailId:"$emailId",
                month:{$month:"$interactionDate"},
                year:{$year:"$interactionDate"}
            }
        },
        {
            $group: {
                _id: {
                    emailId:"$emailId",
                    month:"$month",
                    year:"$year"
                },
                count: {$sum: 1}
            }
        }]).exec(function(error, data) {
        if(!error && data && data.length>0){
            var monthYear = data[0]._id.month+""+data[0]._id.year;
            callback(error,data,monthYear)
        } else {
            callback(error,null,null)
        }
    })
}

function formatDataForStorage(contactsGrouped,interactions,callback) {

    var data =  _
        .chain(interactions)
        .groupBy(function (value) {
            return value._id.month+""+value._id.year
        })
        .map(function(values, key) {
            return attachData(contactsGrouped,values,key)
        })
        .value();

    callback(data);
}

function attachData(contactsGrouped,values,key){

    var obj = {
        monthYear:key,
        important:{
            contacts:contactsGrouped.important.length,
            interactionCount:0
        },
        partners:{
            contacts:contactsGrouped.partners.length,
            interactionCount:0
        },
        decisionMakers:{
            contacts:contactsGrouped.decisionMakers.length,
            interactionCount:0
        },
        influencers:{
            contacts:contactsGrouped.influencers.length,
            interactionCount:0
        },
        others:{
            contacts:contactsGrouped.others.length,
            interactionCount:0
        }
    };

    if(contactsGrouped.important.length){
        _.each(contactsGrouped.important,function (co) {
            _.each(values,function (va) {
                if(co.emailId == va._id.emailId){
                    obj.important.interactionCount = va.count+obj.important.interactionCount
                }
            })
        })
    }

    if(contactsGrouped.partners.length){
        _.each(contactsGrouped.partners,function (co) {
            _.each(values,function (va) {
                if(co.emailId == va._id.emailId){
                    if(co.emailId == va._id.emailId){
                        obj.partners.interactionCount = va.count+obj.partners.interactionCount
                    }
                }
            })
        })
    }

    if(contactsGrouped.decisionMakers.length){
        _.each(contactsGrouped.decisionMakers,function (co) {
            _.each(values,function (va) {
                if(co.emailId == va._id.emailId){
                    if(co.emailId == va._id.emailId){
                        obj.decisionMakers.interactionCount = va.count+obj.decisionMakers.interactionCount
                    }
                }
            })
        })
    }

    if(contactsGrouped.influencers.length){
        _.each(contactsGrouped.influencers,function (co) {
            _.each(values,function (va) {
                if(co.emailId == va._id.emailId){
                    if(co.emailId == va._id.emailId){
                        obj.influencers.interactionCount = va.count+obj.influencers.interactionCount
                    }
                }
            })
        })
    }

    if(contactsGrouped.others.length){
        _.each(contactsGrouped.others,function (co) {
            _.each(values,function (va) {
                if(co.emailId == va._id.emailId){
                    if(co.emailId == va._id.emailId){
                        obj.others.interactionCount = va.count+obj.others.interactionCount
                    }
                }
            })
        })
    }

    return obj
}

Contact.prototype.updateRelationshipTypeByEmail = function(userId, email, relationKey, type, callback) {
    var updateObj;
    if (relationKey == 'prospect_customer') {
        updateObj = { "contacts.$.contactRelation.prospect_customer": type,"contacts.$.stageDate.lead": new Date() }
    } else if (relationKey == 'decisionmaker_influencer') {
        updateObj = { "contacts.$.contactRelation.decisionmaker_influencer": type,"contacts.$.stageDate.lead": new Date() }
    }

    if (checkRequired(updateObj)) {
        myUserCollection.update({ _id: userId, contacts: { $elemMatch: { personEmailId: email } } }, { $set: updateObj },{ multi: true }, function(err, result) {

            if (err) {
                callback(err, false);
            } else if (result && result.ok) {
                callback(err, true);
            } else callback(err, false)
        })
    } else callback(null, false);
};

Contact.prototype.insertNote = function(userId, contactId, noteText, callback) {
    myUserCollection.update({ _id: userId, "contacts._id": contactId }, { $push: { "contacts.$.notes": { text: noteText, createdDate: new Date() } } }, function(err, result) {
        if (err) {
            callback(err, false);
        } else if (result && result.ok) {
            callback(err, true);
        } else callback(err, false)
    })
};

Contact.prototype.addSingleContactNotExist = function(userId, contacts, callback) {

    if (typeof contacts.personEmailId == 'string') {
        contacts.personEmailId = contacts.personEmailId.trim().toLowerCase();
    }

    var queryFind2 = { _id: userId, contacts: { $elemMatch: { $or: [] } } };

    var exist = false;

    if (checkRequired(contacts.personId)) {
        exist = true;
        queryFind2.contacts.$elemMatch.$or.push({ personId: contacts.personId })
    }

    if (checkRequired(contacts.personEmailId)) {
        exist = true
        queryFind2.contacts.$elemMatch.$or.push({ personEmailId: contacts.personEmailId })
    }

    if (checkRequired(contacts.mobileNumber)) {
        exist = true;
        queryFind2.contacts.$elemMatch.$or.push({ mobileNumber: contacts.mobileNumber })
    }

    if (exist) {
        myUserCollection.findOne(queryFind2, { "contacts.$": 1 }, function(error, contactInfo) {

            if (error) {
                if (checkRequired(callback))
                    callback(error, false, { message: 'Search contact failed ' })
            } else {
                //Add single contact if not exists

                if (validateContactInfo(contactInfo, '')) {
                    addContact(userId, contacts, callback);

                } else {
                    logger.info("Contacts Exists");
                }
            }
        })
    }
};

Contact.prototype.addSingleContact = function(userId, contacts, callback) {

    if (typeof contacts.personEmailId == 'string') {
        contacts.personEmailId = contacts.personEmailId.trim().toLowerCase();
    }

    var queryFind2 = { _id: userId, contacts: { $elemMatch: { $or: [] } } };

    var exist = false;

    if (checkRequired(contacts.personId)) {
        exist = true;
        queryFind2.contacts.$elemMatch.$or.push({ personId: contacts.personId })
    }

    if (checkRequired(contacts.personEmailId)) {
        exist = true
        queryFind2.contacts.$elemMatch.$or.push({ personEmailId: contacts.personEmailId })
    }

    if (checkRequired(contacts.mobileNumber)) {
        exist = true;
        queryFind2.contacts.$elemMatch.$or.push({ mobileNumber: contacts.mobileNumber })
    }

    if (exist) {
        myUserCollection.findOne(queryFind2, { "contacts.$": 1 }, function(error, contactInfo) {

            if (error) {
                if (checkRequired(callback))
                    callback(error, false, { message: 'Search contact failed ' })
            } else {
                if (validateContactInfo(contactInfo, '')) {
                    if (!checkRequired(contacts.source)) {
                        //contacts.source = 'relatas'
                    }
                    contacts.personName = contacts.personEmailId;

                    if (contacts.hashtagThis) {
                        contacts.hashtag = ['open'];
                    }

                    addContact(userId, contacts, callback)

                } else {

                    updateContact(userId, contacts, false);

                    var open = 'open';

                    if (contacts.hashtagThis) {
                        updateHashtag(userId, contacts.personEmailId, open, function(err, result) {
                            if (err) {
                                logger.info('Error in updateHashtag(); ', err);
                            }
                        });
                    }

                    if (checkRequired(callback))
                        callback(error, 'exist', { message: 'Contact already exist' })
                }
            }
        })
    }
};

function updateHashtag(userId, personEmailId, hashtag, callback) {

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: { personEmailId: personEmailId } } }, { $addToSet: { "contacts.$.hashtag": hashtag } }, function(err, result) {
        if (err) {
            callback(err, false);
        } else if (result && result.ok) {
            callback(err, true);
        } else callback(err, false)
    })
}

function updateContact(userId, contacts, isMobile) {

    var exist = false;

    var updateObj = { $set: {} }
    if (checkRequired(contacts.birthday)) {
        exist = true;
        updateObj.$set["contacts.$.birthday"] = contacts.birthday
    }

    //This is for HomeLane Feature

    if (checkRequired(contacts.notificationStartTime)) {
        exist = true;
        updateObj.$set["contacts.$.notificationStartTime"] = contacts.notificationStartTime
        updateObj.$set["contacts.$.respLimit"] = contacts.respLimit
    }

    if (checkRequired(contacts.companyName)) {
        exist = true;
        updateObj.$set["contacts.$.companyName"] = contacts.companyName
    }

    if (checkRequired(contacts.designation)) {
        exist = true;
        updateObj.$set["contacts.$.designation"] = contacts.designation
    }

    if (checkRequired(contacts.mobileNumber)) {
        exist = true;
        updateObj.$set["contacts.$.mobileNumber"] = contacts.mobileNumber
    }

    if (checkRequired(contacts.source)) {
        exist = true;
        updateObj.$set["contacts.$.source"] = contacts.source
    }

    if (checkRequired(contacts.personId)) {
        exist = true;
        updateObj.$set["contacts.$.personId"] = contacts.personId
    }

    if (checkRequired(contacts.personName)) {
        exist = true;
        updateObj.$set["contacts.$.personName"] = contacts.personName
    }

    if (checkRequired(contacts.personEmailId)) {
        exist = true;
        updateObj.$set["contacts.$.personEmailId"] = contacts.personEmailId
    }

    if (checkRequired(contacts.skypeId)) {
        exist = true;
        updateObj.$set["contacts.$.skypeId"] = contacts.skypeId
    }

    if (checkRequired(contacts.count) && contacts.count > 0) {
        exist = true;
        updateObj.$inc = {}
        updateObj.$inc["contacts.$.count"] = contacts.count || 0
    }

    if (checkRequired(contacts.lastInteracted)) {
        exist = true;
        updateObj.$set["contacts.$.lastInteracted"] = contacts.lastInteracted
    }

    if (checkRequired(contacts.verified)) {
        exist = true;
        updateObj.$set["contacts.$.verified"] = contacts.verified
    }

    if (checkRequired(contacts.relatasUser)) {
        exist = true;
        updateObj.$set["contacts.$.relatasUser"] = contacts.relatasUser
    }

    if (checkRequired(contacts.relatasContact)) {
        exist = true;
        updateObj.$set["contacts.$.relatasContact"] = contacts.relatasContact
    }

    if (checkRequired(contacts.location)) {
        exist = true;
        updateObj.$set["contacts.$.location"] = contacts.location
    }

    if (checkRequired(contacts.relation)) {
        if (checkRequired(contacts.relation.prospect_customer)) {
            updateObj.$set["contacts.$.relation.prospect_customer"] = contacts.relation.prospect_customer
            exist = true;
        }
        if (checkRequired(contacts.relation.decisionmaker_influencer)) {
            updateObj.$set["contacts.$.relation.decisionmaker_influencer"] = contacts.relation.decisionmaker_influencer
            exist = true;
        }
    }

    var queryFind = { _id: userId, contacts: { $elemMatch: { $or: [] } } };
    var exist2 = false;

    if (checkRequired(contacts.personId)) {
        exist2 = true;
        queryFind.contacts.$elemMatch.$or.push({ personId: contacts.personId })
    }

    if (checkRequired(contacts.personEmailId)) {
        exist2 = true;
        queryFind.contacts.$elemMatch.$or.push({ personEmailId: contacts.personEmailId })
    }

    if (checkRequired(contacts.mobileNumber)) {
        exist2 = true;
        queryFind.contacts.$elemMatch.$or.push({ mobileNumber: contacts.mobileNumber })
    }

    if (exist && exist2) {
        myUserCollection.update(queryFind, updateObj, function(error, result) {
            if (error) {
                logger.info('Error in addSingleContact().updateContact():Contact ', error);
            }
        });
    }
}

function deleteHashtag(userId, personEmailId, hashtag, callback) {

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: { personEmailId: personEmailId } } }, { $pull: { "contacts.$.hashtag": hashtag } }, function(err, result) {
        if (err) {
            callback(err, false);
        } else if (result && result.ok) {
            callback(err, true);
        } else callback(err, false)
    })
}

function addContact(userId, contact, callback) {

    myUserCollection.update({ _id: userId }, { $push: { contacts: contact } }, function(error, result) {
        if (error) {
            if (checkRequired(callback))
                callback(error, false, { message: 'Adding contact failed new' })
        } else if (result && result.ok) {
            if (checkRequired(callback))
                callback(contact, true, { message: 'Adding contact success new' })

        } else {
            if (checkRequired(callback))
                callback(error, false, { message: 'Adding contact failed new' })
        }
    });
}

function validateContactInfo(contactInfo, contact) {
    if (checkRequired(contactInfo)) {
        if (checkRequired(contactInfo.contacts[0])) {
            return false;
        } else return true;
    } else return true;
}

Contact.prototype.getContactImageLinks = function (userId,emailIdArr,callback) {
    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: {
            "contacts.personEmailId": {$in: emailIdArr},
        }
    },{
        $group: {
            _id: "$contacts.personEmailId",
            contactImageLink: { $first: "$contacts.contactImageLink" }
        }
    }]).exec(function(error, data) {
        if(!error && data.length>0){
            callback(data)
        } else {
            callback([])
        }
    })
}

Contact.prototype.getAllNonImportantContacts = function(userIds,callback) {

    myUserCollection.find({_id:{$in:userIds}},{contacts:1}).lean().exec(function (err,results) {
        callback(err,results)
    })
}

Contact.prototype.getAllImportantContacts = function(userIds, callback) {

    myUserCollection.aggregate([
        {
            $match:{
                _id: {$in:userIds}
            }
        },{
            $unwind:"$contacts"
        },{
            $match:{ 'contacts.favorite':true
                // $or:[
                //     {'contacts.contactRelation.prospect_customer':{$ne:null}},
                //     {'contacts.contactRelation.decisionmaker_influencer':{$ne:null}},
                //     {'contacts.contactRelation.influencer':true},
                //     {'contacts.contactRelation.decision_maker':true},
                //     {'contacts.contactRelation.partner':true}
                // ]
            }
        },{
            $project:{
                personEmailId:"$contacts.personEmailId",
                emailId:"$emailId",
                personId:"$contacts.personId",
                contactRelation:"$contacts.contactRelation",
                personName:"$contacts.personName"
            }
        },{
            $group:{
                _id:"$emailId",
                contacts:{
                    $addToSet: "$personEmailId"
                },
                fullContactDetails:{
                    $addToSet: {
                        personEmailId:"$personEmailId",
                        personId:"$personId",
                        contactRelation:"$contactRelation",
                        personName:"$personName"
                    }
                }
            }
        },{
            $project:{
                userEmailId:"$_id",
                contacts:"$contacts",
                fullContactDetails:"$fullContactDetails"
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    })
}

Contact.prototype.getImportantContactsLtWith = function(userIds,userContactGroup, callback) {

    var queryFinal = { "$or": userContactGroup.map(function(el) {
        var obj = {}
        obj["emailId"] = el.ownerEmailId
        obj["contacts.favorite"] = true
        obj["contacts.personEmailId"] = { "$in": el.contacts };

        return obj;
    })};

    myUserCollection.aggregate([
        {
            $match:{
                _id: {$in:userIds}
            }
        },{
            $unwind:"$contacts"
        },{
            $match:queryFinal
        },{
            $project:{
                personEmailId:"$contacts.personEmailId",
                emailId:"$emailId",
                personId:"$contacts.personId",
                contactRelation:"$contacts.contactRelation",
                personName:"$contacts.personName"
            }
        },{
            $group:{
                _id:"$emailId",
                contacts:{
                    $addToSet: {
                        personEmailId:"$personEmailId",
                        personId:"$personId",
                        contactRelation:"$contactRelation",
                        personName:"$personName"
                    }
                }
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    })
}

Contact.prototype.getContactsWithLIUEmailId = function(userEmailIds,callback) {

    myUserCollection.aggregate([{
        $match: { emailId: {$in:userEmailIds} }
    }, {
        $unwind: "$contacts"
    },{
        $match:{
            "contacts.personName":{$in:userEmailIds},
            "contacts.personEmailId":{$nin:userEmailIds},
        }
    },{
        $group:{
            _id:"$emailId",
            contacts:{
                $push:"$contacts.personEmailId"
            },
            contactsName:{
                $push:"$contacts.personName"
            }
        }
    }]
    ).exec(function(error, data) {
        callback(error,data)
    })
}

Contact.prototype.getAllFavoriteContactsEmailIds = function(userId, emailId, mobileNumber,callback) {
    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: {
            "contacts.favorite": true,
            "contacts.personEmailId": { $nin: ['', emailId] },
            "contacts.mobileNumber": { $nin: ['', mobileNumber] }
        }
    }, {
        $group: {
            _id: null,
            emailIdArr: {
                $addToSet: "$contacts.personEmailId"
            },
            mobileNumberArr: {
                $addToSet: "$contacts.mobileNumber"
            }
        }
    }]).exec(function(error, data) {

        if (checkRequired(data) && data.length > 0 && data[0] && data[0].emailIdArr && data[0].emailIdArr.length > 0 || (data[0] && data[0].mobileNumberArr && data[0].mobileNumberArr.length > 0)) {

            var emailIdArr = _.compact(data[0].emailIdArr)
            var mobileNumberArr = _.compact(data[0].mobileNumberArr)

            callback(emailIdArr,mobileNumberArr);
        } else
            callback([])
    })
};

Contact.prototype.getContactsMatchedLocation = function(userId, emailId, location, designation, callback) {

    var search = new RegExp(location, 'i');
    var match = {
        "contacts.location": search
    };
    if (checkRequired(emailId)) {
        match["contacts.personEmailId"] = { $ne: emailId }
    }
    if (checkRequired(designation)) {
        match["contacts.designation"] = new RegExp(designation, 'i');
    }

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: match
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag"
                }
            },
            userIds: {
                $addToSet: "$contacts.personId"
            },
            emailIdList: {
                $addToSet: "$contacts.personEmailId"
            },
            count: {
                $sum: 1
            }
        }
    }]).exec(function(error, data) {
        if (checkRequired(data) && data.length > 0) {
            var emailIdArr = []
            var index = null;
            for (var i = 0; i < data.length; i++) {
                emailIdArr = emailIdArr.concat(data[i].emailIdList)
                if (data[i]._id == true) {
                    index = i;
                }
            }
            if (index != null) {
                myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'firstName lastName companyName designation profilePicUrl publicProfileUrl mobileNumber' }, function(err, list) {
                    if (!err && list) {
                        callback(null, concatAllContacts(data, list), data[index].userIds, emailIdArr)
                    } else
                        callback(null, concatAllContacts(data, []), data[index].userIds, emailIdArr)
                })
            } else callback(null, concatAllContacts(data, []), [])

        } else
            callback(error, concatAllContacts([], []), [])
    })
};

Contact.prototype.getContactsMatchedLocationByEmailIdArr = function(userId, emailIdArr, location, callback) {
    var search = new RegExp(location, 'i');
    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: {
            "contacts.location": search,
            "contacts.personEmailId": { $in: emailIdArr }
        }
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag"
                }
            }
        }
    }]).exec(function(error, data) {

        if (checkRequired(data) && data.length > 0) {
            var index = null;
            for (var i = 0; i < data.length; i++) {
                if (data[i]._id == true) {
                    index = i;
                }
            }
            if (index != null) {
                myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'firstName lastName companyName designation profilePicUrl mobileNumber' }, function(err, list) {
                    if (!err && list) {
                        callback(null, concatAllContacts(data, list), data[index].userIds)
                    } else
                        callback(null, concatAllContacts(data, []), data[index].userIds)
                })
            } else callback(null, concatAllContacts(data, []), [])

        } else
            callback(error, concatAllContacts([], []), [])
    })
};

Contact.prototype.getContactsMatchingLocations = function (userId,locationArray,participants,callback) {

    var queryFinal = { "$or": locationArray.map(function(el) {
        el["contacts.location"] = {'$regex' : '.*' + el.loc + '.*',"$options": 'i'};
        el["contacts.personEmailId"] = {$nin:participants}
        delete  el.loc
        return el;
    })};

    if(locationArray.length>0 && participants.length){

        myUserCollection.aggregate([{
            $match: { _id: userId }
        }, {
            $unwind: "$contacts"
        }, {
            $match: queryFinal
        }, {
            $group: {
                _id: "$emailId",
                contacts: {
                    $push: {
                        _id: "$contacts._id",
                        personId: "$contacts.personId",
                        personName: "$contacts.personName",
                        personEmailId: "$contacts.personEmailId",
                        companyName: "$contacts.companyName",
                        designation: "$contacts.designation",
                        mobileNumber: "$contacts.mobileNumber",
                        contactRelation: "$contacts.contactRelation",
                        favorite: "$contacts.favorite",
                        contactImageLink: "$contacts.contactImageLink",
                        location: "$contacts.location"
                    }
                }
            }
        }]).exec(function(error, data) {

            if(!error){
                callback(error,data)
            } else {
                logger.info("An error occurred in getContactsMatchingLocations();", error)
                callback(error,[])
            }
        });
    } else {
        callback({},[])
    }
}

Contact.prototype.getContactsMatchingLocationsHashtag = function (userId,locations,participants,callback) {

    var queryHashtag = { "$or": locations.map(function(el) {
        // el["contacts.hashtag"] = '/' + el.loc + '/i';
        el["contacts.hashtag"] = new RegExp(el.loc, 'i');
        el["contacts.personEmailId"] = {$nin:participants}
        delete  el.loc
        return el;
    })};

    if(locations.length>0 && participants.length){

        myUserCollection.aggregate([{
            $match: { _id:userId }
        }, {
            $unwind: "$contacts"
        }, {
            $match: queryHashtag
        }, {
            $group: {
                _id: "$emailId",
                contacts: {
                    $push: {
                        _id: "$contacts._id",
                        // personId: "$contacts.personId",
                        // personName: "$contacts.personName",
                        personEmailId: "$contacts.personEmailId",
                        // companyName: "$contacts.companyName",
                        // designation: "$contacts.designation",
                        // mobileNumber: "$contacts.mobileNumber",
                        // contactRelation: "$contacts.contactRelation",
                        // favorite: "$contacts.favorite",
                        // contactImageLink: "$contacts.contactImageLink",
                        // location: "$contacts.location"
                    }
                }
            }
        }]).exec(function(error, data) {
            if(!error){
                callback(error,data)
            } else {
                logger.info("An error occurred in getContactsMatchingLocations();", error)
                callback(error,[])
            }
        });
    } else {
        callback({},[])
    }
}

Contact.prototype.getContactsByEmailIdArray = function (userId,participants,callback) {

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: {
            "contacts.personEmailId":{$in:participants}
        }
    }, {
        $group: {
            _id: null,
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    contactImageLink: "$contacts.contactImageLink",
                    location: "$contacts.location"
                }
            }
        }
    }]).exec(function(error, data) {

        if(!error){
            callback(error,data)
        } else {
            logger.info("An error occured in getContactsByEmailIdArray();", error)
            callback(error,[])
        }
    });
}

Contact.prototype.getContactsForUsers= function (usersAndContacts,callback) {

    var userEmailIds = [];
    var userGroup = _
        .chain(usersAndContacts)
        .groupBy('userEmailId')
        .map(function(value, key) {
            userEmailIds.push(key)
            return {
                emailId: key,
                emailArray: _.pluck(value, 'contactEmailId')
            }
        })
        .value();

    var queryFinal = { "$or": userGroup.map(function(el) {
        var obj = {}
        obj["emailId"] = el.emailId
        obj["contacts.personEmailId"] = {$in:el.emailArray}
        return obj;
    })};

    myUserCollection.aggregate([{
        $match: { emailId: {$in:userEmailIds}}
    }, {
        $unwind: "$contacts"
    }, {
        $match: queryFinal
    }, {
        $group: {
            _id: null,
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    ownerEmailId: "$emailId",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    contactImageLink: "$contacts.contactImageLink"
                }
            }
        }
    }]).exec(function(error, data) {

        if(!error){
            callback(error,data)
        } else {
            logger.info("An error occured in getContactsForUsers();", error)
            callback(error,[])
        }
    });
}

console.log('Update All relation based API for contacts...');
Contact.prototype.getSingleContactRelation = function(userId, contactEmailId, isEmailId, fullContact,mobileNumber, callback) {
    var q;
    if (isEmailId) {
        q = { _id: userId, "contacts.personEmailId": contactEmailId }
    } else {
        q = { _id: userId, "contacts.personId": contactEmailId }
    }

    if(isNumber(contactEmailId) && !isEmailId){
        q = { _id: userId, contacts:{$elemMatch: { personEmailId: null, mobileNumber: contactEmailId }} }
    }

    if(isEmailId && mobileNumber){
        q = {
            _id: userId,
            contacts:{
                $elemMatch: {
                    "$or": [{personEmailId: contactEmailId},{mobileNumber: mobileNumber}]
                }
            }
        }
    }

    if(isEmailId && !mobileNumber){
        // q = { _id: userId, contacts:{$elemMatch: { personEmailId: contactEmailId, mobileNumber: {$in:[null,""]} }} }
        // Last edit by Naveen 12 Aug 2016.
        // Changed this as this function was used in showing details of the meeting participants and the mobile number parameter was missing

        q = { _id: userId, contacts:{$elemMatch: { personEmailId: contactEmailId }} }
    }

    if(isEmailId && contactEmailId && mobileNumber){
        q = {
            _id: userId,
            contacts:{
                $elemMatch: {personEmailId: contactEmailId,mobileNumber: mobileNumber}
            }
        }
    }

    myUserCollection.findOne(q, { "contacts.$": 1 }, function(error, contact) {

        if (checkRequired(contact) && checkRequired(contact.contacts) && contact.contacts.length > 0) {

            var obj;
            if (!fullContact) {
                obj = {
                    favorite: contact.contacts[0].favorite ? contact.contacts[0].favorite : false,
                    relation: contact.contacts[0].contactRelation,
                    publicProfileUrl: contact.contacts[0].publicProfileUrl,
                    personName: contact.contacts[0].personName,
                    personId: contact.contacts[0].personId,
                    personEmailId: contact.contacts[0].personEmailId,
                    companyName: contact.contacts[0].companyName,
                    designation: contact.contacts[0].designation,
                    location: contact.contacts[0].location,
                    mobileNumber: contact.contacts[0].mobileNumber,
                    _id: contact.contacts[0]._id,
                    relationshipStrength_updated: contact.contacts[0].relationshipStrength_updated,
                    twitterUserName: contact.contacts[0].twitterUserName,
                    hashtag: contact.contacts[0].hashtag,
                    remindToConnect: contact.contacts[0].remindToConnect,
                    contactImageLink: contact.contacts[0].contactImageLink,
                    isSalesforceContact: contact.contacts[0].isSalesforceContact ? contact.contacts[0].isSalesforceContact : false
                };
            } else obj = contact.contacts[0];

            if (!checkRequired(obj.personName)) {
                obj.personName = obj.personEmailId;
            }

            callback(obj);
        } else callback({ favorite: false, relation: 'None' });
    })
};

Contact.prototype.getContactsEmailsMatchedCompanyName = function(userId, emailId, companyName, callback) {

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: {
            "contacts.companyName": companyName,
            "contacts.emailId": { $ne: emailId }
        }
    }, {
        $group: {
            _id: null,
            contactEmails: {
                $addToSet: "$contacts.personEmailId"
            }
        }
    }]).exec(function(error, data) {

        if (checkRequired(data) && data.length > 0 && data[0] && checkRequired(data[0].contactEmails) && data[0].contactEmails.length) {
            callback(data[0].contactEmails);
        } else
            callback(error, [])
    })
};

Contact.prototype.searchUserContacts = function(searchContent, searchByEmailId, userId, skip, limit,fetchWatchlistType,callback) {
    var search = new RegExp(searchContent, 'i');

    var q = {
        $or: [{ "contacts.personName": search }, { "contacts.companyName": search }, { "contacts.designation": search }, { "contacts.hashtag": search },{ "contacts.personEmailId": search }]
    };
    if (searchByEmailId) {
        q = {
            $or: [{ "contacts.personName": search }, { "contacts.personEmailId": search }]
        }
    }

    if(fetchWatchlistType === 'companies'){
        q = {
            $or: [{ "contacts.account.name": search }]
        }
    }

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: q
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    location: "$contacts.location",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag",
                    contactImageLink: "$contacts.contactImageLink",
                    account: "$contacts.account",
                    watchThisContact: "$contacts.watchThisContact",
                    source: "$contacts.source",
                    addedDate: "$contacts.addedDate",
                }
            },
            count: {
                $sum: 1
            }
        }
    }]).exec(function(error, data) {

        if (checkRequired(data) && data.length > 0) {
            // var index = null;
            // for (var i = 0; i < data.length; i++) {
            //     if (data[i]._id == true) {
            //         index = i;
            //     }
            // }
            // if (index != null) {
            //     myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'publicProfileUrl profilePicUrl' }, function(err, list) {
            //         if (!err && list) {
            //             callback(null, concatAllContacts(data, list, true), data[index].userIds)
            //         } else
            //             callback(null, concatAllContacts(data, [], true), data[index].userIds)
            //     })
            // } else callback(null, concatAllContacts(data, [], true), [])
            callback(null, concatAllContactsV2(data, [], true), [])

        } else {
            callback(error, concatAllContactsV2([], [], true), [])
        }
    })
};

function concatAllContactsV2(all, some, sortByName) {

    var allContacts = [];
    // if (checkRequired(all) && all.length > 0) {
    //     for (var i = 0; i < all.length; i++) {
    //         if (all[i]._id != true) {
    //             for (var j = 0; j < all[i].contacts.length; j++) {
    //                 if (!checkRequired(all[i].contacts[j].personName)) {
    //                     all[i].contacts[j].personName = all[i].contacts[j].personEmailId
    //                 }
    //             }
    //             allContacts = allContacts.concat(all[i].contacts);
    //         }
    //     }
    // }

    // allContacts = allContacts.concat(some);

    _.each(all,function(el){
        el.contacts.forEach(function(co){
            allContacts.push(co);
        });
    });

    if (sortByName) {
        allContacts.sort(function(a, b) {
            if (checkRequired(a.personName) && checkRequired(b.personName)) {
                if (a.personName.toLocaleLowerCase() < b.personName.toLocaleLowerCase()) return -1;
                if (a.personName.toLocaleLowerCase() > b.personName.toLocaleLowerCase()) return 1;
                return 0;
            } else return 1;
        })
    } else {
        allContacts.sort(function(a, b) {
            return new Date(b.addedDate) - new Date(a.addedDate);
        });
    }

    return allContacts;
}

Contact.prototype.searchUserContact = function(searchContent, searchByEmailId, userId, skip, limit,fetchWatchlistType,callback) {

    var match = { _id: userId }

    if(validateEmail(userId)){
        match = { emailId: userId }
    }

    myUserCollection.aggregate([{
        $match: match
    }, {
        $unwind: "$contacts"
    }, {
        $match: { "contacts.personEmailId": searchContent }
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    location: "$contacts.location",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag",
                    contactImageLink: "$contacts.contactImageLink",
                    account: "$contacts.account",
                    watchThisContact: "$contacts.watchThisContact"
                }
            },
            count: {
                $sum: 1
            }
        }
    }]).exec(function(error, data) {

        if (checkRequired(data) && data.length > 0) {
            var index = null;
            for (var i = 0; i < data.length; i++) {
                if (data[i]._id == true) {
                    index = i;
                }
            }
            if (index != null) {
                myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'publicProfileUrl profilePicUrl' }, function(err, list) {
                    if (!err && list) {
                        callback(null, concatAllContacts(data, list, true), data[index].userIds)
                    } else
                        callback(null, concatAllContacts(data, [], true), data[index].userIds)
                })
            } else callback(null, concatAllContacts(data, [], true), [])

        } else
            callback(error, concatAllContacts([], [], true), [])
    })
};

var searchRelatas = function(userMatch, q, callback) {

    myUserCollection.aggregate([{
        $match: userMatch
    }, {
        $unwind: "$contacts"
    }, {
        $match: q
    }, {
        $group: {
            _id: "$contacts.personEmailId",
            personId: { $addToSet: "$contacts.personId" },
            personName: { $first: "$contacts.personName" },
            personEmailId: { $first: "$contacts.personEmailId" },
            contactImageLink: { $first: "$contacts.contactImageLink" },
            companyName: { $addToSet: "$contacts.companyName" },
            designation: { $addToSet: "$contacts.designation" },
            mobileNumber: { $addToSet: "$contacts.mobileNumber" },
            location: { $addToSet: "$contacts.location" },
            publicProfileUrl: { $addToSet: "$contacts.publicProfileUrl" },
            owner: { $addToSet: "$_id" },
            hashtag: { $addToSet: "$contacts.hashtag" },
            objectId: { $addToSet: "$contacts._id" }
        }
    }, {
        $group: {
            _id: "$_id",
            contacts: {
                $push: {
                    personId: "$personId",
                    personName: "$personName",
                    personEmailId: "$personEmailId",
                    contactImageLink: "$contactImageLink",
                    companyName: "$companyName",
                    designation: "$designation",
                    mobileNumber: "$mobileNumber",
                    location: "$location",
                    publicProfileUrl: "$publicProfileUrl",
                    owner: "$owner",
                    hashtag: "$hashtag",
                    objectId: "$_id"
                }
            }
        }
    }, {
        $unwind: "$contacts"
    }, {
        $project: {
            personId: "$contacts.personId",
            personName: "$contacts.personName",
            personEmailId: "$contacts.personEmailId",
            contactImageLink: "$contacts.contactImageLink",
            companyName: "$contacts.companyName",
            designation: "$contacts.designation",
            mobileNumber: "$contacts.mobileNumber",
            location: "$contacts.location",
            publicProfileUrl: "$contacts.publicProfileUrl",
            owner: "$contacts.owner",
            hashtag: "$contacts.hashtag",
            objectId: "$contacts._id"
        }
    }, {
        $sort: { "personName": -1 }
    }]).exec(function(error, data) {

        setPublicProfileURLForArrayOfContacts(data, 0, function(error, dataWithPublicProfileURL) {

            callback(error, dataWithPublicProfileURL)
        });
    })
}

function setPublicProfileURLForArrayOfContacts(data, iteration, callback) {
    if (iteration < data.length) {
        if (checkRequired(data[iteration]._id)) {

            var findBy;
            if(data[iteration]._id.emailId){
                findBy = data[iteration]._id.emailId
            } else if(data[iteration]._id.mobileNumber) {
                findBy = data[iteration]._id.mobileNumber
            }

            UserManagementObj.findUserProfileByEmailIdWithCustomFields(findBy, { publicProfileUrl: 1 }, function(err, user) {
                if (err) {
                    console.log(err);
                } else {
                    if (checkRequired(user) && checkRequired(user.publicProfileUrl)) {
                        data[iteration].publicProfileUrlStr = user.publicProfileUrl;
                    }
                    return setPublicProfileURLForArrayOfContacts(data, ++iteration, callback);
                }
            });
        } else {
            return setPublicProfileURLForArrayOfContacts(data, ++iteration, callback);
        }
    } else {
        callback(null, data)
    }
}

function setPublicProfileURLForArrayOfContactsForHashtag(data, iteration, callback) {
    if (iteration < data.length) {
        if (checkRequired(data[iteration]._id)) {

            UserManagementObj.findUserProfileByEmailIdWithCustomFields(data[iteration]._id, { publicProfileUrl: 1 }, function(err, user) {
                if (err) {
                    console.log(err);
                } else {
                    if (checkRequired(user) && checkRequired(user.publicProfileUrl)) {
                        data[iteration].publicProfileUrlStr = user.publicProfileUrl;
                    }
                    return setPublicProfileURLForArrayOfContacts(data, ++iteration, callback);
                }
            });
        } else {
            return setPublicProfileURLForArrayOfContacts(data, ++iteration, callback);
        }
    } else {
        callback(null, data)
    }
}

Contact.prototype.searchUserContacts_searchPage = function(searchContent, userId, skip, limit, filters, callback) {

    var multiHashtags = searchContent.split(' ');
    var numberOfHashtags = multiHashtags.length;

    var search = new RegExp('^' + searchContent, 'i');

    var q = {
        $or: [{ "contacts.personName": search }, { "contacts.hashtag": search }]
    };

    //Inefficient way for max 5 hashtags.Please change this to "n" number of hashtags.
    var hashtag1 = multiHashtags[0];
    var hashtag2 = multiHashtags[1];
    var hashtag3 = multiHashtags[2];
    var hashtag4 = multiHashtags[3];
    var hashtag5 = multiHashtags[4];

    var searchTag1 = new RegExp('^' + hashtag1, 'i');
    var searchTag2 = new RegExp('^' + hashtag2, 'i');
    var searchTag3 = new RegExp('^' + hashtag3, 'i');
    var searchTag4 = new RegExp('^' + hashtag4, 'i');
    var searchTag5 = new RegExp('^' + hashtag5, 'i');

    if (numberOfHashtags > 1 && numberOfHashtags == 2) {
        var q = {
            $or: [{ "contacts.personName": search }, { $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }] }]
        }
    } else if (numberOfHashtags > 1 && numberOfHashtags == 3) {
        var q = {
            $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }]
        }
    } else if (numberOfHashtags > 1 && numberOfHashtags == 4) {
        var q = {
            $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }, { "contacts.hashtag": searchTag4 }]
        }
    } else if (numberOfHashtags > 1 && numberOfHashtags == 5) {
        var q = {
            $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }, { "contacts.hashtag": searchTag4 }, { "contacts.hashtag": searchTag5 }]
        }
    } else {
        var q = {
            $or: [{ "contacts.personName": search }, { "contacts.hashtag": search }]
        };
    }

    var userMatch = { _id: userId };

    if (filters.indexOf("extendedNetwork") != -1 && filters.indexOf("yourNetwork") != -1) {

        userMatch = {};
        var q = {
            $or: [{ "contacts.personName": search }]
        };

        searchRelatas(userMatch, q, function(error, data) {

            var multiHashtags = searchContent.split(' ');

            //Inefficeint way for max 5 hashtags.Please change this to "n" number of hashtags.

            var hashtag1 = multiHashtags[0];
            var hashtag2 = multiHashtags[1];
            var hashtag3 = multiHashtags[2];
            var hashtag4 = multiHashtags[3];
            var hashtag5 = multiHashtags[4];

            var searchTag1 = new RegExp('^' + hashtag1, 'i');
            var searchTag2 = new RegExp('^' + hashtag2, 'i');
            var searchTag3 = new RegExp('^' + hashtag3, 'i');
            var searchTag4 = new RegExp('^' + hashtag4, 'i');
            var searchTag5 = new RegExp('^' + hashtag5, 'i');

            var numberOfHashtags = multiHashtags.length;

            if (numberOfHashtags > 1 && numberOfHashtags == 2) {
                var q = {
                    $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }]
                }
            } else if (numberOfHashtags > 1 && numberOfHashtags == 3) {
                var q = {
                    $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }]
                }
            } else if (numberOfHashtags > 1 && numberOfHashtags == 4) {
                var q = {
                    $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }, { "contacts.hashtag": searchTag4 }]
                }
            } else if (numberOfHashtags > 1 && numberOfHashtags == 5) {
                var q = {
                    $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }, { "contacts.hashtag": searchTag4 }, { "contacts.hashtag": searchTag5 }]
                }
            } else {
                var q = {
                    $or: [{ "contacts.hashtag": search }]
                };
            }

            var userMatch = { _id: userId };

            searchRelatas(userMatch, q, function(error, onlyHashtagData) {
                var totalResult = data.concat(onlyHashtagData);
                callback(null, totalResult);

            })
        });
    } else {
        if (filters.indexOf("extendedNetwork") != -1) {
            userMatch = { _id: { $ne: userId } };
            var q = {
                $or: [{ "contacts.personName": search }]
            };
        } else if (filters.indexOf("yourNetwork") != -1 && filters.indexOf("extendedNetwork") == 0) {
            userMatch = { _id: userId }
        }

        if (filters.indexOf("companies") != -1) {
            q = { $or: [{ "contacts.account.name": search }] };
        }

        if (filters.indexOf("insights") != -1) {
            userMatch = { _id: { $ne: userId } };
            q = {  "contacts.account.name": search , "contacts.contactRelation.decisionmaker_influencer":{$in:['decision_maker', 'influencer']}};
        }

        if (filters.indexOf("dashboard") != -1) {
            var companies = searchContent.split(' ');
            userMatch = { _id: { $ne: userId } };
            q = {  "contacts.account.name": {$in:companies} , "contacts.contactRelation.decisionmaker_influencer":{$in:['decision_maker', 'influencer']}};
        }
        if(filters.indexOf("dashboard") != -1 || filters.indexOf("insights") != -1){
            myUserCollection.find({_id:userId},{contacts:1}).exec(function(error, userContacts){
                if(error)
                    callback(err, null);
                else {
                    var emailIdArr = [];
                    emailIdArr = _.filter(userContacts[0].contacts, function(a) {
                        if (a.personEmailId) {
                            if(userContacts[0].emailId) {
                                if (userContacts[0].emailId.toLowerCase() !== a.personEmailId.toLowerCase())
                                    return a.personEmailId;
                            }
                            else
                                return a.personEmailId;
                        }
                    });
                    emailIdArr = _.pluck(emailIdArr, "personEmailId");
                    var query = [];

                    if (filters.indexOf("dashboardTopCompanies") != -1) {
                        var companies = searchContent.split(' ');
                        userMatch = { _id: { $ne: userId } };
                        q = {  "contacts.account.name": {$in:companies}};
                    }

                    query.push(
                        {
                            $match: userMatch
                        }, {
                            $unwind: "$contacts"
                        }, {
                            $match: q
                        }, {
                            $match: {
                                "contacts.personEmailId":{$nin:emailIdArr},
                                emailId:{$in:emailIdArr}
                            }
                        },{
                            $group: {
                                 _id: "$contacts.personEmailId",
                                //_id: {mobileNumber: "$contacts.mobileNumber", emailId: "$contacts.personEmailId"},
                                personId: {$addToSet: "$contacts.personId"},
                                personName: {$first: "$contacts.personName"},
                                personEmailId: {$first: "$contacts.personEmailId"},
                                contactImageLink: {$first: "$contacts.contactImageLink"},
                                companyName: {$addToSet: "$contacts.companyName"},
                                designation: {$addToSet: "$contacts.designation"},
                                mobileNumber: {$first: "$contacts.mobileNumber"},
                                location: {$addToSet: "$contacts.location"},
                                publicProfileUrl: {$addToSet: "$contacts.publicProfileUrl"},
                                owner: {$addToSet: "$_id"},
                                hashtag: {$addToSet: "$contacts.hashtag"},
                                objectId: {$addToSet: "$contacts._id"}
                            }
                        }, {
                            $group: {
                                _id: "$_id",
                                contacts: {
                                    $push: {
                                        personId: "$personId",
                                        personName: "$personName",
                                        personEmailId: "$personEmailId",
                                        contactImageLink: "$contactImageLink",
                                        companyName: "$companyName",
                                        designation: "$designation",
                                        mobileNumber: "$mobileNumber",
                                        location: "$location",
                                        publicProfileUrl: "$publicProfileUrl",
                                        owner: "$owner",
                                        hashtag: "$hashtag",
                                        objectId: "$_id"
                                    }
                                }
                            }
                        }, {
                            $unwind: "$contacts"
                        }, {
                            $project: {
                                _id: "$_id",
                                personId: "$contacts.personId",
                                personName: "$contacts.personName",
                                personEmailId: "$contacts.personEmailId",
                                contactImageLink: "$contacts.contactImageLink",
                                companyName: "$contacts.companyName",
                                designation: "$contacts.designation",
                                mobileNumber: "$contacts.mobileNumber",
                                location: "$contacts.location",
                                publicProfileUrl: "$contacts.publicProfileUrl",
                                owner: "$contacts.owner",
                                hashtag: "$contacts.hashtag",
                                objectId: "$contacts._id"
                            }
                        }, {
                            $sort: {"personName": -1}
                        }
                    );

                    myUserCollection.aggregate(query, function (err, extendedNetForDashboard) {
                        if (filters.indexOf("dashboardTopCompanies") != -1 && extendedNetForDashboard.length > 10) {
                            //query.push({ $sample: { size: 10 } })
                            var randomArr = []
                            var size = 10;
                            while(randomArr.length < size){
                                var randomnumber=Math.ceil(Math.random()* extendedNetForDashboard.length);
                                var found=false;
                                if(randomArr.indexOf(randomnumber) != -1)
                                    found = true;

                                if(!found)randomArr.push(randomnumber);
                            }

                            var filtred = [];
                            randomArr.forEach(function(a){
                                var index = a == 0 ? 0 : (a-1);
                                filtred.push(extendedNetForDashboard[index])
                            })
                            extendedNetForDashboard = filtred;
                        }
                        setPublicProfileURLForArrayOfContacts(extendedNetForDashboard, 0, function (error, dataWithPublicProfileURL) {
                            //for (var i = 0; i < dataWithPublicProfileURL.length; i++) {
                            //    for (var j = 0; j < dataWithPublicProfileURL.length; j++) {
                            //        if (i != j && (dataWithPublicProfileURL[i].personEmailId == dataWithPublicProfileURL[j].personEmailId)) {
                            //            dataWithPublicProfileURL.splice(i, 1);
                            //            i = 0;
                            //        }
                            //    }
                            //}
                            callback(error, dataWithPublicProfileURL)
                        });
                    });
                }
            });
        }
        else {
            myUserCollection.aggregate([{
                $match: userMatch
            }, {
                $unwind: "$contacts"
            }, {
                $match: q
            }, {
                $group: {
                    // _id: "$contacts.personEmailId",
                    _id: {mobileNumber: "$contacts.mobileNumber", emailId: "$contacts.personEmailId"},
                    personId: {$addToSet: "$contacts.personId"},
                    personName: {$first: "$contacts.personName"},
                    personEmailId: {$first: "$contacts.personEmailId"},
                    contactImageLink: {$first: "$contacts.contactImageLink"},
                    companyName: {$addToSet: "$contacts.companyName"},
                    designation: {$addToSet: "$contacts.designation"},
                    mobileNumber: {$first: "$contacts.mobileNumber"},
                    location: {$addToSet: "$contacts.location"},
                    publicProfileUrl: {$addToSet: "$contacts.publicProfileUrl"},
                    owner: {$addToSet: "$_id"},
                    hashtag: {$addToSet: "$contacts.hashtag"},
                    objectId: {$addToSet: "$contacts._id"}
                }
            }, {
                $group: {
                    _id: "$_id",
                    contacts: {
                        $push: {
                            personId: "$personId",
                            personName: "$personName",
                            personEmailId: "$personEmailId",
                            contactImageLink: "$contactImageLink",
                            companyName: "$companyName",
                            designation: "$designation",
                            mobileNumber: "$mobileNumber",
                            location: "$location",
                            publicProfileUrl: "$publicProfileUrl",
                            owner: "$owner",
                            hashtag: "$hashtag",
                            objectId: "$_id"
                        }
                    }
                }
            }, {
                $unwind: "$contacts"
            }, {
                $project: {
                    _id: "$_id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    contactImageLink: "$contacts.contactImageLink",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    location: "$contacts.location",
                    publicProfileUrl: "$contacts.publicProfileUrl",
                    owner: "$contacts.owner",
                    hashtag: "$contacts.hashtag",
                    objectId: "$contacts._id"
                }
            }, {
                $sort: {"personName": -1}
            }]).exec(function (error, data) {
                setPublicProfileURLForArrayOfContacts(data, 0, function (error, dataWithPublicProfileURL) {
                        callback(error, dataWithPublicProfileURL)
                });
            })
        }
    }
};

Contact.prototype.getAccountsOfAllContacts = function(userId, callback) {

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {$group:{
            _id:"$contacts.account.name"
        }
    }, {
        $match:{_id:{$nin:[null,'']}}
    }, {
        $project:{_id:0,company:"$_id"}
    }]).exec(function(error, data) {
        if (checkRequired(data) && data.length > 0) {
            callback(null, data);
        } else
            callback(error, [])
    })
};

//Search for only hashtags

Contact.prototype.searchUserHashtag_searchPage = function(searchContent, userId, skip, limit, filters, callback) {

    var multiHashtags = searchContent.split(' ');
    var numberOfHashtags = multiHashtags.length;

    var search = new RegExp('^' + searchContent, 'i');

    var q = {
        $or: [{ "contacts.hashtag": search }]
    };

    //Inefficeint way for max 5 hashtags.Please change this to "n" number of hashtags.

    var hashtag1 = multiHashtags[0];
    var hashtag2 = multiHashtags[1];
    var hashtag3 = multiHashtags[2];
    var hashtag4 = multiHashtags[3];
    var hashtag5 = multiHashtags[4];

    var searchTag1 = new RegExp('^' + hashtag1, 'i');
    var searchTag2 = new RegExp('^' + hashtag2, 'i');
    var searchTag3 = new RegExp('^' + hashtag3, 'i');
    var searchTag4 = new RegExp('^' + hashtag4, 'i');
    var searchTag5 = new RegExp('^' + hashtag5, 'i');

    if (numberOfHashtags > 1 && numberOfHashtags == 2) {
        var q = {
            $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }]
        }
    } else if (numberOfHashtags > 1 && numberOfHashtags == 3) {
        var q = {
            $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }]
        }
    } else if (numberOfHashtags > 1 && numberOfHashtags == 4) {
        var q = {
            $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }, { "contacts.hashtag": searchTag4 }]
        }
    } else if (numberOfHashtags > 1 && numberOfHashtags == 5) {
        var q = {
            $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }, { "contacts.hashtag": searchTag4 }, { "contacts.hashtag": searchTag5 }]
        }
    } else {
        var q = {
            $or: [{ "contacts.hashtag": search }]
        };
    }

    var userMatch = { _id: userId };

    if (filters.indexOf("extendedNetwork") != -1 && filters.indexOf("yourNetwork") != -1) {

        userMatch = { _id: userId };

        var q = {
            $or: [{ "contacts.hashtag": search }]
        };

        searchRelatas(userMatch, q, function(error, data) {

            var multiHashtags = searchContent.split(' ');

            //Inefficeint way for max 5 hashtags.Please change this to "n" number of hashtags.

            var hashtag1 = multiHashtags[0];
            var hashtag2 = multiHashtags[1];
            var hashtag3 = multiHashtags[2];
            var hashtag4 = multiHashtags[3];
            var hashtag5 = multiHashtags[4];

            var searchTag1 = new RegExp('^' + hashtag1, 'i');
            var searchTag2 = new RegExp('^' + hashtag2, 'i');
            var searchTag3 = new RegExp('^' + hashtag3, 'i');
            var searchTag4 = new RegExp('^' + hashtag4, 'i');
            var searchTag5 = new RegExp('^' + hashtag5, 'i');

            var numberOfHashtags = multiHashtags.length;

            if (numberOfHashtags > 1 && numberOfHashtags == 2) {
                var q = {
                    $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }]
                }
            } else if (numberOfHashtags > 1 && numberOfHashtags == 3) {
                var q = {
                    $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }]
                }
            } else if (numberOfHashtags > 1 && numberOfHashtags == 4) {
                var q = {
                    $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }, { "contacts.hashtag": searchTag4 }]
                }
            } else if (numberOfHashtags > 1 && numberOfHashtags == 5) {
                var q = {
                    $and: [{ "contacts.hashtag": searchTag1 }, { "contacts.hashtag": searchTag2 }, { "contacts.hashtag": searchTag3 }, { "contacts.hashtag": searchTag4 }, { "contacts.hashtag": searchTag5 }]
                }
            } else {
                var q = {
                    $or: [{ "contacts.hashtag": search }]
                };
            }

            var userMatch = { _id: userId };

            searchRelatas(userMatch, q, function(error, onlyHashtagData) {
                //var totalResult=data.concat(onlyHashtagData);
                //Not required for Hashtags
                callback(null, onlyHashtagData);

            })
        });
    } else {
        if (filters.indexOf("extendedNetwork") != -1) {
            userMatch = { _id: userId };
            var q = {
                $or: [{ "contacts.hashtag": search }]
            };
        } else if (filters.indexOf("yourNetwork") != -1 && filters.indexOf("extendedNetwork") == 0) {
            userMatch = { _id: userId }
        }

        if (filters.indexOf("companies") != -1) {
            q = { "contacts.companyName": search }
        }

        myUserCollection.aggregate([{
            $match: userMatch
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }, {
            $group: {
                _id: "$contacts._id",
                personId: { $addToSet: "$contacts.personId" },
                personName: { $first: "$contacts.personName" },
                personEmailId: { $first: "$contacts.personEmailId" },
                contactImageLink: { $first: "$contacts.contactImageLink" },
                companyName: { $addToSet: "$contacts.companyName" },
                designation: { $addToSet: "$contacts.designation" },
                mobileNumber: { $addToSet: "$contacts.mobileNumber" },
                location: { $addToSet: "$contacts.location" },
                publicProfileUrl: { $addToSet: "$contacts.publicProfileUrl" },
                owner: { $addToSet: "$_id" },
                hashtag: { $addToSet: "$contacts.hashtag" },
                objectId: { $addToSet: "$contacts._id" }
            }
        }, {
            $group: {
                _id: "$_id",
                contacts: {
                    $push: {
                        personId: "$personId",
                        personName: "$personName",
                        personEmailId: "$personEmailId",
                        contactImageLink: "$contactImageLink",
                        companyName: "$companyName",
                        designation: "$designation",
                        mobileNumber: "$mobileNumber",
                        location: "$location",
                        publicProfileUrl: "$publicProfileUrl",
                        owner: "$owner",
                        hashtag: "$hashtag",
                        objectId: "$_id"
                    }
                }
            }
        }, {
            $unwind: "$contacts"
        }, {
            $project: {
                personId: "$contacts.personId",
                personName: "$contacts.personName",
                personEmailId: "$contacts.personEmailId",
                contactImageLink: "$contacts.contactImageLink",
                companyName: "$contacts.companyName",
                designation: "$contacts.designation",
                mobileNumber: "$contacts.mobileNumber",
                location: "$contacts.location",
                publicProfileUrl: "$contacts.publicProfileUrl",
                owner: "$contacts.owner",
                hashtag: "$contacts.hashtag",
                objectId: "$contacts._id"
            }
        }, {
            $sort: { "personName": -1 }
        }]).exec(function(error, data) {

            setPublicProfileURLForArrayOfContactsForHashtag(data, 0, function(error, dataWithPublicProfileURL) {

                callback(error, dataWithPublicProfileURL)
            });
        })
    }
};

Contact.prototype.searchUserContactsCompany = function(searchContent, userId, skip, limit, callback) {
    var search = new RegExp(searchContent, 'i');
    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: {
            "contacts.companyName": search
        }
    }, {
        $group: {
            _id: "$contacts.relatasUser",
            contacts: {
                $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag"
                }
            },
            count: {
                $sum: 1
            }
        }
    }]).exec(function(error, data) {

        if (checkRequired(data) && data.length > 0) {
            var index = null;
            for (var i = 0; i < data.length; i++) {
                if (data[i]._id == true) {
                    index = i;
                }
            }
            if (index != null) {
                myUserCollection.populate(data[index].contacts, { path: 'personId', select: 'firstName lastName companyName designation profilePicUrl mobileNumber' }, function(err, list) {
                    if (!err && list) {
                        callback(null, concatAllContacts(data, list), data[index].userIds)
                    } else
                        callback(null, concatAllContacts(data, []), data[index].userIds)
                })
            } else callback(null, concatAllContacts(data, []), [])

        } else
            callback(error, concatAllContacts([], []), [])
    })
};

Contact.prototype.getSingleContact = function(userId, contactEmailId, callback) {

    var q = { _id: userId, "contacts.personEmailId": contactEmailId };
    if(isNumber(contactEmailId)){
        q = { _id: userId, "contacts.mobileNumber": contactEmailId }
    }

    myUserCollection.findOne(q, { "contacts.$": 1 }, function(error, user) {

        if (checkRequired(user) && checkRequired(user.contacts) && user.contacts.length > 0) {
            callback(error, user.contacts[0])
        } else {
            callback(error, null)
        }
    })
};

Contact.prototype.getContactSocialInfo = function(userId, contactEmailId, callback) {
    myUserCollection.findOne({ _id: userId, "contacts.personEmailId": contactEmailId }, { "contacts.$": 1 }, function(error, user) {
        if (checkRequired(user) && checkRequired(user.contacts) && user.contacts.length > 0) {
            callback(error, user.contacts[0])
        } else {
            callback(error, null)
        }
    })
};

Contact.prototype.updateContactSocialInfo = function(emailId, obj, callback) {
    var updateObject = {};
    var updateObjectUserFacebook = null;
    var updateObjectUserLinkedin = null;
    var updateObjectUserTwitter = null;
    var isValidUpdate = false;
    if (checkRequired(obj.facebookUserName)) {
        updateObject["contacts.$.facebookUserName"] = obj.facebookUserName;
        updateObjectUserFacebook = {
            "facebook.name": obj.facebookUserName
        };
        isValidUpdate = true;
    }
    if (checkRequired(obj.linkedinUserName)) {
        updateObject["contacts.$.linkedinUserName"] = obj.linkedinUserName;
        updateObjectUserLinkedin = {
            "linkedin.name": obj.linkedinUserName
        };
        isValidUpdate = true;
    }

    if (checkRequired(obj.twitterUserName)) {
        updateObject["contacts.$.twitterUserName"] = obj.twitterUserName;
        updateObjectUserTwitter = {
            "twitter.userName": obj.twitterUserName
        };
        isValidUpdate = true;
    }

    if (isValidUpdate) {
        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
        bulk.find({ "contacts.personEmailId": emailId }).update({ $set: updateObject }, { multi: true });
        if (checkRequired(updateObjectUserLinkedin)) {
            bulk.find({ emailId: emailId, $or: [{ linkedin: { $exists: false } }, { linkedin: { name: '' } }] }).update({ $set: updateObjectUserLinkedin });
        }
        if (checkRequired(updateObjectUserFacebook)) {
            bulk.find({ emailId: emailId, $or: [{ facebook: { $exists: false } }, { facebook: { name: '' } }] }).update({ $set: updateObjectUserFacebook });
        }
        if (checkRequired(updateObjectUserTwitter)) {
            bulk.find({ emailId: emailId, $or: [{ twitter: { $exists: false } }, { twitter: { userName: '' } }] }).update({ $set: updateObjectUserTwitter });
        }

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updateContactSocialInfo():Contact Bulk', err, emailId)
            } else {
                result = result.toJSON();
                logger.info('updateContactSocialInfo() results ', result, emailId);
            }
        });

        /*myUserCollection.update({"contacts.personEmailId":emailId},{$set:updateObject},{multi:true},function(error,response){

         if(error){
         logger.info('Error in updateContactSocialInfo():Contact ',error,emailId);
         if(callback) callback(error,response);
         }
         else{
         if(callback) callback(error, response);
         }
         })*/
    }
};

Contact.prototype.updateContactsCollectionLevelMulti = function(details, callback) {

    var updateObj = {};
    var isValidUpdate = false;
    if (checkRequired(details.firstName) && checkRequired(details.lastName)) {
        isValidUpdate = true;
        updateObj["contacts.$.personName"] = details.firstName + ' ' + details.lastName;
    }
    if (checkRequired(details._id)) {
        isValidUpdate = true;
        updateObj["contacts.$.personId"] = details._id;
    }
    if (checkRequired(details.emailId)) {
        isValidUpdate = true;
        updateObj["contacts.$.personEmailId"] = details.emailId;
    }
    if (checkRequired(details.location)) {
        isValidUpdate = true;
        updateObj["contacts.$.location"] = details.location;
    }
    if (checkRequired(details.companyName)) {
        isValidUpdate = true;
        updateObj["contacts.$.companyName"] = details.companyName;
    }
    if (checkRequired(details.designation)) {
        isValidUpdate = true;
        updateObj["contacts.$.designation"] = details.designation;
    }
    if (checkRequired(details.skypeId)) {
        isValidUpdate = true;
        updateObj["contacts.$.skypeId"] = details.skypeId;
    }
    if (checkRequired(details.mobileNumber)) {
        isValidUpdate = true;
        updateObj["contacts.$.mobileNumber"] = details.mobileNumber;
    }
    if (checkRequired(details.twitterUserName)) {
        isValidUpdate = true;
        updateObj["contacts.$.twitterUserName"] = details.twitterUserName;
    }
    if (checkRequired(details.facebookUserName)) {
        isValidUpdate = true;
        updateObj["contacts.$.facebookUserName"] = details.facebookUserName;
    }
    if (checkRequired(details.linkedinUserName)) {
        isValidUpdate = true;
        updateObj["contacts.$.linkedinUserName"] = details.linkedinUserName;
    }
    updateObj["contacts.$.relatasUser"] = true;

    var queryFind = { contacts: { $elemMatch: { $or: [] } } };
    var exist2 = false;

    if (checkRequired(details._id)) {
        exist2 = true;
        queryFind.contacts.$elemMatch.$or.push({ personId: details._id })
    }

    if (checkRequired(details.emailId)) {
        exist2 = true;
        queryFind.contacts.$elemMatch.$or.push({ personEmailId: details.emailId })
    }

    if (checkRequired(details.mobileNumber)) {
        exist2 = true;
        queryFind.contacts.$elemMatch.$or.push({ mobileNumber: details.mobileNumber })
    }

    if (isValidUpdate && exist2) {
        myUserCollection.update(queryFind, { $set: updateObj }, { multi: true }, function(error, response) {
            if (error) {
                logger.info('Error in updateContactsCollectionLevelMulti():Contact ', error, queryFind);
                if (callback) callback(error, response)
            } else {
                if (callback) callback(error, response)
            }
        })
    }
};

Contact.prototype.removeDuplicateContacts = function(userId, callback) {

    myUserCollection.aggregate([
        // Match only where array has content
        { "$match": { emailId: userId } },
        // Unwind the array
        { "$unwind": "$contacts" },
        { $match: { "contacts.personEmailId": { $ne: "" } } },
        // Group the keys with counts keep the doc _id's
        {
            "$group": {
                "_id": "$contacts.personEmailId",
                "ids": { "$push": "$contacts._id" },
                "count": { "$sum": 1 }
            }
        },
        // Filter only duplicate matches
        { "$match": { "count": { "$gt": 1 } } }
    ]).exec(function(error, result) {
        callback(error, result)
    })
};

Contact.prototype.getContactsDoesNotHaveId = function(userId, callback) {

    myUserCollection.aggregate([
        // Match only where array has content
        { "$match": { emailId: userId } },
        // Unwind the array
        { "$unwind": "$contacts" },
        { $match: { "contacts._id": { $exists: false } } }, {
            $group: {
                _id: null,
                contacts: { $push: "$contacts" }
            }
        }
        // Group the keys with counts keep the doc _id's
    ]).exec(function(error, result) {
        callback(error, result)
    })
};
//used by MOBILE.
Contact.prototype.addContactNotExistMobile = function(userId, userEmailId, contacts, mobileNumbers, emailIds, source, callback) {
    var q = {
        $or: []
    };

    if(contacts.length>0){
        emailIds = _.pluck(contacts,"personEmailId")
        mobileNumbers = _.pluck(contacts,"mobileNumber")
    }

    var qExist = false;
    if (mobileNumbers.length > 0) {
        qExist = true;
        q.$or.push({ "contacts.mobileNumber": { $in: mobileNumbers } });
    }
    if (emailIds.length > 0) {
        qExist = true;
        q.$or.push({ "contacts.personEmailId": { $in: emailIds } });
    }
    if (qExist) {
        myUserCollection.aggregate([{
            $match: { _id: userId }
        }, {
            $unwind: "$contacts"
        }, {
            $match: q

        }, {
            $group: {
                _id: null,
                existEmailIds: { $addToSet: { $toLower: "$contacts.personEmailId" } },
                existMobileNumbers: { $addToSet: "$contacts.mobileNumber" }
            }
        }]).exec(function(error, result) {

            var existContent = [];
            if (!error && checkRequired(result) && result.length > 0) {

                if ((checkRequired(result[0])) && (result[0].existEmailIds.length > 0 || result[0].existMobileNumbers.length > 0)) {
                    existContent = existContent.concat(result[0].existEmailIds, result[0].existMobileNumbers);
                }
                var nonExistContacts = [];

                for (var i = 0; i < contacts.length; i++) {
                    var exist = false //translates to notExist = true
                    if (checkRequired(contacts[i].personEmailId) || checkRequired(contacts[i].mobileNumber)) {
                        if (existContent.indexOf(contacts[i].personEmailId) != -1) {
                            if (existContent.indexOf(contacts[i].mobileNumber) != -1) {
                                    exist = true;
                            }else{
                                if(contacts[i].mobileNumber == null){
                                    exist = true;
                                }
                            }
                        }
                        if (existContent.indexOf(contacts[i].mobileNumber) != -1) {
                            if (existContent.indexOf(contacts[i].personEmailId) != -1) {
                                    exist = true
                            }else{
                                if(contacts[i].personEmailId == null){
                                    exist = true;
                                }
                            }
                        }
                    }
                    if (!exist) {
                        nonExistContacts.push(contacts[i]);
                    }
                }

                if (nonExistContacts.length > 0) {
                    pushAllContactsToUserProfile(userId, nonExistContacts, callback)
                } else if (callback) callback()

            } else if (error) {
                logger.info('Error in addContactNotExist():Contact ', error);
                if (callback) callback()
            } else {
                pushAllContactsToUserProfile(userId, contacts, callback);
            }
        });
    } else if (callback) callback(true)
};

Contact.prototype.updateOnlyRequiredFields = function(userId,contactList,callBack){

    var emailList = []
    var mobileList = []

    for(var i = 0; i<contactList.length;i++) {

        if(checkRequired(contactList[i].personEmailId))
        emailList.push(contactList[i].personEmailId)
        if(checkRequired(contactList[i].mobileNumber))
        mobileList.push(contactList[i].mobileNumber)
    }

        checkForContactFieldNull(userId,emailList,mobileList,contactList,function(contactObj,contactFromDB){

            var bulk = myUserCollection.collection.initializeUnorderedBulkOp();

            if(contactFromDB.length>0){

                for(var i = 0;i<contactList.length;i++){
                    for(var j = 0;j<contactFromDB.length;j++){

                        var updateObj = {
                            "contacts.$.personName": contactList[i].personName,
                            "contacts.$.mobileNumber":contactList[i].mobileNumber,
                            "contacts.$.personEmailId":contactList[i].personEmailId
                        };

                        if((contactFromDB[j].personEmailId == null) && (contactFromDB[j].mobileNumber == contactList[i].mobileNumber)){
                            bulk.find({_id:userId,"contacts._id":contactFromDB[j]._id}).update({$set:updateObj})
                        }else if((contactFromDB[j].mobileNumber == null) && (contactFromDB[j].personEmailId == contactList[i].personEmailId)){
                            bulk.find({_id:userId,"contacts._id":contactFromDB[j]._id}).update({$set:updateObj})
                        }
                    }
                }

                bulk.execute(function(err,res){
                    if(err) {
                        callBack(contactList)
                    }
                    else{
                        if(callBack){
                            callBack(contactList)
                        }
                    }
                })

            }else{
                //pushAllContactsToUserProfile(userId,contactList,null)
                callBack(contactList)
            }
        })

}

function checkForContactFieldNull(userId,emailList,mobileList,preserveObj,callBack){
    //preserve obj allows the callBack to be passed in the callback allowing this function to be used in a loop.

    var updateObj = {
        _id:"$contacts._id",
        personEmailId:"$contacts.personEmailId",
        mobileNumber:"$contacts.mobileNumber"
    }
    var condition = {$or:[
            {$eq:["$contacts.mobileNumber", null]},
            {$eq:["$contacts.mobileNumber",'']},
            {$eq:["$contacts.personEmailId",""]},
            {$eq:["$contacts.personEmailId",null]}
        ]}

    var q = {}
    var groupQ = {}
        q = {$or:[{"contacts.personEmailId":{$in:emailList}},{"contacts.mobileNumber":{$in:mobileList}}]}
        groupQ = {
            _id:null,
            contacts:{"$push":{
                "$cond":{if:condition,
                        then:updateObj,
                        else:"$noval"
            }}
        }}

    myUserCollection.aggregate([
        {$match:{_id:userId}},
        {$unwind:"$contacts"},
        {$match:q},
        {$group:groupQ}]
    ,function(err,res){

            if(err || res.length==0 || res == "undefined"){
                callBack(preserveObj,[])
            }
            else{
                callBack(preserveObj,res[0].contacts)
            }

        })
}

//used by Web
Contact.prototype.addContactNotExist = function(userId, userEmailId, contacts, mobileNumbers, emailIds, source, callback) {

    var q = {
        $or: []
    };

    if(contacts.length>0){
        emailIds = _.pluck(contacts,"personEmailId")
        mobileNumbers = _.pluck(contacts,"mobileNumber")
    }

    var qExist = false;
    if (mobileNumbers.length > 0) {
        qExist = true;
        q.$or.push({ "contacts.mobileNumber": { $in: mobileNumbers } });
    }
    if (emailIds.length > 0) {
        qExist = true;
        q.$or.push({ "contacts.personEmailId": { $in: emailIds } });
    }

    if (qExist) {
        myUserCollection.aggregate([{
            $match: { _id: userId }
        }, {
            $unwind: "$contacts"
        }, {
            $match: { "contacts.personEmailId": { $in: emailIds } }
        }, {
            $group: {
                _id: null,
                existEmailIds: { $addToSet: { $toLower: "$contacts.personEmailId" } }
            }
        }]).exec(function(error, result) {

            findDntContacts(userId,function (dnt_contacts) {

                var existContent = [];
                if(dnt_contacts && dnt_contacts.length>0){
                    existContent = existContent.concat(dnt_contacts);
                }

                if (!error && checkRequired(result) && result.length > 0) {

                    if (checkRequired(result[0].existEmailIds) && result[0].existEmailIds.length > 0) {
                        existContent = existContent.concat(result[0].existEmailIds);
                    }
                    var nonExistContacts = [];

                    for (var i = 0; i < contacts.length; i++) {
                        var exist = false;
                        if (contacts[i].personEmailId) {
                            if (existContent.indexOf(contacts[i].personEmailId.toLowerCase()) != -1 || existContent.indexOf(contacts[i].personEmailId.toLowerCase()) != -1) {
                                exist = true;
                            }

                            if (!exist) {
                                nonExistContacts.push(contacts[i]);
                            }
                        }
                    }

                    if (nonExistContacts.length > 0) {
                        pushAllContactsToUserProfile(userId, nonExistContacts, callback)
                    } else if (callback) callback()

                } else if (error) {
                    logger.info('Error in addContactNotExist():Contact ', error);
                    if (callback) callback()
                } else {

                    pushAllContactsToUserProfile(userId, contacts, callback);
                }
            });
        });
    } else if (callback) callback(true)
};

Contact.prototype.updateContactLosingTouchDate = function(userId, contactsEmailIdList, callback) {
    if (checkRequired(userId) && contactsEmailIdList && contactsEmailIdList.length > 0) {
        var date = new Date();
        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
        for (var i = 0; i < contactsEmailIdList.length; i++) {
            bulk.find({ _id: userId, "contacts.personEmailId": contactsEmailIdList[i] }).update({ $set: { "contacts.$.losingTouchRecommendation.date": date } });
        }

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updateContactLosingTouchDate():Contact Bulk', err, userId.toString())
            } else {
                result = result.toJSON();
                logger.info('updateContactLosingTouchDate() results ', result, userId.toString());
            }

            if (callback) { callback(err, result) }
        });
    }
};

Contact.prototype.updateContactsAccountSelectedBulk = function(userId, contacts, castToObjectId, callback) {

    if (checkRequired(userId) && contacts && contacts.length > 0) {

        var date = new Date();
        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
        for (var i = 0; i < contacts.length; i++) {
            //Last modified 22 Feb 2016 by Naveen. This used to update all the account details.
            bulk.find({ _id: userId, "contacts._id": castToObjectId(contacts[i].contactId) }).update({ $set: { "contacts.$.account": { name: contacts[i].accountName, selected: contacts[i].selected, updatedOn: date, showToReportingManager: contacts[i].showToReportingManager } } });
            //bulk.find({_id:userId,"contacts._id":castToObjectId(contacts[i].contactId)}).update({$set:{showToReportingManager:contacts[i].showToReportingManager}});
        }

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updateContactsAccountSelectedBulk():Contact Bulk', err, userId.toString())
            } else {
                result = result.toJSON();
                logger.info('updateContactsAccountSelectedBulk() results ', result, userId.toString());
            }

            if (callback) { callback(err, result) }
        });
    }
};

Contact.prototype.updatIsSalesforceContact = function(userId, emailIds, callback) {

    if (checkRequired(userId) && emailIds && emailIds.length > 0) {

        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
        for (var i = 0; i < emailIds.length; i++) {
            bulk.find({ _id: userId, "contacts.personEmailId":emailIds[i] }).update({ $set: { "contacts.$.isSalesforceContact": true } });
        }

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updatIsSalesforceContact():Contact Bulk', err, userId.toString())
            } else {
                result = result.toJSON();
                logger.info('updatIsSalesforceContact() results ', result, userId.toString());
            }

            if (callback) { callback(err, result) }
        });
    }
};

function pushAllContactsToUserProfile(userId, contacts, callback) {

    var accounts = _.compact(_.pluck(contacts,"account.name"));

    if(accounts && accounts.length>0){
        mapAccountVisibilityToExistingSettings(userId,accounts,contacts,callback)
    } else {
        addUsers(userId,contacts,callback)
    }
}

function mapAccountVisibilityToExistingSettings(userId,accounts,contacts,callback) {

    myUserCollection.aggregate([
        {
            $match:{
                _id:userId
            }
        },
        {
            $unwind:"$contacts"
        },
        {
            $match:{'contacts.account.name':{$in:accounts}}
        },
        {
            $group:{
                _id: null,
                contacts: {
                    $push: {
                        account:'$contacts.account.name',
                        showToReportingManager:'$contacts.account.showToReportingManager'
                    }
                }
            }
        }
    ]).exec(function (err,results) {

        if(!err && results.length>0 && results[0].length>0){
            var contactsResult = results[0].contacts;
            var userCompanyFromContactsObject = {}
            _.each(contactsResult, function(u){
                userCompanyFromContactsObject[u.account] = u.showToReportingManager
            })

            contacts = _.map(contacts, function(r){
                r.account.showToReportingManager = userCompanyFromContactsObject[r.account.name]
                return r;
            });

            addUsers(userId,contacts,callback)

        } else {
            addUsers(userId,contacts,callback) // Extreme Unknown conditions fallback
        }
    })
}

function addUsers(userId,contacts,callback) {
    myUserCollection.update({ _id: userId }, { $push: { contacts: {$each:contacts }} }, function(error, result) {
        if (error) {
            logger.info('Error in pushAllContactsToUserProfile():Contact ', error);
        }
        if (callback) callback(true)
    });
}

function concatAllContacts2(all, some) {

    var allContacts = [];
    if (checkRequired(all) && all.length > 0) {
        for (var i = 0; i < all.length; i++) {
            if (all[i]._id != true) {
                allContacts = allContacts.concat(all[i].contacts);
            }
        }
    }
    allContacts = allContacts.concat(some);
    allContacts.sort(function(a, b) {
        return b.relatasUser - a.relatasUser;
    });

    return allContacts;
}

Contact.prototype.removeInvalidContacts = function(callback) {
    var stream = myUserCollection.find().stream();
    var list = {};
    stream.on('data', function(doc) {
        list[doc.emailId] = [];
        if (doc.contacts && doc.contacts.length > 0) {
            var contactsList = []
            for (var i = 0; i < doc.contacts.length; i++) {
                if (!checkRequired(doc.contacts[i]._id)) {
                    //list[doc.emailId].push(doc.contacts[i]);
                    //doc.contacts.splice(i, 1);
                } else if (!checkRequired(doc.contacts[i].personEmailId)) {
                    contactsList.push(doc.contacts[i]._id);
                    //doc.contacts.splice(i, 1);
                } else if (checkRequired(doc.contacts[i].personEmailId) && !checkRequired(doc.contacts[i].personEmailId.trim())) {
                    contactsList.push(doc.contacts[i]._id);
                    //doc.contacts.splice(i, 1);
                } else if (!checkRequired(doc.contacts[i].personName)) {
                    contactsList.push(doc.contacts[i]._id);
                    //doc.contacts.splice(i, 1);
                }
            }
            updateUser(doc._id, doc.emailId, contactsList);
        }
    });

    stream.on('error', function(err) {
        // handle err
        callback('error')
    })

    stream.on('close', function() {
        // all done
        //logger.info(JSON.stringify(list))
        callback("Done")

    })
}

function updateUser(userId, emailId, contactsList) {
    logger.info(userId, emailId, JSON.stringify(contactsList))
    if (contactsList.length > 0) {
        myUserCollection.update({ _id: userId }, { $pull: { contacts: { _id: { $in: contactsList } } } }, function(err, result) {
            logger.info(JSON.stringify(result));
        })
    }
}

function checkRequired(data) {
    if (data == '' || data == null || data == undefined) {
        return false;
    } else {
        return true;
    }
}

Contact.prototype.getNetworkAndExtendedNetworkCount = function(currentUser, otherUsers, companyName, callBack) {
    var userIds = otherUsers.concat(currentUser)

    myUserCollection.aggregate([{
        $match: { _id: currentUser }
    }, {
        $unwind: "$contacts"
    }, {
        $match: { "contacts.account.name": { $eq: companyName } }
    }, {
        //$project: {
        //_id: 0,
        //email: "$contacts.personEmailId"
        //}
        $group: {
            _id: null,
            emails: { $addToSet: "$contacts.personEmailId" }
        }
    }]).exec(function(err, myContacts) {
        myUserCollection.aggregate([{
            $match: { _id: { $in: otherUsers } }
        }, {
            $unwind: "$contacts"
        }, {
            $match: { "contacts.account.name": { $eq: companyName } }
        }, {
            $group: {
                _id: null,
                emails: { $addToSet: "$contacts.personEmailId" }
            }
        }]).exec(function(err, othersContacts) {
            var response = { myNetwork: 0, extendedNetwork: 0 }
            if (myContacts.length > 0) {
                myContacts = myContacts[0].emails
                response.myNetwork = myContacts.length
            }
            if (othersContacts.length > 0)
                othersContacts = othersContacts[0].emails

            response.extendedNetwork = _.difference(othersContacts, myContacts).length
            callBack(response)
        })
    })
}

Contact.prototype.searchContactWithinCompany = function(selectedCompany, searchContent, userId, userIds, callback) {

    var userMatch = { _id: { $in: userIds } };

    var q = {
        "contacts.account.name": { $exists: true, $ne: null },
        $or: [{ "contacts.account.showToReportingManager": true }, { "_id": userId }]
    };
    if (checkRequired(selectedCompany))
        q["contacts.account.name"] = new RegExp('^' + selectedCompany, 'i')
    if (checkRequired(searchContent))
        q["contacts.personName"] = new RegExp('^' + searchContent, 'i')

    myUserCollection.aggregate([{
            $match: userMatch
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }, {
            $project: { contacts: 1 }
        }, {
            $group: {
                _id: "$contacts.personEmailId",
                name: { $first: "$contacts.personName" }
            }
        }]).exec(function(error, data) {
            if (data && data[0]) {
                var emails = _.pluck(data, "_id")
                myUserCollection.find({ emailId: { $in: emails } }, { profilePicUrl: 1, emailId: 1, designation: 1, companyName: 1 }).exec(function(err, profilePics) {
                    var usersWithProfilePic = {}
                    _.each(profilePics, function(pic) {
                        //usersWithProfilePic[pic.emailId] = pic.profilePicUrl
                        usersWithProfilePic[pic.emailId] = pic
                    })
                    data.map(function(user) {
                        if (usersWithProfilePic[user["_id"]]) {
                            user.profilePic = usersWithProfilePic[user["_id"]].profilePicUrl
                            user.designation = usersWithProfilePic[user["_id"]].designation
                            user.companyName = usersWithProfilePic[user["_id"]].companyName
                        }
                        return user
                    })
                    callback(error, data);
                })
            } else callback(error, []);
        })

};

/* Company or Customer landing*/
Contact.prototype.searchUserContacts_accounts = function(searchContent, userId, userIds, skip, limit, filters, callback) {

    var userMatch = { _id: { $in: userIds } };
    var q = { "contacts.account.name": { $exists: true, $ne: null } };

    var group = {
        _id: "$c.account.name",
        emailIdList: { $addToSet: "$c.personEmailId" }
    }

    if (checkRequired(searchContent)) {
        q["contacts.account.name"] = new RegExp('^' + searchContent, 'i')
        group = {
            _id: { $toLower:"$c.account.name" },
            emailIdList: { $addToSet: "$c.personEmailId" }
        }
    }

    myUserCollection.aggregate([{
            $match: userMatch
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }, {
            $group: {
                _id: null,
                contacts: { $push: { contact: "$contacts", userId: "$_id" } }
            }
        }, {
            $unwind: "$contacts"
        }, {
            $project: {
                contacts: { $cond: { if: { $eq: ["$contacts.userId", userId] }, then: "$contacts.contact", else: null } },
                contacts2: { $cond: { if: { $eq: ["$contacts.userId", userId] }, then: null, else: "$contacts.contact" } }
            }
        }, {
            $project: {
                contacts: 1,
                contacts2: { $cond: { if: { $eq: ["$contacts2.account.showToReportingManager", true] }, then: "$contacts2", else: null } }
            }
        },
        {
            $group: {
                _id: null,
                contacts: { $push: "$contacts" },
                contacts2: { $push: "$contacts2" }
            }
        }
        , {
            $project: {
                c: { $setUnion: ["$contacts", "$contacts2"] }
            }
        }, {
            $unwind: "$c"
        }
        ,
        {
            $match: { c: { $ne: null } }
        }, {
            $group: group
        }, {
            $project: {
                _id: 1,
                contactsCount: { $size: "$emailIdList" }
            }
        }, {
            $sort: { _id: 1 }
        }
    ]).exec(function(error, data) {
            if (data && data[0]) {
                callback(error, data);
            } else callback(error, []);
        })
};

Contact.prototype.addContactNotExistRefresh = function(userId, userEmailId, contacts, mobileNumbers, emailIds, source, callback) {

    var q = {
        $or: []
    };
    var qExist = false;
    if (mobileNumbers.length > 0) {
        qExist = true;
        q.$or.push({ "contacts.mobileNumber": { $in: mobileNumbers } });
    }
    if (emailIds.length > 0) {
        qExist = true;
        q.$or.push({ "contacts.personEmailId": { $in: emailIds } });
    }
    if (qExist) {
        myUserCollection.aggregate([{
            $match: { _id: userId }
        }, {
            $unwind: "$contacts"
        }, {
            $match: {
                $or: [
                    { "contacts.personEmailId": { $in: emailIds } }
                ]
            }
        }, {
            $group: {
                _id: null,
                existEmailIds: { $addToSet: { $toLower: "$contacts.personEmailId" } }
            }
        }]).exec(function(error, result) {
            var existContent = [];
            if (!error && checkRequired(result) && result.length > 0) {

                if (checkRequired(result[0].existEmailIds) && result[0].existEmailIds.length > 0) {
                    existContent = existContent.concat(result[0].existEmailIds);
                }
                var nonExistContacts = [];

                for (var i = 0; i < contacts.length; i++) {
                    var exist = false;
                    if (contacts[i].personEmailId) {
                        if (existContent.indexOf(contacts[i].personEmailId) != -1 || existContent.indexOf(contacts[i].personEmailId.toLowerCase()) != -1) {
                            exist = true;
                        }
                    }

                    if (!exist) {
                        nonExistContacts.push(contacts[i]);
                    }
                }

                if (nonExistContacts.length > 0) {

                    var contactsAdded = (contacts.length - result[0].existEmailIds.length)
                    pushAllContactsToUserProfile(userId, nonExistContacts, callback(error, contactsAdded))
                } else if (callback) {
                    var contactsAdded = (contacts.length - result[0].existEmailIds.length)
                    callback(error, contactsAdded)
                }

            } else if (error) {
                logger.info('Error in addContactNotExist():Contact ', error);
                if (callback) callback(error)
            } else {
                pushAllContactsToUserProfile(userId, contacts, callback);
            }
        });
    } else if (callback) callback(qExist)
};

Contact.prototype.updateContactValueContactId = function(userId, contactValue, callback) {

    var updateObj;

    if (contactValue.type == 'inProcess') {
        updateObj = { "contacts.$.account.value.inProcess": contactValue.value }
    } else if (contactValue.type == 'won') {
        updateObj = { "contacts.$.account.value.won": contactValue.value }
    } else if (contactValue.type == 'lost') {
        updateObj = { "contacts.$.account.value.lost": contactValue.value }
    }

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: { _id: contactValue.contactId } } }, { $set: updateObj }, function(err, res) {
        if (res) {
            callback(true)
        } else {
            callback(false)
        }
    });
};

Contact.prototype.updateContactValue = function(userId, contactValue, callback) {

    var updateObj;

    if (contactValue.type == 'inProcess') {
        updateObj = { "contacts.$.account.value.inProcess": contactValue.value }
    } else if (contactValue.type == 'won') {
        updateObj = { "contacts.$.account.value.won": contactValue.value }
    } else if (contactValue.type == 'lost') {
        updateObj = { "contacts.$.account.value.lost": contactValue.value }
    }

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: { personEmailId: contactValue.email } } }, { $set: updateObj }, function(err, res) {
        if (res) {
            callback(true)
        } else {
            callback(false)
        }
    });
};

Contact.prototype.createContact = function(userId,emailId, data, callback) {

    this.getContactsByEmailId(userId,[data.personEmailId],function (err,contacts) {

       if(contacts && contacts.length == 0){
           var updateObj = getContactObj(data)

           myUserCollection.update({ emailId: emailId }, { $push: { contacts: { $each:[updateObj] } } }, function(error, result) {
               if (error) {
                   if (checkRequired(callback))
                       callback(error, false)
               } else if (result && result.ok) {
                   if (checkRequired(callback))
                       callback(error, true)

               } else {
                   if (checkRequired(callback))
                       callback(error, false)
               }
           });
       } else {
           callback(null,false)
       }
    })
};

function getContactObj(data) {
    return {
        personId:data.null,
        personName:data.personName,
        personEmailId:data.personEmailId,
        birthday:null,
        companyName:data.companyName || null,
        designation:data.designation || null,
        count:0,
        addedDate:new Date(),
        verified:false,
        relatasUser:false,
        relatasContact:true,
        mobileNumber:data.mobileNumber || null,
        location:data.location || null,
        lat:data.lat || null,
        lng:data.lng || null,
        source:'relatas-create',
        account:{
            name:data.personEmailId?fetchCompanyFromEmail(data.personEmailId):null,
            selected:false,
            updatedOn:new Date()
        },
        contactImageLink:null
    }
}

function fetchCompanyFromEmail(email){

    if(checkRequired(email)){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            word = word.toLowerCase();
            return removableTextList.indexOf(word.trim()) > -1
        })
        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
    } else {
        return null
    }
}

Contact.prototype.updateRemindToConnect = function(userId, updateObj, callback) {

    myUserCollection.update({ _id: userId, contacts: { $elemMatch: { _id: updateObj.contactId } } }, { $set: { "contacts.$.remindToConnect": updateObj.remind } }, function(err, res) {
        if (res) {
            callback(true)
        } else {
            callback(false)
        }
    });
};

Contact.prototype.updateContactDetails = function(userId,personEmailId,contactId,matchByEmail,updateObj, callback) {

    var match = { $elemMatch: { personEmailId: personEmailId } };

    if(!matchByEmail){
        match = { $elemMatch: { _id: contactId } }
    }

    myUserCollection.update({ _id: userId, contacts: match }, { $set: updateObj }, function(err, res) {

        if (res) {
            callback(err,true)
        } else {
            callback(err,false)
        }
    });
};

Contact.prototype.updateContactDetailsMulti = function(userId,personEmailIds,updateObj, callback) {

    var match = { $elemMatch: { personEmailId: {$in:personEmailIds} } };


    var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
    _.each(personEmailIds,function (el) {
        bulk.find({ _id: userId, contacts:{$elemMatch: { personEmailId: el}}})
            .update({ $set: updateObj });
    });

    bulk.execute(function(err, result) {
        if (err) {
            logger.info('Error in updateContactDetailsMulti():Contact Bulk', err, userId.toString())
        } else {
            result = result.toJSON();
            logger.info('updateContactDetailsMulti() results ', result.nModified, userId.toString());
        }

        if (callback) { callback(err, result) }
    });
};

Contact.prototype.updateLatLngContactsCollection = function(userId, contactEmailId,contactMobile,lat,lng,location,designation,callback) {

    var find = {
        ownerId: userId,
        $or:[{personEmailId: contactEmailId},{mobileNumber: contactMobile}]
    };

    if(contactEmailId && contactMobile){
        find = {
            ownerId: userId,
            $or:[{personEmailId: contactEmailId},{mobileNumber: contactMobile}]
        };
    }


    if(contactEmailId && !contactMobile){
        find = {
            ownerId: userId,
            $or:[{personEmailId: contactEmailId}]
        };
    }

    if(contactMobile && !contactEmailId){
        find = {
            ownerId: userId,
            $or:[{mobileNumber: contactMobile}]
        };
    }

    var update = {
        personEmailId:contactEmailId
    };

    if(!contactEmailId && contactMobile){
        update = {
            mobileNumber:contactMobile
        }
    }

    if(lat && lng && location){
        update.location = location
        update.lat = lat
        update.lng = lng
    }

    if(designation){
        update.designation = designation
    }

    contactsCollection.update(find, { $set: update },{multi:true}, function(err, result) {
        if(callback){
            callback()
        }
    });
}

Contact.prototype.updateLocation = function(userId, contactEmailIds,lat,lng,location,callback) {

    if(lat && lng){

        var find = {
            ownerId: userId,
            $or:[{personEmailId: {$in:contactEmailIds}}]
        };

        contactsCollection.update(find, { $set: {
                "location":location,
                "lat":lat,
                "lng":lng
            } },{multi:true}, function(err, result) {
            if(callback){
                callback()
            }
        });
    } else {
        if(callback){
            callback()
        }
    }
}

Contact.prototype.getContactValues = function(userId, contactEmailId,contactMobile, callback) {

    var q = { _id: userId ,"contacts.personEmailId": contactEmailId}

    if(isNumber(contactEmailId)){
        q = { _id: userId, "contacts.mobileNumber": contactEmailId }
    }
    if(contactMobile){
        q = { _id: userId, contacts:{$elemMatch: { personEmailId: contactEmailId, mobileNumber: contactMobile }} }
    }

    myUserCollection.findOne(q, { "contacts.$": 1 }, function(error, user) {

        if (checkRequired(user) && checkRequired(user.contacts) && user.contacts.length > 0) {
            callback(user.contacts[0].account.value)
        } else {
            callback(error, [])
        }
    })
};

Contact.prototype.getContactsCount = function(userId,callback){
    myUserCollection.aggregate([
        {
            $match: { _id: { $in: userId } }
        },
        {
            $unwind: "$contacts"
        },
        {
            $group: {
                _id: null,
                contactsCount: {$sum: 1}
            }
        }
    ]).exec(function(err,count){
        if(!err){
            callback(count)
        } else {
            callback([])
        }
    })
}

Contact.prototype.getContactsAccountCount = function(userId,callback){
    myUserCollection.aggregate([
        {
            $match: { _id:userId }
        },
        {
            $unwind: "$contacts"
        },
        {
            $match:{
                'contacts.account.name':{$nin:[null,'',' ',false]}
            }
        },
        {
            $group: {
                _id:'$contacts.account.name',
                // contactsWithAccountsCount: {$sum: 1}
            }
        }
    ]).exec(function(err,count){

        if(!err){
            callback({contactsWithAccountsCount:count.length})
        } else {
            callback(0)
        }
    })
}

Contact.prototype.getUsersAccountInteractionGrowth = function(userIds,date,callback){

    myUserCollection.aggregate([
        {
            $match: { _id:{$in:userIds} }
        },
        {
            $unwind: "$contacts"
        },
        {
            $match:{
                'contacts.account.name':{$nin:[null,'',' ',false]},
                "contacts.addedDate": { $gte: new Date(date) }
            }
        },
        {
          $project:{
              emailId:"$emailId",
              personEmailId:"$contacts.personEmailId",
              account:{$toLower:"$contacts.account.name"},
              addedDate:"$contacts.addedDate",
              month: {$month: "$contacts.addedDate"},
              year: {$year: "$contacts.addedDate"}
          }
        },
        {
            $sort: { "addedDate": 1 }
        },
        {
            $group: {
                _id:{emailId:'$emailId',account:"$account"},
                addedDates:{
                    $push: {
                        addedDate:"$addedDate"
                    }
                }
            }
        }
    ]).exec(function(err,count){
        callback(err,count)
    })
}

Contact.prototype.updateContactNameAndProfileImage = function (userId,contacts,castToObjectId,callback)  {

    if (checkRequired(userId) && contacts && contacts.length > 0) {
        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
        var len = contacts.length;

        for (var i = 0; i<len; i++) {
            var updateObj = {
                "contacts.$.personName": contacts[i].personName,
                "contacts.$.companyName": contacts[i].companyName,
                "contacts.$.designation": contacts[i].designation,
                // "contacts.$.contactImageLink": contacts[i].contactImageLink
                "contacts.$.location": contacts[i].location
            }
            bulk.find({ _id: userId, "contacts.personEmailId": contacts[i].personEmailId,"contacts.personId":{$exists:true} }).update({ $set: updateObj });
        }

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updateContactNameAndProfileImageBulk():Contact Bulk', err, userId.toString())
            } else {
                result = result.toJSON();
                logger.info('updateContactNameAndProfileImageBulk() results ', result.nModified, userId.toString());
            }

            if (callback) { callback(err, result) }
        });
    }
}

Contact.prototype.oppTransferContactUpdate = function (contacts,callback)  {

    if (contacts && contacts.length > 0) {
        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();

        _.each(contacts,function (co) {

            bulk.find({ _id: co.ownerId, "contacts.personEmailId": {$ne:co.personEmailId}})
                .update({
                    $push:{
                        contacts:{
                            personId:co.personId,
                            personName:co.personName,
                            personEmailId:co.personEmailId,
                            birthday:co.birthday,
                            companyName:co.companyName,
                            designation:co.designation,
                            addedDate:new Date(),
                            verified:co.verified,
                            relatasUser:co.relatasUser,
                            relatasContact:co.relatasContact,
                            favorite:co.favorite,
                            mobileNumber:co.mobileNumber,
                            location:co.location,
                            source:'opp-transfer',
                            contactRelation : {
                                "decisionmaker_influencer" : null,
                                "prospect_customer" : "prospect",
                                "partner" : false,
                                "influencer" : false,
                                "decision_maker" : false
                            },
                            account:co.account,
                            contactImageLink:null
                        }
                    }
                });
        })

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in oppTransferContactUpdate():Contact Bulk', err)
            } else {
                result = result.toJSON();
                logger.info('oppTransferContactUpdate() results ', result);
            }

            if (callback) { callback(err, result) }
        });
    }
}

Contact.prototype.updateContactFromInteractions = function (userId,contacts,newId,common,callback) {

    if (checkRequired(userId) && contacts && contacts.length > 0) {
        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
        var len = contacts.length;

        for (var i = 0; i<len; i++) {

            var updateObj = {
                "contacts.$.personName": contacts[i],
                "contacts.$.companyName": null,
                "contacts.$.designation": null,
                "contacts.$.contactImageLink": null,
                "contacts.$.personEmailId": contacts[i],
                "contacts.$.mobileNumber": null,
                "contacts.$.source": 'google-email-interactions',
                "contacts.$.account": {
                    name:common.fetchCompanyFromEmail(contacts[i]),
                    selected:false,
                    updatedOn:new Date()
                },
                "contacts.$.relatasUser": false
            }

            bulk.find({ _id: userId, "contacts.personEmailId": { $ne: contacts[i].personEmailId}}).update({ $push: updateObj });
        }

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updateContactFromInteractions():Contact Bulk', err, userId.toString())
            } else {
                result = result.toJSON();

                logger.info('updateContactFromInteractions() results ', result.nModified, userId.toString());
            }

            if (callback) { callback(err, result) }
        });
    }
}

Contact.prototype.updateAccountVisibility = function (userId,contacts,castToObjectId,callback) {

    if (checkRequired(userId) && contacts && contacts.length > 0) {

        var date = new Date();
        var bulk = myUserCollection.collection.initializeUnorderedBulkOp();

        for (var i = 0; i < contacts.length; i++) {

            var updateObj = {
                "contacts.$.account.showToReportingManager": contacts[i].showToReportingManager,
                "contacts.$.account.updatedOn": date
            }

            //Last modified 22 Feb 2016 by Naveen. This used to update all the account details.
            // { "contacts.$.account": { name: contacts[i].accountName, selected: contacts[i].selected, updatedOn: date, showToReportingManager: contacts[i].showToReportingManager } }
            bulk.find({ _id: userId, "contacts._id": castToObjectId(contacts[i].contactId) }).update({ $set: updateObj });
            //bulk.find({_id:userId,"contacts._id":castToObjectId(contacts[i].contactId)}).update({$set:{showToReportingManager:contacts[i].showToReportingManager}});
        }

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updateContactsAccountSelectedBulk():Contact Bulk', err, userId.toString())
            } else {
                result = result.toJSON();
                logger.info('updateContactsAccountSelectedBulk() results ', result, userId.toString());
            }

            if (callback) { callback(err, result) }
        });
    }
};

Contact.prototype.searchUserContactsMobile = function(searchContent, userId, callback) {

    var search = new RegExp(searchContent, 'i');
    var q = {
        $or: [{ "contacts.personName": search }, { "contacts.companyName": search }, { "contacts.designation": search }, { "contacts.hashtag": search }]
    };

    var projectContact = {
        contactId: "$contacts._id",
        personName: "$contacts.personName",
        designation: "$contacts.designation",
        personEmailId: "$contacts.personEmailId",
        mobileNumber: "$contacts.mobileNumber",
        company: "$contacts.company",
        personId: "$contacts.personId",
        hashtag:"$contacts.hashtag",
        ownerId:"$_id",
        favorite:"$contacts.favorite",
        contactRelation:"$contacts.contactRelation"
    };

    myUserCollection.aggregate([{
        $match: { _id: userId }
    }, {
        $unwind: "$contacts"
    }, {
        $match: q
    }, {
        $project: projectContact
    }]).exec(function(error, data) {

        if(!error && data){
            callback(error,data)
        } else {
            callback(error,null)
        }

    })
}

Contact.prototype.searchInvalidContacts = function(list,type,callback) {
    var match = {};
    if(type == 'invalid'){
        match = {"contacts.personEmailId":{$in:list.invalid,$nin:list.exception}}
    }
    else if(type == 'exception'){
        match  = {"contacts.personEmailId":{$in:list.exception}};
    }

    if(match) {
        myUserCollection.aggregate([
            {$unwind: "$contacts"}
            ,
            {$match: match},
            {
                $project: {
                    _id: 0,
                    userId: "$_id",
                    userEmailId: "$emailId",
                    contactEmailId: "$contacts.personEmailId",
                    contactId: "$contacts._id",
                    contactName: "$contacts.personName"
                }
            }
        ]).exec(function (error, data) {

            if (!error && data) {
                callback(error, data)
            } else {
                callback(error, null)
            }
        })
    }
    else{
        callback(true, null);
    }
}

Contact.prototype.searchInvalidContactsPagination = function(list,type,users,callback) {

    var match = {};

    if(type == 'invalid'){
        match = {"contacts.personEmailId":{$in:list.invalid,$nin:list.exception}}
    }
    else if(type == 'exception'){
        match  = {"contacts.personEmailId":{$in:list.exception}};
    }

    if(match) {
        myUserCollection.aggregate([
            {
              $match:{_id:{$in:users}}
            },
            {$unwind: "$contacts"},
            {$match: match},
            {
                $project: {
                    _id: 0,
                    userId: "$_id",
                    userEmailId: "$emailId",
                    contactEmailId: "$contacts.personEmailId",
                    contactId: "$contacts._id",
                    contactName: "$contacts.personName"
                }
            }
        ]).exec(function (error, data) {

            if (!error && data) {
                callback(error, data)
            } else {
                callback(error, null)
            }
        })
    }
    else{
        callback(true, null);
    }
}

Contact.prototype.removeInvalidContactsFromDB = function(contacts,action,castToObjectId,callback) {
    var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
    if(action == 'delete') {
        contacts.forEach(function(a){
            bulk.find({_id: castToObjectId(a.userId.toString())}).update({$pull: {'contacts': {personEmailId: a.contactEmailId}}});
        });
    }
    else if(action == 'updateAsInactive') {
        contacts.forEach(function(a){
            // bulk.find({_id: a.userId,"contacts._id":{$in: a.contactsIds}}).update({$set: {'contacts.$.inactive':true}});
            bulk.find({_id: castToObjectId(a.userId.toString()),"contacts.personEmailId": a.contactEmailId}).update({$set: {'contacts.$.inactive':true}});
        });
    }

    if(contacts.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                callback(err, null)
                logger.info('Error in removeInvalidContacts():Contact Bulk', err)
            } else {
                result = result.toJSON();
                callback(null, result);
                logger.info('removeInvalidContacts() results ', result);
            }
        });
    }
    else{
        callback(null,null)
    }
}

Contact.prototype.insertContact = function (emailId,contacts,callback) {
    myUserCollection.update({ emailId: emailId }, { $push: { contacts: { $each:contacts } } }, function(error, result) {
        if (error) {
            if (checkRequired(callback))
                callback(error, false)
        } else if (result && result.ok) {
            if (checkRequired(callback))
                callback(error, true)

        } else {
            if (checkRequired(callback))
                callback(error, false)
        }
    });
}

Contact.prototype.createFakeProfile = function (profile,callback) {

    // var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
    //
    // _.each(profile,function (pro) {
    //    bulk.insert(pro)
    // });
    //
    // bulk.execute(function(err,res){
    //     if(err) {
    //         callback(err,false)
    //     }
    //     else{
    //         if(callback){
    //             callback(err,true)
    //         }
    //     }
    // });

    myUserCollection.collection.insert(profile, function(error, result) {
        if (error) {
            if (checkRequired(callback))
                callback(error, false)
        } else if (result && result.ok) {
            if (checkRequired(callback))
                callback(error, true)

        } else {
            if (checkRequired(callback))
                callback(error, false)
        }
    });
}

Contact.prototype.getFavoriteContactsByMobileOrEmailId = function(userId,searchBy,callback) {

    var query = {};

    if(isNumber(searchBy)){
        query = {
            // "contacts.favorite": true,
            "contacts.mobileNumber": new RegExp(searchBy, 'i')
        }
    } else if(validateEmail(searchBy)){
        query = {
            // "contacts.favorite": true,
            "contacts.personEmailId": new RegExp(searchBy, 'i')
        }
    } else {
        callback(true,null)
    }

    myUserCollection.aggregate([{
        $match: { _id: userId }
    },{
        $unwind: "$contacts"
    },{
        $match: query
    },
        {
            $group: {
                _id: null,
                contact: {
                    $push: {
                    _id: "$contacts._id",
                    personId: "$contacts.personId",
                    personName: "$contacts.personName",
                    personEmailId: "$contacts.personEmailId",
                    companyName: "$contacts.companyName",
                    designation: "$contacts.designation",
                    mobileNumber: "$contacts.mobileNumber",
                    contactRelation: "$contacts.contactRelation",
                    favorite: "$contacts.favorite",
                    twitterUserName: "$contacts.twitterUserName",
                    facebookUserName: "$contacts.twitterUserName",
                    linkedinUserName: "$contacts.twitterUserName",
                    hashtag: "$contacts.hashtag",
                    contactImageLink: "$contacts.contactImageLink"
                    }
                }
            }
        }]
    ).exec(function (error, data) {

        if (!error && data) {
            callback(error, data)
        } else {
            callback(error, null)
        }

    })
}

Contact.prototype.updateTwitterHandlesForCompanyOrPerson = function (userId,data,updateFor,callback) {

    var bulk = myUserCollection.collection.initializeUnorderedBulkOp();

    if(updateFor == "person" && userId){
        for(var i = 0;i<data.length;i++){

            var updateObj = {
                "contacts.$.twitterUserName": data[i].twitterUserName
            };

            bulk.find({_id:userId,"contacts.personEmailId":data[i].email}).update({$set:updateObj})
        }
    }
    else if(updateFor == "company" && userId){
        for(var i = 0;i<data.length;i++){

            var updateObj = {
                "contacts.$.account.twitterUserName": data[i].twitterUserName
            };

            bulk.find({_id:userId,"contacts.account.name":data[i].updateFor}).update({$set:updateObj})
        }
    }

    bulk.execute(function(err,res){
        if(err) {
            callback(true)
        }
        else{
            if(callback){
                callback(true)
            }
        }
    });

};

function getRelatasUsersInUsersContactList(userId,callback){

    var projectContact = {
        "_id":1,
        // "emailId":1,
        "contacts._id":1,
        "contacts.personId":1,
        "contacts.personName":1,
        "contacts.personEmailId":1,
        // "contacts.designation":1,
        // "contacts.mobileNumber":1
    };

    myUserCollection.aggregate(
        [{
            $match:{_id:userId}
        },
        {
            $unwind:"$contacts"
        },
        {
          $match:{'contacts.personId':{$nin:[null,'',' ']}}
        },
        {
            $project: projectContact

        }]).exec (function(error, contacts){

        if(!error && contacts.length>0){
            callback(contacts)
        }

        else
            callback([])
    });
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

module.exports = Contact;
