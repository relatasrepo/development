var contactsCollection = require('../databaseSchema/contactSchema').contacts;
var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var AccountManagement = require('../dataAccess/accountManagement');
var moment = require('moment');
var dntContactsCollection = require('../databaseSchema/dntContactSchema').dntContacts;

var winstonLog = require('../common/winstonLog');
var accObj = new AccountManagement();

var _ = require("lodash");

function ContactsManagement(){}

ContactsManagement.prototype.fetchExistingContacts = function(userId,contacts,callback){

    contactsCollection.aggregate([
        {
            $match:{ownerId:userId,personEmailId:{$in:contacts}}
        },
        {
            $group:{
                _id:null,
                contacts:{$push:"$personEmailId"}
            }
        }
    ]).exec(function (err,data) {
        if(!err && data && data[0] && data[0].contacts){

            callback(err,data[0].contacts)
        } else {
            callback(err,[])
        }
    });

};

ContactsManagement.prototype.insertContacts = function(userId,ownerEmailId,emailIds,contactObjList,lastLoginDate,callback){

    if(contactObjList){

        this.fetchExistingContacts(userId,emailIds,function (err,contactsList) {
            dntContactsCollection.find({ownerId:userId},{personEmailId:1},function (err_dnt,dnt_contacts) {
                var nonExistingEmailIds = [];
                if(!err_dnt && dnt_contacts && dnt_contacts.length>0){
                    nonExistingEmailIds = nonExistingEmailIds.concat(_.pluck(dnt_contacts,"personEmailId"));
                }

                var nonExistingContacts = contactObjList.filter(function (co) {
                    co.ownerId = userId;
                    co.ownerEmailId = ownerEmailId;
                    co.interactionActivity = {
                        received:0,
                        sent:0,
                        ownerFeedback:0
                    }

                    if(contactsList.indexOf(co.personEmailId) === -1){
                        nonExistingEmailIds.push(co.personEmailId)
                    }

                    return contactsList.indexOf(co.personEmailId) === -1;
                });

                if(nonExistingContacts && nonExistingContacts.length>0){
                    contactsCollection.collection.insert(nonExistingContacts,function (err1,contacts) {
                        callback(err,contacts)
                    });
                } else {
                    if(callback){
                        callback()
                    }
                }
            })
        });
    } else {
        if(callback){
            callback()
        }
    }
    
};

ContactsManagement.prototype.updateAccounts = function (userId,callback) {

    myUserCollection.findOne({_id:userId},{emailId:1,corporateUser:1,companyName:1,companyId:1,contacts:1,lastAccountUpdated:1},function (err,profile) {
        if(!err && profile && profile.companyId){
            var liuAcc = fetchCompanyFromEmail(profile.emailId)
            if(!liuAcc && profile.corporateUser){
                liuAcc = profile.companyName
            }

            kickOffUpdate(profile,liuAcc,userId,callback);
        } else {
            if(callback){
                callback()
            }
        }
    });
}

ContactsManagement.prototype.updateAccountWhenContactAdded = function (userId) {

    myUserCollection.findOne({_id:userId},{emailId:1,corporateUser:1,companyName:1,companyId:1,contacts:1},function (err,profile) {
        if(!err && profile && profile.companyId){
            var liuAcc = fetchCompanyFromEmail(profile.emailId)
            if(!liuAcc && profile.corporateUser){
                liuAcc = profile.companyName
            }

            kickOffUpdate(profile,liuAcc,userId);
        }
    });
}

function kickOffUpdate(profile,liuAcc,userId,callback) {

    var contacts = [];
    var updateObj = {
        names:[],
        accounts:[]
    };

    //7 July 2017.
    // Group by accounts and then update.

    _.each(profile.contacts,function (co) {
        if(co.personEmailId){
            var name = fetchCompanyFromEmail(co.personEmailId);
            if(name && name.toLowerCase() != liuAcc.toLowerCase()){
                contacts.push(co.personEmailId)
                updateObj.names.push(name);

                updateObj.accounts.push({
                    companyId:profile.companyId,
                    ownerId:profile.ownerId,
                    ownerEmailId:profile.emailId,
                    name:name.toLowerCase(),
                    createdDate:new Date(),
                    access:[{
                        emailId:profile.emailId
                    }],
                    reportingHierarchyAccess:true
                })
            }
        }
    });

    accObj.updateAccounts(profile.companyId,updateObj,contacts,function (error,results) {
        myUserCollection.update({_id:userId},{$set:{lastAccountUpdated: new Date()}},{upsert:true},function (err,result) {

            if(callback){
                callback()
            }
        });
    })
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

ContactsManagement.prototype.updateInteractionActivity = function(userId,activities,callback){

    var bulk = contactsCollection.collection.initializeUnorderedBulkOp();

    if(activities && activities.length>0){


        _.each(activities,function (el) {

            bulk.find({ownerId:userId,personEmailId:el.cEmailIds})
                .update({
                    $set: {
                        "interactionActivity.ownerFeedback":el.hasReplied
                    },
                    $inc:{
                        "interactionActivity.received":el.received,
                        "interactionActivity.sent":el.sent
                    }});
        });

        bulk.execute(function(err, result) {
            if(callback) callback(err,result)
        });
    } else {
        if(callback){
            callback(null,null)
        }
    }

}

function fetchCompanyFromEmail(email){
    var domain = email.substring(email.lastIndexOf("@") +1)
        , words = domain.split(".")
        , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
        , removableTextList = ["com", "org", "edu", "in"]

    _.reject(words, function(word){
        return removableTextList.indexOf(word.trim()) > -1
    })

    words = _.sortBy(words, function(word){return -(word.length);})
    return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
}

module.exports = ContactsManagement;

