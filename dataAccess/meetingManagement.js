var scheduleInvitation = require('../databaseSchema/userManagementSchema').scheduleInvitation;
var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var winstonLog = require('../common/winstonLog');
var calendar = require('../databaseSchema/calendarListSchema');
var events = require('../databaseSchema/calendarSchema');
var duplication=require('./duplicatingMeetingOnUpdate');
var commonUtility = require('../common/commonUtility');
var mongoose = require('mongoose');

var logLib = new winstonLog();
var logger =logLib.getMeetingsErrorLogger();
var dup=new duplication();
var _ = require("lodash");
var moment = require("moment");
var momentTZ = require('moment-timezone');
var common = new commonUtility();

function MeetingManagementClass()
{

}

MeetingManagementClass.prototype.saveNewMeeting = function(meetingDetailsObj,callback){
    var  scheduleDetails =  new scheduleInvitation(meetingDetailsObj);
    scheduleDetails.save(function(error,meeting){
        if(error){
            logger.info('Error in saveNewMeeting():MeetingManagementClass ',error);
        }
        callback(error,meeting);
    })
};

MeetingManagementClass.prototype.addOrUpdateMeetingToDo = function(toDoObj,isNew,invitationId,callback){

    if(!isNew){

        scheduleInvitation.findOne({invitationId:invitationId,"toDo._id":toDoObj._id},{"toDo.$":1},function(error,doc){
            if(error){
                logger.info('Error in  addOrUpdateMeetingToDo():MeetingManagementClass 1 ',error);
                callback(false,false,false)
            }
            else{
                if(checkRequired(doc) && checkRequired(doc.toDo) && doc.toDo.length > 0){

                    if(doc.toDo[0].actionItem != toDoObj.actionItem || doc.toDo[0].assignedTo != toDoObj.assignedTo || new Date(doc.toDo[0].dueDate).toISOString() != new Date(toDoObj.dueDate).toISOString() || doc.toDo[0].status != toDoObj.status){
                        var googleUpdate = false;
                        if(doc.toDo[0].actionItem != toDoObj.actionItem || doc.toDo[0].assignedTo != toDoObj.assignedTo || new Date(doc.toDo[0].dueDate).toISOString() != new Date(toDoObj.dueDate).toISOString()){
                            googleUpdate = true;
                        }
                        scheduleInvitation.update({invitationId:invitationId,"toDo._id":toDoObj._id},{$inc:{"toDo.$.updateCount":1},$set:{"toDo.$.actionItem":toDoObj.actionItem,"toDo.$.assignedTo":toDoObj.assignedTo,"toDo.$.dueDate":toDoObj.dueDate,"toDo.$.updatedDate":new Date(),"toDo.$.status":toDoObj.status}},function(error,result){

                            if(error){
                                logger.info('Error in  addOrUpdateMeetingToDo():MeetingManagementClass 2 ',error);
                                callback(false,false,false)
                            }else
                            callback(true,true,true,toDoObj,googleUpdate);
                        })
                    }else callback(true,false,false);
                }else callback(true,false,false);
            }
        })
    }
    else{
        scheduleInvitation.update({invitationId:invitationId},{$push:{toDo:toDoObj}},function(error,result){
            if(error){
                logger.info('Error in  addOrUpdateMeetingToDo():MeetingManagementClass 3 ',error);
                callback(false,false,false)
            }
            else{
                callback(true,true,false);
            }
        })
    }
};

MeetingManagementClass.prototype.updateToDoWithGoogleEventId = function(eventId,meetingId,todoId){
    scheduleInvitation.update({invitationId:meetingId,"toDo._id":todoId},{$set:{"toDo.$.googleEventId":eventId},$inc:{"toDo.$.updateCount":1}},function(error,result){

    })
};

MeetingManagementClass.prototype.getPendingToDoItems = function(userId,dateMin,dateMax,filter,callback){
    var dueDateQ;
    if(filter == 'currentDay'){
        dueDateQ = {$gte:new Date(dateMin),$lte:new Date(dateMax)}
    }
    else{
        dueDateQ = {$gte:new Date(dateMin)}
    }

    scheduleInvitation.aggregate([
        {
            $match: {
                $or:[
                    {
                        toDo:{$elemMatch:{createdBy:userId}}
                    },
                    {
                        toDo:{$elemMatch:{assignedTo:userId}}
                    }
                ]
            }
        },
        {$unwind: '$toDo'},
        {
            $match:{
                'toDo.dueDate': dueDateQ,
                $or:[
                    {
                        'toDo.createdBy':userId
                    },
                    {
                        'toDo.assignedTo':userId
                    }
                ]
            }
        }
    ]).exec(function(e,d){
        callback(d)
    })
};

MeetingManagementClass.prototype.getPendingToDoItemsCount = function(userId,dateMin,callback){

    var projection = {
        "toDo.$":1,
        senderName:1,
        senderId:1,
        invitationId:1
    };
    scheduleInvitation.find({
        $or:[
            {
                toDo:{$elemMatch:{assignedTo:userId,dueDate:{$gte:dateMin}}}
            }
        ],
        "toDo.dueDate":{$gte:dateMin}
    },projection,function(error,meetings){

        if(error){
            logger.info('Error in  getPendingToDoItemsCount():MeetingManagementClass ',error);
        }
        callback(meetings);
    })
};

MeetingManagementClass.prototype.getPendingToDoItemsNotification = function(userId,dateMin,callback){

    scheduleInvitation.aggregate([
        {
            $match: {
                $or:[
                    {
                        toDo:{$elemMatch:{createdBy:userId}}
                    },
                    {
                        toDo:{$elemMatch:{assignedTo:userId}}
                    }
                ]
            }
        },
        {$unwind: '$toDo'},
        {
            $match:{
                'toDo.dueDate': {$gte:new Date(dateMin)},
                $or:[
                    {
                        'toDo.createdBy':userId
                    },
                    {
                        'toDo.assignedTo':userId
                    }
                ]
            }
        },
        {
            $group:{
                _id:null,
                todoArr:{
                    push:"$toDo"
                }
            }
        }
    ]).exec(function(e,d){
        if(checkRequired(d) && d.length > 0 && d[0] && d[0].todoArr && d[0].todoArr.length > 0){
            myUserCollection.populate(d[0].todoArr,{path:'createdBy', select:'firstName lastName emailId'},function(err,list){
                if(err){
                    callback(d[0].todoArr,false)
                }
                else callback(list,true)
            })
        }else
        callback([],false)
    })
};

MeetingManagementClass.prototype.removeToDoItem = function(userId,invitationId,toDoId,callback){
    scheduleInvitation.update({invitationId:invitationId},{$pull:{toDo:{_id:toDoId}}},function(error,result){

         if(error){
             logger.info('Error in  removeToDoItem():MeetingManagementClass ',error);
         }
         callback(result == 1);
    })
};

MeetingManagementClass.prototype.removeInsightMeeting = function(userId,invitationId,callback){

    var dateMin = momentTZ().subtract(0, "days");
    var dateMax = momentTZ().subtract(0, "days");

    dateMin.date(dateMin.date())
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    dateMax.date(dateMax.date())
    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(0)
    dateMax.millisecond(0)

    dateMin = dateMin.format();
    dateMax = dateMax.format();

    scheduleInvitation.remove({
        senderId:userId,
        invitationId:invitationId,
        actionItemSlotType: "meetingFollowUp",
        scheduleTimeSlots: {"$elemMatch": {"start.date": {'$gt': new Date(dateMin), '$lt': new Date(dateMax)}}}},function(error,result){
        
         if(error){
             logger.info('Error in  removeInsightMeeting():MeetingManagementClass ',error);
         }
         callback(result == 1);
    })
};

MeetingManagementClass.prototype.updateMeetingToDoStatus = function(dataObj,invitationId,callback){

    var updateObj = {$set:{"toDo.$.status":dataObj.status,"toDo.$.updatedDate":new Date()}};
    if(dataObj.deuDateUpdated){
        updateObj = {$inc:{"toDo.$.updateCount":1},$set:{"toDo.$.status":dataObj.status,"toDo.$.dueDate":dataObj.dueDate,"toDo.$.updatedDate":new Date()}};
    }

    scheduleInvitation.update({invitationId:invitationId,toDo:{ $elemMatch:{_id:dataObj._id,assignedTo:dataObj.assignedTo}}},updateObj,function(error,result){
        if(error){
            logger.info('Error in  updateMeetingToDoStatus():MeetingManagementClass ',error);
        }
        callback(result == 1)
    })
};

MeetingManagementClass.prototype.findOneCustomQuery = function(query,projection,callback){
    scheduleInvitation.findOne(query,projection,function(error,doc){
        callback(doc);
    })
};

MeetingManagementClass.prototype.findMeetingsCustomQuery = function(query,projection,callback){
    scheduleInvitation.find(query,projection,function(error,doc){
        callback(doc);
    })
};

MeetingManagementClass.prototype.removeUserMeetings = function(userId,callback){
    scheduleInvitation.remove({senderId:userId},function(error,result){
        if(error){
            callback(false,'onDeleteMeetings',error,result);
        }
        else callback(true,'onDeleteMeetings',error,result);
    })
};

MeetingManagementClass.prototype.getMeetingWithTodoPopulate = function(invitationId,callback){
    scheduleInvitation.findOne({$or:[{invitationId:invitationId},{recurrenceId:invitationId}],deleted:{$ne:true}},function(error,invitation){

        if (error) {
            invitation = false;
            callback(invitation,{message:'Error occured while searching for invitations '+error});
        }
        else{
            if(checkRequired(invitation) && checkRequired(invitation.toDo) && invitation.toDo.length > 0){
                myUserCollection.populate(invitation.toDo,{path:'createdBy', select:'firstName lastName emailId publicProfileUrl'},function(error,invi){

                    if(invi){
                        invitation.toDo = invi;
                        callback(invitation)
                    }
                    else callback(invitation)
                })
            }
            else{
                callback(invitation,{message:'Invitation found for invitation id'});
            }
        }
    });
};

MeetingManagementClass.prototype.populateMeetingToDoInfo = function(toDo,callback){
    myUserCollection.populate(toDo,[{path:'createdBy', select:'firstName lastName emailId publicProfileUrl timezone'},{path:'assignedTo', select:'firstName lastName emailId publicProfileUrl timezone'}],function(error,invi){

        if(invi){
            callback(invi)
        }
        else callback(false)
    })
};

MeetingManagementClass.prototype.updateMeetingToNotAccepted = function(invitationId,callback){
    scheduleInvitation.update({invitationId:invitationId},{$set:{"to.canceled":false}},function(error,result){
        if(error){
            callback(false)
        }
        else if(result && result.ok){
            callback(true)
        }
        else callback(false);
    })
};

MeetingManagementClass.prototype.updateMeetingCustomObj = function(invitationId,updateObj,callback){

    scheduleInvitation.update({invitationId:invitationId,deleted:{$ne:true}},updateObj,function(error,result){
        if(error){
            logger.info('Error in  updateMeetingCustomObj():MeetingManagementClass ',error);
        }

        if(checkRequired(callback)){
            callback(result == 1)
        }
    })
};

MeetingManagementClass.prototype.updateRelatasEvent = function(findQuery,updateObj,callback){

    scheduleInvitation.update(findQuery,updateObj,function(error,result){

        if(error){
            logger.info('Error in  updateRelatasEvent():MeetingManagementClass ',error);
            callback(error,false)
        } else {
            callback(error,true)
        }
    })
};

// MOBILE API RELATED
MeetingManagementClass.prototype.getPendingToDoItemsWithContact = function(userId,cUserId,dateMin,callback){

    scheduleInvitation.aggregate([
        {
            $match: {
                $or:[
                    {
                        toDo:{$elemMatch:{createdBy:userId,assignedTo:cUserId}}
                    },
                    {
                        toDo:{$elemMatch:{createdBy:cUserId,assignedTo:userId}}
                    }
                ]
            }
        },
        {$unwind: '$toDo'},
        {
            $match:{
                'toDo.dueDate': {$gte:new Date(dateMin)},
                $or:[
                    {
                        'toDo.createdBy':userId,
                        'toDo.assignedTo':cUserId
                    },
                    {
                        'toDo.createdBy':cUserId,
                        'toDo.assignedTo':userId
                    }
                ]
            }
        },
        {
            $group:{
                _id:null,
                toDo:{$push:"$toDo"}
            }
        }
    ]).exec(function(e,d){
        if(checkRequired(d) && d.length > 0){
            callback(d[0].toDo)
        }
        else callback([])
    })
};

MeetingManagementClass.prototype.cancelMeeting = function(userId,invitationId,selfCalendar,comment,clearIcs,callback){
    if(selfCalendar == true || selfCalendar == 'true'){
        var updateObjSelf = {};
        if(checkRequired(comment)){
            updateObjSelf = {
                $set:{
                    "toList.$.canceled":true,
                    "toList.$.canceledDate":new Date()
                },
                $push:{
                    comments:{
                        messageDate: new Date(),
                        message: comment,
                        userId: userId
                    }
                }
            };
        }
        else{
            updateObjSelf = {
                $set:{
                    "toList.$.canceled":true,
                    "toList.$.canceledDate":new Date()
                }
            };
        }

        if(clearIcs){
            updateObjSelf.$unset = {
                icsFile:''
            }
        }

        scheduleInvitation.findOneAndUpdate({invitationId:invitationId,"toList.receiverId":userId,deleted:{$ne:true}},updateObjSelf,function(error,result){
            if(error){
                logger.info('Error in  cancelMeeting():MeetingManagementClass 1 ',error);
            }else{
                events.findOneAndUpdate({invitationId:invitationId,"toList.receiverId":userId,deleted:{$ne:true}},updateObjSelf,function(err,res){
                   if(!err){
                       callback(result);
                   }else{
                       logger.info('calendar not cancelled');
                   }
                });

            }

        })
    }
    else{
        var updateObjNormal = {};
        if(checkRequired(comment)){
            updateObjNormal = {
                $set:{
                    "to.canceled":true,
                    "to.canceledDate":new Date()
                },
                $push:{
                    comments:{
                        messageDate: new Date(),
                        message: comment,
                        userId: userId
                    }
                }
            };
        }
        else{
            updateObjNormal = {
                $set:{
                    "to.canceled":true,
                    "to.canceledDate":new Date()
                }
            };
        }

        if(clearIcs){
            updateObjNormal.$unset = {
                icsFile:''
            }
        }

        scheduleInvitation.findOneAndUpdate({invitationId:invitationId,deleted:{$ne:true},"to.receiverId":userId},updateObjNormal,function(error,result){
            if(error){
                logger.info('Error in  cancelMeeting():MeetingManagementClass 2 ',error);
            }else{
                events.findOneAndUpdate({invitationId:invitationId,deleted:{$ne:true},"to.receiverId":userId},updateObjNormal,function(err,res){
                   if(!err){
                       callback(result);
                   }else{
                       logger.info('calendar not cancelled');
                   }
                });
            }

        })
    }
};

MeetingManagementClass.prototype.deleteMeeting = function(userId,invitationId,partialMeeting,callback){
    if(partialMeeting){
        var query = {invitationId:invitationId,deleted:{$ne:true}}
    }
    else{
        var query = {invitationId:invitationId,deleted:{$ne:true},senderId:userId}
    }

    scheduleInvitation.findOneAndUpdate(query,{$set:{deleted:true,deletedDate:new Date()},$unset:{icsFile:''}},function(error,result){ // Find and modify will return document.
        if(error){
            logger.info('Error in  deleteMeeting():MeetingManagementClass ',error);
            callback(false)
        }
        else{
            events.findOneAndUpdate(query,{$set:{deleted:true,deletedDate:new Date()},$unset:{icsFile:''}},function(err,res){
               if(!err){
                   callback(result);
               }else{
                   callback(false)
                   logger.info('calendar not cancelled');
               }
            });

        }
    })
};

MeetingManagementClass.prototype.updateInvitationSuggest = function(invitationId,invitationObj,comment,callback){

    var updateObj = {
        $set:invitationObj,
        // $unset:{googleEventId:'',icsFile:''} // Last modified by Naveen 04 sept.
        $unset:{icsFile:''}
    };
    if(checkRequired(comment.message)){
        updateObj.$push = {comments:comment}
    }

    scheduleInvitation.update({invitationId:invitationId,deleted:{$ne:true}},updateObj,function(error,result){
        var flag;
        if (error) {
            callback(error,false,{message:'Error in DB while updating invitation '+error});
        }
        if (result && result.ok) {
            flag = true;
            callback(error,flag,{message:'updateInvitation success'})
        }
        else{
          flag = false;
          callback(error,flag,{message:'updateInvitation failed'});
        }
    });
};

// Function to load new invitations
MeetingManagementClass.prototype.newInvitationsByDateTimezone = function(rId,dateStart,dateEnd,callback){

    scheduleInvitation.find({
        $or: [
            {
                "to.receiverId":rId,
                "to.canceled":{$ne: true},
                //readStatus: false, Last Modified 20 Feb 2016 by Naveen. This was not querying for meetings confirmed
                deleted: {$ne: true},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
            },
            {
                deleted: {$ne: true},
                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
            },
            {
                senderId: rId,
                deleted: {$ne: true},
                //readStatus: false, Last Modified 20 Feb 2016 by Naveen. This was not querying for meetings confirmed
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
            }
        ]
    }).populate('tasks',null,null,{ sort: { 'dueDate': 1 }}).exec(function(error,invitations){

        if(error){
            logger.info('Error in  newInvitationsByDateTimezone():MeetingManagementClass ',error);
        }
        callback(invitations);
    });
};

MeetingManagementClass.prototype.newInvitationsByDateWithProjection = function(rId,dateStart,dateEnd,projection,callback){

    scheduleInvitation.find({
        $or: [
            {
                "to.receiverId":rId,
                "to.canceled":{$ne: true},
                readStatus: false,
                deleted: {$ne: true},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
            },
            {
                deleted: {$ne: true},
                toList: {$elemMatch: {receiverId: rId, isAccepted: false, canceled: {$ne: true}}},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
            },
            {
                senderId: rId,
                deleted: {$ne: true},
                readStatus: false,
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
            }
        ]
    },projection).exec(function(error,invitations){
        if(error){
            logger.info('Error in  newInvitationsByDateWithProjection():MeetingManagementClass ',error);
        }
        callback(invitations);
    });
};

MeetingManagementClass.prototype.userMeetingsByDate = function(rId,dateStart,dateEnd,projection,findOutlookOrGoogle,callback){
    dateStart = new Date(dateStart);
    dateEnd = new Date(dateEnd);

    if(findOutlookOrGoogle == 'outlook'){
        myUserCollection.findOne({_id:common.castToObjectId(rId)},{emailId:1}).exec(function(err, user){
            if(err){
                logger.info('Error in  userMeetingsByDate(): ',err);
            }
            else {
                scheduleInvitation.find({
                        "isOfficeOutlookMeeting":true,
                        "LIUoutlookEmailId":user.emailId,
                        $or: [
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                deleted: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            },
                            {
                                deleted: {$ne: true},
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            },
                            {
                                senderId: rId,
                                deleted: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            }
                        ]
                    },
                    projection,
                    function(error,invitations){
                        if(error){
                            logger.info('Error in  userMeetingsByDate():MeetingManagementClass ',error);
                        }
                        callback(invitations);
                    });
            }
        })

        } else {

            scheduleInvitation.find({
                    isGoogleMeeting:true,
                    $or: [
                        {
                            "to.receiverId":rId,
                            "to.canceled":{$ne: true},
                            deleted: {$ne: true},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                        },
                        {
                            deleted: {$ne: true},
                            toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                        },
                        {
                            senderId: rId,
                            deleted: {$ne: true},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}},
                        }
                    ]
                },
                projection,
                function(error,invitations){
                    if(error){
                        logger.info('Error in  userMeetingsByDate():MeetingManagementClass ',error);
                    }
                    callback(invitations);
                });
    }
};

MeetingManagementClass.prototype.userMeetingsById = function(meetingId,projection,callback){

    scheduleInvitation.findById(common.castToObjectId(meetingId), projection, function(error, invitations) {
        callback(invitations);
    })
   
};

MeetingManagementClass.prototype.userNonConfirmedMeetingsByDate = function(rId,dateMin,dateMax,projection,userProfile,callback){
    dateMin = new Date(dateMin);
    dateMax = new Date(dateMax);

    if(userProfile.serviceLogin == 'outlook'){
        scheduleInvitation.find({
                "isOfficeOutlookMeeting":true,
                "LIUoutlookEmailId":userProfile.emailId,
                $or: [
                    {
                        "to.receiverId":rId,
                        "to.canceled":{$ne: true},
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        readStatus:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax},isAccepted:true}}
                    },
                    {
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:true}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax},isAccepted:true}}
                    },
                    {
                        senderId: rId,
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"isAccepted":false,"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    },
                    {
                        $or:[
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"isAccepted": true}}
                            },
                            {
                                toList: {$elemMatch: {receiverId: rId,"isAccepted": true, canceled: {$ne: true}}}
                            },
                            {
                                senderId: rId,
                                "scheduleTimeSlots": {"$elemMatch": {"isAccepted": true}}
                            },
                            {
                                "suggestedBy.userId":rId
                            }
                        ],
                        deleted: {$ne: true},
                        suggested:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    }
                ]
            },
            {recurrenceId:1,invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1},
            function(error,invitations){

                if(error){
                    logger.info('Error in  userNewMeetingsByDate():MeetingManagementClass ',error);
                }
                callback(invitations);
            });

        } else {

        scheduleInvitation.find({
                "isGoogleMeeting":true,
                $or: [
                    {
                        "to.receiverId":rId,
                        "to.canceled":{$ne: true},
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        readStatus:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax},isAccepted:false}}
                    },
                    {
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:false}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax},isAccepted:false}}
                    },
                    {
                        senderId: rId,
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"isAccepted":false,"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    },
                    {
                        $or:[
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"isAccepted": false}}
                            },
                            {
                                toList: {$elemMatch: {receiverId: rId,"isAccepted": false, canceled: {$ne: true}}}
                            },
                            {
                                senderId: rId,
                                "scheduleTimeSlots": {"$elemMatch": {"isAccepted": false}}
                            },
                            {
                                "suggestedBy.userId":rId
                            }
                        ],
                        deleted: {$ne: true},
                        suggested:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    }
                ]
            },
            {recurrenceId:1,invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1},
            function(error,invitations){

                if(error){
                    logger.info('Error in  userNewMeetingsByDate():MeetingManagementClass ',error);
                }
                callback(invitations);
            });
    }
};

MeetingManagementClass.prototype.allMeetingsByDate = function(rId,dateMin,dateMax,projection,userProfile,callback){
    dateMin = new Date(dateMin);
    dateMax = new Date(dateMax);

    if(userProfile.serviceLogin == 'outlook'){
        scheduleInvitation.find({
                "isOfficeOutlookMeeting":true,
                "LIUoutlookEmailId":userProfile.emailId,
                $or: [
                    {
                        "to.receiverId":rId,
                        "to.canceled":{$ne: true},
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        readStatus:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    },
                    {
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    },
                    {
                        senderId: rId,
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    },
                    {
                        $or:[
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true}
                            },
                            {
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}}
                            },
                            {
                                senderId: rId
                            },
                            {
                                "suggestedBy.userId":rId
                            }
                        ],
                        deleted: {$ne: true},
                        suggested:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    }
                ]
            },
            {recurrenceId:1,invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1},
            function(error,invitations){

                if(error){
                    logger.info('Error in  userNewMeetingsByDate():MeetingManagementClass ',error);
                }
                callback(invitations);
            });

        } else {

        scheduleInvitation.find({
                "isGoogleMeeting":true,
                $or: [
                    {
                        "to.receiverId":rId,
                        "to.canceled":{$ne: true},
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    },
                    {
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    },
                    {
                        senderId: rId,
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    },
                    {
                        $or:[
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true}
                            },
                            {
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}}
                            },
                            {
                                senderId: rId
                            },
                            {
                                "suggestedBy.userId":rId
                            }
                        ],
                        deleted: {$ne: true},
                        suggested:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    }
                ]
            },
            {recurrenceId:1,invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1},
            function(error,invitations){

                if(error){
                    logger.info('Error in  userNewMeetingsByDate():MeetingManagementClass ',error);
                }
                callback(invitations);
            });
    }
};

MeetingManagementClass.prototype.userActionItemSlotMeetingsByDate = function(rId,dateStart,dateEnd,projection,findOutlookOrGoogle,actionItemSlotType,callback){
    dateStart = new Date(dateStart);
    dateEnd = new Date(dateEnd);

    if(findOutlookOrGoogle == 'outlook'){
        myUserCollection.findOne({_id:common.castToObjectId(rId)},{emailId:1}).exec(function(err, user){
            if(err){
                logger.info('Error in  userActionItemSlotMeetingsByDate(): ',err);
                callback([]);
            }
            else {
                scheduleInvitation.find({
                        "isOfficeOutlookMeeting":true,
                        "LIUoutlookEmailId":user.emailId,
                        actionItemSlot:true,
                        actionItemSlotType:{$in:actionItemSlotType},
                        $or: [
                            {
                                senderId: rId,
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            }
                        ]
                    },
                    projection,
                    function(error,invitations){
                        if(error){
                            logger.info('Error in  userActionItemSlotMeetingsByDate():MeetingManagementClass ',error);
                        }
                        callback(invitations);
                    });
            }
        })

    } else {

        scheduleInvitation.find({
                isGoogleMeeting:true,
                actionItemSlot:true,
                actionItemSlotType:{$in:actionItemSlotType},
                $or: [
                    {
                        senderId: rId,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                    }
                ]
            },
            projection,
            function(error,invitations){
                if(error){
                    logger.info('Error in  userActionItemSlotMeetingsByDate():MeetingManagementClass ',error);
                }
                callback(invitations);
            });
    }
};

MeetingManagementClass.prototype.userMeetingsByDate_map_sender_receiver = function(rId,sId,dateStart,dateEnd,findOutlookOrGoogle,callback){
    dateStart = new Date(dateStart);
    dateEnd = new Date(dateEnd);

    scheduleInvitation.find({
            $or:[
                {
                    "to.receiverId":rId,
                    "to.canceled":{$ne: true},
                    deleted: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                },
                {
                    deleted: {$ne: true},
                    toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                },
                {
                    senderId: rId,
                    deleted: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                }
            ]
        },
        {scheduleTimeSlots:1,invitationId:1},
        function(error,invitationsR){
            if(error){
                logger.info('Error in  userMeetingsByDate_map_sender_receiver():MeetingManagementClass ',error);
            }
            scheduleInvitation.find({
                $or:[
                    {
                        "to.receiverId":sId,
                        "to.canceled":{$ne: true},
                        deleted: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                    },
                    {
                        deleted: {$ne: true},
                        toList: {$elemMatch: {receiverId: sId, canceled: {$ne: true}}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                    },
                    {
                        senderId: sId,
                        deleted: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                    }
                ]
            },{scheduleTimeSlots:1,invitationId:1},function(error,invitationsS) {
                if (error) {
                    logger.info('Error in  userMeetingsByDate_map_sender_receiver():MeetingManagementClass 2', error);
                }
                callback(invitationsR || [], invitationsS || []);
            });
        });
};

MeetingManagementClass.prototype.userNewMeetingsByDate = function(rId,dateStart,findOutlookOrGoogle,callback){

    dateStart = new Date(dateStart);

    if(findOutlookOrGoogle == 'outlook'){
        myUserCollection.findOne({_id:common.castToObjectId(rId)},{emailId:1}).exec(function(err, user) {
            if (err) {
                logger.info('Error in  userMeetingsByDate(): ', err);
            }
            else {
                scheduleInvitation.find({
                        "isOfficeOutlookMeeting":true,
                        "LIUoutlookEmailId":user.emailId,
                        $or: [
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                deleted: {$ne: true},
                                suggested: {$ne: true},
                                readStatus:false,
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart}}}
                            },
                            {
                                deleted: {$ne: true},
                                suggested: {$ne: true},
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:{$ne: true}}},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart}}}
                            },
                            {
                                $or:[
                                    {
                                        "to.receiverId":rId,
                                        "to.canceled":{$ne: true},
                                        "scheduleTimeSlots": {"$elemMatch": {"isAccepted": {$ne:true}}},
                                        "suggestedBy.userId":{$ne:rId}
                                    },
                                    {
                                        toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:{$ne:true}}},
                                        "suggestedBy.userId":{$ne:rId}
                                    },
                                    {
                                        senderId: rId,
                                        "scheduleTimeSlots": {"$elemMatch": {"isAccepted": {$ne:true}}},
                                        "suggestedBy.userId":{$ne:rId}
                                    }
                                ],
                                deleted: {$ne: true},
                                suggested:true,
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart}}}
                            }
                        ]
                    },
                    {recurrenceId:1,invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,selfCalendar:1,toList:1,suggested:1,suggestedBy:1},
                    function(error,invitations){
                        if(error){
                            logger.info('Error in  userNewMeetingsByDate():MeetingManagementClass ',error);
                        }
                        callback(invitations);
                    });
            }
        });

    } else{

        scheduleInvitation.find({
                "isGoogleMeeting":true,
                $or: [
                    {
                        "to.receiverId":rId,
                        "to.canceled":{$ne: true},
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        readStatus:false,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart}}}
                    },
                    {
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:{$ne: true}}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart}}}
                    },
                    {
                        $or:[
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"isAccepted": {$ne:true}}},
                                "suggestedBy.userId":{$ne:rId}
                            },
                            {
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:{$ne:true}}},
                                "suggestedBy.userId":{$ne:rId}
                            },
                            {
                                senderId: rId,
                                "scheduleTimeSlots": {"$elemMatch": {"isAccepted": {$ne:true}}},
                                "suggestedBy.userId":{$ne:rId}
                            }
                        ],
                        deleted: {$ne: true},
                        suggested:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart}}}
                    }
                ]
            },
            {invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,selfCalendar:1,toList:1,suggested:1,suggestedBy:1},
            function(error,invitations){
                
                if(error){
                    logger.info('Error in  userNewMeetingsByDate():MeetingManagementClass ',error);
                }
                callback(invitations);
            });
    }

};

MeetingManagementClass.prototype.todayMeetings = function(rId,dateMin,dateMax,findOutlookOrGoogle,callback){
    dateMin = new Date(dateMin);
    dateMax = new Date(dateMax);

    if(findOutlookOrGoogle == 'outlook'){
        myUserCollection.findOne({_id:common.castToObjectId(rId)},{emailId:1}).exec(function(err, user) {
            if (err) {
                logger.info('Error in  userMeetingsByDate(): ', err);
            }
            else {
                scheduleInvitation.find({
                        "isOfficeOutlookMeeting":true,
                        "LIUoutlookEmailId":user.emailId,
                        $or: [
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                deleted: {$ne: true},
                                suggested: {$ne: true},
                                readStatus:true,
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax},isAccepted:true}}
                            },
                            {
                                deleted: {$ne: true},
                                suggested: {$ne: true},
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:true}},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax},isAccepted:true}}
                            },
                            {
                                senderId: rId,
                                deleted: {$ne: true},
                                suggested: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                            },
                            {
                                $or:[
                                    {
                                        "to.receiverId":rId,
                                        "to.canceled":{$ne: true},
                                        "scheduleTimeSlots": {"$elemMatch": {"isAccepted": true}}
                                    },
                                    {
                                        toList: {$elemMatch: {receiverId: rId,"isAccepted": true, canceled: {$ne: true}}}
                                    },
                                    {
                                        senderId: rId,
                                        "scheduleTimeSlots": {"$elemMatch": {"isAccepted": true}}
                                    },
                                    {
                                        "suggestedBy.userId":rId
                                    }
                                ],
                                deleted: {$ne: true},
                                suggested:true,
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                            }
                        ]
                    },
                    {recurrenceId:1,invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1},
                    function(error,invitations){

                        if(error){
                            logger.info('Error in  userNewMeetingsByDate():MeetingManagementClass ',error);
                        }
                        callback(invitations);
                    });
            }
        });

    } else {

        scheduleInvitation.find({
                "isGoogleMeeting":true,
                $or: [
                    {
                        "to.receiverId":rId,
                        "to.canceled":{$ne: true},
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        readStatus:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax},isAccepted:true}}
                    },
                    {
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:true}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax},isAccepted:true}}
                    },
                    {
                        senderId: rId,
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    },
                    {
                        $or:[
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"isAccepted": true}}
                            },
                            {
                                toList: {$elemMatch: {receiverId: rId,"isAccepted": true, canceled: {$ne: true}}}
                            },
                            {
                                senderId: rId,
                                "scheduleTimeSlots": {"$elemMatch": {"isAccepted": true}}
                            },
                            {
                                "suggestedBy.userId":rId
                            }
                        ],
                        deleted: {$ne: true},
                        suggested:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},"end.date": {'$lte': dateMax}}}
                    }
                ]
            },
            {invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1},
            function(error,invitations){

                if(error){
                    logger.info('Error in  userNewMeetingsByDate():MeetingManagementClass ',error);
                }
                callback(invitations);
            });
    }
};

MeetingManagementClass.prototype.upcomingMeetings = function(rId,dateMin,user,callback){
    dateMin = new Date(dateMin);

    if(user.serviceLogin == 'outlook') {

        scheduleInvitation.find({
                "isOfficeOutlookMeeting": true,
                "LIUoutlookEmailId": user.emailId,
                $or: [
                    {
                        "to.receiverId":rId,
                        "to.canceled":{$ne: true},
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        readStatus:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},isAccepted:false}}
                    },
                    {
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:false}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},isAccepted:false}}
                    },
                    {
                        senderId: rId,
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin}}}
                    },
                    {
                        $or:[
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true}
                            },
                            {
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}}
                            },
                            {
                                senderId: rId
                            }
                        ],
                        deleted: {$ne: true},
                        suggested:true,
                        "suggestedBy.userId":rId,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin}}}
                    }
                ]
            },
            {invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1},
            function(error,invitations){
                if(error){
                    logger.info('Error in  upcomingMeetings():MeetingManagementClass ',error);
                }
                callback(invitations);
            });
    } else {
        scheduleInvitation.find({
                "isGoogleMeeting":true,
                $or: [
                    {
                        "to.receiverId":rId,
                        "to.canceled":{$ne: true},
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        readStatus:true,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},isAccepted:false}}
                    },
                    {
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:false}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},isAccepted:false}}
                    },
                    {
                        senderId: rId,
                        deleted: {$ne: true},
                        suggested: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin}}}
                    },
                    {
                        $or:[
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true}
                            },
                            {
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}}
                            },
                            {
                                senderId: rId
                            }
                        ],
                        deleted: {$ne: true},
                        suggested:true,
                        "suggestedBy.userId":rId,
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin}}}
                    }
                ]
            },
            {invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1},
            function(error,invitations){
                if(error){
                    logger.info('Error in  upcomingMeetings():MeetingManagementClass ',error);
                }
                callback(invitations);
            });
    }
};

MeetingManagementClass.prototype.getNextMeetingWithContact = function (userId,rId,dateMin,callback){
    dateMin = new Date(dateMin)
    scheduleInvitation.find({
            $or: [
                {
                    "to.receiverId":rId,
                    "to.canceled":{$ne: true},
                    deleted: {$ne: true},
                    suggested: {$ne: true},
                    readStatus:true,
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},isAccepted:true}},
                    "participants":{"$elemMatch":{"emailId":userId}}
                },
                {
                    deleted: {$ne: true},
                    suggested: {$ne: true},
                    toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true},isAccepted:true}},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},isAccepted:true}},
                    "participants":{"$elemMatch":{"emailId":userId}}
                },
                {
                    senderId: rId,
                    deleted: {$ne: true},
                    suggested: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin}}},
                    "participants":{"$elemMatch":{"emailId":userId}}
                },
                {
                    $or:[
                        {
                            "to.receiverId":rId,
                            "to.canceled":{$ne: true}
                        },
                        {
                            toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}}
                        },
                        {
                            senderId: rId
                        }
                    ],
                    deleted: {$ne: true},
                    suggested:true,
                    "suggestedBy.userId":rId,
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin}}},
                    "participants":{"$elemMatch":{"emailId":userId}}
                }
            ]
        },
        {invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1},
        function(error,invitations){

            if(error){
                logger.info('Error in  upcomingMeetings():MeetingManagementClass ',error);
            }
            callback(invitations);
        });
}

MeetingManagementClass.prototype.getUnpreparedMeetingsCount = function(userId,callback){

    scheduleInvitation.find({
            $or: [
                {
                    "to.receiverId":userId,
                    "to.canceled":{$ne: true},
                    "to.isPrepared":{$ne: true},
                    deleted: {$ne: true},
                    readStatus:false
                },
                {
                    deleted: {$ne: true},
                    toList: {$elemMatch: {receiverId: userId, canceled: {$ne: true},isAccepted:false,isPrepared:{$ne:true}}}
                },
                {
                    senderId:userId,
                    isSenderPrepared:{$ne:true}
                }
            ]
        },
        {_id:1},
        function(error,invitations){

            if(error){
                logger.info('Error in  getUnpreparedMeetingsCount():MeetingManagementClass ',error);
            }
            if(checkRequired(invitations) && invitations.length > 0){
                callback(invitations.length)
            }
            else callback(0);
        });
};

MeetingManagementClass.prototype.unpreparedMeetingsCount = function(rId,dateStart,callback){
    dateStart = new Date(dateStart);

    scheduleInvitation.aggregate([
        {
            $match:{
                $or: [
                    {
                        "to.receiverId":rId,
                        "to.canceled":{$ne: true},
                        "to.isPrepared":{$ne: true},
                        deleted: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart}}}
                    },
                    {
                        deleted: {$ne: true},
                        toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}, isPrepared: {$ne: true}}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart}}}
                    },
                    {
                        senderId:rId,
                        isSenderPrepared:{$ne:true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart}}}
                    }
                ]
            }
        },
        {
            $group:{
                _id:null,
                unpreparedMeetingsCount:{$sum:1}
            }
        }
    ]).exec(function(error,result){
         if(checkRequired(result) && result.length > 0 && result[0] && result[0].unpreparedMeetingsCount){
             callback(result[0].unpreparedMeetingsCount)
         }else callback(0)
    });
};

MeetingManagementClass.prototype.updateMeetingPrepared = function(userId,invitationId,isSender,isSelfCalendar,callback){
    if(isSender){
        scheduleInvitation.update({invitationId:invitationId,"senderId":userId},{$set:{"isSenderPrepared":true}},function(error,response){
            if(error){
                logger.info('Error in updateMeetingPrepared():MeetingManagementClass 1 ',error)
            }
            callback(response.ok)
        })
    }
    else if(isSelfCalendar){
        scheduleInvitation.update({invitationId:invitationId,"toList.receiverId":userId},{$set:{"toList.$.isPrepared":true}},function(error,response){
            if(error){
                logger.info('Error in updateMeetingPrepared():MeetingManagementClass 2 ',error)
            }
            callback(response.ok)
        })
    }
    else{
        scheduleInvitation.update({invitationId:invitationId,"to.receiverId":userId},{$set:{"to.isPrepared":true}},function(error,response){
            if(error){
                logger.info('Error in updateMeetingPrepared():MeetingManagementClass 3 ',error)
            }
            callback(response.ok)
        })
    }
};

MeetingManagementClass.prototype.userCurrentDayTravelingTo = function(rId,dateMin,dateMax,location,callback){
    dateMin = new Date(dateMin);
    dateMax = new Date(dateMax);

    var notMatchSkype = /skype/i;
    var notMatchCall = /call/i;
    var notMatchPhone = /phone/i;

    scheduleInvitation.find({
            $or: [
                {
                    "to.receiverId":rId,
                    "to.canceled":{$ne: true},
                    deleted: {
                        $ne: true
                    },
                    //readStatus: true,
                    "scheduleTimeSlots": {
                        "$elemMatch": {
                            "start.date": {
                                '$gte': dateMin,
                                '$lte': dateMax
                            },
                            //isAccepted: true,
                            locationType: 'In-Person',
                            location: {
                                $nin: [location,null,'',notMatchSkype,notMatchCall,notMatchPhone]
                            }
                        }
                    }
                },
                {
                    deleted: {
                        $ne: true
                    },
                    toList: {
                        $elemMatch: {
                            receiverId: rId,
                            canceled: {
                                $ne: true
                            }
                            //isAccepted: true
                        }
                    },
                    "scheduleTimeSlots": {
                        "$elemMatch": {
                            "start.date": {
                                '$gte': dateMin,
                                '$lte': dateMax
                            },
                            //isAccepted: true,
                            locationType: 'In-Person',
                            location: {
                                $nin: [location,null,'',notMatchSkype,notMatchCall,notMatchPhone]
                            }
                        }
                    }
                },
                {
                    senderId: rId,
                    deleted: {
                        $ne: true
                    },
                    //readStatus: true,
                    "scheduleTimeSlots": {
                        "$elemMatch": {
                            "start.date": {
                                '$gte': dateMin,
                                '$lte': dateMax
                            },
                            //isAccepted: true,
                            locationType: 'In-Person',
                            location: {
                                $nin: [location,null,'',notMatchSkype,notMatchCall,notMatchPhone]
                            }
                        }
                    }
                }
            ]
        }).
        sort({ "scheduleTimeSlots.start.date": 1 }).
        select({ scheduleTimeSlots:1, to:1, toList:1, senderEmailId:1 }).
        limit(1).
        exec(function(error,invitations){
            if(error){
                logger.info('Error in  userCurrentDayTravelingTo():MeetingManagementClass ',error);
            }
            if(invitations && invitations.length > 0){
                callback(invitations[0]);
            }
            else callback(null)
        });
};

MeetingManagementClass.prototype.findMeetingRoomSlotInfo = function(idList,start,end,callback){
    for(var i=0; i<idList.length; i++){
        idList[i] = idList[i].toString()
    }

    scheduleInvitation.aggregate([
        {
            $match:  {
                deleted: {$ne: true},
                "toList.receiverId": {$in:idList},
                "scheduleTimeSlots": {"$elemMatch":
                {$or:[{"start.date": {'$gte': new Date(start), '$lte':new Date(end)}},{"end.date": {'$gte': new Date(start), '$lte':new Date(end)}}]}
                }
            }
        },
        {
            $unwind:"$toList"
        },
        {
            $match:{"toList.receiverId":{$in:idList}}
        },
        {
            $group:{
                _id:null,
                rooms:{$addToSet:"$toList.receiverId"}
            }
        }
    ]).exec(function(error,result){
        if(checkRequired(result) && result.length > 0 && result[0]){
            callback(result)
        }else callback([])
    });
};

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

MeetingManagementClass.prototype.updateOrCreateMeetingBulk = function(meetings,callback){
    if(meetings.length > 0){
        for(var i=0; i<meetings.length; i++){
            updateCreateMeeting(meetings[i]);
        }
        callback()
    }
};

MeetingManagementClass.prototype.updateCreateMeeting = function(userId,meeting,meetings,lastLoginDate,callback){
    updateCreateMeeting(userId,meeting,meetings,lastLoginDate,callback)
};

MeetingManagementClass.prototype.userNextWeekTravelingTo = function(contactsPersonIdList,dateMin,callback){

    dateMin = new Date(dateMin)

    scheduleInvitation.find({
            $or: [
                {
                    "to.receiverId":{$in:contactsPersonIdList},
                    "to.canceled":{$ne: true},
                    deleted: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},locationType: 'In-Person',location: {$ne:null}}}
                },
                {
                    deleted: {$ne: true},
                    suggested: {$ne: true},
                    toList: {$elemMatch: {receiverId: {$in:contactsPersonIdList}, canceled: {$ne: true}}},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},locationType: 'In-Person',location: {$ne:null}}}
                },
                {
                    senderId: {$in:contactsPersonIdList},
                    deleted: {$ne: true},
                    suggested: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},locationType: 'In-Person',location: {$ne:null}}}
                },
                {
                    $or:[
                        {
                            "to.receiverId":{$in:contactsPersonIdList},
                            "to.canceled":{$ne: true}
                        },
                        {
                            toList: {$elemMatch: {receiverId: {$in:contactsPersonIdList}, canceled: {$ne: true}}}
                        },
                        {
                            senderId: {$in:contactsPersonIdList}
                        }
                    ],
                    deleted: {$ne: true},
                    suggested:true,
                    "suggestedBy.userId":{$in:contactsPersonIdList},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin},locationType: 'In-Person',location: {$ne:null}}}
                }
            ]
        },
        {"scheduleTimeSlots":1,senderId:1,"toList.receiverId":1,"participants":1,"invitationId":1},
        function(error,invitations){
            if(error){
                logger.info('Error in  upcomingMeetings():MeetingManagementClass ',error);
                callback([])
            } else {
                callback(invitations);
            }
        });
};

MeetingManagementClass.prototype.userNexTwoWeeksTravelingTo = function(contactsPersonIdList,dateMin,dateMax,outlookOrGoogle,callback){

    dateMin = new Date(dateMin)
    dateMax = new Date(dateMax)

    var match = {
        isOfficeOutlookMeeting:true,
        $or: [
            {
                "to.receiverId":{$in:contactsPersonIdList},
                "to.canceled":{$ne: true},
                deleted: {$ne: true},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin,'$lt': dateMax},locationType: 'In-Person',location: {$ne:null}}}
            },
            {
                deleted: {$ne: true},
                suggested: {$ne: true},
                toList: {$elemMatch: {receiverId: {$in:contactsPersonIdList}, canceled: {$ne: true}}},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin,'$lt': dateMax},locationType: 'In-Person',location: {$ne:null}}}
            },
            {
                senderId: {$in:contactsPersonIdList},
                deleted: {$ne: true},
                suggested: {$ne: true},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin,'$lt': dateMax},locationType: 'In-Person',location: {$ne:null}}}
            },
            {
                $or:[
                    {
                        "to.receiverId":{$in:contactsPersonIdList},
                        "to.canceled":{$ne: true}
                    },
                    {
                        toList: {$elemMatch: {receiverId: {$in:contactsPersonIdList}, canceled: {$ne: true}}}
                    },
                    {
                        senderId: {$in:contactsPersonIdList}
                    }
                ],
                deleted: {$ne: true},
                suggested:true,
                "suggestedBy.userId":{$in:contactsPersonIdList},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin,'$lt': dateMax},locationType: 'In-Person',location: {$ne:null}}}
            }
        ]
    }

    // "isOfficeOutlookMeeting":true,
    if(outlookOrGoogle === 'google'){

        match = {
            isGoogleMeeting:true,
                $or: [
            {
                "to.receiverId":{$in:contactsPersonIdList},
                "to.canceled":{$ne: true},
                deleted: {$ne: true},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin,'$lt': dateMax},locationType: 'In-Person',location: {$ne:null}}}
            },
            {
                deleted: {$ne: true},
                suggested: {$ne: true},
                toList: {$elemMatch: {receiverId: {$in:contactsPersonIdList}, canceled: {$ne: true}}},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin,'$lt': dateMax},locationType: 'In-Person',location: {$ne:null}}}
            },
            {
                senderId: {$in:contactsPersonIdList},
                deleted: {$ne: true},
                suggested: {$ne: true},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin,'$lt': dateMax},locationType: 'In-Person',location: {$ne:null}}}
            },
            {
                $or:[
                    {
                        "to.receiverId":{$in:contactsPersonIdList},
                        "to.canceled":{$ne: true}
                    },
                    {
                        toList: {$elemMatch: {receiverId: {$in:contactsPersonIdList}, canceled: {$ne: true}}}
                    },
                    {
                        senderId: {$in:contactsPersonIdList}
                    }
                ],
                deleted: {$ne: true},
                suggested:true,
                "suggestedBy.userId":{$in:contactsPersonIdList},
                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateMin,'$lt': dateMax},locationType: 'In-Person',location: {$ne:null}}}
            }
        ]
        }
    }

    scheduleInvitation.find(match,
        {"scheduleTimeSlots":1,senderId:1,"toList.receiverId":1,"participants":1,"invitationId":1},
        function(error,invitations){
            if(error){
                logger.info('Error in  userNexTwoWeeksTravelingTo():MeetingManagementClass ',error);
                callback([])
            } else {
                callback(invitations);
            }
        });
};

MeetingManagementClass.prototype.yesterdayMeetings = function (userId,callback) {

    scheduleInvitation.find({
            $or: [
                {
                    "to.receiverId":userId,
                    "to.canceled":{$ne: true},
                    "to.isPrepared":{$ne: true},
                    deleted: {$ne: true},
                    readStatus:false
                },
                {
                    deleted: {$ne: true},
                    toList: {$elemMatch: {receiverId: userId, canceled: {$ne: true},isAccepted:false,isPrepared:{$ne:true}}}
                },
                {
                    senderId:userId,
                    isSenderPrepared:{$ne:true}
                }
            ]
        },
        function(error,invitations){

            if(error){
                logger.info('Error in  getUnpreparedMeetingsCount():MeetingManagementClass ',error);
                callback([])
            }
            else if(checkRequired(invitations) && invitations.length > 0){
                callback(invitations)
            }
            else callback([]);
        });

};

MeetingManagementClass.prototype.updateScheduleTimeSlotsBulk = function (meetings,callback) {
    var len = meetings.length

    var forMeetingInteractionUpdate = [];

    //Update if there are no duplicates
    if(len>0){

        var bulkMeetingUpdate = scheduleInvitation.collection.initializeUnorderedBulkOp();

        for (var i = 0; i<len; i++) {
            var updateObj = {
                "scheduleTimeSlots": meetings[i].scheduleTimeSlots,
                "participants": meetings[i].participants,
                "location": meetings[i].location?meetings[i].location:null,
                "title": meetings[i].title?meetings[i].title:null,
                "locationType": meetings[i].locationType?meetings[i].locationType:null
            }

            forMeetingInteractionUpdate.push({
                invitationId: meetings[i].invitationId,
                interactionDate: meetings[i].scheduleTimeSlots[0].start.date,
                title: meetings[i].title?meetings[i].title:null,
                participants:meetings[i].participants
            })

            bulkMeetingUpdate.find({invitationId: meetings[i].invitationId}).update({ $set: updateObj });
        }

        bulkMeetingUpdate.execute(function(err, data){
            
            if(err){
                logger.info('Error in updateScheduleTimeSlotsBulk():MeetingManagementClass ', err);
                if(callback){
                    callback([])
                }
            }
            else if(callback) {
                callback(forMeetingInteractionUpdate) // Callback with non duplicates to be written to interactions collection
                logger.info('Successfully updated meetings collection for - ');
            }
        })//End Bulk meeting Insert
    } else if(callback) {
        callback(forMeetingInteractionUpdate)
    }
}

MeetingManagementClass.prototype.bulkMeetingsUpdate = function (calendarEmail,meetings,callback) {

    var currentRefIdArray = _.pluck(meetings,"invitationId");

    scheduleInvitation.aggregate([
        {
            $match:{invitationId:{$in:currentRefIdArray}}
        },
        {
            $group: {
                _id:null,
                invitationId:{$addToSet:'$invitationId'}
            }
        }
    ]).exec(function (err,existingMeetings) {

        var addMeetings = meetings.filter(function(val) {
            if(existingMeetings[0]){
                return existingMeetings[0].invitationId.indexOf(val.invitationId) == -1;
            }else{
                return existingMeetings
            }
        });

        var len = addMeetings.length

        //Update if there are no duplicates
        if(len>0){

            var bulkMeetingInsert = scheduleInvitation.collection.initializeUnorderedBulkOp();

            for(var i=0;i<len;i++){
                bulkMeetingInsert.insert(addMeetings[i]);
            }

            bulkMeetingInsert.execute(function(err, data){
                if(err){
                    logger.info('Error in bulkMeetingInsert()--bulkMeetingsUpdate():MeetingManagementClass ', err);
                    callback([])
                }
                else {
                    callback(addMeetings) // Callback with non duplicates to be written to interactions collection
                    logger.info('Successfully updated meetings collection for - ', calendarEmail);
                }
            })//End Bulk meeting Insert
        } else {
            callback([])
        }
    })//End Update prototype
}

MeetingManagementClass.prototype.recurringBulkMeetingsUpdate = function (calendarEmail,recurringMeetingsArr,callback) {

    var currentRefIdArray = _.pluck(recurringMeetingsArr,'recurrenceId')

    scheduleInvitation.aggregate([
        {
            $match:{recurrenceId:{$in:currentRefIdArray}}
        },
        {
            $group: {
                _id:null,
                recurrenceId:{$addToSet:'$recurrenceId'}
            }
        }
    ]).exec(function (err,existingMeetings) {

        var addMeetings = recurringMeetingsArr.filter(function(val) {
            if(existingMeetings[0]){
                return existingMeetings[0].recurrenceId.indexOf(val.recurrenceId) == -1;
            }else{
                return existingMeetings
            }
        });

        var len = addMeetings.length;

        if(len>0){

            var bulkMeetingInsert = scheduleInvitation.collection.initializeUnorderedBulkOp();
            addMeetings.forEach(function(a){
                a._id = mongoose.Types.ObjectId();
            })
            for(var i=0;i<len;i++){
                bulkMeetingInsert.insert(addMeetings[i]);
            }

            bulkMeetingInsert.execute(function(err, data){
                if(err){
                    logger.info('Error in recurringBulkMeetingsUpdate()--bulkMeetingsUpdate():MeetingManagementClass ', err);
                    callback([])
                }
                else {
                    callback(addMeetings) // Callback to be written to interactions collection
                    logger.info('Successfully updated recurring meetings collection for - ', calendarEmail);
                }
            });//End Bulk meeting Insert
        } else {
            callback([])
        }

    });

};

MeetingManagementClass.prototype.updateInvitationId = function (outlookMeetingId,relatasMeetingId,callback) {

    var updateObj = {
        invitationId:outlookMeetingId,
        officeEventId:outlookMeetingId,
        isOfficeOutlookMeeting:true,
        isGoogleMeeting:false,
        partialMeeting:false
    };

    scheduleInvitation.update({invitationId: relatasMeetingId},{ $set: updateObj },function (err,result) {

        if(callback){
            if(!err && result){
                callback(true)
            } else{
                callback(false)
            }
        }
    });
}

MeetingManagementClass.prototype.updateUploadedLocation = function(invitationId, upLoadedLocation, callback) {
    scheduleInvitation.findOneAndUpdate({invitationId:invitationId}, {$set:{"upLoadedMeetingLocation": upLoadedLocation}}, {upsert:true}, function(err, result) {
        callback(err, result);
    })
}

MeetingManagementClass.prototype.updateGoogleEventId = function (googleEventId,relatasMeetingId,callback) {

    var updateObj = {
        googleEventId:googleEventId,
        partialMeeting:false,
        "isOfficeOutlookMeeting" : false,
        "isGoogleMeeting" : true
    };

    scheduleInvitation.update({invitationId: relatasMeetingId},{ $set: updateObj },{upsert: true},function (err,result) {
        
        if(callback){
            if(!err && result){
                callback(true)
            } else{
                callback(false)
            }
        }
    });
}

MeetingManagementClass.prototype.getParticipantsAtLocation = function (contactsPersonIdList,locationArray,participants,dateMin,dateMax,callback) {

    var queryFinal = { "$or": locationArray.map(function(el) {

        el["location"] = {'$regex' : '.*' + el.loc + '.*',"$options": 'i'};
        // el["personEmailId"] = {$nin:participants};
        el["locationType"] = 'In-Person';
        el["start.date"] = {'$gte': new Date(dateMin), '$lt': new Date(dateMax)};
        delete  el.loc;
        return el;
    })};

    var participantsMatch = {$ne:participants}

    if (participants instanceof Array) {
        participantsMatch = {$nin:participants}
    }

    scheduleInvitation.find({
            $or: [
                {
                    "to.receiverId":contactsPersonIdList,
                    "to.canceled":{$ne: true},
                    deleted: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": queryFinal},
                    "participants.emailId":participantsMatch
                },
                {
                    deleted: {$ne: true},
                    suggested: {$ne: true},
                    toList: {$elemMatch: {receiverId: contactsPersonIdList, canceled: {$ne: true}}},
                    "scheduleTimeSlots": {"$elemMatch": queryFinal},
                    "participants.emailId":participantsMatch
                },
                {
                    senderId: contactsPersonIdList,
                    deleted: {$ne: true},
                    suggested: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": queryFinal},
                    "participants.emailId":participantsMatch
                },
                {
                    $or:[
                        {
                            "to.receiverId":contactsPersonIdList,
                            "to.canceled":{$ne: true}
                        },
                        {
                            toList: {$elemMatch: {receiverId: contactsPersonIdList, canceled: {$ne: true}}}
                        },
                        {
                            senderId: contactsPersonIdList
                        }
                    ],
                    deleted: {$ne: true},
                    suggested:true,
                    "suggestedBy.userId":contactsPersonIdList,
                    "scheduleTimeSlots": {"$elemMatch": queryFinal},
                    "participants.emailId":participantsMatch
                }
            ]
        },
        {"participants":1,"scheduleTimeSlots":1},
        function(error,invitations){

            if(error){
                logger.info('Error in  getParticipantsAtLocation():MeetingManagementClass ',error);
                callback(error,[])
            } else {
                callback(error,invitations);
            }
        });

}

MeetingManagementClass.prototype.getParticipantsAtLocationTTL = function (contactsPersonIdList,locationArray,participants,dateMin,dateMax,callback) {

    var queryFinal = { "$or": locationArray.map(function(el) {
        el["location"] = {'$regex' : '.*' + el.loc + '.*',"$options": 'i'};
        // el["personEmailId"] = {$nin:participants};
        el["locationType"] = 'In-Person';
        delete  el.loc;
        return el;
    })};

    scheduleInvitation.find({
            $or: [
                {
                    "to.receiverId":contactsPersonIdList,
                    "to.canceled":{$ne: true},
                    deleted: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": queryFinal},
                    "participants":{"$elemMatch": {"emailId":{$ne:participants}}}
                },
                {
                    deleted: {$ne: true},
                    suggested: {$ne: true},
                    toList: {$elemMatch: {receiverId: contactsPersonIdList, canceled: {$ne: true}}},
                    "scheduleTimeSlots": {"$elemMatch": queryFinal},
                    "participants":{"$elemMatch": {"emailId":{$ne:participants}}}
                },
                {
                    senderId: contactsPersonIdList,
                    deleted: {$ne: true},
                    suggested: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": queryFinal},
                    "participants":{"$elemMatch": {"emailId":{$ne:participants}}}
                },
                {
                    $or:[
                        {
                            "to.receiverId":contactsPersonIdList,
                            "to.canceled":{$ne: true}
                        },
                        {
                            toList: {$elemMatch: {receiverId: contactsPersonIdList, canceled: {$ne: true}}}
                        },
                        {
                            senderId: contactsPersonIdList
                        }
                    ],
                    deleted: {$ne: true},
                    suggested:true,
                    "suggestedBy.userId":contactsPersonIdList,
                    "scheduleTimeSlots": {"$elemMatch": queryFinal},
                    "participants":{"$elemMatch": {"emailId":{$ne:participants}}}
                }
            ]
        },
        {"participants":1,"scheduleTimeSlots":1},
        function(error,invitations){

            if(error){
                logger.info('Error in  getParticipantsAtLocationTTL():MeetingManagementClass ',error);
                callback(error,[])
            } else {
                callback(error,invitations);
            }
        });

}

function updateCreateMeeting(calendarEmail,meeting,meetings,lastLoginDate,callback) {

    scheduleInvitation.findOneAndUpdate({invitationId: meeting.invitationId}, {$set: meeting}, {upsert: true}, function (error, result) {
        if (error) {
            logger.info('Error in updateCreateMeeting()--updateOrCreateMeetingBulk():MeetingManagementClass ', error);
            callback(null)
        } else {

            calendar.findOne().where('email').equals(calendarEmail).exec(function(err,calendarList){
                meeting._list=calendarList._id;
                if(!err){
                    events.findOneAndUpdate({invitationId:meeting.invitationId},{$set:meeting},{upsert:true,'new': true},function(e,cal){
                        if(!e){
                            if(calendarList.calendar.indexOf(cal._id)==-1){
                                calendarList.calendar.push(cal._id);
                                calendarList.save(function(er){
                                    if(!er){
                                        callback(result)
                                    }else{
                                        logger.info('list not updated');
                                        callback(result)
                                    }
                                })
                            }
                            else{
                                logger.info('list not updated 2');
                                callback(result)
                            }
                        }else{
                            logger.info('calendar not saved');
                            callback(result)
                        }
                    });
                }
                else{
                    logger.info('calendar not saved 2');
                    callback(result)
                }
            });
        }
    })
}
// export the User Management Class
module.exports = MeetingManagementClass;