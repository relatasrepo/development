//**************************************************************************************
// File Name            : documentShareManagement
// Functionality        : Document Share Management for share docuemnt
// History              : First Version -> renamed from documents in dataAccess/documentManagement
//
//
//**************************************************************************************var documentsCollection = require('../databaseSchema/userManagementSchema').documents;
var winstonLog = require('../common/winstonLog');

var logLib = new winstonLog();
var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();


function DocumentManagementClass()
{

}

DocumentManagementClass.prototype.removeUserDocuments = function(userId,callback){
    documentsCollection.remove({"sharedBy.userId":userId},function(error,result){
        if(error){
            callback(false,'onDeleteDoc',error,result);
        }
        else callback(true,'onDeleteDoc',error,result);
    })
};

DocumentManagementClass.prototype.documentByIdCustomFields = function(docId,fields,callback){
    documentsCollection.findOne({_id:docId},fields,function(err,doc){
        if(err){
            logger.info('Error in documentByIdCustomFields():DocumentManagementClass ',err);
        }

        callback(err,doc)
    })
};

DocumentManagementClass.prototype.shareDocument = function(docId,shareWith,message,subject,callback){

    documentsCollection.findById({_id:docId},function(error,document){
        if(error && !checkRequired(document)){
            logger.info('Error in shareDocument():DocumentManagementClass ',error);
            callback(error,null,[])
        }
        else{
            var sharedWithNew = [];
            if(document.sharedWith.length > 0){

                for(var i=0; i<shareWith.length; i++){
                    if(shareWith[i].emailId != document.sharedBy.emailId){
                        var isExist = false;
                        for(var j=0; j<document.sharedWith.length; j++){
                            if(shareWith[i].emailId == document.sharedWith[j].emailId){
                                isExist = true;
                                if(checkRequired(shareWith[i].expiryDate)){
                                    document.sharedWith[j].expiryDate = new Date(shareWith[i].expiryDate)
                                    document.sharedWith[j].expirySec = Math.round((new Date(shareWith[i].expiryDate) - new Date()) / 1000)
                                }
                                if(checkRequired(subject))
                                    document.sharedWith[j].subject = subject;

                                if(checkRequired(message))
                                    document.sharedWith[j].message = message;

                                if(checkRequired(shareWith[i].firstName))
                                    document.sharedWith[j].firstName = shareWith[i].firstName;

                                if(checkRequired(shareWith[i].userId))
                                    document.sharedWith[j].userId = shareWith[i].userId;
                            }
                        }

                        if(!isExist){
                            sharedWithNew.push(shareWith[i]);
                            shareWith[i].sharedOn = new Date();
                            if(checkRequired(shareWith[i].expiryDate)){
                                shareWith[i].expiryDate = new Date(shareWith[i].expiryDate)
                                shareWith[i].expirySec = Math.round((shareWith[i].expiryDate - new Date()) / 1000)
                            }
                            if(checkRequired(subject))
                                shareWith[i].subject = subject;

                            if(checkRequired(message))
                                shareWith[i].message = message;

                            document.sharedWith.push(shareWith[i]);
                        }
                    }
                }
            }
            else{
                document.sharedWith = shareWith;
            }
            document.save();
            callback(null,document,sharedWithNew);
        }
    });
};

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

// export the User Management Class
module.exports = DocumentManagementClass;