var taskCollection = require('../databaseSchema/taskSchema').tasks;
var scheduleInvitation = require('../databaseSchema/userManagementSchema').scheduleInvitation;
var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var winstonLog = require('../common/winstonLog');
var _ = require("lodash")
var logLib = new winstonLog();
var moment = require('moment-timezone');

var logger =logLib.getTasksErrorLogger();

function TaskManagement(){

    this.createTask = function(task,callback){
        var taskObj = createTaskObj(task);
        taskObj.save(function(error,savedTask){
            if(error){
                logger.info('Error in createTask():TaskManagement ',error)
            }

            if(checkRequired(savedTask) && savedTask.taskFor == 'meeting'){
                addTaskToMeeting(savedTask.refId,savedTask._id.toString());
            }

            callback(error,savedTask);
        })
    };

    this.getTasks = function(taskFor,refId,callback){
        taskCollection.find({taskFor:taskFor,refId:refId},function(err,tasks){
            if(err){
                logger.info('Error in getTasks():TaskManagement ',err)
            }
            callback(err,tasks)
        })
    };

    this.updateTaskStatus = function(userId,data,callback){
        taskCollection.update(
            {
                _id: data.taskId,
                $or: [
                    {
                        assignedTo: userId
                    },
                    {
                        createdBy: userId
                    }
                ]
            }
            ,{$set:{status:data.status}},function(error,updateResult){
                if(error){
                    logger.info('Error in updateTaskStatus():TaskManagement ',error)
                }

            callback(updateResult && updateResult.ok);
        })
    };

    this.getTasksForDate = function(taskFor,userId,dateMin,dateMax,callback){

        var query = {
            taskFor:taskFor,
            dueDate:{$gte:new Date(dateMin),$lte:new Date(dateMax)},
            $or:[
                {
                    assignedTo:userId
                },
                {
                    createdBy:userId
                }
            ]
        };
        taskCollection.find(query,function(err,tasks){
            if(err){
                logger.info('Error in getTasksForDate():TaskManagement ',err)
            }
            callback(err,tasks)
        })
    };

    this.getTasksForDateAll = function(userId,dateMin,dateMax,callback){

        var query = {
            dueDate:{$gte:new Date(dateMin),$lte:new Date(dateMax)},
            $or:[
                {
                    assignedTo:userId
                },
                {
                    createdBy:userId
                }
            ]
        };
        taskCollection.find(query,null,{sort:{dueDate:1}},function(err,tasks){
            if(err){
                logger.info('Error in getTasksForDateAll():TaskManagement ',err)
            }
            callback(err,tasks)
        })
    };

    this.getTasksForDateAllPopulate = function(userId,dateMin,dateMax,callback){

        var query = {
            dueDate:{$gte:new Date(dateMin),$lte:new Date(dateMax)},
            $or:[
                {
                    assignedTo:userId
                },
                {
                    createdBy:userId
                }
            ]
        };
        taskCollection.find(query,null,{sort:{dueDate:1}},function(err,tasks){
            if(err){
                logger.info('Error in getTasksForDateAll():TaskManagement ',err)
            }
            if(checkRequired(tasks) && tasks.length > 0){
                myUserCollection.populate(tasks,[{path:'createdBy', select:'firstName lastName emailId publicProfileUrl'},{path:'assignedTo', select:'firstName lastName emailId publicProfileUrl'}],function(error,invi){
                    if(error){
                        logger.info('Error in getTasksForDateAllPopulate():TaskManagement populate ',error)
                    }
                    if(checkRequired(invi) && invi.length > 0){
                        callback(invi)
                    }
                    else callback([])
                })
            }
            else callback([])
        })
    };

    this.getPendingTasksByDate = function(userId,dateMin,dateMax,callback){

        var query = {
            $or:[
                {
                    assignedTo:userId
                },
                {
                    createdBy:userId
                }
            ],
            dueDate:{$gte:new Date(dateMin)},
            status:{$ne:'complete'}
        };
        taskCollection.find(query,null,{sort:{dueDate:1}},function(err,tasks){
            if(err){
                logger.info('Error in getPendingTasksByDate():TaskManagement ',err)
            }
            callback(tasks);
        })
    };

    this.getUpcomingTasks = function(emailId, callback) {
        var query = {
            assignedToEmailId:emailId,
            dueDate: {$gt: new Date()},
        }
    
        taskCollection.find(query, {taskName:1}, function(err, tasks) {
            if(err) {
                console.log("error in getting upcoming tasks");
                callback(err, {upcomingTasks:0});
            } else {
                callback(err, {upcomingTasks:tasks.length});
            }
        })
    }

    this.getOverdueTasks = function(emailId, callback) {
        // var startDate = moment("01 Jan 2015").startOf('day').toDate();
        // var endDate = moment().subtract(1,"days").endOf('day').toDate();
        var startDate = moment("01 Jan 2015");
        var endDate = moment().subtract(1,"days");

        var query = {
            assignedToEmailId:emailId,
                    dueDate:{
                        $gte: new Date(moment(startDate)),
                        $lte: new Date(moment(endDate))
                    },
                    status:{$ne:'complete'}
        };
        taskCollection.find(query,{taskName:1, assignedToEmailId:1, status:1},function(err,tasks){
            if(err){
                callback(err, {overdueTasks:0})
            }
            callback(err, {overdueTasks:tasks.length});
        })
    }

    this.getAllTasksBetweenUsers = function(userId,cUserId,isEmailId,callback){

        var query = {
            $or:[
                {
                    assignedTo:userId,
                    createdBy:cUserId
                },
                {
                    assignedTo:cUserId,
                    createdBy:userId
                }
            ]
        };
        if(isEmailId){
            query = {
                $or:[
                    {
                        assignedTo:userId,
                        createdByEmailId:cUserId
                    },
                    {
                        assignedToEmailId:cUserId,
                        createdBy:userId
                    }
                ]
            };
        }

        taskCollection.find(query,null,{sort:{dueDate:1}},function(err,tasks){
            if(err){
                logger.info('Error in getAllTasksBetweenUsers():TaskManagement ',err)
            }
            if(checkRequired(tasks) && tasks.length > 0){
                myUserCollection.populate(tasks,[{path:'createdBy', select:'firstName lastName emailId publicProfileUrl'},{path:'assignedTo', select:'firstName lastName emailId publicProfileUrl'}],function(error,invi){
                    if(error){
                        logger.info('Error in getAllTasksBetweenUsers():TaskManagement populate ',error)
                    }
                    if(checkRequired(invi) && invi.length > 0){
                        callback(invi)
                    }
                    else callback([])
                })
            }
            else callback([])
        })
    };

    this.getAllTasksBetweenUsersLatestLimit = function(userId,cUserId,isEmailId,limit,callback){

        var query = {
            $or:[
                {
                    assignedTo:userId,
                    createdBy:cUserId
                },
                {
                    assignedTo:cUserId,
                    createdBy:userId
                }
            ]
        };

        if(isEmailId){
            query = {
                $or:[
                    {
                        assignedTo:userId,
                        createdByEmailId:cUserId
                    },
                    {
                        assignedToEmailId:cUserId,
                        createdBy:userId
                    }
                ]
            };
        }

        taskCollection.find(query,null,{sort:{createdDate:1}},{limit:limit},function(err,tasks){
            if(err){
                logger.info('Error in getAllTasksBetweenUsersLatestLimit():TaskManagement ',err)
            }
            if(checkRequired(tasks) && tasks.length > 0){
                myUserCollection.populate(tasks,[{path:'createdBy', select:'firstName lastName emailId publicProfileUrl'},{path:'assignedTo', select:'firstName lastName emailId publicProfileUrl'}],function(error,invi){
                    if(error){
                        logger.info('Error in getAllTasksBetweenUsersLatestLimit():TaskManagement populate ',error)
                    }
                    if(checkRequired(invi) && invi.length > 0){
                        callback(invi)
                    }
                    else callback([])
                })
            }
            else callback([])
        })
    };

    this.getTasksForDateAllGreater = function(userId,cUserId,isEmailId,date,callback){

        var query = {
            createdBy:userId,
            dueDate:{$gte:new Date(date)}
        };

        if(!isNumber(parseInt(cUserId))){
            query.assignedTo = cUserId
        }

        if(isEmailId){
            query = {
                assignedToEmailId:cUserId,
                createdBy:userId,
                dueDate:{$gte:new Date(date)}
            }
        }

        if(isEmailId == 'mobileNumber'){
            callback(null,[]);
        }
        else{
            taskCollection.find(query,{taskName:1,dueDate:1},{sort:{dueDate:1}},function(err,tasks){
                if(err){
                    logger.info('Error in getTasksForDateAllGreater():TaskManagement ',err)
                }
                callback(err,tasks)
            })
        }
    };

    this.updateTaskWithGoogleEventId = function(taskId,googleEventId,callback){
        taskCollection.update({_id:taskId},{$set:{googleEventId:googleEventId}},function(err,result){
            if(err){
                logger.info('Error in updateTaskWithGoogleEventId():TaskManagement ',err)
            }
            callback(err,result == 1);
        })
    }
}

function createTaskObj(task){
    return new taskCollection({
        taskName:task.taskName,
        assignedTo:checkRequired(task.assignedTo) ? task.assignedTo : null,
        assignedToEmailId:task.assignedToEmailId || null,
        createdBy:task.createdBy || null,
        createdByEmailId:task.createdByEmailId || null,
        dueDate:new Date(task.dueDate),
        taskFor:task.taskFor,
        description:task.description || null,
        refId:task.refId || null
    });
}

function addTaskToMeeting(invitationId,taskId){

    scheduleInvitation.update({invitationId:invitationId},{$addToSet:{tasks:taskId}},function(err,updateResult){
        if(err){
            logger.info('Error in addTaskToMeeting():TaskManagement ',err)
        }
    })
}

function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
}

TaskManagement.prototype.createTask_v2 = function (user,task,callback) {
    var taskObj = createTaskObj_v2(task);
    taskObj.save(function(error,savedTask){
        callback(error,savedTask)
    })
}

TaskManagement.prototype.getAllTasks = function (user,callback) {
    
    var query = {
        $or:[
            {
                assignedToEmailId:user.emailId
            },
            {
                createdByEmailId:user.emailId
            },
            {
                "participants.emailId":user.emailId
            }
        ]
    };
    
    taskCollection.find(query,null,{sort:{updatedDate:1}},function(err,tasks){
        if(err){
            logger.info('Error in getAllTasks():TaskManagement ',err)
        }
        callback(err,tasks)
    })
}

TaskManagement.prototype.getTaskByRefId = function (refId,callback) {

    var query = {
        refId:refId
    };

    taskCollection.find(query,null,function(err,tasks){
        if(err){
            logger.info('Error in getTaskByRefId():TaskManagement ',err)
        }
        callback(err,tasks)
    })
}

TaskManagement.prototype.getAllTasksForUserList = function (userEmailIds,forWeeklyReview,filter,startDate,endDate,callback) {

    var query = {
        $or:[
            {
                assignedToEmailId:{$in:userEmailIds}
            },
            {
                createdByEmailId:{$in:userEmailIds}
            },
            {
                "participants.emailId":{$in:userEmailIds}
            }
        ]
    };

    if(forWeeklyReview){
        query.taskFor = "weeklyReview"
    }

    if(filter && filter != "dueDate"){
        query.taskFor = filter
    }

    if(filter == "allTasks"){
        query.taskFor = {$ne:filter}
    }

    if(startDate && endDate){
        startDate = startDate.split(" ")[0];
        endDate = endDate.split(" ")[0];
        
        var fromDate = moment(startDate).startOf('day');
        var toDate = moment(endDate).endOf('day');

        query.dueDate = {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
        }
    }

    taskCollection.find(query,null,function(err,tasks){
        if(err){
            logger.info('Error in getAllTasks():TaskManagement ',err)
        }
        callback(err,tasks)
    })
}

TaskManagement.prototype.getAllTasksForAllUsers = function (users,startOfweek,endOfweek,callback) {

    var query = {
        taskFor:"weeklyReview",
        assignedToEmailId: {$in: users},
        createdDate: {$gte: new Date(startOfweek), $lte: endOfweek}
    }

    taskCollection.find(query).lean().exec(function(err,tasks){
        if(err){
            logger.info('Error in getAllTasksForAllUsers():TaskManagement ',err)
        }
        callback(err,tasks)
    })
}

TaskManagement.prototype.updateTask = function (userId,data,callback) {

    var updateObj = {
        status:data.status,
        priority:data.priority,
        updatedDate:new Date(),
        dueDate:new Date(data.dueDate)
    }

    if(data.taskName){
        updateObj["taskName"] = data.taskName
    }

    if(data.status == "complete"){
        updateObj.closeDate = new Date()
    }

    taskCollection.update(
        {
            _id: data._id
        }
        ,{$set:updateObj},function(error,updateResult){
            if(error){
                logger.info('Error in updateTask():TaskManagement ',error)
            }

            callback(updateResult && updateResult.ok);
        })
}

function createTaskObj_v2(task){
    return new taskCollection({
        taskName:task.taskName,
        assignedToEmailId:task.assignedToEmailId || null,
        createdBy:task.createdBy || null,
        createdByEmailId:task.createdByEmailId || null,
        dueDate:new Date(task.dueDate),
        createdDate:new Date(),
        taskFor:task.taskFor,
        priority:task.priority,
        description:task.description || null,
        refId:task.refId || null,
        participants:_.map(task.participants,function (el) {
            return {
                emailId:el.emailId,
                responseRequired:el.responseRequired?true:false
            }
        })
    });
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

module.exports = TaskManagement;
