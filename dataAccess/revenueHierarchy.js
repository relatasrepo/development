var rhCollection = require('../databaseSchema/revenueHierarchySchema').revenueHierarchy
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston();
var _ = require("lodash");

function RevenueHierarchy() {}

RevenueHierarchy.prototype.getHierarchy = function (companyId,callback){
    rhCollection.find({companyId:companyId},{monthlyTargets:0}).lean().exec(function (err,result) {
      callback(err,result)
    });
}

RevenueHierarchy.prototype.updateHierarchy = function (userId,monthlyTargets,callback){
    rhCollection.update({userId:userId},{$set:{monthlyTargets:monthlyTargets}}).exec(function (err,result) {
      callback(err,result)
    });
}

RevenueHierarchy.prototype.updateHierarchyPaths = function (companyId,updateObj,common,callback){

    bulkHierarchyPathForSelf(companyId,common,updateObj,callback);

    if(updateObj.childrenIds && updateObj.childrenIds.length>0){
        bulkHierarchyPathForChildren(companyId,common,updateObj)
    }
}

function bulkHierarchyPathForChildren(companyId,common,updateObj,callback) {
    var bulk = rhCollection.collection.initializeUnorderedBulkOp();
    updateObj.childrenIds.forEach(function(a){
        bulk.find({
            userId:common.castToObjectId(a)
        })
        .updateOne({
            $set:{
                hierarchyPath:updateObj.pathsObjById[a].path
            }
        });
    });

    if(updateObj.childrenIds.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                if(callback){
                    callback(false)
                }
            }
            else {
                result = result.toJSON();
                logger.info('addUserToHierarchy() results ',result);
                if(callback){
                    callback(result.upserted)
                }
            }
        });
    }
    else{
        if(callback){
            callback(true)
        }
    }
}

function bulkHierarchyPathForSelf(companyId,common,updateObj,callback) {

    rhCollection.update({userId:common.castToObjectId(updateObj.selfId)},{$set:{hierarchyParent:updateObj.selfParentId,hierarchyPath:updateObj.pathForSelf}}).exec(function (err,result) {
        callback(err,result)
    });
}

RevenueHierarchy.prototype.removeFromHierarchy = function (userIds,companyId,callback){
    rhCollection.remove({userId:{$in:userIds},companyId:companyId}).exec(function (err,result) {
      callback(err,result)
    });
}

RevenueHierarchy.prototype.setOrgHead = function (updateObj,common,companyId,callback){

    var data = [{companyId: common.castToObjectId(companyId),
        userId: common.castToObjectId(updateObj.userId),
        ownerEmailId:updateObj.emailId,
        hierarchyParent: null,
        hierarchyPath:null,
        orgHead:true
    }];

    var bulk = rhCollection.collection.initializeUnorderedBulkOp();

    data.forEach(function(a){

        bulk.find({
            userId:a.userId
        })
        .upsert()
        .updateOne(
            {$set:{
                companyId:a.companyId,
                userId:a.userId,
                ownerEmailId:a.ownerEmailId,
                hierarchyParent:a.hierarchyParent,
                hierarchyPath:a.hierarchyPath,
                orgHead:a.orgHead
            }});
    });


    if(data.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                callback(false)
            }
            else {
                result = result.toJSON();
                logger.info('addUserToHierarchy() results ',result);
                callback(result.upserted)
            }
        });
    }
    else{
        callback(true)
    }
}

RevenueHierarchy.prototype.addUserToHierarchy = function (updateObj,companyId,common,callback){
    var data = [{companyId: common.castToObjectId(companyId),
            userId: common.castToObjectId(updateObj.userId),
            ownerEmailId:updateObj.emailId,
            hierarchyParent: updateObj.hierarchyParent,
            hierarchyPath:updateObj.hierarchyPath,
            orgHead:false
        }];

    var bulk = rhCollection.collection.initializeUnorderedBulkOp();

    data.forEach(function(a){

        bulk.find({
                userId:a.userId
            })
            .upsert()
            .updateOne(
                {$set:{
                    companyId:a.companyId,
                    userId:a.userId,
                    ownerEmailId:a.ownerEmailId,
                    hierarchyParent:a.hierarchyParent,
                    hierarchyPath:a.hierarchyPath,
                    orgHead:a.orgHead
                }}
            );
    });


    if(data.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                callback(false)
            }
            else {
                result = result.toJSON();
                logger.info('addUserToHierarchy() results ',result);
                callback(result.upserted)
            }
        });
    }
    else{
        callback(true)
    }

}

RevenueHierarchy.prototype.getUserHierarchy = function(userId, callBack) {

    rhCollection.findOne({ userId: userId }).lean().exec(function(err, user) {

        if(!err && user){
            rhCollection.find({ companyId:user.companyId, hierarchyPath: { $regex: userId.toString(), $options: 'i' }}).lean().exec(function(err, users) {
                user["hierarchyParent"] = null
                users.push(user)
                callBack(null, users)
            })
        } else {
            callBack(err,null)
        }
    })
};

RevenueHierarchy.prototype.customCompanyUpdate = function (findQ,updateQ,callback) {

    companyCollection.update(findQ,updateQ,function (err,result) {
        if(callback){
            callback(err,result)
        }
    })
};

module.exports = RevenueHierarchy;