
var mySahredDocs = require('../databaseSchema/userManagementSchema').documents;


var winstonLog = require('../common/winstonLog');

var logLib = new winstonLog();

var logger =logLib.getWinston()

function checkDocumentExpiry(){

}

checkDocumentExpiry.prototype.checkExpiry = function(){
    mySahredDocs.find(function(error,data){
        if(error){
            logger.info('Error in mongo while checking document expiry '+error);
        }
        else{

            for(var i in data){
                if(data[i].sharedWith[0]){
                    for(var j in data[i].sharedWith){
                        if(checkRequired(data[i].sharedWith[j].expirySec)){

                            if(data[i].sharedWith[j].expirySec <= 0){
                                 updateDocAccess(data[i]._id,data[i].sharedWith[j].userId,false);
                            }
                            else{
                                var expiryTime = data[i].sharedWith[j].expirySec - 3600;
                                if(expiryTime <= 0){
                                    updateDocAccess(data[i]._id,data[i].sharedWith[j].userId,false);
                                }
                                updateSec(data[i]._id,data[i].sharedWith[j]._id,expiryTime)

                            }

                        }

                    }
                }
                else{
                  // Documents which does not have sharedWith arr
                }
             }
        }
    })
}

function updateSec(docId,shareWithId,expiryTime){

    mySahredDocs.update({_id:docId,"sharedWith._id":shareWithId},{"sharedWith.$.expirySec":expiryTime},function(error,isupdated){
        var flag;
        if (error) {
            logger.info('Error in DB while updating Document expiry seconds in automatic expiry time API '+error);
        }
        if (isupdated == 0) {
            flag = false;
            logger.info('Document access expiry seconds update failed in automatic expiry time API');
        }
        else{
            //logger.info('Document access expiry seconds update success in automatic expiry time API ');
        }
    });

}
function updateDocAccess(docId,userId,status){
    mySahredDocs.update({_id:docId,"sharedWith.userId":userId},{"sharedWith.$.accessStatus":status,"sharedWith.$.expiryDate":null,"sharedWith.$.expirySec":null},function(error,isupdated){
        var flag;
        if (error) {
            logger.info('Error in DB while updating Document access in automatic expiry time API '+error);
        }
        if (isupdated == 0) {
            flag = false;
            logger.info('Document access update failed in automatic expiry time API');
        }
        else{
            logger.info('Document access update success in automatic expiry time API');
        }

    });
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

module.exports = checkDocumentExpiry;