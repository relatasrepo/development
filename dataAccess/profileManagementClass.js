var myUserCollection = require('../databaseSchema/userManagementSchema').User;

var winstonLog = require('../common/winstonLog');

var logLib = new winstonLog();

var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();

function User(){

    this.updateProfileCustomQuery = function(userId,updateObj,callback){
        myUserCollection.update({_id:userId},{$set:updateObj},function(error,updateResult){
            if(error){
                loggerError.info('User class, updateProfileCustomQuery() ',error);
            }

            callback(updateResult && updateResult.ok);
        })  
    };

    this.updateContactsToFavorite = function(userId,emailIds,callback){
        // emailIds.forEach(function(emailId){
        //     updateUserContactFavorite(userId,emailId);
        // });
        // callback();
        if(emailIds.length > 0) {
            var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
            emailIds.forEach(function(a){
                bulk.find({
                    _id:userId,
                    "contacts.personEmailId":a,
                    "contacts.doNotTrackForFavorite":{$ne:true}
                }).update({$set: {"isFavoriteUpdated":true,"contacts.$.watchThisContact":true,"contacts.$.favorite":true}});
            });

            bulk.execute(function (err, result) {
                if (err) {
                    logger.info('Error in updateContactsToFavorite(): ', err)
                    callback(false)
                }
                else {
                    result = result.toJSON();
                    logger.info('updateContactsToFavorite() results ', result);
                    callback(true)
                }
            });
        }
        else{
            callback(true)
        }

    };

    this.searchByNames = function(userId,searchText,skip,limit,callback){
        var search = new RegExp(searchText,'i');

        myUserCollection.find({$or:[{firstName:search},{lastName:search}],_id:{$ne:userId}},{firstName:1,lastName:1,emailId:1,companyName:1,designation:1,profilePicUrl:1}).skip(skip).limit(limit).exec(function(error,response){
             callback(error,response)
        });
    };

    this.getUserUniqueName = function(uName,uUrl,start,callback){
        getUserUniqueName(uName,uUrl,function(user){
            if(checkRequired(user)){

            }else callback(true)
        })
    };

    this.getUsersByCompanyId = function(userId,companyId,callback){
        myUserCollection.find({companyId:companyId,_id:{$ne:userId},resource:{$ne:true}},{emailId:1,firstName:1,lastName:1,companyId:1},function(error,users){
            if(error){
                loggerError.info('Error getUsersByCompanyId():User ',error);
            }
            else callback(users)
        })
    };

    this.getMeetingRoomIdListWithCompanyId = function(companyId,callback){
        myUserCollection.aggregate([
            {$match:{companyId:companyId,resource:true}},
            {
                $group:{
                    _id:null,
                    list:{$addToSet:"$_id"},
                    rooms:{$push:{_id:"$_id",firstName:"$firstName",lastName:"$lastName",emailId:"$emailId"}}
                }
            }
        ]).exec(function(error,result){
            if(error){
                callback([])
            }
            else{
                callback(result)
            }
        });
    }

    this.getUsersReportingTo = function(userId,emailId,callback){
        myUserCollection.aggregate([
            {
                $match:{corporateUser:true,hierarchyPath:{ $regex: userId.toString(), $options: 'i' },resource:{$ne:true}}
            },
            {
                $project:{_id:1,emailId:1,companyId:1,hierarchyPath:1,hierarchyParent:1}
            },
            {
                $group:{
                    _id:null,
                    idList:{$addToSet:"$_id"},
                    emailIdList:{$addToSet:"$emailId"}
                }
            }
        ]).exec(function(error,users) {
            if (error) {
                logger.info("Error searchUserContacts_accounts: find subtree ", error)
            }
            if (users && users.length > 0 && users[0] && users[0].idList && users[0].idList.length > 0) {
                users[0].idList.push(userId)
                users[0].emailIdList.push(emailId)
                callback(users[0].idList,users[0].emailIdList)
            }
            else callback([userId],[emailId])
        })
    }
}

function getUserUniqueName(uName,uUrl,callback){
    myUserCollection.findOne({publicProfileUrl:{$in:[uName,uUrl]}},{firstName:1,lastName:1,emailId:1},function(err,user){
        callback(user);
    })
}

function updateUserContactFavorite(userId,emailId){
    myUserCollection.update({_id:userId,"contacts.personEmailId":emailId},{
        $set:{
            "contacts.$.favorite":true,
            "contacts.$.watchThisContact":true
        }
    },function(er,updateResult){

    })
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

module.exports = User;