//**************************************************************************************
// File Name            : document
// Functionality        : Router for document(Quotation/Purchase Order/Invoice)
// History              : First Version
//
//
//**************************************************************************************

var documentTemplateCollection = require('../databaseSchema/documentTemplateSchema').documentTemplates;
var documentCollection = require('../databaseSchema/documentSchema').documentModel;
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');
var documentTemplatesManagementClass = require('../dataAccess/documentTemplateManagement');

var common = new commonUtility();
var logLib = new winstonLog();
var loggerError = logLib.getCorporateErrorLogger();
var logger = logLib.getCorporateLogger();
var documentTemplatesManagementClassObj = new documentTemplatesManagementClass();


function documentManagement() {
}

// function to check and create the new document
documentManagement.prototype.checkAndCreateDocument = function (companyId, userId, userEmailId, document, callback) { var templateId = common.castToObjectId(document.documentTemplateId);

    var parentScope = this;
    this.getDocumentByName(companyId, userId, document, function(error, savedDocument) {
        if(savedDocument) {
            callback(error, savedDocument);
        }
        else {
            var newDocTemplateObj = new documentCollection({
                companyId: companyId,
                documentTemplateId : document.documentTemplateId,
                documentCreatorId:userId,
                documentCreatedBy:userEmailId,
                documentTemplateName: document.documentTemplateName,
                documentTemplateType: document.documentTemplateType,
                documentName: document.documentName,
                documentCreatedDate: new Date(),
                documentUpdatedDate: new Date(),
                documentElementList: document.documentElementList,
                documentAttrList: document.documentAttrList,
                isTemplateVersionControlled: document.isTemplateVersionControlled
            });

            newDocTemplateObj.save(function(error, savedTemplate) {

                if(error) {
                    logger.info('Error in createNewDoc(): documentManagement', error);
                }
                callback(error, savedTemplate);
            });

            documentTemplatesManagementClassObj.updateIsDocumentsCreated(companyId,
                templateId,
                function(error1, savedDoc) {
                    if(error1) {
                        logger.info('Error in updateNumDocumentsCreated1(): documentManagement', error1);

                    } else {

                    }
                })
        }
    })
};

// get document by name and type to check for its existence
documentManagement.prototype.getDocumentByName = function(companyId, userId, document, callback) {

    documentCollection.findOne({
            companyId: companyId,
            documentTemplateType:document.documentTemplateType,
            documentTemplateName:document.documentTemplateName,
            documentName:document.documentName
        },
        {  documentTemplateType:1,
            documentTemplateName:1,
            documentName:1},function(error,result){
            if(error){
            } else {
                callback(error,result)
            }
        })
}

documentManagement.prototype.cloneDocument = function(companyId, documentId, documentName, callback) {
    documentCollection.findOne({companyId: companyId, _id: documentId}, {_id:0}).lean().exec(function(error, document) {
        document.documentCreatedDate = document.documentUpdatedDate = new Date();
        document.documentName = documentName;
        document.documentVersion = document.documentVersion = 1;
        document.documentStage = 'Draft';

        if(error && !document) {
            callback(error, document, "Error Finding document");
        
        } else {
            new documentCollection(document)
                .save(function(err, clonedDocument) {
                    if(err) {
                        callback(err, clonedDocument, "Error while saving new document");
                    } else {
                        callback(err, clonedDocument, "Document cloned successfully");                    
                    }
                })
        }


    });
}

// update the existing document
documentManagement.prototype.updateDocumentElements = function (companyId,
                                                                objUserId,
                                                                userEmailId,
                                                                document,
                                                                callback) {

        if (document.incrementVersionClicked){

            documentCollection.update({companyId: companyId,
                    documentTemplateName: document.documentTemplateName,
                    documentTemplateType: document.documentTemplateType,
                    documentName: document.documentName,
                    documentVersion: document.documentVersion},
                {$set:{
                        documentCreatorId:objUserId,
                        documentCreatedBy:userEmailId,
                        documentTemplateName: document.documentTemplateName,
                        documentTemplateType: document.documentTemplateType,
                        documentName: document.documentName,
                        oppReferenceIds: document.reference.referenceIds,
                        documentElementList: document.documentElementList,
                        documentAttrList: document.documentAttrList,
                        documentVersion: document.documentVersion,
                        documentStage: document.documentStage,
                        isTemplateVersionControlled: document.isTemplateVersionControlled,
                        currentDocumentVersion: document.currentDocumentVersion,
                        documentCreatedDate: new Date(),
                        documentUpdatedDate: new Date()
                    }},{upsert:true},function (err,result) {
                    callback(err, result);
                });

            // Update the currentDocumentVersion for all other documents of the same type
            documentCollection.update({companyId: companyId,
                    documentTemplateName: document.documentTemplateName,
                    documentTemplateType: document.documentTemplateType,
                    documentName: document.documentName,
                    documentVersion: {$ne:document.currentDocumentVersion}
                },
                {$set:{
                        currentDocumentVersion: document.currentDocumentVersion
                    }},{multi:true},function (err,result) {
                    callback(err, result);
                });

        } else {
            documentCollection.update({companyId: companyId,
                    documentTemplateName: document.documentTemplateName,
                    documentTemplateType: document.documentTemplateType,
                    documentName: document.documentName,
                    documentVersion: document.documentVersion},
                {$set:{
                        documentTemplateName: document.documentTemplateName,
                        documentTemplateType: document.documentTemplateType,
                        documentName: document.documentName,
                        documentElementList: document.documentElementList,
                        documentAttrList: document.documentAttrList,
                        documentVersion: document.documentVersion,
                        oppReferenceIds: document.reference.referenceIds,
                        currentDocumentVersion: document.currentDocumentVersion,
                        isTemplateVersionControlled: document.isTemplateVersionControlled,
                        documentStage: document.documentStage,
                        documentUpdatedDate: new Date()
                    }},{upsert:true},function (err,result) {
                    callback(err, result);
                });
        }
}

// function to get all the documents 
documentManagement.prototype.getAllDocuments = function (companyId, createdBy, callback) {

    documentCollection.find({companyId:companyId, documentCreatorId:createdBy},
        {}, {sort:{documentUpdatedDate:-1}}, function(err,result){
            if(!err && result){
                callback(err,result)
            } else {
                result = docTemplateObj;
                callback(err,result)
            }
        })
}

documentManagement.prototype.getLinkedDocuments = function(companyId, referenceType, referenceId, callback) {
    documentCollection.find({companyId:companyId, oppReferenceIds:referenceId},
        {},function(err,result){
            callback(err, result);
        })
}

// function to get the document by id
documentManagement.prototype.getDocumentById = function (companyId,documentId, callback) {
    documentCollection.findOne({companyId:companyId,
            _id:documentId},
        {},function(err,result){
            callback(err, result);
        })
}

// function to get the document by id
documentManagement.prototype.getDocumentByVersion = function (companyId,
                                                        userId,
                                                        documentTemplateType,
                                                        documentTemplateName,
                                                        documentName,
                                                        documentVersion,
                                                        callback) {

    documentCollection.findOne({companyId:companyId,
            documentCreatorId:userId,
            documentTemplateType:documentTemplateType,
            documentTemplateName:documentTemplateName,
            documentName:documentName,
            documentVersion:parseInt(documentVersion),
        },function(err,result){
            callback(err, result);
        })
}

documentManagement.prototype.deleteMultipleDocuments = function(companyId, documentIds, callback) {
    documentCollection.remove({companyId:companyId, _id:{$in:documentIds}}, function(error, result) {
        callback(error, result);
    })
}

documentManagement.prototype.getDocumentConsolidatedReport = function(callback) {
    documentCollection.aggregate([
        {
            $match:{
                documentCreatedDate:{$gte:new Date("Jan 01 2018")}
            }
        },
        {
            $project: {
                companyId:1,
                documentCreatorId:1,
                documentCreatedBy:1,
                documentCreatedDate:1,
            },
        },
        {
            $sort: {
                documentCreatedDate:-1
            }
        },
        {
            $group: {
                _id: {
                    companyId: "$companyId",
                    userId: "$documentCreatorId"
                },
                userEmailId:{
                    $first:"$documentCreatedBy",
                },
                totalDocsCreated:{
                    $sum:1
                },
                lastDocCreated:{
                    $first:"$documentCreatedDate"
                }
            }
        }
    ],function(error, result){
        if(error){
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
}

documentManagement.prototype.getDocumentDetailReport = function(callback) {
    documentCollection.aggregate([
        {
            $match:{
                documentCreatedDate:{$gte:new Date("Jan 01 2018")}
            }
        },
        {
            $project: {
                companyId:1,
                documentCreatedBy:1,
                documentCreatedDate:1,
                documentName:1,
                documentVersion:1,
                documentStage:1,
            }
        }
    ],function(error, result){
        if(error){
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
}


documentManagement.prototype.removeUserDocuments = function(userId,callback){
    callback(true,true,true,true)
}
module.exports = documentManagement;
