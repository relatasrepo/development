
var eventCollection = require('../databaseSchema/userManagementSchema').event;
var subscribeToEventsCollection = require('../databaseSchema/userManagementSchema').subscribeToEvents;

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston()

function EventDataAccess()
{

}

/**** Functions on events ****/

function constructEventDetails(eventDetails){
    var date = new Date();
    var eventDetails = new eventCollection({
        eventName:  eventDetails.eventName,
        locationName:  eventDetails.locationName,
        eventLocation : eventDetails.eventLocation,
        eventLocationUrl:eventDetails.eventLocationUrl,
        eventDescription:eventDetails.eventDescription,
        startDateTime: eventDetails.startDateTime,
        endDateTime:eventDetails.endDateTime,
        eventCategory: eventDetails.eventCategory,
        accessType: eventDetails.accessType,
        tags:eventDetails.tags,
        eventMap:eventDetails.eventMap,
        createdDate:eventDetails.createdDate,
        lastUpdated:eventDetails.createdDate,
        createdBy:eventDetails.createdBy,
        speakerName:eventDetails.speakerName,
        speakerDesignation:eventDetails.speakerDesignation,
        docs:eventDetails.docs,
        image:eventDetails.image
    });
    return eventDetails;
}

EventDataAccess.prototype.createEvent = function(eventData,callback){

    var event = constructEventDetails(eventData)

    event.save(function(err,event){
        if(err){
            callback(err,null,{message:'Creating event failed'});
        }
        else{

            callback(err,event,{message:'Creating event success'});

        }
    })
};

EventDataAccess.prototype.getUpComingPublicEvents = function(callback){
    var minDate = new Date();
    minDate.setDate(minDate.getDate() - 10);
    eventCollection.find({startDateTime:{$gte:minDate},accessType:'public'},function(error,events){
        if(error){
            logger.info("Mongo error while getting all public upcoming events "+error);
            callback(error,false);
        }
        else{
            callback(error,events);
        }
    })
};

EventDataAccess.prototype.getEventsByType = function(userId,eType,callback){
    var minDate = new Date();
    minDate.setDate(minDate.getDate() - 2);

    eventCollection.find({eventCategory:eType,accessType:'public',startDateTime:{$gte:minDate}},function(error,events){
        if(error){
            logger.info("Mongo error while getting all public upcoming events by event type "+error);
            callback(error,false);
        }
        else{
            callback(error,events);
        }
    })
};

EventDataAccess.prototype.getEventsByTypeAndLocation = function(userId,eType,eLocation,callback){
    var minDate = new Date();
    minDate.setDate(minDate.getDate() - 2);
    eventCollection.find({eventCategory:eType,eventLocation:eLocation,accessType:'public',startDateTime:{$gte:minDate}},function(error,events){
        if(error){
            logger.info("Mongo error while getting all public upcoming events by event type and location "+error);
            callback(error,false);
        }
        else{
            callback(error,events);
        }
    })
};

EventDataAccess.prototype.getAllEventsLocations = function(callback){
    eventCollection.find({},{eventLocation:1},function(error,locationList){
        var list = removeDuplicates(locationList)
        callback(error,list);
    })
};

EventDataAccess.prototype.getAllPublicEvents = function(callback){
    eventCollection.find({accessType:'public'},function(error,eventList){
        callback(error,eventList);
    })
};

EventDataAccess.prototype.getEventsByArrayOfId = function(idArr,callback){
    eventListById(idArr,callback);
};

EventDataAccess.prototype.getEventById = function(eventId,callback){
    eventCollection.findOne( {"_id":eventId},function(error,event){

        callback(error,event);
    })
};

function eventListById(arr,back){
    eventCollection.find( {"_id": { "$in":arr }},function(error,eventList){
        back(error,eventList);
    })
}

function upComingEventListById(arr,back){
    var date = new Date();
    date.setDate(date.getDate()-3);
    date.setHours(0,0,0,0);
    eventCollection.find( {"_id": { "$in":arr },startDateTime:{$gte:date}},function(error,eventList){
        back(error,eventList);
    })
}

EventDataAccess.prototype.updateEvent = function(event,callback){

    var date = new Date();
    var updateContent = {
        eventName:  event.eventName,
        locationName:event.locationName,
        eventLocation : event.eventLocation,
        eventLocationUrl:event.eventLocationUrl,
        eventDescription:event.eventDescription,
        startDateTime: event.startDateTime,
        endDateTime:event.endDateTime,
        eventCategory: event.eventCategory,
        accessType: event.accessType,
        speakerName:event.speakerName,
        speakerDesignation:event.speakerDesignation,
        lastUpdated:date
    }
    if(checkRequired(event.docs)){
        if(checkRequired(event.docs[0])){
            updateContent.docs = event.docs
        }
    }
    if(checkRequired(event.image)){
        updateContent.image = event.image;
    }

    eventCollection.update({_id:event.eventId,"createdBy.userId":event.creatorId},{$set:updateContent},function(error,result){
        var flag;
        if (error) {
            logger.info('An error occurred while updating event '+error);
            callback(error,false);
        }else
        if (result == 0) {
            flag = false;
            callback(error,flag);
        }
        else{
            flag = true;
            callback(error,flag);
        }
    })
}

EventDataAccess.prototype.deleteEvent = function(creatorId,eventId,callback){

    eventCollection.remove({_id:eventId,"createdBy.userId":creatorId},function(error,result){

        var flag;
        if (error) {
            logger.info('An error occurred while deleting event '+error);
            callback(error,false);
        }else
        if (result == 0) {
            flag = false;
            callback(error,flag);
        }
        else{
            flag = true;
            callback(error,flag);
        }

    })
};

EventDataAccess.prototype.removeUserEvents = function(userId,callback){
    eventCollection.remove({"createdBy.userId":userId},function(error,result){
        if(error){
            callback(false,'onDeleteEvents',error,result);
        }
        else callback(true,'onDeleteEvents',error,result);
    })
};

/***** Functions on count *******/

EventDataAccess.prototype.getEventsCountPostedByUser = function(userId,callback){

    eventCollection.find({"createdBy.userId":userId},function(error,events){
        if(checkRequired(events) && events.length >0){
            var eventsCount = events.length;
            callback(""+eventsCount);
        }
        else callback('0')
    })
};

EventDataAccess.prototype.getUserSubscribeEventsCount = function(userId,callback){
    subscribeToEventsCollection.findOne({userId:userId},function(error,SDetails){
       if(checkRequired(SDetails)){
           if(checkRequired(SDetails.eventsList)){
                var eventsCount = SDetails.eventsList.length;
               callback(""+eventsCount)
           }else callback('0')
       } else callback('0')
    })
};



/**** Functions on Subscribe to events ****/

function constructSubscribeToEventsObject(subscribeDetails){
    var date = new Date();


    var subscribeToEventsDetails = new subscribeToEventsCollection({
        userId:subscribeDetails.userId,
        createdDate:date,
        lastUpdated:date,
        eventTypes:subscribeDetails.eventTypes,
        locations:subscribeDetails.locations,
        eventsList:[]

    });

    return subscribeToEventsDetails;
}

EventDataAccess.prototype.createSubscribeToEvents = function(subscribeDetails,callback){

    var subscribeToEventsDetails = constructSubscribeToEventsObject(subscribeDetails)

    subscribeToEventsDetails.save(function(err,SDetails){
        if(err){
            logger.info('Creating subscribeToEvents failed '+err)
            callback(err,null);
        }
        else{
            logger.info('Creating subscribeToEvents success ')
            callback(err,SDetails);

        }
    })
};

EventDataAccess.prototype.getUserSubscribeToEventsDetails = function(userId,callback){
    subscribeToEventsCollection.findOne({userId:userId},function(error,SDetails){
        if(error){
            logger.info("Mongo error while searching user subscribe to events details "+error);
            callback(error,false);
        }
        else{
            callback(error,SDetails);
        }
    })
};

EventDataAccess.prototype.removeUserSubscribeDetails = function(userId,callback){
    subscribeToEventsCollection.findOneAndRemove({userId:userId},function(error,result){
        if(error){
            callback(false,'onDeleteSubscribeEvents',error,0);
        }
        else callback(true,'onDeleteSubscribeEvents',error,1);
    })
};

EventDataAccess.prototype.getSubscribedEventTypeAndLocation = function(userId,callback){
    var total = {
        eventTypes:[],
        locations:[]
    }
    subscribeToEventsCollection.findOne({userId:userId},function(error,SDetails){
        if(error){
            logger.info("Mongo error while searching user subscribe to events details "+error);
            callback(error,false);
        }
        else{
            if(checkRequired(SDetails)){
                if(checkRequired(SDetails.eventTypes[0]) && checkRequired(SDetails.locations)){
                       for(var i=0; i<SDetails.eventTypes.length;i++){
                           if(SDetails.eventTypes[i].status == true){
                               total.eventTypes.push(SDetails.eventTypes[i])
                           }
                       }
                    for(var j=0; j<SDetails.locations.length;j++){
                        if(SDetails.locations[j].status == true){
                            if(SDetails.locations[j].location == 'All'){
                                total.all = SDetails.locations[j].status;
                            }
                            total.locations.push(SDetails.locations[j])
                        }
                    }
                    callback(error,total);
                }
                else  callback(error,false);
            }
            else  callback(error,false);
        }
    })
}

EventDataAccess.prototype.updateSubscribeToEvents = function(subscribeDetails,callback){
    var date = new Date();

    subscribeToEventsCollection.update({userId:subscribeDetails.userId},{locations:subscribeDetails.locations,eventTypes:subscribeDetails.eventTypes,lastUpdated:date},function(error,result){
        var flag;
        if (error) {
            logger.info('Error in DB while updating subscribeToEvents '+error)
            callback(error,false);
        }
        if (result == 0) {
            logger.info('subscribeToEvents update failed')
            flag = false;
            callback(error,flag);
        }
        else{
            logger.info('subscribeToEvents successfully updated')
            flag = true;
            callback(error,flag);
        }
    })
};

EventDataAccess.prototype.updateSubscribeAccWithEventList = function(userId,e,callback){
    var eData = {
        eventId:e._id,
        googleEvent:false,
        sequenceCount:1
    }
    subscribeToEventsCollection.findOne({userId:userId},function(error,subscribeDetails){
        if(error){
            logger.info('Updating subscribe acc with event list failed '+error)
            callback(error,false,null);
        }
        else if(checkRequired(subscribeDetails)){
            var isExist = false;
            if(checkRequired(subscribeDetails.eventsList)){
                if(subscribeDetails.eventsList[0]){
                    var eventsList = subscribeDetails.eventsList;
                    var eventId = eData.eventId;
                    for(var i=0; i<eventsList.length; i++){
                        if(eventsList[i].eventId == eventId){
                            isExist = true;
                        }
                    }
                }
            }

            if(isExist){
                //logger.info('Updating subscribe acc with event list failed')
                callback(error,'exist',e);
            }
            else{

                subscribeToEventsCollection.update({userId:userId},{ $push: { eventsList: eData } },function(error,result){
                    var flag;
                    if (error) {
                        logger.info('Error in DB while updating subscribe acc with event list '+error)
                        callback(error,false,null);
                    }
                    if (result == 1) {
                        flag = true;
                       // logger.info('Updating subscribe acc with event list success')
                        callback(error,flag,e);
                    }
                    else{
                        flag = false;
                        logger.info('Updating subscribe acc with event list failed')
                        callback(error,flag,null);
                    }
                });
            }
        }
        else{
            logger.info('Updating subscribe acc with event list failed (subscribeDetails not found) ')
            callback(error,false,null);
        }
    });
}

EventDataAccess.prototype.getSingleEventFromSubscribedList = function(userId,e,callback){
    subscribeToEventsCollection.findOne({userId: userId,"eventsList.eventId": e._id},{"eventsList.$":1},function(error,eList){
        if(error){
            logger.info('Searching event in subscribe acc failed '+error)
            callback(error,eList,e);
        }
        else  callback(error,eList,e);
    })
}

EventDataAccess.prototype.updateSingleEventInSubscribedLisWithGoogleEventId = function(userId,eId,googleEventId,count,confirmed){
    subscribeToEventsCollection.update({userId:userId,"eventsList.eventId": eId},{"eventsList.$.googleEventId":googleEventId,"eventsList.$.googleEvent":true,"eventsList.$.isDeleted":false,"eventsList.$.sequenceCount":count,"eventsList.$.confirmed":confirmed},function(error,result){
        if(error){
            logger.info('Updating google eventId in subscribe list failed  mongo error '+error)

        }
    })
}

EventDataAccess.prototype.updateSingleEventInSubscribedLisWithGoogleEventDeleted = function(userId,eId,callback){

    subscribeToEventsCollection.update({userId:userId,"eventsList.eventId": eId},{"eventsList.$.isDeleted":true,"eventsList.$.confirmed":false},function(error,result){
        var flag;

        if (error) {
            logger.info('Error in DB while updating subscribe acc with event delete flag '+error);
            callback(error,false);
        }
        if (result == 1) {
            flag = true;
            // logger.info('Updating subscribe acc with event list success')
            callback(error,flag);
        }
        else{
            flag = false;
            logger.info('Updating subscribe acc with event delete flag failed');
            callback(error,flag);
        }

    })
}

EventDataAccess.prototype.getSubscribedUsersByEventTypeAndAllLocations = function(eType,callback){

    subscribeToEventsCollection.find({ eventTypes:{$elemMatch:{eventType:eType,status:true}},locations:{$elemMatch:{location:"All",status:true}}},function(error,eList){
        if(error){
            logger.info('Searching event in subscribe acc failed '+error)
            callback(error,eList);
        }
        else  callback(error,eList);
    })
}

EventDataAccess.prototype.getSubscribedUsersByEventTypeAndLocation = function(eType,eLocation,callback){
    subscribeToEventsCollection.find({ eventTypes:{$elemMatch:{eventType:eType,status:true}},locations:{$elemMatch:{location:eLocation,status:true},$elemMatch:{location:"All",status:false}}},function(error,eList){
        if(error){
            logger.info('Searching event in subscribe acc failed '+error)
            callback(error,eList);
        }
        else  callback(error,eList);
    })
}

EventDataAccess.prototype.getAllUsersAddedThisEvent = function(eventId,callback){
    subscribeToEventsCollection.find({"eventsList.eventId": eventId},function(error,list){
        if(error){
            logger.info('Searching users with event in subscribe acc failed '+error)
            callback(error,list);
        }
        else  callback(error,list);
    })
}

EventDataAccess.prototype.getAllUsersConfirmedThisEvent = function(eventId,callback){

    subscribeToEventsCollection.find({ eventsList:{$elemMatch:{eventId:eventId,confirmed:true}}},{userId:1},function(error,uList){
        if(error){
            logger.info('Searching event in subscribe acc failed '+error)
            callback(error,uList);
        }
        else  callback(error,uList);
    })
}

EventDataAccess.prototype.removeEventFromUserSubscribeList = function(userId,eventId,callback){
    subscribeToEventsCollection.update({userId:userId},{$pull:{eventsList:{eventId:eventId}}},function(error,result){
        if(result == 1){
            callback(error,true);
            logger.info('Removing event from user subscribing list success ');
        }
        else{
            callback(error,false)
            logger.info('Removing event from user subscribing list failed '+error);
        }
    })
}

EventDataAccess.prototype.getNonConfirmedEvents = function(userId,callback){
    subscribeToEventsCollection.findOne({userId:userId,"eventsList.confirmed":false},{eventsList:1},function(error,result){

        if(checkRequired(result)){
          var eventIdArr = [];
          if(result.eventsList.length > 0){
              for(var i=0; i<result.eventsList.length; i++){
                  if(!result.eventsList[i].confirmed){
                      eventIdArr.push(result.eventsList[i].eventId);
                  }
              }
              if(eventIdArr.length > 0){
                  upComingEventListById(eventIdArr,callback);
              }else callback(error,null)
          }else callback(error,null)
       }else callback(error,null)
    })
};

EventDataAccess.prototype.getNonConfirmedEventsAndCount = function(userId,dateMin,callback){
    subscribeToEventsCollection.aggregate(
        [{
            $match:{userId:userId}
        },
        {
            $unwind:"$eventsList"
        },
        {
            $match:{"eventsList.confirmed":false}
        },
        {
            $group:{
                _id:null,
                eventArr:{$addToSet:"$eventsList.eventId"}
            }
        }]
    ).exec(function(error,data){

            if(checkRequired(data) && data.length > 0 && data[0] && data[0].eventArr && data[0].eventArr.length > 0){
                eventCollection.populate(data[0],{path:'eventArr',match:{startDateTime:{$gte:dateMin}},select:'startDateTime eventName createdBy'},function(err,list){

                    if(checkRequired(list) && list.eventArr && list.eventArr.length > 0){
                        callback(list.eventArr);
                    }else callback([])

                });
            }else
            callback([])
        })
};

EventDataAccess.prototype.getAllSubscribedEvents = function(userId,start,end,callback){
    subscribeToEventsCollection.aggregate([
        {
            $match: {userId:userId}
        },
        {
            $group: {
                _id: null, "list": {$addToSet:"$eventsList.eventId"}
            }
        }
    ]).exec(function ( e, d ) {
            if(e){
                callback([])
            }else{
                if(d.length > 0){
                    if(d[0].list.length > 0 && checkRequired(d[0].list[0]) && d[0].list[0].length > 0){
                        eventListByIdAndDate(d[0].list[0],start,end,callback)
                    }else callback([]);
                }else callback([]);
            }
        });
};

EventDataAccess.prototype.getAllSubscribedEventsMatched = function(userId,eventsIdArr,callback){
    subscribeToEventsCollection.aggregate([
        {
            $match: {userId:userId}
        },
        {
            $project:{
                eventsList:1
            }
        },
        {
            $unwind:"$eventsList"

        },
        {
            $match:{
                "eventsList.eventId":{$in:eventsIdArr}
            }
        },
        {
            $project:{
                eventId:"$eventsList.eventId",
                event:"$eventsList.eventId",
                accepted:{$cond: [ {$eq:["$eventsList.confirmed",true]}, true, false ]}
            }
        }
    ]).exec(function ( e, d ) {
            if(e){
                callback([])
            }
            else{
                if(checkRequired(d) && d.length > 0 && checkRequired(d[0])){
                    eventCollection.populate(d,{path:'event', select:'_id eventName speakerDesignation'},function(err,obj){
                        if(!err && obj){
                            callback(obj)
                        }
                        else callback(d)
                    })
                }else callback([]);
            }
        });
};

function eventListByIdAndDate(arr,start,end,back){
    var date = new Date();
    var today = new Date();

    date.setHours(0,0,0,0);
    today.setHours(0,0,0,0);
    today.setDate(today.getDate() + start);
    date.setDate(date.getDate() - end);

    eventCollection.find( {"_id": { "$in":arr },startDateTime:{$gte:date,$lte:today}},function(error,eventList){
        back(eventList);
    })
}


EventDataAccess.prototype.getAllSubscribedEventsDate = function(userId,dateMin,dateMax,projection,callback){
    subscribeToEventsCollection.aggregate([
        {
            $match: {userId:userId}
        },
        {
            $project:{
                eventsList:1
            }
        },
        {
            $unwind:"$eventsList"

        },
        {
            $group: {
                _id: null,
                "all":{
                    $addToSet:{
                        $cond:{if:{$eq:["$eventsList.confirmed",true]},then:"$eventsList.eventId",else:null}
                    }
                },
                "list": {$addToSet:"$eventsList.eventId"}
            }
        }
    ]).exec(function ( e, d ) {
            if(e){
                callback([])
            }
            else{
                if(checkRequired(d) && d.length > 0 && checkRequired(d[0].list) && d[0].list.length > 0){
                    eventCollection.find( {"_id": { "$in":d[0].list },startDateTime:{$gte:new Date(dateMin),$lte:new Date(dateMax)}},projection,function(error,eventList){
                        if(!error && eventList.length > 0){
                            if(d[0].all && d[0].all.length > 0){
                                for(var e=0; e<eventList.length; e++){
                                    if(d[0].all.indexOf(eventList[e]._id) != -1){
                                        eventList[e].accepted = true;
                                    }
                                }
                            }
                            callback(eventList);
                        }else callback([])
                    })
                }else callback([]);
            }
        });
};


EventDataAccess.prototype.getAllConfirmedEvents = function(userId,dateStart,dateEnd,callback){
    subscribeToEventsCollection.aggregate([
        {$match: {userId:userId}},
        {$unwind: '$eventsList'},
        {$match: {"eventsList.confirmed": true}},
        {$group: {_id: "$userId", "list": {$push:"$eventsList.eventId"}}}
    ]).exec(function ( e, d ) {
            if(e){
                callback([])
            }else{
                if(d.length > 0){
                    if(d[0].list.length > 0){
                        getEventsByDatesArrayBetween(d[0].list,dateStart,dateEnd,callback);
                    }else callback([]);
                }else callback([]);
            }
        });
};

EventDataAccess.prototype.getResponsePendingEventByDateForUser = function(userId,dayStart,dayEnd,callback){
    eventCollection.find({
        startDateTime:{$gt:new Date(dayStart)},
        endDateTime:{$lt:new Date(dayEnd)},
        "createdBy.userId": userId,
        eventName:"Response Pending Mails"
    }).exec(function(error, result){
        callback(error,result);
    });
};

function getEventsByDatesArrayBetween(eventIdArr,dateStart,dateEnd,callback){
    var mainQuery = [];

    for(var dates=0; dates < dateStart.length; dates++){
        var query = {
                    "startDateTime": {
                        '$gte':dateStart[dates],'$lte':dateEnd[dates]
                    }
        }
        mainQuery.push(query);
    }

    eventCollection.find(
        {
            _id:{$in:eventIdArr},
            $or:mainQuery
        },
        function(error,result){

            if(error){
                callback([])
            }else{
                callback(result)
            }
        }
    )
};

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}


function removeDuplicates(arr){

    var end = arr.length;

    for(var i = 0; i < end; i++)
    {
        for(var j = i + 1; j < end; j++)
        {
            if(arr[i].eventLocation == arr[j].eventLocation)
            {
                var shiftLeft = j;
                for(var k = j+1; k < end; k++, shiftLeft++)
                {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

// export the EventDataAccess Class
module.exports = EventDataAccess;