//**************************************************************************************
// File Name            : documentTemplate
// Functionality        : Router for documentTemplates(Quotation/Purchase Order/Invoice)
// History              : First Version
//
//
//**************************************************************************************

var documentTemplateCollection = require('../databaseSchema/documentTemplateSchema').documentTemplates;
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');

var common = new commonUtility();
var logLib = new winstonLog();
var loggerError = logLib.getCorporateErrorLogger();
var logger = logLib.getCorporateLogger();


function documentTemplateManagement() {
}

documentTemplateManagement.prototype.checkAndCreateDocumentTemplate = function (template, callback) {
    var parentScope = this;

    this.getTemplateByName(template, function(error, savedTemplate) {
        if(savedTemplate) {
            callback(error, savedTemplate);
        } else {

            createNewDocTemplate(template, function(error, savedTemplate) {
                callback(error, savedTemplate);
            })
        }
    })
    
};

var createNewDocTemplate = function(template,callback) {
    var newDocTemplateObj = createTemplateObj(template);

    newDocTemplateObj.save(function(error, savedTemplate) {
        if(error) {
            logger.info('Error in createNewDocTemplate(): documentTemplateManagement', error);
        }
        callback(error, savedTemplate);
    })
}

documentTemplateManagement.prototype.getTemplateByName = function(template, callback) {
    documentTemplateCollection.findOne({
        companyId: template.companyId,
        documentTemplateType:template.documentTemplateType,
        documentTemplateName:template.documentTemplateName },
        {},function(error,result){
            if(error){
                logger.info('Error in getTemplateByName(): documentTemplateManagement', error);
            } else {
                console.log("Sending the result",result);
                callback(error,result)
            }
    })
    
}

var createTemplateObj = function(template) {
    return new documentTemplateCollection({
        companyId: template.companyId,
        documentTemplateName: template.documentTemplateName,
        documentTemplateType: template.documentTemplateType,
        createdDate: new Date(),
        documentTemplateUiId:template.documentTemplateUiId,
        docTemplateElementList:template.docTemplateElementList,
        docTemplateAttrList:template.docTemplateAttrList
    });
}

documentTemplateManagement.prototype.constructDocTemplateObject = function (docTemplateDetails) {

    var attributesList = [];
    var tableList = [];
    var docTemplObj = new documentTemplateCollection({
        companyId: docTemplateDetails.companyId,
        documentTemplateName: docTemplateDetails.documentTemplateName,
        documentTemplateType: docTemplateDetails.documentTemplateType,
        documentTemplateDOM: docTemplateDetails.documentTemplateDOM,
        docTemplAttributeList: docTemplateDetails.docTemplAttributeList,
        docTemplTableList: docTemplateDetails.docTemplTableList,
        createdDate: new Date()
    });
    return corporateUserObj;
}

documentTemplateManagement.prototype.getAllDocumentTemplateTypes = function (companyId, callback) {

    documentTemplateCollection.aggregate([
        {
            $match: {
                companyId:companyId
            }
        },
        {
            $group: {
                _id:"$documentTemplateType",
                count:{$sum:1 }
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}

documentTemplateManagement.prototype.getAllDocumentTemplateNamesByType = function (companyId, docTemplateType, callback) {

     documentTemplateCollection.find({companyId:companyId,
                                        documentTemplateType:docTemplateType,
                                        isDocTemplateDeactivated:false,
                                        },
                                    { _id:1,
                                        documentTemplateName:1 },function(err,result){
        if(!err && result){
            callback(err,result)
        } else {
            callback(err,false)
        }
    })
}

documentTemplateManagement.prototype.getAllDocumentTemplates = function (companyId, callback) {

    documentTemplateCollection.find({companyId:companyId},
                                    {},function(err,result){
        if(!err && result){
            callback(err,result)
        } else {
            result = docTemplateObj;
            callback(err,result)
        }
    })
}


documentTemplateManagement.prototype.getDocumentTemplateByTypeAndName = function (companyId,templateType, templateName, callback) {

    documentTemplateCollection.find({companyId:companyId,
                                        documentTemplateType:templateType,
                                        documentTemplateName:templateName},
        {},function(err,result){
            if(!err && result){
                callback(err,result)
            } else {
                // result = docTemplateObj;
                if(callback){
                    callback(err,result)
                }
            }
        })
}

documentTemplateManagement.prototype.getAllDocumentTemplateById = function (companyId,templateId, callback) {

    documentTemplateCollection.find({companyId:companyId,
                                        _id:templateId},
        {},function(err,result){
            if(!err && result){
                callback(err,result)
            } else {
                result = docTemplateObj;
                callback(err,result)
            }
        })
}

documentTemplateManagement.prototype.incrementDocumentCreatedCount = function (companyId,templateId, documentCount, callback) {

    documentTemplateCollection.update({companyId:companyId,
                                        _id:templateId},
        {$set:{numberOfDocumentsCreated: documentCount}},{upsert:true},function(err,result) {
            callback(err, result);
        });
}

documentTemplateManagement.prototype.getAllAttributeNamesForDocTemplateType = function (companyId, docTemplateType, docTemplateName, callback) {
    
}

documentTemplateManagement.prototype.updateDocumentTemplate = function (companyId,
                                                                        docTemplateType,
                                                                        docTemplateName,
                                                                        docTemplateDOM,
                                                                        isActivated,
                                                                        attributeList,
                                                                        tableList,
                                                                        callback) {
    documentTemplateCollection.update({companyId: companyId,
                                        documentTemplateName: docTemplateName,
                                        documentTemplateType: docTemplateType},
                                {$set:{createdDate: new Date(),
                                        documentTemplateName: docTemplateName,
                                        documentTemplateType: docTemplateType,
                                        documentTemplateDOM: docTemplateDOM,
                                        isDocTemplateDeactivated: isActivated,
                                        isDocCreated: false,
                                        docTemplateAttrList: attributeList,
                                        docTemplateTableList: tableList
                                }},{upsert:true},function (err,result) {
                                        callback(err, result);
                                    });

}

documentTemplateManagement.prototype.updateDocumentTemplateElements = function (companyId,
                                                                        docTemplateType,
                                                                        docTemplateName,
                                                                        elementList,
                                                                        uiId,
                                                                        isdefault,
                                                                        versionControlled,
                                                                        attributeList,
                                                                        callback) {

    documentTemplateCollection.update({companyId: companyId,
            documentTemplateName: docTemplateName,
            documentTemplateType: docTemplateType},
        {$set:{
                documentTemplateName: docTemplateName,
                documentTemplateType: docTemplateType,
                isDocumentCreated: false,
                isDefaultTemplate:isdefault,
                isTemplateVersionControlled:versionControlled,
                docTemplateElementList: elementList,
                documentTemplateUiId: uiId,
                docTemplateAttrList: attributeList
            }},{upsert:true},function (err,result) {
            callback(err, result);
        });

}

documentTemplateManagement.prototype.updateDocumentTemplate = function (companyId,
                                                                        docTemplateType,
                                                                        docTemplateName,
                                                                        html,
                                                                        isActivated,
                                                                        callback) {
    documentTemplateCollection.update({companyId: companyId,
            documentTemplateName: docTemplateName,
            documentTemplateType: docTemplateType},
        {$set:{createdDate: new Date(),
                documentTemplateName: docTemplateName,
                documentTemplateType: docTemplateType,
                isDocTemplateDeactivated: isActivated,
                isDocCreated: false,
                documentTemplateDOM: html,
            }},{upsert:true},function (err,result) {
            callback(err, result);
        });

}

documentTemplateManagement.prototype.updateIsTemplateDeactivated = function (companyId,
                                                                        docType,
                                                                        templateId,
                                                                        isDeActivated,
                                                                        callback) {

    if (isDeActivated == true){
        documentTemplateCollection.update({companyId: companyId,
                documentTemplateType: docType,
                _id: templateId },
            {$set:{
                    isDocTemplateDeactivated: isDeActivated,
                    isDefaultTemplate: false
                }},{upsert:true},function (err,result) {
                callback(err, result);
            });
    }else {
        documentTemplateCollection.update({companyId: companyId,
                documentTemplateType: docType,
                _id: templateId },
            {$set:{
                    isDocTemplateDeactivated: isDeActivated
                }},{upsert:true},function (err,result) {
                callback(err, result);
            });
    }
}

documentTemplateManagement.prototype.setDocumentTemplateAsDefault = function (companyId,
                                                                             docTemplateType,
                                                                             templateId,
                                                                             isDefault,
                                                                             callback) {

    documentTemplateCollection.update({companyId: companyId,
            documentTemplateType: docTemplateType,
            _id: {$ne:templateId}
            },
        {$set:{
                isDefaultTemplate: false
            }},{multi:true},function (err,result) {
            callback(err, result);
        });

    documentTemplateCollection.update({companyId: companyId,
            documentTemplateType: docTemplateType,
            _id: templateId },
        {$set:{
                isDefaultTemplate: true
            }},{upsert:true},function (err,result) {
            callback(err, result);
        });
}

documentTemplateManagement.prototype.setDocumentTemplateAsVersionControlled = function (companyId,
                                                                              docTemplateType,
                                                                              templateId,
                                                                              isVersionControlled,
                                                                              callback) {

    documentTemplateCollection.update({companyId: companyId,
            documentTemplateType: docTemplateType,
            _id: templateId },
        {$set:{
                isTemplateVersionControlled: isVersionControlled
            }},{upsert:true},function (err,result) {
            callback(err, result);
        });
}

documentTemplateManagement.prototype.updateIsDocumentsCreated = function (companyId,
                                                                            templateId,
                                                                            callback) {

        documentTemplateCollection.update({companyId: companyId,
                _id: templateId },
            {$set:{
                    isDocumentCreated: false
                }},{upsert:true},function (err,result) {
                callback(err, result);
            });

}

documentTemplateManagement.prototype.deleteMultipleDocumentTemplates = function(companyId, documentTemplateIds, callback) {
    documentTemplateCollection.remove({companyId:companyId, _id:{$in:documentTemplateIds}}, function(error, result) {
        callback(error, result);
    })
}

module.exports = documentTemplateManagement;