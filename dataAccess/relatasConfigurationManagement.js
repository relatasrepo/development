
var relatasConfigCollection = require('../databaseSchema/relatasConfigurationSchema').relatasConfig;
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');

var common = new commonUtility();
var logLib = new winstonLog();
var loggerError = logLib.getCorporateErrorLogger();
var logger = logLib.getCorporateLogger();

function relatasConfigurationManagement() {
}

relatasConfigurationManagement.prototype.getLatestMobileAppVersion = function (callback) {
    relatasConfigCollection.findOne({}, {latestMobileAppVersion:1}, function(err, result) {
        callback(err, result);
    });
};

module.exports = relatasConfigurationManagement;