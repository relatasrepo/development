var algoSeedValue = require('../databaseSchema/userManagementSchema').algoSeedValues;
var losingTouch = require('../databaseSchema/userManagementSchema').losingTouch;
var interaction = require('../databaseSchema/userManagementSchema').interaction;
var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var insightsActionTaken = require('../databaseSchema/userManagementSchema').insightsActionTaken;
var InteractionsCollection = require('../databaseSchema/userManagementSchema').interactions

var commonUtility = require('../common/commonUtility')
var moment = require("moment");
var async = require("async");
var mongoose = require('mongoose');
var _ = require("lodash");
var momentTZ = require('moment-timezone');
var ruleEngine = require('node-rules');

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var common = new commonUtility()

var logger = logLib.getWinston();
var loggerError = logLib.getWinstonError();

/*Rules Engine*/
var rules = [{
    "priority": 16,
    "on": true,
    "condition": function(R) {
        var ptrn = new RegExp(this.contactLocation, 'i');
        R.when(this.contactLocation && ptrn.test(this.myLocationNextWeek) && this.myLocationNextWeek != null && this.myLocationNextWeek != undefined && this.myLocationNextWeek != '');
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.contactName + ' is in ' + this.contactLocation + '.\n You are in ' + this.contactLocation + ' next week.\n Schedule Meeting.'
        R.stop();
    }
}, {
    "priority": 3,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'tweet' && this.lastInteractionHours < 48);
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.contactName + ' just tweeted, Tweet back.'
        R.stop();
    }
}, {
    "priority": 9,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionHours == null || this.lastInteractionDate == undefined || this.lastInteractionDate == null || this.lastInteractionDays == null);
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = 'You have no interaction with ' + this.contactName + ' for over 60 days.\n Connect through mail.';
        R.stop();
    }
}, {
    "priority": 2,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'tweet' && this.lastInteractionHours >= 48);
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.contactName + ' tweeted on ' + this.lastInteracted + '. Tweet back.';
        R.stop();
    }
}, {
    "priority": 4,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'meeting');
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.lastInteractionDays + ' days since you last met ' + this.contactName + '.\n Schedule a meeting.';
        R.stop();
    }
}, {
    "priority": 5,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'email' && this.action === 'receiver');
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.lastInteractionDays + ' days since you last mailed ' + this.contactName + '.\n Send a new mail.';
        R.stop();
    }
}, {
    "priority": 10,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'email' && this.action === 'sender');
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.lastInteractionDays + ' days since ' + this.contactName + ' last mailed you.\n Send a new mail.';
        R.stop();
    }
}, {
    "priority": 6,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'sms');
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.lastInteractionDays + ' days since you last interacted via SMS with ' + this.contactName + '.\n Send a new SMS.';
        R.stop();
    }
}, {
    "priority": 7,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'task');
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.lastInteractionDays + ' days since you last interacted via Task with ' + this.contactName + '.\n Assign a new Task.';
        R.stop();
    }
}, {
    "priority": 8,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'meeting-comment');
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.lastInteractionDays + ' days since you last met ' + this.contactName + '.\n Schedule a meeting.';
        R.stop();
    }
}, {
    "priority": 13,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'linkedin');
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.lastInteractionDays + ' days since you last interacted with ' + this.contactName + '. on Linkedin. \n Connect now.';
        R.stop();
    }
}, {
    "priority": 11,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'facebook');
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.lastInteractionDays + ' days since you last interacted with ' + this.contactName + '. on Facebook. \n Connect now.';
        R.stop();
    }
}, {
    "priority": 14,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'call');
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.lastInteractionDays + ' days since you last called ' + this.contactName + '. \n Call now.';
        R.stop();
    }
}, {
    "priority": 15,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'twitter' && this.lastInteractionHours >= 48);
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.contactName + ' tweeted on ' + this.lastInteracted + '. \n Tweet back.';
        R.stop();
    }
}, {
    "priority": 18,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'message' && this.lastInteractionHours >= 48);
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.contactName + ' messaged you on ' + this.lastInteracted + '. \n Message back.';
        R.stop();
    }
}, {
    "priority": 19,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'document-share' && this.lastInteractionHours >= 48);
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.contactName + ' and you shared documents on ' + this.lastInteracted + '. \n Follow up.';
        R.stop();
    }
}, {
    "priority": 20,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'calendar-password' && this.lastInteractionHours >= 48);
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.contactName + ' Calendar Password set on' + this.lastInteracted + '. \n Follow up.';
        R.stop();
    }
}, {
    "priority": 21,
    "on": true,
    "condition": function(R) {
        R.when(this.lastInteractionType === 'google-meeting' && this.lastInteractionHours >= 48);
    },
    "consequence": function(R) {
        this.result = false;
        this.suggestion = this.contactName + ' met you on' + this.lastInteracted + '. \n Follow up.';
        R.stop();
    }
}];

var ruleObj = new ruleEngine();
ruleObj.register(rules);

/* Json conversion*/
var R1 = new ruleEngine(rules);
var store = R1.toJSON();

var R2 = new ruleEngine();
R2.fromJSON(store)

function insightsManagement() {

    this.getLosingTouchSelf = function(hierarchyList, docName, companyMembers, limit, callback) {
        algoSeedValue.findOne({ documentName: docName }, { thresholdSelf: 1 }, function(err, data) {

            if(!err && data){
                getLosingTouchContacts(hierarchyList, data.thresholdSelf, companyMembers, limit, callback);
            } else {
                if(callback){
                    callback(null,null)
                }
            }
        });
    };

    this.getLosingTouchFullDetails = function(insightsFor, threshold, limit, callback) {
        getLosingTouchAccountValuesAll(insightsFor, threshold, limit, function(result) {
            callback(result)
        })
    }

    this.getLosingTouchAggregatedNumbers = function(insightsFor, callback) {
        getLosingTouchAggregatedNumbers(insightsFor, function(aggRes) {
            callback(aggRes)
        })
    }

    this.getLosingTouchForUsersContacts = function(userIds,userContactGroup,callback){
        getLosingTouchForUsersContacts(userIds,userContactGroup,callback)
    };

    this.getLosingTouchForTeam = function(teamMembers, callback) {
        getLosingTouchForTeam(teamMembers, callback)
    }

    this.getRules = function(rulesFor, callback) {
        getSuggestion(rulesFor, callback)
    };

    this.ignoreContact = function(userId, contactId, contactEmailId, ignoreFor, insightsSection, callback) {

        //if (insightsSection != 'losingTouch') {
            myUserCollection.update({ _id: userId, "contacts._id": contactId }, { "$set": { "contacts.$.account.ignore": ignoreFor } }, { upsert: true }, function(error, result) {
                if (error) {
                    callback(error, false);
                } else if (result) {
                    callback(error, true);
                }
            });
        //}

        if (insightsSection === 'losingTouch') {
            losingTouch.update({ userId: userId, "contacts.personEmailId": contactEmailId }, { "$set": { "contacts.$.actionTaken": true } }, { upsert: true }, function(error, result) {
                if (error) {
                    callback(error, false);
                } else if (result) {
                    callback(error, true);
                }
            });
        }
    };

    this.insightsAction = function(user, actionTaken, callback) {
        var uid = String(user[0]._id);
        var insightsAction = new insightsActionTaken({
            userId: uid,
            userEmailId: user[0].emailId,
            createdDate: new Date(),
            contact: {
                userId: actionTaken.contact.userId,
                name: actionTaken.contact.name,
                personEmailId: actionTaken.contact.personEmailId,
                mobileNumber: actionTaken.contact.mobileNumber,
            },
            actionType: actionTaken.actionType,
            suggestion: actionTaken.suggestion,
            insightSource: actionTaken.insightSource,
            suggestionIndexValue: actionTaken.suggestionIndexValue,
            self_hierarchy: actionTaken.self_hierarchy
        })

        insightsAction.save(function(err, insightsAction) {
            callback(err, insightsAction);
        });
    }

    this.companyRelationRisk = function(userIds, detailsFor,usersCompany,callback) {
        getCompanyRelationshipRisk(userIds, 'graph', detailsFor, 0,usersCompany, callback);
    }

    this.companyRelationRiskCompany = function(userIds,usersCompany,callback) {
        getCompanyRelationshipRisk(userIds, 'graph', 'company', 0,usersCompany, function(err, res) {
            if (res)
                callback(err, { value: res.value, interaction: res.intraction })
            else
                callback(err, null)
        });
    }

    this.companyRelationRiskDetailSelf = function(userIds, limit,usersCompany, callback) {

        getCompanyRelationshipRisk(userIds, 'detail', 'selfLimit', limit, usersCompany,function(err, result) {
            if(err || !result){
                callback(err,[])
            } else {
                getCompanyRelationshipRiskDetailSelf(err, userIds, result,usersCompany, callback);
            }
        });
    }

    this.companyRelationRiskDetailHierarchy = function(userIds, limit, callback) {

        getCompanyRelationshipRisk(userIds, 'detail', 'hierarchy', limit, function(err, result) {
            if (result)
                getCompanyRelationshipRiskDetailHierarchy(err, result, callback);
            else
                callback(err, null);
        });
    }

    this.getInteractionGrowth = function(hierarchy, getFor, explain, companyMembers, callback) {

        var getTopTenCompaniesAtRisk = false;

        getInteractionGrowth(hierarchy, getFor, explain, function(topTenCompaniesAtRisk, totalContactValue, valueAtRisk) {

            getCompanyRevenueRisk(companyMembers, getTopTenCompaniesAtRisk, function(companyContactValue, companyValueAtRisk) {
                var companyRevenueRisk = {
                    companyContactValue: companyContactValue,
                    companyValueAtRisk: companyValueAtRisk
                }
                callback(topTenCompaniesAtRisk, totalContactValue, valueAtRisk, companyRevenueRisk)
            });
        })
    }

    this.getRevenueRiskDetails = function(hierarchy, getFor, explain, callback) {
        getInteractionGrowth(hierarchy, getFor, explain, callback)
    }

    this.getRevenueRiskDetailsTest = function(hierarchy, getFor, callback) {

        var userCollectionQuery = [];
        if (getFor == 'self') {
            userCollectionQuery.push({
                $match: { _id: { $in: hierarchy } }
            }, {
                $unwind: "$contacts"
            }, {
                $match: {
                    "contacts.account.name": { $ne: null }
                }
            }, {
                $project: {
                    _id: "$_id",
                    ownerFullname: { $concat: ["$firstName", " ", "$lastName"] },
                    email: "$contacts.personEmailId",
                    contactCompanyName: "$contacts.account.name",
                    contactFullName: "$contacts.personName",
                    contactLastInteracted: "$contacts.lastInteracted",
                    contactId: "$contacts._id",
                    designation: "$contacts.designation",
                    personId: "$contacts.personId",
                    relatasUser: "$contacts.relatasUser",
                    contactValue: { $cond: { if: "$contacts.account.value.inProcess", then: "$contacts.account.value.inProcess", else: 0 } }
                }
            })
        } else if (getFor == 'team') {
            userCollectionQuery.push({
                $match: { _id: { $in: hierarchy } }
            }, {
                $unwind: "$contacts"
            }, {
                $match: {
                    "contacts.account.showToReportingManager": true,
                    "contacts.account.name": { $ne: null }
                }
            }, {
                $project: {
                    _id: "$_id",
                    ownerFullname: { $concat: ["$firstName", " ", "$lastName"] },
                    email: "$contacts.personEmailId",
                    contactCompanyName: "$contacts.account.name",
                    contactFullName: "$contacts.personName",
                    contactLastInteracted: "$contacts.lastInteracted",
                    contactId: "$contacts._id",
                    designation: "$contacts.designation",
                    personId: "$contacts.personId",
                    relatasUser: "$contacts.relatasUser",
                    contactValue: { $cond: { if: "$contacts.account.value.inProcess", then: "$contacts.account.value.inProcess", else: 0 } }
                }
            });
        }

        myUserCollection.aggregate(userCollectionQuery).exec(function(err, userData) {

            var emailIds = _.pluck(userData, 'email');
            var dateRange1 = {
                $gte: moment().subtract(60, "days").toDate(),
                $lte: moment().subtract(30, "days").toDate()
            }

            var dateRange2 = { $gte: moment().subtract(30, "days").toDate(), $lte: moment().subtract(0, "days").toDate() }

            interactionsByRange(hierarchy, emailIds, dateRange1, function(interactionsRange1) {
                interactionsByRange(hierarchy, emailIds, dateRange2, function(interactionsRange2) {

                    var previousMonth = {
                        interactions: interactionsRange1,
                        contacts: userData
                    }

                    var currentMonth = {
                        interactions: interactionsRange2,
                        contacts: userData
                    }
                    callback(previousMonth, currentMonth)
                })
            })
        }); //End my user
    }

    this.contactInProcess = function(userId, contact, value, contactId, callback) {
        if(contactId)
            var q = { _id: userId, "contacts._id": contactId };
        else
            var q = { _id: userId, "contacts.personEmailId": contact }
        myUserCollection.update(q, { "$set": { "contacts.$.account.value.inProcess": value } },  function(error, result) {
            if (error) {
                callback(error, false);
            } else if (result) {
                callback(error, true);
            }
        });
    }

    this.contactRelation = function(userId, contact, value, contactId, callback) {
        if(contactId)
            var q = { _id: userId, "contacts._id": contactId }
        else
            var q = { _id: userId, "contacts.personEmailId": contact }
        myUserCollection.update(q, { "$set": { "contacts.$.contactRelation.decisionmaker_influencer": value } }, function(error, result) {
            if (error) {
                callback(error, false);
            } else if (result) {
                callback(error, true);
            }
        });
    }

    this.getInsightsReminder = function(userId, callback) {
        myUserCollection.find({ _id: userId }, { insightsRemindIn: 1 }, function(err, results) {
            if (!err) {
                callback(results)
            } else {
                callback(err)
            }
        })
    }

    this.getTopTenInteractedContactsByCompany = function(userId, topTenCompanies, callback) {

        var q = {
            $and: [{
                "contacts.account.name": { $ne: null },
                $or: [
                    { "contacts.account.value.inProcess": { $in: [null, 0] } },
                    { "contacts.account.value.inProcess": { $exists: false } }
                ]
            }]
        }

        myUserCollection.aggregate([{
            $match: {
                _id: userId
            }
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }, {
            $project: {
                _id: "$_id",
                email: "$contacts.personEmailId",
                name: "$contacts.personName",
                contactId: "$contacts._id",
                contactRelation: "$contacts.contactRelation",
                company: "$contacts.account.name",
                designation: "$contacts.designation",
                inProcess: "$contacts.account.value.inProcess"
            }
        }]).exec(function(err, contacts) {

            var emailIds = _.pluck(contacts, 'email');

            var dateRange = { $gte: moment().subtract(60, "days").toDate() };

            getInteractionsByRange(userId, dateRange, emailIds, function(interactionsData) {

                var obj = _.map(contacts, function(a) {
                    return _.extend(a, _.find(interactionsData, function(b) {
                        return a.email === b.email;
                    }))
                });

                //Inefficient way to select only those companies which are shown on the Revenue
                //at risk graph on the Insights landing page. Session is set for the companies in the graph.

                var l = obj.length - 1;
                var o = [];

                for (var i = 0; i < l; i++) {
                    if (obj[i].interactionCount && obj[i].company) {
                        if (obj[i].company == obj[i + 1].company) {
                            if (obj[i].interactionCount > obj[i + 1].interactionCount) {
                                o.push(obj[i])
                            } else {
                                o.push(obj[i + 1])
                            }
                        } else {
                            o.push(obj[i])
                        }
                    }
                }

                var matches = [],
                    i, j;

                topTenCompanies = topTenCompanies.split(",");

                for (i = 0; i < o.length; i++)
                    if (topTenCompanies)
                        if (-1 != (j = topTenCompanies.indexOf(o[i].company)))
                            matches.push(o[i]);

                obj = _.uniq(matches, function(item) {
                    return item.company;
                });

                if (obj.length <= 0) {
                    obj = _.sortBy(o, function(o) {
                        return -o.interactionCount;
                    });

                    var objLen = obj.length

                    if (objLen > 9) {
                        obj.length = 10;
                    } else {
                        obj.length = objLen
                    }

                } else {
                    obj = _.sortBy(obj, function(o) {
                        return -o.interactionCount;
                    });

                    var objLen = obj.length

                    if (objLen > 9) {
                        obj.length = 10;
                    } else {
                        obj.length = objLen
                    }
                }
                callback(obj)
            });
        });
    };

    this.updateRemindInsights = function(userId, remind, callback) {
        myUserCollection.update({ _id: userId }, { "$set": { "insightsRemindIn": remind } }, { upsert: true }, function(error, result) {
            if (error) {
                callback(false);
            } else if (result) {
                callback(true);
            }
        });
    }

    this.peopleToConnect = function(userId, dateMin, dateMax, timezone, callback) {
        getRemindToConnect(userId, dateMin, dateMax, function(err, remind) {
            if (err)
                callback(err, null)
            else {
                // callback(null, remind);
                var peopleToConnect = [];
                if (remind && remind.length > 0)
                    remind.forEach(function(ele) {
                        peopleToConnect.push(ele);
                    })
                getNotifyInSevenDaysContacts(userId, dateMin, dateMax, timezone, function(error, notify) {
                    if (error)
                        callback(error, null)
                    else {
                        if (notify && notify.length > 0)
                            notify.forEach(function(ele) {
                                peopleToConnect.push(ele);
                            })
                        peopleToConnect.sort(function(a, b) {
                            return b.value - a.value;
                        })
                        callback(null, peopleToConnect)

                        // getActionTakenContacts(userId, dateMin, dateMax, 'peopleToConnect', null, function(err, action) {
                        //     if (action && action.length > 0) {
                        //         for (var i = 0; i < peopleToConnect.length; i++) {
                        //             action.forEach(function(ele) {
                        //                 if (peopleToConnect[i].mobileNumber && peopleToConnect[i].mobileNumber == ele.mobileNumber) {
                        //                     peopleToConnect.splice(i, 1)
                        //                     i = 0;
                        //                 } else if (peopleToConnect[i].contactEmail && peopleToConnect[i].contactEmail == ele.email) {
                        //                     peopleToConnect.splice(i, 1)
                        //                     i = 0;
                        //                 }
                        //             })
                        //         }

                        //         peopleToConnect.sort(function(a, b) {
                        //             return b.value - a.value;
                        //         })

                        //         callback(null, peopleToConnect)
                        //     } else {
                        //         peopleToConnect.sort(function(a, b) {
                        //             return b.value - a.value;
                        //         })
                        //         callback(null, peopleToConnect)
                        //     }
                        // })
                    }
                })
            }
        })
    };

    this.insightsActionLogForAdmin = function(from, to, infoFor, callback) {
        getInsightsActionLog(from, to, infoFor, callback);
    }

    this.peopleToConnectIgnore = function(userId, contactId, update, interactionId, callback) {
        if (update == 'reminder') {
            myUserCollection.update({ _id: userId, "contacts._id": contactId }, { "$set": { "contacts.$.remindToConnect": null } }, function(error, result) {
                if (error) {
                    callback(error, false);
                } else if (result) {
                    callback(error, true);
                }
            });
        }
        else if (update == 'notify') {
            InteractionsCollection.update({ ownerId: userId, "_id": interactionId }, { "$set": { "trackInfo.gotResponse": true } }, function(error, result) {
                if (error) {
                    callback(error, false);
                } else if (result) {
                    callback(error, true);
                }
            });
        }
    }

    this.yourResponsePendingIgnore = function(userId, interactionId, emailThreadId, callback) {
        var q = { ownerId: userId }
        if(interactionId)
            q["_id"] = interactionId;
        if(emailThreadId)
            q["emailThreadId"] = emailThreadId;
        callback(null, true)
        InteractionsCollection.update(q, { "$set": { "insightsIgnore.responsePending": true } },{multi:true}, function(error, result) {
            if (error) {
                callback(error, false);
            } else if (result) {
                callback(error, true);
            }
        });
    }

    this.yourResponsePending = function(userId, dateMin, dateMax, filter, emails, callback){
        getEmailInteraction(userId, dateMin, dateMax, filter, emails, callback);
    }
}

insightsManagement.prototype.remindToConnect = function(userId, dateMin, dateMax, timezone, callback) {
    getRemindToConnect(userId, dateMin, dateMax, function(err, remind) {
        callback(err,remind)
    });
};

insightsManagement.prototype.dealsAtRiskWeights = function(callback) {
    algoSeedValue.findOne({ documentName: "dealsAtRisk" },{weightages:1}, function (err,weights) {
        if(!err && weights){
            callback(err,JSON.parse(JSON.stringify(weights)))
        }
    })
};

var getEmailInteraction = function(ownerId, dateMin, dateMax, filter, emails, callback){
    
    var q = { toCcBcc:{$nin: ["cc", "bcc","to"] }, "insightsIgnore.responsePending":{$ne: true}};
    var finalQuery = [];

    finalQuery.push(
        {$match:{
            ownerId:ownerId,
            userId:{$ne:ownerId},
            interactionDate:{$gt:new Date(dateMin), $lt:new Date(dateMax) } ,
            interactionType:"email",
            emailId:{$in:emails}
        }
        }
    );
    
    finalQuery.push({$match:q});

    finalQuery.push(
        {$sort:{"interactionDate":-1}},
        {$group:{
            _id:{emailId:"$emailId", emailThreadId:"$emailThreadId"},
            interaction:{
                $push:{
                    personEmailId:"$emailId",
                    userId:"$userId",
                    emailThreadId:"$emailThreadId",
                    //name:{$concat:["$firstName", ' ', "$lastName"]},
                    source:"$source",
                    interactionDate:"$interactionDate",
                    action:"$action",
                    title:"$title",
                    refId:"$refId",
                    emailContentId:"$emailContentId",
                    description:"$description",
                    companyNameFromInteraction:"$companyName",
                    interactionId:"$_id",
                    trackInfo:"$trackInfo",
                    interactionType:"$interactionType",
                    trackId:"$trackId"
                }
            },
            counts:{$sum:1}
        }
        }
    );

    if(filter == 'thread'){
        finalQuery.push(
            {$match:{"_id.emailThreadId":{$ne:null}, counts:{$gt:1}}},
            {$project:{
                _id:0,
                emailId:"$_id.emailId",
                emailThreadId:"$_id.emailThreadId",
                interaction: 1
            }
            }
        )
    }
    else{
        finalQuery.push(
            {$match:{$or:[{"_id.emailThreadId":{$eq:null}}, {counts:{$lte:1}}] }},
            {$project:{
                _id:0,
                emailId:"$_id.emailId",
                emailThreadId:"$_id.emailThreadId",
                interaction: 1
            }
            },
            {$unwind:"$interaction"},
            {$sort:{"interaction.interactionDate":1}},
            {$group:{
                _id:"$emailId",
                interactions:{$push:"$interaction"}
            }
            }
        )
    }

    InteractionsCollection.aggregate(finalQuery).exec(function(error, emailThreads){
        callback(error, emailThreads)
    });
}

var getInsightsActionLog = function(from, to, infoFor, callback) {

    var dateMin = momentTZ(from);
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)
    var dateMax = momentTZ(to);

    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(59)
    dateMax.millisecond(0)
    insightsActionTaken.find({ createdDate: { $gt: new Date(dateMin), $lt: new Date(dateMax) } }, function(err, log) {
        if (err || log.length == 0)
            callback(err, null);
        else {
            myUserCollection.find({}, { emailId: 1, firstName: 1, lastName: 1, _id: 0 }, function(error, user) {
                if (err || user.length == 0)
                    callback(err, null);
                else {
                    var finalResult = [];
                    log.forEach(function(ele) {
                        for (var i = 0; i < user.length; i++) {
                            if (ele.userEmailId && user[i].emailId && ele.userEmailId.toLowerCase() == user[i].emailId.toLowerCase()) {
                                finalResult.push({
                                    createdDate: moment(ele.createdDate).format("DD MMM YYYY"),
                                    firstName: user[i].firstName,
                                    lastName: user[i].lastName,
                                    userEmailId: ele.userEmailId,
                                    contactName: ele.contact.name,
                                    contactEmail: ele.contact.personEmailId,
                                    insightSource: ele.insightSource,
                                    self_hierarchy: ele.self_hierarchy ? (ele.self_hierarchy == 'self' ? 'S' : 'H') : null,
                                    actionType: ele.actionType,
                                    suggestionIndexValue: ele.suggestionIndexValue,
                                    suggestion: ele.suggestion
                                })
                            }
                        }
                    });
                    callback(null, finalResult);
                }
            });
            // callback(null, log);
        }
    });

}

var getNotifyInSevenDaysContacts = function(userId, dateMin, dateMax, timezone, callback) {
    InteractionsCollection.aggregate([
        { $match: { ownerId: userId } }, {
            $group: {
                _id: {
                    ne: { $ne: ["$userId", "$ownerId"] },
                    emailId: "$ownerEmailId",
                    uid: "$ownerId",
                },
                interactions: { $push: { interactionDate:"$interactionDate", trackInfo:"$trackInfo", action:"$action", emailId:"$emailId",  refId:"$refId", title:"$title", _id:"$_id", description:"$description", emailContentId:"$emailContentId", ownerEmailId:"$ownerEmailId", trackId:"$trackId" }}
            }
        },
        { $match: { "_id.ne": true } },
        { $unwind: "$interactions" }, {
            $match: {
                "interactions.interactionDate": { $lt: new Date(dateMax) },
                "interactions.trackInfo.trackResponse": { $eq: true },
                "interactions.action": { $eq: "receiver" },
                "interactions.trackInfo.gotResponse": { $in: [false,null] }
            }
        }, {
            $project: {
                _id: 0,
                emailId: "$interactions.emailId",
                refId: "$interactions.refId",
                title: "$interactions.title",
                interactionId:"$interactions._id",
                description:"$interactions.description",
                emailContentId:"$interactions.emailContentId",
                ownerEmailId:"$interactions.ownerEmailId",
                trackId:"$interactions.trackId",
                trackInfo:"$interactions.trackInfo",
                action:"$interactions.action"
            }
        }
    ], function(err, result) {
        if (err || result.length == 0)
            callback(err, null);
        else {
            var emailIds = [];
            result.forEach(function(ele) {
                emailIds.push(ele.emailId)
            })

            var q = { "contacts.personEmailId": { $in: emailIds } };
            myUserCollection.aggregate([
                { $match: { _id: userId } },
                { $unwind: "$contacts" },
                { $match: q }, {
                    $project: {
                        _id: 0,
                        contactEmail: "$contacts.personEmailId",
                        company: "$contacts.companyName",
                        designation: "$contacts.designation",
                        value: "$contacts.account.value.inProcess",
                        userId: "$contacts.personId",
                        contactId: "$contacts._id",
                        rel: "$contacts.contactRelation",
                        mobileNumber: "$contacts.mobileNumber",
                        lastInteracted: "$contacts.lastInteracted",
                        contactImageLink: "$contacts.contactImageLink",
                        personName: "$contacts.personName",
                        ownerId: "$_id"
                    }
                }
            ], function(error, notify) {
                if (err || notify.length == 0)
                    callback(err, null)
                else {
                    var emailIds = [];

                    result.forEach(function(ele) {
                        for (var i = 0; i < notify.length; i++) {
                            if (ele.emailId && notify[i].contactEmail && ele.emailId.toLowerCase() == notify[i].contactEmail.toLowerCase()) {
                                ele.contactEmail = notify[i].contactEmail;
                                ele.contactImageLink = notify[i].contactImageLink;
                                ele.company = notify[i].company;
                                ele.designation = notify[i].designation;
                                ele.value = notify[i].value;
                                ele.userId = notify[i].userId;
                                ele.contactId = notify[i].contactId;
                                ele.rel = notify[i].rel;
                                ele.mobileNumber = notify[i].mobileNumber;
                                ele.lastInteracted = notify[i].lastInteracted;
                                ele.personName = notify[i].personName;
                                ele.reminder_notify = 'notify';
                                ele.ownerId = notify[i].ownerId;

                                ele.suggestion = 'You have not received any response on your last mail.\nFollow up.';
                                if (ele.rel && ele.rel.decisionmaker_influencer)
                                    ele.relation = ele.rel.decisionmaker_influencer;
                                else
                                    ele.relation = null;
                                if (ele.lastInteracted)
                                    ele.lastInteracted = moment(ele.lastInteracted).format("DD MMM YYYY")
                                if (!ele.value)
                                    ele.value = 0;
                                delete ele.emailId;
                                delete ele.rel;
                            }
                        }
                    })

                    notify.forEach(function(ele) {
                        if (ele.contactEmail)
                            emailIds.push(ele.contactEmail)
                    })

                    getProfileUrl(emailIds, result, function(notifyResult) {
                        callback(null, notifyResult);
                    });
                }
            });
        }
    });
}

var getRemindToConnect = function(userId, dateMin, dateMax, callback) {

    var dateRange = {
        $gte: new Date(dateMin),
        $lte: new Date(dateMax)
    }

    myUserCollection.aggregate([
        { $match: { _id: userId } },
        { $unwind: "$contacts" },
        // { $match: { "contacts.remindToConnect": { $lt: new Date(dateMax) } } }, {
        { $match: { "contacts.remindToConnect": dateRange } }, {
            $project: {
                _id:0,
                ownerId: "$_id",
                remindToconnect: "$contacts.remindToConnect",
                contactEmail: "$contacts.personEmailId",
                company: "$contacts.companyName",
                designation: "$contacts.designation",
                value: "$contacts.account.value.inProcess",
                userId: "$contacts.personId",
                contactId: "$contacts._id",
                rel: "$contacts.contactRelation",
                mobileNumber: "$contacts.mobileNumber",
                lastInteracted: "$contacts.lastInteracted",
                personName: "$contacts.personName",
                contactImageLink: "$contacts.contactImageLink"
            }
        }
    ], function(err, remind) {
        if (err || remind.length == 0) {
            callback(err, null);
        } else {
            var emailIds = [];
            remind.forEach(function(ele) {
                ele.suggestion = 'Your reminder to connect has been activated.\nConnect now';
                if (ele.rel && ele.rel.decisionmaker_influencer)
                    ele.relation = ele.rel.decisionmaker_influencer;
                else
                    ele.relation = null;
                if (!ele.value)
                    ele.value = 0;

                if (ele.lastInteracted)
                    ele.lastInteracted = moment(ele.lastInteracted).format("DD MMM YYYY")
                delete ele.rel;
                if (ele.contactEmail)
                    emailIds.push(ele.contactEmail);
                ele.reminder_notify = 'reminder';
            })
            if (emailIds.length > 0)
                getProfileUrl(emailIds, remind, function(remindResult) {
                    callback(null, remindResult);
                });
            else
                callback(null, remind);
        }
    });
}

var getProfileUrl = function(emailIds, contacts, callback) {
    getPublicProfileUrl(emailIds, function(err, publicProfileData) {
        if (err || !publicProfileData)
            callback(contacts);
        else {
            contacts.forEach(function(ele) {
                for (var i = 0; i < publicProfileData.length; i++)
                    if (ele.contactEmail == publicProfileData[i].emailId)
                        ele.publicProfileUrl = publicProfileData[i].publicProfileUrl
            })

            callback(contacts);
        }
    });
}

var getActionTakenContacts = function(userId, dateMin, dateMax, insightSource, self_hierarchy, callback) {
    var uid = String(userId);
    insightsActionTaken.aggregate([{
        $match: {
            userId: uid,
            createdDate: { $gt: new Date(dateMin), $lt: new Date(dateMax) },
            insightSource: insightSource,
            self_hierarchy: self_hierarchy
        }
    }, {
        $project: {
            _id: 0,
            userId: "$contact.userId",
            name: "$contact.name",
            email: "$contact.personEmailId",
            mobileNumber: "$mobileNumber"
        }
    }], function(err, result) {
        if (err || result.length == 0)
            callback(err, null);
        else
            callback(null, result);
    })
}

var getInteractionsByRange = function(userId, dateRange, emailIds, callback) {

    InteractionsCollection.aggregate([

        {
            $match: { ownerId: userId }
        }, {
            $match: {
                "emailId": { $in: emailIds },
                "interactionDate": dateRange
            }
        },  {
            $match: {
                $and: [{ "emailId": { $nin: [null, ''] } }, { "userId": { $ne: userId } }]

            }
        }, {
            $group: {
                _id: "$emailId",
                count: { $sum: 1 },
                lastInteracted: { $max: "$interactionDate" }
            }
        },

        {
            $sort: { "count": -1 }
        }, {
            $project: {
                email: "$_id",
                interactionCount: "$count",
                lastInteracted: "$lastInteracted"
            }
        }
    ]).exec(function(error, interactionsData) {
        if (!error) {
            callback(interactionsData)
        } else {
            callback([])
        }
    })
}

var getLosingTouchAggregatedNumbers = function(insightsFor, callback) {

    var threshold = 50;

    myUserCollection.aggregate([
        {
            $match: {
                _id: { $in: insightsFor }
            }
        },
        {
            $unwind: "$contacts"
        },
        {
            $project: {
                _id: "$_id",
                personEmailId: "$contacts.personEmailId"
            }
        }]).exec(function(err,data) {

        var userGroup = _
            .chain(data)
            .groupBy('_id')
            .map(function (value, key) {
                return {
                    userId: castToObjectId(key),
                    emailArray: _.pluck(value, 'personEmailId')
                }
            })
            .value();

        var queryFinal = {
            "$or": userGroup.map(function(el) {
                el["contacts.personEmailId"] = { "$in": el.emailArray };
                delete el.emailArray;
                return el;
            })
        };
        
        losingTouch.aggregate([
            {
                $match: {
                    userId: { $in: insightsFor }
                }
            },
            {
                $unwind: "$contacts"
            },
            {
                $match: queryFinal
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 }
                }
            }
        ]).exec(function(err, losingTouchCount) {
            var userGroup2 = _
                .chain(data)
                .groupBy('_id')
                .map(function(value, key) {
                    return {
                        userId: castToObjectId(key),
                        emailArray: _.pluck(value, 'personEmailId')
                    }
                })
                .value();

            var queryFinal2 = {
                "$or": userGroup2.map(function(el) {
                    el["contacts.personEmailId"] = { "$in": el.emailArray };
                    el["contacts.score"] = { $lte: threshold };
                    el["contacts.actionTaken"] = { $ne: true };
                    delete el.emailArray;
                    return el;
                })
            };

            losingTouch.aggregate([{
                $match: {
                    userId: { $in: insightsFor }
                }
            }, {
                $unwind: "$contacts"
            }, {
                $match: queryFinal2
            }, {
                $group: {
                    _id: null,
                    losingTouch: { $sum: 1 }
                }
            }]).exec(function(err, data) {

                if (losingTouchCount.length > 0 && data.length > 0) {
                    var obj = {
                        selfTotalCount: losingTouchCount[0].count,
                        selfLosingTouch: data[0].losingTouch
                    }

                } else {
                    var obj = {
                        selfTotalCount: 0,
                        selfLosingTouch: 0
                    }
                }
                callback(obj)
            })
        });
    });
}

var getLosingTouchAccountValuesAll = function(insightsFor, threshold, limit, callback) {

    threshold = 50;

    var losingTouchAggregation = [];

    if (insightsFor.length > 1) {
        losingTouchAggregation.push({
            $match: { userId: { $in: insightsFor } }
        });
        losingTouchAggregation.push({
            $unwind: "$contacts"
        });

        losingTouchAggregation.push({
            $match: {
                "contacts.actionTaken": { $ne: true },
                "contacts.score": { $lte: threshold }
            }
        });

        losingTouchAggregation.push({
            $sort: { "contacts.score": 1 }
        });

    } else {
        losingTouchAggregation.push({
            $match: { userId: { $in: insightsFor } }
        });
        losingTouchAggregation.push({
            $unwind: "$contacts"
        });
        losingTouchAggregation.push({
            $sort: { "contacts.score": 1 }
        });

        losingTouchAggregation.push({
            $match: {
                "contacts.actionTaken": { $ne: true },
                "contacts.score": { $lte: threshold }
            }
        });

        if (limit === 'true') {
            losingTouchAggregation.push({
                $limit: 5
            });
        }
    };

    losingTouch.aggregate(losingTouchAggregation).exec(function(err, data) {

        var emailIds = _.compact(_.pluck(data, "contacts.personEmailId"));
        var mobileNumbers = _.compact(_.pluck(data, "contacts.mobileNumber"));
        var todayDate = new Date();

        var q = {
            $and: [
                {$or:[{"contacts.personEmailId": { $in: emailIds }},{"contacts.mobileNumber": { $in: mobileNumbers }}]}
                // ,
                // {$or: [
                //     { "contacts.account.ignore": { $lte: todayDate } },
                //     { "contacts.account.ignore": { $exists: false } },
                //     { "contacts.account.ignore": { $in: [false, null, 'never', true] } }
                // ]}
            ]
        }

        myUserCollection.aggregate([{
            $match: { _id: { $in: insightsFor } }
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }, {
            $project: {
                "_id": "$contacts.personEmailId",
                "fullName": "$contacts.personName",
                "company": "$contacts.account.name",
                "designation": "$contacts.designation",
                "contactValue": "$contacts.account.value.inProcess",
                "relatasUser": "$contacts.relatasUser",
                "baseLocation": "$contacts.location",
                "personId": "$contacts.personId",
                "personEmailId": "$contacts.personEmailId",
                "contactId": "$contacts._id",
                "contactImageLink": "$contacts.contactImageLink",
                "contactMobileNumber": "$contacts.mobileNumber",
                "contactLastInteracted": "$contacts.lastInteracted",
                "twitterUserName": "$twitterUserName",
                "contactRelation": "$contacts.contactRelation",
                "ownerId": "$_id",
                "ownerFirstName": "$firstName",
                "ownerLastName": "$lastName",
                "ownerEmailId": "$emailId"
            }
        }]).exec(function(err, contacts) {

            if (contacts.length > 0) {
                getPublicProfileUrl(emailIds, function(err, publicProfileData) {

                    var userGroup = _
                        .chain(contacts)
                        .groupBy('ownerId')
                        .map(function(value, key) {
                            return {
                                ownerId: castToObjectId(key),
                                emailArray: _.compact(_.pluck(value, 'personEmailId')),
                                mobileNumberArray: _.compact(_.pluck(value, 'contactMobileNumber'))
                            }
                        })
                        .value();
                    //build the query to fetch data

                    var queryFinal = {
                        "$or": userGroup.map(function(el) {
                            el["interactionType"] = { $nin: [null, ''] };
                            el["$or"] = [{"mobileNumber":{$in:el.mobileNumberArray}},{"emailId":{ "$in": el.emailArray }}]
                            delete el.emailArray;
                            delete el.mobileNumberArray;
                            return el;
                        })
                    };

                    getInteractionsByType(insightsFor, queryFinal, function(twitterInteractions) {
                        var objPublicProfileUrl = _.map(contacts, function(a) {
                            return _.extend(a, _.find(publicProfileData, function(b) {
                                return a._id === b.emailId;
                            }))
                        });

                        var obj = _.map(objPublicProfileUrl, function(b) {

                            b.company = b.company || null;
                            b.designation = b.designation || "";
                            b.contactValue = b.contactValue || "0";
                            if (!b.publicProfileUrl) {
                                b.publicProfileUrl = '#';
                            }
                            return b;
                        });

                        var appendTwitter = _.map(obj, function(a) {
                            return _.extend(a, _.find(twitterInteractions, function(b) {
                                return a._id === b.emailId;
                            }))
                        });

                        if (!err) {
                            callback(appendTwitter)
                        } else {
                            callback(null)
                        }
                    });
                });
            } else {

                var obj = _.map(contacts, function(b) {

                    b.company = b.company || null;
                    b.designation = b.designation || "";
                    b.contactValue = b.contactValue || "0";
                    if (b.lastInteracted) {
                        b.lastInteractedISO = new Date(b.lastInteracted);
                        b.lastInteracted = moment(b.lastInteractedISO).format("DD MMM YYYY");
                    } else {
                        b.lastInteracted = "";
                    }
                    if (!b.publicProfileUrl) {
                        b.publicProfileUrl = '#';
                    }
                    return b;
                });

                if (!err) {
                    callback(obj)
                } else {
                    callback(null)
                }
            }
        });
    });
}

function getInteractionsByType(hierarchy, queryFinal, callback) {

    InteractionsCollection.aggregate([{
        $match: { ownerId: { $in: hierarchy } }
    },  {
        $match: queryFinal
    }, {
        $match: {
            $and: [{ "emailId": { $nin: [null, ''] } }, { "userId": { $nin: hierarchy } }]

        }
    }, {
        $project: {
            "_id": "$_id",
            "refId": "$refId",
            "emailId": "$emailId",
            "mobileNumber": "$mobileNumber",
            // "action": "$interactions.action",
            "interactionDate": "$interactionDate",
            "interactionType": "$interactionType",
            "title": "$title",
            "postURL": "$postURL"
        }
    },

    {
        $sort: { "interactionDate": -1 }
    }]).exec(function(err, interactions) {
        if (!err && checkRequired(interactions) && interactions.length > 0) {
            callback(interactions);
        } else callback([])
    });
}

function getPublicProfileUrl(emailIds, callback) {
    myUserCollection.find({ emailId: { $in: emailIds } }, { publicProfileUrl: 1, emailId: 1, registeredUser: 1 }, function(error, contactPublicProfileUrl) {

        var results = _.map(
            _.where(contactPublicProfileUrl, { registeredUser: true }),
            function(data) {
                return { emailId: data.emailId, publicProfileUrl: data.publicProfileUrl };
            }
        );

        if (error) {
            loggerError.info('An error occurred in findUserProfileByEmailIdWithCustomFields() ' + JSON.stringify(error));
        }

        if (checkRequired(results)) {
            callback(error, results);
        } else {

            callback(error, null);
        }

    });
};

function getLosingTouchContacts(hierarchyList, threshold, companyMembers, callback) {

    threshold = 50;
    var todayDate = new Date();

    myUserCollection.aggregate([
        {
            $match: {
                _id: { $in: hierarchyList }
            }
        },
        {
            $unwind: "$contacts"
        },
        {
            $match: {
                "contacts.favorite":true
            }
        },
        {
            $match:{
                $or: [
                    { "contacts.account.ignore": { $lte: todayDate } },
                    { "contacts.account.ignore": { $exists: false } },
                    { "contacts.account.ignore": { $in: [false, null, 'never', true] } }
                ]
            }
        },
        {
            $project: {
                _id: "$_id",
                personEmailId: "$contacts.personEmailId",
                mobileNumber:"$contacts.mobileNumber"
            }
        }]).exec(function(err,data){
        var emailIdArr =[];
        emailIdArr = _.compact(_.pluck(data, 'personEmailId'));
        var mobileNumberArr = [];
        mobileNumberArr = _.compact(_.pluck(data, 'mobileNumber'));
        //var queryElements = userGroup.map(function(el) {
        //    var len = el.mobileNumberArray.length
        //    var tempArr = []
        //
        //    //TODO remove this method when all mobile numbers saved after cleanup.
        //    for(var i=0;i<len;i++){
        //        tempArr.push(el.mobileNumberArray[i].replace(/[^a-zA-Z0-9]/g,''))
        //    }
        //        var k = [];
        //        var a = { "$in": el.emailArray }
        //        var b = { "$in": tempArr }
        //        k.push({"contacts.personEmailId":a})
        //        k.push({"contacts.mobileNumber":b})
        //
        //        emailIdArr = emailIdArr.concat(el.emailArray);
        //        mobileNumberArr = mobileNumberArr.concat(tempArr)
        //        return k;
        //    })

        if(emailIdArr.length === 0){
            emailIdArr = ["nothing to match"]
        }

        if(mobileNumberArr.length === 0){
            mobileNumberArr = ["nothing to match"]
        }

        var queryFinal = {};
        queryFinal["$or"] = [{"contacts.personEmailId":{$in:emailIdArr}},{"contacts.mobileNumber":{$in:mobileNumberArr}} ];
        
        losingTouch.aggregate([
            {
                $match: {
                    userId: { $in: hierarchyList }
                }
            },
            {
                $unwind: "$contacts"
            },
            {
                $match: queryFinal
            },
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 }
                }
            }
        ]).exec(function(err, losingTouchCount) {
            var userGroup2 = _
                .chain(data)
                .groupBy('_id')
                .map(function(value, key) {
                    return {
                        userId: castToObjectId(key),
                        emailArray: _.pluck(value, 'personEmailId'),
                        mobileNumberArray: _.pluck(value, 'mobileNumber')
                    }
                })
                .value();

            if(userGroup2.length>0) {
                var queryFinal2 = {
                    "$or": userGroup2.map(function(el) {

                        el["contacts.personEmailId"] = { "$in": el.emailArray };
                        el["contacts.mobileNumber"] = { "$in": el.mobileNumberArray};
                        el["contacts.score"] = { $lte: threshold };
                        el["contacts.actionTaken"] = { $ne: true };
                        delete el.emailArray;
                        delete el.mobileNumberArray;
                        return el;
                    })
                };
            } else {
                queryFinal2 = {
                    "$or": [
                        {
                            "contacts.personEmailId":{$in:["Nothing to match"]}
                        }
                    ]
                }
            }

            losingTouch.aggregate([{
                $match: {
                    userId: { $in: hierarchyList }
                }
            }, {
                $unwind: "$contacts"
            }, {
                $match: queryFinal2
            }, {
                $group: {
                    _id: null,
                    losingTouch: { $sum: 1 }
                }
            }]).exec(function(err, data) {

                if (losingTouchCount.length > 0 && data.length > 0) {
                    var obj = {
                        selfTotalCount: losingTouchCount[0].count,
                        selfLosingTouch: data[0].losingTouch
                    }

                } else {
                    var obj = {
                        selfTotalCount: 0,
                        selfLosingTouch: 0
                    }
                }

                if (!err) {
                    getLosingTouchContactsCompany(companyMembers, threshold, err, obj, callback)
                } else {
                    callback(err, null)
                }

            });
        })
    })
};

function getLosingTouchContactsCompany(companyMembers, threshold, err, obj, callback) {
    
    myUserCollection.aggregate([
        {
            $match: {
                _id: { $in: companyMembers }
            }
        },
        {
            $unwind: "$contacts"
        },
        {
            $match: {
                "contacts.favorite":true
            }
        },
        {
            $project: {
                _id: "$_id",
                personEmailId: "$contacts.personEmailId"
            }
        }]).exec(function(err,data){
        var userGroup = _
            .chain(data)
            .groupBy('_id')
            .map(function(value, key) {
                return {
                    userId: castToObjectId(key),
                    emailArray: _.pluck(value, 'personEmailId')
                }
            })
            .value();

        var queryFinal = {
            "$or": userGroup.map(function(el) {
                el["contacts.personEmailId"] = { "$in": el.emailArray };
                delete el.emailArray;
                return el;
            })
        };

        losingTouch.aggregate([
            {
                $match: { userId: { $in: companyMembers } }
            },
            {
                $unwind: "$contacts"
            },
            {
                $match: queryFinal
            },
            {
                $group: {
                    _id: "_id",
                    count1: { $sum: 1 }
                }
            }
            //}
        ]).exec(function(err, count) {

            var userGroup2 = _
                .chain(data)
                .groupBy('_id')
                .map(function(value, key) {
                    return {
                        userId: castToObjectId(key),
                        emailArray: _.pluck(value, 'personEmailId')
                    }
                })
                .value();

            var queryFinal2 = {
                "$or": userGroup2.map(function(el) {
                    el["contacts.personEmailId"] = { "$in": el.emailArray };
                    el["contacts.score"] = { $lte: threshold };
                    el["contacts.actionTaken"] = { $ne: true };
                    delete el.emailArray;
                    return el;
                })
            };

            losingTouch.aggregate([{
                $match: { userId: { $in: companyMembers } }
            }, {
                $unwind: "$contacts"
            }, {
                $match: queryFinal2
            }, {
                $group: {
                    _id: null,
                    losingTouch: { $sum: 1 }
                }
            }]).exec(function(err, ltData) {

                var companyObj;
                if(count && count.length>0 && ltData && ltData.length){
                    companyObj = {
                        companyTotalContacts: count[0].count1,
                        companyLosingTouch: ltData[0].losingTouch
                    }
                } else {
                    companyObj = {
                        companyTotalContacts: 0,
                        companyLosingTouch: 0
                    }
                }

                var dataObj = {
                    company: companyObj,
                    self: obj
                }
                callback(err, dataObj)
            })
        });
    });
};

function checkRequired(data) {
    if (data == '' || data == null || data == undefined) {
        return false;
    } else {
        return true;
    }
}

/*Rules Engine*/

function getSuggestion(rulesFor, callback) {

    var arr = [];
    var len = rulesFor.length;

    rulesFor.forEach(function(ele) {
        R2.execute(ele, function(data) {
            if (!data.result) {
                len--;
                arr.push(data)
                if (len == 0) {
                    callback(arr);
                }
            } else {
                arr.push('all conditions are failed');
            }
        });
    });
}

//new
function getCompanyRelationshipRisk(userIds, filter, detailsFor, limit,usersCompany, callback) {
    var userSelectedArr = [];
    userSelectedArr = userIds;
    var crrAgg = [];

    if (detailsFor == 'selfLimit') {
        var q = {
            "contacts.account.name": { $nin: [null,usersCompany] }
        }

        crrAgg = [{
                $match: { _id: { $in: userSelectedArr } }
            }, {
                $unwind: "$contacts"
            }, {
                $match: q
            }, {
                $group: {
                    _id: "$contacts.account.name",
                    userId: { $first: "$_id" },
                    profilePicUrl: { $first: "$profilePicUrl" },
                    emailId: { $first: "$emailId" },
                    firstName: { $first: "$firstName" },
                    lastName: { $first: "$lastName" },
                    // profilePicUrl:{},
                    contacts: { $push: "$contacts" },
                    noOfContacts: { $sum: 1 },
                    inProcess: { $sum: "$contacts.account.value.inProcess" }
                }
            },
            { $sort: { "inProcess": -1 } }
        ];


        if (limit > 0) {
            li = Number(limit)
            crrAgg.push({ $limit: li })
        }

        crrAgg.push({ $unwind: "$contacts" }, {
            $project: {
                _id: "$userId",
                userEmail: "$emailId",
                userFname: "$firstName",
                userLname: "$lastName",
                email: "$contacts.personEmailId",
                profileUrl: "$profilePicUrl",
                companyName: "$contacts.account.name",
                contactName: "$contacts.personName",
                contactRelation: "$contacts.contactRelation.decisionmaker_influencer",
                interaction: { $cond: { if: "$contacts.account.value.inProcess", then: 0, else: 0 } },
                contactValue: { $cond: { if: "$contacts.account.value.inProcess", then: "$contacts.account.value.inProcess", else: 0 } }
            }
        })

    } else {
        if (detailsFor == 'self' || detailsFor == 'company')
            var q = {
                "contacts.account.name": { $nin: [null,usersCompany] }
            }
        else
            var q = {
                "contacts.account.showToReportingManager": true,
                "contacts.account.name": { $nin: [null,usersCompany] }
            }

        crrAgg = [{
            $match: { _id: { $in: userSelectedArr } }
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }];


        if (limit > 0) {
            li = Number(limit)
            crrAgg.push({ $limit: li })
        }

        crrAgg.push({
            $project: {
                _id: "$_id",
                userEmail: "$emailId",
                userFname: "$firstName",
                userLname: "$lastName",
                email: "$contacts.personEmailId",
                profileUrl: "$profilePicUrl",
                url: "$publicProfileUrl",
                mobileNumber: "$mobileNumber",
                companyName: "$contacts.account.name",
                contactName: "$contacts.personName",
                contactRelation: "$contacts.contactRelation.decisionmaker_influencer",
                interaction: { $cond: { if: "$contacts.account.value.inProcess", then: 0, else: 0 } },
                contactValue: { $cond: { if: "$contacts.account.value.inProcess", then: "$contacts.account.value.inProcess", else: 0 } }
            }
        })
    }

    myUserCollection.aggregate(crrAgg).exec(function(err, userCompany) {
        // callback(err,userCompany)
        if (err || userCompany.length == 0)
            callback(err, null);
        else {

            var userGroup = _
                .chain(userCompany)
                .groupBy('_id')
                .map(function(value, key) {
                    return {
                        ownerId: common.castToObjectId(key),
                        emailArray: _.pluck(value, 'email')
                    }
                })
                .value();

            var queryFinal = {
                "$or": userGroup.map(function(el) {
                    el["emailId"] = { "$in": el.emailArray };
                    el["interactionDate"] = { $gt: moment().subtract(30, "days").toDate(), $lt: moment().subtract(0, "days").toDate() }
                    delete el.emailArray;
                    return el;
                })
            };

            InteractionsCollection.aggregate([{
                    $match: { ownerId: { $in: userSelectedArr } }
                },  {
                    $match: queryFinal
                },  {
                    $match: {
                        "emailId": { $nin: [null, ''] }

                    }
                }, {
                    $group: {
                        _id: { emailId: "$ownerEmailId", contactEmail: "$emailId" },
                        count: { $sum: 1 },
                        company: { $first: "$companyName" },
                        lastInteracted: { $max: "$interactionDate" }
                    }
                },
                {
                    $sort: { "count": -1 }
                }, {
                    $project: {
                        emailId: "$_id.emailId",
                        contactEmail: "$_id.contactEmail",
                        interaction: "$count",
                        company: 1,
                        lastInteracted: 1,
                        _id: 0
                    }
                }

            ]).exec(function(err, data) {
                if (err)
                    callback(err, null)
                else {
                    for (var i = 0; i < userCompany.length; i++) {
                        for (var j = 0; j < data.length; j++) {
                            if (userCompany[i].companyName == data[j].company && userCompany[i].userEmail == data[j].emailId && userCompany[i].email == data[j].contactEmail)
                                userCompany[i].interaction = data[j].interaction
                        }
                    }

                    //use it for details self

                    if (filter == 'detail')
                        callback(err, { value: userCompany });
                    else if (filter == 'graph') {

                        var accounts = [];
                        //use it for graph
                        accounts = _
                            .chain(userCompany)
                            .groupBy('companyName')
                            .map(function(value, key) {
                                return {
                                    company: key,
                                    contactsEmail: _.pluck(value, 'email'),
                                    contactValues: _.pluck(value, 'contactValue'),
                                    interaction: _.pluck(value, 'interaction'),
                                    relation: _.pluck(value, 'contactRelation')
                                }
                            })
                            .value()

                        var companyInteraction = 0;
                        var compnayContactValue = 0;

                        accounts.forEach(function(ele) {
                            var influencer = 0;
                            var decisionMaker = 0;
                            ele.interaction = ele.interaction.reduce((a, b) => a + b, 0);
                            ele.contactValues = ele.contactValues.reduce((a, b) => a + b, 0);
                            ele.relation.forEach(function(rel) {
                                if (rel == 'influencer')
                                    influencer++;
                                else if (rel == 'decision_maker')
                                    decisionMaker++;
                            });
                            ele.decisionMaker = decisionMaker;
                            ele.influencer = influencer;
                            companyInteraction += ele.interaction;
                            compnayContactValue += ele.contactValues;
                            for (var i = 0; i < ele.contactsEmail.length; i++) { //remove redundant contacts
                                for (var j = 0; j < ele.contactsEmail.length; j++) {
                                    if (i != j && ele.contactsEmail[i] == ele.contactsEmail[j]) {
                                        ele.contactsEmail.splice(i, 1)
                                        i = 0;
                                    }
                                }
                            }
                            ele.contacts = ele.contactsEmail.length;
                            delete ele.relation;
                        });
                        //graph end

                        accounts.sort(function(a, b) {
                            if (b.contactValues != 0 || a.contactValues != 0)
                                return b.contactValues - a.contactValues || a.interaction - b.interaction;
                            else if (a.interaction != 0 || b.interaction != 0)
                                return b.interaction - a.interaction;
                            else if (a.decisionMaker != 0 || b.decisionMaker != 0)
                                return b.decisionMaker - a.decisionMaker;
                            else if (a.influencer != 0 || b.influencer != 0)
                                return b.influencer - a.influencer
                            else
                                return a.company - b.company;
                        });


                        callback(err, { intraction: companyInteraction, value: compnayContactValue, accounts: accounts });
                    }
                }
            });
        }

    });
};

var sumOfArrayOfByKey = function(items, prop) {
    return items.reduce(function(a, b) {
        return a + b[prop];
    }, 0);
};

function castToObjectId(id) {
    return mongoose.Types.ObjectId(id)
};

function getCompanyRelationshipRiskDetailSelf(err, userIds, result,usersCompany, callback) {

    var accounts = [];
    var temp = [];
    var company = [];
    var testResult = [];
    if(result) {
        testResult = _.chain(result.value)
            .groupBy('companyName').value()
        _.each(testResult, function (value, key) {
            temp.push({
                company: key,
                contacts: value
            });
        })
    }

    _.each(temp, function(ele) {
        var contact = [];
        var interaction = 0;
        var value = 0;
        var noOfContact = 0;
        var influencer = [];
        var decisionmaker = [];
        var noOfDecisionmaker = 0;
        var noOfInfluencer = 0;
        ele.contacts.forEach(function(cont) {
            contact.push({ name: cont.contactName, value: cont.contactValue, relation: cont.contactRelation, email: cont.email })
            interaction += cont.interaction;
            value += cont.contactValue;
            noOfContact++;
            if (cont.contactRelation === 'influencer') {
                noOfInfluencer++;
                influencer.push({ name: cont.contactName, email: cont.email })
            } else if (cont.contactRelation === 'decision_maker') {
                noOfDecisionmaker++;
                decisionmaker.push({ name: cont.contactName, email: cont.email })
            }

        });
        accounts.push({ company: ele.company, interaction: interaction, contactValue: value, noOfContact: noOfContact, noOfDecisionmaker: noOfDecisionmaker, decisionmaker: decisionmaker, noOfInfluencer: noOfInfluencer, influencer: influencer, contacts: contact });
        company.push(ele.company);
    })
    myUserCollection.find({_id:{$in:userIds}},{contacts:1}).exec(function(error, userContacts){
        if(error)
            callback(err, null);
        else {
            var mobileNumberArr = [];
            var emailIdArr = [];
            mobileNumberArr = _.filter(userContacts[0].contacts, function(a) {
                if (a.mobileNumber) {
                    if (userContacts[0].mobileNumber) {
                        if (userContacts[0].mobileNumber !== a.mobileNumber)
                            return a.mobileNumber;
                    }
                    else
                        return a.mobileNumber;

                }
            })
            mobileNumberArr = _.pluck(mobileNumberArr, "mobileNumber");
            emailIdArr = _.filter(userContacts[0].contacts, function(a) {
                if (a.personEmailId) {
                    if(userContacts[0].emailId) {
                        if (userContacts[0].emailId.toLowerCase() !== a.personEmailId.toLowerCase())
                            return a.personEmailId;
                    }
                    else
                        return a.personEmailId;
                }
            })
            emailIdArr = _.pluck(emailIdArr, "personEmailId");
            //emailIdArr.push(userContacts[0].emailId);
            myUserCollection.aggregate([
                {$match: {_id: {$ne: userIds}}},
                {$unwind: "$contacts"},
                {
                    $match: {
                        "contacts.account.name": {$in: company},
                        "contacts.account.name": {$ne: usersCompany},
                        "contacts.contactRelation.decisionmaker_influencer": {$in: ['decision_maker', 'influencer']},
                        "contacts.personEmailId":{$nin:emailIdArr},
                        //"contacts.mobileNumber":{$nin:mobileNumberArr},
                        emailId:{$in:emailIdArr}
                    }
                }, {
                    $group: {
                        _id: "$contacts.account.name",
                        contacts: {$addToSet: {email: "$contacts.personEmailId"}},
                    }
                }, {
                    $project: {
                        _id: 0,
                        company: "$_id",
                        extendedNetwork: {$size: "$contacts"},
                        contacts:"$contacts"
                    }
                }
            ], function (err, extendedNet) {
                if (err)
                    callback(err, null)
                else {

                    _.each(extendedNet, function (ele) {
                        accounts.forEach(function (acco) {
                            if (ele.company == acco.company)
                                acco.extendedNetwork = ele.extendedNetwork;
                        });
                    });
                    _.each(accounts, function (ele) {
                        if (ele.extendedNetwork == undefined)
                            ele.extendedNetwork = 0;
                    });

                    accounts.sort(function (a, b) {
                        if (b.contactValue != 0 || a.contactValue != 0)
                            return b.contactValue - a.contactValue || a.interaction - b.interaction;
                        else if (a.interaction != 0 || b.interaction != 0)
                            return b.interaction - a.interaction;
                        else if (a.noOfDecisionmaker != 0 || b.noOfDecisionmaker != 0)
                            return b.noOfDecisionmaker - a.noOfDecisionmaker;
                        else if (a.noOfInfluencer != 0 || b.noOfInfluencer != 0)
                            return b.noOfInfluencer - a.noOfInfluencer
                        else
                            return a.company - b.company;
                    });
                    callback(err, accounts)

                }
            })
        }
    })
}

function getCompanyRevenueRisk(companyMembers, getTopTenCompaniesAtRisk, callback) {

    var todayDate = new Date();

    var q = {
        $and: [{
            "contacts.account.name": { $ne: null },
            $or: [
                { "contacts.account.ignore": { $lte: todayDate } },
                { "contacts.account.ignore": { $exists: false } },
                { "contacts.account.ignore": { $in: [false, null] } }
            ]
        }]
    }

    myUserCollection.aggregate([{
        $match: { _id: { $in: companyMembers } }
    }, {
        $unwind: "$contacts"
    }, {
        $match: q
    }, {
        $project: {
            _id: "$_id",
            personEmailId: "$contacts.personEmailId",
            company: "$contacts.account.name",
            contactValue: { $cond: { if: "$contacts.account.value.inProcess", then: "$contacts.account.value.inProcess", else: 0 } }
        }
    }]).exec(function(err, companyUsersData) {

        calculateRevenueRisk(companyMembers, companyUsersData, false, getTopTenCompaniesAtRisk, function(companyContactValue, companyValueAtRisk) {
            callback(companyContactValue, companyValueAtRisk)
        })
    });
}

function getInteractionGrowth(hierarchy, getFor, explain, callback) {

    var userCollectionQuery = [];
    var todayDate = new Date();

    var q = {
        $and: [{
            "contacts.account.name": { $ne: null },
            $or: [
                { "contacts.account.ignore": { $lte: todayDate } },
                { "contacts.account.ignore": { $exists: false } },
                { "contacts.account.ignore": { $in: [false, null] } }
            ]
        }]
    }

    if (getFor == 'self') {
        userCollectionQuery.push({
            $match: { _id: { $in: hierarchy } }
        }, {
            $unwind: "$contacts"
        }, {
            $match: q
        }, {
            $project: {
                _id: "$_id",
                "fullName": "$contacts.personName",
                "company": "$contacts.account.name",
                "designation": "$contacts.designation",
                "relatasUser": "$contacts.relatasUser",
                "baseLocation": "$contacts.location",
                "personId": "$contacts.personId",
                "personEmailId": "$contacts.personEmailId",
                "contactMobileNumber": "$contacts.mobileNumber",
                "contactId": "$contacts._id",
                "contactImageLink": "$contacts.contactImageLink",
                "twitterUserName": "$twitterUserName",
                "contactRelation": "$contacts.contactRelation",
                "ownerId": "$_id",
                "ownerFirstName": "$firstName",
                "ownerLastName": "$lastName",
                "ownerEmailId": "$emailId",
                "contactValue": { $cond: { if: "$contacts.account.value.inProcess", then: "$contacts.account.value.inProcess", else: 0 } }
            }
        })
    } else if (getFor == 'team') {
        userCollectionQuery.push({
            $match: { _id: { $in: hierarchy } }
        }, {
            $unwind: "$contacts"
        }, {
            $match: {
                "contacts.account.showToReportingManager": true,
                "contacts.account.name": { $ne: null },
            }
        }, {
            $project: {
                _id: "$_id",
                "fullName": "$contacts.personName",
                "company": "$contacts.account.name",
                "designation": "$contacts.designation",
                "lastInteracted": "$contacts.lastInteracted",
                "relatasUser": "$contacts.relatasUser",
                "baseLocation": "$contacts.location",
                "personId": "$contacts.personId",
                "personEmailId": "$contacts.personEmailId",
                "contactId": "$contacts._id",
                "contactImageLink": "$contacts.contactImageLink",
                "twitterUserName": "$twitterUserName",
                "contactRelation": "$contacts.contactRelation",
                "ownerId": "$_id",
                "ownerFirstName": "$firstName",
                "ownerLastName": "$lastName",
                "ownerEmailId": "$emailId",
                "contactValue": { $cond: { if: "$contacts.account.value.inProcess", then: "$contacts.account.value.inProcess", else: 0 } }
            }
        });
    }

    myUserCollection.aggregate(userCollectionQuery).exec(function(err, userData) {

        var emailIds = _.pluck(userData, 'personEmailId');

        var dateRange1 = { $gte: moment().subtract(60, "days").toDate(), $lte: moment().subtract(30, "days").toDate() }
        var dateRange2 = { $gte: moment().subtract(30, "days").toDate(), $lte: moment().subtract(0, "days").toDate() }

        interactionsByRange(hierarchy, emailIds, dateRange1, function(interactionsRange1) {
            interactionsByRange(hierarchy, emailIds, dateRange2, function(interactionsRange2) {

                //If no interactions exist for the selected contacts, the revenue at risk will be zero.
                if (interactionsRange1.length === 0 && interactionsRange2.length === 0) {
                    // getRevenueAtRiskWithNoInteractionsPastSixtyDays(emailIds)
                    callback([])
                } else {

                    var userCompanyFromContactsObject = {};
                    _.each(userData, function(u) {
                        userCompanyFromContactsObject[u.personEmailId] = u.company
                    });

                    var interactionsMap1 = _.map(interactionsRange1, function(r) {
                        r.company = userCompanyFromContactsObject[r.personEmailId] || r.company || "Others";
                        r.month = 'prev';
                        return r;
                    });

                    var interactionsMap2 = _.map(interactionsRange2, function(r) {
                        r.company = userCompanyFromContactsObject[r.personEmailId] || r.company || "Others"
                        r.month = 'current';
                        return r;
                    });

                    var totalContactValue = 0;
                    for (var i = userData.length; i--;) {
                        totalContactValue += userData[i].contactValue;
                    }
                    //Using this function to fetch data for ONLY the revenue details. Called only when flag 'explain' is true
                    if (explain) {
                        processRevenueRiskDetails(hierarchy, userData, interactionsMap1, interactionsMap2, function(data) {
                            callback(data)
                        });
                    } else {

                        var userDataTemp = groupByThenSum(userData, 'company', 'contactValue');

                        var maxContactValue = Math.max.apply(Math, userDataTemp.map(function(o) {
                            return o.contactValue;
                        }));
                        var minContactValue = Math.min.apply(Math, userDataTemp.map(function(o) {
                            return o.contactValue;
                        }));

                        userDataTemp = _.map(userDataTemp, function(d) {
                            var k = {
                                company: d.company,
                                contactValue: d.contactValue,
                                contactValueScale: scaleBetween(d.contactValue, minContactValue, maxContactValue)
                            }
                            return k;
                        });

                        interactionsMap1 = groupByThenSum2(interactionsMap1, 'company', 'interactionCount', 'month');
                        interactionsMap2 = groupByThenSum2(interactionsMap2, 'company', 'interactionCount', 'month');

                        var interactionGrowth = _.chain(interactionsMap1.concat(interactionsMap2)) //use chain
                            .groupBy(function(d) {
                                return d.company;
                            }) //grouping by email
                            .map(function(d) {

                                var current = _.last(d); //take the last in the group

                                var diff = 0;
                                if (d.length >= 2) {
                                    var prev = _.first(d);
                                    diff = current.interactionCount - prev.interactionCount;
                                } else if (d[0].month === 'prev') {
                                    diff = -d[0].interactionCount
                                } else {
                                    diff = d[0].interactionCount
                                }

                                var k = {
                                    //email: current.email,
                                    company: current.company,
                                    interactionCount: diff
                                };
                                return k;
                            }).value();

                        //Get min and max interactions count after grouping by company
                        var maxInteractionsCount = Math.max.apply(Math, interactionGrowth.map(function(o) {
                            return o.interactionCount;
                        }));
                        var minInteractionsCount = Math.min.apply(Math, interactionGrowth.map(function(o) {
                            return o.interactionCount;
                        }));

                        interactionGrowth = _.map(interactionGrowth, function(d) {
                            var j = {
                                company: d.company,
                                interactionCount: d.interactionCount,
                                interactionCountScale: scaleBetween(d.interactionCount, minInteractionsCount, maxInteractionsCount)
                            }
                            return j;
                        })

                        //The default interaction count scale will be used when the interaction growth is zero
                        var defaultInteractionCountScale = scaleBetween(0, minInteractionsCount, maxInteractionsCount);

                        var interactionsWeightage = 0.1;
                        var contactValueWeightage = 0.9;

                        var weightedArr = _.chain(interactionGrowth.concat(userDataTemp)) //use chain
                            .groupBy(function(d) {
                                return d.company;
                            }) //grouping by email
                            .map(function(d) {
                                var last = _.last(d); //take the last in the group
                                var risk;
                                var interactionGrowthTemp = 0;

                                if (d.length >= 2) {
                                    var first = _.first(d);
                                    risk = (last.contactValueScale * contactValueWeightage) - (first.interactionCountScale * interactionsWeightage);
                                    interactionGrowthTemp = first.interactionCount;
                                } else {
                                    //first is undefined as there are no interactions for this company. Make it as zero.
                                    //This will make sure that the risk factor is always calculated when a company has in process values.
                                    risk = last.contactValueScale * contactValueWeightage - defaultInteractionCountScale * interactionsWeightage;
                                    interactionGrowthTemp = 0;
                                }

                                if (last.company) {
                                    var n = {
                                        //fullName: last.contactFullName,
                                        company: last.company,
                                        contactValue: Math.round(last.contactValue),
                                        interactionGrowth: interactionGrowthTemp,
                                        riskProbability: risk
                                    }
                                    return n;
                                }
                            }).value()

                        weightedArr = weightedArr.filter(function(el) {
                            return (typeof el !== "undefined");
                        });

                        var valueAtRisk = 0;

                        for (var i = weightedArr.length; i--;) {
                            if (weightedArr[i].interactionGrowth <= 0) {
                                valueAtRisk += weightedArr[i].contactValue;
                            }
                        }

                        var sortedArr = _.sortBy(weightedArr, function(o) {
                            return -o.riskProbability;
                        });

                        if (sortedArr.length > 9) {
                            sortedArr.length = 10;
                        } else {
                            sortedArr = sortedArr.splice(0, 10);
                        }

                        if (!err) {
                            callback(sortedArr, totalContactValue, valueAtRisk)
                        } else {
                            callback(err)
                        }
                    }
                }
            })
        });
    })
};

function getRevenueAtRiskWithNoInteractionsPastSixtyDays(emailIds) {

    var todayDate = new Date();

    var q = {
        $and: [
            { "contacts.personEmailId": { $in: emailIds } }, {
                "contacts.account.name": { $ne: null },
                $or: [
                    { "contacts.account.ignore": { $lte: todayDate } },
                    { "contacts.account.ignore": { $exists: false } }
                ]
            }
        ]
    }

    myUserCollection.aggregate([{
        $match: { _id: { $in: hierarchy } }
    }, {
        $unwind: "$contacts"
    }, {
        $match: q
    }, {
        $project: {
            _id: "$_id",
            "fullName": "$contacts.personName",
            "company": "$contacts.account.name",
            "designation": "$contacts.designation",
            "relatasUser": "$contacts.relatasUser",
            "baseLocation": "$contacts.location",
            "personId": "$contacts.personId",
            "personEmailId": "$contacts.personEmailId",
            "contactId": "$contacts._id",
            "twitterUserName": "$twitterUserName",
            "contactRelation": "$contacts.contactRelation",
            "ownerId": "$_id",
            "ownerFirstName": "$firstName",
            "ownerLastName": "$lastName",
            "ownerEmailId": "$emailId",
            "contactValue": { $cond: { if: "$contacts.account.value.inProcess", then: "$contacts.account.value.inProcess", else: 0 } }
        }
    }]).exec(function(err, userData) {


    })

}

function processRevenueRiskDetails(hierarchy, userData, interactionsMap1, interactionsMap2, callback) {

    var interactionsWeightage = 0.1;
    var contactValueWeightage = 0.9;

    var interactionsConcat = _.chain(interactionsMap1.concat(interactionsMap2)) //use chain
        .groupBy(function(d) {
            return d.personEmailId;
        }) //grouping by email
        .map(function(d) {
            var current = _.last(d); //take the last in the group

            var diff = 0;
            if (d.length >= 2) {
                var prev = _.first(d);
                diff = current.interactionCount - prev.interactionCount;
            } else if (d[0].month === 'prev') {
                diff = -d[0].interactionCount
            } else {
                diff = d[0].interactionCount
            }

            var k = {
                contactEmail: current.personEmailId,
                interactionGrowth: diff
            };
            return k;
        }).value()

    var interactionsContacts = _.map(interactionsConcat, function(a) {
        return _.extend(a, _.find(userData, function(b) {
            return a.contactEmail === b.personEmailId;
        }))
    });

    var weightedArr = _.chain(interactionsConcat.concat(userData)) //use chain
        .groupBy(function(d) {
            return d.personEmailId;
        }) //grouping by email
        .map(function(d) {

            //if(d[0].contactValue>0){
            var n = {
                contactEmail: d[0].contactEmail || d[0].personEmailId,
                _id: d[0]._id,
                interactionGrowth: d[0].interactionGrowth || null,
                fullName: d[0].fullName,
                company: d[0].company,
                relatasUser: d[0].relatasUser,
                personId: d[0].personId,
                personEmailId: d[0].personEmailId,
                contactMobileNumber: d[0].contactMobileNumber,
                contactId: d[0].contactId,
                contactImageLink: d[0].contactImageLink,
                contactRelation: d[0].contactRelation || null,
                ownerId: d[0].ownerId,
                ownerFirstName: d[0].ownerFirstName,
                ownerLastName: d[0].ownerLastName,
                ownerEmailId: d[0].ownerEmailId,
                contactValue: d[0].contactValue,
                baseLocation: d[0].baseLocation || null
            }
            return n;
            //}
        }).value()


    var interactionsContacts = weightedArr.filter(function(el) {
        return (typeof el !== "undefined");
    });

    var emailIds = _.pluck(userData, "personEmailId")

    getPublicProfileUrl(emailIds, function(err, publicProfileData) {

        var appendPublicProfileUrl = _.map(interactionsContacts, function(a) {
            return _.extend(a, _.find(publicProfileData, function(b) {
                return a.contactEmail === b.emailId;
            }))
        });

        var userGroup = _
            .chain(userData)
            .groupBy('_id')
            .map(function(value, key) {
                return {
                    ownerId: castToObjectId(key),
                    emailArray: _.pluck(value, 'personEmailId')
                }
            })
            .value();
        //build the query to fetch data

        var queryFinal = {
            "$or": userGroup.map(function(el) {
                el["emailId"] = { "$in": el.emailArray };
                el["interactionType"] = { $nin: [null, ''] }
                delete el.emailArray;
                return el;
            })
        };

        var maxContactValue = Math.max.apply(Math, appendPublicProfileUrl.map(function(o) {
            return o.contactValue;
        }));
        var minContactValue = Math.min.apply(Math, appendPublicProfileUrl.map(function(o) {
            return o.contactValue;
        }));

        var maxInteractionGrowth = Math.max.apply(Math, appendPublicProfileUrl.map(function(o) {
            return o.interactionGrowth;
        }));
        var minInteractionGrowth = Math.min.apply(Math, appendPublicProfileUrl.map(function(o) {
            return o.interactionGrowth;
        }));

        var riskC = _.map(appendPublicProfileUrl, function(d) {
            d.risk = (d.contactValue * contactValueWeightage) - (d.interactionGrowth * interactionsWeightage);
            return d;
        });

        var sortedArr = _.sortBy(riskC, function(o) {
            return -o.risk;
        });

        getInteractionsByType(hierarchy, queryFinal, function(interactions) {

            if (interactions.length > 0) {
                var appendTwitter = _.map(sortedArr, function(a) {
                    return _.extend(a, _.find(interactions, function(b) {
                        return a.email === b._id;
                    }))
                });

                callback(appendTwitter);

            } else {
                callback(sortedArr);
            }
        });
    });
}

function calculateRevenueRisk(companyMembers, userData, explain, getTopTenCompaniesAtRisk, callback) {
    var emailIds = _.pluck(userData, 'personEmailId');
    var dateRange1 = { $gte: moment().subtract(60, "days").toDate(), $lte: moment().subtract(30, "days").toDate() }
    var dateRange2 = { $gte: moment().subtract(30, "days").toDate(), $lte: moment().subtract(0, "days").toDate() }

    interactionsByRange(companyMembers, emailIds, dateRange1, function(interactionsRange1) {
        interactionsByRange(companyMembers, emailIds, dateRange2, function(interactionsRange2) {

            var userCompanyFromContactsObject = {};
            _.each(userData, function(u) {
                userCompanyFromContactsObject[u.personEmailId] = u.company
            });

            var interactionsMap1 = _.map(interactionsRange1, function(r) {
                r.company = userCompanyFromContactsObject[r.personEmailId] || r.company || "Others"
                r.month = 'prev'
                return r;
            })

            var interactionsMap2 = _.map(interactionsRange2, function(r) {
                r.company = userCompanyFromContactsObject[r.personEmailId] || r.company || "Others"
                r.month = 'current'
                return r;
            })

            var totalContactValue = 0;
            var len = userData.length;
            for (var i = len; i--;) {
                totalContactValue += userData[i].contactValue;
            }

            //Using this function to fetch data for ONLY the revenue details. Called only when flag 'explain' is true
            if (explain) {
                processRevenueRiskDetails(userData, interactionsMap1, interactionsMap2, function(data) {
                    callback(data)
                });
            } else {

                var userDataTemp = groupByThenSum(userData, 'company', 'contactValue');

                var maxContactValue = Math.max.apply(Math, userDataTemp.map(function(o) {
                    return o.contactValue;
                }));

                var minContactValue = Math.min.apply(Math, userDataTemp.map(function(o) {
                    return o.contactValue;
                }));

                userDataTemp = _.map(userDataTemp, function(d) {

                    var k = {
                        contactFullName: d.contactFullName,
                        //email: d.email,
                        company: d.company,
                        contactValue: d.contactValue,
                        contactValueScale: scaleBetween(d.contactValue, minContactValue, maxContactValue)
                    }
                    return k;
                });

                interactionsMap1 = groupByThenSum2(interactionsMap1, 'company', 'interactionCount', 'month');
                interactionsMap2 = groupByThenSum2(interactionsMap2, 'company', 'interactionCount', 'month');

                interactionGrowth = _.chain(interactionsMap1.concat(interactionsMap2)) //use chain
                    .groupBy(function(d) {
                        return d.company;
                    }) //grouping by email
                    .map(function(d) {

                        var current = _.last(d); //take the last in the group

                        var diff = 0;
                        if (d.length >= 2) {
                            var prev = _.first(d);
                            diff = current.interactionCount - prev.interactionCount;
                        } else if (d[0].month === 'prev') {
                            diff = -d[0].interactionCount
                        } else {
                            diff = d[0].interactionCount
                        }

                        var k = {
                            //email: last.email,
                            company: current.company,
                            interactionCount: diff
                        };
                        return k;
                    }).value();

                //Get min and max interactions count after grouping by company
                var maxInteractionsCount = Math.max.apply(Math, interactionGrowth.map(function(o) {
                    return o.interactionCount;
                }));
                var minInteractionsCount = Math.min.apply(Math, interactionGrowth.map(function(o) {
                    return o.interactionCount;
                }));

                var interactionGrowth = _.map(interactionGrowth, function(d) {
                    var k = {
                        company: d.company,
                        interactionCount: d.interactionCount,
                        interactionCountScale: scaleBetween(d.interactionCount, minInteractionsCount, maxInteractionsCount)
                    }
                    return k;
                })

                var interactionsWeightage = 0.1;
                var contactValueWeightage = 0.9;
                //The default interaction count scale will be used when the interaction growth is zero
                var defaultInteractionCountScale = scaleBetween(0, minInteractionsCount, maxInteractionsCount);

                var weightedArr = _.chain(interactionGrowth.concat(userDataTemp)) //use chain
                    .groupBy(function(d) {
                        return d.company;
                    }) //grouping by email
                    .map(function(d) {
                        var last = _.last(d); //take the last in the group
                        var risk = 0;
                        var interactionGrowthTemp = 0;

                        if (d.length >= 2) {
                            var first = _.first(d);
                            risk = (last.contactValueScale * contactValueWeightage) - (first.interactionCountScale * interactionsWeightage);
                            interactionGrowthTemp = first.interactionCount
                        } else {
                            //first is undefined as there are no interactions for this company. Make it as zero.
                            //This will make sure that the risk factor is always calculated when a company has in process values.
                            risk = last.contactValueScale * contactValueWeightage - defaultInteractionCountScale * interactionsWeightage;
                            interactionGrowthTemp = 0
                        }

                        var k = {
                            fullName: last.contactFullName,
                            company: last.company,
                            contactValue: Math.round(last.contactValue),
                            interactionGrowth: interactionGrowthTemp,
                            //interactionCount: diff //Scale 1-99
                            riskProbability: risk
                        };
                        return k;
                    }).value()

                var valueAtRisk = 0;
                for (var i = weightedArr.length; i--;) {
                    if (weightedArr[i].interactionGrowth < 0) {
                        valueAtRisk += weightedArr[i].contactValue;
                    }
                }

                if (getTopTenCompaniesAtRisk) {
                    var sortedArr = _.sortBy(weightedArr, function(o) {
                        return -o.riskProbability;
                    });

                    sortedArr = sortedArr.splice(0, 10);

                    callback(sortedArr, totalContactValue, valueAtRisk)

                } else {
                    callback(totalContactValue, valueAtRisk)
                }

            }
        })
    });
}

function interactionsByRange(hierarchy, emailIds, dateRange, callback) {

    InteractionsCollection.aggregate([{
        $match: { ownerId: { $in: hierarchy } }
    }, {
        $match: {
            "emailId": { $in: emailIds },
            "interactionDate": dateRange
        }
    }, {
        $match: {
            $and: [{ "emailId": { $nin: [null, ''] } }, { "userId": { $nin: hierarchy } }]

        }
    }, {
        $group: {
            _id: "$emailId",
            count: { $sum: 1 }
        }
    },

    {
        $sort: { "count": -1 }
    }, {
        $project: {
            personEmailId: "$_id",
            interactionCount: "$count"
        }
    }]).exec(function(err, interLastThirtyDays) {

        if (!err) {
            callback(interLastThirtyDays)
        } else {
            callback(err)
        }
    });
}

function getCompanyRelationshipRiskDetailHierarchy(err, result, callback) {
    var user = [];
    var hierarchy = [];
    var temp1 = [];
    var temp2 = [];
    var temp3 = [];
    user = _
        .chain(result.value)
        .groupBy('_id')
        .value()
    _.each(user, function(value, key) {
        temp1.push({ _id: key, data: value })
    });
    _.each(temp1, function(ele) {
        var _id = ele._id;
        var userEmail = null;
        var userName = null;
        var profileUrl = null;
        var url = null;
        var mobileNumber = null;
        var data = [];
        ele.data.forEach(function(cont) {
            userEmail = cont.userEmail;
            userName = cont.userFname + ' ' + cont.userLname;
            profileUrl = cont.profileUrl;
            url = cont.url;
            mobileNumber = cont.mobileNumber;
            data.push({ companyName: cont.companyName, email: cont.email, contactName: cont.contactName, interaction: cont.interaction, contactValue: cont.contactValue, contactRelation: cont.contactRelation })
        });
        temp2.push({ _id: _id, userEmail: userEmail, userName: userName, mobileNumber: mobileNumber, profileUrl: profileUrl, url: url, data: data })
    });

    _.each(temp2, function(ele) {
        var data = [];
        ele.data = _
            .chain(ele.data)
            .groupBy('companyName')
            .value()

        _.each(ele.data, function(value, key) {
            data.push({ company: key, value: value });
        })
        ele.data = data;
    });

    _.each(temp2, function(ele) {
        var _id = ele._id;
        var userEmail = ele.userEmail;
        var userName = ele.userName;
        var profileUrl = ele.profileUrl;
        var mobileNumber = ele.mobileNumber;
        var data = [];
        var url = ele.url;
        ele.data.forEach(function(comp) {
            var company = comp.company;
            var interaction = 0;
            var value = 0;
            var contact = [];
            comp.value.forEach(function(val) {
                value += val.contactValue;
                interaction += val.interaction;
                contact.push({ email: val.email, contactName: val.contactName, contactRelation: val.contactRelation })
            });
            data.push({ company: company, interaction: interaction, value: value, noOfContact: contact.length, contact: contact })
        });
        temp3.push({ _id: _id, userEmail: userEmail, userName: userName, mobileNumber: mobileNumber, profileUrl: profileUrl, url: url, data: data });
    });
    // hierarchy = temp2;
    _.each(temp3, function(ele) {
        ele.data.forEach(function(merg) {
            merg._id = ele._id;
            merg.userEmail = ele.userEmail;
            merg.userName = ele.userName;
            merg.profileUrl = ele.profileUrl;
            merg.url = ele.url;
            merg.mobileNumber = ele.mobileNumber;
            hierarchy.push(merg);
        });
    });
    hierarchy.sort(function(a, b) {
        return b.company > a.company;
    });

    var hierarchyDetail = _.sortBy(hierarchy, 'company').reverse();
    callback(err, hierarchyDetail);
}

function scaleBetween(unscaledNum, min, max) {

    var minAllowed = 1;
    var maxAllowed = 100;

    if (min == 0 && max == 0) {
        return minAllowed
    } else {
        return (maxAllowed - minAllowed) * (unscaledNum - min) / (max - min) + minAllowed;
    }
}

function groupByThenSum(array, col, value) {
    var r = [],
        o = {};
    array.forEach(function(a) {
        if (!o[a[col]]) {
            o[a[col]] = {};
            o[a[col]][col] = a[col];
            o[a[col]][value] = 0;
            r.push(o[a[col]]);
        }
        o[a[col]][value] += +a[value];
    });
    return r;
};

function groupByThenSum2(array, col, value, month) {
    var r = [],
        o = {};
    array.forEach(function(a) {
        if (!o[a[col]]) {
            o[a[col]] = {};
            o[a[col]][col] = a[col];
            o[a[col]][value] = 0;
            o[a[col]][month] = 0;
            r.push(o[a[col]]);
        }
        o[a[col]][month] = a[month];
        o[a[col]][value] += +a[value];
    });
    return r;
};

function getLosingTouchForTeam(teamMembers, callback) {

    var userIds = _.pluck(teamMembers,"_id")

    var query = { "$or": teamMembers.map(function(el) {
        var obj = {};
        obj["userId"] = el._id
        el["contacts.score"] = { $lte: 50 };
        return obj;
    })};

    losingTouch.aggregate([
        {
            $match: {
                userId: { $in: userIds }
            }
        },
        {
            $unwind: "$contacts"
        },
        {
            $match: query
        },
        {
            $group: {
                _id: "$userEmailId",
                ltCount: { $sum: 1 }
            }
        }
    ]).exec(function(err, data) {
        callback(err,data)
    });

}

function getLosingTouchForUsersContacts(userIds,userContactGroup,callback){

    var queryFinal = { "$or": userContactGroup.map(function(el) {
        var obj = {}
        obj["userEmailId"] = el.userEmailId
        obj["contacts.score"] = {$lt:50}
        obj["contacts.personEmailId"] = { "$in": el.contacts };

        return obj;
    })};
    
    losingTouch.aggregate([
        {
            $match: { userId: { $in: userIds } }
        },
        {
            $unwind: "$contacts"
        },
        {
            $match: queryFinal
        },
        {
            $group: {
                _id:{ownerEmailId:"$userEmailId",emailId:"$contacts.personEmailId"}
            }
        }
        //}
    ]).exec(function(err, count) {
        callback(err,count)
    })
}

insightsManagement.prototype.getLosingTouchContactsGroup = function (userIds,userContactGroup,callback){

    // var queryFinal = { "$or": userContactGroup.map(function(el) {
    //     var obj = {}
    //     obj["userEmailId"] = el.userEmailId
    //     obj["contacts.score"] = {$lt:50}
    //     // obj["contacts.personEmailId"] = { "$in": el.contacts };
    //
    //     return obj;
    // })};

    var queryFinal = {"contacts.score": {$lt:50}}

    losingTouch.aggregate([
        {
            $match: { userId: { $in: userIds } }
        },
        {
            $unwind: "$contacts"
        },
        {
            $match: queryFinal
        },
        {
            $group: {
                _id:{ownerEmailId:"$userEmailId"},
                contacts:{
                    $addToSet: "$contacts.personEmailId"
                }
            }
        }
        //}
    ]).exec(function(err, count) {
        callback(err,count)
    })
}

module.exports = insightsManagement;
