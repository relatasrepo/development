var enterpriseSignUp = require('../databaseSchema/userManagementSchema').enterpriseSignUp;
var mobileAppLaunch = require('../databaseSchema/userManagementSchema').mobileAppLaunch;
var relationshipCartoon = require('../databaseSchema/userManagementSchema').relationshipCartoon;

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston();

function EnterpriseSignUpRequest()
{
  this.saveRequestedUser = function(details,callback){
     var user = constructEnterpriseSignUpRequest(details);
      enterpriseSignUp.findOne({emailId:details.emailId},function(error,userInfo){
          if(error){
             callback(false);
          }
          else if(userInfo){
             callback('exist');
          }
          else{
              user.save(function(error,savedUser){
                    callback(true);
              })
          }
      });
  }
    this.saveMobileAppSubscriber = function(details,callback){
        mobileAppLaunch.findOne({emailId:details.emailId},function(error,userInfo){
            if(error){
                callback(false);
            }
            else if(userInfo){
                callback('exist');
            }
            else{
                var user = new mobileAppLaunch({
                    emailId:details.emailId,
                    iosApp:details.iosApp,
                    androidApp:details.androidApp,
                    windowsApp:details.windowsApp,
                    createdDate:new Date()
                });
                user.save(function(error,savedUser){
                    callback(true);
                })
            }
        });
    }

    this.relationshipCartoonSave = function(details,callback){

        relationshipCartoon.findOne({email:details.email},function(error,userInfo){
            if(error){
                callback(false);
            }
            else if(userInfo){
                callback('exist');
            }
            else{
                var user = new relationshipCartoon({
                    email:details.email,
                    name:details.name,
                    gender:details.gender,
                    emailNumbers:details.emailsNumber,
                    callNumbers:details.callsNumber,
                    meetingNumbers:details.meetingsNumber
                });

                user.save(function(error,savedUser){
                    callback(true);
                })
            }
        });
    }
}

function constructEnterpriseSignUpRequest(details){
    var date = new Date();
    var user = new enterpriseSignUp({

            firstName:details.firstName,
            lastName:details.lastName,
            emailId:details.emailId,
            companyName:details.companyName,
            phoneNumber:details.phoneNumber,
            skypeId:details.skypeId,
           createdDate:date
    });
    return user;
}

EnterpriseSignUpRequest.prototype.saveWebsiteLead = function(details,callback) {
    enterpriseSignUp.collection.insert(details,function (err,result) {
        callback(err,result)
    })
}

EnterpriseSignUpRequest.prototype.getWebsiteLead = function(callback) {
    enterpriseSignUp.find({},function (err,result) {
        callback(err,result)
    })
}

module.exports = EnterpriseSignUpRequest;