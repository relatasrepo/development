/**
 * Created by elavarasan on 5/11/15.
 */
var meeting = require('../databaseSchema/userManagementSchema').scheduleInvitation;
var events=require('../databaseSchema/calendarSchema');
var list =require('../databaseSchema/calendarListSchema');
var winstonLog = require('../common/winstonLog');

var logLib = new winstonLog();
var logger = logLib.getWinston();

function MeetingMigrate(){
    this.runMeetingMigration = function(){
        logger.info('Creating List started **********------------*************');
        var stream = meeting.find().stream();
        stream.on('data', function (doc) {
            stream.pause();
            checkAndUpdate(doc,function(){
                stream.resume();
                logger.info('List created ',doc.senderEmailId, 'completed-------------------');
            })
        });
        stream.on('error', function (err) {
            // handle err
            logger.info('Error while creating Data');
        });

        stream.on('close', function () {
            // all done
            logger.info('List creation completed **********------------*************');
        })
    };
    this.migrateCalendar=function(){
        logger.info('Meeting Migrate started **********------------*************');
        var stream = meeting.find().stream();
        stream.on('data', function (doc) {
            stream.pause();
            updateMeeting(doc,function(){
                stream.resume();
                logger.info('Meeting ',doc.invitationId, 'completed-------------------');
            })
        });
        stream.on('error', function (err) {
            // handle err
            logger.info('Error while Migrating Data');
        });

        stream.on('close', function () {
            // all done
            logger.info('Meeting Migration completed **********------------*************');
        })
    }
}

function updateMeeting(doc,callback) {
    list.findOne({email: doc.senderEmailId}, function (err, l) {
        if (!err) {
            events.findOne({invitationId: doc.invitationId},
                function (error, event) {
                    if (!error) {
                        if(event===null){
                            var evt=new events(doc);
                            evt._list= l._id;
                            evt.save(function(error,evnt){
                                if(!error){
                                    if (l.calendar.indexOf(evnt._id) == -1) {
                                        l.calendar.push(evnt._id);
                                        l.save(function (q) {
                                            if (!q) {
                                                callback();
                                            } else {
                                                logger.info(q);
                                                callback();
                                            }
                                        });
                                    }else{
                                        callback();
                                    }
                                }
                                else {
                                    callback();
                                }
                            });
                        }else{
                            if (l.calendar.indexOf(event._id) == -1) {
                                l.calendar.push(event._id);
                                l.save(function (q) {
                                    if (!q) {
                                        callback();
                                    } else {
                                        logger.info(q);
                                        callback();
                                    }
                                });
                            }else{
                                callback();
                            }
                        }

                    } else {
                        logger.info(error);
                        callback();
                    }
                });
        }else{
            logger.info('server error');
            callback();
        }
    });
}
function checkAndUpdate(doc,callback){
    list.findOne({email:doc.senderEmailId},function(err,l) {
        if (!err) {
            if (l===null) {
                var calendarList = new list({
                    name: 'Primary',
                    email: doc.senderEmailId,
                    userId: doc.senderId,
                    color: 'Red'
                });
                calendarList.save(function (e, cal) {
                    if (!e) {
                        logger.info('list created');
                        callback();
                    }
                })
            }else{
                logger.info('list not null');
                callback();
            }
        }else {
            logger.info('error in list find');
            callback();
        }
    });
}
module.exports=MeetingMigrate;