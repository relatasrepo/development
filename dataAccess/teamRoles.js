
var orgRoleCollection = require('../databaseSchema/orgRoleSchema').orgRoles;
var opportunitiesCollection = require('../databaseSchema/userManagementSchema').opportunities

var async = require("async");

var winstonLog = require('../common/winstonLog');
var Mailer = require('../common/mailer');

var logLib = new winstonLog();
var mailObj = new Mailer();

var logger =logLib.getInteractionsErrorLogger();
var _ = require("lodash")
    , moment = require("moment")

var momentTZ = require('moment-timezone');

var redis = require('redis');
var redisClient = redis.createClient();

function OrgRoleCollection(){}

OrgRoleCollection.prototype.addOrUpdateRole = function (companyId,role,castToObjectId,callback) {

    var updateObj = {
        users:role.users,
        roleName:role.roleName,
        companyId:role.companyId?castToObjectId(role.companyId):castToObjectId(companyId)
    }

    if(!role.prevRoleName){
        role.prevRoleName = role.roleName
    }

    orgRoleCollection.findOne({companyId:castToObjectId(companyId),roleName:role.prevRoleName}).lean().exec(function (err,prevRole) {
        orgRoleCollection.update({companyId:companyId,roleName:role.prevRoleName},{$set:updateObj},{upsert:true},function (err,results) {
            updateRoleName(castToObjectId(companyId),role);
            updateUserAccessGroup(castToObjectId(companyId),role,prevRole)
            callback(err,results)
        })
    })
}

function updateRoleName(companyId,role,callback) {
    opportunitiesCollection.update({companyId:companyId,"usersWithAccess.accessGroup": role.prevRoleName},
        {$set: {"usersWithAccess.$.accessGroup": role.roleName}},
        {multi: true}).exec(function (errO,result) {
    });
}

function updateUserAccessGroup(companyId,role,prevRole) {


    var prevUsersWithAccess = prevRole && prevRole.users?_.pluck(prevRole.users,"emailId"):[];
    var nowUsersWithAccess = _.pluck(role.users,"emailId");
    var usersWithNoAccess = _.xor(prevUsersWithAccess,nowUsersWithAccess);
    var bulk = opportunitiesCollection.collection.initializeUnorderedBulkOp();

    _.each(usersWithNoAccess,function (emailId) {
        bulk.find({companyId:companyId,"usersWithAccess.accessGroup": role.prevRoleName})
            .update({$pull: {usersWithAccess: {emailId: emailId}}});
    });

    bulk.execute();

}

OrgRoleCollection.prototype.deleteRole = function (companyId,roleName,callback) {

    opportunitiesCollection.update({companyId:companyId},
        {$pull: {usersWithAccess: {accessGroup: roleName}}},
        {multi: true}).exec(function (errO,result) {

        orgRoleCollection.remove({companyId:companyId,roleName:roleName},function (err,results) {

            if(!err){
                opportunitiesCollection.update({$or:[{companyId:companyId},{companyId:String(companyId)}]},
                    { $pull: { 'usersWithAccess': { accessGroup: roleName } } },function (errO,results_opp) {
                    })
            }
            callback(err,results)
        })
    })

}

OrgRoleCollection.prototype.getAllRoles = function (companyId,callback) {

    orgRoleCollection.find({companyId:companyId}).lean().exec(function (err,results) {

        if(!err && results && results.length>0){
            _.each(results,function (el) {
                el.createdDate = el._id.getTimestamp()
            })
        }

        callback(err,results)
    })

}

module.exports = OrgRoleCollection;