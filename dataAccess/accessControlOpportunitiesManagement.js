var accessControlData = require('../databaseSchema/accessControlOpportunitieSchema').accessControlOpportunities;
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var opportunitiesCollection = require('../databaseSchema/userManagementSchema').opportunities;

var commonUtility = require('../common/commonUtility');

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston();
var _ = require("lodash");
var common = new commonUtility();

function AccessControlOpportunities() {}

// *************************************************************************
// Function                     :  getSecondaryHierarchy
// Input Parameters             :  secondary Hierarchy Type
// Output Parameter             :
// Functionality                :  returns the
//
// *************************************************************************
AccessControlOpportunities.prototype.getAllAccessControlOpportunities= function (companyId,
                                                                                 userId,
                                                                                 callback){
    accessControlData.findOne({companyId:companyId,
        userId:userId},{allACOpportunities:1}).lean().exec(function (err,result) {
            var oppIds = [];
            if(!err && result && result.allACOpportunities){
                oppIds = result.allACOpportunities;
            }
        callback(err,oppIds)
    });
}

// *************************************************************************
// Function                     :  getSecondaryHierarchy
// Input Parameters             :  secondary Hierarchy Type
// Output Parameter             :
// Functionality                :  returns the
//
// *************************************************************************
AccessControlOpportunities.prototype.getAllAccessControlData= function (companyId,
                                                                        userId,
                                                                        callback){


    accessControlData.find({companyId:companyId,
        userId:userId}).lean().exec(function (err,result) {
        callback(err,result)
    });
}

AccessControlOpportunities.prototype.updateAccessControlOpportunitiesOrgHierarchy = function (companyId,
                                                                                              userId,
                                                                                              oppList,
                                                                                              callback){
    accessControlData.update({companyId:companyId,
            userId:userId},
        {$set:{allAccessControlOpportunities:oppList}},{upsert:true}).exec(function (err,result) {
        callback(err,result)
    });
}

// *************************************************************************
// Function                     :
// Input Parameters             :  secondary Hierarchy Type
// Output Parameter             :
// Functionality                :  update the single user Opportunity and all its RMs when the Opportunity is created,
//
// *************************************************************************
AccessControlOpportunities.prototype.updateAccessControlOpportunitiesForUserAndRMs= function (updateAccessControlData,
                                                                                              callback){
    var objUserId = null;
    var newAccessControlData = [{companyId: null,
        userId: null,
        allACOpportunities:[],
        orgHierarchyACOpportunities:[],
        revenueACOpportunities:[],
        productACOpportunities:[],
        businessUnitACOpportunities:[],
        regionACOpportunities:[],
        verticalACOpportunities:[],
        solutionACOpportunities:[],
        typeACOpportunities:[],

        allACReportingManagers:[],
        orgHierarchyACReportingManagers:[],
        revenueHierarchyACReportingManagers:[],
        productHierarchyACReportingManagers:[],
        businessUnitHierarchyACReportingManagers:[],
        regionHierarchyACReportingManagers:[],
        verticalHierarchyACReportingManagers:[],
        solutionHierarchyACReportingManagers:[],
        typeHierarchyACReportingManagers:[],

        allACTeamMembers:[],
        orgHierarchyACTeamMembers:[],
        revenueHierarchyACTeamMembers:[],
        productHierarchyACTeamMembers:[],
        businessUnitHierarchyACTeamMembers:[],
        regionHierarchyACTeamMembers:[],
        verticalHierarchyACTeamMembers:[],
        solutionHierarchyACTeamMembers:[],
        typeHierarchyACTeamMembers:[],


    }];

    var newAllACOpportunities = [];
    var bulkACDataUpdate = accessControlData.collection.initializeUnorderedBulkOp();

    this.getAccessControlDataForUserIds(updateAccessControlData.companyId, updateAccessControlData.userIds, function (err, existingACDataList){
        if(existingACDataList.length > 0){
            _.each(existingACDataList, function(existingACData){

                // reset the newAccessControlData Object
                newAccessControlData.companyId = null;
                newAccessControlData.userId = null;
                newAccessControlData.orgHierarchyACOpportunities = [];
                newAccessControlData.revenueACOpportunities = [];
                newAccessControlData.productACOpportunities = [];
                newAccessControlData.businessUnitACOpportunities = [];
                newAccessControlData.regionACOpportunities = [];
                newAccessControlData.verticalACOpportunities = [];
                newAccessControlData.solutionACOpportunities = [];
                newAccessControlData.typeACOpportunities = [];

                newAccessControlData.companyId = existingACData.companyId;
                newAccessControlData.userId = existingACData.userId;
                newAccessControlData.allACReportingManagers = existingACData.allACReportingManagers;
                newAccessControlData.orgHierarchyACReportingManagers = existingACData.orgHierarchyACReportingManagers;
                newAccessControlData.revenueHierarchyACReportingManagers = existingACData.revenueHierarchyACReportingManagers;
                newAccessControlData.productHierarchyACReportingManagers = existingACData.productHierarchyACReportingManagers;
                newAccessControlData.businessUnitHierarchyACReportingManagers = existingACData.businessUnitHierarchyACReportingManagers;
                newAccessControlData.regionHierarchyACReportingManagers = existingACData.regionHierarchyACReportingManagers;
                newAccessControlData.solutionHierarchyACReportingManagers = existingACData.solutionHierarchyACReportingManagers;
                newAccessControlData.typeHierarchyACReportingManagers = existingACData.typeHierarchyACReportingManagers;

                newAccessControlData.allACTeamMembers = existingACData.allACTeamMembers;
                newAccessControlData.orgHierarchyACTeamMembers = existingACData.orgHierarchyACTeamMembers;
                newAccessControlData.revenueHierarchyACTeamMembers = existingACData.revenueHierarchyACTeamMembers;
                newAccessControlData.productHierarchyACTeamMembers = existingACData.productHierarchyACTeamMembers;
                newAccessControlData.businessUnitHierarchyACTeamMembers = existingACData.businessUnitHierarchyACTeamMembers;
                newAccessControlData.regionHierarchyACTeamMembers = existingACData.regionHierarchyACTeamMembers;
                newAccessControlData.solutionHierarchyACTeamMembers = existingACData.solutionHierarchyACTeamMembers;
                newAccessControlData.typeHierarchyACTeamMembers = existingACData.typeHierarchyACTeamMembers;


                // if ((existingACData.orgHierarchyACOpportunities.length > 0){
                //     newAccessControlData.orgHierarchyACOpportunities = existingACData.orgHierarchyACOpportunities;
                // }

                // newAccessControlData.orgHierarchyACOpportunities = existingACData.orgHierarchyACOpportunities;
                // if(updateAccessControlData.orgHierarchyACOpportunities.length > 0 && updateAccessControlData.shType == 'Organization'){
                //     newAccessControlData.orgHierarchyACOpportunities =   _.uniq(newAccessControlData.orgHierarchyACOpportunities.concat(updateAccessControlData.orgHierarchyACOpportunities));
                //     newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.orgHierarchyACOpportunities);
                // }

                if (existingACData.revenueACOpportunities.length > 0){
                    newAccessControlData.revenueACOpportunities = _.uniq(existingACData.revenueACOpportunities);
                    newAccessControlData.revenueACOpportunities = _.uniq(newAccessControlData.revenueACOpportunities);

                }
                if(updateAccessControlData.revenueACOpportunities.length > 0 && updateAccessControlData.shType == 'Revenue'){
                    // newAccessControlData.revenueACOpportunities =  _.uniq(newAccessControlData.revenueACOpportunities.concat(updateAccessControlData.revenueACOpportunities));
                    // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.revenueACOpportunities);
                    newAccessControlData.revenueACOpportunities = _.uniq(updateAccessControlData.revenueACOpportunities);
                    newAccessControlData.revenueACOpportunities = _.uniq(newAccessControlData.revenueACOpportunities);


                }

                if (existingACData.productACOpportunities.length > 0){
                    newAccessControlData.productACOpportunities = _.uniq(existingACData.productACOpportunities);
                }
                if(updateAccessControlData.productACOpportunities.length > 0 && updateAccessControlData.shType == 'Product'){
                    // newAccessControlData.productACOpportunities =  _.uniq(newAccessControlData.productACOpportunities.concat(updateAccessControlData.productACOpportunities));
                    // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.productACOpportunities);
                    newAccessControlData.productACOpportunities = updateAccessControlData.productACOpportunities;
                }

                if (existingACData.businessUnitACOpportunities.length > 0){
                    newAccessControlData.businessUnitACOpportunities = _.uniq(existingACData.businessUnitACOpportunities);
                }
                newAccessControlData.businessUnitACOpportunities = existingACData.businessUnitACOpportunities;
                if(updateAccessControlData.businessUnitACOpportunities.length > 0 && updateAccessControlData.shType == 'BusinessUnit'){
                    // newAccessControlData.businessUnitACOpportunities =  _.uniq(newAccessControlData.businessUnitACOpportunities.concat(updateAccessControlData.businessUnitACOpportunities));
                    // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.businessUnitACOpportunities);
                    newAccessControlData.businessUnitACOpportunities = updateAccessControlData.businessUnitACOpportunities;

                }

                if (existingACData.regionACOpportunities.length > 0){
                    newAccessControlData.regionACOpportunities = _.uniq(existingACData.regionACOpportunities);
                }
                if(updateAccessControlData.regionACOpportunities.length > 0 && updateAccessControlData.shType == 'Region'){
                    // newAccessControlData.regionACOpportunities =  _.uniq(newAccessControlData.regionACOpportunities.concat(updateAccessControlData.regionACOpportunities));
                    // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.regionACOpportunities);
                    newAccessControlData.regionACOpportunities = updateAccessControlData.regionACOpportunities;
                }

                if (existingACData.verticalACOpportunities.length > 0){
                    newAccessControlData.verticalACOpportunities = _.uniq(existingACData.verticalACOpportunities);
                }
                if(updateAccessControlData.verticalACOpportunities.length > 0 && updateAccessControlData.shType == 'Vertical'){
                    // newAccessControlData.verticalACOpportunities =  _.uniq(newAccessControlData.verticalACOpportunities.concat(updateAccessControlData.verticalACOpportunities));
                    // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.verticalACOpportunities);
                    newAccessControlData.verticalACOpportunities = updateAccessControlData.verticalACOpportunities;

                }

                if (existingACData.solutionACOpportunities.length > 0){
                    newAccessControlData.solutionACOpportunities = _.uniq(existingACData.solutionACOpportunities);
                }
                newAccessControlData.solutionACOpportunities = existingACData.solutionACOpportunities;
                if(updateAccessControlData.solutionACOpportunities.length > 0 && updateAccessControlData.shType == 'Solution'){
                    // newAccessControlData.solutionACOpportunities =  _.uniq(newAccessControlData.solutionACOpportunities.concat(updateAccessControlData.solutionACOpportunities));
                    // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.solutionACOpportunities);
                    newAccessControlData.solutionACOpportunities = updateAccessControlData.solutionACOpportunities;
                }

                if (existingACData.typeACOpportunities.length > 0){
                    newAccessControlData.typeACOpportunities = _.uniq(existingACData.typeACOpportunities);
                }
                newAccessControlData.typeACOpportunities = existingACData.typeACOpportunities;
                if(updateAccessControlData.typeACOpportunities.length > 0 && updateAccessControlData.shType == 'Type'){
                    // newAccessControlData.typeACOpportunities =  _.uniq(newAccessControlData.typeACOpportunities.concat(updateAccessControlData.typeACOpportunities));
                    // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.typeACOpportunities);
                    newAccessControlData.solutionACOpportunities = updateAccessControlData.solutionACOpportunities;
                }

                // if (existingACData.allACOpportunities.length > 0){
                //     newAccessControlData.allACOpportunities = existingACData.allACOpportunities;
                // }

                // newAccessControlData.allACOpportunities = existingACData.allACOpportunities;
                // if(updateAccessControlData.allACOpportunities.length > 0){
                //     newAccessControlData.allACOpportunities =  _.uniq(newAccessControlData.allACOpportunities.concat(updateAccessControlData.allACOpportunities));
                // newAccessControlData.allACOpportunities =  _.uniq(newAccessControlData.allACOpportunities.concat(newAllACOpportunities));
                // }

                bulkACDataUpdate.find({
                    companyId:newAccessControlData.companyId,
                    userId:newAccessControlData.userId,
                })
                    .upsert()
                    .updateOne(
                        {$set:{
                                companyId:newAccessControlData.companyId,
                                userId:newAccessControlData.userId,
                                allACOpportunities:newAccessControlData.allACOpportunities,
                                orgHierarchyACOpportunities:newAccessControlData.orgHierarchyACOpportunities,
                                revenueACOpportunities:newAccessControlData.revenueACOpportunities,
                                productACOpportunities:newAccessControlData.productACOpportunities,
                                businessUnitACOpportunities:newAccessControlData.businessUnitACOpportunities,
                                regionACOpportunities:newAccessControlData.regionACOpportunities,
                                verticalACOpportunities:newAccessControlData.verticalACOpportunities,
                                solutionACOpportunities:newAccessControlData.solutionACOpportunities,
                                typeACOpportunities:newAccessControlData.typeACOpportunities,

                                allACReportingManagers:newAccessControlData.allACReportingManagers,
                                orgHierarchyACReportingManagers:newAccessControlData.orgHierarchyACReportingManagers,
                                revenueHierarchyACReportingManagers:newAccessControlData.revenueHierarchyACReportingManagers,
                                productHierarchyACReportingManagers:newAccessControlData.productHierarchyACReportingManagers,
                                businessUnitHierarchyACReportingManagers:newAccessControlData.businessUnitHierarchyACReportingManagers,
                                regionHierarchyACReportingManagers:newAccessControlData.regionHierarchyACReportingManagers,
                                verticalHierarchyACReportingManagers:newAccessControlData.verticalHierarchyACReportingManagers,
                                solutionHierarchyACReportingManagers:newAccessControlData.solutionHierarchyACReportingManagers,
                                typeHierarchyACReportingManagers:newAccessControlData.typeHierarchyACReportingManagers,

                                allACTeamMembers:newAccessControlData.allACTeamMembers,
                                orgHierarchyACTeamMembers:newAccessControlData.orgHierarchyACTeamMembers,
                                revenueHierarchyACTeamMembers:newAccessControlData.revenueHierarchyACTeamMembers,
                                productHierarchyACTeamMembers:newAccessControlData.productHierarchyACTeamMembers,
                                businessUnitHierarchyACTeamMembers:newAccessControlData.businessUnitHierarchyACTeamMembers,
                                regionHierarchyACTeamMembers:newAccessControlData.regionHierarchyACTeamMembers,
                                verticalHierarchyACTeamMembers:newAccessControlData.verticalHierarchyACTeamMembers,
                                solutionHierarchyACTeamMembers:newAccessControlData.solutionHierarchyACTeamMembers,
                                typeHierarchyACTeamMembers:newAccessControlData.typeHierarchyACTeamMembers,
                            }}
                    );
            });


            if(updateAccessControlData.allACTeamMembers.length > 0) {

                bulkACDataUpdate.execute(function (err, result) {
                    if (err) {
                        // logger.info('Error in updateAccessControlOpportunitiesForUserAndRMs() ',err );
                    }
                    else {
                        // result = result.toJSON();
                        // logger.info('updateAccessControlOpportunitiesForUserAndRMs() results ',result);
                    }
                    callback(err)

                });
            }
            else{
                callback(true)
            }

        } else {

            _.each(updateAccessControlData.userIds,function (el) {
                bulkACDataUpdate.find({
                    companyId:updateAccessControlData.companyId,
                    userId:el,
                })
                    .upsert()
                    .updateOne(
                        {$set:{
                                companyId:updateAccessControlData.companyId,
                                userId:el,
                                allACOpportunities:updateAccessControlData.allACOpportunities,
                                orgHierarchyACOpportunities:updateAccessControlData.orgHierarchyACOpportunities,
                                revenueACOpportunities:updateAccessControlData.revenueACOpportunities,
                                productACOpportunities:updateAccessControlData.productACOpportunities,
                                businessUnitACOpportunities:updateAccessControlData.businessUnitACOpportunities,
                                regionACOpportunities:updateAccessControlData.regionACOpportunities,
                                verticalACOpportunities:updateAccessControlData.verticalACOpportunities,
                                solutionACOpportunities:updateAccessControlData.solutionACOpportunities,
                                typeACOpportunities:updateAccessControlData.typeACOpportunities,

                                allACReportingManagers:updateAccessControlData.allACReportingManagers,
                                orgHierarchyACReportingManagers:updateAccessControlData.orgHierarchyACReportingManagers,
                                revenueHierarchyACReportingManagers:updateAccessControlData.revenueHierarchyACReportingManagers,
                                productHierarchyACReportingManagers:updateAccessControlData.productHierarchyACReportingManagers,
                                businessUnitHierarchyACReportingManagers:updateAccessControlData.businessUnitHierarchyACReportingManagers,
                                regionHierarchyACReportingManagers:updateAccessControlData.regionHierarchyACReportingManagers,
                                verticalHierarchyACReportingManagers:updateAccessControlData.verticalHierarchyACReportingManagers,
                                solutionHierarchyACReportingManagers:updateAccessControlData.solutionHierarchyACReportingManagers,
                                typeHierarchyACReportingManagers:updateAccessControlData.typeHierarchyACReportingManagers,

                                allACTeamMembers:updateAccessControlData.allACTeamMembers,
                                orgHierarchyACTeamMembers:updateAccessControlData.orgHierarchyACTeamMembers,
                                revenueHierarchyACTeamMembers:updateAccessControlData.revenueHierarchyACTeamMembers,
                                productHierarchyACTeamMembers:updateAccessControlData.productHierarchyACTeamMembers,
                                businessUnitHierarchyACTeamMembers:updateAccessControlData.businessUnitHierarchyACTeamMembers,
                                regionHierarchyACTeamMembers:updateAccessControlData.regionHierarchyACTeamMembers,
                                verticalHierarchyACTeamMembers:updateAccessControlData.verticalHierarchyACTeamMembers,
                                solutionHierarchyACTeamMembers:updateAccessControlData.solutionHierarchyACTeamMembers,
                                typeHierarchyACTeamMembers:updateAccessControlData.typeHierarchyACTeamMembers,
                            }}
                    );
            });

            // if(updateAccessControlData.allACTeamMembers.length > 0) {

            bulkACDataUpdate.execute(function (err, result) {
                if (err) {
                    logger.info('Error in updateAccessControlOpportunitiesForUserAndRMs() ',err );
                    callback(false)
                }
                else {
                    result = result.toJSON();
                    logger.info('updateAccessControlOpportunitiesForUserAndRMs() results ',result);
                    callback(result.upserted)
                }
            });
            // }
            // else{
            //     callback(true)
            // }
        }

    })
    //
    // if(updateAccessControlData.allACTeamMembers.length > 0) {
    //
    //     bulkACDataUpdate.execute(function (err, result) {
    //         if (err) {
    //             logger.info('Error in updateAccessControlOpportunitiesForUserAndRMs() ',err );
    //             callback(false)
    //         }
    //         else {
    //             result = result.toJSON();
    //             logger.info('updateAccessControlOpportunitiesForUserAndRMs() results ',result);
    //             callback(result.upserted)
    //         }
    //     });
    // }
    // else{
    //     callback(true)
    // }
}


// *************************************************************************
// Function                     :
// Input Parameters             :  secondary Hierarchy Type
// Output Parameter             :
// Functionality                :  update the single user Opportunity and all its RMs when the Opportunity is created,
//
// *************************************************************************
AccessControlOpportunities.prototype.updateAccessControlOpportunitiesForUserSHType= function (updateAccessControlData,
                                                                                              shType,
                                                                                              callback){
    var objUserId = null;
    var newAccessControlData = [{companyId: null,
        userId: null,
        allACOpportunities:[],
        orgHierarchyACOpportunities:[],
        revenueACOpportunities:[],
        productACOpportunities:[],
        businessUnitACOpportunities:[],
        regionACOpportunities:[],
        verticalACOpportunities:[],
        solutionACOpportunities:[],
        typeACOpportunities:[],

        allACReportingManagers:[],
        orgHierarchyACReportingManagers:[],
        revenueHierarchyACReportingManagers:[],
        productHierarchyACReportingManagers:[],
        businessUnitHierarchyACReportingManagers:[],
        regionHierarchyACReportingManagers:[],
        verticalHierarchyACReportingManagers:[],
        solutionHierarchyACReportingManagers:[],
        typeHierarchyACReportingManagers:[],

        allACTeamMembers:[],
        orgHierarchyACTeamMembers:[],
        revenueHierarchyACTeamMembers:[],
        productHierarchyACTeamMembers:[],
        businessUnitHierarchyACTeamMembers:[],
        regionHierarchyACTeamMembers:[],
        verticalHierarchyACTeamMembers:[],
        solutionHierarchyACTeamMembers:[],
        typeHierarchyACTeamMembers:[],


    }];

    var newAllACOpportunities = [];


    var updateQuery = {};


    var bulkACDataUpdate = accessControlData.collection.initializeUnorderedBulkOp();
    _.each(updateAccessControlData.userIds,function (el) {
        updateQuery = {};
        updateQuery = {"companyId": updateAccessControlData.companyId,
            "userId": el,}

        if(shType == "Revenue") {
            updateQuery = {
                "revenueACOpportunities": updateAccessControlData.revenueACOpportunities,
                "revenueHierarchyACReportingManagers": updateAccessControlData.revenueHierarchyACReportingManagers,
                "revenueHierarchyACTeamMembers": updateAccessControlData.revenueHierarchyACTeamMembers,
            }
        }else if(shType == 'Product'){
            updateQuery = {
                "productACOpportunities": updateAccessControlData.productACOpportunities,
                "productHierarchyACReportingManagers": updateAccessControlData.productHierarchyACReportingManagers,
                "productHierarchyACTeamMembers": updateAccessControlData.productHierarchyACTeamMembers,
            }
        }else if(shType == 'Vertical'){
            updateQuery = {
                "verticalACOpportunities": updateAccessControlData.verticalACOpportunities,
                "verticalHierarchyACReportingManagers": updateAccessControlData.verticalHierarchyACReportingManagers,
                "verticalHierarchyACTeamMembers": updateAccessControlData.verticalHierarchyACTeamMembers,
            }
        }


        bulkACDataUpdate.find({
            companyId:updateAccessControlData.companyId,
            userId:el,
        })
            .upsert()
            .updateOne(
                {$set:updateQuery }
            );
    });

    // if(updateAccessControlData.allACTeamMembers.length > 0) {

    bulkACDataUpdate.execute(function (err, result) {
        if (err) {
            logger.info('Error in updateAccessControlOpportunitiesForUserAndRMs() ',err );
            callback(false)
        }
        else {
            result = result.toJSON();
            logger.info('updateAccessControlOpportunitiesForUserAndRMs() results ',result);
            callback(result.upserted)
        }
    });

    // this.getAccessControlDataForUserIds(updateAccessControlData.companyId, updateAccessControlData.userIds, function (err, existingACDataList){
    //     if(existingACDataList.length > 0){
    //         _.each(existingACDataList, function(existingACData){
    //
    //             // reset the newAccessControlData Object
    //             newAccessControlData.companyId = null;
    //             newAccessControlData.userId = null;
    //             newAccessControlData.orgHierarchyACOpportunities = [];
    //             newAccessControlData.revenueACOpportunities = [];
    //             newAccessControlData.productACOpportunities = [];
    //             newAccessControlData.businessUnitACOpportunities = [];
    //             newAccessControlData.regionACOpportunities = [];
    //             newAccessControlData.verticalACOpportunities = [];
    //             newAccessControlData.solutionACOpportunities = [];
    //             newAccessControlData.typeACOpportunities = [];
    //
    //             newAccessControlData.companyId = existingACData.companyId;
    //             newAccessControlData.userId = existingACData.userId;
    //             newAccessControlData.allACReportingManagers = existingACData.allACReportingManagers;
    //             newAccessControlData.orgHierarchyACReportingManagers = existingACData.orgHierarchyACReportingManagers;
    //             newAccessControlData.revenueHierarchyACReportingManagers = existingACData.revenueHierarchyACReportingManagers;
    //             newAccessControlData.productHierarchyACReportingManagers = existingACData.productHierarchyACReportingManagers;
    //             newAccessControlData.businessUnitHierarchyACReportingManagers = existingACData.businessUnitHierarchyACReportingManagers;
    //             newAccessControlData.regionHierarchyACReportingManagers = existingACData.regionHierarchyACReportingManagers;
    //             newAccessControlData.solutionHierarchyACReportingManagers = existingACData.solutionHierarchyACReportingManagers;
    //             newAccessControlData.typeHierarchyACReportingManagers = existingACData.typeHierarchyACReportingManagers;
    //
    //             newAccessControlData.allACTeamMembers = existingACData.allACTeamMembers;
    //             newAccessControlData.orgHierarchyACTeamMembers = existingACData.orgHierarchyACTeamMembers;
    //             newAccessControlData.revenueHierarchyACTeamMembers = existingACData.revenueHierarchyACTeamMembers;
    //             newAccessControlData.productHierarchyACTeamMembers = existingACData.productHierarchyACTeamMembers;
    //             newAccessControlData.businessUnitHierarchyACTeamMembers = existingACData.businessUnitHierarchyACTeamMembers;
    //             newAccessControlData.regionHierarchyACTeamMembers = existingACData.regionHierarchyACTeamMembers;
    //             newAccessControlData.solutionHierarchyACTeamMembers = existingACData.solutionHierarchyACTeamMembers;
    //             newAccessControlData.typeHierarchyACTeamMembers = existingACData.typeHierarchyACTeamMembers;
    //
    //
    //             // if ((existingACData.orgHierarchyACOpportunities.length > 0){
    //             //     newAccessControlData.orgHierarchyACOpportunities = existingACData.orgHierarchyACOpportunities;
    //             // }
    //
    //             // newAccessControlData.orgHierarchyACOpportunities = existingACData.orgHierarchyACOpportunities;
    //             // if(updateAccessControlData.orgHierarchyACOpportunities.length > 0 && updateAccessControlData.shType == 'Organization'){
    //             //     newAccessControlData.orgHierarchyACOpportunities =   _.uniq(newAccessControlData.orgHierarchyACOpportunities.concat(updateAccessControlData.orgHierarchyACOpportunities));
    //             //     newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.orgHierarchyACOpportunities);
    //             // }
    //
    //             if (existingACData.revenueACOpportunities.length > 0){
    //                 newAccessControlData.revenueACOpportunities = _.uniq(existingACData.revenueACOpportunities);
    //                 newAccessControlData.revenueACOpportunities = _.uniq(newAccessControlData.revenueACOpportunities);
    //
    //             }
    //             if(updateAccessControlData.revenueACOpportunities.length > 0 && updateAccessControlData.shType == 'Revenue'){
    //                 // newAccessControlData.revenueACOpportunities =  _.uniq(newAccessControlData.revenueACOpportunities.concat(updateAccessControlData.revenueACOpportunities));
    //                 // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.revenueACOpportunities);
    //                 newAccessControlData.revenueACOpportunities = _.uniq(updateAccessControlData.revenueACOpportunities);
    //                 newAccessControlData.revenueACOpportunities = _.uniq(newAccessControlData.revenueACOpportunities);
    //
    //
    //             }
    //
    //
    //             if (existingACData.productACOpportunities.length > 0){
    //
    //                 newAccessControlData.productACOpportunities = _.uniq(existingACData.productACOpportunities);
    //             }
    //             if(updateAccessControlData.productACOpportunities.length > 0 && updateAccessControlData.shType == 'Product'){
    //                 // newAccessControlData.productACOpportunities =  _.uniq(newAccessControlData.productACOpportunities.concat(updateAccessControlData.productACOpportunities));
    //                 // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.productACOpportunities);
    //                 newAccessControlData.productACOpportunities = updateAccessControlData.productACOpportunities;
    //             }
    //
    //             if (existingACData.businessUnitACOpportunities.length > 0){
    //                 newAccessControlData.businessUnitACOpportunities = _.uniq(existingACData.businessUnitACOpportunities);
    //             }
    //             newAccessControlData.businessUnitACOpportunities = existingACData.businessUnitACOpportunities;
    //             if(updateAccessControlData.businessUnitACOpportunities.length > 0 && updateAccessControlData.shType == 'BusinessUnit'){
    //                 // newAccessControlData.businessUnitACOpportunities =  _.uniq(newAccessControlData.businessUnitACOpportunities.concat(updateAccessControlData.businessUnitACOpportunities));
    //                 // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.businessUnitACOpportunities);
    //                 newAccessControlData.businessUnitACOpportunities = updateAccessControlData.businessUnitACOpportunities;
    //
    //             }
    //
    //             if (existingACData.regionACOpportunities.length > 0){
    //                 newAccessControlData.regionACOpportunities = _.uniq(existingACData.regionACOpportunities);
    //             }
    //             if(updateAccessControlData.regionACOpportunities.length > 0 && updateAccessControlData.shType == 'Region'){
    //                 // newAccessControlData.regionACOpportunities =  _.uniq(newAccessControlData.regionACOpportunities.concat(updateAccessControlData.regionACOpportunities));
    //                 // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.regionACOpportunities);
    //                 newAccessControlData.regionACOpportunities = updateAccessControlData.regionACOpportunities;
    //             }
    //
    //
    //             if (existingACData.verticalACOpportunities.length > 0){
    //                 newAccessControlData.verticalACOpportunities = _.uniq(existingACData.verticalACOpportunities);
    //             }
    //             if(updateAccessControlData.verticalACOpportunities.length > 0 && updateAccessControlData.shType == 'Vertical'){
    //                 // newAccessControlData.verticalACOpportunities =  _.uniq(newAccessControlData.verticalACOpportunities.concat(updateAccessControlData.verticalACOpportunities));
    //                 // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.verticalACOpportunities);
    //                 newAccessControlData.verticalACOpportunities = updateAccessControlData.verticalACOpportunities;
    //
    //             }
    //
    //             if (existingACData.solutionACOpportunities.length > 0){
    //                 newAccessControlData.solutionACOpportunities = _.uniq(existingACData.solutionACOpportunities);
    //             }
    //             newAccessControlData.solutionACOpportunities = existingACData.solutionACOpportunities;
    //             if(updateAccessControlData.solutionACOpportunities.length > 0 && updateAccessControlData.shType == 'Solution'){
    //                 // newAccessControlData.solutionACOpportunities =  _.uniq(newAccessControlData.solutionACOpportunities.concat(updateAccessControlData.solutionACOpportunities));
    //                 // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.solutionACOpportunities);
    //                 newAccessControlData.solutionACOpportunities = updateAccessControlData.solutionACOpportunities;
    //             }
    //
    //             if (existingACData.typeACOpportunities.length > 0){
    //                 newAccessControlData.typeACOpportunities = _.uniq(existingACData.typeACOpportunities);
    //             }
    //             newAccessControlData.typeACOpportunities = existingACData.typeACOpportunities;
    //             if(updateAccessControlData.typeACOpportunities.length > 0 && updateAccessControlData.shType == 'Type'){
    //                 // newAccessControlData.typeACOpportunities =  _.uniq(newAccessControlData.typeACOpportunities.concat(updateAccessControlData.typeACOpportunities));
    //                 // newAllACOpportunities = newAllACOpportunities.concat(newAccessControlData.typeACOpportunities);
    //                 newAccessControlData.solutionACOpportunities = updateAccessControlData.solutionACOpportunities;
    //             }
    //
    //             // if (existingACData.allACOpportunities.length > 0){
    //             //     newAccessControlData.allACOpportunities = existingACData.allACOpportunities;
    //             // }
    //
    //             // newAccessControlData.allACOpportunities = existingACData.allACOpportunities;
    //             // if(updateAccessControlData.allACOpportunities.length > 0){
    //             //     newAccessControlData.allACOpportunities =  _.uniq(newAccessControlData.allACOpportunities.concat(updateAccessControlData.allACOpportunities));
    //             // newAccessControlData.allACOpportunities =  _.uniq(newAccessControlData.allACOpportunities.concat(newAllACOpportunities));
    //             // }
    //
    //             bulkACDataUpdate.find({
    //                 companyId:newAccessControlData.companyId,
    //                 userId:newAccessControlData.userId,
    //             })
    //                 .upsert()
    //                 .updateOne(
    //                     {$set:{
    //                             companyId:newAccessControlData.companyId,
    //                             userId:newAccessControlData.userId,
    //                             allACOpportunities:newAccessControlData.allACOpportunities,
    //                             orgHierarchyACOpportunities:newAccessControlData.orgHierarchyACOpportunities,
    //                             revenueACOpportunities:newAccessControlData.revenueACOpportunities,
    //                             productACOpportunities:newAccessControlData.productACOpportunities,
    //                             businessUnitACOpportunities:newAccessControlData.businessUnitACOpportunities,
    //                             regionACOpportunities:newAccessControlData.regionACOpportunities,
    //                             verticalACOpportunities:newAccessControlData.verticalACOpportunities,
    //                             solutionACOpportunities:newAccessControlData.solutionACOpportunities,
    //                             typeACOpportunities:newAccessControlData.typeACOpportunities,
    //
    //                             allACReportingManagers:newAccessControlData.allACReportingManagers,
    //                             orgHierarchyACReportingManagers:newAccessControlData.orgHierarchyACReportingManagers,
    //                             revenueHierarchyACReportingManagers:newAccessControlData.revenueHierarchyACReportingManagers,
    //                             productHierarchyACReportingManagers:newAccessControlData.productHierarchyACReportingManagers,
    //                             businessUnitHierarchyACReportingManagers:newAccessControlData.businessUnitHierarchyACReportingManagers,
    //                             regionHierarchyACReportingManagers:newAccessControlData.regionHierarchyACReportingManagers,
    //                             verticalHierarchyACReportingManagers:newAccessControlData.verticalHierarchyACReportingManagers,
    //                             solutionHierarchyACReportingManagers:newAccessControlData.solutionHierarchyACReportingManagers,
    //                             typeHierarchyACReportingManagers:newAccessControlData.typeHierarchyACReportingManagers,
    //
    //                             allACTeamMembers:newAccessControlData.allACTeamMembers,
    //                             orgHierarchyACTeamMembers:newAccessControlData.orgHierarchyACTeamMembers,
    //                             revenueHierarchyACTeamMembers:newAccessControlData.revenueHierarchyACTeamMembers,
    //                             productHierarchyACTeamMembers:newAccessControlData.productHierarchyACTeamMembers,
    //                             businessUnitHierarchyACTeamMembers:newAccessControlData.businessUnitHierarchyACTeamMembers,
    //                             regionHierarchyACTeamMembers:newAccessControlData.regionHierarchyACTeamMembers,
    //                             verticalHierarchyACTeamMembers:newAccessControlData.verticalHierarchyACTeamMembers,
    //                             solutionHierarchyACTeamMembers:newAccessControlData.solutionHierarchyACTeamMembers,
    //                             typeHierarchyACTeamMembers:newAccessControlData.typeHierarchyACTeamMembers,
    //                         }}
    //                 );
    //         });
    //
    //
    //         // if(updateAccessControlData.allACTeamMembers.length > 0) {
    //
    //             bulkACDataUpdate.execute(function (err, result) {
    //                 if (err) {
    //                     // logger.info('Error in updateAccessControlOpportunitiesForUserAndRMs() ',err );
    //                 }
    //                 else {
    //                     // result = result.toJSON();
    //                     // logger.info('updateAccessControlOpportunitiesForUserAndRMs() results ',result);
    //                 }
    //                 callback(err)
    //
    //             });
    //         // }
    //         // else{
    //         //     callback(true)
    //         // }
    //
    //     } else {
    //
    //         _.each(updateAccessControlData.userIds,function (el) {
    //             bulkACDataUpdate.find({
    //                 companyId:updateAccessControlData.companyId,
    //                 userId:el,
    //             })
    //                 .upsert()
    //                 .updateOne(
    //                     {$set:{
    //                             companyId:updateAccessControlData.companyId,
    //                             userId:el,
    //                             allACOpportunities:updateAccessControlData.allACOpportunities,
    //                             orgHierarchyACOpportunities:updateAccessControlData.orgHierarchyACOpportunities,
    //                             revenueACOpportunities:updateAccessControlData.revenueACOpportunities,
    //                             productACOpportunities:updateAccessControlData.productACOpportunities,
    //                             businessUnitACOpportunities:updateAccessControlData.businessUnitACOpportunities,
    //                             regionACOpportunities:updateAccessControlData.regionACOpportunities,
    //                             verticalACOpportunities:updateAccessControlData.verticalACOpportunities,
    //                             solutionACOpportunities:updateAccessControlData.solutionACOpportunities,
    //                             typeACOpportunities:updateAccessControlData.typeACOpportunities,
    //
    //                             allACReportingManagers:updateAccessControlData.allACReportingManagers,
    //                             orgHierarchyACReportingManagers:updateAccessControlData.orgHierarchyACReportingManagers,
    //                             revenueHierarchyACReportingManagers:updateAccessControlData.revenueHierarchyACReportingManagers,
    //                             productHierarchyACReportingManagers:updateAccessControlData.productHierarchyACReportingManagers,
    //                             businessUnitHierarchyACReportingManagers:updateAccessControlData.businessUnitHierarchyACReportingManagers,
    //                             regionHierarchyACReportingManagers:updateAccessControlData.regionHierarchyACReportingManagers,
    //                             verticalHierarchyACReportingManagers:updateAccessControlData.verticalHierarchyACReportingManagers,
    //                             solutionHierarchyACReportingManagers:updateAccessControlData.solutionHierarchyACReportingManagers,
    //                             typeHierarchyACReportingManagers:updateAccessControlData.typeHierarchyACReportingManagers,
    //
    //                             allACTeamMembers:updateAccessControlData.allACTeamMembers,
    //                             orgHierarchyACTeamMembers:updateAccessControlData.orgHierarchyACTeamMembers,
    //                             revenueHierarchyACTeamMembers:updateAccessControlData.revenueHierarchyACTeamMembers,
    //                             productHierarchyACTeamMembers:updateAccessControlData.productHierarchyACTeamMembers,
    //                             businessUnitHierarchyACTeamMembers:updateAccessControlData.businessUnitHierarchyACTeamMembers,
    //                             regionHierarchyACTeamMembers:updateAccessControlData.regionHierarchyACTeamMembers,
    //                             verticalHierarchyACTeamMembers:updateAccessControlData.verticalHierarchyACTeamMembers,
    //                             solutionHierarchyACTeamMembers:updateAccessControlData.solutionHierarchyACTeamMembers,
    //                             typeHierarchyACTeamMembers:updateAccessControlData.typeHierarchyACTeamMembers,
    //                         }}
    //                 );
    //         });
    //
    //         // if(updateAccessControlData.allACTeamMembers.length > 0) {
    //
    //         bulkACDataUpdate.execute(function (err, result) {
    //             if (err) {
    //                 logger.info('Error in updateAccessControlOpportunitiesForUserAndRMs() ',err );
    //                 callback(false)
    //             }
    //             else {
    //                 result = result.toJSON();
    //                 logger.info('updateAccessControlOpportunitiesForUserAndRMs() results ',result);
    //                 callback(result.upserted)
    //             }
    //         });
    //         // }
    //         // else{
    //         //     callback(true)
    //         // }
    //     }
    //
    // })
    //
    // if(updateAccessControlData.allACTeamMembers.length > 0) {
    //
    //     bulkACDataUpdate.execute(function (err, result) {
    //         if (err) {
    //             logger.info('Error in updateAccessControlOpportunitiesForUserAndRMs() ',err );
    //             callback(false)
    //         }
    //         else {
    //             result = result.toJSON();
    //             logger.info('updateAccessControlOpportunitiesForUserAndRMs() results ',result);
    //             callback(result.upserted)
    //         }
    //     });
    // }
    // else{
    //     callback(true)
    // }
}


AccessControlOpportunities.prototype.getAccessControlDataForUserIds = function (companyId,
                                                                                userIds,
                                                                                callback) {


    var match = {
        companyId:companyId,
        $or:[{userId:{$in:userIds}}]
    }

    var projectQuery = {
        "_id": "$_id",
        "companyId": "$companyId",
        "userId": "$userId",
        "allACOpportunities": "$allACOpportunities",
        "orgHierarchyACOpportunities": "$orgHierarchyACOpportunities",
        "revenueACOpportunities": "$revenueACOpportunities",
        "productACOpportunities": "$productACOpportunities",
        "businessUnitACOpportunities": "$businessUnitACOpportunities",
        "regionACOpportunities": "$regionACOpportunities",
        "verticalACOpportunities": "$verticalACOpportunities",
        "solutionACOpportunities": "$solutionACOpportunities",
        "typeACOpportunities": "$typeACOpportunities",
        "allACReportingManagers": "$allACReportingManagers",
        "orgHierarchyACReportingManagers": "$orgHierarchyACReportingManagers",
        "revenueHierarchyACReportingManagers": "$revenueHierarchyACReportingManagers",
        "productHierarchyACReportingManagers": "$productHierarchyACReportingManagers",
        "businessUnitHierarchyACReportingManagers": "$businessUnitHierarchyACReportingManagers",
        "regionHierarchyACReportingManagers": "$regionHierarchyACReportingManagers",
        "verticalHierarchyACReportingManagers": "$verticalHierarchyACReportingManagers",
        "solutionHierarchyACReportingManagers": "$solutionHierarchyACReportingManagers",
        "typeHierarchyACReportingManagers": "$typeHierarchyACReportingManagers",
        "allACTeamMembers": "$allACTeamMembers",
        "orgHierarchyACTeamMembers": "$orgHierarchyACTeamMembers",
        "revenueHierarchyACTeamMembers": "$revenueHierarchyACTeamMembers",
        "productHierarchyACTeamMembers": "$productHierarchyACTeamMembers",
        "businessUnitHierarchyACTeamMembers": "$businessUnitHierarchyACTeamMembers",
        "regionHierarchyACTeamMembers": "$regionHierarchyACTeamMembers",
        "verticalHierarchyACTeamMembers": "$verticalHierarchyACTeamMembers",
        "solutionHierarchyACTeamMembers": "$solutionHierarchyACTeamMembers",
        "typeHierarchyACTeamMembers": "$typeHierarchyACTeamMembers"

    }

    accessControlData.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        }
    ],function(error, result){
        if(error){
            callback(error,null);
        }
        else{

            callback(null,result);
        }
    });
}


AccessControlOpportunities.prototype.getAccessControlDataForUserIdsForSHType = function (companyId,
                                                                                         shType,
                                                                                         userId,
                                                                                         callback) {
    var match = {
        companyId:companyId,
        userId:userId
    }

    accessControlData.findOne(match).lean().exec(function (error, result) {
        if(!error && result){
            callback(null,filterHierarchy(result,shType));
        } else {
            callback(error,null);
        }
    });
}

function filterHierarchy(data,sHType){

    var obj = {
        opportunities:[],
        reportingManagers:[],
        teamMembers:[]
    }

    if(sHType === "Revenue"){
        obj.opportunities = data.revenueACOpportunities;
        obj.reportingManagers = data.revenueHierarchyACReportingManagers;
        obj.teamMembers = data.revenueHierarchyACTeamMembers;

    } else if (sHType === "Product"){
        obj.opportunities = data.productACOpportunities;
        obj.reportingManagers = data.productHierarchyACReportingManagers;
        obj.teamMembers = data.productHierarchyACTeamMembers;

    } else if (sHType === 'BusinessUnit'){
        obj.opportunities = data.businessUnitACOpportunities;
        obj.reportingManagers = data.businessUnitHierarchyACReportingManagers;
        obj.teamMembers = data.businessUnitHierarchyACTeamMembers;

    } else if (sHType === 'Region'){
        obj.opportunities = data.regionACOpportunities;
        obj.reportingManagers = data.regionHierarchyACReportingManagers;
        obj.teamMembers = data.regionHierarchyACTeamMembers;

    } else if (sHType === 'Vertical'){
        obj.opportunities = data.verticalACOpportunities;
        obj.reportingManagers = data.verticalHierarchyACReportingManagers;
        obj.teamMembers = data.verticalHierarchyACTeamMembers;

    } else if (sHType === 'Solution'){
        obj.opportunities = data.solutionACOpportunities;
        obj.reportingManagers = data.solutionHierarchyACReportingManagers;
        obj.teamMembers = data.solutionHierarchyACTeamMembers;

    } else if (sHType === 'Type'){
        obj.opportunities = data.typeACOpportunities;
        obj.reportingManagers = data.typeHierarchyACReportingManagers;
        obj.teamMembers = data.typeHierarchyACTeamMembers;
    } else if (sHType === 'All_Access'){
        obj.opportunities = obj.opportunities.concat(data.revenueACOpportunities);
        obj.opportunities = obj.opportunities.concat(data.productACOpportunities);
        obj.opportunities = obj.opportunities.concat(data.businessUnitACOpportunities);
        obj.opportunities = obj.opportunities.concat(data.regionACOpportunities);
        obj.opportunities = obj.opportunities.concat(data.verticalACOpportunities);
        obj.opportunities = obj.opportunities.concat(data.solutionACOpportunities);
        obj.opportunities = obj.opportunities.concat(data.typeACOpportunities);

        obj.reportingManagers = obj.reportingManagers.concat(data.revenueHierarchyACReportingManagers);
        obj.reportingManagers = obj.reportingManagers.concat(data.productHierarchyACReportingManagers);
        obj.reportingManagers = obj.reportingManagers.concat(data.businessUnitHierarchyACReportingManagers);
        obj.reportingManagers = obj.reportingManagers.concat(data.regionHierarchyACReportingManagers);
        obj.reportingManagers = obj.reportingManagers.concat(data.verticalHierarchyACReportingManagers);
        obj.reportingManagers = obj.reportingManagers.concat(data.solutionHierarchyACReportingManagers);
        obj.reportingManagers = obj.reportingManagers.concat(data.typeHierarchyACReportingManagers);

        obj.teamMembers = obj.teamMembers.concat(data.revenueHierarchyACTeamMembers);
        obj.teamMembers = obj.teamMembers.concat(data.productHierarchyACTeamMembers);
        obj.teamMembers = obj.teamMembers.concat(data.businessUnitHierarchyACTeamMembers);
        obj.teamMembers = obj.teamMembers.concat(data.regionHierarchyACTeamMembers);
        obj.teamMembers = obj.teamMembers.concat(data.verticalHierarchyACTeamMembers);
        obj.teamMembers = obj.teamMembers.concat(data.solutionHierarchyACTeamMembers);
        obj.teamMembers = obj.teamMembers.concat(data.typeHierarchyACTeamMembers);
    }

    return obj;
}


module.exports = AccessControlOpportunities;