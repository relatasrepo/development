var shCollection = require('../databaseSchema/secondaryHierarchySchema').secondaryHierarchy;
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var opportunitiesCollection = require('../databaseSchema/userManagementSchema').opportunities;
var accessControlData = require('../databaseSchema/accessControlOpportunitieSchema').accessControlOpportunities;
var myUserCollection = require('../databaseSchema/userManagementSchema').User;

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston();
var _ = require("lodash");
var async = require("async")

function SecondaryHierarchy() {}

// *************************************************************************
// Function                     :  getSecondaryHierarchy
// Input Parameters             :  secondary Hierarchy Type
// Output Parameter             :
// Functionality                :  returns the
//
// *************************************************************************
SecondaryHierarchy.prototype.getSecondaryHierarchy = function (companyId,
                                                               shType,
                                                               callback){

    shCollection.find({companyId:companyId,
        secondaryHierarchyType:shType}).lean().exec(function (err,result) {
        callback(err,result)
    });
}

// *************************************************************************
// Function                     :  getSecondaryHierarchy
// Input Parameters             :  secondary Hierarchy Type
// Output Parameter             :
// Functionality                :  returns the
//
// *************************************************************************
SecondaryHierarchy.prototype.getAllSecondaryHierarchyData = function (companyId,
                                                                      callback){

    shCollection.find({companyId:companyId}).lean().exec(function (err,result) {
        callback(err,result)
    });
}

// *************************************************************************
// Function                     :  getSecondaryHierarchy
// Input Parameters             :  secondary Hierarchy Type
// Output Parameter             :
// Functionality                :  returns the
//
// *************************************************************************
SecondaryHierarchy.prototype.getAllSecondaryHierarchyDataForUser = function (companyId,
                                                                             userId,
                                                                             shType,
                                                                             callback){
    shCollection.find({companyId:companyId,
        userId:userId,
        secondaryHierarchyType:shType,
    }).lean().exec(function (err,result) {
        callback(err,result)
    });
}

// *************************************************************************
// Function                     :  getSecondaryHierarchy
// Input Parameters             :  secondary Hierarchy Type
// Output Parameter             :
// Functionality                :  returns the
//
// *************************************************************************
SecondaryHierarchy.prototype.getAllSecondaryHierarchyDataForType = function (companyId,
                                                                             shType,
                                                                             callback){
    shCollection.find({companyId:companyId,
        secondaryHierarchyType:shType,
    }).lean().exec(function (err,result) {
        callback(err,result)
    });
}

// *************************************************************************
// Function                     :  getSecondaryHierarchy
// Input Parameters             :  secondary Hierarchy Type
// Output Parameter             :
// Functionality                :  returns the
//
// *************************************************************************
SecondaryHierarchy.prototype.getSecondaryHierarchyPath = function (companyId,
                                                                   userId,
                                                                   shType,
                                                                   callback){
    shCollection.find({companyId:companyId,
            secondaryHierarchyType:shType,
            userId:userId},
        {hierarchyPath:1},function(error, result){

            callback(error,result)
        })
}

// *************************************************************************
// Function                     :  getSecondaryHierarchy
// Input Parameters             :  secondary Hierarchy Type
// Output Parameter             :
// Functionality                :  returns the
//
// *************************************************************************
SecondaryHierarchy.prototype.getSecondaryHierarchyItems = function (companyId,
                                                                    userId,
                                                                    shType,
                                                                    callback){

    shCollection.find({companyId:companyId,
            secondaryHierarchyType:shType,
            userId:userId},
        {secondaryHierarchyItemsList:1},function(error, result){
            callback(error,result)
        })
}

// *************************************************************************
// Function                     :  updateSecondaryHierarchyMonthlyTargets
// Input Parameters             :  secondary Hierarchy Type,
//                                  Monthly Targets
// Output Parameter             :
// Functionality                :  returns the
//
// *************************************************************************
SecondaryHierarchy.prototype.updateSecondaryHierarchyMonthlyTargets = function (userId,
                                                                                shType,
                                                                                monthlyTargets,
                                                                                callback){
    shCollection.update({userId:userId,
            secondaryHierarchyType:shType},
        {$set:{monthlyTargets:monthlyTargets}}).exec(function (err,result) {
        callback(err,result)
    });
}

SecondaryHierarchy.prototype.updateHierarchyPaths = function (companyId,updateObj,common,callback){

    if(updateObj.childrenIds && updateObj.childrenIds.length>0){
        bulkHierarchyPathForSelf(companyId,common,updateObj,function (result) {
            bulkHierarchyPathForChildren(companyId,common,updateObj,function (response) {
                updateOppAccessSH(companyId,common,updateObj,callback)
            });
        });
    } else {
        bulkHierarchyPathForSelf(companyId,common,updateObj,function (result) {
            updateOppAccessSH(companyId,common,updateObj,callback)
        });
    }

}

SecondaryHierarchy.prototype.updateOppAccess = updateOppAccessSH;

function updateOppAccessSH(companyId,common,updateObj,callback) {
    getHierarchy(companyId,updateObj.secondaryHierarchyType,function (teamHierarchy) {
        getOppsGroupByUserId(companyId,teamHierarchy.userIds,updateObj.secondaryHierarchyType,updateObj.secondaryHierarchyItemsList,null,function(err,oppsObj){
            updateAccessControl(companyId,updateObj.secondaryHierarchyType,teamHierarchy.data,oppsObj,common,function () {
                setAllAccessForAccessControl(companyId,null,function(){
                    if(callback){
                        callback(true);
                    }
                });
            });
        });
    });
};

SecondaryHierarchy.prototype.updateOppAccessOrgHierarchy = updateOppAccessOrgHierarchy;

function updateOppAccessOrgHierarchy(companyId,common,updateObj,teamHierarchy,callback) {
    getOppsGroupByUserIdForOrg(companyId,teamHierarchy.userIds,function(err,oppsObj){
        updateAccessControl(companyId,'org',teamHierarchy.data,oppsObj,common,function () {
            setAllAccessForAccessControl(companyId,null,function(){
                callback(true);
            });
        });
    });
};

function setAllAccessForAccessControl(companyId,userIds,callback) {

    if(companyId || (userIds && userIds.length>0)){

        var findQuery = {
            companyId:companyId
        }

        if(companyId && userIds && userIds.length>0){
            findQuery = {
                companyId:companyId,
                userId:{$in:userIds}
            }
        }

        if(!companyId && userIds && userIds.length>0){
            findQuery = {
                userId:{$in:userIds}
            }
        }

        accessControlData.find(findQuery).lean().exec(function (err,result) {
            var bulk = accessControlData.collection.initializeUnorderedBulkOp();
            _.each(result,function (rs) {
                var allAcObj = composeAllAccessData(rs);

                bulk.find({
                    companyId:rs.companyId,
                    userId:rs.userId
                }).
                upsert().
                updateOne({
                    $set:{
                        allACOpportunities:allAcObj.allACOpportunities,
                        allACReportingManagers:allAcObj.allACReportingManagers,
                        allACTeamMembers:allAcObj.allACTeamMembers
                    }
                });
            });

            if(result.length > 0) {
                bulk.execute(function (err, upsert) {
                    if (err) {
                        logger.info('Error in updateAccessControl_bulk() ',err );
                        if(callback){
                            callback(false)
                        }
                    }
                    else {
                        upsert = upsert.toJSON();
                        logger.info('updateAccessControl_bulk() ',upsert);
                        if(callback){
                            callback(upsert.upserted)
                        }
                    }
                });
            } else{
                if(callback){
                    callback(true)
                }
            }
        });
    } else {
        if(callback){
            callback(false)
        }
    }
}

function composeAllAccessData(data) {

    var allACOpportunities= [],
        allACReportingManagers = [],
        allACTeamMembers = [];

    if(data.orgHierarchyACOpportunities){
        allACOpportunities = allACOpportunities.concat(data.orgHierarchyACOpportunities);
    }

    if(data.revenueACOpportunities){
        allACOpportunities = allACOpportunities.concat(data.revenueACOpportunities);
    }

    if(data.productACOpportunities){
        allACOpportunities = allACOpportunities.concat(data.productACOpportunities);
    }

    if(data.businessUnitACOpportunities){
        allACOpportunities = allACOpportunities.concat(data.businessUnitACOpportunities);
    }

    if(data.regionACOpportunities){
        allACOpportunities = allACOpportunities.concat(data.regionACOpportunities);
    }

    if(data.solutionACOpportunities){
        allACOpportunities = allACOpportunities.concat(data.solutionACOpportunities);
    }

    if(data.typeACOpportunities){
        allACOpportunities = allACOpportunities.concat(data.typeACOpportunities);
    }

    if(data.verticalACOpportunities){
        allACOpportunities = allACOpportunities.concat(data.verticalACOpportunities);
    }

    if(data.itACOpportunities){
        allACOpportunities = allACOpportunities.concat(data.itACOpportunities);
    }

    //...........End Of Opps...........//

    if(data.orgHierarchyACReportingManagers){
        allACReportingManagers = allACReportingManagers.concat(data.orgHierarchyACReportingManagers);
    }

    if(data.revenueHierarchyACReportingManagers){
        allACReportingManagers = allACReportingManagers.concat(data.revenueHierarchyACReportingManagers);
    }

    if(data.productHierarchyACReportingManagers){
        allACReportingManagers = allACReportingManagers.concat(data.productHierarchyACReportingManagers);
    }

    if(data.businessUnitHierarchyACReportingManagers){
        allACReportingManagers = allACReportingManagers.concat(data.businessUnitHierarchyACReportingManagers);
    }

    if(data.regionHierarchyACReportingManagers){
        allACReportingManagers = allACReportingManagers.concat(data.regionHierarchyACReportingManagers);
    }

    if(data.solutionHierarchyACReportingManagers){
        allACReportingManagers = allACReportingManagers.concat(data.solutionHierarchyACReportingManagers);
    }

    if(data.typeHierarchyACReportingManagers){
        allACReportingManagers = allACReportingManagers.concat(data.typeHierarchyACReportingManagers);
    }

    if(data.verticalHierarchyACReportingManagers){
        allACReportingManagers = allACReportingManagers.concat(data.verticalHierarchyACReportingManagers);
    }

    //...........End Of Reporting Managers...........//

    if(data.orgHierarchyACTeamMembers){
        allACTeamMembers= allACTeamMembers.concat(data.orgHierarchyACTeamMembers);
    }

    if(data.revenueHierarchyACTeamMembers){
        allACTeamMembers= allACTeamMembers.concat(data.revenueHierarchyACTeamMembers);
    }

    if(data.productHierarchyACTeamMembers){
        allACTeamMembers= allACTeamMembers.concat(data.productHierarchyACTeamMembers);
    }

    if(data.businessUnitHierarchyACTeamMembers){
        allACTeamMembers= allACTeamMembers.concat(data.businessUnitHierarchyACTeamMembers);
    }

    if(data.regionHierarchyACTeamMembers){
        allACTeamMembers= allACTeamMembers.concat(data.regionHierarchyACTeamMembers);
    }

    if(data.solutionHierarchyACTeamMembers){
        allACTeamMembers= allACTeamMembers.concat(data.solutionHierarchyACTeamMembers);
    }

    if(data.typeHierarchyACTeamMembers){
        allACTeamMembers= allACTeamMembers.concat(data.typeHierarchyACTeamMembers);
    }

    if(data.verticalHierarchyACTeamMembers){
        allACTeamMembers= allACTeamMembers.concat(data.verticalHierarchyACTeamMembers);
    }

    //...........End Of Team Members...........//

    return {
        allACOpportunities: allACOpportunities,
        allACReportingManagers: allACReportingManagers,
        allACTeamMembers: allACTeamMembers
    }
}

function updateAccessControl(companyId,sHType,users,oppsObj,common,callback) {

    var bulk = accessControlData.collection.initializeUnorderedBulkOp();

    _.each(users,function (user) {
        var oppsList = [];
        oppsList = oppsList.concat(oppsObj[user.userId]);

        var accessControlUserIds = [],
            allReportingManagers = [];

        if(user.reportingManagerUserIds && user.reportingManagerUserIds.length>0){
            allReportingManagers = user.reportingManagerUserIds;
        }

        if(user.teamMatesUserId && user.teamMatesUserId.length>0){
            accessControlUserIds = user.teamMatesUserId;
            _.each(user.teamMatesUserId,function (tm) {
                oppsList = oppsList.concat(oppsObj[tm]);
            })
        };

        var query = composeFindAndUpdateQuery(common.castToObjectId(user.userId),
            companyId,
            sHType,
            _.uniq(_.compact(oppsList)),
            accessControlUserIds,
            allReportingManagers);

        bulk.find(query.find).upsert().updateOne(query.update);
    });

    if(users.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in updateAccessControl_bulk() ',err );
                if(callback){
                    callback(false)
                }
            }
            else {
                result = result.toJSON();
                logger.info('updateAccessControl_bulk() results ',result);
                if(callback){
                    callback(result.upserted)
                }
            }
        });
    } else{
        if(callback){
            callback(true)
        }
    }
}

function composeFindAndUpdateQuery(userId,companyId,sHType,opps,accessControlUserIds,allReportingManagers){

    var findQuery = {
        userId:userId,
        companyId:companyId
    }

    var updateQuery = {
        $set:{
        }
    }

    if(sHType === "Revenue"){
        updateQuery.$set.revenueACOpportunities = opps;
        updateQuery.$set.revenueHierarchyACReportingManagers = allReportingManagers;
        updateQuery.$set.revenueHierarchyACTeamMembers = accessControlUserIds;

    } else if (sHType === "Product"){
        updateQuery.$set.productACOpportunities = opps;
        updateQuery.$set.productHierarchyACReportingManagers = allReportingManagers;
        updateQuery.$set.productHierarchyACTeamMembers = accessControlUserIds;

    } else if (sHType === 'BusinessUnit'){
        updateQuery.$set.businessUnitACOpportunities = opps;
        updateQuery.$set.businessUnitHierarchyACReportingManagers = allReportingManagers;
        updateQuery.$set.businessUnitHierarchyACTeamMembers = accessControlUserIds;

    } else if (sHType === 'Region'){
        updateQuery.$set.regionACOpportunities = opps;
        updateQuery.$set.regionHierarchyACReportingManagers = allReportingManagers;
        updateQuery.$set.regionHierarchyACTeamMembers = accessControlUserIds;

    } else if (sHType === 'Vertical'){
        updateQuery.$set.verticalACOpportunities = opps;
        updateQuery.$set.verticalHierarchyACReportingManagers = allReportingManagers;
        updateQuery.$set.verticalHierarchyACTeamMembers = accessControlUserIds;

    } else if (sHType === 'Solution'){
        updateQuery.$set.solutionACOpportunities = opps;
        updateQuery.$set.solutionHierarchyACReportingManagers = allReportingManagers;
        updateQuery.$set.solutionHierarchyACTeamMembers = accessControlUserIds;

    } else if (sHType === 'Type'){
        updateQuery.$set.typeACOpportunities = opps;
        updateQuery.$set.typeHierarchyACReportingManagers = allReportingManagers;
        updateQuery.$set.typeHierarchyACTeamMembers = accessControlUserIds;
    } else if (sHType === 'org'){
        updateQuery.$set.orgHierarchyACOpportunities = opps;
        updateQuery.$set.orgHierarchyACReportingManagers = allReportingManagers;
        updateQuery.$set.orgHierarchyACTeamMembers = accessControlUserIds;
    }

    return {
        find:findQuery,
        update:updateQuery
    }
}

function composeFindAndUpdateQuerySingleOpp(companyId,userId,sHTypeObj,opps,oppAccessible){

    var sHType = sHTypeObj.secondaryHierarchyType;
    var findQuery = {
        userId:userId,
        companyId:companyId
    }

    var updateQuery = {
    }

    if(!oppAccessible){
        updateQuery = {
            $pull: {}
        }

        if(sHType === "Revenue"){
            updateQuery.$pull.revenueACOpportunities = opps;
        } else if (sHType === "Product"){
            updateQuery.$pull.productACOpportunities = opps;
        } else if (sHType === 'BusinessUnit'){
            updateQuery.$pull.businessUnitACOpportunities = opps;
        } else if (sHType === 'Region'){
            updateQuery.$pull.regionACOpportunities = opps;
        } else if (sHType === 'Vertical'){
            updateQuery.$pull.verticalACOpportunities = opps;
        } else if (sHType === 'Solution'){
            updateQuery.$pull.solutionACOpportunities = opps;
        } else if (sHType === 'Type'){
            updateQuery.$pull.typeACOpportunities = opps;
        } else if (sHType === 'org'){
            updateQuery.$pull.orgHierarchyACOpportunities = opps;
        } else if (sHType === 'All_Access'){
            updateQuery.$pull.allACOpportunities = opps;
        }
    } else {
        updateQuery = {
            $addToSet: {}
        }

        if(sHType === "Revenue"){
            updateQuery.$addToSet.revenueACOpportunities = opps;
        } else if (sHType === "Product"){
            updateQuery.$addToSet.productACOpportunities = opps;
        } else if (sHType === 'BusinessUnit'){
            updateQuery.$addToSet.businessUnitACOpportunities = opps;
        } else if (sHType === 'Region'){
            updateQuery.$addToSet.regionACOpportunities = opps;
        } else if (sHType === 'Vertical'){
            updateQuery.$addToSet.verticalACOpportunities = opps;
        } else if (sHType === 'Solution'){
            updateQuery.$addToSet.solutionACOpportunities = opps;
        } else if (sHType === 'Type'){
            updateQuery.$addToSet.typeACOpportunities = opps;
        } else if (sHType === 'org'){
            updateQuery.$addToSet.orgHierarchyACOpportunities = opps;
        } else if (sHType === 'All_Access'){
            updateQuery.$addToSet.allACOpportunities = opps;
        }
    }

    return {
        find:findQuery,
        update:updateQuery
    }
}

function getHierarchy(companyId,secondaryHierarchyType,callback) {

    shCollection.find({companyId:companyId,
        secondaryHierarchyType:secondaryHierarchyType,
    }).lean().exec(function (err,users) {
        var listOfMembers = [],
            userIds = [];

        _.each(users,function (el) {
            el.items = _.pluck(el.secondaryHierarchyItemsList,"name");
            userIds.push(el.userId);
            listOfMembers.push({
                userId:String(el.userId),
                userEmailId:el.ownerEmailId,
                teammate:el,
                items:el.secondaryHierarchyItemsList,
                orgHead:el.orgHead
            });
            _.each(users,function (hi) {
                if(_.includes(hi.hierarchyPath,String(el.userId))){
                    listOfMembers.push({
                        userId:String(el.userId),
                        userEmailId:el.ownerEmailId,
                        teammate:hi,
                        orgHead:el.orgHead
                    });
                }
            });
        });

        var data = _
            .chain(listOfMembers)
            .groupBy("userId")
            .map(function(values, key) {

                var teamMatesEmailId = [],
                    teamMatesUserId = [];

                _.each(values,function (va) {
                    teamMatesEmailId.push(va.teammate.ownerEmailId)
                    teamMatesUserId.push(va.teammate.userId)
                });

                return {
                    userId:key,
                    teamMatesEmailId:_.uniq(teamMatesEmailId),
                    teamMatesUserId:_.uniq(teamMatesUserId),
                    reportingManagerUserIds:getReportingManagers(users,key)
                }
            })
            .value();

        callback({
            data:data,
            userIds:userIds,
            listOfMembers:listOfMembers,
            original:users
        });
    });
}

function getReportingManagers(shData,userId) {
    var reportingManagers = [];
    var rms = [];

    _.each(shData,function (va) {
        if(va.userId.toString() == userId.toString()) {
            if(va.hierarchyPath != null){
                reportingManagers = va.hierarchyPath.split(",");
            }
        }
    });

    _.each(reportingManagers, function(rm){
        if (rm != ''){
            rms.push(rm);
        }
    })

    return rms;
}

function getOppsGroupByUserId(companyId,userIds,shType,shList,isHierarchyHead,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:composeOppMatchQuery(companyId,userIds,shType,shList,isHierarchyHead)
        },
        {
            $project:{
                opportunityId:1,
                userId:1
            }
        },
        {
            $group:{
                _id:"$userId",
                oppIds: {
                    $push: "$opportunityId"
                }
            }
        }
    ]).exec(function (err,opps) {
        var oppsObj = {};

        if(!err && opps){
            _.each(opps,function (op) {
                oppsObj[op._id] = op.oppIds
            });
        }
        callback(err,oppsObj)
    })
};

function getOppsGroupByUserIdForOrg(companyId,userIds,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:{
                $or:[{companyId:companyId},{companyId:String(companyId)}],
                userId: {$in:userIds}
            }
        },
        {
            $project:{
                opportunityId:1,
                userId:1
            }
        },
        {
            $group:{
                _id:"$userId",
                oppIds: {
                    $push: "$opportunityId"
                }
            }
        }
    ]).exec(function (err,opps) {
        var oppsObj = {};

        if(!err && opps){
            _.each(opps,function (op) {
                oppsObj[op._id] = op.oppIds
            });
        }
        callback(err,oppsObj)
    })
};

SecondaryHierarchy.prototype.getAccessibleOpps = function (userId,companyId,shType,callback){
    accessControlData.findOne({companyId:companyId,userId:userId},composeAccessibleHierarchyProject(shType))
        .lean()
        .exec(function (err,result) {
        callback(err,standardizeShData(shType,result))
    })
}

function composeOppMatchQuery(companyId,userIds,shType,shList,isHierarchyHead) {

    var query = {
        $or:[{companyId:companyId},{companyId:String(companyId)}],
        // userId: {$in:userIds}
    }

    if(!isHierarchyHead || shType === "Revenue"){
        //If shList is undefined, then nobody gets access to any opp.

        if(!shList || (shList && shList.length === 0)){
            shList = [{name:"dont_give_access_to_anything_like_seriously_"}]
        }

        if (shType === "Product"){
            query.productType = {$in:_.compact(_.pluck(shList,"name"))};
        } else if (shType === 'BusinessUnit'){
            query.businessUnit = {$in:_.compact(_.pluck(shList,"name"))};
        } else if (shType === 'Region'){
            query.productType = {$in:_.compact(_.pluck(shList,"name"))};
        } else if (shType === 'Vertical'){
            query.vertical = {$in:_.compact(_.pluck(shList,"name"))};
        } else if (shType === 'Solution'){
            query.solution = {$in:_.compact(_.pluck(shList,"name"))};
        } else if (shType === 'Type'){
            query.type = {$in:_.compact(_.pluck(shList,"name"))};
        }
    }

    return query;
}

function composeOppMatchQueryForSh(companyId,userIds,shType,shList,common) {

    var query = {
        $or:[{companyId:common.castToObjectId(companyId)},{companyId:String(companyId)}]
    }
    var shTypeToCheck = null;

    if(!shList || (shList && shList.length === 0)){
        shList = [{name:"dont_give_access_to_anything_like_seriously_"}]
    }

    if (shType === "Product"){
        shTypeToCheck = "productType";
        query.productType = {$in:_.uniq(_.compact(_.pluck(shList,"name")))};
    } else if (shType === 'BusinessUnit'){
        shTypeToCheck = "businessUnit";
        query.businessUnit = {$in:_.uniq(_.compact(_.pluck(shList,"name")))};
    } else if (shType === 'Region'){
        shTypeToCheck = "geoLocation.zone";
        query["geoLocation.zone"] = {$in:_.uniq(_.compact(_.pluck(shList,"name")))};
    } else if (shType === 'Vertical'){
        shTypeToCheck = "vertical"
        query.vertical = {$in:_.uniq(_.compact(_.pluck(shList,"name")))};
    } else if (shType === 'Solution'){
        shTypeToCheck = "solution"
        query.solution = {$in:_.uniq(_.compact(_.pluck(shList,"name")))};
    } else if (shType === 'Type'){
        shTypeToCheck = "type"
        query.type = {$in:_.uniq(_.compact(_.pluck(shList,"name")))};
    }

    return {
        query:query,
        type:shTypeToCheck
    };
}

function composeAccessibleHierarchyProject(shType) {

    var project = {
        productACOpportunities:1
    }

    if (shType === "Product"){
        project = {
            productACOpportunities:1
        }
    } else if (shType === 'BusinessUnit'){

        project = {
            businessUnitACOpportunities:1
        }
    } else if (shType === 'Region'){

        project = {
            regionACOpportunities:1
        }
    } else if (shType === 'Vertical'){

        project = {
            verticalACOpportunities:1
        }
    } else if (shType === 'Solution'){

        project = {
            solutionACOpportunities:1
        }
    } else if (shType === 'Type'){
        project = {
            typeACOpportunities:1
        }
    } else if (shType === 'All_Access'){
        project = {
            allACOpportunities:1
        }
    }

    return project;
}

function standardizeShData(shType,data) {

    var oppsId = [];

    if(data){

        if (shType === "Product"){
            oppsId = data.productACOpportunities
        } else if (shType === 'BusinessUnit'){
            oppsId = data.businessUnitACOpportunities
        } else if (shType === 'Region'){
            oppsId = data.regionACOpportunities
        } else if (shType === 'Vertical'){
            oppsId = data.verticalACOpportunities
        } else if (shType === 'Solution'){
            oppsId = data.solutionACOpportunities
        } else if (shType === 'Type'){
            oppsId = data.typeACOpportunities
        } else if (shType === 'All_Access'){
            oppsId = data.allACOpportunities
        }
    }

    return oppsId;
}

function bulkHierarchyPathForChildren(companyId,common,updateObj,callback) {
    var bulk = shCollection.collection.initializeUnorderedBulkOp();
    updateObj.childrenIds.forEach(function(a){
        bulk.find({
            userId:common.castToObjectId(a),
            secondaryHierarchyType:updateObj.secondaryHierarchyType
        })
            .updateOne({
                $set:{
                    hierarchyPath:updateObj.pathsObjById[a].path
                }
            });
    });

    if(updateObj.childrenIds.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in bulkHierarchyPathForChildren() ',err );
                if(callback){
                    callback(false)
                }
            }
            else {
                result = result.toJSON();
                logger.info('bulkHierarchyPathForChildren() results ',result);
                if(callback){
                    callback(result.upserted)
                }
            }
        });
    }
    else{
        if(callback){
            callback(true)
        }
    }
}

function bulkHierarchyPathForSelf(companyId,common,updateObj,callback) {

    shCollection.update({userId:common.castToObjectId(updateObj.selfId),secondaryHierarchyType:updateObj.secondaryHierarchyType},
        {$set:{hierarchyParent:updateObj.selfParentId,hierarchyPath:updateObj.pathForSelf}})
        .exec(function (err,result) {
            callback(err,result)
        });
}

SecondaryHierarchy.prototype.removeFromHierarchy = function (userIds,shType,companyId,common,callback){
    shCollection.remove({userId:{$in:userIds},companyId:companyId, secondaryHierarchyType:shType }).exec(function (err,result) {
    });
    removeAccessToOpps(companyId,userIds,shType,common,callback)
}

function forEachUserRemoveOppAccess(userIds,companyId,shType,callback) {
    async.eachSeries(userIds, function (userId, next){
        removeOppsAccessForUser(userId,companyId,shType,function () {
            next();
        });
    }, function(err) {
        callback()
    })
}

function removeOppsAccessForUser(userId,companyId,shType,callback){
    accessControlData.findOne({companyId:companyId,userId:userId},composeAccessibleHierarchyProject(shType)).exec(function (err,result) {
        if(!err && result){
            removeAccessForMultiUsersMultiOpps([userId],companyId,standardizeShData(shType,result),callback);
        } else {
            callback()
        }
    })
}

SecondaryHierarchy.prototype.removeAccessToOpps = removeAccessToOpps

function removeAccessToOpps(companyId,userIds,shType,common,callback){
    var bulk = accessControlData.collection.initializeUnorderedBulkOp();

    _.each(userIds,function (userId) {
        var query = composeFindAndUpdateQuery(common.castToObjectId(userId),companyId,shType,[],[],[]);

        bulk.find(query.find).updateOne(query.update);

        bulk.execute(function (err, upsert) {
            if (err) {
                logger.info('Error in () ',err );
                if(callback){
                    callback(false)
                }
            }
            else {

                setAllAccessForAccessControl(companyId,null,function(){
                    if(callback){
                        callback(true);
                    }
                });
            }
        });
    });
}

SecondaryHierarchy.prototype.setOrgHead = function (updateObj,common,companyId,callback){

    updateObj.orgHead = true;

    var data = [{companyId: common.castToObjectId(companyId),
        secondaryHierarchyType:updateObj.secondaryHierarchyType,
        userId: common.castToObjectId(updateObj.userId),
        ownerEmailId:updateObj.emailId,
        hierarchyParent: null,
        hierarchyPath:null,
        secondaryHierarchyItemsList:updateObj.secondaryHierarchyItemsList,
        orgHead:true
    }];

    var bulk = shCollection.collection.initializeUnorderedBulkOp();

    data.forEach(function(a){

        bulk.find({
            userId:a.userId,
            secondaryHierarchyType:a.secondaryHierarchyType
        })
            .upsert()
            .updateOne(
                {$set:{
                        companyId:a.companyId,
                        secondaryHierarchyType:a.secondaryHierarchyType,
                        userId:a.userId,
                        ownerEmailId:a.ownerEmailId,
                        hierarchyParent:a.hierarchyParent,
                        hierarchyPath:a.hierarchyPath,
                        secondaryHierarchyItemsList:a.secondaryHierarchyItemsList,
                        orgHead:a.orgHead
                    }});
    });


    if(data.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in setOrgHead() ',err );
                callback(false)
            }
            else {
                updateOppAccessSH(companyId,common,updateObj,callback)
            }
        });
    }
    else{
        callback(true)
    }
}

SecondaryHierarchy.prototype.removeInternalTeamAccess = function (companyId,opportunityId,callback){

    if(companyId && opportunityId){
        accessControlData.update({companyId:companyId},
            { $pull: { itACOpportunities: opportunityId, allACOpportunities: opportunityId } },
            {multi:true})
            .exec(function (err,result) {
                if(callback){
                    callback(err,result)
                }
            });
    } else {
        if(callback){
            callback("No_companyId_oppId_found","No_companyId_oppId_found")
        }
    }
}

SecondaryHierarchy.prototype.addInternalTeamAccess = function (usersWithAccess,companyId,opportunityId,callback){

    var usersWithAccessEmailIds = _.pluck(usersWithAccess,"emailId");

    if(usersWithAccessEmailIds && usersWithAccessEmailIds.length>0 && opportunityId){
        getUserIds(usersWithAccessEmailIds,function (userIds) {
            if(userIds && userIds.length>0){
                var findQuery = {
                    userId:{$in:userIds}
                };

                if(companyId){
                    findQuery.companyId = companyId
                }

                accessControlData.update(findQuery, {$addToSet:{itACOpportunities:opportunityId}},{multi:true})
                    .exec(function (err,result) {
                        if(!err && result){
                            setAllAccessForAccessControl(companyId,userIds,function () {
                            });
                        }
                        if(callback){
                            callback(err,result)
                        }
                    });
            } else {
                callback("No_userId_found","No_userId_found")
            }
        });
    } else {
        if(callback){
            callback("No_usersWithAccessEmailIds_or_opportunityId_found","No_usersWithAccessEmailIds_or_opportunityId_found")
        }
    }
}

SecondaryHierarchy.prototype.updateAccessForSingleOpp = function (userId,companyId,opportunityId,shType,oppAccessible,callback){

    if(userId){
        var findUpdateObj = composeFindAndUpdateQuerySingleOpp(companyId,userId,shType,opportunityId,oppAccessible);
        var findQuery = findUpdateObj.find,
            updateQuery = findUpdateObj.update;

        accessControlData.update(findQuery, updateQuery,{multi:true})
            .exec(function (err,result) {
                if(!err && result){
                    setAllAccessForAccessControl(companyId,[userId],function () {
                        if(callback){
                            callback(err,result)
                        }
                    });
                } else {
                    if(callback){
                        callback(err,result)
                    }
                }
            });
    } else {
        callback("No_userId_found","No_userId_found")
    }
}

SecondaryHierarchy.prototype.removeInternalTeamAccessForMultiUsers = function (usersWithAccess,companyId,opportunityId,callback){

    var usersWithAccessEmailIds = _.pluck(usersWithAccess,"emailId");

    if(usersWithAccessEmailIds && usersWithAccessEmailIds.length>0 && opportunityId){
        getUserIds(usersWithAccessEmailIds,function (userIds) {
            if(userIds && userIds.length>0){
                var findQuery = {
                    userId:{$in:userIds}
                };

                if(companyId){
                    findQuery.companyId = companyId
                }

                accessControlData.update(findQuery,
                    { $pull: { itACOpportunities: opportunityId, allACOpportunities: opportunityId } },
                    {multi:true})
                    .exec(function (err,result) {
                        if(!err && result){
                            setAllAccessForAccessControl(companyId,userIds,function () {
                            });
                        }
                        if(callback){
                            callback(err,result)
                        }
                    });
            } else {
                callback("No_userId_found","No_userId_found")
            }
        });
    } else {
        if(callback){
            callback("No_usersWithAccessEmailIds_or_opportunityId_found","No_usersWithAccessEmailIds_or_opportunityId_found")
        }
    }
}

SecondaryHierarchy.prototype.removeAccessForMultiUsersMultiOpps = function (userIds,companyId,opportunityIds,callback){

    if(userIds && userIds.length>0){
        var findQuery = {
            userId:{$in:userIds}
        };

        if(companyId){
            findQuery.companyId = companyId
        }

        accessControlData.update(findQuery,
            {
                $pull: {
                    itACOpportunities: {$in:opportunityIds},
                    allACOpportunities: {$in:opportunityIds},
                    orgHierarchyACOpportunities:{$in:opportunityIds},
                    revenueACOpportunities:{$in:opportunityIds},
                    productACOpportunities:{$in:opportunityIds},
                    businessUnitACOpportunities:{$in:opportunityIds},
                    regionACOpportunities:{$in:opportunityIds},
                    solutionACOpportunities:{$in:opportunityIds},
                    typeACOpportunities:{$in:opportunityIds}
                }
            },
            {multi:true})
            .exec(function (err,result) {
                if(callback){
                    callback(err,result)
                }
            });
    } else {
        callback("No_userId_found","No_userId_found")
    }
}

function removeAccessForMultiUsersMultiOpps(userIds,companyId,opportunityIds,callback){

    if(userIds && userIds.length>0){
        var findQuery = {
            userId:{$in:userIds}
        };

        if(companyId){
            findQuery.companyId = companyId
        }

        accessControlData.update(findQuery,
            {
                $pull: {
                    itACOpportunities: {$in:opportunityIds},
                    allACOpportunities: {$in:opportunityIds},
                    orgHierarchyACOpportunities:{$in:opportunityIds},
                    revenueACOpportunities:{$in:opportunityIds},
                    productACOpportunities:{$in:opportunityIds},
                    businessUnitACOpportunities:{$in:opportunityIds},
                    regionACOpportunities:{$in:opportunityIds},
                    solutionACOpportunities:{$in:opportunityIds},
                    typeACOpportunities:{$in:opportunityIds}
                }
            },
            {multi:true})
            .exec(function (err,result) {
                if(callback){
                    callback(err,result)
                }
            });
    } else {
        callback("No_userId_found","No_userId_found")
    }
}

SecondaryHierarchy.prototype.removeAccessForAllUsersMultiOpps = function (companyId,opportunityIds,callback){

    if(companyId && opportunityIds && opportunityIds.length>0){
        var findQuery = {
            companyId:companyId
        };

        accessControlData.update(findQuery,
            {
                $pull: {
                    itACOpportunities: {$in:opportunityIds},
                    allACOpportunities: {$in:opportunityIds},
                    orgHierarchyACOpportunities:{$in:opportunityIds},
                    revenueACOpportunities:{$in:opportunityIds},
                    productACOpportunities:{$in:opportunityIds},
                    businessUnitACOpportunities:{$in:opportunityIds},
                    regionACOpportunities:{$in:opportunityIds},
                    solutionACOpportunities:{$in:opportunityIds},
                    typeACOpportunities:{$in:opportunityIds}
                }
            },
            {multi:true})
            .exec(function (err,result) {
                if(callback){
                    callback(err,result)
                }
            });
    } else {
        callback("No_userId_found","No_userId_found")
    }
}

SecondaryHierarchy.prototype.addAccessForMultiUsersMultiOpps = function (userIds,companyId,opportunityIds,callback){

    if(userIds && userIds.length>0){

        var bulk = accessControlData.collection.initializeUnorderedBulkOp();
        _.each(userIds,function (userId) {

            bulk.find({
                companyId:companyId,
                allACTeamMembers:userId
            }).
            update({
                $addToSet: {
                    allACOpportunities: {$each:opportunityIds}
                }
            });

            bulk.find({
                companyId:companyId,
                orgHierarchyACTeamMembers:userId,
            }).
            update({
                $addToSet: {
                    orgHierarchyACOpportunities:{$each:opportunityIds}
                }
            });

            bulk.find({
                companyId:companyId,
                revenueHierarchyACTeamMembers:userId,
            }).
            update({
                $addToSet: {
                    revenueACOpportunities:{$each:opportunityIds}
                }
            });

            bulk.find({
                companyId:companyId,
                productHierarchyACTeamMembers:userId,
            }).
            update({
                $addToSet: {
                    productACOpportunities:{$each:opportunityIds}
                }
            });

            bulk.find({
                companyId:companyId,
                businessUnitHierarchyACTeamMembers:userId,
            }).
            update({
                $addToSet: {
                    businessUnitACOpportunities:{$each:opportunityIds}
                }
            });
            bulk.find({
                companyId:companyId,
                regionHierarchyACTeamMembers:userId,
            }).
            update({
                $addToSet: {
                    regionACOpportunities:{$each:opportunityIds}
                }
            });
            bulk.find({
                companyId:companyId,
                solutionHierarchyACTeamMembers:userId,
            }).
            update({
                $addToSet: {
                    solutionACOpportunities:{$each:opportunityIds}
                }
            });
            bulk.find({
                companyId:companyId,
                typeHierarchyACTeamMembers:userId,
            }).
            update({
                $addToSet: {
                    typeACOpportunities:{$each:opportunityIds}
                }
            });
        });

        bulk.execute(function (err, upsert) {
            if (err) {
                logger.info('Error in addAccessForMultiUsersMultiOpps() ',err );
                if(callback){
                    callback(false)
                }
            }
            else {
                upsert = upsert.toJSON();
                logger.info('addAccessForMultiUsersMultiOpps() ',upsert);
                if(callback){
                    callback(upsert.upserted)
                }
            }
        });
    } else {
        callback("No_userId_found","No_userId_found")
    }
}

function getUserIds(emailIds,callback){
    myUserCollection.find({emailId:{$in:emailIds}},{_id:1}).lean().exec(function (err,users) {
        var userIds = [];
        if(!err && users && users.length>0){
            userIds = _.pluck(users,"_id");
        }
        callback(userIds)
    })
}

SecondaryHierarchy.prototype.updateShList = function (userId,companyId,common,sHItemsList,sHType,callback){
    shCollection.update({
        userId: common.castToObjectId(userId),
        companyId: common.castToObjectId(companyId),
        secondaryHierarchyType:sHType
    },{
        $set:{
            secondaryHierarchyItemsList:sHItemsList
        }
    }).exec(function (err,result) {
        callback(err,result);
    })
}

SecondaryHierarchy.prototype.updateOppsForSh = updateOppsForSh

function updateOppsForSh(companyId,common,updateObj,callback){

    getHierarchy(companyId,updateObj.secondaryHierarchyType,function (teamHierarchy) {
        var obj = buildFindQuery(companyId,teamHierarchy,updateObj.secondaryHierarchyType,common);
        oppGroupByType(obj.query,obj.type,function (err,opps) {
            updateAccessControlCollection(teamHierarchy,opps,updateObj.secondaryHierarchyType,companyId,common,function(){
                setAllAccessForAccessControl(companyId,null,function(){
                    if(callback){
                        callback(opps);
                    }
                });
            });
        })
    });
}

function updateAccessControlCollection(teamHierarchy,oppsObj,shType,companyId,common,callback) {
    var bulk = accessControlData.collection.initializeUnorderedBulkOp();

    var acUserIdsAndRMs = {};

    _.each(teamHierarchy.data,function (el) {
        acUserIdsAndRMs[String(el.userId)] = el;
    });

    _.each(teamHierarchy.original,function (user) {

        var query = composeFindAndUpdateQuery(common.castToObjectId(user.userId),
            companyId,
            shType,
            getAccessibleOppsList(oppsObj,user.items,user.userId),
            acUserIdsAndRMs[String(user.userId)].teamMatesUserId,
            acUserIdsAndRMs[String(user.userId)].reportingManagerUserIds);

        bulk.find(query.find).upsert().updateOne(query.update);
    });

    if(teamHierarchy.data.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in updateAccessControlCollection() ',err );
                if(callback){
                    callback(false)
                }
            }
            else {
                result = result.toJSON();
                if(callback){
                    callback(result.upserted)
                }
            }
        });
    } else{
        if(callback){
            callback(true)
        }
    }
}

function getAccessibleOppsList(oppsObj,accessItems) {
    var opps = [];

    for(var key in oppsObj){
        if(_.includes(accessItems,key)){
            opps = opps.concat(oppsObj[key])
        }
    }

    return _.compact(_.uniq(opps));
}

function oppGroupByType(query,type,callback) {

    var project = {
        opportunityId:1
    }
    var id = null;

    if(type) {
        project = {
            opportunityId:1
        };
        project[type] = 1
    }

    if(type){
        id = "$"+type;
    }

    opportunitiesCollection.aggregate([
        {
            $match:query
        },
        {
            $project:project
        },
        {
            $group:{
                _id:id,
                oppIds: {
                    $push: "$opportunityId"
                }
            }
        }
    ]).exec(function (err,opps) {
        var oppsObj = {};
        if(!err && opps && opps.length>0){
            _.each(opps,function (op) {
                oppsObj[op._id] = op.oppIds
            });
        }

        callback(err,oppsObj)
    });
}

function getOrgHead(companyId,secondaryHierarchyType,callback) {
    shCollection.findOne({companyId:companyId,
        orgHead:true,
        secondaryHierarchyType:secondaryHierarchyType,
    }).lean().exec(function (err,users) {
        callback(err,users);
    })
}

function buildFindQuery(companyId,teamHierarchy,secondaryHierarchyType,common) {
    var userIds = [],
        shList = [];
    _.each(teamHierarchy.listOfMembers,function (el) {
        userIds.push(el.userId);
        if(el.teammate){
            shList = shList.concat(el.teammate.secondaryHierarchyItemsList);
        }
    });

    return composeOppMatchQueryForSh(companyId,userIds,secondaryHierarchyType,shList,common);
}

SecondaryHierarchy.prototype.addUserToHierarchy = function (updateObj,companyId,common,callback){
    var data = [{companyId: common.castToObjectId(companyId),
        secondaryHierarchyType:updateObj.secondaryHierarchyType,
        userId: common.castToObjectId(updateObj.userId),
        ownerEmailId:updateObj.emailId,
        hierarchyParent: updateObj.hierarchyParent,
        hierarchyPath:updateObj.hierarchyPath,
        orgHead:false,
        secondaryHierarchyItemsList:updateObj.secondaryHierarchyItemsList
    }];

    var bulk = shCollection.collection.initializeUnorderedBulkOp();

    data.forEach(function(a){

        bulk.find({
            userId:a.userId,
            secondaryHierarchyType:a.secondaryHierarchyType
        })
            .upsert()
            .updateOne(
                {$set:{
                        companyId:a.companyId,
                        secondaryHierarchyType:a.secondaryHierarchyType,
                        userId:a.userId,
                        ownerEmailId:a.ownerEmailId,
                        hierarchyParent:a.hierarchyParent,
                        hierarchyPath:a.hierarchyPath,
                        orgHead:a.orgHead,
                        secondaryHierarchyItemsList:a.secondaryHierarchyItemsList
                    }}
            );
    });


    if(data.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                callback(false)
            }
            else {
                result = result.toJSON();
                if(updateObj.secondaryHierarchyType == "Revenue"){
                    updateOppAccessSH(companyId,common,updateObj,callback)
                } else {
                    updateOppsForSh(companyId,common,updateObj,callback)
                }
            }
        });
    }
    else{
        callback(true)
    }
}

SecondaryHierarchy.prototype.getUserHierarchy = function(userId, shType, callBack) {

    shCollection.findOne({ userId: userId, secondaryHierarchyType:shType }).lean().exec(function(err, user) {

        if(!err && user){
            shCollection.find({ companyId:user.companyId,
                secondaryHierarchyType:shType,
                hierarchyPath: { $regex: userId.toString(), $options: 'i' }}).lean().exec(function(err, users) {
                user["hierarchyParent"] = null
                users.push(user)
                callBack(null, users)
            })
        } else {
            callBack(err,null)
        }
    })
};

SecondaryHierarchy.prototype.getUserHierarchyByEmailId = function(emailId, shType, callBack) {

    if(!shType){
        callBack(null,null)
    } else if(shType.toLowerCase() == "all access" ||shType.toLowerCase() == "revenue" || shType.toLowerCase() == "org. hierarchy"){
        callBack(null,null)
    } else {

        shCollection.find({ ownerEmailId: {$in:emailId}, secondaryHierarchyType:shType }).lean().exec(function(err, user) {

            if(!err && user){

                var access = [];
                _.each(user,function (el) {
                    _.each(el.secondaryHierarchyItemsList,function (sh) {
                        access.push({
                                type:formatShTypes(shType),
                                name:sh.name
                            })
                    })
                });

                access = _.flatten(access);

                if(access.length == 0){
                    access.push({
                        type:formatShTypes(shType),
                        name:new Date() //So that no opp types are found.
                    })
                }

                callBack(null, access)
            } else {
                callBack(err,null)
            }
        })
    }
};

function formatShTypes(sh){

    var type = "productType"
    if(sh == "Product"){
        type = "productType"
    }
    if(sh == "Solution"){
        type = "solution"
    }
    if(sh == "Vertical"){
        type = "vertical"
    }
    if(sh == "BusinessUnit"){
        type = "businessUnit"
    }
    if(sh == "Type"){
        type = "type"
    }

    return type;
}

SecondaryHierarchy.prototype.getAllChildren = function(userIds,companyId, callBack) {

    var query = {
        $in: _.map(userIds,function (userId) {
            return new RegExp(userId.toString())
        })
    }

    var dataCollect = [];

    async.eachSeries(userIds, function (userId, next){
        shCollection.findOne({ userId: userId }).lean().exec(function(err, user) {
            shCollection.find({ companyId:companyId,
                hierarchyPath: { $regex: userId.toString(), $options: 'i' }},{userId:1,secondaryHierarchyType:1,ownerEmailId:1,hierarchyPath:1}).lean().exec(function(err, users) {

                    dataCollect.push(user);
                    if(!err && users && users.length>0){
                    dataCollect = dataCollect.concat(users);
                }
                next();
            })
        });
    }, function(err,data) {

        var listOfMembers = [];
        if(dataCollect.length>0){
            _.each(userIds,function (el) {
                _.each(dataCollect,function (hi) {
                    if(hi && _.includes(hi.hierarchyPath,String(el))){
                        listOfMembers.push({
                            userId:el,
                            type:"secondary",
                            teammate:hi
                        })
                    }
                })
            });
        }

        callBack(err,listOfMembers)
    });
};

SecondaryHierarchy.prototype.customCompanyUpdate = function (findQ,updateQ,callback) {

    companyCollection.update(findQ,updateQ,function (err,result) {
        if(callback){
            callback(err,result)
        }
    })
};

SecondaryHierarchy.prototype.getAllHierarchyTypes = function(userId, callBack) {

    shCollection.aggregate([{
        $match: { userId: userId}
    }, {
        $group:{
            _id:null,
            hierarchies:{$push:"$secondaryHierarchyType"}
        }
    }]).exec(function(err, hierarchies) {
        if(!err && hierarchies && hierarchies[0] && hierarchies[0].hierarchies){
            callBack(null, hierarchies[0].hierarchies)
        } else {
            callBack(err,null)
        }
    })
};

SecondaryHierarchy.prototype.getAllHierarchyTypesWithItemAccess = function(userId, callBack) {

    shCollection.aggregate([{
        $match: { userId: userId}
    }, {
        $group:{
            _id:null,
            hierarchies:{
                $push:{
                    "secondaryHierarchyType":"$secondaryHierarchyType",
                    "secondaryHierarchyItemsList":"$secondaryHierarchyItemsList",
                }
            }
        }
    }]).exec(function(err, hierarchies) {
        if(!err && hierarchies && hierarchies[0] && hierarchies[0].hierarchies){
            callBack(null, hierarchies[0].hierarchies)
        } else {
            callBack(err,null)
        }
    })
};

SecondaryHierarchy.prototype.getAllHierarchyTypesWithItemAccessByCompanyId = function(companyId, callBack) {

    shCollection.aggregate([{
        $match: {
            companyId: companyId
        }
    }, {
        $group:{
            _id:null,
            hierarchies:{
                $push:{
                    "userId":"$userId",
                    "secondaryHierarchyType":"$secondaryHierarchyType",
                    "secondaryHierarchyItemsList":"$secondaryHierarchyItemsList",
                }
            }
        }
    }]).exec(function(err, hierarchies) {
        if(!err && hierarchies && hierarchies[0] && hierarchies[0].hierarchies){
            callBack(null, hierarchies[0].hierarchies)
        } else {
            callBack(err,null)
        }
    })
};

SecondaryHierarchy.prototype.getUserSecondaryHierarchy = function (companyId,userId,shType,callback){
    shCollection.findOne({companyId:companyId,
        userId:userId,
        secondaryHierarchyType:shType,
    }).lean().exec(function (err,user) {
        if(!err && user){
            shCollection.find({ companyId:companyId,secondaryHierarchyType:shType, hierarchyPath: { $regex: userId.toString(), $options: 'i' }}).lean().exec(function(err, users) {
                user["hierarchyParent"] = null
                users.push(user)
                callback(null, users)
            })
        } else {
            callback(err,null)
        }
    });
}

module.exports = SecondaryHierarchy;
