var myUserCollection = require('../databaseSchema/userManagementSchema').User;

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston();
var _ = require("lodash");

var redis = require('redis');
var redisClient = redis.createClient();

function OrgHierarchy() {}

OrgHierarchy.prototype.getHierarchy = function (companyId,callback){
    myUserCollection.find({companyId:companyId},{contacts:0}).lean().exec(function (err,result) {
      callback(err,result)
    });
}

OrgHierarchy.prototype.updateHierarchyPaths = function (companyId,updateObj,common,callback){

    if(updateObj.childrenIds && updateObj.childrenIds.length>0){
        bulkHierarchyPathForSelf(companyId,common,updateObj,function () {
            bulkHierarchyPathForChildren(companyId,common,updateObj,function () {
                callback()
            })
        });
    } else {
        bulkHierarchyPathForSelf(companyId,common,updateObj,function () {
            callback()
        });
    }
}

function bulkHierarchyPathForChildren(companyId,common,updateObj,callback) {
    var bulk = myUserCollection.collection.initializeUnorderedBulkOp();
    updateObj.childrenIds.forEach(function(a){

        bulk.find({
            _id:common.castToObjectId(a),
            companyId:common.castToObjectId(companyId)
        })
        .updateOne({
            $set:{
                hierarchyPath:updateObj.pathsObjById[a].path
            }
        });
    });

    if(updateObj.childrenIds.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                if(callback){
                    callback(false)
                }
            }
            else {
                result = result.toJSON();
                logger.info('addUserToHierarchy() results ',result);
                if(callback){
                    callback(result.upserted)
                }
            }
        });
    }
    else{
        if(callback){
            callback(true)
        }
    }
}

function bulkHierarchyPathForSelf(companyId,common,updateObj,callback) {

    myUserCollection.update({_id:common.castToObjectId(updateObj.selfId)},{
        $set: {
            hierarchyParent:common.castToObjectId(updateObj.selfParentId),
            hierarchyPath:updateObj.pathForSelf
        }},function(err,result) {

        var key = "getTeam"+String(companyId);
        redisClient.del(key)
        callback(err,result)
    });
}

OrgHierarchy.prototype.removeFromHierarchy = function (userIds,companyId,callback){
    myUserCollection.update({_id:{$in:userIds},companyId:companyId},{$set:{hierarchyParent:null,hierarchyPath:null}},{multi:true}).exec(function (err,result) {
      callback(err,result)
    });
}

OrgHierarchy.prototype.setOrgHead = function (updateObj,common,companyId,callback){

    var data = [{companyId: common.castToObjectId(companyId),
        _id: common.castToObjectId(updateObj.userId),
        userId: common.castToObjectId(updateObj.userId),
        hierarchyParent: null,
        hierarchyPath:null,
        orgHead:true
    }];

    var bulk = myUserCollection.collection.initializeUnorderedBulkOp();

    data.forEach(function(a){

        bulk.find({
            _id:a.userId
        })
        .upsert()
        .updateOne(
            {$set:{
                hierarchyParent:a.hierarchyParent,
                hierarchyPath:a.hierarchyPath,
                orgHead:a.orgHead
            }});
    });

    if(data.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                callback(false)
            }
            else {
                result = result.toJSON();
                logger.info('addUserToHierarchy() results ',result);
                callback(result.upserted)
            }
        });
    }
    else{
        callback(true)
    }
}

OrgHierarchy.prototype.addUserToHierarchy = function (updateObj,companyId,common,callback){
    var data = [{companyId: common.castToObjectId(companyId),
            userId: common.castToObjectId(updateObj.userId),
            hierarchyParent: updateObj.hierarchyParent,
            hierarchyPath:updateObj.hierarchyPath,
            orgHead:false
        }];

    var bulk = myUserCollection.collection.initializeUnorderedBulkOp();

    data.forEach(function(a){

        bulk.find({
                _id:a.userId
            })
            .upsert()
            .updateOne(
                {$set:{
                    hierarchyParent:a.hierarchyParent,
                    hierarchyPath:a.hierarchyPath,
                    orgHead:a.orgHead
                }}
            );
    });


    if(data.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                callback(false)
            }
            else {
                result = result.toJSON();
                logger.info('addUserToHierarchy() results ',result);
                callback(result.upserted)
            }
        });
    }
    else{
        callback(true)
    }

}

module.exports = OrgHierarchy;