
var messageCollection = require('../databaseSchema/userManagementSchema').message;
var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var userManagement = require('../dataAccess/userManagementDataAccess');
var notificationsCollection = require('../databaseSchema/notificationsSchema').Notifications;

var mongoose = require('mongoose');
var _ = require("lodash");
var moment = require("moment");
var momentTZ = require('moment-timezone');
var async = require('async');

var userManagementObj = new userManagement();

var winstonLog = require('../common/winstonLog');

var logLib = new winstonLog();
var logger =logLib.getWinston()

function MessageDataAccess()
{

}

/**** Functions on Messages ****/

function constructMessageDetails(messageDetails){
    var date = new Date();
    var message = new messageCollection({
        senderId:messageDetails.senderId,
        receiverId:messageDetails.receiverId,
        receiverEmailId:messageDetails.receiverEmailId,
        subject:messageDetails.subject,
        message:messageDetails.message,
        type:messageDetails.type || 'message',
        headerImage:messageDetails.headerImage ? true : false,
        sentOn:date,
        readStatus:false
    });

    if(messageDetails.headerImage){
        var image = {
            url:messageDetails.imageUrl,
            uploadedOn:date,
            name:messageDetails.imageName
        }
        message.image = image;
    }

    return message;
}

MessageDataAccess.prototype.createMessage = function(messageData,back){

   var message = constructMessageDetails(messageData);
    message.save(function(err,message){
        if(err){
            logger.info('An error occurred while creating message '+JSON.stringify(err));
            back(false);
        }
        else{
            back(message);
        }
    })
};

MessageDataAccess.prototype.getMessageInteractions = function(userId,otherUserId,callback){
    messageCollection.find({
        $or:[
            {senderId:userId,receiverId:otherUserId},
            {senderId:otherUserId,receiverId:userId}
        ]
    }).sort({sentOn:-1}).exec(function(error,messages){
        if(checkRequred(messages) && messages.length > 0){
            myUserCollection.populate(messages,[{ path: 'senderId' ,select: 'firstName lastName emailId'},{ path: 'receiverId' ,select: 'firstName lastName emailId'}],function(eror,users){
                callback(users);
            });
        }else callback([])
    })
};

MessageDataAccess.prototype.populateMessageUserDetails = function(messages,callback){
    myUserCollection.populate(messages,[{ path: 'senderId' ,select: 'firstName lastName emailId'},{ path: 'receiverId' ,select: 'firstName lastName emailId'}],function(eror,users){
        if(eror){
          callback(messages)
        }else
        callback(users);
    });
}

MessageDataAccess.prototype.getMessageById = function(messageId,callback){
    messageCollection.findOne({_id:messageId},function(error,message){
        if(checkRequred(message)){
            myUserCollection.populate(message,[{ path: 'senderId' ,select: 'firstName lastName emailId'},{ path: 'receiverId' ,select: 'firstName lastName emailId'}],function(eror,users){
                callback(users);
            });
        }else callback([]);
    })
};

MessageDataAccess.prototype.getMessagesSent = function(loggedInUser,callback){
    messageCollection.find({senderId:loggedInUser},function(error,messages1){
        if(messages1){
            if(messages1[0]){
                callback(messages1.length);
            }else callback(0)
        }else callback(0)
    })
};

MessageDataAccess.prototype.removeUserMessages = function(userId,callback){
    messageCollection.remove({senderId:userId},function(error,result){
        if(error){
            callback(false,'onDeleteMessages',error,result);
        }
        else callback(true,'onDeleteMessages',error,result);
    })
};

MessageDataAccess.prototype.getOneMessageInteraction = function(userId,callback){
    messageCollection.findOne({senderId:userId},function(error,message){
        callback(message);
    })
};

MessageDataAccess.prototype.getMessageInteractionsCount = function(loggedInUser,notLoggedInUser,callback){
    var q = {
        $or:[
            {senderId:loggedInUser,receiverId:notLoggedInUser},
            {senderId:notLoggedInUser,receiverId:loggedInUser}
        ]
    }
    messageCollection.aggregate([
        {
            $match:q
        },
        {
            $group: {
                _id: null,
                count: {
                    $sum: 1
                }
            }
        }
    ]).exec(function ( e, d ) {

        if(e){
            logger.info('mongo aggregate error in interaction count message '+e);
            callback(0)
        }else{
            if(checkRequred(d) && d.length > 0){
                callback(d[0].count || 0);
            }else callback(0)
        }
    })
}

MessageDataAccess.prototype.getUnReadMessageCount = function(userId,messageTypes,callback){
    messageCollection.aggregate([
        {
            $match:{
                receiverId:userId,
                type:{$in:messageTypes},
                readStatus:{$ne:true}
            }
        },
        {
            $group: {
                _id: null,
                count: {
                    $sum: 1
                }
            }
        }
    ]).exec(function ( e, d ) {

        if(e){
            logger.info('mongo aggregate error in un-read messages count'+e);
            callback(0)
        }else{
            if(checkRequred(d) && d.length > 0){
                callback(d[0].count || 0);
            }else callback(0)
        }
    })
}

MessageDataAccess.prototype.getUnReadMessages = function(userId,messageTypes,callback){
    messageCollection.aggregate([
        {
            $match:{
                receiverId:userId,
                type:{$in:messageTypes},
                readStatus:{$ne:true}
            }
        },
        {
            $group: {
                _id: null,
                userIdArr: { $addToSet: "$senderId" },
                messages:{$addToSet:{
                    "_id" : "$_id",
                    "senderId" : "$senderId",
                    "receiverId" : "$receiverId",
                    "subject" : "$subject",
                    "message" : "$message",
                    "type" : "$type",
                    "sentOn" : "$sentOn"
                }
                }
            }
        }
    ]).exec(function ( e, d ) {

        if(e){
            logger.info('mongo aggregate error in un-read messages count'+e);
            callback(null)
        }else{
            if(checkRequred(d) && d.length > 0){
                callback(d[0]);
            }else callback(null)
        }
    })
}

MessageDataAccess.prototype.updateMessageReadStatus = function(messageId){
    messageCollection.update({_id:messageId},{$set:{readStatus:true,redOn:new Date()}},function(error,result){

    })
}

MessageDataAccess.prototype.getAllUsersMessages = function(callback){
    messageCollection.find({},function(error,messages){
        if(error){
            logger.info('Mongo error in fetching all users messages');
        }
        callback(messages)
    })
}

MessageDataAccess.prototype.sendEmail = function (userId,data,googleCalendar,gmailObj,officeOutlook,callback) {

    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1,outlook:1},function(error,user){

        if(user){
            if(user.google.length == 0 && user.outlook.length>0){
                _.each(user.outlook,function (outlookObj) {
                    if(user.emailId == outlookObj.emailId){
                        sendOutlookMail(outlookObj.refreshToken,data,user,googleCalendar,gmailObj,officeOutlook,callback);
                    }
                });

            } else {

                var dataToSend = {
                    email_cc:data.email_cc,
                    receiverEmailId:data.receiverEmailId,
                    receiverId:checkRequred(data.receiverId) ? data.receiverId : '',
                    receiverName:data.receiverName,
                    senderId:user._id,
                    senderEmailId:user.emailId,
                    senderName:user.firstName+' '+user.lastName,
                    message:data.message,
                    subject:data.subject,
                    trackViewed:true, //data.trackViewed,
                    docTrack:data.docTrack,
                    remind:data.remind,
                    refId:data.refId,
                    updateReplied:data.updateReplied || false
                };

                sendEmailViaGoogle(dataToSend,user,googleCalendar,gmailObj,officeOutlook,callback);
            }

        } else {
            if(callback){
                callback(null,false)
            }
        }
    });
}

MessageDataAccess.prototype.getNotificationData = function(reqBody, userEmailId, callback) {
    var msgId = reqBody.messageId;
    var category = reqBody.category;
    var dayString = reqBody.dayString;

    var query = {
        emailId:userEmailId
    };

    if(checkRequred(msgId)) {
        query = {
            messageId:msgId,
        };
    }

    if(checkRequred(category) && checkRequred(dayString)) {
        query.category = category;
        query.dayString = dayString;

        fromDate = moment(dayString, "DDMMYYYY").startOf('day').toDate();
        toDate = moment(dayString, "DDMMYYYY").endOf('day').toDate();

        query.sentDate = {
            $gte:new Date(fromDate),
            $lte:new Date(toDate)
        }
    }

    query.dataReferenceType = {
        $ne:"war"
    }

    notificationsCollection.findOne(query, function(error, notification) {

        if(!error && notification) {
            callback(error, notification);
        } else {
            callback(error, null);
        }
    })
}

MessageDataAccess.prototype.getNotificationDataForReport = function(body, userEmailId, callback) {
    var fromDate = body.fromDate;
    var toDate = body.toDate;
    var query = {};

    query.dataReferenceType = {
        $ne:"war"
    }

    if(fromDate && toDate) {
        fromDate = moment(fromDate).startOf('day').toDate();
        toDate = moment(toDate).endOf('day').toDate();

        query.sentDate = {
            $gte:new Date(fromDate),
            $lte:new Date(toDate)
        }
    }

    if(body.companyId && body.companyId !== 'all') {
        query.companyId = castToObjectId(body.companyId);
    }

    notificationsCollection.find(query, function(error, notification) {
        if(!error && notification) {
            callback(error, notification);
        } else {
            callback(error, null);
        }
    })
}

MessageDataAccess.prototype.getNotificationDataForToday = function(queryParams, userEmailId, companyId, callback) {
    var fromDate = moment().startOf('day').toDate();
    var toDate = moment().endOf('day').toDate();

    var weekStart = moment().startOf('week').toDate();
    var weekEnd = moment().startOf('week').toDate();
    
    var query = {
        emailId: userEmailId,
        dataReferenceType: {$ne:"war"},
        companyId: castToObjectId(companyId),
        dataFor: queryParams.dataFor
    };

    var today = {
        sentDate: {
            $gte:new Date(fromDate),
            $lte:new Date(toDate)
        }
    }

    var thisWeek = {
        sentDate: {
            $gte:new Date(weekStart),
            $lte:new Date(weekEnd)
        },
        notificationType: 'weekly'
    }

    var finalQuery = {
        $and: [query, { $or: [today, thisWeek] } ]
    }

    var project = {
        notificationTitle: 1,
        notificationBody: 1,
        url: 1, 
        sentDate: 1,
        category:1,
        messageId: 1,
        dayString: 1,
        _id: 0
    }

    notificationsCollection.find(finalQuery, project, {sort: {sentDate:-1}}, function(error, notifications) {
        if(!error && notifications) {
            callback(error, notifications);
        } else {
            callback(error, null);
        }
    })
}

MessageDataAccess.prototype.updateNotificationOpenDate = function(reqBody, userEmailId, callback) {
    var query = {
        emailId: userEmailId,
        dataReferenceType: { $ne:"war" }
    };

    if(reqBody.from == "mobile") {
        query.messageId = reqBody.messageId;
        query.dataFor = "mobile"
        query.accessed = false
    }

    else if(reqBody.from == "web") {
        query.dayString = reqBody.dayString
        query.dataFor = "web",
        query.category = reqBody.category,
        query.accessed = false;
    }

    notificationsCollection.update(query, {$set: { openDate:new Date(), accessed:true}}, function(err, result) {
        callback(err, result);
    })
}

MessageDataAccess.prototype.insertNotifications = function(notifications, callback) {
    async.eachSeries(notifications, function(insertObj, next) {
        new notificationsCollection(insertObj).save(function(error, result) {
            next();
        })
    }, function(error3) {
        callback();                           
    })
}

MessageDataAccess.prototype.getWarData = function(body, callback) {
    var fromDate = body.fromDate;
    var toDate = body.toDate;
    var query = {};

    query.dataReferenceType = "war"

    if(fromDate && toDate) {
        fromDate = moment(fromDate).startOf('day').toDate();
        toDate = moment(toDate).endOf('day').toDate();

        query.sentDate = {
            $gte:new Date(fromDate),
            $lte:new Date(toDate)
        }
    }

    if(body.companyId && body.companyId !== 'all') {
        query.companyId = castToObjectId(body.companyId);
    }

    if(body.emailId && body.emailId !== 'all') {
        query.emailId = castToObjectId(body.emailId)
    }

    notificationsCollection.find(query, function(error, warData) {
        callback(error, warData)
    })
}

MessageDataAccess.prototype.getWarDataForReport = function(body, callback) {
    var fromDate = body.fromDate;
    var toDate = body.toDate;
    var query = {};

    query.dataReferenceType = "war"

    if(fromDate && toDate) {
        fromDate = moment(fromDate).startOf('day').toDate();
        toDate = moment(toDate).endOf('day').toDate();

        query.sentDate = {
            $gte:new Date(fromDate),
            $lte:new Date(toDate)
        }
    }

    if(body.companyId && body.companyId !== 'all') {
        query.companyId = castToObjectId(body.companyId);
    }

    if(body.emailId && body.emailId !== 'all') {
        query.emailId = castToObjectId(body.emailId)
    }

    notificationsCollection.find(query, function(error, warData) {
        callback(error, warData)
    })
}

function castToObjectId(id){
    return mongoose.Types.ObjectId(id)
}

MessageDataAccess.prototype.storeNotificationData = function(insertObj, callback) {
    new notificationsCollection(insertObj).save(function(error, result) {
        callback(error, result);
    })
}

function sendEmailViaGoogle(dataToSend,user,googleCalendar,gmailObj,officeOutlook,callback){

    if(googleCalendar.validateUserGoogleAccount(user)){
        gmailObj.sendEmail(user._id,user.google[0].token,user.google[0].refreshToken,user.google[0].emailId,dataToSend,function(error,response){
            if(error){
                if(callback){
                    callback(error,false)
                }
            }
            else{

                if(callback){
                    callback(null,true)
                }
            }
        })
    }
    else {
        if(callback){
            callback(null,false)
        }
    };
}

function sendOutlookMail(refreshToken,mailBody,user,googleCalendar,gmailObj,officeOutlook,callback) {

    var mail = {
        "Comment": mailBody.message
    }

    officeOutlook.getNewAccessTokenWithoutStore(refreshToken,function (newToken) {

        if(mailBody.newMessage){
            sendNewOutlookMail(newToken.access_token,mailBody,user,googleCalendar,gmailObj,officeOutlook,callback)
        } else {

            mail = {
                "Comment": mailBody.originalBody
            }

            var id = mailBody.refId?mailBody.refId:mailBody.id;

            officeOutlook.createReplyEmailMSGraphAPI(newToken.access_token,mail,id,function (error,response) {

                if(!error && response){

                    var draftMessage = JSON.parse(response);

                    var content = "<html><body><div>"+mailBody.originalBody+"</div></body></html>" +draftMessage.body.content
                    var recipients = [];

                    if(mailBody.email_cc && mailBody.email_cc.length>0){

                        _.each(mailBody.email_cc,function (emailId) {

                            recipients.push({
                                "EmailAddress": {
                                    "Address": emailId.trim().toLowerCase(),
                                }
                            })
                        });
                    }

                    var updateDraft = {
                        "body": {
                            "contentType": "html",
                            "content": content
                        },
                        "ccRecipients":recipients
                    }

                    officeOutlook.updateDraftMSGraphAPI(newToken.access_token,draftMessage.id,updateDraft,function (err,updateRes) {

                        officeOutlook.sendDraftMSGraphAPI(newToken.access_token,draftMessage.id,function (errSend,sent) {
                            if(!errSend && response){
                                if(callback){
                                    callback(errSend,sent)
                                }
                            } else {
                                if(callback){
                                    callback(null,false)
                                }
                            }
                        })
                    })
                } else {
                    if(callback){
                        callback(null,false)
                    }
                }
            });
        }
    });
};

function sendNewOutlookMail(refreshToken,mailBody,user,googleCalendar,gmailObj,officeOutlook,callback) {

    var recipients = []
    if(mailBody.email_cc && mailBody.email_cc.length>0){

        _.each(mailBody.email_cc,function (emailId) {
            recipients.push({
                "EmailAddress": {
                    "Address": emailId
                }
            })
        });
    }

    var mail = {
        "Message": {
            "Subject": mailBody.subject,
            "Body": {
                "ContentType": "Text",
                "Content": mailBody.message
            },
            "ToRecipients": [
                {
                    "EmailAddress": {
                        "Address": mailBody.receiverEmailId
                    }
                }
            ],
            "ccRecipients":recipients
        },
        "SaveToSentItems": "true"
    };

    officeOutlook.sendEmailMSGraphAPI(refreshToken,mail,function (error,response) {

        if(!error && response){
            if(callback){
                callback(error,response)
            }
        } else {
            if(callback){
                callback(null,false)
            }
        }
    });
}

function checkRequred(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

function removeDuplicates_id(arr){

    var end = arr.length;

    for(var i = 0; i < end; i++)
    {
        for(var j = i + 1; j < end; j++)
        {
            if(arr[i]._id == arr[j]._id)
            {
                var shiftLeft = j;
                for(var k = j+1; k < end; k++, shiftLeft++)
                {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}
// export the MessageDataAccess Class
module.exports = MessageDataAccess;
