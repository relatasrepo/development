/**
 * Created by naveen on 24/8/17.
 */

var dealsAtRiskMeta = require('../databaseSchema/userManagementSchema').dealsAtRiskMeta;
var mongoose = require('mongoose');
var moment = require("moment-timezone");

var _ = require("lodash");

function DealsAtRisk(){

}

DealsAtRisk.prototype.findDealsAtRiskInAccount = function (userIds,oppCloseDateFrom,oppCloseDateTo,contacts,callback) {
    findDealsAtRisk(userIds,oppCloseDateFrom,oppCloseDateTo,contacts,function (err,dealsAtRisk) {

        if(!err && dealsAtRisk && dealsAtRisk.length>0){
            callback(err,dealsAtRisk)
        } else {
            callback(err,[])
        }
    });
}

DealsAtRisk.prototype.findDealsAtRiskTodayForUser = function (userId,timezone,callback) {

    var to = moment().tz(timezone).endOf('day')
    var from = moment().tz(timezone).startOf('day')

    dealsAtRiskMeta.find({userId:userId,date:{$gte: new Date(from),$lte:new Date(to)}},{deals:1},function (err,dealsAtRisk) {
        callback(err,dealsAtRisk)
    })
}

DealsAtRisk.prototype.findAtRiskOpp = function (userIds,opportunityId,callback) {

    var to = moment().endOf('day')
    var from = moment().startOf('day')
    var query = {
        userId:{$in:userIds},
        date:{$gte:new Date(from),$lte:new Date(to)}
    }

    dealsAtRiskMeta.aggregate([
        {
            $match:query
        },
        {
            $project:{
                deals:1
            }
        },
        {
            $unwind:"$deals"
        },
        {
            $match:{
                "deals.opportunityId": opportunityId
            }
        }
    ]).exec(function (err,dealsAtRisk) {
        if(!err && dealsAtRisk && dealsAtRisk.length){
            callback(err,dealsAtRisk)
        } else {
            callback(err,[])
        }
    });
}

DealsAtRisk.prototype.findAtRiskOppIds = function (userIds,opportunityIds,callback) {

    var to = moment().endOf('day')
    var from = moment().startOf('day')
    var query = {
        userId:{$in:userIds},
        date:{$gte:new Date(from),$lte:new Date(to)}
    }

    dealsAtRiskMeta.aggregate([
        {
            $match:query
        },
        {
            $project:{
                deals:1
            }
        },
        {
            $unwind:"$deals"
        },
        {
            $match:{
                "deals.opportunityId": {$in:opportunityIds}
            }
        }
    ]).exec(function (err,dealsAtRisk) {
        if(!err && dealsAtRisk && dealsAtRisk.length){
            var deals = {};
            _.each(dealsAtRisk,function (el) {
                deals[el.deals.opportunityId] = el.deals.riskMeter
            })
            callback(err,deals)
        } else {
            callback(err,null)
        }
    });
}

function findDealsAtRisk(userIds,oppCloseDateFrom,oppCloseDateTo,contacts,callback){
    var from = moment(new Date()).subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    dealsAtRiskMeta.aggregate([
        {
            $match:{
                userId:{$in:userIds},
                date:{$gte:new Date(from),$lte:new Date(to)}
            }
        },
        {
            $unwind:"$deals"
        },
        {
            $match:{
                "deals.contactEmailId": {$in:contacts},
                "deals.closeDate": {$gte:new Date(oppCloseDateFrom),$lte:new Date(oppCloseDateTo)}
            }
        }
    ]).exec(function (err,dealsAtRisk) {
        if(!err && dealsAtRisk && dealsAtRisk.length){
            callback(err,dealsAtRisk)
        } else {
            callback(err,[])
        }
    });
}

module.exports = DealsAtRisk;