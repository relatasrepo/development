
var interaction = require('../databaseSchema/userManagementSchema').interaction;
var interactionsOld = require('../databaseSchema/userManagementSchema').interactions;
var User = require('../databaseSchema/userManagementSchema').User;
var emailSender = require('../public/javascripts/emailSender');
var winstonLog = require('../common/winstonLog');

var emailSenders = new emailSender();
var logLib = new winstonLog();
var logger = logLib.getWinston();

function InteractionsMigrate(){
    this.runInteractionMigration = function(){
        logger.info('InteractionsMigrate started **********------------*************')
        var stream = User.find({},{_id:1,emailId:1}).batchSize(100).stream();
        stream.on('data', function (doc) {
            stream.pause();
            getUserAllInteractions(doc,function(){
                stream.resume();
                logger.info('User ',doc.emailId, 'completed-------------------')
            })
        });
        stream.on('error', function (err) {
            // handle err
            emailSenders.sendUncaughtException(err,JSON.stringify(err),err.stack," uncaughtException runInteractionMigration()");
        });

        stream.on('close', function () {
            // all done
            logger.info('InteractionsMigrate completed **********------------*************')
            emailSenders.sendUncaughtException('-----------** DONE **-----------','DONE','DONE'," Completed runInteractionMigration()");
        })
    };
}

function getUserAllInteractions(user,callback) {

    interactionsOld.aggregate([
        {
            $match: {
                $or: [{userId: user._id.toString()}, {emailId: user.emailId}],
                action: {$in: ['sender', 'receiver']}
            }
        },
        {
            $group: {
                _id: "$action",
                refArr: {$addToSet: "$refId"},
                count: {
                    $sum: 1
                }
            }
        }
    ]).exec(function (e, d) {
        if(!e && d != null && d != undefined && d.length > 0){
            var queryOr = [];
            if(checkRequired(d[0]) && checkRequired(d[0]._id)){
                queryOr.push( {
                    action:d[0]._id == 'receiver' ? 'sender' : 'receiver',
                    refId:{$in:d[0].refArr}
                })
            }
            if(checkRequired(d[1]) && checkRequired(d[1]._id)){
                queryOr.push( {
                    action:d[1]._id == 'receiver' ? 'sender' : 'receiver',
                    refId:{$in:d[1].refArr}
                })
            }
            checkDocExists(user,queryOr,callback);
        }
        else{
            emailSenders.sendUncaughtException(e,JSON.stringify(e),e," Error getUserAllInteractions() "+user.emailId);
            callback()
        }
    })
}

function checkDocExists(user,queryOr,callback){
    interaction.findOne({userId:user._id},{userId:1,emailId:1},function(error,intDoc){
        if(error){
            emailSenders.sendUncaughtException(error,JSON.stringify(error),error," Error checkDocExists() findOne() "+user.emailId);
            callback()
        }
        else if(intDoc && intDoc.userId){
            fetchUserInteractionsByRefId(user,queryOr,callback)
        }
        else{
            var userInteractionObj = new interaction( {
                userId: user._id,
                emailId: user.emailId,
                interactions: []
            });
            userInteractionObj.save(function(error,savedDoc){
                if(error){
                    emailSenders.sendUncaughtException(error,JSON.stringify(error),error," Error checkDocExists() save() "+user.emailId);
                    callback()
                }
                else if(savedDoc && savedDoc.userId){
                    fetchUserInteractionsByRefId(user,queryOr,callback)
                }
                else{
                    emailSenders.sendUncaughtException('NONE','NONE','NONE'," Error checkDocExists() save() unknown "+user.emailId);
                    callback()
                }
            })
        }
    });
}

function fetchUserInteractionsByRefId(user,queryOr,callback){

    var stream = interactionsOld.find({$or:queryOr}).batchSize(200).stream();
    var userInteractionsArr = [];
    stream.on('data', function (doc) {

        var newDoc = JSON.parse(JSON.stringify(doc));
        if(!checkRequired(doc.userId)){
            newDoc.userId = null;
        }
        if(!checkRequired(doc.emailId)){
            newDoc.emailId = null;
        }
        if(!checkRequired(doc.firstName)){
            newDoc.firstName = null;
        }
        if(!checkRequired(doc.lastName)){
            newDoc.lastName = null;
        }
        if(!checkRequired(doc.companyName)){
            newDoc.companyName = null;
        }
        if(!checkRequired(doc.designation)){
            newDoc.designation = null;
        }
        if(!checkRequired(doc.profilePicUrl)){
            newDoc.profilePicUrl = null;
        }
        if(!checkRequired(doc.publicProfileUrl)){
            newDoc.publicProfileUrl = null;
        }
        if(!checkRequired(doc.location)){
            newDoc.location = null;
        }
        if(!checkRequired(doc.interactionDate)){
            newDoc.interactionDate = null;
        }
        if(!checkRequired(doc.mobileNumber)){
            newDoc.mobileNumber = null;
        }
        if(!checkRequired(doc.endDate)){
            newDoc.endDate = null;
        }
        if(!checkRequired(doc.action)){
            newDoc.action = null;
        }

        if(!checkRequired(doc.type)){
            newDoc.interactionType = null;
        }else newDoc.interactionType = doc.type;

        //delete doc.type;

        if(!checkRequired(doc.subType)){
            newDoc.subType = null;
        }
        if(!checkRequired(doc.refId)){
            newDoc.refId = null;
        }
        if(!checkRequired(doc.createdDate)){
            newDoc.createdDate = null;
        }
        if(!checkRequired(doc.source)){
            newDoc.source = null;
        }
        if(!checkRequired(doc.title)){
            newDoc.title = null;
        }
        if(!checkRequired(doc.description)){
            newDoc.description = null;
        }
        if(!checkRequired(doc.trackId)){
            newDoc.trackId = null;
        }
        if(!checkRequired(doc.user_twitter_id)){
            newDoc.user_twitter_id = null;
        }
        if(!checkRequired(doc.user_twitter_id)){
            newDoc.user_twitter_name = null;
        }
        if(!checkRequired(doc.user_twitter_id)){
            newDoc.user_facebook_id = null;
        }

        if(newDoc.refId != null){
            userInteractionsArr.push(newDoc)
        }

        if(userInteractionsArr.length == 300){
            stream.pause();
            addInteractions(user,userInteractionsArr,function(){
                userInteractionsArr = [];
                stream.resume();
            })
        }
    });

    stream.on('error', function (err) {
        // handle err
        emailSenders.sendUncaughtException(err,JSON.stringify(err),err," Error fetchUserInteractionsByRefId() 1 "+user.emailId);
        callback()
    });

    stream.on('close', function () {
        if(userInteractionsArr.length > 0){
            addInteractions(user,userInteractionsArr,function(){
                userInteractionsArr = [];
                callback();
            })
        }
        else callback();
    })
}

function addInteractions(user,userInteractionsArr,callback){
    interaction.update({userId:user._id},{$push:{interactions:{$each:userInteractionsArr}}},function(error,result){
        if(error){
            logger.info('Error in addAllInteractions(): InteractionsMigrate ',error,user.emailId);
        }
        else{
            logger.info('addAllInteractions(): InteractionsMigrate Done ',userInteractionsArr.length, result, user.emailId);
        }
       callback()
    });
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

// export the Class
module.exports = InteractionsMigrate;
