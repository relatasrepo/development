//**************************************************************************************
// File Name            : customerProfileTemplate
// Functionality        : Router for customerProfileTemplates(Quotation/Purchase Order/Invoice)
// History              : First Version
//
//
//**************************************************************************************

var customerProfileTemplateCollection = require('../databaseSchema/customerProfileTemplateSchema').customerProfileTemplateModel;
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');

var common = new commonUtility();
var logLib = new winstonLog();
var loggerError = logLib.getCorporateErrorLogger();
var logger = logLib.getCorporateLogger();


function customerProfileTemplateManagement() {
}

customerProfileTemplateManagement.prototype.checkAndCreateCustomerProfileTemplate = function (template,
                                                                                              callback) {
    var parentScope = this;
    this.getTemplateByName(template, function(error, savedTemplate) {
        if(savedTemplate && savedTemplate[0]._id) {
            console.log("the template exist");
            callback(error, savedTemplate);
        } else {
            this.createNewCustomerProfileTemplate(template, function(error, savedTemplate) {
                console.log("the template not exist");
                callback(error, savedTemplate);
            })
        }
    })

};


customerProfileTemplateManagement.prototype.createNewCustomerProfileTemplate = function(companyId,
                                                                                        template,
                                                                                        callback) {
    var newCustomerProfileTemplateObj = createTemplateObj(companyId, template);

    newCustomerProfileTemplateObj.save(function(error, savedTemplate) {
        if(error) {
            logger.info('Error in createNewCustomerProfileTemplate(): customerProfileTemplateManagement', error);
        }
        callback(error, savedTemplate);
    })
}


customerProfileTemplateManagement.prototype.getTemplateByName = function(template, callback) {
    customerProfileTemplateCollection.find({
            companyId: template.companyId,
            customerProfileTemplateType:template.customerProfileTemplateType,
            customerProfileTemplateName:template.customerProfileTemplateName },
        { _id:1,
            customerProfileTemplateType:1,
            customerProfileTemplateName:1 },function(error,result){
            if(error){
                logger.info('Error in getTemplateByName(): customerProfileTemplateManagement', error);
            } else {
                console.log("Result from the management class:", result);
                console.log("Template Id:", result[0]._id);
                callback(error,result)
            }
        })

}


var createTemplateObj = function(companyId, template) {
    return new customerProfileTemplateCollection({
        companyId: companyId,
        customerProfileTemplateName: template.customerProfileTemplateName,
        customerProfileTemplateType: template.customerProfileTemplateType,
        createdDate: new Date()
    });
}


customerProfileTemplateManagement.prototype.constructCustomerProfileTemplateObject = function (templateDetails) {

    var attributesList = [];
    var tableList = [];
    var templObj = new customerProfileTemplateCollection({
        companyId: templateDetails.companyId,
        customerProfileTemplateName: templateDetails.customerProfileTemplateName,
        customerProfileTemplateType: templateDetails.customerProfileTemplateType,
        customerProfileTemplateAttrList: templateDetails.customerProfileTemplateAttrList,
        templTableList: templateDetails.templTableList,
        createdDate: new Date()
    });
    return corporateUserObj;
}


customerProfileTemplateManagement.prototype.getAllCustomerProfileTemplateTypes = function (companyId,
                                                                                           callback) {

    customerProfileTemplateCollection.aggregate([
        {
            $match: {
                companyId:companyId
            }
        },
        {
            $group: {
                _id:"$customerProfileTemplateType",
                count:{$sum:1 }
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}


customerProfileTemplateManagement.prototype.getAllCustomerProfileTemplateNamesByType = function (companyId,
                                                                                                 templateType,
                                                                                                 callback) {

    customerProfileTemplateCollection.aggregate([
        {
            $match: {
                companyId: companyId,
                isTemplateDeactivated: false,
                customerProfileTemplateType: templateType
            }
        },
        {
            $group: {
                _id: "$customerProfileTemplateName",
                count: {$sum: 1}
            }
        }
    ]).exec(function (err, results) {

        callback(err, results)

    });
}

customerProfileTemplateManagement.prototype.getAllCustomerProfileTemplates = function (companyId,
                                                                                       callback) {

    customerProfileTemplateCollection.find({companyId:companyId},
        {},function(err,result){
            if(!err && result){
                callback(err,result)
            } else {
                callback(err,result)
            }
        })
}


customerProfileTemplateManagement.prototype.getCustomerProfileTemplateByTypeAndName = function (companyId,
                                                                                                templateType,
                                                                                                templateName,
                                                                                                callback) {

    customerProfileTemplateCollection.findOne({companyId:companyId,
            customerProfileTemplateType:templateType,
            customerProfileTemplateName:templateName},
        {},function(err,result){
            if(!err && result){
                callback(err,result)
            } else {
                if(callback){
                    callback(err,result)
                }
            }
        })
}


customerProfileTemplateManagement.prototype.getCustomerProfileTemplateById = function (companyId,
                                                                                       templateId,
                                                                                       callback) {

    customerProfileTemplateCollection.findOne({companyId:companyId,
            _id:templateId},
        {},function(err,result){
            if(!err && result){
                callback(err,result)
            } else {
                callback(err,result)
            }
        })
}


customerProfileTemplateManagement.prototype.getAllCustomerProfileTemplateAttributeNames = function (companyId,
                                                                                                           templateType,
                                                                                                           templateName,
                                                                                                           callback) {

    customerProfileTemplateCollection.aggregate([
        {
            $match: {
                companyId: companyId,
                isTemplateDeactivated: false,
                customerProfileTemplateType: templateType,
                customerProfileTemplateName: templateName,
            }
        },
        {
            $group: {
                _id: "$customerProfileTemplateAttrList.attributeName",
                count: {$sum: 1}
            }
        }
    ]).exec(function (err, results) {

        callback(err, results)

    });


}


customerProfileTemplateManagement.prototype.updateCustomerProfileTemplate = function (companyId,
                                                                        templateType,
                                                                        templateName,
                                                                        templateDOM,
                                                                        isActivated,
                                                                        attributeList,
                                                                        tableList,
                                                                        callback) {
    customerProfileTemplateCollection.update({companyId: companyId,
            customerProfileTemplateName: templateName,
            customerProfileTemplateType: templateType},
        {$set:{createdDate: new Date(),
                customerProfileTemplateName: templateName,
                customerProfileTemplateType: templateType,
                isTemplateDeactivated: isActivated,
                isCustomerProfileCreated: false,
                customerProfileTemplateAttrList: attributeList,
                templateTableList: tableList
            }},{upsert:true},function (err,result) {
            callback(err, result);
        });

}


customerProfileTemplateManagement.prototype.updateCustomerProfileTemplateAll = function (companyId,
                                                                                templateId,
                                                                                templateType,
                                                                                templateName,
                                                                                elementList,
                                                                                isActivated,
                                                                                uiId,
                                                                                attributeList,
                                                                                callback) {

    console.log("Updating for ",companyId , templateId );
    customerProfileTemplateCollection.update({companyId: companyId,
            _id : templateId},
        {$set:{
                isTemplateDeactivated: isActivated,
                isCustomerProfileCreated: false,
                customerProfileTemplateElementList: elementList,
                customerProfileTemplateUiId: uiId,
                customerProfileTemplateAttrList: attributeList
            }},{upsert:true},function (err,result) {
            callback(err, result);
        });

}


customerProfileTemplateManagement.prototype.updateCustomerProfileTemplate = function (companyId,
                                                                        templateType,
                                                                        templateName,
                                                                        html,
                                                                        isActivated,
                                                                        callback) {
    customerProfileTemplateCollection.update({companyId: companyId,
            customerProfileTemplateName: templateName,
            customerProfileTemplateType: templateType},
        {$set:{createdDate: new Date(),
                customerProfileTemplateName: templateName,
                customerProfileTemplateType: templateType,
                isTemplateDeactivated: isActivated,
                isCustomerProfileCreated: false,
                customerProfileTemplateDOM: html,
            }},{upsert:true},function (err,result) {
            callback(err, result);
        });

}


customerProfileTemplateManagement.prototype.updateIsTemplateDeactivated = function (companyId,
                                                                             type,
                                                                             templateId,
                                                                             isDeActivated,
                                                                             callback) {

    if (isDeActivated == true){
        customerProfileTemplateCollection.update({companyId: companyId,
                customerProfileTemplateType: type,
                _id: templateId },
            {$set:{
                    isTemplateDeactivated: isDeActivated,
                    isDefaultTemplate: false
                }},{upsert:true},function (err,result) {
                callback(err, result);
            });
    }else {
        customerProfileTemplateCollection.update({companyId: companyId,
                customerProfileTemplateType: type,
                _id: templateId },
            {$set:{
                    isTemplateDeactivated: isDeActivated
                }},{upsert:true},function (err,result) {
                callback(err, result);
            });
    }
}


customerProfileTemplateManagement.prototype.updateCustomerProfileTemplateAsDefault = function (companyId,
                                                                              templateType,
                                                                              templateId,
                                                                              isDefault,
                                                                              callback) {

    customerProfileTemplateCollection.update({companyId: companyId,
            customerProfileTemplateType: templateType,
            _id: {$ne:templateId}
        },
        {$set:{
                isDefaultTemplate: false
            }},{multi:true},function (err,result) {
            callback(err, result);
        });

    customerProfileTemplateCollection.update({companyId: companyId,
            customerProfileTemplateType: templateType,
            _id: templateId },
        {$set:{
                isDefaultTemplate: true
            }},{upsert:true},function (err,result) {
            callback(err, result);
        });
}


customerProfileTemplateManagement.prototype.setCustomerProfileTemplateAsVersionControlled = function (companyId,
                                                                                        templateType,
                                                                                        templateId,
                                                                                        isVersionControlled,
                                                                                        callback) {

    console.log("called setCustomerProfileTemplateAsVersionControlled with isVersionControlled as ", isVersionControlled )
    customerProfileTemplateCollection.update({companyId: companyId,
            customerProfileTemplateType: templateType,
            _id: templateId },
        {$set:{
                isTemplateVersionControlled: isVersionControlled
            }},{upsert:true},function (err,result) {
            callback(err, result);
        });
}

module.exports = customerProfileTemplateManagement;