var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var opportunitiesCollection = require('../databaseSchema/userManagementSchema').opportunities;
var opportunitiesTargetCollection = require('../databaseSchema/userManagementSchema').opportunitiesTarget;
var InteractionsCollection = require('../databaseSchema/userManagementSchema').interactions
var InteractionsV2Collection = require('../databaseSchema/interactionSchema').interactionsV2
var OppTrashCollection = require('../databaseSchema/oppTrashSchema').oppTrash
var dealsAtRiskMetaCollection = require('../databaseSchema/userManagementSchema').dealsAtRiskMeta
var OppMetaDataCollection = require('../databaseSchema/oppMetaData').oppMetaData;
var cacheOppBq = require('../databaseSchema/cacheOppBq').cacheOppBq;
var contactClass = require('../dataAccess/contactsManagementClass');
var oppLogClass = require('../dataAccess/oppLogManagement');
var winstonLog = require('../common/winstonLog');
var SecondaryHierarchy = require('../dataAccess/secondaryHierarchyManagement');
var secondaryHierarchyObj = new SecondaryHierarchy()
var appCredentials = require('../config/relatasConfiguration');

// const {BigQuery} = require('@google-cloud/bigquery');

var contactObj = new contactClass();
var oppLogObj = new oppLogClass();
var moment = require("moment");
var momentTz = require('moment-timezone');
var appCredential = new appCredentials();
// var googleBq = appCredential.googleBq();

var _ = require("lodash");
var async = require("async");
var differenceBy = require('lodash.differenceby');

var logLib = new winstonLog();
var logger = logLib.getWinston();
var loggerError = logLib.getWinstonError();

function OpportunitiesManagement() {

}

OpportunitiesManagement.prototype.loadBQData = function (companyId
                                                         ,userEmailIds
                                                         ,userIds
                                                         ,dateMin
                                                         ,dateMax
                                                         ,primaryCurrency
                                                         ,currenciesObj
                                                         ,fyStart
                                                         ,fyEnd
                                                         ,oppCommitObj
                                                         ,selectedPortfolio
                                                         ,allportfolios
                                                         ,usersDictionary
                                                         ,userProfile
                                                         ,callback) {
    var allDates = [];

    async.parallel([
        function (callback) {
            getDataBQ(companyId,userEmailIds,dateMin,dateMax,primaryCurrency,currenciesObj,selectedPortfolio,fyStart,fyEnd,allportfolios,userProfile,callback);
        },
        function (callback) {
            getTargets(userIds,dateMin,dateMax,fyStart,fyEnd,allportfolios,callback)
        },
        function (callback) {

            var datesInBetween = getMonthsBetweenDatesForTargets(dateMin,dateMax);
            var monthYears = [];

            _.each(datesInBetween,function(a){
                allDates.push({
                    monthYear: moment(a).month()+""+moment(a).year(),
                    date:moment(moment(a).add(1,"day")),
                    amount:0
                });

                monthYears.push(moment(a).month()+""+moment(a).year())
            })

            var monthYears = datesInBetween.map(function(a){
                return moment(a).month()+""+moment(a).year()
            });

            oppCommitObj.getCommitsByDateMultiUsersWithProjection(userIds,dateMin,dateMax,null,callback)
        }
    ], function (errors,data) {
        callback(errors,attachTargetsAndCommits(data,usersDictionary,allDates));
    });

}

function getMonthsBetweenDatesForTargets(from,to) {
    var startDate = moment(from).add(2, 'days');
    var endDate = moment(to).add(2, 'days');

    var result = [];

    if (endDate.isBefore(startDate)) {
        throw "End date must be greated than start date."
    }

    var currentDate = startDate.clone();

    while (currentDate.isBefore(endDate)) {
        result.push(currentDate.format("MMM YYYY"));
        currentDate.add(1, 'month');
    }

    return result;
}

function getTargets(userIds,fromDate,toDate,fyStart,fyEnd,allportfolios,callback) {

    var portTarget = {};
    var portfolioTargets = [];

    if(allportfolios){

        var allUsers = [];
        var allPortTr = {};

        _.each(allportfolios,function(po){
            allUsers = allUsers.concat(po.users);
        });

        var allUsers__grp_data = _
            .chain(allUsers)
            .groupBy('userId')
            .map(function(opps, key) {
                // var userTr = [];
                // _.each(user.targets,function(tr){
                //
                //     _.each(tr.values,function(va){
                //         if(new Date(va.date) >= new Date(fromDate) && new Date(va.date) <= new Date(toDate)){
                //             va.target = va.amount;
                //             userTr.push(va);
                //         }
                //     });
                // });
            })
            .value();

        allportfolios[0].users.forEach(function(user){
            var userTr = [];
            _.each(user.targets,function(tr){

                _.each(tr.values,function(va){
                    if(new Date(va.date) >= new Date(fromDate) && new Date(va.date) <= new Date(toDate)){
                        va.target = va.amount;
                        userTr.push(va);
                    }
                });
            });

            portfolioTargets.push({
                _id:user.userId,
                targets: userTr
            });
        });

        callback(null,portfolioTargets)
    } else {

        OpportunitiesManagement.prototype.getGrpTargetForUserByMonths(userIds,fromDate,toDate,fyStart,fyEnd,function (err,users) {

            var datesInBetween = getMonthsBetweenDatesForTargets(fromDate,toDate);
            if(!err && users){

                _.each(users,function (user) {
                    var monthsSet = _.pluck(user.targets,"monthYear");
                    var notSet = _.difference(datesInBetween,monthsSet)
                    _.each(notSet,function (el) {

                        user.targets.push({
                            monthYear:el,
                            "target": 0,
                            "date": new Date(moment(new Date(el)).add(1,'d')) //compensate for timezones
                        });
                    });
                });

                callback(err,users)

            } else {
                callback(err,null)
            }
        });
    }
}

function attachTargetsAndCommits(allData,usersDictionary,allDates) {

    var data = allData[0].processed;
    var leaderboard = allData[0] && allData[0].leaderboard?allData[0].leaderboard:null;
    var existingUserIds = _.uniq(allData[0].existingUserIds);
    var targets = allData[1];
    var commits = allData[2];
    var allUserIds = [];

    var trObj = {}
    _.each(targets,function (tr) {
        allUserIds.push(String(tr._id));
        trObj[tr._id] = tr.targets;
    });

    if(!data){
        data = [];
    }

    var notSet = _.difference(allUserIds,existingUserIds);

    if(notSet && notSet.length>0 && !leaderboard){
        _.each(notSet,function(el){
            data.push({
                userEmailId:usersDictionary[el].emailId,
                userId:el,
                userEmailId_original:usersDictionary[el].emailId,
                userId_original:el,
                totalDeals:0,
                wonDeals:0,
                totalOppAmount:0,
                wonAmount:0,
                lostAmount:0,
                interactionsCountWon:0,
                interactionsCountAll:0,
                daysToLostCloseDeal:0,
                daysToWinCloseDeal:0,
                allContacts:[],
                companies:[],
                products:[],
                verticals:[],
                regions:[],
                bus:[],
                monthlyCreated: [],
                monthlyClosed: [],
                monthlyOppWon: [],
                contactsByRelation: {
                    influencers:[],
                    dms:[],
                    partners:[],
                    owners:[]
                }
            })
        })
    };

    if(leaderboard){
        _.each(leaderboard,function (lb) {
            _.each(lb.processed,function (el) {
                var targetsCount = 0;
                var targets = [];
                if(el.userEmailId_original){
                    el.commits = commits[el.userEmailId_original];
                } else if(el.userEmailId){
                    el.commits = commits[el.userEmailId];
                }

                if(trObj[el.userId_original]){
                    _.each(trObj[el.userId_original],function (tr) {
                        tr.amount = tr.target
                        targets.push(tr);
                        targetsCount = targetsCount+tr.target
                    })
                } else {
                    _.each(trObj[el.userId],function (tr) {
                        tr.amount = tr.target
                        targets.push(tr);
                        targetsCount = targetsCount+tr.target
                    })
                }

                if(!el.commits){
                    el.commits = [];
                }
                el.commits = el.commits.concat(differenceBy(allDates,el.commits,"monthYear"))

                el.targetsCount = targetsCount;
                el.targets = targets;
            })
        });
    }

    _.each(data,function (el) {

        var targetsCount = 0;
        var targets = [];
        if(el.userEmailId_original){
            el.commits = commits[el.userEmailId_original];
        } else if(el.userEmailId){
            el.commits = commits[el.userEmailId];
        }

        if(trObj[el.userId_original]){
            _.each(trObj[el.userId_original],function (tr) {
                tr.amount = tr.target
                targets.push(tr);
                targetsCount = targetsCount+tr.target
            })
        } else {
            _.each(trObj[el.userId],function (tr) {
                tr.amount = tr.target
                targets.push(tr);
                targetsCount = targetsCount+tr.target
            })
        }

        if(!el.commits){
            el.commits = [];
        }

        el.commits = el.commits.concat(differenceBy(allDates,el.commits,"monthYear"));

        el.targetsCount = targetsCount;
        el.targets = targets;
    });

    return {
        leaderboard:allData[0].leaderboard,
        thisQtr:data,
        thisFy:allData[0].opps_thisFy,
        prevQtr:allData[0].opps_prevQtr
    };
}

// async function getDataBQ(companyId,userEmailIds,dateMin,dateMax,primaryCurrency,currenciesObj,selectedPortfolio,fyStart,fyEnd,allportfolios,userProfile,callback) {
function getDataBQ(companyId,userEmailIds,dateMin,dateMax,primaryCurrency,currenciesObj,selectedPortfolio,fyStart,fyEnd,allportfolios,userProfile,callback) {

    try {

        if(allportfolios && allportfolios[0] && allportfolios[0].users){
            var leaderboard = {};
            var allUsers = [];
            _.each(allportfolios,function(po){
                allUsers = allUsers.concat(po.users);
            });

            async.eachSeries(allUsers, function(user, next) {

                getOppFromOppCache(companyId,userEmailIds,dateMin,dateMax,primaryCurrency,currenciesObj,[user],fyStart,fyEnd,true,userProfile,function(err_a,opps){

                    if(!leaderboard[user.ownerEmailId]){
                        leaderboard[user.ownerEmailId] = {
                            processed:[],
                            existingUserIds: []
                        }
                    };

                    leaderboard[user.ownerEmailId].processed = leaderboard[user.ownerEmailId].processed.concat(opps.processed);
                    leaderboard[user.ownerEmailId].existingUserIds = leaderboard[user.ownerEmailId].existingUserIds.concat(opps.existingUserIds);
                    leaderboard[user.ownerEmailId].processed = _.uniq(leaderboard[user.ownerEmailId].processed,'opportunityId');
                    next();
                })
            }, function(error3,leaderboardData) {

                if(callback){
                    callback(error3,{
                        leaderboard:leaderboard
                    });
                }
            });
        } else {
            getOppFromOppCache(companyId,userEmailIds,dateMin,dateMax,primaryCurrency,currenciesObj,selectedPortfolio,fyStart,fyEnd,false,userProfile,function(err_c,data){
                if(callback){
                    callback(err_c,data);
                }
            })
        }

    } catch(err){
        if(callback){
            callback(err,err);
        }
    }
}

function bqQueryForUsers(userEmailIds){
    var userEmailIds_sql = userEmailIds.map(function (a) { return "'" + a.replace("'", "''") + "'"; }).join();
    return `SELECT
                          a.opportunityId,
                          a.opportunityName,
                          a.userEmailId,
                          a.contactEmailId,
                          a.createdDate,
                          a.closeDate,
                          a.currency,
                          CAST(a.amount AS FLOAT64),
                          a.netGrossMargin,
                          a.stageName,
                          a.relatasStage,
                          a.productType,
                          a.vertical,
                          a.businessUnit,
                          a.companyId.oid as companyId,
                          a._id.oid as _id,
                          a.userId.oid as userId,
                          a.geoLocation.zone as zone,
                          a.geoLocation.town as town,
                        COUNT(b._id.oid) AS interactionCount
                        FROM
                        relatas_showcase.opportunities a LEFT JOIN relatas_showcase.interactionsV2 b
                        ON a.userEmailId = b.ownerEmailId
                        AND a.contactEmailId=b.emailId
                        AND interactionDate BETWEEN a.createdDate AND closeDate
                        WHERE
                          a.userEmailId IN (`+userEmailIds_sql+`)
                        GROUP BY
                          a.opportunityId,
                          a.opportunityName,
                          a.userEmailId,
                          a.contactEmailId,
                          a.createdDate,
                          a.closeDate,
                          a.currency,
                          a.amount,
                          a.netGrossMargin,
                          a.stageName,
                          a.relatasStage,
                          a.productType,
                          a.vertical,
                          a.companyId.oid,
                          a._id.oid,
                          a.userId.oid,
                          a.geoLocation.zone,
                          a.geoLocation.town,
                          a.businessUnit
                        ORDER BY
                          userEmailId,
                          interactionCount DESC`;
}

function bqQueryForCompany(companyId){
    return `SELECT
              a.opportunityId,
              a.opportunityName,
              a.userEmailId,
              a.contactEmailId,
              a.createdDate,
              a.closeDate,
              a.currency,
              CAST(a.amount AS FLOAT64),
              a.netGrossMargin,
              a.stageName,
              a.relatasStage,
              a.productType,
              a.vertical,
              a.businessUnit,
              a.companyId.oid as companyId,
              a._id.oid as _id,
              a.userId.oid as userId,
              a.geoLocation.zone as zone,
              a.geoLocation.town as town,
            COUNT(b._id.oid) AS interactionCount
            FROM
            relatas_showcase.opportunities a LEFT JOIN relatas_showcase.interactionsV2 b
            ON a.userEmailId = b.ownerEmailId
            AND a.contactEmailId=b.emailId
            AND interactionDate BETWEEN a.createdDate AND closeDate
            GROUP BY
              a.opportunityId,
              a.opportunityName,
              a.userEmailId,
              a.contactEmailId,
              a.createdDate,
              a.closeDate,
              a.currency,
              a.amount,
              a.netGrossMargin,
              a.stageName,
              a.relatasStage,
              a.productType,
              a.vertical,
              a.companyId.oid,
              a._id.oid,
              a.userId.oid,
              a.geoLocation.zone,
              a.geoLocation.town,
              a.businessUnit
            ORDER BY
              userEmailId,
              interactionCount DESC`;
}

function getOppFromOppCache(companyId,forUsers,dateMin,dateMax,primaryCurrency,currenciesObj,selectedPortfolio,fyStart,fyEnd,forRank,userProfile,callback){
    OpportunitiesManagement.prototype.getOppForSingleByDateFromCache(companyId,forUsers,dateMin,dateMax,selectedPortfolio,fyStart,fyEnd,forRank,function(err,opp_data){

        var data = {
            processed: [],
            existingUserIds: []
        };

        var rows = opp_data.opps_thisQtr;

        if(!err && rows.length>0){

            rows.forEach(function(el){
                if(forRank && selectedPortfolio && selectedPortfolio[0]){
                    el.userId_original = String(el.userId);
                    el.userEmailId_original = String(el.userEmailId);

                    el.userId = selectedPortfolio[0].userId;
                    el.userEmailId = selectedPortfolio[0].ownerEmailId;
                } else {
                    el.userId_original = String(el.userId);
                    el.userEmailId_original = String(el.userEmailId);
                }
            });

            var oppObj = {};

            var grp_data = _
                .chain(rows)
                .groupBy('userId')
                .map(function(opps, key) {
                    data.existingUserIds.push(key);
                    _.each(opps,function (op) {

                        if(!oppObj[String(op.userId)]){
                            oppObj[String(op.userId)] = []
                            oppObj[String(op.userId)].push(op)
                        } else {
                            oppObj[String(op.userId)].push(op)
                        }
                    });

                    return {
                        userId:key,
                        opportunities:opps
                    }
                })
                .value();

            data.processed = getOppData(grp_data,oppObj,dateMin,dateMax,primaryCurrency,currenciesObj);
        } else {

            if(selectedPortfolio && selectedPortfolio[0]){
                data.processed = getFiller(selectedPortfolio[0].ownerEmailId,selectedPortfolio[0].userId);
            } else {
                data.processed = getFiller(userProfile.emailId,String(userProfile._id));
            }
        }

        if(!err && opp_data.opps_prevQtr && opp_data.opps_prevQtr.length>0){

            var oppObj_prevQtr = {};

            var grp_data_prevQtr = _
                .chain(opp_data.opps_prevQtr)
                .groupBy('userId')
                .map(function(opps, key) {
                    data.existingUserIds.push(key);
                    _.each(opps,function (op) {
                        if(!oppObj_prevQtr[String(op.userId)]){
                            oppObj_prevQtr[String(op.userId)] = []
                            oppObj_prevQtr[String(op.userId)].push(op)
                        } else {
                            oppObj_prevQtr[String(op.userId)].push(op)
                        }
                    });

                    return {
                        userId:key,
                        opportunities:opps
                    }
                })
                .value();

            data.opps_prevQtr = getOppData(grp_data_prevQtr,oppObj_prevQtr,new Date(moment(dateMin).subtract(3,"month")),new Date(moment(dateMax).subtract(3,"month")),primaryCurrency,currenciesObj);
        }

        if(!err && opp_data.opps_thisFy && opp_data.opps_thisFy.length>0){

            var oppObj_thisFy = {};

            var grp_data_thisFy = _
                .chain(opp_data.opps_thisFy)
                .groupBy('userId')
                .map(function(opps, key) {
                    data.existingUserIds.push(key);
                    _.each(opps,function (op) {
                        if(!oppObj_thisFy[String(op.userId)]){
                            oppObj_thisFy[String(op.userId)] = []
                            oppObj_thisFy[String(op.userId)].push(op)
                        } else {
                            oppObj_thisFy[String(op.userId)].push(op)
                        }
                    });

                    return {
                        userId:key,
                        opportunities:opps
                    }
                })
                .value();

            if(new Date(dateMin)< new Date(fyStart)){
                fyStart = moment(fyStart).subtract(1,"year");
                fyEnd = moment(fyEnd).subtract(1,"year");
            }

            data.opps_thisFy = getOppData(grp_data_thisFy,oppObj_thisFy,fyStart,fyEnd,primaryCurrency,currenciesObj);
        }
        callback(err,data);
    });
}

function getFiller(userEmailId,userId){
    return [{
        userEmailId:userEmailId,
        userId:userId,
        userEmailId_original:userEmailId,
        userId_original:userId,
        totalDeals:0,
        wonDeals:0,
        totalOppAmount:0,
        wonAmount:0,
        lostAmount:0,
        interactionsCountWon:0,
        interactionsCountAll:0,
        daysToLostCloseDeal:0,
        daysToWinCloseDeal:0,
        allContacts: [],
        companies: [],
        products: [],
        verticals: [],
        regions: [],
        bus: [],
        monthlyCreated: [],
        monthlyClosed: [],
        monthlyOppWon: [],
        contactsByRelation: {
            influencers:[],
            dms:[],
            partners:[],
            owners:[]
        }
    }]
}

function storeInOppCache(companyId,opps,callback){

    _.each(opps,function (op) {

        op.companyId = companyId;

        if(!op.geoLocation){
            op.geoLocation = {
                zone: op.zone,
                town: op.town
            }
        }

        if(op.createdDate.value){
            op.createdDate = new Date(op.createdDate.value);
        }

        if(op.closeDate.value){
            op.closeDate = new Date(op.closeDate.value);
        }

        if(op.f0_ && !op.amount){
            op.amount = op.f0_;
        }
    });

    cacheOppBq.remove({companyId:companyId},function(err,result_rm){
        cacheOppBq.collection.insert(opps,function(err,result){
            callback(err,result);
        })
    });
}

function getOppData(opps,oppsCreatedObj,dateMin,dateMax,primaryCurrency,currenciesObj){
    var data = [];

    _.each(opps,function (opp) {
        var oppCreated = oppsCreatedObj?oppsCreatedObj[String(opp.userId)]:null;
        data.push(oppGroupByStage(opp.opportunities,oppCreated,dateMin,dateMax,primaryCurrency,currenciesObj))
    });

    return data;

}

function oppGroupByStage(opps,oppsCreatedObj,fromDate,toDate,primaryCurrency,currenciesObj) {

    var interactionsCountAll=0,
        interactionsCountWon=0,
        interactionsCountLost=0,
        daysToWinCloseDeal=0,
        daysToLostCloseDeal=0,
        lostAmount=0,
        wonAmount=0,
        totalOppAmount=0,
        totalDeals=0,
        wonDeals=0,
        userId = "",
        allContacts = [],
        influencers = [],
        dms = [],
        partners = [],
        owners = [],
        companies = [],
        products = [],
        verticals = [],
        regions = [],
        bus = [],
        companies_yoy = [],
        products_yoy = [],
        verticals_yoy = [],
        regions_yoy = [],
        bus_yoy = [],
        monthlyCreated = [],
        monthlyClosed = [],
        monthlyOppWon = [],
        userEmailId = "";
        userId_original = "";
        userEmailId_original = "";

    var won = ['Close Won','Closed Won'];
    var lost = ['Close Lost','Closed Lost'];

    var yoyDateMin = new Date(moment(fromDate).subtract(1,"year"));
    var yoyDateMax = new Date(moment(toDate).subtract(1,"year"));

    if(oppsCreatedObj){

        _.each(oppsCreatedObj,function (op) {
            if(!op.createdDate){
                op.createdDate = op._id.getTimestamp();
            }

            if(new Date(op.createdDate)>= new Date(fromDate) && new Date(op.createdDate)<= new Date(toDate)){
                monthlyCreated.push(op.createdDate)
            }

        });
    }

    _.each(opps,function (op) {

        if(!op.createdDate){
            op.createdDate = op._id.getTimestamp();
        }

        op.amountWithNgm = op.amount;

        if(op.netGrossMargin || op.netGrossMargin == 0){
            op.amountWithNgm = (op.amount*op.netGrossMargin)/100
        }

        op.convertedAmt = op.amount;
        op.convertedAmtWithNgm = op.amountWithNgm

        if(op.currency && op.currency !== primaryCurrency){

            if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                op.convertedAmt = op.amount/currenciesObj[op.currency].xr
            }

            if(op.netGrossMargin || op.netGrossMargin == 0){
                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
            }

            op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

        }

        allContacts.push(op.contactEmailId)
        owners.push(op.contactEmailId)

        totalDeals++;
        userId = op.userId;
        userId_original = op.userId_original?op.userId_original:op.userId;

        userEmailId = op.userEmailId;
        userEmailId_original = op.userEmailId_original?op.userEmailId_original:op.userEmailId;

        var a = moment(op.createdDate);
        var b = moment(op.closeDate);

        if(_.contains(won,op.relatasStage)){

            if(new Date(op.closeDate)>= new Date(yoyDateMin) && new Date(op.closeDate)<= new Date(yoyDateMax)){
                regions_yoy.push({
                    name:op.geoLocation.zone,
                    nameTruncated:op.geoLocation.zone,
                    amount:op.convertedAmtWithNgm
                });

                products_yoy.push({
                    name:op.productType,
                    nameTruncated:op.productType,
                    amount:op.convertedAmtWithNgm
                });

                verticals_yoy.push({
                    name:op.vertical,
                    nameTruncated:op.vertical,
                    amount:op.convertedAmtWithNgm
                });

                bus_yoy.push({
                    name:op.businessUnit,
                    nameTruncated:op.businessUnit,
                    amount:op.convertedAmtWithNgm
                });
            }

            if(new Date(op.closeDate)>= new Date(fromDate) && new Date(op.closeDate)<= new Date(toDate)){

                monthlyOppWon.push({
                    date:op.closeDate,
                    amount:op.convertedAmtWithNgm
                })

                monthlyClosed.push(op.closeDate);

                companies.push({
                    name:fetchCompanyFromEmail(op.contactEmailId),
                    nameTruncated:fetchCompanyFromEmail(op.contactEmailId),
                    amount:op.convertedAmtWithNgm
                });

                regions.push({
                    name:op.geoLocation.zone,
                    nameTruncated:op.geoLocation.zone,
                    amount:op.convertedAmtWithNgm
                });

                products.push({
                    name:op.productType,
                    nameTruncated:op.productType,
                    amount:op.convertedAmtWithNgm
                });

                verticals.push({
                    name:op.vertical,
                    nameTruncated:op.vertical,
                    amount:op.convertedAmtWithNgm
                });

                bus.push({
                    name:op.businessUnit,
                    nameTruncated:op.businessUnit,
                    amount:op.convertedAmtWithNgm
                });

                wonDeals++;
                wonAmount = wonAmount+op.convertedAmtWithNgm;
                if(op.interactionCount){
                    interactionsCountWon = interactionsCountWon+op.interactionCount
                }

                if(b.diff(a, 'days') === 0){
                    daysToWinCloseDeal = daysToWinCloseDeal+1 //Minimum one day taken to close opp
                } else {
                    daysToWinCloseDeal = daysToWinCloseDeal+Math.abs(b.diff(a, 'days'));
                }
            }


        } else if(_.contains(lost,op.relatasStage)){

            if(op.interactionCount){
                interactionsCountLost = interactionsCountLost+op.interactionCount
            }

            daysToLostCloseDeal = daysToLostCloseDeal+Math.abs(b.diff(a, 'days'));


            lostAmount = lostAmount+op.convertedAmtWithNgm;
        }

        interactionsCountAll = interactionsCountLost+interactionsCountWon;
        totalOppAmount = totalOppAmount+op.convertedAmtWithNgm;

    });

    return {
        userEmailId:userEmailId,
        userId:userId,
        userEmailId_original:userEmailId_original,
        userId_original:userId_original,
        totalDeals:totalDeals,
        wonDeals:wonDeals,
        totalOppAmount:totalOppAmount,
        wonAmount:wonAmount,
        lostAmount:lostAmount,
        interactionsCountWon:interactionsCountWon,
        interactionsCountAll:interactionsCountAll,
        daysToLostCloseDeal:daysToLostCloseDeal,
        daysToWinCloseDeal:daysToWinCloseDeal,
        allContacts:_.uniq(allContacts),
        companies: companies,
        products: products,
        verticals: verticals,
        regions: regions,
        bus: bus,
        products_yoy: products_yoy,
        verticals_yoy: verticals_yoy,
        regions_yoy: regions_yoy,
        bus_yoy: bus_yoy,
        monthlyCreated: monthlyCreated,
        monthlyClosed: monthlyClosed,
        monthlyOppWon: monthlyOppWon,
        contactsByRelation: {
            influencers:_.uniq(influencers),
            dms:_.uniq(dms),
            partners:_.uniq(partners),
            owners:_.uniq(owners)
        }
    }

}

OpportunitiesManagement.prototype.insertOrUpdateOpportunitiesBulk = function (updateObj,callback) {
    var bulk = opportunitiesCollection.collection.initializeUnorderedBulkOp();
    var usersWithAccess = [],
        companyId = null,
        opportunityId = null;

    updateObj.forEach(function(a){
        usersWithAccess = a.usersWithAccess || [];
        opportunityId = a.opportunityId;
        companyId = a.companyId;

        var geoLocation = {town:null,zone:null};
        if(a.geoLocation){
            geoLocation = a.geoLocation
        }

        bulk.find(
                {
                    userId:a.userId,
                    salesforceContactId:a.salesforceContactId?a.salesforceContactId:null,
                    opportunityId:a.opportunityId,
                    contactEmailId:a.contactEmailId,
                    mobileNumber:a.mobileNumber ? a.mobileNumber : null,
                    userEmailId:a.userEmailId
                }
            )
            .upsert()
            .updateOne(
                {$set:{
                    opportunityName:a.opportunityName,
                    amount:a.amount,
                    userId:a.userId,
                    opportunityId:a.opportunityId,
                    contactEmailId:a.contactEmailId,
                    netGrossMargin: a.netGrossMargin,
                    isClosed:a.isClosed,
                    isWon:a.isWon,
                    stageName:a.stageName,
                    source:a.source,
                    relatasStage:a.relatasStage,
                    createdDate:a.createdDate,
                    closeDate:a.closeDate,
                    closeReasons:a.closeReasons,
                    geoLocation:geoLocation,
                    influencers: a.influencers,
                    partners: a.partners,
                    decisionMakers: a.decisionMakers,
                    productType:a.productType,
                    sourceType:a.sourceType,
                    vertical:a.vertical,
                    solution:a.solution,
                    type:a.type,
                    businessUnit:a.businessUnit,
                    currency:a.currency,
                    xr:parseFloat(a.xr),
                    BANT:a.BANT,
                    competitor:a.competitor,
                    companyId:a.companyId,
                    userEmailId:a.userEmailId,
                    createdByEmailId:a.createdByEmailId,
                    usersWithAccess:a.usersWithAccess,
                    accounts:a.accounts,
                    masterData:a.masterData?a.masterData:[],
                    renewed:a.renewed,
                    renewalStatusSet:a.renewalStatusSet,
                    sourceOpportunityId:a.sourceOpportunityId?a.sourceOpportunityId:null
                }}
            );
    });

    if(updateObj.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                loggerError.info('Error in insertOrUpdateOpportunitiesBulk():opportunitiesManagement ',err );
                callback(false)
            }
            else {
                result = result.toJSON();

                // if(usersWithAccess && usersWithAccess.length>0){
                //     secondaryHierarchyObj.addInternalTeamAccess(usersWithAccess,companyId,opportunityId);
                // } else {
                //     secondaryHierarchyObj.removeInternalTeamAccess(companyId,opportunityId);
                // }
                // updateAllHierarchies(updateObj[0].userId,companyId,opportunityId,updateObj[0],true)
                logger.info('insertOrUpdateOpportunitiesBulk() results ',result);
                callback(result)
            }
        });
    }
    else{
        callback(true)
    }
}

OpportunitiesManagement.prototype.insertOrUpdateOpportunitiesBulk2 = function (updateObj,callback) {
    var bulk = opportunitiesCollection.collection.initializeUnorderedBulkOp();

    updateObj.forEach(function(a){
        var geoLocation = {town:null,zone:null};
        if(a.geoLocation){
            geoLocation = a.geoLocation
        }

        bulk.find(
                {
                    userId:a.userId,
                    contactEmailId:a.contactEmailId,
                    userEmailId:a.userEmailId,
                    opportunityName:a.opportunityName,
                    amount:a.amount
                }
            )
            .upsert()
            .updateOne(
                {$set:{
                    productType:a.productType
                }}
            );
    });
    if(updateObj.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                loggerError.info('Error in insertOrUpdateOpportunitiesBulk()2:opportunitiesManagement ',err );
                callback(false)
            }
            else {
                result = result.toJSON();

                logger.info('insertOrUpdateOpportunitiesBulk()2 results ',result);
                callback(result.upserted)
            }
        });
    }
    else{
        callback(true)
    }
}

OpportunitiesManagement.prototype.updateOpportunityForContact = function (userId,opportunityId,updateObj,callback) {

    opportunitiesCollection.update({userId:userId,opportunityId:opportunityId},{$set:updateObj},function(err,result){

        if(err){
            loggerError.info('Error in updateOpportunityForContact():opportunitiesManagement ',err)
            callback(err,result)
        }
        else{
            callback(err,result)
        }
    })
}

OpportunitiesManagement.prototype.updateOpportunityForUsersWithAccess = function (userId,userEmailIds,opportunityId,updateObj,oppAccessible,callback) {

    var findQuery = {
        opportunityId:opportunityId,
        $or:[{
            userEmailId:{$in:userEmailIds}
        }]
    }

    if(checkRequired(userEmailIds)){
        if(findQuery.$or){
            findQuery.$or.push({
                "usersWithAccess.emailId":{$in:userEmailIds}
            })
        }
    }

    opportunitiesCollection.update(findQuery,{$set:updateObj},function(err,result){

        if(err){
            loggerError.info('Error in updateOpportunityForUsersWithAccess():opportunitiesManagement ',err)
            callback(err,result)
        }
        else{
            if(updateObj.usersWithAccess && updateObj.usersWithAccess.length>0){
                secondaryHierarchyObj.addInternalTeamAccess(updateObj.usersWithAccess,updateObj.companyId,opportunityId);
            } else {
                secondaryHierarchyObj.removeInternalTeamAccess(updateObj.companyId,opportunityId);
            }

            updateAllHierarchies(updateObj.userId,updateObj.companyId,opportunityId,updateObj,oppAccessible)

            callback(err,result)
        }
    })
}

function updateAllHierarchies(userId,companyId,opportunityId,opp,oppAccessible,callback){

    // secondaryHierarchyObj.getAllHierarchyTypesWithItemAccess(userId,function (errors, hierarchies) {
    secondaryHierarchyObj.getAllHierarchyTypesWithItemAccessByCompanyId(companyId,function (errors, hierarchyList) {

        if(!errors && hierarchyList && hierarchyList.length>0){
            async.eachSeries(hierarchyList, function (shType, next){

                if(shType.secondaryHierarchyType === "All_Access" || shType.secondaryHierarchyType === "org" || shType.secondaryHierarchyType === "Revenue"){
                    secondaryHierarchyObj.updateAccessForSingleOpp(shType.userId,companyId,opportunityId,shType,oppAccessible,function () {
                        next();
                    });
                } else {

                    if(checkShTypeAccess(shType.secondaryHierarchyType,_.pluck(shType.secondaryHierarchyItemsList,'name'),opp)){
                        secondaryHierarchyObj.updateAccessForSingleOpp(shType.userId,companyId,opportunityId,shType,oppAccessible,function () {
                            next();
                        });
                    } else {
                        next();
                    }
                }

            }, function(err) {
                if(callback){
                    callback()
                }
            });
        } else {
            if(callback){
                callback()
            }
        }
    });
}

function checkShTypeAccess(sHType,shList,opp){

    var hasAccess = false;

    if (sHType === "Product" && _.includes(shList,opp.productType)){
        hasAccess = true;
    } else if (sHType === 'BusinessUnit' && _.includes(shList,opp.businessUnit)){
        hasAccess = true;
    } else if (sHType === 'Region' && opp.geoLocation && opp.geoLocation.zone && _.includes(shList,opp.geoLocation.zone)){
        hasAccess = true;
    } else if (sHType === 'Vertical' && _.includes(shList,opp.vertical)){
        hasAccess = true;
    } else if (sHType === 'Solution' && _.includes(shList,opp.solution)){
        hasAccess = true;
    } else if (sHType === 'Type' && _.includes(shList,opp.type)){
        hasAccess = true;
    }

    return hasAccess;
}

OpportunitiesManagement.prototype.removeInternalTeamAccess = function (companyId,opportunityId,callback) {
    secondaryHierarchyObj.removeInternalTeamAccess(companyId,opportunityId,callback);
}

OpportunitiesManagement.prototype.removeInternalTeamAccessForMultiUsers = function (usersWithAccess,companyId,opportunityId,callback) {
    secondaryHierarchyObj.removeInternalTeamAccessForMultiUsers(usersWithAccess,companyId,opportunityId,callback);
}

OpportunitiesManagement.prototype.findOpportunityById = function (userId,opportunityId,callback) {
    opportunitiesCollection.find({userId:userId,opportunityId:opportunityId},function(err,result){
        if(err){
            loggerError.info('Error in findOpportunityById():opportunitiesManagement ',err)
            callback(err,result)
        }
        else{
            callback(err,result)
        }
    })
}

OpportunitiesManagement.prototype.findOpportunityByIdForMultipleUsers = function (userIds,opportunityId,callback) {

    opportunitiesCollection.findOne({opportunityId:opportunityId}).lean().exec(function(err,result){

        if(err){
            loggerError.info('Error in findOpportunityById():opportunitiesManagement ',err)
        }

        callback(err,result)
    })
}

OpportunitiesManagement.prototype.findOpportunityCustomQuery = function (findQuery,callback) {
    opportunitiesCollection.find(findQuery,function(err,result){
        if(err){
            loggerError.info('Error in findOpportunityCustomQuery():opportunitiesManagement ',err)
            callback(err,result)
        }
        else{
            callback(err,result)
        }
    })
}

//not using
OpportunitiesManagement.prototype.insertNonExistingOpportunities = function (userId,updateObj,opportunityIds,callback) {
    opportunitiesCollection.find({userId:userId,opportunityId:{$in:opportunityIds}},function(error, result){
        if(error){
            logger.error('Error in opportunityManagement - insertNonExistingOpportunities()', error);
            callback(false)
        }
        else{
            var nonExistsOpp = [];
            var existingOpp = _.pluck(result,"opportunityId");
            if(existingOpp.length > 0) {
                updateObj.forEach(function (a) {
                    if (existingOpp.indexOf(a.opportunityId) == -1) {
                        nonExistsOpp.push(a);
                    }
                })
            }
            else{
                nonExistsOpp = updateObj;
            }
            if(nonExistsOpp.length > 0) {
                insertAllOpportunities(nonExistsOpp, function (result) {
                    callback(result);
                })
            }
            else{
                callback(true);
            }
        }
    });

}

OpportunitiesManagement.prototype.getOpportunitiesByContact = function (userId,contactEmail,contactMobile,callback) {
    var q = {};
    q["userId"] = userId;
    q["contactEmailId"] = contactEmail;

    opportunitiesCollection.find(q,function(error, result){
        if(error){
            loggerError.info('Error in getOpportunitiesByContact():opportunitiesManagement ',error)
            callback(error,null)
        }
        else{
            callback(null,result)
        }
    });
}

OpportunitiesManagement.prototype.getOpportunitiesByByDate = function (userIds,fromDate,toDate,existingOpps,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId);

    var match = {
        $or:[{userId:{$in:userIds}},
            {
                $and:accessControlQuery
            }
        ]
    }

    if(!fromDate && !toDate){
        match["closeDate"] = {$lte:new Date(moment().add(30, "days")),$gte:new Date(moment().startOf('day'))};
    }

    if(existingOpps){
        match["_id"] = {$nin:existingOpps}
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds}
        }

        if(!fromDate && !toDate){
            match["closeDate"] = {$lte:new Date(moment().add(30, "days")),$gte:new Date(moment().startOf('day'))};
        }

        if(existingOpps){
            match["_id"] = {$nin:existingOpps}
        }
    }

    match["stageName"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    match["isClosed"] = {$ne:true}
    match["relatasStage"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}

    var projectQuery = {
        "_id": "$_id",
        "contactEmailId": "$contactEmailId",
        "mobileNumber": "$mobileNumber",
        "opportunityId": "$opportunityId",
        "opportunityName": "$opportunityName",
        "stageName": "$relatasStage",
        "relatasStage": "$relatasStage",
        "sourceType": "$sourceType",
        "closeDate": "$closeDate",
        "createdDate": "$createdDate",
        "geoLocation": "$geoLocation",
        "partners": "$partners",
        "decisionMakers": "$decisionMakers",
        "influencers": "$influencers",
        "productType": "$productType",
        "userEmailId": "$userEmailId",
        "userId": "$userId",
        "BANT": "$BANT",
        "vertical": "$vertical",
        "competitor": "$competitor",
        "isWon": "$isWon",
        "notes": "$notes",
        "isClosed": "$isClosed",
        "lastStageUpdated": "$lastStageUpdated",
        "netGrossMargin": "$netGrossMargin",
        "amount": "$amount",
        "currency": 1
    }

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount()
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in getOpportunitiesByByDate():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });

}

OpportunitiesManagement.prototype.getOpportunityIdsByUserIds = function (accessControlOppsData,
                                                                       companyId,
                                                                       callback) {

    var userIds = accessControlOppsData;
    var match = {
            userId:{$in:userIds}
        }

    // if(!fromDate && !toDate){
    //     match["closeDate"] = {$lte:new Date(moment().add(30, "days")),$gte:new Date(moment().startOf('day'))};
    // }

    // match["stageName"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    // match["isClosed"] = {$ne:true}
    // match["relatasStage"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}

    var projectQuery = {
        "_id": "$_id",
     }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in getOpportunityIdsByUserIds():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });

}

OpportunitiesManagement.prototype.getAllOpportunities = function (callback) {
    var match = {$or:[
            {'partners.0': {$exists: true}},
            {'influencers.0': {$exists: true}},
            {'decisionMakers.0': {$exists: true}},
            {'stageName':{$in:['Close Won','Closed Won']}}
        ]};

    opportunitiesCollection.find({},{partners:1,influencers:1,decisionMakers:1,userEmailId:1,contactEmailId:1,stageName:1},function(error, result){
        callback(error,result)
    })
}

OpportunitiesManagement.prototype.checkOpportunitiesForPeople = function (userId,contactEmail,contactMobile,type,contactId,callback) {

    var q = {};
    q["userId"] = userId;

    if(type === 'partner'){
        q["partners.contactId"] = contactId
    } else if(type === 'decisionMaker'){
        q["decisionMakers.contactId"] = contactId
    } else if(type === 'influencer'){
        q["influencers.contactId"] = contactId
    }

    opportunitiesCollection.find(q,function(error, result){
        if(error){
            loggerError.info('Error in checkOpportunitiesForPeople():opportunitiesManagement ',error)
            callback(error,null)
        }
        else{
            callback(null,result)
        }
    });
}

OpportunitiesManagement.prototype.getPipelineValueByContact = function (userId,contactEmail,callback) {
    opportunitiesCollection.aggregate([
        {$match:{userId:userId,contactEmailId:contactEmail,isClosed:{$ne:true}}},
        {$group:{_id:"$contactEmailId",total:{$sum:"$amount"}}}
    ],function(error, result){
        if(error){
            loggerError.info('Error in getPipelineValueByContact():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result[0]);
        }
    });

}

OpportunitiesManagement.prototype.removeSalseforceAccount = function (userId,callback) {
    myUserCollection.update({_id:userId},{salesforce:{}},function(error, result){
       if(error){
           loggerError.info('Error in removeSalesforceAccount():opportunitiesManagement ',error)
           callback(false);
       }
       else{
           callback(true)
       }
    });
};

function fetchCompanyFromEmail(email){

    if(checkRequired(email)){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            word = word.toLowerCase();
            return removableTextList.indexOf(word.trim()) > -1
        })
        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
    } else {
        return null
    }
}


function buildContactObj(emailId,common,ownerId,ownerEmailId){
    return {
        personId:null,
        personName:emailId,
        personEmailId:emailId,
        birthday:null,
        companyName:null,
        designation:null,
        addedDate:new Date(),
        verified:false,
        relatasUser:false,
        relatasContact:true,
        favorite:true,
        mobileNumber:null,
        location:null,
        source:'opp-transfer',
        contactRelation : {
            "decisionmaker_influencer" : null,
            "prospect_customer" : "prospect",
            "partner" : false,
            "influencer" : false,
            "decision_maker" : false
        },
        account:{
            name:common.fetchCompanyFromEmail(emailId),
            selected:false,
            updatedOn:new Date()
        },
        contactImageLink:null,
        ownerEmailId:ownerEmailId,
        ownerId:common.castToObjectId(ownerId)
    };
}

OpportunitiesManagement.prototype.upcomingOpportunities = function (userId,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:{
                userId:userId,
                isClosed:false,
                stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']},
                $or:[
                    {closeDate:{$lt:moment().subtract(1, "days").toDate()}}
                ]
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in upcomingOpportunities():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.opportunitiesCount = function (userEmailId,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:{
                userEmailId:userEmailId
            }
        },
        {
            $group:{
                _id:null,
                opportunitiesCount: {$sum: 1}
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in opportunitiesCount():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.opportunitiesCountAndValues = function (userEmailId,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:{
                userEmailId:userEmailId
            }
        },
        {
            $group:{
                _id:null,
                opportunitiesCount: {$sum: 1},
                opportunitiesValue: {$sum: "$amount"}
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in opportunitiesCount():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.staleOpportunities = function (userId,regionAccess,productAccess,verticalAccess,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId);

    var match = {
        $or:[{userId:userId},
            {
                $and:accessControlQuery
            }
        ],
        closeDate:{$lte:new Date()},
        isClosed:{$ne:true},
        stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:userId,
            closeDate:{$lte:new Date()},
            isClosed:{$ne:true},
            stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
        }
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in staleOpportunities():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.staleOpportunitiesCount = function (forUserEmailId,companyId,callback) {

    var match = {
        userEmailId:{$in:forUserEmailId},
        closeDate:{$lte:new Date()},
        isClosed:{$ne:true},
        stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $group:{
                _id:null,
                count:{$sum:1},
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in staleOpportunitiesCount():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.getStaleOpportunitiesCount = function (forUserEmailId,companyId,callback) {
    var minDate = moment("Jan 01 2015").startOf('day').toDate();
    var maxDate = moment().subtract(1, 'days').endOf('day').toDate();

    var match = {
        companyId:companyId,
        userEmailId:forUserEmailId,
        closeDate:{$gte:minDate, $lte:maxDate},
        isClosed:{$ne:true},
        stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $group:{
                _id:null,
                count:{$sum:1},
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in staleOpportunitiesCount():opportunitiesManagement ',error)
            callback(error,{staleOpportunities:0});
        }
        else{
            callback(error,{staleOpportunities:result[0] ? result[0].count : 0});
        }
    });
};

OpportunitiesManagement.prototype.getDealsAtRisk = function(emailId,primaryCurrency,currenciesObj,callback) {
    var from = moment().subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    dealsAtRiskMetaCollection.find({userEmailId:{$in:emailId},date:{$gte:new Date(from),$lte:new Date(to)}},{opportunityIds:0}, function (err,deals) {

        var totalDealValueAtRisk = 0;
        var dealsAtRiskCount = 0,
            dealsRiskAsOfDate = null;

        if(!err && deals && deals[0]){
            _.each(deals,function (de) {
                var eachUserVal = 0;
                dealsRiskAsOfDate = de.date;
                _.each(de.deals,function (el) {

                    el.amountWithNgm = el.amount;

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                    }

                    el.convertedAmt = el.amount;
                    el.convertedAmtWithNgm = el.amountWithNgm

                    if(el.currency && el.currency !== primaryCurrency){

                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                        }

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                        }

                        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))

                    }

                    eachUserVal = eachUserVal+parseFloat(el.convertedAmtWithNgm);
                });

                de.totalDealValueAtRisk = eachUserVal;
                totalDealValueAtRisk = totalDealValueAtRisk+de.totalDealValueAtRisk
                dealsAtRiskCount = dealsAtRiskCount+de.count
            })
        }

        callback(err,{
            count:deals,
            amount:totalDealValueAtRisk,
            dealsRiskAsOfDate:dealsRiskAsOfDate
        })
    })
}

OpportunitiesManagement.prototype.todayOpportunities = function (userId,dateMin,dateMax,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:{
                userId:userId,
                closeDate:{ $gte: new Date(dateMin), $lte:new Date(dateMax) },
                isClosed:{$ne:true},
                stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in todayOpportunities():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.opportunitiesForContacts = function (userEmailId,contacts,callback) {

    opportunitiesCollection.find({
                userEmailId:userEmailId,
                contactEmailId:{$in:contacts}},{
                userId:0,
                salesforceContactId:0,
                userEmailId:0,
                opportunityId:0
            },function (err,data) {
        callback(err,data)
    })
}

OpportunitiesManagement.prototype.opportunitiesForContactsAndUsers = function (companyId,userEmailIds,contacts,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:{
                companyId:companyId,
                userEmailId:{$in:userEmailIds},
                contactEmailId:{$in:contacts}
            }
        },
        {
            $group: {
                _id: {owner:"$userEmailId",emailId:"$contactEmailId"},
                amount:{$sum:"$amount"}
            }
        }
    ]).exec(function(err,opps){
        callback(err,opps)
    });
}

OpportunitiesManagement.prototype.matchContactInOpportunity = function (userIds,cEmailId,netGrossMargin,primaryCurrency,currenciesObj,callback) {

    var queryFinal = {
        "$or": userIds.map(function (el) {
            var obj = {};
            obj["userId"] = el;
            obj["$or"] = [
                {contactEmailId: cEmailId},
                {partners:{ $elemMatch: { emailId: cEmailId }}},
                {decisionMakers:{ $elemMatch: { emailId: cEmailId }}},
                {influencers:{ $elemMatch: { emailId: cEmailId }}}
            ]

            return obj;
        })
    }

    opportunitiesCollection.aggregate([
        {
            $match:queryFinal
        },{
            $project:{
                amount:"$amount",
                currency:"$currency",
                netGrossMargin:"$netGrossMargin",
                contactEmailId:"$contactEmailId",
                partners:"$partners",
                decisionMakers:"$decisionMakers",
                influencers:"$influencers",
                lastStageUpdated:"$lastStageUpdated"
            }
        }
    ],function (err,data) {

        if(!err && data && data.length>0){
            _.each(data,function (el) {
                convertAmount_edit_amount(el,primaryCurrency,currenciesObj);
            })
        }
        callback(err,data)
    })
}

OpportunitiesManagement.prototype.opportunitiesForUser = function (userId,companyId,callback){
    opportunitiesCollection.find({userId:userId}).lean().exec(function (err,opps) {
        callback(err,opps);
    })
}

OpportunitiesManagement.prototype.opportunitiesForUsers = function (userIds,dateMin,dateMax,cEmailId,regionAccess,productAccess,verticalAccess,buAccess,teamUserIds,companyId,usersWithAccess,portfolios,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId,buAccess);

    if(portfolios){
        var portfolios_raw = portfolios.split(",");
        var pAccess = [],
        rAccess = [],
        vAccess = [],
        bAccess = [];

        if(portfolios_raw && portfolios_raw.length>0){
            _.each(portfolios_raw,function (po) {
                var splits  = po.split("_type_");
                if(splits[1] == "productType"){
                    productAccess.push(splits[0])
                }
                if(splits[1] == "vertical"){
                    verticalAccess.push(splits[0])
                }
                if(splits[1] == "businessUnit"){
                    bAccess.push(splits[0])
                }
                if(splits[1] == "region"){
                    regionAccess.push(splits[0])
                }
            })
        }
    }

    var portfolioAccessQuery = portfolioAccess(rAccess,pAccess,vAccess,companyId,bAccess);

    if(usersWithAccess && usersWithAccess.length>0){

        if(!_.isArray(usersWithAccess)){
            var copyVar = usersWithAccess;
            usersWithAccess = [];
            usersWithAccess.push(copyVar)
        }
    }

    var match = {
        $or:[{userId:{$in:teamUserIds}}
        ]
    }

    if(checkRequired(usersWithAccess)){
        if(match.$or){
            match.$or.push({
                "usersWithAccess.emailId":{$in:usersWithAccess}
            })
        }
    }

    if(companyId){
        match = {
            $or:[{
                userId:{$in:userIds}
            },{"usersWithAccess.emailId":{$in:usersWithAccess}}]
        }
    }

    if(dateMax && dateMin) {
        if(!regionAccess && !productAccess && !verticalAccess && !buAccess){

            if(companyId){
                match = {
                    closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)},
                    $or:[{
                        userId:{$in:userIds}
                    },{"usersWithAccess.emailId":{$in:usersWithAccess}}]
                }
            } else {
                match = {
                    closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)},
                    $or:[{
                        userId:{$in:userIds}
                    },{"usersWithAccess.emailId":{$in:usersWithAccess}}]
                }
            }
        } else {
            match = {
                closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)},
                $or:[{
                    userId:{$in:userIds}
                },{"usersWithAccess.emailId":{$in:usersWithAccess}}]
            }

            if(checkRequired(usersWithAccess)){
                if(match.$or){
                    match.$or.push({
                        "usersWithAccess.emailId":{$in:usersWithAccess}
                    })
                }
            }
        }

    }

    if(!teamUserIds && !usersWithAccess && companyId && userIds){

        match = {
            companyId:companyId,
            userId:{$in:userIds}
        }
    }

    if(cEmailId && cEmailId != usersWithAccess[0]){
        match = {
            closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)},
            $or:[{userId:{$in:userIds}}]
        }

        if(checkRequired(usersWithAccess)){
            if(match.$or){
                match.$or.push({
                    "usersWithAccess.emailId":{$in:usersWithAccess}
                })
            }
        }
    }

    var aggregateQ = [{
            $match:match
        }];
    if(accessControlQuery && accessControlQuery.length>0){

        if(aggregateQ[0] && aggregateQ[0].$match && aggregateQ[0].$match.$or){
            aggregateQ[0].$match.$or.push({
                $and:accessControlQuery
            })
        }
    }

    if(portfolioAccessQuery && portfolioAccessQuery.length>0){
        aggregateQ.push({
            $match:{
                $and:portfolioAccessQuery
            }
        })
    }

    aggregateQ.push({
        $group: {
            _id: "$userEmailId",
            opportunities: {
                $push: "$$ROOT"
            },
            usersAndContacts:{
                $addToSet: {
                    "contactEmailId": "$contactEmailId",
                    "userEmailId": "$userEmailId"
                }
            }
        }
    });

    opportunitiesCollection.aggregate(aggregateQ,function(error, result){
        if(error){
            loggerError.info('Error in opportunitiesForUsers():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.getOpenOpportunitiesForUsers = function (userIds,dateMin,dateMax,cEmailId,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId);

    var match = {
        $or:[{userId:{$in:userIds}},
            {
                $and:accessControlQuery
            }
        ],
        isClosed:{$ne:true}
    };

    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds},
            isClosed:{$ne:true}
        }
    }

    if(cEmailId){
        match = {
            "$or": userIds.map(function (el) {
                var obj = {};
                obj["userId"] = el;
                obj["$or"] = [
                    {contactEmailId: cEmailId},
                    {partners:{ $elemMatch: { emailId: cEmailId }}},
                    {decisionMakers:{ $elemMatch: { emailId: cEmailId }}},
                    {influencers:{ $elemMatch: { emailId: cEmailId }}}
                ]
                return obj;
            })
        }
    }

    match["stageName"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}

    var projectQuery = {
        "_id": "$_id",
        "contactEmailId": "$contactEmailId",
        "mobileNumber": "$mobileNumber",
        "opportunityId": "$opportunityId",
        "opportunityName": "$opportunityName",
        "stageName": "$relatasStage",
        "relatasStage": "$relatasStage",
        "closeDate": "$closeDate",
        "createdDate": "$createdDate",
        "geoLocation": "$geoLocation",
        "partners": "$partners",
        "decisionMakers": "$decisionMakers",
        "influencers": "$influencers",
        "productType": "$productType",
        "userEmailId": "$userEmailId",
        "userId": "$userId",
        "amount": "$amount",
        "sourceType": "$sourceType",
        "netGrossMargin": "$netGrossMargin",
        "lastStageUpdated": "$lastStageUpdated"
    }

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount()
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        },
        {
            $group: {
                _id: "$userEmailId",
                opportunities: {
                    $push: {
                        "_id": "$_id",
                        "contactEmailId": "$contactEmailId",
                        "mobileNumber": "$mobileNumber",
                        "opportunityId": "$opportunityId",
                        "opportunityName": "$opportunityName",
                        "stageName": "$relatasStage",
                        "amount": "$amount",
                        "closeDate": "$closeDate",
                        "createdDate": "$createdDate",
                        "geoLocation": "$geoLocation",
                        "partners": "$partners",
                        "decisionMakers": "$decisionMakers",
                        "influencers": "$influencers",
                        "productType": "$productType",
                        "userEmailId": "$userEmailId",
                        "userId": "$userId",
                        "sourceType": "$sourceType",
                        "lastStageUpdated": "$lastStageUpdated",
                        "netGrossMargin": "$netGrossMargin"
                    }
                },
                usersAndContacts:{
                    $addToSet: {
                        "contactEmailId": "$contactEmailId",
                        "userEmailId": "$userEmailId"
                    }
                }
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in opportunitiesForUsers():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.getOppGroupDayMonthYear = function (userIds,dateMin,dateMax,companyId,type,callback) {

    var match = {
        companyId: companyId,
        userId: { $in:userIds },
        closeDate: {$gte:new Date(dateMin),$lte:new Date(dateMax)}
    }

    if(type === "open"){
        match["stageName"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    } else {
        match["stageName"] = {$in:['Close Won','Closed Won']}
    }

    var projectQuery = {
        day: {$dayOfMonth:"$closeDate"},
        month: {$month:"$closeDate"},
        year: {$year:"$closeDate"},
        date: "$closeDate",
        userId:"$userId",
        userEmailId:"$userEmailId",
        createdByEmailId:"$createdByEmailId",
        createdDate:"$createdDate",
        closeDate:"$closeDate",
        isClosed:"$isClosed",
        relatasStage:"$relatasStage",
        stageName:"$stageName",
        opportunityName:"$opportunityName",
        geoLocation:"$geoLocation",
        productType:"$productType",
        "netGrossMargin": "$netGrossMargin",
    };

    projectQuery.amount = netGrossAmount();

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project: projectQuery
        },
        {
            $group: {
                _id: {
                    day:"$day",
                    month:"$month",
                    year:"$year"
                },
                date: {$first:"$closeDate"},
                opps: {$push: "$$ROOT"}
            }
        },
        {
            $project: {
                day: "$_id.day",
                month: "$_id.month",
                year: "$_id.year",
                date: "$date",
                opps: "$opps"
            }
        }
    ],function(error, result){

        if(error){
            loggerError.info('Error in getOppGroupMonthYear():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.getWonOpp = function (userIds,dateMin,dateMax,companyId,callback) {

    var match = {
        companyId: companyId,
        userId: { $in:userIds },
        closeDate: {$gte:new Date(dateMin),$lte:new Date(dateMax)}
    }

    match["stageName"] = {$in:['Close Won','Closed Won']}

    var projectQuery = {
        day: {$dayOfMonth:"$closeDate"},
        month: {$month:"$closeDate"},
        year: {$year:"$closeDate"},
        date: "$closeDate",
        userId:"$userId",
        userEmailId:"$userEmailId",
        createdDate:"$createdDate",
        closeDate:"$closeDate",
        isClosed:"$isClosed",
        relatasStage:"$relatasStage",
        stageName:"$stageName",
        opportunityName:"$opportunityName",
        geoLocation:"$geoLocation",
        productType:"$productType",
        createdByEmailId:"$createdByEmailId",
        "netGrossMargin": "$netGrossMargin",
        contactEmailId:"$contactEmailId"
    };

    projectQuery.amount = netGrossAmount();

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project: projectQuery
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in getOpenOpp():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.getOppsByIds = function (companyId,oppIds,startDate,endDate,callback) {

    var match = {
        companyId: companyId,
        opportunityId: { $in:oppIds },
        closeDate: {$gte:new Date(startDate),$lte:new Date(endDate)}
    }

    var projectQuery = {
        day: {$dayOfMonth:"$closeDate"},
        month: {$month:"$closeDate"},
        year: {$year:"$closeDate"},
        date: "$closeDate",
        userId:"$userId",
        userEmailId:"$userEmailId",
        contactEmailId:"$contactEmailId",
        createdDate:"$createdDate",
        closeDate:"$closeDate",
        isClosed:"$isClosed",
        relatasStage:"$relatasStage",
        stageName:"$stageName",
        opportunityName:"$opportunityName",
        geoLocation:"$geoLocation",
        productType:"$productType",
        createdByEmailId:"$createdByEmailId",
        "netGrossMargin": "$netGrossMargin"
    };

    projectQuery.amount = netGrossAmountWithZero();

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project: projectQuery
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in getOppsByIds():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.opportunitiesForUsersByDate = function (userIds,dateMin,dateMax,cEmailId,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId);

    var match = {
        $or:[{userId:{$in:userIds}},
            {
                $and:accessControlQuery
            }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds}
        }
    }

    if(dateMax && dateMin) {
        match = {
            $or:[{  userId:{$in:userIds}},
                {
                    $and:accessControlQuery
                }],
            closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
        }

        if(accessControlQuery.length == 0){
            match = {
                userId:{$in:userIds},
                closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
            }
        }
    }

    if(cEmailId){
        match = {
            "$or": userIds.map(function (el) {
                var obj = {};
                obj["userId"] = el;
                obj["$or"] = [
                    {contactEmailId: cEmailId},
                    {partners:{ $elemMatch: { emailId: cEmailId }}},
                    {decisionMakers:{ $elemMatch: { emailId: cEmailId }}},
                    {influencers:{ $elemMatch: { emailId: cEmailId }}}
                ]
                return obj;
            })
        }
    }

    var projectQuery = {
        "_id": "$_id",
        "contactEmailId": "$contactEmailId",
        "mobileNumber": "$mobileNumber",
        "opportunityId": "$opportunityId",
        "opportunityName": "$opportunityName",
        "stageName": "$relatasStage",
        "relatasStage": "$relatasStage",
        "amount": "$amount",
        "netGrossMargin": "$netGrossMargin",
        "currency": "$currency",
        "closeDate": "$closeDate",
        "createdDate": "$createdDate",
        "geoLocation": "$geoLocation",
        "partners": "$partners",
        "decisionMakers": "$decisionMakers",
        "influencers": "$influencers",
        "productType": "$productType",
        "userEmailId": "$userEmailId",
        "userId": "$userId",
        "lastStageUpdated": "$lastStageUpdated",
        "interactionCount": "$interactionCount"
    }

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount()
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
          $project:projectQuery
        },
        {
            $group: {
                _id: "$userEmailId",
                opportunities: {
                    $push: {
                        "_id": "$_id",
                        "contactEmailId": "$contactEmailId",
                        "mobileNumber": "$mobileNumber",
                        "opportunityId": "$opportunityId",
                        "opportunityName": "$opportunityName",
                        "stageName": "$relatasStage",
                        "amount": "$amount",
                        "netGrossMargin": "$netGrossMargin",
                        "currency": "$currency",
                        "closeDate": "$closeDate",
                        "createdDate": "$createdDate",
                        "geoLocation": "$geoLocation",
                        "partners": "$partners",
                        "decisionMakers": "$decisionMakers",
                        "influencers": "$influencers",
                        "productType": "$productType",
                        "userEmailId": "$userEmailId",
                        "userId": "$userId",
                        "lastStageUpdated": "$lastStageUpdated",
                        "interactionCount": "$interactionCount"
                    }
                },
                usersAndContacts:{
                    $addToSet: {
                        "contactEmailId": "$contactEmailId",
                        "userEmailId": "$userEmailId"
                    }
                }
            }
        }
    ],function(error, result){

        if(error){
            loggerError.info('Error in opportunitiesForUsers():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.opportunitiesForUsersByDateWon = function (userIds,dateMin,dateMax,cEmailId,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId);

    var match = {
        $or:[{userId:{$in:userIds}},
            {
                $and:accessControlQuery
            }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds}
        }
    }

    if(dateMax && dateMin) {
        match = {
            $or:[{  userId:{$in:userIds}},
                {
                    $and:accessControlQuery
                }],
            closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
        }

        if(accessControlQuery.length == 0){
            match = {
                userId:{$in:userIds},
                closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
            }
        }
    }

    if(cEmailId){
        match = {
            "$or": userIds.map(function (el) {
                var obj = {};
                obj["userId"] = el;
                obj["$or"] = [
                    {contactEmailId: cEmailId},
                    {partners:{ $elemMatch: { emailId: cEmailId }}},
                    {decisionMakers:{ $elemMatch: { emailId: cEmailId }}},
                    {influencers:{ $elemMatch: { emailId: cEmailId }}}
                ]
                return obj;
            })
        }
    }

    var projectQuery = {
        "_id": "$_id",
        "contactEmailId": "$contactEmailId",
        "mobileNumber": "$mobileNumber",
        "opportunityId": "$opportunityId",
        "opportunityName": "$opportunityName",
        "stageName": "$relatasStage",
        "relatasStage": "$relatasStage",
        "amount": "$amount",
        "netGrossMargin": "$netGrossMargin",
        "currency": "$currency",
        "closeDate": "$closeDate",
        "createdDate": "$createdDate",
        "geoLocation": "$geoLocation",
        "partners": "$partners",
        "decisionMakers": "$decisionMakers",
        "influencers": "$influencers",
        "productType": "$productType",
        "userEmailId": "$userEmailId",
        "userId": "$userId",
        "lastStageUpdated": "$lastStageUpdated",
        "interactionCount": "$interactionCount"
    }

    match.stageName = "Close Won";

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount()
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
          $project:projectQuery
        },
        {
            $group: {
                _id: "$userId",
                opportunities: {
                    $push: {
                        "_id": "$_id",
                        "contactEmailId": "$contactEmailId",
                        "mobileNumber": "$mobileNumber",
                        "opportunityId": "$opportunityId",
                        "opportunityName": "$opportunityName",
                        "stageName": "$relatasStage",
                        "amount": "$amount",
                        "netGrossMargin": "$netGrossMargin",
                        "currency": "$currency",
                        "closeDate": "$closeDate",
                        "createdDate": "$createdDate",
                        "geoLocation": "$geoLocation",
                        "partners": "$partners",
                        "decisionMakers": "$decisionMakers",
                        "influencers": "$influencers",
                        "productType": "$productType",
                        "userEmailId": "$userEmailId",
                        "userId": "$userId",
                        "lastStageUpdated": "$lastStageUpdated",
                        "interactionCount": "$interactionCount"
                    }
                },
                usersAndContacts:{
                    $addToSet: {
                        "contactEmailId": "$contactEmailId",
                        "userEmailId": "$userEmailId"
                    }
                }
            }
        }
    ],function(error, result){

        if(error){
            loggerError.info('Error in opportunitiesForUsers():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.opportunitiesForUsersByDateWithLessData = function (userIds,dateMin,dateMax,cEmailId,regionAccess,productAccess,verticalAccess,netGrossMarginReq,filter,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId);

    var match = {
        $or:[{
            userId:{$in:userIds}
        },
            {
                $and:accessControlQuery
            }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds}
        }
    }

    if(dateMax && dateMin) {
        match = {
            $or:[{  userId:{$in:userIds}},
                {
                    $and:accessControlQuery
                }],
            closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
        }

        if(accessControlQuery.length == 0){
            match = {
                userId:{$in:userIds},
                closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
            }
        }
    }

    if(filter){

        if(filter["location"]){
            match["geoLocation.zone"] = new RegExp(filter["location"], "i")
        }

        if(filter["vertical"]){
            match["vertical"] = new RegExp(filter["vertical"], "i")
        }

        if(filter["product"]){
            match["productType"] = new RegExp(filter["product"], "i")
        }

        if(filter["source"]){
            match["sourceType"] = new RegExp(filter["source"], "i")
        }
    }

    var projectQuery = {
        "_id": "$_id",
        "contactEmailId": "$contactEmailId",
        "amount": "$amount",
        "closeDate": "$closeDate",
        "createdDate": "$createdDate",
        "geoLocation": "$geoLocation",
        "partners": "$partners",
        "decisionMakers": "$decisionMakers",
        "influencers": "$influencers",
        "productType": "$productType",
        "userEmailId": "$userEmailId",
        "userId": "$userId",
        "relatasStage": "$relatasStage",
        "interactionCount": "$interactionCount",
        "vertical": "$vertical",
        "opportunityName": "$opportunityName",
        "sourceType": "$sourceType",
        "currency": "$currency",
        "netGrossMargin": "$netGrossMargin"
    }

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount()
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        },
        {
            $group: {
                _id: "$userId",
                opportunities: {
                    $push: {
                        "_id": "$_id",
                        "contactEmailId": "$contactEmailId",
                        "amount": "$amount",
                        "closeDate": "$closeDate",
                        "createdDate": "$createdDate",
                        "geoLocation": "$geoLocation",
                        "partners": "$partners",
                        "decisionMakers": "$decisionMakers",
                        "influencers": "$influencers",
                        "productType": "$productType",
                        "userEmailId": "$userEmailId",
                        "userId": "$userId",
                        "relatasStage": "$relatasStage",
                        "interactionCount": "$interactionCount",
                        "vertical": "$vertical",
                        "opportunityName": "$opportunityName",
                        "currency": "$currency",
                        "netGrossMargin": "$netGrossMargin"
                    }
                }
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in opportunitiesForUsersByDateWithLessData():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.getAllContactsinOpp = function (userIds,callback) {

    var match = {
        userId:{$in:userIds}
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $group: {
                _id: "$userId",
                contacts:{
                    $addToSet:{
                        partners:"$partners.emailId",
                        decisionMakers:"$decisionMakers.emailId",
                        influencers:"$influencers.emailId",
                        contactEmailId:"$contactEmailId"
                    }
                }
            }
        },
        {
            $project:{
                _id:"$_id",
                contacts:{ "$setUnion": [ "$contacts.partners","$contacts.decisionMakers", "$contacts.influencers", "$contacts.contactEmailId" ] }
            }

        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in allOpenOpportunitiesForUserByOppId():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.allOpenOpportunitiesForUsers = function (userIds,dateMin,dateMax,callback) {

    var match = {
        userId:{$in:userIds},
        // closeDate:{ $gte: new Date(dateMin), $lte:new Date(dateMax) }
        isClosed:{$ne:true}
    }

    if(!dateMin && !dateMax){
        match = {
            userId: {$in: userIds},
            isClosed:{$ne:true}
        }
    }


    opportunitiesCollection.aggregate([
        {
            $match:{
                userId:{$in:userIds},
                // closeDate:{ $gte: new Date(dateMin), $lte:new Date(dateMax) },
                isClosed:{$ne:true}
            }
        },
        {
            $group: {
                _id: {
                    userId:"$userId",
                    userEmailId:"$userEmailId",
                    contactEmailId:"$contactEmailId"
                },
                contacts:{
                    $addToSet:{
                        partners:"$partners.emailId",
                        decisionMakers:"$decisionMakers.emailId",
                        influencers:"$influencers.emailId",
                        oppId:"$_id"
                    }
                },
                total:{$sum:"$amount"}
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in allOpenOpportunitiesForUsers():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.allOpenOpportunitiesForUserByOppId = function (userIds,dateMin,dateMax,callback) {

    var match = {
        userId:{$in:userIds},
        isClosed:{$ne:true},
        relatasStage:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    }

    if(!dateMin && !dateMax){
        match = {
            userId: {$in: userIds},
            isClosed:{$ne:true},
            relatasStage:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
        }
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $group: {
                _id: "$_id",
                contacts:{
                    $addToSet:{
                        partners:"$partners.emailId",
                        decisionMakers:"$decisionMakers.emailId",
                        influencers:"$influencers.emailId",
                        contactEmailId:"$contactEmailId"
                    }
                }
            }
        },
        {
            $project:{
                _id:"$_id",
                contacts:{ "$setUnion": [ "$contacts.partners","$contacts.decisionMakers", "$contacts.influencers", "$contacts.contactEmailId" ] }
            }

        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in allOpenOpportunitiesForUserByOppId():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.groupByUsersAndStage = function (userIds,fromDate,toDate,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,primaryCurrency,currenciesObj,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId)

    var match = {
        $or:[{userId:{$in:userIds}},
            {
                $and:accessControlQuery
            }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds}
        }
    }

    if(fromDate && toDate) {
        match = {
            $or:[{  userId:{$in:userIds}},
                {
                    $and:accessControlQuery
                }],
            closeDate:{$lte:new Date(toDate),$gte:new Date(fromDate)}
        }

        if(accessControlQuery.length == 0){
            match = {
                userId:{$in:userIds},
                closeDate:{$lte:new Date(toDate),$gte:new Date(fromDate)}
            }
        }
    }

    var projectQuery = {
        relatasStage:"$relatasStage",
        amount: "$amount",
        netGrossMargin: "$netGrossMargin",
        currency: "$currency"
    };

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        },
        {
            $group: {
                _id:"$relatasStage",
                totalAmount:{$sum:"$amount"},
                count:{$sum:1},
                opps:{$push:"$$ROOT"}
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in groupByUsersAndStage():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{

            _.each(result,function (re) {
                var bottomLineConverted = 0;
                _.each(re.opps,function (el) {
                    el.amountWithNgm = el.amount;

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                    }

                    el.convertedAmt = el.amount;

                    el.convertedAmtWithNgm = el.amountWithNgm

                    if(el.currency && el.currency !== primaryCurrency){

                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                        }

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                        } else {
                            el.convertedAmtWithNgm = el.convertedAmt;
                        }

                        bottomLineConverted = bottomLineConverted+el.convertedAmtWithNgm;

                        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2));
                    } else {
                        bottomLineConverted = bottomLineConverted+el.convertedAmtWithNgm;
                    }
                });

                re.totalAmount = bottomLineConverted // This is the value being used for the graphs.

            });

            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.getAllOppsByUserId = function (userIds,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId)

    var match = {
        $or:[{userId:{$in:userIds}},
            {
                $and:accessControlQuery
            }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds}
        }
    }

    var projectQuery = {
        userId:"$userId",
        userEmailId:"$userEmailId",
        createdDate:"$createdDate",
        closeDate:"$closeDate",
        isClosed:"$isClosed",
        relatasStage:"$relatasStage",
        amount:"$amount",
        opportunityName:"$opportunityName"
    };

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount();
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in groupByUsersAndStage():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.groupByUsersAndStageForAccount = function (userIds,fromDate,toDate,contacts,netGrossMarginReq,primaryCurrency,currenciesObj,callback) {

    var match = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["userId"] = el;
        obj["contactEmailId"] = {$in:contacts}
        // obj["$or"] = [
        //     {"contactEmailId":new RegExp('.'+account+'.', "i")},
        //     {"contactEmailId":new RegExp('@'+account, "i")}
        // ]
        obj["closeDate"] = {$lte:new Date(toDate),$gte:new Date(fromDate)}

        return obj;
    })};

    var projectQuery = {
        relatasStage:"$relatasStage",
        amount: "$amount",
        netGrossMargin: "$netGrossMargin",
        currency: "$currency",
    };

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        },
        {
            $group: {
                _id:"$relatasStage",
                totalAmount:{$sum:"$amount"},
                count:{$sum:1},
                opps:{$push:"$$ROOT"}
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in groupByUsersAndStageForAccount():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            _.each(result,function (re) {
                var bottomLineConverted = 0;
                _.each(re.opps,function (el) {
                    el.amountWithNgm = el.amount;

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                    }

                    el.convertedAmt = el.amount;

                    el.convertedAmtWithNgm = el.amountWithNgm

                    if(el.currency && el.currency !== primaryCurrency){

                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                        }

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                        } else {
                            el.convertedAmtWithNgm = el.convertedAmt;
                        }

                        bottomLineConverted = bottomLineConverted+el.convertedAmtWithNgm;

                        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2));
                    } else {
                        bottomLineConverted = bottomLineConverted+el.convertedAmtWithNgm;
                    }
                });

                re.totalAmount = bottomLineConverted // This is the value being used for the graphs.

            });

            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.allOpenOpportunitiesForUsersByDateRange = function (userIds,dateMin,dateMax,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:{
                userId:{$in:userIds},
                closeDate:{ $gte: new Date(dateMin), $lte:new Date(dateMax) },
                isClosed:{$ne:true}
            }
        },
        {
            $group: {
                _id: {
                    userId:"$userId",
                    userEmailId:"$userEmailId",
                    contactEmailId:"$contactEmailId"
                },
                total:{$sum:"$amount"}}
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in allOpenOpportunitiesForUsersByDateRange():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.successFullClosures = function (userIds,callback) {

    opportunitiesCollection.aggregate([
        {
            $match: {
                userId: {$in: userIds},
                stageName:{$in:['Close Won','Closed Won']}
            }
        },
        {
            $group:{
                _id:"$userEmailId",
                count:{$sum:1},
                contacts:{
                    $addToSet:{
                        mainContact:"$contactEmailId",
                        partners:"$partners",
                        decisionMakers:"$decisionMakers",
                        influencers:"$influencers",
                        createdDates:"$createdDate",
                        closeDates:"$closeDate",
                        _id:"$_id"
                    }
                }
            }
        }
    ]).exec(function (err, opps) {
        callback(err,opps)
    })
}

OpportunitiesManagement.prototype.closedOpportunitiesForUsersByDateRange = function (userIds,dateMin,dateMax,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:{
                userId:{$in:userIds},
                closeDate:{ $gte: new Date(dateMax), $lte:new Date(dateMin) },
                isClosed:true
            }
        },
        {
            $group: {
                _id: {
                    userId:"$userId",
                    userEmailId:"$userEmailId",
                    contactEmailId:"$contactEmailId",
                    $dayOfYear:"$closeDate"
                },
                total:{$sum:"$amount"}}
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in closedOpportunitiesForUsersByDateRange():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.allOpenOpportunityContacts = function (userId,callback) {
    opportunitiesCollection.aggregate([
        {
            $match:{
                userId:userId,
                isClosed:{$ne:true}
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in getPipelineValueByContact():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.updateRelatedPeople = function (userId,opportunityId,updateObj,callback) {

    opportunitiesCollection.update({userId:userId,_id:opportunityId},{ $addToSet: updateObj },{upsert:true},function(err,result){
        if(err){
            loggerError.info('Error in updateRelatedPeople():opportunitiesManagement ',err)
            callback(err,result)
        }
        else{
            callback(err,result)
        }
    })
}

OpportunitiesManagement.prototype.removeRelatedPeople = function (userId,opportunityId,updateObj,callback) {

    opportunitiesCollection.update({userId:userId,_id:opportunityId},{ $pull: updateObj },function(err,result){
        if(err){
            loggerError.info('Error in removeRelatedPeople():opportunitiesManagement ',err)
            callback(err,result)
        }
        else{
            callback(err,result)
        }
    })
}

OpportunitiesManagement.prototype.opportunitiesByMonthYear = function (userId,fromDate,toDate,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,primaryCurrency,currenciesObj,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId)

    var match = {
        $or:[{userId:{$in:userId}},
            {
                $and:accessControlQuery
            }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userId}
        }
    }

    if(fromDate && toDate) {
        match = {
            $or:[{  userId:{$in:userId}},
                {
                    $and:accessControlQuery
                }],
            closeDate:{$lte:new Date(toDate),$gte:new Date(fromDate)}
        }

        if(accessControlQuery.length == 0){
            match = {
                userId:{$in:userId},
                closeDate:{$lte:new Date(toDate),$gte:new Date(fromDate)}
            }
        }
    }

    var projectQuery = {
        month: { "$month": "$closeDate" },
        year: { "$year": "$closeDate" },
        amount: "$amount",
        // openValue: "$amount",
        netGrossMargin: "$netGrossMargin",
        currency: "$currency",
        stageName: "$relatasStage",
        sortDate:"$closeDate",
        isClosed:"$isClosed"
    }

    var openValue = {
        "$and":[
            { "$ne": [ "$stageName", 'Close Won' ] },
            { "$ne": [ "$stageName", 'Close Lost' ]},
            { "$ne": [ "$stageName", 'Closed Won' ]},
            { "$ne": [ "$stageName", 'Closed Lost' ]}
        ]
    };

    opportunitiesCollection.aggregate([
        {
            $match: match
        },
        {
            $project: projectQuery
        },
        {
            $group:{
                _id: {month: "$month", year: "$year",stage:"$stageName"},
                count: {$sum: 1},
                opportunitiesValue: {$sum: "$amount"},
                openValue: {$sum: { $cond: [ openValue, "$amount" , 0 ] }},
                sortDate:{ $max: "$sortDate" },
                opps:{$push:"$$ROOT"}
            }
        }
    ],function(error, result){

        _.each(result,function (re) {

            var topLineConverted = 0,
                bottomLineConverted = 0,
                openValue = 0,
                opportunitiesValue = 0;

            _.each(re.opps,function (el) {
                el.amountWithNgm = el.amount;

                if(el.netGrossMargin || el.netGrossMargin == 0){
                    el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                }
                opportunitiesValue = opportunitiesValue+el.amountWithNgm;

                el.convertedAmt = el.amount;
                el.convertedAmtWithNgm = el.amountWithNgm

                if(el.currency && el.currency !== primaryCurrency){

                    if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                        el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                    }

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                    } else {
                        el.convertedAmtWithNgm = el.convertedAmt;
                    }

                    topLineConverted = topLineConverted+el.convertedAmt;
                    bottomLineConverted = bottomLineConverted+el.convertedAmtWithNgm;

                    el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2));
                } else {
                    topLineConverted = topLineConverted+el.convertedAmt;
                    bottomLineConverted = bottomLineConverted+el.convertedAmtWithNgm;
                }
            });

            re.topLineConverted = topLineConverted
            re.bottomLineConverted = bottomLineConverted
            re.opportunitiesValue = bottomLineConverted // This is the value being used for the graphs.
            if(!_.includes(["Close Won","Close Lost"],re._id.stage)){
                re.openValue = bottomLineConverted;
            }
        });

        if(error){
            loggerError.info('Error in opportunitiesByMonthYear():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

function topLineConvertedAmt(currencyObj,primaryCurrency) {
    var xr = currencyObj["INR"].xr;

    return  { $cond: [ {$ne:["$currency", primaryCurrency]} ,
            {$divide:["$amount", xr]}, "$amount"]
    }
}

function bottomLineConvertedAmt() {
    return {
        $cond: [ { $ne: ["$netGrossMargin", 0] } ,
            {$multiply:[{ "$divide": [ "$netGrossMargin", 100 ] },"$topLineAmount"]},
            "$topLineAmount" ]
    }
}

OpportunitiesManagement.prototype.opportunitiesByMonthYearAndAccount = function (userIds,fromDate,toDate,contacts,netGrossMarginReq,companyId,primaryCurrency,currenciesObj,callback) {

    var match = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["userId"] = el;
        obj["closeDate"] = {$lte:new Date(toDate),$gte:new Date(fromDate)};
        obj["contactEmailId"] = {$in:contacts}

        return obj;
    })};

    var projectQuery = {
        month: { "$month": "$closeDate" },
        year: { "$year": "$closeDate" },
        amount: "$amount",
        currency: "$currency",
        netGrossMargin: "$netGrossMargin",
        stageName: "$relatasStage",
        sortDate:"$closeDate",
        isClosed:"$isClosed"
    }

    var openValue = {
        "$and":[
            { "$ne": [ "$stageName", 'Close Won' ] },
            { "$ne": [ "$stageName", 'Close Lost' ]},
            { "$ne": [ "$stageName", 'Closed Won' ]},
            { "$ne": [ "$stageName", 'Closed Lost' ]}
        ]
    };

    opportunitiesCollection.aggregate([
        {
            $match: match
        },
        {
            $project: projectQuery
        },
        {
            $group:{
                _id: {month: "$month", year: "$year",stage:"$stageName"},
                count: {$sum: 1},
                opportunitiesValue: {$sum: "$amount"},
                openValue: {$sum: { $cond: [ openValue, "$amount" , 0 ] }},
                // openValue: {$sum: { $cond: [ { $eq: [ "$isClosed", true] } , 0, "$amount" ] }},
                sortDate:{ $max: "$sortDate" },
                opps:{$push:"$$ROOT"}
            }
        }
    ],function(error, result){

        if(error){
            loggerError.info('Error in opportunitiesByMonthYear():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{

            _.each(result,function (re) {

                var topLineConverted = 0,
                    bottomLineConverted = 0,
                    openValue = 0,
                    opportunitiesValue = 0;

                _.each(re.opps,function (el) {
                    el.amountWithNgm = el.amount;

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                    }
                    opportunitiesValue = opportunitiesValue+el.amountWithNgm;

                    el.convertedAmt = el.amount;

                    el.convertedAmtWithNgm = el.amountWithNgm

                    if(el.currency && el.currency !== primaryCurrency){

                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                        }

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                        } else {
                            el.convertedAmtWithNgm = el.convertedAmt;
                        }

                        topLineConverted = topLineConverted+el.convertedAmt;
                        bottomLineConverted = bottomLineConverted+el.convertedAmtWithNgm;

                        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2));
                    } else {
                        topLineConverted = topLineConverted+el.convertedAmt;
                        bottomLineConverted = bottomLineConverted+el.convertedAmtWithNgm;
                    }
                });

                re.topLineConverted = topLineConverted
                re.bottomLineConverted = bottomLineConverted
                re.opportunitiesValue = bottomLineConverted // This is the value being used for the graphs.
                if(!_.includes(["Close Won","Close Lost"],re._id.stage)){
                    re.openValue = bottomLineConverted;
                }
            });

            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.getTargetsForAllUsers = function(userIds,fromDate,toDate,fyStart,fyEnd,callback){

    var aggregation = [
        {
            $match:{
                userId:{$in:userIds},
                "fy.start":{$gte:new Date(fyStart)},
                "fy.end":{$lte:new Date(fyEnd)}
            }
        },
        {
            $unwind:"$salesTarget"
        },
        {
            $project: {
                month: { "$month": "$salesTarget.date" },
                year: { "$year": "$salesTarget.date" },
                target: "$salesTarget.target",
                date:"$salesTarget.date"
            }
        },
        {
            $group:{
                _id:{month:"$month",year:"$year"},
                date:{ $max: "$date" },
                month:{ $max: "$month" },
                year:{ $max: "$year" },
                target:{$sum:"$target"}

            }
        }
    ];

    if(fromDate && toDate) {
        aggregation = [
            {
                $match:{
                    userId:{$in:userIds},
                    "fy.start":{$gte:new Date(fyStart)},
                    "fy.end":{$lte:new Date(fyEnd)}
                }
            },
            {
                $unwind:"$salesTarget"
            },
            {
                $match:{
                    "salesTarget.date":{$lte:new Date(toDate),$gte:new Date(fromDate)}
                }
            },
            {
                $project: {
                    month: { "$month": "$salesTarget.date" },
                    year: { "$year": "$salesTarget.date" },
                    target: "$salesTarget.target",
                    date:"$salesTarget.date"
                }
            },
            {
                $group:{
                    _id:{month:"$month",year:"$year"},
                    date:{ $max: "$date" },
                    month:{ $max: "$month" },
                    year:{ $max: "$year" },
                    target:{$sum:"$target"}

                }
            }
        ]
    }

    opportunitiesTargetCollection.aggregate(aggregation).exec(function (err,targets) {

        if(err){
            loggerError.info('Error in getTargetsForAllUsers():opportunitiesManagement ',err)
            callback(err,[])
        }
        else{
            callback(err,targets);
        }
    })
};

OpportunitiesManagement.prototype.getTargetsForAllUsersByUserEmailIds = function(userEmailIds,fromDate,toDate,fyStart,fyEnd,callback){

    var aggregation = [];

    if(fromDate && toDate) {
        aggregation = [
            {
                $match:{
                    userEmailId:{$in:userEmailIds},
                    "fy.start":{$gte:new Date(fyStart)},
                    "fy.end":{$lte:new Date(fyEnd)}
                }
            },
            {
                $unwind:"$salesTarget"
            },
            {
                $match:{
                    "salesTarget.date":{$lte:new Date(toDate),$gte:new Date(fromDate)}
                }
            },
            {
                $project: {
                    month: { "$month": "$salesTarget.date" },
                    year: { "$year": "$salesTarget.date" },
                    target: "$salesTarget.target",
                    date:"$salesTarget.date"
                }
            },
            {
                $group:{
                    _id:{month:"$month",year:"$year"},
                    date:{ $max: "$date" },
                    month:{ $max: "$month" },
                    year:{ $max: "$year" },
                    target:{$sum:"$target"}

                }
            }
        ]
    }

    opportunitiesTargetCollection.aggregate(aggregation).exec(function (err,targets) {

        if(err){
            loggerError.info('Error in getTargetsForAllUsersByUserEmailIds():opportunitiesManagement ',err)
            callback(err,[])
        }
        else{
            callback(err,targets);
        }
    })
};

OpportunitiesManagement.prototype.getTargetForUser = function (userIds,start,end,callback) {

    opportunitiesTargetCollection.find({userId:{$in:userIds},"fy.start":{$gte:new Date(start)},"fy.end":{$lte:new Date(end)}},function (err,targets) {
        callback(err,targets)
    })
}

OpportunitiesManagement.prototype.getTargetForUserByMonthRange = function (userIds,dateMin,dateMax,regionAccess,productAccess,verticalAccess,fyStart,fyEnd,callback) {

    var match = {
        userId:{$in:userIds},
        "fy.start":{$gte:new Date(fyStart)},
        "fy.end":{$lte:new Date(fyEnd)}
    }

    opportunitiesTargetCollection.aggregate([
        {
            $match:match
        },
        {
            $unwind:"$salesTarget"
        },
        {
            $match:{
                "salesTarget.date":{$gte:new Date(dateMin),$lte:new Date(dateMax)}
            }
        },
        {
            $group:{
                _id:"$userId",
                userEmailId:{$last:"$userEmailId"},
                target:{$sum:"$salesTarget.target"}
            }
        }
    ]).exec(function (err,targets) {
        callback(err,targets)
    })
}

OpportunitiesManagement.prototype.getTargetForUserByMonths = function (userIds,dateMin,dateMax,callback) {

    var match = {
        userId:{$in:userIds}
    }

    opportunitiesTargetCollection.aggregate([
        {
            $match:match
        },
        {
            $unwind:"$salesTarget"
        },
        {
            $match:{
                "salesTarget.date":{$gte:new Date(dateMin),$lte:new Date(moment().endOf("month"))}
            }
        },
        {
            $project:{
                userId:"$userId",
                target:"$salesTarget.target",
                date:"$salesTarget.date"
            }
        }
    ]).exec(function (err,targets) {
        callback(err,targets)
    })
}

OpportunitiesManagement.prototype.getGrpTargetForUserByMonths = function (userIds,dateMin,dateMax,fyStart,fyEnd,callback) {

    var match = {
        userId:{$in:userIds},
        // "fy.start":{$gte:new Date(fyStart)},
        // "fy.end":{$lte:new Date(fyEnd)}
    }

    opportunitiesTargetCollection.aggregate([
        {
            $match:match
        },
        // {
        //     $unwind:"$salesTarget"
        // },
        // {
        //     $match:{
        //         "salesTarget.date":{$gte:new Date(dateMin),$lte:new Date(moment(dateMax).endOf("month"))}
        //     }
        // },
        {
            $group:{
                _id:"$userId",
                targets:{
                    $push:
                        "$salesTarget"
                }
            }
        }
    ]).exec(function (err,targets) {
        var reducedData = [];
        if(targets && targets.length>0){
            _.each(targets,function(el){
                var values = []
                el.targets = _.flatten(el.targets);
               _.each(el.targets,function(tr){
                   if(new Date(tr.date)>=new Date(dateMin) && new Date(tr.date)<=new Date(dateMax)){
                       values.push(tr)
                   }
               });
                reducedData.push({
                    _id:el._id,
                    targets: values
                })
            });
        };
        callback(err,reducedData)
    })

}

OpportunitiesManagement.prototype.getTargetForUserByQtrs = function (userIds,dateMin,dateMax,callback) {

    var match = {
        userId:{$in:userIds}
    }

    opportunitiesTargetCollection.aggregate([
        {
            $match:match
        },
        {
            $unwind:"$salesTarget"
        },
        {
            $match:{
                "salesTarget.date":{$gte:new Date(dateMin),$lte:new Date(dateMax)}
            }
        },
        {
            $project:{
                userId:"$userId",
                target:"$salesTarget.target",
                date:"$salesTarget.date"
            }
        }
    ]).exec(function (err,targets) {

        if(targets && targets.length>0){
            _.each(targets,function (tr) {
                tr.monthYear = moment(tr.date).format('MMYYYY')
            });

            targets = _.uniq(targets,'monthYear')
        }
        callback(err,targets)
    })
}

OpportunitiesManagement.prototype.getTargetsByUsers = function (userIds,dateMin,dateMax,fyStart,fyEnd,callback) {

    var match = {
        userId:{$in:userIds},
        "fy.start":{$gte:new Date(fyStart)},
        "fy.end":{$lte:new Date(fyEnd)}
    };

    opportunitiesTargetCollection.aggregate([
        {
            $match:match
        },
        {
            $unwind:"$salesTarget"
        },
        {
            $match:{
                "salesTarget.date":{$gte:new Date(dateMin),$lte:new Date(dateMax)}
            }
        },
        {
            $group:{
                _id:"$userId",
                targets:{$push:"$salesTarget"}
            }
        }
    ]).exec(function (err,targets) {
        callback(err,targets)

    })
}

OpportunitiesManagement.prototype.setTargetForUserBulk = function(data,castToObjectId,fyRange,callback) {

    var bulk = opportunitiesTargetCollection.collection.initializeUnorderedBulkOp();

    _.each(data,function (el) {
        el.userId = castToObjectId(el.userId)
        _.each(el.targets,function (tr) {
            tr.target = parseFloat(tr.target)
            tr.date = new Date(moment(tr.date).add(1, "days"))
        })

        bulk.find({ userId: el.userId,"fy.start":new Date(fyRange.start),"fy.end":new Date(fyRange.end)}).upsert().updateOne({ $set: {salesTarget:el.targets}});
    });

    bulk.execute(function(err, result) {
        if (err) {
            logger.info('Error in setTargetForUserBulk():opportunitiesManagement Bulk', err)
        }

        if (callback) { callback(err, result) }
    });
};

OpportunitiesManagement.prototype.transferOpp = function (userId,data,common,companyId,callback) {

    var bulk = opportunitiesCollection.collection.initializeUnorderedBulkOp();
    var opportunityIds = [],
    onlyOppsIds = [],
        userIdsFrom = [],
        userIdsTo = [];

    _.each(data,function (el) {

        onlyOppsIds.push(el.opps);
        userIdsFrom.push(el.fromUserId.toString());
        userIdsTo.push(el.to.toString());

        opportunityIds.push({
            opportunityId:el.opps,
            userId:common.castToObjectId(el.to.toString()),
            userEmailId:el.emailId,
            companyId:common.castToObjectId(companyId)
        });

        bulk.find({ userId: common.castToObjectId(el.fromUserId),opportunityId:el.opps}).update({
            $set: {
                userId: common.castToObjectId(el.to.toString()),
                userEmailId:el.emailId
            }
        });
    });

    updateContacts(data,common,function () {
        // callback(err,updateResult)
    });

    createLogForOppTransfer(data);

    bulk.execute(function(err, result) {
        if (err) {
            logger.info('Error in transferOpp():opportunitiesManagement Bulk', err)
        } else {
            updateOppMetaDataForOppTransfer(opportunityIds);

            userIdsFrom = common.castListToObjectIds(_.uniq(userIdsFrom))
            userIdsTo = common.castListToObjectIds(_.uniq(userIdsTo))

            secondaryHierarchyObj.removeAccessForMultiUsersMultiOpps(userIdsFrom,companyId,onlyOppsIds,function () {
                secondaryHierarchyObj.addAccessForMultiUsersMultiOpps(userIdsTo,companyId,onlyOppsIds)
            })
        }

        if (callback) { callback(err, result) }
    });
}

OpportunitiesManagement.prototype.updateContacts = updateContacts


function updateOppMetaDataForOppTransfer(data,callback){

    var findQuery = {
        userId:data[0].userId,
        userEmailId:data[0].userEmailId,
        opportunityId:{$in:_.map(data,"opportunityId")}
    };

    opportunitiesCollection.find(findQuery).lean().exec(function (err,opps) {
        updateMetaDataForBulkOpps(data[0].companyId,data[0].userEmailId,opps,function (err,result) {
           if(callback){callback(err,result)}
        });
    });
}


function createLogForOppTransfer(data){
    oppLogObj.createLogForOppTransfer(data)
}

OpportunitiesManagement.prototype.createLogForOppStateChange = function (userId,oldStateOpp,newStateOpp,callback){
    oppLogObj.createLogForOppStateChange(userId,oldStateOpp,newStateOpp,callback)
}

OpportunitiesManagement.prototype.createLogForPeopleAdded = function (userId,opportunityId,oldState,newState,type,action,callback){
    oppLogObj.createLogForPeopleAdded(userId,opportunityId,oldState,newState,type,action,callback)
}

OpportunitiesManagement.prototype.createLogForCreation = function (userId,userEmailId,opportunityId,action,callback){
    oppLogObj.createLogForCreation(userId,userEmailId,opportunityId,action,callback)
}

function updateContacts(data,common,callback) {
    var contacts = [];
    _.each(data,function (el) {
        if(el){
            if(el.contacts && el.contacts.length>0){
                _.each(el.contacts,function (co) {

                    contacts.push(buildContactObj(co,common,el.to,el.emailId))
                })
            }
        }
    });

    contactObj.oppTransferContactUpdate(contacts,function (err,res) {
        callback();
    })

}

OpportunitiesManagement.prototype.setTargetForCompanyBulk = function(data,castToObjectId,callback) {

    var bulk = opportunitiesTargetCollection.collection.initializeUnorderedBulkOp();

    _.each(data,function (el) {
        el.userId = castToObjectId(el.userId)
        _.each(el.targets,function (tr) {
            tr.target = parseFloat(tr.target)
            tr.date = new Date(moment(tr.date).add(1, "days"))
        })

        bulk.find({ userId: el.userId}).upsert().updateOne({ $set: {salesTarget:el.targets,isCompanyTarget:true}});
    });

    bulk.execute(function(err, result) {
        if (err) {
            logger.info('Error in setTargetForUserBulk():opportunitiesManagement Bulk', err)
        }

        if (callback) { callback(err, result) }
    });
};

OpportunitiesManagement.prototype.oppCreatedByMonth = function (userIds,date,regionAccess,productAccess,verticalAccess,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId);

    var match = {
        companyId,companyId,
        $or:[{
            userId:{$in:userIds},
            $or:[
                {createdDate:{$gte: new Date(date)}},
                {closeDate:{$gte: new Date(date)}},
                {createdDate:{$exists:false}}
            ]
        }
        // ,{
        //         $and:accessControlQuery,
        //     $or:[
        //         {createdDate:{$gte: new Date(date)}},
        //         {createdDate:{$exists:false}}
        //     ]
        //     }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds},
            companyId,companyId,
            $or:[
                {createdDate:{$gte: new Date(date)}},,
                {closeDate:{$gte: new Date(date)}},
                {createdDate:{$exists:false}}
            ]
        }
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:{
                userId:"$userId",
                userEmailId:"$userEmailId",
                createdDate:"$createdDate",
                closeDate:"$closeDate",
                isClosed:"$isClosed",
                stageName:"$relatasStage",
                opportunityName:"$opportunityName"
            }
        }
    ]).exec(function (err, opps) {
        callback(err,opps)
    })
}

OpportunitiesManagement.prototype.oppCreatedByMonthForAccount = function (userIds,date,contacts,callback) {

    var match = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["userId"] = el;
        obj["$or"] = [{closeDate:{$gte: new Date(date)}},{createdDate:{$gte: new Date(date)}}, {createdDate:{$exists:false}}];
        obj["contactEmailId"] = {$in:contacts}

        // obj["$or"] = [
        //     {"contactEmailId":new RegExp('.'+account+'.', "i")},
        //     {"contactEmailId":new RegExp('@'+account, "i")}
        // ]

        return obj;
    })};

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:{
                userId:"$userId",
                userEmailId:"$userEmailId",
                createdDate:"$createdDate",
                closeDate:"$closeDate",
                isClosed:"$isClosed",
                stageName:"$relatasStage",
                opportunityName:"$opportunityName"
            }
        }
    ]).exec(function (err, opps) {
        callback(err,opps)
    })
}

OpportunitiesManagement.prototype.updateInteractionCountForOpenOpp = function (userId,date,callback) {

    var userIds = [userId];

    this.allOpenOpportunitiesForUserByOppId(userIds,null,null,function (err,opps) {

        var contactsAndOpp = [],oppGroup = [],allContacts = [],oppCreatedDates = [];

        _.each(opps,function (op) {
            var contacts = _.flatten(op.contacts)

            oppCreatedDates.push(op._id.getTimestamp());

            var obj = {
                oppId:op._id,
                contacts:contacts
            };

            var obj2 = {
                oppId:op._id,
                contacts:_.map(contacts,function (co) {
                    return {
                        emailId:co
                    }
                })
            };

            allContacts.push(contacts);
            contactsAndOpp.push(obj);
            oppGroup.push(obj2);
        });

        var minDate = _.min(oppCreatedDates);

        getInteractionCount(userIds,_.flatten(allContacts),minDate,new Date(),function (err,interactions) {

            var data = _
                .chain(interactions)
                .groupBy('_id.emailId')
                .map(function(value, key) {

                    var values = [];
                    _.each(value,function (va) {
                        values.push({
                            count:va.count,
                            date:va._id.day+'-'+va._id.month+'-'+va._id.year,
                            compareDate:new Date(va._id.year, va._id.month-1, va._id.day)
                        })
                    });

                    return {
                        emailId:key,
                        data:values
                    }
                })
                .value();

            var intWithContactsDictionary = {};

            if(data.length>0){
                _.each(data,function (el) {
                    intWithContactsDictionary[el.emailId] = el.data;
                })
            }

            _.each(oppGroup,function (op) {
                var oppDate = op.oppId.getTimestamp();

                var day = moment(oppDate).date()
                var month = moment(oppDate).month()
                var year = moment(oppDate).year()

                var compareDate = new Date(year, month, day)

                _.each(op.contacts,function (co) {
                    var counter = 0;
                    var values = intWithContactsDictionary[co.emailId];
                    if(values && values.length>0){
                        _.each(values,function (va) {
                            if(new Date(va.compareDate)>=compareDate){
                                counter = counter+va.count
                            }
                        });
                    }

                    co.counter = counter
                });
            });

            updateOpenOppWithInteractions(oppGroup,function (updateErr,result) {

                if(callback){
                    callback(updateErr,result);
                }
                updateLastSyncDate(userId)
            })
        })

    });
};

OpportunitiesManagement.prototype.getInteractionActivity = function (userIds,contacts,closeDate,callback) {

    var from = moment(moment().subtract(6,'month')).startOf('month');
    var query = {
        ownerId:{$in:userIds},
        emailId:{$in:contacts},
        interactionDate:{
            $gte: new Date(from),
            $lte: closeDate?new Date(closeDate):new Date()
        }
    }

    InteractionsV2Collection.aggregate([
        {
            $match:query
        },
        {
            $group: {
                _id:"$refId",
                ownerEmailId: { "$first": "$ownerEmailId" },
                ownerId: { "$first": "$ownerId" },
                emailId: { "$first": "$emailId" },
                title: { "$first": "$title" },
                interactionDate: { "$first": "$interactionDate" },
                interactionType: { "$first": "$interactionType" },
                action: { "$first": "$action" }
            }
        },
        {
            $project: {
                ownerEmailId:1,
                ownerId:1,
                emailId:1,
                title:1,
                interactionDate:1,
                interactionType:1,
                month: {$month: "$interactionDate"},
                year: {$year: "$interactionDate"},
                receiver: {$cond: [{$eq: ["$action", "sender"]}, 1, 0]},
                sender: {$cond: [{$eq: ["$action", "receiver"]}, 1, 0]}
            }
        },
        {
            $group: {
                _id: {month: "$month", year: "$year"},
                count: {$sum: 1},
                initiatedByThem: {$sum: "$sender"},
                initiatedByUs: {$sum: "$receiver"},
                date: {$first: "$interactionDate"},
                interactions: {
                    $push: "$$ROOT"
                }
            }
        }
    ]).exec(function (err,data) {

        var allInteractions = [],
            interactionsFlow = [];

        var result = {
            interactionsFlow:[],
            allInteractions:[]
        }

        var existingMYs = {};

        if(!err && data && data.length>0){

            _.each(data,function (el) {
                existingMYs[el._id.month+""+el._id.year] = el._id.month+""+el._id.year;

                var them = 0,
                    us = 0;

                interactionsFlow.push({
                    monthYear: el._id,
                    monthYearFormat: el._id.month+""+el._id.year,
                    count: el.count,
                    initiatedByThem: el.initiatedByThem,
                    initiatedByUs: el.initiatedByUs,
                    date: el.date
                });

                _.each(el.interactions,function (int) {
                    allInteractions.push(int);
                });

            })
        }

        allInteractions.sort(function (o2, o1) {
            return new Date(o1.interactionDate) > new Date(o2.interactionDate) ? 1 : new Date(o1.interactionDate) < new Date(o2.interactionDate) ? -1 : 0;
        });

        result = {
            interactionsFlow:interactionsFlow.concat(interactionsFiller(existingMYs,from)),
            allInteractions:allInteractions.slice(0,3)
        }

        callback(err,result);
    });
}

OpportunitiesManagement.prototype.getInteractionHistoryByDate = function (userEmailIds,contacts,from,to,callback) {
    var query = {
        ownerEmailId:{$in:userEmailIds},
        emailId:{$in:contacts},
        interactionDate:{
            $gte: new Date(from),
            $lte: to?new Date(to):new Date()
        }
    }

    InteractionsV2Collection.aggregate([
        {
            $match:query
        },
        {
            $group: {
                _id:"$refId",
                interactionDate: { "$first": "$interactionDate" },
                action: { "$first": "$action" }
            }
        },
        {
            $project: {
                interactionDate:1,
                dayOfMonth: {$dayOfMonth: "$interactionDate"},
                month: {$month: "$interactionDate"},
                year: {$year: "$interactionDate"},
                receiver: {$cond: [{$eq: ["$action", "sender"]}, 1, 0]},
                sender: {$cond: [{$eq: ["$action", "receiver"]}, 1, 0]}
            }
        },
        {
            $group: {
                _id: {dayOfMonth: "$dayOfMonth",month: "$month", year: "$year"},
                count: {$sum: 1},
                initiatedByThem: {$sum: "$sender"},
                initiatedByUs: {$sum: "$receiver"}
            }
        }
    ]).exec(function (err,data) {
        callback(err,data)
    })
}

function interactionsFiller(existingMYsObj,from){
    var startDate = moment(from).add(1,'d');
    var endDate = moment();

    var result = [],allMys = [];

    if (endDate.isBefore(startDate)) {
        throw "End date must be greater than start date."
    }

    var currentDate = startDate.clone();

    while (currentDate.isBefore(endDate)) {
        var monthYear = moment(currentDate).month()+1+""+moment(currentDate).year();

        if(!existingMYsObj[monthYear]){
            result.push({
                date:new Date(currentDate),
                monthYearFormat:monthYear,
                initiatedByThem:0,
                initiatedByUs:0,
                monthYear:{
                    month:moment(currentDate).month()+1,
                    year:moment(currentDate).year()
                },
                count:0
            });
        }

        currentDate.add(1, 'month');
    };

    return result;
}

OpportunitiesManagement.prototype.updateInteractionsForAllOpenOpps = function (userId,callback) {

    opportunitiesCollection.find({userId:userId,
        stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}})
        .lean()
        .exec(function (err,opps) {
            var forIntQuery = [],allUserEmailIds = [],contacts = [],oldestCreatedDate = new Date();
            if(!err && opps && opps.length>0){
                _.each(opps,function (op) {
                    allUserEmailIds.push(op.userEmailId);
                    op.contacts = [];
                    op.allUserEmailIds = [op.userEmailId];

                    var createdDate = new Date(op.createdDate?op.createdDate:op._id.getTimestamp());
                    if(createdDate<oldestCreatedDate){
                        oldestCreatedDate = createdDate
                    }

                    if(op.usersWithAccess && op.usersWithAccess.length>0){
                        var emailIds = _.pluck(op.usersWithAccess,"emailId");
                        allUserEmailIds = allUserEmailIds.concat(emailIds)
                        op.allUserEmailIds = op.allUserEmailIds.concat(emailIds)
                    }

                    contacts.push(op.contactEmailId);
                    op.contacts.push(op.contactEmailId)

                    if(op.partners && op.partners.length>0){
                        var partners = _.pluck(op.partners,"emailId")

                        op.contacts = op.contacts.concat(partners)
                        contacts = contacts.concat(partners)
                    }

                    if(op.decisionMakers && op.decisionMakers.length>0){
                        var decisionMakers = _.pluck(op.decisionMakers,"emailId")

                        op.contacts = op.contacts.concat(decisionMakers)
                        contacts = contacts.concat(decisionMakers)
                    }

                    if(op.influencers && op.influencers.length>0){
                        var influencers = _.pluck(op.influencers,"emailId")
                        op.contacts = op.contacts.concat(influencers)
                        contacts = contacts.concat(influencers)
                    }

                })

                allUserEmailIds = _.uniq(allUserEmailIds)
                contacts = _.uniq(contacts);

                getInteractionsForAllUsers(allUserEmailIds,contacts,oldestCreatedDate,function (err1,interactionsByUser) {

                    if(!err1 && interactionsByUser){

                        var bulk = opportunitiesCollection.collection.initializeUnorderedBulkOp();

                        _.each(opps,function (el) {

                            var usersAndInteractions = [],
                                primaryCount = 0,
                                partnerCount = 0,
                                dmCount = 0,
                                infCount = 0;
                            if(interactionsByUser[el.userEmailId]){
                                if(interactionsByUser[el.userEmailId][el.contactEmailId]){
                                    _.each(interactionsByUser[el.userEmailId][el.contactEmailId],function (it) {
                                        if((new Date(it.interactionDate)>= new Date(el.createdDate)) && new Date(it.interactionDate)<= new Date(el.closeDate)){
                                            primaryCount++
                                        }
                                    })
                                }
                            }
                            bulk.find({ _id: el._id}).update({ $set: {usersAndInteractions:usersAndInteractions} });
                            bulk.find({ _id: el._id,"contactEmailId": el.emailId}).update({ $set: {interactionCount:primaryCount} });
                            bulk.find({ _id: el._id,"partners.emailId": el.emailId}).update({ $set: {"partners.$.interactionCount":partnerCount} });
                            bulk.find({ _id: el._id,"decisionMakers.emailId": el.emailId}).update({ $set: {"decisionMakers.$.interactionCount":dmCount} });
                            bulk.find({ _id: el._id,"influencers.emailId": el.emailId}).update({ $set: {"influencers.$.interactionCount":infCount} });
                        });

                        bulk.execute(function(err, result) {
                            if (err) {
                                logger.info('Error in updateInteractionsForAllOpenOpps():opportunitiesManagement Bulk', err)
                            } else {
                                result = result.toJSON();
                                logger.info('updateInteractionsForAllOpenOpps() opportunitiesManagement ', result);
                            }

                            if (callback) { callback(err, result) }
                        });
                    } else {
                        if (callback) { callback(err1, null) }
                    }
                })
            } else {
                if(callback){
                    callback()
                }
            }
    })
}

function getInteractionsForAllUsers(allUserEmailIds,contacts,oldestCreatedDate,callback){

    var query = {
        ownerEmailId:{$in:allUserEmailIds},
        emailId:{$in:contacts},
        interactionDate:{
            $gte: new Date(oldestCreatedDate),
            $lte: new Date()
        }
    }

    InteractionsV2Collection.aggregate([
        {
            $match:query
        },
        {
            $project: {
                ownerEmailId:1,
                emailId:1,
                interactionDate:1,
            }
        },
        {
            $group:{
                _id:"$ownerEmailId",
                interactions: {$push: "$$ROOT"}
            }
        }
    ]).exec(function (err,data) {

        var interactionsByUser = {};

        if(!err && data){

            _.each(data,function (el) {
                var intGroupByContactObj = {};
                var intGroupByContact =  _
                    .chain(el.interactions)
                    .groupBy('emailId')
                    .map(function(values, key) {
                        intGroupByContactObj[key] = values;
                        return null;
                    })
                    .value();

                interactionsByUser[el._id] = intGroupByContactObj;
            })
            callback(err,interactionsByUser)
        } else {
            callback(null,null)
        }
    })
}

OpportunitiesManagement.prototype.getInteractionCount = function(userIds,contacts,fromDate,toDate,callback){
    var query = {
        ownerId:{$in:userIds},
        emailId:{$in:contacts},
        interactionDate:{
            $gte: new Date(fromDate),
            $lte: toDate?new Date(toDate):new Date()
        }
    }

    InteractionsV2Collection.aggregate([
        {
            $match:query
        },
        {
            $project:{
                _id:1,
                refId:1
            }
        },
        {
            $group:{
                _id:null,
                uniqueInteractions: { "$addToSet": "$refId" }
                // interactionsCount: { "$sum": 1 }
            }
        },
        {
            $project:{
                interactionsCount: { "$size": "$uniqueInteractions" }
            }
        }
    ]).exec(function (err,data) {
        if(!err && data && data[0]){
            callback(err,data[0].interactionsCount);
        } else {
            callback(err,0)
        }
    });
}

OpportunitiesManagement.prototype.updateInteractionCountForClosingOpp = function (userIds,opportunity,callback) {

    var allContacts = [],contactsForUpdate = [],oppGroup = [], allEmailIds = [];
    opportunity = opportunity[0];

    allEmailIds.push(opportunity.userEmailId);
    if(opportunity.usersWithAccess && opportunity.usersWithAccess.length>0){
        allEmailIds = allEmailIds.concat(_.pluck(opportunity.usersWithAccess,"emailId"))
    }

    allContacts.push(opportunity.contactEmailId);
    contactsForUpdate.push({emailId:opportunity.contactEmailId});

    if(opportunity.influencers && opportunity.influencers.length>0){
        _.each(opportunity.influencers,function (el) {
            allContacts.push(el.emailId)
            contactsForUpdate.push({emailId:el.emailId})
        })
    }

    if(opportunity.partners && opportunity.partners.length>0){

        _.each(opportunity.partners,function (el) {
            allContacts.push(el.emailId)
            contactsForUpdate.push({emailId:el.emailId})
        })

    }

    if(opportunity.decisionMakers && opportunity.decisionMakers.length>0){

        _.each(opportunity.decisionMakers,function (el) {
            allContacts.push(el.emailId)
            contactsForUpdate.push({emailId:el.emailId})
        })
    }

    var obj = {
        oppId:opportunity._id,
        contacts:contactsForUpdate,
        createdDate:opportunity.createdDate
    };

    oppGroup.push(obj);

    var query = {
        ownerEmailId:{$in:_.uniq(allEmailIds)},
        emailId:{$in:_.uniq(allContacts)},
        interactionDate:{
            $gte: new Date(opportunity.createdDate?opportunity.createdDate:opportunity._id.getTimestamp()),
            $lte: new Date(opportunity.closeDate)
        }
    }

    InteractionsV2Collection.aggregate([
        {
            $match:query
        },
        {
            $group:{
                _id:{
                    ownerEmailId:"$ownerEmailId",
                    emailId:"$emailId"
                },
                interactionsCount: { "$sum": 1 }
            }
        }
    ]).exec(function (err,data) {

        var usersAndInteractions = [];

        if(!err && data) {

            if(data.length>0){
                _.each(data,function (el) {
                    usersAndInteractions.push({
                        ownerEmailId: el._id.ownerEmailId,
                        emailId: el._id.emailId,
                        count: el.interactionsCount
                    })

                    if(el._id.ownerEmailId === opportunity.userEmailId){
                        oppGroup.push({
                            oppId:opportunity._id,
                            emailId:el._id.emailId,
                            count: el.interactionsCount
                        });
                    }
                })
            }

            updateOppWithInteractions(oppGroup,opportunity._id,usersAndInteractions,function (updateErr,result) {

                if(callback){
                    callback(updateErr,result);
                }

            })
        } else {
            if(callback){
                callback(null,null);
            }
        }

    })
};

OpportunitiesManagement.prototype.getOppByCustomQuery = function (query,projection,callback){

    if(projection){
        opportunitiesCollection.find(query,projection).lean().exec(function (err,opps) {
            callback(err,opps)
        })
    } else {
        opportunitiesCollection.find(query).lean().exec(function (err,opps) {
            callback(err,opps)
        })
    }
}


OpportunitiesManagement.prototype.getOppByCustomAggregateQuery = function (query,callback){
    opportunitiesCollection.aggregate(query,function(error, result){
        callback(error,result)
    })
}

OpportunitiesManagement.prototype.getOppByCustomQueryWithPagination = function (query,projection,skip,limit,callback){

    logger.info('Temp getOppByCustomQuery ',query);
    var skip_limit = {skip:skip,limit:1};

    if(projection){
        opportunitiesCollection.find(query,projection,skip_limit).lean().exec(function (err,opps) {
            callback(err,opps)
        })
    } else {
        opportunitiesCollection.find(query,null,skip_limit).lean().exec(function (err,opps) {
            callback(err,opps)
        })
    }
}

OpportunitiesManagement.prototype.getAllOpportunitiesByStageName = function (userId,dateMin,dateMax,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId)

    var match = {
        $or:[{userId:userId},
            {
                $and:accessControlQuery
            }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:userId
        }
    }

    if(dateMax && dateMin) {
        match = {
            $or:[{  userId:userId},
                {
                    $and:accessControlQuery
                }],
            closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
        }

        if(accessControlQuery.length == 0){
            match = {
                userId:userId,
                closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
            }
        }
    }

    var projectQuery = {
        relatasStage:"$relatasStage",
        amount: "$amount"
    }

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount()
    }

    opportunitiesCollection.aggregate([
        {
            $match: match
        },
        {
            $project:projectQuery
        },
        {
            $group:{
                _id:"$relatasStage",
                countOfOpps:{$sum:1},
                sumOfAmount:{$sum:"$amount"}
            }
        }
    ]).exec(function (err, opps) {
        callback(err,opps)
    })
}

OpportunitiesManagement.prototype.getInteractionCountForOpp = function (userIds,dateMin,dateMax,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:{
                userId:{$in:userIds},
                stageName:{$in:['Close Won','Close Lost','Closed Won','Closed Lost']},
                closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
            }
        },{
            $project:{
                amount:1,
                closeDate:1,
                interactionCount:1,
                partners:1,
                decisionMakers:1,
                influencers:1,
                userId:1,
                opportunityName:1,
                userEmailId:1
            }
        }],function(error, result){
            if(error){
                loggerError.info('Error in getInteractionCountForOpp():opportunitiesManagement ',error)
                callback(error,null);
            }
            else{
                callback(null,result);
            }
        });
}

OpportunitiesManagement.prototype.getOppByIds = function (oppIds,callback) {

    opportunitiesCollection.aggregate([
        {
            $match:{_id:{$in:oppIds}}
        },
        {
            $group: {
                _id: "$userEmailId",
                opportunities: {
                    $push: {
                        "_id": "$_id",
                        "contactEmailId": "$contactEmailId",
                        "mobileNumber": "$mobileNumber",
                        "opportunityId": "$opportunityId",
                        "opportunityName": "$opportunityName",
                        "stageName": "$relatasStage",
                        "amount": "$amount",
                        "closeDate": "$closeDate",
                        "createdDate": "$createdDate",
                        "geoLocation": "$geoLocation",
                        "partners": "$partners",
                        "decisionMakers": "$decisionMakers",
                        "influencers": "$influencers",
                        "productType": "$productType",
                        "userEmailId": "$userEmailId",
                        "userId": "$userId",
                        "lastStageUpdated": "$lastStageUpdated"
                    }
                },
                usersAndContacts:{
                    $addToSet: {
                        "contactEmailId": "$contactEmailId",
                        "userEmailId": "$userEmailId"
                    }
                }
            }
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in getOppById():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });

}

OpportunitiesManagement.prototype.getOpportunitiesByAccount = function (userIds,dateMin,dateMax,contacts,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["userId"] = el;
        obj["contactEmailId"] = {$in:contacts}

        if(dateMin && dateMax){
            obj["closeDate"] = {
                $gte: new Date(dateMin),
                $lte: new Date(dateMax)
            }
        }

        return obj;
    })};

    opportunitiesCollection.aggregate([
        {
            $match:query
        }
    ],function(error, result){

        if(error){
            loggerError.info('Error in getOpportunitiesByAccount():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
}

OpportunitiesManagement.prototype.takeOppSnapshotForWeek = function (userIds,callback){
    callback()
}

OpportunitiesManagement.prototype.oppConversionByWeek = function (userId,dateMin,dateMax,primaryCurrency,currenciesObj,companyId,portfolioAcc,portfolioHead,callback){

    var query = {
        userId:userId,
        $or:[
            {createdDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
            {closeDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
            {createdDate:{$exists:false}}
        ]
    }

    if(portfolioAcc){
        query = {
            companyId:companyId,
            $and: portfolioAcc,
            $or:[
                {createdDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
                {closeDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
                {createdDate:{$exists:false}}
            ]
        }
    }

    if(portfolioHead){
        query = {
            companyId:companyId,
            $and: portfolioHead,
            $or:[
                {createdDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
                {closeDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
                {createdDate:{$exists:false}}
            ]
        }
    }

    opportunitiesCollection.find(query,
        {relatasStage:1,createdDate:1,closeDate:1,amount:1,netGrossMargin:1,currency:1}
        ,function (err,opps) {
            if(!err && opps){
                callback(err,groupOppsWonLostByWeek(opps,dateMin,primaryCurrency,currenciesObj))
            } else {
                callback(err,opps)
            }
    });
}

OpportunitiesManagement.prototype.oppConversionByMonth = function (userId,dateMin,dateMax,primaryCurrency,currenciesObj,callback){

    var query = {
        userId:userId,
        $or:[
            {createdDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
            {closeDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
            {createdDate:{$exists:false}}
        ]
    }

    opportunitiesCollection.find(query,
        {relatasStage:1,createdDate:1,closeDate:1,amount:1,netGrossMargin:1,currency:1}
        ,function (err,opps) {
            if(!err && opps){
                callback(err,groupOppsWonLostByMonth(opps,dateMin,dateMax,primaryCurrency,currenciesObj))
            } else {
                callback(err,opps)
            }
    });
}

OpportunitiesManagement.prototype.oppConversionByMonthForPortfolios = function (userId,dateMin,dateMax,primaryCurrency,currenciesObj,portfolioAcc,companyId,portfolioHead,callback){

    var query = {
        userId:userId,
        $or:[
            {createdDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
            {closeDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
            {createdDate:{$exists:false}}
        ]
    }

    if(portfolioAcc){
        query = {
            companyId:companyId,
            $and: portfolioAcc,
            $or:[
                {createdDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
                {closeDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
                {createdDate:{$exists:false}}
            ]
        }
    }

    if(portfolioHead){
        query = {
            companyId:companyId,
            $and: portfolioHead,
            $or:[
                {createdDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
                {closeDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
                {createdDate:{$exists:false}}
            ]
        }
    }

    opportunitiesCollection.find(query,
        {relatasStage:1,createdDate:1,closeDate:1,amount:1,netGrossMargin:1,currency:1}
        ,function (err,opps) {
            if(!err && opps){
                callback(err,groupOppsWonLostByMonth(opps,dateMin,dateMax,primaryCurrency,currenciesObj))
            } else {
                callback(err,opps)
            }
    });
}

OpportunitiesManagement.prototype.oppConversionByMonthBasedOnAccess = function (userId,dateMin,dateMax,primaryCurrency,currenciesObj,accessOpps,companyId,callback){

    var query = {
        userId:userId,
        $or:[
            {createdDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
            {closeDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
            {createdDate:{$exists:false}}
        ]
    }

    if(accessOpps && accessOpps.length>0){
        query = {
            companyId:companyId,
            userId:userId,
            $or:[
                {createdDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
                {closeDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
                {createdDate:{$exists:false}},
                {opportunityId:{$in: accessOpps}}
            ]
        }
    }

    opportunitiesCollection.find(query,
        {relatasStage:1,createdDate:1,closeDate:1,amount:1,netGrossMargin:1,currency:1}
        ,function (err,opps) {
            if(!err && opps){
                callback(err,groupOppsWonLostByMonth(opps,dateMin,dateMax,primaryCurrency,currenciesObj))
            } else {
                callback(err,opps)
            }
    });
}

OpportunitiesManagement.prototype.oppConversionByQuarter = function (userId,dateMin,dateMax,allQuarters,timezone,fyStartDt,primaryCurrency,currenciesObj,callback){

    dateMin = moment(dateMin).subtract(9,"month")

    var query = {
        userId:userId,
        $or:[
            {createdDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
            {closeDate:{$gte: new Date(dateMin),$lte: new Date(dateMax)}},
            {createdDate:{$exists:false}}
        ]
    }

    opportunitiesCollection.find(query,
        {relatasStage:1,createdDate:1,closeDate:1,amount:1,netGrossMargin:1,currency:1}
        ,function (err,opps) {
            if(!err && opps){
                callback(err,groupOppsWonLostByQtr(opps,allQuarters,dateMin,dateMax,timezone,fyStartDt,primaryCurrency,currenciesObj))
            } else {
                callback(err,opps)
            }
    });
}

OpportunitiesManagement.prototype.oppConversionByPastQuarter = function (userId,qtrRanges,primaryCurrency,currenciesObj,allQuarters,portfolioAcc,portfolioHead,companyId,callback){

    var asyncFunctions = [],
        qtrYrs = [],
        qtrYrsObj = {};

    _.each(qtrRanges,function(qtr){

        if(qtr.qtrStart && qtr.qtrEnd){
            qtrYrs.push(qtr.quarterYear);
            qtrYrsObj[qtr.quarterYear] = qtr;

            var query = {
                userId:userId,
                $or:[
                    {createdDate:{$gte: new Date(qtr.qtrStart),$lte: new Date(qtr.qtrEnd)}},
                    {closeDate:{$gte: new Date(qtr.qtrStart),$lte: new Date(qtr.qtrEnd)}},
                    {createdDate:{$exists:false}}
                ]
            };


            if(portfolioAcc){
                query = {
                    companyId:companyId,
                    $and: portfolioAcc,
                    $or:[
                        {createdDate:{$gte: new Date(qtr.qtrStart),$lte: new Date(qtr.qtrEnd)}},
                        {closeDate:{$gte: new Date(qtr.qtrStart),$lte: new Date(qtr.qtrEnd)}},
                        {createdDate:{$exists:false}}
                    ]
                }
            }

            if(portfolioHead){
                query = {
                    companyId:companyId,
                    $and: portfolioHead,
                    $or:[
                        {createdDate:{$gte: new Date(qtr.qtrStart),$lte: new Date(qtr.qtrEnd)}},
                        {closeDate:{$gte: new Date(qtr.qtrStart),$lte: new Date(qtr.qtrEnd)}},
                        {createdDate:{$exists:false}}
                    ]
                }
            }

            asyncFunctions.push(function (callback) {
                opportunitiesCollection.find(query,{relatasStage:1,createdDate:1,closeDate:1,amount:1,netGrossMargin:1,currency:1}
                    ,function (err,opps) {
                        if(!err && opps){
                            callback(err,groupOppsWonLostByQtr_async(opps,qtr,primaryCurrency,currenciesObj,allQuarters))
                        } else {
                            callback(err,{created:[],oppsWon:[]})
                        }
                    })
            });
        }
    });

    async.parallel(asyncFunctions, function (errors,data) {
        var created = [],
            oppsWon = [];

        if(!errors && data && data.length>0){
            _.each(data,function (el) {
                created = created.concat(el.created)
                oppsWon = oppsWon.concat(el.oppsWon)
            })
        }

        var nonExistingOppCreated = _.difference(qtrYrs,_.pluck(created,"quarterYear"));
        var nonExistingOppWon = _.difference(qtrYrs,_.pluck(oppsWon,"quarterYear"));

        _.each(nonExistingOppCreated,function (el) {
            created.push({
                forDate:qtrYrsObj[el].qtrStart,
                quarterYear: qtrYrsObj[el].quarterYear,
                date:qtrYrsObj[el].qtrStart,
                count: 0,
                amount:0
            })
        })
        _.each(nonExistingOppWon,function (el) {
            oppsWon.push({
                forDate:qtrYrsObj[el].qtrStart,
                quarterYear: qtrYrsObj[el].quarterYear,
                date:qtrYrsObj[el].qtrStart,
                count: 0,
                amount:0
            })
        });

        callback(errors,{created:created, oppsWon:oppsWon})
    });
}


function groupOppsWonLostByQtr_async(opps,qtr,primaryCurrency,currenciesObj,allQuarters){
    var oppsWon = [],
        oppsCreated = [];

    _.each(opps,function (op) {

        if(!op.createdDate){
            op.createdDate = op._id.getTimestamp()
        }

        if(op.netGrossMargin || op.netGrossMargin == 0){
            var amountWithNGM = op.netGrossMargin*op.amount
            if(amountWithNGM){
                amountWithNGM = amountWithNGM/100
            }
            op.amountWithNgm = amountWithNGM;
        }

        convertAmount(op,primaryCurrency,currenciesObj);

        if(op.relatasStage && (op.relatasStage.toLowerCase() == "closed won" || op.relatasStage.toLowerCase() == "close won")){

            if( new Date(op.closeDate)>=new Date(qtr.qtrStart)){
                oppsWon.push({
                    _id:op._id,
                    createdDate:op.createdDate,
                    amountNonNGM:op.convertedAmt,
                    amount:op.convertedAmtWithNgm,
                    netGrossMargin:op.netGrossMargin,
                    relatasStage:op.relatasStage,
                    forDate:op.closeDate,
                    quarterYear:qtr.quarterYear
                })
            }
        }

        if( new Date(op.createdDate)>=new Date(qtr.qtrStart)){
            oppsCreated.push({
                _id:op._id,
                amountNonNGM:op.convertedAmt,
                amount:op.convertedAmtWithNgm,
                netGrossMargin:op.netGrossMargin,
                createdDate:op.createdDate,
                relatasStage:op.relatasStage,
                forDate:op.createdDate,
                quarterYear:qtr.quarterYear
            })
        }
    });

    var dataCreated = _
        .chain(oppsCreated)
        .groupBy('quarterYear')
        .map(function(value, key) {

            var sumVal = _.sum(value,"amount");
            return {
                forDate:value[0].forDate,
                quarterYear: key,
                date:value[0].forDate,
                count: value.length,
                amount:sumVal?sumVal:0
            }
        })
        .value();

    var dataWon = _
        .chain(oppsWon)
        .groupBy('quarterYear')
        .map(function(value, key) {
            var sumVal = _.sum(value,"amount");
            return {
                forDate:value[0].forDate,
                quarterYear: key,
                date:value[0].forDate,
                count: value.length,
                amount:sumVal?sumVal:0
            }
        })
        .value();

    return {
        created:dataCreated,
        oppsWon:dataWon
    };
}

OpportunitiesManagement.prototype.getOppForUserById = function (userId,netGrossMarginReq,callback){

    var projectQuery = {
        "_id": "$_id",
        "contactEmailId": "$contactEmailId",
        "mobileNumber": "$mobileNumber",
        "opportunityId": "$opportunityId",
        "opportunityName": "$opportunityName",
        "stageName": "$stageName",
        "relatasStage": "$relatasStage",
        "sourceType": "$sourceType",
        "closeDate": "$closeDate",
        "createdDate": "$createdDate",
        "geoLocation": "$geoLocation",
        "partners": "$partners",
        "decisionMakers": "$decisionMakers",
        "influencers": "$influencers",
        "productType": "$productType",
        "userEmailId": "$userEmailId",
        "userId": "$userId",
        "BANT": "$BANT",
        "vertical": "$vertical",
        "competitor": "$competitor",
        "isWon": "$isWon",
        "notes": "$notes",
        "isClosed": "$isClosed",
        "closeReasonDescription": "$closeReasonDescription",
        "closeReasons": "$closeReasons",
        "netGrossMargin": "$netGrossMargin",
        "lastStageUpdated": "$lastStageUpdated",
        "amount": "$amount",
        "amountNonNGM": "$amount"
    }

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount()
    }

    opportunitiesCollection.aggregate([
        {
            $match:{userId:userId}
        },
        {
            $project:projectQuery
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in getOppForUserById():opportunitiesManagement ',error)
            callback(error,[]);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.findOppByCurrency = function(companyId,currency,callback){
    opportunitiesCollection.findOne({companyId:companyId})
}

function groupOppsWonLostByWeek(opps,dateMin,primaryCurrency,currenciesObj){

    var oppsWon = [],
        oppsCreated = [];
    _.each(opps,function (op) {

        if(!op.createdDate){
            op.createdDate = op._id.getTimestamp()
        }

        if(op.netGrossMargin || op.netGrossMargin == 0){
            var amountWithNGM = op.netGrossMargin*op.amount
            if(amountWithNGM){
                amountWithNGM = amountWithNGM/100
            }

            op.amountWithNgm = amountWithNGM;
        }

        convertAmount(op,primaryCurrency,currenciesObj);

        if(op.relatasStage && (op.relatasStage.toLowerCase() == "closed won" || op.relatasStage.toLowerCase() == "close won")){

            if(new Date(op.closeDate)>= new Date() || new Date(op.closeDate)<= new Date(dateMin)){
            } else {
                oppsWon.push({
                    _id:op._id,
                    createdDate:op.createdDate,
                    amountNonNGM:op.convertedAmt,
                    amount:op.convertedAmtWithNgm,
                    netGrossMargin:op.netGrossMargin,
                    relatasStage:op.relatasStage,
                    forDate:op.closeDate,
                    weekYear:moment(op.closeDate).week()+""+moment(op.closeDate).year()
                })
            }

        }

        if( new Date(op.createdDate)>=new Date(dateMin)){
            oppsCreated.push({
                _id:op._id,
                amountNonNGM:op.convertedAmt,
                amount:op.convertedAmtWithNgm,
                netGrossMargin:op.netGrossMargin,
                createdDate:op.createdDate,
                relatasStage:op.relatasStage,
                forDate:op.createdDate,
                weekYear:moment(op.createdDate).week()+""+moment(op.createdDate).year()
            })
        }
    });

    var dataCreated = _
        .chain(oppsCreated)
        .groupBy('weekYear')
        .map(function(value, key) {

            var sumVal = _.sum(value,"amount");
            return {
                forDate:value[0].forDate,
                weekYear: key,
                date:value[0].forDate,
                count: value.length,
                amount:sumVal?sumVal:0
            }
        })
        .value();

    var dataWon = _
        .chain(oppsWon)
        .groupBy('weekYear')
        .map(function(value, key) {
            var sumVal = _.sum(value,"amount");
            return {
                forDate:value[0].forDate,
                weekYear: key,
                date:value[0].forDate,
                count: value.length,
                amount:sumVal?sumVal:0
            }
        })
        .value();

    return {
        created:dataCreated,
        oppsWon:dataWon
    };

}

function groupOppsWonLostByMonth(opps,dateMin,dateMax,primaryCurrency,currenciesObj){

    var oppsWon = [],
        oppsCreated = [];
    _.each(opps,function (op) {

        if(op.f0_ && !op.amount){
            op.createdDate = op.createdDate.value;
            op.closeDate = op.closeDate.value;
            op.amount = op.f0_;
        }

        if(!op.createdDate){
            op.createdDate = op._id.getTimestamp()
        }

        op.amountWithNgm = op.amount

        if(op.netGrossMargin || op.netGrossMargin == 0){
            var amountWithNGM = op.netGrossMargin*op.amount
            if(amountWithNGM){
                amountWithNGM = amountWithNGM/100
            }
            op.amountWithNgm = amountWithNGM;
        }

        convertAmount(op,primaryCurrency,currenciesObj);

        if(op.relatasStage && (op.relatasStage.toLowerCase() == "closed won" || op.relatasStage.toLowerCase() == "close won")){

            if(new Date(op.closeDate)>= new Date() || new Date(op.closeDate)<= new Date(dateMin)){
            } else {

                oppsWon.push({
                    _id:op._id,
                    createdDate:op.createdDate,
                    amountNonNGM:op.convertedAmt,
                    amount:op.convertedAmtWithNgm,
                    netGrossMargin:op.netGrossMargin,
                    relatasStage:op.relatasStage,
                    forDate:op.closeDate,
                    monthYear:moment(op.closeDate).month()+""+moment(op.closeDate).year()
                })
            }

        }

        if( new Date(op.createdDate)>=new Date(dateMin)){
            oppsCreated.push({
                _id:op._id,
                amountNonNGM:op.convertedAmt,
                amount:op.convertedAmtWithNgm,
                netGrossMargin:op.netGrossMargin,
                createdDate:op.createdDate,
                relatasStage:op.relatasStage,
                forDate:op.createdDate,
                monthYear:moment(op.createdDate).month()+""+moment(op.createdDate).year()
            })
        }
    });

    var dataCreated = _
        .chain(oppsCreated)
        .groupBy('monthYear')
        .map(function(value, key) {

            var sumVal = _.sum(value,"amount");
            return {
                forDate:value[0].forDate,
                monthYear: key,
                date:value[0].forDate,
                count: value.length,
                amount:sumVal?sumVal:0
            }
        })
        .value();

    var dataWon = _
        .chain(oppsWon)
        .groupBy('monthYear')
        .map(function(value, key) {
            var sumVal = _.sum(value,"amount");
            return {
                forDate:value[0].forDate,
                monthYear: key,
                date:value[0].forDate,
                count: value.length,
                amount:sumVal?sumVal:0
            }
        })
        .value();

    var months = getMonthsBetweenDates(dateMin,dateMax)
    var monthsYears = [],
        monthsYearsObj = {};

    _.each(months,function (el) {
        monthsYears.push(el.monthYear);
        monthsYearsObj[el.monthYear] = el
    })

    var nonExistingOppCreated = _.difference(monthsYears,_.pluck(dataCreated,"monthYear"))
    var nonExistingOppWon = _.difference(monthsYears,_.pluck(dataWon,"monthYear"))

    _.each(nonExistingOppCreated,function (el) {
        dataCreated.push({
            forDate:monthsYearsObj[el].date,
            monthYear: el,
            date:monthsYearsObj[el].date,
            count: 0,
            amount:0
        })
    })
    _.each(nonExistingOppWon,function (el) {
        dataWon.push({
            forDate:monthsYearsObj[el].date,
            monthYear: el,
            date:monthsYearsObj[el].date,
            count: 0,
            amount:0
        })
    })

    return {
        created:dataCreated,
        oppsWon:dataWon
    };

}

function getQtrYearString(allQuarters,startOfQuarter,timezone,fyStartDt){
    var qtrString = null;
    for( var key in allQuarters){
        if(new Date(startOfQuarter)>=new Date(allQuarters[key].start) && new Date(startOfQuarter)<=new Date(allQuarters[key].end)){
            qtrString = key
        }
    }

    var prevQtrs = buildQuarters(moment(fyStartDt).subtract(1,"year"),timezone);
    var nextQtrs = buildQuarters(moment(fyStartDt).add(1,"year"),timezone);

    //If not found in the current quarter, find in the next qtr
    if(!qtrString){
        for( var key in nextQtrs){
            if(new Date(startOfQuarter)>=new Date(nextQtrs[key].start) && new Date(startOfQuarter)<=new Date(nextQtrs[key].end)){
                qtrString = key
            }
        }
    }

    //If not found in the current and next quarter, find in the prev qtr
    if(!qtrString){
        for( var key in prevQtrs){
            if(new Date(startOfQuarter)>=new Date(prevQtrs[key].start) && new Date(startOfQuarter)<=new Date(prevQtrs[key].end)){
                qtrString = key
            }
        }
    }

    return qtrString;
}

function buildQuarters(fyStartDate,timezone) {

    var quarters = [],
        twelve_months_for_loop = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November"],
        months = [],
        qtrYears = [],
        qtrObj = {};

    months.push(fyStartDate);

    _.each(twelve_months_for_loop,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    quarters.push({start:momentTz(months[0]).tz(timezone).startOf('month').format(),end:momentTz(months[2]).tz(timezone).endOf('month').format(),qtr:"quarter1"})
    quarters.push({start:momentTz(months[3]).tz(timezone).startOf('month').format(),end:momentTz(months[5]).tz(timezone).endOf('month').format(),qtr:"quarter2"})
    quarters.push({start:momentTz(months[6]).tz(timezone).startOf('month').format(),end:momentTz(months[8]).tz(timezone).endOf('month').format(),qtr:"quarter3"})
    quarters.push({start:momentTz(months[9]).tz(timezone).startOf('month').format(),end:momentTz(months[11]).tz(timezone).endOf('month').format(),qtr:"quarter4"})

    _.each(quarters,function (el) {
        qtrObj[el.qtr] = el
    });

    return qtrObj;
}

function groupOppsWonLostByQtr(opps,allQuarters,dateMin,dateMax,timezone,fyStartDt,primaryCurrency,currenciesObj){

    var oppsWon = [],
        oppsCreated = [];
    _.each(opps,function (op) {

        if(!op.createdDate){
            op.createdDate = op._id.getTimestamp()
        }

        if(op.netGrossMargin || op.netGrossMargin == 0){
            var amountWithNGM = op.netGrossMargin*op.amount
            if(amountWithNGM){
                amountWithNGM = amountWithNGM/100
            }
            op.amountWithNgm = amountWithNGM;
        }

        convertAmount(op,primaryCurrency,currenciesObj);

        if(op.relatasStage && (op.relatasStage.toLowerCase() == "closed won" || op.relatasStage.toLowerCase() == "close won")){

            if(new Date(op.closeDate)>= new Date() || new Date(op.closeDate)<= new Date(dateMin)){
            } else {
                if(getQtrYearString(allQuarters,op.closeDate,timezone,fyStartDt)){
                    oppsWon.push({
                        _id:op._id,
                        createdDate:op.createdDate,
                        amountNonNGM:op.convertedAmt,
                        amount:op.convertedAmtWithNgm,
                        netGrossMargin:op.netGrossMargin,
                        relatasStage:op.relatasStage,
                        forDate:op.closeDate,
                        quarterYear:getQtrYearString(allQuarters,op.closeDate,timezone,fyStartDt)+""+moment(op.closeDate).year()
                    })
                }
            }

        }

        if( new Date(op.createdDate)>=new Date(dateMin)){

            if(getQtrYearString(allQuarters,op.createdDate,timezone,fyStartDt)){
                oppsCreated.push({
                    _id:op._id,
                    amountNonNGM:op.convertedAmt,
                    amount:op.convertedAmtWithNgm,
                    netGrossMargin:op.netGrossMargin,
                    createdDate:op.createdDate,
                    relatasStage:op.relatasStage,
                    forDate:op.createdDate,
                    quarterYear:getQtrYearString(allQuarters,op.createdDate,timezone,fyStartDt)+""+moment(op.createdDate).year()
                })
            }
        }
    });

    var dataCreated = _
        .chain(oppsCreated)
        .groupBy('quarterYear')
        .map(function(value, key) {

            var sumVal = _.sum(value,"amount");
            return {
                forDate:value[0].forDate,
                quarterYear: key,
                date:value[0].forDate,
                count: value.length,
                amount:sumVal?sumVal:0
            }
        })
        .value();

    var dataWon = _
        .chain(oppsWon)
        .groupBy('quarterYear')
        .map(function(value, key) {
            var sumVal = _.sum(value,"amount");
            return {
                forDate:value[0].forDate,
                quarterYear: key,
                date:value[0].forDate,
                count: value.length,
                amount:sumVal?sumVal:0
            }
        })
        .value();

    var qtrs = [];

    for( var key in allQuarters){
        if(key != "currentQuarter"){
            qtrs.push({
                qtrYear:key+""+moment(momentTz(allQuarters[key].start).tz(timezone)).year(),
                date:new Date(momentTz(allQuarters[key].start).tz(timezone))
            })
        }
    }

    var prevQtrs = getPreviousQuarters(moment(fyStartDt).subtract(1,"year"),timezone);

    qtrs = prevQtrs.concat(qtrs);

    qtrs = qtrs.filter(function (el) {
        return new Date(el.date)<new Date()
    })

    qtrs.sort(function (o1, o2) {
        return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
    });

    qtrs = _.takeRight(qtrs, 4);

    var qtrsYear = [],
        qtrsYearsObj = {};

    _.each(qtrs,function (el) {
        qtrsYear.push(el.qtrYear);
        qtrsYearsObj[el.qtrYear] = el
    });

    var nonExistingOppCreated = _.difference(qtrsYear,_.pluck(dataCreated,"quarterYear"))
    var nonExistingOppWon = _.difference(qtrsYear,_.pluck(dataWon,"quarterYear"))

    _.each(nonExistingOppCreated,function (el) {
        dataCreated.push({
            forDate:qtrsYearsObj[el].date,
            quarterYear: el,
            date:qtrsYearsObj[el].date,
            count: 0,
            amount:0
        })
    })
    _.each(nonExistingOppWon,function (el) {
        dataWon.push({
            forDate:qtrsYearsObj[el].date,
            quarterYear: el,
            date:qtrsYearsObj[el].date,
            count: 0,
            amount:0
        })
    });


    var fyStart = new Date(fyStartDt),
        fyEnd = new Date(moment(fyStartDt).add(1,"year"));

    var creations = [],
        won = [];

    _.each(dataCreated,function (el) {
       if(new Date(el.forDate)>= fyStart && new Date(el.forDate)<= fyEnd){
           creations.push(el)
       }
    });

    _.each(dataWon,function (el) {
        if(new Date(el.forDate)>= fyStart && new Date(el.forDate)<= fyEnd){
            won.push(el)
        }
    });

    return {
        created:creations,
        oppsWon:won
    };

}

function getPreviousQuarters(fyStartDate,timezone) {

    var quarters = [],
        twelve_months_for_loop = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November"],
        months = [],
        qtrYears = [];

    months.push(fyStartDate);

    _.each(twelve_months_for_loop,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    quarters.push({start:momentTz(months[0]).tz(timezone).startOf('month').format(),end:momentTz(months[2]).tz(timezone).endOf('month').format(),qtr:"quarter1"})
    quarters.push({start:momentTz(months[3]).tz(timezone).startOf('month').format(),end:momentTz(months[5]).tz(timezone).endOf('month').format(),qtr:"quarter2"})
    quarters.push({start:momentTz(months[6]).tz(timezone).startOf('month').format(),end:momentTz(months[8]).tz(timezone).endOf('month').format(),qtr:"quarter3"})
    quarters.push({start:momentTz(months[9]).tz(timezone).startOf('month').format(),end:momentTz(months[11]).tz(timezone).endOf('month').format(),qtr:"quarter4"})

    _.each(quarters,function (el) {
        qtrYears.push({
            qtrYear:el.qtr+""+moment(el.start).year(),
            date:new Date(el.start)
        })
    });

    return qtrYears;
}

OpportunitiesManagement.prototype.opportunitiesPastCloseDateByAccount = function (userIds,contacts,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["userId"] = el;
        obj["contactEmailId"] = {$in:contacts}
        // obj["$or"] = [
        //     {"contactEmailId":new RegExp('.'+account+'.', "i")},
        //     {"contactEmailId":new RegExp('@'+account, "i")}
        // ]

        obj["closeDate"] = {$lte: new Date()}

        obj["stageName"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
        obj["isClosed"] = {$ne:true}
        obj["relatasStage"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}

        return obj;
    })};

    opportunitiesCollection.aggregate([
        {
            $match:query
        }],function(error, result){

        if(error){
            loggerError.info('Error in opportunitiesPastCloseDateByAccount():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
}

OpportunitiesManagement.prototype.deleteOpp = function (userId,oppId,callback) {

    opportunitiesCollection.find({_id:oppId}).lean().exec(function (err,opp) {
        if(!err && opp && opp.length>0){
            OppTrashCollection.collection.insert(opp,function (iErr,result) {
                if(!iErr && result){
                    opportunitiesCollection.remove({_id:oppId},function (rErr,rResult) {
                        callback(rErr,rResult)
                    });
                } else {
                    callback(iErr,result)
                }
            })
        } else {
            callback(err,false)
        }
    });
}

OpportunitiesManagement.prototype.deleteMultipleOpps = function (companyId,oppIds,deletedByEmailId,callback) {

    opportunitiesCollection.find({companyId:companyId,opportunityId:{$in:oppIds}}).lean().exec(function (err,opp) {
        if(!err && opp && opp.length>0){
            opp.forEach(function (op) {
                delete op._id;
                op.deletedBy = deletedByEmailId;
            });

            OppTrashCollection.collection.insert(opp,function (iErr,result) {
                if(!iErr && result){
                    opportunitiesCollection.remove({companyId:companyId,opportunityId:{$in:oppIds}},function (rErr,rResult) {
                        secondaryHierarchyObj.removeAccessForAllUsersMultiOpps(companyId,oppIds,function () {

                        });
                        callback(rErr,rResult)
                    });
                } else {
                    callback(iErr,result)
                }
            })
        } else {
            callback(err,false)
        }
    });
}

function updateLastSyncDate(userId){

    myUserCollection.update({_id:userId},{$set:{"lastOppInteractionsSyncDate":new Date()}},{upsert:true}, function(error,result){
        if(error){
            loggerError.info('Error in updateLastSyncDate():opportunitiesManagement ',error)
        }
    })
}

function updateOppWithInteractions(userGroup,oppId,usersAndInteractions,callback) {

    if(userGroup.length>0){
        var bulk = opportunitiesCollection.collection.initializeUnorderedBulkOp();
        bulk.find({ _id: userGroup[0].oppId}).update({ $set: {usersAndInteractions:usersAndInteractions} });
        _.each(userGroup,function (el) {

            bulk.find({ _id: el.oppId,"contactEmailId": el.emailId}).update({ $set: {interactionCount:el.count} });
            bulk.find({ _id: el.oppId,"partners.emailId": el.emailId}).update({ $set: {"partners.$.interactionCount":el.count} });
            bulk.find({ _id: el.oppId,"decisionMakers.emailId": el.emailId}).update({ $set: {"decisionMakers.$.interactionCount":el.count} });
            bulk.find({ _id: el.oppId,"influencers.emailId": el.emailId}).update({ $set: {"influencers.$.interactionCount":el.count} });
        });

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updateOppWithInteractions():opportunitiesManagement Bulk', err)
            } else {
                result = result.toJSON();
                logger.info('updateOppWithInteractions() opportunitiesManagement ', result);
            }

            if (callback) { callback(err, result) }
        });
    }
}

OpportunitiesManagement.prototype.updateMasterData = function(companyId,data,callback) {

    var findQuery = {
        $or:[{companyId:companyId},
            {companyId:String(companyId)}],
        "masterData.type": data.acc_identifier_rel
    }

    opportunitiesCollection.find(findQuery).lean().exec(function (err,opps) {
        var bulk = opportunitiesCollection.collection.initializeUnorderedBulkOp();

        if(!err && opps && opps.length>0){
            _.each(opps,function (op) {

                if(op.masterData && op.masterData.length>0){
                    _.each(op.masterData,function (md) {
                        if(md.type === data.acc_identifier_rel){
                            _.each(md.data,function (el) {
                                if(String(el._id) == String(data._id)){
                                    for(var key in data){
                                        if(key !== "acc_identifier_rel"){
                                            el[key] = data[key]
                                        }
                                    }
                                }
                            })
                        }
                    });

                    bulk.find({
                        opportunityId: op.opportunityId
                    }).update({$set: {masterData: op.masterData}});

                }
            });

            bulk.execute(function(err1, result) {
                if (err1) {
                    logger.info('Error in updateMasterData():opportunitiesManagement Bulk', err1)
                }

                if (callback) { callback(err, result) }
            });
        }

    });

}

function updateOpenOppWithInteractions(userGroup,callback) {

    if(userGroup.length>0){
        var bulk = opportunitiesCollection.collection.initializeUnorderedBulkOp();

        _.each(userGroup,function (el) {

            _.each(el.contacts,function (co) {

                bulk.find({ _id: el.oppId,"contactEmailId": co.emailId}).update({ $set: {interactionsInProgress:co.counter} });
                bulk.find({ _id: el.oppId,"partners.emailId": co.emailId}).update({ $set: {"partners.$.interactionsInProgress":co.counter} });
                bulk.find({ _id: el.oppId,"decisionMakers.emailId": co.emailId}).update({ $set: {"decisionMakers.$.interactionsInProgress":co.counter} });
                bulk.find({ _id: el.oppId,"influencers.emailId": co.emailId}).update({ $set: {"influencers.$.interactionsInProgress":co.counter} });
            })
        });

        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updateOpenOppWithInteractions():opportunitiesManagement Bulk', err)
            } else {
                result = result.toJSON();
                logger.info('updateOpenOppWithInteractions() opportunitiesManagement ', result);
            }

            if (callback) { callback(err, result) }
        });
    }
}

function getInteractionCount(userIds,contacts,from,to,callback){

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el
        obj["userId"] = {$ne:el}
        obj["emailId"] = {$in:contacts}

        obj["interactionDate"] = {
            $gte: new Date(from),
            $lte: new Date(to)
        }
        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
          $project:{
              emailId:"$emailId",
              day:{$dayOfMonth:"$interactionDate"},
              month:{$month:"$interactionDate"},
              year:{$year:"$interactionDate"}
          }
        },
        {
            $group: {
                _id: {
                    emailId:"$emailId",
                    day:"$day",
                    month:"$month",
                    year:"$year"
                },
                count: {$sum: 1}
            }
        }
    ]).exec(function(err, data){
        callback(err,data)
    });
}

function insertAllOpportunities(nonExistsOpp, callback){
    var opportunitiesCollection = require('../databaseSchema/userManagementSchema').opportunities;
    opportunitiesCollection.collection.insert(nonExistsOpp, function(error, result){
        if(error){
            loggerError.info('Error in insertAllOpportunities():opportunitiesManagement ',error)
            callback(false)
        }
        else{
            callback(true);
        }

    })
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined || data == 'null') {
        return false;
    }
    else{
        return true;
    }
}

function netGrossAmount() {

    return {
        $cond: [ { $ne: [ "$netGrossMargin", 0] } ,
            {$multiply:[{ "$divide": [ "$netGrossMargin", 100 ] },"$amount"]},
            "$amount" ] }
}

function netGrossAmountWithZero() {

    return {
        $cond: [ { $ne: [ "$netGrossMargin", ""] } ,
            {$multiply:[{ "$divide": [ "$netGrossMargin", 100 ] },"$amount"]},
            "$amount" ] }
}

function accessControlSettings(regionAccess,productAccess,verticalAccess,companyId,buAccess) {

    var accessControlQuery = [];

    if(regionAccess && regionAccess.length>0 && companyId){
        accessControlQuery.push({"geoLocation.zone":{$in:regionAccess}})
    } else {
        accessControlQuery.push({"geoLocation.zone":"no_access"})
    }

    if(productAccess && productAccess.length>0 && companyId){
        accessControlQuery.push({"productType":{$in:productAccess}})
    } else {
        accessControlQuery.push({"productType":"no_access"})
    }

    if(verticalAccess && verticalAccess.length>0 && companyId){
        accessControlQuery.push({vertical:{$in:verticalAccess}})
    } else {
        accessControlQuery.push({vertical:"no_access"})
    }

    if(buAccess && buAccess.length>0 && companyId){
        accessControlQuery.push({businessUnit:{$in:buAccess}})
    } else {
        accessControlQuery.push({businessUnit:"no_access"})
    }

    accessControlQuery.push({companyId:companyId})

    return accessControlQuery;

}

function portfolioAccess(regionAccess,productAccess,verticalAccess,companyId,buAccess) {

    var accessControlQuery = [];

    if(regionAccess && regionAccess.length>0 && companyId){
        accessControlQuery.push({"geoLocation.zone":{$in:regionAccess}})
    }

    if(productAccess && productAccess.length>0 && companyId){
        accessControlQuery.push({"productType":{$in:productAccess}})
    }

    if(verticalAccess && verticalAccess.length>0 && companyId){
        accessControlQuery.push({vertical:{$in:verticalAccess}})
    }

    if(buAccess && buAccess.length>0 && companyId){
        accessControlQuery.push({businessUnit:{$in:buAccess}})
    }

    return accessControlQuery;

}

function getMonthsBetweenDates(from,to) {
    var startDate = moment(from);
    var endDate = moment(to);

    var result = [];

    if (endDate.isBefore(startDate)) {
        throw "End date must be greater than start date."
    }

    var currentDate = startDate.clone();

    while (currentDate.isBefore(endDate)) {
        result.push({
            date:new Date(currentDate),
            monthYear:moment(currentDate).month()+""+moment(currentDate).year()
        });
        currentDate.add(1, 'month');
    }

    return result;
}

OpportunitiesManagement.prototype.getMetaDataForMonth = function (companyId,userEmailId,startMonthYear,endMonthYear,qStart,qEnd,primaryCurrency,currenciesObj,callback) {

    var findQuery = {
        companyId:companyId,
        userEmailId:{$in:userEmailId},
        $or:[
                {month:startMonthYear.month,year:startMonthYear.year},
                {month:endMonthYear.month,year:endMonthYear.year}
            ]
    }

    if(new Date(moment().month(endMonthYear.month).year(endMonthYear.year))>new Date()){

        findQuery = {
            companyId:companyId,
            userEmailId:{$in:userEmailId},
            $or:[{month:startMonthYear.month,year:startMonthYear.year}]
        }

        getOppsAsOfToday(companyId,userEmailId,endMonthYear,primaryCurrency,currenciesObj,qStart,qEnd,function (errO,asOfToday) {

            OppMetaDataCollection.find(findQuery,function (err,asOfMonthStart) {
                if(errO){
                    loggerError.info('Error in getMetaDataForMonth():opportunitiesManagement ',err );
                }
                if(!errO && asOfToday){
                    callback(err,[flattenOppMeta(asOfMonthStart),asOfToday]);
                } else {
                    callback(err,[{},{}]);
                }
            });
        });

    } else {
        OppMetaDataCollection.find(findQuery,function (err,result) {
            if(err){
                loggerError.info('Error in getMetaDataForMonth():OppMetaDataCollection:opportunitiesManagement ',err );
            }
            callback(err,result);
        });
    }

};

function flattenOppMeta(asOfMonthStart){
    var flattenedData = {};

    if(asOfMonthStart && asOfMonthStart[0]){

        if(asOfMonthStart && asOfMonthStart.length === 1){
            flattenedData = asOfMonthStart[0];
        } else {
            flattenedData = {
                companyId:asOfMonthStart[0].companyId,
                month:asOfMonthStart[0].month,
                year:asOfMonthStart[0].year,
                userEmailId:"allUsers",
                data:[]
            }

            var obj = {};
            _.each(asOfMonthStart,function (el) {

                _.each(el.data,function (da) {

                    if(da.stageName){
                        if(!obj[da.stageName]){
                            obj[da.stageName] = {
                                oppIds:[],
                                regions:[],
                                reasons:[],
                                products:[],
                                solutions:[],
                                verticals:[],
                                amount:0,
                                amountWithNgm:0,
                                numberOfOpps:0,
                                stageName:da.stageName
                            };
                        }
                        obj[da.stageName].oppIds = obj[da.stageName].oppIds.concat(da.oppIds)
                        obj[da.stageName].regions = obj[da.stageName].regions.concat(da.regions)
                        obj[da.stageName].reasons = obj[da.stageName].reasons.concat(da.reasons)
                        obj[da.stageName].products = obj[da.stageName].products.concat(da.products)
                        obj[da.stageName].solutions = obj[da.stageName].solutions.concat(da.solutions)
                        obj[da.stageName].verticals = obj[da.stageName].verticals.concat(da.verticals)
                        obj[da.stageName].amount = obj[da.stageName].amount+da.amount
                        obj[da.stageName].amountWithNgm = obj[da.stageName].amountWithNgm+da.amountWithNgm
                        obj[da.stageName].numberOfOpps = obj[da.stageName].numberOfOpps+da.numberOfOpps
                    }
                });
            });

            for(var key in obj){
                flattenedData.data.push(obj[key])
            }
        }
    }

    return flattenedData;
}

function getOppsAsOfToday(companyId,userEmailId,endMonthYear,primaryCurrency,currenciesObj,qStart,qEnd,callback){

    var query = {
        companyId:companyId,
        userEmailId:{$in:userEmailId}
    }

    opportunitiesCollection.aggregate([
        {
            $match:query
        },
        {
            $group: {
                _id:"$relatasStage",
                opportunities: {
                    $push: "$$ROOT"
                }
            }
        }
    ]).exec(function (err,opps) {

        if(!err && opps){

            var forUser = userEmailId.length == 1?userEmailId[0]:"allUsers"
            var obj = buildOppMetaDataObj(opps, forUser, companyId, endMonthYear.month, endMonthYear.year,primaryCurrency,currenciesObj,qStart,qEnd);
            callback(err,obj)
        } else {
            callback(err,{})
        }

    });
}

OpportunitiesManagement.prototype.updateMetaData = updateMetaData;

OpportunitiesManagement.prototype.getOppForSingleByDate = function (companyId,listOfUsers,dateMin,dateMax,selectedPortfolio,callback) {

    var findQuery = {
        companyId:companyId,
        closeDate:{$gte:new Date(dateMin),$lte:new Date(dateMax)},
        $or:[{
            userEmailId:{$in:listOfUsers}
        }, {
            'usersWithAccess.emailId':{$in:listOfUsers}
        }]
    };

    var portfolioAcc = null;
    var portfolioHead = null;

    if(selectedPortfolio){

        var regionAccess = [],
            productAccess = [],
            businessUnits = [],
            verticalAccess = [];

        if(selectedPortfolio.type == "Products"){
            productAccess.push(selectedPortfolio.name);

            if(selectedPortfolio.isHead){
                portfolioHead = [{productType:selectedPortfolio.name}]
            }
        }

        if(selectedPortfolio.type == "Verticals"){
            verticalAccess.push(selectedPortfolio.name);
            if(selectedPortfolio.isHead){
                portfolioHead = [{vertical:selectedPortfolio.name}]
            }
        }

        if(selectedPortfolio.type == "Business Units"){
            businessUnits.push(selectedPortfolio.name)
            if(selectedPortfolio.isHead){
                portfolioHead = [{businessUnit:selectedPortfolio.name}]
            }
        }

        if(selectedPortfolio.type == "Regions"){
            regionAccess.push(selectedPortfolio.name)
            if(selectedPortfolio.isHead){
                portfolioHead = [{"geoLocation.zone":selectedPortfolio.name}]
            }
        }

        _.each(selectedPortfolio.accessLevel,function(ac){

            if(ac.type == "Products"){
                productAccess = ac.values;
            }

            if(ac.type == "Verticals"){
                verticalAccess = ac.values;
            }

            if(ac.type == "Business Units"){
                businessUnits = ac.values;
            }

            if(ac.type == "Regions"){
                regionAccess = ac.values;
            }

        });

        portfolioAcc = accessControlSettings(regionAccess,
            productAccess,
            verticalAccess,
            companyId,
            businessUnits)

        if(portfolioHead){
            findQuery = {
                companyId:companyId,
                closeDate:{$gte:new Date(dateMin),$lte:new Date(dateMax)},
                $and:portfolioHead
            }
        } else {
            findQuery = {
                companyId:companyId,
                closeDate:{$gte:new Date(dateMin),$lte:new Date(dateMax)},
                $or:[
                    {$and: portfolioAcc}
                ]
            }
        }
    }

    var projection = {
        influencers:0,
        decisionMakers:0,
        notes:0,
        lastStageUpdated:0,
        BANT:0,
        isClosed:0,
        isWon:0,
        salesforceContactId:0,
        mobileNumber:0
    }

    opportunitiesCollection.aggregate([
        {
            $match:findQuery
        },
        {
            $project:projection
        }
    ]).exec(function (err,opps) {

        if(!err){
            callback(err,opps)
        } else {
            callback(err,[])
        }
    });
};

OpportunitiesManagement.prototype.getOppForSingleByDateFromCache = function (companyId,listOfUsers,dateMin,dateMax,selectedPortfolios,fyStart,fyEnd,forRank,callback) {

    var findQuery = {
        companyId:companyId,
        // closeDate:{$gte:new Date(dateMin),$lte:new Date(dateMax)},
        $or:[{
            userEmailId:{$in:listOfUsers}
        }, {
            'usersWithAccess.emailId':{$in:listOfUsers}
        }]
    };

    var prevQtrQuery = {
        companyId: companyId,
        closeDate: {
            $gte: new Date(moment(dateMin).subtract(3, "month")),
            $lte: new Date(moment(dateMax).subtract(3, "month"))
        },
        $or:[{
            userEmailId:{$in:listOfUsers}
        }, {
            'usersWithAccess.emailId':{$in:listOfUsers}
        }]
    }

    if(new Date(dateMin)< new Date(fyStart)){
        fyStart = moment(fyStart).subtract(1,"year");
        fyEnd = moment(fyEnd).subtract(1,"year");
    }

    var thisFyQuery = {
        companyId:companyId,
        closeDate:{$gte:new Date(fyStart),$lte:new Date(fyEnd)},
        $or:[{
            userEmailId:{$in:listOfUsers}
        }, {
            'usersWithAccess.emailId':{$in:listOfUsers}
        }]
    }

    var portfolioAcc = null;
    var portfolioHead = null;
    var selectedPortfolio = null;
    var allPortfolios = [];

    if(selectedPortfolios){

        var portfolioQuery = selectedPortfolios.map(function(selectedPortfolio){

            if(selectedPortfolio){

                var regionAccess = [],
                    productAccess = [],
                    businessUnits = [],
                    verticalAccess = [];

                if(selectedPortfolio.type == "Products"){
                    productAccess.push(selectedPortfolio.name);

                    allPortfolios.push({type:selectedPortfolio.type,name:selectedPortfolio.name})

                    if(selectedPortfolio.isHead){
                        portfolioHead = [{productType:selectedPortfolio.name}]
                    }
                }

                if(selectedPortfolio.type == "Verticals"){
                    verticalAccess.push(selectedPortfolio.name);
                    if(selectedPortfolio.isHead){
                        portfolioHead = [{vertical:selectedPortfolio.name}]
                    }
                }

                if(selectedPortfolio.type == "Business Units"){
                    businessUnits.push(selectedPortfolio.name)
                    if(selectedPortfolio.isHead){
                        portfolioHead = [{businessUnit:selectedPortfolio.name}]
                    }
                }

                if(selectedPortfolio.type == "Regions"){
                    regionAccess.push(selectedPortfolio.name)
                    if(selectedPortfolio.isHead){
                        portfolioHead = [{"geoLocation.zone":selectedPortfolio.name}]
                    }
                }

                _.each(selectedPortfolio.accessLevel,function(ac){

                    if(ac.type == "Products"){
                        productAccess = ac.values;
                    }

                    if(ac.type == "Verticals"){
                        verticalAccess = ac.values;
                    }

                    if(ac.type == "Business Units"){
                        businessUnits = ac.values;
                    }

                    if(ac.type == "Regions"){
                        regionAccess = ac.values;
                    }

                });

                portfolioAcc = accessControlSettings(regionAccess,
                    productAccess,
                    verticalAccess,
                    companyId,
                    businessUnits);

                if(portfolioHead){
                    return {$and:portfolioHead};
                } else {
                    return {$and:portfolioAcc};
                }
            }
        });

        findQuery = {
            companyId:companyId,
            // closeDate:{$gte:new Date(dateMin),$lte:new Date(dateMax)},
            $or:portfolioQuery
        }

        prevQtrQuery = {
            companyId: companyId,
            closeDate: {
                $gte: new Date(moment(dateMin).subtract(3, "month")),
                $lte: new Date(moment(dateMax).subtract(3, "month"))
            },
            $or: portfolioQuery
        }

        thisFyQuery = {
            companyId:companyId,
            closeDate:{$gte:new Date(fyStart),$lte:new Date(fyEnd)},
            $or:portfolioQuery
        }
    }

    var data = {
        allPortfolios:allPortfolios
    };

    if(forRank){
        oppByStartAndEnd(findQuery,function(err,opps_thisQtr){
            data.opps_thisQtr = opps_thisQtr;
            callback(err,data);
        })
    } else {

        oppByStartAndEnd(findQuery,function(err,opps_thisQtr){
            oppByStartAndEnd(prevQtrQuery,function(err,opps_prevQtr){
                oppByStartAndEnd(thisFyQuery,function(err,opps_thisFy){

                    data.opps_thisQtr = opps_thisQtr;
                    data.opps_prevQtr = opps_prevQtr;
                    data.opps_thisFy = opps_thisFy;
                    callback(err,data);
                })
            })
        })
    }
};

function oppByStartAndEnd(findQuery,callback){

    var projection = {
        influencers:0,
        decisionMakers:0,
        notes:0,
        lastStageUpdated:0,
        BANT:0,
        isClosed:0,
        isWon:0,
        salesforceContactId:0,
        mobileNumber:0
    }

    // cacheOppBq.aggregate([
    opportunitiesCollection.aggregate([
        {
            $match:findQuery
        },
        {
            $project:projection
        }
    ]).exec(function (err,opps) {
        if(!err){
            callback(err,opps)
        } else {
            callback(err,[])
        }
    });}

OpportunitiesManagement.prototype.getOppByFilters = function (companyId,filters,qStart,qEnd,selectedPortfolio,userEmailId,callback) {

    var query = buildQuery(filters,selectedPortfolio,companyId);
    var portfolioAcc = null;
    var portfolioHead = null;

    var findQuery = {
        companyId:companyId,
        $and:query
    }

    if(query && query.length === 0){
        var findQuery = {
            companyId:companyId
        }
    }

    if(selectedPortfolio){

        var regionAccess = [],
            productAccess = [],
            businessUnits = [],
            verticalAccess = [];

        if(selectedPortfolio.type == "Products"){
            productAccess.push(selectedPortfolio.name);

            if(selectedPortfolio.isHead){
                portfolioHead = [{productType:selectedPortfolio.name}]
            }
        }

        if(selectedPortfolio.type == "Verticals"){
            verticalAccess.push(selectedPortfolio.name);
            if(selectedPortfolio.isHead){
                portfolioHead = [{vertical:selectedPortfolio.name}]
            }
        }

        if(selectedPortfolio.type == "Business Units"){
            businessUnits.push(selectedPortfolio.name)
            if(selectedPortfolio.isHead){
                portfolioHead = [{businessUnit:selectedPortfolio.name}]
            }
        }

        if(selectedPortfolio.type == "Regions"){
            regionAccess.push(selectedPortfolio.name)
            if(selectedPortfolio.isHead){
                portfolioHead = [{"geoLocation.zone":selectedPortfolio.name}]
            }
        }

        _.each(selectedPortfolio.accessLevel,function(ac){

            if(ac.type == "Products"){
                productAccess = ac.values;
            }

            if(ac.type == "Verticals"){
                verticalAccess = ac.values;
            }

            if(ac.type == "Business Units"){
                businessUnits = ac.values;
            }

            if(ac.type == "Regions"){
                regionAccess = ac.values;
            }

        });

        portfolioAcc = accessControlSettings(regionAccess,
            productAccess,
            verticalAccess,
            companyId,
            businessUnits);

        if(portfolioHead){
            findQuery = {
                companyId:companyId,
                $and:[{$and:portfolioHead}]
            }

            if(query && query.length>0){
                findQuery.$and.push({$and:query})
            }

        } else {
            findQuery = {
                companyId:companyId,
                $and:[{$and:portfolioAcc}]
            }

            if(query && query.length>0){
                findQuery.$and.push({$and:query})
            }
        }
    }

    var projection = {
        influencers:0,
        decisionMakers:0,
        notes:0,
        lastStageUpdated:0,
        BANT:0,
        isClosed:0,
        isWon:0,
        salesforceContactId:0,
        mobileNumber:0
    }

    opportunitiesCollection.aggregate([
        {
            $match:findQuery
        },
        {
            $project:projection
        }
    ]).exec(function (err,opps) {
        if(!err){
            callback(err,opps)
        } else {
            callback(err,[])
        }
    });
};

function buildQuery(filtersApplied,selectedPortfolio,companyId){

    var groupedFilter = _
        .chain(filtersApplied)
        .groupBy('type')
        .map(function(value, key) {
            var obj = {};

            if(key === "geoLocation"){
                obj[key+".zone"] = {$in:_.pluck(value,"name")}
            } else if(key === "account"){
                obj["contactEmailId"] = {$in:_.map(value,function (el) {
                    if(el.name !== "Others"){return new RegExp(el.name, "i");}
                    })
                }
            } else if(key === "closeDate"){
                obj["closeDate"] = {
                    $gte: new Date(value[0].start),
                    $lte: new Date(value[0].end)
                }

            } else if(key === "region"){
                obj["geoLocation.zone"] = {$in:_.pluck(value,"name")}
            } else {

                if(key === "userEmailId" && !selectedPortfolio){
                    obj.$or = [{"usersWithAccess.emailId": {$in:_.pluck(value,"name")}},
                        {userEmailId:{$in:_.pluck(value,"name")}}];
                } else {
                    if(key != "userEmailId"){
                        obj[key] = {$in:_.pluck(value,"name")}
                    }
                }
            }

            return obj;
        })
        .value();

    groupedFilter = groupedFilter.filter(function (obj) {
        return !_.isEmpty(obj)
    });

    return groupedFilter;
}

function updateMetaData(companyId,userEmailId,opportunity,callback){

    var forDate = moment().subtract(1,"month");

    var month = moment().month()
        ,year = moment().year()
        ,dateMin = moment().startOf('month')
        ,dateMax = moment().endOf('month');

    OppMetaDataCollection.findOne({companyId:companyId,userEmailId:userEmailId,month:month,year:year}).lean().exec(function (err,existingMetaData) {

        if(!err && existingMetaData && existingMetaData.year){

        } else {
            existingMetaData = {
                companyId:companyId,
                userEmailId:userEmailId,
                month:month,
                year:year,
                data:[{
                    stageName:opportunity.stageName,
                    oppIds:[],
                    amount:0,
                    amountWithNgm:0,
                    numberOfOpps:0,
                    regions:[],
                    reasons:[],
                    verticals:[],
                    products:[],
                    accounts:[],
                    types:[],
                    sources:[],
                    solutions:[],
                    businessUnits:[]
                }]
            }
        }

        formatAnUpdateMetaData(companyId,userEmailId,opportunity,month,year,existingMetaData,callback)

    })
}

function formatAnUpdateMetaData(companyId,userEmailId,opportunity,month,year,existingMetaData,callback) {

    var stageNotExistsInMetaData = false;

    _.each(existingMetaData.data,function (el) {
        if(el.stageName === opportunity.stageName){
            stageNotExistsInMetaData = true;
            var amtWithNgm = opportunity.netGrossMargin || opportunity.netGrossMargin == 0?((opportunity.netGrossMargin*opportunity.amount)/100):opportunity.amount
            el.oppIds.push({
                "closeDate":opportunity.closeDate,
                "createdDate":opportunity.createdDate,
                "opportunityId" : opportunity.opportunityId,
                "amount" : amtWithNgm,
                "amount_original" : opportunity.amount,
                "fromSnapShot" : false
            })

            el.amount = el.amount+opportunity.amount
            el.amountWithNgm = el.amount+amtWithNgm

            el.numberOfOpps = el.numberOfOpps+1
            el.reasons = el.reasons.concat(opportunity.reasons)
            el.regions.push(opportunity.geoLocation)
            el.products.push(opportunity.productType)
            el.solutions.push(opportunity.solution)
            el.verticals.push(opportunity.vertical)
        }
    })

    if(!stageNotExistsInMetaData){
        var amtWithNgm = opportunity.netGrossMargin || opportunity.netGrossMargin == 0?((opportunity.netGrossMargin*opportunity.amount)/100):opportunity.amount

        existingMetaData.data.push({
            stageName:opportunity.stageName,
            oppIds:[{
                "closeDate":opportunity.closeDate,
                "createdDate":opportunity.createdDate,
                "opportunityId" : opportunity.opportunityId,
                "amount" : amtWithNgm,
                "amount_original" : opportunity.amount,
                "fromSnapShot" : false
            }],
            amount:opportunity.amount,
            amountWithNgm:amtWithNgm,
            numberOfOpps:1,
            regions:[opportunity.geoLocation],
            reasons:[opportunity.reasons],
            verticals:[opportunity.vertical],
            products:[opportunity.productType],
            solutions:[opportunity.solution]
        })
    }

    OppMetaDataCollection.update({companyId:companyId,userEmailId:userEmailId,month:month,year:year},{$set:{data:existingMetaData.data}},{upsert:true},function (err,results) {

        if(callback){
            callback(err,results);
        }
    });
}

function updateMetaDataForBulkOpps(companyId,userEmailId,opportunities,callback){

    var forDate = moment().subtract(1,"month");

    var month = moment().month()
        ,year = moment().year()
        ,dateMin = moment().startOf('month')
        ,dateMax = moment().endOf('month');

    OppMetaDataCollection.findOne({companyId:companyId,userEmailId:userEmailId,month:month,year:year}).lean().exec(function (err,existingMetaData) {

        if(!err && existingMetaData){
            _.each(existingMetaData.data,function (el) {
                _.each(opportunities,function (opportunity) {
                    if(el.stageName === opportunity.stageName){
                        var amtWithNgm = opportunity.netGrossMargin || opportunity.netGrossMargin == 0?((opportunity.netGrossMargin*opportunity.amount)/100):opportunity.amount
                        el.oppIds.push({
                            "closeDate":opportunity.closeDate,
                            "createdDate":opportunity.createdDate,
                            "opportunityId" : opportunity.opportunityId,
                            "amount" : amtWithNgm,
                            "amount_original" : opportunity.amount,
                            "fromSnapShot" : false
                        })

                        el.amount = el.amount+opportunity.amount
                        el.amountWithNgm = el.amount+amtWithNgm

                        el.numberOfOpps = el.numberOfOpps+1
                        el.reasons = el.reasons.concat(opportunity.reasons)
                        el.regions.push(opportunity.geoLocation)
                        el.products.push(opportunity.productType)
                        el.solutions.push(opportunity.solution)
                        el.verticals.push(opportunity.vertical)
                    }
                })
            })

            OppMetaDataCollection.update({companyId:companyId,userEmailId:userEmailId,month:month,year:year},{$set:{data:existingMetaData.data}},function (err,results) {

                if(callback){
                    callback(err,results);
                }
            });
        }

    })
}

function buildOppMetaDataObj(opps,userEmailId,companyId,month,year,primaryCurrency,currenciesObj,startSelection,endSelection){
    var obj = {
        data:[]
    };

    var createdThisSelection = [];
    var dataForMonthYear = month+""+year

    _.each(opps,function (el) {
        obj.userEmailId = userEmailId;
        obj.companyId = companyId;
        obj.month = month;
        obj.year = year;

        var amount = 0
            ,amountWithNgm = 0
            ,oppIds = []
            ,regions = []
            ,products = []
            ,verticals = []
            ,reasons = []
            ,types = []
            ,solutions = []
            ,businessUnits = [];

        _.each(el.opportunities,function (op) {

            op.amount = parseFloat(op.amount);
            op.createdDate = op.createdDate?op.createdDate:op._id.getTimestamp();

            var withNgm = op.amount;
            var amountWithNgm_original = op.amount;
            if(op.netGrossMargin || op.netGrossMargin == 0){
                withNgm = op.netGrossMargin*op.amount
                amountWithNgm_original = (op.netGrossMargin*op.amount)/100;
                if(withNgm){
                    withNgm = withNgm/100
                }
            }

            op.amountWithNgm = withNgm

            if(primaryCurrency && currenciesObj){
                convertAmount(op,primaryCurrency,currenciesObj)
            } else {
                op.convertedAmt = op.amount;
                op.convertedAmtWithNgm = op.amountWithNgm
            }

            amountWithNgm = amountWithNgm+withNgm
            amount = amount+op.amount;
            if(op.geoLocation && (op.geoLocation.zone || op.geoLocation.town)){
                regions.push(op.geoLocation);
            }

            products.push(op.productType);
            reasons.push(op.closeReasons);
            types.push(op.type);
            solutions.push(op.solution);
            verticals.push(op.vertical);
            businessUnits.push(op.businessUnit);

            var monthYearCreated = moment(op.createdDate).month()+""+moment(op.createdDate).year();

            if(new Date(op.createdDate)>= new Date(startSelection) && new Date(op.createdDate)<= new Date(endSelection)){
                createdThisSelection.push({
                    closeDate:op.closeDate,
                    createdDate:op.createdDate,
                    opportunityId:op.opportunityId,
                    amount:amountWithNgm_original,
                    amount_original:op.amount,
                    convertedAmt:op.convertedAmt,
                    convertedAmtWithNgm:op.convertedAmtWithNgm,
                    fromSnapShot:false
                })
            }

            oppIds.push({
                closeDate:op.closeDate,
                createdDate:op.createdDate,
                opportunityId:op.opportunityId,
                amount:amountWithNgm_original,
                amount_original:op.amount,
                convertedAmt:op.convertedAmt,
                convertedAmtWithNgm:op.convertedAmtWithNgm,
                fromSnapShot:monthYearCreated == dataForMonthYear?false:true
            });

        });

        obj.data.push({
            stageName:el._id,
            oppIds:oppIds,
            amount:amount,
            amountWithNgm:amountWithNgm,
            numberOfOpps:el.opportunities.length,
            regions:_.compact(_.flatten(regions)),
            reasons:_.compact(_.flatten(reasons)),
            products:_.compact(_.flatten(products)),
            solutions:_.compact(_.flatten(solutions)),
            verticals:_.compact(_.flatten(verticals))
        })
    })

    return {
        createdThisSelection:createdThisSelection,
        oppMetaDataFormat:obj
    };
}

function convertAmount(el,primaryCurrency,currenciesObj) {
    el.convertedAmt = el.amount;
    el.convertedAmtWithNgm = el.amountWithNgm

    if(el.currency && el.currency !== primaryCurrency){

        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
        }

        if(el.netGrossMargin || el.netGrossMargin == 0){
            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
        } else {
            el.convertedAmtWithNgm = el.convertedAmt;
        }

        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))
    }
}

function convertAmount_edit_amount(el,primaryCurrency,currenciesObj) {

    el.amountWithNgm = el.amount;

    if(el.netGrossMargin || el.netGrossMargin == 0){
        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
    }

    el.convertedAmt = el.amount;
    el.convertedAmtWithNgm = el.amountWithNgm

    if(el.currency && el.currency !== primaryCurrency){

        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
        }

        if(el.netGrossMargin || el.netGrossMargin == 0){
            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100;
        } else {
            el.convertedAmtWithNgm = el.convertedAmt;
        }

        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))
    }

    el.amount = el.convertedAmtWithNgm
}

OpportunitiesManagement.prototype.getAllSystemReferencedOppsByUserId = function (userIds,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {
    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId)
    var match = {
        $or:[{userId:{$in:userIds}},
            {
                $and:accessControlQuery
            }
        ]
    }
    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds}
        }
    }
    var projectQuery = {
        _id:0,
        'User Email':"$userEmailId",
        'Opportunity Name':"$opportunityName",
        'Contact Email':"$contactEmailId",
        Product:"$productType",
        Vertical:"$vertical",
        'Business Unit':"$businessUnit",
        Currency:"$currency",
        Amount:"$amount",
        opportunityId:"$opportunityId"
    };
    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount();
    }
    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in getAllSystemReferencedOppsByUserId():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

OpportunitiesManagement.prototype.getMLOpportunityInsightsByUserId = function (userIds,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {
    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId)
    var match = {
        $or:[{userId:{$in:userIds}},
            {
                $and:accessControlQuery
            }
        ]
    }
    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds}
        }
    }
    var projectQuery = {
        _id:0,
        'User Email':"$userEmailId",
        'Opportunity Name':"$opportunityName",
        'Contact Email':"$contactEmailId",
        'RoadBlock Observed':"$roadBlockForOpportunity",
        'Negative Sentiment':"$negativeSentiment",
        'No Response To Customer':"$respondedToCustomer"
    };
    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount();
    }
    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in getMLOpportunityInsightsByUserId():opportunitiesManagement ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

module.exports = OpportunitiesManagement;
