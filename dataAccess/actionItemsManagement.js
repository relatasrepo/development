var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var contactClass = require('../dataAccess/contactsManagementClass');
var meetingManagement = require('../dataAccess/meetingManagement');
var actionItemsCollection = require('../databaseSchema/userManagementSchema').actionItems;
var InteractionsCollection = require('../databaseSchema/userManagementSchema').interactions;
var insightsManagement = require('../dataAccess/insightsManagement');
var userManagement = require('../dataAccess/userManagementDataAccess');
var gcal = require('google-calendar');
var simple_unique_id = require('simple-unique-id');
var unionBy = require('lodash.unionby');

var commonUtility = require('../common/commonUtility')
var userOpenSlots = require('../common/userOpenSlots');
var officeOutlookApi = require('../common/officeOutlookAPI');
var moment = require("moment");
var _ = require("lodash");
var momentTZ = require('moment-timezone');

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var common = new commonUtility();
var contactObj = new contactClass();
var userOpenSlotsObj = new userOpenSlots();
var meetingClassObj = new meetingManagement();
var officeOutlook = new officeOutlookApi();
var loggerActionItems = logLib.getActionErrorLogger();
var insightsManagementObj = new insightsManagement();
var userManagementObj = new userManagement();


function ActionItemsManagement() {
    
}

/*
* IMPORTANT!
* Start updating the actionItems collection as soon each corresponding process is completed. TODO- Update which files and functions call these methods
* 1. When Emails are processed start kickOffActionItemsMailProcess()
* 2. When Meetings are processed start kickOffActionItemsMeetingsProcess()
* */

ActionItemsManagement.prototype.kickOffActionItemsMailProcess = function (userId,lastLogin,req,callback) {

    /*
     * Calling file and function
     * 1. kickOffMailsProcess() in office365.js
     * 2. addInteractionsBatch() in googleCalender.js
     * updates the action items and action item meetings once the meeting and mails from google and outlook are updated
     * */

    // if(req.session && req.session.updateActionItemMeeting && req.session.updateActionItemMail) {
        loggerActionItems.info("meeting updated session variable -",req.session.updateActionItemMeeting,' for ', userId);
        loggerActionItems.info("mail updated session variable - ",req.session.updateActionItemMail,' for', userId);
        loggerActionItems.info("kickOffActionItemsMailProcess()- ", userId)
        userOpenSlotsObj.findAndStoreEmailSentOutFrequency(common.castToObjectId(userId),function(error, result){
            loggerActionItems.info("store email sent out frequency - ",result,' for - ', userId)

            myUserCollection.findOne({_id:common.castToObjectId(userId)},{contacts:0},function(error,user) {

                var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
                var mUrl = protocol+req.headers.host;

                getAndInsertFollowUpMail(user, user.emailId, function (result,updateFollowUpMeeting) {//TODO sunil store session
                    loggerActionItems.info("Action item follow mail update - ",result.invitationId,userId)
                    findAndUpdateResponsePendingItem(user, lastLogin, function (result,updateResponsePendingMeeting) {
                        loggerActionItems.info("Action item response pending and ML mail update - ",result.invitationId,userId)
                        var DRPsetting = user && user.dashboardSetting && user.dashboardSetting.responsePending ? user.dashboardSetting.responsePending : false;
                        loggerActionItems.info("Dashboard setting responsePending - ", DRPsetting, " exist function result:" + updateResponsePendingMeeting)

                        findAndInsertActionItemSlotsMeeting(user,"responsePending",checkForTrue(true,updateResponsePendingMeeting),mUrl,function(result,message){
                            loggerActionItems.info("Action item meeting update - responsePending - ", result.invitationId," message:"+message+" for",String(userId))
                            var DFUsetting = user && user.dashboardSetting && user.dashboardSetting.followUp ? user.dashboardSetting.followUp : false;
                            loggerActionItems.info("Dashboard setting followUp - ", DFUsetting, " exist function result:" + updateFollowUpMeeting)

                            findAndInsertActionItemSlotsMeeting(user,"followUp",checkForTrue(true,false),mUrl,function(result,message){
                                loggerActionItems.info("Action item mail followup update - followUp - ", result.invitationId," message:"+message+" for",String(userId))

                                findYesterdaysMeetings(user,'store',function (meetingsExist) {

                                    var DMFUsetting = user && user.dashboardSetting && user.dashboardSetting.meetingFollowUp ? user.dashboardSetting.meetingFollowUp : false;
                                    loggerActionItems.info("Dashboard setting meetingFollowUp - ", DMFUsetting, " exist function result:" + meetingsExist)

                                    findAndInsertActionItemSlotsMeeting(user,"meetingFollowUp",checkForTrue(true,meetingsExist),mUrl,function(result,message) {
                                        loggerActionItems.info("Action item meeting update - meetingFollowUp - ", result.invitationId, " message:" + message + " for", String(userId))

                                        findReminderToConnectContacts(userId,user.timezone.name,function (rtcExists) {
                                            
                                            loggerActionItems.info("Dashboard setting remindToConnect - ", DMFUsetting, " exist function result:" + rtcExists)

                                            findAndInsertActionItemSlotsMeeting(user, "remindToConnect", checkForTrue(true,rtcExists),mUrl, function (result, message) {
                                                loggerActionItems.info("Action item meeting update - remindToConnect - ", result.invitationId, " message:" + message + " for", String(userId))
                                                var DTTLCsetting = user && user.dashboardSetting && user.dashboardSetting.travellingToLocation ? user.dashboardSetting.travellingToLocation : false;
                                                loggerActionItems.info("Dashboard setting travellingToLocation - ", DTTLCsetting, " exist function result:" + true)
                                                findTravellingToLocationItems(user, function(TTLExist) {

                                                    findAndInsertActionItemSlotsMeeting(user, "travellingToLocation", checkForTrue(true, TTLExist),mUrl, function (result, message) {
                                                        loggerActionItems.info("Action item meeting update - travellingToLocation - ", result.invitationId, " message:" + message + " for", String(userId))

                                                        findLosingTouchContacts(common.castToObjectId(userId), function (LTExist) {
                                                            var DLTCsetting = user && user.dashboardSetting && user.dashboardSetting.losingTouch ? user.dashboardSetting.losingTouch : false;
                                                            loggerActionItems.info("Dashboard setting losingTouch - ", DLTCsetting, " exist function result:" + LTExist)

                                                            // findAndInsertActionItemSlotsMeeting(user, "losingTouch", checkForTrue(DLTCsetting, /*LTExist*/false),mUrl, function (result, message) {
                                                            findAndInsertActionItemSlotsMeeting(user, "losingTouch", checkForTrue(true, LTExist),mUrl, function (result, message) {
                                                                loggerActionItems.info("Action item meeting update - losingTouch - ", result.invitationId, " message:" + message + " for", String(userId))

                                                                //store session about update
                                                                setInsightsBuiltFlag(userId,true);
                                                                callback(result)
                                                            });
                                                        })
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    // }
    // else{
    //     callback(false)
    // }
};

ActionItemsManagement.prototype.updateTodayMeetings = updateTodayMeetings;

ActionItemsManagement.prototype.updateRemindTConnect = updateRemindTConnect;

ActionItemsManagement.prototype.getAndInsertFollowUpMail = getAndInsertFollowUpMail;

ActionItemsManagement.prototype.fetchMailRecordIds = fetchMailRecordIds;

ActionItemsManagement.prototype.getMailsByInteractionId = getMailsByInteractionId;

ActionItemsManagement.prototype.updateFollowUpMailAction = updateFollowUpMailAction;

ActionItemsManagement.prototype.contactsAtMyMeetingLocation = contactsAtMyMeetingLocation;

ActionItemsManagement.prototype.findAndUpdateResponsePendingItem = findAndUpdateResponsePendingItem;

ActionItemsManagement.prototype.getAndStoreImportantMails = getAndStoreImportantMails;

ActionItemsManagement.prototype.updateCalendarWithActionItems = updateCalendarWithActionItems;

ActionItemsManagement.prototype.getActionItemSlotMeetings = getActionItemSlotMeetings;

ActionItemsManagement.prototype.findAndInsertActionItemSlotsMeeting = findAndInsertActionItemSlotsMeeting;

ActionItemsManagement.prototype.fetch = findAndInsertActionItemSlotsMeeting;

ActionItemsManagement.prototype.updateActionTakenInfoByDocumentRecordId = updateActionTakenInfoByDocumentRecordId;

ActionItemsManagement.prototype.updateLosingTouchActionTaken = updateLosingTouchActionTaken;

ActionItemsManagement.prototype.findYesterdaysMeetings = findYesterdaysMeetings;

ActionItemsManagement.prototype.findMeetingFollowUpByDate = findMeetingFollowUpByDate;

ActionItemsManagement.prototype.findRemindToConnectByDate = findRemindToConnectByDate;

ActionItemsManagement.prototype.findAndInsertNonExistingItemsByMeetingId = findAndInsertNonExistingItemsByMeetingId;

ActionItemsManagement.prototype.findActionItemsByMeetingId = findActionItemsByMeetingId;

ActionItemsManagement.prototype.findActionItemsByType = findActionItemsByType;

ActionItemsManagement.prototype.findAndInsertNonExistingItemsByRecordIdAndActionType = findAndInsertNonExistingItemsByRecordIdAndActionType;

function updateCalendarWithActionItems(user,slot,actionItem,userUrl,callback) {
    
    var meeting = {
        isGoogleMeeting:false,
        isOfficeOutlookMeeting:false,
        toList:[
            {
                receiverId:null,
                receiverFirstName:"relatas",
                receiverLastName:"event",
                receiverEmailId:"events@relatas.com",
                isAccepted:true,
                canceled:false

            }
        ],
        scheduleTimeSlots : [
            {
                isAccepted : false,
                start : {
                    date : new Date(slot.start)
                },
                end : {
                    // date : new Date(slot.end) // Adding 5 min
                    date : new Date(moment(slot.start).add(5, "minutes"))
                },
                title : "Relatas Insights"+" "+actionItem,
                location : null,
                suggestedLocation : null,
                locationType : null,
                description : null
            }
        ],
        senderId : user._id.toString(),
        senderName : user.firstName,
        senderEmailId : user.emailId,
        senderPicUrl : null,
        invitationId : null, //update
        googleEventId : null, //update same as invitId
        scheduledDate : new Date(),
        mobileNumber : null,
        location : null,
        publicProfileUrl : null,
        designation : null,
        companyName : null,
        title : null,
        locationType : null,
        actionItemSlot:true,
        actionItemSlotType:actionItem
    }

    if(user.serviceLogin == 'google'){
        meeting.isGoogleMeeting = true;
        createGoogleEvent(user,slot,meeting,actionItem,userUrl,function(error, result){
            callback(error, result)
        });
    }
    else if(user.serviceLogin == 'outlook'){
        meeting.isOfficeOutlookMeeting = true;
        createOutlookEvent(user,slot,meeting,actionItem,userUrl,function(error, result){
            callback(error, result)
        });
    }
    else{
        meeting.isGoogleMeeting = true;
        createGoogleEvent(user,slot,meeting,actionItem,userUrl,function(error, result){
            callback(error, result)
        });
    }
};

function createGoogleEvent(user,slot,meeting,actionItem,userUrl,callback){

    var id = simple_unique_id.generate('googleRelatas');
    var gId = 'relat'+common.getNumString(0)+id;
    var attendees = [];
    var description = '';
    var location = userUrl;
    var updateExternalCalendar = false;

    if(actionItem === 'responsePending') {
        actionItem = 'Response Pending';
        description = description + 'Relatas recommends you to respond to these mails that you have received from your contacts.' +'\n' + userUrl + '/insights/mail/action';
        location = location+'/insights/mail/action';

        if(user.dashboardSetting.responsePending){
            updateExternalCalendar = true;
        }

    } else if(actionItem === 'meetingFollowUp'){
        actionItem = 'Meeting Follow-ups';
        description = description + 'Relatas recommends you to follow up on the following few meetings you had in the past.' +'\n' + userUrl + '/today/dashboard';
        location = location+'/today/dashboard';
        if(user.dashboardSetting.meetingFollowUp){
            updateExternalCalendar = true;
        }

    } else if(actionItem === 'remindToConnect'){
        actionItem = 'Remind to Connect';
        description = description + 'Relatas has activated you reminder to re-connect with people. Happy to help you stay connected.' +'\n' + userUrl + '/today/dashboard';
        location = location+'/today/dashboard';
        if(user.dashboardSetting.remindToConnect){
            updateExternalCalendar = true;
        }

    } else if(actionItem === 'losingTouch'){
        actionItem = 'Losing Touch';
        description = description + 'Relatas recommends to connect with these people today to help you stay connected.' +'\n' + userUrl + '/insights/losing/touch/details';
        location = location+'/insights/losing/touch/details';
        if(user.dashboardSetting.losingTouch){
            updateExternalCalendar = true;
        }

    } else if(actionItem === 'mailFollowUp'){
        actionItem = 'Mail Follow-ups';
        description = description + ' Few of your contacts have not responded back. We recommend you follow up with them.' +'\n' + userUrl + '/insights/mail/action';
        location = location+'/insights/mail/action';
        if(user.dashboardSetting.followUp){
            updateExternalCalendar = true;
        }

    }else if(actionItem === 'travellingToLocation'){
        // actionItem = 'Travelling To Location';
        actionItem = 'People Recommended To Meet';
        description = description + ' Relatas recommends to connect with these people while you are traveling to a different location.' +'\n' + userUrl + '/today/dashboard';
        location = location+'/today/dashboard';
        if(user.dashboardSetting.travellingToLocation){
            updateExternalCalendar = true;
        }
    }

    attendees.push({
        email:user.emailId,
        displayName:user.firstName+' '+user.lastName,
        responseStatus: 'accepted'
    });

    var rec = {
        email : 'events@relatas.com', //change
        displayName : 'relatas' +' '+'event'
    };

    attendees.push(rec);

    var request_body = {
        "id": gId,
        "summary": 'Relatas Insights -'+' '+actionItem,
        "end": {
            // "dateTime": new Date(slot.end)
            "dateTime": new Date(moment(slot.start).add(5, "minutes"))
        },
        "start": {
            "dateTime": new Date(slot.start)
        },
        // "description": description+"\n\n--------------------------\n"+"You may change the settings for this insight at www.relatas.com/settings",
        "description": description+"\n\n--------------------------\n"+"You may change the settings for this insight at www.relatas.com/settings",
        "location": location,
        "locationType": '',
        "locationDetails": '',
        "relatasEvent": true,
        "attendees": attendees
    };

    if(!updateExternalCalendar){

        meeting.invitationId = gId;
        meeting.googleEventId = gId;

        meetingClassObj.saveNewMeeting(meeting,function(error,inserted){
            if(error){
                loggerActionItems.info("ERROR: createGoogleEvent() in actionManagement ",error)
                callback(error, inserted)
            }
            else {
                loggerActionItems.info("SUCCESS: Updated only relatas events")
                callback(error,inserted)
            }
        })

    } else {
        common.getNewGoogleToken(user.google[0].token,user.google[0].refreshToken,function (newAccessToken) {
            if (newAccessToken) {
                var google_calendar = new gcal.GoogleCalendar(newAccessToken);
                google_calendar.events.insert('primary', request_body,{sendNotifications:true}, function (err, result) {
                    if(err){
                        loggerActionItems.info("ERROR: createGoogleEvent in actionManagement ",err)
                        callback(err,result)
                    }
                    else {
                        meeting.invitationId = result.id;
                        meeting.googleEventId = result.id;

                        meetingClassObj.saveNewMeeting(meeting,function(error,inserted){
                            if(error){
                                loggerActionItems.info("ERROR: createGoogleEvent() in actionManagement ",error)
                                callback(error, inserted)
                            }
                            else {
                                callback(error,inserted)
                            }
                        })
                    }
                });
            } else {
                callback(false,false);
            }
        });
    }
}

function createOutlookEvent(user,slot,meetingToStore,actionItem,userUrl,callback){

    var attendees = [];
    var description = '';
    var updateExternalCalendar = false;
    var location = userUrl;

    if(actionItem === 'responsePending') {
        actionItem = 'Response Pending';
        description = description + 'Relatas recommends you to respond to these mails that you have received from your contacts.' +'\n' + userUrl + '/insights/mail/action';
        location = location+'/insights/mail/action';

        if(user.dashboardSetting.responsePending){
            updateExternalCalendar = true;
        }

    } else if(actionItem === 'meetingFollowUp'){
        actionItem = 'Meeting Follow-ups';
        description = description + 'Relatas recommends you to follow up on the following few meetings you had in the past.' +'\n' + userUrl + '/today/dashboard';
        location = location+'/today/dashboard';
        if(user.dashboardSetting.meetingFollowUp){
            updateExternalCalendar = true;
        }

    } else if(actionItem === 'remindToConnect'){
        actionItem = 'Remind to Connect';
        description = description + 'Relatas has activated you reminder to re-connect with people. Happy to help you stay connected.' +'\n' + userUrl + '/today/dashboard';
        location = location+'/today/dashboard';
        if(user.dashboardSetting.remindToConnect){
            updateExternalCalendar = true;
        }

    } else if(actionItem === 'losingTouch'){
        actionItem = 'Losing Touch';
        description = description + 'Relatas recommends to connect with these people today to help you stay connected.' +'\n' + userUrl + '/insights/losing/touch/details';
        location = location+'/insights/losing/touch/details';
        if(user.dashboardSetting.losingTouch){
            updateExternalCalendar = true;
        }

    } else if(actionItem === 'mailFollowUp'){
        actionItem = 'Mail Follow-ups';
        description = description + ' Few of your contacts have not responded back. We recommend you follow up with them.' +'\n' + userUrl + '/insights/mail/action';
        location = location+'/insights/mail/action';
        if(user.dashboardSetting.followUp){
            updateExternalCalendar = true;
        }

    }else if(actionItem === 'travellingToLocation'){
        // actionItem = 'Travelling To Location';
        actionItem = 'People Recommended To Meet';
        description = description + ' Relatas recommends to connect with these people while you are traveling to a different location.' +'\n' + userUrl + '/today/dashboard';
        location = location+'/today/dashboard';
        if(user.dashboardSetting.travellingToLocation){
            updateExternalCalendar = true;
        }
    }

    if(!updateExternalCalendar){
        var id = simple_unique_id.generate('googleRelatas');
        var gId = 'relat'+common.getNumString(0)+id;

        meetingToStore.invitationId = gId;
        meetingToStore.LIUoutlookEmailId = user.emailId;

        meetingClassObj.saveNewMeeting(meetingToStore, function (error, inserted) {
            if (error) {
                loggerActionItems.info("ERROR: createOutlookEvent() in actionManagement ", error)
                callback(error, inserted)
            }
            else {
                loggerActionItems.info("SUCCESS: Updated only relatas events")
                callback(error, inserted)
            }
        })
    } else {
        attendees.push({
            "EmailAddress": {
                "Address": "events@relatas.com",
                "Name": 'relatas' +' '+ 'event'
            },
            "Type": "Required"
        });

        var meeting = {
            "Subject": 'Relatas Insights -'+' '+actionItem,
            "Body": {
                "ContentType": "Text",
                "Content": description+"\n\n--------------------------\n"+"You may change the settings for this insight at www.relatas.com/settings"
            },
            "Start": {
                "DateTime": slot.start,
                "TimeZone": user.timezone && user.timezone.name ? user.timezone.name:'UTC'
            },
            "End": {
                "DateTime": slot.end,
                "TimeZone": user.timezone && user.timezone.name ? user.timezone.name:'UTC'
            },
            "Attendees": attendees,
            "Location":{
                "DisplayName":location
            }
        };
        if(user.outlook[0] && user.outlook[0].emailId == user.emailId){
            officeOutlook.getNewAccessTokenWithoutStore(user.outlook[0].refreshToken,function (newToken) {
                officeOutlook.writeMeetingOnCalendarMSGraphAPI(newToken.access_token,meeting,function(error,result){
                    if(error){
                        loggerActionItems.info("ERROR: createOutlookEvent() in actionManagement ",error)
                        callback(error,result)
                    }
                    else {
                        var outlookMeetingObj = JSON.parse(result);
                        meetingToStore.invitationId = outlookMeetingObj.id;
                        meetingToStore.LIUoutlookEmailId = user.emailId;

                        meetingClassObj.saveNewMeeting(meetingToStore, function (error, inserted) {
                            if (error) {
                                loggerActionItems.info("ERROR: createOutlookEvent() in actionManagement ", error)
                                callback(error, inserted)
                            }
                            else {
                                callback(error, inserted)
                            }
                        })
                    }
                });
            });
        }
        else{
            callback(null, false)
        }
    }
}

function getActionItemSlotMeetings(userId,date,actionItemSlotType,findGoogleOrOutlook,callback){
    meetingClassObj.userActionItemSlotMeetingsByDate(userId.toString(),date.min, date.max,{scheduleTimeSlots:1,actionItemSlotType:1,actionItemSlot:1},findGoogleOrOutlook,actionItemSlotType,function(meeting){
       callback(meeting)
    });
}

function findAndInsertActionItemSlotsMeeting(user,actionItemSlotType,updateActionItemMeeting,userUrl,callback) {

    var userId = user._id;

    /*
        actionItemSlotType = "responsePending"/"followUp"/"remindToConnect"/"travellingToLocation"
        This function finds the actionItemSlotType meeting for today and updates if not exists
     */
    loggerActionItems.info("updating ",actionItemSlotType," for", userId)
    if(updateActionItemMeeting) {
        todayDateMinMax(user, function (error, date) {
            if (error) {
                loggerActionItems.info("ERROR: findAndInsertActionItemSlotsMeeting() in actionManagement ", error)
                callback(false, 'todayDateMinMax Error');
            }
            else {
                meetingClassObj.userActionItemSlotMeetingsByDate(user._id.toString(), date.min, date.max, {
                    scheduleTimeSlots: 1,
                    actionItemSlotType: 1,
                    actionItemSlot: 1
                }, user.serviceLogin, actionItemSlotType, function (meeting) {

                    // callback(meeting)
                    if (meeting.length <= 0) {
                        userOpenSlotsObj.findRelatasPreferredSlots(common.castToObjectId(userId.toString()), function (openSlots) {
                            // callback(openSlots)

                            if (openSlots.length > 0) {
                                updateCalendarWithActionItems(user, openSlots[0], actionItemSlotType,userUrl, function (err, result) {
                                    if (err) {
                                        loggerActionItems.info("ERROR: findAndInsertActionItemSlotsMeeting() in actionManagement ", err)
                                        callback(false, 'updateCalendarWithActionItems Error');
                                    }
                                    else {
                                        callback(result, 'updated successfully');
                                    }
                                });
                            } else {
                                callback(false, 'no free slots');
                            }
                        });
                    }
                    else {
                        callback(true, 'meeting already exists')
                    }
                });
            }
        });
    }
    else{
        callback(false, 'No mails to update action item meeting')
    }
}

function updateRemindTConnect(userId,req) {

    var remindDetails = req.body

    var emailId = null;
    var timezone = 'UTC'
    if(req.session && req.session.profile && req.session.profile.emailId){
        emailId = req.session.profile.emailId;
        if(req.session.profile.timezone && req.session.profile.timezone.name){
            timezone = req.session.profile.timezone.name;
        }
    }

    var actionItems = [];
    var obj = {
        userId: common.castToObjectId(userId.toString()),
        userEmailId: emailId,
        itemCreatedDate:new Date(momentTZ(remindDetails.remind).tz(timezone)),
        actionTakenSource:null,
        actionTaken: false,
        actionTakenDate: null,
        documentRecordFrom: 'userCollection',
        itemType: 'remindToConnect',
        documentRecordId:common.castToObjectId(remindDetails.contactId.toString())
    };

    actionItems.push(obj)
    actionItemsCollection.update({userId:common.castToObjectId(userId),documentRecordId:common.castToObjectId(remindDetails.contactId.toString()),itemType: 'remindToConnect'},{actionTaken:true},{multi:true},function (err, updated) {
        /*
        * update the previous reminder for this contact and insert new reminder action item.
        * */
        actionItemsCollection.collection.insert(actionItems,function (err,result) {

            if(!err && result) {
                loggerActionItems.info("Remind to connect updated for user", emailId)
            } else {
                loggerActionItems.info("Remind to connect unsuccessful for user", emailId,err)
            }
        });
    })

}

function updateTodayMeetings(userProfile,todayMeetings) {

    var actionItems = [];

    _.each(todayMeetings,function (meeting) {

        var isAccepted = false,itemCreatedDate = new Date();
        _.each(meeting.scheduleTimeSlots,function (slot) {
            isAccepted = slot.isAccepted
            itemCreatedDate = new Date(slot.start.date)
        });

        var source = null;

        if(isAccepted && meeting.isGoogleMeeting){
            source = 'google'
        } else if(isAccepted && meeting.isOfficeOutlookMeeting){
            source = 'outlook'
        }

        var obj = {
            userId: common.castToObjectId(userProfile._id.toString()),
            userEmailId: userProfile.emailId,
            itemCreatedDate: itemCreatedDate,
            actionTakenSource:source,
            actionTaken: isAccepted,
            actionTakenDate: isAccepted?new Date():null,
            itemType: 'meeting',
            documentRecordFrom: 'meetingCollection',
            documentRecordId:common.castToObjectId(meeting._id.toString())
        };
        actionItems.push(obj)
    });
    if(actionItems.length > 0) {
        actionItemsCollection.collection.insert(actionItems, function (err, result) {

            if (!err && result) {
                loggerActionItems.info("Meeting action items updated for user", userProfile.emailId)
            } else {
                loggerActionItems.info("Meeting action items unsuccessful for user", userProfile.emailId, err)
            }
        });
    }
}

function getAndInsertFollowUpMail(user,emailId,callback) {

    var userId = user._id;

    followUpMails(user,function(error, interactionIds){

        if(error){
            callback(false)
        }
        else {
            var updateFollowUpMeeting = false;
            if(interactionIds.length > 0){
                updateFollowUpMeeting = true;
            }

            var interactionIdString = [];

            interactionIds.forEach(function(a){
                interactionIdString.push(a.toString());
            })
            findNonExistingItemsByInteractionId(userId, interactionIdString, function (nonExisting) {

                var actionItems = [];
                nonExisting.obj.forEach(function (a) {
                    var obj = {
                        userId: common.castToObjectId(userId.toString()),
                        userEmailId: emailId,
                        itemCreatedDate: new Date(),
                        actionTakenSource: null,
                        actionTaken: false,
                        actionTakenDate: null,
                        documentRecordFrom: 'interactionsCollection',
                        itemType: 'mailFollowUp',
                        documentRecordId: common.castToObjectId(a.toString())
                    };

                    actionItems.push(obj)
                });

                if(actionItems.length > 0) {
                    actionItemsCollection.collection.insert(actionItems, function (err, result) {
                        if (!err && result) {
                            callback(true,updateFollowUpMeeting)
                            loggerActionItems.info("Email follow up updated for user", emailId)
                        } else {
                            callback(false,updateFollowUpMeeting)
                            loggerActionItems.info("Email follow up updated unsuccessful for user", emailId, err)
                        }
                    });
                }
                else{
                    callback(true,updateFollowUpMeeting)
                }

            });
        }
    });

}

function findNonExistingItemsByInteractionId(userId,interactionIds,callback) {
    actionItemsCollection.find({userId: userId, documentRecordId: {$in: interactionIds}}, function (err, result) {
        if (!err && result) {
            var existing = _.pluck(result, "documentRecordId")
            var idsString = [];
            existing.forEach(function (a) {
                idsString.push(a.toString());
            })
            var nonExisting = _.difference(interactionIds, idsString);

            var nonExitingIds = [];
            nonExisting.forEach(function (a) {
                nonExitingIds.push(common.castToObjectId(a));
            })
            callback({obj:nonExitingIds,strings:idsString})
        } else {
            callback({obj:[],strings:[]});
            loggerActionItems.info("ERROR: findNonExistingitemsByInteractionId() in actionItemsManagement", err)
        }
    });
};

function findAndInsertNonExistingItemsByMeetingId(userId,meetingId,actionItems,actionType,callback) {

    actionItemsCollection.find({userId: userId, meetingId: meetingId,itemType: actionType }, function (err, result) {

        if (!err) {
            if(result.length == 0){
                if(actionItems.length > 0) {
                    actionItemsCollection.collection.insert(actionItems, function (err, result) {
                        if (!err && result) {
                            callback(true)
                            loggerActionItems.info("Traveling to location updated for user",userId)
                        } else {
                            callback(false)
                            loggerActionItems.info("Traveling to location updated unsuccessful for user",userId, err)
                        }
                    });
                }
                else{
                    callback(true)
                }
            }
            else{
                callback(true)
            }
        } else {
            callback(false)
            loggerActionItems.info("ERROR: findAndInsertNonExistingItemsByMeetingId() in actionItemsManagement", err)
        }
    });
};

function findAndInsertNonExistingItemsByRecordIdAndActionType(userId,actionItems,actionType,callback) {

    var recordIds = _.pluck(actionItems,"documentRecordId");

    actionItemsCollection.find({userId: userId, documentRecordId: {$in:recordIds},itemType: actionType }, function (err, result) {

        if(!err){

            var addRecords = actionItems.filter(function(val) {
                return result.indexOf(val.documentRecordIdString) == -1;
            });
            
            var zxc = unionBy(actionItems,result,"documentRecordIdString")
            
            if(addRecords.length>0){
                actionItemsCollection.collection.insert(addRecords, function (err, result) {
                    if (!err && result) {
                        callback(true)
                        loggerActionItems.info(actionType +"updated for user",userId.toString())
                    } else {
                        callback(false)
                        loggerActionItems.info(actionType +"updated unsuccessful for user",userId.toString(), err)
                    }
                });
            }
            else{
                callback(true)
            }

        } else {
            callback(false)
            loggerActionItems.info("ERROR: findAndInsertNonExistingItemsByRecordIdAndActionType() in actionItemsManagement", err)
        }
    });
};

function findActionItemsByMeetingId(userId,meetingId,actionType,callback) {
    actionItemsCollection.find({userId: userId, meetingId: meetingId,itemType: actionType ,actionTaken :{$in:[false,null]} }, function (err, result) {
        if (!err) {
            callback(result)
        } else {
            callback([])
            loggerActionItems.info("ERROR: findAndInsertNonExistingItemsByMeetingId() in actionItemsManagement", err)
        }
    });
};

function findActionItemsByType(userId,actionType,callback) {
    actionItemsCollection.find({userId: userId,itemType: actionType ,actionTaken :{$in:[false,null]} }, function (err, result) {
        if (!err) {
            callback(result)
        } else {
            callback([])
            loggerActionItems.info("ERROR: findActionItemsByType() in actionItemsManagement", err)
        }
    });
};

function contactsAtMyMeetingLocation(userId,locations,participants,callback) {

    //Locations and participants both are arrays.

    contactObj.getContactsMatchingLocations(common.castToObjectId(userId.toString()),locations,participants,function (err,contacts) {

        if(!err && contacts && contacts.length>0){

            callback({
                contacts:contacts
            });

        } else {
            callback(false)
        }
    });
}

function updateFollowUpMailAction(userId,interactionIds,callback){
    actionItemsCollection.update({userId:userId, documentRecordId:{$in:interactionIds}},{$set:{actionTaken:true}},function (err,result) {
        if(!err && result) {
            callback(result)
        } else {
            callback([]);
            loggerActionItems.info("ERROR: updateFollowUpAction() in actionItemsManagement",err)
        }
    });
}

var followUpMails = function(user, callback) {

    var userId = user._id;

    todayDateMinMax(user,function(error, date){
        if (error) {
            callback(err, []);
        }
        else {
            var dateMin = moment(date.min).subtract(7, "days");
            var dateMax = moment(date.max).subtract(7, "days");

            var aggregation = InteractionsCollection.aggregate([
                {$match: {ownerId: userId}}, {
                    $group: {
                        _id: {
                            ne: {$ne: ["$userId", "$ownerId"]},
                            emailId: "$ownerEmailId",
                            uid: "$ownerId",
                        },
                        interactions: {
                            $push: {
                                interactionDate: "$interactionDate",
                                trackInfo: "$trackInfo",
                                action: "$action",
                                emailId: "$emailId",
                                refId: "$refId",
                                title: "$title",
                                _id: "$_id",
                                description: "$description",
                                emailContentId: "$emailContentId",
                                ownerEmailId: "$ownerEmailId",
                                trackId: "$trackId"
                            }
                        }
                    }
                },
                {$match: {"_id.ne": true}},
                {$unwind: "$interactions"}, {
                    $match: {
                        "interactions.interactionDate": {$gt:new Date(dateMin), $lt: new Date(dateMax)},
                        "interactions.trackInfo.trackResponse": {$eq: true},
                        "interactions.action": {$eq: "receiver"},
                        "interactions.trackInfo.gotResponse": {$in: [false, null]},
                    }
                }, {
                    $project: {
                        _id: 0,
                        // emailId: "$interactions.emailId",
                        // refId: "$interactions.refId",
                        // title: "$interactions.title",
                        interactionId: "$interactions._id",
                        // description: "$interactions.description",
                        // emailContentId: "$interactions.emailContentId",
                        // ownerEmailId: "$interactions.ownerEmailId",
                        // trackId: "$interactions.trackId",
                        // trackInfo: "$interactions.trackInfo",
                        // action: "$interactions.action"
                    }
                }
            ]);
            aggregation.options = { allowDiskUse: true };
            aggregation.exec(function (err, result) {
                if (err) {
                    callback(err, []);
                    loggerActionItems.info("ERROR: followUpMails() in actionItemsManagement",err)
                }
                else {
                    var interactionIds = _.pluck(result,"interactionId")
                    callback(null, interactionIds)
                }
            });
        }
    });
}

function fetchMailRecordIds(userId,dataFor,filter, date,callback){

    var query = {userId:userId,actionTaken : false};
    var itemType = [];
    var eAction = [];

    if(filter == 'all'){
        itemType.push("mailResponsePending");
        itemType.push("mailFollowUp");
        itemType.push("MLanalysed");
    }
    else if(filter == "followUp"){
        itemType.push("mailFollowUp");
    }
    else if(filter == "mailResponsePending"){
        itemType.push("mailResponsePending");
    }
    else if(filter == 'important'){
        // eAction.push("important");
        itemType.push("mailResponsePending");
        itemType.push("MLanalysed");
    }
    else if(filter == "responsePendingAndImportant"){
        itemType.push("mailResponsePending");
        itemType.push("MLanalysed");
        itemType.push("mailFollowUp");
    }
    if(itemType.length >0) {
        query["itemType"] = {$in: itemType};
    }

    if(eAction.length > 0){
        query["eAction"] = {$in:eAction}
    }
    
    actionItemsCollection.find(query,function (err,result) {
        if(!err && result) {
            var recordIds = _.pluck(result,"documentRecordId")
            callback(null,recordIds,result);
        } else {
            callback(err,[],[]);
            loggerActionItems.info("ERROR: fetchFollowUpMail() in actionItemsManagement",err)
        }
    });
}

function getMailsByInteractionId(userId, interactionIds, callback) {
    InteractionsCollection.aggregate([
         {
            $match: {
                ownerId: userId,
                _id:{$in:interactionIds}
            }
        }, {
            $project: {
                emailId: "$emailId",
                refId: "$refId",
                title: "$title",
                recordId:"$_id",
                description:"$description",
                emailContentId:"$emailContentId",
                ownerEmailId:"$ownerEmailId",
                trackId:"$trackId",
                trackInfo:"$trackInfo",
                eAction:"$eAction",
                interactionDate:"$interactionDate",
                emailThreadId:"$emailThreadId",
                action:"$action",
                toCcBcc:"$toCcBcc",
                futureEvents:"$futureEvents"
            }
        }
    ], function(err, result) {
        if (err || result.length == 0) {
            callback(err, []);
            if(err){
                loggerActionItems.info("ERROR: getMailsByInteractionId in actionItemsManagement",err)
            }
        }
        else {
            var emailIds = [];
            result.forEach(function(ele) {
                emailIds.push(ele.emailId)
            })

            var q = { "contacts.personEmailId": { $in: emailIds } };
            myUserCollection.aggregate([
                { $match: { _id: userId } },
                { $unwind: "$contacts" },
                { $match: q }, {
                    $project: {
                        _id: 0,
                        contactEmail: "$contacts.personEmailId",
                        company: "$contacts.companyName",
                        designation: "$contacts.designation",
                        value: "$contacts.account.value.inProcess",
                        userId: "$contacts.personId",
                        contactId: "$contacts._id",
                        rel: "$contacts.contactRelation",
                        mobileNumber: "$contacts.mobileNumber",
                        lastInteracted: "$contacts.lastInteracted",
                        contactImageLink: "$contacts.contactImageLink",
                        personName: "$contacts.personName",
                        personId: "$contacts.personId",
                        ownerId: "$_id",
                        favorite:"$contacts.favorite"
                    }
                }
            ], function(error, notify) {
                if (err || notify.length == 0) {
                    callback(err, [])
                    if(err){
                        loggerActionItems.info("ERROR: getMailsByInteractionId in actionItemsManagement",err)
                    }
                }
                else {
                    var emailIds = [];

                    result.forEach(function(ele) {
                        for (var i = 0; i < notify.length; i++) {
                            if (ele.emailId && notify[i].contactEmail && ele.emailId.toLowerCase() == notify[i].contactEmail.toLowerCase()) {
                                ele.contactEmail = notify[i].contactEmail;
                                ele.contactImageLink = notify[i].contactImageLink;
                                ele.company = notify[i].company;
                                ele.designation = notify[i].designation;
                                ele.value = notify[i].value;
                                ele.userId = notify[i].userId;
                                ele.contactId = notify[i].contactId;
                                ele.rel = notify[i].rel;
                                ele.mobileNumber = notify[i].mobileNumber;
                                ele.lastInteracted = notify[i].lastInteracted;
                                ele.personName = notify[i].personName;
                                // ele.reminder_notify = 'notify';
                                ele.ownerId = notify[i].ownerId;
                                ele.personId = notify[i].personId;

                                if (ele.rel && ele.rel.decisionmaker_influencer)
                                    ele.relation = ele.rel.decisionmaker_influencer;
                                else
                                    ele.relation = null;
                                if (ele.lastInteracted)
                                    ele.lastInteracted = moment(ele.lastInteracted).format("DD MMM YYYY")
                                if (!ele.value)
                                    ele.value = 0;
                                ele.favorite = notify[i].favorite
                                delete ele.rel;
                            }
                        }
                    })

                    notify.forEach(function(ele) {
                        if (ele.contactEmail)
                            emailIds.push(ele.contactEmail)
                    });

                    getProfileUrl(emailIds, result, function(notifyResult) {
                        callback(null, notifyResult);
                    });
                }
            });
        }
    });
}

function getProfileUrl(emailIds, contacts, callback) {
    getPublicProfileUrl(emailIds, function(err, publicProfileData) {
        if (err || !publicProfileData) {
            callback(contacts);
        }
        else {
            contacts.forEach(function(ele) {
                for (var i = 0; i < publicProfileData.length; i++)
                    if (ele.contactEmail == publicProfileData[i].emailId)
                        ele.publicProfileUrl = publicProfileData[i].publicProfileUrl
            })

            callback(contacts);
        }
    });
}

function getPublicProfileUrl(emailIds, callback) {
    myUserCollection.find({ emailId: { $in: emailIds } }, { publicProfileUrl: 1, emailId: 1, registeredUser: 1 }, function(error, contactPublicProfileUrl) {

        var results = _.map(
            _.where(contactPublicProfileUrl, { registeredUser: true }),
            function(data) {
                return { emailId: data.emailId, publicProfileUrl: data.publicProfileUrl };
            }
        );

        if (error) {
            loggerActionItems.info("ERROR: getPublicProfileUrl in actionItemsManagement",error)
        }

        if (results) {
            callback(error, results);
        } else {
            callback(error, null);
        }
    });
};

function todayDateMinMax(user, callback) {
    var timezone = 'UTC';

    if (user.timezone && user.timezone.name){
        timezone = user.timezone.name;
    }

    var dateMin = momentTZ().tz(timezone);
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)
    var dateMax = momentTZ().tz(timezone);

    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(59)
    dateMax.millisecond(0)
    callback(null, {min: dateMin, max: dateMax})
}

function contactsAtMyMeetingLocationCopy(userId,meetings,callback) {

    var meetingsLocations = [];
    var allLocations = [];
    var participants = [];

    _.each(meetings,function (meeting) {
        _.each(meeting.scheduleTimeSlots,function (slot) {
            meetingsLocations.push({
                location: slot.location,
                invitationId: meeting.invitationId,
                date: slot.start.date
            });
            allLocations.push(slot.location);
        });

        _.each(meeting.participants,function (participant) {
            participants.push(participant.emailId)
        })
    });

    allLocations = _.uniq(_.flatten(allLocations));

    var allLocationsTrimmed = [];
    var counter = 0;
    var len = allLocations.length;

    _.each(allLocations,function (loc) {

        counter++;
        if(!common.isNumber(loc)) {
            common.getLocationGoogleAPI(loc,function (googleLocations) {

                _.each(googleLocations.results[0].address_components,function (component) {

                    if(component.types.indexOf("locality") > -1 && component.types.indexOf("political") > -1){
                        allLocationsTrimmed.push({loc:component.long_name.trim()})
                    }
                });

                if(counter == len){

                    contactObj.getContactsMatchingLocations(common.castToObjectId(userId),allLocationsTrimmed,participants,function (err,contacts) {

                        callback({
                            meetingsLocations:meetingsLocations,
                            allLocations:allLocationsTrimmed,
                            contacts:contacts
                        });

                    });
                }
            });
        }
    });
}

function findAndUpdateResponsePendingItem(user,lastLogin,callback){

    var userId = user._id;

    findResponsePending(userId,lastLogin,function(error, emails){

        if(error){
            callback(false)
        }
        else {

            checkForPendingEmails(emails, function (data) {
                var interactionIds = _.pluck(data.pending, "documentRecordId");

                if(data.updateResponded.length > 0) {
                    var threadIds = _.pluck(data.updateResponded, "threadId")
                    updateRespondedItemsByThreadId(userId, threadIds,function(result){
                        findAndInsertNonExisting(userId,interactionIds,data.pending,function(inserted){
                            getAndStoreImportantMails(userId,lastLogin,function(result){
                                findResponsePendingMailsForToday(user,function(responsePendingMail){
                                    var updateMeeting = false;
                                    if(responsePendingMail > 0){
                                        updateMeeting = true;
                                    }
                                    callback(result,updateMeeting);
                                })
                            })
                        })
                    });
                }
                else{
                    findAndInsertNonExisting(userId,interactionIds,data.pending,function(inserted){
                        getAndStoreImportantMails(userId,lastLogin,function(result){
                            findResponsePendingMailsForToday(user,function(responsePendingMail){
                                var updateMeeting = false;
                                if(responsePendingMail > 0){
                                    updateMeeting = true;
                                }
                                callback(result,updateMeeting);
                            })
                        })
                    })
                }
            });
        }
    });
}

function getAndStoreImportantMails(userId,lastLogin, callback){

    var fromDate = momentTZ(lastLogin).subtract(7, "days");

    findImportantMails(userId,new Date(fromDate),["important"],function(error, important){
       if(error){
           callback(false)
       }
        else{
           var impMails = []
           important.forEach(function (a) {
               var obj = {
                   userId:a.ownerId,
                   userEmailId:a.ownerEmailId,
                   documentRecordId:a.interactionId,
                   emailThreadId:a.emailThreadId,
                   documentRecordFrom : "interactionsCollection",
                   itemType : 'MLanalysed',
                   itemCreatedDate: new Date(a.interactionDate),
                   eAction:a.eAction ? a.eAction : null,
                   actionTakenSource : null,
                   actionTaken : false,
                   actionTakenDate : null,
                   idString: a.interactionId.toString()
               }
               impMails.push(obj)
           });

           var interactionIds = _.pluck(impMails, "documentRecordId");
           findAndInsertNonExisting(userId,interactionIds,impMails,function(inserted,updateMeeting){
               callback(inserted,updateMeeting)
           })
           // callback(error, important)
       }
    });
}

function findAndInsertNonExisting(userId,interactionIds,items,callback){
    findNonExistingItemsByInteractionId(userId,interactionIds,function(existingIds){
        var newItems = []
        if(existingIds.strings.length > 0) {
            items.forEach(function (a) {
                if (existingIds.strings.indexOf(String(a.idString)) == -1) {
                    newItems.push(a)
                }
            });
        }
        else{
            newItems = items;
        }
        newItems.forEach(function(a){
            delete a.idString;
        });

        if(newItems.length) {
            insertAllActionItems(newItems, function (inserted,updateMeeting) {
                callback(inserted,updateMeeting)
            });
        }
        else{
            callback(true)
        }
    })
}

function updateLosingTouchActionTaken(items,callback) {
    insertAllActionItems(items, callback)
}

function insertAllActionItems(items, callback) {
    actionItemsCollection.collection.insert(items,function (error, result) {
        if(error){
            callback(false)
            loggerActionItems.info('ERROR: insertAllActionItems() in actionItemManagement',error);
        }
        else{
            callback(true,true)
        }
    })
}

function updateRespondedItemsByThreadId(userId,threadIds, callback){ //first update action taken and insert new response pending items
    
    actionItemsCollection.update({userId:userId,itemType:"mailResponsePending",eAction:{$ne:"important"}, emailThreadId:{$in:threadIds}},{$set:{actionTaken:true}},{multi:true},function(error,result){
        if(error) {
            loggerActionItems.info('ERROR: updateRespondedItemsByThreadId() in actionItemManagement',error);
            callback(false)
        }
        else{
            callback(true)
        }
    })
}

function checkForPendingEmails(emails,callback){
    var updateResponded  = [];
    var pending = [];
    emails.forEach(function (a) {
        if(a.action == 'sender'){
            var obj = {
                userId:a.ownerId,
                userEmailId:a.ownerEmailId,
                documentRecordId:a.interactionId,
                emailThreadId:a.emailThreadId,
                documentRecordFrom : "interactionsCollection",
                itemType : 'mailResponsePending',
                itemCreatedDate: new Date(a.interactionDate),
                eAction:a.eAction ? a.eAction : null,
                actionTakenSource : null,
                actionTaken : false,
                actionTakenDate : null,
                idString: a.interactionId.toString()
            }
            pending.push(obj)
        }
        else if(a.action == 'receiver'){
            var obj = {
                userId:a.ownerId,
                userEmailId:a.ownerEmailId,
                documentRecordId:a.interactionId,
                threadId:a.emailThreadId
            }
            updateResponded.push(obj)
        }
    });
    callback({updateResponded:updateResponded, pending:pending});
}

function findResponsePending(userId,lastLogin,callback){

    var date = momentTZ(lastLogin).subtract(7, "days");

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerId: userId,
                userId: { $ne: userId },
                emailThreadId:{$ne:null},
                toCcBcc:{$in: ["me",null]},
                interactionType:"email",
                interactionDate:{$gt:new Date(date)} //TODO sunil remove or consider only lastlogin
            }
        },
        {$sort:{"interactionDate":-1}},
        {$group:{
            _id:{emailId:"$emailId", emailThreadId:"$emailThreadId"},
            interaction:{
                $max:{
                    personEmailId:"$emailId",
                    // userId:"$userId",
                    emailThreadId:"$emailThreadId",
                    //name:{$concat:["$firstName", ' ', "$lastName"]},
                    source:"$source",
                    interactionDate:"$interactionDate",
                    action:"$action",
                    title:"$title",
                    refId:"$refId",
                    emailContentId:"$emailContentId",
                    description:"$description",
                    companyNameFromInteraction:"$companyName",
                    interactionId:"$_id",
                    trackInfo:"$trackInfo",
                    interactionType:"$interactionType",
                    trackId:"$trackId",
                    toCcBcc:"$toCcBcc",
                    eAction:"$eAction",
                    ownerId:"$ownerId",
                    ownerEmailId:"$ownerEmailId"
                }
            },
            counts:{$sum:1}
            }
        },
        {
            $project:{
                _id:0,
                ownerEmailId:"$interaction.ownerEmailId",
                ownerId:"$interaction.ownerId",
                interactionId:"$interaction.interactionId",
                emailThreadId:"$interaction.emailThreadId",
                title:"$interaction.title",
                action:"$interaction.action",
                interactionDate:"$interaction.interactionDate",
                eAction:"$interaction.eAction"
            }
        }
    ],function(error, result){
        if(error){
            loggerActionItems.info("ERROR: findResponsePending() in actionItemsManagement", error)
            callback(error, [])
        }
        else {
            callback(error, result)
        }
    });
}

function findImportantMails(userId,lastLogin,eAction,callback){
    InteractionsCollection.aggregate([
        {
            $match: {
                ownerId: userId,
                userId: {$ne: userId},
                eAction: {$in:eAction},
                interactionDate:{$gt:new Date(lastLogin)}
            }
        },
        {
            $project:{
                _id:0,
                ownerEmailId:"$ownerEmailId",
                ownerId:"$ownerId",
                interactionId:"$_id",
                emailThreadId:"$emailThreadId",
                title:"$title",
                action:"$action",
                interactionDate:"$interactionDate",
                eAction:"$eAction"
            }
        }
    ],function(error, result){
        if(error){
            loggerActionItems.info("ERROR: findImportantMails() in actionItemsManagement", error)
            callback(error, [])
        }
        else {
            callback(error, result)
        }
    });
}

function findResponsePendingMailsForToday(user,callback){

    var userId = user._id;
    var minDate = momentTZ().subtract(7, "days");
    var maxDate = momentTZ().subtract(7, "days");

    todayDateMinMax(user,function(err,date){
        if(err){
            loggerActionItems.info('ERROR: findResponsePendingMailsForToday in actionItemManagement', err);
            callback(0)
        }
        else {
            actionItemsCollection.find({
                userId: userId,
                "itemType": "mailResponsePending",
                "actionTaken": {$in: [null, false]},
                // itemCreatedDate: {$gt: new Date(date.min), $lt: new Date(date.max)}
                itemCreatedDate: {$gt: new Date(minDate), $lt: new Date()} // LM 13 Apr 17, naveen paul
            }, function (error, result) {

                if (error) {
                    loggerActionItems.info('ERROR: findResponsePendingMailsForToday in actionItemManagement', error);
                    callback(0)
                }
                else {
                    callback(result.length)
                }
            })
        }
    })
}

function findMeetingFollowUpByDate(userId,date,callback){
    actionItemsCollection.find({
        userId: userId,
        "itemType": "meetingFollowUp",
        "actionTaken": {$in: [null, false]},
        itemCreatedDate: {$gt: new Date(date.min), $lt: new Date(date.max)}
    }, function (error, result) {
        if (error) {
            loggerActionItems.info('ERROR: findMeetingFollowUpByDate in actionItemManagement', error);
            callback([])
        }
        else {
            callback(result)
        }
    })
}

function findRemindToConnectByDate(userId,date,callback){
    actionItemsCollection.find({
        userId: userId,
        "itemType": "remindToConnect",
        "actionTaken": {$in: [null, false]},
        itemCreatedDate: {$gte: new Date(date.min), $lt: new Date(date.max)}
    }, function (error, result) {
        if (error) {
            loggerActionItems.info('ERROR: findMeetingFollowUpByDate in actionItemManagement', error);
            callback([])
        }
        else {
            callback(result)
        }
    })
}

function findYesterdaysMeetings(user,filter,callback) {

    var dateMin = momentTZ().subtract(7, "days");
    var dateMax = momentTZ().subtract(0, "days");

    dateMin.date(dateMin.date())
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    dateMax.date(dateMax.date())
    dateMax.hour(0)
    dateMax.minute(0)
    dateMax.second(0)
    dateMax.millisecond(0)

    dateMin = dateMin.format();
    dateMax = dateMax.format();

    var projection = {
        recurrenceId:1,invitationId:1,
        scheduleTimeSlots:1,senderId:1,
        senderName:1,senderEmailId:1,
        senderPicUrl:1,isSenderPrepared:1,
        to:1,toList:1,
        suggested:1,suggestedBy:1,
        selfCalendar:1,
        actionItemSlot:1,
        participants:1
    };

    getTeamUserIds(user._id,null,null,function (err,teamData) {
        meetingClassObj.userMeetingsByDate(user._id,new Date(dateMin),new Date(dateMax),projection,user.serviceLogin,function(meetings){

            if(common.checkRequired(meetings) && meetings.length > 0){
                var nonInsights = [];
                var contactEmailIds = [];
                var invitationIds = [];
                meetings.forEach(function(a){
                    if(!a.actionItemSlot && (a.participants && a.participants.length > 1)){
                        invitationIds.push(a.invitationId)
                        if(a.senderEmailId !== user.emailId) {
                            nonInsights.push({emailId:a.senderEmailId,invitationId:a.invitationId});
                            contactEmailIds.push(a.senderEmailId);
                        }
                        a.toList.forEach(function(b){
                            if(b.receiverEmailId !== user.emailId) {
                                nonInsights.push({emailId:b.receiverEmailId,invitationId:a.invitationId});
                                contactEmailIds.push(b.receiverEmailId);
                            }
                        });
                    }
                });

                var teamMembers = _.pluck(teamData,"emailId")
                var uniqEmailIds = _.uniq(contactEmailIds)

                uniqEmailIds = _.difference(uniqEmailIds,teamMembers)

                userManagementObj.findContactsByEmailIdArray(user._id,uniqEmailIds,function (error,data) {
                    if(error){
                        loggerActionItems.info("Meeting follow up updated for user", emailId)
                        callback(false);
                    }
                    else{
                        nonInsights.forEach(function(a){
                            data.forEach(function(b){
                                if(a.emailId == b.personEmailId){
                                    a.contactId = b.contactId;
                                }
                            });
                        });
                        if(filter == 'store') {
                            actionItemsCollection.find({
                                userId: user._id,
                                meetingId: {$in: invitationIds},
                                itemType: 'meetingFollowUp'
                            }, function (err, result) {
                                if (err) {
                                    callback(false);
                                }
                                else {
                                    var actionMeetingId = _.uniq(_.pluck(result, "meetingId"));
                                    var nonExisting = [];
                                    nonInsights.forEach(function (a) {
                                        actionMeetingId.forEach(function (b) {
                                            if (a.invitationId == b) {
                                                a.exist = true;
                                            }
                                        })
                                    })
                                    
                                    nonInsights.forEach(function (a) {
                                        if (!a.exist) {
                                            nonExisting.push(a);
                                        }
                                    });

                                    var actionItemsToStore = [];
                                    nonExisting.forEach(function (a) {

                                        if(a.contactId && a.emailId != 'unknownorganizer@calendar.google.com') {
                                            var obj = {
                                                meetingId: a.invitationId,
                                                userId: common.castToObjectId(user._id.toString()),
                                                userEmailId: user.emailId,
                                                itemCreatedDate: new Date(),
                                                actionTakenSource: null,
                                                actionTaken: false,
                                                actionTakenDate: null,
                                                documentRecordFrom: 'userCollection',
                                                itemType: 'meetingFollowUp',
                                                documentRecordId: common.castToObjectId(a.contactId.toString())
                                            }
                                            actionItemsToStore.push(obj);
                                        }
                                    });
                                    if (actionItemsToStore.length > 0) {
                                        actionItemsCollection.collection.insert(actionItemsToStore, function (err, result) {
                                            if (!err && result) {
                                                callback(true)
                                                loggerActionItems.info("meeting follow up updated for user", user._id)
                                            } else {
                                                callback(false)
                                                loggerActionItems.info("meeting follow up updated unsuccessful for user", user._id, err)
                                            }
                                        });
                                    }
                                    else {
                                        callback(false)
                                    }
                                }
                            });
                        }
                        else if(filter == 'fetch'){
                            var formated = [];
                            meetings.forEach(function(a){
                                if(!a.actionItemSlot&& (a.participants && a.participants.length > 1)) {

                                    var obj = {
                                        title: a.scheduleTimeSlots[0].title,
                                        description: a.scheduleTimeSlots[0].description,
                                        meetingDate: a.scheduleTimeSlots[0].start.date,
                                        invitationId: a.invitationId,
                                        contacts: []
                                    }
                                    if (a.senderEmailId !== user.emailId) {
                                        obj.contacts.push({emailId: a.senderEmailId})
                                    }
                                    a.toList.forEach(function (b) {
                                        if (user.emailId !== b.receiverEmailId) {
                                            obj.contacts.push({emailId: b.receiverEmailId})
                                        }
                                    })
                                    formated.push(obj)
                                }
                            })

                            data.forEach(function(contact){
                                formated.forEach(function(meeting){
                                    meeting.contacts.forEach(function(perti){
                                        if(contact.personEmailId == perti.emailId){
                                            perti.contactId = contact.contactId;
                                            perti.personId = contact.personId;
                                            perti.contactImageLink = contact.contactImageLink;
                                            perti.contactRelation = contact.contactRelation;
                                            perti.personEmailId = contact.personEmailId;
                                            perti.personName = contact.personName;
                                            perti.mobileNumber = contact.mobileNumber;
                                            perti.designation = contact.designation;
                                            perti.companyName = contact.companyName;
                                        }
                                    })
                                })
                            })

                            var dMin = momentTZ().subtract(7, "days");
                            var dMax = momentTZ().subtract(0, "days");

                            dMin.date(dMin.date())
                            dMin.hour(0)
                            dMin.minute(0)
                            dMin.second(0)
                            dMin.millisecond(0)

                            dMax.date(dMax.date())
                            dMax.hour(23)
                            dMax.minute(59)
                            dMax.second(59)
                            dMax.millisecond(0)

                            dMin = dMin.format();
                            dMax = dMax.format();

                            actionItemsCollection.find({
                                userId: user._id,
                                itemCreatedDate: {$gt:new Date(dMin), $lt:new Date(dMax)},
                                itemType: 'meetingFollowUp',
                                actionTaken:{$in:[false, null]}
                            }, function (err, result) {
                                if(result.length > 0){
                                    var finalResult = [];
                                    result.forEach(function(action){
                                        formated.forEach(function(meeting){
                                            meeting.contacts.forEach(function(contact){
                                                if(meeting.invitationId == action.meetingId && contact.contactId && contact.contactId.toString() == action.documentRecordId.toString()){
                                                    contact.actionItemId = action._id;
                                                    contact.itemType = action.itemType;
                                                }
                                            });
                                        });
                                    });
                                    formated.forEach(function(a){
                                        a.contacts.forEach(function(b){
                                            if(b.actionItemId){
                                                b.invitationId = a.invitationId;
                                                b.description = a.description;
                                                b.title = a.title;
                                                b.meetingDate = a.meetingDate;
                                                finalResult.push(b)
                                            }
                                        })
                                    });
                                    callback(finalResult)
                                }
                                else{
                                    callback([]);
                                }
                            });
                        }
                    }
                });

            }else{
                callback(false)
            }
        });
    });
}

function findReminderToConnectContacts(userId,timezone,callback) {

    var tz = timezone?timezone:'UTC';

    var dateMin = momentTZ().tz(tz);
    dateMin.hour(0);
    dateMin.minute(0);
    dateMin.second(0);
    dateMin.millisecond(0);
    var dateMax = momentTZ().tz(tz);

    dateMax.hour(23);
    dateMax.minute(59);
    dateMax.second(59);
    dateMax.millisecond(0);

    var date = {min:dateMin,max:dateMax};

    findRemindToConnectByDate(common.castToObjectId(userId),date,function(actionResult){
        if (actionResult && actionResult.length>0) {
            callback(true)
        } else {
            callback(false)
        }
    });
}

function findLosingTouchContacts(userId,callback){
    insightsManagementObj.getLosingTouchSelf([userId], 'losingTouch', [userId], function(err, result) {
        if(err){
            loggerActionItems.info('ERROR: findLosingTouchContacts() in actionItemManagement', err);
            callback(false)
        }
        else{
            if(result && result.self && result.self.selfTotalCount > 0){
                callback(true);
            }
            else{
                callback(false)
            }
        }
    });
}

function updateActionTakenInfoByDocumentRecordId(userId,actionItemId,actionTakenSource,actionTakenType, callback){
    actionItemsCollection.update({userId:userId, _id:actionItemId},{$set:{actionTaken:true,actionTakenDate:new Date(),actionTakenSource:actionTakenSource,actionTakenType:actionTakenType}},function(error,result){
        if(error) {
            loggerActionItems.info('ERROR: updateActionTakenInfoByDocumentRecordId() in actionItemManagement',error);
            callback(false)
        }
        else{
            callback(true)
        }
    })
}

function checkForTrue(value1, value2){
    if(value1 && value2){
        return true;
    }
    else{
        return false;
    }
}

function findTravellingToLocationItems(user,callback){
    var userIdArr = [];
    userIdArr.push(user._id);
    var outlookOrGoogle = null;
    var dateMin = moment().add(1, "days");
    var dateMax = moment().add(14, "days");

    dateMin.date(dateMin.date())
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    dateMax.date(dateMax.date())
    dateMax.hour(0)
    dateMax.minute(0)
    dateMax.second(0)
    dateMax.millisecond(0)

    if(user.google.length>0){
        outlookOrGoogle = 'google'
    } else if(user.outlook.length>0) {
        outlookOrGoogle = 'outlook'
    }

    meetingClassObj.userNexTwoWeeksTravelingTo(userIdArr, dateMin,dateMax,outlookOrGoogle,function (meetings) {
        if(meetings && meetings.length > 0){
            callback(true);
        }
        else{
            callback(false)
        }
    })
}

ActionItemsManagement.prototype.setInsightsBuiltFlag = setInsightsBuiltFlag;

function setInsightsBuiltFlag(userId,flag) {
    userManagementObj.setInsightsBuiltStatus(common.castToObjectId(userId),flag,function (err,result) {
        loggerActionItems.info("Set Insights Built Status :: setInsightsBuiltFlag() ", flag,' for ', userId.toString())
    });
}

ActionItemsManagement.prototype.getTeamUserIds = getTeamUserIds

function getTeamUserIds(userId,minDate,MaxDate,callback){
    userManagementObj.findUserProfileByIdWithCustomFields(userId, { companyId: 1, companyName: 1, hierarchyPath: 1, emailId:1,firstName:1,lastName:1 }, function(error, data) {
        userManagementObj.getTeamMembers(data.companyId, function(err, teamData) {
            teamData.push({_id:common.castToObjectId(userId),emailId:data.emailId,firstName:data.firstName,lastName:data.lastName});
            callback(err,teamData)
        })
    });
}

module.exports = ActionItemsManagement;