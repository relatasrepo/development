//**************************************************************************************
// File Name            : customerProfile
// Functionality        : Router for customerProfile(Quotation/Purchase Order/Invoice)
// History              : First Version
//
//
//**************************************************************************************

var customerProfileTemplateCollection = require('../databaseSchema/customerProfileTemplateSchema').customerProfileTemplateModel;
var customerProfileCollection = require('../databaseSchema/customerProfileSchema').customerProfileModel;
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');

var common = new commonUtility();
var logLib = new winstonLog();
var loggerError = logLib.getCorporateErrorLogger();
var logger = logLib.getCorporateLogger();


function customerProfileManagement() {
}

// function to check and create the new customerProfile
customerProfileManagement.prototype.checkAndCreateCustomerProfile = function (companyId, userId, customerProfile, callback) {
    console.log("***************************************************************************");

    var parentScope = this;
    this.getCustomerProfileByName(companyId, userId, customerProfile, function(error, savedCustomerProfile) {
        if(savedCustomerProfile) {
            console.log("the CustomerProfile exist...updating the same");
            callback(error, savedCustomerProfile);
        }
        else {
            console.log("the template not exist");
            var newCustomerProfileTemplateObj = new customerProfileCollection({
                companyId: companyId,
                customerProfileTemplateName: customerProfile.customerProfileTemplateName,
                customerProfileTemplateType: customerProfile.customerProfileTemplateType,
                customerProfileName: customerProfile.customerProfileName,
                createdDate: new Date()
            });

            console.log("Saving the customerProfile");
            newCustomerProfileTemplateObj.save(function(error, savedTemplate) {
                if(error) {
                    logger.info('Error in createNewCustomerProfile(): customerProfileManagement', error);
                }
                callback(error, savedTemplate);
            });
        }
    })
};

// get customerProfile by name and type to check for its existence
customerProfileManagement.prototype.getCustomerProfileByName = function(companyId, userId, customerProfile, callback) {

    customerProfileCollection.findOne({
            companyId: companyId,
            customerProfileTemplateType:customerProfile.customerProfileTemplateType,
            customerProfileTemplateName:customerProfile.customerProfileTemplateName,
            customerProfileName:customerProfile.customerProfileName
        },
        {  customerProfileTemplateType:1,
            customerProfileTemplateName:1,
            customerProfileName:1},function(error,result){
            if(error){
                console.log('Error in getCustomerProfileByName()', error);
            } else {
                console.log("Result from the getCustomerProfileByName:", result);
                callback(error,result)
            }
        })

}

// update the existing customerProfile
customerProfileManagement.prototype.updateCustomerProfileElements = function (companyId, customerProfile, callback) {
    console.log("updateCustomerProfileElements with",customerProfile )

    customerProfileCollection.update({companyId: companyId,
            customerProfileTemplateName: customerProfile.customerProfileTemplateName,
            customerProfileTemplateType: customerProfile.customerProfileTemplateType,
            customerProfileName: customerProfile.customerProfileName},
        {$set:{
                customerProfileTemplateName: customerProfile.customerProfileTemplateName,
                customerProfileTemplateType: customerProfile.customerProfileTemplateType,
                customerProfileName: customerProfile.customerProfileName,
                customerProfileElementList: customerProfile.customerProfileElementList,
                customerProfileAttrList: customerProfile.customerProfileAttrList,
            }},{upsert:true},function (err,result) {
            callback(err, result);
        });

}

// function to get all the customerProfiles
customerProfileManagement.prototype.getAllCustomerProfiles = function (companyId, callback) {

    customerProfileCollection.find({companyId:companyId},
        {},function(err,result){
            if(!err && result){
                callback(err,result)
            } else {
                result = customerProfileTemplateObj;
                callback(err,result)
            }
        })
}

// function to get the customerProfile by id
customerProfileManagement.prototype.getCustomerProfileById = function (companyId,customerProfileId, callback) {

    console.log("invoking the findOne for customerProfile by CustomerProfile Id");
    customerProfileCollection.findOne({companyId:companyId,
            _id:customerProfileId},
        {},function(err,result){
            callback(err, result);
        })
}

// get customerProfile by name and type to check for its existence
customerProfileManagement.prototype.getCustomerProfilesByTemplateName = function(companyId, templateType, templateName, callback) {

    customerProfileCollection.find({
            companyId: companyId,
            customerProfileTemplateType:templateType,
            customerProfileTemplateName:templateName
        },
        {  },function(error,result){
            if(error){
                console.log('Error in getCustomerProfilesByTemplateName()', error);
            } else {
                callback(error,result)
            }
        })

}
module.exports = customerProfileManagement;