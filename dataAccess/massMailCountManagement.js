
var massMailCountCollection = require('../databaseSchema/userManagementSchema').massMailCount;

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston()

function MassMailCount()
{

}

function constructMassMailCountAccountObject(userId){
    var date = new Date();
    var massMailCountAccountObject = new massMailCountCollection({
         userId:userId,
         count:0,
         date:date,
         createdDate:date

    });
    return massMailCountAccountObject;
}

MassMailCount.prototype.createMassMailCountAccount = function(userId,callback){
    var massMailCountAccountObject = constructMassMailCountAccountObject(userId)

    massMailCountCollection.findOne({userId:massMailCountAccountObject.userId},function(error,accDetails){
        if(error){
            logger.info("Mongo error in searching mass mail count account "+error);
            callback(error,'error',null);
        }
        else{
            if(checkRequired(accDetails)){
                callback(error,'exist',accDetails);
            }
            else{
                massMailCountAccountObject.save(function(err,massMailCount){
                    if(err){
                        logger.info("mongo error in creating mass mail count account "+err);
                        callback(err,'error',null);
                    }
                    else{

                        callback(err,'success',massMailCount);

                    }
                })
            }
        }
    })
}

MassMailCount.prototype.getMassMailCountAccount = function(userId,callback){
    massMailCountCollection.findOne({userId:userId},function(error,accDetails) {
        if (error) {
            logger.info("Mongo error in searching mass mail count account " + error);
            callback(error,'error',null);
        }
        else{
            if(checkRequired(accDetails)){
                callback(error,'exist',accDetails)
            }
            else{
                var massMailCountAccountObject = constructMassMailCountAccountObject(userId)
                massMailCountAccountObject.save(function(err,massMailCount){
                    if(err){
                        logger.info("mongo error in creating mass mail count account "+err);
                        callback(err,'error',null);
                    }
                    else{

                        callback(err,'success',massMailCount);

                    }
                })
            }
        }
    })
}

MassMailCount.prototype.incrementMassMailCount = function(userId,count,callback){

    massMailCountCollection.findOne({userId:userId},function(error1,accDetails) {
        if (error1) {
            logger.info("Mongo error in searching mass mail count account " + error1);
            callback(error1,false);
        }
        else{
            if(checkRequired(accDetails)){
                var now = new Date()
                var dateUpdated = accDetails.date;
                now.setHours(0,0,0,0);
                dateUpdated.setDate(dateUpdated.getDate()+7);
                dateUpdated.setHours(0,0,0,0);

                if(dateUpdated <= now){
                    var date = new Date();
                    massMailCountCollection.update({userId:userId},{$set:{count:0,date:date}},function(error2,result){
                        var flag;
                        if (error2) {
                            logger.info("Error in DB while updating mass mail counter to 0 " + error2);
                            callback(error2,false);
                        }
                        if (result == 0) {
                            flag = false;
                            logger.info("Updating mass mail counter to 0 failed")
                            callback(error2,flag);
                        }
                        else{
                            massMailCountCollection.update({userId:userId},{$inc:{count:count}},function(error3,result2){
                                var flag;
                                if (error3) {
                                    logger.info("Error in DB while updating mass mail counter "+error3)
                                    callback(error3,false);
                                }
                                if (result2 == 0) {
                                    flag = false;
                                    logger.info("Updating mass mail counter failed")
                                    callback(error3,flag);
                                }
                                else{
                                    flag = true;
                                    logger.info("Updating mass mail counter Success after date")
                                    callback(error3,flag);
                                }
                            })
                        }
                    })
                }
                else{
                    massMailCountCollection.update({userId:userId},{$inc:{count:count}},function(error,result){
                        var flag;
                        if (error) {
                            logger.info("Error in DB while updating mass mail counter "+error)
                            callback(error,false);
                        }
                        if (result == 0) {
                            flag = false;
                            logger.info("Updating mass mail counter failed")
                            callback(error,flag);
                        }
                        else{
                            flag = true;
                            logger.info("Updating mass mail counter Success before date")
                            callback(error,flag);
                        }
                    })
                }
            }
            else  callback(error1,false);
        }
    })

};

MassMailCount.prototype.removeUserMailCount = function(userId,callback){
    massMailCountCollection.findOneAndRemove({userId:userId},function(error,result){
        if(error){
            callback(false,'onDeleteMailCount',error,result);
        }
        else callback(true,'onDeleteMailCount',error,result);
    })
};

function checkRequired(data){
    if(data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

// export the User EventDataAccess Class
module.exports = MassMailCount;