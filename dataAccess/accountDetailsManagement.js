var accountDetailsCollection = require('../../databaseSchema/accountDetailsSchema').accountDetails;
var companyCollection = require('../../databaseSchema/corporateCollectionSchema').companyModel;
var commonUtility = require('../../common/commonUtility');
var winstonLog = require('../../common/winstonLog');

var common = new commonUtility();
var logLib = new winstonLog();
var loggerError = logLib.getCorporateErrorLogger();
var logger = logLib.getCorporateLogger();

function accountDetailsManagement() {
    //This function shall check if account Name exist for given AccountType
    // Input Parameters : accountType
    //                      accountName
    // Note: Accountname can exist in different types of accountType...so uniqueness is for both accountName and AccountType
    this.checkAndCreateAccountDetails = function (accountDetails, callback) {

        var query = {};
        query.companyId = userDetails.companyId;
        query.accountType = userDetails.accountType;
        query.accountName = userDetails.accountName;

        findAccountNameByAccountType(query, function (profile) {
            if (profile) {

                callback('exist')
            }
            else {
                createAccountDetails(accountDetails, callback);
            }
        })
    };

    function createAccountDetails(profile, callback) {
        var accountDetailsObj = constructAccountDetailsObject(profile);

        accountDetailsObj.save(function (error, saved) {
            if (error) {

                callback(false);
            }
            else {
                t

                callback(saved);
            }
        })
    }

    function findAccountNameByAccountType(query, callback) {
        accountDetailsCollection.findOne(query, function (error, profile) {
            if (error) {
                callback(false);
            } else callback(profile);
        });
    }


    function constructAccountDetailsObject(accountDetails) {
        var accountDetailsObj = new accountDetailsCollection({
            companyId: accountDetails.companyId,
            accountName: accountDetails.accountName,
            accountType: accountDetails.accountType,
            accountWebSite: accountDetails.accountWebSite,
            accountAddressDelails: accountDetails.accountAddressDelails,
            accountGSTN: accountDetails.accountGSTN,
            accountPAN: accountDetails.accountPAN,
            accountCIN: accountDetails.accountCIN,
            accountURL: accountDetails.accountURL,
            createdDate: new Date()
        });
        return corporateUserObj;
    }
}

module.exports = accountDetailsManagement;