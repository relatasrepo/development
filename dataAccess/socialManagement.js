var socialCollection = require('../databaseSchema/socialSchema').socialfeed;
var tweetCollection = require('../databaseSchema/tweetSchema').twitterfeed;

var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var socialLogger = logLib.getActionErrorLogger();
var _ = require("lodash");

var Converter = require("csvtojson").Converter;
var converter = new Converter({});

function SocialManagement(){}

SocialManagement.prototype.fetchFeeds = function(companyName, callBack){
  if(!companyName)
    callBack(new Error("Invalid Company Name"), null)
  else{
    var companyRegex = new RegExp("^"+companyName+"$", "i")
     socialCollection.findOne({entityType: "company", entityName:companyRegex},{newsFeeds: 1, _id: 0}, function(err, result){
      if(err){
        console.log(err)
        callBack(err, null)
      }else{
        if(result)
          callBack(null, result.newsFeeds)
        else
          callBack(null, [])
      }
    })
  }
};

SocialManagement.prototype.storeTweets = function (tweet,twitterUserName,callback) {

    /*
    * Check if tweet exists. If exists, don't update. If not then store.
    */

    tweetCollection.find({$or:[{tweetId:tweet.id,twitterUserName:twitterUserName}]},function (err,tweetExists) {

        if(!err && tweetExists && tweetExists.length === 0){
            var data = {
                tweet: tweet.data,
                twitterUserName: twitterUserName,
                tweetId: tweet.id,
                tweetedDate: new Date(tweet.created_at),
                imageUrl: tweet.imageUrl
            };

            tweetCollection.collection.insert(data,function (err,result) {

                if(!err){
                    if(callback){
                        callback(err,true)
                    }
                } else {
                    socialLogger.info("Error: Could not store tweets storeTweets()",err)
                    if(callback){
                        callback(err,false)
                    }
                }
            });
        } else if(!err && tweetExists.length>0) {
            tweetCollection.remove({twitterUserName:twitterUserName,tweetId:{$ne:tweet.id}},function (error,result) {
                if(error){
                    socialLogger.info("Error: Can not remove old tweets storeTweets()",error)
                }
            })
        }
        else {
            if(callback){
                callback(err,false)
            }
        }
    });
};

SocialManagement.prototype.bulkStoreTweets = function (data,callback) {

    /*
    * Check if tweet exists. If exists, don't update. If not then store.
    */

    var len = data.length;

    var bulkTweetsUpdate = tweetCollection.collection.initializeUnorderedBulkOp();

    for(var i=0;i<len;i++){
        bulkTweetsUpdate.find({twitterUserName: data[i].twitterUserName}).upsert().update({ $set: data[i] });
    }

    if(len>0){
        bulkTweetsUpdate.execute(function(err, data){

            if(err){
                socialLogger.info('Could not store tweets bulkStoreTweets', err);
                if(callback){
                    callback(false)
                }
            }
            else {
                if(callback){
                    callback(true)
                }
                socialLogger.info('Tweets updated successfully- ',data.ok);
            }
        })
    }
};

SocialManagement.prototype.fetchTweet = function (twitterUserName,callback) {

    tweetCollection.find({twitterUserName:twitterUserName}).exec(function(err, tweet) {
        callback(err,tweet[0])
    });
};

SocialManagement.prototype.fetchTweetByEmailId = function (emailId,callback) {

    tweetCollection.find({emailId:emailId}).exec(function(err, tweet) {
        callback(err,tweet[0])
    });
};

SocialManagement.prototype.fetchTweets = function (twitterUserNames,callback) {

    tweetCollection.find({twitterUserName:{$in:twitterUserNames}}).exec(function(err, tweet) {
        callback(err,tweet)
    });
};

SocialManagement.prototype.fetchTweetsForNonTwitterUser = function (emailIds,companies,callback) {

    tweetCollection.find({$or:[{emailId:{$in:emailIds}},{company:{$in:companies}}]}).exec(function(err, tweet) {
        if(!err && tweet){
            callback(err,JSON.parse(JSON.stringify(tweet)))
        } else {
            callback(err,[])
        }
    });
};

SocialManagement.prototype.fetchTwitterUserLocation = function (twitterUserNames,locations,callback) {

    var query = { "$or": locations.map(function(el) {
        el["userLocation"] = {'$regex' : '.*' + el.loc + '.*',"$options": 'i'};
        el["twitterUserName"] = {$in:twitterUserNames}
        delete  el.loc
        return el;
    })};

    tweetCollection.find(query,{emailId:1,userLocation:1}).exec(function(err, tweet) {
        if(!err && tweet){
            callback(err,JSON.parse(JSON.stringify(tweet)))
        } else {
            callback(err,[])
        }
    });
};

SocialManagement.prototype.csvToJSON = function (fileLocation,callback) {
    converter.fromFile(fileLocation,function(err,result){
        callback(err,result)
    });
};

module.exports = SocialManagement;

