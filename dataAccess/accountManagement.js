var InteractionsCollection = require('../databaseSchema/userManagementSchema').interactions
var NewInteractionsCollection = require('../databaseSchema/interactionSchema').interactionsV2
var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var userRelationCollection = require('../databaseSchema/userRelatonShipSchema').userRelation;
var accountsCollection = require('../databaseSchema/accountSchema').accounts;
var manualAccountsCollection = require('../databaseSchema/manualAccountSchema').manualAccounts;
var accountsLogCollection = require('../databaseSchema/accountLog').accountsLog;

var async = require("async");
var mongoose = require('mongoose');
var winstonLog = require('../common/winstonLog');
var Mailer = require('../common/mailer');

var logLib = new winstonLog();
var mailObj = new Mailer();

var logger =logLib.getInteractionsErrorLogger();
var _ = require("lodash")
    , moment = require("moment")

var momentTZ = require('moment-timezone');

var redis = require('redis');
var redisClient = redis.createClient();

function AccountManagement(){}

AccountManagement.prototype.getInteractionsByDateRange = function (userIds,dateMin,dateMax,contacts,serviceLogin,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};

        obj["$or"] = [
            {"emailId":{$in:contacts}}
        ];

        if(dateMin && dateMax){
            obj["interactionDate"] = {$gte: new Date(moment().subtract(60, "days")),$lte: new Date()};
        } else {
            obj["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }
        
        return obj;
    })};
    
    if(companyId){
        query = {
            companyId:companyId,
            userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(dateMin && dateMax){
            query["interactionDate"] = {$gte: new Date(moment().subtract(60, "days"))}
        } else {
            query["interactionDate"] = {
                $gte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }
    
    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {$sort:{interactionDate:-1}}
    ]).exec(function (err,results) {
        callback(err,formatForFutureMeetingsAndPastInteractions(results))
    });

};

AccountManagement.prototype.getInteractionsByDateRange_new = function (userIds,dateMin,dateMax,contacts,serviceLogin,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};

        obj["$or"] = [
            {"emailId":{$in:contacts}}
        ];

        if(dateMin && dateMax){
            obj["interactionDate"] = {$gte: new Date(moment().subtract(60, "days")),$lte: new Date()};
        } else {
            obj["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        query = {
            companyId:companyId,
            emailId:{$in:contacts}
        }

        if(dateMin && dateMax){
            query["interactionDate"] = {$gte: new Date(moment().subtract(60, "days"))}
        } else {
            query["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }

    NewInteractionsCollection.aggregate([
        {
            $match: query
        },
        {$sort:{interactionDate:-1}},
        {
            $group: {
                _id:"$refId",
                interactionDate: { "$first": "$interactionDate" },
                interactionType: { "$first": "$interactionType" },
                title: { "$first": "$title" },
                action: { "$first": "$action" },
                refId: { "$first": "$refId" },
                emailId: { "$first": "$emailId" },
                ignore: { "$first": "$ignore" },
                trackInfo: { "$first": "$trackInfo" },
                description: { "$first": "$description" },
                subject: { "$first": "$subject" },
                ownerEmailId: { "$first": "$ownerEmailId" },
            }
        }
    ]).exec(function (err,results) {
        callback(err,formatForFutureMeetingsAndPastInteractions(results))
    });

};

AccountManagement.prototype.getInteractionsCountForUsers = function (userIds,dateMin,dateMax,contacts,serviceLogin,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};

        obj["$or"] = [
            {"emailId":{$in:contacts}}
        ];

        if(dateMin && dateMax){
            obj["interactionDate"] = {$gte: new Date(moment().subtract(60, "days")),$lte: new Date()};
        } else {
            obj["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        query = {
            companyId:companyId,
            emailId:{$in:contacts}
        }

        if(dateMin && dateMax){
            query["interactionDate"] = {$gte: new Date(moment().subtract(60, "days"))}
        } else {
            query["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }

    NewInteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group:{
                _id:"$ownerEmailId",
                count:{$sum:1},
                lastInteractedDate: { $max: "$interactionDate" }
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.getInteractionsPaginated = function (userIds,dateMin,dateMax,skip,contacts,serviceLogin,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};
        obj["$or"] = [
            {"emailId":{$in:contacts}}
        ];

        if(dateMin && dateMax){
            obj["interactionDate"] = {
                $gte: new Date(dateMin),
                // $lte: new Date(dateMax)
                $lte: new Date()
            }
        } else {
            obj["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        query = {
            companyId:companyId,
            userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(dateMin && dateMax){
            query["interactionDate"] = {$gte: new Date(dateMin),$lte: new Date()}
        } else {
            query["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }
    
    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {$sort:{interactionDate:-1}},
        {$skip:skip},
        {$limit:25}
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.getInteractionsPaginated_new = function (userIds,dateMin,dateMax,skip,contacts,serviceLogin,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};
        obj["$or"] = [
            {"emailId":{$in:contacts}}
        ];

        if(dateMin && dateMax){
            obj["interactionDate"] = {
                $gte: new Date(dateMin),
                // $lte: new Date(dateMax)
                $lte: new Date()
            }
        } else {
            obj["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        query = {
            companyId:companyId,
            userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(dateMin && dateMax){
            query["interactionDate"] = {$gte: new Date(dateMin),$lte: new Date()}
        } else {
            query["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }
    
    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {$sort:{interactionDate:-1}},
        {$skip:skip},
        {$limit:25}
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.getInteractionsCount = function (liu,account,userIds,dateMin,dateMax,skip,contacts,serviceLogin,companyId,callback) {

    var key = liu+account+"accountInteractionsCount";

    getAccInteractionsCount(key,function (count) {
        if(count){
            callback(null,count);

        } else {
            storeAccInteractionsCount(key,userIds,dateMin,dateMax,skip,contacts,serviceLogin,companyId,callback)
        }
    })

};

AccountManagement.prototype.getInteractionsCount_new = function (liu,account,userIds,dateMin,dateMax,skip,contacts,serviceLogin,companyId,callback) {

    var key = liu+account+"accountInteractionsCount"+"old";

    getAccInteractionsCount(key,function (count) {
        if(count){
            callback(null,count);

        } else {
            storeAccInteractionsCount_new(key,userIds,dateMin,dateMax,skip,contacts,serviceLogin,companyId,callback)
        }
    })

};

function storeAccInteractionsCount(key,userIds,dateMin,dateMax,skip,contacts,serviceLogin,companyId,callback){
    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};
        obj["emailId"] = {$in:contacts}

        if(dateMin && dateMax){
            obj["interactionDate"] = {
                $gte: new Date(dateMin),
                $lte: new Date()
            }
        } else {
            obj["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        query = {
            companyId:companyId,
            userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(dateMin && dateMax){
            query["interactionDate"] = {$gte: new Date(dateMin),$lte: new Date()}
        } else {
            query["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }
    
    InteractionsCollection.find(query).count(function(err, count){
        redisClient.setex(key, 3600, JSON.stringify(count));
        callback(err,count)
    });

}

function storeAccInteractionsCount_new(key,userIds,dateMin,dateMax,skip,contacts,serviceLogin,companyId,callback){
    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};
        obj["emailId"] = {$in:contacts}

        if(dateMin && dateMax){
            obj["interactionDate"] = {
                $gte: new Date(dateMin),
                $lte: new Date()
            }
        } else {
            obj["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        query = {
            companyId:companyId,
            // userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(dateMin && dateMax){
            query["interactionDate"] = {$gte: new Date(dateMin),$lte: new Date()}
        } else {
            query["interactionDate"] = {
                $lte: new Date()
            }
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }
    
    NewInteractionsCollection.find(query).count(function(err, count){
        redisClient.setex(key, 3600, JSON.stringify(count));
        callback(err,count)
    });

}

function getAccInteractionsCount(key,callback){

    redisClient.get(key, function (error, cacheResult) {
        try {
            var data = JSON.parse(cacheResult);
            callback(data)
        } catch (err) {
            callback(null)
        }
    });
}

AccountManagement.prototype.updateAccountDetails = function (findQuery,updateObj,callback) {
    findAndUpdateTemplate(findQuery,updateObj,callback)
};

function findAndUpdateTemplate(findQuery,updateObj,callback){
    accountsCollection.update(findQuery,updateObj,{upsert:true},function (err,result) {
        if(callback){
            callback(err,result)
        }
    });
}

AccountManagement.prototype.getAllAccounts = function(findQuery,sortOption,callback){
    accountsCollection.find(findQuery).sort(sortOption).exec(function (err,result) {
        callback(err,result)
    })
}

AccountManagement.prototype.getMasterAccTypesByGroup = function(companyId,callback){

    var query = {
        companyId:companyId
    }

    manualAccountsCollection.aggregate([
        {
            $match: query
        },
        {
            $group:{
                _id:"$group"
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });
}


AccountManagement.prototype.getAllSystemReferencedManualAccounts = function (companyId,callback) {

    var query = {
        companyId:companyId
    }

    var projectQuery = {
        _id:0,
        'Account Name':"$name",
        'Domain':"$domain",
        'Branch':"$branch",
        'Address':"$address",
        'Phone':"$phone"
    };

    manualAccountsCollection.aggregate([
        {
            $match:query
        },
        {
            $project:projectQuery
        }
    ],function(error, result){
        if(error){
            loggerError.info('Error in getAllSystemReferencedManualAccount ',error)
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

AccountManagement.prototype.getMasterAccsForGroup = function(companyId,group,callback){

    var query = {
        companyId:companyId,
        group:group
    }

    manualAccountsCollection.find(query,{name:1}).lean().exec(function (err,results) {
        callback(err,results)
    });
}

AccountManagement.prototype.updateRelationship = function (updateObj,common,companyId,callback){

    var findQuery = {
        companyId:common.castToObjectId(companyId),
        name:updateObj.name,
        hierarchy: { $elemMatch: { emailId: updateObj.emailId} }
    }

    var updateQuery = {
        $set:{
            "hierarchy.$.relationshipType": updateObj.relationshipType
        }
    }

    accountsCollection.update(findQuery,updateQuery,function (err,result) {
        callback(err,result)
    })

    var logs = [];
    logs.push(buildAccountLog(companyId,updateObj.name,updateObj.liuEmailId,"",'updateRelationship',updateObj.relationshipType,updateObj.emailId));
    createAccountLogs(logs);
};

AccountManagement.prototype.updateParentAndPath = function (updateObj,companyId,callback){

    var findQuery = {
        companyId:companyId,
        name:updateObj.name,
        hierarchy: { $elemMatch: { emailId: updateObj.emailId} }
    }

    var updateQuery = {
        $set:{
            "hierarchy.$.hierarchyParent": updateObj.hierarchyParent,
            "hierarchy.$.hierarchyPath": updateObj.hierarchyPath
        }
    }

    accountsCollection.update(findQuery,updateQuery,function (err,result) {
        callback(err,result)
    });

    var logs = [];
    logs.push(buildAccountLog(companyId,updateObj.name,updateObj.liuEmailId,"",'updateParent',updateObj.hierarchyParent,updateObj.emailId));
    createAccountLogs(logs);
};

AccountManagement.prototype.updateContactDetails = function (updateObj,companyId,callback){

    if(updateObj){

        var findQuery = {
            companyId:companyId,
            name:updateObj.name,
            hierarchy: { $elemMatch: { emailId: updateObj.emailId} }
        }

        var updateQuery = {
            $set:{
                "hierarchy.$.fullName": updateObj.fullName,
                "hierarchy.$.designation": updateObj.designation
            }
        }

        accountsCollection.update(findQuery,updateQuery,function (err,result) {
            callback(err,result)
        })

        var logs = [];

        logs.push(buildAccountLog(companyId,updateObj.name,updateObj.liuEmailId,"",'updateContactName',updateObj.fullName,updateObj.emailId));
        logs.push(buildAccountLog(companyId,updateObj.name,updateObj.liuEmailId,"",'updateContactDesignation',updateObj.designation,updateObj.emailId));
        createAccountLogs(logs);
    } else {
        callback(null,null)
    }
};

AccountManagement.prototype.updateHierarchyType = function (updateObj,common,companyId,callback){

    var findQuery = {
        companyId:common.castToObjectId(companyId),
        name:updateObj.name,
        hierarchy: { $elemMatch: { emailId: updateObj.emailId} }
    }

    var updateQuery = {
        $set:{
            "hierarchy.$.type": updateObj.type
        }
    }

    accountsCollection.update(findQuery,updateQuery,function (err,result) {
        callback(err,result)
    })

    var logs = [];
    logs.push(buildAccountLog(companyId,updateObj.name,updateObj.liuEmailId,"",'updateType',updateObj.type,updateObj.emailId));
    createAccountLogs(logs);
};

AccountManagement.prototype.removeFromHierarchy = function (data,companyId,callback){

    var bulk = accountsCollection.collection.initializeUnorderedBulkOp();
    var logs = [];
    data.forEach(function(a){

        logs.push(buildAccountLog(companyId,a.name,a.liuEmailId,"",'removeFromHierarchy',a.type,a.emailId));

        bulk.find({
            companyId:companyId,
            name:a.name,
            "hierarchy.emailId":a.emailId,
        }).update({
            $set:{
                "hierarchy.$.hierarchyParent": null,
                "hierarchy.$.hierarchyPath": null,
                "hierarchy.$.type": null,
                "hierarchy.$.relationshipType": null,
                "hierarchy.$.orgHead": a.orgHead
            }});

        var id = a.emailId.split("@")
        var newId = "";
        _.each(id,function (el) {
            newId = newId+el
        });

        bulk.find({
            companyId:companyId,
            name:a.name,
            "hierarchy.hierarchyParent":a.hierarchyParent
        }).update({
            $set:{
                "hierarchy.$.hierarchyParent": null,
                "hierarchy.$.hierarchyPath": null,
                "hierarchy.$.type": null,
                "hierarchy.$.relationshipType": null,
                "hierarchy.$.orgHead": a.orgHead
            }});

        if(a.orgHeadRm){
            bulk.find({
                companyId:companyId,
                name:a.name
            }).update({
                $set:{
                    "hierarchy": []
                }});
        }
    });

    if(data.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                callback(err,false)
            }
            else {
                result = result.toJSON();
                logger.info('addUserToHierarchy() results ',result);
                callback(err,result.upserted)
            }
        });
    }
    else{
        callback(null,null)
    }

    createAccountLogs(logs);

}

AccountManagement.prototype.addUserToHierarchy = function (updateObj,common,companyId,callback){

    if(updateObj){
        var data = [{
            emailId:updateObj.emailId,
            hierarchyParent: updateObj.hierarchyParent,
            hierarchyPath:updateObj.hierarchyPath,
            orgHead:false,
        }];

        accountsCollection.findOne({ companyId: common.castToObjectId(companyId),name:updateObj.name, hierarchy: { $elemMatch: { emailId: updateObj.emailId} } },function (err,existingHiearchy) {

            if(existingHiearchy){

                var bulk = accountsCollection.collection.initializeUnorderedBulkOp();

                data.forEach(function(a){

                    bulk.find({
                        companyId: common.castToObjectId(companyId),
                        name:updateObj.name,
                        "hierarchy.emailId":a.emailId
                    }).updateOne({
                        $set:{
                            "hierarchy.$.hierarchyParent":a.hierarchyParent,
                            "hierarchy.$.hierarchyPath":a.hierarchyPath,
                            "hierarchy.$.orgHead":false
                        }});
                });

                if(data.length > 0) {
                    bulk.execute(function (err, result) {
                        if (err) {
                            logger.info('Error in addUserToHierarchy() ',err );
                            callback(false)
                        }
                        else {
                            result = result.toJSON();
                            logger.info('addUserToHierarchy() results ',result);
                            callback(result.upserted)
                        }
                    });
                }
                else{
                    callback(true)
                }
            } else {
                accountsCollection.update({ companyId: common.castToObjectId(companyId),name:updateObj.name }, { $push: { hierarchy: { $each:data } } }, function(error, result) {
                    callback(true);
                })
            }
        });
        var logs = [];
        logs.push(buildAccountLog(companyId,updateObj.name,updateObj.liuEmailId,"",'addUserToHierarchy',updateObj.emailId,updateObj.emailId));
        createAccountLogs(logs);
    } else {
        callback()
    }

}

AccountManagement.prototype.setOrgHead = function (updateObj,common,companyId,callback){

    var data = [{
        hierarchyParent: null,
        hierarchyPath:null,
        orgHead:true,
        emailId:updateObj.emailId
    }];

    var bulk = accountsCollection.collection.initializeUnorderedBulkOp();

    data.forEach(function(a){

        bulk.find({
            companyId:common.castToObjectId(companyId),
            name:updateObj.name,
            "hierarchy.emailId":a.emailId
        }).updateOne({
            $set:{
                "hierarchy.$.hierarchyParent":null,
                "hierarchy.$.hierarchyPath":null,
                "hierarchy.$.orgHead":true,
                "hierarchy.$.emailId":a.emailId
            }});
        });


    if(updateObj.hierarchy && updateObj.hierarchy.length>0) {

        if(data.length > 0) {
            bulk.execute(function (err, result) {
                if (err) {
                    logger.info('Error in setOrgHead() ',err );
                    callback(false)
                }
                else {
                    result = result.toJSON();
                    logger.info('setOrgHead() results ',result);
                    callback(result.upserted)
                }
            });
        }
        else{
            callback(true)
        }
    } else {
        accountsCollection.update({ companyId:common.castToObjectId(companyId), name:updateObj.name,},{$push: { hierarchy: { $each:data } }},function (err,results) {
            logger.info('setOrgHead() results ',results);
            callback(true)
        })
    }

    var logs = [];
    logs.push(buildAccountLog(companyId,updateObj.name,updateObj.liuEmailId,"",'accountOrgHead',updateObj.emailId,updateObj.emailId));
    createAccountLogs(logs);
}

AccountManagement.prototype.getLimitedAccounts = function(findQuery,sortOption,skip,callback){
    accountsCollection.find(findQuery).sort(sortOption).skip(skip).limit(25).exec(function (err,result) {
        callback(err,result)
    })
}

AccountManagement.prototype.findTemplate = function(findQuery,callback){
    accountsCollection.findOne(findQuery,function (err,result) {
        callback(err,result)
    })
}

function addContactsToAccount(companyId,contacts,callback) {

    var bulk = accountsCollection.collection.initializeUnorderedBulkOp();

    var counter = 0;
    _.each(contacts,function (co) {

        var accountName = fetchCompanyFromEmail(co);

        if(accountName){
            bulk.find({companyId:companyId,name:accountName.toLowerCase()})
                .update({$addToSet:{contacts: co}});

            counter++
        }
    });

    if(counter>0){
        bulk.execute(function (err, result) {
            if(callback){
                callback(err,result)
            }
        });
    }

}

AccountManagement.prototype.getInteractionsByAccountMembers = function (userIds,contacts,from,to,serviceLogin,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["action"] = "sender";
        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};

        obj["emailId"] = {$in:contacts}

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        query = {
            action:"sender",
            companyId:companyId,
            userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(from && to){
            query["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        { $sort: { 'interactionDate': 1 } },
        {
            $group:{
                _id:"$emailId",
                count:{$sum:1},
                lastInteractedDate: { $last: "$interactionDate" }
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.getInteractionsByTeamMembers = function (userIds,contacts,from,to,serviceLogin,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["action"] = "receiver";
        obj["userId"] = {$ne:el};
        // obj["$or"] = [
        //     {"emailId":new RegExp('.'+account+'.', "i")},
        //     {"emailId":new RegExp('@'+account, "i")}
        // ]

        obj["emailId"] = {$in:contacts}

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        query = {
            action:"receiver",
            companyId:companyId,
            userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(from && to){
            query["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }
    
    InteractionsCollection.aggregate([
        {
            $match: query
        },
        { $sort: { 'interactionDate': 1 } },
        {
            $group:{
                _id:"$ownerEmailId",
                count:{$sum:1},
                lastInteractedDate: { $last: "$interactionDate" }
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.getInteractionsByAccountMembers_new = function (userIds,contacts,from,to,serviceLogin,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["action"] = "sender";
        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};

        obj["emailId"] = {$in:contacts};
        obj["interactionDate"] = {$lte: new Date()};

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        query = {
            action:"receiver",
            companyId:companyId,
            // userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(from && to){
            query["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }

    NewInteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id:"$refId",
                interactionDate: { "$first": "$interactionDate" },
                emailId: { "$first": "$emailId" }
            }
        },
        {
            $group:{
                _id:"$emailId",
                count:{$sum:1},
                lastInteractedDate: { $first: "$interactionDate" }
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.getInteractionsByTeamMembers_new = function (userIds,contacts,from,to,serviceLogin,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["action"] = "receiver";
        obj["userId"] = {$ne:el};
        obj["emailId"] = {$in:contacts}
        obj["interactionDate"] = {$lte: new Date()};

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                obj["source"] = {$ne:"google"};
            } else {
                obj["source"] = {$ne:"outlook"};
            }
        }

        return obj;
    })};

    if(companyId){
        query = {
            action:"sender",
            companyId:companyId,
            emailId:{$in:contacts}
        }

        if(from && to){
            query["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        if(serviceLogin){

            if(serviceLogin == "outlook"){
                query["source"] = {$ne:"google"};
            } else {
                query["source"] = {$ne:"outlook"};
            }
        }
    }

    NewInteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id:"$refId",
                interactionDate: { "$first": "$interactionDate" },
                ownerEmailId: { "$first": "$ownerEmailId" }
            }
        },
        {
            $group:{
                _id:"$ownerEmailId",
                count:{$sum:1},
                lastInteractedDate: { $first: "$interactionDate" }
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.getLastInteraction = function (userIds,contacts,from,to,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["action"] = "receiver";
        obj["userId"] = {$ne:el};
        obj["emailId"] = {$in:contacts};
        // obj["$or"] = [
        //     {"emailId":new RegExp('.'+account+'.', "i")},
        //     {"emailId":new RegExp('@'+account, "i")}
        // ]

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        return obj;
    })};

    if(companyId){
        query = {
            action:"receiver",
            companyId:companyId,
            userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(from && to){
            query["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }
    }
    
    InteractionsCollection.aggregate([
        {
            $match:query
        },
        { $sort: { 'interactionDate': 1 } },
        {
            $group:
            {
                _id: '$sentiment',
                lastInteractedType:{$last:"$interactionType"},
                lastInteractedDate: { $last: "$interactionDate" },
                action: { $last: "$action" },
                firstName:{$last:"$firstName"},
                emailId:{$last:"$emailId"},
                lastName:{$last:"$lastName"}
            }
        }
    ]).exec(function(err,result){

        if(!err){
            callback(err,result)
        } else {
            callback(err,[])
        }
    });
}

AccountManagement.prototype.getLastInteraction_new = function (userIds,contacts,from,to,companyId,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["action"] = "receiver";
        obj["userId"] = {$ne:el};
        obj["emailId"] = {$in:contacts};
        // obj["$or"] = [
        //     {"emailId":new RegExp('.'+account+'.', "i")},
        //     {"emailId":new RegExp('@'+account, "i")}
        // ]

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        return obj;
    })};

    if(companyId){
        query = {
            action:"receiver",
            companyId:companyId,
            // userId:{$nin:userIds},
            emailId:{$in:contacts}
        }

        if(from && to){
            query["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        // if(serviceLogin){
        //
        //     if(serviceLogin == "outlook"){
        //         query["source"] = {$ne:"google"};
        //     } else {
        //         query["source"] = {$ne:"outlook"};
        //     }
        // }
    }

    NewInteractionsCollection.aggregate([
        {
            $match:query
        },
        { $sort: { 'interactionDate': 1 } },
        {
            $group:
            {
                _id: '$sentiment',
                lastInteractedType:{$last:"$interactionType"},
                lastInteractedDate: { $last: "$interactionDate" },
                action: { $last: "$action" },
                firstName:{$last:"$firstName"},
                emailId:{$last:"$emailId"},
                lastName:{$last:"$lastName"}
            }
        }
    ]).exec(function(err,result){

        if(!err){
            callback(err,result)
        } else {
            callback(err,[])
        }
    });
}

AccountManagement.prototype.getLastInteractionThread = function (userIds,account,from,to,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["interactionType"] = "email";
        obj["userId"] = {$ne:el};
        obj["emailId"] = new RegExp('@'+account, "i")

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match:query
        },
        { $sort: { 'interactionDate': 1 } },
        {
            $group: {
                _id: '$emailThreadId',
                lastInteractedDate: { $last: "$interactionDate" },
                action: { $last: "$action" },
                firstName:{$last:"$firstName"},
                emailId:{$last:"$emailId"},
                ownerEmailId:{$last:"$ownerEmailId"},
                lastName:{$last:"$lastName"}
            }
        }
    ]).exec(function(err,result){

        if(!err){

            callback(err,getMailsNotRespondedTo(result))
        } else {
            callback(err,[])
        }
    });
}

function getMailsNotRespondedTo(data){

    var mailsNotResponded = [];
    _.each(data,function (el) {

        if(el.emailId != el.ownerEmailId){
            mailsNotResponded.push(el)
        }
    });

    return data;
}

AccountManagement.prototype.getLastInteractedDate = function (userIds,account,from,to,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["$or"] = [
            {"emailId":new RegExp('.'+account+'.', "i")},
            {"emailId":new RegExp('@'+account, "i")}
        ]

        if(from && to){
            obj["interactionDate"] = {$gte: new Date(from),$lte: new Date(to)}
        }

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match:query
        },
        { $sort: { 'interactionDate': 1 } },
        {
            $group:
            {
                _id: '$ownerEmailId',
                lastInteractedType:{$last:"$interactionType"},
                lastInteractedDate: { $last: "$interactionDate" },
                action: { $last: "$action" },
                firstName:{$last:"$firstName"},
                emailId:{$last:"$emailId"},
                lastName:{$last:"$lastName"}
            }
        }
    ]).exec(function(err,result){

        if(!err){
            callback(err,result)
        } else {
            callback(err,[])
        }
    });
}

AccountManagement.prototype.getInteractionsByContacts = function (userIds,contacts,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};
        obj["emailId"] = {$in:contacts}

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group:{
                // _id:"$ownerEmailId",
                _id:null,
                count:{$sum:1}
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.getInteractionsByContacts_new = function (userIds,contacts,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};
        obj["emailId"] = {$in:contacts}

        return obj;
    })};

    NewInteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $group:{
                // _id:"$ownerEmailId",
                _id:null,
                count:{$sum:1}
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.getLastInteractedDateByContacts = function (userIds,contacts,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};
        obj["emailId"] = {$in:contacts}

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        { $sort: { 'interactionDate': 1 } },
        {
            $group:
            {
                _id: '$emailId',
                lastInteractedType:{$last:"$interactionType"},
                lastInteractedDate: { $last: "$interactionDate" },
                action: { $last: "$action" },
                firstName:{$last:"$firstName"},
                emailId:{$last:"$emailId"},
                lastName:{$last:"$lastName"}
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.recentlyInteractedAccounts = function (userIds,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el;
        obj["userId"] = {$ne:el};
        obj["interactionDate"] = {$gte: new Date(moment().subtract(14, "days")),$lte: new Date()};

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        { $sort: { 'interactionDate': 1 } },
        {
            $group:
            {
                _id: '$companyName',
                lastInteractedType:{$last:"$interactionType"},
                lastInteractedDate: { $last: "$interactionDate" },
                action: { $last: "$action" },
                companyName: { $last: "$companyName" },
                firstName:{$last:"$firstName"},
                emailId:{$last:"$emailId"},
                lastName:{$last:"$lastName"}
            }
        }
    ]).exec(function (err,results) {
        callback(err,results)
    });

};

AccountManagement.prototype.getInteractionsByCxo = function (userIds,account,callback) {

    var query = { "$or": userIds.map(function(el) {
        var obj = {};
        obj["_id"] = el;
        obj["contacts.personEmailId"] = new RegExp('@'+account, "i")

        return obj;
    })};

    myUserCollection.find(query, { "contacts.$": 1 }, function(error, contacts) {
        callback(error,contacts)
    });

};

AccountManagement.prototype.accountRelationshipRelevance = function (users,fromDate,toDate,timezone,contacts,callback) {
    accountRelationshipRelevance(users,fromDate,toDate,timezone,contacts,callback)
}

AccountManagement.prototype.updateAccounts = function (companyId,accountsObj,contacts,callback) {

    removeAccountDuplicates(companyId,accountsObj,function (err,nonExistingAccounts) {

        if(!err && nonExistingAccounts){
            updateAccounts(nonExistingAccounts,function (err,result) {
                addContactsToAccount(companyId,contacts,function () {
                    if(callback){
                        callback()
                    }
                })
            })
        } else {
            addContactsToAccount(companyId,contacts,function () {
                if(callback){
                    callback()
                }
            })
        }
    })
}

AccountManagement.prototype.requestAccess = function (companyId,account,requester,callback) {

    var requesterObj = {
        emailId:requester,
        date:new Date()
    };

    accountsCollection.update({companyId:companyId,name:account},{$addToSet:{accountAccessRequested:requesterObj}},{upsert:true},function (err,result) {

        if(!err){
            // notifyViaEmail(message,user)
        }

        callback(err,result)
    });
}

AccountManagement.prototype.getRecentlyInteractedContactsForAccounts = function (query,callback) {

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $project: {
                emailId:"$emailId",
                interactionDate: "$interactionDate",
                interactionType: "$interactionType",
                action: "$action"
            }
        },
        {$sort: {'interactionDate': 1}},
        {
            $group: {
                _id: "$emailId",
                lastInteractedDate: {$max: "$interactionDate"},
                interactionType: {$last: "$interactionType"},
                interactionAction: {$last: "$action"}
            }
        }
    ]).exec(function (err, contacts) {
        if (!err) {
            callback(contacts)
        } else {
            callback([])
        }
    });
}

function notifyViaEmail(message,user,callback){
    mailObj.send(message,user,callback)
}

AccountManagement.prototype.grantAccess = function (companyId,account,accessTo,accessGivenBy,callback) {
    accountsCollection.update({companyId:companyId,
        name:account,
        "access.emailId" : {$ne : accessTo }
        },
        {
            $addToSet:{"access":{emailId:accessTo}},
            $pull: { "accountAccessRequested" : { emailId: accessTo } }
        }
        ,function (err,result) {
            if(!err && result){
                var logs = [];
                logs.push(buildAccountLog(companyId,account,accessGivenBy,accessTo,'accountAccessGranted'));
                createAccountLogs(logs)
            }
        callback(err,result)
    });
}

AccountManagement.prototype.grantAccessBulk = function (companyId,accounts,accessGivenBy,callback) {

    var bulk = accountsCollection.collection.initializeUnorderedBulkOp();
    var counter = 0;
    var logs = [];

    _.each(accounts,function (co) {

        if(co.accessStatus){

            logs.push(buildAccountLog(companyId,co.name.toLowerCase(),accessGivenBy,co.emailId,'accountAccessGranted'));

            bulk.find({companyId:companyId,name:co.name.toLowerCase(),"access.emailId" : {$ne : co.emailId }})
                .update({
                    $addToSet:{"access":{emailId:co.emailId}},
                    $pull: { "accountAccessRequested" : { emailId: co.emailId } }
                });

            counter++
        }
    });

    if(counter>0){
        bulk.execute(function (err, result) {
            createAccountLogs(logs);
            callback(err,result)
        });
    } else {
        callback(null,null)
    }
}

AccountManagement.prototype.transferOwnershipBulk = function (companyId,accounts,transferredBy,transferTo,callback) {

    var logs = [];
    var bulk = accountsCollection.collection.initializeUnorderedBulkOp();
    var bulkUpdateAccessArray = accountsCollection.collection.initializeUnorderedBulkOp();
    var counter = 0;

    _.each(accounts,function (co) {
        logs.push(buildAccountLog(companyId,co.fullName.toLowerCase(),transferredBy,transferTo.emailId,'accountTransferred'));

        bulk.find({companyId:companyId,name:co.fullName.toLowerCase()})
            .update({
                $set:{
                    "ownerId":transferTo.userId,
                    "ownerEmailId":transferTo.emailId
                },
                $pull: { "access" : { emailId: co.ownerEmailId } }
            });

        bulkUpdateAccessArray.find({companyId:companyId,name:co.fullName.toLowerCase()})
            .update({
                $addToSet: { "access" : { emailId: transferTo.emailId } }
            });

        counter++
    });

    if(counter>0){
        bulk.execute(function (err, result) {
            createAccountLogs(logs)
            bulkUpdateAccessArray.execute(function (err1,result1) {

            });
            callback(err,result)
        });
    } else {
        callback(null,null)
    }
}

AccountManagement.prototype.revokeAccess = function (companyId,account,accessTo,accessGivenBy,callback) {
    accountsCollection.update({companyId:companyId, name:account},
        {
            $pull: { "access" : { emailId: accessTo } }
        }
        ,function (err,result) {
            if(!err && result){
                var logs = [];
                logs.push(buildAccountLog(companyId,account,accessGivenBy,accessTo,'accountAccessRevoked'));
                createAccountLogs(logs)
            }
        callback(err,result)
    });
}

AccountManagement.prototype.activityLog = function (companyId,account,callback) {

    accountsLogCollection.find({$or:[{companyId:companyId},{companyId:String(companyId)}],accountName:account},function (err,result) {
        callback(err,result)
    })
}

AccountManagement.prototype.updateLastInteractedDate = function (companyId,interactions) {

    var bulk = accountsCollection.collection.initializeUnorderedBulkOp();

    var counter = 0;
    var interactionsData = [];

    _.each(interactions,function (el) {
        if(el.ownerEmailId != el.emailId && el.ownerId != el.userId){
            interactionsData.push(el)
        }
    });

    _.each(interactionsData,function (el) {

        var accountName = fetchCompanyFromEmail(el.emailId)
        if(accountName){
            bulk.find({companyId:companyId,name:accountName.toLowerCase()})
                .update({$set:{lastInteractedDate: new Date(el.interactionDate)}});

            counter++
        }
    });

    if(counter>0){
        bulk.execute(function (err, result) {
        });
    }
}

AccountManagement.prototype.updateManualAccounts = function (companyId,accounts,group,callback) {

    _.each(accounts,function (ac) {
        ac.companyId = companyId;
        ac.group = group;
        ac.createdDate = new Date();
    });

    manualAccountsCollection.collection.insert(accounts,function (err,result) {
        callback(err,result)
    })
}

AccountManagement.prototype.getManualAccounts = function (companyId,skip,search,accountType,callback) {

    var findQuery = {companyId:companyId};

    if(search){
        findQuery = {
            companyId:companyId,
            name:new RegExp(search,"i")
        }
    }

    if(accountType){
        findQuery.group = accountType
    }

    manualAccountsCollection.find(findQuery).sort({name:1}).skip(skip).limit(50).lean().exec(function (err,result) {
        callback(err,result)
    });
}

AccountManagement.prototype.updateAccountName = function (companyId,acccount,castToObjectId,callback) {

    var findQuery = {companyId:companyId,_id:castToObjectId(acccount._id)};

    manualAccountsCollection.update(findQuery,{$set:{name:acccount.name}}).exec(function (err,result) {
        callback(err,result)
    });
}

AccountManagement.prototype.deleteAccount = function (companyId,group,callback) {

    var findQuery = {companyId:companyId,group:group};

    manualAccountsCollection.remove(findQuery).exec(function (err,result) {
        callback(err,result)
    });
}

function buildAccountLog(companyId,account,fromEmailId,toEmailId,type,updateValue,updateFor){
    return {
        companyId: companyId,
        date:new Date(),
        accountName:account,
        fromEmailId:fromEmailId,
        toEmailId:toEmailId,
        type: type,
        updateValue:updateValue?updateValue:null,
        updateFor:updateFor?updateFor:null
    };
}

function comparer(otherArray){
    return function(current){
        return otherArray.filter(function(other){
                return other.name == current.name
            }).length == 0;
    }
}

function createAccountLogs(logs,callback){
    accountsLogCollection.collection.insert(logs,function (err,result) {
        logger.info('results createAccountLogs() ',result );
        if(callback){
            callback(err,result)
        }
    })
}

function removeAccountDuplicates(companyId,accountsObj,callback){

    var accounts = accountsObj.accounts
    var names = accountsObj.names

    accountsCollection.find({companyId:companyId,name:{$in:names}},{name:1},function (err,existingAccounts) {
        var nonExistingAccounts = getNonExistingAccounts(existingAccounts,accounts)
        callback(err,_.uniq(nonExistingAccounts,"name"));
    })
}

function getNonExistingAccounts(existingAccounts,toBeUpdatedAccounts){

    var nonExistingAccounts = [];
    var onlyInA = existingAccounts.filter(comparer(toBeUpdatedAccounts));
    var onlyInB = toBeUpdatedAccounts.filter(comparer(existingAccounts));

    nonExistingAccounts = onlyInA.concat(onlyInB);

    return nonExistingAccounts;

}

function updateAccounts(accounts,callback){

    _.each(accounts,function (el) {
        el.companyId = castToObjectId(el.companyId)
        el.ownerId = castToObjectId(el.ownerId)
        el.createdDate = new Date(el.createdDate)
    });

    if(accounts && accounts.length>0){
        accountsCollection.collection.insert(accounts,function (err,result) {
            logger.info('Results updateAccounts() ',result );
            if(callback){
                callback(err,result)
            }
        })
    } else {
        if(callback){
            callback(null,null)
        }
    }
}

function accountRelationshipRelevance(users,fromDate,toDate,timezone,contacts,callback){

    // var monthYears = enumerateDaysBetweenDates(fromDate,toDate,timezone);

    userRelationCollection.aggregate([
        {
            $match:{
                userId:{$in:users},
                // monthYear:{$in:monthYears}
            }
        },
        {
            $project:{
                partners:"$partners.contactsAndInteractions",
                decisionMakers:"$decisionMakers.contactsAndInteractions",
                influencers:"$influencers.contactsAndInteractions",
                others:"$others.contactsAndInteractions"
            }
        }
    ],function (err,result) {

        if(!err && result && result.length>0){
            callback(err,formatAccountRelationshipRelevance(result,contacts))
        } else {
            callback(err,result)
        }
    });
}

function formatAccountRelationshipRelevance(data,contacts){

    var obj = {
        partners:0,
        decisionMakers:0,
        influencers:0,
        others:0
    }

    _.each(data,function (el) {
        if(el.partners && el.partners.length>0){
            obj.partners = obj.partners+_.sum(el.partners,function (val) {
                    if(_.includes(contacts,val.emailId)){
                        return val.count
                    }
                })
        }
        if(el.decisionMakers && el.decisionMakers.length>0){
            obj.decisionMakers = obj.decisionMakers+_.sum(el.decisionMakers,function (val) {

                    if(_.includes(contacts,val.emailId)){
                        return val.count
                    }
                })
        }
        if(el.influencers && el.influencers.length>0){
            obj.influencers = obj.influencers+_.sum(el.influencers,function (val) {
                    if(_.includes(contacts,val.emailId)){
                        return val.count
                    }
                })
        }
        if(el.others && el.others.length>0){
            obj.others = obj.others+_.sum(el.others,function (val) {
                    if(_.includes(contacts,val.emailId)){
                        return val.count
                    }
                })
        }
    })

    return obj;
}

function formatForFutureMeetingsAndPastInteractions(interactions) {

    var futureMeetings = [],pastInteractions = [];

    _.each(interactions,function (el) {
        if(el.interactionType == "meeting" && new Date(el.interactionDate)>new Date()){
            futureMeetings.push(el)
        }

        if(new Date(el.interactionDate)< new Date()){
            pastInteractions.push(el)
        }
    })

    if(pastInteractions.length>5){
        pastInteractions.sort(function (o1, o2) {
            return new Date(o1.interactionDate) > new Date(o2.interactionDate) ? 1 : new Date(o1.interactionDate) < new Date(o2.interactionDate) ? -1 : 0;
        });
        // pastInteractions = pastInteractions.slice(0,3)
    }

    return {
        futureMeetings:futureMeetings,
        pastInteractions:pastInteractions
    }

}

function enumerateDaysBetweenDates(startDate, endDate) {

    var start = moment(startDate)
    var end = moment(endDate)

    var now = start, dates = [];

    while (now <= end) {
        dates.push(now.format("MYYYY"));
        now.add('days', 1);
    }
    return dates;
};

function castToObjectId(id){
    return mongoose.Types.ObjectId(id)
}

function fetchCompanyFromEmail(email){

    if(email){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            word = word.toLowerCase();
            return removableTextList.indexOf(word.trim()) > -1
        })
        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
    } else {
        return null
    }
}

module.exports = AccountManagement;