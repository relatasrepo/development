var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var contactsCollection = require('../databaseSchema/contactSchema').contacts;
var scheduleInvitation = require('../databaseSchema/userManagementSchema').scheduleInvitation;
var facebookStatus = require('../databaseSchema/userManagementSchema').facebookStatus;
var linkedinStatus = require('../databaseSchema/userManagementSchema').linkedinStatus;
var twitterStatus = require('../databaseSchema/userManagementSchema').twitterStatus;
var mySharedDocs = require('../databaseSchema/userManagementSchema').documents;
var userlog = require('../databaseSchema/userManagementSchema').userlog;
var interaction = require('../databaseSchema/userManagementSchema').interaction;
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var todayInsightsCollection = require('../databaseSchema/userManagementSchema').todayInsights;
var portfolioAccess = require('../databaseSchema/portfolioAccess').portfolioAccess;

var async = require('async');
var crypto = require('crypto');
var winstonLog = require('../common/winstonLog');
var admin = require('../config/firebaseConfiguration');

var mongoose = require('mongoose');
var momentTZ = require('moment-timezone');
var moment = require("moment");

var castToObjectId = require('mongodb').ObjectID;
var _ = require("lodash");

var logLib = new winstonLog();

var logger = logLib.getWinston();
var loggerError = logLib.getWinstonError();

var redis = require('redis');
var redisClient = redis.createClient();

function UserManagement() {

}

function isThirdpartyUser(serviceLogIn) {
    var isThirdPartyAccount;

    if (serviceLogIn == "relatas") {
        isThirdPartyAccount = false;
    }
    if (serviceLogIn == "facebook" || serviceLogIn == "gmail" || serviceLogIn == "linkedin" || serviceLogIn == "twitter") {
        isThirdPartyAccount = true;
    }

    return isThirdPartyAccount;
}

function getTokenResetFlag(google) {
    if (checkRequred(google) && google.length > 0) {
        return true;
    } else return false;
}

//On signup using google, create the user here.

function ConstructUSerData(userInfo) {
    var profileType = {
        timeOne: {
            start: userInfo.startTime1 || '',
            end: userInfo.endTime1 || ''
        },
        timeTwo: {
            start: userInfo.startTime2 || '',
            end: userInfo.endTime2 || ''
        }
    }

    var linkedinId;
    var googleId;
    if (userInfo.serviceLogin == 'linkedin') {
        linkedinId = userInfo.linkedinId
    } else if (userInfo.serviceLogin == 'office') {

    } else googleId = userInfo.googleId

    var partialFill;

    if (userInfo.partialFill == true || userInfo.partialFill == 'true') {
        partialFill = true;
    } else partialFill = false;

    if (userInfo.public == true || userInfo.public == 'true') {
        profileType.calendarType = 'public';
    } else profileType.calendarType = 'selectPublic';

    var registeredUser;

    if (userInfo.registeredUser == false || userInfo.registeredUser == 'false') {
        registeredUser = false
    } else registeredUser = true;

    var corporateUser = false;
    if (userInfo.corporateUser == true || userInfo.corporateUser == 'true') {
        corporateUser = true;
    }

    var isMobileNumberVerified = false;
    if (checkRequred(userInfo.isMobileNumberVerified) && (userInfo.isMobileNumberVerified == true || userInfo.isMobileNumberVerified == 'true')) {
        isMobileNumberVerified = true;
    }



    var date = new Date();
    var userProfile = new myUserCollection({
        firstName: userInfo.firstName,
        lastName: userInfo.lastName,
        publicProfileUrl: userInfo.publicProfileUrl,
        emailId: userInfo.emailId,
        password: userInfo.password,
        birthday: {
            day: userInfo.day,
            month: userInfo.month,
            year: userInfo.year
        },
        registeredUser: registeredUser,
        mobileNumber: userInfo.mobileNumber,
        mobileNumberWithoutCC:userInfo.mobileNumberWithoutCC,
        countryCode:userInfo.countryCode,
        companyName: userInfo.companyName,
        designation: userInfo.designation,
        calendarAccess: profileType,
        linkedinId: linkedinId,
        googleId: googleId,
        skypeId: userInfo.skypeId,
        location: userInfo.location,
        profilePicUrl: userInfo.profilePicUrl,
        linkedin: userInfo.linkedin,
        google: userInfo.google,
        outlook: userInfo.outlook,
        facebook: userInfo.facebook,
        twitter: userInfo.twitter,
        currentLocation: userInfo.currentLocation,
        profilePrivatePassword: userInfo.profilePrivatePassword,
        profileDescription: userInfo.profileDescription,
        serviceLogin: userInfo.serviceLogin,
        sendDailyAgenda: true,
        corporateUser: corporateUser,
        resource: userInfo.resource || false,
        companyId: userInfo.companyId || null,
        firstLogin: false,
        createdDate: date.toJSON(),
        registeredDate: date,
        workHours: {
            start: '00:00',
            end: '23:59'
        },
        partialFill: partialFill,
        isMobileNumberVerified: isMobileNumberVerified,
        mobileNumberOTP: checkRequred(userInfo.mobileNumberOTP) ? userInfo.mobileNumberOTP : '',
        tokenReset: getTokenResetFlag(userInfo.google),
        userType: userInfo.userType || null
    });

    if (checkRequred(userInfo.companyId)) {
        userProfile.companyId = userInfo.companyId;
    }

    if (userInfo.serviceLogin == 'office') {
        userProfile.officeCalendar = userInfo.officeData;
        userProfile.officeCalendar.addedDate = new Date();
    }

    if (checkRequred(userInfo.tzName) && checkRequred(userInfo.tzZone)) {
        var zone = {};
        zone.name = userInfo.tzName;
        zone.zone = userInfo.tzZone;
        zone.updated = true;
        var zoneNum = getZoneNumber(zone.zone);
        userProfile.timezone = zone
        userProfile.zoneNumber = zoneNum
    }

    if (checkRequred(userInfo.mobileNumber)) {
        userProfile.mobileNumber = userInfo.mobileNumber;
        userProfile.mobileNumberWithoutCC = userInfo.mobileNumberWithoutCC;
        userProfile.countryCode = userInfo.countryCode;
    }

    return userProfile;
}

function constructInvitationData(invitationDetails, self) {

    var date = new Date()
    if (!self) {
        var scheduleDetails = new scheduleInvitation({
            suggested: invitationDetails.suggested,
            invitationId: invitationDetails.invitationId,
            senderId: invitationDetails.senderId,
            senderName: invitationDetails.senderName,
            senderEmailId: invitationDetails.senderEmailId,
            senderPicUrl: invitationDetails.senderPicUrl,
            readStatus: false,
            isGoogleMeeting: invitationDetails.isGoogleMeeting,
            isOfficeOutlookMeeting: invitationDetails.isOfficeOutlookMeeting == true,
            scheduledDate: date,
            to: {
                receiverId: invitationDetails.to.receiverId,
                receiverName: invitationDetails.to.receiverName,
                receiverEmailId: invitationDetails.to.receiverEmailId

            },
            scheduleTimeSlots: invitationDetails.scheduleTimeSlots,
            meetingLocation: invitationDetails.meetingLocation,
            participants: invitationDetails.participants,
            docs: invitationDetails.docs
        });
        return scheduleDetails;
    } else {
        var scheduleDetails2 = new scheduleInvitation({
            suggested: invitationDetails.suggested,
            invitationId: invitationDetails.invitationId,
            senderId: invitationDetails.senderId,
            senderName: invitationDetails.senderName,
            senderEmailId: invitationDetails.senderEmailId,
            senderPicUrl: invitationDetails.senderPicUrl,
            googleEventId: invitationDetails.googleEventId || '',
            readStatus: false,
            selfCalendar: true,
            isGoogleMeeting: invitationDetails.isGoogleMeeting,
            isOfficeOutlookMeeting: invitationDetails.isOfficeOutlookMeeting == true,
            scheduledDate: date,
            toList: invitationDetails.toList,
            scheduleTimeSlots: invitationDetails.scheduleTimeSlots,
            meetingLocation: invitationDetails.meetingLocation,
            participants: invitationDetails.participants,
            docs: invitationDetails.docs
        });
        return scheduleDetails2;
    }
}

function constructDocument(docDetails) {
    var date = new Date();
    var documentDetails = new mySharedDocs({
        sharedBy: docDetails.sharedBy,
        sharedWith: docDetails.sharedWith,
        documentName: docDetails.documentName,
        documentCategory: docDetails.documentCategory,
        documentUrl: docDetails.documentUrl,
        access: docDetails.access == 'public' ? 'public' : 'private',
        awsKey: docDetails.awsKey,
        sharedDate: date
    });
    return documentDetails;
}

function constructLinkedinStatusInfo(statusInfo) {

    var date = new Date();
    var linkedinStatusInfo = new linkedinStatus({
        _id: statusInfo.id,
        userId: statusInfo.userId,
        userEmailId: statusInfo.userEmailId,
        userFirstName: statusInfo.userFirstName,
        dateModified: date,
        linkedin: statusInfo.linkedin
    });
    return linkedinStatusInfo;
}

function constructTwitterStatusInfo(statusInfo) {

    var date = new Date()
    var twitterStatusInfo = new twitterStatus({
        _id: statusInfo.id,
        userId: statusInfo.userId,
        userEmailId: statusInfo.userEmailId,
        userFirstName: statusInfo.userFirstName,
        dateModified: date,
        twitter: statusInfo.twitter
    });
    return twitterStatusInfo;
}

function constructFacebookStatusInfo(statusInfo) {

    var date = new Date()
    var facebookStatusInfo = new facebookStatus({
        _id: statusInfo.id,
        userId: statusInfo.userId,
        userEmailId: statusInfo.userEmailId,
        userFirstName: statusInfo.userFirstName,
        dateModified: date,
        facebook: statusInfo.facebook
    });
    return facebookStatusInfo;
}

UserManagement.prototype.getUserCompanyDetails = function (userId,callback) {

    myUserCollection.findOne({_id:userId},{companyId:1,timezone:1,serviceLogin:1},function (err,result) {

        if(!err && result && result.companyId){
            companyCollection.findOne({_id:castToObjectId(result.companyId.toString())},function (err1,companyInfo) {

                if(!err1 && companyInfo){
                    callback(null,companyInfo,result)
                } else {
                    callback(err1,null,result)
                }
            });
        } else {
            callback("not_corporateUser",null,null)
        }
    })
}

UserManagement.prototype.isRelatasUser = function(sourceEmail, callback) {
    myUserCollection.findOne({ "emailId": sourceEmail }, { "_id": 1, "facebook.id": 1 }, function(error, result) {
        if (error) {
            callback(error, "Error in finding contact");
        } else if (result == null || result == undefined)
            callback(error, false);
        else
            callback(error, true, result._id, result.facebook.id);
    })
}

UserManagement.prototype.searchUserConatctsByEmailAndMobile = function(userId, contacts, callback) {
    myUserCollection.aggregate(
        [{$match:{_id:userId}},
        {$unwind:"$contacts"},
        {$match:
            {$or:[
                { "contacts.personEmailId":{$in:
                    contacts, $nin:['', null] }
                }, 
                {"contacts.mobileNumber":{$in:
                    contacts, $nin:['', null] }
                } 
            ]}
        },
        {$project:{
                _id:0,
                userEmailId:"$emailId",
                userId:"$_id",
                name:"$contacts.personName",
                emailId:"$contacts.personEmailId",
                mobile:"$contacts.mobileNumber",
                contactId:"$contacts._id",
                contactRelation:"$contacts.contactRelation.decisionmaker_influencer"
            }
        }
    ]).exec(function(error, contacts){
        if(error)
            callback(error, contacts);
        else
            callback(null, contacts);
    })
}

UserManagement.prototype.getUserConatctsByRelationship = function(userId, callback) {
    myUserCollection.aggregate(
        [{$match:{_id:userId}},
        {$unwind:"$contacts"},
        {$project:{
            _id:0,
            emailId:"$contacts.personEmailId",
            contactRelation:"$contacts.contactRelation"
        }
        }
    ]).exec(function(error, contacts){
        if(error)
            callback(error, contacts);
        else
            callback(null, contacts);
    })
}

//get the hierarchy new

UserManagement.prototype.getHierarchy = function(comapnyId, callback) {
    var q;
    if (typeof comapnyId === 'string')
        q = castToObjectId(comapnyId);
    else
        q = comapnyId;
    myUserCollection.aggregate([
        { $match: { resource: { $ne: true },"companyId": q } }, {
            $group: {
                _id: "$companyId",
                name: {
                    $addToSet: {
                        n: "$firstName",
                        l: "$lastName",
                        designation: "$designation",
                        email: "$emailId",
                        id: "$_id",
                        parent: "$hierarchyParent",
                        path: "$hierarchyPath",
                        imageUrl: "$profilePicUrl",
                        orgHead:"$orgHead"
                    }
                },
                companyName: { $first: "$companyName" },
                companyId: { $first: "$companyId" },
            }
        }, { $unwind: "$name" }, {
            $project: {
                _id: 0,
                id: "$name.id",
                nameF: "$name.n",
                nameL: "$name.l",
                designation: "$name.designation",
                email: "$name.email",
                parentName: "$name.parent",
                parentId: "$name.parent",
                path: "$name.path",
                imageUrl: '$name.imageUrl',
                companyName: "$companyName",
                companyId: "$companyId",
                orgHead:"$name.orgHead"
            }
        }
    ], function(err, result) {
        if (err) throw err;
        if (result.length !== 0) {
            for (var i = 0; i < result.length; i++) {
                for (var j = 0; j < result.length; j++) {
                    if (i !== j) {
                        if (String(result[i].parentId) === String(result[j].id)) {
                            result[i].parentName = result[j].nameF + ' ' + result[j].nameL;
                        }
                    }
                }
            }
            result.sort();
            callback(result);
        } else
            callback(result);
    });
}

UserManagement.prototype.updateHierarchyNew = function(hierarchys, userId, callback) {
    async.parallel([
        function(callback) {
            async.forEach(hierarchys, function(hierarchy, callback) {
                if (hierarchy.parentId !== null)
                    myUserCollection.update({ emailId: hierarchy.email }, { $set: { hierarchyParent: new castToObjectId(hierarchy.parentId), hierarchyPath: hierarchy.path } }, callback);
                else if (hierarchy.parentId === null)
                    myUserCollection.update({ emailId: hierarchy.email }, { $set: { hierarchyParent: null, hierarchyPath: null } }, callback);
            }, function(err) {
                if (err) callback({ status: 'failure' });
                callback();
            });
        },
        function(callback) {
            var userlogId = null;
            var status = null;
            async.series([
                function(callback) {
                    userlog.find({ userId: castToObjectId(userId) }).sort({ loginDate: -1 }).limit(1).exec(function(err, result) {
                        if (err) throw err;
                        if (result.length > 0) {
                            status = result[0].adminActionTaken;
                            userlogId = result[0]._id;
                        }
                        callback();
                    });
                },
                function(callback) {
                    date = new Date();
                    iso = date.toISOString();
                    if (status === null)
                        adminActionTaken = iso + ' hierarchy updated';
                    else
                        adminActionTaken = status + ',' + iso + ' hierarchy updated';
                    userlog.update({ _id: userlogId }, { $set: { adminActionTaken: adminActionTaken } }, function(e, r) {
                        if (e) throw e;
                        callback();
                    })
                }
            ], function(err) {
                if (err) return next(err);
                callback()
            });
        }
    ], function(err) {
        if (err) return next(err);
        callback({ status: 'ok' });
    });

}

// Get all redundant contacts
UserManagement.prototype.getRedundantContacts = function(callback) {
    var userEmails = [];
    var LimituserEmails = [];
    var redundantContacts = [];
    var count = 0;

    async.series([
        function(callback) {
            myUserCollection.find({}, { emailId: 1, _id: 0 }, function(e, r) {
                if (e) throw err;
                if (r.length !== 0) {
                    r.forEach(function(el) {
                        if (el.emailId !== undefined) {
                            userEmails.push(el.emailId);
                        }
                    });
                }
                callback();
            });
        },
        function(callback) {
            async.forEach(userEmails, function(uEmailId, callback) {
                myUserCollection.aggregate([
                    { $match: { 'emailId': uEmailId } },
                    { $unwind: "$contacts" }, {
                        $project: {
                            emailId: "$emailId",
                            contacts: "$contacts",
                            firstName: "$firstName",
                            lastName: "$lastName",
                            //check: { $cond: { if: "$contacts.personEmailId",
                            //        then: "$contacts.personEmailId",
                            //        else :"$contacts.mobileNumber" } }
                        }
                    }, {
                        $group: {
                            "_id": {
                                email: "$emailId",
                                personemail: "$contacts.personEmailId",
                                mobileNumber: {
                                    "$cond" :{
                                        if:'$contacts.mobileNumber',
                                        then:'$contacts.mobileNumber',
                                        else:null
                                    }
                                }
                                //contact:"$check"
                            },
                            "count": { $sum: 1 }
                        }
                    },
                    { $match: { $or: [{ "count": { $gt: 1 } }, { "_id.personemail": "" }] } }, {
                        $project: {
                            _id: 0,
                            email: "$_id.email",
                        }
                    }

                ], function(err, result) {
                    if (err) throw err;
                    if (result.length !== 0) {
                        // if(LimituserEmails.length < 3)
                        LimituserEmails.push(uEmailId);
                    }
                    callback();
                });
            }, function(err) {
                if (err) return next(err);
                callback();
            });
        },
        function(callback) {
            var filterUserEmail = [];
            LimituserEmails.sort()
            for (var i = 0; i < 10; i++) {
                if (LimituserEmails[i] !== undefined)
                    filterUserEmail[i] = LimituserEmails[i];
            }

            async.forEach(filterUserEmail, function(uEmailId, callback) {
                myUserCollection.aggregate([
                    { $match: { 'emailId': uEmailId } },
                    { $unwind: "$contacts" }, {
                        $project: {
                            emailId: "$emailId",
                            contacts: "$contacts",
                            firstName: "$firstName",
                            lastName: "$lastName",
                        }
                    } ,{
                        $group: {
                            "_id": {
                                email: "$emailId",
                                 personemail: "$contacts.personEmailId",
                                mobileNumber: {
                                    "$cond" :{
                                        if:'$contacts.mobileNumber',
                                        then:'$contacts.mobileNumber',
                                        else:null
                                    }
                                },
                                //contact:"$check",
                                uFname: "$firstName",
                                uLname: "$lastName"
                            },
                            pname: { $addToSet: "$contacts.personName" },
                            firstId: { $first: { id: "$contacts._id", date: "$contacts.addedDate" } },
                            ids: { $push: { id: "$contacts._id", date: "$contacts.addedDate" } },
                            "count": { $sum: 1 }
                        }
                    },
                    { $match: { $or: [{ "count": { $gt: 1 } }, { "_id.personemail": "" }] } }, {
                        $project: {
                            _id: 1,
                            email: "$_id.email",
                            pname: "$pname",
                            firstid: "$firstId",
                            ids: "$ids",
                            //   count:1
                        }
                    },
                    { $unwind: "$ids" }, {
                        "$redact": {
                            "$cond": {
                                "if": { "$eq": ["$ids", "$firstid"] },
                                "then": "$$PRUNE",
                                "else": "$$KEEP"
                            }
                        }
                    },
                    //   {$limit:200}
                    {
                        $project: {
                            _id: 0,
                            userNameF: "$_id.uFname",
                            userNameL: "$_id.uLname",
                            userEmail: "$_id.email",
                            contactName: "$pname",
                            contactEmail: "$_id.personemail",
                            contactMobile:"$_id.mobileNumber",
                            id: "$ids.id",
                            addedDate: "$ids.date"
                        }
                    }
                ], function(err, result) {
                    if (err) throw err;
                    if (result.length !== 0) {
                        result.forEach(function(ele) {
                            redundantContacts.push(ele);
                        });
                    }
                    callback();
                });
            }, function(err) {
                if (err) return next(err);
                callback();
            });

        },
        function(callback) { // to count the total
            async.forEach(userEmails, function(uEmailId, callback) {
                myUserCollection.aggregate([
                    { $match: { 'emailId': uEmailId } },
                    { $unwind: "$contacts" }, {
                        $project: {
                            emailId: "$emailId",
                            contacts: "$contacts",
                            firstName: "$firstName",
                            lastName: "$lastName",
                        }
                    }, {
                        $group: {
                            "_id": {
                                email: "$emailId",
                                 personemail: "$contacts.personEmailId",
                                mobileNumber: {
                                    "$cond" :{
                                        if:'$contacts.mobileNumber',
                                        then:'$contacts.mobileNumber',
                                        else:null
                                    }
                                },
                                //contact:"$check",
                                uFname: "$firstName",
                                uLname: "$lastName"
                            },
                            pname: { $addToSet: "$contacts.personName" },
                            firstId: { $first: { id: "$contacts._id", date: "$contacts.addedDate" } },
                            ids: { $push: { id: "$contacts._id", date: "$contacts.addedDate" } },
                            "count": { $sum: 1 }
                        }
                    },
                    { $match: { $or: [{ "count": { $gt: 1 } }, { personemail: "" }] } }, {
                        $project: {
                            _id: 1,
                            email: "$_id.email",
                            pname: "$pname",
                            firstid: "$firstId",
                            ids: "$ids",
                        }
                    },
                    { $unwind: "$ids" }, {
                        "$redact": {
                            "$cond": {
                                "if": { "$eq": ["$ids", "$firstid"] },
                                "then": "$$PRUNE",
                                "else": "$$KEEP"
                            }
                        }
                    }, {
                        $project: {
                            _id: 0,
                            userEmail: "$_id.email",
                        }
                    },
                    { $group: { _id: "$userEmail", count: { $sum: 1 } } }
                ], function(err, result) {
                    if (err) throw err;
                    if (result.length !== 0) {
                        count += result[0].count;
                    }
                    callback();
                });
            }, function(err) {
                if (err) return next(err);
                callback();
            });
        }
    ], function(err) {
        if (err) return next(err);
        // redundantContacts.push(count);
        callback({contacts:redundantContacts, count:count});
    });
}

UserManagement.prototype.deleteRedundantContact = function(contactsToBeDeleted, callback) {
    async.forEach(contactsToBeDeleted, function(profile, callback) {
        myUserCollection.update({ emailId: profile.email }, { $pull: { contacts: { _id: { $in: castListToObjectIds(profile.id) } } } }, { multi: true },
            callback
        )
    }, function(err) {
        if (err) return next(err);
        callback({ status: "success" });
    });
}

UserManagement.prototype.updateReminderForContacts = function(userId, contactIds, date, callback) {
    async.forEach(contactIds, function(contactId, callback) {
        myUserCollection.update({ _id: userId, "contacts":{$elemMatch:{_id:contactId, remindToConnect:{$eq:null}}} }, { "$set": { "contacts.$.remindToConnect": new Date(date) } }, callback )
    }, function(error) {
        if (error) {
            callback(error, false);
        } else
            callback(error, true);
    });
}

UserManagement.prototype.getUserContactsCountByDateAndCompany = function(companies, date, filter, callback) {
    var q = {createdDate:{$lt:new Date(date.max)},resource: { $ne: true }};
    var finalQuery = [];
    if(companies.length > 0 && filter == 'business'){
        q["corporateUser"] = true;
        q["companyId"] = {$in:companies};
    }
    else if(filter == 'individuals'){
        q["corporateUser"] = {$in:[false, null]};
    }
    finalQuery.push(
        {$match:q},
        {$unwind:"$contacts"},
        {$match: {$or:[{"contacts.addedDate":{$lt:new Date(date.max)}}, {"contacts.addedDate":null}]}}
    )
    if(filter == 'business'){
        finalQuery.push(
            {$group:{
                _id:{id:"$_id", email:"$emailId"},
                details:{$first:{companyId:"$companyId", registeredDate:"$registeredDate"}},
                contacts:{$sum:1}
            }
            },
            {$group:{
                _id:"$details.companyId",
                contacts:{$sum:"$contacts"}
            }
            }
        )
    }
    else{
        finalQuery.push(
            {$group:{
                _id:{id:"$_id", email:"$emailId"},
                contacts:{$sum:1}
            }
            },
            {$group:{
                _id:null,
                contacts:{$sum:"$contacts"}
            }
            }
        )
    }

    myUserCollection.aggregate(finalQuery).exec(function(error, users){
        if(error){
            logger.error('Error in userManagement - getUsersByCompany()', error);
            callback([])
        }
        else{
            callback(users)
        }
    })
}

UserManagement.prototype.getRemindToConnectContacts = function(userId, callback) {

    myUserCollection.aggregate([
        {
            $match:{_id:userId}
        },
        {
            $unwind:"$contacts"
        },
        {
            $match:{
                "contacts.remindToConnect":{$gte:new Date()}
            }
        },{
            $group:{
                _id:null,
                count: {
                    $sum: 1
                }
            }
        }
    ]).exec(function (err,contacts) {
        if(!err && contacts.length>0){
            callback(contacts[0])
        } else {
            callback({ _id: null, count: 0 })
        }
    })
}

UserManagement.prototype.getFavoriteContacts = function(sourceEmail, callback) {
    myUserCollection.findOne({ "emailId": sourceEmail }, { _id: 0, "contacts.personEmailId": 1, "contacts.favorite": 1 }, function(error, favoriteContacts) {
        if (error) {
            callback(error, "Error in finding favorite contacts");
        } else if (favoriteContacts == null || favoriteContacts == undefined)
            callback(null, "No favorites");
        else {
            var myFavorites = favoriteContacts.contacts;
            var filteredCts = myFavorites.filter(function(contact) {
                if (contact.favorite === true)
                    return true
                else
                    return false
            })
            callback(null, filteredCts);
        }
    })

};

UserManagement.prototype.getTodayInsightsMetaInformation = function(userId, callback) {
    
    todayInsightsCollection.findOne({userId:userId}, {}, function(error, todayInsights){
        callback(error, todayInsights);
    })
}

UserManagement.prototype.checkInsightsBuiltStatus = function(userId, callback) {
    myUserCollection.findOne({"_id": userId}, {insightsBuilt: 1,publicProfileUrl:1}, function (error, insightsBuilt) {
        if(!error && insightsBuilt) {
            callback(error,insightsBuilt)
        } else {
            callback(error,false)
        }
    })
}

UserManagement.prototype.setInsightsBuiltStatus = function(userId,flag,callback) {

    myUserCollection.update({"_id": userId}, { $set: { "insightsBuilt": flag } }, { upsert: true }, function (error, insightsBuilt) {
        if(!error && insightsBuilt) {
            callback(error,true)
        } else {
            callback(error,false)
        }
    })
}

UserManagement.prototype.addUserPartialProfile = function (basicPartial,callback) {

    var userProfile = new myUserCollection(basicPartial)
    userProfile.save(function(err, result){
        if(!err){
            logger.info("Successfully added new user - ",basicPartial.emailId)
            callback(true)
        } else {
            loggerError.info("Unsuccessfully added new user - ",basicPartial.emailId,JSON.stringify(err))
            callback(false)
        }
    });

};

/* Add new user to the DB*/
UserManagement.prototype.addUser = function(userInfo, otherSource, callback) {

    var userM = this;
    // include /u/ also to check unique name exists
    var nameRegex = new RegExp('/u/' + userInfo.publicProfileUrl);
    userM.isExistingProfile(nameRegex, userInfo.publicProfileUrl, function(error, flag, user) {
        if (!flag) {
            var userProfile = ConstructUSerData(userInfo);
            myUserCollection.findOne({ emailId: userProfile.emailId }, { contacts: 0 }, function(err, userInfo) {
                if (userInfo != null) {
                    if (otherSource) {
                        userInfo.alreadyRegistered = true;
                        callback(err, userInfo, { message: 'userExist' });
                    } else {
                        callback(err, 'exist', { message: 'email address already exist in Relatas database' });
                    }
                } else {
                    userProfile.save(function(err, userProfile) {
                        if (err) {
                            callback(err, null, { message: 'Error occurred while writing user data into DB ' + err });
                            logger.info('Error occurred while writing user data into DB ' + err)
                        } else {
                            userProfile.alreadyRegistered = false;
                            callback(err, userProfile, { message: 'User registration success' });

                        }
                    })
                }
            })
        } else {
            var userInfo2 = JSON.parse(JSON.stringify(userInfo));
            var str = userInfo.publicProfileUrl;
            var num = 1;
            var matches = str.match(/\d+$/);
            if (matches) {
                num = parseInt(matches[0]) + 1;
                str = str.substring(0, matches['index']);
            }
            str = str + num;

            userInfo2.publicProfileUrl = str;
            userInfo2.uniqueName = userInfo.uniqueName + num;
            userM.addUser(userInfo2, otherSource, callback);
        }
    });
};

UserManagement.prototype.updateUserProfile = function (userProfile,callback) {
    
    var userM = this;
    var nameRegex = new RegExp('/u/' + userProfile.publicProfileUrl);
    userM.isExistingProfile(nameRegex, userProfile.publicProfileUrl, function(error, flag, user) {

        if (!flag) {

            myUserCollection.update({emailId: userProfile.emailId},
                {
                    $set:{
                        publicProfileUrl:userProfile.publicProfileUrl,
                        countryCode:userProfile.countryCode,
                        phoneNumber:userProfile.phoneNumber,
                        uniqueName:userProfile.uniqueName,
                        firstName:userProfile.firstName,
                        lastName:userProfile.lastName,
                        location:userProfile.location,
                        companyName:userProfile.companyName,
                        designation:userProfile.designation,
                        mobileNumberOTP:userProfile.mobileNumberOTP,
                        isMobileNumberVerified:userProfile.isMobileNumberVerified,
                        partialFill:userProfile.partialFill,
                        registeredUser:userProfile.registeredUser
                    }
                },function(err,res){
                    
                    if(!err && res){
                        callback(res)
                    } else {
                        callback(err)
                    }
                })
        } else {
            
            var userProfile2 = JSON.parse(JSON.stringify(userProfile));
            var str = userProfile.publicProfileUrl;
            var num = 1;
            var matches = str.match(/\d+$/);
            if (matches) {
                num = parseInt(matches[0]) + 1;
                str = str.substring(0, matches['index']);
            }
            str = str + num;

            userProfile2.publicProfileUrl = str;
            userProfile2.uniqueName = userProfile.uniqueName + num;

            userM.updateUserProfile(userProfile,callback)
        }

    });

}


/* Add new user to the DB from Schedule process*/
UserManagement.prototype.addUserSchedule = function(userInfo, otherSource, updateCurrentLocation, ipAddress, callback) {
    var userM = this;
    // include /u/ also to check unique name exists
    var nameRegex = new RegExp('/u/' + userInfo.publicProfileUrl);
    userM.isExistingProfile(nameRegex, userInfo.publicProfileUrl, function(error, flag, user) {
        if (!flag) {
            var userProfile = ConstructUSerData(userInfo);
            myUserCollection.findOne({ emailId: userProfile.emailId }, { contacts: 0 }, function(err, userInfo) {
                if (userInfo != null) {
                    if (otherSource) {
                        userInfo.alreadyRegistered = true;
                        callback(err, userInfo, { message: 'userExist' });
                    } else {
                        callback(err, 'exist', { message: 'email address already exist in Relatas database' });
                    }
                } else {
                    userProfile.save(function(err, userProfile) {
                        if (err) {
                            callback(err, null, { message: 'Error occurred while writing user data into DB ' + err });
                            logger.info('Error occurred while writing user data into DB ' + err)
                        } else {

                            if (updateCurrentLocation && ipAddress) {
                                //Update Location
                                updateCurrentLocation(ipAddress, userProfile._id);
                            }

                            userProfile.alreadyRegistered = false;
                            callback(err, userProfile, { message: 'User registration success' });

                        }
                    })
                }
            })
        } else {
            var userInfo2 = JSON.parse(JSON.stringify(userInfo));
            var str = userInfo.publicProfileUrl;
            var num = 1;
            var matches = str.match(/\d+$/);
            if (matches) {
                num = parseInt(matches[0]) + 1;
                str = str.substring(0, matches['index']);
            }
            str = str + num;

            userInfo2.publicProfileUrl = str;
            userInfo2.uniqueName = userInfo.uniqueName + num;
            userM.addUser(userInfo2, otherSource, callback);
        }
    });
};

//Get a Relatas User by Email ID.

UserManagement.prototype.getUserByEmail = function(emailId, callback) {

    myUserCollection.findOne({ emailId: emailId }, { contacts: 0 }, function(err, userInfo) {

        if (userInfo != null) {
            userInfo.alreadyRegistered = true;
            callback(err, userInfo, { message: 'userExist' });
        } else {
            callback(err);
        }
    });
}

UserManagement.prototype.getTwoUsersByEmail = function(primaryEmailId,secondaryEmailId, callback) {

    myUserCollection.find({ emailId: {$in:[primaryEmailId,secondaryEmailId] }}, { contacts: 0,notification:0,workHoursWeek:0,workHours:0,calendarAccess:0,officeCalendar:0 }, function(err, userInfo) {

        if (userInfo != null) {
            callback(err, userInfo, { message: 'userExist' });
        } else {
            callback(err);
        }
    });
}

UserManagement.prototype.getContacts = function(findQ,projection, callback) {
    contactsCollection.find(findQ, projection, function(err, contacts) {
        callback(err,contacts)
    });
}

UserManagement.prototype.addUserThirdParty = function(userInfo, callback) {
    this.addUser(userInfo, true, callback);
};

UserManagement.prototype.removeUserProfile = function(userId, callback) {
    myUserCollection.findOneAndRemove({ _id: userId }, function(error, result) {
        if (error) {
            callback(false, 'onDeleteUser', error, result);
        } else callback(true, 'onDeleteUser', error, result);
    })
};

/* To store social info to db */

UserManagement.prototype.storeLinkedinStatus = function(lStatus, callback) {

    var linkedinStatusInfo = constructLinkedinStatusInfo(lStatus);

    linkedinStatus.findOne({ userEmailId: linkedinStatusInfo.userEmailId }, function(error, statusInfo) {
        if (error) {
            callback(error, statusInfo, { message: 'Error in searching userlinkedin status' })
        } else if (statusInfo == undefined || statusInfo == null) {
            linkedinStatusInfo.save(function(error, statusInfo) {
                if (error) {
                    statusInfo = false
                    callback(error, statusInfo, { message: 'Error in writing userlinkedin status' })
                } else if (statusInfo != null) {
                    statusInfo = true
                    callback(error, statusInfo, { message: 'Writing userlinkedin status is success' })
                }
            })
        } else {
            linkedinStatus.update({ userEmailId: linkedinStatusInfo.userEmailId }, {
                dateModified: linkedinStatusInfo.dateModified,
                linkedin: linkedinStatusInfo.linkedin
            }, function(error, result) {
                var flag;
                if (error) {
                    callback(error, false, { message: 'Error in DB while updating linkedinStatus ' + error });
                }
                if (result == 0) {
                    flag = false;
                    callback(error, flag, { message: 'update linkedinStatus failed' });
                } else {
                    flag = true;
                    callback(error, flag, { message: 'linkedin Status successfully updated' });
                }

            })
        }
    });
}

UserManagement.prototype.storeTwitterStatus = function(tStatus, callback) {

    var twitterStatusInfo = constructTwitterStatusInfo(tStatus);

    twitterStatus.findOne({ userEmailId: twitterStatusInfo.userEmailId }, function(error, statusInfo) {
        if (error) {
            callback(error, statusInfo, { message: 'Error in searching user twitter status' })
        } else if (statusInfo == undefined || statusInfo == null) {
            twitterStatusInfo.save(function(error, statusInfo) {
                if (error) {
                    statusInfo = false
                    callback(error, statusInfo, { message: 'Error in writing usertwitter status' })
                } else if (statusInfo != null) {
                    statusInfo = true
                    callback(error, statusInfo, { message: 'Writing usertwitter status is success' })
                }
            })
        } else {
            twitterStatus.update({ userEmailId: twitterStatusInfo.userEmailId }, {
                dateModified: twitterStatusInfo.dateModified,
                twitter: twitterStatusInfo.twitter
            }, function(error, result) {
                var flag;
                if (error) {
                    callback(error, false, { message: 'Error in DB while updating twitterStatus ' + error });
                }
                if (result == 0) {
                    flag = false;
                    callback(error, flag, { message: 'update twitterStatus failed' });
                } else {
                    flag = true;
                    callback(error, flag, { message: 'twitterStatus successfully updated' });
                }

            })
        }
    });
}

UserManagement.prototype.storeFacebookStatus = function(fStatus, callback) {

        var facebookStatusInfo = constructFacebookStatusInfo(fStatus);

        facebookStatus.findOne({ userEmailId: facebookStatusInfo.userEmailId }, function(error, statusInfo) {
            if (error) {
                callback(error, statusInfo, { message: 'Error in searching userfacebook status' })
            } else if (statusInfo == undefined || statusInfo == null) {
                facebookStatusInfo.save(function(error, statusInfo) {
                    if (error) {
                        statusInfo = false
                        callback(error, statusInfo, { message: 'Error in writing userfacebook status' })
                    } else if (statusInfo != null) {
                        statusInfo = true
                        callback(error, statusInfo, { message: 'Writing userfacebook status is success' })
                    }
                })
            } else {
                facebookStatus.update({ userEmailId: facebookStatusInfo.userEmailId }, {
                    dateModified: facebookStatusInfo.dateModified,
                    facebook: facebookStatusInfo.facebook
                }, function(error, result) {
                    var flag;
                    if (error) {
                        callback(error, false, { message: 'Error in DB while updating facebookStatus ' + error });
                    }
                    if (result == 0) {
                        flag = false;
                        callback(error, flag, { message: 'update facebookStatus failed' });
                    } else {
                        flag = true;
                        callback(error, flag, { message: 'facebookStatus successfully updated' });
                    }

                })
            }
        });
    }
    //***********************
UserManagement.prototype.isValidRelatasUser = function(emailId, password, callback) {
    myUserCollection.findOne({ emailId: emailId, serviceLogin: "relatas" }, { contacts: 0 }, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            var encpass = EncryptionValue(password);

            if (encpass == userData.password) {

                return callback(error, userData, { message: 'Valid password' });

            } else {

                userData = null;
                return callback(error, userData, { message: 'Wrong password' });
            }
        } else {

            return callback(error, userData, { message: 'No such email' });
        }
    });
};

UserManagement.prototype.authoriseCalendar = function(userId, calPassword, callback) {
    myUserCollection.findOne({ _id: userId }, { profilePrivatePassword: 1 }, function(error, profile) {
        if (error) {
            callback(error, false, { message: 'error while authorising calendar ' + error });
        } else {
            var encpass = EncryptionValue(calPassword);
            if (encpass == profile.profilePrivatePassword) {
                callback(error, true, { message: 'Authorising calendar success' });
            } else {
                callback(error, false, { message: 'Authorising calendar fail "invalid password"' });
            }
        }
    });
};

UserManagement.prototype.authoriseCalendarWithEncryptedPassword = function(userId, calPassword, callback) {
    myUserCollection.findOne({ _id: userId }, { profilePrivatePassword: 1 }, { profilePrivatePassword: 1, emailId: 1 }, function(error, profile) {
        if (error) {
            callback(error, false, { message: 'error while authorising calendar with encrypted password ' + error });
        } else {

            if (calPassword == profile.profilePrivatePassword) {
                callback(error, true, { message: 'Authorising calendar with encrypted password success' });
            } else {
                callback(error, false, { message: 'Authorising calendar with encrypted password fail "invalid password"' });
            }
        }
    });
};

UserManagement.prototype.isExistingUser = function(email, callback) {
    myUserCollection.findOne({ emailId: email }, { emailId: 1 }, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData, { message: 'existing user' });

        } else {
            userData = null;
            callback(error, userData, { message: 'new user' });
        }
    });
}

UserManagement.prototype.isExistingUserLinkedin = function(lId, callback) {
    myUserCollection.findOne({ linkedinId: lId }, { contacts: 0 }, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData, { message: 'existing user' });
        } else {
            userData = null;
            callback(error, userData, { message: 'new user' });
        }
    });
}

UserManagement.prototype.isExistingUserByLinkedinId = function(lId, callback) {
    myUserCollection.findOne({ "linkedin.id": lId }, { contacts: 0 }, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData, { message: 'existing user' });
        } else {
            userData = null;
            callback(error, userData, { message: 'new user' });
        }
    });
}

UserManagement.prototype.updateFacebookLastSyncByFBId = function(fbid, date) {
    myUserCollection.update({ "facebook.id": fbid }, { $set: { "facebook.lastSync": date } }, function(error, result) {
        if (error) {
            console.log(error);
        } else if (result && result.ok) {
        } else {
            flag = false;
            callback(error, flag, { message: 'updateFacebookLastSyncByFBId update failed' });
        }
    });
}

UserManagement.prototype.isExistingUserByFBId = function(fbid, callback) {
    myUserCollection.findOne({ "facebook.id": fbid }, { contacts: 0 }, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData, { message: 'existing user' });
        } else {
            userData = null;
            callback(error, userData, { message: 'new user' });
        }
    });
}

UserManagement.prototype.isExistingUserByFBIdWithIndex = function(index, fbid, callback) {
    myUserCollection.findOne({ "facebook.id": fbid }, { contacts: 0 }, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData, { message: 'existing user' }, index);
        } else {
            userData = null;
            callback(error, userData, { message: 'new user' }, index);
        }
    });
}

UserManagement.prototype.isExistingUserLinkedinAndEmail = function(lId, emailId, callback) {
    myUserCollection.findOne({ $or: [{ linkedinId: lId }, { emailId: emailId }] }, { contacts: 0 }, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData, { message: 'existing user' });
        } else {
            userData = null;
            callback(error, userData, { message: 'new user' });
        }
    });
}

UserManagement.prototype.isExistingUserGoogle = function(gId, callback) {
    myUserCollection.findOne({ googleId: gId }, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData, { message: 'existing user' });
        } else {
            userData = null;
            callback(error, userData, { message: 'new user' });
        }
    });
}

UserManagement.prototype.isExistingUserGoogleAndEmailId = function(gId, emailId, callback) {
    myUserCollection.findOne({ $or: [{ googleId: gId }, { emailId: emailId }, { emailId: emailId.toLowerCase() }] }, { contacts: 0 }, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData, { message: 'existing user' });
        } else {
            userData = null;
            callback(error, userData, { message: 'new user' });
        }
    });
};

// Function to get user profile using userId
UserManagement.prototype.getAllUsersGoogleAccounts = function(number, callback) {

    if (number != 2) {

        myUserCollection.find({ zoneNumber: number, sendDailyAgenda: { $ne: false } }, function(error, userData) {

            if (error) {
                loggerError.info('Error while getting all users google account info ' + JSON.stringify(error));
                callback(userData);
            } else callback(userData);
        });
    } else {
        myUserCollection.find({ sendDailyAgenda: { $ne: false } }, function(error, userData) {

            if (error) {
                loggerError.info('Error while getting all users google account info ' + JSON.stringify(error));
                callback(userData);
            } else {
                if (checkRequred(userData)) {

                    if (userData.length > 0) {
                        var users = [];
                        for (var i = 0; i < userData.length; i++) {
                            if (userData[i].zoneNumber == 2 || !checkRequred(userData[i].zoneNumber)) {
                                users.push(userData[i])
                            }
                        }
                        callback(users)
                    } else callback(userData);
                } else callback(userData);
            }
        });
    }
};

/* Using in admin userlist1 page */
UserManagement.prototype.getAllUsers = function(callback) {
    myUserCollection.find({}, { password: 0, calendarAccess: 0, workHours: 0, profilePrivatePassword: 0, google: 0, facebook: 0, twitter: 0, linkedin: 0 }, function(error, userData) {
        callback(userData);
    });
};

/*using in admin new userlist page*/
UserManagement.prototype.getListOfAllUser = function(fromDate, toDate, category, callback) {
    var dateMin = momentTZ(fromDate);
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)
    var dateMax = momentTZ(toDate);
    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(59)
    dateMax.millisecond(0)

    var q;
    if (category === 'all')
        q = { resource: { $ne: true } };
    else if (category === 'corporate')
        q = { corporateUser: true, resource: { $ne: true } };
    else if (category === 'retail')
        q = {
            $or: [
                { corporateUser: { '$exists': false } }, {
                    $and: [
                        { corporateUser: { '$exists': true } },
                        { corporateUser: false }
                    ]
                }
            ],
            resource: { $ne: true }
        };
    else
        q = { companyId: castToObjectId(category), resource: { $ne: true } }

    myUserCollection.aggregate(
        [
            { $match: { "createdDate": { $gt: new Date(dateMin), $lt: new Date(dateMax) } } },
            { $match: q },
            {
                $project: {
                    lastLoginDate: 1,
                    lastName: 1,
                    firstName: 1,
                    registeredUser: 1,
                    registeredDate: 1,
                    designation: 1,
                    companyName: 1,
                    emailId: 1,
                    serviceLogin: 1,
                    location: 1,
                    createdDate: 1,
                    publicProfileUrl: 1,
                    corporateUser: 1,
                    mobileOnBoardingDate: 1,
                    lastDataSync:1,
                    lastDAMSentDate:1,
                    mobileAppVersion:1, 
                    lastMobileSyncDate:1,
                    mobileLastLoginDate:1,
                    contacts: { $size: "$contacts" }
                }
            }
        ],
        function(err, userList) {

            userList.sort(function(a, b) {
                return new Date(a.createdDate).getTime() - new Date(b.createdDate).getTime()
            });
            var ordered = [];
            for (var i = userList.length - 1; i >= 0; i--)
                ordered.push(userList[i]);
            callback(ordered);
        });
};

//Get all user info.
UserManagement.prototype.getAllUserInfoBasic = function(callback) {

    var o = {};
    o.map = function() {
        emit(this._id, {
            firstName: this.firstName,
            lastName: this.lastName,
            emailId: this.emailId,
            designation: this.designation,
            companyName: this.companyName,
            createdDate: this.createdDate,
            registeredDate: this.registeredDate,
            location: this.location,
            serviceLogin: this.serviceLogin,
            contacts: this.contacts ? this.contacts.length : 0,
            lastLoginDate: this.lastLoginDate,
            uniqueName: this.publicProfileUrl
        });
    }
    o.reduce = function(id, arr) {
        return arr
    }
    myUserCollection.mapReduce(o, function(err, results) {
        if (err) logger.info('Error in map reduce admin function ' + err)

        callback(results);
    })
};

// Function to get user profile using userId
UserManagement.prototype.selectUserProfile = function(id, callback) {
    myUserCollection.findOne({ _id: id }, function(error, userData) {

        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData);
        } else {
            callback(error, null);
        }

    })
};

UserManagement.prototype.selectUserProfileWithContactCount = function(idObjectId, fields, callback) {

    myUserCollection.aggregate(
        [{
            $match: { _id: idObjectId }
        }, {
            $project: fields
        }]
    ).exec(function(error, userData) {
        if (checkRequred(userData) && userData.length > 0) {
            callback(error, userData[0]);
        } else {
            callback(error, null);
        }
    });
};

UserManagement.prototype.selectUserProfileWitOutContatcs = function(id, callback) {
    myUserCollection.findOne({ _id: id }, { contacts: 0 }, function(error, userData) {

        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData);
        } else {
            callback(error, null);
        }

    })
};

UserManagement.prototype.selectUserProfileCustomQuery = function(query, projection, callback) {
    myUserCollection.findOne(query, projection, function(error, userData) {

        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData);
        } else {
            callback(error, null);
        }

    })
};

UserManagement.prototype.selectUserProfilesCustomQuery = function(query, projection, callback) {
    myUserCollection.find(query, projection, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData);
        } else {
            callback(error, null);
        }
    })
};

//This function adds a contact from the email interactions that we get from Google if not existing in the User's contacts list.
// Last modified 11 March 2016, by Naveen.
UserManagement.prototype.addContactFromGoogleInteractions = function(userEmailId, contactObj, callback) {

    myUserCollection.update({ "emailId": userEmailId, 'contacts.personEmailId': { $ne: contactObj.personEmailId } }, {
        $push: {
            contacts: contactObj
        }
    }, function(err, result) {

        logger.info("Result of adding a contact - " + JSON.stringify(result))
        if (!err) {
            if(callback){
                callback(true)
            }
            logger.info("Successfully added contact - " + contactObj.personEmailId + "to User -" + userEmailId)
        } else {
            logger.info("There was an error while adding contact - " + contactObj.personEmailId + "to User - " + userEmailId)
        }
    });
};

UserManagement.prototype.selectUserProfilesCustomQueryWithLimit = function(query, projection, limit, callback) {
    myUserCollection.find(query, projection, { limit: limit }, function(error, userData) {

        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData);
        } else {
            callback(error, null);
        }
    })
};

UserManagement.prototype.selectUserProfileWithWidgetKey = function(key, projection, callback) {

    myUserCollection.findOne({ "widgetKey.key": key }, projection, function(error, userData) {

        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData);
        } else {
            callback(error, null);
        }

    })
};

// Function to get user profile using userId
UserManagement.prototype.getUserBasicProfile = function(userId, callback) {
    if (checkRequred(userId)) {
        myUserCollection.findOne({ _id: userId }, { contacts: 0 }, function(error, userData) {

            if (userData != null && userData != undefined && userData != '') {
                callback(error, userData);

            } else {

                callback(error, null);
            }

        });
    } else callback({}, null);
};

UserManagement.prototype.getUserSocialInfo = function(idArr, callback) {
    if (checkRequred(idArr)) {
        myUserCollection.find({ "_id": { "$in": idArr } }, { firstName: 1, lastName: 1, publicProfileUrl: 1, google: 1, facebook: 1, linkedin: 1, twitter: 1 }, function(error, userData) {

            if (userData != null && userData != undefined && userData != '') {
                callback(error, userData);
            } else {
                callback(error, null);
            }
        });
    } else callback(null, null);
};

UserManagement.prototype.getBasicUserInfoByArrayOfId = function(idArr, callback) {
    myUserCollection.find({ "_id": { "$in": idArr } }, { contacts: 0, google: 0, facebook: 0, linkedin: 0, twitter: 0, birthday: 0, calendarAccess: 0 }, function(error, userData) {

        callback(error, userData);
    });
};

UserManagement.prototype.getUserListByIdRequestedFields = function(idArr, fields, callback) {
    myUserCollection.find({ "_id": { "$in": idArr } }, fields, function(error, userData) {
        if (error) {
            callback([])
        } else
            callback(userData);
    });
};

UserManagement.prototype.getBasicUserInfoThreeFieldsByArrayOfId = function(idArr, callback) {
    getUserListByIdThreeFields(idArr, callback)
};

function getUserListByIdThreeFields(idArr, callback) {
    myUserCollection.find({ "_id": { "$in": idArr } }, { firstName: 1, lastName: 1, locationLatLang: 1, location: 1, currentLocation: 1, publicProfileUrl: 1 }, function(error, userData) {
        callback(error, userData);
    });
}

UserManagement.prototype.getUserInfoByArrayOfEmailIdWithCustomFields = function(idArr, fields, callback) {

    myUserCollection.find({ "emailId": { "$in": idArr } }, fields, function(error, userData) {

        callback(error, userData);
    });
};

UserManagement.prototype.getUsersWithCustomFields = function(idArr, fields, callback) {

    myUserCollection.find({ "_id": { "$in": idArr } }, fields, function(error, userData) {
        callback(error, userData);
    });
};

//Function to get user profile using emailId
UserManagement.prototype.findUserProfileByEmailId = function(email, callback) {
    myUserCollection.findOne({ emailId: email },{contacts:0}, function(error, userData) {
        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData);
        } else {
            callback(error, null);
        }
    });
};

UserManagement.prototype.findUserProfileByEmailIdWithCustomFields = function(email, fields, callback) {
    myUserCollection.findOne({ emailId: email }, fields, function(error, userData) {

        if (error) {
            loggerError.info('An error occurred in findUserProfileByEmailIdWithCustomFields() ' + JSON.stringify(error));
        }

        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData);
        } else {

            callback(error, null);
        }

    });
};

UserManagement.prototype.updateLastInteractedDate = function(userId, contacts, callback) {
    
    contacts.forEach(function(obj) {

        myUserCollection.update({ _id: userId, 'contacts.personEmailId': obj._id }, { "$set": { "contacts.$.lastInteracted": new Date(obj.lastInteractedDate) } }, { upsert: true }, function(error, result) {
            if (error) {
                callback(false);
            } else if (result) {
                callback(true);
            }
        });
    });
}

UserManagement.prototype.findUserProfileByIdWithCustomFields = function(id, fields, callback) {
    myUserCollection.findOne({ _id: id }, fields, function(error, userData) {

        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData);

        } else {

            callback(error, null);
        }
    });
}

UserManagement.prototype.findUserProfileByEmailWithCustomFields = function(email, fields, callback) {
    myUserCollection.findOne({ emailId: email }, fields, function(error, userData) {

        if (userData != null && userData != undefined && userData != '') {
            callback(error, userData);

        } else {

            callback(error, null);
        }
    });
}

UserManagement.prototype.findFavoriteTwitterContacts = function(id, callback) {
    
    var q = {
        $or: [
            {
                "contacts.twitterUserName": { $exists: true },
                "contacts.twitterUserName": { $nin: ["", " ", null] }
            },
            {
                "contacts.account.twitterUserName": { $exists: true },
                "contacts.account.twitterUserName": { $nin: ["", " ", null] }
            }
        ]
    }

    myUserCollection.aggregate([
            {   $match: { _id: id } },
            {   $unwind: '$contacts' },
            { $match: q
            },
            { $group:{
                _id:null,
                contacts: { $push: '$contacts' }
            }}
        ]).exec(function (err,contacts) {
        callback(err,contacts)
    });
};

UserManagement.prototype.findTwitterUsername = function(id, callback) {

    var q = {
        $or: [
            {
                "contacts.twitterUserName": { $exists: true },
                "contacts.twitterUserName": { $nin: ["", " ", null] }
            },
            {
                "contacts.account.twitterUserName": { $exists: true },
                "contacts.account.twitterUserName": { $nin: ["", " ", null] }
            }
        ]
    }

    myUserCollection.aggregate([
            {   $match: { _id: id } },
            {   $unwind: '$contacts' },
            { $match: q
            },
            { $group:{
                _id:"$twitter",
                contacts: { $push: '$contacts' }
            }}
        ]).exec(function (err,contacts) {
        callback(err,contacts)
    });
};

UserManagement.prototype.findWatchListContacts = function(id,type,callback) {

    var query = {
        $or: [
            { "contacts.watchThisContact": true },
            { "contacts.favorite": true }
        ]
    };

    // var query = {
    //     "contacts.watchThisContact": true
    // };

    // if(type && type === 'companies'){
    //     query = {
    //         "contacts.account.watchThisAccount": true
    //     };
    // }

    if(type && type === 'companies'){
        query = {
            $or: [
                { "contacts.account.watchThisAccount": true },
                { "contacts.favorite": true }
            ],
            "contacts.account.name":{$nin:[null,false,'', ' ']}
        };
    }

    myUserCollection.aggregate([
        {   $match: { _id: id } },
        {   $unwind: '$contacts' },
        { $match: query
        },
        { $group:{
            _id:null,
            contacts: { $push: '$contacts' },
            // pname: { $addToSet: "$contacts.personName" }

        }}
    ]).exec(function (err,contacts) {
        callback(err,contacts)
    });
};

UserManagement.prototype.findFavoriteContacts = function(id,callback) {

    myUserCollection.aggregate([
        {   $match: { _id: id } },
        {   $unwind: '$contacts' },
        { $match: { "contacts.favorite": true }
        },
        { $group:{
            _id:"$emailId",
            contacts: { $push: '$contacts' }
        }}
    ]).exec(function (err,contacts) {
        callback(err,contacts)
    });
};

UserManagement.prototype.findUserContactsTwitterUsername = function(id,participants,callback) {

    var queryFinal = {
        "contacts.personEmailId": {$nin:participants},
        "contacts.twitterUserName": {$nin:['',' ',null]}
    }

    var project = {
        personEmailId:"$contacts.personEmailId",
        twitterUserName:"$contacts.twitterUserName"
    }

    myUserCollection.aggregate([
        {   $match: { _id: id } },
        {   $unwind: '$contacts' },
        {
            $match: queryFinal
        },
        {
            $project:project
        }
    ]).exec(function (err,contacts) {
        callback(err,contacts)
    });
};

UserManagement.prototype.findUserProfileByIdWithCustomFieldsWithIndex = function(id, fields, index, callback) {
    myUserCollection.findOne({ _id: id }, fields, function(error, userData) {

        if (userData != null && userData != undefined && userData != '') {
            callback(error, index, userData);

        } else {

            callback(error, null);
        }

    });
}

// Function to check profile exist or not using public profile url
UserManagement.prototype.isExistingProfile = function(url, uniqueName, callback) {

    findUserExistByUniqueName(uniqueName, function(error, flag, user) {

        if (flag) {
            callback(error, flag, user)
        } else {
            findUserExistByUniqueName(url, callback)
        }
    });
}

function findUserExistByUniqueName(uniqueName, callback) {
    myUserCollection.findOne({ publicProfileUrl: uniqueName }, { contacts: 0 }, function(error, user) {
        if (error) {
            callback(error, false, user);
        } else if (user == null || user == '' || user == undefined) {
            callback(error, false, user);
        } else callback(error, true, user);

    });
}

function getUserByUniqueName(uniqueName, callback) {
    myUserCollection.findOne({ publicProfileUrl: uniqueName }, { contacts: 0 }, function(error, user) {

        if (error) {
            callback(error, false);
        } else if (user == null || user == '' || user == undefined) {
            callback(error, false);
        } else {
            callback(error, user)
        }
    });
}

// Function to find use public profile
UserManagement.prototype.findUserPublicProfile = function(url, uniqueName, callback) {

    getUserByUniqueName(uniqueName, function(error, user) {

        if (user) {
            callback(error, user)
        } else {

            getUserByUniqueName(url, callback);
        }
    });
};

//Function to send invitation
UserManagement.prototype.scheduleInvitation = function(invitationDetails, callback) {
    var invitation = constructInvitationData(invitationDetails, false);

    createInvitation(invitation, function(error, invitation, msg) {
        callback(error, invitation, msg)
    })
};

UserManagement.prototype.scheduleInvitationSelf = function(invitationDetails, callback) {
    var invitation = constructInvitationData(invitationDetails, true);

    createInvitation(invitation, function(error, invitation, msg) {
        callback(error, invitation, msg)
    })
};

function createInvitation(invitation, callback) {
    invitation.save(function(error, invitationData) {
        if (error) {
            invitationData = false;
            callback(error, invitationData, { message: 'Error occurred while saving invitation in DB ' + error })
        } else {
            callback(error, invitationData, { message: 'saving invitation in DB is success' })
        }
    });
}

UserManagement.prototype.getMeetingInteractionsSummary = function(loggedInUser, notLoggedInUser, callback) {
    scheduleInvitation.find({ senderId: loggedInUser, deleted: { $ne: true }, "to.receiverId": notLoggedInUser, scheduleTimeSlots: { $elemMatch: { isAccepted: true } } }, function(error, meetings1) {
        scheduleInvitation.find({ senderId: notLoggedInUser, deleted: { $ne: true }, "to.receiverId": loggedInUser, scheduleTimeSlots: { $elemMatch: { isAccepted: true } } }, function(error, meetings2) {
            scheduleInvitation.find({ senderId: notLoggedInUser, deleted: { $ne: true }, "toList.receiverId": loggedInUser, scheduleTimeSlots: { $elemMatch: { isAccepted: true } } }, function(error, meetings3) {
                scheduleInvitation.find({ senderId: loggedInUser, deleted: { $ne: true }, "toList.receiverId": notLoggedInUser, scheduleTimeSlots: { $elemMatch: { isAccepted: true } } }, function(error, meetings4) {
                    callback(meetings1, meetings2, meetings3, meetings4);
                })
            })
        })
    })
};

UserManagement.prototype.getMeetingInteractionsCount = function(loggedInUser, notLoggedInUser, callback) {
    var q = {
        $or: [
            { senderId: loggedInUser, "to.receiverId": notLoggedInUser },
            { senderId: notLoggedInUser, "to.receiverId": loggedInUser },
            { senderId: notLoggedInUser, "toList.receiverId": loggedInUser },
            { senderId: loggedInUser, "toList.receiverId": notLoggedInUser }
        ],
        deleted: { $ne: true },
        scheduleTimeSlots: { $elemMatch: { isAccepted: true } }
    }
    scheduleInvitation.aggregate([{
        $match: q
    }, {
        $group: {
            _id: null,
            count: {
                $sum: 1
            }
        }
    }]).exec(function(e, d) {

        if (e) {
            logger.info('mongo aggregate error meetings count ' + e);
            callback(0)
        } else {
            if (checkRequred(d) && d.length > 0) {
                callback(d[0].count || 0);
            } else callback(0)
        }
    });

};
// Function to load new invitations
UserManagement.prototype.newInvitations = function(rId, callback) {

    scheduleInvitation.find({
        $or: [
            { 'to.canceled': { $ne: true }, 'to.receiverId': rId, deleted: { $ne: true }, readStatus: false },
            { deleted: { $ne: true }, toList: { $elemMatch: { receiverId: rId, isAccepted: false, canceled: { $ne: true } } } },
            { senderId: rId, deleted: { $ne: true }, readStatus: false }
        ]
    }, function(error, invitations) {
        if (error) {
            logger.info('Mongo error while searching new invitations');
        }

        callback(error, invitations, { message: 'Invitations found in DB' });
    })
};

// Function to load new invitations
UserManagement.prototype.newInvitationsByDateTimezone = function(rId, dateStart, dateEnd, callback) {

    scheduleInvitation.find({
        $or: [{
            to: { canceled: { $ne: true }, receiverId: rId },
            readStatus: false,
            deleted: { $ne: true },
            "scheduleTimeSlots": { "$elemMatch": { "start.date": { '$gte': dateStart, '$lt': dateEnd } } }
        }, {
            deleted: { $ne: true },
            toList: { $elemMatch: { receiverId: rId, isAccepted: false, canceled: { $ne: true } } },
            "scheduleTimeSlots": { "$elemMatch": { "start.date": { '$gte': dateStart, '$lt': dateEnd } } }
        }, {
            senderId: rId,
            deleted: { $ne: true },
            readStatus: false,
            "scheduleTimeSlots": { "$elemMatch": { "start.date": { '$gte': dateStart, '$lt': dateEnd } } }
        }]
    }, function(error, invitations) {
        if (error) {
            loggerError.info('Mongo error in newInvitationsByDateTimezone() ' + JSON.stringify(error));
        }
        callback(invitations);
    });
};

// Function to load new invitations
UserManagement.prototype.newInvitationsByDate = function(rId, callback) {
    var date = new Date();
    var start = new Date().setDate(date.getDate() - 2);
    var end = new Date().setDate(date.getDate() + 2);

    scheduleInvitation.find({ 'to.canceled': { $ne: true }, 'to.receiverId': rId, deleted: { $ne: true }, readStatus: false, "scheduleTimeSlots": { "$elemMatch": { "start.date": { '$gte': start, '$lt': end } } } }, function(error, invitations) {
        scheduleInvitation.find({ deleted: { $ne: true }, toList: { $elemMatch: { receiverId: rId, isAccepted: false, canceled: { $ne: true } } }, "scheduleTimeSlots": { "$elemMatch": { "start.date": { '$gte': start, '$lt': end } } } }, function(error2, invitations2) {
            scheduleInvitation.find({ senderId: rId, deleted: { $ne: true }, readStatus: false, "scheduleTimeSlots": { "$elemMatch": { "start.date": { '$gte': start, '$lt': end } } } }, function(error3, invitations3) {
                if (error) {
                    invitations = false;
                    callback(error, invitations, { message: 'Error occurred while searching for new invitations ' + error });
                } else {
                    invitations = invitations.concat(invitations2);
                    invitations = invitations.concat(invitations3);

                    callback(error, invitations, { message: 'Invitations found in DB' });
                }
            })
        })
    });
};

// Function to load new invitations
UserManagement.prototype.newInvitationsSender = function(sId, callback) {
    scheduleInvitation.find({ senderId: sId, deleted: { $ne: true }, readStatus: false }, function(error, invitations) {
        if (error) {
            invitations = false;
            callback(error, invitations, { message: 'Error occurred while searching for new invitations as sender' + error });
        } else {
            callback(error, invitations, { message: 'Invitations as sender found in DB' });
        }
    });
};

// Function to load new invitations
UserManagement.prototype.newInvitationsAsSender = function(sId, callback) {
    scheduleInvitation.find({ senderId: sId, readStatus: false, deleted: { $ne: true } }, function(error1, invitations) {
        scheduleInvitation.find({ senderId: sId, deleted: { $ne: true }, "toList.isAccepted": false }, function(error2, invitations2) {
            if (checkRequred(invitations) && checkRequred(invitations2)) {
                invitations = invitations.concat(invitations2)
                callback(invitations)
            } else if (checkRequred(invitations)) {
                callback(invitations)
            } else {
                callback(invitations2)
            }
        })

    });
};

UserManagement.prototype.newInvitationsAsReceiver = function(rId, callback) {
    scheduleInvitation.find({ 'to.canceled': { $ne: true }, 'to.receiverId': rId, deleted: { $ne: true }, readStatus: false }, function(error, invitations) {
        scheduleInvitation.find({ deleted: { $ne: true }, toList: { $elemMatch: { receiverId: rId, isAccepted: false, canceled: { $ne: true } } } }, function(error2, invitations2) {

            if (error) {
                invitations = false;
                callback(error, invitations, { message: 'Error occurred while searching for new invitations ' + error });
            } else {
                invitations = invitations.concat(invitations2)

                callback(error, invitations, { message: 'Invitations found in DB' });
            }
        })
    });
};

// Function to retrive invitation using invitation id
UserManagement.prototype.findInvitationById = function(invitationId, callback) {
    scheduleInvitation.findOne({ $or:[{invitationId: invitationId},{recurrenceId:invitationId}], deleted: { $ne: true } }, function(error, invitation) {
        if (error) {
            invitation = false;
            callback(error, invitation, { message: 'Error occured while searching for invitations ' + error });
        } else {
            callback(error, invitation, { message: 'Invitation found for invitation id' });
        }
    });
};

UserManagement.prototype.findInvitationByIdCustomFields = function(invitationId, fields, fromAllMeetings, callback) {
    if (fromAllMeetings) {
        scheduleInvitation.findOne({ invitationId: invitationId }, fields, function(error, invitation) {
            callback(invitation)
        });
    } else {
        scheduleInvitation.findOne({ invitationId: invitationId, deleted: { $ne: true } }, fields, function(error, invitation) {
            callback(invitation)
        });
    }
};

UserManagement.prototype.findInvitationWithEmail = function(iId, receiverEmailId, callback) {

    scheduleInvitation.findOne({ invitationId: iId, deleted: { $ne: true }, "toList.receiverEmailId": receiverEmailId }, function(error, result) {
        var flag;
        if (error) {
            callback(error, null);
        } else callback(error, result)

    });
};

UserManagement.prototype.getMeetingsByDatesBetween = function(userId, invitationId, dateStart, dateEnd, notList, callback) {

    var mainQuery = [];

    for (var dates = 0; dates < dateStart.length; dates++) {
        var query = {
            $or: [
                { senderId: userId, deleted: { $ne: true } },
                { deleted: { $ne: true }, "to.receiverId": userId },
                { deleted: { $ne: true }, "toList.receiverId": userId }
            ],
            "scheduleTimeSlots": {
                "$elemMatch": {
                    "start.date": {
                        '$gte': dateStart[dates],
                        '$lte': dateEnd[dates]
                    }
                }
            }
        };
        mainQuery.push(query);
    }

    scheduleInvitation.find({
            invitationId: { $ne: invitationId },
            $or: mainQuery
        },
        function(error, result) {
            if (error) {
                callback([]);
            } else
                callback(result);
        }
    )
}

UserManagement.prototype.updateInvitationToList = function(iId, userInfo, callback) {

    scheduleInvitation.update({ invitationId: iId, deleted: { $ne: true }, "toList.receiverEmailId": userInfo.receiverEmailId }, { $set: { "toList.$.receiverId": userInfo.receiverId, "toList.$.receiverEmailId": userInfo.receiverEmailId, "toList.$.receiverFirstName": userInfo.receiverFirstName, "toList.$.receiverLastName": userInfo.receiverLastName } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating invitation with user to list ' + error });
        } else
        if (result == 0) {
            flag = false;

            callback(error, flag, { message: 'Update invitation with user to list failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'invitation with user to list successfully updated' });
        }

    });
}

UserManagement.prototype.updateInvitationToListNotAccepted = function(iId, toList, callback) {
    scheduleInvitation.update({ invitationId: iId, deleted: { $ne: true } }, { $set: { toList: toList } }, function(error, result) {
        var flag;

        if (error) {
            callback(error, false, { message: 'Error in DB while updating invitation with user to list to not accepted' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'invitation with user to list to not accepted successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'Update invitation with user to list to not accepted failed' });
        }
    });
}

UserManagement.prototype.updateInvitationToListAccepted = function(iId, userInfo, callback) {
    var date = new Date();

    var rId = userInfo.receiverId;

    if (userInfo.isSuggested == true || userInfo.isSuggested == 'true') {
        rId = userInfo.toListUserId;
    }

    if (userInfo.toListLength > 1) {
        rId = userInfo.loggeinUserId;
    }

    scheduleInvitation.update({ invitationId: iId, deleted: { $ne: true }, "toList.receiverId": rId }, { $set: { "toList.$.isAccepted": true, "toList.$.acceptedDate": date } }, function(error, result) {
        var flag;

        if (error) {
            callback(error, false, { message: 'Error in DB while updating invitation with user to list accepted' + error });
        } else
        if (result == 0) {
            flag = false;

            callback(error, flag, { message: 'Update invitation with user to list accepted failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'invitation with user to list accepted successfully updated' });
        }
    });
}

UserManagement.prototype.getMeetingsByArrayOfId = function(idArr, callback) {

    scheduleInvitation.find({ deleted: { $ne: true }, "invitationId": { "$in": idArr } }, function(error, meetingList) {
        callback(error, meetingList);
    })
};

UserManagement.prototype.updateInvitationStatusToDelete = function(invitationId, callback) {
    scheduleInvitation.update({ invitationId: invitationId, deleted: { $ne: true } }, { $set: { readStatus: true, deleted: true } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating invitation to delete ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'updateInvitation to delete failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'updateInvitation to delete success' });
        }
    });
};

UserManagement.prototype.deleteMeeting = function(userId, invitationId, callback) {

    scheduleInvitation.findOneAndUpdate({ invitationId: invitationId, deleted: { $ne: true }, senderId: userId }, { deleted: true, deletedDate: new Date() }, function(error, result) { // Find and modify will return document.
        if (error) {
            loggerError.info('Mongo error in deleting meeting ' + JSON.stringify(error));
            callback(false)
        } else {
            callback(result);
        }
    })
};

UserManagement.prototype.cancelMeeting = function(userId, invitationId, selfCalendar, comment, callback) {
    if (selfCalendar == true || selfCalendar == 'true') {
        var updateObjSelf = {};
        if (checkRequred(comment)) {
            updateObjSelf = {
                "toList.$.canceled": true,
                "toList.$.canceledDate": new Date(),
                $push: {
                    comments: {
                        messageDate: new Date(),
                        message: comment,
                        userId: userId
                    }
                }
            };
        } else {
            updateObjSelf = {
                "toList.$.canceled": true,
                "toList.$.canceledDate": new Date()
            };
        }

        scheduleInvitation.findOneAndUpdate({ invitationId: invitationId, "toList.receiverId": userId, deleted: { $ne: true } }, updateObjSelf, function(error, result) {
            if (error) {
                loggerError.info('Mongo error in cancelling meeting ' + JSON.stringify(error))
            }
            callback(result);
        })
    } else {
        var updateObjNormal = {};
        if (checkRequred(comment)) {
            updateObjNormal = {
                "to.canceled": true,
                "to.canceledDate": new Date(),
                $push: {
                    comments: {
                        messageDate: new Date(),
                        message: comment,
                        userId: userId
                    }
                }
            };
        } else {
            updateObjNormal = {
                "to.canceled": true,
                "to.canceledDate": new Date()
            };
        }

        scheduleInvitation.findOneAndUpdate({ invitationId: invitationId, deleted: { $ne: true }, "to.receiverId": userId }, updateObjNormal, function(error, result) {
            if (error) {
                loggerError.info('Mongo error in cancelling meeting ' + JSON.stringify(error))
            }
            callback(result);
        })
    }
};

UserManagement.prototype.updateInvitation = function(invitationId, invitationObj, comment, callback) {
    scheduleInvitation.update({ invitationId: invitationId, deleted: { $ne: true } }, { $set: invitationObj }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating invitation ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'updateInvitation failed' });
        } else {
            flag = true;
            if (checkRequred(comment.message)) {
                scheduleInvitation.update({ invitationId: invitationId, deleted: { $ne: true } }, { $push: { comments: comment } }, function(e, r) {
                    callback(error, flag, { message: 'updateInvitation success' });
                });
            } else callback(error, flag, { message: 'updateInvitation success' });
        }
    });
}

UserManagement.prototype.updateInvitationStatusToRescheduled = function(invitationId, callback) {
    scheduleInvitation.update({ invitationId: invitationId, deleted: { $ne: true } }, { $set: { readStatus: true, status: 'reScheduled' } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating invitation to reScheduled ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'updateInvitation to reScheduled failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'updateInvitation to reScheduled success' });
        }
    });
}

UserManagement.prototype.updateInvitationReadStatus = function(invitation, userId, callback) {
    var iId = invitation.invitationId;
    // var slotId = invitation.scheduleTimeSlots[invitation.timeSlotNumber]._id;

    var recurrenceId = invitation.recurrenceId;

    scheduleInvitation.update({ $or:[{invitationId: iId},{recurrenceId:recurrenceId}], deleted: { $ne: true } }, { $set: { readStatus: true } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating invitationReadStatus ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'updateInvitationReadStatus failed' });
        } else {
            if (checkRequred(invitation.slotId)) {
                updateSelectedTimeSlot(invitation, userId, function() {
                    flag = true;
                    callback(error, flag, { message: 'InvitationReadStatus successfully updated' });
                });
            } else {
                flag = true;
                callback(error, flag, { message: 'InvitationReadStatus successfully updated but slot not updated.' });
            }
        }
    });
};

function updateSelectedTimeSlot(invitation, userId, back) {
    var date = new Date();
    var iId = invitation.invitationId;
    var slotId = invitation.slotId;
    var message = invitation.message.replace(/<br>/g, '\n')
    scheduleInvitation.update({ "scheduleTimeSlots._id": slotId, deleted: { $ne: true } }, { $set: { "scheduleTimeSlots.$.isAccepted": true, "scheduleTimeSlots.$.message": invitation.message, "scheduleTimeSlots.$.acceptedDate": date } }, function(error, result) {
        if (checkRequred(invitation.message)) {
            var msg = {
                messageDate: date,
                message: message,
                userId: userId
            }
            scheduleInvitation.update({ "scheduleTimeSlots._id": slotId, deleted: { $ne: true } }, { $push: { "scheduleTimeSlots.$.messages": msg } }, function(error, result2) {
                back()
            })
        } else back()

    });
}

UserManagement.prototype.updateInvitationWithDocument = function(iId, docInfo, callback) {
    scheduleInvitation.update({ invitationId: iId, deleted: { $ne: true }, "docs.documentId": { $ne: docInfo.documentId } }, { $push: { docs: docInfo } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating invitation with document ' + error });
        } else
        if (result == 0) {
            flag = false;

            callback(error, flag, { message: 'Update invitation with document failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'invitation with document successfully updated' });
        }

    });
}

UserManagement.prototype.updateInvitationWithDocId = function(invitationId, mdocId, document, callback) {

    scheduleInvitation.update({ invitationId: invitationId, deleted: { $ne: true }, "docs._id": mdocId }, { $set: { "docs.$.documentId": document._id } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while Updating invitation with document id ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'Updating invitation with document id failed' });
        } else {
            flag = true;

            callback(error, flag, { message: 'Updating invitation with document id success' });
        }

    });
};

UserManagement.prototype.updateInvitationWithIcs = function(icsDetails, callback) {
    var ics = {
        awsKey: icsDetails.icsAwsKey,
        url: icsDetails.icsUrl
    };
    scheduleInvitation.update({ invitationId: icsDetails.invitationId, deleted: { $ne: true } }, { $set: { icsFile: ics } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while Updating invitation with ics file ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'Updating invitation with ics file failed' });
        } else {
            flag = true;

            callback(error, flag, { message: 'Updating invitation with ics file success' });
        }

    });
}

UserManagement.prototype.removeInvitation = function(rId, callback) {
    scheduleInvitation.remove({ invitationId: rId, deleted: { $ne: true } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while removeing invitation ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'removeInvitation failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'removeInvitation success' });
        }

    });
};

// Function to store document details
UserManagement.prototype.storeDocument = function(docDetails, callback) {
    var documentDetails = constructDocument(docDetails);
    documentDetails.save(function(error, document) {
        if (error) {
            document = false;
            callback(error, document, { message: 'Error occurred while saving document details ' + error });
        } else {
            callback(error, document, { message: 'Document details successfully stored' });
        }
    });
};

//Function to get total meetings of one user(Total meetings related to user)
UserManagement.prototype.getTotalMeetingsUser = function(userId, callback) {
    scheduleInvitation.find({ senderId: userId, readStatus: true, deleted: { $ne: true } }, function(error, invitations) {
        if (error) {
            invitations = [];
            callback(error, invitations, { message: 'Error occurred while searching meetings count ' + error });
        } else {
            //var count = invitations.length;
            scheduleInvitation.find({ "to.canceled": { $ne: true }, "to.receiverId": userId, readStatus: true, deleted: { $ne: true } }, function(error, invitationsMe) {
                if (error) {
                    invitationsMe = invitations;
                    callback(error, invitationsMe, { message: 'Error occurred while searching meetings count ' + error });
                } else {
                    invitationsMe.forEach(function(invi) {
                        invitations.push(invi)
                    });
                    callback(error, invitations, { message: 'Searching meeting count success' });
                }
            })

        }
    })
};

UserManagement.prototype.getAllUsersAcceptedMeetings = function(callback) {
    scheduleInvitation.find({ readStatus: true, "scheduleTimeSlots.isAccepted": true, deleted: { $ne: true } }, function(error, meetings) {
        if (error) {
            logger.info('Mongo error in fetching all users accepted meetings')
        }
        callback(meetings)
    });
};

UserManagement.prototype.getAllUsersDocuments = function(callback) {
    mySharedDocs.find({}, function(error, documents) {
        if (error) {
            logger.info('Mongo error in fetching all users documents');
        }
        callback(documents)
    })
}

//Function to get total meetings of one user(Total meetings related to user)
UserManagement.prototype.getTotalAcceptedMeetings = function(userId, callback) {
    scheduleInvitation.find({
        $or: [
            { senderId: userId, readStatus: true, deleted: { $ne: true }, "scheduleTimeSlots.isAccepted": true },
            { "to.canceled": { $ne: true }, "to.receiverId": userId, readStatus: true, deleted: { $ne: true }, "scheduleTimeSlots.isAccepted": true },
            { toList: { $elemMatch: { receiverId: userId, isAccepted: true, canceled: { $ne: true } } }, readStatus: true, deleted: { $ne: true }, "scheduleTimeSlots.isAccepted": true }
        ]
    }, function(error, invitations) {
        if (error) {
            logger.info('Mongo error while querying all accepted meetings ' + error);
        }
        callback(error, invitations, { message: 'Searching accepted meetings success' });
    })

};

//Function to update meeting with google event id
UserManagement.prototype.updateMeetingWithGoogleEventId = function(invitationId, googleEId, iCalUID, count, userId, callback) {
    var updateObj = { googleEventId: googleEId, updateCount: count, lastActionUserId: userId };
    if (checkRequred(iCalUID)) {
        updateObj.iCalUID = iCalUID;
    }

    scheduleInvitation.update({ invitationId: invitationId, deleted: { $ne: true } }, { $set: updateObj }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating Invitation with google event id ' + error });
        }
        if (result == 0) {
            flag = false;

            callback(error, flag, { message: 'Updating invitation with google event id failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'Invitation with google event id successfully updated' });
        }
    });
};

//Function to update meeting with google event id
UserManagement.prototype.updateMeetingWithOfficeEventId = function(invitationId, officeEventId, iCalUID, userId, callback) {
    var updateObj = { officeEventId: officeEventId, lastActionUserId: userId };
    if (checkRequred(iCalUID)) {
        updateObj.iCalUId = iCalUID;
    }
    scheduleInvitation.update({ invitationId: invitationId, deleted: { $ne: true } }, { $set: updateObj }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating Invitation with office event id ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'Updating invitation with office event id failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'Invitation with office event id successfully updated' });
        }
    });
};

// Function to add invities
UserManagement.prototype.updateMeetingWithNewInvities = function(invitationId, invities, callback) {
    scheduleInvitation.findOne({ invitationId: invitationId, deleted: { $ne: true } }, function(error, invitation) {
        if (error) {
            callback(error, false, { message: 'Updating invitation with invities failed ' + error });
        } else if (invitation != null && invitation != undefined) {
            if (invitation.participants[0]) {
                invitation.participants.forEach(function(person) {
                    invities.push({
                        emailId: person.emailId
                    });
                })
            }
            var invitiesArray = removeDuplicates(invities);
            scheduleInvitation.update({ invitationId: invitationId, deleted: { $ne: true } }, { $set: { participants: invitiesArray } }, function(error, result) {
                var flag;
                if (error) {
                    callback(error, false, { message: 'Error in DB while updating Invitation with participants ' + error });
                }
                if (result == 0) {
                    flag = false;

                    callback(error, flag, { message: 'Updating invitation with participants failed' });
                } else {
                    flag = true;
                    callback(error, flag, { message: 'Invitation with participants successfully updated' });
                }
            });
        } else {
            callback(error, false, { message: 'Updating invitation with invitees failed (invitation not found) ' });
        }
    });
};

// Function to share a document
UserManagement.prototype.shareDocument = function(share, callback) {
    var docId = share.docId;
    var shareDetails = share.emails;

    mySharedDocs.findOne({ _id: docId }, function(error, document) {
        if (error) {
            callback(error, false, { message: 'Updating document with share failed ' + error });
        } else if (checkRequred(document)) {
            var isExist = false;
            if (document.sharedWith[0]) {
                var shareWith = document.sharedWith;
                var email = share.emails[0].emailId;
                for (var i = 0; i < shareWith.length; i++) {
                    if (shareWith[i].emailId == email) {
                        isExist = true;
                    }
                }
            }
            if (isExist) {
                callback(error, 'exist', { message: 'Updating document with sharing failed' });
            } else {

                mySharedDocs.update({ _id: docId }, { $push: { sharedWith: share.emails[0] } }, function(error, result) {
                    var flag;
                    if (error) {
                        callback(error, false, { message: 'Error in DB while updating document with share details ' + error });
                    }
                    if (result && result.ok) {
                        flag = true;
                        callback(error, flag, { message: 'document sharing success' });
                    } else {
                        flag = false;
                        callback(error, flag, { message: 'Updating document with sharing failed' });
                    }

                });
            }
        } else {
            callback(error, false, { message: 'Updating document with share details failed (document not found) ' });
        }
    });
}

UserManagement.prototype.shareDocumentV2 = function(docId, share, callback) {

    mySharedDocs.findOne({ _id: docId }, function(error, document) {
        if (error) {
            callback(error, false, { message: 'Updating document with share failed ' + error });

        } else if (checkRequred(document)) {
            var isExist = false;
            if(document.sharedWith.length > 0) {
                _.each(document.sharedWith, function(shareWith) {
                    if(shareWith.emailId == share.emailId) {
                        isExist = true;
                    }
                });
            }

            if(isExist) 
                callback(error, 'exist', 'Document already shared with this contact');
            else {
                mySharedDocs.update({ _id: docId }, { $push: { sharedWith: share } }, function(error, result) {
                    if (error) 
                        callback(error, false, { message: 'Error in DB while updating document with share details ' + error });
                    else {
                        callback(error, result, {message: 'Document shared Successfully'})
                    }
                });
            }
        } else {
            callback(error, false, { message: 'Updating document with share details failed (document not found) ' });
        }
    });
}

UserManagement.prototype.getDocumentSharedWithDetails = function(docId, sharedWithEmailId, callback) {
    mySharedDocs.find({ _id: docId, "sharedWith.emailId": sharedWithEmailId }, { "sharedWith.$": 1, "documentName":1, _id:1}, function(error, document) {
        if(error) {
            callback(error, document, {message: 'Failed to get shared document'})
        } else {
            callback(error, document, {message: 'Shared document got successfully'})
        }
    })
}


UserManagement.prototype.getDocumentsInteractionsSummary = function(loggedInUser, notLoggedInUser, callback) {
    mySharedDocs.find({ "sharedBy.userId": loggedInUser, "sharedWith.userId": notLoggedInUser }, { "sharedWith.$": 1, documentName: 1, sharedDate: 1 }, function(error, docs1) {
        mySharedDocs.find({ "sharedBy.userId": notLoggedInUser, "sharedWith.userId": loggedInUser }, { "sharedWith.$": 1, documentName: 1, sharedDate: 1 }, function(error, docs2) {
            callback(docs1, docs2);
        })
    })
}

UserManagement.prototype.getDocumentsInteractionsCount = function(loggedInUser, notLoggedInUser, callback) {
    var q = {
        $or: [
            { "sharedBy.userId": loggedInUser, "sharedWith.userId": notLoggedInUser },
            { "sharedBy.userId": notLoggedInUser, "sharedWith.userId": loggedInUser }
        ]
    }
    mySharedDocs.aggregate([{
        $match: q
    }, {
        $group: {
            _id: null,
            count: {
                $sum: 1
            }
        }
    }]).exec(function(e, d) {

        if (e) {
            logger.info('mongo aggregate error in interaction count docs ' + e);
            callback(0)
        } else {
            if (checkRequred(d) && d.length > 0) {
                callback(d[0].count || 0);
            } else callback(0)
        }
    })

}

//Function to get total documents of one user(Documents shared with others)
UserManagement.prototype.getDocumentsSharedWithOthers = function(userId, callback) {
    mySharedDocs.find({ "sharedBy.userId": userId, isDeleted: { $ne: true} }).lean().exec(function(error, documents) {

        if (error) {
            documents = []
            callback(error, documents, { message: 'Searching documents (Documents shared with others) fail ' + error });
        } else {
            callback(error, documents, { message: 'Searching documents (Documents shared with others) success' });
        }
    });
};

UserManagement.prototype.getNotOpenedDocuments = function(userId, query, timezone, callback) {
    var match = {
        "sharedBy.userId": userId,
        "isDeleted": {$ne: true}
    };
    var fromDate, toDate, filter;

    if(query.fromDate) {
        fromDate = moment(query.fromDate, "DDMMYYYY").tz(timezone).subtract(1, 'days').startOf('day');
        toDate = moment(fromDate).tz(timezone).endOf('day');

        filter = {
            input: "$sharedWith",
            as: "doc",
            cond: { $and: [
                {$eq: ["$$doc.accessed", false]},
                {$gte: ["$$doc.sharedOn", new Date(fromDate)]},
                {$lte: ["$$doc.sharedOn", new Date(toDate)]}
            ]
                
            }
        }
    }
    else {
        filter = {
            input: "$sharedWith",
            as: "doc",
            cond: { $eq: ["$$doc.accessed", false] }
        }
    }

    var aggregation = [
        {
            $match: match
        }, 
        {
            $addFields: {
                sharedWith: {
                    $filter: filter
                }
            }
        },
        {
            $addFields: {
                notAccessedDocsCount: {
                    $size:"$sharedWith"
                }
            }
        },
    ];

    mySharedDocs.aggregate(aggregation).exec(function(error, documents) {
        if (error) {
            documents = []
            callback(error, documents, { message: 'Searching documents (Documents not opened by shared users) fail ' + error });
        } else {
            callback(error, documents, { message: 'Searching documents (Documents not opened by shared users) success' });
        }
    })
}

//Function to get total documents of one user(Documents shared with me)
UserManagement.prototype.documentsSharedWithMe = function(userId, callback) {
    mySharedDocs.find({ sharedWith: { $elemMatch: { userId: userId, accessStatus: true } } }, function(error, documents) {

        if (error) {
            documents = []
            callback(error, documents, { message: 'Searching documents (Documents shared with me) fail ' + error });
        } else {
            callback(error, documents, { message: 'Searching document (Documents shared with me) success' });
        }
    })
};

//Function to get total documents of one user(Documents shared with me)
UserManagement.prototype.documentsSharedWithMeNonRead = function(userId, callback) {
    mySharedDocs.find({ sharedWith: { $elemMatch: { userId: userId, accessStatus: true, firstAccessed: { $exists: false } } } }, { "sharedWith.$": 1, sharedBy: 1, documentName: 1, sharedDate: 1 }, function(error, documents) {
        if (error) {
            documents = [];
            callback(error, documents, { message: 'Searching documents (Documents shared with me) fail ' + error });
        } else {
            callback(error, documents, { message: 'Searching document (Documents shared with me) success' });
        }
    })
};

UserManagement.prototype.documentsSharedWithMeNonReadCount = function(userId, callback) {

    mySharedDocs.aggregate([{
        $match: { sharedWith: { $elemMatch: { userId: userId, accessStatus: true, firstAccessed: { $exists: false } } } }
    }, {
        $group: {
            _id: null,
            count: {
                $sum: 1
            }
        }
    }]).exec(function(e, d) {
        if (e) {
            logger.info('mongo aggregate error documentsSharedWithMeNonReadCount ' + e);
            callback(0)
        } else {
            if (checkRequred(d) && d.length > 0) {
                callback(d[0].count || 0);
            } else callback(0)
        }
    });
}

UserManagement.prototype.sharedDocRed = function(userId, callback) {
    mySharedDocs.find({ "sharedBy.userId": userId, sharedWith: { $elemMatch: { accessed: true } } }, function(error, documents) {
        if (error) {
            documents = []
            callback(error, documents, { message: 'Searching documents (Documents shared with me) fail ' + error });
        } else {
            callback(error, documents, { message: 'Searching document (Documents shared with me) success' });
        }
    })
};

UserManagement.prototype.sharedDocRedCount = function(userId, callback) {
    mySharedDocs.aggregate([{
        $match: { "sharedBy.userId": userId, sharedWith: { $elemMatch: { accessed: true } } }
    }, {
        $group: {
            _id: null,
            count: {
                $sum: 1
            }
        }
    }]).exec(function(e, d) {
        if (e) {
            logger.info('mongo aggregate error sharedDocRedCount ' + e);
            callback(0)
        } else {
            if (checkRequred(d) && d.length > 0) {
                callback(d[0].count || 0);
            } else callback(0)
        }
    });
}

UserManagement.prototype.findDocumentById = function(docId, callback) {
    mySharedDocs.findOne({ _id: docId }, function(error, document) {
        if (error) {
            document = false
            callback(error, document, { message: 'Searching document with url fail ' + error });
        } else {
            callback(error, document, { message: 'Searching document with url success' });
        }
    });
};

UserManagement.prototype.findDocumentBySharedObjId = function(docId, sharedObjId, callback) {
    
    mySharedDocs.findOne({_id: docId, "sharedWith._id":sharedObjId}, function(error, document) {
        if(error) {
            callback(error, false, {message: 'User not authorized to access document'})
        } else {
            callback(error, document, {message:'user authorized to access document'})
        }
    })
}

// Update document read status
UserManagement.prototype.updateDocumentReadInfo = function(readInfo, callback) {
    var docId = readInfo.documentId;
    var userId = readInfo.userId;
    var firstAccessed = readInfo.firstAccessed;
    var count = readInfo.count;
    var pageReadInfo = {
        pageNumber: readInfo.pageNumberReaded,
        Time: readInfo.pageReadDiff,
        lastAccessed: readInfo.lastAccessed
    };

    mySharedDocs.findOne({ _id: docId }, function(error, doc) {
        if (error) {
            logger.error('Error while finding doc using doc id ' + error)
        } else {
            doc.sharedWith.forEach(function(user) {
                if (userId == user.userId) {
                    if (user.firstAccessed == undefined) {
                        mySharedDocs.update({ _id: docId, "sharedWith.userId": userId }, { $set: { "sharedWith.$.firstAccessed": firstAccessed } }, function(error, result) {
                            if (error) {
                                logger.error('Error while inserting first accessed time ' + error);
                            }
                        })
                    }
                }
            });
            mySharedDocs.update({ _id: docId, "sharedWith.userId": userId }, { $push: { "sharedWith.$.accessInfo.pageReadTimings": pageReadInfo } }, function(error, result) {
                if (error) {
                    logger.error('Error while updating pageread timings ' + error)
                }
            });
        }

    });
    updateDocumentFirstAccessed(docId, userId, firstAccessed);
    updateDocumentAccessCount(count, docId, userId)

}

function updateDocumentFirstAccessed(docId, userId, firstAccessed) {
    mySharedDocs.update({ _id: docId, "sharedWith.userId": userId }, { $set: { "sharedWith.$.lastAccessed": firstAccessed } }, function(error, result) {
        if (error) {
            logger.error('Error while updating last accessed time timings ' + error);
        }
    })
}

function updateDocumentAccessCount(count, docId, userId) {
    if (count == true || count == 'true') {
        mySharedDocs.update({ _id: docId, "sharedWith.userId": userId }, { $inc: { "sharedWith.$.accessCount": 1 }, "sharedWith.$.accessed": true }, function(error, docInfo) {
            if (error) {
                logger.error('Error while updating document read count ' + error);
            }
        });
    }
}

UserManagement.prototype.updateDocumentReadInfoV2 = function(readInfo, callback) {
    var docId = readInfo.documentId;
    var sharedUserObjId = readInfo.sharedUserObjId;
    var firstAccessed = readInfo.firstAccessed;
    var lastAccessed = readInfo.lastAccessed;
    var count = readInfo.count;

    var pageReadInfo = {
        pageNumber: readInfo.pageNumberReaded,
        Time: readInfo.pageReadDiff,
        lastAccessed: readInfo.lastAccessed
    };

    mySharedDocs.findOne({ _id: docId }, function(error, doc) {
        if (error) {
            logger.error('Error while finding doc using doc id ' + error);
        } else {

            doc.sharedWith.forEach(function(user) {
                if (sharedUserObjId === user._id.toString()) {
                    if (user.firstAccessed == undefined) {
                        mySharedDocs.update({ _id: docId, "sharedWith._id": sharedUserObjId }, { $set: { "sharedWith.$.firstAccessed": firstAccessed} }, function(error, result) {
                            if (error) {
                                logger.error('Error while inserting first accessed time ' + error);
                            } else {
                                /* myUserCollection.findOne({ emailId: doc.sharedBy.emailId }, {mobileFirebaseToken:1}, function(error, userData) {
                                    if (userData != null && userData != undefined && userData != '') {
                                        composeNotificationMessage(doc.documentName, user, firstAccessed, userData.mobileFirebaseToken);
                                    }
                                }); */
                            }
                        })
                    }
                }
            });
            mySharedDocs.update({ _id: docId, "sharedWith._id": sharedUserObjId }, { $push: { "sharedWith.$.accessInfo.pageReadTimings": pageReadInfo } }, function(error, result) {
                if (error) {
                    logger.error('Error while updating pageread timings ' + error)
                }
            });
        }
    });
    updateDocumentLastAccessed(docId, sharedUserObjId, lastAccessed);
    // updateDocumentAccessCountV2(count, docId, sharedUserObjId)

}

function updateDocumentLastAccessed(docId, sharedUserObjId, lastAccessed) {
    mySharedDocs.update({ _id: docId, "sharedWith._id": sharedUserObjId }, { $set: { "sharedWith.$.lastAccessed": lastAccessed } }, function(error, result) {
        if (error) {
            logger.error('Error while updating last accessed time timings ' + error);
        }
    })
}

UserManagement.prototype.updateDocumentAccessCountV2 = function(docId, sharedUserObjId, callback) {
    // if (count == true || count == 'true') {
        mySharedDocs.update({ _id: docId, "sharedWith._id": sharedUserObjId }, { $inc: { "sharedWith.$.accessCount": 1 }, "sharedWith.$.accessed": true }, function(error, docInfo) {
            if (error) {
                logger.error('Error while updating document read count ' + error);
                callback(error, false);
            } else {
                callback(error, docInfo);
            }
        });
    // }
}

UserManagement.prototype.updateDocumentLinkAccessCount = function(docId, sharedUserObjId, callback) {
    mySharedDocs.update({ _id: docId, "sharedWith._id": sharedUserObjId }, { $inc: { "sharedWith.$.linkAccessCount": 1 }}, function(error, docInfo) {
        if (error) {
            logger.error('Error while updating document read count ' + error);
            callback(error, false);
        } else {
            callback(error, docInfo);
        }
    });
}

UserManagement.prototype.updateAccessed = function(docId, userId, callback) {
        mySharedDocs.update({ _id: docId, "sharedWith.userId": userId }, { "sharedWith.$.accessed": false }, function(error, docInfo) {
            if (error) {
                callback(error, docInfo)
                logger.error('Error while updating document access ' + error);
            } else {
                callback(error, docInfo)
            }
        });
    }
    // Function to check authorization of document
UserManagement.prototype.checkDocumentAuthorization = function(userId, docId, callback) {

    mySharedDocs.findOne({ _id: docId, sharedWith: { $elemMatch: { userId: userId, accessStatus: true } } }, function(error, document) {
        mySharedDocs.findOne({ _id: docId, "sharedBy.userId": userId }, function(errorMe, documentMe) {
            mySharedDocs.findOne({ _id: docId, access: 'public' }, function(errorMe, documentPublic) {
                if (error || errorMe) {
                    document = false;
                    callback(error, false, { message: 'Checking authorization of document fail ' + error });
                } else
                if (document != null || document != undefined) {
                    callback(error, document, { message: 'User authorised to access document' });
                } else if (documentMe != null || documentMe != undefined) {
                    callback(error, documentMe, { message: 'User authorised to access document' });
                } else if (documentPublic != null || documentPublic != undefined) {
                    callback(error, documentPublic, { message: 'User authorised to access document' });
                } else callback(error, false, { message: 'User not authorised to access document' });
            })


        });

    })
};

// Function to check authorization of document
UserManagement.prototype.checkDocumentAuthorizationByEmail = function(emailId, docId, callback) {

    mySharedDocs.findOne({ _id: docId, sharedWith: { $elemMatch: { emailId: emailId, accessStatus: true } } }, function(error, document) {
        if (error) {
            document = false;
            callback(error, false, { message: 'Checking authorization of document fail ' + error });
        } else
        if (document != null || document != undefined) {
            callback(error, document, { message: 'User authorised to access document' });
        } else callback(error, false, { message: 'User not authorised to access document' });
    })
};

UserManagement.prototype.updateDocExpiry = function(expObj, callback) {
    mySharedDocs.update({ _id: expObj.docId, "sharedWith._id": expObj.sharedWithId }, { $set: { "sharedWith.$.expiryDate": expObj.expiryDate, "sharedWith.$.expirySec": expObj.expirySec, "sharedWith.$.accessStatus": true } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating Document expiry date ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'Updating Document expiry date failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'Updating Document expiry date success' });
        }
    });
}

UserManagement.prototype.findAnalytics = function(docId, userId, callback) {
    mySharedDocs.findOne({ _id: docId, "sharedWith.userId": userId }, function(error, accessInfo) {
        if (error) {
            callback(error, false, { message: 'loading of access info failed ' + error });
        } else {
            callback(error, accessInfo, { message: 'loading of access info success ' });
        }
    });
};

UserManagement.prototype.findAnalyticsBySharedUserObjId = function(docId, sharedUserObjId, callback) {
    mySharedDocs.findOne({ _id: docId, "sharedWith._id": sharedUserObjId }, function(error, accessInfo) {
        if (error) {
            callback(error, false, { message: 'Loading of Analytics failed'});
        } else {
            callback(error, accessInfo, { message: 'Loading of Analytics success' });
        }
    });
};

UserManagement.prototype.findAnalytics_email = function(docId, emailId, callback) {
    mySharedDocs.findOne({ _id: docId, "sharedWith.emailId": emailId }, function(error, accessInfo) {
        if (error) {
            callback(error, false, { message: 'loading of access info failed ' + error });
        } else {
            callback(error, accessInfo, { message: 'loading of access info success ' });
        }
    });
};

UserManagement.prototype.updateDocShareWithEmailId = function(docId, userId, obj, callback) {
    mySharedDocs.update({ _id: docId, "sharedWith.emailId": obj.emailId }, { $set: { "sharedWith.$.accessStatus": true, "sharedWith.$.userId": obj.userId, "sharedWith.$.firstName": obj.firstName } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating Document  updateDocShareWithEmailId' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'Document updateDocShareWithEmailId update failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'Document updateDocShareWithEmailId successfully updated' });
        }

    });
};

UserManagement.prototype.updateDocAccess = function(docId, userId, status, callback) {
    mySharedDocs.update({ _id: docId, "sharedWith.userId": userId }, { $set: { "sharedWith.$.accessStatus": status } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating Document access ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'Document access update failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'Document access successfully updated' });
        }

    });
};

UserManagement.prototype.updateDocAccessBySharedWithObjId = function(docId, objId, status, callback) {
    mySharedDocs.update({ _id: docId, "sharedWith._id": objId }, { $set: { "sharedWith.$.accessStatus": status } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating Document access ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'Document access update failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'Document access successfully updated' });
        }

    });
};

UserManagement.prototype.deleteDocument = function(docId, callback) {
    mySharedDocs.updateOne({ _id: docId }, {$set : {isDeleted:true}}, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while deleting Document ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'Document not deleted' });
        } else {
            flag = true;
            callback(error, flag, { message: 'Document deleted successfully ' });
        }

    })
}

UserManagement.prototype.updateDocumentSharedWith = function(docId, shareDetails, callback) {
    mySharedDocs.update({ _id: docId, "sharedWith.emailId": shareDetails.emailId }, { $set: { "sharedWith.$.userId": shareDetails.userId, "sharedWith.$.firstName": shareDetails.firstName } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating Document sharedWith ' + error });
        }
        if (result == 0) {
            flag = false;
            callback(error, flag, { message: 'Updating Document sharedWith failed' });
        } else {
            flag = true;
            callback(error, flag, { message: 'Updating Document sharedWith success ' });
        }

    })
}

UserManagement.prototype.getConsolidateDocumentReport = function(callback) {
    mySharedDocs.aggregate([
        {
            $match:{
                sharedDate:{$gte:new Date("Jan 01 2019")}
            }
        },
        {
            $project: {
                sharedWith:1,
                sharedBy:1,
                documentName:1,
                sharedDate:1,
                openedDocuments: {
                    $filter: {
                        input: "$sharedWith",
                        as: "doc",
                        cond: {
                            $eq: ["$$doc.accessed", true]
                        }
                    }
                }
            }
        },
        {
            $sort: {
                sharedDate:-1
            }
        },
        {
            $group: {
                _id: {
                    companyId:"$sharedBy.companyId",
                    userId:"$sharedBy.userId"
                },
                totalUploadedDocs:{
                    $sum:1
                },
                totalDocsShared: {
                    $sum:{
                        $size:"$sharedWith"
                    }
                },
                totalDocsOpened: {
                    $sum:{
                        $size:"$openedDocuments"
                    }
                },
                lastUploadedDate: {
                    $first:"$sharedDate"
                },
                companyName: {
                    $first:"$sharedBy.companyName"
                }, 
                userEmailId: {
                    $first:"$sharedBy.emailId"
                },
                documentName: {
                    $first:"$documentName"
                },
                lastSharedDate: {
                    $push:"$sharedWith.sharedOn"
                },
                lastAccessedDate: {
                    $push:"$sharedWith.lastAccessed"
                }
            }
        }
    ],function(error, result){
        if(error){
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
}

UserManagement.prototype.getDetailDocumentReport = function(callback) {
    mySharedDocs.aggregate([
        {
            $match:{
                sharedDate:{$gte:new Date("Jan 01 2019")}
            }
        },
        {
            $unwind:"$sharedWith"
        },
        {
            $project: {
                companyName:"$sharedBy.companyName",
                companyId:"$sharedBy.companyId",
                userEmailId:"$sharedBy.emailId",
                documentName:"$documentName",
                category:"$documentCategory",
                sharedBy:"$sharedBy.emailId",
                sharedWith:"$sharedWith.emailId",
                sharedOnDate:"$sharedWith.sharedOn",
                linkAccessCount:"$sharedWith.linkAccessCount",
                firstAccess:"$sharedWith.firstAccessed",
                docAccessCount:"$sharedWith.accessCount", 
                totalReadTime: {
                    $reduce: {
                        input:"$sharedWith.accessInfo.pageReadTimings",
                        initialValue:0,
                        in: {
                            $add:["$$value", "$$this.Time"]
                        }
                    }
                }
            }
        }
    ],function(error, result){
        if(error){
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
}

//Function to add contact to user account
// *** not using **//
UserManagement.prototype.addContact = function(userId, contacts) {

    //contacts.forEach(function(contact){
    myUserCollection.findOne({ _id: userId, "contacts.personEmailId": contacts.personEmailId }, { "contacts.$": 1 }, function(error, contactInfo) {

            if (error) {
                logger.info('Search contact failed ' + error);
            } else {
                if (validateContactInfo(contactInfo, '')) {
                    myUserCollection.update({ _id: userId }, { $push: { contacts: contacts } }, function(error, result) {

                        if (result && result.ok) {

                            logger.info('Adding contacts success ');
                        } else {

                            logger.info('Adding contacts failed ' + error);
                        }
                    });
                }
            }
        })
        //})
};

//*** not using **//
UserManagement.prototype.addSingleContact = function(userId, contacts, callback) {

    //contacts.forEach(function(contact){
    myUserCollection.findOne({ _id: userId, "contacts.personEmailId": contacts.personEmailId }, { "contacts.$": 1 }, function(error, contactInfo) {

            if (error) {
                callback(error, false, { message: 'Search contact failed ' })
                logger.info('Search contact failed ' + error);
            } else {
                if (validateContactInfo(contactInfo, '')) {
                    myUserCollection.update({ _id: userId }, { $push: { contacts: contacts } }, function(error, result) {
                        if (error) {
                            callback(error, false, { message: 'Adding contact failed ' })
                            logger.info('Search contact failed ' + error);
                        }
                        if (result && result.ok) {
                            callback(contacts, true, { message: 'Adding contact success ' })

                        } else {
                            callback(error, false, { message: 'Adding contact failed ' })
                            logger.info('Adding contact failed ' + error);
                        }
                    });
                } else {
                    callback(error, 'exist', { message: 'Contact already exist' })
                    if (checkRequred(contacts.birthday) || checkRequred(contacts.companyName) || checkRequred(contacts.designation) || checkRequred(contacts.mobileNumber)) {

                        var exist = false;

                        var updateObj = { $set: {} }
                        if (checkRequred(contacts.birthday)) {
                            exist = true;
                            updateObj.$set["contacts.$.birthday"] = contacts.birthday
                        }

                        if (checkRequred(contacts.companyName)) {
                            exist = true;
                            updateObj.$set["contacts.$.companyName"] = contacts.companyName
                        }

                        if (checkRequred(contacts.designation)) {
                            exist = true;
                            updateObj.$set["contacts.$.designation"] = contacts.designation
                        }

                        if (checkRequred(contacts.mobileNumber)) {
                            exist = true;
                            updateObj.$set["contacts.$.mobileNumber"] = contacts.mobileNumber
                        }

                        if (checkRequred(contacts.source)) {
                            exist = true;
                            updateObj.$set["contacts.$.source"] = contacts.source
                        }

                        if (exist) {
                            myUserCollection.update({ _id: userId, "contacts.personEmailId": contacts.personEmailId }, updateObj, function(error, result) {
                                if (error) {
                                    logger.info('Update contact failed ' + error);
                                } else
                                if (result && result.ok) {

                                } else {
                                    logger.info('Update contact failed ');
                                }
                            });
                        }
                    }
                    //logger.info('Contact already exist');
                }
            }
        })
        //})
};

UserManagement.prototype.deleteContact = function(userId, contactId, callback) {
    myUserCollection.update({ _id: userId }, { $pull: { contacts: { _id: contactId } } }, function(error, result) {
        if (result && result.ok) {
            callback(error, true);
            logger.info('Deleting contact success ');
        } else {
            callback(error, false)
            logger.info('Deleting contact failed ');
        }
    })
}

function validateContactInfo(contactInfo, contact) {
    if (checkRequred(contactInfo)) {
        if (checkRequred(contactInfo.contacts[0])) {
            return false;
        } else return true;
    } else return true;
}

// Function to update contact
// *** not using *** //
UserManagement.prototype.updateContact = function(userId, contact) {

    if (!checkRequred(contact.personId)) {

        if (!checkRequred(contact.personEmailId)) {
            logger.info('Invalid contact')
        } else {
            updateContactUsingEmail(userId, contact)
        }
    } else {

        updateContacUsingUserId(userId, contact)
    }
};

function checkRequred(data) {
    if (data == '' || data == null || data == undefined) {
        return false;
    } else {
        return true;
    }
}

function updateContacUsingUserId(userId, contact) {
    myUserCollection.findOne({ _id: userId, "contacts.personId": contact.personId }, { "contacts.$": 1 }, function(error, contactInfo) {

        if (error) {
            logger.info('Search contact failed ' + error);
        } else {
            if (validateContactInfo(contactInfo, contact)) {
                updateContactUsingEmail(userId, contact)
            } else {

                myUserCollection.update({ _id: userId, "contacts.personId": contact.personId }, { $inc: { "contacts.$.count": 1 }, "contacts.$.lastInteracted": contact.lastInteracted, "contacts.$.personEmailId": contact.personEmailId, "contacts.$.relatasUser": contact.relatasUser, "contacts.$.relatasContact": contact.relatasContact }, function(error, result) {
                    if (error) {
                        logger.info('Update contact failed with id ' + error);
                    } else
                    if (result && result.ok) {

                    } else {
                        logger.info('Update contact failed ' + error);
                    }

                });
            }
        }
    });
}

function updateContactUsingEmail(userId, contact) {
    myUserCollection.findOne({ _id: userId, "contacts.personEmailId": contact.personEmailId }, { "contacts.$": 1 }, function(error, contactInfo) {

        if (error) {
            logger.info('Search contact failed ' + error);
        } else {
            if (validateContactInfo(contactInfo, contact)) {

                myUserCollection.update({ _id: userId }, { $push: { contacts: contact } }, function(error, result) {
                    if (error) {
                        logger.info('Update contact failed with email ' + error);
                    } else
                    if (result && result.ok) {

                    } else {
                        logger.info('Add contact failed with email ' + error);
                    }

                });
            } else {
                myUserCollection.update({ _id: userId, "contacts.personEmailId": contact.personEmailId }, { $inc: { "contacts.$.count": 1 }, "contacts.$.personId": contact.personId, "contacts.$.lastInteracted": contact.lastInteracted, "contacts.$.relatasUser": contact.relatasUser, "contacts.$.relatasContact": contact.relatasContact }, function(error, result) {
                    if (error) {
                        logger.info('Update contact failed ' + error);
                    } else
                    if (result && result.ok) {

                    } else {
                        logger.info('Update contact failed ');
                    }

                });
            }
        }
    });
}

// Function to get user total contacts
UserManagement.prototype.totalContacts = function(userId, callback) {
    myUserCollection.find({ _id: userId }, { contacts: 1 }, function(error, contacts) {
        if (error) {
            callback(error, contacts, { message: 'Searching contacts failed' });
        } else {

            callback(error, contacts, { message: 'Contacts found in user profile' });
        }
    });
};

// Function to get user total contacts
UserManagement.prototype.totalContactsFindOne = function(userId, callback) {
    myUserCollection.findOne({ _id: userId }, { contacts: 1 }, function(error, user) {
        if (error) {
            callback(error, user, { message: 'Searching contacts failed' });
        } else {
            callback(error, user, { message: 'Contacts found in user profile' });
        }
    });
};

UserManagement.prototype.getIdInContacts = function(userId, emailId, callback) {

    myUserCollection.aggregate([{ $match: { emailId: emailId } }, { $unwind: '$contacts' }, { $match: { "contacts.personId": { $nin: [userId, ''] } } }, { $group: { _id: "$emailId", "list": { $addToSet: "$contacts.personId" } } }]).exec(function(e, d) {
        if (e) {
            logger.info('mongo aggregation error in getting contacts ids ' + e);
            callback([])
        } else {
            if (d.length > 0) {
                if (d[0].list.length > 0) {
                    getUserListByIdThreeFields(d[0].list, callback)
                } else callback([]);
            } else callback([]);
        }
    });
};

UserManagement.prototype.getRelatasUserContacts = function(userId, callback) {

    myUserCollection.aggregate([{ $match: { _id: userId } }, { $unwind: '$contacts' }, {
        $match: {
            "contacts.personId": { $ne: '' }
        }
    }, {
        $match: {
            "contacts.personId": { $exists: true }
        }
    }, {
        $group: {
            _id: "$_id",
            contacts: { $push: '$contacts' },
            emails: { $addToSet: '$contacts.personEmailId' }
        }
    }]).exec(function(e, d) {

        if (e || !d) {
            logger.info('mongo aggregation error in getting relatas user contacts ' + e);
            callback([], [])
        } else {
            if (d.length > 0 && d[0] && d[0].contacts && d[0].contacts.length > 0) {
                myUserCollection.populate(d[0].contacts, { path: 'personId', select: 'firstName lastName emailId profilePicUrl companyName designation location publicProfileUrl skypeId mobileNumber' }, function(error, list) {
                    callback(list, d[0].emails)
                })
            } else callback([], []);
        }
    });
};

UserManagement.prototype.getNonRelatasContacts = function(userId, emails, callback) {

    if (emails.length > 0) {

        myUserCollection.aggregate([{ $match: { _id: userId } }, { $unwind: '$contacts' }, {
            $match: {
                "contacts.relatasContact": false
            }
        }, {
            $match: {
                "contacts.personEmailId": { $nin: emails }
            }
        }, {
            $group: {
                _id: "$_id",
                contacts: { $push: '$contacts' },
                emails: { $addToSet: '$contacts.personEmailId' }
            }
        }]).exec(function(e, d) {
            if (e || !d) {
                logger.info('mongo aggregation error in getting relatas user contacts ' + e);
                callback([], emails)
            } else {
                if (d.length > 0 && d[0] && d[0].contacts && d[0].contacts.length > 0) {
                    if (checkRequred(d[0].emails) && d[0].emails.length > 0) {
                        d[0].emails = d[0].emails.concat(emails)
                    }
                    callback(d[0].contacts, d[0].emails)
                } else {
                    callback([], emails);
                }
            }
        });
    } else {
        myUserCollection.aggregate([{ $match: { _id: userId } }, { $unwind: '$contacts' }, {
                $match: {
                    "contacts.relatasContact": false
                }
            },

            {
                $group: {
                    _id: "$_id",
                    contacts: { $push: '$contacts' },
                    emails: { $addToSet: '$contacts.personEmailId' }
                }
            }
        ]).exec(function(e, d) {
            if (e || !d) {
                logger.info('mongo aggregation error in getting relatas user contacts ' + e);
                callback([], emails)
            } else {
                if (d.length > 0 && d[0] && d[0].contacts && d[0].contacts.length > 0) {
                    if (checkRequred(d[0].emails) && d[0].emails.length > 0) {
                        d[0].emails = d[0].emails.concat(emails)
                    }
                    callback(d[0].contacts, d[0].emails)
                } else callback([], emails);
            }
        });
    }
};

UserManagement.prototype.getRelatasContactsNoUser = function(userId, emails, callback) {

    if (emails.length > 0) {

        myUserCollection.aggregate([{ $match: { _id: userId } }, { $unwind: '$contacts' }, {
            $match: {
                "contacts.relatasUser": { $ne: true }
            }
        }, {
            $match: {
                "contacts.personEmailId": { $nin: emails }
            }
        }, {
            $group: {
                _id: "$_id",
                contacts: { $push: '$contacts' },
                emails: { $addToSet: '$contacts.personEmailId' }
            }
        }]).exec(function(e, d) {

            if (e || !d) {
                logger.info('mongo aggregation error in getting relatas user contacts ' + e);
                callback([], emails)
            } else {
                if (d.length > 0 && d[0] && d[0].contacts && d[0].contacts.length > 0) {
                    if (checkRequred(d[0].emails) && d[0].emails.length > 0) {
                        d[0].emails = d[0].emails.concat(emails)
                    }
                    callback(d[0].contacts, d[0].emails)
                } else callback([], emails);
            }
        });
    } else {
        myUserCollection.aggregate([{ $match: { _id: userId } }, { $unwind: '$contacts' }, {
            $match: {
                "contacts.relatasUser": { $ne: true }
            }
        }, {
            $match: {
                "contacts.relatasContact": { $ne: false }
            }
        }, {
            $group: {
                _id: "$_id",
                contacts: { $push: '$contacts' },
                emails: { $addToSet: '$contacts.personEmailId' }
            }
        }]).exec(function(e, d) {

            if (e || !d) {
                logger.info('mongo aggregation error in getting relatas user contacts ' + e);
                callback([], emails)
            } else {
                if (d.length > 0 && d[0] && d[0].contacts && d[0].contacts.length > 0) {
                    if (checkRequred(d[0].emails) && d[0].emails.length > 0) {
                        d[0].emails = d[0].emails.concat(emails)
                    }
                    callback(d[0].contacts, d[0].emails)
                } else callback([], emails);
            }
        });
    }
};

// Function to get user total contacts
UserManagement.prototype.totalContactsCount = function(userId, callback) {
    myUserCollection.findOne({ _id: userId }, { contacts: 1 }, function(error, contacts) {
        var count = 0;
        if (checkRequred(contacts) && checkRequred(contacts.contacts)) {
            count = contacts.contacts.length;
        }

        callback(error, count, { message: "count" })
    });
};

// Function to get user total contacts
UserManagement.prototype.getSingleContactByUserId = function(userId, otherUserId, callback) {
    myUserCollection.findOne({ _id: userId, "contacts.personId": otherUserId }, { "contacts.$": 1 }, function(error, contact) {
        callback(contact);
    });
};

UserManagement.prototype.getUserIdListAddedTwitter = function(callback) {
    myUserCollection.aggregate([{
        $match: {
            twitter: {
                $exists: true
            },
            "twitter.id": {
                $exists: true,
                $nin: ["", " ", null]
            }
        }
    }, {
        $group: {
            _id: null,
            idList: { $addToSet: "$_id" }
        }
    }]).exec(function(error, result) {
        if (error) {
            loggerError.info('Error in getUserIdListAddedTwitter():UserManagement ', error);
            callback([])
        } else if (result && result.length > 0 && result[0] && result[0].idList && result[0].idList.length > 0) {
            callback(result[0].idList);
        } else callback([])
    })
};

UserManagement.prototype.getUserIdListAddedLinkedin = function(callback) {
    myUserCollection.aggregate([{
        $match: {
            linkedin: {
                $exists: true
            },
            "linkedin.id": {
                $exists: true,
                $nin: ["", " ", null]
            }
        }
    }, {
        $group: {
            _id: null,
            idList: { $addToSet: "$_id" }
        }
    }]).exec(function(error, result) {
        if (error) {
            loggerError.info('Error in getUserIdListAddedLinkedin():UserManagement ', error);
            callback([])
        } else if (result && result.length > 0 && result[0] && result[0].idList && result[0].idList.length > 0) {
            callback(result[0].idList);
        } else callback([])
    })
};

// ****** UPDATE FUNCTIONS ******

// Functions to update user profile individually
UserManagement.prototype.updateFirstName = function(id, value, callback) {

    myUserCollection.update({ _id: id }, { $set: { firstName: value } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating firstName ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'firstName successfully updated' });

        } else {
            flag = false;
            callback(error, flag, { message: 'firstName update failed' });
        }
    });
}

UserManagement.prototype.updateLastName = function(id, value, callback) {

    myUserCollection.update({ _id: id }, { $set: { lastName: value } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating lastName ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'lastName successfully updated' });

        } else {
            flag = false;
            callback(error, flag, { message: 'lastName update failed' });
        }

    });
};

UserManagement.prototype.updateEmailId = function(id, email, callback) {

    myUserCollection.update({ _id: id }, { $set: { emailId: email } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating emailId ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'emailId successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'emailId update failed' });

        }
    });
};

UserManagement.prototype.updateWorkHours = function(userId, obj, callback) {

    myUserCollection.update({ _id: userId }, { $set: { workHours: obj } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating work hours ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'work hours successfully updated' });

        } else {
            flag = false;
            callback(error, flag, { message: 'work hours update failed' });
        }
    });
}

UserManagement.prototype.updateDesignation = function(id, value, callback) {

    myUserCollection.update({ _id: id }, { $set: { designation: value } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating designation ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'designation successfully updated' });

        } else {
            flag = false;
            callback(error, flag, { message: 'designation update failed' });
        }
    });
};

UserManagement.prototype.updateCompanyName = function(id, value, callback) {

    myUserCollection.update({ _id: id }, { $set: { companyName: value } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating companyName ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'companyName successfully updated' });

        } else {
            flag = false;
            callback(error, flag, { message: 'companyName update failed' });
        }
    });
};

UserManagement.prototype.updateCompanyNameForAllUsers = function(companyId, value, callback) {

    myUserCollection.update({ companyId: companyId }, { $set: { companyName: value } },{multi:true}, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating companyName ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'companyName successfully updated' });

        } else {
            flag = false;
            callback(error, flag, { message: 'companyName update failed' });
        }
    });
};

UserManagement.prototype.updateMobileNumber = function(id, countryCode,phoneNumber, callback) {

    var mobileNumber = countryCode+phoneNumber

    myUserCollection.update({ _id: id }, { $set: { mobileNumber: mobileNumber,mobileNumberWithoutCC : phoneNumber } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating mobileNumber ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'mobileNumber successfully updated' });

        } else {
            flag = false;
            callback(error, flag, { message: 'mobileNumber update failed' });
        }
    });
};

UserManagement.prototype.verifyMobileNumberWithOTP = function(id, OTPCode, toVerify, callBack) {
    myUserCollection.update({ _id: id }, { $set: { isMobileNumberVerified: toVerify, mobileNumberOTP: OTPCode } }, function(err, res) {
        callBack(err, res)
    })
}

UserManagement.prototype.updateMobileSyncDate = function(id, currentDate, callBack) {
    myUserCollection.update({ _id: id }, { $set: { lastMobileSyncDate: new Date(currentDate) } }, function(err, res) {
        callBack(err, res)
    })
}

UserManagement.prototype.updateMobileLastLoginDate = function(id, currentAppVersion, callBack) {
    myUserCollection.update({ _id: id }, { $set: { mobileLastLoginDate: new Date(), mobileAppVersion: currentAppVersion } }, function(err, res) {
        callBack(err, res)
    })
}

UserManagement.prototype.updateMobileOnBoardDate = function(id, onBoardDate, callBack) {
    myUserCollection.update({ _id: id }, { $set: { mobileOnBoardingDate: new Date(onBoardDate) } }, function(err, res) {
        callBack(err, res)
    })
}

UserManagement.prototype.updatePartialProfileDate = function(id,createdDate,callBack){
    myUserCollection.update({_id:id},{partialFill:new Date(createdDate)},function(err,res){
        callBack(err,res)
    })
}

UserManagement.prototype.updateRegisteredDateUser = function(id,registeredDate,callBack){
    myUserCollection.update({_id:id},{registeredDate:new Date(registeredDate)},function(err,res){
        callBack(err,res)
    })
}

UserManagement.prototype.updateMobileAppVersionName = function(id,versionName,callBack){
    myUserCollection.update({_id:id},{$set:{mobileAppVersion:versionName}},function(err,res){
        callBack(err,res)
    })
}

UserManagement.prototype.updateMobileAppVersionName = function(id,versionName,callBack){
    myUserCollection.update({_id:id},{$set:{mobileAppVersion:versionName}},function(err,res){
        callBack(err,res)
    })
}

UserManagement.prototype.updateSkypeId = function(id, value, callback) {

    myUserCollection.update({ _id: id }, { $set: { skypeId: value } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating skypeId ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'skypeId successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'skypeId update failed' });
        }
    });
};

UserManagement.prototype.updateRelatasIdentity = function(id, identity, callback) {

    myUserCollection.update({ _id: id }, { $set: { publicProfileUrl: identity } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating relatasIdentity ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'relatasIdentity successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'relatasIdentity update failed' });
        }
    });
};

UserManagement.prototype.updateRegistereStatus = function(id, callback) {

    myUserCollection.update({ _id: id }, { $set: { registeredUser: true } }, function(error, result) {

        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating updateRegistereStatus ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'updateRegistereStatus successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'updateRegistereStatus update failed' });
        }
    });
};

UserManagement.prototype.updateLocation = function(id, value, callback) {

    myUserCollection.update({ _id: id }, { $set: { location: value } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating location ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'location successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'location update failed' });
        }
    });
};

UserManagement.prototype.updateBirthday = function(id, days, months, years, callback) {
    myUserCollection.update({ _id: id }, { $set: { birthday: { day: days, month: months, year: years } } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating birthday ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'birthday successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'birthday update failed' });
        }
    });
};

UserManagement.prototype.updateProfilePrivatePassword = function(userId, profilePrivatePassword, callback) {

    myUserCollection.update({ _id: userId }, { $set: { profilePrivatePassword: profilePrivatePassword } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating profilePrivatePassword ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'profilePrivatePassword successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'profilePrivatePassword update failed' });
        }
    });
};

UserManagement.prototype.updateProfilePassword = function(userId, password, callback) {
    var encpass = EncryptionValue(password);
    myUserCollection.update({ _id: userId }, { $set: { password: encpass } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating profile password ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'Profile password successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'Profile password update failed' });
        }

    });
};

UserManagement.prototype.updateSendDailyAgenda = function(userId, flag, callback) {

    myUserCollection.update({ _id: userId }, { $set: { sendDailyAgenda: flag } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating daily agenda property ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'Daily agenda property successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'Daily agenda property update failed' });
        }
    });
};

UserManagement.prototype.updateProfileDescription = function(userId, profileDescription, callback) {
    myUserCollection.update({ _id: userId }, { $set: { profileDescription: profileDescription } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating profileDescription ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'profileDescription successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'profileDescription update failed' });
        }
    });
};

UserManagement.prototype.updateProfileImage = function(userId, profilePicUrl, callback) {
    myUserCollection.update({ _id: userId }, { $set: { profilePicUrl: profilePicUrl } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating profile picture ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'profile picture successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'profile picture update failed' });
        }
    })
};

UserManagement.prototype.updateCalendarAccess = function(userId, calendarAccessInfo, callback) {
    myUserCollection.update({ _id: userId }, { $set: { calendarAccess: calendarAccessInfo } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating calendarAccess ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'calendarAccess successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'calendarAccess update failed' });
        }
    });
};

UserManagement.prototype.updateLinkedin = function(id, lId, lToken, lEmailId, lName, callback) {
    var d = (new Date());
    d.setDate(d.getDate() - 90);
    myUserCollection.update({ _id: id }, { $set: { linkedin: { id: lId, token: lToken, emailId: lEmailId, name: lName, lastSync: d.toISOString() } } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating linkedin ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'linkedin successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'linkedin update failed' });
        }
    });
};

UserManagement.prototype.updateFacebook = function(id, fId, fToken, fEmailId, fName, callback) {
    var d = (new Date());
    d.setDate(d.getDate() - 90);
    myUserCollection.update({ _id: id }, { $set: { facebook: { id: fId, token: fToken, emailId: fEmailId, name: fName, lastSync: d.toISOString() } } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating facebook ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'facebook successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'facebook update failed' });
        }
    });
};

UserManagement.prototype.updateTwitter = function(id, tId, tToken, tRefreshToken, tUserName, tDisplayName, callback) {
    myUserCollection.update({ _id: id }, { $set: { twitter: { id: tId, token: tToken, refreshToken: tRefreshToken, userName: tUserName, displayName: tDisplayName } } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating twitter ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'twitter successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'twitter update failed' });
        }
    });
};

UserManagement.prototype.updateGoogle = function(id, googleData, callback) {
    myUserCollection.findOne({ _id: id }, function(error, profile) {

        if (profile == null && profile == undefined && profile == '') {
            callback(id, false, { message: 'updating google failed ' + error });
        } else {
            var exist = false;
            if (profile.google.length > 0) {
                for (var i = 0; i < profile.google.length; i++) {
                    if (profile.google[i].id == googleData[0].id) {
                        exist = true;
                    }
                }
            }

            if (exist) {
                updateExistingGoogleAccount(id, googleData[0], function(error, success, msg) {
                    callback(error, success, msg);
                })
            } else {
                addNewGoogleAcc(id, googleData[0], function(error, success, msg) {
                    callback(error, success, msg);
                })
            }
        }
    });
}

function updateExistingGoogleAccount(id, googleData, callback) {

    myUserCollection.update({ _id: id, "google.id": googleData.id }, { $set: { "google.$.id": googleData.id, "google.$.token": googleData.token, "google.$.refreshToken": googleData.refreshToken, "google.$.emailId": googleData.emailId, "google.$.name": googleData.name } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, flag, { message: 'Error in DB while updating existing google acc ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'updating existing google acc successful' });
        } else {
            flag = false;
            callback(error, flag, { message: 'updating existing google acc failed' });
        }
    });
}

function addNewGoogleAcc(id, googleData, callback) {
    myUserCollection.update({ _id: id }, { $push: { google: googleData } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, flag, { message: 'Error in DB while updating new google acc ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'updating new google acc successful' });
        } else {
            flag = false;
            callback(error, flag, { message: 'updating new google acc failed' });
        }
    });
};

UserManagement.prototype.updateGoogleAccountToPrimary = function(userId, _id, callback) {
    myUserCollection.findOne({ _id: userId }, { emailId: 1, google: 1 }, function(error, user) {
        if (error) {
            logger.info("Error in updateGoogleAccountToPrimary():UserManagement 1 ", error);
            callback(error, false);
        } else if (user && user._id) {
            var accounts = JSON.parse(JSON.stringify(user.google));
            var makePrimaryAcc = null;
            for (var i = 0; i < accounts.length; i++) {
                if (accounts[i]._id.toString() == _id) {
                    makePrimaryAcc = accounts.splice(i, 1)[0];
                }
            }
            if (makePrimaryAcc != null) {
                var newArrange = [makePrimaryAcc];
                accounts.forEach(function(acc) {
                    newArrange.push(acc)
                });

                myUserCollection.update({ _id: userId }, { $set: { google: newArrange } }, function(error, result) {
                    if (error) {
                        logger.info("Error in updateGoogleAccountToPrimary():UserManagement 2 ", error);
                        callback(error, false);
                    } else if (result && result.ok) {
                        callback(error, true);
                    } else callback(error, false);
                })
            } else callback(error, false);
        } else callback(error, false);
    })
};

UserManagement.prototype.updateOutlookAccountToPrimary = function(userId, _id, callback) {
    myUserCollection.findOne({ _id: userId }, { emailId: 1, outlook: 1 }, function(error, user) {
        if (error) {
            logger.info("Error in updateOutlookAccountToPrimary():UserManagement 1 ", error);
            callback(error, false);
        } else if (user && user._id) {
            var accounts = JSON.parse(JSON.stringify(user.outlook));
            var makePrimaryAcc = null;
            for (var i = 0; i < accounts.length; i++) {
                if (accounts[i]._id.toString() == _id) {
                    accounts[i].isPrimary = true;
                    makePrimaryAcc = accounts.splice(i, 1)[0];
                } else {
                    accounts[i].isPrimary = false;
                }
            }
            if (makePrimaryAcc != null) {
                var newArrange = [makePrimaryAcc];
                accounts.forEach(function(acc) {
                    newArrange.push(acc)
                });

                myUserCollection.update({ _id: userId }, { $set: { outlook: newArrange } }, function(error, result) {
                    if (error) {
                        logger.info("Error in updateOutlookAccountToPrimary():UserManagement 2 ", error);
                        callback(error, false);
                    } else if (result && result.ok) {
                        callback(error, true);
                    } else callback(error, false);
                })
            } else callback(error, false);
        } else callback(error, false);
    })
};

UserManagement.prototype.updateGooglePrimaryAcc = function(userId, googleData, callback) {
    addNewGoogleAccAsPrimary(userId, googleData, function(error, result) {
        callback(error, result)
    })
};

function addNewGoogleAccAsPrimary(id, googleData, callback) {
    myUserCollection.findOne({ _id: id }, { emailId: 1, google: 1 }, function(error, user) {
        if (!error && checkRequred(user)) {
            if (checkRequred(user.google) && user.google.length > 0) {
                var exist = false;

                for (var i = 0; i < user.google.length; i++) {
                    if (user.google[i].id == googleData.id) {
                        exist = true;
                    }
                }

                if (exist) {
                    myUserCollection.update({ _id: id, "google.id": googleData.id }, { $set: { "google.$.id": googleData.id, "google.$.token": googleData.token, "google.$.refreshToken": googleData.refreshToken, "google.$.emailId": googleData.emailId, "google.$.name": googleData.name } }, function(error, result) {
                        if (result && result.ok) {
                            callback(error, true)
                        } else callback(error, false)
                    })
                } else {

                    var googleAccounts = user.google;
                    var newArr = [googleData];
                    for (var j = 0; j < googleAccounts.length; j++) {
                        if (newArr.length != 4) {
                            newArr.push(googleAccounts[j]);
                        }
                    }

                    myUserCollection.update({ _id: id }, { $set: { google: newArr } }, function(err, result) {
                        if (result && result.ok) {
                            callback(err, true)
                        } else callback(err, false)
                    })
                }
            } else {
                myUserCollection.update({ _id: id }, { $push: { google: googleData } }, function(error, result) {
                    if (result && result.ok) {
                        callback(error, true)
                    } else callback(error, false)
                })
            }
        } else callback(error, false)
    });
}

UserManagement.prototype.removeGoogleAccountBy_Id = function(userId, googleId, callback) {
    myUserCollection.update({ _id: userId }, { $pull: { google: { _id: googleId } } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, flag, { message: 'Error in DB while removing google ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'Removing google successful' });
        } else {
            flag = false;
            callback(error, flag, { message: 'removing google failed' });
        }
    });
};

UserManagement.prototype.removeOutlookAccountBy_Id = function(userId, outlookId, callback) {
    myUserCollection.update({ _id: userId }, { $pull: { outlook: { _id: outlookId } } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, flag, { message: 'Error in DB while removing Outlook acccount ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'Removing Outlook account successful' });
        } else {
            flag = false;
            callback(error, flag, { message: 'removing Outlook account failed' });
        }
    });
};

/*Delete a hashtag from a contact*/

UserManagement.prototype.deleteHashtagMgmt = function(userId, contactId, hashtag, callback) {

    myUserCollection.update({ _id: userId, "contacts._id": contactId }, { "$pull": { "contacts.$.hashtag": hashtag } }, function(error, result) {
        if (error) {
            callback(error, false);
        } else if (result) {
            callback(error, true);
        }
    });
};

UserManagement.prototype.removeGoogleAccountById = function(userId, googleId, callback) {
    myUserCollection.update({ _id: userId }, { $pull: { google: { id: googleId } } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, flag, { message: 'Error in DB while removing google ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'Removing google successful' });
        } else {
            flag = false;
            callback(error, flag, { message: 'removing google failed' });
        }
    });
};

// Function to update user current location using userId
UserManagement.prototype.updateCurrentLocation = function(userId, currentLocation, callback) {
    if (checkRequred(currentLocation) && checkRequred(currentLocation.city)) {
        var updateObj = {
            currentLocation: currentLocation,
            location: currentLocation.city
        };
        if (checkRequred(currentLocation.country)) {
            updateObj.location = updateObj.location + ' ' + currentLocation.country;
        }

        myUserCollection.update({ _id: userId }, { $set: updateObj }, function(error, result) {
            if (result && result.ok) {
                callback(true)
            } else callback(false)
        });
    } else callback(false);
};

UserManagement.prototype.updateTokenResetFlag = function(id, callback) {

    myUserCollection.update({ _id: id }, { $set: { tokenReset: true } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating tokenReset ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'tokenReset successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'tokenReset update failed' });
        }
    });
};

UserManagement.prototype.updateFirstLoginFlag = function(id, callback) {
    myUserCollection.update({ _id: id }, { $set: { firstLogin: true } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating firstLogin ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'firstLogin successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'firstLogin update failed' });
        }
    });
};

UserManagement.prototype.updateLastLogin = function(id, date, callback) {

    myUserCollection.update({ _id: id }, { $set: { lastLoginDate: date } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating lastLoginDate ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'lastLoginDate successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'lastLoginDate update failed' });
        }
    });
};

UserManagement.prototype.getLastLogin = function(id, callback) {

    myUserCollection.find({ _id: id }, { lastLoginDate:1 }, function(error, result) {

        if(!error && result){
            callback(error,result)
        } else {
            callback(error,false)
        }
    });
};

UserManagement.prototype.updatePenultimateLogin = function(id, date, callback) {

    myUserCollection.update({ _id: id }, { $set: { penultimateLoginDate: date } }, {upsert:true}, function(error, result) {
        
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating penultimateLoginDate ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'penultimateLoginDate successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'penultimateLoginDate update failed' });
        }
    });
};


UserManagement.prototype.updateLastLogout = function(id, date, callback) {

    myUserCollection.update({ _id: id }, { $set: { lastLogoutDate: date } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating lastLoginDate ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'lastLogoutDate successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'lastLogoutDate update failed' });
        }
    });
};

UserManagement.prototype.updateLastAgendaSentDate = function(id, date) {
    myUserCollection.update({ _id: id }, { $set: { lastAgendaSentDate: date } }, function(error, result) {

    });
};

UserManagement.prototype.updateDashboardPopup = function(id, callback) {

    myUserCollection.update({ _id: id }, { $set: { dashboardPopup: true } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating dashboardPopupFlag ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'dashboardPopupFlag successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'dashboardPopupFlag update failed' });
        }
    });
};

function getZoneNumber(zone) {
    if (zone) {
        var zoneStr = zone.toString();
        var num = parseInt(zoneStr.substring(0, 3));
        if (zoneStr.charAt(0) == '+') {

            if (num >= 6 && num <= 11) {
                return 1;
            } else if (num >= 0 && num <= 5) {
                return 2;
            } else return 0;
        } else if (zoneStr.charAt(0) == '-') {
            if (num >= -6 && num <= 0) {
                return 3;
            } else if (num >= -11 && num <= -6) {
                return 4;
            } else return 0;
        } else return 0;
    } else return 0;
}

UserManagement.prototype.updateMobilePreferences = function(id, updateObj, callback) {
    myUserCollection.update({ _id: id }, {
        $set: { openActionItemAfterCall: updateObj.isOpenActionItem, "notification.notificationOn": updateObj.agenda, sendDailyAgenda: updateObj.agendaMailEnabled,location:updateObj.location }
    }, function(err, res) {
        callback(err, res)
    })
}

UserManagement.prototype.updateTimezone = function(id, zoneObj, callback) {
    var zoneNum = getZoneNumber(zoneObj.zone);

    myUserCollection.update({ _id: id }, { $set: { timezone: zoneObj, zoneNumber: zoneNum } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating time zone ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'time zone successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'time zone update failed' });
        }
    });
};

UserManagement.prototype.updateLocationLatLang = function(id, latLangObj, callback) {
    myUserCollection.update({ _id: id }, { $set: { locationLatLang: latLangObj } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating location lat lang ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'location lat lang successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'location lat lang update failed' });
        }
    });
};

UserManagement.prototype.updateWidgetKey = function(userId, updateObj, callback) {

    myUserCollection.update({ _id: userId }, { $set: { widgetKey: updateObj } }, function(error, result) {
        var flag;
        if (error) {
            loggerError.info('Mongo error while updating widget key. ' + JSON.stringify());
            callback(false);
        } else if (result && result.ok) {
            callback(true)
        } else callback(false);
    });
};

UserManagement.prototype.updateUserToCorporate = function(userId, companyId, callback) {
    myUserCollection.update({ _id: userId }, { $set: { corporateUser: true, companyId: companyId } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updating user to corporate ' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'user to corporate successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'user to corporate update failed' });
        }
    })
};

UserManagement.prototype.updateProfileObject = function(userData, callback) {
    myUserCollection.update({ _id: userData.userId }, { $set: userData }, function(error, result) {
        if (result && result.ok) {
            callback(error, 1)
        } else callback(error, 0);
    });
};

UserManagement.prototype.updateProfileObject_new = function(userId, userData, callback) {
    myUserCollection.update({ _id: userId }, { $set: userData }, function(error, result) {
        if (result && result.ok) {
            callback(error, 1)
        } else callback(error, 0);
    });
};

UserManagement.prototype.updateIdShare = function(userId, callback) {
    myUserCollection.update({ _id: userId }, { $set: { identityShare: true } }, function(error, result) {
        callback(error, result);
    });
};

UserManagement.prototype.updateOfficeCalender = function(userId, calObj) {
    myUserCollection.update({ _id: userId }, { $set: { officeCalendar: calObj } }, function(error, result) {
        if (error) {
            loggerError.info('Office calendar is not updated ' + error);
        } else if (result && result.ok) {
            logger.info('Updating office calendar success ');
        } else
            loggerError.info('Office calendar is not updated ' + error);
    })
}

/********************* Functions on count *********************/

UserManagement.prototype.getSentInvitationsCount = function(userId, callback) {
    scheduleInvitation.find({ senderId: userId, deleted: { $ne: true } }, function(error, meetings) {
        if (checkRequred(meetings) && meetings.length > 0) {
            var meetingsCount = meetings.length;
            callback("" + meetingsCount);
        } else callback('0')
    })
};

//Function to get total meetings of one user(Total meetings rec related to user)
UserManagement.prototype.getTotalMeetingsRecCount = function(userId, callback) {
    scheduleInvitation.find({ senderId: userId, deleted: { $ne: true } }, function(error, meetings) {
        var meetingCount = 0;
        meetingCount += meetings.length;
        //var count = invitations.length;
        scheduleInvitation.find({ "to.canceled": { $ne: true }, "to.receiverId": userId, deleted: { $ne: true } }, function(error, invitationsMe) {
            meetingCount += invitationsMe.length;
            callback("" + meetingCount)
        })
    })
};

UserManagement.prototype.getDocumentsUploadedCount = function(userId, callback) {
    mySharedDocs.find({ "sharedBy.userId": userId }, function(error, documents) {
        if (checkRequred(documents) && documents.length > 0) {
            var documentsCount = documents.length;
            callback("" + documentsCount);
        } else callback('0');
    })
};

UserManagement.prototype.getDocumentsSharedWithMeCount = function(userId, callback) {
    mySharedDocs.find({ sharedWith: { $elemMatch: { userId: userId, accessStatus: true } } }, function(error, documents) {
        var documentsCount = documents.length;
        callback("" + documentsCount);
    })
};

UserManagement.prototype.getDocumentsRecCount = function(userId, callback) {

    var q = {
        $or: [
            { "sharedBy.userId": userId },
            { sharedWith: { $elemMatch: { userId: userId, accessStatus: true } } }
        ]
    }
    mySharedDocs.aggregate([{
        $match: q
    }, {
        $group: {
            _id: null,
            count: {
                $sum: 1
            }
        }
    }]).exec(function(e, d) {

        if (e) {
            logger.info('mongo aggregate error total related docs count ' + e);
            callback(0)
        } else {
            if (checkRequred(d) && d.length > 0) {
                callback(d[0].count || 0);
            } else callback(0)
        }
    });
};

UserManagement.prototype.populateCustomQuery = function(array, path, callback) {
    myUserCollection.populate(array, path, function(err, list) {
        if (err) {
            callback(array, false)
        } else callback(list, true)
    })
};

UserManagement.prototype.customQueryUpdate = function(query, update, callback) {
    myUserCollection.update(query, update, callback);
};

UserManagement.prototype.updateTwitterSinceId = function(userId, sinceId, callback) {
    myUserCollection.update({ _id: userId }, { $set: { "twitter.sinceId": sinceId, "twitter.lastSync": new Date() } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updateTwitterSinceId' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'updateTwitterSinceId successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'updateTwitterSinceId update failed' });
        }
    })
};

UserManagement.prototype.updateLinkedinLastSync = function(userId, callback) {
    myUserCollection.update({ _id: userId }, { $set: { "linkedin.lastSync": new Date() } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updateLinkedinLastSync' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'updateLinkedinLastSync successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'updateLinkedinLastSync update failed' });
        }
    })
};

UserManagement.prototype.updateLinkedinLastSyncWithTimestamp = function(userId, timestamp, callback) {
    myUserCollection.update({ _id: userId }, { $set: { "linkedin.lastSync": new Date(timestamp) } }, function(error, result) {
        var flag;
        if (error) {
            callback(error, false, { message: 'Error in DB while updateLinkedinLastSync' + error });
        } else if (result && result.ok) {
            flag = true;
            callback(error, flag, { message: 'updateLinkedinLastSync successfully updated' });
        } else {
            flag = false;
            callback(error, flag, { message: 'updateLinkedinLastSync update failed' });
        }
    })
};

UserManagement.prototype.getUserHierarchy = function(userId, callBack) {
    
    myUserCollection.findOne({ _id: userId }, { _id: 1, emailId: 1, designation: 1, firstName: 1, lastName: 1,companyId:1,hierarchyParent:1,hierarchyPath:1 }).lean().exec(function(err, user) {

        if(!err && user){
            myUserCollection.find({ corporateUser: true,companyId:user.companyId, hierarchyPath: { $regex: userId.toString(), $options: 'i' },
                resource: { $ne: true } },
                { _id: 1, emailId: 1, designation: 1, hierarchyParent: 1, hierarchyPath: 1, firstName: 1, lastName: 1,companyId:1,regionOwner: 1,productTypeOwner: 1,verticalOwner: 1 }).lean().exec(function(err, users) {

                user["hierarchyParent"] = null
                users.push(user)
                callBack(null, users)
            })
        } else {
            callBack(err,null)
        }
    })
};

UserManagement.prototype.getUserHierarchyWithoutLiu = function(userId, callBack) {

    myUserCollection.find({ corporateUser: true, hierarchyPath: { $regex: userId.toString(), $options: 'i' }, resource: { $ne: true } }, { _id: 1 }).exec(function(err, users) {
        if (err) {
            callBack(err, null)
        } else {
            callBack(null, users)
        }
    })
};

UserManagement.prototype.getUserOppOwnerDetails = function(userIds, callBack) {

    myUserCollection.find({ _id:{$in:userIds} }, { regionOwner: 1,productTypeOwner: 1,businessUnits: 1,verticalOwner: 1,companyId:1,companyTargetAccess:1 }).exec(function(err, result) {
        if (err) {
            callBack(err, null)
        } else {
            callBack(null, result)
        }
    })
};

UserManagement.prototype.getUserHierarchyWithoutLiuWithNames = function(userId, callBack) {

    myUserCollection.find({ corporateUser: true, hierarchyPath: { $regex: userId.toString(), $options: 'i' }, resource: { $ne: true } }, { _id: 1,firstName:1,lastName:1,emailId:1 }).exec(function(err, users) {
        if (err) {
            callBack(err, null)
        } else {
            callBack(null, users)
        }
    })
};

//added Prototype to update isMobileUser field
UserManagement.prototype.updateFirstUseOfMobile = function(emailId, callback) {
    var email = emailId.trim()
    myUserCollection.update({ emailId: email }, { $set: { isMobileUser: new Date() } }, function(err, res) {
        callback(err, res)
    })
}

UserManagement.prototype.getUserGoogleRefreshToken = function(userId, callback) {
    myUserCollection.findOne({ _id: userId }, { google: 1, emailId: 1 }, function(err, token) {
        if (!err) {
            callback(token)
        } else {
            callback(err, null)
        }
    });
};

UserManagement.prototype.getUserProfileWithoutContacts = function(userId, callback) {
    myUserCollection.findOne({ _id: userId }, { contacts: 0 }, function(err, profile) {
        if (!err) {
            callback(err, profile)
        } else {
            callback(err, null)
        }
    });
};

UserManagement.prototype.createPartialProfile = function (profile,token,outlookOrGoogleUser,callback) {
    if(outlookOrGoogleUser == 'googleUser'){

    } else if(outlookOrGoogleUser == 'outlookUser'){

        var fullName = profile.givenName.toLowerCase() + profile.surname.toLowerCase();
        var basicProfile = getBasicProfileFields();
        basicProfile.publicProfileUrl = fullName.replace(/[^a-zA-Z0-9]/g,'');
        basicProfile.serviceLogin = 'outlook';
        basicProfile.firstName = profile.givenName;
        basicProfile.lastName = profile.surname;
        basicProfile.emailId = profile.userPrincipalName.toLowerCase();
        basicProfile.createdDate = new Date();
        basicProfile.outlook = [{
            id: profile.id,
            token: token.access_token,
            refreshToken: token.refresh_token,
            emailId: profile.userPrincipalName.toLowerCase(),
            name: profile.displayName,
            addedOn: new Date()
        }];

        var self = this;

        //Check for unique public profile URL
        self.getUniquePublicProfileUrl(basicProfile.publicProfileUrl,function (uniquePublicProfileUrl) {
            basicProfile.publicProfileUrl = uniquePublicProfileUrl;
            self.addUserPartialProfile(basicProfile,callback)
        });
    }
};

UserManagement.prototype.updateUserCompany = function(userId,companyProfile,callback){
    myUserCollection.update({ _id: userId }, { $set: { corporateUser: true, companyId: companyProfile.companyId,companyName:companyProfile.companyName } }, function(error, result) {
        if (error) {
            callback(error, false);
        } else if (result && result.ok) {
            callback(error, true);
        } else {
            callback(error, false);
        }
    })
}

UserManagement.prototype.getUniquePublicProfileUrl = function (publicProfileUrl,callback) {
    var counter = 0;
    var self = this;
    getUserByUniqueName(publicProfileUrl,function (err,userProfile) {
        if(!err && userProfile){
            counter++;
            self.getUniquePublicProfileUrl(publicProfileUrl+counter,callback);
        } else{
            callback(publicProfileUrl)
        }
    });
}

UserManagement.prototype.updateUserSalesforceInfo = function (userId,updateFields,callback) {
    myUserCollection.update({ _id: userId }, { $set:{"salesforce.id":updateFields.id, "salesforce.instanceUrl":updateFields.instanceUrl,"salesforce.refreshToken":updateFields.refreshToken,"salesforce.userInfoUrl":updateFields.userInfoUrl,"salesforce.emailId":updateFields.emailId} }, {upsert:true}, function(err, res) {
        if (!err) {
            callback(err, res)
        } else {
            callback(err, null)
        }
    });
}

UserManagement.prototype.updateUserSalesforceLastSync = function (userId,callback) {
    myUserCollection.update({ _id: userId }, { $set:{"salesforce.lastSync":new Date()} }, function(err, res) {
        if (!err) {
            callback(err, res)
        } else {
            callback(err, null)
        }
    });
};

UserManagement.prototype.findContactsByEmailIdArray = function(userId, emailIdArr, callback) {

    var project = {
        _id:0,
        personName:"$contacts.personName",
        personEmailId:"$contacts.personEmailId",
        personId:"$contacts.personId",
        contactImageLink:"$contacts.contactImageLink",
        designation:"$contacts.designation",
        mobileNumber:"$contacts.mobileNumber",
        companyName:"$contacts.companyName",
        contactRelation:"$contacts.contactRelation.decisionmaker_influencer",
        contactId:"$contacts._id",
        twitterUserName:"$contacts.twitterUserName",
        location:"$contacts.location",
        favorite:"$contacts.favorite"
    }

    myUserCollection.aggregate([
        {
            $match:{_id:userId}},
        {
            $unwind:"$contacts"},
        {
            $match: {
                "contacts.personEmailId": {$in:emailIdArr}

            }
        },
        {
            $project:project
        }
    ]).exec(function (err,contacts) {

        callback(err,contacts)

    });
}

UserManagement.prototype.updateTwitterHandelForCompanyOrPerson = function (emailId,company,twitterUserName,updateFor,callback) {

    var query = {};
    var setObj = {};
    if(updateFor == "person" && emailId){
        query = {
            "contacts": {
                $elemMatch: {
                    "personEmailId": emailId
                }
            }
        };
        setObj = {$set: { "contacts.$.twitterUserName": twitterUserName } };
    }
    else if(updateFor == "company" && company){
        query = {
            "contacts": {
                $elemMatch: {
                    "account.name": company
                }
            }
        };
        setObj = {$set: { "contacts.$.account.twitterUserName": twitterUserName } };
    }

    if(query && setObj) {

        myUserCollection.update(
            query,
            setObj,
            {multi: true, upsert: true},
            function (err, res) {

                if (!err) {
                    callback(err, res)
                } else {
                    callback(err, null)
                }
            });
    }
    else{
        callback('No input', null)
    }
};

UserManagement.prototype.updateEmailSentOutFrequency = function (userId, emailFrequency,callback) {
    myUserCollection.update({ _id: userId }, { $set:{emailSentOutFrequency: emailFrequency,emailSentOutFrequencyUpdatedOn:new Date()}}, {upsert:true}, function(err, res) {
        if (!err) {
            callback(err, res)
        } else {
            callback(err, null)
        }
    });
};

UserManagement.prototype.getAllUserIds = function(isCorporate,callback){

    myUserCollection.find({corporateUser:isCorporate},{_id:1,emailId:1},function (err,userIds) {
        callback(err,userIds)
    })
}

UserManagement.prototype.getUserTzByUname = function(publicProfileUrl,callback){
    myUserCollection.find({publicProfileUrl:publicProfileUrl},{timezone:1},function (err,timezone) {
        callback(err,timezone)
    })
}

UserManagement.prototype.getTeam = function(companyId,emailId,callback){

    var search = new RegExp(emailId,'i');

    myUserCollection.find({companyId:companyId,emailId:search},{_id:1,emailId:1,firstName:1,lastName:1,designation:1},function (err,team) {
        callback(err,team)
    })

};

UserManagement.prototype.getTeamMembers = function(companyId,callback){

    var key = "getTeam"+String(companyId)

    getCache(key,function (data) {
        if(data){
            callback(null,data)
        } else {
            myUserCollection.find({companyId:companyId,corporateUser:true},{_id:1,emailId:1,firstName:1,
                lastName:1,designation:1,createdDate:1,
                productTypeOwner:1,verticalOwner:1,regionOwner:1,
                hierarchyParent:1,hierarchyPath:1,orgHead:1,companyTargetAccess:1,corporateAdmin:1,matrixAcess:1,businessUnits:1}).lean().exec(function (err,team) {
                setCache(key,team)
                callback(err,team)
            })
        }
    })
};

UserManagement.prototype.getTeamDataForUserReport = function(companyId,callback){
    myUserCollection.find({companyId:companyId,corporateUser:true},{_id:1,emailId:1,firstName:1,
        lastName:1,designation:1,createdDate:1,orgHead:1,
        lastLoginDate:1,registeredDate:1,location:1, mobileAppVersion:1, lastMobileSyncDate:1
    },function (err,team) {
        callback(err,team)
    })
};

UserManagement.prototype.getTeamMembersWithContacts = function(companyId,callback){
    myUserCollection.aggregate([
        {
            $match:{companyId:companyId}
        },
        {$unwind:"$contacts"},
        { $group:{
            _id:{emailId:"$emailId",createdDate:"$createdDate",userId:"$_id"},
            contacts: { $push: {
                contactEmailId: '$contacts.personEmailId',
                contactRelation: '$contacts.contactRelation'
            } },
            count: {
                $sum: 1
            }
        }}
    ]).exec(function (err,team) {
        callback(err,team)
    })
};

UserManagement.prototype.addOppOwner = function (userId,settings,callback) {

    var updateObj = {
        productTypeOwner:settings.productTypeOwner,
        regionOwner:settings.regionOwner,
        businessUnits:settings.businessUnits,
        verticalOwner:settings.verticalOwner
    }

    if(settings.companyTargetAccess){
        updateObj.companyTargetAccess = settings.companyTargetAccess
    }

    var matrixAccess = [];

    matrixAccess.push({
        type:"products",
        list:settings.productTypeOwner?settings.productTypeOwner:[]
    })

    matrixAccess.push({
        type:"regions",
        list:settings.regionOwner?settings.regionOwner:[]
    })

    matrixAccess.push({
        type:"verticals",
        list:settings.verticalOwner?settings.verticalOwner:[]
    })

    matrixAccess.push({
        type:"businessUnits",
        list:settings.businessUnits?settings.businessUnits:[]
    });

    myUserCollection.update({"_id": userId}, { $set: {matrixAccess:matrixAccess} }, { upsert: true }, function (error, result) {
    })

    myUserCollection.update({"_id": userId}, { $set: updateObj }, { upsert: true }, function (error, result) {
        if(!error && result) {
            callback(error,true)
        } else {
            callback(error,false)
        }
    })
}

UserManagement.prototype.setOrgHead = function (userId,companyId,callback) {

    myUserCollection.update({companyId:companyId}, { $set: {orgHead:false} }, { upsert: true,multi: true }, function (error1, result1) {
        myUserCollection.update({"_id": userId}, { $set: {orgHead:true} }, { upsert: true }, function (error, result) {
            if(!error && result) {
                callback(error,true)
            } else {
                callback(error,false)
            }
        })
    });
}

UserManagement.prototype.removeOppOwner = function (userIds,settings,callback) {

    var updateObj = {}
    updateObj[settings.type] = settings.name;

    myUserCollection.update({"_id": {$in:userIds}}, { $pull: updateObj }, { multi: true }, function (error, result) {
        if(!error && result) {
            callback(error,true)
        } else {
            callback(error,false)
        }
    })
}

UserManagement.prototype.getPortfolioHeads = function (query,callback) {

    portfolioAccess.find(query,function(err,portfolios){
        callback(err,portfolios)
    })
}

UserManagement.prototype.getPortfolios = function (companyId,userEmailIds,userId,callback) {

    var key = "portfolios" + String(userId);

    getCachePortfolio(key, function (cachePortfolio) {

        if (cachePortfolio) {
            callback(null, cachePortfolio);
        } else {

            portfolioAccess.aggregate([{
                $match: {
                    companyId: companyId, ownerEmailId: {$in: userEmailIds}
                }
            }, {
                $group: {
                    _id: {type: "$type", name: "$name"},
                    users: {
                        $push: "$$ROOT"
                    }
                }
            }], function (error, result) {
                if (error) {
                    loggerError.info('getPortfolios ' + JSON.stringify(error))
                    callback(error, null)
                } else {

                    if (result && result.length > 0) {

                        var forReportees = [];

                        _.each(result, function (re) {
                            var isHead = false;

                            re.type_name = re._id.type+re._id.name;

                            _.each(re.users,function(user){
                               if(user.isHead){
                                   isHead = true;
                               }
                            });

                            if (isHead) {
                                forReportees.push({
                                    type: re._id.type,
                                    name: re._id.name,
                                })
                            }
                        });

                        if (forReportees && forReportees.length > 0) {
                            var queryToGetReportees = {
                                "$or": forReportees.map(function (el) {
                                    return {
                                        type: el.type,
                                        name: el.name,
                                        companyId: companyId
                                    };
                                })
                            };

                            queryToGetReportees["companyId"] = companyId;

                            portfolioAccess.aggregate([{
                                $match: queryToGetReportees
                            }, {
                                $group: {
                                    _id: {type: "$type", name: "$name"},
                                    users: {
                                        $push: "$$ROOT"
                                    }
                                }
                            }], function (err, reportees) {
                                if (reportees && reportees.length > 0) {
                                    _.each(reportees, function (re) {
                                        re.type_name = re._id.type + re._id.name;
                                    });
                                }

                                reportees = reportees.concat(result);

                                reportees = _.uniq(reportees,"type_name");

                                setCache(key, {owner: result, reportees: reportees})
                                callback(error, {owner: result, reportees: reportees});
                            })
                        } else {
                            callback(error, {owner: result, reportees: result});
                        }
                    } else {
                        callback(error, {owner: result, reportees: result});
                    }
                }
            })
        }
    })
}

UserManagement.prototype.updateNotificationSettings = function(userId, settingName, status, updateFor, callback) {
    var updateQuery = {};

    switch(settingName) {
        case "documentView": 
                updateQuery = updateFor === "web" ? {"notificationSettings.documentView.web":status} : 
                                                        {"notificationSettings.documentView.mobile":status};
                break;
        case "commitPending":
                updateQuery = updateFor === "web" ? {"notificationSettings.commitPending.web":status} : 
                                                        {"notificationSettings.commitPending.mobile":status};
                break;
        case "tasksForToday":
                updateQuery = updateFor === "web" ? {"notificationSettings.tasksForToday.web":status} : 
                                                        {"notificationSettings.tasksForToday.mobile":status};
                break;
        case "losingTouch":
                updateQuery = updateFor === "web" ? {"notificationSettings.losingTouch.web":status} : 
                                                        {"notificationSettings.losingTouch.mobile":status};
                break;
        case "oppClosingToday":
                updateQuery = updateFor === "web" ? {"notificationSettings.oppClosingToday.web":status} : 
                                                        {"notificationSettings.oppClosingToday.mobile":status};
                break;
        case "dealsAtRiskForUser":
                updateQuery = updateFor === "web" ? {"notificationSettings.dealsAtRiskForUser.web":status} : 
                                                        {"notificationSettings.dealsAtRiskForUser.mobile":status};
                break;
        case "dealsAtRiskForManager":
                updateQuery = updateFor === "web" ? {"notificationSettings.dealsAtRiskForManager.web":status} : 
                                                        {"notificationSettings.dealsAtRiskForManager.mobile":status};
                break;
        case "oppClosingThisWeek":
                updateQuery = updateFor === "web" ? {"notificationSettings.oppClosingThisWeek.web":status} : 
                                                        {"notificationSettings.oppClosingThisWeek.mobile":status};
                break;
        case "weeklyCommit":
                updateQuery = updateFor === "web" ? {"notificationSettings.weeklyCommit.web":status} : 
                                                        {"notificationSettings.weeklyCommit.mobile":status};
                break;
        case "targetWonLostManager":
                updateQuery = updateFor === "web" ? {"notificationSettings.targetWonLostManager.web":status} : 
                                                        {"notificationSettings.targetWonLostManager.mobile":status};
                break;
        case "documentSharingStatus":
                updateQuery = updateFor === "web" ? {"notificationSettings.documentSharingStatus.web":status} : 
                                                        {"notificationSettings.documentSharingStatus.mobile":status};
                break;
    };

    myUserCollection.update({"_id": userId}, {$set: updateQuery }, function(error, result) {
        callback(error, result);
    })
}

UserManagement.prototype.updateWarEmailSettings = function(userId, settingName, callback) {
    myUserCollection.update({"_id": userId}, {$set: {"notification.notificationOn": settingName}}, function(error, result) {
        callback(error, result);
    })
}

UserManagement.prototype.updateFirebaseToken = function(userId, body, callback) {
    var from = body.from;
    var token = body.token;
    var notificationEnabledStatus = body.notificationEnabled;

    if(from == "web") {

        if(token) {
            var updateObj = {
                webFirebaseSettings: {
                    firebaseToken: token,
                    notificationEnabled: notificationEnabledStatus
                }
            };
    
            myUserCollection.update({"_id": userId}, {$set: updateObj}, function(error, result) {
                callback(error, result);
            });
        } else {
            myUserCollection.update({"_id": userId}, {$set: {"webFirebaseSettings.notificationEnabled": notificationEnabledStatus}}, function(error, result) {
                callback(error, result);
            });
        }
    }
    else if(from == "mobile") {
        myUserCollection.update({"_id": userId}, {$set: {mobileFirebaseToken: token}}, function(error, result) {
            callback(error, result);
        });
    } else {
        callback(true, null);
    }
}

UserManagement.prototype.getFirebaseToken = function(userId, callback) {
    myUserCollection.findOne({"_id": userId}, {_id: 0, mobileFirebaseToken:1}, function(error, result) {
        callback(error, result);
    });
}

function getBasicProfileFields(){
    var profileObj = {
        firstName: '',
        lastName: '',
        profilePicUrl: '',
        publicProfileUrl: '',
        designation: '',
        companyName: '',
        location: '',
        public: true,
        emailId: '',
        registeredUser: false,
        partialFill: true,
        serviceLogin: '',
        currentLocation: '',
        userType: 'not-registered'
    }

    return profileObj;

}

function EncryptionValue(data) {
    var shaAlgorithm = crypto.createHash('sha256');
    shaAlgorithm.update(data);
    var hashedData = shaAlgorithm.digest('hex');
    return hashedData;
}

function constuctPublicProfile(user, callback) {
    var profile = {
        'id': user._id,
        'firstName': user.firstName,
        'lastName': user.lastName,
        'companyName': user.companyName,
        'designation': user.designation,
        'location': user.location,
        'emailId': user.emailId,
        'phoneNumber': user.mobileNumber,
        'skypeId': user.skypeId,
        'profilePicUrl': user.profilePicUrl,
        'google': user.google,
        'currentLocation': user.currentLocation,
        'workHours': user.workHours,
        'timezone': user.timezone
    }
    user.contacts = []
    callback(user);
}

function removeDuplicates(arr) {

    var end = arr.length;

    for (var i = 0; i < end; i++) {
        for (var j = i + 1; j < end; j++) {
            if (arr[i].emailId == arr[j].emailId) {
                var shiftLeft = j;
                for (var k = j + 1; k < end; k++, shiftLeft++) {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for (var l = 0; l < end; l++) {
        whitelist[l] = arr[l];
    }
    //whitelist.reverse();
    return whitelist;
}

function removeDuplicates_id(arr) {

    var end = arr.length;

    for (var i = 0; i < end; i++) {
        for (var j = i + 1; j < end; j++) {
            if (arr[i]._id == arr[j]._id) {
                var shiftLeft = j;
                for (var k = j + 1; k < end; k++, shiftLeft++) {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for (var l = 0; l < end; l++) {
        whitelist[l] = arr[l];
    }
    //whitelist.reverse();
    return whitelist;
}

function checkRequired(data) {
    if (data == '' || data == null || data == undefined) {
        return false;
    } else {
        return true;
    }
}

function getClientIp(req) {
    var ipAddress;
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        ipAddress = req.connection.remoteAddress;
    }
    return ipAddress;
}

function updateCurrentLocation(IP, userId) {
    IPLocator.lookup(IP, userId, function(location) {});
}

function castToObjectId(id) {
    return mongoose.Types.ObjectId(id)
};

function castListToObjectIds (ids){
    return _.map(ids, function(id){
        return castToObjectId(id)
    })
};

UserManagement.prototype.updateHierarchy = function (data,callback) {

    myUserCollection.findOne({_id:castToObjectId(data.parentId)},{hierarchyPath:1,orgHead:1},function (err,parentsHierarchyPath) {

        var newHierarchyPath = null;

        if(parentsHierarchyPath && parentsHierarchyPath.hierarchyPath && !parentsHierarchyPath.orgHead){
            newHierarchyPath = parentsHierarchyPath.hierarchyPath+data.parentId+",";
        }

        if(parentsHierarchyPath.orgHead){
            newHierarchyPath = ","+data.parentId+",";
        }

        if(newHierarchyPath){
            myUserCollection.update({_id:castToObjectId(data.userId)},
                {$set:{hierarchyPath:newHierarchyPath,hierarchyParent:castToObjectId(data.parentId)}},function (err,result) {
                    callback(err,true)
            });
        } else {
            callback(err,false)
        }
    })

}

function setCache(key,data){
    //Storing for 60 min
    redisClient.setex(key, 3600, JSON.stringify(data));
}

function getCache(key,callback){
    redisClient.get(key, function (error, result) {
        try {
            var data = JSON.parse(result);
            // callback(data)
            callback(null)
        } catch (err) {
            callback(null)
        }
    });
}

function getCachePortfolio(key,callback){
    redisClient.get(key, function (error, result) {
        try {
            var data = JSON.parse(result);
            // callback(data)
            callback(null)
        } catch (err) {
            callback(null)
        }
    });
}

// export the User Management Class
module.exports = UserManagement;
