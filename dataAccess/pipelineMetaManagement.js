var pipelineMeta = require('../databaseSchema/userManagementSchema').pipelineMeta;
var dashboardInsight = require('../databaseSchema/dashboardInsightSchema').dashboardInsight;
var _ = require("lodash");
var moment = require('moment-timezone');

function PipelineMetaManagement() {

}

PipelineMetaManagement.prototype.findPipelineDashboardColl = function(users,startDate,endDate,
                                                                      targets,wonOpps,commits,
                                                                      currenciesObj,primaryCurrency,details,callback){

    var query = {
        ownerId: {$in:users},
        forTeam:false,
        date: {$gte: new Date(startDate),$lte: new Date(endDate)}
    }

    dashboardInsight.find(query,{"pipelineFlow.metaData":1,date:1}).lean().exec(function (err, data) {
        var flattenedData = [];
        if(!err && data && data.length){
            _.each(data,function (el) {

                var month = moment(el.date).month()+1;
                var year = moment(el.date).year();
                var day = moment(el.date).date();

                flattenedData.push({
                    monthYear:day+""+month+""+year,
                    date:el.date,
                    day:day,
                    month:month,
                    year:year,
                    opps:el.pipelineFlow && el.pipelineFlow.metaData && el.pipelineFlow.metaData[1]?getRequiredOpps(el.pipelineFlow.metaData[1],startDate,endDate):[]

                })
            })
        }

        if(details){

            var flattenedOpps = [];
            _.each(flattenedData,function (el) {
                flattenedOpps = flattenedOpps.concat(el.opps)
            });

            callback(err,flattenedOpps);

        } else {

            var result = _(flattenedData)
                .groupBy('monthYear')
                .map(function(val,key) {

                    var opps = [];
                    _.each(val,function (va) {
                        _.each(va.opps,function (el) {
                            opps.push(el)
                        })
                    })

                    var obj = {
                        monthYear:key,
                        date:val[0].date,
                        day:val[0].day,
                        month:val[0].month,
                        year:val[0].year,
                        opps:opps
                    };

                    return obj;
                })
                .value();

            var wonOppsObj = {};
            _.each(wonOpps,function (op) {
                var wonAmt = 0;
                _.each(op.opps,function (o) {
                    wonAmt = wonAmt+parseFloat(o.amount)
                });
                wonOppsObj[op.day+""+op.month+""+op.year] = wonAmt;
            });

            callback(err,formatForDisplay(result,startDate,endDate,targets,wonOppsObj,commits,currenciesObj,primaryCurrency,true))
        }

    });
}

function getRequiredOpps(data,startDate,endDate){

    var result = [];

    if(data.oppMetaDataFormat && data.oppMetaDataFormat && data.oppMetaDataFormat.data && data.oppMetaDataFormat.data.length>0){
        _.each(data.oppMetaDataFormat.data,function (el) {
            if(el.stageName && el.stageName !== "Close Won" && el.stageName !== "Close Lost"){

                _.each(el.oppIds,function (op) {
                    if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate)<= new Date(endDate)){
                        result.push({
                            stageName:el.stageName,
                            amount_original:op.amount_original,
                            // amount:op.amount_original,
                            amount:op.amount,
                            amountWithNgm:op.amount,
                            opportunityId:op.opportunityId,
                            closeDate:op.closeDate
                        })
                    }
                });
            }
        })
    }

    return result;
}

PipelineMetaManagement.prototype.findPipelineMetaByDateRange = function(users,startDate,endDate,targets,wonOpps,commits,currenciesObj,primaryCurrency,callback){

    var query = {
        userId: {$in:users},
        date: {$gte: new Date(startDate),$lte: new Date(endDate)}
    }

    pipelineMeta.aggregate([
        {
            $match: query
        },
        {
            $project: {
                day: {$dayOfMonth:"$date"},
                month: {$month:"$date"},
                year: {$year:"$date"},
                date: 1,
                opportunities:1
            }
        },
        {
            $group: {
                _id: {
                    day:"$day",
                    month:"$month",
                    year:"$year"
                },
                date: {$first:"$date"},
                opps: {$push: "$opportunities"}
            }
        }
    ]).exec(function (err,opps) {
        var allOpps = [];
        if(!err && opps && opps.length>0){
            _.each(opps,function (op) {
                allOpps.push({
                    day:op._id.day,
                    month:op._id.month,
                    year:op._id.year,
                    date:op.date,
                    opps:_.uniq(_.flatten(op.opps),"opportunityId")
                });
            });
        };

        var wonOppsObj = {};
        _.each(wonOpps,function (op) {
            var wonAmt = 0;
            _.each(op.opps,function (o) {
                wonAmt = wonAmt+parseFloat(o.amount)
            });
            wonOppsObj[op.day+""+op.month+""+op.year] = wonAmt;
        });

        callback(err,formatForDisplay(allOpps,startDate,endDate,targets,wonOppsObj,commits,currenciesObj,primaryCurrency))
    });
}

PipelineMetaManagement.prototype.findPipelineByDateRange = function(users,startDate,endDate,wonOpps,callback){

    var query = {
        userId: {$in:users},
        date: {$gte: new Date(startDate),$lte: new Date(endDate)},
        opportunities: { $exists: true, $ne: [] }
    }

    pipelineMeta.aggregate([
        {
            $match: query
        },
        {
            $group: {
                _id: null,
                date: {$last:"$date"},
                opps: {$last: "$opportunities"}
            }
        }
    ]).exec(function (err,data) {
        var allOpps = [];
        if(data && data[0] && data[0].opps){
            _.each(data[0].opps,function (op) {
                if(op.stageName != "Close Won" && op.stageName != "CloseLost"){

                    convertAmount(op);
                    op.amount = op.amountWithNgm;
                    if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate)<= new Date(endDate)){
                        allOpps.push(op)
                    }
                }
            })
        }
        callback(err,allOpps.concat(wonOpps))
    });
}

PipelineMetaManagement.prototype.formatOpps = function (opps,startDate,endDate,targets,commits,currenciesObj,primaryCurrency,fromDashIns,callback) {
    callback(formatForDisplay(opps,startDate,endDate,targets,[],commits,currenciesObj,primaryCurrency,fromDashIns))
}

function formatForDisplay(data,startDate,endDate,targets,wonOppsObj,commits,currenciesObj,primaryCurrency,fromDashIns){

    var data_formatted = [];
    var dtFormat = "DD MMM YYYY";
    var allMonthYear = enumerateDaysBetweenDates(startDate,endDate,dtFormat);
    var allMonthYearObj = {};
    _.each(allMonthYear,function (el) {
        allMonthYearObj[el.monthYear] = el.date
    });

    var existingMonthYear = [];
    var targetObj = getTargetsSum(targets);
    var commitObj = {};
    if(commits && commits.length>0){
        _.each(commits,function (co) {
            if(commitObj[co.monthYear]){
                commitObj[co.monthYear] = co.commitValue+commitObj[co.monthYear]
            } else {
                commitObj[co.monthYear] = co.commitValue
            }
        })
    };

    if(data.length>0){
        _.each(data,function (da) {

            existingMonthYear.push(moment(da.date).format(dtFormat));

            var won = 0,
                pipeline = 0;

            _.each(da.opps,function (op) {

                if(!fromDashIns){
                    convertAmount(op,primaryCurrency,currenciesObj);
                }

                if(op.stageName === "Close Won"){
                    won = won+parseFloat(op.amountWithNgm);
                }

                if(!_.includes(["Close Won","Close Lost"],op.stageName)){

                    if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate)<= new Date(endDate)){

                        if(op.amountWithNgm){
                            pipeline = pipeline+parseFloat(op.amountWithNgm);
                        } else {
                            pipeline = pipeline+parseFloat(op.amount);
                        }
                    }
                }
            });

            data_formatted.push({
                monthYear:da.day+""+da.month+""+da.year,
                date:da.date,
                month:da.month,
                year:da.year,
                won:won,
                pipeline:pipeline,
                target: 0
            });

        })
    }

    var nonExistingMonthYear = _.xor(_.pluck(allMonthYear,"monthYear"),existingMonthYear);

    if(nonExistingMonthYear && nonExistingMonthYear.length>0){
        _.each(nonExistingMonthYear,function (el) {

            var month = moment(allMonthYearObj[el]).month()+1;
            var year = moment(allMonthYearObj[el]).year();
            var day = moment(allMonthYearObj[el]).date();

            data_formatted.push({
                monthYear:day+""+month+""+year,
                date:allMonthYearObj[el],
                day:day,
                month:month,
                year:year,
                won:0,
                pipeline:0,
                target:0
            });
        })
    }

    if(data_formatted.length == 91){
        var copyObj = data_formatted[data_formatted.length-1];
        var obj = {
            monthYear:copyObj.monthYear,
            date:copyObj.date,
            day:copyObj.day,
            month:copyObj.month,
            year:copyObj.year,
            won:0,
            pipeline:0,
            target:0
        };
        data_formatted.push(obj);
    }

    if(data_formatted.length == 90){
        var copyObj = data_formatted[data_formatted.length-1];

        data_formatted.push({
            monthYear:copyObj.monthYear,
            date:copyObj.date,
            day:copyObj.day,
            month:copyObj.month,
            year:copyObj.year,
            won:0,
            pipeline:0,
            target:0
        });
        data_formatted.push({
            monthYear:copyObj.monthYear,
            date:copyObj.date,
            day:copyObj.day,
            month:copyObj.month,
            year:copyObj.year,
            won:0,
            pipeline:0,
            target:0
        });
    }

    _.each(data_formatted,function (el) {
        if(wonOppsObj && wonOppsObj[el.monthYear]){
            el.won = wonOppsObj[el.monthYear];
        }

        if(commitObj[el.monthYear]){
            el.commits = commitObj[el.monthYear];
        } else {
            el.commits = 0;
        }
    })

    data_formatted[data_formatted.length-1].target = targetObj;

    return data_formatted;
}

function enumerateDaysBetweenDates(startDate, endDate, dtFormat) {

    var start = moment(startDate)
    var end = moment(endDate)

    var now = start, dates = [];

    while (now <= end) {
        // dates.push(now.format("DD MMM YY"));
        dates.push({date:new Date(now),monthYear:now.format(dtFormat)});
        now.add('days', 1);
    }
    return dates;
};

function getMonthsBetweenDates(from,to) {
    var startDate = moment(from).add(2, 'days');
    var endDate = moment(to).add(2, 'days');

    var result = [];

    if (endDate.isBefore(startDate)) {
        throw "End date must be greater than start date."
    }

    var currentDate = startDate.clone();

    while (currentDate.isBefore(endDate)) {
        result.push({date:new Date(currentDate),monthYear:currentDate.format("MMM YYYY")});
        currentDate.add(1, 'month');
    }

    return result;
}

function getTargetsSum(data,dtFormat){
    var obj = {};
    var targets = data && data[0]? data[0].targets:[];
    var targetVal = 0;

    if(targets && targets.length>0){
        _.each(targets,function (tr) {
            targetVal = targetVal+tr.target;
            obj[moment(tr.date).format("MMYYYY")] = tr.target;
        })
    }

    // return obj;
    return targetVal;
}

function convertAmount(el,primaryCurrency,currenciesObj) {

    el.amount = parseFloat(el.amount);
    el.amountWithNgm = parseFloat(el.amount);

    if(el.netGrossMargin || el.netGrossMargin == 0){
        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
    }

    el.convertedAmt = el.amount;
    el.convertedAmtWithNgm = el.amountWithNgm

    if(currenciesObj && el.currency && el.currency !== primaryCurrency){

        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
        }

        if(el.netGrossMargin || el.netGrossMargin == 0){
            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
        }

        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))

    }
}

module.exports = PipelineMetaManagement;
