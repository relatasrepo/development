var phCollection = require('../databaseSchema/productHierarchySchema').productHierarchy
var winstonLog = require('../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston();
var _ = require("lodash");

function ProductHierarchy() {}

ProductHierarchy.prototype.getHierarchy = function (companyId,callback){
    phCollection.find({companyId:companyId},{monthlyTargets:0}).lean().exec(function (err,result) {
      callback(err,result)
    });
}

ProductHierarchy.prototype.updateHierarchy = function (userId,monthlyTargets,callback){
    phCollection.update({userId:userId},{$set:{monthlyTargets:monthlyTargets}}).exec(function (err,result) {
      callback(err,result)
    });
}

ProductHierarchy.prototype.updateHierarchyPaths = function (companyId,updateObj,common,callback){

    bulkHierarchyPathForSelf(companyId,common,updateObj,callback);

    if(updateObj.childrenIds && updateObj.childrenIds.length>0){
        bulkHierarchyPathForChildren(companyId,common,updateObj)
    }
}

function bulkHierarchyPathForChildren(companyId,common,updateObj,callback) {
    var bulk = phCollection.collection.initializeUnorderedBulkOp();
    updateObj.childrenIds.forEach(function(a){

        bulk.find({
            userId:common.castToObjectId(a)
        })
        .updateOne({
            $set:{
                hierarchyPath:updateObj.pathForChildren
            }
        });
    });

    if(updateObj.childrenIds.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                if(callback){
                    callback(false)
                }
            }
            else {
                result = result.toJSON();
                logger.info('addUserToHierarchy() results ',result);
                if(callback){
                    callback(result.upserted)
                }
            }
        });
    }
    else{
        if(callback){
            callback(true)
        }
    }
}

function bulkHierarchyPathForSelf(companyId,common,updateObj,callback) {

    phCollection.update({userId:common.castToObjectId(updateObj.selfId)},{$set:{hierarchyParent:updateObj.selfParentId,hierarchyPath:updateObj.pathForSelf}}).exec(function (err,result) {
        callback(err,result)
    });
}

ProductHierarchy.prototype.removeFromHierarchy = function (userIds,companyId,callback){
    phCollection.remove({userId:{$in:userIds},companyId:companyId}).exec(function (err,result) {
      callback(err,result)
    });
}

ProductHierarchy.prototype.setOrgHead = function (updateObj,common,companyId,callback){

    var data = [{companyId: common.castToObjectId(companyId),
        userId: common.castToObjectId(updateObj.userId),
        ownerEmailId:updateObj.emailId,
        hierarchyParent: null,
        hierarchyPath:null,
        orgHead:true
    }];

    var bulk = phCollection.collection.initializeUnorderedBulkOp();

    data.forEach(function(a){

        bulk.find({
            userId:a.userId
        })
        .upsert()
        .updateOne(
            {$set:{
                companyId:a.companyId,
                userId:a.userId,
                ownerEmailId:a.ownerEmailId,
                hierarchyParent:a.hierarchyParent,
                hierarchyPath:a.hierarchyPath,
                orgHead:a.orgHead
            }});
    });


    if(data.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                callback(false)
            }
            else {
                result = result.toJSON();
                logger.info('addUserToHierarchy() results ',result);
                callback(result.upserted)
            }
        });
    }
    else{
        callback(true)
    }
}

ProductHierarchy.prototype.addUserToHierarchy = function (updateObj,companyId,common,callback){
    var data = [{companyId: common.castToObjectId(companyId),
            userId: common.castToObjectId(updateObj.userId),
            ownerEmailId:updateObj.emailId,
            hierarchyParent: updateObj.hierarchyParent,
            hierarchyPath:updateObj.hierarchyPath,
            orgHead:false
        }];

    var bulk = phCollection.collection.initializeUnorderedBulkOp();

    data.forEach(function(a){

        bulk.find({
                userId:a.userId
            })
            .upsert()
            .updateOne(
                {$set:{
                    companyId:a.companyId,
                    userId:a.userId,
                    ownerEmailId:a.ownerEmailId,
                    hierarchyParent:a.hierarchyParent,
                    hierarchyPath:a.hierarchyPath,
                    orgHead:a.orgHead
                }}
            );
    });


    if(data.length > 0) {
        bulk.execute(function (err, result) {
            if (err) {
                logger.info('Error in addUserToHierarchy() ',err );
                callback(false)
            }
            else {
                result = result.toJSON();
                logger.info('addUserToHierarchy() results ',result);
                callback(result.upserted)
            }
        });
    }
    else{
        callback(true)
    }

}

ProductHierarchy.prototype.getUserHierarchy = function(userId, callBack) {

    phCollection.findOne({ userId: userId }).lean().exec(function(err, user) {

        if(!err && user){
            phCollection.find({ companyId:user.companyId, hierarchyPath: { $regex: userId.toString(), $options: 'i' }}).lean().exec(function(err, users) {
                user["hierarchyParent"] = null
                users.push(user)
                callBack(null, users)
            })
        } else {
            callBack(err,null)
        }
    })
};

module.exports = ProductHierarchy;