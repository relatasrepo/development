var jsdiff = require('diff');

var companyCollection = require('../../databaseSchema/corporateCollectionSchema').companyModel;
var corporateCollection = require('../../databaseSchema/corporateCollectionSchema').corporateModel;
var winstonLog = require('../../common/winstonLog');
var commonUtility = require('../../common/commonUtility');

var logLib = new winstonLog();
var common = new commonUtility();
var loggerError = logLib.getCorporateErrorLogger();
var logger = logLib.getCorporateLogger();
var userCollection = require('../../databaseSchema/userManagementSchema').User;
function corporateAdminClass(){
    this.updateAssigned = function(updateInfo,callback){
        updateHierarchy(updateInfo.companyId,updateInfo.userId,updateInfo.parentId,callback);
    };

    this.resetHierarchy = function(companyId,callback){

        userCollection.update({companyId:companyId,resource:{$ne:true}},{$set:{hierarchyPath:null,hierarchyParent:null}},{multi:true},function(error,result){
            if(error){
                loggerError.info('Mongo error in resetHierarchy() in adminClass ',error);
            }

            callback(result && result.ok);
        })
    }
}

// new logic ***

function updateHierarchy(companyId,userId,parentId,callback){

    userCollection.findOne({_id:userId,companyId:companyId},{emailId:1,companyId:1,hierarchyPath:1},function(error,user){
         if(!error && common.checkRequired(user)){
          var oldPath = user.hierarchyPath;
             updateParentAndPaths(companyId,userId,parentId,oldPath,callback)
         }
         else{
             loggerError.info('Mongo error in updateHierarchy() in adminClass ',error);
             callback(false);
         }
    });
}

function updateParentAndPaths(companyId,userId,parentId,oldPath,callback){
    userCollection.findOne({_id:parentId,companyId:companyId},{hierarchyPath:1},function(error,parentObj){
        if(error || !common.checkRequired(parentObj)){
            loggerError.info('Mongo error in updateParentAndPaths() 1 in adminClass ',error);
            callback(false);
        }
        else if(common.checkRequired(parentObj)){

            var newPath = parentObj.hierarchyPath || ',';

            newPath = common.checkRequired(newPath) ? newPath+','+String(parentObj._id)+',' : String(parentObj._id)+',';
            newPath = newPath.charAt(0) == ',' ? newPath : ','+newPath;

            userCollection.update({_id:userId,companyId:companyId},{$set:{hierarchyParent:parentId,hierarchyPath:newPath}},function(error,result){

                if(!error && result && result.ok){

                        oldPath = common.checkRequired(oldPath) ? oldPath : ',';
                        newPath = common.checkRequired(newPath) ? newPath : ',';
                        oldPath = oldPath.split(',').join(" ").trim();
                        newPath = newPath.split(',').join(" ").trim();
                        var diff = jsdiff.diffWords(oldPath, newPath);

                        updateChildSubTree(companyId,userId,parentId,newPath,diff,callback);

                }else{
                    loggerError.info('Mongo error in updateParentAndPaths() 2 in adminClass ',error);
                    callback(false);
                }
            });
        }else{
            loggerError.info('Error in updateParentAndPaths() 3 in adminClass No Parent object found');
            callback(false);
        }
    });
}
// end ***
function updateChildSubTree(companyId,userId,parentId,newPath,diff,callback){
    var criteria = ','+userId+',';

    userCollection.find({companyId:companyId,hierarchyPath:{ $regex: criteria, $options: 'i' }},{emailId:1,companyId:1,hierarchyPath:1,hierarchyParent:1},function(error,subTree){
         if(!error && subTree.length > 0){

             var bulk = userCollection.collection.initializeUnorderedBulkOp();
             var isValid = false;
             for(var i=0; i<subTree.length; i++){
                 if(common.checkRequired(subTree[i].path)){
                     isValid = true;
                     var resultPath = getNewPath(newPath,subTree[i].path,diff);
                     bulk.find({_id:subTree[i].userId,companyId:companyId}).update({$set:{hierarchyPath:resultPath}});
                 }
             }
             if(isValid){
                 bulk.execute(function(err, result) {
                     if(err){
                         logger.info('Error in updateChildSubTreeBulk():Contact Bulk',err,userId.toString())
                         if(callback){callback(false)}
                     }
                     else{
                         result = result.toJSON();
                         logger.info('updateChildSubTreeBulk() results ',result, userId.toString());
                         if(callback){callback(true)}
                     }
                 });
             }
             else{
                 if(callback){callback(true)}
             }
         }
         else{
             if(error){
                 loggerError.info('Mongo error in updateChildSubTree() 2 in adminClass ',error);
             }

             callback(true);
         }
    });
}

function getNewPath(newP,path,diff){  // newP and path with delimiter space
    if(common.checkRequired(path)){
        path = path.split(',').join(" ").trim();
    }

    if(common.checkRequired(diff) && diff.length > 0){
        var p = '';
        for(var i=0; i<diff.length; i++){
            if(diff[i].added){

                var addedIndex = newP.indexOf(diff[i].value);

                p += path.substring(0,addedIndex);

                p += addedIndex == 0 ? diff[i].value+" " : diff[i].value;

                p += path.substring(addedIndex,path.length);


            }else if(diff[i].removed){
                p = common.checkRequired(p) ? p : path;
                var pat = diff[i].value.trim();
                p = p.replace(pat,"");

            }
        }
        if(common.checkRequired(p)){

            p = ","+p.split(" ").join(",")+",";

            return p;

        }else return false
    }
}

function contains(str,subStr) {
    return str.indexOf(subStr) != -1;
}

corporateAdminClass.prototype.getTeamReport = function (companyId,fromDate,toDate,callback) {

    callback(true);
}

module.exports = corporateAdminClass;