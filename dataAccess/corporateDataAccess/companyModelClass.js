var companyCollection = require('../../databaseSchema/corporateCollectionSchema').companyModel;
var myUserCollection = require('../../databaseSchema/userManagementSchema').User;
var winstonLog = require('../../common/winstonLog');
var opportunitiesCollection = require('../../databaseSchema/userManagementSchema').opportunities;
var portfoliosCollection = require('../../databaseSchema/portfolios').portfolios;
var portfolioAccess = require('../../databaseSchema/portfolioAccess').portfolioAccess;

var logLib = new winstonLog();
var loggerError = logLib.getCorporateErrorLogger();
var logger = logLib.getCorporateLogger();

var _ = require("lodash")
var redis = require('redis');
var redisClient = redis.createClient();
var async = require("async")

function CorporateCompanyManagement()
{
  this.findOrCreateCompany = function(companyInfo,callback){
      companyCollection.findOne({_id:companyInfo.companyId},function(error,company){
          if(error){
              loggerError.info('An error occurred in findOrCreateCompany() '+JSON.stringify(error));
              callback(false);
          }else if(checkRequired(company)){

              callback(company);
          }else{
            logger.info('Creating new company '+companyInfo.companyName);
              createCompany(companyInfo,callback);
          }
      })
  };

    this.findCompanyById = function(companyId,callback){
        companyCollection.findOne({_id:companyId},function(error,companyInfo){
            if(error){
                loggerError.info('Error occurred while finding company by id findCompanyById() '+JSON.stringify(error));
            }
            callback(companyInfo)
        })
    };

    this.findCompanyByIdWithProjection = function(companyId,fields,callback){
        companyCollection.findOne({_id:companyId},fields,function(error,companyInfo){
            if(error){
                loggerError.info('Error occurred while finding company by id findCompanyById() '+JSON.stringify(error));
            }
            callback(companyInfo)
        })
    };
    
    this.getOppsByCompanyId = function (companyId,common,callback) {

        myUserCollection.find({companyId:common.castToObjectId(companyId.toString())},{_id:1},function (err,users) {

            opportunitiesCollection.aggregate([
                {
                    $match:{
                        userId:{$in:_.pluck(users,"_id")}
                    }
                },
                {
                    $group:{
                        _id:"$relatasStage",
                        count:{$sum:1}
                    }
                }
            ]).exec(function(er,oppsCount){
                callback(er,oppsCount)
            });
        })
    }

    this.updateOpps = function (companyId,fromStage,updateObj,callback) {

        myUserCollection.find({companyId:companyId},{_id:1},function (err,users) {

            var userIds = _.pluck(users,"_id");

            var match = { "$or": userIds.map(function(el) {
                var obj = {};
                obj["relatasStage"] = fromStage;
                obj["userId"] = el;

                return obj;
            })};

            opportunitiesCollection.update(match,{$set:updateObj},{multi:true},function (err,result) {
                callback(err,result);
            });
        });
    }

    this.getCompanyContacts = function(companyId,callback){
        companyCollection.findOne({_id:companyId},{contacts:1}).exec(function(error,obj){

            if(checkRequired(obj) && checkRequired(obj.contacts)){
                myUserCollection.populate(obj.contacts,{path:'userId', select:'emailId firstName lastName publicProfileUrl designation companyName corporateUser'},function(error,con){
                    callback(con);
                })
            }
        })
    };

    this.findOrCreateCompanyByUrl = function(companyInfo,projection,callback){

        if(companyInfo.companyName){
            findOrCreateCompanyByUrlPrivate(companyInfo,projection,callback);
        }
        else callback(false);
    };
    
    this.addGeoLocation = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { geoLocations:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.addReason = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { closeReasons:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.addAccountTypes = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { accountTypes:{name:updateObj} }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating addAccountTypes info '+JSON.stringify(error))
                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

  
    this.removeAccountTypes = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $pull: { accountTypes:{name:updateObj} }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating addAccountTypes info '+JSON.stringify(error))
                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.updateObj = function (companyId,updateObj,callback) {

        companyCollection.update(
            {
                _id:companyId
            },
            {
                $set:updateObj
            },{
                upsert:true
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }

                var key = "oppStages"+String(companyId);
                var key2 = "commitDayHour"+String(companyId);

                redisClient.del(key);
                redisClient.del(key2);
            })
    };

    this.addProductTotList = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { productList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };
    
    this.addSourceList = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { sourceList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.addSolutionList = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { solutionList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.addTypeList = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { typeList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.addBusinessUnit = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { businessUnits:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.addVerticalTotList = function (companyId,updateObj,callback) {
        
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { verticalList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.addDocumentCategory = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { documentCategoryList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.updateCurrency = function (companyId,updateObj,callback) {

        companyCollection.update(
            {
                _id:companyId
            },
            {
                $addToSet: { currency:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };
    
    this.netGrossMarginUpdate = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $set: { netGrossMargin:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.updateSalesforceSettings = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $set: { salesForceSettings:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.removeGeoLocation = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $pull: { geoLocations:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.removeSource = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $pull: { sourceList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.removeProductTotList = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $pull: { productList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.removeCloseReason = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $pull: { closeReasons:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.removeSolution = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $pull: { solutionList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.removeBusinessUnit= function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $pull: { businessUnits:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.removeType= function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $pull: { typeList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.removeFromVerticalList = function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $pull: { verticalList:updateObj }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.modifyDocumentCategory= function (companyId,updateObj,callback) {
        companyCollection.update(
            {
                _id:companyId,
                "documentCategoryList.name": updateObj.name
            },
            {
                $set: {"documentCategoryList.$.active": updateObj.active}
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                    callback(error,[])
                } else {
                    callback(error,result);
                }
            })
    };

    this.updateCompanyDetails = function(updateObj,callback){
        companyCollection.update(
            {
                _id:updateObj.companyId
            },
            {
                $set:
                {
                    companyName:updateObj.companyName,
                    emailDomains:updateObj.emailDomains,
                    description:updateObj.description,
                    presentationTitle:updateObj.presentationTitle,
                    presentationType:updateObj.presentationType,
                    socialInfo:updateObj.socialInfo,
                    latestNews:updateObj.latestNews,
                    quickStats:updateObj.quickStats
                }
            },
            function(error,result){
            if(error){
                loggerError.info('An error occurred while updating company info '+JSON.stringify(error))
            }
            callback(result && result.ok == 1);
        })
    };

    this.updateCompanyVideo = function(companyId,updateObj,callback){
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $set:
                {
                    videos:updateObj
                }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company video info '+JSON.stringify(error))
                }
                callback(result == 1);
            })
    };

    this.addCompanyDocument = function(companyId,updateObj,callback){

        companyCollection.update(
            {
                _id:companyId
            },
            {
                $push:
                {
                    documents:updateObj
                }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while adding company doc '+error);
                }
                callback(result == 1);
            })
    };

    this.addCompanySlide = function(companyId,updateObj,callback){

        companyCollection.update(
            {
                _id:companyId
            },
            {
                $push:
                {
                    slides:updateObj
                }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while adding company slides '+error);
                }
                callback(result == 1);
            })
    };

    this.updateCompanyLogo = function(companyId,updateObj,callback){
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $set:
                {
                    logo:updateObj
                }
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating company logo info '+JSON.stringify(error))
                }
                callback(result == 1);
            })
    };

    this.updateCompanyProductRequired = function(companyId,updateObj,callback){
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $set: {
                    "opportunitySettings.productRequired":updateObj
                }
            },{
                upsert:true
            },function(error,result){

                if(error){
                    loggerError.info('An error occurred while updating updateCompanyProductRequired info '+JSON.stringify(error));
                }
                callback(result == 1);
            })
    };

    this.updateCompanyBuRequired = function(companyId,updateObj,callback){

        companyCollection.update(
            {
                _id:companyId
            },
            {
                $set: {
                    "opportunitySettings.businessUnitRequired":updateObj
                }
            },{
                upsert:true
            },function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating updateCompanyBuRequired info '+JSON.stringify(error));
                }
                callback(result == 1);
            })
    };

    this.updateCompanySourceRequired = function(companyId,updateObj,callback){
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $set: {
                    "opportunitySettings.sourceRequired":updateObj
                }
            },{
                upsert:true
            },function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating updateCompanySourceRequired info '+JSON.stringify(error));
                }
                callback(result == 1);
            })
    };

    this.updateCompanyVerticalRequired = function(companyId,updateObj,callback){
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $set: {
                    "opportunitySettings.verticalRequired":updateObj
                }
            },{
                upsert:true
            },function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating updateCompanyVerticalRequired info '+JSON.stringify(error));
                }
                callback(result == 1);
            })
    };

    this.updateCompanyRegionRequired = function(companyId,updateObj,callback){
        companyCollection.update(
            {
                _id:companyId
            },
            {
                $set:
                {
                    "opportunitySettings.regionRequired":updateObj
                }
            },{
                upsert:true
            },
            function(error,result){
                if(error){
                    loggerError.info('An error occurred while updating updateCompanyRegionRequired info '+JSON.stringify(error));
                }
                callback(result == 1);
            })
    };

    this.addCompanyContact = function(companyId,updateObj,callback){

        companyCollection.findOne({_id:companyId,"contacts.userId":updateObj.userId},{"contacts.$":1},function(error,contact){

            if(error){
                loggerError.info('An error occurred while updating company contacts 1 '+JSON.stringify(error));
                callback(false);
            }
            else{
                 if(!isContactExist(contact)){
                     companyCollection.update(
                         {
                             _id:companyId
                         },
                         {
                           $push:{contacts:updateObj}
                         },
                         function(error,result){
                             if(error){
                                 loggerError.info('An error occurred while updating company contacts 2 '+JSON.stringify(error))
                             }
                             callback(result == 1);
                         })
                 }
                else{
                     callback(true);
                 }
            }
        });
    };

    function isContactExist(obj){
        if(checkRequired(obj)){
            if(checkRequired(obj.contacts) && obj.contacts.length > 0){
                return true;
            }else return false;
        }else return false;
    }

    this.getAllCompanies = function(callback){
        companyCollection.find({},{companyName:1,admin:1,logFile:1},function(error,companies){
            if(error){
               loggerError.info('Error occurred in company model class getAllCompanies() '+JSON.stringify(error));
            }
            callback(companies);
        })
    };

    this.getAllUserByCompany = function(callback){
        myUserCollection.aggregate([
            {
                $match:{
                    corporateUser:true,
                    companyId:{$exists:1},
                    resource:{$ne:true}
                }
            },
            {
                $project:{
                    _id:1,
                    emailId:1,
                    firstName:1,
                    lastName:1,
                    companyId:1,
                    corporateUser:1,
                    corporateAdmin:1
                }
            },
            {
                $group:{
                    _id:"$companyId",
                    users:{
                        $addToSet:{
                            _id:"$_id",
                            emailId:"$emailId",
                            firstName:"$firstName",
                            lastName:"$lastName",
                            corporateAdmin:"$corporateAdmin"
                        }
                    }
                }
            }
        ]).exec(function(er,users){
            if(!er && checkRequired(users) && users.length > 0){
                companyCollection.populate(users,{ path: '_id', select:'_id companyName admin'},function(eror,data){
                    data = data.filter(function(n){ return n._id != undefined });
                    callback(data);
                });

            }else callback([])
        });
    };

    this.updateCompanyAdmin = function(obj,companyId,callback){
        myUserCollection.update({companyId:companyId},{$set:{corporateAdmin:false}},{multi:true},function(error,result){
            myUserCollection.update({_id:obj[0].adminId,companyId:companyId},{$set:{corporateAdmin:true}},function(error,result){
                companyCollection.update({_id:companyId},{$set:{admin:obj}},function(errorC,resultC){
                    if(errorC){
                        loggerError.info('Error occurred in company model class updateCompanyAdmin() companyCollection ',error);
                    }
                    if(error){
                        loggerError.info('Error occurred in company model class updateCompanyAdmin() myUserCollection ',error);
                    }
                    callback(result && result.ok);
                })
            });
        })
    }

}

CorporateCompanyManagement.prototype.findCompanyByUrl = findCompanyByUrl;

function findOrCreateCompanyByUrlPrivate(companyInfo,projection,callback){
    
    companyCollection.findOne({url:companyInfo.url},projection,function(error,company){
        if(error){
            loggerError.info('An error occurred in findOrCreateCompany() '+JSON.stringify(error));
            callback(false);
        }else if(checkRequired(company)){
            callback(company);
        }else{
            logger.info('Creating new company '+companyInfo.companyName);
            createCompany(companyInfo,callback);
        }
    })
}

function findCompanyByUrl(companyInfo,projection,callback) {

    companyCollection.findOne({url:companyInfo.url},projection,function(error,company) {
        if (error) {
            loggerError.info('An error occurred in findCompanyByUrl() ' + JSON.stringify(error));
            callback(false);
        } else (checkRequired(company))
        {
            callback(company);
        }
    });
}

function createCompany(company,callback){
    var companyObj = constructCorporateCompanyObject(company);
    companyObj.save(function(error,companyProfile){
        if(error || !checkRequired(companyProfile)){
            loggerError.info('An error occurred in createCompany() '+JSON.stringify(error));
            callback(false);
        }
        else{
            callback(companyProfile);
        }
    })
}

function constructCorporateCompanyObject(company){
    var companyObj = new companyCollection({
        url:company.url,
        companyName:company.companyName,
        createdDate:new Date()
    });

    return companyObj;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

CorporateCompanyManagement.prototype.getSupportTeam = function (companyId,callback) {
    companyCollection.findOne({_id:companyId},{supportTeam:1},function(err,company){
        company = JSON.parse(JSON.stringify(company))
        if(!err && company.supportTeam.length>0){

            callback(err,company.supportTeam)
        } else {
            callback(err,[])
        }
    })
}

CorporateCompanyManagement.prototype.setTitlesForWidget = function (companyId,updateObj,callback) {
    companyCollection.update({
            _id:companyId
        },
        {
            $set: updateObj
        },
        function(error,result){
            if(error){
                loggerError.info('An error occurred while updating company info '+JSON.stringify(error))

                callback(error,[])
            } else {
                callback(error,result);
            }
        })
}

CorporateCompanyManagement.prototype.addToSupportTeam = function (companyId,userId,callback) {

    companyCollection.update({_id:companyId},{ $addToSet: { supportTeam: userId } },function(err,result){
        if(!err && result){
            callback(err,true)
        } else {
            callback(err,false)
        }
    })
}

CorporateCompanyManagement.prototype.removeFromSupportTeam = function (companyId,userId,callback) {

    companyCollection.update({_id:companyId},{ $pull: { supportTeam: userId } },function(err,result){
        if(!err && result){
            callback(err,true)
        } else {
            callback(err,false)
        }
    })
}

CorporateCompanyManagement.prototype.setFyMonth = function (companyId,fyMonth,callback) {

    companyCollection.update({_id:companyId},{ $set: { fyMonth: fyMonth } },{upsert:true},function(err,result){
        if(!err && result){
            updatePortfolioFy(companyId,fyMonth);
            callback(err,true)
        } else {
            callback(err,false)
        }
    });
}

CorporateCompanyManagement.prototype.updatePortfolioFy = function (companyId,fyRange){

    // portfolioAccess.find({companyId: companyId,"targets."}, function(error,result){
    //     if(error){
    //         loggerError.info('updatePortfolioFy '+JSON.stringify(error))
    //         callback(error,null)
    //     } else {
    //         callback(error,result);
    //     }
    // })
}

CorporateCompanyManagement.prototype.setCommitWeekday = function (companyId,commitDay,commitHour,callback) {

    companyCollection.update({_id:companyId},{ $set: { commitDay: commitDay,commitHour: commitHour } },{upsert:true},function(err,result){

        var key = "oppStages"+String(companyId);
        var key2 = "commitDayHour"+String(companyId);

        redisClient.del(key);
        redisClient.del(key2);

        if(!err && result){
            callback(err,true)
        } else {
            callback(err,false)
        }
    });

}

CorporateCompanyManagement.prototype.setDocumentNumber = function (companyId,docNumber,callback) {

    companyCollection.update({_id:companyId},{ $set: { documentNumber: docNumber} },{upsert:true},function(err,result){
        if(!err && result){
            callback(err,true)
        } else {
            callback(err,false)
        }
    });
}

CorporateCompanyManagement.prototype.getDocumentNumber = function (companyId,callback) {

    companyCollection.findOne({_id:companyId},{documentNumber:1},function(err,result){
        if(!err && result){
            callback(err,result)
        } else {
            callback(err,false)
        }
    })
}

CorporateCompanyManagement.prototype.getOrgHead = function (companyId,callback) {

    myUserCollection.find({companyId:companyId,orgHead:true},{ emailId:1,_id:1 },function(err,result){
        if(!err && result){
            callback(err,result)
        } else {
            callback(err,false)
        }
    })
}

CorporateCompanyManagement.prototype.updatePrimaryCurrency = function (companyId,updateObj,castToObjectId,callback) {

    var bulk = companyCollection.collection.initializeUnorderedBulkOp();
    var bulk2 = companyCollection.collection.initializeUnorderedBulkOp();

    _.each(updateObj,function (el) {

        if(!el.isPrimary){
            // bulk2.find({ _id: companyId, "currency._id": castToObjectId(el._id), "currency.isPrimary": true }).update({ $set: { "currency.$.xr": 0 } });
            bulk2.find({ _id: companyId, "currency._id": castToObjectId(el._id) }).update({ $set: { "currency.$.xr": 0 } });
        }

        if(el.isPrimary){
            bulk.find({ _id: companyId, "currency._id": castToObjectId(el._id) }).update({ $set: { "currency.$.isPrimary": true,"currency.$.xr": 1 } });
        } else {
            bulk.find({ _id: companyId, "currency._id": castToObjectId(el._id) }).update({ $set: { "currency.$.isPrimary": false } });
        }

        if(el.isDeactivated && !el.isPrimary){
            bulk.find({ _id: companyId, "currency._id": castToObjectId(el._id) }).update({ $set: { "currency.$.isDeactivated": true } });
        } else {
            bulk.find({ _id: companyId, "currency._id": castToObjectId(el._id) }).update({ $set: { "currency.$.isDeactivated": false } });
        }
    });

    bulk2.execute(function(err, result) {
        bulk.execute(function(err, result) {
            if (err) {
                logger.info('Error in updatePrimaryCurrency(): Bulk', err)
            } else {
                result = result.toJSON();
                logger.info('updatePrimaryCurrency() results ', result);
            }

            if (callback) { callback(err, result) }
        });
    });
}

CorporateCompanyManagement.prototype.updateDefaultType = function (companyId,updateObj,castToObjectId,callback) {

    var bulk = companyCollection.collection.initializeUnorderedBulkOp();

    _.each(updateObj,function (el) {

        if(el.isDefaultRenewalType){
            bulk.find({ _id: companyId, "typeList._id": castToObjectId(el._id) }).update({ $set: { "typeList.$.isDefaultRenewalType": true } });
        } else {
            bulk.find({ _id: companyId, "typeList._id": castToObjectId(el._id) }).update({ $set: { "typeList.$.isDefaultRenewalType": false } });
        }

    });

    bulk.execute(function(err, result) {
        if (err) {
            logger.info('Error in updateDefaultType(): Bulk', err)
        } else {
            result = result.toJSON();
            logger.info('updateDefaultType() results ', result);
        }

        if (callback) { callback(err, result) }
    });
}

CorporateCompanyManagement.prototype.updateCustomerSettings = function (companyId,updateObj,callback) {

    var bulk = companyCollection.collection.initializeUnorderedBulkOp();

    _.each(updateObj,function (el) {
        bulk.find({ _id: companyId, "customerSetting.name": el.name}).upsert().update({ $set: {
            "customerSetting.$.mandatory": el.mandatory,
            "customerSetting.$.name": el.name
        } });
    });

    bulk.execute(function(err, result) {
        if (err) {
            logger.info('Error in updateCustomerSettings(): Bulk', err)
        } else {
            result = result.toJSON();
            logger.info('updateCustomerSettings() results ', result);
        }

        if (callback) { callback(err, result) }
    });
}

CorporateCompanyManagement.prototype.setRenewalType = function (companyId,updateObj,castToObjectId,callback) {

    var bulk = companyCollection.collection.initializeUnorderedBulkOp();

    _.each(updateObj,function (el) {
        bulk.find({ _id: companyId, "typeList._id": castToObjectId(el._id) }).update({ $set: { "typeList.$.isTypeRenewal": el.isTypeRenewal } });

        if(!el.isTypeRenewal){
            bulk.find({ _id: companyId, "typeList._id": castToObjectId(el._id) }).update({ $set: { "typeList.$.isDefaultRenewalType": false } });
        }
    });

    bulk.execute(function(err, result) {
        if (err) {
            logger.info('Error in setRenewalType(): Bulk', err)
        } else {
            result = result.toJSON();
            logger.info('setRenewalType() results ', result);
        }

        if (callback) { callback(err, result) }
    });
}

CorporateCompanyManagement.prototype.updateXrCurrency = function (companyId,updateObj,castToObjectId,callback) {

    var bulk = companyCollection.collection.initializeUnorderedBulkOp();

    _.each(updateObj,function (el) {
        bulk.find({ _id: companyId, "currency._id": castToObjectId(el._id) }).update({ $set: { "currency.$.xr": parseFloat(el.xr) } });
    });

    bulk.execute(function(err, result) {
        if (err) {
            logger.info('Error in updateXrCurrency():Company Bulk', err)
        } else {
            result = result.toJSON();
            logger.info('updateXrCurrency() results ', result);
        }
        var key = "oppStages"+String(companyId);
        var key2 = "commitDayHour"+String(companyId);

        redisClient.del(key);
        redisClient.del(key2);

        if (callback) { callback(err, result) }
    });
}

CorporateCompanyManagement.prototype.deleteCurrency = function (companyId,symbol,castToObjectId,callback) {

    companyCollection.update(
        {
            _id:companyId
        },
        {
            $pull: { currency:{symbol:symbol} }
        },
        function(error,result){
            if(error){
                loggerError.info('An error occurred while updating deleteCurrency info '+JSON.stringify(error))
                callback(error,[])
            } else {
                callback(error,result);
            }
        })
}

CorporateCompanyManagement.prototype.updatePortfolio = function (companyId,data,castToObjectId,callback) {

    _.each(data.content.targets,function (co) {
        co._id = castToObjectId(co._id);

        _.each(co.targets,function (tr) {
            tr._id = castToObjectId(tr._id);
            tr.fy.start = new Date(tr.fy.start)
            tr.fy.end = new Date(tr.fy.end)
            _.each(tr.values,function (va) {
                va.date = new Date(va.date);
                va._id = castToObjectId(va._id);
            })
        })
    })

    var updateObj = {
        $set:{
            geoLocations:data.content
        }
    };

    if(data.name === "Business Units") {
        updateObj = {
            $set:{
                businessUnits:data.content
            }
        }
    }

    if(data.name === "Products") {
        updateObj = {
            $set:{
                productList:data.content
            }
        }
    }

    if(data.name === "Verticals") {
        updateObj = {
            $set:{
                verticalList:data.content
            }
        }
    }

    var findQuery = {
        companyId:companyId,
        name:data.name
    };

    var updateQuery = {$set:{list:data.content}};

    if(data.fromPortfolios){
        portfoliosCollection.update(findQuery,updateQuery).exec(function (err,result) {
            callback(err,result);
        });
    } else {

        companyCollection.update({_id:companyId},updateObj, function(error,result){

                if(error){
                    loggerError.info('updatePortfolio '+JSON.stringify(error))
                    callback(error,null)
                } else {
                    callback(error,result);
                }
            })
    }

    if(data.namesToChange && data.namesToChange.length>0){
        async.forEach(data.namesToChange, function(el, callbackLocal) {
            portfolioAccess.update({companyId: companyId,name:el.old,type:el.type},{$set:{name:el.new}}, function(error,result){});
        },function(errors,results){
        })
    }

    if(data.deleteUsersWithPortfolio && data.deleteUsersWithPortfolio.length>0){
        async.forEach(data.deleteUsersWithPortfolio, function(el, callbackLocal) {
            portfolioAccess.remove({companyId: companyId,name:el.name,type:el.type}, function(error,result){});
        },function(errors,results){
        })
    }
}

CorporateCompanyManagement.prototype.createPortfolio = function (companyId,data,callback) {

    portfoliosCollection.collection.insert([data], function(error,result){
        if(error){
            loggerError.info('createPortfolio '+JSON.stringify(error))
            callback(error,null)
        } else {
            callback(error,result);
        }
    })
}

CorporateCompanyManagement.prototype.portfolioRequired = function (companyId,data,callback) {
    var findQuery = {
        companyId:companyId,
        name:data.name
    };

    var updateQuery = {$set:{required:data.required}};

    portfoliosCollection.update(findQuery,updateQuery).exec(function (err,result) {
        callback(err,result);
    });
}

CorporateCompanyManagement.prototype.deletePortfolio = function (companyId,data,callback) {

    var updateObj = {
        $set:{
            geoLocations:[]
        }
    };

    if(data.name === "Business Units") {
        updateObj = {
            $set:{
                businessUnits:[]
            }
        }
    }

    if(data.name === "Products") {
        updateObj = {
            $set:{
                productList:[]
            }
        }
    }

    if(data.name === "Verticals") {
        updateObj = {
            $set:{
                verticalList:[]
            }
        }
    }

    var findQuery = {
        companyId:companyId,
        name:data.name
    };

    if(data.fromPortfolios){
        portfoliosCollection.remove(findQuery).exec(function (err,result) {
            callback(err,result);
        });
    } else {
        companyCollection.update({_id:companyId},updateObj, function(error,result){
            if(error){
                loggerError.info('updatePortfolio '+JSON.stringify(error))
                callback(error,null)
            } else {
                callback(error,result);
            }
        })
    }
}

CorporateCompanyManagement.prototype.getAllPortfolios = function (companyId,callback) {

    portfoliosCollection.find({companyId: companyId}, function(error,result){
        if(error){
            loggerError.info('getAllPortfolios '+JSON.stringify(error))
            callback(error,null)
        } else {
            callback(error,result);
        }
    })
}

CorporateCompanyManagement.prototype.getAllPortfoliosByTypeAndName = function (companyId,name,type,callback) {

    portfolioAccess.find({companyId: companyId,name:name,type:type}, function(error,result){
        if(error){
            loggerError.info('getAllPortfoliosByTypeAndName '+JSON.stringify(error))
            callback(error,null)
        } else {
            callback(error,result);
        }
    })
}

CorporateCompanyManagement.prototype.getAllPortfoliosTargetsByType = function (companyId,type,forHead,dateMin,dateMax,callback) {

    var match = {
        companyId: companyId,
        type:type,
        // isHead:true
    }

    if(!forHead){
        match = {
            companyId: companyId,
            type:type
        }
    }

    portfolioAccess.aggregate([
        {
            $match: match
        },
        {
            $unwind:"$targets"
        },
        // {
        //     $match:{
        //         "targets.fy.start":{$gte:new Date(dateMin)},
        //         "targets.fy.end":{$lte:new Date(dateMax)},
        //     }
        // },
        {

            $group:{
                _id:{name:"$name",isHead:"$isHead"},
                targets:{
                    $push:{
                        values:"$targets.values"
                    }
                }

            }
        }
    ], function(error,result){
        if(error){
            loggerError.info('getAllPortfoliosTargetsByType '+JSON.stringify(error))
            callback(error,null)
        } else {
            var data = {};
            _.each(result,function(re){
                var totalAmt = 0;
                var headAmt = 0;
                _.each(re.targets,function(tr){
                    _.each(tr.values,function(va){
                        if(re._id.isHead){
                            headAmt = headAmt+va.amount;
                        };

                        totalAmt = totalAmt+va.amount;
                    });
                });

                if(data[re._id.name]){
                    data[re._id.name].head = data[re._id.name].head+headAmt
                    data[re._id.name].all = data[re._id.name].all+totalAmt
                } else {
                    data[re._id.name] = {
                        head:headAmt,
                        all:totalAmt,
                    }
                }
            });

           callback(error,data);
            // callback(error,result);
        }
    })
}

CorporateCompanyManagement.prototype.deletePortfoliosByUser = function (companyId,data,callback) {

    portfolioAccess.remove({companyId: companyId,ownerEmailId:data.ownerEmailId,name:data.name,type:data.type}, function(error,result){
        if(error){
            loggerError.info('deletePortfoliosByUser '+JSON.stringify(error))
            callback(error,null)
        } else {
            callback(error,result);
        }
    })
}

CorporateCompanyManagement.prototype.savePortfoliosForUsers = function (companyId,data,callback) {

    var bulk = portfolioAccess.collection.initializeUnorderedBulkOp();

    _.each(data,function (el) {
        bulk.find({ companyId: companyId, ownerEmailId:el.ownerEmailId, type:el.type, name:el.name })
            .upsert()
            .updateOne({
                $set: {
                    companyId: companyId,
                    ownerEmailId: el.ownerEmailId,
                    isHead: el.isHead,
                    name: el.name,
                    targets: el.targets,
                    type: el.type,
                    accessLevel: el.accessLevel
                }
            });
    });

    bulk.execute(function(err, result) {
        callback(err,result)
    });
}

module.exports = CorporateCompanyManagement;
