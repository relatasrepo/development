var corporateCollection = require('../../databaseSchema/corporateCollectionSchema').corporateModel;
var corporatelogoCollection = require('../../databaseSchema/corporateCollectionSchema').corporatelogoModel;
var companyCollection = require('../../databaseSchema/corporateCollectionSchema').companyModel;
var userCollection = require('../../databaseSchema/userManagementSchema').User;
var commonUtility = require('../../common/commonUtility');
var winstonLog = require('../../common/winstonLog');

var common = new commonUtility();
var logLib = new winstonLog();
var loggerError = logLib.getCorporateErrorLogger();
var logger = logLib.getCorporateLogger();
var fs = require("fs")
var https = require('https')
function corporateUserManagement()
{
  this.saveCompanyLogo = function(companyUrl, callBack){
    corporatelogoCollection.findOne({companyUrl: companyUrl}, function(err, data){
      if(!data){
        https.get('https://logo.clearbit.com/' + companyUrl, function(res){
          var imagedata = ''
          res.setEncoding('binary')

          res.on('data', function(chunk){
            imagedata += chunk
          })

          res.on('end', function(){
            if(imagedata != '')
              fs.writeFile(__dirname + "/../../public/images/companylogos/"+ companyUrl.split(".")[0] + '.png', imagedata, 'binary', function(err){
                if (err) throw err
                var corporateLogo = new corporatelogoCollection({
                  companyUrl: companyUrl,
                  logoUrl: "/images/companylogos/"+ companyUrl.split(".")[0] + '.png'
                })
                corporateLogo.save(function(err, data){
                  if(err)
                    throw err
                })
              })
            else{
              var corporateLogo = new corporatelogoCollection({
                companyUrl: companyUrl,
                logoUrl: ""
              })
              corporateLogo.save(function(err, data){
                if(err)
                  throw err
              })
            }
          })
        })
      }
      callBack()
    })
  }
  this.checkCorporateUserExist = function(userDetails,callback){

       var query = {};

          query.emailId = userDetails.emailId;


       findCorporateUser(query,function(profile){
           if(profile){

               callback('exist')
           }
           else{
               createPartialCorporateAcc(userDetails,callback);
           }
       })
  };

    function createPartialCorporateAcc(profile,callback){
        var corporateProfile = constructPartialCorporateAccount(profile);

        corporateProfile.save(function(error,saved){
            if(error){

                callback(false);
            }
            else{

                callback(saved);
            }
        })
    }

    this.findOrCreateCorporateUser = function(userDetails,callback){
        var query = {};
        if(common.checkRequired(userDetails.userId)){
            query.userId = userDetails.userId;
        }else if(common.checkRequired(userDetails.emailId)){
            query.emailId = userDetails.emailId;
        }

        findCorporateUser(query,function(profile){
            if(profile){
                callback(profile)
            }else{
                saveCorporateUser(userDetails,callback);
            }
        })
    };

    this.getUsersByCompany = function(companyId,callback){
        corporateCollection.find({companyId:companyId},function(error,members){
            if(error){
                loggerError.info('an error occurred in getUsersByCompany() in corporateModelClass '+JSON.stringify(error));
            }
             callback(members)
        });
    };

    this.getUserHierarchy = function(userId,callback){
        var criteria = ','+userId+',';

        corporateCollection.find({$or:[{userId:userId},{path:{ $regex: criteria, $options: 'i' }}]}).sort({path:1}).exec(function(error,list){
            if(error){
                loggerError.info('Mongo error in getUserHierarchy() 1 in corporateModelClass '+JSON.stringify(error));
            }

            callback(list);
        })
    };

    this.updatePartialAccount = function(profile,callback){

        corporateCollection.update({emailId:profile.emailId},{$set:profile},function(error,result){

             if(error || result == 0){
                callback(false)
             }
             else{
                callback(true);
             }
        })
    };

    this.updateCorporateAcc = function(profile,callback){

        corporateCollection.update({userId:profile.userId},{$set:{emailId:profile.emailId,firstName:profile.firstName,lastName:profile.lastName}},function(error,result){

            if(error ){
               loggerError.info('Mongo error while updating corp user '+JSON.stringify(error))
            }
        })
    };

    this.getAllCompaniesAdmins = function(callback){
        companyCollection.find({},{companyName:1,admin:1}).populate('admin.adminId', '_id firstName lastName emailId')
            .exec(function(error,companies){

            })
    };



    this.getAllUsersAndCompanies = function(callback){
        userCollection.aggregate([
            {
                $match:{
                    corporateUser:true,
                    userType:'registered',
                    companyId:{$exists:true}
                }
            },
            {
                $group:{
                    _id:"$companyId",
                    users:{
                        $addToSet:{
                            _id:"$_id",
                            userId:"$userId",
                            emailId:"$emailId",
                            firstName:"$firstName",
                            lastName:"$lastName"
                        }
                    }
                }
            }
        ]).exec(function(er,users){
            if(!er && common.checkRequired(users) && users.length > 0){
                companyCollection.populate(users,{ path: '_id'},function(eror,data){
                    callback(data);
                });

            }else callback([])
        })
    };
}

function saveCorporateUser(userDetails,callback){
    var userObj = constructCorporateUserObject(userDetails);
    userObj.save(function(erorr,profile){
        if(erorr){
            callback(false);
        }else callback(profile);
    })
}

function findCorporateUser(query,callback){
    corporateCollection.findOne(query,function(error,profile){
        if(error){
            callback(false);
        }else callback(profile);
    });
}

function constructCorporateUserObject(corporateUser){

    var corporateUserObj = new corporateCollection({
        userId : corporateUser.userId,
        companyId:corporateUser.companyId,
        emailId:corporateUser.emailId,
        active:true,
        keySent:false,
        createdDate:new Date()
    });

    if(common.checkRequired(corporateUser.firstName))
        corporateUserObj.firstName = corporateUser.firstName;

    if(common.checkRequired(corporateUser.lastName))
        corporateUserObj.lastName = corporateUser.lastName;

    return corporateUserObj;
}

function constructPartialCorporateAccount(corporateUser){
    var corporateUserObj = new corporateCollection({
        companyId:corporateUser.companyId,
        emailId:corporateUser.emailId,
        firstName : corporateUser.firstName,
        lastName : corporateUser.lastName,
        key:corporateUser.token,
        active:false,
        keySent:true,
        createdDate:new Date()
    });
    return corporateUserObj;
}


module.exports = corporateUserManagement;