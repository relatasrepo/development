
var errorMessages = require('../errors/errorMessage');
var errorMsg = new errorMessages();

function ErrorClass(){

    this.generateErrorResponse = function(status,message){
        return {
            "SuccessCode": 0,
            "Message":message || errorMsg.getErrorMsg(status),
            "ErrorCode":status.status,
            "Error":status.key,
            "Data": {}
        }
    }
}

module.exports = ErrorClass;