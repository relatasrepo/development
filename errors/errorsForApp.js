
var errorMessages = require('../errors/errorMessage');
var errorMsg = new errorMessages();

function ErrorClass(){
}

ErrorClass.prototype.getErrorsForOpportunity = function () {
    var required = " is required";

    return {
        opportunityName:"Opportunity name"+required,
        closeDate:"Close date"+required,
        renewalCloseDate:"Renewal close date"+required,
        contactEmailId:"A valid primary contact email Id"+required,
        relatasStage:"Stage selection"+required,
        stageName:"Stage selection"+required,
        stage:"Stage selection"+required,
        geoLocation:"City selection"+required,
        amount:"Please enter a <br> valid number for amount",
        renewalAmount:"Please enter a <br> valid number for amount",
        netGrossMargin:"Please enter a <br> valid number for margin",
        type:"Type selection"+required,
        vertical:"Vertical selection"+required,
        closeReasons:"Close reasons"+required,
        currency:"Currency selection"+required,
        sourceType:"Source selection"+required,
        productType:"Product selection"+required,
        solution:"Solution selection"+required,
        accounts:"Account selection"+required,
        businessUnit:"Business unit selection"+required,
    }
}

module.exports = ErrorClass;