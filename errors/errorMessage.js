function ErrorMessage() {

    var mandatoryFieldError = {
        EMAIL_NOT_FOUND: 'Email address not found',
        INVALID_EMAIL_ID: 'Invalid Email address. Please enter valid email address',
        UNIQUE_NAME_NOT_FOUND: 'Please enter valid unique name',
        INVALID_UNIQUE_NAME: 'Please enter valid unique name',
        FIRST_NAME_NOT_FOUND: 'Enter valid first name',
        LAST_NAME_NOT_FOUND: 'Enter valid last name',
        PHONE_NUMBER_NOT_FOUND: 'Enter valid phone number',
        INVALID_PHONE_NUMBER: 'Invalid phone number',
        GOOGLE_ID_NOT_FOUND: 'Please enter valid Google account',
        GOOGLE_ACCOUNT_NOT_FOUND: 'Google account not found',
        GOOGLE_ACCOUNT_TOKEN_NOT_FOUND: 'Google account token not found',
        GOOGLE_ACCOUNT_REFRESH_TOKEN_NOT_FOUND: 'Google account refresh token not found',
        GOOGLE_ACCOUNT_ID_NOT_FOUND: 'Google account not found',
        GOOGLE_ACCOUNT_EMAIL_ID_NOT_FOUND: 'Google account email id not found',

        /* CREATE MEETING MANDATORY FIELDS */

        MEETING_INFO_NOT_FOUND:'',
        MEETING_TITLE_NOT_FOUND:'',
        MEETING_LOCATION_TYPE_NOT_FOUND:'',
        MEETING_LOCATION_NOT_FOUND:'',
        MEETING_DESCRIPTION_NOT_FOUND:'',
        MEETING_START_DATE_MISSING:'',
        MEETING_END_DATE_MISSING:'',
        MEETING_TO_LIST_NOT_FOUND:'',
        MEETING_SENDER_EMAIL_ID_NOT_FOUND:'',
        MEETING_SENDER_NAME_NOT_FOUND:''
    };

    var badRequest = {
        NO_REQUEST_BODY: 'Request body is empty',
        TOKEN_NOT_FOUND: 'Authorization token not found',
        EMAIL_ID_NOT_FOUND_IN_REQUEST: 'Email address is not found in request body',
        GOOGLE_ID_NOT_FOUND_IN_REQUEST: 'Google account _id not found in request body',
        USER_ID_NOT_FOUND_IN_REQUEST: 'User address is not found in request body',
        SEARCH_TEXT_NOT_FOUND_IN_REQUEST: 'Search content not found in request body',
        UNIQUE_NAME_NOT_FOUND_IN_REQUEST: 'Unique Name not found in request body',
        INVALID_TOKEN: 'Invalid authorization token',
        INVALID_HISTORY_TYPE:'Unknown history type',
        NO_HISTORY_FOUND:'History data not available',
        QUERY_PARAMETER_MISSED:'Required query parameter missed',
        NO_CONTACT_ID_FOUND:'Contact address not found in request body',
        NO_CONTACT_FOUND:'Contact could not found',
        INVALID_DATA_RECEIVED:'Invalid data received',
        NO_CONTACT_ID_OR_NOTE_TEXT_FOUND:'Either Contact address or Note Text is missing',
        INVALID_CONTACT_TYPE:'Invalid contact filter type in request',
        INVALID_TYPE:'Invalid type in request',
        INVALID_ACTION:'Invalid action type in request',
        INVALID_URL_PARAMETER:'Invalid parameters received in URL',
        COMPANY_NAME_NOT_EXISTS:'Company name not found in the request',

        DATE_TIME_MISSED_IN_REQUEST_BODY:"Date/Time not found in the request",

        TASK_DETAILS_NOT_FOUND:'Required task details not found',
        TASK_ASSIGNED_TO_NOT_FOUND:'Either assignedTo or assignedToEmailId missed ',
        TASK_NAME_NOT_FOUND:'Task name not found',
        TASK_ASSIGNED_TO_ID_NOT_FOUND:'Task Assigned to address not found',
        TASK_ASSIGNED_TO_EMAIL_ID_NOT_FOUND:'Task Assigned to user email address not found',
        TASK_DUE_DATE_NOT_FOUND:'Task Due Date not found',
        INVALID_EMAIL_ID:'Invalid email address found',
        'TASK_FOR_NOT_FOUND [meeting or call or other]':'TaskFor Either meeting or call or other not found',
        'TASK_REF_ID_NOT_FOUND [invitationId or call id]':'Reference id not found either meeting invitation id or call id',

        PHONE_NUMBER_NOT_FOUND: 'Phone Number not found',

        SLOT_ID_OR_INVITATION_ID_MISSED:'Either SlotId or InvitationId details are not provided',

        INVITATION_ID_NOT_FOUND:'InvitationId not found in the request',
        NEW_DATES_AND_LOCATIONS_SLOT_NOT_FOUND: 'New time and location details not found in the request',
        NEW_START_DATE_TIME_NOT_FOUND: 'New start date time not found in the request',
        MEETING_NEW_SUGGESTED_LOCATION_NOT_FOUND: 'New suggested location is not available',
        MEETING_NEW_SUGGESTED_LOCATION_TYPE_NOT_FOUND: 'New suggested location type not available',

        TASK_ID_OR_STATUS_NOT_FOUND: 'Either taskId or status missed in the request body',
        INVALID_STATUS: 'Invalid status received',
        INVALID_OTP_RECEIVED: 'Invalid OTP received',
        NO_FILES_RECEIVED: 'No files received',
        INVALID_IMAGE_FILE_TYPE: 'Received file is not an image type',

        LINKEDIN_DETAILS_NOT_FOUND:'LinkedIn account details not found',
        LINKEDIN_ID_NOT_FOUND:'LinkedIn address not found',
        LINKEDIN_TOKEN_NOT_FOUND:'LinkedIn token not found',
        LINKEDIN_EMAIL_ID_NOT_FOUND:'LinkedIn user email address not found',
        LINKEDIN_NAME_NOT_FOUND:'LinkedIn user name not found',

        FACEBOOK_DETAILS_NOT_FOUND:'Facebook account details not found',
        FACEBOOK_ID_NOT_FOUND:'Facebook id not found',
        FACEBOOK_TOKEN_NOT_FOUND:'Facebook token not found',
        FACEBOOK_EMAIL_ID_NOT_FOUND:'Facebook user email id not found',
        FACEBOOK_NAME_NOT_FOUND:'Facebook user name not found',
        FACEBOOK_POST_ID_FOUND:'Facebook post id not found',
        FACEBOOK_COMMENT_FOUND:'Facebook comment not found',

        TWITTER_DETAILS_NOT_FOUND:'Twitter account details not found',
        TWITTER_ID_NOT_FOUND:'Twitter id not found',
        TWITTER_TOKEN_NOT_FOUND:'Twitter token not found',
        TWITTER_REFRESH_TOKEN_NOT_FOUND:'Twitter refresh Token not found',
        TWITTER_DISPLAY_NAME_NOT_FOUND:'Twitter display name not found',
        TWITTER_USERNAME_NOT_FOUND:'Twitter userName not found',
        TWEET_ID_NOT_FOUND:'Twitter tweet id not found',
        TWEET_STATUS_NOT_FOUND:'Twitter tweet status not found',

        DOCUMENT_ID_NOT_FOUND:'Document ID is missed in the request',
        DOCUMENT_SHARE_LIST_NOT_FOUND:'Share with list not found in the request',
        INVALID_SHARE_IN_TYPE:'Invalid shareIn type in request',

        RECIPIENTS_NOT_FOUND:'Recipients list not found in request',
        INVALID_RECIPIENTS:'Invalid recipients list in request',
        START_END_DATES_MISSED:'Start and End dates are missed',

        INTERACTION_ID_NOT_FOUND_IN_REQUEST:'Interaction id not found in the request.',
        EMAIL_CONTENT_ID_NOT_FOUND_IN_REQUEST:'Id of email not found in requesr'
    };

    var profileErrors = {
        ERROR_SAVING_PROFILE: 'Error while saving user profile',
        ERROR_UPDATE_PROFILE: 'Error while updating user profile',
        USER_ALREADY_EXISTS: 'User profile already exists in db',
        UNIQUE_NAME_ALREADY_EXISTS: 'Unique name already exists in the db',
        UNIQUE_NAME_NOT_EXISTS: 'Unique name not exists in the db',
        ERROR_FETCHING_PROFILE: 'Error fetching user profile',
        LINKEDIN_ERROR: 'Error Fetching Data',
        TWITTER_ERROR: 'Error Fetching Data',
        NO_LINKEDIN_ACCOUNT: 'Linkedin Account not found',
        NO_TWITTER_ACCOUNT: 'Twitter Account not found',
        NO_FACEBOOK_ACCOUNT: 'Facebook Account not found',
        NO_GOOGLE_ACCOUNTS: 'Google Accounts not found',
        USER_PROFILE_NOT_FOUND: 'No profile found for your search',
        FAILED_TO_UPDATE_PROFILE_PIC: 'An error occurred while updating profile pic',
        COMPANY_NAME_NOT_EXISTS: 'There is no company name exist in profile',
        MOBILE_NUMBER_NOT_FOUND:'Mobile number not found in profile',
        ERROR_IN_SEARCH:'Error occurred while searching',
        ERROR_IN_FIND_PATH:'Error occurred while finding best path',
        USER_ALREADY_CONNECTED:'User already connected',
        NO_PATH_FOUND:'No best path found',
        EITHER_GOOGLE_OR_OFFICE_ACCOUNT_NOT_FOUND:'Either google or office account not found in user profile'
    };

    var fileError = {
        FILE_UPLOAD_ERROR:'Error occurred while uploading file',
        AWS_UPLOAD_ERROR:'Error occurred while uploading file to AWS'
    };

    var someThingWrong = {
        SOMETHING_WENT_WRONG: 'Something went wrong',
        UPDATE_DB_ERROR:'Error occurred In database',
        SAVE_DOCUMENT_ERROR:'Error occurred while storing document in database',
        CREATE_GOOGLE_EVENT_FAILED:'Creating Google event failed',
        UPDATE_GOOGLE_EVENT_FAILED:'Updating Google event failed',
        ACCEPT_MEETING_FAILED:'Accept meeting failed',
        MEETING_ALREADY_ACCEPTED:'Meeting was already accepted',
        MEETING_ALREADY_CANCELED:'Meeting was already canceled',
        INVALID_USER_TO_ACCEPT:'User is not the right person to accept meeting (Either Sender or Suggested User)',
        FAILED_TO_FETCH_MEETING_DETAILS:'Failed to fetch meeting data',
        UPDATE_MEETING_FAILED:'Failed to update meeting as confirmed',
        GENERATING_ICS_FAILED:'Failed to generate ICS file',
        NO_LOCATION_DETAILS_FOUND:'No location details found to get weather report',
        INTERACTIONS_NOT_FOUND:'No interactions found in the past',

        FACEBOOK_ERROR_LIKE:'Failed to like facebook post',
        FACEBOOK_ERROR_COMMENT:'Failed to comment facebook post',
        FACEBOOK_ERROR_SHARE:'Failed to share facebook post',

        TWITTER_ERROR_FAVORITE:'Failed to favorite twitter tweet',
        TWITTER_ERROR_REPLAY:'Failed to replay twitter tweet',
        TWITTER_ERROR_RE_TWEET:'Failed to re-tweet twitter tweet',

        DOCUMENT_NOT_FOUND:'Document not found',
        SHARING_DOCUMENT_FAILED:'Sharing doc filed',
        UPDATE_MEETING_DOC_FAILED:'Updating meeting with doc failed',
        ROUTE_DOES_NOT_EXIST: 'Accessed route does not exist'
    };

    var unAuthorisedError = {
        UN_AUTHORISED_ACCESS:'Invalid Access',
        LOGIN_REQUIRED : 'Access to resource needs login'
    };
    var COMMON_MESSAGE = 'Please try again. If problem persists, please contact hello@relatas.com.';
    var messages = {
        // Schedule Page Messages
        NO_COMMON_CONNECTIONS_FOUND:'No common connections found',
        NO_LINKEDIN_INFO_FOUND:'No common connections found',
        LINKEDIN_ERROR:'Linkedin information is not available. '+COMMON_MESSAGE,
        NO_TWITTER_INFO_FOUND:'Twitter information is not available. '+COMMON_MESSAGE,
        TWITTER_ERROR:'Twitter information is not available. '+COMMON_MESSAGE,
        CALENDAR_PASSWORD_REQUEST_SENT:'Calendar password request has been sent',
        CALENDAR_PASSWORD_REQUEST_FAILED:'Calendar password request failed.'+COMMON_MESSAGE,
        GOOGLE_AUTHENTICATION_FAILED:'User authentication failed. '+COMMON_MESSAGE,
        INVALID_ACTION_SPECIFIED:'Server error. '+COMMON_MESSAGE,
        MEETING_INVITATION_FAILED:'Meeting invitation was not delivered. '+COMMON_MESSAGE,
        MEETING_INVITATION_SUCCESS:'Meeting invitation sent',
        // Today Landing Page Messages
        NEW_INVITATIONS_NOT_FOUND:'There are no new invitations',
        CONFIRMED_MEETINGS_NOT_FOUND:'No upcoming meetings scheduled for today',
        UPCOMING_MEETINGS_NOT_FOUND:'No upcoming meetings scheduled',
        NO_TASKS_FOUND:'Click on Add to assign tasks',
        //AGENDA_NOT_FOUND:'There is no agenda on your calendar',
        AGENDA_NOT_FOUND:'',
        NO_CONNECTIONS_FOUND_BY_LOCATION:'You have no connection for the specified search',
        NO_CONNECTIONS_FOUND_BY_COMPANY_WITH_USER:'No person from your company has interacted with this contact',
        LOCATION_IS_NOT_VALID:'Location could not be identified. Please use location search to find contacts.',
        // Today Details page messages
        ACCEPTING_MEETING_FAILED:'Meeting cannot be accepted. '+COMMON_MESSAGE,
        ACCEPTING_MEETING_FAILED_AUTHORIZATION:'Meeting cannot be accepted. You are not the authorised person to accept the meeting.',
        ACCEPTING_MEETING_SUCCESS:'Meeting accepted',
        RESCHEDULE_MEETING_FAILED:'Meeting cannot be rescheduled. '+COMMON_MESSAGE,
        RESCHEDULE_MEETING_SUCCESS:'Meeting has been rescheduled',
        CANCEL_MEETING_FAILED:'Meeting cannot be cancelled. '+COMMON_MESSAGE,
        CANCEL_MEETING_SUCCESS:'Meeting cancelled',
        MEETING_DELETED_CANCELED:'Meeting has been either deleted or cancelled.',
        UPDATE_CONTACT_RELATION_FAILED:'Contact relationship is not updated. '+COMMON_MESSAGE,
        UPDATE_CONTACT_FAVORITE_FAILED:'Contact favorite is not updated. '+COMMON_MESSAGE,
        UPDATE_CONTACT_DETAILS_FAILED:'Contact detials not updated. '+COMMON_MESSAGE,
        SEND_EMAIL_FAILED:'Email not sent. '+COMMON_MESSAGE,
        SEND_EMAIL_SUCCESS:'Email sent',
        TASK_STATUS_UPDATE_FAILED:'Task status cannot be changed. '+COMMON_MESSAGE,
        TASK_STATUS_UPDATE_SUCCESS:'Task status has been changed',
        TASK_MARKED_COMPLETED:'Task is marked as completed',
        TASK_MARKED_OPEN:'Task is marked as open',
        SOCIAL_FEED_NOT_AVAILABLE:'Social feeds are not available',
        // Dashboard page messages
        NO_SOCIAL_MEDIA_ACTIVITY_TODAY:'Pl. add more contacts to favorites',
        NO_FAVORITE_CONTACTS:'Please setup favorite contacts',
        NO_CONNECTIONS_FOUND_FOR_LOSING_TOUCH:'You are in touch with all your connections',
        NO_CONNECTIONS_FOUND_TOP_FIVE:'You have not interacted with your connections',
        NO_INTERACTIONS_FOUND_PAST_SEVEN_DAY:'There are no interactions',
        // Interactions page messages
        INTERACTION_IGNORE_UPDATE_FAILED:'Interaction ignore failed. '+COMMON_MESSAGE,
        INTERACTION_IGNORE_UPDATE_SUCCESS:'Interaction ignore success. ',
        CALENDAR_PASSWORD_APPROVE_FAILED:'Action is unsuccessful. '+COMMON_MESSAGE,
        TWITTER_ERROR_FAVORITE:'Twitter favorite cannot be updated. '+COMMON_MESSAGE,
        TWITTER_ERROR_REPLAY:'Twitter reply failed. '+COMMON_MESSAGE,
        TWITTER_ERROR_RE_TWEET:'Twitter re-tweet failed. '+COMMON_MESSAGE,
        TWITTER_FAVORITE_SUCCESS:'Twitter favorite has been updated',
        TWITTER_REPLAY_SUCCESS:'Twitter reply has been updated',
        TWITTER_RE_TWEET_SUCCESS:'Twitter re-tweet success',
        TASK_CREATED:'Task assigned',
        TASK_CREATE_FAILED:'Task not created. '+COMMON_MESSAGE,
        GET_EMAIL_BODY_FAILED:"Unable to fetch email content. "+COMMON_MESSAGE,
        // Settings page Messages
        REMOVE_LINKEDIN_ACCOUNT_FAILED:'LinkedIn account not removed. '+COMMON_MESSAGE,
        REMOVE_FACEBOOK_ACCOUNT_FAILED:'Facebook account not removed. '+COMMON_MESSAGE,
        REMOVE_TWITTER_ACCOUNT_FAILED:'Twitter account not removed. '+COMMON_MESSAGE,
        REMOVE_GOOGLE_ACCOUNT_FAILED:'Google account not removed. '+COMMON_MESSAGE,
        UPDATE_PROFILE_FAILED:'Profile changes not saved. '+COMMON_MESSAGE,
        UPDATE_PROFILE_SUCCESS:'Profile saved',
        UPDATE_PROFILE_PIC_SUCCESS:'Profile picture has been changed',
        UPDATE_PROFILE_PIC_FAILED:'Profile picture not updated. '+COMMON_MESSAGE,
        UPDATE_GOOGLE_PRIMARY_FAILED:'Google account primary not updated. '+COMMON_MESSAGE,
        UPLOAD_CONTACTS_SUCCESS:'Contacts successfully uploaded',
        UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED:'<span>Uploaded file format is not supported. Please click <a style="color: #000000" target="_blank" href="https://medium.com/relatas/mass-email-linkedin-contacts-in-5-steps/">here</a> for correct format.</span>',
        CSV_XLS_FILES_NOT_VALID:'<span>Please upload a valid csv or xlsx file. Please click <a style="color: #000000" target="_blank" href="https://medium.com/relatas/mass-email-linkedin-contacts-in-5-steps/">here</a> for correct format.</span>',
        // On boarding
        NO_ACCOUNTS_CONTACTS: 'Please add contacts to create customer accounts',
        NOT_CORPORATE_USER:''
    };

    this.getMessage = function(key,isPostMeetingMessage,uniqueName,userId){
        if(!isPostMeetingMessage){
            if(key != null)
                return messages[key]
            else return "";
        }
        else{
            var url = '/schedule/partialProfile/next/onboarding?userId='+userId;
            return 'Your partial Relatas profile with user name <strong>"'+uniqueName+'"</strong> has been created. To edit/complete the same please click <a style="text-decoration: underline" href="'+url+'" class="mPartialProfileComplete">here</a>';
        }
    };

    var statusCodes = {
        INVALID_REQUEST_CODE: 5000,
        MANDATORY_FIELD_ERROR_CODE: 4010,
        PROFILE_ERROR_CODE: 5001,
        UPDATE_FAILED:5003,
        SOMETHING_WENT_WRONG_CODE: 5002,
        AUTHORIZATION_ERROR_CODE: 5004,
        FILE_ERROR_CODE: 5005,
        MESSAGE:6000
    };

    this.getStatusCodes = function () {
        return statusCodes;
    };

    this.getErrorMsg = function (status) {
        if (status.status >= 4000 && status.status <= 4050) {
            return mandatoryFieldError[status.key];
        }
        else if (status.status == 5000 || status.status == 5003) {
            return badRequest[status.key];
        }
        else if (status.status == 5001) {
            return profileErrors[status.key];
        }
        else if (status.status == 5004) {
            return unAuthorisedError[status.key];
        }
        else if (status.status == 5005) {
            return fileError[status.key];
        }
        else if (status.status == 6000) {
            return messages[status.key];
        }
        else return someThingWrong[status.key];
    }
}

module.exports = ErrorMessage;

