var request = require('request');
var userManagement = require('../dataAccess/userManagementDataAccess');
var commonUtility = require('./commonUtility');
var emailSender = require('../public/javascripts/emailSender');
var moment = require('moment-timezone');

var emailSenders = new emailSender();
var common = new commonUtility();
var userManagements = new userManagement();

function IPLocator(){

}

IPLocator.prototype.getIP = function(req){
    // return getClientIp(req)
    return getClientIpFromReq(req)
};


/*
*Lookup response
*
 { as: 'AS24560 Bharti Airtel Ltd., Telemedia Services',
 city: 'Bangalore',
 country: 'India',
 countryCode: 'IN',
 isp: 'Airtel',
 lat: 12.9833,
 lon: 77.5833,
 org: 'Airtel',
 query: '122.171.117.133',
 region: 'KA',
 regionName: 'Karnataka',
 status: 'success',
 timezone: 'Asia/Kolkata',
 zip: '' }

 * */
IPLocator.prototype.lookup = function(IP,userId,callback){

    if(IP != null && IP != undefined && IP != ''){
        request('http://ip-api.com/json/'+IP, function (error, response, body) {
            if (!error && response && response.statusCode == 200 && common.checkRequired(body)) {

                    try{
                        body = JSON.parse(body);
                        if(body.status == 'success'){
                            if(common.checkRequired(userId)){
                                var  currentLocation = {
                                    country: body.country || '',
                                    city: body.city || '',
                                    region: body.region || '',
                                    latitude: body.lat || '',
                                    longitude: body.lon || '',
                                    timezone: body.timezone || ''
                                }
                                updateUserLocation(userId,currentLocation,callback);
                            }
                            else callback(body);
                        }else{
                            if(common.checkRequired(userId)){
                                updateUserLocation(userId,null,callback);
                            }
                            else callback(body);
                        }

                    }catch (e){
                        if(common.checkRequired(userId)){
                            updateUserLocation(userId,null,callback);
                        }
                        else callback(false);
                    }
            }else{
                if(common.checkRequired(userId)){
                    updateUserLocation(userId,null,callback);
                }
                else callback(false);
            }
        })
    }
    else {
        if(common.checkRequired(userId)){
            updateUserLocation(userId,null,callback);
        }
        else callback(false);
    }
};

function updateUserLocation(userId,location,callback){
    userManagements.updateCurrentLocation(userId,location,function(isSuccess){
        if(isSuccess){
            userManagements.findUserProfileByIdWithCustomFields(userId,{location:1},function(error,user){
                if(!error && user && common.checkRequired(user.location)){
                    request.get('http://maps.googleapis.com/maps/api/geocode/json?address='+user.location,function(err,res,data){
                        if(err){
                            common.updateContactMultiCollectionLevel(userId,false);
                            logger.info("Error Google location Search updateUserLocation():ipLocator ",err, 'search content ',user.location);
                            callback(isSuccess)
                        }
                        else {
                            try {
                                data = JSON.parse(data);
                            }
                            catch (e) {
                                logger.info("Exception Google location Search searchByAddress():ipLocator ", e, 'search content ', user.location);
                            }
                            if(data && data.results && data.results.length > 0 && data.results[0] && data.results[0].formatted_address) {

                                //Get user's timezone from IP

                                if(common.checkRequired(location.timezone)){
                                    var timezone = location.timezone;
                                    var timezoneAbbr = moment.tz(timezone).zoneName();
                                   var offset = moment().tz(timezone).format('Z');
                                }

                                var profileUpdateTimezone = {timezone:{updated:true,zone:offset,name:timezone}};

                                //Update user's timezone on signup
                                if(common.checkRequired(profileUpdateTimezone)){
                                    userManagements.updateProfileObject_new(userId,profileUpdateTimezone,function(err,res){
                                        common.updateContactMultiCollectionLevel(userId,false);
                                        callback(isSuccess)
                                    });
                                }

                                var locationLatLang = data.results[0].geometry.location;
                                var profileUpdateObj = {location:data.results[0].formatted_address};

                                if(common.checkRequired(locationLatLang) && common.checkRequired(locationLatLang.lat) && common.checkRequired(locationLatLang.lng)){
                                    profileUpdateObj = {location:profileUpdateObj.location,locationLatLang:{latitude:locationLatLang.lat,longitude:locationLatLang.lng}};
                                }
                                userManagements.updateProfileObject_new(userId,profileUpdateObj,function(err,res){
                                    common.updateContactMultiCollectionLevel(userId,false);
                                    callback(isSuccess)
                                });
                            }
                            else callback(isSuccess)
                        }
                    })
                }
                else{
                    common.updateContactMultiCollectionLevel(userId,false);
                    callback(isSuccess);
                }
            });
        }
        else callback(isSuccess)
    });
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
};


//New API to return IP address after implementation of HTTPS

function getClientIpFromReq(req) {
    var forwardedIpsStr = req.headers.userRemoteIP?req.headers.userRemoteIP:null;
    return forwardedIpsStr;
}

module.exports = IPLocator;