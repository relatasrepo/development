/**
 * Created by naveen on 22/8/17.
 */

// var Gmail = require('../common/gmail');
// var officeOutlookApi = require('../common/officeOutlookAPI');
// var interactions = require('../dataAccess/interactionManagement');
//
// var gmailObj = new Gmail();
// var officeOutlook = new officeOutlookApi();
// var interactionObj = new interactions();

var _ = require("lodash");
var moment = require('moment-timezone');

function Mailer(){

}

Mailer.prototype.send = function (message,user,callback) {

    if(user.google.length == 0 && user.outlook.length>0){
        _.each(user.outlook,function (outlookObj) {
            if(user.emailId == outlookObj.emailId){
                sendNewOutlookMail(outlookObj.refreshToken,message);
            }
        });

    } else {
        sendGmail(message,user,callback);
    }

}

function sendNewOutlookMail(refreshToken,mailBody,user,res) {

    var recipients = []
    if(mailBody.email_cc && mailBody.email_cc.length>0){

        _.each(mailBody.email_cc,function (emailId) {
            recipients.push({
                "EmailAddress": {
                    "Address": emailId
                }
            })
        });
    }

    var mail = {
        "Message": {
            "Subject": mailBody.subject,
            "Body": {
                "ContentType": "Text",
                "Content": mailBody.message
            },
            "ToRecipients": [
                {
                    "EmailAddress": {
                        "Address": mailBody.receiverEmailId
                    }
                }
            ],
            "ccRecipients":recipients
        },
        "SaveToSentItems": "true"
    };

    officeOutlook.sendEmailMSGraphAPI(refreshToken,mail,function (error,response) {

        if(!error && response){

            setTimeout(function(){
                updateOutlookMailsMSGraphAPI(refreshToken,user);
            },2000)
        }
    });
}

function updateOutlookMailsMSGraphAPI(refreshToken,user) {
    officeOutlook.getEmailsMSGraphAPI(refreshToken,user,function (mails) {
        officeOutlook.getMailsAsInteractions(mails, user, function (interactions, emailIdArr, referenceIds) {
            if(interactions && interactions.length>0){
                officeOutlook.mapInteractionsEmailIdToRelatasUsers(interactions,emailIdArr,function (mappedInteractions) {

                    if(mappedInteractions && mappedInteractions.length>0){
                        interactionObj.updateEmailInteractions(user,mappedInteractions,referenceIds,function(isSuccess){
                        });
                    }
                });
            }
        });
    });
}

function sendGmail(data,user,callback){

    var dataToSend = {
        email_cc:data.email_cc,
        receiverEmailId:data.receiverEmailId,
        receiverId:common.checkRequired(data.receiverId) ? data.receiverId : '',
        receiverName:data.receiverName,
        senderId:user._id,
        senderEmailId:user.emailId,
        senderName:user.firstName+' '+user.lastName,
        message:data.message,
        subject:data.subject,
        trackViewed:true,
        docTrack:data.docTrack,
        remind:data.remind,
        refId:data.refId,
        updateReplied:data.updateReplied || false
    };

    gmailObj.sendEmail(user._id,user.google[0].token,user.google[0].refreshToken,user.google[0].emailId,dataToSend,function(error,response){
        if(!error){
            if(dataToSend.updateReplied){
                var obj = {
                    emailId1:dataToSend.receiverEmailId,
                    emailId2:dataToSend.senderEmailId,
                    refId:dataToSend.refId,
                    interactionDate:new Date()
                };
                interactionObj.updateInteractionReplied(obj);
            }
        }
    })
}

module.exports = Mailer;
