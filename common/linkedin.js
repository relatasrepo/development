var request = require('request');
var moment = require('moment-timezone');
var appCredentials = require('../config/relatasConfiguration');
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');
var userManagement = require('../dataAccess/userManagementDataAccess');
var interactions = require('../dataAccess/interactionManagement');
var interaction = require('../databaseSchema/userManagementSchema').interaction;
var userManagementObj = new userManagement();
var appCredential = new appCredentials();
var interactionObj = new interactions();
var common = new commonUtility();
var logLib = new winstonLog();
var logger = logLib.getWinston();
var authConfig = appCredential.getAuthCredentials();
var LinkedInObj = require('node-linkedin') (authConfig.LINKEDIN_CONSUMER_KEY, authConfig.LINKEDIN_CONSUMER_SECRET, authConfig.LINKEDIN_REDIRECT_URL);
function processPostsLikes(allPosts ,likes, user, iteration){
    // console.log("Likes");
    userManagementObj.isExistingUserByLinkedinId(likes[iteration].person.id.toString(), function (error, existingUser, message) {
        if (message.message === 'existing user') {
            // console.log('existing user');
            // console.log(existingUser.emailId);
            // console.log(user.emailId);
            var arr = [
            ];
            var my = new Object();
            my.userId = existingUser._id;
            my.user_linkedin_id = likes[iteration].person.id;
            my.firstName = existingUser.firstName;
            my.lastName = existingUser.lastName;
            my.publicProfileUrl = existingUser.publicProfileUrl;
            my.profilePicUrl = existingUser.profilePicUrl;
            my.companyName = existingUser.companyName;
            my.designation = existingUser.designation;
            my.location = existingUser.location;
            my.mobileNumber = existingUser.mobileNumber;
            my.action = 'sender';
            my.type = 'linkedin';
            my.subType = 'like';
            my.emailId = existingUser.emailId;
            my.refId = (allPosts[0].updateKey || allPosts[0].updateContent.person.currentShare.id)+"_like_"+existingUser.id;
            my.source = 'linkedin';
            my.interactionType = 'linkedin';
            if(common.checkRequired(allPosts[0].updateContent.person.currentShare.content)){
                my.title = existingUser.firstName + ' ' + existingUser.lastName + ' liked : ' + allPosts[0].updateContent.person.currentShare.content.title;
                my.description = allPosts[0].updateContent.person.currentShare.content.description;
            }
            else{
                my.title = existingUser.firstName + ' ' + existingUser.lastName + ' liked : ' + allPosts[0].updateContent.person.currentShare.comment;
                my.description = allPosts[0].updateContent.person.currentShare.comment;
            }
            my.interactionDate = new Date(allPosts[0].updateContent.person.currentShare.timestamp);
            my.createdDate = my.interactionDate;
            arr.push(my);
            my = new Object();
            my.userId = user._id;
            my.user_linkedin_id = user.linkedin.id;
            my.firstName = user.firstName;
            my.lastName = user.lastName;
            my.publicProfileUrl = user.publicProfileUrl;
            my.profilePicUrl = user.profilePicUrl;
            my.companyName = user.companyName;
            my.designation = user.designation;
            my.location = user.location;
            my.mobileNumber = user.mobileNumber;
            my.action = 'receiver';
            my.type = 'linkedin';
            my.subType = 'like';
            my.emailId = user.emailId;
            my.refId = (allPosts[0].updateKey || allPosts[0].updateContent.person.currentShare.id)+"_like_"+existingUser.id;
            my.source = 'linkedin';
            my.interactionType = 'linkedin';
            if(common.checkRequired(allPosts[0].updateContent.person.currentShare.content)){
                my.title = existingUser.firstName + ' ' + existingUser.lastName + ' liked : ' + allPosts[0].updateContent.person.currentShare.content.title;
                my.description = allPosts[0].updateContent.person.currentShare.content.description;
            }
            else{
                my.title = existingUser.firstName + ' ' + existingUser.lastName + ' liked : ' + allPosts[0].updateContent.person.currentShare.comment;
                my.description = allPosts[0].updateContent.person.currentShare.comment;
            }
            my.interactionDate = new Date(allPosts[0].updateContent.person.currentShare.timestamp);
            my.createdDate = my.interactionDate;
            arr.push(my);
            interaction.findOne({
                'userId': user._id,
            },{
                interactions: {
                    $elemMatch: {
                        refId: my.refId
                    }
                }
            }, function(error, data){
                if(!common.checkRequired(data) || data.interactions.length==0){
                    addAllInteractions(user._id, arr, function (isSuccess) {
                    });
                    if(common.checkRequired(allPosts[0].updateContent.person.currentShare.content))
                        arr[1].title = arr[0].title = 'You liked : ' + allPosts[0].updateContent.person.currentShare.content.title;
                    else
                        arr[1].title = arr[0].title = 'You liked : ' + allPosts[0].updateContent.person.currentShare.comment;
                    addAllInteractions(existingUser._id, arr, function (isSuccess) {
                    });
                }
            });
        }
        if(iteration+1<likes.length)
            processPostsLikes(allPosts, likes, user, iteration+1);
    });
}

function processPostsComments(allPosts ,comments, user, iteration){
    // console.log("Comments");
    userManagementObj.isExistingUserByLinkedinId(comments[iteration].person.id.toString(), function (error, existingUser, message) {
        if (message.message === 'existing user') {
            console.log('existing user (Post Owner)');
            console.log(existingUser.emailId);
            console.log(user.emailId);
            var arr = [
            ];
            var my = new Object();
            my.userId = existingUser._id;
            my.user_linkedin_id = comments[iteration].person.id;
            my.firstName = existingUser.firstName;
            my.lastName = existingUser.lastName;
            my.publicProfileUrl = existingUser.publicProfileUrl;
            my.profilePicUrl = existingUser.profilePicUrl;
            my.companyName = existingUser.companyName;
            my.designation = existingUser.designation;
            my.location = existingUser.location;
            my.mobileNumber = existingUser.mobileNumber;
            my.action = 'sender';
            my.type = 'linkedin';
            my.subType = 'comment';
            my.emailId = existingUser.emailId;
            my.refId = comments[iteration].id;
            my.source = 'linkedin';
            my.interactionType = 'linkedin';
            if(common.checkRequired(allPosts[0].updateContent.person.currentShare.content)){
                my.title = existingUser.firstName + ' ' + existingUser.lastName + ' commented : ' + comments[iteration].comment + " on : " + allPosts[0].updateContent.person.currentShare.content.title;
                my.description = allPosts[0].updateContent.person.currentShare.content.description;
            }
            else{
                my.title = existingUser.firstName + ' ' + existingUser.lastName + ' commented : ' + comments[iteration].comment + " on : " + allPosts[0].updateContent.person.currentShare.comment;
                my.description = allPosts[0].updateContent.person.currentShare.comment;
            }
            my.interactionDate = new Date(comments[iteration].timestamp);
            my.createdDate = my.interactionDate;
            arr.push(my);
            my = new Object();
            my.userId = user._id;
            my.user_linkedin_id = user.linkedin.id;
            my.firstName = user.firstName;
            my.lastName = user.lastName;
            my.publicProfileUrl = user.publicProfileUrl;
            my.profilePicUrl = user.profilePicUrl;
            my.companyName = user.companyName;
            my.designation = user.designation;
            my.location = user.location;
            my.mobileNumber = user.mobileNumber;
            my.action = 'receiver';
            my.type = 'linkedin';
            my.subType = 'comment';
            my.emailId = user.emailId;
            my.refId = comments[iteration].id;
            my.source = 'linkedin';
            my.interactionType = 'linkedin';
            if(common.checkRequired(allPosts[0].updateContent.person.currentShare.content)){
                my.title = existingUser.firstName + ' ' + existingUser.lastName + ' commented : ' + comments[iteration].comment + " on : " + allPosts[0].updateContent.person.currentShare.content.title;
                my.description = allPosts[0].updateContent.person.currentShare.content.description;
            }
            else{
                my.title = existingUser.firstName + ' ' + existingUser.lastName + ' commented : ' + comments[iteration].comment + " on : " + allPosts[0].updateContent.person.currentShare.comment;
                my.description = allPosts[0].updateContent.person.currentShare.comment;
            }
            my.interactionDate = new Date(comments[iteration].timestamp);
            my.createdDate = my.interactionDate;
            arr.push(my);
            interaction.findOne({
                'userId': user._id,
            },{
                interactions: {
                    $elemMatch: {
                        refId: my.refId
                    }
                }
            }, function(error, data){
                if(!common.checkRequired(data) || data.interactions.length==0){
                    addAllInteractions(user._id, arr, function (isSuccess) {

                    });
                    if(common.checkRequired(allPosts[0].updateContent.person.currentShare.content))
                        arr[1].title = arr[0].title = 'You commented : ' + comments[iteration].comment + " on : " + allPosts[0].updateContent.person.currentShare.content.title;
                    else
                        arr[1].title = arr[0].title = 'You commented : ' + comments[iteration].comment + " on : " + allPosts[0].updateContent.person.currentShare.comment;
                    addAllInteractions(existingUser._id, arr, function (isSuccess) {
                    });
                }
            });
        }
        if(iteration+1<comments.length)
            processPostsComments(allPosts, comments, user, iteration+1);
    })
}

function processPosts(allPosts, user, linkedin, callback) {
    if (allPosts.length>0 && common.checkRequired(allPosts[0]) && common.checkRequired(allPosts[0].updateContent) && common.checkRequired(allPosts[0].updateContent.person) && common.checkRequired(allPosts[0].updateContent.person.currentShare)) {
        // console.log("Inside processPosts with allPosts[0]\n"+JSON.stringify(allPosts[0],null,4));
        // console.log("Newer with allPosts[0]\n"+JSON.stringify(allPosts[0],null,4));
        userManagementObj.isExistingUserByLinkedinId(allPosts[0].updateContent.person.currentShare.author.id.toString(), function (error, existingUser, message) {
            if (message.message === 'existing user') {
                if(common.checkRequired(allPosts[0].likes)){
                    processPostsLikes(allPosts.slice(0,1), allPosts[0].likes.values.slice(0), existingUser, 0)
                }
                if(common.checkRequired(allPosts[0].updateComments) && allPosts[0].updateComments._total > 0){
                    processPostsComments(allPosts.slice(0,1), allPosts[0].updateComments.values.slice(0), existingUser, 0)
                }
                var arr = [
                ];
                var my = new Object();
                my.userId = existingUser._id;
                my.user_linkedin_id = allPosts[0].updateContent.person.currentShare.author.id;
                my.firstName = existingUser.firstName;
                my.lastName = existingUser.lastName;
                my.publicProfileUrl = existingUser.publicProfileUrl;
                my.profilePicUrl = existingUser.profilePicUrl;
                my.companyName = existingUser.companyName;
                my.designation = existingUser.designation;
                my.location = existingUser.location;
                my.mobileNumber = existingUser.mobileNumber;
                my.action = 'sender';
                my.type = 'linkedin';
                my.subType = 'share';
                my.emailId = existingUser.emailId;
                my.refId = allPosts[0].updateKey || allPosts[0].updateContent.person.currentShare.id;
                my.source = 'linkedin';
                my.interactionType = 'linkedin';
                if(common.checkRequired(allPosts[0].updateContent.person.currentShare.content)){
                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' shared : ' + allPosts[0].updateContent.person.currentShare.content.title;
                    my.postURL=allPosts[0].updateContent.person.currentShare.content.submittedUrl;
                    my.description = allPosts[0].updateContent.person.currentShare.content.description;
                }
                else{
                    my.title=existingUser.firstName + ' ' + existingUser.lastName + ' shared : ' + allPosts[0].updateContent.person.currentShare.comment;
                    my.description=allPosts[0].updateContent.person.currentShare.comment;
                }
                my.interactionDate = new Date(allPosts[0].updateContent.person.currentShare.timestamp);
                my.createdDate = my.interactionDate;
                arr.push(my);
                // my = new Object();
                // my.userId = existingUser._id;
                // my.user_linkedin_id = allPosts[0].updateContent.person.currentShare.author.id;
                // my.firstName = existingUser.firstName;
                // my.lastName = existingUser.lastName;
                // my.publicProfileUrl = existingUser.publicProfileUrl;
                // my.profilePicUrl = existingUser.profilePicUrl;
                // my.companyName = existingUser.companyName;
                // my.designation = existingUser.designation;
                // my.location = existingUser.location;
                // my.mobileNumber = existingUser.mobileNumber;
                // my.action = 'receiver';
                // my.type = 'linkedin';
                // my.subType = 'share';
                // my.emailId = existingUser.emailId;
                // my.refId = allPosts[0].updateKey || allPosts[0].updateContent.person.currentShare.id;
                // my.source = 'linkedin';
                // my.interactionType = 'linkedin';
                // if(common.checkRequired(allPosts[0].updateContent.person.currentShare.content)){
                //     my.title = existingUser.firstName + ' ' + existingUser.lastName + ' shared : ' + allPosts[0].updateContent.person.currentShare.content.title;
                //     my.postURL=allPosts[0].updateContent.person.currentShare.content.submittedUrl;
                //     my.description = allPosts[0].updateContent.person.currentShare.content.description;
                // }
                // else{
                //     my.title=existingUser.firstName + ' ' + existingUser.lastName + ' shared : ' + allPosts[0].updateContent.person.currentShare.comment;
                //     my.description=allPosts[0].updateContent.person.currentShare.comment;
                // }
                // my.interactionDate = new Date(allPosts[0].updateContent.person.currentShare.timestamp);
                // my.createdDate = my.interactionDate;
                // arr.push(my);
                interaction.findOne({
                    'userId': existingUser._id,
                },{
                    interactions: {
                        $elemMatch: {
                            refId: my.refId
                        }
                    }
                }, function(error, data){
                    if(!common.checkRequired(data) || data.interactions.length==0){
                        addAllInteractions(existingUser._id, arr, function (isSuccess) {
                            if (isSuccess === true)
                                callback(true);
                            else
                                callback(false);
                        });
                        if(common.checkRequired(allPosts[0].updateContent.person.currentShare.content))
                            arr[0].title = 'You shared : ' + allPosts[0].updateContent.person.currentShare.content.title;
                        else
                            arr[0].title = 'You shared : ' + allPosts[0].updateContent.person.currentShare.comment;
                        addAllInteractions(existingUser._id, arr, function (isSuccess) {
                        });
                        var newAllPosts = allPosts.slice(1);
                        if (newAllPosts.length > 0) {
                            processPosts(newAllPosts, user, linkedin,function () {
                            });
                        }
                    }
                    else{
                        newAllPosts = allPosts.slice(1);
                        if (newAllPosts.length > 0) {
                            processPosts(newAllPosts, user, linkedin,function () {
                            });
                        }
                    }
                });
            }
        })
    }
    else{
        var newAllPosts = allPosts.slice(1);
        if (newAllPosts.length > 0) {
            processPosts(newAllPosts, user, linkedin,function () {
            });
        }
    }
}

function addAllInteractions(userId, interactions, callback) {
    // console.log('addAllInteractions called with userId\n' + userId + '\n\ninteractions\n' + JSON.stringify(interactions, null, 4) + '\n\n')
    interaction.update({
        userId: userId
    }, {
        $addToSet: {
            interactions: {
                $each: interactions
            }
        }
    }, {
        upsert: true
    }, function (error, result) {
        if (error) {
            logger.info('Error in addAllInteractionsNonRepeating(): interactionsManagement '+ JSON.stringify(error,null,4));
            callback(false)
        }
        else {
            // console.log('result is ' + JSON.stringify(result) + '\n\n');
            // console.log('Added the following FB interaction for ' + userId + '\n\n');
            // console.log(JSON.stringify(interactions, null, 4) + '\n\n');
            callback(true);
        }
    });
}

function LinkedIn() {
    this.getUserProfileSelf = function (linkedin, callback) {
        if (this.validateLinkedinAccount(linkedin)) {
            var linkedIn = LinkedInObj.init(linkedin.token);
            linkedIn.people.me(['first-name',
                'last-name',
                'headline',
                'educations',
                'summary'], function (err, profile) {
                if (err || !common.checkRequired(profile)) {
                    callback(true, null)
                }
                else {
                    callback(false, profile)
                }
            });
        } else callback('NO_LINKEDIN_ACCOUNT', null)
    };
    this.getOtherUserProfileWithoutCommonConnections = function (LinkedInO, callback) {
        if (this.validateLinkedinAccount(LinkedInO)) {
            var linkedIn = LinkedInObj.init(LinkedInO.token);
            linkedIn.people.id(LinkedInO.id, [
                'id',
                'public-profile-url',
                'first-name',
                'last-name',
                'current-share',
                'headline',
                'positions'
            ], function (err, data) {
                if (common.checkRequired(data)) {
                    if (data.errorCode == 0 || data.errorCode == '0') {
                        callback('LINKEDIN_ERROR', null)
                    }
                    else {
                        callback(false, data)
                    }
                }
            });
        }
        else callback('NO_LINKEDIN_ACCOUNT', null)
    };
    this.shareOnLinkedIn=function(userId,shareURL,callback){
        userManagementObj.findUserProfileByIdWithCustomFields(userId, {linkedin :1}, function(error,user){
            if(error)
                callback(error,false);
            else if(user){
                var body={
                    "comment": shareURL,
                    "visibility": {
                        "code": "anyone"
                    }
                }
                var options = {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        "x-li-format": "json"
                    },
                    uri: 'https://api.linkedin.com/v1/people/~/shares?format=json&oauth2_access_token='+user.linkedin.token,
                    body: JSON.stringify(body)
                }
                request(options,function(error,response,body){
                    if(error)
                        callback(error,false)
                    else{
                        var result=JSON.parse(body);
                        if(common.checkRequired(result.updateKey)){
                            callback(null,true)
                        }
                        else
                            callback(error,false)
                    }
                })
            }
        })
    }
    this.getOtherUserProfileWithCommonConnections = function (LinkedInL, LinkedInO, callback) {
        if (this.validateLinkedinAccount(LinkedInO) && this.validateLinkedinAccount(LinkedInL)) {
            var linkedIn = LinkedInObj.init(LinkedInL.token);
            linkedIn.people.id(LinkedInO.id, [
                'id',
                'public-profile-url',
                'first-name',
                'last-name',
                'current-share',
                'headline',
                'positions',
                'relation-to-viewer:(related-connections:(id,first-name,last-name,siteStandardProfileRequest,pictureUrl))'
            ], function (err, data) {
                if (common.checkRequired(data)) {
                    if (data.errorCode == 0 || data.errorCode == '0') {
                        callback('LINKEDIN_ERROR', null)
                    }
                    else {
                        callback(false, data)
                    }
                }
            });
        }
        else callback('NO_LINKEDIN_ACCOUNT', null)
    };
    this.getUserFeeds = function (linkedin, count, start, callback) {
        if (this.validateLinkedinAccount(linkedin)) {
            var connectionOptions = {
                url: 'https://api.linkedin.com/v1/people/~/network/updates?type=SHAR&count=' + count + '&start=' + start,
                headers: {
                    'x-li-format': 'json'
                },
                qs: {
                    oauth2_access_token: linkedin.token
                }
            };
            request(connectionOptions, function (error, r, status) {
                try {
                    status = JSON.parse(status)
                    if (status.errorCode == 0) {
                        logger.info('linkedin Feed error ', status)
                    }
                }
                catch (e) {
                    logger.info('Exception in getUserFeeds(): LinkedIn ', e);
                    status = null;
                }
                callback(false, status)
            })
        } else callback('NO_LINKEDIN_ACCOUNT', null)
    };
    this.likeFeed = function (linkedin, updateKey, callback) {
        if (this.validateLinkedinAccount(linkedin)) {
            var likeOptions = {
                url: 'https://api.linkedin.com/v1/people/~/network/updates/key=' + updateKey + '/is-liked',
                headers: {
                    'x-li-format': 'json'
                },
                qs: {
                    oauth2_access_token: linkedin.token
                },
                body: JSON.stringify({
                    isLiked: true
                })
            };
            request.put(likeOptions, function (error, r, status) {
                callback(false, false);
                /*if ( r.statusCode != 201 ) {

                 }
                 else{

                 }*/
            })
        } else callback('NO_LINKEDIN_ACCOUNT', null)
    };
    this.getLinkedInConnections = function (linkedin, callback) {
        if (this.validateLinkedinAccount(linkedin)) {
            var linkedIn = LinkedInObj.init(linkedin.token);
            linkedIn.connections.retrieve(function (err, connections) {
                if (err || !common.checkRequired(connections)) {
                    callback(true, null)
                }
                else {
                    callback(false, connections)
                }
            });
        } else callback('NO_LINKEDIN_ACCOUNT', null)
    };
    this.validateLinkedinAccount = function (linkedin) {
        if (common.checkRequired(linkedin) && common.checkRequired(linkedin.token)) {
            return true;
        }
        else return false;
    };
    this.getUserFeedsByDate = function (linkedin, start, callback) {
        if (this.validateLinkedinAccount(linkedin)) {
            var after = moment();
            after.date(after.date() - 90);
            after = new Date(after.format()).getTime();
            if (common.checkRequired(linkedin.lastSync)) {
                // after = new Date(linkedin.lastSync).getTime();
            }
            var connectionOptions = {
                url: 'https://api.linkedin.com/v1/people/~/network/updates?type=SHAR&count=200&after=' + after,
                headers: {
                    'x-li-format': 'json'
                },
                qs: {
                    oauth2_access_token: linkedin.token
                }
            };
            request(connectionOptions, function (error, r, status) {
                try {
                    status = JSON.parse(status);
                    if (status.errorCode == 0) {
                        logger.info('linkedin Feed error ', status)
                        logger.info(status)
                        callback(true, status)
                    }
                    else {
                        callback(false, status)
                    }
                }
                catch (e) {
                    logger.info('Exception in getUserFeeds(): LinkedIn ', e);
                    logger.info(e)
                    status = null;
                    callback(true, status)
                }
            })
        } else callback('NO_LINKEDIN_ACCOUNT', null)
    };
    this.getUserFeedsSinceLastUpdate = function (user, linkedin, callback) {
        var after = (new Date(((new Date()).setDate((new Date(Date.now())).getDate() - 30)))).getTime();
        var connectionOptions = {
            url: 'https://api.linkedin.com/v1/people/~/network/updates?type=SHAR&count=100&after=' + after + '&format=json',
            headers: {
                'x-li-format': 'json'
            },
            qs: {
                oauth2_access_token: linkedin.token
            }
        };
        request(connectionOptions, function (error, r, status) {
            if (error)
                console.log(error);
            else {
                status = JSON.parse(status);
                if (status.errorCode == 0) {
                    logger.info('linkedin Feed error ', status)
                    logger.info(status)
                    callback(true, status)
                }
                else {
                    callback(false, status);
                }
            }
        })
    };
    this.importLinkedInFeedToInteractions = function (user, linkedin, callback) {
        this.getUserFeedsSinceLastUpdate(user, linkedin, function (err, data) {
            if (!err) {
                if(common.checkRequired(data) && common.checkRequired(data.values) && data.values.length>0){
                    // console.log(user.emailId);
                    var posts = data.values.slice(0);
                    processPosts(posts, user, linkedin, function(isSuccess){
                    });
                    callback(true);
                }
                else{
                    console.log("No new linkedin posts");
                    callback(true);
                }
            }
            else {
                callback(false);
            }
        });
    }
}
module.exports = LinkedIn;