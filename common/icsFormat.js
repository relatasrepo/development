
var iCalEvent = require('icalevent');
var moment = require('moment-timezone');

var userManagement = require('../dataAccess/userManagementDataAccess');
var calendarPasswordManagement = require('../dataAccess/calendarPasswordManagement');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');


var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var domainName = domain.domainName;

var logLib = new winstonLog();
var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();

function Ics(){

}

Ics.prototype.createMeetingIcs = function(meeting,timezone,receiverName,callback){
    var event = new iCalEvent();
    console.log(meeting, timezone,receiverName)
    event.set('uid', meeting.invitationId+'@relatas.com');

    event.set('method', 'request');
    event.set('status', 'confirmed');

    for(var i=0; i<meeting.scheduleTimeSlots.length; i++){
        if(meeting.scheduleTimeSlots[i].isAccepted){
            var start,end;
            if(timezone){
                var zone = moment.tz.zone(timezone);
                var offset = zone.parse(Date.UTC(meeting.scheduleTimeSlots[i].start.date))
                start = moment(meeting.scheduleTimeSlots[i].start.date).tz(timezone).toDate();
                end = moment(meeting.scheduleTimeSlots[i].end.date).tz(timezone).toDate();
                event.set('offset', offset);
            }else{
                event.set('offset', new Date().getTimezoneOffset());
                start =  meeting.scheduleTimeSlots[i].start.date;
                end = meeting.scheduleTimeSlots[i].end.date;
            }

            event.set('start',start);
            event.set('end', end);
            event.set('summary',  meeting.scheduleTimeSlots[i].title);
            var text = '';
            if(meeting.scheduleTimeSlots[i].message){
                 text = 'Message from '+meeting.scheduleTimeSlots[i].message;
            }


            if(meeting.scheduleTimeSlots[i].messages){
                if(meeting.scheduleTimeSlots[i].messages.length > 0){
                    for(var msg=0; msg<meeting.scheduleTimeSlots[i].messages.length; msg++){
                        text += 'Message from '+receiverName+': '+meeting.scheduleTimeSlots[i].messages[msg].message;
                    }
                }
            }
            var description = meeting.scheduleTimeSlots[i].description+'. '+text;
            if(timezone){
                event.set('timezone', timezone);
            }

            event.set('description', description);
            event.set('location', meeting.scheduleTimeSlots[i].locationType+': '+meeting.scheduleTimeSlots[i].location);
        }
    }

    //event.set('organizer', { name:  meeting.senderName });
    event.set('url', domainName+'/meeting/'+meeting.invitationId);
    var icsFile = event.toFile();
    callback(icsFile);
};

module.exports = Ics;