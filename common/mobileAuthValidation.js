
var commonUtility = require('../common/commonUtility');
var errorClass = require('../errors/errorClass');

var common = new commonUtility();
var errorObj = new errorClass();

function MobileAuthValidation(){

    // ROUTER MIDDLEWARE
    this.isAuthenticatedMobile = function(req,res,next){
        if(common.checkRequired(req.headers.token)){
            next();
        }
        else res.send(errorObj.generateErrorResponse({status:5000,key:'TOKEN_NOT_FOUND'}));
    };
}



module.exports = MobileAuthValidation;