
var accountsCollection = require('../databaseSchema/accountSchema').accounts;
var moment = require('moment-timezone');
var _ = require("lodash");
var commonUtility = require('../common/commonUtility');
var contactClass = require('../dataAccess/contactsManagementClass');
var interactions = require('../dataAccess/interactionManagement');
var userManagement = require('../dataAccess/userManagementDataAccess');
var linkedinM = require('../common/linkedin');
var fullContact = require('../common/fullContact');
var twitterM = require('../common/twitter');
var errorClass = require('../errors/errorClass');
var errorMessages = require('../errors/errorMessage');
var accounts = require('../dataAccess/accountManagement');

var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var statusCodes = errorMessagesObj.getStatusCodes();
var twitterObj = new twitterM();
var fullContactObj = new fullContact();
var interactionObj = new interactions();
var linkedinObj = new linkedinM();
var userManagementObj = new userManagement();
var common = new commonUtility();
var contactObj = new contactClass();
var accountsObj = new accounts();

function ContactSupport(){

    this.getContactsLosingTouch = function(userId,user,timezone,returndata,skip,limit,sortByDate,callback){
        common.interactionDataFor(function(days){
            var dateMin = moment().tz(timezone);
            dateMin.date(dateMin.date() - days)
            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)

            contactObj.getAllFavoriteContactsEmailIds(userId,user.emailId,user.mobileNumber,function(emailIdArr){
                if(emailIdArr.length > 0){

                    interactionObj.losingTouchByDateORDaysGetList(userId,emailIdArr,dateMin,sortByDate,function(losingTouch){
                        // losingTouch = common.removeDuplicates_emailId(losingTouch);
                        var count = 0;
                        if(losingTouch && losingTouch.length > 0){
                            count = losingTouch.length;
                        }
                        if(returndata == 'count'){
                            callback({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{
                                    count:count
                                }
                            })
                        }
                        else{
                            var total = losingTouch.length;
                            var results = losingTouch.splice(skip,limit);

                            contactObj.getContactImageLinks(userId,emailIdArr,function (response) {
                                var obj2 = _.map(response, function(a) {
                                    return _.extend(a, _.find(results, function(b) {
                                        return a._id === b._id.emailId;
                                    }))
                                });

                                var finalArr = []
                                for(var i=0;i<obj2.length;i++){
                                    if(obj2[i].lastInteracted){
                                        finalArr.push(obj2[i])
                                    }
                                }

                                var resObj = {};
                                if(total == 0){
                                    resObj = {
                                        "SuccessCode": 0,
                                        "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_FOR_LOSING_TOUCH"),
                                        "ErrorCode": 1,
                                        "Data":{
                                            total:total,
                                            skipped:skip,
                                            limit:limit,
                                            returned:finalArr.length,
                                            contacts:finalArr,
                                            timezone:timezone
                                        }
                                    };
                                }
                                else resObj = {
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":{
                                        total:total,
                                        skipped:skip,
                                        limit:limit,
                                        returned:finalArr.length,
                                        contacts:finalArr,
                                        timezone:timezone
                                    }
                                };
                                callback(resObj);
                            });
                        }
                    })
                }
                else{
                    if(returndata == 'count'){
                        callback({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":{
                                count: 0
                            }
                        })
                    }
                    else{
                        callback({
                            "SuccessCode": 1,
                            "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_FOR_LOSING_TOUCH"),
                            "ErrorCode": 0,
                            "Data":{
                                total:0,
                                skipped:skip,
                                limit:limit,
                                returned:0,
                                contacts:[],
                                timezone:timezone
                            }
                        })
                    }
                }
            });
        })
    };

    this.getContactsInUserLocationFromLosingTouch_new = function(userId,user,timezone,location,sortByDate,callback) {
        var dateMin = moment().tz(timezone);
        dateMin.date(dateMin.date() - 15)
        dateMin.hour(0)
        dateMin.minute(0)
        dateMin.second(0)
        dateMin.millisecond(0)

        contactObj.getAllFavoriteContactsEmailIds(user._id,user.emailId,function(emailIdArr){
            if(emailIdArr.length > 0){
                interactionObj.losingTouchByDateORDaysGetList_location_designation(userId,emailIdArr,dateMin,location,null,sortByDate,function(losingTouch){

                    if(common.checkRequired(losingTouch) && losingTouch.length > 0){
                        callback(losingTouch)
                    }
                    else{
                        callback([])
                    }
                })
            }
            else{
                callback([])
            }
        });
    };

    this.getContactsInUserLocationFromLosingTouch = function(userId,user,timezone,location,callback) {
        var dateMin = moment().tz(timezone);
        dateMin.date(dateMin.date() - 15)
        dateMin.hour(0)
        dateMin.minute(0)
        dateMin.second(0)
        dateMin.millisecond(0)

        contactObj.getAllFavoriteContactsEmailIds(user._id,user.emailId,function(emailIdArr){
            if(emailIdArr.length > 0){
                interactionObj.losingTouchByDateORDaysGetEmailIdArray(userId,emailIdArr,dateMin,function(losingTouch){

                    if(common.checkRequired(losingTouch) && losingTouch.length > 0){
                        contactObj.getContactsMatchedLocationByEmailIdArr(user._id,losingTouch,location,function(error,contacts,userIdList){

                            if(error){
                                // Log error
                            }
                            callback(contacts)
                        })
                    }
                    else{
                        callback([])
                    }
                })
            }
            else{
                  callback([])
            }
        });
    };

    this.getCommonContacts = function (userId, cUserId, skip, limit, callback) {

        contactObj.getCommonContacts(common.castToObjectId(userId), common.castToObjectId(cUserId), false,function (contacts) {

            var total = contacts.length;
            var results = contacts.splice(skip, limit);
            callback({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": {
                    total: total,
                    skipped: skip,
                    limit: limit,
                    returned: results.length,
                    contacts: results
                }
            });
        })
    };

    this.getCommonContacts_web = function (userId, cUserId, skip, limit, fetchWithMobile,callback) {

        if(!common.validateEmail(cUserId) && !fetchWithMobile){

            contactObj.getCommonContacts(common.castToObjectId(userId), common.castToObjectId(cUserId), false,function (contacts) {

                var total = contacts.length;

                contacts = _.uniq(contacts, 'personEmailId');

                var results = contacts.splice(skip, limit);

                if(results.length > 0){

                    var newArray = common.removeDuplicates_field(results, 'personEmailId')

                    callback({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": {
                            total: total,
                            skipped: skip,
                            limit: limit,
                            returned: newArray.length,
                            contacts: newArray
                        }
                    });
                }
                else callback(errorObj.generateErrorResponse({
                    status: statusCodes.MESSAGE,
                    key: 'NO_COMMON_CONNECTIONS_FOUND'
                }));
            })
        } else if(common.validateEmail(cUserId) || common.isNumber(cUserId)){

            //cUserId is either email or mobileNumber here.

            callback(errorObj.generateErrorResponse({
                status: statusCodes.MESSAGE,
                key: 'NO_COMMON_CONNECTIONS_FOUND'
            }));
        }
    };

    this.getContactsWhoAllInCompanyInteractedWithArrayOfEmailsContact = function(userId,emailId,contactEmilIdArr,companyName,skip,limit,callback){
        contactObj.getContactsEmailsMatchedCompanyName(common.castToObjectId(userId),emailId,companyName,function(emailIdArr){

            if(common.checkRequired(emailIdArr) && emailIdArr.length > 0){
                contactObj.getContactsEmailsExistInOther(common.castToObjectId(userId),contactEmilIdArr,true,emailIdArr,function(error,contacts){
                    if(error){
                        callback(error,{
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {
                                total: 0,
                                skipped: skip,
                                limit: limit,
                                returned: 0,
                                contacts: []
                            }
                        });
                    }
                    else{
                        var total = contacts.length;
                        var results = contacts.splice(skip, limit);

                        callback(error,{
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {
                                total: total,
                                skipped: skip,
                                limit: limit,
                                returned: results.length,
                                contacts: results
                            }
                        });
                    }
                })
            }
            else{
                callback(null,{
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": {
                        total: 0,
                        skipped: skip,
                        limit: limit,
                        returned: 0,
                        contacts: []
                    }
                });
            }
        })
    };

    this.getAllContacts = function(userId,skip,limit,callback){
        contactObj.getAllContactsWithProfile(userId,function(err,contacts){
            if(err){
                callback({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'},{
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":{
                        total:0,
                        skipped:skip,
                        limit:limit,
                        returned:0,
                        contacts:[]
                    }})
            }
            else {

                var total = contacts.length;
                var results = contacts.splice(skip,limit);

                _.each(results,function (c) {
                    if(c.personId == " " || c.personId == ""){
                        c.personId = null;
                    }
                })

                callback(false,{
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        total:total,
                        skipped:skip,
                        limit:limit,
                        returned:results.length,
                        contacts:results
                    }
                });
            }
        })
    };

    this.getContactsCount = function (userId,callback) {
        contactObj.getContactsCount(userId,callback)
    }

    this.getContactsAccountCount = function (userId,callback) {
        contactObj.getContactsAccountCount(userId,callback)
    }

    this.getFavoriteContacts = function(userId,skip,limit,searchContent,callback){
        contactObj.getAllFavoriteContactsWithProfile(userId,searchContent,function(error,contacts,ids){
            var total = contacts.length;
            var results = contacts.splice(skip,limit);

            _.each(results,function (c) {
                if(c.personId == " " || c.personId == ""){
                    c.personId = null;
                }
            })

            callback({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{
                    total:total,
                    skipped:skip,
                    limit:limit,
                    returned:results.length,
                    contacts:results
                }
            });
        })
    };

    this.getRecentlyInteractedContacts = function(userId,skip,limit,searchContent,companyId,callback){
        interactionObj.getRecentlyInteractedContacts(userId,function (interactedContacts) {

            var searchContent = _.compact(_.pluck(interactedContacts,'_id'));

            if(interactedContacts && interactedContacts.length>0){
                contactObj.getRecentlyInteractedContacts(userId,searchContent,function(error,contacts,ids){

                    var mergedContacts = _.map(contacts, function(a) {
                        return _.extend(a, _.find(interactedContacts, function(b) {
                            if(common.isNumber(b._id)){
                                return a.mobileNumber === b._id;
                            } else {
                                return a.personEmailId === b._id;
                            }
                        }))
                    });

                    var temp = mergedContacts.filter(function (c) {

                        if(c.personId == " " || c.personId == ""){
                            c.personId = null;
                        }

                        if(c.personEmailId != c.ownerEmailId){
                            return c;
                        }
                    });

                    contactObj.getAllDntCotnacts(common.castToObjectId(userId), function(dntContacts) {
                        
                        temp = temp.filter( function( el ) {
                            return ! (_.includes(dntContacts.mobileNumbers, el.mobileNumber ) && _.includes(dntContacts.personEmailIds, el.personEmailId));
                        });

                        temp = _.sortBy(temp, function (c) {
                            return -c.lastInteractedDate
                        });

                        var total = temp.length;
                        var results = temp.splice(skip,limit);

                        callback({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":{
                                total:total,
                                skipped:skip,
                                limit:limit,
                                returned:results.length,
                                contacts:results
                            }
                        });
            
                    })
                })
            }else{
                callback({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": {
                        total: 0,
                        skipped: skip,
                        limit: limit,
                        returned: 0,
                        contacts: []
                    }});

            }
        });
    };

    this.getNewContacts = function(userId,user,timezone,returndata,skip,limit,callback){

        var dateMin = moment().tz(timezone);
        dateMin.date(dateMin.date() - 15)
        dateMin.hour(0)
        dateMin.minute(0)
        dateMin.second(0)
        dateMin.millisecond(0)

        contactObj.getNewContactsAdded(user._id,dateMin,function(err,contacts){

            if(err){
                callback({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'},[])
            }else
            if(contacts.length > 0){
                if(returndata == 'count'){
                    callback(false,{
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":{
                            count:contacts[0].count || 0
                        }
                    });
                }
                else{
                    contactObj.populateUserProfileContacts(contacts[0].contacts,function(pContacts,isPopulated){
                        pContacts.sort(function(a,b){
                            return new Date(b.addedDate) - new Date(a.addedDate);
                        });
                        var total = pContacts.length;
                        var results = pContacts.splice(skip,limit);

                        callback(false,{
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":{
                                total:total,
                                skipped:skip,
                                limit:limit,
                                returned:results.length,
                                contacts:results
                            }
                        });
                    })
                }
            }
            else{
                if(returndata == 'count'){
                    callback(false,{
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":{
                            count: 0
                        }
                    });
                }
                else{
                    callback(false,{
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":{
                            total:0,
                            skipped:skip,
                            limit:limit,
                            returned:0,
                            contacts:[]
                        }
                    });
                }
            }
        })
    };

    this.getContactsByType = function(userId,type,emailIdArr,skip,limit,searchContent,callback){

        contactObj.getContactsByType(userId,type,emailIdArr,searchContent,function(error,contacts,ids){

            var total = contacts.length;
            var results = contacts.splice(skip,limit);

            _.each(results,function (c) {
                if(c.personId == " " || c.personId == ""){
                    c.personId = null;
                }
            })

            callback({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{
                    total:total,
                    skipped:skip,
                    limit:limit,
                    returned:results.length,
                    contacts:results
                }
            });
        })
    };

    this.getContactsMatchedLocation = function(userId,user,location,skip,limit,callback){
        contactObj.getContactsMatchedLocation(user._id,user.emailId,location,null,function(error,contacts){

            var total = contacts.length;
            var results = contacts.splice(skip,limit);
            callback({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{
                    total:total,
                    skipped:skip,
                    limit:limit,
                    returned:results.length,
                    contacts:results,
                    location:location
                }
            });
        })
    };

    this.getCountsOfContactsByTypeInteracted = function(userId,timezone,callback){

        var dateMin = moment().tz(timezone);
        dateMin.date(dateMin.date() - 7)
        dateMin.hour(0)
        dateMin.minute(0)
        dateMin.second(0)
        dateMin.millisecond(0)

        interactionObj.getInteractedEmailsDate(userId,dateMin,function(emails){
             if(emails.length > 0){
                 contactObj.getContactsByTypeCount(common.castToObjectId(userId),emails,[],function(error,data){
                     callback(emails,data);
                 })
             }
             else{
                 callback(emails,[]);
             }
        })
    };

    this.searchUserContacts = function(searchContent,searchByEmailId,userId,skip,limit,callback){
        contactObj.searchUserContacts(searchContent,searchByEmailId,common.castToObjectId(userId),skip,limit,null,function(error,contacts){
            var total = contacts.length;
            var results = contacts.splice(skip,limit);

            _.each(results,function (c) {
                if(c.personId == " " || c.personId == ""){
                    c.personId = null;
                }
            })

            callback(error,{
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{
                    total:total,
                    skipped:skip,
                    limit:limit,
                    returned:results.length,
                    contacts:results
                }
            });
        })
    };

    this.searchUserContact = function(searchContent,searchByEmailId,userId,skip,limit,callback){
        contactObj.searchUserContact(searchContent,searchByEmailId,userId,skip,limit,null,function(error,contacts){
            var total = contacts.length;
            var results = contacts.splice(skip,limit);
            callback(error,{
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{
                    total:total,
                    skipped:skip,
                    limit:limit,
                    returned:results.length,
                    contacts:results
                }
            });
        })
    };

    this.searchUserContacts_for_search_page = function(searchContent,userId,skip,limit,filters,callback){

        contactObj.searchUserContacts_searchPage(searchContent,common.castToObjectId(userId),skip,limit,filters,function(error,contacts){
            //var total = contacts.length;
            //var results = contacts.splice(skip,limit);

            callback(error,{
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "userId":userId,
                "Data":{
                    total:contacts.length,
                    //skipped:skip,
                    //limit:limit,
                    returned:contacts.length,
                    contacts:contacts
                }
            });
        })
    };

    this.searchUserHashtag_for_search_page = function(searchContent,userId,skip,limit,filters,callback){

        contactObj.searchUserHashtag_searchPage(searchContent,common.castToObjectId(userId),skip,limit,filters,function(error,contacts){
            //var total = contacts.length;
            //var results = contacts.splice(skip,limit);

            callback(error,{
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "userId":userId,
                "Data":{
                    total:contacts.length,
                    //skipped:skip,
                    //limit:limit,
                    returned:contacts.length,
                    contacts:contacts
                }
            });
        })
    };

    this.searchUserContactsCompany = function(searchContent,userId,skip,limit,callback){
        contactObj.searchUserContactsCompany(searchContent,common.castToObjectId(userId),skip,limit,function(error,contacts){
            var total = contacts.length;
            var results = contacts.splice(skip,limit);
            callback(error,{
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{
                    total:total,
                    skipped:skip,
                    limit:limit,
                    returned:results.length,
                    contacts:results
                }
            });
        })
    };

    this.getCommonContactsByEmailIdWithinCompany = function(userId,emailId,fetchWith,companyName,fetchWithMobile,callback){

        var liuCompanyEmailIdDomain = common.fetchCompanyFromEmail(emailId)?'@'+common.fetchCompanyFromEmail(emailId):null;

        if(liuCompanyEmailIdDomain){

            contactObj.getCommonContactsWithEmailWithinCompany(common.castToObjectId(userId),emailId,fetchWith,companyName,liuCompanyEmailIdDomain,function(contacts){

                if(common.checkRequired(contacts) && contacts.length > 0){
                    callback(contacts)
                }
                else callback([])
            });
        } else {
            callback([])
        }
    };

    this.importLinkedInConnections = function(linkedin,callback){
        linkedinObj.getLinkedInConnections(linkedin,function(error,connections){

        })
    };

    this.getSingleContactRelation = function(userId,contactEmailId,isEmailId,fullContact,mobileNumber, callback){
        contactObj.getSingleContactRelation(userId,contactEmailId,isEmailId,fullContact,mobileNumber,function(obj){
            callback(obj);
        })
    };

    this.getContactSocialDetails = function(userId,contactEmailId,req,callback){
        req.session.fullContactSession = null;
        if (req.session.fullContactSession == undefined || req.session.fullContactSession == null) {
            req.session.fullContactSession = {};
        }

        if(common.checkRequired(req.session.fullContactSession[contactEmailId])){

            if(!req.session.fullContactSession[contactEmailId+'treq']){
                req.session.fullContactSession[contactEmailId+'treq'] = true;
                getImportTwitterTweets(userId,contactEmailId,req.session.fullContactSession[contactEmailId]);
            }
            else req.session.fullContactSession[contactEmailId+'treq'] = false;

            callback(req.session.fullContactSession[contactEmailId]);
        }
        else{

            contactObj.getContactSocialInfo(userId,contactEmailId,function(error,contact){
                var data = null;
                var isValid1 = false;
                if(common.checkRequired(contact)){
                    data = {};
                    if(common.checkRequired(contact.twitterUserName)){
                        isValid1 = true;
                        data.twitterUserName = contact.twitterUserName;

                    }
                    if(common.checkRequired(contact.twitterUserName)){
                        isValid1 = true;
                        data.facebookUserName = contact.facebookUserName;

                    }
                    if(common.checkRequired(contact.twitterUserName)){
                        isValid1 = true;
                        data.linkedinUserName = contact.linkedinUserName;

                    }
                    if(common.checkRequired(contact.mobileNumber)){
                        data.mobileNumber = contact.mobileNumber;
                    }
                }
                if(isValid1){
                    req.session.fullContactSession[contactEmailId] = data;
                    if(!req.session.fullContactSession[contactEmailId+'treq']){
                        req.session.fullContactSession[contactEmailId+'treq'] = true;
                        getImportTwitterTweets(userId,contactEmailId,data);
                    }
                    else req.session.fullContactSession[contactEmailId+'treq'] = false;

                    callback(data);
                }
                else{
                    userManagementObj.selectUserProfileCustomQuery({emailId:contactEmailId},{twitter:1,linkedin:1,facebook:1,mobileNumber:1},function(error,user){
                        data = {};
                        var isValid2 = false;
                        if(common.checkRequired(user)){
                            if(common.checkRequired(user.twitter) && common.checkRequired(user.twitter.userName)){
                                isValid2 = true;
                                data.twitterUserName = user.twitter.userName;
                            }
                            if(common.checkRequired(user.facebook) && common.checkRequired(user.facebook.name)){
                                isValid2 = true;
                                data.facebookUserName = user.facebook.name;
                            }
                            if(common.checkRequired(user.linkedin) && common.checkRequired(user.linkedin.name)){
                                isValid2 = true;
                                data.linkedinUserName = user.linkedin.name;
                            }
                            if(common.checkRequired(user.mobileNumber)){
                                data.mobileNumber = user.mobileNumber;
                            }

                        }
                        if(isValid2){
                            req.session.fullContactSession[contactEmailId] = data;
                            if(!req.session.fullContactSession[contactEmailId+'treq']){
                                req.session.fullContactSession[contactEmailId+'treq'] = true;
                                getImportTwitterTweets(userId,contactEmailId,data);
                            }
                            else req.session.fullContactSession[contactEmailId+'treq'] = false;
                            callback(data);
                        }
                        else{
                            callback(null)
                            // fullContactObj.getPersonByEmail(userId, contactEmailId, req, function (obj) {
                            //     if(common.checkRequired(obj)){
                            //         getImportTwitterTweets(userId,contactEmailId,obj);
                            //         callback(data);
                            //     }
                            //     else callback(null)
                            // })
                        }
                    })
                }
            })
        }
    }
}

function getImportTwitterTweets(userId,contactEmailId,obj){
    var t_user_name;
    if(common.checkRequired(obj) && common.checkRequired(obj.twitterUserName)){
        t_user_name = obj.twitterUserName;
    }
    if(common.checkRequired(t_user_name)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{twitter:1,emailId:1,mobileNumber:1,designation:1,companyName:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,location:1},function(error,user){
            if(common.checkRequired(user)){
                twitterObj.importTwitterFeedToInteractions(user.twitter,user,t_user_name,contactEmailId);
            }
        });
    }
}

ContactSupport.prototype.getContactsWithLastInteractedDate = function(userId,searchContent,callback){

    contactObj.searchUserContactsMobile(searchContent, userId, function (error,contacts) {

        var personEmailIdArr = _.pluck(contacts,'personEmailId');

        if(!error && contacts){

            interactionObj.getLastInteractiondDate(userId,personEmailIdArr,function (err,interactedContacts) {
                var mergedContacts = _.map(contacts, function(a) {
                    return _.extend(a, _.find(interactedContacts, function(b) {
                        if(common.isNumber(b._id)){
                            return a.mobileNumber === b._id;
                        } else {
                            return a.personEmailId === b._id;
                        }
                    }))
                });

                mergedContacts = _.sortBy(mergedContacts, function (c) {
                    return -c.lastInteractedDate
                });

                callback(null,mergedContacts)
            });

        } else {
            callback(error,null)
        }
    });

};

module.exports = ContactSupport;