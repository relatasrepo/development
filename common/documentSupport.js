
var userManagement = require('../dataAccess/userManagementDataAccess');
var documentManagement = require('../dataAccess/documentManagement');
var commonUtility = require('../common/commonUtility');
var emailSender = require('../public/javascripts/emailSender');
var winstonLog = require('../common/winstonLog');

var userManagementObj = new userManagement();
var documentManagementObj = new documentManagement();
var common = new commonUtility();
var emailSenders = new emailSender();
var logLib = new winstonLog();

var logger =logLib.getFilesErrorLogger();

function DocumentSupport(){

    this.storeNewDocument = function(userId,docInfo,shareWithArr,isMeeting,invitationId,callback){

        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1},function(error,profile){
            if(error){
                logger.info('Error in storeNewDocument():DocumentSupport 1 ',error)
                callback(1,null)
            }
            else{
                var docDetails = {
                    sharedBy:{
                        userId:profile._id,
                        emailId:profile.emailId,
                        firstName:profile.firstName+' '+profile.lastName
                    },
                    sharedWith:[],
                    awsKey:docInfo.awsKey,
                    documentName:docInfo.documentName,
                    documentUrl:docInfo.documentUrl,
                    access:docInfo.access == 'public' ? 'public': 'private',
                    sharedDate:new Date()
                };
                if(shareWithArr.length > 0){
                    userManagementObj.selectUserProfilesCustomQuery({emailId:{$in:shareWithArr}},{emailId:1,firstName:1,lastName:1},function(error,users){
                        if(error){
                            logger.info('Error in storeNewDocument():DocumentSupport 2 ',error);
                            callback(1,null)
                        }
                        else{
                            for(var j=0; j<users.length; j++){
                                docDetails.sharedWith.push({
                                    userId:users[j]._id,
                                    emailId:users[j].emailId,
                                    firstName:users[j].firstName+' '+users[j].lastName,
                                    accessStatus:true,
                                    sharedOn:new Date()
                                })
                            }
                            if(shareWithArr.length != docDetails.sharedWith.length){
                                for(var k=0; k<shareWithArr.length > 0; k++){
                                    var isExist = false;
                                   for(var l=0; l<docDetails.sharedWith.length; l++){
                                       if(docDetails.sharedWith[l].emailId == shareWithArr[k]){
                                            isExist = true;
                                       }
                                   }
                                    if(!isExist){
                                        docDetails.sharedWith.push({
                                            userId:'',
                                            emailId:shareWithArr[k],
                                            firstName:'',
                                            accessStatus:true,
                                            sharedOn:new Date()
                                        })
                                    }
                                }
                            }
                            storeDocument(userId,docDetails,isMeeting,invitationId,callback);
                        }
                    })
                }
                else{
                    storeDocument(userId,docDetails,isMeeting,invitationId,callback);
                }
            }
        })
    };

    this.addDocumentToMeeting = function(invitationId,document,docInfoForMeeting,shareWith,callback){
        addDocumentToMeeting(invitationId,document,docInfoForMeeting,shareWith,callback);
    };

    this.shareDocument = function(userId,docId,shareWithArr,sendConfirmMail,message,subject,headerImage,imageUrl,imageName,callback){
        var emailIdArr = [];
        if(shareWithArr.length > 0){
            for(var i=0; i<shareWithArr.length; i++){
                if(common.checkRequired(shareWithArr[i].emailId))
                   emailIdArr.push(shareWithArr[i].emailId)
            }
        }
        if(emailIdArr.length > 0){
            userManagementObj.selectUserProfilesCustomQuery({emailId:{$in:emailIdArr}},{firstName:1,lastName:1,emailId:1},function(error,users){
                if(error){
                    logger.info('Error in shareDocument():DocumentSupport ',error);
                    callback(error,null);
                }
                else if(common.checkRequired(users) && users.length > 0){
                    for(var j=0; j<shareWithArr.length; j++){
                        for(var user=0; user<users.length; user++){
                            if(shareWithArr[j].emailId == users[user].emailId){
                                shareWithArr[j].firstName = users[user].firstName+' '+users[user].lastName;
                                shareWithArr[j].userId = users[user]._id;
                            }
                        }
                    }

                    documentManagementObj.shareDocument(docId,shareWithArr,message,subject,function(error,doc,sharedWith){
                        if(sendConfirmMail){
                            sendDocShareConfirmationMail(userId,shareWithArr,message,subject,headerImage,imageUrl,imageName,doc);
                        }
                        callback(error,doc,shareWithArr);
                    });

                }
                else{
                    documentManagementObj.shareDocument(docId,shareWithArr,message,subject,function(error,doc,sharedWith){
                        if(sendConfirmMail){
                            sendDocShareConfirmationMail(userId,shareWithArr,message,subject,headerImage,imageUrl,imageName,doc);
                        }
                        callback(error,doc,shareWithArr);
                    });
                }
            })
        }
    }
}

function addDocumentToMeeting(invitationId,document,docInfoForMeeting,shareWith,callback){
    userManagementObj.updateInvitationWithDocument(invitationId,docInfoForMeeting,function(error,isUpdated,message){
        if(error || !isUpdated){
            logger.info('Error in :DocumentSupport addDocumentToMeeting() ',error);
            callback(2,null);
        }
        else{
            sendDocUploadConfirmationMails(document,docInfoForMeeting,shareWith);
            callback(false,document);
        }
    });
}

function storeDocument(userId,docDetails,isMeeting,invitationId,callback){

    userManagementObj.storeDocument(docDetails,function(error,document,message){
        if(error || !common.checkRequired(document)){
            logger.info('Error in :DocumentSupport storeDocument() ',error);
            callback(1,null);
        }
        else{
            var docInfoForMeeting =  {
                documentId: document._id,
                documentName: document.documentName,
                documentUrl: document.documentUrl,
                addedBy:userId,
                addedOn: new Date()
            };
            if(isMeeting){
                addDocumentToMeeting(invitationId,document,docInfoForMeeting,[],callback);
            }
            else{
                sendDocUploadConfirmationMails(document,docInfoForMeeting,[]);
                callback(false,document)
            }
        }
    });
}

function sendDocUploadConfirmationMails(docInfo,docInfoForMeeting,sharedWith){
    if(sharedWith.length > 0){
        docInfo.sharedWith = sharedWith;
    }
    if(docInfo.sharedWith.length > 0){
        for (var i=0; i<docInfo.sharedWith.length; i++){
            if(docInfo.sharedWith[i].userId != docInfo.sharedBy.userId){

                var mailDataToSend = {
                    uploadedBy:docInfo.sharedBy,
                    receivedBy:docInfo.sharedWith[i],
                    docInfo   :docInfoForMeeting
                };

                if(common.checkRequired(mailDataToSend.receivedBy)){
                    emailSenders.sendDocUploadConfirmationMailToReceiver(mailDataToSend);
                }
            }
        }
    }
}

function sendDocShareConfirmationMail(userId,shareWith,message,subject,headerImage,imageUrl,imageName,doc){
    if(common.checkRequired(shareWith) && shareWith.length > 0){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1},function(error,profile){
            if(!error && common.checkRequired(profile)){
                for(var i=0; i<shareWith.length; i++){
                    if(common.checkRequired(shareWith[i].userId)){

                        emailSenders.sendShareDocMailToRelatasUser({
                            emailId:shareWith[i].emailId,
                            receivedByFirstName:shareWith[i].firstName || '',
                            url:'/readDocument/'+doc._id,
                            sharedByEmail:profile.emailId,
                            sharedByFirstName:profile.firstName+' '+profile.lastName,
                            userMsg:message,
                            subject:subject,
                            headerImage:headerImage,
                            imageUrl:imageUrl,
                            imageName:imageName
                        });
                    }
                    else{

                        emailSenders.sendShareDocMailToNonRelatasUser({
                            emailId:shareWith[i].emailId,
                            url:'accessDocByEmail/'+shareWith[i].emailId+'/'+doc._id,
                            sharedWithName:shareWith[i].firstName || '',
                            sharedByEmail:profile.emailId,
                            sharedByFirstName:profile.firstName+' '+profile.lastName,
                            userMsg:message,
                            subject:subject,
                            headerImage:headerImage,
                            imageUrl:imageUrl,
                            imageName:imageName
                        });
                    }
                }
            }
        })
    }
}

module.exports = DocumentSupport;