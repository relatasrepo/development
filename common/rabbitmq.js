var amqp = require('amqplib/callback_api');
var appCredentials = require('../config/relatasConfiguration');
var appCredential = new appCredentials();

function rabbitmq(){this.producer = function(object, callback){
        var domain;
	console.log("rabbitmq --------");

        if(appCredential.getServerEnvironment() == 'LOCAL1'){
            domain = 'localhost';
        }
        else{
          //domain = '35.199.47.49'; //batch server IP Showcase Batch
            domain = '10.150.0.4'; //batch server IP Replace this with LIVE
        }
        var q = appCredential.getQueues();
        var processQueue = q.processQueue;
        console.log('connecting to batch server ---queue', processQueue, domain );
        amqp.connect('amqp://relatas:relatas@'+domain, function(error, conn) {

            console.log('trying  to batch server ---queue', processQueue, domain );
            if(error){
                console.log('failed  to batch server ---queue', processQueue, domain );
                callback(false);
            }
            else {
                   console.log('creating Channel', processQueue, domain );
                   conn.createChannel(function (err, ch) {
                    if (err) {
                   	console.log('creating Channel failed', processQueue, domain );
                        callback(false);
                    }
                    else {
                        ch.assertQueue(processQueue, {durable: true});
                        ch.sendToQueue(processQueue, new Buffer(JSON.stringify(object)), {persistent: true});
                        console.log('rabbitmq --------');
                        console.log('connected to batch server ---queue', processQueue, domain );
                        console.log(object);
                        callback(true);
                    }
                });
            }
        });
    }

    this.sendUserInfo = function(object, callback) {
        var recommendQueue = 'senderRecommendQueue';
        // var domain = '192.168.43.52';
        // var url = 'amqp://relatas:relatas@'+domain;
        // console.log("Url for connection:"+url);

        amqp.connect('amqp://localhost', function(error, conn) {
            if(error){
                console.log("connection error:"+error);                
                callback(false);
            }
            else {
                conn.createChannel(function(err, ch){
                    if(err) {
                        console.log("Error receiving data");
                        callback(false);
                    }
                    else {
                        ch.assertQueue(recommendQueue, {durable:false});
                        ch.sendToQueue(recommendQueue, new Buffer(JSON.stringify(object)), {persistent:true});
                        console.log("message sent successfully");                        
                        callback(true);
                    }
                });
            }
        });
    }

    this.getRecommendations = function(callback) {
        var queue = 'receiverRecommendQueue';
        // var domain = '192.168.43.52';
        // var url = 'amqp://relatas:relatas@'+domain;
        
        amqp.connect('amqp://localhost', function(err, conn){
            if(err) {
                callback(false);
            } else {
                conn.createChannel(function(err, ch){
                    if(err) {
                        callback(false);
                    } else {
                        ch.assertQueue(queue, {durable:false});
                        ch.consume(queue, function(message){
                            callback(message.content.toString());
                        }, {noAck:true});
                    }
                });
            }
            
        });

    }
}

module.exports = rabbitmq;
