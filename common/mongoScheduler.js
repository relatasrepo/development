
var Scheduler = require('mongo-scheduler');
var mongoose = require('mongoose');
var schedulerObj = null;


function startScheduler(handlerObj){
    if(mongoose.connection.readyState == 1){
        schedulerObj = new Scheduler(mongoose.connection,{pollInterval:600000},handlerObj);
    }
    else{
        setTimeout(function(){
            startScheduler(handlerObj)
        },1000)
    }
}

function MongoScheduler(){

}

MongoScheduler.prototype.startScheduler = function(handlerObj){
    startScheduler(handlerObj);
};

MongoScheduler.prototype.scheduleMeetingTimeExpireEvent = function(meetingEvent){
    scheduleEventTODO(meetingEvent);
};

function scheduleEventTODO(meetingEvent){
    var event = {name: meetingEvent.eventName, collection: meetingEvent.collectionName, after:meetingEvent.after, query:{invitationId:meetingEvent.invitationId}, data: meetingEvent.data};
    schedulerObj.schedule(event,function(a,b){

    });
}

MongoScheduler.prototype.removeSchedule = function(eventName){
    if(eventName != null && eventName != undefined){
        if(mongoose.connection.readyState == 1){
            var db = mongoose.connection.db;
            if(db != null && db != undefined){
                db.collection('scheduled_events',function(err,collection){
                    collection.remove({event:eventName},function(error,result){

                    })
                })
            }
        }
    }
};

MongoScheduler.prototype.updateSchedule = function(eventName,updateObj){

    if(eventName != null && eventName != undefined){

        if(mongoose.connection.readyState == 1){

            var db = mongoose.connection.db;
            if(db != null && db != undefined){

                db.collection('scheduled_events',function(err,collection){

                    collection.update({event:eventName},{$set:{"conditions.after":updateObj.after}},function(error,result){

                    })
                })
            }
        }
    }
};

module.exports = MongoScheduler;