
// initializing  the required module

var request = require('request');
var querystring = require("querystring");
var simple_unique_id = require('simple-unique-id');
var moment = require('moment-timezone');

var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var userManagement = require('../dataAccess/userManagementDataAccess');
var calendarPasswordManagement = require('../dataAccess/calendarPasswordManagement')
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var OutLookAPIModule = require('../common/outlookRecursiveModule');
var interactionManagement = require('../dataAccess/interactionManagement');
var contactClass = require('../dataAccess/contactsManagementClass');
var meetingManagement = require('../dataAccess/meetingManagement');
var ContactManagement = require('../dataAccess/contactManagement');

var meetingClassObj = new meetingManagement();
var contactManagementObj = new ContactManagement();
var common = new commonUtility();
var interactionManagementObj = new interactionManagement();
var contactObj = new contactClass();
var userManagements = new userManagement();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var endPoint = '';

var logLib = new winstonLog();
var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();
var _ = require("lodash");

// var v2_client_id = "2300ccc1-7db1-4971-a5d9-fd12e6b7b54b";

var credentials = {
    clientID: authConfig.OUTLOOK_clientID,
    clientSecret: authConfig.OUTLOOK_clientSecret,
    site: authConfig.OUTLOOK_site,
    authorizationPath: authConfig.OUTLOOK_authorizationPath,
    tokenPath: authConfig.OUTLOOK_tokenPath,
    redirectURI:authConfig.OUTLOOK_redirectURI
}
var oauth2Simple = require("simple-oauth2")(credentials);
function OfficeOutlook(){

}
/* New V2 app model authentication */

OfficeOutlook.prototype.authenticate_v2_appModel = function(req,res,redirectURI,state){
    var redirect = redirectURI || authConfig.OFFICE365_REDIRECT_URL;
    //var url = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=d075f207-c792-4e7e-b00f-73ed47f6fc86&redirect_uri='+querystring.escape(redirect)+'&response_type=code&resource=http%3A%2F%2Foutlook.office.com&scope=https%3A%2F%2Foutlook.office.com%2Fmail.read'

    var url = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize"+
        "?client_id="+v2_client_id+
        "&redirect_uri="+querystring.escape(redirect)+
        "&response_type=code"+
        // "&resource="+querystring.escape("https://outlook.office.com")+
        "&scope=offline_access "+querystring.escape(['https://outlook.office.com/Mail.Read','https://outlook.office.com/Mail.Send','https://outlook.office.com/Contacts.Read','https://outlook.office.com/Calendars.ReadWrite'].join(" "));
    res.redirect(url);
};

OfficeOutlook.prototype.authoriseCode_v2_appModel = function(req,res,redirectURI,callback){
    var redirect = redirectURI || authConfig.OFFICE365_REDIRECT_URL;
    if(checkRequired(req.query) && req.query.code){

        var url = 'https://login.microsoftonline.com/common/oauth2/v2.0/token';
        var result = querystring.escape(authConfig.OFFICE365_CLIENT_SECRETE);
        var body = 'client_id=11a7b59a-6c3e-41ac-8caa-88a1efe1b8ad'+
            '&client_secret=M775gJRVXkpkTDPrOBTyuOo'+
            '&code='+req.query.code+
            '&redirect_uri='+redirect+
            '&grant_type=authorization_code'
        //"&resource="+querystring.escape("https://outlook.office.com")

        var connectionOptions = {
            url: url,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body:body
        };

        request(connectionOptions, function ( error, r, status ) {

        })
    }
    else{
        if(checkRequired(req.query) && checkRequired(req.query.error)){
            loggerError.info('Error in office 365 authentication ',req.query);
        }

        callback(false);
    }
};

OfficeOutlook.prototype.authenticate = function(req,res,redirectURI,state){
    var redirect = redirectURI || authConfig.OFFICE365_REDIRECT_URL;
    var url = "https://login.windows.net/"+authConfig.OFFICE365_TENANT+"/oauth2/authorize"+
        "?client_id="+authConfig.OFFICE365_CLIENT_ID+
        "&client_secret="+authConfig.OFFICE365_CLIENT_SECRETE+
        "&resource="+authConfig.OFFICE365_RESOURCE+
        "&redirect_uri="+redirect+
        "&response_type=code" +
        "&state="+state;

    res.redirect(url);

};

OfficeOutlook.prototype.fetchContacts = function (token,callback) {
    var url = 'https://graph.microsoft.com/v1.0/me/contacts/$count';

    var connectionOptions = {
        url: url,
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer '+token
        }
    };

    request(connectionOptions, function ( error, r, status ) {

        if(!error && status){
            getContacts(status,token,callback);
        }
    });
}

function getContacts(top,token,callback){
    var url = 'https://graph.microsoft.com/v1.0/me/contacts';
    var endDateTime = moment().subtract(2, "days").toDate()
    var filter = '?$filter=lastModifiedDateTime%20ge%20'+moment(endDateTime).toISOString();
    url = url+filter;

    var connectionOptions = {
        url: url,
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer '+token
        }
    };

    request(connectionOptions, function ( error, r, status ) {
        if(!error){

            status = JSON.parse(status)
            callback({len:status.value.length,contacts:status})
        } else {
            callback(false)
        }
    });
}

OfficeOutlook.prototype.getEvents = function(){
    var url = ""

    request.get({url: url}, function (err, response, body) {

    });
};

OfficeOutlook.prototype.authoriseCode = function(req,res,redirectURI,callback){
    var redirect = redirectURI || authConfig.OFFICE365_REDIRECT_URL;
    if(checkRequired(req.query) && req.query.code){

        var url = 'https://login.windows.net/'+authConfig.OFFICE365_TENANT+'/oauth2/token';
        var result = querystring.escape(authConfig.OFFICE365_CLIENT_SECRETE);
        var body = 'grant_type=authorization_code'+
            '&redirect_uri='+redirect+
            '&client_id='+authConfig.OFFICE365_CLIENT_ID+
            '&client_secret='+result+
            '&code='+req.query.code+
            '&resource='+authConfig.OFFICE365_RESOURCE;

        var connectionOptions = {
            url: url,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body:body
        };

        request(connectionOptions, function ( error, r, status ) {

            if(checkRequired(status)){

                var tokenInfo = JSON.parse(status);
                if(checkRequired(tokenInfo.access_token)){
                    var accessToken = tokenInfo.access_token;

                    //getOffice365CalendarEvents(accessToken)
                    var profile = {};
                    profile.access_token = tokenInfo.access_token;
                    profile.refresh_token = tokenInfo.refresh_token;
                    profile.tokenInfo = tokenInfo;
                    try {
                        var stringUserInfo = new Buffer(accessToken.split('.')[1], 'base64').toString('utf-8');
                        profile.raw = stringUserInfo;
                        var objUserInfo = JSON.parse(stringUserInfo);
                        profile.rawObject = objUserInfo;
                        profile.username = objUserInfo.unique_name;
                        profile.displayName = objUserInfo.given_name + ' ' + objUserInfo.family_name;

                        callback(profile)
                    }
                    catch(exception) {
                        loggerError.info('Error in getting office 365 user profile '+exception);
                        callback(false);

                    }
                }else callback(false);
            }else callback(false);
        })
    }
    else{
        if(checkRequired(req.query) && checkRequired(req.query.error)){
            loggerError.info('Error in office 365 authentication ',req.query);
        }

        callback(false);
    }
};

//Naveen
//Generating new access token generates new refresh tokens as well. Update the whole outlook property once a new access token is generated.
//reference: https://blogs.msdn.microsoft.com/exchangedev/2014/03/25/using-oauth2-to-access-calendar-contact-and-mail-api-in-office-365-exchange-online/
OfficeOutlook.prototype.getNewAccessToken = function(refreshToken,userId,callback){

    var self = this;

    var url = credentials.site+credentials.tokenPath;

    var body = "client_id="+credentials.clientID+
        "&client_secret="+credentials.clientSecret+
        "&redirect_uri="+credentials.redirectURI+
        "&refresh_token="+refreshToken+
        "&grant_type=refresh_token";

    var connectionOptions = {
        url: url,
        method:'POST',
        body:body
    };

    request(connectionOptions, function ( error, r, status ) {

        if(checkRequired(status)){
            try{
                status = JSON.parse(status);
                if(common.checkRequired(status.error)){
                    loggerError.info('Error in getNewAccessToken():OfficeOutlook with Store',status);
                    callback(null);
                }
                else{
                    self.storePrimaryOutlookObject({_id:userId},status,callback)
                }
            }
            catch(e){
                loggerError.info('Error in parsing to JSON getNewAccessToken():OfficeOutlook ',e);
                callback(null)
            }
        }
        else{
            callback(null)
        }
    })
};

OfficeOutlook.prototype.getNewAccessTokenForMobileSync = function(refreshToken,userProfile,callback){

    var self = this;

    var url = credentials.site+credentials.tokenPath;

    var body = "client_id="+credentials.clientID+
        "&client_secret="+credentials.clientSecret+
        "&redirect_uri="+credentials.redirectURI+
        "&refresh_token="+refreshToken+
        "&grant_type=refresh_token";

    var connectionOptions = {
        url: url,
        method:'POST',
        body:body
    };

    request(connectionOptions, function ( error, r, status ) {

        if(checkRequired(status)){
            try{
                status = JSON.parse(status);
                if(common.checkRequired(status.error)){
                    loggerError.info('Error in getNewAccessToken():OfficeOutlook with Store',status);
                    callback(null);
                }
                else{
                    self.storePrimaryOutlookObject(userProfile,status,callback)
                }
            }
            catch(e){
                loggerError.info('Error in parsing to JSON getNewAccessToken():OfficeOutlook ',e);
                callback(null)
            }
        }
        else{
            callback(null)
        }
    })
};

OfficeOutlook.prototype.getMessageCollection = function(refreshToken,fromDate,callback){

    var url = "https://graph.microsoft.com/v1.0/me/MailFolders/Inbox/messages";

    if(fromDate){
        url = url+'?$filter=ReceivedDateTime%20ge%20'+moment(fromDate).format("YYYY-MM-DD");
    }

    var temporaryDataList = [];

    var outLookAPIRecursionMails = new OutLookAPIModule(refreshToken);
    outLookAPIRecursionMails.outLookAPIRecursion(url,temporaryDataList,callback);

    // request(connectionOptions, function ( error, r, status ) {
    //
    //     status = JSON.parse(status);
    //
    //     if (!error && status) {
    //         callback(error, status);
    //     } else {
    //         callback(error,[]);
    //     }
    // });
};

OfficeOutlook.prototype.getNewAccessTokenForNonPrimaryAccount = function(refreshToken,userId,outlookObjectId,callback){

    var self = this;

    var url = credentials.site+credentials.tokenPath;

    var body = "client_id="+credentials.clientID+
        "&client_secret="+credentials.clientSecret+
        "&redirect_uri="+credentials.redirectURI+
        "&refresh_token="+refreshToken+
        "&grant_type=refresh_token";

    var connectionOptions = {
        url: url,
        method:'POST',
        body:body
    };

    request(connectionOptions, function ( error, r, status ) {

        if(checkRequired(status)){
            try{
                status = JSON.parse(status);
                if(common.checkRequired(status.error)){
                    loggerError.info('Error in getNewAccessTokenForNonPrimaryAccount():OfficeOutlook ',status);
                    callback(null);
                }
                else{
                    self.storeOutlookObjectWithId({_id:userId},status,outlookObjectId,callback)
                }
            }
            catch(e){
                loggerError.info('Error in parsing to JSON getNewAccessToken():OfficeOutlook ',e);
                callback(null)
            }
        }
        else{
            callback(null)
        }
    })
};

OfficeOutlook.prototype.getNewAccessTokenWithoutStore = function(refreshToken,callback){

    var url = credentials.site+credentials.tokenPath;

    var body = "client_id="+credentials.clientID+
        "&client_secret="+credentials.clientSecret+
        "&redirect_uri="+credentials.redirectURI+
        "&refresh_token="+refreshToken+
        "&grant_type=refresh_token"

    var connectionOptions = {
        url: url,
        method:'POST',
        body:body
    };

    request(connectionOptions, function ( error, r, status ) {

        if(checkRequired(status)){
            status = JSON.parse(status);
            callback(status)
        }
        else{
            callback(null)
        }
    })
};

OfficeOutlook.prototype.validateOffice365Acc = function(user){
    if(common.checkRequired(user) && common.checkRequired(user.officeCalendar) && common.checkRequired(user.officeCalendar.refresh_token)){
        return true;
    }
    else return false;
};

// action = accept or decline
OfficeOutlook.prototype.acceptOrDeclineOfficeOutlookEvent = function(userId,token,eventId,action,callback){
    var url = "https://outlook.office365.com/api/v1.0/me/events/"+eventId+"/"+action;

    var body = "{}";
    var connectionOptions = {
        url: url,
        method:'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer '+token
        },
        body:body
    };
    request(connectionOptions, function ( error, r, status ) {
        if(checkRequired(status)){
            try{
                status = JSON.parse(status);
                if(common.checkRequired(status.error)){
                    loggerError.info('Error in acceptOrDeclineOfficeOutlookEvent():OfficeOutlook ',status, action);
                }
                else{

                }
                callback(status);
            }
            catch(e){
                loggerError.info('Error in parsing to JSON acceptOrDeclineOfficeOutlookEvent():OfficeOutlook ',e, action);
                callback(null)
            }
        }
        else{
            callback(null)
        }
    })
};

OfficeOutlook.prototype.discoverAPIService = function(token,callback){
    var connectionOptions = {
        url: 'https://api.office.com/discovery/v1.0/me/services',
        headers: {
            Authorization: 'Bearer '+token
        }
    };
    request(connectionOptions, function ( error, r, status ) {
        callback(status)
    })
};

//Naveen mails
OfficeOutlook.prototype.getEmailsMSGraphAPI = function(token,userProfile,callback){

    var fromDate;

    if(userProfile && userProfile.registeredUser){

        if(common.checkRequired(userProfile.lastMobileSyncDate)){
            if(userProfile.lastDataSyncDate>userProfile.lastMobileSyncDate){
                fromDate = userProfile.lastDataSyncDate
            } else {
                fromDate = userProfile.lastMobileSyncDate
            }
        } else {
            fromDate = userProfile.lastDataSyncDate
        }
    } else {
        fromDate = moment().subtract(90, "days").toDate()
    }

    //Getting too many fields. They may not be required.
    var select = '&$select=id,Subject,Sender,ReceivedDateTime,createdDateTime,lastModifiedDateTime,sentDateTime,sender,from,toRecipients,ccRecipients,bccRecipients,conversationId,isRead,internetMessageId';
    var filter = '?$filter=ReceivedDateTime%20ge%20'+moment(fromDate).format("YYYY-MM-DD");
    var url = 'https://graph.microsoft.com/v1.0/me/messages'+filter+select;
    var temporaryDataList = [];

    var outLookAPIRecursionMails = new OutLookAPIModule(token);
    outLookAPIRecursionMails.outLookAPIRecursion(url,temporaryDataList,callback);
};

OfficeOutlook.prototype.getSentEmailsMSGraphAPI = function(token,userProfile,callback){

    var fromDate;

    if(userProfile && userProfile.registeredUser){

        if(common.checkRequired(userProfile.lastMobileSyncDate)){
            if(userProfile.lastDataSyncDate>userProfile.lastMobileSyncDate){
                fromDate = userProfile.lastDataSyncDate
            } else {
                fromDate = userProfile.lastMobileSyncDate
            }
        } else {
            fromDate = userProfile.lastDataSyncDate
        }
    } else {
        fromDate = moment().subtract(90, "days").toDate()
    }

    // fromDate = moment().subtract(1, "days").toDate()

    //Getting too many fields. They may not be required.
    var select = '&$select=id,Subject,Sender,ReceivedDateTime,createdDateTime,lastModifiedDateTime,sentDateTime,sender,from,toRecipients,ccRecipients,bccRecipients,conversationId,isRead,internetMessageId';
    var filter = '?$filter=ReceivedDateTime%20ge%20'+moment(fromDate).format("YYYY-MM-DD");
    var top = '&$top=1000';

    var url = 'https://graph.microsoft.com/v1.0/me/MailFolders/sentitems/messages'+filter+select+top;
    var temporaryDataList = [];

    var outLookAPIRecursionMails = new OutLookAPIModule(token);
    outLookAPIRecursionMails.outLookAPIRecursion(url,temporaryDataList,callback);
};

OfficeOutlook.prototype.getInboxEmailsMSGraphAPI = function(token,userProfile,callback){

    var fromDate = moment().subtract(90, "days").toDate();

    if(userProfile && userProfile.registeredUser){

        if(common.checkRequired(userProfile.lastMobileSyncDate)){
            if(userProfile.lastDataSyncDate>userProfile.lastMobileSyncDate){
                fromDate = userProfile.lastDataSyncDate
            } else {
                fromDate = userProfile.lastMobileSyncDate
            }
        } else {
            fromDate = userProfile.lastDataSyncDate
        }
    }

    // fromDate = moment().subtract(1, "days").toDate()

    //Getting too many fields. They may not be required.
    var filter = '?$filter=ReceivedDateTime%20ge%20'+moment(fromDate).format("YYYY-MM-DD");
    var select = '&$select=id,Subject,Sender,ReceivedDateTime,createdDateTime,lastModifiedDateTime,sentDateTime,sender,from,toRecipients,ccRecipients,bccRecipients,conversationId,isRead,internetMessageId';
    var top = '&$top=1000';

    var url = 'https://graph.microsoft.com/v1.0/me/MailFolders/Inbox/messages'+filter+select+top;
    var temporaryDataList = [];

    var outLookAPIRecursionMails = new OutLookAPIModule(token);
    outLookAPIRecursionMails.outLookAPIRecursion(url,temporaryDataList,callback);
};

OfficeOutlook.prototype.getCalendarEventsMSGraphAPI = function(token,userProfile,callback){

    var startDateTime;

    if(common.checkRequired(userProfile.lastMobileSyncDate)){
        if(userProfile.lastDataSyncDate>userProfile.lastMobileSyncDate){
            startDateTime = userProfile.lastDataSyncDate
        } else {
            startDateTime = userProfile.lastMobileSyncDate
        }
    } else {
        startDateTime = userProfile.lastDataSyncDate
    }

    var endDateTime = moment().add(15, "days").toDate();

    //Getting too many fields. They may not be required.
    // var select = '&$select=id,Subject,createdDateTime,lastModifiedDateTime,responseStatus,iCalUId,bodyPreview,start,end,location,isCancelled,attendees,organizer,IsOrganizer';
    var filter = '?startDateTime='+moment(startDateTime).format("YYYY-MM-DD") + '&endDateTime='+moment(endDateTime).format("YYYY-MM-DD");
    // var url = 'https://graph.microsoft.com/v1.0/me/calendar/events'+filter+select;
    var url = 'https://graph.microsoft.com/v1.0/me/calendar/events'+filter;
    // var url = 'https://graph.microsoft.com/v1.0/me/events';
    var temporaryDataList = [];

    var outLookAPIRecursionMeetings = new OutLookAPIModule(token);
    outLookAPIRecursionMeetings.outLookAPIRecursion(url,temporaryDataList,callback);
};

OfficeOutlook.prototype.getCalendarEventsMSGraphAPIV2 = function(token,userProfile,callback){

    var startDateTime;

    if(common.checkRequired(userProfile.lastMobileSyncDate)){
        if(userProfile.lastDataSyncDate>userProfile.lastMobileSyncDate){
            startDateTime = userProfile.lastDataSyncDate
        } else {
            startDateTime = userProfile.lastMobileSyncDate
        }
    } else {
        startDateTime = userProfile.lastDataSyncDate
    }

    var endDateTime = moment().add(15, "days").toDate();

    //Getting too many fields. They may not be required.
    // var select = '&$select=id,Subject,createdDateTime,lastModifiedDateTime,responseStatus,iCalUId,bodyPreview,start,end,location,isCancelled,attendees,organizer,IsOrganizer';
    var filter = '?startDateTime='+moment(startDateTime).format("YYYY-MM-DD") + '&endDateTime='+moment(endDateTime).format("YYYY-MM-DD");
    // var url = 'https://graph.microsoft.com/v1.0/me/calendar/calendarView'+filter+select;
    var url = 'https://graph.microsoft.com/v1.0/me/calendar/calendarView'+filter;
    var temporaryDataList = [];

    var outLookAPIRecursionMeetings = new OutLookAPIModule(token);
    outLookAPIRecursionMeetings.outLookAPIRecursion(url,temporaryDataList,callback);
};

OfficeOutlook.prototype.getMeetingObjectFromOutlookCalendar = function (userProfile,calendarEvents,callback) {

    var eventsArray = [];
    var attendeesTemp = [];
    var attendeesArr = [];
    var recurringEventsIdArray = [];
    _.each(calendarEvents,function (events) {
        _.each(events,function (event) {

            var meetingObj;
            if(event.recurrence) {
                meetingObj = common.buildOutlookMeetingObject(userProfile,event)
                eventsArray.push(meetingObj.meeting);
                attendeesTemp.push(meetingObj.attendees);
                recurringEventsIdArray.push({id:event.id,recurrence:event.recurrence});
            } else {
                meetingObj = common.buildOutlookMeetingObject(userProfile,event)
                eventsArray.push(meetingObj.meeting);
                attendeesTemp.push(meetingObj.attendees);
            }
        });
    });

    _.each(attendeesTemp,function (attendees) {
        _.each(attendees,function (a) {
            attendeesArr.push(a);
        });
    });

    attendeesArr = _.uniq(attendeesArr)

    //Map each receiver and sender to Relatas User profile.
    var projection = {firstName:1,lastName:1,emailId:1,profilePicUrl:1,publicProfileUrl:1,designation:1,location:1,mobileNumber:1,_id:1};
    userManagements.selectUserProfilesCustomQuery({emailId:{$in:attendeesArr}},projection,function(error,relatasUsers){

        if(!error && relatasUsers && relatasUsers.length > 0){
            _.each(relatasUsers,function (rUser) {
                _.each(eventsArray,function (event) {
                    //Map senders
                    if(event.senderEmailId == rUser.emailId){
                        event.senderEmailId = rUser.emailId
                        event.senderId = rUser._id.toString()
                        event.senderName = rUser.firstName+' '+rUser.lastName
                        event.senderPicUrl = rUser.profilePicUrl
                        event.mobileNumber = rUser.mobileNumber
                        event.location = rUser.location
                        event.publicProfileUrl = rUser.publicProfileUrl
                        event.designation = rUser.designation
                    }

                    //Now Receivers
                    _.each(event.toList,function (receiver) {
                        if(receiver.receiverEmailId == rUser.emailId){
                            receiver.receiverEmailId = rUser.emailId
                            receiver.receiverId = rUser._id.toString()
                            receiver.receiverFirstName = rUser.firstName
                            receiver.receiverLastName = rUser.lastName
                            receiver.receiverPicUrl = rUser.profilePicUrl
                            receiver.mobileNumber = rUser.mobileNumber
                            receiver.location = rUser.location
                            receiver.publicProfileUrl = rUser.publicProfileUrl
                            receiver.designation = rUser.designation
                        }
                    });
                });
            });
        }
        callback(eventsArray,relatasUsers)
    });
};

OfficeOutlook.prototype.insertMeetingsInBulk = function (userPro,meetings,req,users,callback) {

    meetingClassObj.bulkMeetingsUpdate(userPro.emailId,meetings,function (nonExistingMeetings) {

        if (req) {
            req.session.meetingsWrittenGlobal = true;
            req.session.save();
        }

        var usersDictionary = {},
            usersEmailIdObj = [];

        _.each(users,function (user) {
            usersEmailIdObj[user.emailId] = user.emailId;
            usersDictionary[String(user._id)] = user
        });

        if (nonExistingMeetings.length > 0) {

            logger.info("Non Existing Meetings - ", nonExistingMeetings.length);
            var len = nonExistingMeetings.length;

            var refIdArr = [];

            var timezone = "UTC";

            if(userPro.timezone && userPro.timezone.name){
                timezone = userPro.timezone.name
            }

            var meetingInteractions = [];
            for (var i = 0; i < len; i++) {
                meetingInteractions.push(common.createMeetingInteractionsObjects(nonExistingMeetings[i], false, 'outlook',timezone));
                refIdArr.push(nonExistingMeetings[i].invitationId)
            }

            var interactionsToSave = [];

            _.each(meetingInteractions,function (meetingInteractions) {
                var interactionsForUsers = _.uniq(_.compact(_.pluck(meetingInteractions, "userId")))
                _.each(interactionsForUsers,function (userId) {
                    var userDetails = usersDictionary[userId];
                    _.each(meetingInteractions,function (el) {
                        el["userId"] = el.userId?common.castToObjectId(el.userId):null
                        el["ownerId"] = userDetails._id
                        el["ownerEmailId"] = userDetails.emailId
                        el["companyId"] = userPro.companyId?common.castToObjectId(String(userPro.companyId)):null;
                        interactionsToSave.push(_.clone(el))
                    });
                });
            });

            interactionManagementObj.insertInteractions(common.castToObjectId(String(userPro._id)),interactionsToSave,userPro,function () {

                if (callback) {
                    callback(true);
                }
            });
        } else {

            if (callback) {
                callback(true);
            }
        }

        if(meetings.length>0){
            meetingClassObj.updateScheduleTimeSlotsBulk(meetings,function (forMeetingInteractionUpdate) {
                interactionManagementObj.updateInteractionDateById(forMeetingInteractionUpdate,usersEmailIdObj,function (result) {
                });
            });
        }
    });
}

OfficeOutlook.prototype.insertRecurringMeetingsInBulk = function (userEmailId,recurringMeetingsArr) {
    meetingClassObj.recurringBulkMeetingsUpdate(userEmailId,recurringMeetingsArr,function (existingMeetings) {

        if(existingMeetings.length>0){

            logger.info("recurring Meetings - ",existingMeetings.length);
            var len = existingMeetings.length;

            var meetingInteractions = [];
            for(var i=0;i<len;i++) {
                meetingInteractions.push(common.createRecurringMeetingInteractionsObjects(existingMeetings[i],false,'outlook'));
            }

            var meetingsLen = meetingInteractions.length;
            // var meetingsLen = 0;

            if(meetingsLen>0){
                for(var j=0;j<meetingsLen;j++){
                    var addInteractionsForUsers = _.compact(_.pluck(meetingInteractions[j],"userId"));

                    addInteractionsForUsers.forEach(function(userId) {
                        interactionManagementObj.updateMeetingsAsInteractions(common.castToObjectId(userId),meetingInteractions[j],function(result){
                            if(result){
                                logger.info("recurring Meeting Interactions Successfully updated for - ",userId)
                            } else {
                                logger.info("recurring Meeting Interactions Not updated for - ",userId)
                            }
                        });
                    });
                }
            }
        }
        // meetingClassObj.updateScheduleTimeSlotsBulk(existingMeetings);
    });
}

OfficeOutlook.prototype.getContactsMSGraphAPI = function(token,lastLogin,callback){
    // var url = 'https://graph.microsoft.com/v1.0/me/contacts';
    var temporaryDataList = [];

    var select = '$select=id,createdDateTime,emailAddresses,jobTitle,companyName,mobilePhone,givenName,surname,displayName,officeLocation';
    var filter = '?$filter=lastModifiedDateTime%20ge%20'+moment(lastLogin).toISOString();
    var top = '&$top=1000'

    var url = 'https://graph.microsoft.com/v1.0/me/contacts?'+select+top;

    //Commented this until all the outlook users' contacts are created in the new schema.
    // if(lastLogin){
    //     url = 'https://graph.microsoft.com/v1.0/me/contacts'+filter+'&'+select+top;
    // }

    var outLookAPIRecursionContacts = new OutLookAPIModule(token);
    outLookAPIRecursionContacts.outLookAPIRecursion(url,temporaryDataList,callback);
};

OfficeOutlook.prototype.writeMeetingOnCalendarMSGraphAPI = function(token,meeting,callback) {
    var outLookAPIMeetings = new OutLookAPIModule(token);
    var url = 'https://graph.microsoft.com/v1.0/me/events'

    outLookAPIMeetings.writeMeeting(url,meeting,callback)
}

OfficeOutlook.prototype.updateMeetingOnCalendarMSGraphAPI = function(token,update,id,callback) {
    var outLookAPIMeetings = new OutLookAPIModule(token);
    var url = 'https://graph.microsoft.com/v1.0/me/events/'+id;

    outLookAPIMeetings.updateMeeting(url,update,callback)
}

OfficeOutlook.prototype.deleteMeetingOnCalendarMSGraphAPI = function(token,id,callback) {
    var outLookAPIMeetings = new OutLookAPIModule(token);
    var url = 'https://graph.microsoft.com/v1.0/me/events/'+id;

    outLookAPIMeetings.deleteMeeting(url,callback)
}

OfficeOutlook.prototype.declineMeetingOnCalendarMSGraphAPI = function(token,message,id,callback) {
    var outLookAPIMeetings = new OutLookAPIModule(token);
    var url = 'https://graph.microsoft.com/v1.0/me/events/'+id+'/decline';

    outLookAPIMeetings.declineMeeting(url,message,callback)
}

OfficeOutlook.prototype.confirmOutlookMeetingMSGraphAPI = function(token,id,message,callback) {
    var outLookAPIMeetings = new OutLookAPIModule(token);
    var url = 'https://graph.microsoft.com/v1.0/me/events/'+id+'/accept';

    outLookAPIMeetings.confirmMeeting(url,message,callback)
}

OfficeOutlook.prototype.createOutlookCalEvent = function(token,meetingDetails,callback){

    var url = "https://outlook.office365.com/api/v1.0/me/events";
    //var url = "https://outlook.office365.com/api/v1.0/me/calendars/primary/events";

    var a = {
        "Subject": meetingDetails.title,
        "Body": {
            "ContentType": "HTML",
            "Content": meetingDetails.description
        },
        "Location":{
            "DisplayName":meetingDetails.location+":"+meetingDetails.locationType
        },
        "Start": moment(meetingDetails.start).format(),
        "End": moment(meetingDetails.end).format(),
        "Attendees": [
            {
                "EmailAddress": {
                    "Address": meetingDetails.senderPrimaryEmail || meetingDetails.senderEmailId,
                    "Name": meetingDetails.senderName
                },
                "Status":{
                    "Response":"Accepted",
                    "Time":moment().format()
                }
            }
        ]
    };

    var body = JSON.stringify(a);

    var connectionOptions = {
        url: url,
        method:'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer '+token
        },
        body:body
    };
    request(connectionOptions, function ( error, r, status ) {

        if(checkRequired(status)){
            try{
                status = JSON.parse(status);
                if(common.checkRequired(status.error)){
                    loggerError.info('Error in createOutlookCalEvent():OfficeOutlook ',status);
                    callback(status,null);
                }
                else{
                    callback(null,status);
                }
            }
            catch(e){
                loggerError.info('Error in parsing to JSON createOutlookCalEvent():OfficeOutlook ',e);
                callback(e,null)
            }
        }
        else{
            callback(true,null)
        }
    })
};

OfficeOutlook.prototype.newOutlookAuthenticate = function (redirectUri,callback) {

    var scopes = ['openid','offline_access','User.Read','Contacts.Read','Mail.ReadWrite','Mail.Send','Calendars.ReadWrite'];

    var returnVal = oauth2Simple.authCode.authorizeURL({
        redirect_uri: redirectUri,
        scope: scopes.join(" ")
    });

    callback(returnVal)

};

OfficeOutlook.prototype.getUserProfileMSGraphAPI = function (token,callback) {

    var url = 'https://graph.microsoft.com/v1.0/me';
    var contentType = 'application/json';
    getDataWithConnectionParameters(url,token,contentType,callback);

};

OfficeOutlook.prototype.getUserProfilePictureMSGraphAPI = function (token,callback) {

    // var id = <id|userPrincipalName>
    var url = 'https://graph.microsoft.com/v1.0/me/photo';
    var outLookAPIGetProfilePicture = new OutLookAPIModule(token);
    outLookAPIGetProfilePicture.getPhoto(url,callback)

};

function getDataWithConnectionParameters(url,token,contentType,callback) {

    var connectionOptions = {
        url: url,
        method:'GET',
        headers: {
            'Content-Type': contentType,
            Authorization: 'Bearer '+token
        }
    };

    request(connectionOptions, function ( error, r, status ) {
        if(!error){
            status = JSON.parse(status)
            callback(status)
        } else {
            callback(false)
        }
    });
}

OfficeOutlook.prototype.getOutlookTokenFromCode = function (auth_code,redirectUri,res,callback) {

    var url = credentials.site+credentials.tokenPath;
    var body = 'client_id='+credentials.clientID+
        '&client_secret='+credentials.clientSecret+
        '&code='+auth_code+
        '&redirect_uri='+redirectUri+
        '&grant_type=authorization_code'

    var connectionOptions = {
        url: url,
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body:body
    };

    request(connectionOptions, function ( error, r, token ) {
        if (error) {
            logger.info("Access token error: ", error.message);
            callback(false);
        } else {
            token = JSON.parse(token);
            callback(token);
        };
    });
};

OfficeOutlook.prototype.getUserProfileFromIdToken = function(id_token) {
    // JWT is in three parts, separated by a '.'

    var token_parts = id_token.split('.');

    // Token content is in the second part, in urlsafe base64
    var encoded_token = new Buffer(token_parts[1].replace("-", "_").replace("+", "/"), 'base64');

    var decoded_token = encoded_token.toString();

    var jwt = JSON.parse(decoded_token);

    // Email is in the preferred_username field
    return jwt
};

OfficeOutlook.prototype.checkExistingUser = function (userEmailId,callback) {
    userManagements.findUserProfileByEmailId(userEmailId,callback)
};

OfficeOutlook.prototype.outLookAPIRecursion = function (url,token,temporaryDataList,callback) {

    var self = this;
    var thisToken = token;
    var thisCallback = callback;

    var connectionOptions = {
        url: url,
        headers: {
            Accept:'application/json',
            Authorization: 'Bearer '+token
        }
    };

    request(connectionOptions, function ( error, r, status ) {

        if(checkRequired(status)){
            try{
                status = JSON.parse(status);
                if(common.checkRequired(status.error)){
                    loggerError.info('Error in  ',url,status);
                } else {

                    temporaryDataList.push(status.value);

                    if(status['@odata.nextLink']){
                        self.outLookAPIRecursion(status['@odata.nextLink'],thisToken,temporaryDataList,thisCallback)
                    } else {
                        callback(temporaryDataList);
                    }
                }
            }
            catch(e){
                loggerError.info('Error in parsing to JSON- ',url,e);
                callback(false)
            }
        }
        else{
            callback(false)
        }
    })
};

OfficeOutlook.prototype.kickOffContactsProcess = function (outlookAccessToken,status,syncContacts,callback) {

    var fromDate;

    if(status && status.registeredUser){

        if(common.checkRequired(status.lastMobileSyncDate)){
            if(status.lastDataSyncDate>status.lastMobileSyncDate){
                fromDate = status.lastDataSyncDate
            } else {
                fromDate = status.lastMobileSyncDate
            }
        } else {
            fromDate = status.lastDataSyncDate
        }
    }

    this.getContactsMSGraphAPI(outlookAccessToken,fromDate,function (contactsNestedArray) {
        processContacts(status,contactsNestedArray,syncContacts,callback);
    });
};

OfficeOutlook.prototype.storePrimaryOutlookObject = function(userProfile,token,callback) {

    var query;
    if(userProfile._id){
        query = {
            "_id":common.castToObjectId(userProfile._id.toString())
        };
    } else if (userProfile.emailId){
        query = {
            "emailId":userProfile.emailId
        };
    }

    var obj = {
        "id": token.id?token.id:null,
        "token": token.access_token,
        "refreshToken": token.refresh_token,
        "emailId": userProfile.emailId?userProfile.emailId:null,
        "name": userProfile.firstName?userProfile.firstName:''+' '+userProfile.lastName?userProfile.lastName:'',
        "addedOn": new Date()
    };

    userManagements.customQueryUpdate(query, { $set: { outlook: [obj] } }, function (err,response){

        if(!err){
            logger.info("Successfully stored new access tokens for user ", userProfile.emailId)
            if(callback){
                callback(token.access_token)
            }
        } else {
            logger.info("Unsuccessfully stored new access tokens for user ", userProfile.emailId);
            if(callback){
                if(token.access_token){
                    callback(token.access_token)
                } else {
                    callback(false)
                }
            }
        }
    });
};

OfficeOutlook.prototype.storeOutlookObjectWithId = function(userProfile,token,outlookObjectId,callback) {

    var query;
    if(userProfile._id){
        query = {
            "_id":common.castToObjectId(userProfile._id.toString()),
            "outlook._id":common.castToObjectId(outlookObjectId.toString())
        };
    } else if (userProfile.emailId){
        query = {
            "emailId":userProfile.emailId,
            "outlook._id":common.castToObjectId(outlookObjectId.toString())
        };
    }

    var obj = {
        "outlook.$.id": token.id,
        "outlook.$.token": token.access_token,
        "outlook.$.refreshToken": token.refresh_token,
        "outlook.$.addedOn": new Date()
    };

    userManagements.customQueryUpdate(query, { $set: obj }, function (err,response){

        if(!err){
            logger.info("Successfully stored new access tokens for user ", userProfile)
            if(callback){
                callback(token.access_token)
            }
        } else {
            logger.info("Unsuccessfully stored new access tokens for user ", userProfile);
            if(callback){
                callback(false)
            }
        }
    });
};

OfficeOutlook.prototype.storeSecondaryOutlookObject = function(tokenProfile,token,sessionProfile,callback) {

    var query;
    if(sessionProfile._id){
        query = {
            '_id':common.castToObjectId(sessionProfile._id.toString()),
            'outlook.emailId':{$ne:tokenProfile.userPrincipalName}
        };
    } else if (sessionProfile.emailId){
        query = {
            'emailId':sessionProfile.emailId,
            'outlook.emailId':{$ne:tokenProfile.userPrincipalName}
        };
    }

    var obj = {
        id: null,
        token: token.access_token,
        refreshToken: token.refresh_token,
        emailId: tokenProfile.userPrincipalName,
        name: tokenProfile.displayName,
        addedOn: new Date(),
        isPrimary:false
    };

    var update = {
        $addToSet : {outlook:obj}
    };

    userManagements.customQueryUpdate(query, update, function (err,response) {

        if(!err){
            logger.info("Successfully stored secondary access tokens for user ", sessionProfile.emailId)
            if(callback){
                callback(token.access_token)
            }
        } else {
            logger.info("Unsuccessfully stored secondary access tokens for user ", sessionProfile.emailId);
            if(callback){
                if(token.access_token){
                    callback(token.access_token)
                } else {
                    callback(false)
                }
            }
        }
    });
}

OfficeOutlook.prototype.updateBasicProfile = function (userProfile,callback) {

    var query = {
        emailId: userProfile.userPrincipalName.toLowerCase()
    };

    var update = {
        $set:{
            firstName: userProfile.givenName,
            lastName: userProfile.surname,
            emailId: userProfile.userPrincipalName.toLowerCase()
        }
    };

    userManagements.customQueryUpdate(query, update, function (err,response) {
        if(!err){
            callback(true)
        } else {
            logger.info("Error in customQueryUpdate",err)
            callback(false)
        }
    });
}

OfficeOutlook.prototype.getMailsAsInteractions = function(mails,user,callback) {

    var interactions = [],emailIdArr = [],referenceIdArr = [],singleInteractionRecords=[];
    var userId = common.castToObjectId(user._id);

    emailIdArr.push(user.emailId);
    common.getInvalidEmailListFromDb(function (list) {
        _.each(mails, function (eachMailsArray) {
            _.each(eachMailsArray, function (mail) {

                referenceIdArr.push(mail.id)

                validateOutlookMail(mail, list, user.emailId, function (source) {

                    if(source){

                        var newInteraction = buildObjForNewEmailInteractions(user,mail,source);

                        singleInteractionRecords = singleInteractionRecords.concat(newInteraction)

                        if(source.userAction == 'receiver'){
                            if(source.allRecipients.length == 1) {
                                var interactionAsReceiver1 = common.buildOutlookInteractionObject(user, mail, source.allRecipients[0].emailId, 'receiver', userId, source.allRecipients[0].name, null,source.to,source.cc);
                                interactions.push(interactionAsReceiver1);
                            }else{ // if LIU is a part of BCC
                                _.each(source.allRecipients,function (rc) {
                                    if(rc.emailId != user.emailId){
                                        userId = null
                                    } else {
                                        userId = user._id
                                    }
                                    interactions.push(common.buildOutlookInteractionObject(user, mail, rc.emailId, 'receiver', userId, user.emailId, null,source.to,source.cc));
                                })
                            }

                            var interactionAsReceiver2 = common.buildOutlookInteractionObject(user, mail, source.from.emailId, 'sender', null, source.from.name, source.userToCCBcc,source.to,source.cc);
                            interactions.push(interactionAsReceiver2);
                            emailIdArr.push(source.from.emailId)
                        }
                        else if(source.userAction == 'sender' && source.allRecipients.length > 0){

                            var interactionAsSender1 = common.buildOutlookInteractionObject(user, mail, source.from.emailId, 'sender', userId, source.from.name, null,source.to,source.cc);
                            interactions.push(interactionAsSender1);
                            _.each(source.allRecipients, function (recipients) {
                                var interactionAsSender2 = common.buildOutlookInteractionObject(user, mail, recipients.emailId, 'receiver', null, recipients.name, null,source.to,source.cc);
                                interactions.push(interactionAsSender2);
                                emailIdArr.push(recipients.emailId)
                            });
                        }
                    }
                });
            });
        });

        callback(interactions, _.uniq(emailIdArr), referenceIdArr,singleInteractionRecords)
    });
};

function buildObjForNewEmailInteractions(userProfile,email,source){

    var interactions = [];

    var obj = {
        ownerEmailId:userProfile.emailId,
        ownerId:String(userProfile._id),
        user_twitter_id:null,
        user_twitter_name:null,
        firstName:null,
        lastName:null,
        publicProfileUrl:null,
        profilePicUrl:null,
        emailId:null,
        companyName:null,
        designation:null,
        location:null,
        mobileNumber:null,
        action:source.userAction,
        interactionType:"email",
        subType:"email",
        refId:email.id,
        source:"outlook-login",
        title:email.subject,
        description:null,
        interactionDate:new Date(email.receivedDateTime),
        endDate:null,
        createdDate:new Date(),
        trackId:null,
        emailContentId:email.emailContentId,
        googleAccountEmailId:userProfile.emailId,
        toCcBcc:source.userToCCBcc,
        emailThreadId:email.conversationId,
        to:source.to?source.to:[],
        cc:source.cc?source.cc:[],
        hasUnsubscribe:email.hasUnsubscribe,
        companyId:userProfile.companyId?String(userProfile.companyId):null
    };

    var recordsFor = [];

    _.each(source.to,function (el) {
        recordsFor.push(el.emailId)
    });

    _.each(source.cc,function (el) {
        recordsFor.push(el.emailId)
    })

    _.each(source.allRecipients,function (el) {
        recordsFor.push(el.emailId)
    })
    
    recordsFor.push(source.from.emailId)
    recordsFor = _.uniq(recordsFor);

    _.each(recordsFor,function (rc) {
        var intObj = _.cloneDeep(obj)
        if(rc!= userProfile.emailId){
            intObj.emailId = rc;
        }

        intObj.ownerId = common.castToObjectId(String(userProfile._id));
        intObj.companyId = userProfile.companyId?common.castToObjectId(String(userProfile.companyId)):null;
        if(intObj.emailId){
            interactions.push(intObj)
        }
    })

    return interactions;
}

OfficeOutlook.prototype.mapInteractionsEmailIdToRelatasUsers = function (interactions,emailIdArr,callback) {

    var query = {"emailId":{$in:emailIdArr}};
    var projection = {emailId:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,companyName:1,designation:1,mobileNumber:1,skypeId:1,location:1};

    userManagements.selectUserProfilesCustomQuery(query,projection,function(error,users){
        _.each(interactions,function (interaction) {
            _.each(users,function (user) {
                if(interaction.emailId == user.emailId){
                    interaction.userId = user._id;
                    interaction.firstName = user.firstName;
                    interaction.lastName = user.lastName;
                    interaction.publicProfileUrl = user.publicProfileUrl;
                    interaction.profilePicUrl = user.profilePicUrl;
                    interaction.companyName =  common.fetchCompanyFromEmail(user.emailId);
                    interaction.designation = user.designation;
                    interaction.location = user.location;
                    interaction.mobileNumber = user.mobileNumber;
                    interaction.emailId = user.emailId;
                }
            });
        });
        callback(interactions)
    });
}

OfficeOutlook.prototype.sendEmailMSGraphAPI = function(token,mailBody,callback){
    var outLookAPISendMail = new OutLookAPIModule(token);
    var url = 'https://graph.microsoft.com/v1.0/me/sendMail'
    outLookAPISendMail.sendEmail(url,mailBody,callback)
};

OfficeOutlook.prototype.createReplyEmailMSGraphAPI = function(token,mailBody,id,callback){
    var outLookAPISendMail = new OutLookAPIModule(token);

    var url = 'https://graph.microsoft.com/v1.0/me/messages/'+id+'/createReply'
    outLookAPISendMail.createReply(url,mailBody,callback)
};

OfficeOutlook.prototype.sendDraftMSGraphAPI = function(token,id,callback){
    var outLookAPISendMail = new OutLookAPIModule(token);

    var url = 'https://graph.microsoft.com/v1.0/me/messages/'+id+'/send'
    outLookAPISendMail.sendDraft(url,callback)
};

OfficeOutlook.prototype.updateDraftMSGraphAPI = function(token,id,mailBody,callback){
    var outLookAPISendMail = new OutLookAPIModule(token);

    var url = 'https://graph.microsoft.com/v1.0/me/messages/'+id
    outLookAPISendMail.updateDraft(url,mailBody,callback)
};

OfficeOutlook.prototype.replyToEmailMSGraphAPI = function(token,mailBody,id,callback){
    var outLookAPISendMail = new OutLookAPIModule(token);
    var url = 'https://graph.microsoft.com/v1.0/me/messages/'+id+'/reply'
    
    outLookAPISendMail.sendEmail(url,mailBody,callback)
};

OfficeOutlook.prototype.getEmailByIdMSGraphAPI = function(token,id,callback){
    var outLookAPIGetMail = new OutLookAPIModule(token);
    var url = 'https://graph.microsoft.com/v1.0/me/messages/'+id
    outLookAPIGetMail.getEmail(url,callback)
};

OfficeOutlook.prototype.getContactProfileImageMSGraph = function (url,token,callback) {
    var outLookAPIGetContactImage = new OutLookAPIModule(token);
    outLookAPIGetContactImage.getContactImage(url,callback)
}

/* office 365 events to relatas */
function parseAndStoreAsMeetings(events,user){
    if(common.checkRequired(events) && events.length > 0){
        for(var i=0; i<events.length; i++){
            if(!events[i].IsAllDay)
                convertOfficeMeetingsToRelatas(events[i],user.officeCalendar.emailId,user.emailId);
        }
    }
};

function processContacts(userProfile,contactsNestedArray,syncContacts,callback) {

    var contacts = [],contactsEmailIdArr = [],mobileNumbers = [];

    common.getInvalidEmailListFromDb(function (list) {
        _.each(contactsNestedArray,function (eachContactsArray) {
            _.each(eachContactsArray,function (contact) {

                if(contact && contact.emailAddresses.length>0 && contact.emailAddresses[0].address) {

                    var ok = common.isValidEmail(contact.emailAddresses[0].address, list);
                    if(ok){
                        var c = common.buildOutlookContactObject(contact);

                        contacts.push(c);
                        contactsEmailIdArr.push(c.personEmailId);
                        mobileNumbers.push(c.mobileNumber);
                    }
                }
            });
        });

        contactsEmailIdArr = _.uniq(contactsEmailIdArr);

        //Map each receiver and sender to Relatas User profile.
        var projection = {firstName:1,lastName:1,emailId:1,profilePicUrl:1,publicProfileUrl:1,designation:1,location:1,mobileNumber:1,_id:1,lastDataSyncDate:1};
        userManagements.selectUserProfilesCustomQuery({emailId:{$in:contactsEmailIdArr}},projection,function(error,relatasUsers){

            if(!error && relatasUsers && relatasUsers.length > 0){
                _.each(relatasUsers,function (rUser) {
                    _.each(contacts,function (contact) {

                        if(contact.mobileNumber && contact.mobileNumber.match(new RegExp(/[^0-9]/g))) {
                            contact.mobileNumber = contact.mobileNumber.replace(new RegExp(/[^0-9]/g), ''); //remove special character from mobile number
                        }

                        if(rUser.emailId == contact.personEmailId){
                            contact.personId = rUser._id.toString()
                            contact.personName = rUser.firstName+' '+rUser.lastName
                            contact.companyName = rUser.companyName || null
                            contact.designation = rUser.designation || null
                            contact.location = rUser.location || null
                            contact.verified = true
                            contact.relatasrUser = true
                            contact.relatasContact = true
                            contact.mobileNumber = rUser.mobileNumber || null
                            contact.skypeId = rUser.skypeId || null
                        }
                    });
                });
            }

            contacts = common.removeDuplicates_field(contacts, 'personEmailId')

            if(syncContacts){
                contactObj.addContactNotExistRefresh(common.castToObjectId(userProfile._id.toString()),userProfile.emailId,contacts,mobileNumbers,contactsEmailIdArr,'outlook',callback);
            } else {
                contactObj.addContactNotExist(common.castToObjectId(userProfile._id.toString()),userProfile.emailId,contacts,mobileNumbers,contactsEmailIdArr,'outlook',callback);
            }

            //This is for creating the new contact collection.
            contactManagementObj.insertContacts(common.castToObjectId(userProfile._id.toString()),userProfile.emailId,contactsEmailIdArr,contacts,userProfile.lastDataSyncDate,function (cErr,contactsList) {

            });
            //Set off another process to update contact name and image link
            // contactObj.updateContactNameAndProfileImage(common.castToObjectId(userProfile._id.toString()),contacts,common.castToObjectId,false);
        });
    });
}

function convertOfficeMeetingsToRelatas(event,officeEmailId,emailId){

    var meeting = {
        selfCalendar:true,
        suggested:false,
        readStatus:false,
        isGoogleMeeting:false,
        isOfficeOutlookMeeting:true,
        toList:[],
        scheduleTimeSlots:[],
        participants:[],
        docs:[]
    };

    if(common.checkRequired(event) && common.checkRequired(event.Attendees) && event.Attendees.length > 0 && !common.checkRequired(event.SeriesMasterId)){
        var organizer;
        var oOrganizer;
        if(common.checkRequired(event.Organizer) && common.checkRequired(event.Organizer.EmailAddress) && common.checkRequired(event.Organizer.EmailAddress.Address)){
            organizer = event.Organizer.EmailAddress.Address == officeEmailId ? emailId : event.Organizer.EmailAddress.Address;
            oOrganizer = event.Organizer.EmailAddress.Address;
            meeting.participants.push({emailId:organizer});
            meeting.senderId = '';
            meeting.senderName = getNameOfParticipant(event.Organizer.EmailAddress.Name);
            meeting.senderEmailId = organizer;
            meeting.senderPicUrl = '';

        }

        meeting.invitationId = event.Id;
        meeting.googleEventId = event.Id;
        meeting.scheduledDate = new Date(event.DateTimeCreated);
        var isAccepted = false;

        for(var attendee = 0; attendee<event.Attendees.length; attendee++){
            if(common.checkRequired(event.Attendees[attendee])){
                if(event.Attendees[attendee].EmailAddress.Address == oOrganizer){

                }
                else{
                    function isAcceptedMeeting(attendee,needAcceptDate){
                        if(!needAcceptDate){
                            if(common.checkRequired(attendee.Status)){
                                return attendee.Status.Response == "Accepted";
                            }
                            else return false;
                        }
                        else{
                            if(common.checkRequired(attendee.Status)){
                                return attendee.Status.Response == "Accepted" ? new Date(attendee.Status.Time) : "";
                            }
                            else return "";
                        }
                    }
                    function isCanceledMeeting(attendee,needDate){
                        if(!needDate){
                            if(common.checkRequired(attendee.Status)){
                                return attendee.Status.Response == "Declined";
                            }
                            else return false;
                        }
                        else{
                            if(common.checkRequired(attendee.Status)){
                                return attendee.Status.Response == "Declined" ? new Date(attendee.Status.Time) : "";
                            }
                            else return "";
                        }
                    }
                    var to = {
                        receiverId:'',
                        receiverFirstName:getNameOfParticipant(event.Attendees[attendee].EmailAddress.Name),
                        receiverLastName:'',
                        receiverEmailId:event.Attendees[attendee].EmailAddress.Address == officeEmailId ? emailId : event.Attendees[attendee].EmailAddress.Address,
                        isAccepted:isAcceptedMeeting(event.Attendees[attendee],false),
                        canceled:isCanceledMeeting(event.Attendees[attendee],false)
                    };
                    if(to.canceled && common.checkRequired(isCanceledMeeting(event.Attendees[attendee],true))){
                        to.canceledDate = isCanceledMeeting(event.Attendees[attendee],true);
                    }
                    if(to.isAccepted && common.checkRequired(isAcceptedMeeting(event.Attendees[attendee],true))){
                        to.acceptedDate = isAcceptedMeeting(event.Attendees[attendee],true);
                    }

                    if(to.receiverEmailId != organizer)
                        meeting.participants.push({emailId:to.receiverEmailId})

                    meeting.toList.push(to)
                    if(!isAccepted)
                        isAccepted = isAcceptedMeeting(event.Attendees[attendee],false)
                }
            }
        }
        var slot =  {
            isAccepted: isAccepted,
            start: {
                date: new Date(event.Start)
            },
            end: {
                date: new Date(event.End)
            },
            title: event.Subject,
            location: getLocation(event.Location),
            suggestedLocation: '',
            locationType: "In-Person",
            description: event.BodyPreview || ""
        };
        meeting.scheduleTimeSlots.push(slot);

        //meetings.push(meeting)
        if(meeting.scheduleTimeSlots.length > 0 && meeting.toList.length > 0){
            mapUserProfilesToMeeting(meeting);
        }
    }
}

function getLocation(location){
    if(common.checkRequired(location) && common.checkRequired(location.DisplayName)){
        return location.DisplayName;
    }else return "";
}

function getNameOfParticipant(name){
    if(common.checkRequired(name) && !common.contains(name,'@')){
        return name;
    }else return "";
}

function mapUserProfilesToMeeting(meeting){
    userManagements.findUserProfileByEmailIdWithCustomFields(meeting.senderEmailId,{firstName:1,lastName:1,emailId:1,profilePicUrl:1},function(err,user){
        if(common.checkRequired(user)){
            meeting.senderEmailId = user.emailId
            meeting.senderId = user._id
            meeting.senderName = user.firstName+' '+user.lastName
            meeting.senderPicUrl = user.senderPicUrl
        }
        var receivers = [];
        for(var i=0; i<meeting.toList.length; i++){
            receivers.push(meeting.toList[i].receiverEmailId);
        }
        userManagements.selectUserProfilesCustomQuery({emailId:{$in:receivers}},{firstName:1,lastName:1,emailId:1},function(err,users){
            if(common.checkRequired(users) && users.length > 0){
                for(var user=0; user<users.length; user++){
                    for(var i=0; i<meeting.toList.length; i++){
                        if(meeting.toList[i].receiverEmailId == users[user].emailId){
                            meeting.toList[i].receiverId = users[user]._id
                            meeting.toList[i].receiverFirstName = users[user].firstName
                            meeting.toList[i].receiverLastName = users[user].lastName
                            meeting.toList[i].receiverEmailId = users[user].emailId
                        }
                    }
                }
            }

            validateAndStoreMeeting(meeting)
        })
    })
}

function validateAndStoreMeeting(meeting){
    userManagements.findInvitationByIdCustomFields(meeting.invitationId,{scheduledDate:0},true,function(invi){
        if(!common.checkRequired(invi)){
            userManagements.scheduleInvitationSelf(meeting,function(er,invi,msg){
                if(common.checkRequired(invi)){

                    var accepted = false;
                    for (var i = 0; i < invi.toList.length; i++) {
                        if (invi.toList[i].isAccepted) {
                            accepted = true;
                            storeInteractionGoogle(invi.toList[i].receiverId, invi.toList[i].receiverEmailId, 'receiver', invi.scheduleTimeSlots[0].start.date, invi.scheduleTimeSlots[0].end.date, invi.invitationId, invi.scheduleTimeSlots[0].title, invi.scheduleTimeSlots[0].description);
                            storeInteractionGoogle(invi.senderId, invi.senderEmailId, 'sender', invi.scheduleTimeSlots[0].start.date, invi.scheduleTimeSlots[0].end.date, invi.invitationId, invi.scheduleTimeSlots[0].title, invi.scheduleTimeSlots[0].description);
                        }
                        else {
                            interactionManagementObj.removeUserInteractionRefIdEmailId(invi.invitationId, invi.toList[i].receiverEmailId)
                        }
                    }

                    if (!accepted) {
                        interactionManagementObj.removeInteractions(invi.invitationId);
                    }
                }
            });
        }
        else{
            updateExistingGoogleMeeting(meeting,invi);
        }
    })
}

function updateExistingGoogleMeeting(meeting,invi){

    // Making update Slot
    var updateSlot = false;
    var slotOld = invi.scheduleTimeSlots[0]
    var slotNew = meeting.scheduleTimeSlots[0]

    var upSlot = {
        start:{},
        end:{}
    };
    if(slotOld.title != slotNew.title && common.checkRequired(slotNew.title)){
        updateSlot = true;
        upSlot.title = slotNew.title
    }
    else upSlot.title = slotOld.title

    if(slotOld.description != slotNew.description && common.checkRequired(slotNew.description)){
        updateSlot = true;
        upSlot.description = slotNew.description
    }
    else upSlot.description = slotOld.description

    if(new Date(slotOld.start.date) != new Date(slotNew.start.date) || new Date(slotOld.end.date) != new Date(slotNew.end.date)){
        updateSlot = true;
        upSlot.start.date = new Date(slotNew.start.date)
        upSlot.end.date = new Date(slotNew.end.date)
    }
    else upSlot.start.date = new Date(slotOld.start.date)


    upSlot.isAccepted = slotNew.isAccepted
    upSlot.messages = slotOld.messages
    //upSlot._id = slotOld._id

    if(slotOld.location != slotNew.location && common.checkRequired(slotNew.location)){
        updateSlot = true;
        upSlot.location = slotNew.location
    }
    else upSlot.location = slotOld.location

    if(slotOld.suggestedLocation != slotNew.suggestedLocation && common.checkRequired(slotNew.suggestedLocation)){
        updateSlot = true;
        upSlot.suggestedLocation = slotNew.suggestedLocation
    }
    else upSlot.suggestedLocation = slotOld.suggestedLocation

    if(slotOld.locationType != slotNew.locationType && common.checkRequired(slotNew.locationType)){
        updateSlot = true;
        upSlot.locationType = slotNew.locationType
    }
    else upSlot.locationType = slotOld.locationType



    // Making updated toList
    var newList = []
    var removeParticipants = false;

    if(meeting.toList.length > 0){

        newList = meeting.toList;
    }
    else{
        removeParticipants = true;
    }

    // Making updated participant List
    var newPList = []
    var removeParticipantsP = false;

    if(invi.participants.length > 0){

        newPList = meeting.participants;
    }
    else{
        removeParticipantsP = true;
    }

    // Making final update Obj
    var mUpdateObj = {

    };
    var updateFlagFinal = false;

    if(updateSlot){
        // update Meeting Slot
        mUpdateObj.$set = {
            scheduleTimeSlots:[upSlot]
        }
        updateFlagFinal = true;
    }

    //if(removeParticipants){
    if(common.checkRequired(mUpdateObj.$set)){
        mUpdateObj.$set.toList = newList
    }
    else{
        mUpdateObj.$set = {
            toList:newList
        }
    }
    updateFlagFinal = true;

    if(common.checkRequired(mUpdateObj.$set)){
        mUpdateObj.$set.participants = newPList
    }
    else{
        mUpdateObj.$set = {
            participants:newPList
        }
    }

    if(common.checkRequired(mUpdateObj.$set)){
        mUpdateObj.$set.readStatus = upSlot.isAccepted
    }
    else{
        mUpdateObj.$set = {
            readStatus:upSlot.isAccepted
        }
    }

    if (updateFlagFinal) {
        meetingClassObj.updateMeetingCustomObj(invi.invitationId, mUpdateObj, function (result) {
            if (result) {
                userManagements.findInvitationByIdCustomFields(invi.invitationId, {scheduledDate: 0}, false,function (invitation) {
                    if (common.checkRequired(invitation)) {

                        var accepted = false;
                        for (var i = 0; i < invitation.toList.length; i++) {
                            if (invitation.toList[i].isAccepted) {
                                accepted = true;
                                storeInteractionGoogle(invitation.toList[i].receiverId, invitation.toList[i].receiverEmailId, 'receiver', invitation.scheduleTimeSlots[0].start.date, invitation.scheduleTimeSlots[0].end.date, invitation.invitationId, invitation.scheduleTimeSlots[0].title, invitation.scheduleTimeSlots[0].description);
                                storeInteractionGoogle(invitation.senderId, invitation.senderEmailId, 'sender', invitation.scheduleTimeSlots[0].start.date, invitation.scheduleTimeSlots[0].end.date, invitation.invitationId, invitation.scheduleTimeSlots[0].title, invitation.scheduleTimeSlots[0].description);
                            }
                            else {
                                interactionManagementObj.removeUserInteractionRefIdEmailId(invitation.invitationId, invitation.toList[i].receiverEmailId)
                            }
                        }

                        if (!accepted) {
                            interactionManagementObj.removeInteractions(invitation.invitationId);
                        }
                    }
                })
            }
        });
    }
}

function storeInteractionGoogle(userId,emailId,action,interactionDate,endDate,refId,title,description){

    var objG = {
        userId:userId,
        emailId:emailId,
        action:action,
        interactionDate:interactionDate,
        endDate:endDate,
        type:'meeting',
        subType:'In-Person',
        refId:refId,
        source:'office365',
        title:title,
        description:description
    };
    common.storeInteractionReceiver(objG);

}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

var validateOutlookMail = function(mail, list, userEmailId, callback){
    var from;
    var allRecipients = [];
    var cc = [];
    var to = [];
    var userToCCBcc = null;
    var userAction;
    var source = null;

    if(mail.from && mail.from.emailAddress.address){
        if(mail.from.emailAddress.address.toLowerCase() == userEmailId){ //if LIU is sender
            userAction = "sender";
            from = {emailId:mail.from.emailAddress.address.toLowerCase(), name:mail.from.emailAddress.name};
            _.each(mail.toRecipients, function (recipient){

                if(recipient.emailAddress.address){
                    var ok = common.isValidEmail(recipient.emailAddress.address.toLowerCase(), list);
                    if(ok){
                        to.push({emailId:recipient.emailAddress.address.toLowerCase()})
                        allRecipients.push({emailId:recipient.emailAddress.address.toLowerCase(), name:recipient.emailAddress.name});
                    }
                }
            });
            _.each(mail.ccRecipients, function (recipient){

                if(recipient.emailAddress.address){
                    var ok = common.isValidEmail(recipient.emailAddress.address.toLowerCase(), list);
                    if(ok){
                        cc.push({emailId:recipient.emailAddress.address.toLowerCase()})
                        allRecipients.push({emailId:recipient.emailAddress.address.toLowerCase(), name:recipient.emailAddress.name});
                    }
                }
            });
            _.each(mail.bccRecipients, function (recipient){
                if(recipient.emailAddress.address){

                    var ok = common.isValidEmail(recipient.emailAddress.address.toLowerCase(), list);
                    if(ok){
                        allRecipients.push({emailId:recipient.emailAddress.address.toLowerCase(), name:recipient.emailAddress.name});
                    }
                }
            });
            source = {userAction:userAction, from:from, allRecipients:allRecipients, userToCCBcc:userToCCBcc};
            callback(source)
        }else if(mail.from && mail.from.emailAddress.address){ //if LIU is receiver
            var validSender = common.isValidEmail(mail.from.emailAddress.address.toLowerCase(), list);
            if(validSender){
                userAction = "receiver";
                from = {emailId:mail.from.emailAddress.address.toLowerCase(), name:mail.from.emailAddress.name};
                _.each(mail.toRecipients, function (recipient){
                    if(recipient.emailAddress.address && recipient.emailAddress.address.toLowerCase() == userEmailId){
                        userToCCBcc = mail.toRecipients.length == 1 ? "me" : "to";
                    }
                    if(recipient.emailAddress.address){
                        to.push({emailId:recipient.emailAddress.address.toLowerCase()})
                        allRecipients.push({emailId:recipient.emailAddress.address.toLowerCase(), name:recipient.emailAddress.name});
                    }
                });
                _.each(mail.ccRecipients, function (recipient){
                    // if(recipient.emailAddress.address.toLowerCase() == userEmailId){
                        userToCCBcc = "cc";
                        if(recipient.emailAddress.address){
                            allRecipients.push({emailId:recipient.emailAddress.address.toLowerCase(), name:recipient.emailAddress.name});
                            cc.push({emailId:recipient.emailAddress.address.toLowerCase()})
                        }
                    // }
                });
                _.each(mail.bccRecipients, function (recipient){
                    //EXECUTION WILL NOT HAPPENS: outlook api will not provide bcc list
                    // if(recipient.emailAddress.address.toLowerCase() == userEmailId){
                        userToCCBcc = "bcc";
                        if(recipient.emailAddress.address){
                            allRecipients.push({emailId:recipient.emailAddress.address.toLowerCase(), name:recipient.emailAddress.name});
                        }
                    // }
                });
                if(!userToCCBcc){
                    //since we don't get bcc list from outlook so if userToCCBcc still null update it to bcc
                    userToCCBcc = "bcc";
                }
                source = {userAction:userAction, from:from, allRecipients:allRecipients, userToCCBcc:userToCCBcc,to:to,cc:cc};
                callback(source)
            }
            else{
                callback(null)
            }
        }
    }
    else{
        callback(null)
    }
}

module.exports = OfficeOutlook;
