
var commonUtility = require('../common/commonUtility');
var meetingSupportClass = require('../common/meetingSupportClass');
var userManagement = require('../dataAccess/userManagementDataAccess');
var googleCalendarAPI = require('../common/googleCalendar');
var winstonLog = require('../common/winstonLog');
var taskManagementClass = require('../dataAccess/taskManagementClass');
var errorClass = require('../errors/errorClass');
var errorMessages = require('../errors/errorMessage');


var meetingSupportClassObj = new meetingSupportClass();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var taskManagementClassObj = new taskManagementClass();
var common = new commonUtility();
var googleCalendar = new googleCalendarAPI();
var userManagements = new userManagement();
var logLib = new winstonLog();

var statusCodes = errorMessagesObj.getStatusCodes();
var logger =logLib.getWinston();

function TaskSupport(){
    this.createNewTask = function(taskIn,userId,callback){
        this.mapTaskWithUser(taskIn,function(task){
            taskManagementClassObj.createTask(task,function(error,savedTask){
                if(error){
                    callback(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'},errorMessagesObj.getMessage("TASK_CREATE_FAILED")));
                }
                else if(common.checkRequired(savedTask)){

                    var interaction2 = {};
                    interaction2.userId = savedTask.createdBy;
                    interaction2.emailId = savedTask.createdByEmailId || '';
                    interaction2.firstName = '';
                    interaction2.lastName = '';
                    interaction2.publicProfileUrl = '';
                    interaction2.profilePicUrl = '';
                    interaction2.companyName = '';
                    interaction2.designation = '';
                    interaction2.location = '';
                    interaction2.mobileNumber = '';
                    interaction2.action = 'sender';
                    interaction2.type = 'task';
                    interaction2.subType = 'task';
                    interaction2.refId = savedTask._id;
                    interaction2.source = 'relatas';
                    interaction2.title = savedTask.taskName;
                    interaction2.description = savedTask.description || '';
                    interaction2.interactionDate = new Date(savedTask.dueDate);
                    //common.storeInteraction(interaction2);

                    var interaction3 = {};
                    interaction3.userId = savedTask.assignedTo;
                    interaction3.emailId = savedTask.assignedToEmailId || '';
                    interaction3.firstName = '';
                    interaction3.lastName = '';
                    interaction3.publicProfileUrl = '';
                    interaction3.profilePicUrl = '';
                    interaction3.companyName = '';
                    interaction3.designation = '';
                    interaction3.location = '';
                    interaction3.mobileNumber = '';
                    interaction3.action = 'receiver';
                    interaction3.type = 'task';
                    interaction3.subType = 'task';
                    interaction3.refId = savedTask._id;
                    interaction3.source = 'relatas';
                    interaction3.title = savedTask.taskName;
                    interaction3.description = savedTask.description || '';
                    interaction3.interactionDate = new Date(savedTask.dueDate);
                    meetingSupportClassObj.storeAcceptMeetingInteractions(interaction2,interaction3);

                    googleCalendar.createTaskEvent(userId,savedTask,function(error,result){
                        if(error){
                            logger.info('Error in createTaskEvent():tasks web router taskId: '+savedTask._id, error);
                        }
                        else {
                            taskManagementClassObj.updateTaskWithGoogleEventId(savedTask._id,result.id,function(err,isSuccess){
                                if(err){
                                    logger.info('Error in updateTaskWithGoogleEventId():tasks web router taskId: '+savedTask._id, err);
                                }
                            })
                        }
                    });
                    callback({
                        "SuccessCode": 1,
                        "Message": errorMessagesObj.getMessage("TASK_CREATED"),
                        "ErrorCode": 0,
                        "Data": {
                            task:savedTask
                        }
                    });
                }else callback(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'},errorMessagesObj.getMessage("TASK_CREATE_FAILED")));
            })
        });
    };

    this.mapTaskWithUser = function(task,callback){
        if(common.checkRequired(task.assignedTo) && common.checkRequired(task.assignedToEmailId)){
            callback(task);
        }
        else if(common.checkRequired(task.assignedTo) && !common.checkRequired(task.assignedToEmailId)){
            userManagements.findUserProfileByIdWithCustomFields(task.assignedTo,{emailId:1},function(err,user){
                if(common.checkRequired(user)){
                    task.assignedToEmailId = user.emailId;
                }
                callback(task);
            })
        }
        else if(!common.checkRequired(task.assignedTo) && common.checkRequired(task.assignedToEmailId)){
            userManagements.findUserProfileByEmailIdWithCustomFields(task.assignedToEmailId,{_id:1},function(err,user){
                if(common.checkRequired(user)){
                    task.assignedTo = user._id;
                }
                callback(task);
            })
        }else callback(task);
    }
}

TaskSupport.prototype.createNewTask_v2= function(task,user,updateCalendar,callback){
    taskManagementClassObj.createTask_v2(user,task,function(error,savedTask){
        callback(error,savedTask)
    })
}


module.exports = TaskSupport;