
var request = require('request');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var common = new commonUtility();
var logLib = new winstonLog();
var loggerError = logLib.getWinstonError();

function OutLookAPIModule(token){
    this.token = token;
};

OutLookAPIModule.prototype.outLookAPIRecursion = function(url,temporaryDataList,callback) {
    var token = this.token;
    fetchRequestedData(url,temporaryDataList,token,callback);
};

OutLookAPIModule.prototype.getPhoto = function (url,callback) {
    var token = this.token;
    var connectionOptions = {
        url: url,
        method:'GET',
        headers: {
            // 'Content-Type': 'application/json',
            Authorization: 'Bearer '+token
        }
    };

    request(connectionOptions, function ( error, r, status ) {

        if (!error && common.checkRequired(r.statusCode) && r.statusCode == 201) {
            callback(error, status)
        } else {
            callback(error,r.statusCode);
        }
    });
}

OutLookAPIModule.prototype.sendEmail = function (url,mailBody,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'POST',
        body:JSON.stringify(mailBody)
    };

    request(connectionOptions, function ( error, r, status ) {
        if (!error && common.checkRequired(r.statusCode) && r.statusCode == 202) {
            callback(error, true)
        } else {
            callback(error,r.statusCode);
        }
    });
};

OutLookAPIModule.prototype.createReply = function (url,mailBody,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'POST',
        body:JSON.stringify(mailBody)
    };

    request(connectionOptions, function ( error, r, status ) {
        if (!error && common.checkRequired(r.statusCode) && r.statusCode == 202) {
            callback(error, status)
        } else {
            callback(error,status);
        }
    });
};

OutLookAPIModule.prototype.updateDraft = function (url,mailBody,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'PATCH',
        body:JSON.stringify(mailBody)
    };

    request(connectionOptions, function ( error, r, status ) {
        if (!error && common.checkRequired(r.statusCode) && r.statusCode == 202) {
            callback(error, true)
        } else {
            callback(error,status);
        }
    });
};

OutLookAPIModule.prototype.sendDraft = function (url,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'POST'
    };

    request(connectionOptions, function ( error, r, status ) {
        if (!error && common.checkRequired(r.statusCode) && r.statusCode == 202) {
            callback(error, true)
        } else {
            callback(error,status);
        }
    });
};

OutLookAPIModule.prototype.writeMeeting = function (url,meeting,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'POST',
        body:JSON.stringify(meeting)
    };

    request(connectionOptions, function ( error, r, status ) {

        if (!error && common.checkRequired(r.statusCode) && r.statusCode == 201) {
            callback(error, status)
        } else {
            callback(error,r.statusCode);
        }
    });
}

OutLookAPIModule.prototype.updateMeeting = function (url,update,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'PATCH',
        body:JSON.stringify(update)
    };

    request(connectionOptions, function ( error, r, status ) {
        
        if (!error && common.checkRequired(r.statusCode) && r.statusCode == 200) {
            callback(error, true)
        } else {
            callback(error,r.statusCode);
        }
    });
}

OutLookAPIModule.prototype.deleteMeeting = function (url,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'DELETE'
    };

    request(connectionOptions, function ( error, r, status ) {

        if (!error && common.checkRequired(r.statusCode) && (r.statusCode == 200 || r.statusCode == 204)) {
            callback(error, true)
        } else {
            loggerError.info("Error in deleteMeeting(). URL is "+url+":::Error stack "+r.statusCode)
            callback(error,false);
        }
    });
}

OutLookAPIModule.prototype.declineMeeting = function (url,message,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'POST',
        body:JSON.stringify(message)
    };

    request(connectionOptions, function ( error, r, status ) {

        if (!error && common.checkRequired(r.statusCode) && (r.statusCode == 202 || r.statusCode == 204)) {
            callback(error, true)
        } else {
            loggerError.info("Error in declineMeeting(). URL is "+url+":::Error stack "+r.statusCode)
            callback(error,false);

        }
    });
}

OutLookAPIModule.prototype.confirmMeeting = function (url,message,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'POST',
        body:JSON.stringify(message)
    };

    request(connectionOptions, function ( error, r, status ) {

        if (!error && common.checkRequired(r.statusCode) && r.statusCode == 202) {
            callback(error, true)
        } else {
            callback(error,r.statusCode);
        }
    });
}

OutLookAPIModule.prototype.getEmail = function (url,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'GET'
    };

    request(connectionOptions, function ( error, r, status ) {

        status = JSON.parse(status);

        if (!error && common.checkRequired(r.statusCode) && r.statusCode == 200 && status) {
            callback(error, status.body.content)
        } else {
            callback(error,false);
        }
    });
};

OutLookAPIModule.prototype.getContactImage = function (url,callback) {
    var token = this.token;

    var connectionOptions = {
        url: url,
        headers: {
            'Content-Type':'application/json',
            'Authorization': 'Bearer '+token
        },
        method:'GET'
    };

    request(connectionOptions, function ( error, r, status ) {
        
        status = JSON.parse(status);

        if (!error && common.checkRequired(r.statusCode) && r.statusCode == 200 && status) {
            callback(error, status.body.content)
        } else {
            callback(error,false);
        }
    });
};

OutLookAPIModule.prototype.fetchContacts = function (url,token,callback) {
    var url = 'https://graph.microsoft.com/v1.0/me/contacts/$count';

    var connectionOptions = {
        url: url,
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer '+token
        }
    };

    request(connectionOptions, function ( error, r, status ) {

        if(!error && status){
            getContacts(status,token,callback);
        }
    });
}

function getContacts(token,callback){
    var url = 'https://graph.microsoft.com/v1.0/me/contacts?$top=1000';
    var connectionOptions = {
        url: url,
        method:'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer '+token
        }
    };

    request(connectionOptions, function ( error, r, status ) {
        if(!error){
            status = JSON.parse(status)
            callback({len:status.value.length,contacts:status})
        } else {
            callback(false)
        }
    });
}

function fetchRequestedData(url,temporaryDataList,token,callback) {

    var connectionOptions = {
        url: url,
        headers: {
            Accept:'application/json',
            Authorization: 'Bearer '+token
        }
    };

    request(connectionOptions, function ( error, r, status ) {

        if(!error && common.checkRequired(status)){

            status = JSON.parse(status);

            if(common.checkRequired(status.error)){

                loggerError.info('Error in  ',url,status.error);
                if(temporaryDataList.length>0){
                    callback(temporaryDataList)
                } else {
                    callback(false)
                }
            } else {
                temporaryDataList.push(status.value);

                if(status['@odata.nextLink']){
                    fetchRequestedData(status['@odata.nextLink'],temporaryDataList,token,callback)
                } else {
                    callback(temporaryDataList);
                }
            }
        }
        else{
            loggerError.info('Error in calling Outlook API - ',url,error);
            callback(false)
        }
    })
}

module.exports = OutLookAPIModule;