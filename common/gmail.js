
var request = require('request');
var simple_unique_id = require('simple-unique-id');
var _ = require("lodash");

var googleCalendarAPI = require('../common/googleCalendar');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var meetingSupportClass = require('../common/meetingSupportClass');

var appCredential = new appCredentials();
var googleCalendar = new googleCalendarAPI();
var logLib = new winstonLog();
var common = new commonUtility();
var meetingSupportClassObj = new meetingSupportClass();

var domain = appCredential.getDomain();
var logger =logLib.getGmailErrorLogger();
var domainName = domain.domainName;

function Gmail(){

    this.sendEmailWithToken = function(userId,token,googleEmailId,dataToSend,callback){
        sendMail(token,dataToSend,function(error,mail,trackId){
            if(mail){
                getEmail(userId,mail.id,token,dataToSend.senderEmailId,googleEmailId,trackId,dataToSend,false);
            }

            callback(error,mail)
        });
    };

    this.sendEmail = function(userId,token,refreshToken,googleEmailId,dataToSend,callback){
        googleCalendar.getNewGoogleTokenNew(token,refreshToken,function(token){
            if(token){

                sendMail(userId,token,dataToSend,function(error,mail,trackId){
                   if(mail){
                       getEmail(mail.id,token,dataToSend.senderEmailId,googleEmailId,trackId,dataToSend,function () {
                           callback(error,mail)
                       });
                   }
                });
            }
            else{
                callback(true,'TOKEN_ERROR');
            }
        })
    };

    this.getEmail = function(id,token,refreshToken,googleEmailId,callback){
        googleCalendar.getNewGoogleTokenNew(token,refreshToken,function(token){
            if(token){
                getEmailWithIdAndToken(id,token,callback);
            }
            else{
                callback(true,'TOKEN_ERROR');
            }
        })
    };
}

function getEmailWithIdAndToken(id,token,callback){
    request({
            method: "GET",
            uri: "https://www.googleapis.com/gmail/v1/users/me/messages/"+id+"?format=full&fields=payload",
            headers: {
                "Authorization": "Bearer "+token,
                "Content-Type": "application/json"
            }
        },
        function(err, response, body) {

            if(common.checkRequired(body)){
                body = JSON.parse(body);

                if(body.error){
                    err = body;
                    logger.info('Error in getEmailWithIdAndToken():Gmail ',err);
                }
                if(common.checkRequired(body.id)){
                    callback(err,body)
                }
                else {
                    callback(err,body)
                }
            }
            else {
                callback(err,body)
            }
        });
}

function getEmail(id,token,userEmailId,googleEmailId,trackId,dataToSend,callback){

    request({
            method: "GET",
            uri: "https://www.googleapis.com/gmail/v1/users/me/messages/"+id,
            headers: {
                "Authorization": "Bearer "+token,
                "Content-Type": "application/json"
            }
        },
        function(err, response, body) {

            if(common.checkRequired(body)){
                body = JSON.parse(body);
                if(body.error){
                    err = body;
                    logger.info('Error in getEmail():Gmail ',"userEmailId: "+userEmailId,"googleEmailId"+googleEmailId,err);
                }
                if(common.checkRequired(body.id)){
                    if(callback){
                        callback(body)
                    }
                    storeEmailToInteractions(body,userEmailId,googleEmailId,trackId,dataToSend);
                }
                else {
                    if(callback){
                        callback(false)
                    }
                }
            }
            else {
                if(callback){
                    callback(false)
                }
            }
        });
}

function sendMail(userId,token,dataToSend,callback){

    var trackId = simple_unique_id.generate(new Date().toISOString()+dataToSend.receiverEmailId);
    
    var url = domainName+'/track/email/open/'+dataToSend.receiverEmailId+'/track/id/'+trackId+'/user/'+userId;
    var track = "";
    if(dataToSend.trackViewed){
        track = "<img style='width:1px;height:1px' src="+url+">"
    }

    var recipients = dataToSend.receiverEmailId;

    if(dataToSend.email_cc && dataToSend.email_cc.length>0){
        _.each(dataToSend.email_cc,function (emailId) {
            recipients = recipients+","+emailId
        })
    }

    var encodedMail = new Buffer(
        "Content-Type: text/html; charset=\"UTF-8\"\n" +
        "MIME-Version: 1.0\n" +
        "Content-Transfer-Encoding: 7bit\n" +
        //"Message-ID :"+" <CAL+Kp_=RjfQxAVdGkBtTvSM-oyQHivF1N_1mB7crfPd0i8xrUA@mail.gmail.com>"+" \n" +
        "to: "+recipients+"\n" +
        "from:"+dataToSend.senderName+"<"+dataToSend.senderEmailId+">\n" +
        "subject: "+dataToSend.subject+"\n\n" +
        dataToSend.message.replace(/\n/g, "<br />")+track
    ).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');

    request({
            method: "POST",
            uri: "https://www.googleapis.com/gmail/v1/users/me/messages/send",
            headers: {
                "Authorization": "Bearer "+token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "raw": encodedMail
            })
        },
        function(err, response, body) {
            if(common.checkRequired(body)){
                body = JSON.parse(body);
                if(body.error){
                    err = body;
                    logger.info('Error in sendMail():Gmail ',dataToSend,err);
                }

                callback(err,body,trackId)
            }
            else callback(err,false,trackId)
        });
}

function storeEmailToInteractions(message,officeEmail,googleEmail,trackId,dataToSend){
    
    dataToSend.message = dataToSend.message.replace(/\n\n\n/g,'<br/>-------------------------------------------------<br/>');
    if(common.checkRequired(message) && common.checkRequired(message.payload) && common.checkRequired(message.payload.headers)){
        if(message.payload.headers.length > 0){
            var headers = message.payload.headers;
            var email = parseEmailHeader(message.id,headers);
            if(common.checkRequired(email) && common.checkRequired(email.id)){
               var interactionSender = null;
               var interactionReceiver = null;
               var interactionsArr = generateAndStoreInteraction(email,true,officeEmail,googleEmail,trackId,dataToSend.message,dataToSend.trackViewed,dataToSend.remind,dataToSend.docTrack);
               if(interactionsArr.length == 2){
                   for(var i=0; i<interactionsArr.length; i++){
                       if(interactionsArr[i].action == 'sender'){
                           interactionSender = JSON.parse(JSON.stringify(interactionsArr[i]));
                           if(interactionSender.emailId == dataToSend.senderEmailId){
                               interactionsArr[i].ownerId = dataToSend.senderId
                               interactionsArr[i].ownerEmailId = dataToSend.senderEmailId
                               interactionSender.userId = dataToSend.senderId
                           }
                           if(interactionSender.emailId == dataToSend.receiverEmailId){
                               interactionSender.userId = dataToSend.receiverId
                               interactionsArr[i].ownerId = dataToSend.receiverId
                               interactionsArr[i].ownerEmailId = dataToSend.receiverEmailId
                           }
                       }
                       else if(interactionsArr[i].action == 'receiver'){
                           interactionReceiver = JSON.parse(JSON.stringify(interactionsArr[i]));
                           if(interactionReceiver.emailId == dataToSend.senderEmailId){
                               interactionReceiver.userId = dataToSend.senderId
                               interactionsArr[i].ownerId = dataToSend.senderId
                               interactionsArr[i].ownerEmailId = dataToSend.senderEmailId
                           }
                           if(interactionReceiver.emailId == dataToSend.receiverEmailId){
                               interactionReceiver.userId = dataToSend.receiverId
                               interactionsArr[i].ownerId = dataToSend.receiverId
                               interactionsArr[i].ownerEmailId = dataToSend.receiverEmailId
                           }
                       }
                   }

                   if(interactionSender != null && interactionReceiver != null){
                       meetingSupportClassObj.storeAcceptMeetingInteractions(interactionSender,interactionReceiver);
                   }
                   else{
                       logger.info('Error in storeEmailToInteractions():Gmail sender or receiver not exist',officeEmail);
                   }
               }
               else{
                   for(var j=0; j<interactionsArr.length; j++){
                       if(interactionsArr[j].action == 'sender'){
                           interactionSender = JSON.parse(JSON.stringify(interactionsArr[j]));
                           if(interactionSender.emailId == dataToSend.senderEmailId){
                               interactionSender.userId = dataToSend.senderId
                           }
                           if(interactionSender.emailId == dataToSend.receiverEmailId){
                               interactionSender.userId = dataToSend.receiverId
                           }
                       }
                   }
                   var saved = true;
                   for(var k=0; k<interactionsArr.length; k++){
                      if(interactionsArr[k].action == 'receiver'){
                           interactionReceiver = JSON.parse(JSON.stringify(interactionsArr[k]));
                           if(interactionReceiver.emailId == dataToSend.senderEmailId){
                               interactionReceiver.userId = dataToSend.senderId
                           }
                           if(interactionReceiver.emailId == dataToSend.receiverEmailId){
                               interactionReceiver.userId = dataToSend.receiverId
                           }
                          if(interactionSender != null && interactionReceiver != null){
                              meetingSupportClassObj.storeAcceptMeetingInteractions(interactionSender,interactionReceiver,saved);
                              saved = false;
                          }
                          else{
                              logger.info('Error in storeEmailToInteractions():Gmail sender or receiver not exist 2',officeEmail);
                          }
                       }
                   }
               }
            }
        }
    }
}

function parseEmailHeader(id,headers){

    var email = {emailContentId:id};
    for(var i=0; i<headers.length; i++){
        if(headers[i].name == 'From' || headers[i].name == 'from'){
            email.from = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'To' || headers[i].name == 'to'){
            email.To = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Subject' || headers[i].name == 'subject'){
            email.subject = headers[i].value;
        }
        if(headers[i].name == 'Cc' || headers[i].name == 'cc'){
            email.Cc = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Bcc' || headers[i].name == 'bcc'){
            email.Bcc = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Date' || headers[i].name == 'date'){
            email.date = new Date(headers[i].value);
        }
        if(headers[i].name == 'Message-Id' || headers[i].name == 'Message-ID'){
            email.id = headers[i].value;
        }
    }
    return email;
}

function extractEmails (text)
{
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

function generateAndStoreInteraction(email,office,officeEmail,googleEmail,trackId,description,trackOpen,trackResponse,trackDocument){

    var interactionsArr = [];

    var toCcBcc = null;
    var ccList = [];
    var toList = [];

    if (email.To.length == 1 && !email.Cc && !email.Bcc) {
        toCcBcc = 'me';
    }

    _.each(email.To,function (el) {
        toList.push({emailId:el})
    })

    if(email.Cc && email.Cc.length>0){
        _.each(email.Cc,function (el) {
            ccList.push({emailId:el})
        })
    }

    if(email.Bcc && email.Bcc.length>0){
        _.each(email.Bcc,function (el) {
            ccList.push({emailId:el})
        })
    }

    if(common.checkRequired(email.To)){
        for(var to=0; to<email.To.length; to++){
            var emailIdTo = email.To[to] ? email.To[to] : '';
            if(office && googleEmail == emailIdTo){
                emailIdTo = officeEmail;
            }

            var interactionTo = {};
            interactionTo.userId = '';
            interactionTo.action = 'receiver';
            interactionTo.type = 'email';
            interactionTo.subType = 'email';
            interactionTo.emailId = emailIdTo;
            interactionTo.refId = email.id;
            interactionTo.source = 'relatas';
            interactionTo.title = email.subject || '';
            interactionTo.description = description;
            interactionTo.interactionDate = email.date;
            interactionTo.trackId = trackId;
            interactionTo.emailContentId = email.emailContentId;
            interactionTo.googleAccountEmailId = email.googleAccountEmailId;
            interactionTo.trackInfo = {
                trackOpen:trackOpen,
                trackResponse:trackResponse,
                trackDocument:trackDocument,
                emailOpens:0
            };

            interactionsArr.push(interactionTo);
        }
    }

    if(common.checkRequired(email.from)){
        for(var from=0; from<email.from.length; from++){
            var emailIdFrom = email.from[from] ? email.from[from] : '';
            if(office && googleEmail == emailIdFrom){
                emailIdFrom = officeEmail;

            }

            var interaction = {};
            interaction.userId = '';
            interaction.action = 'sender';
            interaction.type = 'email';
            interaction.subType = 'email';
            interaction.emailId = emailIdFrom;
            interaction.refId = email.id;
            interaction.source = 'relatas';
            interaction.title = email.subject || '';
            interaction.description = description;
            interaction.interactionDate = email.date;
            interaction.trackId = trackId;
            interaction.emailContentId = email.emailContentId;
            interaction.googleAccountEmailId = email.googleAccountEmailId;
            interaction.trackInfo = {
                trackOpen:trackOpen,
                trackResponse:trackResponse,
                trackDocument:trackDocument
            };
            interactionsArr.push(interaction);
        }
    }
    if(common.checkRequired(email.Cc)){
        for(var cc=0; cc<email.Cc.length; cc++){
            var emailIdCc = email.Cc[cc] ? email.Cc[cc] : '';
            if(office && googleEmail == emailIdCc){
                emailIdCc = officeEmail;

            }
            var interactionCc = {};

            interactionCc.userId = '';
            interactionCc.action = 'receiver';
            interactionCc.type = 'email';
            interactionCc.subType = 'email';
            interactionCc.emailId = emailIdCc;
            interactionCc.refId = email.id;
            interactionCc.source = 'relatas';
            interactionCc.title = email.subject || '';
            interactionCc.description = description;
            interactionCc.interactionDate = email.date;
            interactionCc.trackId = trackId;
            interactionCc.emailContentId = email.emailContentId;
            interactionCc.googleAccountEmailId = email.googleAccountEmailId;
            interactionCc.trackInfo = {
                trackOpen:trackOpen,
                trackResponse:trackResponse,
                trackDocument:trackDocument
            };
            interactionsArr.push(interactionCc);
        }
    }
    if(common.checkRequired(email.Bcc)){
        for(var bcc=0; bcc<email.Bcc.length; bcc++){
            var emailIdBcc = email.Bcc[bcc] ? email.Bcc[bcc] : '';
            if(office && googleEmail == emailIdBcc){
                emailIdBcc = officeEmail;
            }

            var interactionBcc = {};
            interactionBcc.userId = '';
            interactionBcc.action = 'receiver';
            interactionBcc.type = 'email';
            interactionBcc.subType = 'email';
            interactionBcc.emailId = emailIdBcc;
            interactionBcc.refId = email.id;
            interactionBcc.source = 'relatas';
            interactionBcc.title = email.subject || '';
            interactionBcc.description = description;
            interactionBcc.interactionDate = email.date;
            interactionBcc.trackId = trackId;
            interactionBcc.emailContentId = email.emailContentId;
            interactionBcc.googleAccountEmailId = email.googleAccountEmailId;
            interactionBcc.trackInfo = {
                trackOpen:trackOpen,
                trackResponse:trackResponse,
                trackDocument:trackDocument
            };
            interactionsArr.push(interactionBcc);
        }
    }

    _.each(interactionsArr,function (el) {
        el.toCcBcc = toCcBcc;
        el.ccList = ccList;
        el.toList = toList;
    })

    return interactionsArr;
}

module.exports = Gmail;