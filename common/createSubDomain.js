var config = new (require('../config/relatasConfiguration'))()
  , AWS = require('aws-sdk')
  , route53 = new AWS.Route53({apiVersion: '2013-04-01'})
  , _ = require("lodash")
  
  //params = {
      //HostedZoneId: config.getRoute53Details().hostedZoneId
    //}
var createSubDomain = function(subDomainName, callback){
  resourceRecordSetName = subDomainName.toLowerCase() + ".relatas.com."
  
    checkIfSubdomainExists(resourceRecordSetName, function(err, doesSubdomainExists){
      if(err)
        callback(err, false)
      else{
        if(doesSubdomainExists)
          callback(new Error("Subdomain " + resourceRecordSetName.replace(/\.$/, "") + " already exists"), false)
        else{
          params = {
            ChangeBatch: {
              Changes: [ 
                {
                  Action: 'CREATE', 
                  ResourceRecordSet: { 
                    Name: resourceRecordSetName,
                    Type: 'CNAME',
                    TTL: 300,
                    ResourceRecords: [
                      {
                        Value: config.getRoute53Details().endpoint
                      },
                    ]
                  }
                }
              ]
            },
            HostedZoneId: config.getRoute53Details().hostedZoneId
          }
          route53.changeResourceRecordSets(params, function (err, data) {
            if (err) 
              callback(err, false)
            else
              callback(err, true)    
          });
        }
      }
    })

}

var checkIfSubdomainExists = function(resourceRecordSetName, callback){
  route53.listResourceRecordSets({
        HostedZoneId: config.getRoute53Details().hostedZoneId,
        MaxItems: "20",
        StartRecordName: resourceRecordSetName
      }, function (err, data) {
        if (err) 
          callback(err)
        else{
            callback(null,!_.isUndefined(_.find(data.ResourceRecordSets, function(r){
              return r.Name == resourceRecordSetName
            })))
        }
  }); 
}

module.exports = createSubDomain
