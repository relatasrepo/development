var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var gcal = require('google-calendar');
var simple_unique_id = require('simple-unique-id');
var AWS = require('aws-sdk');
var moment = require('moment-timezone');
var mongoose = require('mongoose');
var request = require('request');

var commonUtility = require('../common/commonUtility');
var meetingManagement = require('../dataAccess/meetingManagement');
var userManagement = require('../dataAccess/userManagementDataAccess');
var appCredentials = require('../config/relatasConfiguration');
var googleCalendarAPI = require('../common/googleCalendar');
var winstonLog = require('../common/winstonLog');
var Ics = require('../common/icsFormat');
var emailSender = require('../public/javascripts/emailSender');
var messageDataAccess = require('../dataAccess/messageManagement');
var interactions = require('../dataAccess/interactionManagement');
var contactClass = require('../dataAccess/contactsManagementClass');
var officeOutlookApi = require('../common/officeOutlookAPI');

var officeOutlook = new officeOutlookApi();
var interaction = new interactions();
var meetingManagementObj = new meetingManagement();
var contactObj = new contactClass();
var common = new commonUtility();
var googleCalendar = new googleCalendarAPI();
var userManagements = new userManagement();
var appCredential = new appCredentials();
var emailSenders = new emailSender();
var logLib = new winstonLog();

var logger =logLib.getWinston();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var domainName = domain.domainName;

var awsData = appCredential.getAwsCredentials();
AWS.config.update(awsData);
AWS.config.region = awsData.region;

var _ = require("lodash");
var customObjectId = require('mongodb').ObjectID

function Meeting(){

    /* CONFIRM MEETING */
  this.validateUserAcceptance = function(invitationId,userId,slotId,message,callback){
       getMeetingById(invitationId,function(meeting){

            if(meeting && common.checkRequired(meeting)){
                if(!isAuthorisedUserToAccessMeeting(meeting,userId)){
                    callback(false,'UN_AUTHORISED_ACCESS',null)
                }else
                if(isMeetingAlreadyAccepted(meeting,userId)){

                    callback(false,'MEETING_ALREADY_ACCEPTED',null)
                }else
                if(meeting.suggested){
                    if(meeting.suggestedBy.userId == userId){
                        callback(false,'INVALID_USER_TO_ACCEPT',null)
                    }
                    else{
                        var isAcceptedS = false;
                        if(meeting.senderId != userId){
                            if(meeting.selfCalendar){

                                for(var j=0; j<meeting.toList.length; j++){
                                    if(meeting.toList[j].receiverId == userId && meeting.toList[j].isAccepted){
                                        isAcceptedS = true;
                                        break;
                                    }
                                }

                            }
                            else{

                                for(var i=0; i<meeting.scheduleTimeSlots.length; i++){
                                    if(meeting.scheduleTimeSlots[i].isAccepted){
                                        isAcceptedS = true;
                                        break;
                                    }
                                }

                            }
                        }
                        else{
                            if(meeting.selfCalendar){

                                for(var k=0; k<meeting.toList.length; k++){
                                    if(meeting.toList[k].isAccepted){
                                        isAcceptedS = true;
                                        break;
                                    }
                                }

                            }
                            else{

                                for(var l=0; l<meeting.scheduleTimeSlots.length; l++){
                                    if(meeting.scheduleTimeSlots[l].isAccepted){
                                        isAcceptedS = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if(!isAcceptedS){
                            callback(true,null,getAcceptMeetingObject(invitationId,userId,slotId,message,meeting))
                        }
                        else{
                            callback(false,'MEETING_ALREADY_ACCEPTED',null)
                        }
                    }
                }
                else if(meeting.senderId == userId){
                    callback(false,'INVALID_USER_TO_ACCEPT',null)
                }
                else callback(true,null,getAcceptMeetingObject(invitationId,userId,slotId,message,meeting))
            }
           else callback(false,'FAILED_TO_FETCH_MEETING_DETAILS',null);
       })
  };

  this.confirmMeeting = function(eventDetails,userId,message,callback){
        getMeetingById(eventDetails.invitationId,function(invitation){
            var count = invitation.updateCount || 0;
            userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,outlook:1},function(error,profile){

                if(profile.google.length == 0 && profile.outlook.length>0){
                    _.each(profile.outlook,function (outlookObj) {
                        if(profile.emailId == outlookObj.emailId){

                            if(invitation.partialMeeting){

                                createNewGoogleOrOutlookMeeting(profile, invitation, eventDetails, 'outlook', outlookObj.refreshToken , callback);
                            }
                            else {
                                confirmOutlookMeeting(outlookObj.refreshToken, eventDetails.invitationId, message, function (err, response) {
                                    //TODO 1.update meetings, 2.Update interactions, 3.Last interaction date contacts
                                    if (!err && response) {
                                        updateOfficeEvent(userId, eventDetails, invitation, callback)
                                    } else {

                                    }
                                });
                            }
                        }
                    });
                } else {

                    if(googleCalendar.validateUserGoogleAccount(profile)){

                        // if(invitation.googleEventId)
                        if(invitation.partialMeeting || !invitation.googleEventId){

                            createNewGoogleOrOutlookMeeting(profile, invitation, eventDetails, 'google', null, callback);
                        }
                        else {
                            createOrUpdateGoogleEvent(userId, eventDetails, true, false, invitation.googleEventId, invitation, count, false, invitation.isGoogleMeeting, invitation.isOfficeOutlookMeeting, callback);
                        }
                    }else{

                        if(invitation.senderId != userId){
                            createOrUpdateGoogleEvent(invitation.senderId,eventDetails,true,false,invitation.googleEventId,invitation,count,true,invitation.isGoogleMeeting,invitation.isOfficeOutlookMeeting,callback);
                        }else{

                            if(invitation.selfCalendar && invitation.toList.length == 1){
                                createOrUpdateGoogleEvent(invitation.toList[0].receiverId,eventDetails,true,false,invitation.googleEventId,invitation,count,true,invitation.isGoogleMeeting,invitation.isOfficeOutlookMeeting,callback);
                            }
                            else{
                                createOrUpdateGoogleEvent(invitation.to.receiverId,eventDetails,true,false,invitation.googleEventId,invitation,count,true,invitation.isGoogleMeeting,invitation.isOfficeOutlookMeeting,callback);
                            }
                        }
                    }
                }
            })
        })
  };

  this.confirmMeeting_new = function(eventDetails,userId,serviceLogin,callback){
        getMeetingById(eventDetails.invitationId,function(invitation){
            if(invitation != null && invitation != undefined){
                var count = invitation.updateCount || 0;
                if(serviceLogin == 'office'){
                    createOrUpdateOfficeEvent(userId,eventDetails,invitation,callback);
                }
                else{
                    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1},function(error,profile){
                        if(googleCalendar.validateUserGoogleAccount(profile)){
                            createOrUpdateGoogleEvent(userId,eventDetails,true,false,invitation.googleEventId,invitation,count,false,invitation.isGoogleMeeting,callback);
                        }
                        else{
                            if(invitation.senderId != userId){
                                createOrUpdateGoogleEvent(invitation.senderId,eventDetails,true,false,invitation.googleEventId,invitation,count,true,invitation.isGoogleMeeting,callback);
                            }
                            else{
                                if(invitation.selfCalendar && invitation.toList.length == 1){
                                    createOrUpdateGoogleEvent(invitation.toList[0].receiverId,eventDetails,true,false,invitation.googleEventId,invitation,count,true,invitation.isGoogleMeeting,callback);
                                }
                                else{
                                    createOrUpdateGoogleEvent(invitation.to.receiverId,eventDetails,true,false,invitation.googleEventId,invitation,count,true,invitation.isGoogleMeeting,callback);
                                }
                            }
                        }
                    })
                }
            }
            else callback(false,'NO_MEETING');
        })
    };

  this.updateMeetingInvitation = function(invitation,userId,callback){
      userManagements.updateInvitationReadStatus(invitation,userId,function(error,isUpdated,message){

          if (isUpdated) {

              if(invitation.selfCalendar){
                  invitation.loggeinUserId = userId;

                  acceptMeetingSelfCalendar(invitation,function(result,err){
                      callback(result,err)
                  })
              }
              else{

                  generateIcsFile(invitation,function(result,err){
                      callback(result,err)
                  })
              }

          }
          else{
              logger.info('Update meeting failed 1');
              callback(false,'UPDATE_MEETING_FAILED');
          }
      });
  };

  this.sendAcceptInvitationEmail = function(invitation,userId){
        var date = new Date();

        if(common.checkRequired(invitation.message)){

            var messageAccess = new messageDataAccess();
            var message = {
                senderId:userId,
                receiverId:invitation.senderId || '',
                receiverEmailId:invitation.senderEmailId,
                type:'meeting-comment',
                subject:invitation.description,
                message:invitation.message.replace(/<br>/g,'\n')
            };

            messageAccess.createMessage(message,function(message){

                if(message){
                    var interaction = {};
                    interaction.userId = message.senderId;
                    interaction.action = 'sender';
                    interaction.type = 'meeting-comment';
                    interaction.subType = 'meeting-comment';
                    interaction.refId = message._id;
                    interaction.source = 'relatas';
                    interaction.title = message.message;
                    interaction.description = message.subject;
                    //common.storeInteraction(interaction);

                    var interactionReceiver = {};
                    interactionReceiver.userId = message.receiverId || '';
                    interactionReceiver.emailId = message.receiverEmailId;
                    interactionReceiver.action = 'receiver';
                    interactionReceiver.type = 'meeting-comment';
                    interactionReceiver.subType = 'meeting-comment';
                    interactionReceiver.refId = message._id;
                    interactionReceiver.source = 'relatas';
                    interactionReceiver.title = message.message;
                    interactionReceiver.description = message.subject;
                    //common.storeInteractionReceiver(interactionReceiver);
                    storeAcceptMeetingInteractions(interaction,interactionReceiver);
                }
            });
        }
        //emailSenders.sendInvitationAcceptMailToSender(invitation);
        //emailSenders.sendInvitationAcceptMailToReceiver(invitation);
      this.sendMeetingAcceptEmails(invitation.invitationId,userId,invitation.message);
    };

  this.updateMeetingWithIcs = function(details,callback){
      if(common.checkRequired(details.invitationId) && common.checkRequired(details.icsAwsKey) && common.checkRequired(details.icsUrl)){
          userManagements.updateInvitationWithIcs(details,function(error,isUpdated){
              if(isUpdated){
                    callback(true,null)
              }else{
                  logger.info('Update meeting failed 3');
                  callback(false,'UPDATE_MEETING_FAILED');
              }
          })
      }else callback(false,'INVALID_ICS_DETAILS');
  };

   /* CANCEL OR UNDO MEETING */
  this.cancelMeeting = function(invitationId,userId,message,timezone,user,callback){
      
      //Only for outlook
      var outlookMessage = {
          "comment": message?message:"No comments",
          "sendResponse": true
      };

     getMeetingById(invitationId,function(meeting){
         if(meeting && !meeting.partialMeeting){
             var participantListId = getAllParticipantsUserIdList(meeting);
            if(meeting.senderId == userId){
                if(common.checkRequired(meeting.googleEventId)){

                    updateMeetingToDelete(userId,meeting.invitationId,participantListId,timezone,message,false,function(isSuccess,error){
                        callback(isSuccess,error);
                    })
                }
                else{

                    if(user.google.length == 0 && user.outlook.length>0){

                        var isOrganizer = true;

                        _.each(user.outlook,function (outlookObj) {
                            if(user.emailId == outlookObj.emailId){
                                deleteOutlookMeeting(user,outlookObj,outlookMessage,invitationId,isOrganizer,function (err,response) {

                                    if(!err && response){
                                        if(common.checkRequired(meeting.icsFile) && common.checkRequired(meeting.icsFile.awsKey)){
                                            deleteIcsFileFromS3(meeting.icsFile.awsKey);
                                        }

                                        updateMeetingToDelete(userId,meeting.invitationId,participantListId,timezone,message,false,function(isSuccess,error){
                                            callback(isSuccess,error);
                                        })
                                    } else{
                                        callback(false,'MEETING_CANCEL_ERROR');
                                    }
                                });
                            }
                        });
                    } else {
                        googleCalendar.removeEventFromGoogleCalendarForMeeting(meeting.googleEventId,meeting.lastActionUserId || userId,function(isSuccess){
                            if(isSuccess){
                                if(common.checkRequired(meeting.icsFile) && common.checkRequired(meeting.icsFile.awsKey)){
                                    deleteIcsFileFromS3(meeting.icsFile.awsKey);
                                }

                                updateMeetingToDelete(userId,meeting.invitationId,participantListId,timezone,message,false,function(isSuccess,error){
                                    callback(isSuccess,error);
                                })
                            }
                            else{
                                callback(false,'MEETING_CANCEL_ERROR');
                            }
                        })
                    }
                }
            }
            else{
                
                if(common.checkRequired(meeting.googleEventId)){

                    if(meeting.selfCalendar){
                        if(meeting.toList.length == 1){
                            cancelMeetingNextStep(userId,meeting,participantListId,message,timezone,true,true,callback);
                        }
                        else{
                            callback(false,'MEETING_CANCEL_ERROR_MULTIPLE_PARTICIPANTS');
                        }
                    }
                    else{
                        cancelMeetingNextStep(userId,meeting,participantListId,message,timezone,true,true,callback);
                    }
                }
                else{
                    
                    if(user.google.length == 0 && user.outlook.length>0){
                        
                        _.each(user.outlook,function (outlookObj) {
                            if(user.emailId == outlookObj.emailId){
                                deleteOutlookMeeting(user,outlookObj,outlookMessage,invitationId,false,function (err,response) {

                                    if(!err && response){
                                    } else {
                                        callback(false,'MEETING_CANCEL_ERROR');
                                    }
                                })
                            }
                        });
                    }

                    cancelMeetingNextStep(userId,meeting,participantListId,message,timezone,false,false,callback);
                }
            }
         }
         else if(meeting.partialMeeting){
             // console.log('------------------in else part ;;; partial');
             updateMeetingToDelete(userId,meeting.invitationId,participantListId,timezone,message,true,function(isSuccess,error){
                 callback(isSuccess,error);
             })
         }
         else{
            callback(false,'MEETING_CANCEL_ERROR');
         }
     })
  };

  this.deleteICSFile = function(awsKey){
      deleteIcsFileFromS3(awsKey)
  };

  this.validateUserAuthorizationForMeeting = function(invitation,userId){
      return isAuthorisedUserToAccessMeeting(invitation,userId);
  };

    /* SEND ACCEPT MEETING EMAILS */
  this.sendMeetingAcceptEmails = function(invitationId,userId,message){

      getMeetingById(invitationId,function(meeting){
         if(meeting){
             var mUrl = domainName+'/today/details/'+invitationId;
             var maxSlot;
             for(var j=0; j<meeting.scheduleTimeSlots.length; j++){
                 if(meeting.scheduleTimeSlots[j].isAccepted){
                     maxSlot = meeting.scheduleTimeSlots[j];
                 }
             }

             if(meeting.senderId == userId){
                 getUserProfile(userId,function(sender){
                      if(common.checkRequired(sender)){
                          var receiverId;
                          if(meeting.selfCalendar){
                              if(meeting.toList.length > 0){
                                  receiverId = meeting.toList[0].receiverId;
                              }
                          }
                          else{
                             receiverId = meeting.to.receiverId;
                          }

                          if(common.checkRequired(receiverId)){
                              getUserProfile(receiverId,function(receiver){
                                  if(common.checkRequired(receiver)){
                                      sendConfirmMailNext(sender,receiver,'sender',message,mUrl,maxSlot,sender.firstName+' '+sender.lastName)
                                      sendConfirmMailNext(sender,receiver,'receiver',message,mUrl,maxSlot,sender.firstName+' '+sender.lastName)
                                  }
                              })
                          }
                          else {
                              // NO RECEIVER USER PROFILE
                          }
                      }
                      else{
                          // NO SENDER PROFILE
                      }
                 })
             }
             else{
                getUserProfile(meeting.senderId,function(receiver){
                    if(common.checkRequired(receiver)){
                        var senderId;
                        if(meeting.selfCalendar){
                            if(meeting.toList.length > 0){
                                for(var i=0; i<meeting.toList.length; i++){
                                    if(meeting.toList[i].receiverId == userId){
                                        senderId = meeting.toList[i].receiverId;
                                    }
                                }
                            }
                        }
                        else{
                            senderId = meeting.to.receiverId;
                        }

                        if(common.checkRequired(senderId)){
                            getUserProfile(senderId,function(sender){
                                if(common.checkRequired(sender)){
                                    sendConfirmMailNext(sender,receiver,'sender',message,mUrl,maxSlot,sender.firstName+' '+sender.lastName)
                                    sendConfirmMailNext(sender,receiver,'receiver',message,mUrl,maxSlot,sender.firstName+' '+sender.lastName)
                                }
                            })
                        }
                        else{
                            // NO SENDER PROFILE
                        }
                    }
                    else {
                        // NO RECEIVER USER PROFILE
                    }
                })
             }
         }
      })
  };

  this.sendNewMeetingRequestEmail = function(invitationId,userId){
        sendNewMeetingEmails(invitationId,userId,false)
  };

  this.sendSuggestMeetingEmail = function(invitationId,oldSlot,message){
      getMeetingById(invitationId,function(meeting){
          if(common.checkRequired(meeting) && meeting.suggested && common.checkRequired(meeting.suggestedBy)){
              var senderObj;
              var receiverObj;
              var receiverName = '';
              var maxSlot;
              for(var s=0; s<meeting.scheduleTimeSlots.length; s++){
                  if(meeting.scheduleTimeSlots[s].isAccepted){
                      maxSlot = meeting.scheduleTimeSlots[s];
                  }
              }

              if(!common.checkRequired(maxSlot)){
                  maxSlot = meeting.scheduleTimeSlots.reduce(function (a, b) {
                      return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                  });
              }

              if(meeting.suggestedBy.userId == meeting.senderId){
                  senderObj = {
                      senderId:common.checkRequired(meeting.senderId) ? meeting.senderId : null,
                      senderName:common.checkRequired(meeting.senderName) ? meeting.senderName : '',
                      senderEmailId:meeting.senderEmailId
                  };
                  var multi = false;
                  if(meeting.selfCalendar){
                      if(meeting.toList.length > 1){
                         multi = true;
                      }

                      for(var j=0; j<meeting.toList.length; j++){
                          if(!meeting.toList[j].isResource){
                              receiverName += meeting.toList[j].receiverEmailId+'<br>';
                              var receiverNameF2 = meeting.toList[j].receiverFirstName || '';
                              var receiverNameL2 = meeting.toList[j].receiverLastName || '';
                              var rfn2 = receiverNameF2+' '+receiverNameL2;

                              receiverObj = {
                                  receiverId:common.checkRequired(meeting.toList[j].receiverId) ? meeting.toList[j].receiverId : null,
                                  receiverName:rfn2,
                                  receiverEmailId:meeting.toList[j].receiverEmailId
                              };

                              sendSuggestMeetingEmailNext(senderObj.senderId,receiverObj.receiverId,senderObj,receiverObj,meeting,maxSlot,oldSlot,'receiver',message,true);
                          }
                      }
                  }
                  else{
                      receiverObj = {
                          receiverId:common.checkRequired(meeting.to.receiverId) ? meeting.to.receiverId : null,
                          receiverName:common.checkRequired(meeting.to.receiverName) ? meeting.to.receiverName : '',
                          receiverEmailId:meeting.to.receiverEmailId
                      };
                      sendSuggestMeetingEmailNext(senderObj.senderId,receiverObj.receiverId,senderObj,receiverObj,meeting,maxSlot,oldSlot,'receiver',message,true);
                  }

                  if(multi){
                      sendSuggestMeetingEmailNext_Multiple(senderObj.senderId,senderObj,meeting,maxSlot);
                  }
                  else{
                      sendSuggestMeetingEmailNext(senderObj.senderId,receiverObj.receiverId,senderObj,receiverObj,meeting,maxSlot,oldSlot,'sender',message,false);
                  }
              }
              else{
                  receiverObj = {
                      receiverId:common.checkRequired(meeting.senderId) ? meeting.senderId : null,
                      receiverName:common.checkRequired(meeting.senderName) ? meeting.senderName : '',
                      receiverEmailId:meeting.senderEmailId
                  };

                if(meeting.selfCalendar){
                    for(var i=0; i<meeting.toList.length; i++){
                        if(meeting.toList[i].receiverId == meeting.suggestedBy.userId){
                            if(!meeting.toList[i].isResource){
                                var receiverNameF = meeting.toList[i].receiverFirstName || '';
                                var receiverNameL = meeting.toList[i].receiverLastName || '';
                                var rfn = receiverNameF+' '+receiverNameL;

                                senderObj = {
                                    senderId:common.checkRequired(meeting.toList[i].receiverId) ? meeting.toList[i].receiverId : null,
                                    senderName:rfn,
                                    senderEmailId:meeting.toList[i].receiverEmailId
                                }
                                sendSuggestMeetingEmailNext(senderObj.senderId,receiverObj.receiverId,senderObj,receiverObj,meeting,maxSlot,oldSlot,'sender',message,false);
                            }
                        }
                    }
                }
                else{
                    senderObj = {
                        senderId:common.checkRequired(meeting.to.receiverId) ? meeting.to.receiverId : null,
                        senderName:common.checkRequired(meeting.to.receiverName) ? meeting.to.receiverName : '',
                        senderEmailId:meeting.to.receiverEmailId
                    };
                    sendSuggestMeetingEmailNext(senderObj.senderId,receiverObj.receiverId,senderObj,receiverObj,meeting,maxSlot,oldSlot,'sender',message,false);
                }
                  sendSuggestMeetingEmailNext(senderObj.senderId,receiverObj.receiverId,senderObj,receiverObj,meeting,maxSlot,oldSlot,'receiver',message,true);
              }
          }
      })
  };

  /* SCHEDULE NEW MEETING */
  this.scheduleNewMeeting = function(senderObj,receiverObj,meetingType,meetingDetails,req,isWeb2Lead,callback){

      var senderSelectedTimezone = senderObj.senderSelectedTimezone;

      if(meetingDetails.locationType == "In-Person"){
          //If the meeting is of type in Person, then get the meeting's location from Google to display on the Today Landing Meeting location. Last edited 17 Feb 2016 by Naveen.

          var loc = meetingDetails.location.replace(/[^\w\s]/gi, '');
          request('https://maps.googleapis.com/maps/api/geocode/json?address='+loc,function(e,r,b){

              var new_location = null;

              if(e){
                  logger.info("Error Google location Search searchByAddress() ",e, 'search content ',meetingDetails.location);
              }
              else {
                  try {
                      b = JSON.parse(b);
                  }
                  catch (e) {
                      logger.info("Exception Google location Search searchByAddress() ", e, 'search content ', meetingDetails.location);
                  }

                  if (b && b.results && b.results.length > 0 && b.results[0] && b.results[0].address_components && b.results[0].address_components.length > 0) {

                      for (var i = 0; i < b.results[0].address_components.length; i++) {
                          if (b.results[0].address_components[i].types.indexOf("locality") != -1) {
                              new_location = b.results[0].address_components[i].long_name;
                              break;
                          }
                      }

                      var meeting = {
                          invitationId:simple_unique_id.generate('invitationSecrete'),
                          senderId:senderObj.senderId,
                          senderEmailId:senderObj.senderEmailId,
                          senderName:senderObj.senderName,
                          senderPicUrl:senderObj.senderPicUrl,
                          senderSelectedTimezone:senderObj.senderSelectedTimezone,
                          suggested:false,
                          readStatus:false,
                          scheduledDate:new Date(),
                          isGoogleMeeting:true,
                          scheduleTimeSlots: [
                              {
                                  _id: new customObjectId(),
                                  isAccepted: isWeb2Lead?true:false,
                                  start: {
                                      date: new Date(meetingDetails.start)
                                  },
                                  end: {
                                      date: new Date(meetingDetails.end)
                                  },
                                  title: meetingDetails.title,
                                  location:meetingDetails.location,
                                  locationType: meetingDetails.locationType,
                                  description: meetingDetails.description
                              }
                          ],
                          participants: [
                              {
                                  emailId: senderObj.senderEmailId
                              }
                          ],
                          deleted:false,
                          LIUoutlookEmailId:senderObj.senderEmailId,
                          partialMeeting: false
                      };

                      if(req.session.partialProfile){
                          meeting.partialMeeting = true;
                          meeting.LIUoutlookEmailId = receiverObj.emailId;
                          meeting.isOfficeOutlookMeeting = true;
                      }

                      if(meetingType == 'selfCalendar'){
                          meeting.toList = meetingDetails.toList;
                          // validate receivers email id's, trim emails, lowercase emails
                          meeting.selfCalendar = true;
                          var emails = [];
                          for(var j=0; j<meetingDetails.toList.length; j++){
                              emails.push(meetingDetails.toList[j].receiverEmailId);
                              if(!meetingDetails.toList[j].isResource){
                                  meeting.participants.push({emailId:meetingDetails.toList[j].receiverEmailId})
                              }
                          }

                          userManagements.selectUserProfilesCustomQuery({emailId:{$in:emails}},{firstName:1,lastName:1,emailId:1},function(error,users){
                              if(common.checkRequired(users) && users.length > 0){
                                  var toList = [];
                                  for(var i=0; i<users.length; i++){
                                      for(var l=0; l<meeting.toList.length; l++){
                                          if(users[i].emailId == meeting.toList[l].receiverEmailId){
                                              meeting.toList[l].receiverId = users[i]._id
                                              meeting.toList[l].receiverFirstName = users[i].firstName
                                              meeting.toList[l].receiverLastName = users[i].lastName
                                              meeting.toList[l].receiverEmailId = users[i].emailId
                                          }
                                      }
                                  }
                              }
                              saveNewMeetingRequest(meeting,senderSelectedTimezone,isWeb2Lead,callback)
                          })
                      }
                      else{
                          meeting.participants.push({emailId:receiverObj.emailId});
                          meeting.to = {
                              receiverId:receiverObj._id,
                              receiverName:receiverObj.firstName+' '+receiverObj.lastName,
                              receiverEmailId:receiverObj.emailId,
                              canceled:false
                          };
                          saveNewMeetingRequest(meeting,senderSelectedTimezone,isWeb2Lead,callback)
                      }
                  }
                  else {

                      if(b.results.length === 0 || b.status === 'ZERO_RESULTS'){
                          req.session.locationStatus = "ZERO_RESULTS";
                          req.session.save();
                          callback(false);
                      }
                  }
              }
          });
      }
      else {

          var meeting = {
              invitationId:simple_unique_id.generate('invitationSecrete'), //req.session.partialProfile
              senderId:senderObj.senderId,
              senderEmailId:senderObj.senderEmailId,
              senderName:senderObj.senderName,
              senderPicUrl:senderObj.senderPicUrl,
              senderSelectedTimezone:senderObj.senderSelectedTimezone,
              suggested:false,
              readStatus:false,
              scheduledDate:new Date(),
              isGoogleMeeting:true,
              scheduleTimeSlots: [
                  {
                      _id: new customObjectId(),
                      isAccepted: isWeb2Lead?true:false,
                      start: {
                          date: new Date(meetingDetails.start)
                      },
                      end: {
                          date: new Date(meetingDetails.end)
                      },
                      title: meetingDetails.title,
                      location: meetingDetails.location,
                      locationType: meetingDetails.locationType,
                      description: meetingDetails.description
                  }
              ],
              participants: [
                  {
                      emailId: senderObj.senderEmailId
                  }
              ],
              deleted:false,
              LIUoutlookEmailId:senderObj.senderEmailId,
              partialMeeting: false
          };

          if(req.session.partialProfile && !isWeb2Lead){
              meeting.partialMeeting = true;
              meeting.LIUoutlookEmailId = receiverObj.emailId;
              meeting.isOfficeOutlookMeeting = true;
          }

          if(meetingType == 'selfCalendar'){
          meeting.toList = meetingDetails.toList;
          // validate receivers email id's, trim emails, lowercase emails
          meeting.selfCalendar = true;
          var emails = [];
          for(var j=0; j<meetingDetails.toList.length; j++){
              emails.push(meetingDetails.toList[j].receiverEmailId);
              if(!meetingDetails.toList[j].isResource){
                  meeting.participants.push({emailId:meetingDetails.toList[j].receiverEmailId})
              }
          }

          userManagements.selectUserProfilesCustomQuery({emailId:{$in:emails}},{firstName:1,lastName:1,emailId:1},function(error,users){
               if(common.checkRequired(users) && users.length > 0){
                   var toList = [];
                   for(var i=0; i<users.length; i++){
                       for(var l=0; l<meeting.toList.length; l++){
                           if(users[i].emailId == meeting.toList[l].receiverEmailId){
                               meeting.toList[l].receiverId = users[i]._id
                               meeting.toList[l].receiverFirstName = users[i].firstName
                               meeting.toList[l].receiverLastName = users[i].lastName
                               meeting.toList[l].receiverEmailId = users[i].emailId
                           }
                       }
                   }
               }

              saveNewMeetingRequest(meeting,senderSelectedTimezone,isWeb2Lead,callback)
          })
      }
      else{
         meeting.participants.push({emailId:receiverObj.emailId});
         meeting.to = {
             receiverId:receiverObj._id,
             receiverName:receiverObj.firstName+' '+receiverObj.lastName,
             receiverEmailId:receiverObj.emailId,
             canceled:false
          };
          saveNewMeetingRequest(meeting,senderSelectedTimezone,isWeb2Lead,callback)
      }
      }//End Else
  };

  this.getParticipantListForDocument = function(userId,invitationId,callback){
        getMeetingById(invitationId,function(meeting){
            var participants = [];
            if(common.checkRequired(meeting)){
                if(meeting.senderId != userId){
                    if(common.checkRequired(meeting.senderEmailId))
                        participants.push(meeting.senderEmailId)
                }
                if(meeting.selfCalendar){
                    for(var i=0; i<meeting.toList.length; i++){
                        if(meeting.toList[i].receiverId != userId){
                            if(common.checkRequired(meeting.toList[i].receiverEmailId))
                                participants.push(meeting.toList[i].receiverEmailId)
                        }
                    }
                }
                else{
                    if(meeting.to.receiverId != userId){
                        if(common.checkRequired(meeting.to.receiverEmailId))
                            participants.push(meeting.to.receiverEmailId)
                    }
                }
                callback(participants);
            }
            else callback(participants)
        })
    };

  this.updateMeetingPrepared = function(userId,invitationId,callback){
      getMeetingById(invitationId,function(meeting){
          if(meeting){
              meetingManagementObj.updateMeetingPrepared(userId,invitationId,meeting.senderId == userId,meeting.selfCalendar,function(isSuccess){
                   callback(isSuccess);
              })
          }
          else callback(false)
      })
  }
}

function confirmOutlookMeeting(refreshToken,invitationId,message,callback) {

    var message = {
        "comment": message,
        "sendResponse": true
    };

    officeOutlook.getNewAccessTokenWithoutStore(refreshToken,function (newToken) {
        officeOutlook.confirmOutlookMeetingMSGraphAPI(newToken.access_token,invitationId,message,callback)
    })

}

function deleteOutlookMeeting(user,outlookObj,message,invitationId,isOrganizer,callback) {
    officeOutlook.getNewAccessTokenWithoutStore(outlookObj.refreshToken,function (newToken) {

        if(isOrganizer){
            officeOutlook.deleteMeetingOnCalendarMSGraphAPI(newToken.access_token,invitationId,callback)
        } else {
            officeOutlook.declineMeetingOnCalendarMSGraphAPI(newToken.access_token,message,invitationId,callback)
        }
    });
}

function saveNewMeetingRequest(meetingObj,senderSelectedTimezone,isWeb2Lead,callback){
    meetingManagementObj.saveNewMeeting(meetingObj,function(error,meeting){
        if(error){
            callback(false)
        }
        else{
            sendNewMeetingEmails(meeting.invitationId,meeting.senderId,isWeb2Lead,senderSelectedTimezone);
            addParticipantsAsContacts(meeting);
            storeCreateMeetingInteractions(meeting,null,false);
            callback(meeting)
        }
    })
}

function addParticipantsAsContacts(meeting){
    var senderContact = {
        personId:meeting.senderId,
        personName:meeting.senderName,
        personEmailId:meeting.senderEmailId,
        count:1,
        lastInteracted:new Date(),
        addedDate: new Date(),
        verified:false,
        relatasContact:true,
        relatasUser:false,
        source:'relatas',
        mobileNumber:null
    };

    if(meeting.selfCalendar){
        if(meeting.toList && meeting.toList.length > 0){
            for(var i=0; i<meeting.toList.length; i++){
                if(!meeting.toList[i].isResource){
                    if(common.checkRequired(meeting.toList[i].receiverFirstName) && common.checkRequired(meeting.toList[i].receiverEmailId) && meeting.toList[i].receiverEmailId != meeting.senderEmailId && meeting.toList[i].receiverId != meeting.senderId){
                        var nameN = meeting.toList[i].receiverFirstName+" "+meeting.toList[i].receiverLastName || ""
                        var contact = {
                            personId:meeting.toList[i].receiverId,
                            personName:nameN,
                            personEmailId:meeting.toList[i].receiverEmailId,
                            count:1,
                            lastInteracted:new Date(),
                            addedDate: new Date(),
                            verified:false,
                            relatasContact:true,
                            relatasUser:false,
                            source:'relatas',
                            mobileNumber:null
                        };
                        if(common.checkRequired(meeting.senderId)){
                            addContact(meeting.senderId,contact)
                        }
                    }
                    if(common.checkRequired(meeting.toList[i].receiverId)){
                        addContact(meeting.toList[i].receiverId,senderContact)
                    }
                }
            }
        }
    }
    else if(common.checkRequired(meeting.to) && common.checkRequired(meeting.to.receiverEmailId) && meeting.to.receiverId != meeting.senderId && meeting.to.receiverEmailId != meeting.senderEmailId){
       if(common.checkRequired(meeting.to.receiverName)){
           var contact2 = {
               personId:meeting.to.receiverId,
               personName:meeting.to.receiverName,
               personEmailId:meeting.to.receiverEmailId,
               count:1,
               lastInteracted:new Date(),
               addedDate: new Date(),
               verified:false,
               relatasContact:true,
               relatasUser:false,
               source:'relatas',
               mobileNumber:null
           };
           if(common.checkRequired(meeting.senderId)){
               addContact(meeting.senderId,contact2)
           }
        }
        if(common.checkRequired(meeting.to.receiverId)){
            addContact(meeting.to.receiverId,senderContact)
        }
    }
}

function addContact(userId,contact){
    userManagements.findUserProfileByEmailIdWithCustomFields(contact.personEmailId,{emailId:1,firstName:1,lastName:1,mobileNumber:1,location:1,designation:1,companyName:1,skypeId:1},function(error,user){
        if(error){

        }
        else if(user && common.checkRequired(user._id)){
            contact.personId = user._id
            contact.personName = user.firstName+' '+user.lastName
            contact.companyName = user.companyName
            contact.designation = user.designation
            contact.mobileNumber = user.mobileNumber
            contact.location = user.location
            contact.skypeId = user.skypeId
            contact.relatasUser = true
        }
        contactObj.addSingleContact(userId,contact)
    })
}

function sendNewMeetingEmails(invitationId,userId,isWeb2Lead,senderSelectedTimezone){
    getMeetingById(invitationId,function(meeting){
        if(common.checkRequired(meeting)){
            var isMultiple = false;
            var senderId = meeting.senderId;

            var senderObj = {
                senderId:common.checkRequired(meeting.senderId) ? meeting.senderId : null,
                senderName:common.checkRequired(meeting.senderName) ? meeting.senderName : '',
                senderEmailId:common.checkRequired(meeting.senderEmailId) ? meeting.senderEmailId : '',
                senderSelectedTimezone: senderSelectedTimezone
            };

            var maxSlot;
            for( var j=0; j<meeting.scheduleTimeSlots.length; j++){
                if(meeting.scheduleTimeSlots[j].isAccepted){
                    maxSlot = meeting.scheduleTimeSlots[j];
                }
            }

            if(!common.checkRequired(maxSlot)){
                maxSlot = meeting.scheduleTimeSlots.reduce(function (a, b) {
                    return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                });
            }
            var receiverObjTop = {}
            if(meeting.selfCalendar){

                if(meeting.toList.length > 1){
                    isMultiple = true;
                }

                for(var i=0; i<meeting.toList.length; i++){
                    if(!meeting.toList[i].isResource){
                        var receiverNameF = meeting.toList[i].receiverFirstName || '';
                        var receiverNameL = meeting.toList[i].receiverLastName || '';
                        var rfn = receiverNameF+' '+receiverNameL;
                        var newReceiverObj = {
                            receiverId:common.checkRequired(meeting.toList[i].receiverId) ? meeting.toList[i].receiverId : null ,
                            receiverName:rfn,
                            receiverEmailId:common.checkRequired(meeting.toList[i].receiverEmailId) ? meeting.toList[i].receiverEmailId : ''
                        };
                        receiverObjTop = newReceiverObj;

                        sendNewMeetingRequestEmailNext(senderId,meeting.toList[i].receiverId,senderObj,newReceiverObj,meeting,maxSlot,'receiver',isMultiple,isWeb2Lead)
                    }
                }
            }
            else{
                var receiverObj = {
                    receiverId:common.checkRequired(meeting.to.receiverId) ? meeting.to.receiverId : null,
                    receiverName:common.checkRequired(meeting.to.receiverName) ? meeting.to.receiverName : '',
                    receiverEmailId:common.checkRequired(meeting.to.receiverEmailId) ? meeting.to.receiverEmailId : ''
                };
                receiverObjTop = receiverObj;
                sendNewMeetingRequestEmailNext(senderId,meeting.to.receiverId,senderObj,receiverObj,meeting,maxSlot,'receiver',isMultiple,isWeb2Lead)
            }

            if(isMultiple){
                sendNewMeetingRequestEmailNext_Multiple(senderId,senderObj,meeting,maxSlot);
            }
            else{
                sendNewMeetingRequestEmailNext(senderId,receiverObjTop.receiverId,senderObj,receiverObjTop,meeting,maxSlot,'sender',isMultiple,isWeb2Lead)
            }
        }
    })
}

function sendSuggestMeetingEmailNext(senderId,receiverId,senderObj,receiverObj,meeting,newSlot,oldSlot,action,message,storeCommentToInteractions){
    var messageAccess = new messageDataAccess();
    userManagements.findUserProfileByIdWithCustomFields(senderId,{firstName:1,lastName:1,emailId:1,timezone:1,companyName:1,designation:1,location:1},function(error,senderP){
        userManagements.findUserProfileByIdWithCustomFields(receiverId,{firstName:1,lastName:1,emailId:1,timezone:1,companyName:1,designation:1,location:1,partialFill:1},function(err,receiverP){
            if(action == 'sender'){
                var timezoneS;
                if(common.checkRequired(senderP) && common.checkRequired(senderP.timezone) && common.checkRequired(senderP.timezone.name)){
                    timezoneS = senderP.timezone.name;
                }else timezoneS = 'UTC';
                var newReceiverP = getUserInfoProfile(receiverP,receiverObj);

                var startSNew = moment(newSlot.start.date).tz(timezoneS || 'UTC');
                var dateSNew = startSNew.format("DD-MMM-YYYY");
                var timeSNew = startSNew.format("h:mm a z");

                var startSOld = moment(oldSlot.start.date).tz(timezoneS || 'UTC');
                var dateSOld = startSOld.format("DD-MMM-YYYY");
                var timeSOld = startSOld.format("h:mm a z");

                var dataToSendS = {
                    invitationId:meeting.invitationId,
                    receiverEmailId:common.checkRequired(receiverP) ? receiverP.emailId : receiverObj.receiverEmailId,
                    receiverName:common.checkRequired(receiverP) ? receiverP.firstName+' '+receiverP.lastName : receiverObj.receiverName,
                    senderEmailId:common.checkRequired(senderP) ? senderP.emailId : senderObj.senderEmailId,
                    senderName:common.checkRequired(senderP) ? senderP.firstName+' '+senderP.lastName : senderObj.senderName,
                    senderPic:common.checkRequired(senderP) ? domainName+'/getImage/'+senderP._id : 'http://relatas.com/images/default.png',
                    o_mDate:dateSOld,
                    o_mTime:timeSOld,
                    o_mLocation:oldSlot.locationType+' : '+oldSlot.location,
                    n_mDate:dateSNew,
                    n_mTime:timeSNew,
                    n_mLocation:newSlot.locationType+' : '+newSlot.location,
                    message:common.checkRequired(message) ? message : 'No Message'
                };

                emailSenders.sendInvitationMailToReceiver_NewTemplate_R_SuggestedTime_Sender(dataToSendS);

                if(message && common.checkRequired(message) && storeCommentToInteractions){

                    var messageObj = {
                        senderId:common.checkRequired(senderP) ? senderP._id : senderObj.senderId,
                        receiverId:common.checkRequired(receiverP) ? receiverP._id : receiverObj.receiverId,
                        receiverEmailId:common.checkRequired(receiverP) ? receiverP.emailId : receiverObj.receiverEmailId,
                        type:'meeting-comment',
                        subject:message,
                        message:oldSlot.title
                    };

                    messageAccess.createMessage(messageObj,function(messageSaved){
                        if(messageSaved){
                            var interaction = {};
                            interaction.userId = messageSaved.senderId;
                            interaction.emailId = common.checkRequired(senderP) ? senderP.emailId : senderObj.senderEmailId;
                            interaction.action = 'sender';
                            interaction.type = 'meeting-comment';
                            interaction.subType = 'meeting-comment';
                            interaction.refId = messageSaved._id;
                            interaction.source = 'relatas';
                            interaction.title = messageSaved.subject;
                            interaction.description = messageSaved.message;
                            //common.storeInteraction(interaction);

                            var interactionReceiver = {};
                            interactionReceiver.userId = messageSaved.receiverId || '';
                            interactionReceiver.emailId = messageSaved.receiverEmailId;
                            interactionReceiver.action = 'receiver';
                            interactionReceiver.type = 'meeting-comment';
                            interactionReceiver.subType = 'meeting-comment';
                            interactionReceiver.refId = messageSaved._id;
                            interactionReceiver.source = 'relatas';
                            interactionReceiver.title = messageSaved.subject;
                            interactionReceiver.description = messageSaved.message;
                            //common.storeInteractionReceiver(interactionReceiver);
                            storeAcceptMeetingInteractions(interaction,interactionReceiver);
                        }
                    });
                }
            }
            else{
                var timezone;
                if(common.checkRequired(receiverP) && common.checkRequired(receiverP.timezone) && common.checkRequired(receiverP.timezone.name)){
                    timezone = receiverP.timezone.name;
                }else if(common.checkRequired(senderP) && common.checkRequired(senderP.timezone) && common.checkRequired(senderP.timezone.name)){
                    timezone = senderP.timezone.name;
                }else timezone = 'UTC';
                var newSenderP = getUserInfoProfile(senderP,senderObj);

                var startNew = moment(newSlot.start.date).tz(timezone || 'UTC');
                var dateNew = startNew.format("DD-MMM-YYYY");
                var timeNew = startNew.format("h:mm a z");

                var startOld = moment(oldSlot.start.date).tz(timezone || 'UTC');
                var dateOld = startOld.format("DD-MMM-YYYY");
                var timeOld = startOld.format("h:mm a z");
                var partialProfile = false;

                if(receiverP && receiverP.partialFill){
                    partialProfile = true;
                }

                var dataToSend = {
                    invitationId:meeting.invitationId,
                    receiverEmailId:common.checkRequired(receiverP) ? receiverP.emailId : receiverObj.receiverEmailId,
                    receiverName:common.checkRequired(receiverP) ? receiverP.firstName+' '+receiverP.lastName : receiverObj.receiverName,
                    senderName:newSenderP.senderName,
                    senderPicUrl:common.checkRequired(senderP) ? domainName+'/getImage/'+senderP._id : 'http://relatas.com/images/default.png',
                    senderInfo:newSenderP.senderInfo,
                    senderLocation:newSenderP.senderLocation,
                    o_mDate:dateOld,
                    o_mTime:timeOld,
                    o_mLocation:oldSlot.locationType+' : '+oldSlot.location,
                    n_mDate:dateNew,
                    n_mTime:timeNew,
                    n_mLocation:newSlot.locationType+' : '+newSlot.location,
                    message:common.checkRequired(message) ? message : 'No Message',
                    partialProfile:partialProfile,
                    receiverId:receiverId
                };

                emailSenders.sendInvitationMailToReceiver_NewTemplate_R_SuggestedTime_Receiver(dataToSend);

                if(message && common.checkRequired(message) && storeCommentToInteractions){

                    var messageObj2 = {
                        senderId:common.checkRequired(senderP) ? senderP._id : newSenderP.senderId,
                        receiverId:common.checkRequired(receiverP) ? receiverP._id : receiverObj.receiverId,
                        receiverEmailId:common.checkRequired(receiverP) ? receiverP.emailId : receiverObj.receiverEmailId,
                        type:'meeting-comment',
                        subject:message,
                        message:oldSlot.title
                    };

                    messageAccess.createMessage(messageObj2,function(messageSaved){
                        if(messageSaved){
                            var interaction = {};
                            interaction.userId = messageSaved.senderId;
                            interaction.emailId = common.checkRequired(senderP) ? senderP.emailId : senderObj.senderEmailId;
                            interaction.action = 'sender';
                            interaction.type = 'meeting-comment';
                            interaction.subType = 'meeting-comment';
                            interaction.refId = messageSaved._id;
                            interaction.source = 'relatas';
                            interaction.title = messageSaved.subject;
                            interaction.description = messageSaved.message;
                            //common.storeInteraction(interaction);

                            var interactionReceiver = {};
                            interactionReceiver.userId = messageSaved.receiverId || '';
                            interactionReceiver.emailId = messageSaved.receiverEmailId;
                            interactionReceiver.action = 'receiver';
                            interactionReceiver.type = 'meeting-comment';
                            interactionReceiver.subType = 'meeting-comment';
                            interactionReceiver.refId = messageSaved._id;
                            interactionReceiver.source = 'relatas';
                            interactionReceiver.title = messageSaved.subject;
                            interactionReceiver.description = messageSaved.message;
                            //common.storeInteractionReceiver(interactionReceiver);
                            storeAcceptMeetingInteractions(interaction,interactionReceiver);
                        }
                    });
                }
            }
        });
    })
}

function sendSuggestMeetingEmailNext_Multiple(senderId,senderObj,meeting,slot){

    userManagements.findUserProfileByIdWithCustomFields(senderId,{firstName:1,lastName:1,emailId:1,timezone:1,companyName:1,designation:1,location:1},function(error,senderP){

        var timezoneS;
        if(common.checkRequired(senderP) && common.checkRequired(senderP.timezone) && common.checkRequired(senderP.timezone.name)){
            timezoneS = senderP.timezone.name;
        }else timezoneS = 'UTC';

        var startS = moment(slot.start.date).tz(timezoneS || 'UTC');
        var dateS = startS.format("DD-MMM-YYYY");
        var timeS = startS.format("h:mm a z");

        var meetingAttendees = '';

        if(meeting.toList.length > 0) {

            var comConnectionsLength = meeting.toList.length >= 3 ? 3 : meeting.toList.length;
            meetingAttendees = '<table>';
            var commonConnectionImages ='<tr>';
            var commonConnectionNames = '<tr>';
            for(var i=0; i<comConnectionsLength; i++){
                var firstName = common.checkRequired(meeting.toList[i].receiverFirstName) ? meeting.toList[i].receiverFirstName : getTextLength(meeting.toList[i].receiverEmailId,5);

                var iUrl = common.checkRequired(meeting.toList[i].receiverId) ? domainName+'/getImage/'+meeting.toList[i].receiverId : 'http://relatas.com/images/default.png';

                commonConnectionImages += '<td>';
                commonConnectionImages += '<div>';
                commonConnectionImages += '<img style="border-radius: 55%;width:30px" src='+iUrl+' title='+firstName.replace(/\s/g, '&nbsp;')+'><br></div>';
                commonConnectionImages += '</td>';
                commonConnectionNames += '<td><span title='+firstName.replace(/\s/g, '&nbsp;')+'>'+getTextLength(firstName,5)+'</span></td>';
            }

            if(meeting.toList.length > 3){
                var mUrl = domainName+'/today/details/'+meeting.invitationId;

                commonConnectionImages += '<td>';
                commonConnectionImages += '<a href='+mUrl+'>+</a>';
                commonConnectionImages += '</td>';
                commonConnectionNames += '<td>&nbsp;</td>';
            }

            commonConnectionImages += '</tr>';
            commonConnectionNames += '</tr>';
            meetingAttendees += commonConnectionImages+commonConnectionNames+'</table>';
        }

        var dataToSendS = {
            invitationId:meeting.invitationId,
            senderEmailId:common.checkRequired(senderP) ? senderP.emailId : senderObj.senderEmailId,
            senderName:common.checkRequired(senderP) ? senderP.firstName+' '+senderP.lastName : senderObj.senderName,
            senderPic:common.checkRequired(senderP) ? domainName+'/getImage/'+senderP._id : 'http://relatas.com/images/default.png',
            mAgenda:slot.description,
            mDate:dateS,
            mTime:timeS,
            mLocation:slot.locationType+' : '+slot.location,
            meetingAttendees:meetingAttendees
        };

        emailSenders.sendInvitationMailToReceiver_NewTemplate_R_SuggestedTime_Sender_Multi(dataToSendS);
    })
}

function sendNewMeetingRequestEmailNext(senderId,receiverId,senderObj,receiverObj,meeting,slot,action,multi,isWeb2Lead){
    
    var senderSelectedTimezone = senderObj.senderSelectedTimezone;
    var n_senderId = senderId;
    var n_receiverId = senderId;

    if(common.checkRequired(senderId)){
        n_senderId = mongoose.Types.ObjectId(senderId);
    }

    if(common.checkRequired(receiverId)){
        n_receiverId = mongoose.Types.ObjectId(receiverId);
    }
         userManagements.findUserProfileByIdWithCustomFields(senderId,{firstName:1,lastName:1,emailId:1,timezone:1,companyName:1,designation:1,location:1,registeredUser:1},function(error,senderP){
            userManagements.findUserProfileByIdWithCustomFields(receiverId,{firstName:1,lastName:1,emailId:1,timezone:1,companyName:1,designation:1,location:1},function(err,receiverP){
                if(action == 'sender'){
                    var timezoneS;
                    if(common.checkRequired(senderP) && common.checkRequired(senderP.timezone) && common.checkRequired(senderP.timezone.name)){
                        timezoneS = senderP.timezone.name;
                    }else timezoneS = senderSelectedTimezone;
                    var newReceiverP = getUserInfoProfile(receiverP,receiverObj);

                    var startS = moment(slot.start.date).tz(timezoneS || 'UTC');

                    var dateS = startS.format("DD-MMM-YYYY");
                    var timeS = startS.format("h:mm a z");
                    var isRegisteredUser = senderP.registeredUser;

                    getCommonConnectionsAndLastInteractionDetails({senderId:n_senderId,receiverId:n_receiverId,receiverEmailId:receiverObj.receiverEmailId,timezone:timezoneS || 'UTC'},function(obj){
                        var dataToSendS = {
                            invitationId:meeting.invitationId,
                            mAgenda:slot.description,
                            senderEmailId:common.checkRequired(senderP) ? senderP.emailId : senderObj.senderEmailId,
                            senderName:common.checkRequired(senderP) ? senderP.firstName+' '+senderP.lastName : senderObj.senderName,
                            receiverPic:common.checkRequired(receiverP) ? domainName+'/getImage/'+receiverP._id : 'http://relatas.com/images/default.png',
                            receiverName:newReceiverP.senderName,
                            receiverInfo:newReceiverP.senderInfo,
                            receiverLocation:newReceiverP.senderLocation,
                            mDate:dateS,
                            mTime:timeS,
                            mLocation:slot.locationType+' : '+slot.location,
                            lastInteractedOn:obj.lastInteractedOn == 'none' ? obj.lastInteractedOn : moment(obj.lastInteracted).tz(timezoneS || 'UTC').format("DD-MMM-YYYY"),
                            totalInteractions:obj.totalInteractions,
                            commonConnections:obj.commonConnections,
                            totalCommonConnections:obj.totalCommonConnections,
                            n_senderId:n_senderId,
                            isRegisteredUser: isRegisteredUser
                        };

                        if(common.checkRequired(receiverP)){
                            dataToSendS.interactionUrl = domainName+'/today/details/'+meeting.invitationId;
                        }

                        if(!isWeb2Lead){
                            emailSenders.sendInvitationMailToReceiver_NewTemplate_R_Meeting_Sender(dataToSendS);
                        } else {
                            
                            var sName = dataToSendS.senderName;
                            dataToSendS.receiverEmailId = dataToSendS.senderEmailId;
                            dataToSendS.senderPicUrl = dataToSendS.receiverPic;
                            dataToSendS.senderName = receiverObj.receiverName;
                            dataToSendS.receiverName = sName;
                            dataToSendS.isRegisteredUser = false;

                            emailSenders.web2Lead_Receiver(dataToSendS);
                        }
                    });
                }
                else{
                    var timezone;
                    if(common.checkRequired(receiverP) && common.checkRequired(receiverP.timezone) && common.checkRequired(receiverP.timezone.name)){
                        timezone = receiverP.timezone.name;
                    }else if(common.checkRequired(senderP) && common.checkRequired(senderP.timezone) && common.checkRequired(senderP.timezone.name)){
                        timezone = senderP.timezone.name;
                    }else timezone = senderSelectedTimezone;
                    var newSenderP = getUserInfoProfile(senderP,senderObj);

                    var start = moment(slot.start.date).tz(timezone || senderSelectedTimezone);
                    var date = start.format("DD-MMM-YYYY");
                    var time = start.format("h:mm a z");
                    getCommonConnectionsAndLastInteractionDetails({senderId:n_senderId,receiverId:n_receiverId,receiverEmailId:receiverObj.receiverEmailId,timezone:timezone || 'UTC'},function(obj){
                        var dataToSend = {
                            invitationId:meeting.invitationId,
                            mAgenda:slot.description,
                            senderName:newSenderP.senderName,
                            receiverEmailId:common.checkRequired(receiverP) ? receiverP.emailId : receiverObj.receiverEmailId,
                            receiverName:common.checkRequired(receiverP) ? receiverP.firstName+' '+receiverP.lastName : receiverObj.receiverName,
                            senderPicUrl:common.checkRequired(senderP) ? domainName+'/getImage/'+senderP._id : 'http://relatas.com/images/default.png',
                            senderInfo:newSenderP.senderInfo,
                            senderLocation:newSenderP.senderLocation,
                            mDate:date,
                            mTime:time,
                            mLocation:slot.locationType+' : '+slot.location,
                            lastInteractedOn:obj.lastInteractedOn == 'none' ? obj.lastInteractedOn : moment(obj.lastInteracted).tz(timezone).format("DD-MMM-YYYY"),
                            totalInteractions:obj.totalInteractions,
                            commonConnections:obj.commonConnections,
                            totalCommonConnections:obj.totalCommonConnections
                        }

                        if(common.checkRequired(senderP)){
                            dataToSend.interactionUrl = domainName+'/today/details/'+meeting.invitationId;
                        }
                        if(!isWeb2Lead){
                            emailSenders.sendInvitationMailToReceiver_NewTemplate_R_Meeting_Receiver(dataToSend);
                        } else {
                            var sName = dataToSend.senderName;
                            var rName = dataToSend.receiverName;

                            dataToSend.senderEmailId = dataToSend.receiverEmailId
                            dataToSend.senderName = rName
                            dataToSend.receiverName = sName;
                            dataToSend.receiverInfo = dataToSend.senderInfo;
                            dataToSend.receiverPic = dataToSend.senderPicUrl
                            emailSenders.web2Lead_Sender(dataToSend);
                        }
                    })
                }
            });
         })
}

function sendNewMeetingRequestEmailNext_Multiple(senderId,senderObj,meeting,slot){

        userManagements.findUserProfileByIdWithCustomFields(senderId,{firstName:1,lastName:1,emailId:1,timezone:1,companyName:1,designation:1,location:1},function(error,senderP){

                    var timezoneS;
                    if(common.checkRequired(senderP) && common.checkRequired(senderP.timezone) && common.checkRequired(senderP.timezone.name)){
                        timezoneS = senderP.timezone.name;
                    }else timezoneS = 'UTC';

                    var startS = moment(slot.start.date).tz(timezoneS || 'UTC');
                    var dateS = startS.format("DD-MMM-YYYY");
                    var timeS = startS.format("h:mm a z");

            var meetingAttendees = '';

            if(meeting.toList.length > 0) {

                var comConnectionsLength = meeting.toList.length >= 3 ? 3 : meeting.toList.length;
                meetingAttendees = '<table>';
                var commonConnectionImages ='<tr>';
                var commonConnectionNames = '<tr>';
                for(var i=0; i<comConnectionsLength; i++){
                    var firstName = common.checkRequired(meeting.toList[i].receiverFirstName) ? meeting.toList[i].receiverFirstName : getTextLength(meeting.toList[i].receiverEmailId,5);

                    var iUrl = common.checkRequired(meeting.toList[i].receiverId) ? domainName+'/getImage/'+meeting.toList[i].receiverId : 'http://relatas.com/images/default.png';

                    commonConnectionImages += '<td>';
                    commonConnectionImages += '<div>';
                    commonConnectionImages += '<img style="border-radius: 55%;width:30px" src='+iUrl+' title='+firstName.replace(/\s/g, '&nbsp;')+'><br></div>';
                    commonConnectionImages += '</td>';
                    commonConnectionNames += '<td><span title='+firstName.replace(/\s/g, '&nbsp;')+'>'+getTextLength(firstName,5)+'</span></td>';
                }

                if(meeting.toList.length > 3){
                    var mUrl = domainName+'/today/details/'+meeting.invitationId;

                    commonConnectionImages += '<td>';
                    commonConnectionImages += '<a href='+mUrl+'>+</a>';
                    commonConnectionImages += '</td>';
                    commonConnectionNames += '<td>&nbsp;</td>';
                }

                commonConnectionImages += '</tr>';
                commonConnectionNames += '</tr>';
                meetingAttendees += commonConnectionImages+commonConnectionNames+'</table>';

            }

                    var dataToSendS = {
                        invitationId:meeting.invitationId,
                        senderEmailId:common.checkRequired(senderP) ? senderP.emailId : senderObj.senderEmailId,
                        senderName:common.checkRequired(senderP) ? senderP.firstName+' '+senderP.lastName : senderObj.senderName,
                        senderPic:common.checkRequired(senderP) ? domainName+'/getImage/'+senderP._id : 'http://relatas.com/images/default.png',
                        mAgenda:slot.description,
                        mDate:dateS,
                        mTime:timeS,
                        mLocation:slot.locationType+' : '+slot.location,
                        meetingAttendees:meetingAttendees
                    };

                    emailSenders.sendInvitationMailToReceiver_NewTemplate_R_Meeting_Sender_Multi(dataToSendS);
        })
}

function sendConfirmMailNext(sender,receiver,action,message,mUrl,maxSlot,senderName){
    if(action == 'sender'){

        var timezone1;
        if(common.checkRequired(sender.timezone) && common.checkRequired(sender.timezone.name)){
            timezone1 = sender.timezone.name;
        }else timezone1 = 'UTC';

        var dataToSendSender = getUserInfo(receiver);
        dataToSendSender.emailId = sender.emailId;
        dataToSendSender.receiverName = sender.firstName+' '+sender.lastName;
        dataToSendSender.message = common.checkRequired(message) ? message : 'No Message';
        dataToSendSender.mUrl = mUrl;
        dataToSendSender.subject = 'Relatas : Your meeting with '+dataToSendSender.senderName+' is confirmed';
        dataToSendSender.senderPic = domainName+'/getImage/'+receiver._id;
        dataToSendSender.senderName2 = senderName;

        var start = moment(maxSlot.start.date).tz(timezone1 || 'UTC');
        var date = start.format("DD-MMM-YYYY");
        var time = start.format("h:mm a z");
        dataToSendSender.mDate = date;
        dataToSendSender.mTime = time;
        dataToSendSender.mLocation = maxSlot.locationType+' : '+maxSlot.location;
        emailSenders.sendMeetingConfirmEmail_new(dataToSendSender);
    }
    else{
        var timezone2;
        if(common.checkRequired(receiver.timezone) && common.checkRequired(receiver.timezone.name)){
            timezone2 = receiver.timezone.name;
        }else timezone2 = 'UTC';

        var dataToSendReceiver = getUserInfo(sender);
        dataToSendReceiver.emailId = receiver.emailId;
        dataToSendReceiver.receiverName = receiver.firstName+' '+receiver.lastName;
        dataToSendReceiver.message = common.checkRequired(message) ? message : 'No Message';
        dataToSendReceiver.mUrl = mUrl;
        dataToSendReceiver.subject = 'Relatas : Your meeting with '+dataToSendReceiver.senderName+' is confirmed';
        dataToSendReceiver.senderPic = domainName+'/getImage/'+sender._id;
        dataToSendReceiver.senderName2 = senderName;

        var start2 = moment(maxSlot.start.date).tz(timezone2 || 'UTC');
        var date2 = start2.format("DD-MMM-YYYY");
        var time2 = start2.format("h:mm a z");
        dataToSendReceiver.mDate = date2;
        dataToSendReceiver.mTime = time2;
        dataToSendReceiver.mLocation = maxSlot.locationType+' : '+maxSlot.location;
        emailSenders.sendMeetingConfirmEmail_new(dataToSendReceiver);
    }
}

function getUserInfo(user){
    var senderName_new = user.firstName+' '+user.lastName;

    var cName = user.companyName || '';
    var des = user.designation || '';
    var comma = (common.checkRequired(cName) && common.checkRequired(des)) ? ', ' : ' ';
    var senderInfo = des+comma+cName;
    var senderLocation = user.location;
    return {senderName:senderName_new,senderInfo:senderInfo,senderLocation:senderLocation};
}

function getUserInfoProfile(userP,userObj){
    var senderName_new = '';
    var cName = '';
    var des = '';
    var comma = '';
    var senderInfo = '';
    var senderLocation = '';
    var senderPic = '';
    if(common.checkRequired(userP)){
        senderName_new = userP.firstName+' '+userP.lastName;
        cName = userP.companyName || '';
        des = userP.designation || '';
        comma = (common.checkRequired(cName) && common.checkRequired(des)) ? ', ' : ' ';
        senderInfo = des+comma+cName;
        senderLocation = userP.location;
        senderPic = domainName+'/getImage/'+userP._id;
    }
    else{
        senderName_new = userObj.senderName;
        senderInfo = des+comma+cName;
        senderLocation = userObj.location;
    }

    return {senderName:senderName_new,senderInfo:senderInfo,senderLocation:senderLocation};
}

function getUserProfile(userId,callback){
   userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,firstName:1,lastName:1,location:1,currentLocation:1,companyName:1,designation:1},function(err,user){
       callback(user)
   })
}

function getAllParticipantsUserIdList(meeting){
    var list = [];
    if(common.checkRequired(meeting.senderId)){
        list.push(common.castToObjectId(meeting.senderId));
    }

    if(meeting.selfCalendar){
        for(var i=0; i<meeting.toList.length; i++){
            if(common.checkRequired(meeting.toList[i].receiverId)){
                list.push(common.castToObjectId(meeting.toList[i].receiverId))
            }
        }
    }
    else if(meeting.to && meeting.to.receiverId){
        list.push(common.castToObjectId(meeting.to.receiverId))
    }
    return list;
}

function cancelMeetingNextStep(userId,meeting,participantListId,message,timezone,clearGoogleEvent,clearIcs,callback){

    if(meeting.isGoogleMeeting == true){
        googleCalendar.acceptGoogleMeeting(userId,meeting.googleEventId,meeting.scheduleTimeSlots[0],"declined",meeting,function(isSuccess){
           if(isSuccess){
               if(clearIcs && common.checkRequired(meeting.icsFile) && common.checkRequired(meeting.icsFile.awsKey)){
                   deleteIcsFileFromS3(meeting.icsFile.awsKey);
               }

               updateMeetingToCancel(userId,meeting.invitationId,participantListId,meeting.selfCalendar,message,timezone,clearIcs,function(result){
                   if(result){
                       callback(true)
                   }
                   else callback(false,'MEETING_CANCEL_ERROR');
               })
           }
           else{
               callback(false,'MEETING_CANCEL_ERROR');
           }
        })
    }else
    if(clearGoogleEvent){
        googleCalendar.removeEventFromGoogleCalendarForMeeting(meeting.googleEventId,meeting.lastActionUserId || userId,function(isSuccess){
            if(isSuccess){
                if(clearIcs && common.checkRequired(meeting.icsFile) && common.checkRequired(meeting.icsFile.awsKey)){
                    deleteIcsFileFromS3(meeting.icsFile.awsKey);
                }

                updateMeetingToCancel(userId,meeting.invitationId,participantListId,meeting.selfCalendar,message,timezone,clearIcs,function(result){
                    if(result){
                        callback(true)
                    }
                    else callback(false,'MEETING_CANCEL_ERROR');
                })
            }
            else{
                callback(false,'MEETING_CANCEL_ERROR');
            }
        })
    }
    else{
        updateMeetingToCancel(userId,meeting.invitationId,participantListId,meeting.selfCalendar,message,timezone,false,function(result){
            if(result){
                callback(true)
            }
            else callback(false,'MEETING_CANCEL_ERROR');
        })
    }
}

function isAuthorisedUserToAccessMeeting(invitation,userId){
    if(invitation.senderId == userId){
       return true
    }
    else{

        var exist = false;
        var canceled = false;
        for(var i=0; i<invitation.toList.length; i++){

            if(invitation.toList[i].receiverId == userId){
                if(!invitation.toList[i].isResource){
                    if(invitation.toList[i].canceled){
                        canceled = true;
                    }else
                        exist = true;
                }
                else canceled = true;
            }
        }

        if(invitation.to){
            if(invitation.to.receiverId == userId){
                if(invitation.to.canceled){
                    canceled = true;
                }else
                    exist = true;
            }
        }

        if(canceled){
           return false;
        }else
        if(exist){
            return true;
        }
        else{
           return false;
        }
    }
}

function isMeetingAlreadyAccepted(meeting,userId){
    if(meeting.selfCalendar){
        var isAcceptedS = false;
        for(var j=0; j<meeting.toList.length; j++){
            if(meeting.toList[j].receiverId == userId && meeting.toList[j].isAccepted){
                isAcceptedS = true;
                break;
            }
        }
        return isAcceptedS;
    }
    else{
        var isAccepted = false;
        for(var i=0; i<meeting.scheduleTimeSlots.length; i++){
            if(meeting.scheduleTimeSlots[i].isAccepted){
                isAccepted = true;
                break;
            }
        }
        return isAccepted;
    }
}

function createOrUpdateGoogleEvent(userId,eventDetails,create,update,googleEventId,invitation,count,oneP,isGoogleMeeting,isOfficeOutlookMeeting,callback){
    if(isGoogleMeeting == true){

        if(invitation.scheduleTimeSlots.length > 0){
            googleCalendar.acceptGoogleMeeting(userId,googleEventId,invitation.scheduleTimeSlots[0],"accepted",invitation,function(result){
                if(result){
                    updateReceiverContacts(userId,eventDetails.senderId);
                    updateReceiverContacts(eventDetails.senderId,userId);
                    var interaction = {};
                    interaction.userId = eventDetails.receiverId;
                    interaction.action = 'receiver';
                    interaction.interactionDate = eventDetails.start;
                    interaction.endDate = eventDetails.end;
                    interaction.type = 'meeting';
                    interaction.subType = eventDetails.locationType;
                    interaction.refId = eventDetails.invitationId;
                    interaction.source = 'relatas';
                    interaction.title = eventDetails.title;
                    interaction.description = eventDetails.description;
                    interaction.trackInfo = {lastOpenedOn: new Date()};
                    //common.storeInteraction(interaction);

                    var interactionNew = {};
                    interactionNew.userId = eventDetails.senderId;
                    interactionNew.action = 'sender';
                    interactionNew.interactionDate = eventDetails.start;
                    interactionNew.endDate = eventDetails.end;
                    interactionNew.type = 'meeting';
                    interactionNew.subType = eventDetails.locationType;
                    interactionNew.refId = eventDetails.invitationId;
                    interactionNew.source = 'relatas';
                    interactionNew.title = eventDetails.title;
                    interactionNew.description = eventDetails.description;
                    interactionNew.trackInfo = {lastOpenedOn: new Date()};
                    //common.storeInteraction(interactionNew);
                    //storeAcceptMeetingInteractions(interactionNew,interaction);
                    var senderId = common.checkRequired(eventDetails.senderId) ? common.castToObjectId(eventDetails.senderId) : null;
                    var receiverId = common.checkRequired(eventDetails.receiverId) ? common.castToObjectId(eventDetails.receiverId) : null;
                    updateMeetingInteractions(senderId,eventDetails.senderEmailId,receiverId,eventDetails.receiverEmailId,new Date(),null,eventDetails.invitationId);

                    callback(true,null);
                }
                else {
                    callback(false,'CREATE_GOOGLE_EVENT_FAILED');
                }
            })
        }
        else {
            callback(false, "No time slot found")
        }
    }
    else{
        
        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1},function(error,userProfile) {
            if (error) {
                logger.error('Error occurred while searching authenticated user profile in the DB');
                callback(false,'ERROR_FETCHING_PROFILE')
            }
            else{
                var userInfo=userProfile;
                if(common.checkRequired(userInfo.google)){
                    if(common.checkRequired(userInfo.google[0])){
                        if(common.checkRequired(userInfo.google[0].id)){
                            var tokenProvider = new GoogleTokenProvider({
                                refresh_token: userInfo.google[0].refreshToken,
                                client_id:     authConfig.GOOGLE_CLIENT_ID,
                                client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                                access_token: userInfo.google[0].token
                            });
                            tokenProvider.getToken(function (err, token) {
                                if (err) {
                                    logger.info('Error in token provider'+err);
                                }
                                var location = eventDetails.locationType+': '+eventDetails.location;
                                var id = eventDetails.invitationId || simple_unique_id.generate('googleRelatas');
                                var google_calendar = new gcal.GoogleCalendar(token);
                                var calendarId='primary';
                                var gId = 'relat'+getNumString(count)+id;
                                var updateCountFlag = true;
                                if(invitation.selfCalendar){
                                    if(invitation.toList.length > 1){
                                        gId = googleEventId ? googleEventId : gId;
                                        updateCountFlag = googleEventId ? false : true;
                                    }
                                }
                                var request_body = {
                                    id:gId,
                                    summary:eventDetails.title ,
                                    end:{
                                        dateTime:new Date(eventDetails.end)
                                    },
                                    start:{
                                        dateTime:new Date(eventDetails.start)
                                    },
                                    description:eventDetails.description,
                                    location:location,
                                    locationType:eventDetails.locationType,
                                    locationDetails:eventDetails.location,
                                    relatasEvent:true
                                };

                                if(oneP){
                                    request_body.attendees = [
                                        {
                                            email:userInfo.google[0].emailId,
                                            displayName:userInfo.firstName,
                                            responseStatus:"accepted"
                                        }
                                    ]
                                }
                                else{
                                    request_body.attendees = [
                                        {
                                            email:userInfo.google[0].emailId,
                                            displayName:userInfo.firstName,
                                            responseStatus:"accepted"
                                        },
                                        {
                                            email:eventDetails.senderPrimaryEmail || eventDetails.senderEmailId,
                                            displayName:eventDetails.senderName,
                                            responseStatus:"accepted"
                                        }
                                    ]
                                }

                                if(create){
                                    google_calendar.events.insert(calendarId,request_body,function(err,result){
                                        if(err){
                                            logger.error('creating event in google failed ',err);
                                            callback(false,'CREATE_GOOGLE_EVENT_FAILED');
                                        }
                                        else{
                                            count++;
                                            if(updateCountFlag)
                                                updateMeetingWithGoogleEventId(eventDetails.invitationId,result.id,result.iCalUID,count,userId);

                                            logger.info('creating event in google success');

                                            updateReceiverContacts(userId,eventDetails.senderId);
                                            updateReceiverContacts(eventDetails.senderId,userId);
                                            var interaction = {};
                                            interaction.userId = eventDetails.receiverId;
                                            interaction.action = 'receiver';
                                            interaction.interactionDate = eventDetails.start;
                                            interaction.endDate = eventDetails.end;
                                            interaction.type = 'meeting';
                                            interaction.subType = eventDetails.locationType;
                                            interaction.refId = eventDetails.invitationId;
                                            interaction.source = 'relatas';
                                            interaction.title = eventDetails.title;
                                            interaction.description = eventDetails.description;
                                            interaction.trackInfo = {lastOpenedOn: new Date()};
                                            //common.storeInteraction(interaction);

                                            var interactionNew = {};
                                            interactionNew.userId = eventDetails.senderId;
                                            interactionNew.action = 'sender';
                                            interactionNew.interactionDate = eventDetails.start;
                                            interactionNew.endDate = eventDetails.end;
                                            interactionNew.type = 'meeting';
                                            interactionNew.subType = eventDetails.locationType;
                                            interactionNew.refId = eventDetails.invitationId;
                                            interactionNew.source = 'relatas';
                                            interactionNew.title = eventDetails.title;
                                            interactionNew.description = eventDetails.description;
                                            interactionNew.trackInfo = {lastOpenedOn: new Date()};
                                            //common.storeInteraction(interactionNew);
                                            //storeAcceptMeetingInteractions(interactionNew,interaction);
                                            var senderId = common.checkRequired(eventDetails.senderId) ? common.castToObjectId(eventDetails.senderId) : null;
                                            var receiverId = common.checkRequired(eventDetails.receiverId) ? common.castToObjectId(eventDetails.receiverId) : null;
                                            updateMeetingInteractions(senderId,eventDetails.senderEmailId,receiverId,eventDetails.receiverEmailId,new Date(),null,eventDetails.invitationId);

                                            callback(true,null);
                                        }
                                    });
                                }
                                else if(update){
                                    var eventId = googleEventId;
                                    request_body.sequence=count;
                                    google_calendar.events.update(calendarId,eventId,request_body,function(err,result){

                                        if(err){
                                            logger.info('Error in Google event update '+JSON.stringify(err));
                                            callback(false,'UPDATE_GOOGLE_EVENT_FAILED');
                                        }
                                        else{
                                            logger.info('updating event in google success');
                                            count = count+1;
                                            updateMeetingWithGoogleEventId(eventDetails.invitationId,result.id,result.iCalUID,count,userId);
                                            callback(true,null);
                                        }
                                    });
                                }
                            }); // end of token provider
                        }else  callback(true,null);
                    }else  callback(true,null);
                }else  callback(true,null);
            }
        });
    }
}

function createOrUpdateOfficeEvent(userId,eventDetails,invitation,callback){
    if(invitation.scheduleTimeSlots.length > 0){
        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,outlook:1,firstName:1,lastName:1},function(error,userProfile) {
            if (error) {
                logger.error('Error in createOrUpdateOfficeEvent():meetingSupportClass ',error);
                callback(false,'ERROR_FETCHING_PROFILE');
            }
            else{
                var userInfo=userProfile;
                if(officeOutlook.validateOffice365Acc(userInfo)){
                     officeOutlook.getNewAccessToken(userInfo.officeCalendar.refresh_token,userInfo._id,function(token){
                          if(common.checkRequired(token)){
                              officeOutlook.createOutlookCalEvent(token.access_token,eventDetails,function(error,result){
                                  if(!error && result){

                                      logger.info('creating event in outlook success');
                                      userManagements.updateMeetingWithOfficeEventId(eventDetails.invitationId,result.Id,result.iCalUId,userId,function(error,isSuccess,message){
                                           logger.info(message);
                                      });
                                      updateReceiverContacts(userId,eventDetails.senderId);
                                      updateReceiverContacts(eventDetails.senderId,userId);
                                      var interaction = {};
                                      interaction.userId = eventDetails.receiverId;
                                      interaction.action = 'receiver';
                                      interaction.interactionDate = eventDetails.start;
                                      interaction.endDate = eventDetails.end;
                                      interaction.type = 'meeting';
                                      interaction.subType = eventDetails.locationType;
                                      interaction.refId = eventDetails.invitationId;
                                      interaction.source = 'office365';
                                      interaction.title = eventDetails.title;
                                      interaction.description = eventDetails.description;
                                      interaction.trackInfo = {lastOpenedOn: new Date()};
                                      //common.storeInteraction(interaction);

                                      var interactionNew = {};
                                      interactionNew.userId = eventDetails.senderId;
                                      interactionNew.action = 'sender';
                                      interactionNew.interactionDate = eventDetails.start;
                                      interactionNew.endDate = eventDetails.end;
                                      interactionNew.type = 'meeting';
                                      interactionNew.subType = eventDetails.locationType;
                                      interactionNew.refId = eventDetails.invitationId;
                                      interactionNew.source = 'office365';
                                      interactionNew.title = eventDetails.title;
                                      interactionNew.description = eventDetails.description;
                                      interactionNew.trackInfo = {lastOpenedOn: new Date()};
                                      //common.storeInteraction(interactionNew);
                                      //storeAcceptMeetingInteractions(interactionNew,interaction);
                                      var senderId = common.checkRequired(eventDetails.senderId) ? common.castToObjectId(eventDetails.senderId) : null;
                                      var receiverId = common.checkRequired(eventDetails.receiverId) ? common.castToObjectId(eventDetails.receiverId) : null;
                                      updateMeetingInteractions(senderId,eventDetails.senderEmailId,receiverId,eventDetails.receiverEmailId,new Date(),null,eventDetails.invitationId);

                                      callback(true,null);
                                  }
                                  else callback(false,'CREATE_GOOGLE_EVENT_FAILED');
                              })
                          }
                     });
                }else callback(false,'OUTLOOK_ACCOUNT_NOT_FOUND');
            }
        });
    }
    else callback(false, "NO_TIME_SLOT_FOUND");
}

function updateOfficeEvent(userId,eventDetails,invitation,callback){

    if(invitation.scheduleTimeSlots.length > 0){
        updateReceiverContacts(userId,eventDetails.senderId);
        updateReceiverContacts(eventDetails.senderId,userId);
        var interaction = {};
        interaction.userId = eventDetails.receiverId;
        interaction.action = 'receiver';
        interaction.interactionDate = eventDetails.start;
        interaction.endDate = eventDetails.end;
        interaction.type = 'meeting';
        interaction.subType = eventDetails.locationType;
        interaction.refId = eventDetails.invitationId;
        interaction.source = 'outlook';
        interaction.title = eventDetails.title;
        interaction.description = eventDetails.description;
        interaction.trackInfo = {lastOpenedOn: new Date()};
        //common.storeInteraction(interaction);

        var interactionNew = {};
        interactionNew.userId = eventDetails.senderId;
        interactionNew.action = 'sender';
        interactionNew.interactionDate = eventDetails.start;
        interactionNew.endDate = eventDetails.end;
        interactionNew.type = 'meeting';
        interactionNew.subType = eventDetails.locationType;
        interactionNew.refId = eventDetails.invitationId;
        interactionNew.source = 'outlook';
        interactionNew.title = eventDetails.title;
        interactionNew.description = eventDetails.description;
        interactionNew.trackInfo = {lastOpenedOn: new Date()};
        //common.storeInteraction(interactionNew);
        //storeAcceptMeetingInteractions(interactionNew,interaction);
        var senderId = common.checkRequired(eventDetails.senderId) ? common.castToObjectId(eventDetails.senderId) : null;
        var receiverId = common.checkRequired(eventDetails.receiverId) ? common.castToObjectId(eventDetails.receiverId) : null;
        updateMeetingInteractions(senderId,eventDetails.senderEmailId,receiverId,eventDetails.receiverEmailId,new Date(),null,eventDetails.invitationId);

        callback(true,null);
    }
    else callback(false, "NO_TIME_SLOT_FOUND");
}

function getNumString(num){
    return num < 10 ? '0'+num : ''+num
}

function getMeetingById(invitationId,callback){

    if(common.checkRequired(invitationId)){
        userManagements.findInvitationById(invitationId,function(error,invitation,message){

            if(error || !invitation){
                callback(false)
            }
            else callback(invitation)
        });
    }else callback(false)
}

function updateReceiverContacts(senderId,receiverId){

    if(common.checkRequired(senderId) && common.checkRequired(receiverId)){
        userManagements.findUserProfileByIdWithCustomFields(senderId,{firstName:1,lastName:1,emailId:1,mobileNumber:1,companyName:1,designation:1,location:1},function(error,profile) {
            if(error){

            }
            else if(profile != null || profile != undefined){

                var contact1 = {
                    personId:profile._id,
                    personName:profile.firstName+' '+profile.lastName,
                    personEmailId:profile.emailId,
                    companyName:profile.companyName,
                    designation:profile.designation,
                    location:profile.location,
                    count:1,
                    lastInteracted:new Date(),
                    addedDate: new Date(),
                    verified:true,
                    relatasContact:true,
                    relatasUser:true,
                    mobileNumber:profile.mobileNumber,
                    source:'relatas'
                };
                updateContact(receiverId,contact1);
            }
        });
    }
}

//Function to add Contact
function updateContact(userId,contact){
    contactObj.addSingleContact(userId,contact)
}

function updateMeetingWithGoogleEventId(invitationId,googleEId,iCalUID,count,userId){

    userManagements.updateMeetingWithGoogleEventId(invitationId,googleEId,iCalUID,count,userId,function(error,isUpdated,message){
        logger.info(message.message);
    })
}

function acceptMeetingSelfCalendar(invitation,callback){

    userManagements.updateInvitationToListAccepted(invitation.invitationId,invitation,function(error,isSuccess,message){

        if(error || !isSuccess){
            logger.info('Update meeting failed 2');
            callback(false,'UPDATE_MEETING_FAILED');
        }else{
            generateIcsFile(invitation,function(result,err){
                callback(result,err);
            })
        }
    })
}

function generateIcsFile(invitation,callback){
    var icsFormat = new Ics();

    userManagements.findInvitationById(invitation.invitationId,function(error,meeting){
        if(common.checkRequired(meeting)){
            icsFormat.createMeetingIcs(meeting,invitation.timezone,invitation.receiverName,function(icsFile){
                callback(icsFile,null);
            })
        }else{
            //sendAcceptInvitationEmail(invitation);

            callback(false,'GENERATING_ICS_FAILED');
        }
    })
}

function getAcceptMeetingObject(invitationId,userId,slotId,message,meeting){
    
    if(meeting && common.checkRequired(meeting)){
        var acceptObj = {};

        acceptObj.recurrenceId = meeting.recurrenceId?meeting.recurrenceId:null
        acceptObj.icsFile = meeting.icsFile;

        acceptObj.isSuggested = common.checkRequired(meeting.suggested) ? meeting.suggested : false;
        acceptObj.slotId = slotId;
        acceptObj.invitationId = invitationId;
        acceptObj.message = message || '';
        acceptObj.selfCalendar = meeting.selfCalendar;

        if(meeting.suggested && meeting.selfCalendar){
            acceptObj.toListUserId = meeting.toList[0].receiverId;
        }

        for(var k=0; k<meeting.scheduleTimeSlots.length; k++){
            if(meeting.scheduleTimeSlots[k]._id == slotId){
                acceptObj.title = meeting.scheduleTimeSlots[k].title || '';
                acceptObj.description = meeting.scheduleTimeSlots[k].description || '';
                acceptObj.location = meeting.scheduleTimeSlots[k].location || '';
                acceptObj.locationType = meeting.scheduleTimeSlots[k].locationType || '';
                acceptObj.start = meeting.scheduleTimeSlots[k].start.date;
                acceptObj.end = meeting.scheduleTimeSlots[k].end.date;
            }
        }

        if(meeting.senderId != userId){
            if(meeting.selfCalendar){
                acceptObj.toListLength = meeting.toList.length;

                for(var l=0; l<meeting.toList.length; l++){
                    if(meeting.toList[l].receiverId == userId){

                        var rFName = meeting.toList[l].receiverFirstName || '';
                        var rLName = meeting.toList[l].receiverLastName || '';

                        acceptObj.receiverId = meeting.toList[l].receiverId;
                        acceptObj.receiverName = rFName+' '+rLName;
                        acceptObj.receiverEmailId = meeting.toList[l].receiverEmailId
                    }
                }
            }
            else{
                acceptObj.receiverId = meeting.to.receiverId;
                acceptObj.receiverName = meeting.to.receiverName;
                acceptObj.receiverEmailId = meeting.to.receiverEmailId;
            }
            acceptObj.senderId = meeting.senderId;
            acceptObj.senderName = meeting.senderName;
            acceptObj.senderEmailId = meeting.senderEmailId;
        }
        else{
            acceptObj.receiverId = meeting.senderId;
            acceptObj.receiverName = meeting.senderName;
            acceptObj.receiverEmailId = meeting.senderEmailId;

            if (meeting.selfCalendar) {
                acceptObj.toListLength = meeting.toList.length;

                var FName = meeting.toList[0].receiverFirstName || '';
                var LName = meeting.toList[0].receiverLastName || '';

                acceptObj.senderId = meeting.toList[0].receiverId;
                acceptObj.senderName = FName + ' ' + LName;
                acceptObj.senderEmailId = meeting.toList[0].receiverEmailId

            }
            else {
                acceptObj.senderId = meeting.to.receiverId;
                acceptObj.senderName = meeting.to.receiverName;
                acceptObj.senderEmailId = meeting.to.receiverEmailId;
            }
        }

        return acceptObj;

    }
    else return null;
}

function updateMeetingToCancel(userId,invitationId,participantListId,selfCalendar,message,timezone,clearIcs,callback){
    meetingManagementObj.cancelMeeting(userId,invitationId,selfCalendar,message,clearIcs,function(result){
        if(common.checkRequired(result)){

            var dataToSend_sender = {};
            var dataToSend_receiver = {};
            dataToSend_sender.emailId = result.senderEmailId;
            dataToSend_sender.toName = result.senderName;
            var maxSlot;
            for(var i=0; i<result.scheduleTimeSlots.length; i++){
                if(result.scheduleTimeSlots[i].isAccepted){
                    maxSlot = result.scheduleTimeSlots[i];
                }
            }

            if(!common.checkRequired(maxSlot)){
                maxSlot = result.scheduleTimeSlots.reduce(function (a, b) {
                    return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                });
            }

            userManagements.findUserProfileByIdWithCustomFields(userId,{firstName:1,lastName:1,emailId:1,timezone:1,companyName:1,designation:1,location:1},function(error,senderPr){
                if(common.checkRequired(senderPr)){
                    if(common.checkRequired(result.senderId)){
                        sendMeetingDeleteCancelEmails(result.senderId,null,senderPr,maxSlot,message,true,'Canceled');
                    }
                    else sendMeetingDeleteCancelEmails(result.senderEmailId,null,senderPr,maxSlot,message,false,'Canceled');
                }
            });

            callback(true);
        }else
            callback(result);
    })
}

function updateMeetingToDelete(userId,invitationId,participantListId,timezone,message,partialMeeting,callback){

    meetingManagementObj.deleteMeeting(userId,invitationId,partialMeeting,function(result){
        if(result != false && common.checkRequired(result)){
            interaction.removeInteractions_new(participantListId,result.invitationId);

            userManagements.findUserProfileByIdWithCustomFields(userId,{firstName:1,lastName:1,emailId:1,timezone:1,companyName:1,designation:1,location:1},function(err,senderPr){
                if(common.checkRequired(senderPr)){
                    var maxSlot;
                    for( var i=0; i<result.scheduleTimeSlots.length; i++){
                        if(result.scheduleTimeSlots[i].isAccepted){
                            maxSlot = result.scheduleTimeSlots[i];
                        }
                    }

                    if(!common.checkRequired(maxSlot)){
                        maxSlot = result.scheduleTimeSlots.reduce(function (a, b) {
                            return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                        });
                    }

                    if(result.selfCalendar){
                        for(var l=0; l<result.toList.length; l++){
                            sendMeetingDeleteCancelEmails(result.toList[l].receiverId,result.toList[l],senderPr,maxSlot,message,true,'Deleted');
                        }
                    }
                    else{
                        sendMeetingDeleteCancelEmails(result.to.receiverId,result.to,senderPr,maxSlot,message,true,'Deleted');
                    }

                    if(result.senderId != userId){
                        sendMeetingDeleteCancelEmails(result.senderId,null,senderPr,maxSlot,message,true,'Deleted');
                    }
                }
                callback(true);
            });
        }else
            callback(false,'DELETE_MEETING_FAILED');
    })
}

function deleteIcsFileFromS3(awsKey){
    var s3bucket = new AWS.S3({ params: {Bucket: awsData.icsBucket} });
    var params = {
        Key: awsKey /* required */
    };
    s3bucket.deleteObject(params, function(err, data) {

    });
}

function sendMeetingDeleteCancelEmails(userId,receiverPr,senderPr,maxSlot,message,isUser,action){
    if(isUser){
        userManagements.findUserProfileByIdWithCustomFields(userId,{firstName:1,lastName:1,emailId:1,timezone:1},function(error,userDetails){
            var timezone;
            var dataToSend = {};

            if(common.checkRequired(userDetails)){
                if(common.checkRequired(userDetails.timezone) && common.checkRequired(userDetails.timezone.name)){
                    timezone = userDetails.timezone.name;
                }
                else if(common.checkRequired(senderPr.timezone) && common.checkRequired(senderPr.timezone.name)){
                    timezone = senderPr.timezone.name;
                }

                dataToSend.emailId = userDetails.emailId;
                dataToSend.receiverName = userDetails.firstName+' '+userDetails.lastName;
            }
            else if(receiverPr){
                dataToSend.emailId = receiverPr.receiverEmailId;
                if(common.checkRequired(receiverPr.receiverName)){
                    dataToSend.receiverName = receiverPr.receiverName;
                }
                if(common.checkRequired(receiverPr.receiverFirstName)){
                    dataToSend.receiverName = receiverPr.receiverFirstName+" "+receiverPr.receiverLastName || "";
                }
            }

            if(!common.checkRequired(timezone)){
                if(common.checkRequired(senderPr.timezone) && common.checkRequired(senderPr.timezone.name)){
                    timezone = senderPr.timezone.name;
                }
                else timezone = 'UTC';
            }
            
            dataToSend.subject = ' Relatas Meeting '+action+' : '+maxSlot.title;
            dataToSend.senderName = senderPr.firstName+' '+senderPr.lastName;
            var cName = senderPr.companyName || '';
            var des = senderPr.designation || '';
            var comma = (common.checkRequired(cName) && common.checkRequired(des)) ? ', ' : ' ';
            dataToSend.senderInfo = des+comma+cName;
            dataToSend.senderLocation = senderPr.location;
            dataToSend.meetingTitle = maxSlot.title;
            dataToSend.meetingAgenda = maxSlot.description;

            var start = moment(maxSlot.start.date).tz(timezone || 'UTC');
            var date = start.format("DD-MMM-YYYY");
            var time = start.format("h:mm a z");
            dataToSend.mDate = date;
            dataToSend.mTime = time;
            dataToSend.mLocation = maxSlot.locationType+' : '+maxSlot.location;
            dataToSend.mMessage = common.checkRequired(message) ? message : 'No message';

            emailSenders.sendMeetingCancelOrDeletionMail_new(dataToSend,action);
        })
    }
}

function getCommonConnectionsAndLastInteractionDetails(data,callback){
    var obj = {};
    var now = moment().tz(data.timezone || 'UTC').format()
    if(common.checkRequired(data.senderId) && common.checkRequired(data.receiverId)){
        data.interactionUrl = domainName+'/today/details/'+data.senderId;
        interaction.totalInteractionsWithOneUserCountAndLastInteracted(data.senderId.toString(),data.receiverEmailId,common.castToObjectId,now,function(interactionsCount){

            if(common.checkRequired(interactionsCount) && interactionsCount.length > 0 && common.checkRequired(interactionsCount[0])){

                obj.lastInteractedOn = interactionsCount[0].lastInteracted;
                obj.totalInteractions = interactionsCount[0].count || 0;
               //obj.commonConnections = 'Not Found';

                 getCommonContactsWithUI(data.senderId,data.receiverId,function(cC,exist,count){
                     obj.commonConnections = exist ? cC : 'Not Found';
                     obj.totalCommonConnections =count;
                    callback(obj)
                 });
            }
            else{
                obj.lastInteractedOn = "none";
                obj.totalInteractions = 0;
               //obj.commonConnections = 'Not Found';

                getCommonContactsWithUI(data.senderId,data.receiverId,function(cC,exist,count){
                    obj.commonConnections = exist ? cC : 'Not Found';
                    obj.totalCommonConnections =count;
                    callback(obj)
                });
            }
        });
    }
    else{
        obj.lastInteractedOn = "none";
        obj.totalInteractions = 0;
        obj.commonConnections = 'Not Found';
        callback(obj)
    }
}

function getCommonContactsWithUI(userId,cUserId,callback){
    contactObj.getCommonContacts(userId,cUserId,false,function(connections){
        if(connections.length > 0) {

            var comConnectionsLength = connections.length >= 3 ? 3 : connections.length;
            var commonConnectionTable = '<table>';
            var commonConnectionImages ='<tr>';
            var commonConnectionNames = '<tr>';
            for(var i=0; i<comConnectionsLength; i++){

                var linkedinUrl = common.checkRequired(connections[i].personId) ? common.getValidUniqueUrl(connections[i].personId.publicProfileUrl) : '#';
                var title = common.checkRequired(connections[i].personId) ? connections[i].personId.firstName : connections[i].personName || '' ;
                title = common.checkRequired(title) ? title : '';
                var iUrl = common.checkRequired(connections[i].personId) ? domainName+'/getImage/'+connections[i].personId._id : 'http://relatas.com/images/default.png';

                commonConnectionImages += '<td>';
                commonConnectionImages += '<div>';
                commonConnectionImages += '<img style="border-radius: 55%;width:30px" src='+iUrl+' title='+title.replace(/\s/g, '&nbsp;')+'><br></div>';
                commonConnectionImages += '</td>';
                commonConnectionNames += '<td><span title='+title.replace(/\s/g, '&nbsp;')+'>'+getTextLength(title,5)+'</span></td>';
            }

            commonConnectionImages += '</tr>';
            commonConnectionNames += '</tr>';
            commonConnectionTable += commonConnectionImages+commonConnectionNames+'</table>';
            callback(commonConnectionTable,comConnectionsLength > 0,connections.length);
        }
        else{
            callback('',false);
        }
    });
}

Meeting.prototype.storeAcceptMeetingInteractions = function(interactionSender,interactionReceiver,addToSender){
    storeAcceptMeetingInteractions(interactionSender,interactionReceiver,addToSender)
};

function storeAcceptMeetingInteractions(interactionSender,interactionReceiver,addToSender){

    userManagements.findUserProfileByIdWithCustomFields(interactionSender.userId,{emailId:1,firstName:1,lastName:1,companyName:1,designation:1,mobileNumber:1,publicProfileUrl:1,profilePicUrl:1,skypeId:1,location:1,twitter:1},function(error,user){
        var feed1 = interactionSender;
        var feed2 = interactionReceiver;
        if(user){
            feed1 = common.getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,interactionSender.action,interactionSender.type,interactionSender.subType,user.emailId,interactionSender.refId,'relatas',interactionSender.title,interactionSender.description,interactionSender.interactionDate,interactionSender.endDate,interactionSender.trackId,interactionSender.trackInfo,interactionSender.emailContentId,interactionSender.googleAccountEmailId);
        }
        else feed1 = common.getInteractionObject(interactionSender.userId,null,null,interactionSender.firstName,interactionSender.lastName,interactionSender.publicProfileUrl,interactionSender.profilePicUrl,interactionSender.companyName,interactionSender.designation,interactionSender.location,interactionSender.mobileNumber,interactionSender.action,interactionSender.type,interactionSender.subType,interactionSender.emailId,interactionSender.refId,'relatas',interactionSender.title,interactionSender.description,interactionSender.interactionDate,interactionSender.endDate,interactionSender.trackId,interactionSender.trackInfo,interactionSender.emailContentId,interactionSender.googleAccountEmailId);

        userManagements.findUserProfileByIdWithCustomFields(interactionReceiver.userId,{emailId:1,firstName:1,lastName:1,companyName:1,designation:1,mobileNumber:1,publicProfileUrl:1,profilePicUrl:1,skypeId:1,location:1,twitter:1},function(error,user2){
            if(user2){
                feed2 = common.getInteractionObject(user2._id,null,null,user2.firstName,user2.lastName,user2.publicProfileUrl,user2.profilePicUrl,user2.companyName,user2.designation,user2.location,user2.mobileNumber,interactionReceiver.action,interactionReceiver.type,interactionReceiver.subType,user2.emailId,interactionReceiver.refId,'relatas',interactionReceiver.title,interactionReceiver.description,interactionReceiver.interactionDate,interactionReceiver.endDate,interactionReceiver.trackId,interactionReceiver.trackInfo,interactionReceiver.emailContentId,interactionReceiver.googleAccountEmailId);
            }
            else feed2 = common.getInteractionObject(interactionReceiver.userId,null,null,interactionReceiver.firstName,interactionReceiver.lastName,interactionReceiver.publicProfileUrl,interactionReceiver.profilePicUrl,interactionReceiver.companyName,interactionReceiver.designation,interactionReceiver.location,interactionReceiver.mobileNumber,interactionReceiver.action,interactionReceiver.type,interactionReceiver.subType,interactionReceiver.emailId,interactionReceiver.refId,'relatas',interactionReceiver.title,interactionReceiver.description,interactionReceiver.interactionDate,interactionReceiver.endDate,interactionReceiver.trackId,interactionReceiver.trackInfo,interactionReceiver.emailContentId,interactionReceiver.googleAccountEmailId);

            feed2.toCcBcc = '';
            feed2.ccList = interactionSender.ccList?interactionSender.ccList:null;
            feed2.toList = interactionSender.toList?interactionSender.toList:null;

            feed1.toCcBcc = interactionSender.toCcBcc?interactionSender.toCcBcc:"";
            feed1.ccList = interactionSender.ccList?interactionSender.ccList:null;
            feed1.toList = interactionSender.toList?interactionSender.toList:null;

            var intArr = [feed1,feed2];
            if(addToSender == false){
                intArr = [feed2];
            }
            addInteractionsMeetingNext(intArr,interactionSender,interactionReceiver);
        })
    })
}

function addInteractionsMeetingNext(intArr,interactionSender,interactionReceiver){

    if(common.checkRequired(interactionSender.userId) && common.checkRequired(interactionReceiver.userId) && interactionSender.userId.toString() == interactionReceiver.userId.toString()){
        var rIn = [];
        for(var i=0; i<intArr.length; i++){
            if(intArr[i].action == 'sender'){
                if(rIn.length == 0){
                    rIn.push(intArr[i])
                }
            }
        }

        interaction.addInteractionsMeeting(common.castToObjectId(interactionSender.userId),rIn,function(isSuccess){
            if(!isSuccess){
                logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting self ',interactionSender.emailId,interactionSender.refId);
            }
        })
    }
    else{
        // if(common.checkRequired(interactionSender.userId)){
        //     interaction.addInteractionsMeeting(common.castToObjectId(interactionSender.userId),intArr,function(isSuccess){
        //         if(!isSuccess){
        //             logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting 1 ',interactionSender.emailId,interactionSender.refId);
        //         }
        //     })
        // }
        // if(common.checkRequired(interactionReceiver.userId)){
        //     interaction.addInteractionsMeeting(common.castToObjectId(interactionReceiver.userId),intArr,function(isSuccess){
        //         if(!isSuccess){
        //             logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting 2 ',interactionReceiver.emailId,interactionReceiver.refId);
        //         }
        //     })
        // }

        if(common.checkRequired(interactionSender.userId)){
            interaction.addInteractionsMeeting(common.castToObjectId(interactionSender.userId),intArr,function(isSuccess){
                if(!isSuccess){
                    logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting 1 ',interactionSender.emailId,interactionSender.refId);
                }
                else if(isSuccess && common.checkRequired(interactionReceiver.userId)){
                    interaction.addInteractionsMeeting(common.castToObjectId(interactionReceiver.userId),intArr,function(isSuccess){
                        if(!isSuccess){
                            logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting 2 ',interactionReceiver.emailId,interactionReceiver.refId);
                        }
                    })
                }
            })
        }
        
    }
}

/* NEW MEETING INTERACTIONS */

Meeting.prototype.storeCreateMeetingInteractions_ = function(meeting,suggestedUserId,isSuggested){
    storeCreateMeetingInteractions(meeting,suggestedUserId,isSuggested);
};

Meeting.prototype.createNewGoogleOrOutlookMeetingAndUpdateId = function(profile, invitation, eventDetails, googleOrOutlook, outlookRefreshToken, callback){
    createNewGoogleOrOutlookMeeting(profile, invitation, eventDetails, googleOrOutlook, outlookRefreshToken, callback)
}

function storeCreateMeetingInteractions(meeting,suggestedUserId,isSuggested){
    var allMeetingInteractions = [];
    var date = common.checkRequired(meeting.scheduledDate) ? new Date(meeting.scheduledDate) : new Date();
    var trackInfo = {action:"request"};
    if(isSuggested){
        trackInfo = {action:"re scheduled"};
        date = meeting.suggestedBy.suggestedDate ? new Date(meeting.suggestedBy.suggestedDate) : new Date()
    }
    var senderI = common.getInteractionObject(meeting.senderId,null,null,meeting.senderName,null,null,meeting.senderPicUrl,null,null,null,null,"sender","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.senderEmailId,meeting.invitationId,'relatas',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null);
    allMeetingInteractions.push(senderI);
    var emailIdList = [meeting.senderEmailId];
    if(meeting.selfCalendar && meeting.toList && meeting.toList.length>0){
        for(var i=0; i<meeting.toList.length; i++){
            emailIdList.push(meeting.toList[i].receiverEmailId);
            allMeetingInteractions.push(common.getInteractionObject(meeting.toList[i].receiverId,null,null,meeting.toList[i].receiverFirstName,meeting.toList[i].receiverLastName,null,null,null,null,null,null,"receiver","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.toList[i].receiverEmailId,meeting.invitationId,'relatas',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null));
        }
    }
    else if(meeting.to && common.checkRequired(meeting.to.receiverEmailId)){
        emailIdList.push(meeting.to.receiverEmailId);
        allMeetingInteractions.push(common.getInteractionObject(meeting.to.receiverId,null,null,meeting.to.receiverName,null,null,null,null,null,null,null,"receiver","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.to.receiverEmailId,meeting.invitationId,'relatas',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null));
    }

    userManagements.selectUserProfilesCustomQuery({emailId:{$in:emailIdList}},function(error,users){
        
        if(!error && users && users.length > 0){
            for(var inter=0; inter<allMeetingInteractions.length; inter++){
                for(var user=0; user<users.length; user++){
                    if (allMeetingInteractions[inter].emailId == users[user].emailId) {
                        allMeetingInteractions[inter].userId = users[user]._id.toString();
                        allMeetingInteractions[inter].firstName = users[user].firstName;
                        allMeetingInteractions[inter].lastName = users[user].lastName;
                        allMeetingInteractions[inter].publicProfileUrl = users[user].publicProfileUrl;
                        allMeetingInteractions[inter].profilePicUrl = users[user].profilePicUrl;
                        allMeetingInteractions[inter].companyName = users[user].companyName;
                        allMeetingInteractions[inter].designation = users[user].designation;
                        allMeetingInteractions[inter].location = users[user].location;
                        allMeetingInteractions[inter].mobileNumber = users[user].mobileNumber;
                        allMeetingInteractions[inter].emailId = users[user].emailId;
                    }
                }
            }
        }
        if(allMeetingInteractions.length > 0){
            var senderUser = {_id:null,emailId:meeting.senderEmailId,forMeeting:true};
            if(common.checkRequired(meeting.senderId)){
                senderUser = {_id:common.castToObjectId(meeting.senderId),emailId:meeting.senderEmailId,forMeeting:true};
            }

            if(isSuggested && common.checkRequired(suggestedUserId)){
                for(var s=0; s<allMeetingInteractions.length; s++){
                    if(allMeetingInteractions[s].userId == suggestedUserId){
                        allMeetingInteractions[s].action = 'sender';
                        senderI = allMeetingInteractions[s];
                        senderUser = {_id:common.castToObjectId(suggestedUserId),emailId:allMeetingInteractions[s].emailId,forMeeting:true};
                    }
                    else allMeetingInteractions[s].action = 'receiver';
                }
            }
            
            // var addInteractionsForUsers = _.compact(_.pluck(allMeetingInteractions,"userId"));
            var allUsers = _.compact(_.pluck(allMeetingInteractions,"userId"));
            
            userManagements.selectUserProfilesCustomQuery({_id:{$in:common.castListToObjectIds(allUsers)}},{serviceLogin:1,emailId:1},function(err,userSource){
                if(!err && userSource.length > 0) {
                    var service = null;
                    userSource.forEach(function (a) {
                        if (meeting.senderId == a._id) {
                            service = a.serviceLogin;
                        }
                    });
                    
                    var finalUserIds = [];
                    userSource.forEach(function (b) {
                        if (b.serviceLogin != 'outlook' && b.serviceLogin == service) {
                            finalUserIds.push(b._id.toString())
                        }
                    });

                    if(service == 'outlook')
                        finalUserIds.push(meeting.senderId.toString())
                    
                    var addInteractionsForUsers = finalUserIds;
                }
                else{
                    var addInteractionsForUsers = [];
                    addInteractionsForUsers.push(meeting.senderId.toString());  
                }
                
                addInteractionsForUsers.forEach(function(userId) {
                    interaction.updateMeetingsAsInteractions(common.castToObjectId(userId),allMeetingInteractions,function(result){

                        if(result){
                            logger.info("Meeting Interactions Successfully updated for - ",userId)
                        } else {
                            logger.info("Meeting Interactions Not updated for - ",userId)
                        }
                    });
                });
            });

            //var senderUser = {_id:common.castToObjectId(meeting.senderId),emailId:meeting.senderEmailId,forMeeting:true};
            // if(common.checkRequired(senderUser._id)){
            //     interaction.addInteractionsBulk(senderUser,allMeetingInteractions,[],function(){});
            // }

            // createInteractionsReceivers(allMeetingInteractions,senderI);
        }
    });
}

function createInteractionsReceivers(allMeetingInteractions,senderI){
    for(var i=0; i<allMeetingInteractions.length; i++){
        if(allMeetingInteractions[i].action == 'receiver' && common.checkRequired(allMeetingInteractions[i].userId)){
            var user = {_id:common.castToObjectId(allMeetingInteractions[i].userId),emailId:allMeetingInteractions[i].receiverEmailId,forMeeting:true}
            interaction.addInteractionsBulk(user,[allMeetingInteractions[i],senderI],[],function(){});
        }
    }
}

/* Accept meeting interactions */
function updateMeetingInteractions(senderId,senderEmailId,receiverId,receiverEmailId,startDate,endDate,refId){
    interaction.updateInteractionMeetingAccepted(refId,senderId,senderEmailId,receiverId,receiverEmailId,startDate,endDate,new Date(),false,function(isSuccess){

    });
}

function createNewGoogleOrOutlookMeeting(profile, invitation, eventDetails, googleOrOutlook, outlookRefreshToken, callback){
    /*
    * this function called when the non relatas user sets up meeting with relatas user
    * to update the relatas user's calendar
    * */
    if(googleOrOutlook == 'google'){
        var id = simple_unique_id.generate('googleRelatas');
        var gId = 'relat'+common.getNumString(0)+id;
        var attendees = [];
        if(eventDetails.type == 'reschedule'){
            invitation.participants.forEach(function(a){
                if(a.emailId == profile.emailId){
                    var obj = {
                        email:profile.emailId,
                        displayName:profile.firstName+' '+profile.lastName,
                        responseStatus: 'accepted'
                    }
                }else{
                    var obj = {
                        email:a.emailId,
                        displayName:a.emailId
                    }
                }
                attendees.push(obj)

            })
        }
        else{
            attendees.push({
                email:eventDetails.senderEmailId,
                displayName:eventDetails.senderName,
                responseStatus: 'accepted'
            });

            var rec = {
                email : eventDetails.receiverEmailId,
                displayName : eventDetails.receiverName,
                responseStatus: 'accepted'
            };

            attendees.push(rec);
        }

        var request_body = {
            "id": gId,
            "summary": eventDetails.title,
            "end": {
                "dateTime": new Date(eventDetails.end)
            },
            "start": {
                "dateTime": new Date(eventDetails.start)
            },
            "description": eventDetails.description,
            "location": eventDetails.location,
            "locationType": eventDetails.locationType,
            "locationDetails": '',
            "relatasEvent": true,
            "attendees": attendees
        };

        common.getNewGoogleToken(profile.google[0].token,profile.google[0].refreshToken,function (newAccessToken) {
            if (newAccessToken) {
                var google_calendar = new gcal.GoogleCalendar(newAccessToken);
                google_calendar.events.insert('primary', request_body,{sendNotifications:true}, function (err, result) {
                    if(err){
                        callback(false,'CREATE_GOOGLE_EVENT_FAILED')
                    }
                    else {
                        var googleEventId = result.id;

                        meetingManagementObj.updateGoogleEventId(googleEventId, invitation.invitationId, function(result){
                            if(result){
                                callback(true,null)
                            }
                            else{
                                callback(false,'UPDATE_GOOGLE_EVENT_ID_FAILED')
                            }
                        });

                    }
                });
            } else {
                callback(false,'CREATE_GOOGLE_EVENT_FAILED');
            }
        });
    }
    else if(googleOrOutlook == 'outlook'){
        var attendees = [];

        if(eventDetails.type == 'reschedule'){
            invitation.participants.forEach(function(a){
                if(a.emailId !== profile.emailId){
                    var obj = {
                        "EmailAddress": {
                            "Address": a.emailId,
                            "Name": a.emailId
                        },
                        "Type": "Required"
                    }
                    attendees.push(obj)
                }
            })
        }
        else {
            attendees.push({
                "EmailAddress": {
                    "Address": eventDetails.senderEmailId,
                    "Name": eventDetails.senderName
                },
                "Type": "Required"
            })
        }

        var meeting = {
            "Subject": eventDetails.title,
            "Body": {
                "ContentType": "Text",
                "Content": eventDetails.description
            },
            "Start": {
                "DateTime": new Date(eventDetails.start),
                "TimeZone": profile.timezone && profile.timezone.name ? profile.timezone.name:'UTC'
            },
            "End": {
                "DateTime": new Date(eventDetails.end),
                "TimeZone": profile.timezone && profile.timezone.name ? profile.timezone.name:'UTC'
            },
            "Attendees": attendees,
            "Location":{
                "DisplayName":eventDetails.location
            }
        };

        officeOutlook.getNewAccessTokenWithoutStore(outlookRefreshToken,function (newToken) {
            officeOutlook.writeMeetingOnCalendarMSGraphAPI(newToken.access_token,meeting,function(error,result){
                if(error){
                    callback(false,'GET_OUTLOOK_ACCESS_TOKEN_FAILED')
                }
                else {
                    var outlookMeetingObj = JSON.parse(result);
                    if(outlookMeetingObj.id) {
                        meetingManagementObj.updateInvitationId(outlookMeetingObj.id, invitation.invitationId, function(result){
                            if(result){
                                callback(true, null,outlookMeetingObj.id);
                            }else{
                                callback(false,'UPDATE_INVITATION_ID_FAILED')
                            }
                        })
                    }else{
                        callback(false,'UPDATE_OUTLOOK_EVENT_FAILED')
                    }
                }
            });
        });
    }
    else{
        callback(false,'INVALID_SERVICE')
    }


}

function getTextLength(text,maxLength){
    if(!common.checkRequired(text))
        return '';

    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

module.exports = Meeting;
