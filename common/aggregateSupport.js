// New Aggregate Support -- Update By Rajiv - 18032016. 
var commonUtility = require('../common/commonUtility');
var userManagement = require('../dataAccess/userManagementDataAccess');
var contactsManagementClass = require('../dataAccess/contactsManagementClass');
var interactions = require('../dataAccess/interactionManagement');
var winstonLog = require('../common/winstonLog');

var aggSuppObj = new AggregateSupport();
var interactionObj = new interactions();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactsManagementClassObj = new contactsManagementClass();

var interactionCollection = require('../databaseSchema/userManagementSchema').interactions;
var InteractionsV2Collection = require('../databaseSchema/interactionSchema').interactionsV2
var myUserCollection = require('../databaseSchema/userManagementSchema').User;

var logLib = new winstonLog();
var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();
var _ = require('lodash');

function AggregateSupport(){

}

//Aggregate Support prototype. Changes Made
AggregateSupport.prototype.parseAndStoreMobileHistory = function(history,user,type,callback){

    if(common.checkRequired(history)&& history.length > 0){
        contactsManagementClassObj.getAllDntCotnacts(common.castToObjectId(user._id), function(dntContacts) {

            var allPhoneNumbers = [];

            history = history.filter( function( el ) {
                if(el.mobileNumber){
                    allPhoneNumbers.push(el.mobileNumber)
                };
                return ! (_.includes(dntContacts.mobileNumbers, el.mobileNumber ) || _.includes(dntContacts.personEmailIds, el.emailId));
            });

            allPhoneNumbers = _.uniq(allPhoneNumbers);

            if(history.length > 0) {

                getEmailIdsForMobileNumbers(common.castToObjectId(user._id),allPhoneNumbers,function (mobEmailObj) {

                    if(type == 'contacts'){
                        makeContacts(user,history,callback);
                    }
                    else if(type == 'call' || type == 'sms'){
                        history = common.removeDuplicates_field(history,'id');
                        makeInteractionByType(history,user,type,mobEmailObj,function(status){
                            callback(status);
                        })
                    }
                });

            } else callback(false)
        })
        
    }else callback(false)
};

function getEmailIdsForMobileNumbers(userId,mobNums,callback) {

    myUserCollection.aggregate([
            {
                $match:{
                    _id:userId
                }},
            {"$unwind":"$contacts"},
            {$match:{"contacts.mobileNumber":{$in:mobNums}}},
            {$group:{_id:null,
                    "contacts":{$push:"$contacts"}
                }}]
        ,function(err,result){

        var mobObj = null;

        if(!err && result && result.length>0){
            mobObj = {};
            _.each(result[0].contacts,function (co) {
                mobObj[co.mobileNumber] = co.personEmailId
            })
        }

        callback(mobObj)
        });
}

//check Profile for ref Id, no matter what now, the profile will not be updated
function checkRefIdInProfile(userEmail,user, dataObject, dataTempObject, callBack){
    interactionCollection.find({ownerEmailId:userEmail,emailId:userEmail, refId:dataObject.id},{refId:1,_id:1},function(err,res){
        var length = res.length;
        if (!length> 0) {
            //interactions with same refId are not present.
                callBack(false, dataTempObject)
            }
        else{
            callBack(true,null)
        }
    })
}

//get Profile of the Other Person

function getInteractedUsersProfile(interactedUserMobileNumber,callBack){
    myUserCollection.find({$or:[{mobileNumber:{$eq:interactedUserMobileNumber}},{mobileNumberWithoutCC:{$eq:interactedUserMobileNumber}}]},{firstName: 1,
        "lastName": 1,
        "emailId": 1,
        publicProfileUrl: 1,
        profilePicUrl: 1,
        companyName: 1,
        designation: 1,
        location: 1,
        mobileNumber: 1,
        mobileNumberWithoutCC:1,
        isMobileUser: 1
    }, function(err,userData) {
        if(userData.length!=1){
            if(interactedUserMobileNumber.charAt(0)=='0'){
                var num = interactedUserMobileNumber.substring(1)
                getInteractedUsersProfile(num,function(othProf){
                  callBack(othProf)
                })
            }else{
                callBack(userData)
            }
        }else{
            callBack(userData)
        }
    })
}

function getContactInfo(user,interactedUserNumber,interactedDate,callBack){
    var regEx = new RegExp('^\\d{0,5}'+interactedUserNumber+'$');
    //var num = new RegExp('^'+ interadUserNumber+'{7,}$','i');

    myUserCollection.aggregate([
        {$match:{emailId:user.emailId}},
        {"$unwind":"$contacts"},
        //{$match:{"contacts.mobileNumber":{$regex: /\d{0,7}92021$/}}},
        {$match:{"contacts.mobileNumber":new RegExp('^\\d{0,5}'+interactedUserNumber+'$')}},
        {$group:{_id:null,
            "contacts":{$push:"$contacts"}
        }}]
        ,function(err,profile){


        if(err){
            // console.log(err)
        }else{
            updateLastInteractedDate(user,profile,interactedDate)
        }

    })
}

function updateLastInteractedDate(user,interactedContact,interactionDate){
    if(interactedContact.length>0){
        if(interactedContact.length==1){
            //only if one profile is found, update interacted date.
            if(interactedContact[0].contacts[0].lastInteracted==null || interactedContact[0].contacts[0].lastInteracted == "undefined"){
                myUserCollection.update({emailId:user.emailId,"contacts._id":interactedContact[0].contacts[0]._id},
                    {$set:{"contacts.$.lastInteracted":new Date(interactionDate)}}
                    ,function(err,res){
                        if(err){
                            // console.log("could not update contact last interacted"+err)
                        }
                    }
                )
            }else{
                var lastIntDate = new Date(interactedContact[0].contacts[0].lastInteracted)
                var intDate = new Date(interactionDate)
                if(intDate>lastIntDate){
                    myUserCollection.update({emailId:user.emailId,"contacts._id":interactedContact[0].contacts[0]._id},
                        {$set:{"contacts.$.lastInteracted":intDate}}
                    ,function(err,res){
                            if(err){
                                // console.log("could not update contact last interacted"+err)
                            }
                        }
                    )
                }
            }
        }
    }
}

//for phone interactions -- new
function makeInteractionByType(reqDataList,user,operationType,mobEmailObj,callBack){
    for(var i = 0; i< reqDataList.length;i++){
        checkRefIdInProfile(user.emailId,user,reqDataList[i],reqDataList[i],function(isPresent,reqData){
            if(isPresent){
                // console.log("Mobile Interaction Exists")
            }
            else {
                //mobile Api finds profiles based on their mobile Number.
                getInteractedUsersProfile(reqData.mobileNumber,function(otherProfile){
                    if(otherProfile.length==1){
                        if(otherProfile[0].mobileNumber!="" && otherProfile[0].mobileNumber!=null){
                            reqData.mobileNumber = otherProfile[0].mobileNumber
                        }
                    }

                    reqData.emailId = mobEmailObj && mobEmailObj[reqData.mobileNumber]?mobEmailObj[reqData.mobileNumber]:null

                    if(operationType=="sms"){
                        checkWhereToUpdate(reqData, user, otherProfile, operationType,null)
                    }
                    else if(operationType == "call"){
                        checkWhereToUpdate(reqData, user, otherProfile, operationType,null)
                    }
                })
            }
        })
    }callBack(1)
}

//for updating User's Interaction Profile.
// I visited
function updateInteractionObjects(arrayBuiltObject){
    interactionCollection.collection.insert(arrayBuiltObject,function(err,rdata){
        InteractionsV2Collection.collection.insert(arrayBuiltObject,function(err,rdata){

        });

        if(err){
            throw err
        }
    })

}

// check type of sms -- incoming or outgoing
function checkWhereToUpdate(reqData, user, otherUserData, type, back){
    if(type == "sms"){
        forSMSUpdate(reqData,user,otherUserData,reqData.smsType)
    }
    else if(type == "call"){
        forCallUpdate(reqData,user,otherUserData,reqData.callType)
    }
}

//Logic Check for what profiles to be updated after SMS
function forSMSUpdate(messageData, user, otherUserData, type){

    //update Last Interacted Date for contact
    getContactInfo(user,messageData.mobileNumber,messageData.sentDate,function(contactProfile){

    })


    if(otherUserData.length>0){
        if(otherUserData[0].isMobileUser!=null){
            //for A sends B sms, where A,B = RAU , B uploads and A uploads independently.
            buildSMSObject(user,otherUserData[0],messageData, type, function(data){
                //updates 
                updateInteractionObjects(data);
                checkIfContact(user,otherUserData[0].mobileNumber,otherUserData[0].emailId,function(dataRes) {
                    if (dataRes.length == 0){
                        ContactUpdate(user, otherUserData[0], true, function (err, dataUpdate) {
                        })
                    }
                })
            })
            //createContactLoop
        }
        else{
            //for A sends sms to B, where  A is RAU and B is RU
            if(type == "outgoing"){
                buildSMSObject(user,otherUserData[0],messageData,"outgoing",function(data){
                    updateInteractionObjects(data)
                })
                buildSMSObject(otherUserData[0],user,messageData,"incoming",function(data){
                    updateInteractionObjects(data)
                })
            }
            else{
                //for B sending sms to A, where B is RU and A is RAU
                buildSMSObject(user,otherUserData[0],messageData,"incoming",function(data){
                    updateInteractionObjects(data)
                })
                buildSMSObject(otherUserData[0],user,messageData,"outgoing",function(data){
                    updateInteractionObjects(data)
                })
                //createContactLoop
            }
            //for A update B as a contact
            checkIfContact(user,otherUserData[0].mobileNumber,otherUserData[0].emailId,function(dataRes){
                if (dataRes.length == 0) {
                    ContactUpdate(user, otherUserData[0], true, function (err, dataUpdate) {
                    })
                }
            })
            //for B update A as a contact
            checkIfContact(otherUserData[0],user.mobileNumber,user.emailId,function(dataRes){
                if (dataRes.length == 0) {
                    ContactUpdate(otherUserData[0], user, true, function (err, dataUpdate) {
                    })
                }
            })
        }
    }
    else{
        // for A sms to B, where A is RAU and B is NRU or vice versa
        buildSMSObject(user,otherUserData[0],messageData, type, function(data){
            updateInteractionObjects(data);
            //no creation of Contact
        })
    }
}

// Logic Check for what profiles have to be updated.
function forCallUpdate(callData, user, otherUserData, type){

    //update Interaction Date
    getContactInfo(user,callData.mobileNumber,callData.callStartDate,function(contactProfile){

    })

    if(otherUserData.length>0){
        if(otherUserData[0].isMobileUser!= null){
            //for A calling B, where A,B = RAU
            buildCallObject(user,otherUserData[0],callData, type, function(data){
                updateInteractionObjects(data);
                //update Contact
                checkIfContact(user,otherUserData[0].mobileNumber,otherUserData[0].emailId,function(dataRes){
                    if (dataRes.length == 0) {
                        ContactUpdate(user, otherUserData[0], false, function (err, dataUpdate) {
                        })
                    }
                })
            })
        }
        else{
            //for A calling B, where  A is RAU and B is RU
            if(type == "outgoing"){
                buildCallObject(user,otherUserData[0],callData,"outgoing",function(data){
                    updateInteractionObjects(data)
                    //update Contact
                })
                buildCallObject(otherUserData[0],user,callData,"incoming",function(data){
                    updateInteractionObjects(data)
                })
            }
            else{
                buildCallObject(user,otherUserData[0],callData,"incoming",function(data){
                    updateInteractionObjects(data)
                })
                buildCallObject(otherUserData[0],user,callData,"outgoing",function(data){
                    updateInteractionObjects(data)
                })
            }
            //for A update B as a contact
            checkIfContact(user,otherUserData[0].mobileNumber,otherUserData[0].emailId,function(dataRes){
                if (dataRes.length == 0) {
                    ContactUpdate(user, otherUserData[0], false, function (err, dataUpdate) {
                    })
                }
            })
            //for B update A as a contact
            checkIfContact(otherUserData[0],user.mobileNumber,user.emailId,function(dataRes){
                if (dataRes.length == 0) {
                    ContactUpdate(otherUserData[0], user, false, function (err, dataUpdate) {
                    })
                }
            })
        }
    }
    else{
        // for A calling B, where A is RAU and B is NRU
        buildCallObject(user,otherUserData[0],callData,type,function(data){
            updateInteractionObjects(data)
            //no creation of Contact
        })
    }
}

// Build Call Object for Profile and the Other Profile being updated to
function buildCallObject(user, otherUser, reqData,type,callBack) {
    var direct = []

    if (type == "outgoing") {

        var userIsSender = {}; // outgoing -- action sender for user A

        userIsSender.userId = user._id;
        userIsSender.firstName = user.firstName;
        userIsSender.lastName = user.lastName;
        userIsSender.publicProfileUrl = user.publicProfileUrl;
        userIsSender.profilePicUrl = user.profilePicUrl;
        userIsSender.companyName = user.companyName;
        userIsSender.designation = user.designation;
        userIsSender.location = user.location;
        userIsSender.action = 'sender';
        userIsSender.createdDate = new Date();
        userIsSender.interactionDate = new Date(reqData.callStartDate);
        userIsSender.interactionType = 'call';
        userIsSender.emailId = reqData.emailId;
        userIsSender.mobileNumber = reqData.mobileNumber;
        userIsSender.refId = reqData.id;
        userIsSender.subType = 'call';
        userIsSender.source = 'mobile';
        userIsSender.ownerId = user._id
        userIsSender.ownerEmailId= user.emailId;
        userIsSender.duration= reqData.duration?reqData.duration:0
        if (reqData.callEndDate != null)
            userIsSender.endDate = new Date(reqData.callEndDate)
        direct.push(userIsSender)

        var interactionReceiver = {}; // outgoing -- action sender for otheruser A
        if(otherUser!=null) {
            interactionReceiver.userId = otherUser._id;
            interactionReceiver.firstName = otherUser.firstName;
            interactionReceiver.lastName = otherUser.lastName;
            interactionReceiver.publicProfileUrl = otherUser.publicProfileUrl;
            interactionReceiver.profilePicUrl = otherUser.profilePicUrl;
            interactionReceiver.companyName = otherUser.companyName;
            interactionReceiver.designation = otherUser.designation;
            interactionReceiver.location = otherUser.location;
            interactionReceiver.action = 'receiver';
            interactionReceiver.createdDate = new Date()
            interactionReceiver.interactionDate = new Date(reqData.callStartDate);
            interactionReceiver.interactionType = 'call';
            interactionReceiver.emailId = reqData.emailId;
            interactionReceiver.mobileNumber = reqData.mobileNumber;
            interactionReceiver.refId = reqData.id;
            interactionReceiver.subType = 'call';
            interactionReceiver.source = 'mobile';
            interactionReceiver.ownerEmailId = user.emailId
            interactionReceiver.ownerId = user._id;
            interactionReceiver.duration= reqData.duration?reqData.duration:0;
            if (reqData.callEndDate != null)
                interactionReceiver.endDate = new Date(reqData.callEndDate)
            direct.push(interactionReceiver)
        }else{
            interactionReceiver.userId = null
            interactionReceiver.action = 'receiver';
            interactionReceiver.createdDate = new Date()
            interactionReceiver.interactionDate = new Date(reqData.callStartDate);
            interactionReceiver.interactionType = 'call';
            interactionReceiver.mobileNumber = reqData.mobileNumber;
            interactionReceiver.refId = reqData.id;
            interactionReceiver.subType = 'call';
            interactionReceiver.source = 'mobile';
            if (reqData.callEndDate != null)
                interactionReceiver.endDate = new Date(reqData.callEndDate)
            interactionReceiver.ownerEmailId = reqData.emailId
            interactionReceiver.ownerId = user._id;
            interactionReceiver.duration= reqData.duration?reqData.duration:0

            direct.push(interactionReceiver)
        }
        callBack(direct)
    }
    else {
        var userIsReceiver = {}
        userIsReceiver.userId = user._id;
        userIsReceiver.firstName = user.firstName;
        userIsReceiver.lastName = user.lastName;
        userIsReceiver.publicProfileUrl = user.publicProfileUrl;
        userIsReceiver.profilePicUrl = user.profilePicUrl;
        userIsReceiver.companyName = user.companyName;
        userIsReceiver.designation = user.designation;
        userIsReceiver.location = user.location;
        userIsReceiver.action = 'receiver';
        userIsReceiver.createdDate = new Date()
        userIsReceiver.interactionDate = new Date(reqData.callStartDate);
        userIsReceiver.interactionType = 'call';
        userIsReceiver.emailId = reqData.emailId;
        userIsReceiver.mobileNumber = reqData.mobileNumber;
        userIsReceiver.refId = reqData.id;
        userIsReceiver.subType = 'call';
        userIsReceiver.source = 'mobile';
        userIsReceiver.ownerId = user._id
        userIsReceiver.duration= reqData.duration?reqData.duration:0
        userIsReceiver.ownerEmailId = user.emailId
        if (reqData.callEndDate != null)
            userIsReceiver.endDate = new Date(reqData.callEndDate)
        direct.push(userIsReceiver)

        var interactionSender = {}
        if (otherUser != null){
            interactionSender.userId = otherUser._id;
            interactionSender.firstName = otherUser.firstName;
            interactionSender.lastName = otherUser.lastName;
            interactionSender.publicProfileUrl = otherUser.publicProfileUrl;
            interactionSender.profilePicUrl = otherUser.profilePicUrl;
            interactionSender.companyName = otherUser.companyName;
            interactionSender.designation = otherUser.designation;
            interactionSender.location = otherUser.location;
            interactionSender.action = 'sender';
            interactionSender.createdDate = new Date();
            interactionSender.interactionDate = new Date(reqData.callStartDate);
            interactionSender.interactionType = 'call';
            interactionSender.emailId = reqData.emailId;
            interactionSender.mobileNumber = reqData.mobileNumber;
            interactionSender.refId = reqData.id;
            interactionSender.subType = 'call';
            interactionSender.source = 'mobile';
            interactionSender.ownerEmailId = user.emailId
            interactionSender.ownerId = user._id
            interactionSender.duration= reqData.duration?reqData.duration:0

            if (reqData.callEndDate != null)
                interactionSender.endDate = new Date(reqData.callEndDate)
            direct.push(interactionSender)
        }else{
            interactionSender.userId = null;
            interactionSender.action = 'sender';
            interactionSender.createdDate = new Date();
            interactionSender.interactionDate = new Date(reqData.callStartDate);
            interactionSender.interactionType = 'call';
            interactionSender.emailId = reqData.emailId
            interactionSender.mobileNumber = reqData.mobileNumber;
            interactionSender.refId = reqData.id;
            interactionSender.subType = 'call';
            interactionSender.source = 'mobile';
            if (reqData.callEndDate != null)
                interactionSender.endDate = new Date(reqData.callEndDate)
            interactionSender.ownerEmailId = user.emailId
            interactionSender.ownerId = user._id
            interactionSender.duration= reqData.duration?reqData.duration:0

            direct.push(interactionSender)
        }
        callBack(direct)

    }
}

//Build SMS object for Profile and the Other Profile being updated to.
function buildSMSObject(user, otherUser, reqData,type,callBack){
    var direct = []

    if(/*reqData.smsT*/type == "outgoing"){
        var userIsSender = {}; // outgoing -- action sender for user A
        userIsSender.userId = user._id;
        userIsSender.firstName = user.firstName;
        userIsSender.lastName = user.lastName;
        userIsSender.publicProfileUrl = user.publicProfileUrl;
        userIsSender.profilePicUrl = user.profilePicUrl;
        userIsSender.companyName = user.companyName;
        userIsSender.designation = user.designation;
        userIsSender.location = user.location;
        userIsSender.action = 'sender';
        userIsSender.createdDate = new Date();
        userIsSender.interactionDate = new Date(reqData.sentDate);
        userIsSender.interactionType = 'sms';
        userIsSender.emailId = user.emailId;
        userIsSender.mobileNumber = reqData.mobileNumber;
        userIsSender.refId = reqData.id;
        userIsSender.subType = 'sms';
        userIsSender.source = 'mobile';
        userIsSender.title = reqData.text;
        userIsSender.ownerId = user._id
        userIsSender.ownerEmailId = user.emailId
        direct.push(userIsSender)

        var interactionReceiver = {};
        if(otherUser!=null) {
            // outgoing -- action sender for otheruser A
            interactionReceiver.userId = otherUser._id;
            interactionReceiver.firstName = otherUser.firstName;
            interactionReceiver.lastName = otherUser.lastName;
            interactionReceiver.publicProfileUrl = otherUser.publicProfileUrl;
            interactionReceiver.profilePicUrl = otherUser.profilePicUrl;
            interactionReceiver.companyName = otherUser.companyName;
            interactionReceiver.designation = otherUser.designation;
            interactionReceiver.location = otherUser.location;
            interactionReceiver.action = 'receiver';
            interactionReceiver.createdDate = new Date();
            interactionReceiver.interactionDate = new Date(reqData.sentDate);
            interactionReceiver.interactionType = 'sms';
            interactionReceiver.emailId = otherUser.emailId;
            interactionReceiver.mobileNumber = reqData.mobileNumber;
            interactionReceiver.refId = reqData.id;
            interactionReceiver.subType = 'sms';
            interactionReceiver.source = 'mobile';
            interactionReceiver.title = reqData.text;
            interactionReceiver.ownerId = user._id
            interactionReceiver.ownerEmailId= user.emailId

            direct.push(interactionReceiver)
        }else{
            interactionReceiver.userId = null;
            interactionReceiver.action = 'receiver';
            interactionReceiver.createdDate = new Date();
            interactionReceiver.interactionDate = new Date(reqData.sentDate);
            interactionReceiver.interactionType = 'sms';
            interactionReceiver.mobileNumber = reqData.mobileNumber;
            interactionReceiver.refId = reqData.id;
            interactionReceiver.subType = 'sms';
            interactionReceiver.source = 'mobile';
            interactionReceiver.title = reqData.text;
            interactionReceiver.ownerId = user._id
            interactionReceiver.ownerEmailId= user.emailId
            direct.push(interactionReceiver)
        }
        callBack(direct)
    }
    else{
        var userIsReceiver = {}
        userIsReceiver.userId = user._id;
        userIsReceiver.firstName = user.firstName;
        userIsReceiver.lastName = user.lastName;
        userIsReceiver.publicProfileUrl = user.publicProfileUrl;
        userIsReceiver.profilePicUrl = user.profilePicUrl;
        userIsReceiver.companyName = user.companyName;
        userIsReceiver.designation = user.designation;
        userIsReceiver.location = user.location;
        userIsReceiver.action = 'receiver';
        userIsReceiver.createdDate = new Date()
        userIsReceiver.interactionDate = new Date(reqData.sentDate);
        userIsReceiver.interactionType = 'sms';
        userIsReceiver.emailId = user.emailId;
        userIsReceiver.mobileNumber = reqData.mobileNumber;
        userIsReceiver.refId = reqData.id;
        userIsReceiver.subType = 'sms';
        userIsReceiver.source = 'mobile';
        userIsReceiver.title = reqData.text;
        userIsReceiver.ownerId = user._id
        userIsReceiver.ownerEmailId = user.emailId
        direct.push(userIsReceiver)

        var interactionSender = {}
        if(otherUser!=null) {
            interactionSender.userId = otherUser._id;
            interactionSender.firstName = otherUser.firstName;
            interactionSender.lastName = otherUser.lastName;
            interactionSender.publicProfileUrl = otherUser.publicProfileUrl;
            interactionSender.profilePicUrl = otherUser.profilePicUrl;
            interactionSender.companyName = otherUser.companyName;
            interactionSender.designation = otherUser.designation;
            interactionSender.location = otherUser.location;
            interactionSender.action = 'sender';
            interactionSender.createdDate = new Date();
            interactionSender.interactionDate = new Date(reqData.sentDate);
            interactionSender.interactionType = 'sms';
            interactionSender.emailId = otherUser.emailId;
            interactionSender.mobileNumber = reqData.mobileNumber;
            interactionSender.refId = reqData.id;
            interactionSender.subType = 'sms';
            interactionSender.source = 'mobile';
            interactionSender.title = reqData.text;
            interactionSender.ownerId = user._id
            interactionSender.ownerEmailId = user.emailId
            direct.push(interactionSender)
        }
        else{
            interactionSender.userId = null;
            interactionSender.action = 'sender';
            interactionSender.createdDate = new Date()
            interactionSender.interactionDate = new Date(reqData.sentDate);
            interactionSender.interactionType = 'sms';
            interactionSender.mobileNumber = reqData.mobileNumber;
            interactionSender.refId = reqData.id;
            interactionSender.subType = 'sms';
            interactionSender.source = 'mobile';
            interactionSender.title = reqData.text;
            interactionSender.ownerId = user._id
            interactionSender.ownerEmailId = user.emailId
            direct.push(interactionSender)
        }
        callBack(direct)

    }
}

// EMAIL HISTORY TO INTERACTIONS
function makeInteractionEmails(emailList,user,callback){
    var direct = [];
    var other = [];
    var emailIds = [];
    for(var i=0; i<emailList.length; i++){
        var exist = false;
        if(common.checkRequired(emailList[i].from) && emailList[i].from.emailId == user.emailId){
            exist = true;
            var interactionSender = {};
            interactionSender.userId = user._id;
            interactionSender.firstName = user.firstName;
            interactionSender.lastName = user.lastName;
            interactionSender.publicProfileUrl = user.publicProfileUrl;
            interactionSender.profilePicUrl = user.profilePicUrl;
            interactionSender.companyName = user.companyName;
            interactionSender.designation = user.designation;
            interactionSender.location = user.location;
            interactionSender.action = 'sender';
            interactionSender.interactionDate = new Date(emailList[i].emailedDate);
            interactionSender.type = 'email';
            interactionSender.emailId = user.emailId;
            interactionSender.mobileNumber = user.mobileNumber;
            interactionSender.refId = emailList[i].id;
            interactionSender.subType = 'email';
            interactionSender.source = 'mobile';
            interactionSender.title = emailList[i].text;
            direct.push(interactionSender);
        }
        else if(common.checkRequired(emailList[i].from) && common.checkRequired(emailList[i].from.emailId)){
            var interactionReceiverF = {};
            interactionReceiverF.userId = '';
            interactionReceiverF.action = 'sender';
            interactionReceiverF.interactionDate = new Date(emailList[i].emailedDate);
            interactionReceiverF.type = 'email';
            interactionReceiverF.emailId = emailList[i].from.emailId;
            interactionReceiverF.refId = emailList[i].id;
            interactionReceiverF.subType = 'email';
            interactionReceiverF.source = 'mobile';
            interactionReceiverF.title = emailList[i].text;
            other.push(interactionReceiverF);
            emailIds.push(emailList[i].from.emailId)
        }

        if(common.checkRequired(emailList[i].to) && emailList[i].to.length > 0){
            for(var j=0; j<emailList[i].to.length; j++){

                var interactionReceiver = {};
                interactionReceiver.userId = '';
                interactionReceiver.action = 'receiver';
                interactionReceiver.interactionDate = new Date(emailList[i].emailedDate);
                interactionReceiver.type = 'email';
                interactionReceiver.emailId = emailList[i].to[j].emailId;
                interactionReceiver.refId = emailList[i].id;
                interactionReceiver.subType = 'email';
                interactionReceiver.source = 'mobile';
                interactionReceiver.title = emailList[i].text;
                if(emailList[i].to[j].emailId == user.emailId){
                    if(!exist){
                        interactionReceiver.userId = user._id;
                        interactionReceiver.firstName = user.firstName;
                        interactionReceiver.lastName = user.lastName;
                        interactionReceiver.publicProfileUrl = user.publicProfileUrl;
                        interactionReceiver.profilePicUrl = user.profilePicUrl;
                        interactionReceiver.companyName = user.companyName;
                        interactionReceiver.designation = user.designation;
                        interactionReceiver.location = user.location;
                        interactionReceiver.mobileNumber = user.mobileNumber;
                        direct.push(interactionReceiver);
                    }
                }
                else{
                    emailIds.push(emailList[i].to[j].emailId)
                    other.push(interactionReceiver)
                }
            }
        }
    }

    mapUserInteractionsWithProfileEmails(user,other,common.removeDuplicate_id(emailIds,true),direct,"email-list",false);
    callback(true);
}

// CALENDAR HISTORY TO INTERACTIONS
function makeInteractionCalendar(eventList,user,callback){
    var direct = [];
    var other = [];
    var emailIds = [];
    for(var i=0; i<eventList.length; i++){
        var exist = false;
        if(common.checkRequired(eventList[i].organizer) && eventList[i].organizer.emailId == user.emailId){
            exist = true;
            var interactionSender = {};
            interactionSender.userId = user._id;
            interactionSender.firstName = user.firstName;
            interactionSender.lastName = user.lastName;
            interactionSender.publicProfileUrl = user.publicProfileUrl;
            interactionSender.profilePicUrl = user.profilePicUrl;
            interactionSender.companyName = user.companyName;
            interactionSender.designation = user.designation;
            interactionSender.location = user.location;
            interactionSender.mobileNumber = user.mobileNumber;
            interactionSender.action = 'sender';
            interactionSender.interactionDate = new Date(eventList[i].startDate);
            interactionSender.endDate = new Date(eventList[i].endDate);
            interactionSender.type = 'meeting';
            interactionSender.emailId = user.emailId;
            interactionSender.refId = eventList[i].id;
            interactionSender.subType = 'google-meeting';
            interactionSender.source = 'mobile';
            interactionSender.title = eventList[i].text;
            direct.push(interactionSender);
        }
        else if(common.checkRequired(eventList[i].organizer) && eventList[i].organizer.emailId){
            var interactionReceiverF = {};
            interactionReceiverF.userId = '';
            interactionReceiverF.action = 'sender';
            interactionReceiverF.interactionDate = new Date(eventList[i].startDate);
            interactionReceiverF.endDate = new Date(eventList[i].endDate);
            interactionReceiverF.type = 'meeting';
            interactionReceiverF.emailId = eventList[i].organizer.emailId;
            interactionReceiverF.refId = eventList[i].id;
            interactionReceiverF.subType = 'google-meeting';
            interactionReceiverF.source = 'mobile';
            interactionReceiverF.title = eventList[i].text;
            other.push(interactionReceiverF);
            emailIds.push(eventList[i].organizer.emailId)
        }

        if(common.checkRequired(eventList[i].participants) && eventList[i].participants.length > 0){
            for(var j=0; j<eventList[i].participants.length; j++){

                var interactionReceiver = {};
                interactionReceiver.userId = '';
                interactionReceiver.action = 'receiver';
                interactionReceiver.interactionDate = new Date(eventList[i].startDate);
                interactionReceiver.endDate = new Date(eventList[i].endDate);
                interactionReceiver.type = 'meeting';
                interactionReceiver.emailId = eventList[i].participants[j].emailId;
                interactionReceiver.refId = eventList[i].id;
                interactionReceiver.subType = 'google-meeting';
                interactionReceiver.source = 'mobile';
                interactionReceiver.title = eventList[i].text;
                if(eventList[i].participants[j].emailId == user.emailId){
                    if(!exist){
                        interactionReceiver.userId = user._id;
                        interactionReceiver.firstName = user.firstName;
                        interactionReceiver.lastName = user.lastName;
                        interactionReceiver.publicProfileUrl = user.publicProfileUrl;
                        interactionReceiver.profilePicUrl = user.profilePicUrl;
                        interactionReceiver.companyName = user.companyName;
                        interactionReceiver.designation = user.designation;
                        interactionReceiver.location = user.location;
                        interactionReceiver.mobileNumber = user.mobileNumber;
                        direct.push(interactionReceiver);
                    }
                }
                else{
                    other.push(interactionReceiver);
                    emailIds.push(eventList[i].participants[j].emailId)
                }
            }
        }
    }

    mapUserInteractionsWithProfileEmails(user,other,common.removeDuplicate_id(emailIds,true),direct,"calendar-events",false);
    callback(true);
}

//Bulk Contact addition
function makeContacts(user,contacts,callback){
    var contactsParsedEmail = [];
    var emailIds = [];
    var mobileNumbers = [];

    if(contacts && contacts.length>0){

        common.getInvalidEmailListFromDb(function (invalidEmailList) {

            for(var i=0; i<contacts.length; i++) {
                if (common.checkRequired(contacts[i]) && common.checkRequired(contacts[i].name) && common.validObject(contacts[i])/* && common.checkRequired(contacts[i].emailId)*/) {
                    var emailId = null
                    var mobileNumber = null
                    var accountName = null;

                    if(common.checkRequired(contacts[i].emailId)) {
                        emailId = contacts[i].emailId.trim().toLowerCase();
                        accountName = common.fetchCompanyFromEmail(emailId)
                    }
                    
                    if(common.checkRequired(contacts[i].mobileNumber)) {
                        mobileNumber = contacts[i].mobileNumber;
                    }

                    if(common.checkRequired(contacts[i].mobileNumber) || common.checkRequired(contacts[i].emailId) /*&& contacts[i].mobileNumber.length >= 5*/) {
                        contactsParsedEmail.push({
                            personId: '',
                            personName: contacts[i].name,
                            personEmailId: emailId,
                            count: 0,
                            addedDate: new Date(),
                            verified: false,
                            relatasContact: true,
                            relatasUser: false,
                            mobileNumber: contacts[i].mobileNumber || null,
                            source: 'mobile',
                            account:{
                                name:accountName
                            }
                        });

                        if (common.checkRequired(contacts[i].emailId) && common.isValidEmail(contacts[i].emailId, invalidEmailList)){
                            emailIds.push(emailId);
                        }

                        if(common.checkRequired(contacts[i].mobileNumber)){
                            mobileNumbers.push(mobileNumber)
                        }
                    }

                }
            }


            if(contactsParsedEmail.length > 0){
                //contactsParsedEmail = common.removeDuplicates_field(contactsParsedEmail,'personEmailId');
                //contactsParsedEmail = common.removeDuplicates_field(contactsParsedEmail,'mobileNumber');
                contactsParsedEmail = common.rmDuplicatesByFields(contactsParsedEmail,'personEmailId','mobileNumber')
            }
            if(contactsParsedEmail.length>0){
                contactsParsedEmail = common.mergeSimilarContact(contactsParsedEmail)
            }

            /*if(contactsParsedEmail.length > 0) {
            if (emailIds.length > 0){
            emailIds = common.removeDuplicate_id(emailIds, true);
            }
            if(mobileNumbers.length>0){
            mobileNumbers = common.removeDuplicate_id(mobileNumbers,true)
             }*/

            mapUserContactsWithProfileMobile(user,mobileNumbers,emailIds,contactsParsedEmail,false);
            callback(true);

        });
        //}
        //else callback(true)
    } else {
        callback(true)
    }
}

//contact addition after Call.
function checkIfContact(user,cUserMobileNumber, cUserEmailId,back){
    if(cUserMobileNumber!=null){
        myUserCollection.aggregate(
            [{$match:{emailId:user.emailId}},
            {$unwind:"$contacts"},
            {$match:{$or:[{"contacts.mobileNumber":cUserMobileNumber},{"contacts.personEmailId":cUserEmailId}]}}
            ],function(err,res){
                if(err){
                    throw new err
                }
                back(res)
            })
    }
}

function ContactUpdate(user, reqData, isFromContactUpload, callBack){
    //get User's profile to check if Relatas User
    getInteractedUsersProfile(reqData.mobileNumber,function(userProfile){
        if(userProfile.length==0){
            buildContactObject(userProfile[0],reqData,false,function(data){
                userManagementObj.update({emailId:user.emailId},{$addToSet:{"contacts":{$each:data}}},function(err,res){
                    callBack(err,res)
                })
            })
        }
        else{
            buildContactObject(userProfile[0],reqData,true,function(data){
                myUserCollection.update({emailId:user.emailId},{$addToSet:{"contacts":data}},function(err,res){
                    callBack(err,res)
                })
            })
        }
    })
}

function buildContactObject(profile, reqData,isRelatasUser, callBack){
    //building the contact object.
    var contactObj = {}
    if(isRelatasUser){
        contactObj.personId = profile._id
        contactObj.personName = profile.firstName + " " + profile.lastName
        contactObj.personEmailId = profile.emailId
        contactObj.count = 0
        contactObj.addedDate = new Date()
        contactObj.verified = false
        contactObj.relatasContact = true
        contactObj.relatasUser = false
        if(profile.mobileNumber==null){
            contactObj.mobileNumber = profile.mobileNumber
        }
        else {
            contactObj.mobileNumber = reqData.mobileNumber
        }
        contactObj.source = 'mobile'
    }
    else{
        contactObj.personId = null
        contactObj.personName = reqData.name.trim()
        contactObj.personEmailId = reqData.emailId.toLowerCase()
        contactObj.count = 0
        contactObj.addedDate = new Date()
        contactObj.verified = false
        contactObj.relatasContact = true
        contactObj.relatasUser = false
        contactObj.mobileNumber = reqData.mobileNumber || null
        contactObj.source = 'mobile'
    }
    callBack(contactObj)
}

function mapUserContactsWithProfileMobile(profile,mobileNumbers,emailIds,contacts,callback){

    var q = {$or:[]};
    var qExist = false;
    if(mobileNumbers.length > 0){
        qExist = true;
        q.$or.push({mobileNumber:{$in:mobileNumbers},mobileNumberWithoutCC:{$in:mobileNumbers}});
    }
    if(emailIds.length > 0){
        qExist = true;
        q.$or.push({emailId:{$in:emailIds}})
    }
    if(qExist){
        userManagementObj.selectUserProfilesCustomQuery(q,{skypeId:1,firstName:1,lastName:1,emailId:1,publicProfileUrl:1,profilePicUrl:1,companyName:1,designation:1,location:1,mobileNumber:1},function(err,users){
            if(err){
                loggerError.info('Error in mapUserContactsWithProfile():aggregateSupport ',err);
            }
            if(users != null && users != undefined && users.length > 0){
                users.forEach(function(user){
                    for(var i=0; i<contacts.length; i++){
                        if((contacts[i].personEmailId == user.emailId) || (contacts[i].mobileNumber == user.mobileNumber)){
                            contacts[i].personId = user._id
                            contacts[i].personName = user.firstName+' '+user.lastName
                            contacts[i].personEmailId = user.emailId
                            contacts[i].companyName = user.companyName
                            contacts[i].designation = user.designation
                            contacts[i].location = user.location
                            contacts[i].verified = true
                            contacts[i].relatasUser = true
                            contacts[i].relatasContact = true
                            contacts[i].mobileNumber = user.mobileNumber
                            contacts[i].skypeId = user.skypeId || ''
                        }
                    }
                })
            }
            //contactsManagementClassObj.addContactNotExistMobile2(common.castToObjectId(profile._id.toString()),profile.emailId,contacts,mobileNumbers,emailIds,'mobile',false);
            contactsManagementClassObj.updateOnlyRequiredFields(common.castToObjectId(profile.id.toString()),contacts,function(contactList){

                if(contactList.length>0){

                    contactsManagementClassObj.addContactNotExistMobile(common.castToObjectId(profile._id.toString()),profile.emailId,contacts,mobileNumbers,emailIds,'mobile',false)
                }
            })
            if(callback) callback(true)
        })
    }
    else {
        if(callback) callback(true)
    }
}

//used By web
function mapUserContactsWithProfile(profile,mobileNumbers,emailIds,contacts,callback){
    var q = {$or:[]};
    var qExist = false;
    if(mobileNumbers.length > 0){
        qExist = true;
        q.$or.push({mobileNumber:{$in:mobileNumbers}});
    }
    if(emailIds.length > 0){
        qExist = true;
        q.$or.push({emailId:{$in:emailIds}})
    }
    if(qExist){
        userManagementObj.selectUserProfilesCustomQuery(q,{skypeId:1,firstName:1,lastName:1,emailId:1,publicProfileUrl:1,profilePicUrl:1,companyName:1,designation:1,location:1,mobileNumber:1},function(err,users){
            if(err){
                loggerError.info('Error in mapUserContactsWithProfile():aggregateSupport ',err);
            }
            if(users != null && users != undefined && users.length > 0){
                users.forEach(function(user){
                    for(var i=0; i<contacts.length; i++){
                        
                        if(contacts[i].mobileNumber && contacts[i].mobileNumber.match(new RegExp(/[^0-9]/g))) {
                            contacts[i].mobileNumber = contacts[i].mobileNumber.replace(new RegExp(/[^0-9]/g), ''); //remove special character from mobile number
                        }

                        if(contacts[i].personEmailId == user.emailId){
                            contacts[i].personId = user._id
                            contacts[i].personName = user.firstName+' '+user.lastName
                            contacts[i].personEmailId = user.emailId
                            contacts[i].companyName = user.companyName
                            contacts[i].designation = user.designation
                            contacts[i].location = user.location
                            contacts[i].verified = true
                            contacts[i].relatasUser = true
                            contacts[i].relatasContact = true
                            contacts[i].mobileNumber = user.mobileNumber
                            contacts[i].skypeId = user.skypeId || ''
                        }
                    }
                })
            }

            contactsManagementClassObj.addContactNotExist(common.castToObjectId(profile._id.toString()),profile.emailId,contacts,mobileNumbers,emailIds,'mobile',false);
            //Set off another process to update contact name and image link
            // contactsManagementClassObj.updateContactNameAndProfileImage(common.castToObjectId(profile._id.toString()),contacts,common.castToObjectId,false);

            if(callback) callback(true)
        })
    }
    else {
        if(callback) callback(true)
    }
}

AggregateSupport.prototype.mapUserContactsWithProfile_ = function(profile,mobileNumbers,emailIds,contacts,callback){
    mapUserContactsWithProfile(profile,mobileNumbers,emailIds,contacts,callback);
};
module.exports = AggregateSupport;
