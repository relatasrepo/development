var salesforce = require("node-salesforce");
var request = require('request');
var moment = require("moment");
var momentTZ = require('moment-timezone');

var _ = require("lodash");

var userManagement = require('../dataAccess/userManagementDataAccess');
var opportunityManagement = require('../dataAccess/opportunitiesManagement');
var winstonLog = require('./winstonLog');
var appCredentials = require('../config/relatasConfiguration');

var userManagements = new userManagement();
var oppManagementObj = new opportunityManagement();
var logLib = new winstonLog();
var loggerError = logLib.getWinstonError();

function salesforceAPI(){
    this.getAccessToken = function(userId,refreshToken,callback){
        
        userManagements.getUserCompanyDetails(userId,function (err,companyInfo) {

            if(companyInfo) {

                request.post({
                    url:'https://login.salesforce.com/services/oauth2/token',
                    form: {
                        grant_type:"refresh_token",
                        client_id:companyInfo.salesForceSettings.clientId,
                        client_secret:companyInfo.salesForceSettings.clientSecret,
                        refresh_token:refreshToken,
                        format:"json"
                    }
                }, function(err,response,body){
                    var r = JSON.parse(response.body)
                    callback(r.error, r)
                });

            } else {
                callback(true,null)
            }
        });
    };

    this.getSalesforceContactsByIds = function(user,salesforceContactIds,callback){
        var conn = new salesforce.Connection({
            instanceUrl : user.instance_url,
            accessToken : user.access_token
        });
        if(salesforceContactIds.length  >= 1) {
            var whereIn = "(";
            for (var i = 0; i < salesforceContactIds.length; i++) {
                if (i < salesforceContactIds.length - 1) {
                    whereIn = whereIn + "'" + salesforceContactIds[i] + "'" + ",";
                }
                else {
                    whereIn = whereIn + "'" + salesforceContactIds[i] + "'" + ")";
                }
            }

            conn.sobject("Contact")
                .select('email,firstName,lastName,id,CreatedDate')
                .where('Id IN ' + whereIn)
                .execute(function (err, oppContRole) {
                    if (err) {
                        loggerError.info('ERROR: salesforceAPI - getSalesforceContactsByIds() ',err );
                        callback(err, null)
                        conn.cache.clear();
                        conn.describeGlobal$.clear();
                    }
                    else {
                        callback(null, oppContRole)
                        conn.cache.clear();
                        conn.describeGlobal$.clear();
                    }
                });
        }
        else{
            callback(null, [])
        }
    };

    this.getSalesforceOpportunityContactRoleByOppIds = function(user,salesforceOppIds,callback){
        var records = [];
        var conn = new salesforce.Connection({
            instanceUrl : user.instance_url,
            accessToken : user.access_token
        });
        if(salesforceOppIds.length  >= 1) {
            var whereIn = "(";
            for (var i = 0; i < salesforceOppIds.length; i++) {
                if (i < salesforceOppIds.length - 1) {
                    whereIn = whereIn + "'" + salesforceOppIds[i] + "'" + ",";
                }
                else {
                    whereIn = whereIn + "'" + salesforceOppIds[i] + "'" + ")";
                }
            }

            conn.sobject("OpportunityContactRole")
                .select('contactId,opportunityId')
                .where('opportunityId IN ' + whereIn)
                .execute(function (err, oppContRole) {
                    if (err) {
                        loggerError.info('ERROR: salesforceAPI - getSalesforceOpportunityContactRoleByOppIds() ',err );
                        callback(err, null)
                        conn.cache.clear();
                        conn.describeGlobal$.clear();
                    }
                    else {
                        callback(null, oppContRole)
                        conn.cache.clear();
                        conn.describeGlobal$.clear();
                    }
                });
        }
        else{
            callback(null, [])
        }
    };

    this.getAndUpdateStageNames = function(user,callback){

        var conn = new salesforce.Connection({
            instanceUrl : user.instance_url,
            accessToken : user.access_token
        });

        var listOfStages = [];

        conn.sobject("Opportunity").describe(function(err, meta) {

            if(meta && meta.fields && meta.fields.length>0){
                _.each(meta.fields,function (el) {

                    if(el.name == "StageName"){
                        _.each(el.picklistValues,function (stage) {
                            listOfStages.push(stage.value)
                        });
                    }
                })
            }
            callback(listOfStages)
        });
    }

    this.getSalesforceOpportunitiesbyFilter = function(user,date,filter,callback){
        var records = [];
        var conn = new salesforce.Connection({
            instanceUrl : user.instance_url,
            accessToken : user.access_token
        });

        if(!date) {
            filter = 'all';
        }
        if(filter == 'all'){
            conn.sobject("Opportunity")
                .select('amount,isClosed,isWon,name,stageName,id,LastModifiedDate,CreatedDate,CloseDate,OwnerId')
                .execute(function (err, opp) {
                    if (err) {
                        loggerError.info('ERROR: salesforceAPI - getSalesforceOpportunitiesbyFilter() ',err );
                        callback(err, null)
                        conn.cache.clear();
                        conn.describeGlobal$.clear();
                    }
                    else {
                        callback(null, opp)
                        conn.cache.clear();
                        conn.describeGlobal$.clear();
                    }
                });
        }
        else{
            var condition = '';
            var timezone = 'UTC';
            var dateMin = momentTZ(date).tz(timezone)
            var minDate = moment(dateMin).format("YYYY-MM-DDTHH:mm:ss") + 'Z';
            if(filter == 'edited'){
                condition = condition +'LastModifiedDate > '+minDate+' AND CreatedDate < '+minDate;
            }
            else if(filter == 'new'){
                condition = condition +'CreatedDate > '+minDate;
            }
            else if(filter == 'newAndEdited'){
                condition = condition +'LastModifiedDate > '+minDate+' OR (LastModifiedDate > '+minDate+' AND CreatedDate < '+minDate +')';
            }

            conn.sobject("Opportunity")
                .select('amount,isClosed,isWon,name,stageName,id,LastModifiedDate,CreatedDate,CloseDate,OwnerId')
                .where(condition)
                .execute(function (err, opp) {
                    if (err) {
                        loggerError.info('ERROR: salesforceAPI - getSalesforceOpportunitiesbyFilter() ',err );
                        callback(err, null)
                        conn.cache.clear();
                        conn.describeGlobal$.clear();
                    }
                    else {
                        callback(null, opp)
                        conn.cache.clear();
                        conn.describeGlobal$.clear();
                    }
                });
        }
    };

    this.insertNonExistingOpportunities = function(userId,updateObj, oppIds, callback){
        oppManagementObj.insertNonExistingOpportunities(userId,updateObj, oppIds, callback)
    }

    this.insertOrUpdateOpportunities = function(updateObj, callback){
        oppManagementObj.insertOrUpdateOpportunitiesBulk(updateObj, callback)
    }

    this.insertOrUpdateOpportunities2 = function(updateObj, callback){
        oppManagementObj.insertOrUpdateOpportunitiesBulk2(updateObj, callback)
    }
}

module.exports = salesforceAPI;