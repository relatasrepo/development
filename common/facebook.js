var appCredentials = require('../config/relatasConfiguration');
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');
var userManagement = require('../dataAccess/userManagementDataAccess');
var interactions = require('../dataAccess/interactionManagement');
var request = require('request');
var userManagementObj = new userManagement();
var interactionObj = new interactions();
var interaction = require('../databaseSchema/userManagementSchema').interaction;
var appCredential = new appCredentials();
var common = new commonUtility();
var logLib = new winstonLog();
var logger = logLib.getWinston();
var authConfig = appCredential.getAuthCredentials();
var FB = require('fb');
var graph = require('fbgraph');
var https = require('https');
function buildPosts(id, postsDataArray, nextPageUrl, lastSync, builtPosts, allPosts, callback) {
    //console.log('nextPageUrl\n' + nextPageUrl + '\n\n' + 'lastSync\n' + lastSync + '\n\n' + 'postsDataArray\n');
    //postsDataArray.forEach(function (thisPost) {
    //    console.log(JSON.stringify(thisPost, null, 4));
    //});
    //console.log('\n\n');
    var last30 = (new Date(((new Date()).setDate((new Date(Date.now())).getDate() - 30))))
    for (var i = 0; i < postsDataArray.length; i++) {
        thisPost = postsDataArray[i];
        if ((new Date(thisPost.created_time)) > last30) {
            if ((new Date(thisPost.updated_time)) > (new Date(lastSync))) { //If current post is newer
                //if (common.checkRequired(thisPost.story))
                allPosts.push(thisPost);
                // if (common.checkRequired(thisPost.story) && common.checkRequired(thisPost.to))
                if (common.checkRequired(thisPost.to))
                builtPosts.push(thisPost);
                if ((postsDataArray.length - 1) === i && common.checkRequired(nextPageUrl)) {
                    https.get(nextPageUrl, function (res) {
                        var totStr = '';
                        res.setEncoding('utf8'); //Else you will get Buffer object
                        res.on('data', function (data) {
                            totStr += data;
                        });
                        res.on('end', function (data) {
                            var newObj = JSON.parse(totStr);
                            if (!common.checkRequired(newObj.paging))
                            newObj.paging = {
                            }
                            return buildPosts(id, newObj.data, newObj.paging.next, lastSync, builtPosts, allPosts);
                        });
                    });
                }
            }
        } 
        else {
            break;
        }
    }
    callback({
        'builtPosts': builtPosts,
        'allPosts': allPosts
    });
}
function processLikes(allLikes, currentLikesArray, nextPageUrl, callback) {
    //console.log('\nInside processLikes() with \n\n' + JSON.stringify(currentLikesArray, null, 4) + '\n' + nextPageUrl + '\n\n')
    allLikes = allLikes.concat(currentLikesArray);
    if (common.checkRequired(nextPageUrl)) {
        https.get(nextPageUrl, function (res) {
            var totStr = '';
            res.setEncoding('utf8'); //Else you will get Buffer object
            res.on('data', function (data) {
                totStr += data;
            });
            res.on('end', function (data) {
                var newObj = JSON.parse(totStr);
                if (!common.checkRequired(newObj.paging))
                newObj.paging = {
                }
                return processLikes(allLikes, newObj.data, newObj.paging.next);
            });
        });
    } else {
        callback(allLikes);
    }
}
function processComments(allComments, currentCommentsArray, nextPageUrl, callback) {
    //console.log('\nInside processLikes() with \n\n' + JSON.stringify(currentLikesArray, null, 4) + '\n' + nextPageUrl + '\n\n')
    allComments = allComments.concat(currentCommentsArray);
    if (common.checkRequired(nextPageUrl)) {
        https.get(nextPageUrl, function (res) {
            var totStr = '';
            res.setEncoding('utf8'); //Else you will get Buffer object
            res.on('data', function (data) {
                totStr += data;
            });
            res.on('end', function (data) {
                var newObj = JSON.parse(totStr);
                if (!common.checkRequired(newObj.paging))
                newObj.paging = {
                }
                return processComments(allComments, newObj.data, newObj.paging.next);
            });
        });
    } else {
        callback(allComments);
    }
}
var processPostsLikes = function (facebook, allPosts, user) {
    graph.setAccessToken(facebook.token);
    graph.setVersion('2.5');
    graph.get('/' + allPosts[0].id + '/likes', function (err, res) {
        if (err) {
        } 
        else {
            if (res.data.length > 0) {
                if (!common.checkRequired(res.paging))
                res.paging = {
                }
                processLikes([], res.data, res.paging.next, function (allLikes) {
                    // console.log("allPosts[0] inside processLikes:\n"+JSON.stringify(allPosts, null, 4)+"\n\n");
                    //console.log('Facebook\n' + JSON.stringify(facebook, null, 4) + 'allLikes:\n' + JSON.stringify(allLikes, null, 4) + '\n\n');
                    //Add to interaction table if allLikes.forEach() is a favorite
                    // if(!common.checkRequired(allPosts[0].from)){
                    //     allPosts[0].from={};
                    //     allPosts[0].from.id=facebook.id;
                    // }
                    userManagementObj.isExistingUserByFBId(allPosts[0].from.id, function (error, user, message) {
                        if (message.message === 'existing user')
                        for (var i = 0; i < allLikes.length; i++) {
                            //if (message.message === 'existing user')
                            userManagementObj.isExistingUserByFBIdWithIndex(i, allLikes[i].id, function (error, existingUser, message, i) {
                                if (message.message === 'existing user') {
                                    var arr = [
                                    ];
                                    var my = new Object();
                                    my.userId = existingUser._id;
                                    my.user_facebook_id = existingUser.facebook.id;
                                    my.firstName = existingUser.firstName;
                                    my.lastName = existingUser.lastName;
                                    my.publicProfileUrl = existingUser.publicProfileUrl;
                                    my.profilePicUrl = existingUser.profilePicUrl;
                                    my.companyName = existingUser.companyName;
                                    my.designation = existingUser.designation;
                                    my.location = existingUser.location;
                                    my.mobileNumber = existingUser.mobileNumber;
                                    my.action = 'sender';
                                    my.type = 'facebook';
                                    my.subType = 'like';
                                    my.emailId = existingUser.emailId;
                                    my.refId = allPosts[0].id + '_like_' + existingUser._id;
                                    my.parentRefId = allPosts[0].id;
                                    my.source = 'facebook';
                                    my.interactionType = 'facebook';
                                    if (common.checkRequired(allPosts[0].message))
                                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' liked : ' + allPosts[0].message;
                                     else
                                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' liked : ' + allPosts[0].name || allPosts[0].story || allPosts[0].description || '';
                                    my.description = allPosts[0].description || '';
                                    my.interactionDate = new Date(allPosts[0].updated_time);
                                    my.createdDate = new Date(allPosts[0].updated_time);
                                    arr.push(my);
                                    var my = new Object();
                                    my.userId = user._id;
                                    my.user_facebook_id = user.facebook.id;
                                    my.firstName = user.firstName;
                                    my.lastName = user.lastName;
                                    my.publicProfileUrl = user.publicProfileUrl;
                                    my.profilePicUrl = user.profilePicUrl;
                                    my.companyName = user.companyName;
                                    my.designation = user.designation;
                                    my.location = user.location;
                                    my.mobileNumber = user.mobileNumber;
                                    my.action = 'receiver';
                                    my.type = 'facebook';
                                    my.subType = 'like';
                                    my.emailId = user.emailId;
                                    my.refId = allPosts[0].id + '_like_' + user._id;
                                    my.parentRefId = allPosts[0].id;
                                    my.source = 'facebook';
                                    my.interactionType = 'facebook';
                                    if (common.checkRequired(allPosts[0].message))
                                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' liked : ' + allPosts[0].message;
                                     else
                                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' liked : ' + allPosts[0].name || allPosts[0].story || allPosts[0].description || '';
                                    my.description = allPosts[0].description || '';
                                    my.interactionDate = new Date(allPosts[0].updated_time);
                                    my.createdDate = new Date(allPosts[0].updated_time);
                                    arr.push(my);
                                    addAllInteractions(user._id, arr, function (isSuccess) {
                                    });
                                    if (common.checkRequired(allPosts[0].message))
                                    arr[1].title = arr[0].title = 'You liked : ' + allPosts[0].message;
                                     else
                                    arr[1].title = arr[0].title = 'You liked : ' + allPosts[0].name || allPosts[0].message || allPosts[0].story || allPosts[0].description || '';
                                    //if (arr[0].user_facebook_id.toString() != arr[1].user_facebook_id.toString()) {
                                        addAllInteractions(existingUser._id, arr, function (isSuccess) {
                                        });
                                    //}
                                }
                            });
                        }
                    })
                })
            }
        }
        var newAllPosts = allPosts.slice(1);
        if (allPosts.length > 0) {
            processPostsLikes(facebook, newAllPosts, user);
        }
    })
}
var processPostsComments = function (facebook, allPosts, user) {
    graph.setAccessToken(facebook.token);
    graph.setVersion('2.5');
    graph.get('/' + allPosts[0].id + '/comments', function (err, res) {
        if (err) {
        } 
        else {
            if (res.data.length > 0) {
                if (!common.checkRequired(res.paging))
                res.paging = {
                }
                processComments([], res.data, res.paging.next, function (allComments) {
                    //console.log('After processComments\n' + JSON.stringify(facebook, null, 4) + '\nallComments:\n' + JSON.stringify(allComments, null, 4) + '\n\n');
                    //Add to interaction table if allComments.forEach() is a favorite
                    // if(!common.checkRequired(allPosts[0].from)){
                    //     allPosts[0].from={};
                    //     allPosts[0].from.id=facebook.id;
                    // }
                    userManagementObj.isExistingUserByFBId(allPosts[0].from.id, function (error, user, message) {
                        if (message.message === 'existing user')
                        for (var i = 0; i < allComments.length; i++) {
                            //console.log('allComments[i] is ' + JSON.stringify(allComments[i], null, 4))
                            userManagementObj.isExistingUserByFBIdWithIndex(i, allComments[i].from.id, function (error, existingUser, message, i) {
                                if (message.message === 'existing user') {
                                    var arr = [
                                    ];
                                    var my = new Object();
                                    my.userId = existingUser._id;
                                    my.user_facebook_id = existingUser.facebook.id;
                                    my.firstName = existingUser.firstName;
                                    my.lastName = existingUser.lastName;
                                    my.publicProfileUrl = existingUser.publicProfileUrl;
                                    my.profilePicUrl = existingUser.profilePicUrl;
                                    my.companyName = existingUser.companyName;
                                    my.designation = existingUser.designation;
                                    my.location = existingUser.location;
                                    my.mobileNumber = existingUser.mobileNumber;
                                    my.action = 'sender';
                                    my.type = 'facebook';
                                    my.subType = 'comment';
                                    my.emailId = existingUser.emailId;
                                    my.refId = allComments[i].id;
                                    my.parentRefId = allPosts[0].id;
                                    my.source = 'facebook';
                                    my.interactionType = 'facebook';
                                    if (common.checkRequired(allPosts[0].message))
                                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' commented : ' + allComments[i].message + ' on ' + allPosts[0].message;
                                     else
                                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' commented : ' + allComments[i].message + ' on ' + allPosts[0].name || allPosts[0].message || allPosts[0].story || allPosts[0].description || '';
                                    my.description = allComments[i].message + ' on ' + allPosts[0].description || '';
                                    my.interactionDate = new Date(allComments[i].created_time);
                                    my.createdDate = new Date(allComments[i].created_time);
                                    arr.push(my);
                                    var my = new Object();
                                    my.userId = user._id;
                                    my.user_facebook_id = user.facebook.id;
                                    my.firstName = user.firstName;
                                    my.lastName = user.lastName;
                                    my.publicProfileUrl = user.publicProfileUrl;
                                    my.profilePicUrl = user.profilePicUrl;
                                    my.companyName = user.companyName;
                                    my.designation = user.designation;
                                    my.location = user.location;
                                    my.mobileNumber = user.mobileNumber;
                                    my.action = 'receiver';
                                    my.type = 'facebook';
                                    my.subType = 'comment';
                                    my.emailId = user.emailId;
                                    my.refId = allComments[i].id;
                                    my.parentRefId = allPosts[0].id;
                                    my.source = 'facebook';
                                    my.interactionType = 'facebook';
                                    if (common.checkRequired(allPosts[0].message))
                                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' commented : ' + allComments[i].message + ' on ' + allPosts[0].message;
                                     else
                                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' commented : ' + allComments[i].message + ' on ' + allPosts[0].name || allPosts[0].message || allPosts[0].story || allPosts[0].description || '';
                                    my.description = allComments[i].message + ' on ' + allPosts[0].description || '';
                                    my.interactionDate = new Date(allComments[i].created_time);
                                    my.createdDate = new Date(allComments[i].created_time);
                                    arr.push(my);
                                    addAllInteractions(user._id, arr, function (isSuccess) {
                                    });
                                    if (common.checkRequired(allPosts[0].message))
                                    arr[1].title = arr[0].title = 'You commented : ' + allComments[i].message + ' on ' + allPosts[0].message;
                                     else
                                    arr[1].title = arr[0].title = 'You commented : ' + allComments[i].message + ' on ' + allPosts[0].name || allPosts[0].message || allPosts[0].story || allPosts[0].description || '';
                                    //if (arr[0].user_facebook_id.toString() != arr[1].user_facebook_id.toString()) {
                                        addAllInteractions(existingUser._id, arr, function (isSuccess) {
                                        });
                                    //}
                                }
                            });
                        }
                    })
                })
            }
        }
        var newAllPosts = allPosts.slice(1);
        if (allPosts.length > 0) {
            processPostsComments(facebook, newAllPosts, user);
        }
    })
}
function Facebook() {
    this.getFBFeedsSinceLastUpdate = function (facebook, callback) {
        graph.setAccessToken(facebook.token);
        graph.setVersion('2.5');
        var params = {
            fields: 'id,name,description,type,story,created_time,updated_time,from,to,message'
        };
        graph.get(facebook.id + '/feed', params, function (err, res) {
            if (err) {
                callback(err, null);
            } 
            else {
                var posts = undefined;
                if (common.checkRequired(res.data[0])) {
                    if (!common.checkRequired(res.paging))
                    res.paging = {
                    }
                    buildPosts(facebook.id, res.data, res.paging.next, facebook.lastSync, [
                    ], [
                    ], function (postsObject) {
                        //console.log('getFBFeedsSinceLastUpdate gave\n' + JSON.stringify(postsObject, null, 4) + '\n\n');
                        //processPosts(facebook,postsObject.allPosts);
                        //console.log(posts.allPosts);
                        callback(null, postsObject);
                    });
                } 
                else
                callback(null, null);
            }
        })
    }
    this.likeFacebookFeed = function (facebook, postId, callback) {
        if (this.validateFBAccount(facebook) && common.checkRequired(postId)) {
            FB.api(postId + '/likes', 'post', {
                url: 'https://graph.facebook.com/' + postId + '/likes',
                access_token: facebook.token
            }, function (result) {
                if (result === true) {
                    callback(false, true)
                } 
                else {
                    logger.info('Error in likeFacebookFeed():Facebook ', result);
                    callback('error', false)
                }
            }
            )
        } else callback('no_account', false)
    };
    this.commentOnFacebookFeed = function (facebook, postId, message, callback) {
        if (this.validateFBAccount(facebook) && common.checkRequired(postId)) {
            FB.api(postId + '/comments', 'post', {
                message: message,
                access_token: facebook.token
            }, function (result) {
                if (result === true) {
                    callback(false, true)
                } 
                else {
                    logger.info('Error in commentOnFacebookFeed():Facebook ', result);
                    callback('error', false)
                }
            }
            )
        } else callback('no_account', false)
    };
    function getFBFriends(facebook, url, callback) {
        FB.api(url, 'GET', {
            url: url,
            access_token: facebook.token
        }, function (result) {
            if (result.paging && result.paging.next) {
                request.get(result.paging.next, function (err, res, data) {
                });
                // getFBFriends(facebook,result.paging.next,callback)
            } 
            else callback()
        }
        )
    }
    this.getFBFriends = function (facebook, url, callback) {
        if (this.validateFBAccount(facebook)) {
            getFBFriends(facebook, url, callback)
        } else callback('no_account', false)
    };
    this.findIsFbFriend = function (facebook, otherUserFBID, callback) {
        if (this.validateFBAccount(facebook) && common.checkRequired(otherUserFBID)) {
            FB.api('me/friends/' + otherUserFBID, 'GET', {
                access_token: facebook.token
            }, function (result) {
                callback(result)
            }
            )
        } else callback('no_account', false)
    };
    this.validateFBAccount = function (facebook) {
        if (common.checkRequired(facebook) && common.checkRequired(facebook.token)) {
            return true;
        } 
        else return false;
    };
    this.likeFBObject = function (userId, objectId, callback) {
        userManagementObj.findUserProfileByIdWithCustomFields(userId, {
            facebook: 1
        }, function (error, user) {
            if (error)
            callback(error, false);
             else if (user) {
                var options = {
                    method: 'POST',
                    uri: 'https://graph.facebook.com/v2.5/' + objectId + '/likes' + '?access_token=' + user.facebook.token
                }
                request(options, function (error, response, body) {
                    if (error)
                    callback(error, false) 
                    else {
                        var result = JSON.parse(body);
                        if (result.success === true) {
                            callback(null, true)
                        } 
                        else
                        callback(error, false)
                    }
                })
            }
        })
    }
    this.commentOnFBObject = function (userId, objectId, comment, callback) {
        userManagementObj.findUserProfileByIdWithCustomFields(userId, {
            facebook: 1
        }, function (error, user) {
            if (error)
            callback(error, false);
             else if (user) {
                graph.setAccessToken(user.facebook.token);
                graph.setVersion('2.5');
                var myComment = {
                    message: comment
                }
                graph.post('/' + objectId + '/comments', myComment, function (err, res) {
                    if (err)
                    callback(err, false) 
                    else if (common.checkRequired(res) && common.checkRequired(res.id)) {
                        callback(null, true)
                    } 
                    else {
                        callback(null, false)
                    }
                })
            }
        })
    }
    this.shareFBObject = function (userId, objectId, callback) {
        userManagementObj.findUserProfileByIdWithCustomFields(userId, {
            facebook: 1
        }, function (error, user) {
            if (error)
            callback(error, false);
             else if (user) {
                graph.setAccessToken(user.facebook.token);
                graph.setVersion('2.5');
                var params = {
                    fields: 'link'
                };
                graph.get('/' + objectId, params, function (err, res) {
                    if (err)
                    callback(err, false) 
                    else if (common.checkRequired(res) && common.checkRequired(res.link)) {
                        var myPost = {
                            link: res.link
                        }
                        graph.post('/me/feed', myPost, function (err, res) {
                            if (err)
                            callback(err, false) 
                            else if (common.checkRequired(res) && common.checkRequired(res.id)) {
                                callback(null, true)
                            } 
                            else {
                                callback(null, false)
                            }
                        })
                    } 
                    else
                    callback(null, false)
                })
            }
        })
    }
    this.FacebookFeedToInteractions = function (facebook, user, callback) {
        this.getFBFeedsSinceLastUpdate(facebook, function (error, posts) {
            if (error) {
                logger.info('FacebookFeedToInteractions() exited with error ', error);
                callback(false);
            } 
            else if (posts && posts.allPosts && posts.allPosts.length > 0) {
                userManagementObj.updateFacebookLastSyncByFBId(facebook.id, new Date(posts.allPosts[0].created_time));
                //for(var i=0;i<posts.allPosts.length;i++)
                processPostsLikes(facebook, posts.allPosts, user);
                processPostsComments(facebook, posts.allPosts, user);
                posts = posts.allPosts;
                if (posts && posts.length > 0)
                {
                    for (var i = 0; i < posts.length; i++) {
                        //console.log("Posts is :\n"+JSON.stringify(posts,null,4)+"\n\n")
                        userManagementObj.isExistingUserByFBIdWithIndex(i, posts[i].from.id, function (error, existingUser, message, i) {
                            if (message.message === 'existing user') {
                                interaction.update({
                                }, {
                                    $pull: {
                                        interactions: {
                                            parentRefId: posts[i].id
                                        }
                                    }
                                }, {
                                    multi: true
                                }, function (error, result) {
                                    console.log("FLAG")
                                    console.log(error)
                                    console.log(result)
                                    var arr = [
                                    ];
                                    var my = new Object();
                                    my.userId = existingUser._id;
                                    my.user_facebook_id = existingUser.facebook.id;
                                    my.firstName = existingUser.firstName;
                                    my.lastName = existingUser.lastName;
                                    my.publicProfileUrl = existingUser.publicProfileUrl;
                                    my.profilePicUrl = existingUser.profilePicUrl;
                                    my.companyName = existingUser.companyName;
                                    my.designation = existingUser.designation;
                                    my.location = existingUser.location;
                                    my.mobileNumber = existingUser.mobileNumber;
                                    my.action = 'sender';
                                    my.type = 'facebook';
                                    my.subType = 'share';
                                    my.emailId = existingUser.emailId;
                                    my.refId = posts[i].id;
                                    my.parentRefId = posts[i].id;
                                    my.source = 'facebook';
                                    my.interactionType = 'facebook';
                                    if (common.checkRequired(posts[i].message)) {
                                        my.title = existingUser.firstName + ' ' + existingUser.lastName + ' posted : ' + posts[i].message
                                        my.subType = 'status';
                                    } 
                                    else
                                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' shared : ' + posts[i].name || posts[i].story || posts[i].description || '';
                                    my.description = posts[i].description || '';
                                    my.interactionDate = new Date(posts[i].created_time);
                                    my.createdDate = new Date(posts[i].created_time);
                                    arr.push(my);
                                    var my = new Object();
                                    my.userId = user._id;
                                    my.user_facebook_id = user.facebook.id;
                                    my.firstName = user.firstName;
                                    my.lastName = user.lastName;
                                    my.publicProfileUrl = user.publicProfileUrl;
                                    my.profilePicUrl = user.profilePicUrl;
                                    my.companyName = user.companyName;
                                    my.designation = user.designation;
                                    my.location = user.location;
                                    my.mobileNumber = user.mobileNumber;
                                    my.action = 'receiver';
                                    my.type = 'facebook';
                                    my.subType = 'share';
                                    my.emailId = user.emailId;
                                    my.refId = posts[i].id;
                                    my.parentRefId = posts[i].id;
                                    my.source = 'facebook';
                                    my.interactionType = 'facebook';
                                    if (common.checkRequired(posts[i].message)) {
                                        my.title = existingUser.firstName + ' ' + existingUser.lastName + ' posted : ' + posts[i].message
                                        my.subType = 'status';
                                    } 
                                    else
                                    my.title = existingUser.firstName + ' ' + existingUser.lastName + ' shared : ' + posts[i].name || posts[i].story || posts[i].description || '';
                                    my.description = posts[i].description || '';
                                    my.interactionDate = new Date(posts[i].created_time);
                                    my.createdDate = new Date(posts[i].created_time);
                                    arr.push(my);
                                    addAllInteractions(user._id, arr, function (isSuccess) {
                                        if (isSuccess === true)
                                        callback(true);
                                         else
                                        callback(false);
                                    });
                                    if (common.checkRequired(posts[i].message))
                                    arr[1].title = arr[0].title = 'You posted : ' + posts[i].message;
                                     else
                                    arr[1].title = arr[0].title = 'You shared : ' + posts[i].name || posts[i].story || posts[i].description || '';
                                    addAllInteractions(existingUser._id, arr, function (isSuccess) {
                                    });
                                })
                            }
                        })
                    }
                }
            } 
            else {
                logger.info('No new Facebook posts since last update');
                callback(true);
            }
        })
    }
}
function addAllInteractions(userId, interactions, callback) {
    // console.log('addAllInteractions called with userId\n' + userId + '\n\ninteractions\n' + JSON.stringify(interactions, null, 4) + '\n\n')
    interaction.update({
        userId: userId
    }, {
        $push: {
            interactions: {
                $each: interactions
            }
        }
    }, {
        upsert: true
    }, function (error, result) {
        if (error) {
            logger.info('Error in addAllInteractions(): interactionsManagement ', error);
            callback(false)
        } 
        else {
            // console.log('result is ' + JSON.stringify(result) + '\n\n');
            // console.log('Added the following FB interaction for ' + userId + '\n\n');
            // console.log(JSON.stringify(interactions, null, 4) + '\n\n');
            callback(true);
        }
    });
}
String.prototype.toObjectId = function () {
    var ObjectId = (require('mongoose').Types.ObjectId);
    return new ObjectId(this.toString());
};
module.exports = Facebook;
