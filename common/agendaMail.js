
// initializing  the required module

var gcal = require('google-calendar');
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var moment = require('moment-timezone');
var querystring = require("querystring");

var userManagement = require('../dataAccess/userManagementDataAccess');
var meetingManagement = require('../dataAccess/meetingManagement');
var eventDataAccess = require('../dataAccess/eventDataAccess');
var interactions = require('../dataAccess/interactionManagement');
var calendarPasswordManagement = require('../dataAccess/calendarPasswordManagement');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var googleCalendarApi = require('../common/googleCalendar');
var contactClass = require('../dataAccess/contactsManagementClass');
var myUserCollection = require('../databaseSchema/userManagementSchema').User;

var interaction = new interactions();
var appCredential = new appCredentials();
var common = new commonUtility();
var googleCalendar = new googleCalendarApi();
var contactObj = new contactClass();
var meetingClassObj = new meetingManagement();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var domainName = domain.domainName;

var googleA = require('node-google-api')(authConfig.GOOGLE_CLIENT_ID);
var googleApI;
googleA.build(function(api) {
    googleApI = api;
});
var usersAll = [];
var lastUser;
var logLib = new winstonLog();
var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();
var allUserEvents = {};
var userManagements = new userManagement();
var eventAccess = new  eventDataAccess();
var emailSenders = new emailSender();
var today;

var icons = appCredential.getRelatasHeaders();

var icon_conf = domainName+'/images/mailer/icon_conf.png';
var icon_relatas = domainName+'/images/mailer/icon_relatas.png';
var icon_location = domainName+'/images/mailer/icon_location.png';
var icon_converse = domainName+'/images/mailer/icon_converse_new.png';
var icon_facebook = domainName+'/images/mailer/icon_facebook_new.png';
var icon_linkedin = domainName+'/images/mailer/icon_linkedin_new.png';
var icon_pdf = domainName+'/images/mailer/icon_pdf_new.png';
var icon_phone = domainName+'/images/mailer/icon_phone.png';
var icon_skype = domainName+'/images/mailer/icon_skype.png';
var icon_twitter = domainName+'/images/mailer/icon_twitter_new.png';
var social_g = domainName+'/images/mailer/social_g+.png';
var event_new = domainName+'/images/event_new.png';
var not_confirmed = domainName+'/images/mailer/not-confirmed.png';
var rsvp = domainName+'/images/mailer/rsvp.png';
var icon_pending = domainName+'/images/mailer/pending.jpg';
var icon_confirm = domainName+'/images/mailer/confirm.jpg';
var meeting_todo = domainName+'/images/todo1.jpg';

function pad(n){
    return n<10 ? '0'+n : n
}

function AgendaMails(){

}

AgendaMails.prototype.nextUser = function(){
    if(checkRequired(googleApI)) {
        if(checkRequired(lastUser)){
            loggerError.info('Agenda Mail not sent '+lastUser.emailId);
        }
        series(usersAll.shift());
    }
};

AgendaMails.prototype.sendDailyCalendarEventsAgenda = function(number){
    var date = new Date();
    date.setDate(date.getDate()+1);
    today = date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate();
    loggerError.info('API started ');
a(number);
};


function a(number){
    loggerError.info('API started function a()');
     allUserEvents = {};

    if(checkRequired(googleApI)){
        loggerError.info('api created');
        userManagements.getAllUsersGoogleAccounts(number,function(users){
            //Find only the userID and then in the callback for each user, fecth other details and send the mail. Rght now this has all the data of all the users
            if(checkRequired(users.length>0)){

                if(checkRequired(users)){
                    loggerError.info('REMOVED RECURSION');
                    loggerError.info('user length '+users.length);

                    for(var j= 0,len = users.length; j < len; j++){

                        myUserCollection.find({_id:users[j]._id},{firstName:1,lastName:1,emailId:1,google:1,sendDailyAgenda:1,facebook:1,linkedin:1,twitter:1,zoneNumber:1,timezone:1,lastLoginDate:1,dashboardPopup:1,lastAgendaSentDate:1,profilePicUrl:1},function(err,user){

                            getAllGoogleAccountsCalendarEventsByUser2(user[0],function(err,result){
                                if(err){
                                    loggerError.info("getAllGoogleAccountsCalendarEventsByUser2"+err);
                                } else {
                                    loggerError.info("Successfully sent AgendaMail for " +user[0].emailId);
                                }
                            });
                        });

                    }

                    //usersAll = users;
                    //    series(usersAll.shift()); This is recursion implemented by Sudhakar. Changing to FOR loop for better optimisation. Last 18 Feb 2016 modified by Naveen
                }
            }
        })
    }else{
        loggerError.info('no api created');
        setTimeout(function(){
            a(number);
        },1000)
    }
}

function checkRequired(data){
    if(data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

var eventsOfOneUser = [];
function series(item) {
    if (item) {
        lastUser = item;
        loggerError.info('User '+usersAll.length);

        getAllGoogleAccountsCalendarEventsByUser2(item, function (result) {
            return series(usersAll.shift());
        });
    } else {
        //return final();
    }
}

var google = [];
var currentUser;

function getAllGoogleAccountsCalendarEventsByUser2(user,callback){
 currentUser = user;
    loggerError.info('user started '+user.emailId);

 if(user.firstName){
     if(checkRequired(user) && checkRequired(googleApI)){

         if(user.sendDailyAgenda == false){
             loggerError.info('Daily agenda option false '+user.emailId);
             callback(null)
         }else
         if(checkRequired(user.google)){
             if(checkRequired(user.google[0])){

                 google = user.google;
                 seriesGoogle(google.shift(),callback)
             }else  callback(null)
         }else  callback(null)
     } else  callback(null)
 }else  callback(null)
}

function seriesGoogle(googleAcc,callback) {
    if (googleAcc) {

        getAllGoogleAccountsCalendarEvents(googleAcc, function (events) {

                if(checkRequired(allUserEvents[currentUser.emailId])){
                    if(checkRequired(events)){
                        allUserEvents[currentUser.emailId].push(events)
                    }

                }else{
                    allUserEvents[currentUser.emailId] = [];
                    allUserEvents[currentUser.emailId][0] = currentUser;
                    if(checkRequired(events)){
                        allUserEvents[currentUser.emailId].push(events)
                    }
                }

            return seriesGoogle(google.shift(),callback);
        });
    } else {

        var abc = allUserEvents[currentUser.emailId]
        allUserEvents[currentUser.emailId] = null;

        userExecutedBefore(abc);
        callback(null);

    }
}

function getAllGoogleAccountsCalendarEvents(googleAcc,callback){

    var dateMin = moment();
    var dateMax = moment();
    if(common.checkRequired(currentUser.lastAgendaSentDate)){
        dateMin = moment(currentUser.lastAgendaSentDate)
    }

    if(checkRequired(currentUser.timezone) && checkRequired(currentUser.timezone.name)){
        dateMin = dateMin.tz(currentUser.timezone.name)
        dateMax = dateMax.tz(currentUser.timezone.name)
    }
    else{
        dateMin.date(dateMin.date()-1);
        dateMax.date(dateMax.date()+1)
    }

    if(!common.checkRequired(currentUser.lastAgendaSentDate)){
        dateMin.hours(0)
        dateMin.minutes(0)
        dateMin.seconds(0)
    }

    dateMax.hours(23)
    dateMax.minutes(59)
    dateMax.seconds(59)

  if(checkRequired(googleAcc)){
      if(checkRequired(googleAcc.refreshToken) && checkRequired(googleAcc.token)){

          var tokenRefresh = new GoogleTokenProvider({
              refresh_token:googleAcc.refreshToken,
              client_id:     authConfig.GOOGLE_CLIENT_ID,
              client_secret: authConfig.GOOGLE_CLIENT_SECRET,
              access_token: googleAcc.token
          });

              tokenRefresh.getToken(function (err, token,parseError,body) {
                  if(parseError == 'parseError'){
                      loggerError.info('Parse error agenda Mail '+currentUser.emailId+' '+body);
                      callback(null)
                  }else
                  if(!checkRequired(err) && checkRequired(token)){

                      googleCalendar.getGoogleEmailInteractions(currentUser._id,token,currentUser.lastLoginDate,true,true,currentUser.emailId,googleAcc.emailId,true,function(){

                          var obj =  {
                              calendarId: 'primary',
                              access_token:token, // You have to supply the OAuth token for the current user
                              singleEvents:true,
                              maxResults:2499,
                              timeMin:dateMin.format(),
                              timeMax:dateMax.format()
                          };

                          googleApI.calendar.events.list(  // calendar event list start
                              obj,
                              function(events) {

                                  if (!checkRequired(events)) {
                                      loggerError.info('no events for '+currentUser.emailId);
                                      callback(null)
                                  }else if(events.error || events.errors){
                                      loggerError.info('error to get events for '+currentUser.emailId);
                                      callback(null)
                                  }else{
                                      loggerError.info('events success '+currentUser.emailId);

                                      googleCalendar.googleMeetingsToRelatas(events.items,true,currentUser.emailId,googleAcc.emailId,true,function(err,res){
                                          callback(events);
                                      });
                                  }
                              }
                          ) // end of calendar event list

                      });
                  }
                  else{
                      loggerError.info('token error '+currentUser.emailId+' stack: '+JSON.stringify(err));
                      callback(null)
                  }
              })

          }else{
          loggerError.info(' no token in acc '+currentUser.emailId);
          callback(null)
       }
  }else{
      loggerError.info(' No cc '+currentUser.emailId);
      callback(null)
  }
}

function getDates(timezone){
    var givenDate = moment().tz(timezone);

    var dateMin = givenDate.clone();     //min date
    var dateMax = givenDate.clone();     //max date

    dateMin.date(dateMin.date()-1)
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    dateMax.date(dateMax.date()+1)
    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(59)
    dateMax.millisecond(59)
    dateMin = dateMin.format()
    dateMax = dateMax.format()
    return {dateMin:dateMin,dateMax:dateMax};
}

function getTimezone(user){
    if(checkRequired(user.timezone) && checkRequired(user.timezone.name)){
        return user.timezone.name;
    }else return null;
}

function userExecutedBefore(events){
    var user = events[0];
    var userTimezone;

    if(checkRequired(user.timezone) && checkRequired(user.timezone.name)){
        userTimezone = user.timezone.name;
    }
    else if(events.length > 1 && events[1] && events[1].timeZone){
        userTimezone = events[1].timeZone;
    }
    userTimezone = userTimezone || 'UTC';

    var dates = getDates(userTimezone);

    meetingClassObj.getPendingToDoItems(user._id.toString(),dates.dateMin,dates.dateMax,'currentDay',function(meetings){

        userExecuted(events,meetings);
    });
}

function userExecuted(events,todoItems){
    var user = events[0];
    user.eventInfo = [];
    var evensIdArr = [];
    var isEvensIdArr = [];
    var eventsArr = [];
    var userTimezone;
    var yesterdayMeetings = [];
    var yesterdayMeetingsIdArr = [];
    var googleMeetingParticipantsArr = [];

    if(checkRequired(user.timezone) && checkRequired(user.timezone.name)){
        userTimezone = user.timezone.name;
    }
    else if(events.length > 1 && events[1] && events[1].timeZone){
        userTimezone = events[1].timeZone;
    }
    userTimezone = userTimezone || 'UTC';

    function isSender(invitation){
        if (invitation.suggested && invitation.suggestedBy) {
            if (invitation.suggestedBy.userId == user._id) {
                return true;
            } else return false;
        }
        else if(invitation.senderId == user._id){
            return true;
        }else return false;
    }

    if(checkRequired(todoItems) && todoItems.length > 0){
        for(var todo=0; todo<todoItems.length; todo++){
            var d2 = moment(new Date()).tz(userTimezone);
            var mDate = moment(todoItems[todo].toDo.dueDate).tz(userTimezone);

            if(mDate.year()+'-'+mDate.month()+'-'+mDate.date() == d2.year()+'-'+d2.month()+'-'+d2.date()){

                var obj = {dateTime:mDate};
                var objNew = {
                    id: todoItems[todo]._id.toString(),
                    invitationId: todoItems[todo].invitationId,
                    senderId: todoItems[todo].senderId,
                    senderName: todoItems[todo].senderName,
                    start:obj,
                    timeZone:userTimezone,
                    meetingObj:todoItems[todo],
                    meetingId:todoItems[todo].invitationId,
                    isMeeting:false,
                    isRequest:false,
                    isTodo:true,
                    summary:todoItems[todo].toDo.actionItem,
                    toDo:todoItems[todo].toDo
                };

                eventsArr.push(objNew);
            }
        }
    }

    var dateMin = moment();
    var dateMax = moment();

    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(59)
    dateMax.millisecond(59)
    dateMin = dateMin.format()
    dateMax = dateMax.format()

    meetingClassObj.newInvitationsByDateTimezone(user._id,dateMin,dateMax,function(invitations){

        console.log("invitations");
        console.log(JSON.stringify(invitations.length));


        var d2 = moment(new Date()).tz(userTimezone);
        var existArr = [];
        var today = new Date();
        today.setSeconds(0, 0);
        var slotFlag = true;
        var todayStr = today.getFullYear()+'-'+today.getMonth()+'-'+today.getDate();
        if(common.checkRequired(invitations)){
            if(invitations.length > 0){
                for(var inv=0; inv<invitations.length; inv++){
                    existArr.push(invitations[inv].invitationId);
                    if(common.checkRequired(invitations[inv]) && common.checkRequired(invitations[inv].scheduleTimeSlots) && invitations[inv].scheduleTimeSlots.length > 0) {
                        slotFlag = true;
                        for (var p = 0; p < invitations[inv].scheduleTimeSlots.length; p++) {

                            var mDate = moment(invitations[inv].scheduleTimeSlots[p].start.date).tz(userTimezone);

                            if (mDate.year() + '-' + mDate.month() + '-' + mDate.date() == d2.year() + '-' + d2.month() + '-' + d2.date() && slotFlag) {
                                invitations[inv].isMeeting = true;
                                invitations[inv].isRequest = true;
                                invitations[inv].isSender = isSender(invitations[inv]);
                                invitations[inv].slotNumber = p;
                                var obj = {
                                    dateTime: mDate
                                }
                                invitations[inv].start = obj;
                                invitations[inv].timeZone = userTimezone;
                                invitations[inv].meetingObj = invitations[inv];
                                invitations[inv].meetingId = invitations[inv].invitationId;
                                eventsArr.push(invitations[inv]);

                            }
                        }
                    }
                }
            }
        }

        for(var i=1; i<events.length; i++){
            if(checkRequired(events[i])){
                if(checkRequired(events[i].items)){
                    if(events[i].items.length > 0){
                        for(var j=0; j<events[i].items.length; j++){
                            if(existArr.indexOf(events[i].items[j].id) == -1){
                                var eStart = new Date(events[i].items[j].start.dateTime || events[i].items[j].start.date);
                                var start = moment(eStart).tz(userTimezone);
                                var d = moment(new Date()).tz(userTimezone);
                                var yesterday = d.clone()
                                yesterday = yesterday.date(yesterday.date()-1);

                                if(start.year()+'-'+start.month()+'-'+start.date() == d.year()+'-'+d.month()+'-'+d.date()){
                                    events[i].items[j].timeZone = userTimezone;
                                    if(events[i].items[j].id.substr(0,12) == 'relatasevent'){
                                        var eId = events[i].items[j].id.substr(12,events[i].items[j].id.length);
                                        events[i].items[j].isEvent = true;
                                        events[i].items[j].isEventId = eId;
                                        isEvensIdArr.push(eId)
                                    }
                                    else if(events[i].items[j].id.substr(0,7) == 'relatas' || events[i].items[j].id.substr(0,5) == 'relat'){
                                        var id = events[i].items[j].id.substr(7,events[i].items[j].id.length);
                                        evensIdArr.push(id);
                                        events[i].items[j].isMeeting = true;
                                        events[i].items[j].meetingId = id;
                                    }
                                    else{
                                        if(checkRequired(events[i].items[j].attendees) && events[i].items[j].attendees.length > 1){
                                            evensIdArr.push(events[i].items[j].id);
                                            events[i].items[j].isMeeting = true;
                                            events[i].items[j].meetingId = events[i].items[j].id;

                                        }
                                    }

                                    if(events[i].items[j].id.substr(0,6) != 'todoid'){
                                        eventsArr.push(events[i].items[j])
                                    }
                                }else
                                if(start.year()+'-'+start.month()+'-'+start.date() == yesterday.year()+'-'+yesterday.month()+'-'+yesterday.date()){
                                    events[i].items[j].timeZone = userTimezone;
                                    if(events[i].items[j].id.substr(0,12) == 'relatasevent'){

                                    }
                                    else if(events[i].items[j].id.substr(0,7) == 'relatas' || events[i].items[j].id.substr(0,5) == 'relat'){
                                        var id2 = events[i].items[j].id.substr(7,events[i].items[j].id.length);
                                        yesterdayMeetingsIdArr.push(id2);
                                        events[i].items[j].isMeeting = true;
                                        events[i].items[j].meetingId = id2;
                                    }

                                    if(events[i].items[j].id.substr(0,6) != 'todoid'){
                                        yesterdayMeetings.push(events[i].items[j])
                                    }
                                }
                            }
                        }
                    }else{
                        loggerError.info('No events for '+user.emailId);
                    }
                }else{
                    loggerError.info('No events for '+user.emailId);
                }
            }else{
                loggerError.info('No events for '+user.emailId);
            }
        }

        eventsArr.sort(function(o1,o2){
            var start1 = new Date(o1.start.dateTime || o1.start.date)
            var start2 = new Date(o2.start.dateTime || o2.start.date)
            return start1 < start2 ? -1 : start1 > start2 ? 1 : 0;
        });

        if(yesterdayMeetings.length > 0){
            yesterdayMeetings.sort(function(o1,o2){
                var start1 = new Date(o1.start.dateTime || o1.start.date)
                var start2 = new Date(o2.start.dateTime || o2.start.date)
                return start1 < start2 ? -1 : start1 > start2 ? 1 : 0;
            });
        }

        var onceFlag = false;
        if(isEvensIdArr.length > 0){
            eventAccess.getAllSubscribedEventsMatched(user._id.toString(),isEvensIdArr,function(arr){
                user.eventInfo = arr;
                nextStepAgenda(user,evensIdArr,eventsArr,yesterdayMeetingsIdArr,yesterdayMeetings,googleMeetingParticipantsArr,userTimezone)
            });
        }else nextStepAgenda(user,evensIdArr,eventsArr,yesterdayMeetingsIdArr,yesterdayMeetings,googleMeetingParticipantsArr,userTimezone)
    })
}

function nextStepAgenda(user,evensIdArr,eventsArr,yesterdayMeetingsIdArr,yesterdayMeetings,googleMeetingParticipantsArr,timezone){

    function addUser2Id(meetingList){
        var id;
        if(meetingList.selfCalendar){
            if(meetingList.senderId == user._id){
                for(var m=0; m<meetingList.toList.length; m++){
                    if(checkRequired(meetingList.toList[m].receiverId)){
                        id = meetingList.toList[m].receiverId
                    }
                }
            }else{
                id = meetingList.senderId;
            }
        }else{
            id = meetingList.senderId == user._id ? meetingList.to.receiverId : meetingList.senderId;
        }
        user2IdArr.push(id);
    }

    console.log("Events array");
    console.log(JSON.stringify(evensIdArr));

    if(checkRequired(evensIdArr[0])){
        var user2IdArr = []
        userManagements.getMeetingsByArrayOfId(evensIdArr,function(error,meetingList){
            if(checkRequired(meetingList)){
                for(var l=0; l<eventsArr.length; l++){
                    for(var k=0; k<meetingList.length; k++){
                        if(eventsArr[l].meetingId == meetingList[k].invitationId){
                            eventsArr[l].meetingObj = meetingList[k];
                            addUser2Id(meetingList[k]);
                        }
                    }
                }
                getGoogleParticipantsProfiles(googleMeetingParticipantsArr,function(parList){
                    getUsers(eventsArr,user2IdArr,user,yesterdayMeetings,parList,timezone);
                })

            }
            else{
                getGoogleParticipantsProfiles(googleMeetingParticipantsArr,function(parList){
                    constructAgendaMailFormat(eventsArr,user,[],yesterdayMeetings,parList,timezone);
                })
            }
        })
    }
    else{
        loggerError.info('No today relatas events for '+user.emailId);
        getGoogleParticipantsProfiles(googleMeetingParticipantsArr,function(parList) {
            constructAgendaMailFormat(eventsArr, user, [], yesterdayMeetings, parList,timezone);
        })
    }
}

function getGoogleParticipantsProfiles(googleMeetingParticipantsArr,callback){
    if(googleMeetingParticipantsArr.length > 0){
        googleMeetingParticipantsArr = common.removeDuplicate_id(googleMeetingParticipantsArr,true);
        userManagements.getUserInfoByArrayOfEmailIdWithCustomFields(googleMeetingParticipantsArr,{contacts:0,google:0,linkedin:0,twitter:0,facebook:0,officeCalendar:0,widgetKey:0,calendarAccess:0,currentLocation:0,birthday:0,locationLatLang:0},function(error,parList){
            if(!error && checkRequired(parList) && parList.length){
                callback(parList);
            }
            else callback([])
        })
    }
    else callback([])
}

function getUsers(eventsArr,user2IdArr,user,yesterdayMeetings,googleMeetingParticipantsArr,timezone){

    userManagements.getUserSocialInfo(user2IdArr,function(error,uList){
        if(!checkRequired(uList)){
            uList = [];
        }
        constructAgendaMailFormat(eventsArr,user,uList,yesterdayMeetings,googleMeetingParticipantsArr,timezone);
    })
}

function constructAgendaMailFormat(eventsArr,user,user2List,yesterdayMeetings,googleMeetingParticipantsArr,timezone){

    //1. We need Meeting loc+temp,
    //2. people at this
    // Today Agenda template is built here

    console.log("Today's Agenda");
    console.log(eventsArr);

    var yesterdayHtmlObj = getYesterdayHtml(yesterdayMeetings);

    var todayMeeting2 = moment(new Date()).tz(timezone).format('D-M-YYYY');
    var todayDate2 = moment(new Date()).tz(timezone).format('D-MMM-YYYY');

    user.todayMeeting = todayMeeting2;
    user.todayDate = todayDate2;
    if(eventsArr.length > 0){

        var totalDuration = 0;
        for(var m=0; m<eventsArr.length; m++){

            if(eventsArr[m].isMeeting) {
                if (checkRequired(eventsArr[m].meetingObj)) {

                    eventsArr[m].meetingObj.scheduleTimeSlots.forEach(function (slot) {

                        var start = moment(slot.start.date).tz(timezone);
                        var end = moment(slot.end.date).tz(timezone);
                        var timedur = '';
                        timedur = end - start;

                        var durationTime = moment.duration(timedur).as('minutes');

                        //floatDuration = parseInt(durationTime);
                        totalDuration +=parseInt(durationTime);
                        });
                }
            }
        }

        //OverAll Snaphot for Today
        var  htmlStrTop = '<table cellpadding="7" style="width: 100%;border-collapse: collapse;background-color: #f9f9f9;">';

        //Table Head
        htmlStrTop += '<tr style="border: 1px solid #c3c3c3;font-weight: 600;line-height: 13px;">';

        htmlStrTop += '<td style="border: 1px solid #c3c3c3;"> Today';
        htmlStrTop += '</td>';

        htmlStrTop += '<td style="border: 1px solid #c3c3c3;"> Meeting Hours';
        htmlStrTop += '</td>';

        htmlStrTop += '<td style="border: 1px solid #c3c3c3;"> Tomorrow';
        htmlStrTop += '</td>';

        htmlStrTop += '<td style="border: 1px solid #c3c3c3;"> Mails To Respond';
        htmlStrTop += '</td>';

        htmlStrTop += '<td style="border: 1px solid #c3c3c3;"> Mails Avaiting Response';
        htmlStrTop += '</td>';

        htmlStrTop += '<td style="border: 1px solid #c3c3c3;"> Potential People to Meet';
        htmlStrTop += '</td>';

        htmlStrTop += '</tr>';

        //End Table Head

        //Table Body Start

        //Col One
        htmlStrTop += '<tr style="line-height: 13px;">';
        htmlStrTop += '<td style="border: 1px solid #c3c3c3;">';
        htmlStrTop += '<span style="color: #666666">' + totalDuration + '</span>';
        htmlStrTop +='</td></tr>'
        //Col Teo
        htmlStrTop += '<tr style="line-height: 13px;">';
        htmlStrTop += '<td style="border: 1px solid #c3c3c3;">';
        htmlStrTop += '<span style="color: #666666">' + totalDuration + '</span>';
        htmlStrTop +='</td></tr>'
        //Col Three
        htmlStrTop += '<tr style="line-height: 13px;">';
        htmlStrTop += '<td style="border: 1px solid #c3c3c3;">';
        htmlStrTop += '<span style="color: #666666">' + totalDuration + '</span>';
        htmlStrTop +='</td></tr>'
        //Col Four
        htmlStrTop += '<tr style="line-height: 13px;">';
        htmlStrTop += '<td style="border: 1px solid #c3c3c3;">';
        htmlStrTop += '<span style="color: #666666">' + totalDuration + '</span>';
        htmlStrTop +='</td></tr>'
        //Col Five
        htmlStrTop += '<tr style="line-height: 13px;">';
        htmlStrTop += '<td style="border: 1px solid #c3c3c3;">';
        htmlStrTop += '<span style="color: #666666">' + totalDuration + '</span>';
        htmlStrTop +='</td></tr>'
        //Col Six
        htmlStrTop += '<tr style="line-height: 13px;">';
        htmlStrTop += '<td style="border: 1px solid #c3c3c3;">';
        htmlStrTop += '<span style="color: #666666">' + totalDuration + '</span>';
        htmlStrTop +='</td></tr>'

        //Table Body end

        htmlStrTop +='</table>';

        //End Overall snapshot for Today

        var  htmlStr = '<table cellpadding="7" style="width: 100%;border-collapse: collapse;background-color: #f9f9f9;">';
        var agendaFlag = false;

        htmlStr += '<tr style="border: 1px solid #c3c3c3;font-weight: 600;line-height: 13px;">';

        htmlStr += '<td style="border: 1px solid #c3c3c3;"> #';
        htmlStr += '</td>';

        htmlStr += '<td style="border: 1px solid #c3c3c3;"> Time';
        htmlStr += '</td>';

        htmlStr += '<td style="border: 1px solid #c3c3c3;"> Description';
        htmlStr += '</td>';

        htmlStr += '<td style="border: 1px solid #c3c3c3;"> Duration';
        htmlStr += '</td>';

        htmlStr += '<td style="border: 1px solid #c3c3c3;"> Location';
        htmlStr += '</td>';

        htmlStr += '<td style="border: 1px solid #c3c3c3;"> With';
        htmlStr += '</td>';

        htmlStr += '<td style="border: 1px solid #c3c3c3;"> Accepted';
        htmlStr += '</td>';

        htmlStr += '<td style="border: 1px solid #c3c3c3;"> # Common Connections';
        htmlStr += '</td>';

        htmlStr += '</tr>';

        for(var m=0; m<eventsArr.length; m++){

            if(eventsArr[m].isMeeting) {

                var userId1 = eventsArr[m].meetingObj.senderId;
                var userId2 = eventsArr[m].meetingObj.toList[0].receiverId;

                contactObj.getCommonContacts(userId1,userId2,false,function(connections){
                    console.log("Common Connections");
                    console.log(JSON.stringify(connections));
                });

                if (checkRequired(eventsArr[m].meetingObj)) {

                    agendaFlag = true;
                    var m1 = m;

                    var isAccepted = "Not Accepted";
                    if(eventsArr[m].meetingObj.toList[0].isAccepted){
                        isAccepted = "Accepted";
                    }

                    if(eventsArr[m].isRequest){
                        eventsArr[m].meetingObj.scheduleTimeSlots[eventsArr[m].slotNumber].isAccepted = true;
                    }

                eventsArr[m].meetingObj.scheduleTimeSlots.forEach(function (slot) {

                    //if (slot.isAccepted == true) {
                        var user2 = ''
                        var start = moment(slot.start.date).tz(timezone);
                        var end = moment(slot.end.date).tz(timezone);
                        var todayMeeting = moment(new Date()).tz(timezone).format('D-M-YYYY');
                        var todayDate = moment(new Date()).tz(timezone).format('D-MMM-YYYY');
                        user.todayMeeting = todayMeeting;
                        user.todayDate = todayDate;
                        var timedur = '';
                        timedur = end-start;

                        var dateStr = start.format('h:mm A') + ' - ' + end.format('h:mm A');

                        var durationTime = moment.duration(timedur).as('minutes');

                        var title = slot.title || '';
                        var location = slot.location || '';
                        var meetingNumber = m+1;

                        if(eventsArr[m].meetingObj.suggested){
                           location = checkRequired(slot.suggestedLocation) ? slot.suggestedLocation : slot.location;
                        }

                        var linkedinUrl = domainName + '/today/details/' + eventsArr[m].meetingObj.invitationId + '/linkedin';
                        var facebookUrl = domainName + '/today/details/' + eventsArr[m].meetingObj.invitationId + '/facebook';
                        var twitterUrl = domainName + '/today/details/' + eventsArr[m].meetingObj.invitationId + '/twitter';
                        var meetingUrl = domainName + '/today/details/' + eventsArr[m].meetingObj.invitationId;
                        var acceptUrl = domainName + '/today/details/' + eventsArr[m].meetingObj.invitationId;
                        var interactionUrl, profileImage,emailP;

                        if (eventsArr[m].meetingObj.senderId == user._id) {
                            if(eventsArr[m].meetingObj.selfCalendar){
                                var id;
                                for(var n=0; n<eventsArr[m].meetingObj.toList.length; n++){
                                    if(checkRequired(eventsArr[m].meetingObj.toList[n].receiverId)){
                                        id = eventsArr[m].meetingObj.toList[n].receiverId
                                    }
                                    emailP = eventsArr[m].meetingObj.toList[n].receiverEmailId;
                                }
                                interactionUrl = domainName + '/interactions/summary/' + id;
                                profileImage = domainName + '/getImage/' +id;
                                for(var w=0; w<user2List.length; w++){
                                    if(user2List[w]._id == id){
                                        user2 = user2List[w];
                                    }
                                }
                            }else{
                                interactionUrl = domainName + '/interactions/summary/' + eventsArr[m].meetingObj.to.receiverId;
                                profileImage = domainName + '/getImage/' + eventsArr[m].meetingObj.to.receiverId;
                                emailP = eventsArr[m].meetingObj.to.receiverEmailId;
                                for(var u=0; u<user2List.length; u++){
                                    if(user2List[u]._id == eventsArr[m].meetingObj.to.receiverId){
                                        user2 = user2List[u];
                                    }
                                }
                            }
                        }else {
                            interactionUrl = domainName + '/interactions/summary/' + eventsArr[m].meetingObj.senderId;
                            profileImage = domainName + '/getImage/' + eventsArr[m].meetingObj.senderId;
                            emailP = eventsArr[m].meetingObj.senderEmailId;
                            for(var v=0; v<user2List.length; v++){
                                if(user2List[v]._id == eventsArr[m].meetingObj.senderId){
                                    user2 = user2List[v];
                                }
                            }
                        }

                        htmlStr += '<tr style="line-height: 13px;">';

                        htmlStr += '<td style="border: 1px solid #c3c3c3;">';
                        htmlStr += '<span style="color: #666666">' + meetingNumber + '</span>';
                        htmlStr +='</td>'

                        //11
                        htmlStr += '<td style="border: 1px solid #c3c3c3;">';
                        htmlStr += '<span style="color: #666666">' + dateStr + '</span>';
                        htmlStr +='</td>'

                        //10
                        htmlStr += '<td style="border: 1px solid #c3c3c3;">';

                        if(eventsArr[m].isSender == true){

                            htmlStr += '<table><tr><td></td><td><a style="color: #666666; text-decoration: none" href=' + meetingUrl + '>' + getTextLength(title, 9) + '</a></td></tr></table>';
                        }else if(eventsArr[m].isSender == false){

                            htmlStr += '<table><tr><td></td><td><a style="color: #666666; text-decoration: none" href=' + meetingUrl + '>' + getTextLength(title, 10) + '</a></td></tr></table>';
                        }else  htmlStr += '<a style="color: #666666; text-decoration: none" href=' + meetingUrl + '>' + getTextLength(title, 15) + '</a>';
                        htmlStr +='</td>';

                        //9
                        //htmlStr += '<td style="border: 1px solid #c3c3c3;">';
                        //switch (slot.locationType) {
                        //    case 'In-Person':
                        //        htmlStr += '<img style="margin-top: 10%" src=' + icon_location + '>';
                        //        break;
                        //    case 'Phone':
                        //        htmlStr += '<img style="margin-top: 10%" src=' + icon_phone + '>';
                        //        break;
                        //    case 'Skype':
                        //        htmlStr += '<img style="margin-top: 10%" src=' + icon_skype + '>';
                        //        break;
                        //    case 'Conference Call':
                        //        htmlStr += '<img style="margin-top: 10%" src=' + icon_conf + '>';
                        //        break;
                        //}
                        //htmlStr += ' </td>';

                        htmlStr += '<td style="border: 1px solid #c3c3c3;">' + durationTime + '</td>';

                        //8
                        htmlStr += '<td style="border: 1px solid #c3c3c3;">';
                        htmlStr += '<span style="color: #666666" title=' + location.replace(/\s/g, '&nbsp;') + '>' + getTextLength(location, 15) + '</span>';
                        htmlStr += '</td>';

                        //7
                        htmlStr += '<td style="border: 1px solid #c3c3c3;">';
                        if(checkRequired(user2)){
                            var name = user2.firstName+' '+user2.lastName;

                            if(eventsArr[m].meetingObj.selfCalendar){
                                htmlStr += '<a href='+common.getValidUniqueUrl(user2.publicProfileUrl)+'><img style="width:30px;height:30px;border-radius: 25px" src=' + profileImage + ' title='+name.replace(/\s/g, '&nbsp;')+'></a>';
                            }else
                            htmlStr += '<a href='+common.getValidUniqueUrl(user2.publicProfileUrl)+'><img style="width:30px;height:30px;border-radius: 25px" src=' + profileImage + ' title='+name.replace(/\s/g, '&nbsp;')+'></a>';
                        }else{
                            if(eventsArr[m].meetingObj.selfCalendar) {
                                htmlStr += '<a><img style="width:30px;height:30px;border-radius: 25px" src="http://relatas.com/images/default.png" title='+emailP+'></a>';
                            }else htmlStr += '<a><img style="width:30px;height:30px;border-radius: 25px" src="http://relatas.com/images/default.png" title='+emailP+'></a>';
                        }
                        htmlStr += '</td>';

                        htmlStr +='<td style="border: 1px solid #c3c3c3;">'+isAccepted;
                        htmlStr +='</td>';

                        //6
                        htmlStr += '<td style="border: 1px solid #c3c3c3;">';
                            if(eventsArr[m].meetingObj.selfCalendar && eventsArr[m].meetingObj.toList.length > 1){
                                htmlStr += '<a href='+meetingUrl+'>+</a>';
                            }

                        htmlStr += '</td>';
                        var prepare = domainName+'/interactions/profileIntelligence';
                        //5 document

                        htmlStr += '</tr>';

                    //}    console.log("Not Accepted");

                })
            }else{}
            }else{

                agendaFlag = true;
                var start = moment(eventsArr[m].start.dateTime || eventsArr[m].start.date).tz(timezone);
                var end;
                var dateStr;
                if(eventsArr[m].isTodo){
                    dateStr = start.format('h:mm A');
                }
                else{
                   end = moment(eventsArr[m].end.dateTime || eventsArr[m].end.date).tz(timezone);
                    dateStr = start.format('h:mm A')+' - '+end.format('h:mm A');
                }

                var user2P;
                var isParticipants = false;
                var isMoreParticipants = false;
                var pEmailId;
                if(googleMeetingParticipantsArr.length > 0){
                    for(var g=0; g<googleMeetingParticipantsArr.length; g++){
                        if(googleMeetingParticipantsArr[g]._id != user._id){
                            user2P = googleMeetingParticipantsArr[g];
                        }
                    }
                }

                if(checkRequired(eventsArr[m].attendees) && eventsArr[m].attendees.length > 1){
                   if(eventsArr[m].attendees.length > 2){
                      isMoreParticipants = true;
                      isParticipants = true;
                   }else isParticipants = true;
                   for(var z=0; z<eventsArr[m].attendees.length; z++){
                        if(eventsArr[m].attendees[z].email != user.emailId){
                            pEmailId = eventsArr[m].attendees[z].email;
                        }
                   }
                }

                var todayMeeting = moment(new Date()).tz(timezone).format('D-M-YYYY');
                var todayDate = moment(new Date()).tz(timezone).format('D-MMM-YYYY');
                user.todayMeeting = todayMeeting;
                user.todayDate = todayDate;
                var title = eventsArr[m].summary || 'No Title';
                var location = eventsArr[m].location || '';
                var meetingUrl ='';
                if(eventsArr[m].isEvent){
                    meetingUrl = domainName+'/event/'+eventsArr[m].id.substr(12,eventsArr[m].id.length);
                }

                var iId = eventsArr[m].id.substr(12,eventsArr[m].id.length);

                htmlStr +='<tr>';

                //12
                htmlStr +='<td style="width: 4%;border-bottom: 1px solid #c3c3c3;">';
                if(eventsArr[m].isTodo){
                    htmlStr +='<img style="height: 25px" src='+icon_relatas+'>';
                }else
                if(eventsArr[m].isEvent){
                    htmlStr +='<img style="height: 25px" src='+icon_relatas+'>';
                }else{
                    htmlStr +='<img style="width: 30px" src='+social_g+'>';
                }
                htmlStr +='</td>';

                //11
                htmlStr +='<td style="width: 23%;border-bottom: 1px solid #c3c3c3;">';
                htmlStr +='<span style="color: #666666">'+dateStr+'</span>';
                htmlStr +='</td>';

                //10
                htmlStr +='<td style="width: 23%;border-bottom: 1px solid #c3c3c3;">';
                var flag = false;
                var cUrl = meetingUrl;
                var facebookUrl,linkedinUrl,twitterUrl;
                if(user.eventInfo.length > 0){
                    for(var o=0; o<user.eventInfo.length; o++){

                        if(user.eventInfo[o].eventId == iId && !user.eventInfo[o].accepted){
                            cUrl = domainName+'/event/addToCalendar/'+iId+'/'+user._id;
                            var url = domainName+'/event/'+iId;
                            flag = true;

                            if(checkRequired(user.eventInfo[o].event)){
                                facebookUrl = "https://www.facebook.com/sharer/sharer.php?"+querystring.stringify({u:url});
                                linkedinUrl = "http://www.linkedin.com/shareArticle?mini=true&"+querystring.stringify({url:url})+"&"+querystring.stringify({title:user.eventInfo[o].event.eventName})+"&"+querystring.stringify({summary:user.eventInfo[o].event.speakerDesignation || user.eventInfo[o].event.eventName})+"&source=Relatas"
                                twitterUrl = "http://twitter.com/share?"+querystring.stringify({text:'Checkout this event '+user.eventInfo[o].event.eventName})+":&"+querystring.stringify({url:url})
                            }
                        }
                    }
                }

                if(eventsArr[m].isTodo){
                    var aaa = domainName+'/today/details/'+eventsArr[m].invitationId
                    htmlStr +='<a style="color: #666666; text-decoration: none" href='+aaa+'><img style="width:20px" src='+meeting_todo+'><span style="vertical-align: super;">'+getTextLength(title,15)+'</span></a>';
                }else
                if(eventsArr[m].isEvent && flag){
                    htmlStr +='<table><tr><td><img src='+event_new+'></td><td><a href=' + cUrl + '><img src='+rsvp+'></a></td><td><a style="color: #666666; text-decoration: none" href='+meetingUrl+'>'+getTextLength(title,9)+'</a></td></tr></table>';
                }else
                if(checkRequired(meetingUrl)){
                    htmlStr +='<a style="color: #666666; text-decoration: none" href='+meetingUrl+'><img src='+event_new+'><span style="vertical-align: super;">'+getTextLength(title,15)+'</span></a>';
                }else{
                    htmlStr +='<a style="color: #666666; text-decoration: none" >'+getTextLength(title,15)+'</a>';
                }
                htmlStr += '</td>';

                if(checkRequired(location)){
                    //9
                    htmlStr += '<td style="width: 2%;border-bottom: 1px solid #c3c3c3;">';
                    htmlStr += '<img style="margin-top: 10%" src='+icon_location+'>';
                    htmlStr += ' </td>';

                    //8
                    htmlStr +='<td style="width: 21%;border-bottom: 1px solid #c3c3c3;">';
                    htmlStr +='<span style="color: #666666" title='+location.replace(/\s/g,'&nbsp;')+'>'+getTextLength(location,15)+'</span>';
                    htmlStr +='</td>';
                }
                else{
                    htmlStr += '<td style="width: 2%;border-bottom: 1px solid #c3c3c3;">';
                    htmlStr += ' </td>';
                    htmlStr +='<td style="width: 21%;border-bottom: 1px solid #c3c3c3;">';
                    htmlStr +='</td>';
                }

                //7
                htmlStr += '<td style="width: 5%;border-bottom: 1px solid #c3c3c3;">';
                if(isParticipants && checkRequired(user2P)){
                   var name = user2P.firstName+' '+user2P.lastName;
                   var profileImage = domainName + '/getImage/' +user2P._id;
                   htmlStr += '<a href='+common.getValidUniqueUrl(user2P.publicProfileUrl)+'><img style="width:30px;height:30px;border-radius: 25px" src=' + profileImage + ' title='+name.replace(/\s/g, '&nbsp;')+'></a>';

                }else if(isParticipants){

                   htmlStr += '<a><img style="width:30px;height:30px;border-radius: 25px" src="http://relatas.com/images/default.png" title='+pEmailId+'></a>';

                }
                htmlStr += '</td>';

                //6
                htmlStr += '<td style="width: 2%;border-bottom: 1px solid #c3c3c3;">';
                if(isMoreParticipants){
                    htmlStr += '<a>+</a>';
                }
                htmlStr += '</td>';

                //5
                htmlStr += '<td style="width: 2%;border-bottom: 1px solid #c3c3c3;">';
                if(checkRequired(linkedinUrl) && eventsArr[m].isEvent)
                       htmlStr += '<span style="color: #666666">Share:</span> '

                htmlStr += '</td>';

                //4
                htmlStr += '<td style="width: 3%;border-bottom: 1px solid #c3c3c3;">';
                 if(checkRequired(linkedinUrl) && eventsArr[m].isEvent)
                       htmlStr += '<a href=' + linkedinUrl + '><img src=' + icon_linkedin + '></a>';

                htmlStr += '</td>';

                //3
                htmlStr += '<td style="width: 3%;border-bottom: 1px solid #c3c3c3;">';
                  if(checkRequired(twitterUrl) && eventsArr[m].isEvent)
                   htmlStr += '<a href=' + twitterUrl + '><img src=' + icon_twitter + '></a>';

                htmlStr += '</td>';

                //2
                htmlStr += '<td style="width: 3%;border-bottom: 1px solid #c3c3c3;">';
                   if(checkRequired(facebookUrl) && eventsArr[m].isEvent)
                     htmlStr += '<a href=' + facebookUrl + '><img src=' + icon_facebook + '></a>';

                htmlStr += '</td>';

                //1
                htmlStr +='<td style="width: 3%;border-bottom: 1px solid #c3c3c3;">';
                htmlStr +='</td>';

                htmlStr +='<td style="width: 3%;border-bottom: 1px solid #c3c3c3;">'+isAccepted;
                htmlStr +='</td>';

                htmlStr +='</tr>';

            }
        }
        htmlStr +='</table>';
        if(agendaFlag){
            var yesterdayAgenda = getYesterdayTable(yesterdayHtmlObj);
          var todayAgenda = "<table width='89.44%' border='0' align='center' cellspacing='0' style='font-size: 12px;'>"+
              "<tr>"+
                 "<td rowspan='2'>"+htmlStr+"</td>"+
              "</tr>"+
            "</table>";

            getPastSevenDaysInteractionsStatus(user,yesterdayAgenda,todayAgenda,timezone);
        }
    }
    else{

        var yesterdayAgenda2 = getYesterdayTable(yesterdayHtmlObj);
        getPastSevenDaysInteractionsStatus(user,yesterdayAgenda2,'',timezone);

    }
}

function getYesterdayTable(yesterdayHtmlObj){
    if(checkRequired(yesterdayHtmlObj.htmlStr)){
        return "<table width='100%' border='0' cellspacing='0'>"+
            "<tr>"+
        "<td colspan='2'><table style='margin-bottom: -2%'><tr><td><h2 class='content-title' style='color: #000000'><b>Take action - yesterday's meetings &nbsp;&nbsp;&nbsp;</b></h2></td></tr></table></td>"+
        "</tr>"+
        "<tr>"+
        "<td rowspan='2'>"+yesterdayHtmlObj.htmlStr+ "</td>"+
        "</tr>"+
        "</table>";
    }
    else return '';
}

function generateParameters(user,htmlStr){

  var today = user.todayMeeting;
  var date = user.todayDate;
    var params = {
        "template_name": "R_noreply_DailyAgendaMail",
        "template_content": [
            {
                "name": "today",
                "content": "example content"
            }
        ],
        "message": {
            "subject":"Relatas: Agenda for "+date,
            "from_email": "no-reply@relatas.co.in",
            "from_name":"Relatas",
            "to": [
                {
                    "email": user.emailId,
                    "type": "to"
                }
            ],
            "important": false,
            "track_opens": null,
            "track_clicks": null,
            "auto_text": null,
            "auto_html": null,
            "inline_css": null,
            "url_strip_qs": null,
            "preserve_recipients": null,
            "view_content_link": null,

            "tracking_domain": null,
            "signing_domain": null,
            "return_path_domain": null,
            "merge": true,
            "global_merge_vars": [
                {
                    "name": "var1",
                    "content": "Global Value 1"
                }
            ],
            "merge_vars": [
                {
                    "rcpt": user.emailId,
                    "vars": [
                        {
                            "name": "name",
                            "content": user.firstName
                        },
                        {
                            "name": "profilePic",
                            "content": user.profilePicUrl
                        },
                        {
                            "name": "today",
                            "content": date
                        },
                        {
                            "name": "date",
                            "content": date
                        },
                        {
                            "name": "agendaTable",
                            "content": htmlStr
                        }
                    ]
                }
            ]
        }
    }

    return params;
}


function getYesterdayHtml(yesterdayMeetings) {
    var timezone;
    if (yesterdayMeetings.length > 0) {
        var htmlStr = '<table cellspacing="0" style="width: 100%;border-top: 1px solid #c3c3c3;background-color: #f9f9f9;">';
        var agendaFlag = false;
        var todayDate = moment(new Date()).tz(yesterdayMeetings[0].timeZone);
        todayDate = todayDate.date(todayDate.date()-1);
        todayDate = todayDate.format('D-MMM-YYYY');
        timezone = yesterdayMeetings[0].timeZone;
        for (var m = 0; m < yesterdayMeetings.length; m++) {

            agendaFlag = true;
            var start = moment(yesterdayMeetings[m].start.dateTime || yesterdayMeetings[m].start.date).tz(yesterdayMeetings[m].timeZone);
            var end = moment(yesterdayMeetings[m].end.dateTime || yesterdayMeetings[m].end.date).tz(yesterdayMeetings[m].timeZone);
            var dateStr = start.format('h:mm A') + ' - ' + end.format('h:mm A');

            var title = yesterdayMeetings[m].summary || '';
            var location = yesterdayMeetings[m].location || '';
            var id2 = yesterdayMeetings[m].id.substr(7, yesterdayMeetings[m].id.length);
            var meetingUrl = domainName + '/today/details/' + id2;
            var meetingUrlToDo = domainName + '/today/details/' + id2+'?open=todo';
            var meetingUrlMail = domainName + '/today/details/' + id2+'?open=mail';

            htmlStr += '<tr>';
            //12
            htmlStr += '<td style="width: 4%;border-bottom: 1px solid #c3c3c3;">';
            if (yesterdayMeetings[m].isMeeting) {
                htmlStr += '<img style="height: 25px" src=' + icon_relatas + '>';
            }
            else htmlStr += '<img style="width: 30px" src=' + social_g + '>';

            htmlStr += '</td>';

            //11
            htmlStr += '<td style="width: 25%;border-bottom: 1px solid #c3c3c3;">';
            htmlStr += '<span style="color: #666666">' + dateStr + '</span>';
            htmlStr += '</td>';

            //10
            htmlStr += '<td style="width: 25%;border-bottom: 1px solid #c3c3c3;">';
            if (yesterdayMeetings[m].isMeeting) {
                htmlStr += '<a style="color: #666666; text-decoration: none" href=' + meetingUrl + '>' + getTextLength(title, 15) + '</a>';
            }
            else htmlStr += '<a style="color: #666666; text-decoration: none">' + getTextLength(title, 15) + '</a>';
            htmlStr += '</td>';

            //7 profile image // participants
            htmlStr += '<td style="width: 20%;border-bottom: 1px solid #c3c3c3;">';
            if (yesterdayMeetings[m].isMeeting) {
                htmlStr += '<a href='+meetingUrlToDo+' style="color:#ffffff!important;border:2px solid #03a2ea;background-color:#03a2ea;font-family:helvetica neue,arial,helvetica,verdana,sans-serif;font-size:14px;line-height:22.5px;border-radius:8px;min-height:40px;text-align:center;text-decoration:none" target="_blank">&nbsp;ToDo&nbsp;</a>'
            }
            htmlStr += '</td>';

            //6 more participants symbol
            htmlStr += '<td style="width: 20%;border-bottom: 1px solid #c3c3c3;">';
            if (yesterdayMeetings[m].isMeeting) {
                htmlStr += '<a href='+meetingUrlMail+' style="color:#ffffff!important;border:2px solid #03a2ea;background-color:#03a2ea;font-family:helvetica neue,arial,helvetica,verdana,sans-serif;font-size:14px;line-height:22.5px;border-radius:8px;min-height:40px;text-align:center;text-decoration:none" target="_blank">&nbsp;Email&nbsp;</a>'
            }
            htmlStr += '</td>';

            htmlStr += '</tr>';

        }
        htmlStr += '</table>';
        return {
            htmlStr:htmlStr,
            yesterday:todayDate,
            timezone:timezone
        };
    }
    else {
        return {
            htmlStr:'',
            yesterday:'',
            timezone:null
        };
    }
}

function getPastSevenDaysInteractionsStatus(user,yesterdayAgenda,todayAgenda,timezone){
    var end = moment().tz(timezone || user.timezone);

    end.hours(23);
    end.minutes(59);
    end.seconds(59);
    end.milliseconds(0);

    var start = end.clone();
    start.date(start.date() - 7);
    start.hours(0);
    start.minutes(0);
    start.seconds(0);
    start.milliseconds(0);

    start = start.format();
    end = end.format();
    interaction.pastSevenDayInteractionStatus(user._id.toString(),user.emailId,{start:start,end:end},false,function(obj){

        if(common.checkRequired(obj) && (obj.youNotReplied.length > 0 || obj.othersNotReplied.length > 0)){
            var url1 = domainName+'/interactions/pastSevenDays/status?open=left';
            var url2 = domainName+'/interactions/pastSevenDays/status?open=right';
            var sevenDays = "<table style='width: 100%' border='0' cellspacing='0'>"+
                "<tr>"+
                "<td colspan='2'><p style='text-align: justify; font-family:helvetica neue,arial,helvetica,verdana,sans-serif; font-size:15px; line-height:22.5px'><b><h2 class='content-title' style='color: #000000'>Take action - past 7 days</h2></b></p></td>"+
                "</tr>"+
                "<tr>"+
                "<td>" +
                    "<table cellspacing='0' style='width: 100%;border-top: 1px solid #c3c3c3;background-color: #f9f9f9;'><tr>"+
                        "<td style='border-bottom: 1px solid #c3c3c3;'>"+
                            "<div style='height: 30px'>&nbsp;</div>"+
                        "</td>"+
                        "<td style='border-bottom: 1px solid #c3c3c3;'>"+
                            "<span style='color:#666666;text-decoration:none'>&nbsp;&nbsp; Interactions you haven't responded to: <a href="+url1+">"+obj.youNotReplied.length+"</a></span>" +
                        "</td>"+
                        "</tr>"+
                      "<tr>"+
                "<td style='border-bottom: 1px solid #c3c3c3;'>"+
                     "<div style='height: 30px'>&nbsp;</div>"+
                "</td>"+
                "<td style='border-bottom: 1px solid #c3c3c3;'>"+
                    "<span style='color:#666666;text-decoration:none'>&nbsp;&nbsp; Interactions others have not responded: <a href="+url2+">"+obj.othersNotReplied.length+"</a></span>"+
                "</td></tr></table>"+

                "</td>"+
                "<td>&nbsp;</td>"+
                "</tr>"+
                "</table>";

            todayAgenda = getTodayAgendaFinal(todayAgenda,user.todayDate);
            yesterdayAgenda = getYesterdayAgendaFinal(yesterdayAgenda);

            var agenda = todayAgenda+sevenDays+yesterdayAgenda;
            var params = generateParameters(user,agenda);
            loggerError.info('sending agenda to '+user.emailId);
            userManagements.updateLastAgendaSentDate(user._id,new Date());
            emailSenders.sendDailyAgenda(params);
        }
        else{
            if(checkRequired(todayAgenda) || checkRequired(yesterdayAgenda)){
                todayAgenda = getTodayAgendaFinal(todayAgenda,user.todayDate);
                yesterdayAgenda = getYesterdayAgendaFinal(yesterdayAgenda);
                var sevenDay = getPastSevenDaysFinal();

                var agenda2 = todayAgenda+sevenDay+yesterdayAgenda;
                var params2 = generateParameters(user,agenda2);
                loggerError.info('sending agenda to '+user.emailId);
                userManagements.updateLastAgendaSentDate(user._id,new Date());
                emailSenders.sendDailyAgenda(params2);
            }
        }
    });
}

function getTodayAgendaFinal(agenda,todayDate){
    if(checkRequired(agenda)){
        return agenda;
    }
    else{
        return "<table width='100%' border='0' cellspacing='0'>"+
            "<tr>"+
            "<td colspan='2'><p style='text-align: justify; font-family:helvetica neue,arial,helvetica,verdana,sans-serif; font-size:15px; line-height:22.5px'><b><h2 class='content-title' style='color: #000000'>Today's agenda ("+todayDate+")</h2></b></p></td>"+
            "</tr>"+
            "<tr>"+
            "<td rowspan='2'>" +

            "<table cellspacing='0' style='width: 100%;border-top: 1px solid #c3c3c3;background-color: #f9f9f9;'><tr>"+
            "<td style='border-bottom: 1px solid #c3c3c3;'>"+
            "<div style='height: 30px'>&nbsp;</div>"+
            "</td>"+
            "<td style='border-bottom: 1px solid #c3c3c3;'>"+
            "<span style='color:#666666;text-decoration:none'>&nbsp;&nbsp; No meetings scheduled for today</span>" +
            "</td>"+
            "</tr>"+
            "</table>"+

            "</td>"+
            "</tr>"+
            "</table>";
    }
}

function getYesterdayAgendaFinal(agenda){
    if(checkRequired(agenda)){
        return agenda;
    }
    else{
        return "<table width='100%' border='0' cellspacing='0'>"+
            "<tr>"+
            "<td colspan='2'><table style='margin-bottom: -2%'><tr><td><h2 class='content-title' style='color: #000000'><b>Take action - yesterday's meetings &nbsp;&nbsp;&nbsp;</b></h2></td></tr></table></td>"+
            "</tr>"+
            "<tr>"+
            "<td rowspan='2'>" +

            "<table cellspacing='0' style='width: 100%;border-top: 1px solid #c3c3c3;background-color: #f9f9f9;'><tr>"+
            "<td style='border-bottom: 1px solid #c3c3c3;'>"+
            "<div style='height: 30px'>&nbsp;</div>"+
            "</td>"+
            "<td style='border-bottom: 1px solid #c3c3c3;'>"+
            "<span style='color:#666666;text-decoration:none'>&nbsp;&nbsp; No meetings took place yesterday</span>" +
            "</td>"+
            "</tr>"+
            "</table>"+

            "</td>"+
            "</tr>"+
            "</table>";
    }
}

function getPastSevenDaysFinal(){
    return "<table style='width: 100%' border='0' cellspacing='0'>"+
        "<tr>"+
        "<td colspan='2'><p style='text-align: justify; font-family:helvetica neue,arial,helvetica,verdana,sans-serif; font-size:15px; line-height:22.5px'><b><h2 class='content-title' style='color: #000000'>Take action - past 7 days</h2></b></p></td>"+
        "</tr>"+
        "<tr>"+
        "<td>" +

        "<table cellspacing='0' style='width: 100%;border-top: 1px solid #c3c3c3;background-color: #f9f9f9;'><tr>"+
        "<td style='border-bottom: 1px solid #c3c3c3;'>"+
        "<div style='height: 30px'>&nbsp;</div>"+
        "</td>"+
        "<td style='border-bottom: 1px solid #c3c3c3;'>"+
        "<span style='color:#666666;text-decoration:none'>&nbsp;&nbsp; No data for mail sync available</span>" +
        "</td>"+
        "</tr>"+
        "</table>"+

        "</td>"+
        "<td>&nbsp;</td>"+
        "</tr>"+
        "</table>";
}

function getTextLength(text,maxLength){
    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

module.exports = AgendaMails;