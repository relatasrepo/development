//var forecast = require('../node_modules/forecast.io/lib/index');
var request = require('request');

var commonUtility = require('../common/commonUtility');
var appCredentials = require('../config/relatasConfiguration');

var common = new commonUtility();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();

//forecast = new forecast({ APIKey: authConfig.FORECAST_API_KEY});

function Weather(){

    this.getForecast = function(query,callback){

        request.get('http://api.openweathermap.org/data/2.5/forecast/daily?q='+query+'&mode=json&units=metric&cnt=3&APPID='+authConfig.OPEN_WEATHER_MAP_API_KEY,function(err,res,data){
            if(err){
                callback(err,data,query)
            }
            else if(common.checkRequired(data)){
                try{
                    data = JSON.parse(data);
                    if(data.list && data.list.length > 0){
                        for(var i=0; i<data.list.length; i++){
                            if(data.list[i].temp){
                                for(var key in data.list[i].temp){
                                    data.list[i].temp[key] = Math.round(data.list[i].temp[key]);
                                }
                            }
                        }
                    }
                    callback(err,data,query)
                }
                catch (e){
                    callback(e,data,query)
                }
            }
            else{
                callback(err,data,query)
            }
        })
    }
}

function convertCelsius(f){
    return Math.round((f-32)/1.8)
}

module.exports = Weather;