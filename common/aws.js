
var AWS_sdk = require('aws-sdk');

var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var errorClass = require('../errors/errorClass');
var errorMessages = require('../errors/errorMessage');

var logLib = new winstonLog();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var appCredential = new appCredentials();

var logger =logLib.getAWSErrorLogger();
var awsData = appCredential.getAwsCredentials();
AWS_sdk.config.update(awsData);
AWS_sdk.config.region = awsData.region;
var statusCodes = errorMessagesObj.getStatusCodes();

function AWS(){
    this.uploadFileToAWS = function(file,o_fileName,n_fileName,bucket,mimetype,callback){

        var s3bucket = new AWS_sdk.S3({ params: {Bucket: awsData[bucket]} });
        var url = 'https://' + awsData[bucket] + '.s3.amazonaws.com/';
        var params = {Key: n_fileName, ContentType: mimetype, Body: file};

        s3bucket.putObject(params, function(err, data) {
            if (err) {
                logger.info('Uploading file failed ',err,{fileName:o_fileName,mimetype:mimetype,bucket:bucket});
                callback(errorObj.generateErrorResponse({status: statusCodes.FILE_ERROR_CODE, key: 'AWS_UPLOAD_ERROR'}),null);
            }
            else {
                callback(null,{awsRes:data,fileName:o_fileName,fileUrl:url+n_fileName,awsKey:n_fileName})
            }
        });
    }
}

module.exports = AWS;