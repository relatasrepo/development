
var moment = require('moment-timezone');

var appCredentials = require('../config/relatasConfiguration');
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');
var userManagement = require('../dataAccess/userManagementDataAccess');
var interactions = require('../dataAccess/interactionManagement');

var userManagementObj = new userManagement();
var appCredential = new appCredentials();
var common = new commonUtility();
var interactionObj = new interactions();
var logLib = new winstonLog();

var logger =logLib.getTwitterErrorLogger();
var authConfig = appCredential.getAuthCredentials();
var _ = require("lodash");

var Twit = require('twit');

function Twitter(){
    this.getTwitterFeed = function(twitter,needLimited,limit,callback){
        if(this.validateTwitterAccount(twitter)){
            var T = new Twit({
                consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                access_token:        twitter.token,
                access_token_secret:  twitter.refreshToken
            });
            var limitObj = needLimited ? {count:limit} : {};
            T.get('statuses/home_timeline',limitObj, function(err, data, response) {
                if (err) {
                    logger.info('Twitter Feed error ',err)
                    callback('TWITTER_ERROR',[])
                }
                else{
                    if (!common.checkRequired(data) && !common.checkRequired(data[0])) {
                        callback(false,[])
                    }
                    else{
                        if(needLimited){
                            data = data.splice(0, limit || 3);
                        }

                        callback(false,data)
                    }
                }
            })
        } else callback('NO_TWITTER_ACCOUNT',[])
    };

    this.getTwitterFeedHomeTimeline = function(twitter,needLimited,limit,callback){

        if(this.validateTwitterAccount(twitter)){

            var T = new Twit({
                consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                access_token:        twitter.token,
                access_token_secret:  twitter.refreshToken
            });

            var sinceId = common.checkRequired(twitter.sinceId) ? {since_id:twitter.sinceId,count:200} : {count:200};

            T.get('statuses/home_timeline',sinceId, function(err, data, response) {

                if (err) {
                    logger.info('Twitter Feed error ',err);
                    callback('TWITTER_ERROR',[])
                }
                else{

                    if (!common.checkRequired(data) && !common.checkRequired(data[0])) {
                        callback(false,[])
                    }
                    else{
                        if(needLimited){
                            data = data.splice(0, limit || 3);
                        }

                        callback(false,data)
                    }
                }
            })
        } else callback('NO_TWITTER_ACCOUNT',[])
    };

    this.getTwitterFeedCountByDate = function(twitter,needLimited,dateMin,dateMax,callback){
        if(this.validateTwitterAccount(twitter)){
            var T = new Twit({
                consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                access_token:        twitter.token,
                access_token_secret:  twitter.refreshToken
            });

            T.get('statuses/user_timeline', function(err, data, response) {
                if (err) {
                    logger.info('Twitter Feed error ',err)
                    callback('TWITTER_ERROR',0)
                }
                else{
                    if (!common.checkRequired(data) && !common.checkRequired(data[0])) {
                        callback(false,0)
                    }
                    else{

                        callback(false,data.length)
                    }
                }
            })
        } else callback('NO_TWITTER_ACCOUNT',0)
    };

    this.getTwitterFeed_with_screen_name = function(twitter,screenName,needLimited,callback){

        if(this.validateTwitterAccount(twitter)){
            var T = new Twit({
                consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                access_token:        twitter.token,
                access_token_secret:  twitter.refreshToken
            });

            T.get('statuses/user_timeline?user_id='+screenName, function(err, data, response) {

                if (err) {
                    logger.info('Twitter Feed error2 ',JSON.stringify(err))
                    callback('Twitter_Error',[])
                }
                else{
                    if (!common.checkRequired(data) && !common.checkRequired(data[0])) {
                        callback(false,[])
                    }
                    else{
                        if(needLimited){
                            data = data.splice(0, 3);
                        }

                        callback(false,data)
                    }
                }
            })
        } else callback('No_Twitter_Account',[])
    };

    this.getTwitterFeed_Top_Trending_post_B = function(twitter,needLimited,limit,callback){
        if(this.validateTwitterAccount(twitter)){
            var T = new Twit({
                consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                access_token:        twitter.token,
                access_token_secret:  twitter.refreshToken
            });
            var limitObj = needLimited ? {count:limit} : {};
            limitObj.user_id = twitter.id
            T.get('statuses/user_timeline',limitObj, function(err, data, response) {
                if (err) {
                    logger.info('Twitter Feed error ',err)
                    callback('TWITTER_ERROR',[])
                }
                else{
                    if (!common.checkRequired(data) && !common.checkRequired(data[0])) {
                        callback(false,[])
                    }
                    else{
                        if(needLimited){
                            data = data.splice(0, limit || 3);
                        }

                        callback(false,data)
                    }
                }
            })
        } else callback('NO_TWITTER_ACCOUNT',[])
    };

    this.favoriteTwitterFeed = function(twitter,tweetId,callback){
        if(this.validateTwitterAccount(twitter)){
            var T = new Twit({
                consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                access_token:        twitter.token,
                access_token_secret:  twitter.refreshToken
            });

            T.post('/favorites/create',{id:tweetId}, function(err, data, response) {
                if(err){
                    logger.info('Twitter error favoriteTwitterFeed():Twitter ',err);
                    callback('error',false,err);
                }
                else{
                    callback(false,true,null);
                }
            })
        } else callback('no_account',[],null);
    };

    this.reTweetFeed = function(twitter,tweetId,callback){
        if(this.validateTwitterAccount(twitter)){
            var T = new Twit({
                consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                access_token:        twitter.token,
                access_token_secret:  twitter.refreshToken
            });

            T.post('/statuses/retweet/'+tweetId,{id:tweetId}, function(err, data, response) {

                if(err){
                    logger.info('Twitter error reTweetFeed():Twitter ',err);
                    callback('error',false,err);
                }
                else{
                    callback(false,true,null);
                }
            })
        } else callback('no_account',[],null);
    };

    this.replyToTweet = function(twitter,tweetId,status,callback){
        if(this.validateTwitterAccount(twitter)){
            var T = new Twit({
                consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                access_token:        twitter.token,
                access_token_secret:  twitter.refreshToken
            });

            T.post('statuses/update',{status:status,in_reply_to_status_id:tweetId}, function(err, data, response) {
                if(err){
                    logger.info('Twitter error replyToTweet():Twitter ',err);
                    callback('error',false,err);
                }
                else{
                    callback(false,true,null);
                }
            })
        } else callback('no_account',[],null);
    };

    this.getTwitterFriends = function(twitter,callback){
        if(this.validateTwitterAccount(twitter)){
            var T = new Twit({
                consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                access_token:        twitter.token,
                access_token_secret:  twitter.refreshToken
            });

            T.get('friends/list',{include_user_entities:false}, function(err, data, response) {
                if (err) {
                    logger.info('Twitter Feed error ',err);
                    callback('TWITTER_ERROR',[])
                }
                else{
                    if (!common.checkRequired(data) && !common.checkRequired(data[0])) {
                        callback(false,[])
                    }
                    else{
                        callback(false,data)
                    }
                }
            })
        } else callback('NO_TWITTER_ACCOUNT',[])
    };

    this.getTwitterFriendship = function(twitter,screen_name,callback){

        if(this.validateTwitterAccount(twitter)){

            var T = new Twit({
                consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                access_token:        twitter.token,
                access_token_secret:  twitter.refreshToken
            });

            T.get('friendships/lookup',{include_user_entities:false,screen_name:screen_name}, function(err, data, response) {

                if (err) {
                    logger.info('Twitter Feed error ',err);
                    callback('TWITTER_ERROR',null)
                }
                else{
                    if (!common.checkRequired(data)) {
                        callback(false,null)
                    }
                    else{
                        callback(false,data)
                    }
                }
            })
        } else callback('NO_TWITTER_ACCOUNT',[])
    };

    this.validateTwitterAccount = function(twitter){
        if(common.checkRequired(twitter) && common.checkRequired(twitter.token) && common.checkRequired(twitter.refreshToken)){
            return true;
        }
        else return false;
    };

    this.importTwitterFeedToInteractions = function(twitter,user,screen_name_of_contact,emailId_of_contact){
        this.getTwitterFeed(twitter,false,0,function(error,data){

            if(!error){
                var idArr = [];
                var directStoreInteractions = [];
                var otherInteractions = [];
                for(var i=0; i<data.length; i++){

                    if((data[i].user.screen_name == screen_name_of_contact || data[i].in_reply_to_screen_name == screen_name_of_contact) || (common.contains(data[i].text,'@'+screen_name_of_contact)) && data[i].user.id_str == user.twitter.id){
                        if(data[i].user.screen_name == screen_name_of_contact ){
                            var interactionR1 = {};
                            interactionR1.userId = '';
                            interactionR1.emailId = emailId_of_contact;
                            interactionR1.user_twitter_id = data[i].user.id_str;
                            interactionR1.user_twitter_name = screen_name_of_contact;
                            interactionR1.action = 'sender';
                            interactionR1.type = 'twitter';
                            interactionR1.subType = 'tweet';
                            interactionR1.refId = data[i].id_str;
                            interactionR1.source = 'twitter';
                            interactionR1.title = data[i].text || '';
                            interactionR1.interactionDate = new Date(data[i].created_at);
                            //idArr.push(data[i].in_reply_to_user_id_str)
                            otherInteractions.push(interactionR1)

                            var interactionTo = {};
                            interactionTo.userId = user._id;
                            interactionTo.user_twitter_id = user.twitter.id;
                            interactionTo.user_twitter_name = user.twitter.userName;
                            interactionTo.firstName = user.firstName;
                            interactionTo.lastName = user.lastName;
                            interactionTo.publicProfileUrl = user.publicProfileUrl;
                            interactionTo.profilePicUrl = user.profilePicUrl;
                            interactionTo.companyName = user.companyName;
                            interactionTo.designation = user.designation;
                            interactionTo.location = user.location;
                            interactionTo.mobileNumber = user.mobileNumber;
                            interactionTo.action = 'receiver';
                            interactionTo.type = 'twitter';
                            interactionTo.subType = 'tweet';
                            interactionTo.emailId = user.emailId;
                            interactionTo.refId = data[i].id_str;
                            interactionTo.source = 'twitter';
                            interactionTo.title = data[i].text || '';
                            interactionTo.description = '';
                            interactionTo.interactionDate = new Date(data[i].created_at);
                            directStoreInteractions.push(interactionTo);
                        }
                        else{
                            var interaction2 = {};
                            interaction2.userId = user._id;
                            interaction2.user_twitter_id = user.twitter.id;
                            interaction2.user_twitter_name = user.twitter.userName;
                            interaction2.firstName = user.firstName;
                            interaction2.lastName = user.lastName;
                            interaction2.publicProfileUrl = user.publicProfileUrl;
                            interaction2.profilePicUrl = user.profilePicUrl;
                            interaction2.companyName = user.companyName;
                            interaction2.designation = user.designation;
                            interaction2.location = user.location;
                            interaction2.mobileNumber = user.mobileNumber;
                            interaction2.action = 'sender';
                            interaction2.type = 'twitter';
                            interaction2.subType = 'tweet';
                            interaction2.emailId = user.emailId;
                            interaction2.refId = data[i].id_str;
                            interaction2.source = 'twitter';
                            interaction2.title = data[i].text || '';
                            interaction2.description = '';
                            interaction2.interactionDate = new Date(data[i].created_at);
                            directStoreInteractions.push(interaction2);

                            var interaction3 = {};
                            interaction3.userId = '';
                            interaction3.emailId = emailId_of_contact;
                            interaction3.user_twitter_id = '';
                            interaction3.user_twitter_name = screen_name_of_contact;
                            interaction3.action = 'receiver';
                            interaction3.type = 'twitter';
                            interaction3.subType = 'tweet';
                            interaction3.refId = data[i].id_str;
                            interaction3.source = 'twitter';
                            interaction3.title = data[i].text || '';
                            interaction3.interactionDate = new Date(data[i].created_at);
                            //idArr.push(data[i].in_reply_to_user_id_str)
                            otherInteractions.push(interaction3)
                        }
                    }
                }

                if(directStoreInteractions.length > 0 || otherInteractions.length > 0){
                    userManagementObj.findUserProfileByEmailIdWithCustomFields(emailId_of_contact,{emailId:1,mobileNumber:1,designation:1,companyName:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,location:1},function(error,user){
                        if(!error && common.checkRequired(user)){
                            for(var int=0; int<otherInteractions.length; int++){
                                if(otherInteractions[int].emailId == user.emailId){
                                    otherInteractions[int].userId = user._id;
                                    otherInteractions[int].firstName = user.firstName;
                                    otherInteractions[int].lastName = user.lastName;
                                    otherInteractions[int].publicProfileUrl = user.publicProfileUrl;
                                    otherInteractions[int].profilePicUrl = user.profilePicUrl;
                                    otherInteractions[int].companyName = user.companyName;
                                    otherInteractions[int].designation = user.designation;
                                    otherInteractions[int].location = user.location;
                                    otherInteractions[int].mobileNumber = user.mobileNumber;
                                    otherInteractions[int].emailId = user.emailId;
                                }
                            }
                        }
                        directStoreInteractions = directStoreInteractions.concat(otherInteractions);
                        interactionObj.bulkUpdateInteractions(directStoreInteractions,user.emailId,'twitter')
                    });
                }
                else if(directStoreInteractions.length > 0){
                    interactionObj.bulkUpdateInteractions(directStoreInteractions,user.emailId,'twitter')
                }
            }
        })
    };

    this.twitterFeedToInteractions = function(twitter,user,callback){

        this.getTwitterFeedHomeTimeline(twitter,false,0,function(error,data){

            if(error){
                logger.info('Error Twitter importTwitterFeedToInteractions():Twitter ',error);
                callback(false);
            }
            else if(data && data.length > 0){
                var sinceId = data[0].id_str;
                if(common.checkRequired(sinceId)){
                    userManagementObj.updateTwitterSinceId(user._id,sinceId,function(error,isSuccess,message){});
                }
                var tweetsDirect = [];
                var tweetsOther = [];
                var allScreenNames = [];
                var referenceIds = [];
                for(var i=0; i<data.length; i++){
                    var result = validateAndParseTweet(user,data[i]);
                    if(result.isValid){
                        tweetsDirect = tweetsDirect.concat(result.tweetsDirect);
                        tweetsOther = tweetsOther.concat(result.tweetsOther);
                        allScreenNames = allScreenNames.concat(result.userScreenNames);
                        referenceIds = referenceIds.concat(result.referenceIds);
                    }
                }
                if(referenceIds.length > 0){
                    referenceIds = common.removeDuplicate_id(referenceIds,true);
                }

                if(allScreenNames.length > 0){
                    allScreenNames = common.removeDuplicate_id(allScreenNames,true);
                    var query = {"twitter.userName":{$in:allScreenNames}};
                    var projection = {emailId:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,companyName:1,designation:1,mobileNumber:1,skypeId:1,location:1,twitter:1};
                    userManagementObj.selectUserProfilesCustomQuery(query,projection,function(error,users){

                        if(error){
                            logger.info('Error Twitter importTwitterFeedToInteractions():Twitter selectUserProfilesCustomQuery() ',error);
                            callback(false);
                        }
                        else if(users && users.length > 0){
                            for(var u=0; u<users.length; u++){
                                for(var int=0; int<tweetsOther.length; int++) {
                                    if (tweetsOther[int].user_twitter_name == users[u].twitter.userName) {
                                        tweetsOther[int].userId = users[u]._id;
                                        tweetsOther[int].firstName = users[u].firstName;
                                        tweetsOther[int].lastName = users[u].lastName;
                                        tweetsOther[int].publicProfileUrl = users[u].publicProfileUrl;
                                        tweetsOther[int].profilePicUrl = users[u].profilePicUrl;
                                        tweetsOther[int].companyName = users[u].companyName;
                                        tweetsOther[int].designation = users[u].designation;
                                        tweetsOther[int].location = users[u].location;
                                        tweetsOther[int].mobileNumber = users[u].mobileNumber;
                                        tweetsOther[int].emailId = users[u].emailId;
                                    }
                                }
                            }

                            addInteractionsBatch(user,tweetsDirect,tweetsOther,referenceIds,callback)
                        }
                        else addInteractionsBatch(user,tweetsDirect,tweetsOther,referenceIds,callback)

                    })
                }
                else{
                    addInteractionsBatch(user,tweetsDirect,tweetsOther,referenceIds,callback)
                }
            }
            else{
                callback(true)
            }
        })
    };
}

Twitter.prototype.getTopTweetByTwitterUserName = function(twitter,twitterUserName,callback){
    if(this.validateTwitterAccount(twitter)){
        var T = new Twit({
            consumer_key:  authConfig.TWITTER_CONSUMER_KEY,
            consumer_secret:  authConfig.TWITTER_CONSUMER_SECRET,
            access_token:  twitter.token,
            access_token_secret:  twitter.refreshToken
        });

        //since:2016-08-26
        // var q = {q:'from:@srelatastest+OR+from:@narendramodi+OR+@mCent_airtime+OR+@SamsungMobile'}
        // var q = {q:'from:'+twitterUserName,count:1,result_type:'recent'} //not using 'search/tweets' since it failed to return previous months data
        var q = {screen_name:twitterUserName, count:1} //statuses/user_timeline
        // var test = {screen_name:twitterUserName}
        //
        // T.get('users/lookup',q, function(err, data, response) {
        //
        //     console.log(JSON.stringify(data,null,2))
        //
        // })

        T.get('statuses/user_timeline',q, function(err, data, response) {
            
            if (err) {
                logger.info('Twitter Feed error ',err)
                callback('TWITTER_ERROR',[])
            }
            else{
                if (!common.checkRequired(data) && !common.checkRequired(data[0])) {
                    callback(false,[])
                }
                else {

                    common.getLocationGoogleAPI(data[0].user.location,function (googleLocations) {

                        if (googleLocations && googleLocations.results[0]) {

                            var location = googleLocations.results[0].formatted_address;
                            var obj = {
                                data:data[0] ? data[0].text: null,
                                id: data[0].id_str,
                                imageUrl:data[0].user.profile_image_url_https,
                                created_at:data[0].created_at,
                                location:location
                            }

                            callback(false,obj)
                        } else {
                            obj = {
                                data:data[0] ? data[0].text: null,
                                id: data[0].id_str,
                                imageUrl:data[0].user.profile_image_url_https,
                                created_at:data[0].created_at,
                                location:data[0].user.location
                            }
                            
                            callback(false,obj)
                        }
                    });
                }
            }
        })
    } else callback('NO_TWITTER_ACCOUNT',[])
}

function addInteractionsBatch(user,tweetsDirect,tweetsOther,referenceIds,callback){
    tweetsDirect = tweetsDirect.concat(tweetsOther);

    interactionObj.addInteractionsBulk(user,tweetsDirect,referenceIds,function(isSuccess){
        callback(isSuccess)
    })
}

function validateAndParseTweet(user,tweet){

    var tweetsDirect = [];
    var tweetsOther = [];
    var userScreenNames = [];
    var referenceIds = [];

    var isSender = false;
    var isValid = false;
    var past = moment();
    past.date(past.date() - 90);

    var tweetDate = moment(new Date(tweet.created_at).toISOString());
    if(tweetDate.isAfter(past)){
        if(tweet.user.screen_name == user.twitter.userName){
            isSender = true;
            isValid = true;
        }
        else if(tweet.in_reply_to_screen_name == user.twitter.userName){
            isSender = false;
            isValid = true;
        }
        else if(tweet.entities && tweet.entities.user_mentions && tweet.entities.user_mentions.length > 0){
            for(var i=0; i<tweet.entities.user_mentions.length; i++){
                if(tweet.entities.user_mentions[i].screen_name == user.twitter.userName){
                    isSender = false;
                    isValid = true;
                }
            }
        }
    }

    if(isValid){
        referenceIds.push(tweet.id_str);
        if(isSender){
            tweetsDirect.push(common.getInteractionObject(user._id,user.twitter.id,user.twitter.userName,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'sender','twitter','tweet',user.emailId,tweet.id_str,'twitter',tweet.text,null,new Date(tweet.created_at),null,null,null,null));
            if(common.checkRequired(tweet.in_reply_to_screen_name)){
                tweetsOther.push(common.getInteractionObject(null,tweet.in_reply_to_user_id_str,tweet.in_reply_to_screen_name,null,null,null,null,null,null,null,null,'receiver','twitter','tweet',null,tweet.id_str,'twitter',tweet.text,null,new Date(tweet.created_at),null,null,null,null))
                userScreenNames.push(tweet.in_reply_to_screen_name);
            }
            if(tweet.entities && tweet.entities.user_mentions && tweet.entities.user_mentions.length > 0){
                for(var j=0; j<tweet.entities.user_mentions.length; j++){
                    if(tweet.entities.user_mentions[j].screen_name != user.twitter.userName && tweet.entities.user_mentions[j].screen_name != tweet.in_reply_to_screen_name){
                        tweetsOther.push(common.getInteractionObject(null,tweet.entities.user_mentions[j].id_str,tweet.entities.user_mentions[j].screen_name,null,null,null,null,null,null,null,null,'receiver','twitter','tweet',null,tweet.id_str,'twitter',tweet.text,null,new Date(tweet.created_at),null,null,null,null))
                        userScreenNames.push(tweet.entities.user_mentions[j].screen_name);
                    }
                }
            }
        }
        else{
            tweetsDirect.push(common.getInteractionObject(user._id,user.twitter.id,user.twitter.userName,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','twitter','tweet',user.emailId,tweet.id_str,'twitter',tweet.text,null,new Date(tweet.created_at),null,null,null,null));
            if(tweet.user && common.checkRequired(tweet.user.screen_name)){
                tweetsOther.push(common.getInteractionObject(null,tweet.user.id_str,tweet.user.screen_name,null,null,null,null,null,null,null,null,'sender','twitter','tweet',null,tweet.id_str,'twitter',tweet.text,null,new Date(tweet.created_at),null,null,null,null));
                userScreenNames.push(tweet.user.screen_name);
            }
        }
    }

    return {
        isValid:isValid,
        tweetsDirect :tweetsDirect ,
        tweetsOther :tweetsOther,
        userScreenNames :userScreenNames,
        referenceIds:referenceIds
    }
}

module.exports = Twitter;