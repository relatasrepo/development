var moment = require('moment-timezone');

var commonUtility = require('../common/commonUtility');
var contactClass = require('../dataAccess/contactsManagementClass');
var mobileAuth = require('../common/mobileAuthValidation');
var appCredentials = require('../config/relatasConfiguration');
var errorClass = require('../errors/errorClass');
var validations = require('../public/javascripts/validation');
var userManagement = require('../dataAccess/userManagementDataAccess');
var emailSender = require('../public/javascripts/emailSender');
var errorMessages = require('../errors/errorMessage');
var meetingManagement = require('../dataAccess/meetingManagement');
var eventDataAccess = require('../dataAccess/eventDataAccess');
var calendarPasswordManagement = require('../dataAccess/calendarPasswordManagement');
var messageDataAccess = require('../dataAccess/messageManagement');

var meetingClassObj = new meetingManagement();
var messageAccess = new messageDataAccess();
var errorObj = new errorClass();
var appCredential = new appCredentials();
var common = new commonUtility();
var validation = new validations();
var userManagementObj = new userManagement();
var errorMessagesObj = new errorMessages();
var contactObj = new contactClass();
var eventAccess = new eventDataAccess();

var statusCodes = errorMessagesObj.getStatusCodes();
var domain = appCredential.getDomain();

function NotificationSupportClass(){

    this.getNotificationsCount = function(userId, dateMin, timezone, callback){
        getNotificationsCount(userId, dateMin, timezone, callback)
    };
    this.getNotificationsList = function(userId, dateMin, timezone, callback){
        getNotificationsList(userId, dateMin, timezone, callback)
    };

}

function getNotificationsCount(userId, dateMin, timezone, callback) {
    var count = 0
    getPendingToDoItems(userId, dateMin, false, function (countTodo) {
        count += countTodo;
        getNewInvitationsCount(userId, false, timezone, function (countInvi) {
            count += countInvi;
            getNonConfirmedEventsAndCount(userId, dateMin, false, function (countEvents) {
                count += countEvents;
                getCalendarPasswordRequests(userId, false, function (countReq) {
                    count += countReq;
                    getSharedDocRed(userId, false, function (countDocRed) {
                        count += countDocRed;
                        getSharedDocNonRead(userId, false, function (countDocNonRead) {
                            count += countDocNonRead;
                            getMessagesUnRead(userId, false, function (countMessages) {
                                count += countMessages;
                                callback(count)
                            })
                        })
                    })
                })
            })
        })
    })
}

function getNotificationsList(userId, dateMin, timezone, callback) {
    var list = []
    getPendingToDoItems(userId, dateMin, true, function (listTodo) {
        list = list.concat(listTodo)
        getNewInvitationsCount(userId, true, timezone, function (listInvi) {
            list = list.concat(listInvi)
            getNonConfirmedEventsAndCount(userId, dateMin, true, function (listEvents) {
                list = list.concat(listEvents)
                getCalendarPasswordRequests(userId, true, function (listReq) {
                    list = list.concat(listReq)
                    getSharedDocRed(userId, true, function (listDocRed) {
                        list = list.concat(listDocRed)
                        getSharedDocNonRead(userId, true, function (listDocNonRead) {
                            list = list.concat(listDocNonRead)
                            getMessagesUnRead(userId, true, function (listMessages) {
                                list = list.concat(listMessages)
                                callback(list);
                            })
                        })
                    })
                })
            })
        })
    })
}


function getPendingToDoItems(userId, dateMin, isList, callback) {
    if (isList) {
        meetingClassObj.getPendingToDoItemsNotification(userId, dateMin, function (list, isPopulated) {
            var notificationArr = [];
            if (common.checkRequired(list) && list.length > 0) {
                for (var n = 0; n < list.length; n++) {
                    var obj = {
                        type: 'task',
                        title: list[n].actionItem,
                        dateTime: list[n].createdDate,
                        eventId: list[n]._id
                    };
                    if (isPopulated) {
                        obj.sentBy = list[n].createdBy.firstName + ' ' + list[n].createdBy.lastName
                    }
                    notificationArr.push(obj)
                }
            }

            callback(notificationArr);
        })
    }
    else {
        meetingClassObj.getPendingToDoItemsCount(userId, dateMin, function (countTodo) {
            if (common.checkRequired(countTodo)) {
                callback(countTodo)
            } else callback(0)
        })
    }
}

function getNewInvitationsCount(userId, isList, timezone, callback) {
    userManagementObj.newInvitations(userId, function (error, invitations, message) {
        var notificationCount = 0;
        var notificationArr = [];
        if (error) {
            if (isList) {
                callback([])
            } else
                callback(0);
        }
        else {
            if (invitations.length > 0) {

                var today = moment().tz(timezone)
                today.hours(0)
                today.minutes(0)
                today.seconds(0)
                today.milliseconds(0)
                today = new Date(today.format())
                invitations = common.removeDuplicate_id(invitations);

                for (var invi = 0; invi < invitations.length; invi++) {
                    var maxSlot = invitations[invi].scheduleTimeSlots.reduce(function (a, b) {
                        return new Date(a.start.date) > new Date(b.start.date) ? a : b;
                    });
                    if (new Date(moment(maxSlot.start.date).tz(timezone).format()) >= today) {
                        var title = maxSlot.title;
                        var sentBy = invitations[invi].senderName;
                        var addRow = true;
                        if (invitations[invi].suggested && invitations[invi].suggestedBy) {
                            if (invitations[invi].suggestedBy.userId != userId) {
                                if (invitations[invi].suggestedBy.userId == invitations[invi].senderId) {

                                } else {
                                    if (invitations[invi].selfCalendar) {
                                        var rFName = invitations[invi].toList[0].receiverFirstName || '';
                                        var rLName = invitations[invi].toList[0].receiverLastName || '';

                                        sentBy = rFName + ' ' + rLName;
                                    } else {
                                        sentBy = invitations[invi].to.receiverName;
                                    }
                                }
                            } else addRow = false;
                        } else {
                            if (invitations[invi].senderId == userId) {
                                addRow = false;
                            }
                        }

                        if (addRow) {
                            notificationCount += 1;
                            var note = {
                                type: 'meeting',
                                title: title,
                                dateTime: invitations[invi].scheduledDate,
                                meetingId: invitations[invi].invitationId,
                                sentBy: sentBy
                            };
                            notificationArr.push(note)
                        }
                    }
                }
            }

            if (isList) {
                callback(notificationArr)
            } else
                callback(notificationCount);
        }
    });
}

function getNonConfirmedEventsAndCount(userId, dateMin, isList, callback) {
    eventAccess.getNonConfirmedEventsAndCount(userId, dateMin, function (events) {
        if (isList) {
            var notificationArr = [];
            for (var n = 0; n < events.length; n++) {
                var obj = {
                    type: 'event',
                    title: events[n].eventName,
                    dateTime: events[n].createdDate,
                    eventId: events[n]._id,
                    sentBy: events[n].createdBy.userName
                };
                notificationArr.push(obj)
            }
            callback(notificationArr);
        }
        else {
            callback(events.length)
        }

    })
}

function getCalendarPasswordMessage(req, isPopulated) {
    if (isPopulated)
        return 'Calendar password request from ' + req.requestFrom.userId.firstName;
    else return 'Calendar Password Request';
}

function getCalendarPasswordSentBy(req, isPopulated) {
    if (isPopulated)
        return req.requestFrom.userId.firstName + ' ' + req.requestFrom.userId.lastName;
    else return 'Calendar Password Request';
}

function getCalendarPasswordRequests(userId, isList, callback) {
    var calendarPasswordManagements = new calendarPasswordManagement();
    calendarPasswordManagements.getReceivedCalendarPasswordRequests(userId, function (error, calendarRequests) {
        if (common.checkRequired(calendarRequests) && calendarRequests.length > 0) {
            if (isList) {
                userManagementObj.populateCustomQuery(calendarRequests, [{
                    path: 'requestFrom.userId',
                    select: 'firstName lastName emailId'
                }, {path: 'requestTo.userId', select: 'firstName lastName emailId'}], function (list, isPopulated) {
                    var notificationArr = [];
                    for (var n = 0; n < list.length; n++) {
                        var obj = {
                            type: 'calendarPassword',
                            message: getCalendarPasswordMessage(list[n], isPopulated),
                            dateTime: list[n].requestDate,
                            reqId: list[n]._id,
                            sentBy: getCalendarPasswordSentBy(list[n], isPopulated)
                        };
                        notificationArr.push(obj)
                    }
                    callback(notificationArr);
                });
            }
            else callback(calendarRequests.length)
        }
        else {
            if (isList) {
                callback([])
            } else callback(0)
        }
    })
}

function getSharedDocRed(userId, isList, callback) {
    if (isList) {
        userManagementObj.sharedDocRed(userId, function (err, docs, msg) {
            var notificationArr = [];
            if (common.checkRequired(docs) && docs.length > 0) {
                for (var doc = 0; doc < docs.length; doc++) {
                    for (var share = 0; share < docs[doc].sharedWith.length; share++) {
                        if (docs[doc].sharedWith[share].accessed) {
                            var title = docs[doc].documentName;
                            var docUrl = '/docs';
                            var isoDate = docs[doc].sharedWith[share].lastAccessed;
                            // var docDate = getDateFormat(isoDate);
                            var sharedBy = docs[doc].sharedWith[share].firstName;
                            var obj = {
                                type: 'document',
                                message: sharedBy + ' red your document',
                                dateTime: isoDate,
                                docId: docs[doc]._id,
                                sentBy: sharedBy
                            };
                            notificationArr.push(obj)
                        }
                    }
                }
            }
            callback(notificationArr);
        });
    }
    else {
        userManagementObj.sharedDocRedCount(userId, function (count) {
            callback(count)
        });
    }
}

function getSharedDocNonRead(userId, isList, callback) {
    if (isList) {
        userManagementObj.documentsSharedWithMeNonRead(userId, function (error, docs, msg) {
            var notificationArr = [];
            if (common.checkRequired(docs) && docs.length > 0) {
                for (var doc = 0; doc < docs.length; doc++) {
                    var isoDate = docs[doc].sharedWith[0].sharedOn || docs[doc].sharedDate;
                    var sharedBy = docs[doc].sharedBy.firstName;
                    var obj = {
                        type: 'document',
                        message: sharedBy + ' shared document with you',
                        dateTime: isoDate,
                        docId: docs[doc]._id,
                        sentBy: sharedBy
                    };
                    notificationArr.push(obj)
                }
            }
            callback(notificationArr);
        });
    }
    else {
        userManagementObj.documentsSharedWithMeNonReadCount(userId, function (count) {
            callback(count)
        });
    }
}


function getMessagesUnRead(userId, isList, callback) {
    if (!isList) {
        messageAccess.getUnReadMessageCount(userId, ['message'], function (count) {
            callback(count);
        })
    }
    else {
        messageAccess.getUnReadMessages(userId, ['message'], function (messages) {
            if (common.checkRequired(messages) && common.checkRequired(messages.messages) && common.checkRequired(messages.messages.length > 0)) {
                messageAccess.populateMessageUserDetails(messages.messages, function (messagesList) {

                    var notificationArr = [];
                    for (var n = 0; n < messagesList.length; n++) {
                        var obj = {
                            type: 'message',
                            message: messagesList[n].message,
                            dateTime: messagesList[n].sentOn,
                            messageId: messagesList[n]._id,
                            sentBy: messagesList[n].senderId.firstName + ' ' + messagesList[n].senderId.lastName
                        }
                        notificationArr.push(obj)
                    }
                    callback(notificationArr);
                })

            } else callback([]);

        })
    }
}

module.exports = NotificationSupportClass;