
var FullContactAPI = require('fullcontact');

var userManagement = require('../dataAccess/userManagementDataAccess');
var contactsManagementClass = require('../dataAccess/contactsManagementClass');
var appCredentials = require('../config/relatasConfiguration');
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');

var common = new commonUtility();
var contactsManagementClassObj = new contactsManagementClass();
var userManagementObj = new userManagement();
var appCredential = new appCredentials();
var logLib = new winstonLog();
var logger =logLib.getWinston();
var fullcontact = new FullContactAPI(appCredential.getAuthCredentials().FULL_CONTACT_API_KEY);


function FullContact(){

}

FullContact.prototype = fullcontact;

FullContact.prototype.getPersonByEmail = function(userId,emailId,req,callback){
    if(!req.session.fullContactSession){
        req.session.fullContactSession = {}
    }
    this.person.email(emailId, function (err, data) {

        if(err){
            logger.info('Error in fullContactObj.person.email: '+emailId,err);
            req.session.fullContactSession[emailId] = false;
            callback()
        }
        else{
            req.session.fullContactSession[emailId] = false;
            var obj = null;
            if(data.status == 200){

                if(common.checkRequired(data.socialProfiles) && data.socialProfiles.length > 0){
                    obj = {};
                    var isValid = false;
                    for(var i=0; i<data.socialProfiles.length; i++){
                        if(data.socialProfiles[i].type == 'twitter'){
                            isValid = true;
                            obj.twitterUserName = data.socialProfiles[i].username
                        }
                        else if(data.socialProfiles[i].type == 'linkedin'){
                            isValid = true;
                            obj.linkedinUserName = data.socialProfiles[i].username
                        }
                        else if(data.socialProfiles[i].type == 'facebook'){
                            isValid = true;
                            obj.facebookUserName = data.socialProfiles[i].username
                        }
                    }
                    if(isValid){
                        req.session.fullContactSession[emailId] = obj;
                        contactsManagementClassObj.updateContactSocialInfo(emailId,obj,false);
                        callback(obj)
                    }
                    else{
                        req.session.fullContactSession[emailId] = false;
                        callback(null)
                    }
                }
                else callback(null)
            }
            else
            callback(null);
        }
    });
};

module.exports = FullContact;