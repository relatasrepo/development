var gcal = require('google-calendar');
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var moment = require('moment-timezone');
var simple_unique_id = require('simple-unique-id');
var request=require('request');
var gmailApi = require('node-gmail-api');
var userManagement = require('../dataAccess/userManagementDataAccess');
var meetingManagement = require('../dataAccess/meetingManagement');
var interactionManagement = require('../dataAccess/interactionManagement');
var commonUtility = require('../common/commonUtility');
var aggregateSupport = require('../common/aggregateSupport');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var contactClass = require('../dataAccess/contactsManagementClass');
var rabbitmq = require('./rabbitmq');
var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var ActionItemsManagement = require('../dataAccess/actionItemsManagement');
var ContactManagement = require('../dataAccess/contactManagement');

var contactObj = new contactClass();
var aggregateSupportObj = new aggregateSupport();
var interactionManagementObj = new interactionManagement();
var common = new commonUtility();
var appCredential = new appCredentials();
var userManagements = new userManagement();
var meetingClassObj = new meetingManagement();
var authConfig = appCredential.getAuthCredentials();
var rabbitmqObj = new rabbitmq();
var actionItemsManagementObj = new ActionItemsManagement();
var contactManagementObj = new ContactManagement();

var logLib = new winstonLog();
var logger =logLib.getWinston();
// var google = require('node-google-api')({apiKey:authConfig.GOOGLE_CLIENT_ID,debugMode: true});
var google = require('node-google-api')(authConfig.GOOGLE_CLIENT_ID);
var _ = require("lodash");

var loggerGoogle =logLib.getMeetingsErrorLogger();

var customObjectId = require('mongodb').ObjectID;

function GoogleCalendar() {

}

/* Create and update todo events */
GoogleCalendar.prototype.createToDoEvent = function(userId,todo,meetingId,callback){
    getCreateToDoEventData(userId,todo,meetingId,function(token,body) {
        if(token){

            var google_calendar = new gcal.GoogleCalendar(token);
            google_calendar.events.insert('primary', body, function (err, result) {
                if (err) {
                    logger.error('creating TODO event in google failed ' + JSON.stringify(err));
                    callback(false);
                }
                else {
                    logger.info('creating TODO event in google success');
                    meetingClassObj.updateToDoWithGoogleEventId(result.id,meetingId,todo._id);
                    callback(true);
                }
            })
        }
        else callback(false);
    })
};

GoogleCalendar.prototype.createTaskEvent = function(userId,todo,callback){
    getCreateToDoEventData(userId,todo,false,function(token,body) {
        if(token){

            var google_calendar = new gcal.GoogleCalendar(token);
            google_calendar.events.insert('primary', body, function (err, result) {
                if (err) {
                    logger.error('creating TODO event in google failed ', err);
                    callback(err,false);
                }
                else {
                    logger.info('creating TODO event in google success');
                    //meetingClassObj.updateToDoWithGoogleEventId(result.id,meetingId,todo._id);
                    callback(err,result);
                }
            })
        }
        else callback({error:'token failure'},false);
    })
};

GoogleCalendar.prototype.createReplyRemainderEvent = function(user,replyObj,callback){
    var date = new Date();
    if(validateGoogleAccount(user)){
        getNewGoogleToken(user.google[0].token,user.google[0].refreshToken,function(token){
            if(token){
                var body = {
                    summary:replyObj.title,

                    end:{
                        dateTime:new Date(date.getFullYear(),date.getMonth(),date.getDate(),date.getHours(),date.getMinutes()+30,0,0)
                    },
                    start:{
                        dateTime:new Date(date.getFullYear(),date.getMonth(),date.getDate(),date.getHours(),date.getMinutes()+15,0,0)
                    },
                    description:'Reply phone call in 15 minutes',
                    location:'Reminder'
                }

                var google_calendar = new gcal.GoogleCalendar(token);
                google_calendar.events.insert('primary', body, function (err, result) {
                    if (err) {
                        logger.error('creating TODO event in google failed ' + JSON.stringify(err));
                        callback(err,false);
                    }
                    else {
                        logger.info('creating TODO event in google success');
                        //meetingClassObj.updateToDoWithGoogleEventId(result.id,meetingId,todo._id);
                        callback(err,result);
                    }
                })
            }callback({error:'Token error'},false);
        })
    }callback({error:'No google account'},false);
};

GoogleCalendar.prototype.updateToDoEvent = function(userId,todo,meetingId,count,eventId,callback){
    getCreateToDoEventData(userId,todo,meetingId,function(token,body) {

        if(token){

            var google_calendar = new gcal.GoogleCalendar(token);
            body.sequence = count;

            google_calendar.events.update('primary', eventId, body, function (err, result) {

                if (err) {

                    logger.info('Error in Google event update ' + JSON.stringify(err));
                    callback(null);
                }
                else {

                    callback(result);
                }
            });
        }
        else callback(false);
    })
};

GoogleCalendar.prototype.deleteToDoEvent = function(userId,eventId,callback){

    userManagements.findUserProfileByIdWithCustomFields(userId,{google:1,emailId:1,firstName:1,lastName:1},function(error,user){

        if(validateGoogleAccount(user)){

            getNewGoogleToken(user.google[0].token,user.google[0].refreshToken,function(token){

                if(token){

                    var google_calendar = new gcal.GoogleCalendar(token);

                    google_calendar.events.delete('primary',eventId,function(err,result){

                        if(err){
                            logger.info('error occurred in creating todo google event '+JSON.stringify(err))
                            if(common.checkRequired(err.errors) && err.errors.length > 0 && common.checkRequired(err.errors[0]) && err.errors[0].reason == 'deleted'){

                                callback(true)
                            }else
                                callback(false)
                        }
                        else{

                            callback(true)
                        }
                    });
                }else callback(false)
            })
        }else callback(false)
    });
};

function getCreateToDoEventData(userId,todo,meetingId,callback){

    userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0,linkedin:0,twitter:0,officeCalendar:0,currentLocation:0,calendarAccess:0},function(error,user){

        if(validateGoogleAccount(user)){

            getToDoBody(todo,meetingId,user.google[0].emailId,function(body){

                if(body){

                    getNewGoogleToken(user.google[0].token,user.google[0].refreshToken,function(token){

                        if(token){

                            callback(token,body)
                        }else callback(false)
                    })
                }else callback(false);
            })
        }
        else callback(false);
    })
}

function getToDoBody(toDoItem,meetingId,senderEmailId,callback){
    var query;
    if(common.checkRequired(toDoItem.assignedTo)){
        query = {_id:toDoItem.assignedTo}
    }
    else if(common.checkRequired(toDoItem.assignedToEmailId)){
        query = {emailId:toDoItem.assignedToEmailId}
    }
    if(common.checkRequired(query)){
        userManagements.selectUserProfileCustomQuery(query,{emailId:1,google:1,firstName:1},function(error,user){
            if(common.checkRequired(user)){
                var userPrimaryGoogleEmail = validateGoogleAccount(user) ? user.google[0].emailId : user.emailId;
                callback(getLastBody(meetingId,toDoItem,userPrimaryGoogleEmail,senderEmailId));
            }else
                callback(getLastBody(meetingId,toDoItem,toDoItem.assignedToEmailId,senderEmailId));
        });
    }
    else{
        callback(false);
    }
}

function getLastBody(meetingId,toDoItem,receiverEmailId,senderEmailId){

    var date = new Date(toDoItem.dueDate);

    var description = '';
    if(meetingId){
        description = toDoItem.actionItem;
    }
    else if(checkRequired(toDoItem.description)){
        description = toDoItem.description
    }else description = toDoItem.taskName || '';

    return {
        id:meetingId ? 'todoid'+toDoItem._id+'meetingid'+meetingId : 'todoid'+toDoItem._id,
        summary:meetingId ? toDoItem.actionItem : toDoItem.taskName,
        attendees:[
            {
                email:receiverEmailId
            },
            {
                email:senderEmailId
            }
        ],
        end:{
            dateTime:new Date(date.getFullYear(),date.getMonth(),date.getDate(),date.getHours(),date.getMinutes()+30,date.getSeconds())
        },
        start:{
            dateTime:date
        },
        description:description,
        location:'TASK'
    }
}

function getGoogleTokens(userInfo,back){
    var tokenProvider = new GoogleTokenProvider({
        refresh_token: userInfo.google[0].refreshToken,
        client_id:     authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        access_token: userInfo.google[0].token
    });
    tokenProvider.getToken(back)
}

function getRequestBody(eventDetails,attendees,eventId,eName){
    var maxSlot =  eventDetails.scheduleTimeSlots.reduce(function (a, b) { return new Date(a.start.date) < new Date(b.start.date) ? a : b; });

    var request_body = {
        id:eventId,
        summary:eName ? maxSlot.title : 'Not Conf. ('+maxSlot.title+' )',
        attendees:attendees,
        end:{
            dateTime:new Date(maxSlot.end.date)
        },
        start:{
            dateTime:new Date(maxSlot.start.date)
        },
        description:maxSlot.description,
        location:maxSlot.locationType,
        locationType:maxSlot.locationType,
        locationDetails:maxSlot.location,
        relatasEvent:true
    };

    return request_body;
}

function validateGoogleAccount(userInfo){
    if(checkRequired(userInfo)){
        if(checkRequired(userInfo.google)){
            if(checkRequired(userInfo.google[0])){
                if(checkRequired(userInfo.google[0].id)){
                    return true;
                }else  return false;
            }else  return false;
        }else return false;
    }else return false;
}

GoogleCalendar.prototype.validateUserGoogleAccount = function(userInfo){
    return validateGoogleAccount(userInfo);
}

GoogleCalendar.prototype.createEvent = function(userId,eventDetails,eName,part,callback){

    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1},function(error,userProfile) {
        if (error) {
            logger.error('Error occurred while searching authenticated user profile in the DB');
            userProfile = null;
            callback(userProfile);
        }
        else{
            var userInfo=userProfile;
            if(validateGoogleAccount(userInfo)){
                getGoogleTokens(userInfo,function (err, token) {
                    if (err) {
                        logger.info('Error in token provider'+err);
                    }

                    var id = eventDetails.invitationId || simple_unique_id.generate('googleRelatas');
                    var google_calendar = new gcal.GoogleCalendar(token);
                    var calendarId='primary';

                    var attendees = [
                        {
                            email:userInfo.google[0].emailId
                        }
                    ];

                    var request_body = getRequestBody(eventDetails,attendees,'relatas'+id,eName);
                    google_calendar.events.insert(calendarId,request_body,function(err,result){
                        if(err){
                            logger.error('creating event in google failed '+JSON.stringify(err));
                            callback(false);
                        }
                        else{
                            logger.info('creating event in google success');
                            callback(result);
                        }
                    });
                }); // end of token provider
            }else callback('noAccount');
        }
    });
};

GoogleCalendar.prototype.updateEvent = function(userId,eventDetails,googleEventId,count,eName,callback) {
    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1}, function (error, userProfile) {
        if (error || !checkRequired(userProfile)) {
            logger.error('Error occurred while searching authenticated user profile in the DB');
            userProfile = null;
            callback(userProfile);
        }
        else {
            var userInfo = userProfile;
            if (validateGoogleAccount(userInfo)) {
                getGoogleTokens(userInfo, function (err, token) {
                    if (err) {
                        logger.info('Error in token provider' + err);
                    }

                    var google_calendar = new gcal.GoogleCalendar(token);
                    var calendarId = 'primary';

                    var attendees = [
                        {
                            email:userInfo.google[0].emailId
                        }
                    ];
                    var request_body = getRequestBody(eventDetails, attendees, googleEventId, eName);
                    var eventId = googleEventId
                    request_body.sequence = count;
                    google_calendar.events.update(calendarId, eventId, request_body, function (err, result) {

                        if (err) {
                            logger.info('Error in Google event update ' + JSON.stringify(err));
                            callback(null);
                        }
                        else {
                            count = count + 1;
                            callback(result, count);
                        }
                    });
                }); // end of token provider
            } else callback('noAccount', 1);
        }
    })
};

GoogleCalendar.prototype.getGoogleContactsEmailsEventsOffice365user = function(userInfo){

    if (validateGoogleAccount(userInfo)) {

        getNewGoogleToken(userInfo.google[0].token,userInfo.google[0].refreshToken,function(token){

            if(token){
                getContactsFromGoogleApi(token,userInfo._id,userInfo.lastLoginDate);
            }
        });
        getGoogleMeetingInteractionsFun(userInfo._id,userInfo.lastLoginDate);
    }
};

GoogleCalendar.prototype.getGoogleEmailInteractions = function(userId,access_token,lastLoginDate,dashboardPopup,office,officeEmail,googleEmail,isCallback,callback){
    getUserEmailListNext (userId,access_token,lastLoginDate,dashboardPopup,office,officeEmail,googleEmail,isCallback,callback)
};

GoogleCalendar.prototype.getNewGoogleTokenNew = function(token,refreshToken,callback){
    getNewGoogleToken(token,refreshToken,callback);
};

GoogleCalendar.prototype.updateAllDataForGoogleUsers = function (userProfile,req) {

    getNewGoogleToken(userProfile.google[0].accessToken,userProfile.google[0].refreshToken,function (token) {
        // getContactsFromGoogleApi(token,userProfile._id,userProfile.lastLoginDate);
        getGoogleMeetingInteractionsFun(userProfile._id,userProfile.lastDataSyncDate,userProfile.google,false,req,"google")
    })
}

function getNewGoogleToken(token,refreshToken,callback){
    var tokenProvider = new GoogleTokenProvider({
        refresh_token: refreshToken,
        client_id:     authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        access_token: token
    });

    tokenProvider.getToken(function (err, token) { // start of token provider
        if (err) {
            callback(false)
        }
        else{
            callback(token)
        }
    })
}

function getUserEmailListNext(userProfile,access_token,lastLoginDate,dashboardPopup,office,officeEmail,googleEmail,req,isCallback,callback){

    var userId = userProfile
    if(userProfile._id){
        getUserEmailList (userProfile,access_token,lastLoginDate,dashboardPopup,office,officeEmail,googleEmail,req,isCallback,callback)
    } else {

        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1,_id:1,companyId:1,companyName:1,designation:1,mobileNumber:1,publicProfileUrl:1,profilePicUrl:1,skypeId:1,location:1,lastLoginDate:1,lastMobileSyncDate:1},function(error,user){
            if(user){
                getUserEmailList (user,access_token,lastLoginDate,dashboardPopup,office,officeEmail,googleEmail,req,isCallback,callback)
            }
        })
    }
}

function getUserEmailList (user,access_token,lastLoginDate,dashboardPopup,office,officeEmail,googleEmail,req,isCallback,callback) {

    var fromDate;

    //TODO: Naveen: last mobile sync date has to check for null for both types. This code is to be used in GetGoogleInteractionForFun.

    if(common.checkRequired(user.lastMobileSyncDate)){
        if(lastLoginDate>user.lastMobileSyncDate){
            fromDate = lastLoginDate
        } else {
            fromDate = user.lastMobileSyncDate
        }
    } else {
        fromDate = lastLoginDate
    }

//Add email interactions for the past 90 days for new users. For registered users by last login date
    var interactionsDirect = [];
    var interactionsOther = [];
    var newInteractionsCollection = [];
    var refIdArr = [];
    var emailIdArr = [];

    var filter = '';
    var after = new Date();
    var before = new Date();
    var now = new Date();

    if(dashboardPopup != true){

        after.setDate(after.getDate() - 90);
        before.setDate(before.getDate() + 3);
        var monthAfter = after.getMonth()+1;
        var monthBefore = before.getMonth()+1;
        filter = 'after:'+after.getFullYear()+'/'+monthAfter+'/'+after.getDate()+' before:'+before.getFullYear()+'/'+monthBefore+'/'+before.getDate();
    }
    else{
        if(common.checkRequired(fromDate)){
            after = new Date(fromDate);
            before = new Date();
            after.setDate(after.getDate() - 1); //Testing
            before.setDate(before.getDate() + 2);
            var monthAfter2 = after.getMonth()+1;
            var monthBefore2 = before.getMonth()+1;
            filter = 'after:'+after.getFullYear()+'/'+monthAfter2+'/'+after.getDate()+' before:'+before.getFullYear()+'/'+monthBefore2+'/'+before.getDate();

        }
        else{
            after.setDate(after.getDate() - 90);
            before.setDate(before.getDate() + 3);
            var monthAfter3 = after.getMonth()+1;
            var monthBefore3 = before.getMonth()+1;
            filter = 'after:'+after.getFullYear()+'/'+monthAfter3+'/'+after.getDate()+' before:'+before.getFullYear()+'/'+monthBefore3+'/'+before.getDate();
        }
    }

    if(req) {
        req.session.beforeDateForEmailAnalysis = before;
        req.session.afterDateForEmailAnalysis = after;
    }
    // if(before > after){

    if(filter){
        common.getInvalidEmailListFromDb(function (invalidEmailList) {
            var gmail = new gmailApi(access_token);

            filter = filter+" -label:drafts";

            var s = gmail.messages(filter);

            s.on('end', function () {
                mapUserDataToInteractions(user, interactionsDirect, interactionsOther, refIdArr, emailIdArr, req,callback);

                interactionManagementObj.updateNewInteractionsCollection(common.castToObjectId(user._id.toString()),user,newInteractionsCollection);
                
                if (isCallback) {
                    callback()
                }
            });

            s.on('error', function (a) {
                mapUserDataToInteractions(user, interactionsDirect, interactionsOther, refIdArr, emailIdArr, req,callback);
                if (isCallback) {
                    callback()
                }
            });

            s.on('data', function (d) {

                if (common.checkRequired(d) && common.checkRequired(d.payload) && common.checkRequired(d.payload.headers)) {
                    if (d.payload.headers.length > 0) {

                        var headers = d.payload.headers;
                        var email = parseEmailHeader(d.id, headers, d.threadId,invalidEmailList,d);

                        if (common.checkRequired(email) && common.checkRequired(email.id)) {
                            newInteractionsCollection = newInteractionsCollection.concat(buildObjForNewEmailInteractions(user,email));
                            var obj = generateAndStoreInteraction(user, email, office, officeEmail, googleEmail);
                            
                            interactionsDirect = interactionsDirect.concat(obj.interactionDirect);
                            
                            interactionsOther = interactionsOther.concat(obj.interactionOther);
                            refIdArr.push(obj.refId);
                            emailIdArr = emailIdArr.concat(obj.emailIdArr);
                        }
                    }
                }
            })
        });
    }
    else{
        if(isCallback || callback){
            callback()
        }
    }
}

function buildObjForNewMeetingInteractions(userProfile,meetings){

    var interactions = [];

    _.each(meetings,function (el) {
        var data = getCommonMeetingInteractionObj(userProfile,el);

        _.each(data.recordsFor,function (emailId) {
            var intObj = _.cloneDeep(data.obj)
            intObj.emailId = emailId;
            intObj.ownerId = userProfile._id
            intObj.companyId = userProfile.companyId?userProfile.companyId:null
            interactions.push(intObj)
        })
    });

    return interactions;

}

function getCommonMeetingInteractionObj(userProfile,meeting){
    var action = "receiver",
        recordsFor = [];

    if(meeting.senderEmailId == userProfile.emailId){
        action = "sender"
    }

    var obj = {
        ownerEmailId:userProfile.emailId,
        ownerId:userProfile._id,
        user_twitter_id:null,
        user_twitter_name:null,
        firstName:null,
        lastName:null,
        publicProfileUrl:null,
        profilePicUrl:null,
        emailId:null,
        companyName:null,
        designation:null,
        location:null,
        mobileNumber:null,
        action:action,
        interactionType:"meeting",
        subType:meeting.scheduleTimeSlots[0].locationType,
        refId:meeting.invitationId,
        source:"google",
        title:meeting.scheduleTimeSlots[0].title,
        description:meeting.scheduleTimeSlots[0].description,
        interactionDate:new Date(meeting.scheduleTimeSlots[0].start.date),
        endDate:null,
        createdDate:new Date(),
        trackId:null,
        emailContentId:null,
        googleAccountEmailId:userProfile.emailId,
        toCcBcc:null,
        emailThreadId:null,
        to:[],
        cc:[],
        hasUnsubscribe:false,
        companyId:userProfile.companyId?userProfile.companyId:null
    };

    _.each(meeting.participants,function (el) {
        if(userProfile.emailId != el.emailId){
            recordsFor.push(el.emailId)
        }
    })

    return {
        obj:obj,
        recordsFor:_.uniq(recordsFor)
    }
}

function buildObjForNewEmailInteractions(userProfile,email){

    var emailIdAndAction = getEmailIdAndAction(userProfile,email),
        interactions = [];

    var obj = {
        ownerEmailId:userProfile.emailId,
        ownerId:userProfile._id,
        user_twitter_id:null,
        user_twitter_name:null,
        firstName:null,
        lastName:null,
        publicProfileUrl:null,
        profilePicUrl:null,
        emailId:null,
        companyName:null,
        designation:null,
        location:null,
        mobileNumber:null,
        action:emailIdAndAction.action,
        interactionType:"email",
        subType:"email",
        refId:email.id,
        source:"google",
        title:email.subject,
        description:null,
        interactionDate:new Date(email.date),
        endDate:null,
        createdDate:new Date(),
        trackId:null,
        emailContentId:email.emailContentId,
        googleAccountEmailId:userProfile.emailId,
        toCcBcc:getToCcBccType(userProfile,email),
        emailThreadId:email.emailThreadId,
        to:email.To?email.To:[],
        cc:email.Cc?email.Cc:[],
        hasUnsubscribe:email.hasUnsubscribe,
        companyId:userProfile.companyId?userProfile.companyId:null
    };

    _.each(emailIdAndAction.recordsFor,function (emailId) {
        var intObj = _.cloneDeep(obj)
        intObj.emailId = emailId;
        intObj.ownerId = userProfile._id
        intObj.companyId = userProfile.companyId?userProfile.companyId:null
        interactions.push(intObj)
    })

    return interactions;
}

function getEmailIdAndAction(userProfile,email) {
    var action = "receiver",
        recordsFor = [];

    if(email.from && email.from[0] && email.from[0].toLowerCase() == userProfile.emailId){
        action = "sender";
    }

    if(email.from && email.from[0] && email.from[0].toLowerCase() != userProfile.emailId){
        recordsFor.push(email.from[0].toLowerCase());
    }

    if(email.from && email.from[0]){
        _.each(email.from,function (el) {
            if(el.toLowerCase() != userProfile.emailId){
                recordsFor.push(el.toLowerCase())
            }
        })
    }

    if(email.To && email.To[0]){
        _.each(email.To,function (el) {
            if(el.toLowerCase() != userProfile.emailId){
                recordsFor.push(el.toLowerCase())
            }
        })
    }

    if(email.Cc && email.Cc[0]){
        _.each(email.Cc,function (el) {
            if(el.toLowerCase() != userProfile.emailId){
                recordsFor.push(el.toLowerCase())
            }
        })
    }

    if(email.Bcc && email.Bcc[0]){
        _.each(email.Bcc,function (el) {
            if(el.toLowerCase() != userProfile.emailId){
                recordsFor.push(el.toLowerCase())
            }
        })
    }

    return {
        action:action,
        recordsFor:_.uniq(recordsFor)
    }

}

function getToCcBccType(userProfile,email){
    var toCcBcc = null,
        userEmailId = userProfile.emailId;
    if(email.To) {
        for (var to = 0; to < email.To.length; to++) {
            var emailIdTo = email.To[to] ? email.To[to] : '';

            if (userEmailId == emailIdTo) {
                toCcBcc = 'to';
                if (email.To.length == 1 && !email.Cc && !email.Bcc) {
                    toCcBcc = 'me';
                }
            }
        }
    }
    if(!toCcBcc && email.Cc){
        for(var cc=0; cc<email.Cc.length; cc++) {
            var emailIdCc = email.Cc[cc] ? email.Cc[cc] : '';
            if (userEmailId == emailIdCc) {
                toCcBcc = 'cc';
            }
        }
    }
    if(!toCcBcc && email.Bcc){
        for(var bcc=0; bcc<email.Bcc.length; bcc++) {
            var emailIdBcc = email.Bcc[bcc] ? email.Bcc[bcc] : '';
            if (userEmailId == emailIdBcc) {
                toCcBcc = 'bcc';
            }
        }
    }

    if(!toCcBcc){
        /*if mail is sent to google group and LIU is member of that group,
         LIU emailId will not be a part of TO,
         CC or BCC list it contains googlegroup email id
         in this case toCcBcc can be 'to'*/
        toCcBcc = 'to';
    }

    return toCcBcc;
}


function parseEmailHeader(id,headers, threadId,invalidEmailList,data){
    var email = {emailContentId:id, emailThreadId:threadId};
    headers = data.payload.headers;
    for(var i=0; i<headers.length; i++){

        if(headers[i].name == 'From' || headers[i].name == 'from'|| headers[i].name == 'FROM'){
            email.from = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'To' || headers[i].name == 'to' || headers[i].name == 'TO'){
            email.To = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Subject' || headers[i].name == 'subject'){
            email.subject = headers[i].value;
        }
        if(headers[i].name == 'Cc' || headers[i].name == 'cc' || headers[i].name == 'CC'){
            email.Cc = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Bcc' || headers[i].name == 'bcc' || headers[i].name == 'BCC'){
            email.Bcc = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Date' || headers[i].name == 'date'){
            email.date = new Date(headers[i].value);
        }
        if(headers[i].name == 'Message-Id' || headers[i].name == 'Message-ID'){
            email.id = headers[i].value;
        }
    }
    
    var isValid = true;
    if(email.to && email.to.length > 0){
        for(var to in email.to){
            // isValid = !matchInArray(email.to[to].split('@')[0])
            isValid = common.isValidEmail(email.to[to], invalidEmailList);
        }
    }
    if(email.from && email.from.length > 0){
        for(var from in email.from){
            // isValid = !matchInArray(email.from[from].split('@')[0])
            isValid = common.isValidEmail(email.from[from], invalidEmailList);
        }
    }
    if(email.Cc && email.Cc.length > 0){
        for(var Cc in email.Cc){
            // isValid = !matchInArray(email.Cc[Cc].split('@')[0])
            isValid = common.isValidEmail(email.Cc[Cc], invalidEmailList);
        }
    }
    if(email.Bcc && email.Bcc.length > 0){
        for(var Bcc in email.Bcc){
            // isValid = !matchInArray(email.Bcc[Bcc].split('@')[0])
            isValid = common.isValidEmail(email.Bcc[Bcc], invalidEmailList);
        }
    }

    email.hasUnsubscribe = checkForUnsubscribeLink(data,email.from)

    return isValid ? email : null;
}

function matchInArray(stringSearch){
    var position = String(common.getInvalidEmailList()).search(stringSearch);
    return (position > -1);
}

function extractEmails (text) {
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

function checkForUnsubscribeLink (data,emailId){
    var body = {};

    var unsubscribeExists = false;

    if(data.payload.parts && data.payload.parts.length > 0){

        for(var i=0; i<data.payload.parts.length; i++){
            if(data.payload.parts[i].mimeType == "text/html"){
                body = data.payload.parts[i].body;
            }
        }

        if(body && !body.data){
            for(var j=0; j<data.payload.parts.length; j++){
                if(data.payload.parts[j].parts && data.payload.parts[j].parts.length > 0){
                    data.payload.parts[j].parts.forEach(function(part){
                        if(part.mimeType == "text/html"){
                            body = part.body;
                        }
                    })
                }
            }
        }

        if(body && body.data){
            body.data = new Buffer(body.data, 'base64').toString("ascii")

            var str = body.data;

            str = str.toString();
            str = str.toLowerCase();

            if(_.includes(str,"change your notification settings for this calendar")
                || _.includes(str,"you wish to remove")
                || _.includes(str,"email on web browser")
                || _.includes(str,"opt-out")
                || _.includes(str,"opt out")
                || _.includes(str,"do not wish to receive future emails")
                || _.includes(str,"you wish to remove this e")
                || _.includes(str,"email preferences")
                || _.includes(str,"unsubscribe")
                || _.includes(str,"subscribe")
                || _.includes(str,"if you don't want to receive these emails in the future")
                || _.includes(str,"would not like to hear from us")
                || _.includes(str,"because you are subscribed for invitations")
                || _.includes(str,"stop receiving these emails")
                || _.includes(str,"are receiving this email at the account")
            ){
                unsubscribeExists = true;
            }
        }
    }

    if(emailId && (emailId == "calendar-notification@google.com" || emailId == "no-reply@microsoft.com")){
        unsubscribeExists = true;
    }

    return unsubscribeExists
}

function generateAndStoreInteraction(user,email,office,officeEmail,googleEmail){
    var interactionDirect = [];
    var interactionOther = [];
    var interactionRefId = [];
    var emailIdArr = [];
    var isSender = false;
    var ccList = [];
    var toList = [];

    _.each(email.To,function (el) {
        toList.push({emailId:el})
    })

    _.each(email.Cc,function (el) {
        emailIdArr.push(el)
        ccList.push({emailId:el})
    })

    if(common.checkRequired(email.from)){
        for(var from=0; from<email.from.length; from++){
            var emailIdFrom = email.from[from] ? email.from[from] : '';
            if(office && googleEmail == emailIdFrom){
                emailIdFrom = officeEmail;
            }

            if(emailIdFrom == user.emailId){
                isSender = true;
                interactionDirect.push(common.getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'sender','email','email',user.emailId,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail, null,email.emailThreadId,toList,ccList,email.hasUnsubscribe));
            }
            else{
                emailIdArr.push(emailIdFrom.trim().toLowerCase());
                var toCcBcc = null;
                if(email.To) {
                    for (var to = 0; to < email.To.length; to++) {
                        var emailIdTo = email.To[to] ? email.To[to] : '';

                        if (office && googleEmail == emailIdTo) {
                            toCcBcc = 'to';
                            if (email.To.length == 1 && !email.Cc && !email.Bcc) {
                                toCcBcc = 'me';
                            }
                        }
                    }
                }
                if(!toCcBcc && email.Cc){
                    for(var cc=0; cc<email.Cc.length; cc++) {
                        var emailIdCc = email.Cc[cc] ? email.Cc[cc] : '';
                        if (office && googleEmail == emailIdCc) {
                            toCcBcc = 'cc';
                        }
                    }
                }
                if(!toCcBcc && email.Bcc){
                    for(var bcc=0; bcc<email.Bcc.length; bcc++) {
                        var emailIdBcc = email.Bcc[bcc] ? email.Bcc[bcc] : '';
                        if (office && googleEmail == emailIdBcc) {
                            toCcBcc = 'bcc';
                        }
                    }
                }

                if(!toCcBcc){
                    /*if mail is sent to google group and LIU is member of that group,
                        LIU emailId will not be a part of TO,
                        CC or BCC list it contains googlegroup email id
                        in this case toCcBcc can be 'to'*/
                    toCcBcc = 'to';
                }

                interactionOther.push(common.getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'sender','email','email',emailIdFrom,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail, toCcBcc, email.emailThreadId,toList,ccList,email.hasUnsubscribe));
            }
        }
    }

    if(common.checkRequired(email.To)){
        for(var to=0; to<email.To.length; to++){
            var emailIdTo = email.To[to] ? email.To[to] : '';
            if(office && googleEmail == emailIdTo){
                emailIdTo = officeEmail;
            }

            var userId = user._id

            if(emailIdTo != user.emailId){
                userId = null
            } else {
                userId = user._id
            }

            if(_.includes(email.To,user.emailId)){
                interactionDirect.push(common.getInteractionObject(null,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','email','email',email.To[to],email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail, null, email.emailThreadId,toList,ccList,email.hasUnsubscribe));
            }
            else if(isSender){
                emailIdArr.push(emailIdTo.trim().toLowerCase());
                interactionOther.push(common.getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'receiver','email','email',email.To[to],email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail, null, email.emailThreadId,toList,ccList,email.hasUnsubscribe));
            }
        }
    }

    if(common.checkRequired(email.Cc)){
        for(var cc=0; cc<email.Cc.length; cc++){

            var emailIdCc = email.Cc[cc] ? email.Cc[cc] : '';
            if(office && googleEmail == emailIdCc){
                emailIdCc = officeEmail;
            }
            var userIdCheck = user._id

            if(emailIdTo != user.emailId){
                userIdCheck = null
            }

            if(!_.includes(email.Cc,user.emailId)){
                interactionDirect.push(common.getInteractionObject(null,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','email','email',email.Cc[cc],email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail, null, email.emailThreadId,toList,ccList,email.hasUnsubscribe));
            } else if(_.includes(email.Cc,user.emailId)){
                interactionDirect.push(common.getInteractionObject(userIdCheck,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','email','email',email.Cc[cc],email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail, null, email.emailThreadId,toList,ccList,email.hasUnsubscribe));
            }
            else if(isSender){
                emailIdArr.push(emailIdCc.trim().toLowerCase());
                interactionOther.push(common.getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'receiver','email','email',email.Cc[cc],email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail, null, email.emailThreadId,toList,ccList,email.hasUnsubscribe));
            }
        }
    }
    if(common.checkRequired(email.Bcc)){
        for(var bcc=0; bcc<email.Bcc.length; bcc++){
            var emailIdBcc = email.Bcc[bcc] ? email.Bcc[bcc] : '';
            if(office && googleEmail == emailIdBcc){
                emailIdBcc = officeEmail;
            }

            var userIdCheck2 = user._id

            if(emailIdTo != user.emailId){
                userIdCheck2 = null
            }

            if(_.includes(email.Bcc,user.emailId)){
                interactionDirect.push(common.getInteractionObject(userIdCheck2,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','email','email',email.Bcc[bcc],email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail, null, email.emailThreadId,null,null,email.hasUnsubscribe));
            }
            else if(isSender){
                emailIdArr.push(emailIdBcc.trim().toLowerCase());
                interactionOther.push(common.getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'receiver','email','email',email.Bcc[bcc],email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail, null, email.emailThreadId,toList,ccList,email.hasUnsubscribe));
            }
        }
    }

    return {
        interactionDirect:interactionDirect,
        interactionOther:interactionOther,
        interactionRefId:interactionRefId,
        refId:email.id,
        emailIdArr:emailIdArr
    }
}

//Prototype function to sync email for mobile.
GoogleCalendar.prototype.mapUserDataToInteractions_ = function(user,interactionsDirect,interactionsOther,refIdArr,emailIdArr){
    mapUserDataToInteractions(user,interactionsDirect,interactionsOther,refIdArr,emailIdArr)
}

function mapUserDataToInteractions(user,interactionsDirect,interactionsOther,refIdArr,emailIdArr,req,callback){
    
    if(emailIdArr.length > 0){
        emailIdArr = common.removeDuplicate_id(emailIdArr,true);
        var query = {"emailId":{$in:emailIdArr}};

        var projection = {emailId:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,companyName:1,designation:1,mobileNumber:1,skypeId:1,location:1,lastLoginDate:1};

        userManagements.selectUserProfilesCustomQuery(query,projection,function(error,users){
            if(error){
                logger.info('Error Linkedin mapUserDataToInteractions():GoogleCalendar selectUserProfilesCustomQuery() ',error);
                addInteractionsBatch(user,interactionsDirect,interactionsOther,refIdArr,req,callback);
            }
            else if(users && users.length > 0){
                for(var u=0; u<users.length; u++){
                    for(var int=0; int<interactionsOther.length; int++) {
                        if (interactionsOther[int].emailId == users[u].emailId) {
                            interactionsOther[int].userId = users[u]._id;
                            interactionsOther[int].firstName = users[u].firstName;
                            interactionsOther[int].lastName = users[u].lastName;
                            interactionsOther[int].publicProfileUrl = users[u].publicProfileUrl;
                            interactionsOther[int].profilePicUrl = users[u].profilePicUrl;
                            interactionsOther[int].companyName =  common.fetchCompanyFromEmail(users[u].emailId)
                            interactionsOther[int].designation = users[u].designation;
                            interactionsOther[int].location = users[u].location;
                            interactionsOther[int].mobileNumber = users[u].mobileNumber;
                            interactionsOther[int].emailId = users[u].emailId;
                        }
                    }
                }
                addInteractionsBatch(user,interactionsDirect,interactionsOther,refIdArr,req,callback);
            }
            else addInteractionsBatch(user,interactionsDirect,interactionsOther,refIdArr,req,callback);
        })
    }
    else{
        addInteractionsBatch(user,interactionsDirect,interactionsOther,refIdArr,req,callback);
    }

    //Add each contact from the email interactions to the user's contact if they do not exist
    common.getInvalidEmailListFromDb(function (list) {
        var cleanArray = common.removeDuplicate_id(emailIdArr, true).filter(function (elem) {
            var ok = common.isValidEmail(elem, list);
            return ok;
        });

        var contactsArray = [];

        for(var i =0;i<cleanArray.length;i++){
            var contactObj2 = {
                _id:new customObjectId(),
                personId:null,
                personName:cleanArray[i],
                personEmailId:cleanArray[i].toLowerCase(),
                birthday:null,
                companyName:null,
                designation:null,
                addedDate:new Date(),
                verified:false,
                relatasUser:false,
                relatasContact:true,
                mobileNumber:null,
                location:null,
                source:'google-email-interactions',
                account:{
                    name:common.fetchCompanyFromEmail(cleanArray[i]),
                    selected:false,
                    updatedOn:new Date()
                }
            };

            contactsArray.push(contactObj2)
        }

        // Creating new Contact Collection
        contactManagementObj.insertContacts(common.castToObjectId(user._id.toString()),user.emailId,cleanArray,contactsArray,user.lastLoginDate,function (cErr,contactsList) {

        });

        contactObj.addContactNotExist(common.castToObjectId(user._id.toString()),user.emailId,contactsArray,[],cleanArray,'mobile',false);
    });
}

function addInteractionsBatch(user,tweetsDirect,tweetsOther,referenceIds,req,callback){

    tweetsDirect = tweetsDirect.concat(tweetsOther);
    
    if(tweetsDirect.length > 0 && referenceIds.length > 0){
        interactionManagementObj.updateEmailInteractions(user,tweetsDirect,referenceIds,function(isSuccess){
            var match = {ownerId:common.castToObjectId(user._id.toString())};

            // interactionManagementObj.removeInvalidInteractions(match,common.getInvalidEmailList());
            // interactionManagementObj.trackResponseEmails(match.ownerId,user.emailId);

            if(req && !isSuccess){
                req.session.interactionsWritten = true;//This is a fallback for unknown reasons so that the interactions loading message is disabled after a while
                req.session.updateActionItemMail = true;
                req.session.save();
                logger.info("Failed to add interactions bulk EMAIL ",user.emailId);
                if(callback){
                    callback(true)
                }
            }

            if(req) {

                req.session.interactionsWritten = isSuccess;//This is for showing the loading your interactions message for only onboarding users.
                req.session.updateActionItemMail = true;
                req.session.save();

                logger.info("Add interactions bulk EMAIL Success and send rabitmq message for GoogleSync",user.emailId);
                //call rabbitmq producer
                var obj = {
                    'userId': user._id.toString(),
                    'afterDate': moment(req.session.afterDateForEmailAnalysis).format('YYYY-MM-DD'),
                    'beforeDate': moment(req.session.beforeDateForEmailAnalysis).format('YYYY-MM-DD')
                };

                rabbitmqObj.producer(obj,function(result){
                    if(result)
                        logger.info("Add OBJ to processQueue Success ",obj);
                    else
                        logger.info("Failed to Add OBJ to processQueue ",obj);
                });
            }

            if(callback){
                callback(true)
            }
        })
    }
    else{
        if(callback){
            callback(true)
        }
    }
}

GoogleCalendar.prototype.getGoogleMeetingInteractions = function(userId,lastLoginDate,googleData,fromOnboarding,req,serviceLogin,callback){
    getGoogleMeetingInteractionsFun(userId,lastLoginDate,googleData,fromOnboarding,req,serviceLogin,callback);
};

//This function actually gets both mail as well as meetings on login.
function getGoogleMeetingInteractionsFun(userId,lastLoginDate,googleData,fromOnboarding,req,serviceLogin,callback){

    if(!serviceLogin || serviceLogin != "outlook"){
        actionItemsManagementObj.setInsightsBuiltFlag(userId,false);
    }

    var endDate = new Date();
    var timeMax = new Date();

    userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,userProfile) {

        if(error){
            logger.error("Profile error")
            userProfile = null;
        } else{
            var userInfo=userProfile;
            if(userProfile.dashboardPopup != true){
                endDate.setDate(endDate.getDate() - 90);
                timeMax.setDate(timeMax.getDate() + 14); //From today, get meetings for the next 15 Days. Earlier it was for 4 Days.Last edited: 17 Feb 2016 by Naveen
            }
            else{
                if(common.checkRequired(lastLoginDate)){
                    endDate = new Date(lastLoginDate);
                    timeMax = new Date();
                    
                    endDate.setDate(endDate.getDate() - 2);
                    timeMax.setDate(timeMax.getDate() + 14); //From today, get meetings for the next 15 Days. Earlier it was for 4 Days.Last edited: 17 Feb 2016 by Naveen
                }
                else{
                    endDate.setDate(endDate.getDate() - 90);
                    timeMax.setDate(timeMax.getDate() + 14); //From today, get meetings for the next 15 Days. Earlier it was for 4 Days.Last edited: 17 Feb 2016 by Naveen
                }
            }

            var endMonth = endDate.getMonth()+1;
            var maxMonth = timeMax.getMonth()+1;
            var filter = {
                timeMin:endDate.getFullYear()+'-'+endMonth+'-'+endDate.getDate()+'T00:00:00.000Z',
                timeMax:timeMax.getFullYear()+'-'+maxMonth+'-'+timeMax.getDate()+'T00:00:00.000Z'
            };

            if(common.checkRequired(googleData)){
                userInfo.google = googleData;
            }

            google.build(function(api) {  // start of google api

                if (!validateGoogleAccount(userInfo)) {
                    logger.error("Google validation error")
                }
                else{

                    userInfo.google.forEach(function(info){ // start of forEach
                        var tokenProvider = new GoogleTokenProvider({
                            refresh_token: info.refreshToken,
                            client_id:authConfig.GOOGLE_CLIENT_ID,
                            client_secret:authConfig.GOOGLE_CLIENT_SECRET,
                            access_token: info.token
                        });

                        tokenProvider.getToken(function (err, token) { // start of token provider
                            if(err || !token){
                                logger.error("Token error")
                            } else getUserEmailListNext (userProfile,token,lastLoginDate,userInfo.dashboardPopup,true,userInfo.emailId,info.emailId,req,false,function (mailsUpdated) {

                                logger.info("Mails Updated...")

                                //This used to be a parallel async process,
                                // but was brought into a callback so that Calendar insights are not duplicated.
                                // Last Edit Naveen @15 Oct 2016
                                api.calendar.events.list(  // calendar event list start
                                    {
                                        calendarId: 'primary',
                                        access_token:token, // You have to supply the OAuth token for the current user
                                        singleEvents:true,
                                        maxResults:2499,
                                        timeMin:endDate.getFullYear()+'-'+endMonth+'-'+endDate.getDate()+'T00:00:00.000Z',
                                        timeMax:timeMax.getFullYear()+'-'+maxMonth+'-'+timeMax.getDate()+'T00:00:00.000Z'
                                    },
                                    function(events) {

                                        if (events.items == undefined || events.items == null || events.items == '' || (events.items && events.items.length == 0)) {
                                            if(callback){
                                                callback()
                                            }
                                            //If there are no meetings scheduled then log in the user.
                                            // actionItemsManagementObj.kickOffActionItemsMailProcess(common.getUserId(req.user),lastLoginDate,req,function(result){});
                                        }
                                        else{
                                            events = events.items;

                                            userManagements.updateDashboardPopup(userId,function(err,result,msg){

                                            });
                                            //loopGoogleEvents(events,true,userInfo.emailId,info.emailId,true);
                                            //If user has meetings then login him in the function googleMeetings_Relatas();
                                            googleMeetings_Relatas(info.emailId,events,true,userInfo.emailId,info.emailId,true,fromOnboarding,lastLoginDate,req,function (err,meetingsUpdated) {
                                                logger.info("Meetings Updated...")
                                                if(callback){
                                                    callback()
                                                }
                                                // actionItemsManagementObj.kickOffActionItemsMailProcess(common.getUserId(req.user),lastLoginDate,req,function(result){});
                                            });
                                        }
                                    }
                                );
                            });
                             // end of calendar event list
                        }); // end of token provider
                    }); // end of forEach
                } // end of if (google profile check)
                // } else res.send(false);
            });  // end of google api
        }
    });

    //Start updating last login date from the interaction collection to the user collection for each contact.
    // This is not waiting for updation of meeting interactions right now.
    //updateLastInteractedDate(userId);
}


function updateMeetingsAndInteractions(userProfile,req,token,callback){
    var userInfo=userProfile,
        endDate = new Date(),
        timeMax = new Date(),
        fromOnboarding = false,
        lastLoginDate = userProfile.lastDataSyncDate;

    if(userProfile.dashboardPopup != true){
        endDate.setDate(endDate.getDate() - 90);
        timeMax.setDate(timeMax.getDate() + 14);
    }
    else{
        if(common.checkRequired(lastLoginDate)){
            endDate = new Date(lastLoginDate);
            timeMax = new Date();

            endDate.setDate(endDate.getDate() - 2);
            timeMax.setDate(timeMax.getDate() + 14);
        }
        else{
            endDate.setDate(endDate.getDate() - 90);
            timeMax.setDate(timeMax.getDate() + 14);
        }
    }

    var endMonth = endDate.getMonth()+1;
    var maxMonth = timeMax.getMonth()+1;
    var filter = {
        timeMin:endDate.getFullYear()+'-'+endMonth+'-'+endDate.getDate()+'T00:00:00.000Z',
        timeMax:timeMax.getFullYear()+'-'+maxMonth+'-'+timeMax.getDate()+'T00:00:00.000Z'
    };

    google.build(function(api) {  // start of google api
        getUserEmailListNext (userProfile,token,lastLoginDate,userInfo.dashboardPopup,true,userInfo.emailId,userProfile.emailId,req,false,function (mailsUpdated) {

            api.calendar.events.list(
                {
                    calendarId: 'primary',
                    access_token:token,
                    singleEvents:true,
                    maxResults:2499,
                    timeMin:endDate.getFullYear()+'-'+endMonth+'-'+endDate.getDate()+'T00:00:00.000Z',
                    timeMax:timeMax.getFullYear()+'-'+maxMonth+'-'+timeMax.getDate()+'T00:00:00.000Z'
                },
                function(events) {

                    if (events.items == undefined || events.items == null || events.items == '') {
                        callback()
                    }
                    else{
                        events = events.items;

                        googleMeetings_Relatas(userProfile.emailId,events,true,userInfo.emailId,userProfile.emailId,true,fromOnboarding,lastLoginDate,req,function (err,meetingsUpdated) {
                            callback()
                        });
                    }
                }
            );
        });
    });
}

GoogleCalendar.prototype.googleMeetingsToRelatas = function(events,office,officeEmailId,googleEmailId, toMeeting, callback){
    googleMeetings_Relatas(events,office,officeEmailId,googleEmailId,toMeeting,callback);
};

function loopGoogleEvents(events,office,officeEmailId,googleEmailId, toMeeting){
    if(checkRequired(events) && events.length > 0){

        for(var i=0; i<events.length; i++){
            if(!toMeeting)
                generateInteraction(events[i],office,officeEmailId,googleEmailId);
            else convertGoogleMeetingsToRelatas(events[i],office,officeEmailId,googleEmailId)
        }
    }
}

function generateInteraction(event,office,officeEmailId,googleEmailId){
    if(checkRequired(event) && checkRequired(event.attendees)){

        for(var par=0; par<event.attendees.length; par++){

            if(event.id.substr(0,12) != 'relatasevent' && event.id.substr(0,6) != 'todoid'){

                if(event.id.substr(0,7) == 'relatas' || event.id.substr(0,5) == 'relat'){

                }
                else{
                    var localStartG = new Date(event.start.dateTime || event.start.date).toISOString();
                    var localEnd = new Date(event.end.dateTime || event.end.date).toISOString();
                    var emailId = event.attendees[par].email;

                    if(office && emailId == googleEmailId){
                        emailId = officeEmailId;
                    }

                    var objG = {
                        userId:'',
                        emailId:emailId,
                        action:event.attendees[par].organizer ? 'sender' : 'receiver',
                        interactionDate:new Date(localStartG),
                        endDate:new Date(localEnd),
                        type:'meeting',
                        subType:'google-meeting',
                        refId:event.id,
                        source:'google',
                        title:event.summary || '',
                        description:''
                    };

                    common.storeInteractionReceiver(objG);
                }
            }
            // }
        }
    }
    else if(checkRequired(event.organizer)){
        var localStartG2 = new Date(event.start.dateTime || event.start.date).toISOString();
        var localEnd2 = new Date(event.end.dateTime || event.end.date).toISOString();
        var emailId2 = event.organizer.email;

        if(office && emailId2 == googleEmailId){
            emailId2 = officeEmailId;
        }

        var objG2 = {
            userId:'',
            emailId:emailId2,
            action:'sender',
            interactionDate:new Date(localStartG2),
            endDate:new Date(localEnd2),
            type:'meeting',
            subType:'google-meeting',
            refId:event.id,
            source:'google',
            title:event.summary || '',
            description:''
        };

        common.storeInteractionReceiver(objG2);
    }
}

GoogleCalendar.prototype.getContactsFromGoogleApi = function(access_token,userId,callback){
    if(common.checkRequired(userId)){
        getContactsFromGoogleApi(access_token,userId,null,callback)
    } else {
        if(callback){
            callback()
        }
    }
};

GoogleCalendar.prototype.getContactsFromGoogleApi_bip = function(access_token,userId,lastLoginDate,callback){
    if(common.checkRequired(userId)){
        getContactsFromGoogleApi(access_token,userId,null,callback)
    } else {
        if(callback){
            callback()
        }
    }
};

GoogleCalendar.prototype.getGoogleContacts = function(googleUser,userId,isLastLoginDate){

    if(common.checkRequired(userId)){
        if(isLastLoginDate){
            userManagements.findUserProfileByIdWithCustomFields(userId,{lastLoginDate:1,emailId:1},function(error,user){
                if(common.checkRequired(user)){
                    getContactsFromGoogleApi(googleUser.accessToken,userId,user.lastLoginDate);
                }
            })
        }
        else getContactsFromGoogleApi(googleUser.accessToken,userId,null);
    }
};

GoogleCalendar.prototype.updateContactsMeetingsEmails = function (userProfile,req,callback) {

    var token = userProfile.google[0].token,
        refreshToken = userProfile.google[0].refreshToken;

    getNewGoogleToken(token,refreshToken,function (newToken) {
        updateMeetingsAndInteractions(userProfile,req,newToken,callback)
        getContactsFromGoogleApi(newToken,userProfile._id,userProfile.lastDataSyncDate);
    });

}

function getContactsFromGoogleApi(access_token,userId,lastLoginDate,callback) {

    var qs;

    if(common.checkRequired(lastLoginDate)){

        var after = new Date(lastLoginDate);
        after.setDate(after.getDate() - 1);
        var monthAfter2 = after.getMonth()+1;

        qs = {
            'alt': 'json',
            'max-results': 10000,
            'orderby': 'lastmodified',
            // 'updated-min':after.toISOString()
        }
    }
    else{
        qs = {
            'alt': 'json',
            'max-results': 10000,
            'orderby': 'lastmodified'
        }
    }

    request.get({

        url: 'https://www.google.com/m8/feeds/contacts/default/full',
        qs: qs,
        headers: {
            'Authorization': 'Bearer ' + access_token,
            'GData-Version': '3.0'
        }
    }, function (err, res, body) {
        if(!res){
            logger.info("Error get google contacts ",body, err);
            if(callback){
                callback()
            }
        }
        else if (res.statusCode === 401) {
            logger.info("Error get google contacts ",body, err);
            if(callback){
                callback()
            }
        }
        else{
            var feed = JSON.parse(body);
            var emailIdArr = [];
            if(feed && feed.feed && feed.feed.entry){
                var users = feed.feed.entry.map(function (c) {
                    var r = {};
                    if (c['gd$name'] && c['gd$name']['gd$fullName']) {
                        r.name = c['gd$name']['gd$fullName']['$t'];
                    }
                    if(c['gContact$birthday'] && c['gContact$birthday'].when){
                        r.birthday = c['gContact$birthday'].when
                    }
                    if(c['gd$organization'] && c['gd$organization'].length >  0){
                        if(c['gd$organization'][0]['gd$orgName'])
                            r.companyName = c['gd$organization'][0]['gd$orgName']['$t'];
                        if(c['gd$organization'][0]['gd$orgTitle'])
                            r.designation = c['gd$organization'][0]['gd$orgTitle']['$t'];
                    }
                    if (c['gd$email'] && c['gd$email'].length > 0) {
                        r.email = c['gd$email'][0]['address'];
                        if(typeof r.email == 'string'){
                            emailIdArr.push(r.email.trim().toLowerCase());
                        }
                        r.nickname = c['gd$email'][0]['address'].split('@')[0];
                    }

                    if(c['gd$phoneNumber']){
                        if(c['gd$phoneNumber'].length > 0){
                            r.phone = c['gd$phoneNumber'][0]['$t'];
                        }
                    }

                    if(c['gd$where'] && c['gd$where'].valueString){
                        r.location = c['gd$where'].valueString
                    }

                    if (c['link']) {
                        var photoLink = c['link'].filter(function(link) {
                            return link.rel == 'http://schemas.google.com/contacts/2008/rel#photo' &&
                                'gd$etag' in link;
                        })[0];
                        if (photoLink) {
                            r.picture = photoLink.href;
                        }
                    }

                    return r;
                })
            }

            if(users && users.length > 0){
                common.getInvalidEmailListFromDb(function (list) {
                    var newArray = [];
                    if (users.length > 0) {
                        for (var i = 0; i < users.length; i++) {
                            var con = addContacts(userId, users[i]);

                            if (con) {
                                if (!con.account.name)
                                    con.account = {};
                                con.account.name = common.fetchCompanyFromEmail(users[i].email)
                                if (con != null) {
                                    newArray.push(con);
                                }
                            }
                        }
                        
                        if (newArray.length > 0) {
                            newArray = common.removeDuplicates_field(newArray, 'personEmailId').filter(function (elem) {
                                var ok = common.isValidEmail(elem.personEmailId, list);

                                return ok;
                            });
                        }
                    }

                    userManagements.findUserProfileByIdWithCustomFields(userId, {
                        skypeId: 1,
                        firstName: 1,
                        lastName: 1,
                        emailId: 1,
                        publicProfileUrl: 1,
                        profilePicUrl: 1,
                        companyName: 1,
                        designation: 1,
                        location: 1,
                        mobileNumber: 1,
                        lastLoginDate:1
                    }, function (error, user) {

                        if (user && common.checkRequired(user._id)) {
                            contactManagementObj.insertContacts(common.castToObjectId(userId),user.emailId,emailIdArr,newArray,user.lastLoginDate,function (cErr,contactsList) {

                                if(callback){
                                    callback()
                                }
                            });
                            
                            aggregateSupportObj.mapUserContactsWithProfile_(user, [], emailIdArr, newArray, false);
                        } else {
                            if(callback){
                                callback()
                            }
                        }
                    });
                });
            } else {
                if(callback){
                    callback()
                }
            }
        }
    });
}

function addContacts(userId,contact){
    var emailId = contact.email;
    var name = contact.name || contact.nickname || contact.email; // Last modified 12 March 16, Naveen. Added 'contact.email'
    var phone = contact.phone;
    var birthday = contact.birthday;
    var companyName = contact.companyName;
    var designation = contact.designation;
    var location = contact.location;
    if(common.checkRequired(userId) && common.checkRequired(name) && common.checkRequired(emailId)){
        emailId = emailId.trim().toLowerCase();
        return {
            personId:null,
            personName:name,
            personEmailId:emailId.trim().toLowerCase(),
            birthday:birthday,
            companyName:companyName,
            designation:designation,
            count:0,
            addedDate:new Date(),
            verified:false,
            relatasUser:false,
            relatasContact:true,
            mobileNumber:phone?phone:null,
            location:location,
            source:'google-login',
            account:{
                name:null,
                selected:false,
                updatedOn:new Date()
            },
            contactImageLink:contact.picture || null
        };
    }
    else return null;
}

function addSingleContact(userId,contactObjU){

    contactObj.addSingleContact(userId,contactObjU,function(error,isAdded,message){

    })
}

function getValidNum(num){
    return num < 10 ? '0'+num : ''+num
}

GoogleCalendar.prototype.getGoogleCalendarEventsByTimezone = function(userProfile,timezone,endDate,timeMax,emailId,callback){

    if(!checkRequired(userProfile)){
        callback(false)
    }
    else{
        var userInfo=userProfile;
        var count = 0;

        var allEvents=[];
        var timezoneCalendar;

        google.build(function(api) {  // start of google api

            if (!validateGoogleAccount(userInfo)) {
                callback(false)
            }
            else{
                userInfo.google.forEach(function(info){ // start of forEach

                    var tokenProvider = new GoogleTokenProvider({
                        refresh_token: info.refreshToken,
                        client_id:     authConfig.GOOGLE_CLIENT_ID,
                        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                        access_token: info.token
                    });

                    tokenProvider.getToken(function (err, token) { // start of token provider
                        if(err){
                            count += 1;
                            if (count == userInfo.google.length) {
                                callback(true);
                            }
                        }
                        api.calendar.events.list(  // calendar event list start
                            {
                                calendarId: 'primary',
                                access_token:token, // You have to supply the OAuth token for the current user
                                singleEvents:true,
                                maxResults:2499,
                                timeMin:endDate,
                                timeMax:timeMax
                            },
                            function(events) {

                                count += 1;
                                if (events == undefined || events ==  null ) {

                                }else{
                                    events = events.items;
                                    googleMeetings_Relatas(info.emailId,events,true,userInfo.emailId,info.emailId,true,false);
                                    //loopGoogleEvents(events.items,true,emailId,info.emailId, true)
                                }
                                if (count == userInfo.google.length) {
                                    callback(true)
                                }
                            }
                        ); // end of calendar event list
                    }); // end of token provider
                }); // end of forEach
            } // end of if (google profile check)
            // } else res.send(false);
        });  // end of google api
    }
};

GoogleCalendar.prototype.removeEventFromGoogleCalendarForMeeting = function(googleEventId,userId,callback){

    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1},function(error,userProfile){
        if(error){
            callback(false)
        }
        else{
            var userInfo = userProfile;
            if(!validateGoogleAccount(userInfo)){
                logger.info('No Google account found in '+userInfo.firstName+'account to remove event from google calendar');
                callback(false)
            }
            else{
                var tokenProvider = new GoogleTokenProvider({
                    refresh_token: userInfo.google[0].refreshToken,
                    client_id:     authConfig.GOOGLE_CLIENT_ID,
                    client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                    access_token: userInfo.google[0].token
                });
                tokenProvider.getToken(function (err, token) {
                    if (err) {
                        logger.info('Error in token provider module');
                    }

                    var google_calendar = new gcal.GoogleCalendar(token);
                    var calendarId='primary';

                    google_calendar.events.delete(calendarId,googleEventId,function(err,result){

                        if(err){
                            logger.info('Error in Google event delete'+JSON.stringify(err));
                            if(err.errors && err.errors[0] && (err.errors[0].reason == 'deleted' || err.errors[0].reason == 'notFound')){
                                callback(true)
                            }
                            else callback(false)

                        }
                        else{
                            logger.info('Deleting event in google success');
                            callback(true)
                        }
                    });
                }); // end of token provider
            }
        }
    })
};

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

/* Google meetings to Relatas meetings */

function convertGoogleMeetingsToRelatas(event,office,officeEmailId,googleEmailId){

    var meeting = {
        selfCalendar:true,
        suggested:false,
        readStatus:false,
        isGoogleMeeting:true,
        toList:[],
        scheduleTimeSlots:[],
        participants:[],
        docs:[]
    };

    if(common.checkRequired(event) && common.checkRequired(event.attendees) && event.attendees.length > 0){
        if(event.id.substr(0,12) != 'relatasevent' && event.id.substr(0,6) != 'todoid'){

            if(event.id.substr(0,7) == 'relatas' || event.id.substr(0,5) == 'relat'){

            }
            else{
                var organizer;
                if(common.checkRequired(event.organizer)){
                    organizer = event.organizer.email == googleEmailId ? officeEmailId : event.organizer.email;
                    meeting.participants.push({emailId:organizer})

                }
                else if(common.checkRequired(event.creator)){
                    organizer = event.creator.email == googleEmailId ? officeEmailId : event.creator.email;
                    meeting.participants.push({emailId:organizer})
                }

                meeting.invitationId = event.id;
                meeting.googleEventId = event.id;
                meeting.scheduledDate = new Date(event.created);
                var isAccepted = false;

                for(var attendee = 0; attendee<event.attendees.length; attendee++){
                    if(common.checkRequired(event.attendees[attendee])){
                        if(event.attendees[attendee].organizer){
                            meeting.senderId = '',
                                meeting.senderName = common.checkRequired(event.attendees[attendee].displayName) ? event.attendees[attendee].displayName : '',
                                meeting.senderEmailId = event.attendees[attendee].email == googleEmailId ? officeEmailId : event.attendees[attendee].email
                            meeting.senderPicUrl = '';
                            if(meeting.senderEmailId != organizer)
                                meeting.participants.push({emailId:meeting.senderEmailId})
                        }
                        else{
                            var to = {
                                receiverId:'',
                                receiverFirstName:common.checkRequired(event.attendees[attendee].displayName) ? event.attendees[attendee].displayName : '',
                                receiverLastName:'',
                                receiverEmailId:event.attendees[attendee].email == googleEmailId ? officeEmailId : event.attendees[attendee].email,
                                isAccepted:event.attendees[attendee].responseStatus == "accepted",
                                canceled:event.attendees[attendee].responseStatus == "declined"
                            }

                            if(to.receiverEmailId != organizer)
                                meeting.participants.push({emailId:to.receiverEmailId})

                            meeting.toList.push(to)
                            if(!isAccepted)
                                isAccepted = event.attendees[attendee].responseStatus == "accepted"
                        }
                    }
                }
                var slot =  {
                    isAccepted: isAccepted,
                    start: {
                        date: new Date(event.start.dateTime || event.start.date)
                    },
                    end: {
                        date: new Date(event.end.dateTime || event.end.date)
                    },
                    title: event.summary,
                    location: event.location,
                    suggestedLocation: '',
                    description: event.description
                };

                if(slot.location){
                    slot.locationType = common.setMeetingLocationType(slot.location)?common.setMeetingLocationType(slot.location):"In-Person"
                }

                meeting.scheduleTimeSlots.push(slot);

                //meetings.push(meeting)
                if(meeting.scheduleTimeSlots.length > 0 && meeting.toList.length > 0){
                    mapUserProfilesToMeeting(meeting);
                }
            }
        }
    }
}

function mapUserProfilesToMeeting(meeting){
    userManagements.findUserProfileByEmailIdWithCustomFields(meeting.senderEmailId,{firstName:1,lastName:1,emailId:1,profilePicUrl:1},function(err,user){
        if(common.checkRequired(user)){
            meeting.senderEmailId = user.emailId
            meeting.senderId = user._id
            meeting.senderName = user.firstName+' '+user.lastName
            meeting.senderPicUrl = user.profilePicUrl
        }
        beforeValidateAndStoreMeeting(meeting)
    })
}

function beforeValidateAndStoreMeeting(meeting){
    var receivers = [];
    for(var i=0; i<meeting.toList.length; i++){
        receivers.push(meeting.toList[i].receiverEmailId);
    }
    userManagements.selectUserProfilesCustomQuery({emailId:{$in:receivers}},{firstName:1,lastName:1,emailId:1},function(err,users){
        if(common.checkRequired(users) && users.length > 0){
            for(var user=0; user<users.length; user++){
                for(var i=0; i<meeting.toList.length; i++){
                    if(meeting.toList[i].receiverEmailId == users[user].emailId){
                        meeting.toList[i].receiverId = users[user]._id
                        meeting.toList[i].receiverFirstName = users[user].firstName
                        meeting.toList[i].receiverLastName = users[user].lastName
                        meeting.toList[i].receiverEmailId = users[user].emailId
                    }
                }
            }
        }

        validateAndStoreMeeting(meeting)
    })
}

function validateAndStoreMeeting(meeting){
    userManagements.findInvitationByIdCustomFields(meeting.invitationId,{scheduledDate:0},true,function(invi){
        if(!common.checkRequired(invi)){
            userManagements.scheduleInvitationSelf(meeting,function(er,invi,msg){
                if(common.checkRequired(invi)){

                    var accepted = false;
                    for (var i = 0; i < invi.toList.length; i++) {
                        if (invi.toList[i].isAccepted) {
                            accepted = true;
                            storeInteractionGoogle(invi.toList[i].receiverId, invi.toList[i].receiverEmailId, 'receiver', invi.scheduleTimeSlots[0].start.date, invi.scheduleTimeSlots[0].end.date, invi.invitationId, invi.scheduleTimeSlots[0].title, invi.scheduleTimeSlots[0].description);
                            storeInteractionGoogle(invi.senderId, invi.senderEmailId, 'sender', invi.scheduleTimeSlots[0].start.date, invi.scheduleTimeSlots[0].end.date, invi.invitationId, invi.scheduleTimeSlots[0].title, invi.scheduleTimeSlots[0].description);
                        }
                        else {
                            interactionManagementObj.removeUserInteractionRefIdEmailId(invi.invitationId, invi.toList[i].receiverEmailId)
                        }
                    }

                    if (!accepted) {
                        interactionManagementObj.removeInteractions(invi.invitationId);
                    }
                }
            });
        }
        else{
            updateExistingGoogleMeeting(meeting,invi);
        }
    })
}

function updateExistingGoogleMeeting(meeting,invi){

    // Making update Slot
    var updateSlot = false;
    var slotOld = invi.scheduleTimeSlots[0]
    var slotNew = meeting.scheduleTimeSlots[0]

    var upSlot = {
        start:{},
        end:{}
    };
    if(slotOld.title != slotNew.title && common.checkRequired(slotNew.title)){
        updateSlot = true;
        upSlot.title = slotNew.title
    }
    else upSlot.title = slotOld.title

    if(slotOld.description != slotNew.description && common.checkRequired(slotNew.description)){
        updateSlot = true;
        upSlot.description = slotNew.description
    }
    else upSlot.description = slotOld.description

    if(new Date(slotOld.start.date) != new Date(slotNew.start.date) || new Date(slotOld.end.date) != new Date(slotNew.end.date)){
        updateSlot = true;
        upSlot.start.date = new Date(slotNew.start.date)
        upSlot.end.date = new Date(slotNew.end.date)
    }
    else upSlot.start.date = new Date(slotOld.start.date)

    upSlot.isAccepted = slotNew.isAccepted
    upSlot.messages = slotOld.messages
    //upSlot._id = slotOld._id

    if(slotOld.location != slotNew.location && common.checkRequired(slotNew.location)){
        updateSlot = true;
        upSlot.location = slotNew.location
    }
    else upSlot.location = slotOld.location

    if(slotOld.suggestedLocation != slotNew.suggestedLocation && common.checkRequired(slotNew.suggestedLocation)){
        updateSlot = true;
        upSlot.suggestedLocation = slotNew.suggestedLocation
    }
    else upSlot.suggestedLocation = slotOld.suggestedLocation

    if(slotOld.locationType != slotNew.locationType && common.checkRequired(slotNew.locationType)){
        updateSlot = true;
        upSlot.locationType = slotNew.locationType
    }
    else upSlot.locationType = slotOld.locationType



    // Making updated toList
    var newList = []
    var removeParticipants = false;

    if(meeting.toList.length > 0){
        newList = meeting.toList;
    }
    else{
        removeParticipants = true;
    }

    /*-------------------------------*/

    // Making updated participant List
    var newPList = []
    var removeParticipantsP = false;

    if(invi.participants.length > 0){
        newPList = meeting.participants;
    }
    else{
        removeParticipantsP = true;
    }


    /*-------------------------------*/
    // Making final update Obj
    var mUpdateObj = {

    };
    var updateFlagFinal = false;

    if(updateSlot){
        // update Meeting Slot
        mUpdateObj.$set = {
            scheduleTimeSlots:[upSlot]
        }
        updateFlagFinal = true;
    }

    //if(removeParticipants){
    if(common.checkRequired(mUpdateObj.$set)){
        mUpdateObj.$set.toList = newList
    }
    else{
        mUpdateObj.$set = {
            toList:newList
        }
    }
    updateFlagFinal = true;


    //if(removeParticipantsP){
    if(common.checkRequired(mUpdateObj.$set)){
        mUpdateObj.$set.participants = newPList
    }
    else{
        mUpdateObj.$set = {
            participants:newPList
        }
    }

    if(common.checkRequired(mUpdateObj.$set)){
        mUpdateObj.$set.readStatus = upSlot.isAccepted
    }
    else{
        mUpdateObj.$set = {
            readStatus:upSlot.isAccepted
        }
    }

    if(common.checkRequired(mUpdateObj.$set)){
        updateFlagFinal = true;
        mUpdateObj.$set.senderId = meeting.senderId
        mUpdateObj.$set.senderEmailId = meeting.senderEmailId
        mUpdateObj.$set.senderPicUrl = meeting.senderPicUrl || ''
        mUpdateObj.$set.senderName = meeting.senderName
    }
    else{
        updateFlagFinal = true;
        mUpdateObj.$set = {
            senderId:meeting.senderId,
            senderEmailId:meeting.senderEmailId,
            senderPicUrl:meeting.senderPicUrl,
            senderName:meeting.senderName
        }
    }

    if (updateFlagFinal) {
        meetingClassObj.updateMeetingCustomObj(invi.invitationId, mUpdateObj, function (result) {
            if (result) {
                userManagements.findInvitationByIdCustomFields(invi.invitationId, {scheduledDate: 0}, false,function (invitation) {
                    if (common.checkRequired(invitation)) {

                        var accepted = false;
                        for (var i = 0; i < invitation.toList.length; i++) {
                            if (invitation.toList[i].isAccepted) {
                                accepted = true;
                                storeInteractionGoogle(invitation.toList[i].receiverId, invitation.toList[i].receiverEmailId, 'receiver', invitation.scheduleTimeSlots[0].start.date, invitation.scheduleTimeSlots[0].end.date, invitation.invitationId, invitation.scheduleTimeSlots[0].title, invitation.scheduleTimeSlots[0].description);
                                storeInteractionGoogle(invitation.senderId, invitation.senderEmailId, 'sender', invitation.scheduleTimeSlots[0].start.date, invitation.scheduleTimeSlots[0].end.date, invitation.invitationId, invitation.scheduleTimeSlots[0].title, invitation.scheduleTimeSlots[0].description);
                            }
                            else {
                                interactionManagementObj.removeUserInteractionRefIdEmailId(invitation.invitationId, invitation.toList[i].receiverEmailId)
                            }
                        }

                        if (!accepted) {
                            interactionManagementObj.removeInteractions(invitation.invitationId);
                        }
                    }
                })
            }
        });
    }
}

function storeInteractionGoogle(userId,emailId,action,interactionDate,endDate,refId,title,description){

    return {
        userId:userId,
        emailId:emailId,
        action:action,
        interactionDate:interactionDate,
        endDate:endDate,
        type:'meeting',
        subType:'In-Person',
        refId:refId,
        source:'google',
        title:title,
        description:description
    };
}

GoogleCalendar.prototype.acceptGoogleMeeting = function(userId,googleEventId,maxSlot,status,invitation,callback){
    updateGoogleMeetingToAccepted(userId,googleEventId,maxSlot,status,invitation,callback)
};

GoogleCalendar.prototype.updateExistingGoogleMeetingNew = function(userId,googleEventId,oldInvitation,newInvitation,callback){
    updateExistingGoogleMeetingNew(userId,googleEventId,oldInvitation,newInvitation,callback)
};

function updateExistingGoogleMeetingNew(userId,googleEventId,oldInvitation,newInvitation,callback){
    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1}, function (error, userProfile) {
        if (error || !checkRequired(userProfile)) {

            logger.error('Error occurred while searching authenticated user profile in the DB');
            userProfile = null;
            callback(false);
        }
        else {

            var userInfo = userProfile;
            if (validateGoogleAccount(userInfo)) {

                getGoogleTokens(userInfo, function (err, token) {
                    if (err) {
                        logger.info('Error in token provider' + err);
                    }

                    var google_calendar = new gcal.GoogleCalendar(token);
                    var calendarId = 'primary';

                    var attendees = [
                        {
                            "email": userInfo.emailId,
                            "responseStatus": "accepted"
                        }
                    ];

                    oldInvitation.participants.forEach(function(a){
                        if(userInfo.emailId != a.emailId){
                            attendees.push({"email": a.emailId})
                        }
                    })

                    var request_body = {
                        id:googleEventId,
                        summary:newInvitation.scheduleTimeSlots[0].title,
                        attendees:attendees,
                        end:{
                            dateTime:new Date(newInvitation.scheduleTimeSlots[0].end.date)
                        },
                        start:{
                            dateTime:new Date(newInvitation.scheduleTimeSlots[0].start.date)
                        },
                        description:newInvitation.scheduleTimeSlots[0].description,
                        location:newInvitation.scheduleTimeSlots[0].suggestedLocation
                    };

                    //request_body.sequence = 3;

                    google_calendar.events.update(calendarId, googleEventId, request_body,{sendNotifications:true}, function (err, result) {

                        if (err) {
                            logger.error('creating event in google failed '+JSON.stringify(err));
                            callback(false)
                        }
                        else {
                            callback(result)
                        }
                    });
                }); // end of token provider
            } else callback(false);
        }
    })
}

function updateGoogleMeetingToAccepted(userId,googleEventId,maxSlot,status,invitation,callback){

    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1}, function (error, userProfile) {
        if (error || !checkRequired(userProfile)) {

            logger.error('Error occurred while searching authenticated user profile in the DB');
            userProfile = null;
            callback(false);
        }
        else {

            var userInfo = userProfile;
            if (validateGoogleAccount(userInfo)) {

                getGoogleTokens(userInfo, function (err, token) {
                    if (err) {
                        logger.info('Error in token provider' + err);
                    }

                    var google_calendar = new gcal.GoogleCalendar(token);
                    var calendarId = 'primary';

                    if(invitation && invitation.toList){
                        var attendees = []
                        invitation.toList.forEach(function(a){
                            var obj = {
                                "email": a.receiverEmailId
                            }
                            if(a.isAccepted){
                                obj.responseStatus = "accepted";
                            }
                            attendees.push(obj);
                        });
                    }else {

                        var attendees = [
                            {
                                "email": userInfo.emailId,
                                "responseStatus": status || "accepted"
                            }
                        ];
                    }

                    var locationType = maxSlot.locationType || ''
                    var location = maxSlot.location || ''
                    var loctionAndLocationType = ''
                    if(common.checkRequired(locationType) && common.checkRequired(location)){
                        loctionAndLocationType = locationType+': '+location;
                    }

                    var request_body = {
                        id:googleEventId,
                        summary:maxSlot.title,
                        attendees:attendees,
                        end:{
                            dateTime:new Date(maxSlot.end.date)
                        },
                        start:{
                            dateTime:new Date(maxSlot.start.date)
                        },
                        description:maxSlot.description,
                        location:location
                    };

                    google_calendar.events.update(calendarId, googleEventId, request_body, function (err, result) {

                        if (err) {
                            logger.error('Updating event in google failed '+JSON.stringify(err));
                            callback(false)
                        }
                        else {
                            callback(result)
                        }
                    });
                }); // end of token provider
            } else callback(false);
        }
    })
}

// New Code for Google meetings to Relatas

function googleMeetings_Relatas(calendarEmail,events,office,officeEmailId,googleEmailId,toMeeting,fromOnboarding,lastLoginDate,req,callback){
    
    if(checkRequired(events) && events.length > 0 && toMeeting){
        var meetingsArr = [];
        var emailIdArr = [];
        for(var i=0; i<events.length; i++){
            var obj = generateMeetingWithGEvent(events[i],googleEmailId,officeEmailId);

            if(obj.emailIdArr.length > 0 && obj.meeting != null){
                meetingsArr.push(obj.meeting);
                emailIdArr = emailIdArr.concat(obj.emailIdArr);
            }
        }

        if(meetingsArr.length > 0 && emailIdArr.length > 0){
            emailIdArr = common.removeDuplicate_id(emailIdArr,true);

            userManagements.selectUserProfilesCustomQuery({emailId:{$in:emailIdArr}},{contacts:0},function(error,users){
                
                if(!error && users.length > 0){
                    for(var user=0; user<users.length; user++){
                        for(var meeting=0; meeting<meetingsArr.length > 0; meeting++){
                            
                            if(meetingsArr[meeting].senderEmailId == users[user].emailId){
                                meetingsArr[meeting].senderEmailId = users[user].emailId
                                meetingsArr[meeting].senderId = users[user]._id.toString()
                                meetingsArr[meeting].senderName = users[user].firstName+' '+users[user].lastName
                                meetingsArr[meeting].senderPicUrl = users[user].profilePicUrl
                                meetingsArr[meeting].mobileNumber = users[user].mobileNumber
                                meetingsArr[meeting].location = users[user].location
                                meetingsArr[meeting].publicProfileUrl = users[user].publicProfileUrl
                                meetingsArr[meeting].designation = users[user].designation
                                meetingsArr[meeting].companyName = users[user].companyName
                            }
                            if(common.checkRequired(meetingsArr[meeting].toList) && meetingsArr[meeting].toList.length > 0){
                                for(var i=0; i<meetingsArr[meeting].toList.length; i++){
                                    if(meetingsArr[meeting].toList[i].receiverEmailId == users[user].emailId){
                                        meetingsArr[meeting].toList[i].receiverId = users[user]._id.toString()
                                        meetingsArr[meeting].toList[i].receiverFirstName = users[user].firstName
                                        meetingsArr[meeting].toList[i].receiverLastName = users[user].lastName
                                        meetingsArr[meeting].toList[i].receiverEmailId = users[user].emailId
                                        meetingsArr[meeting].toList[i].picUrl = users[user].profilePicUrl
                                        meetingsArr[meeting].toList[i].mobileNumber = users[user].mobileNumber
                                        meetingsArr[meeting].toList[i].location = users[user].location
                                        meetingsArr[meeting].toList[i].publicProfileUrl = users[user].publicProfileUrl
                                        meetingsArr[meeting].toList[i].designation = users[user].designation
                                        meetingsArr[meeting].toList[i].companyName = users[user].companyName
                                    }
                                }
                            }
                        }
                    }
                }
                logger.info('Getting meetings for onboarding users',fromOnboarding);
                if(fromOnboarding){
                    //For a new user don't redirect here to not set the session headers again.
                    google_relatas_final(calendarEmail,users[0]._id,meetingsArr,lastLoginDate,req,users,callback);

                } else {
                    //res.redirect('/');
                    google_relatas_final(calendarEmail,users[0]._id,meetingsArr,lastLoginDate,req,users,callback);
                }
            })
        }
        else if(callback) {
            callback(true)
        } else {
            if(callback) {
                callback(true)
            }
        }
    }
    else if(callback){
        callback(true);
    }
}

function google_relatas_final(calendarEmail,userId,meetings,lastLoginDate,req,users,callback){
    //TODO update interactions collection and participants as contacts.
    // This function gets an array of refId that have to be added to the interactions collection.

    var usersDictionary = {},
        usersEmailIdObj = {};

    _.each(users,function (user) {
        usersEmailIdObj[user.emailId] = user.emailId;
        usersDictionary[String(user._id)] = user
    });

    meetingClassObj.bulkMeetingsUpdate(calendarEmail,meetings,function (nonExistingMeetings) {
        myUserCollection.findOne({emailId:calendarEmail},{_id:1,emailId:1,lastLoginDate:1,companyId:1}).exec(function(error, userPro) {

            if (error && !userPro) {
                logger.info("ERROR: in insertMeetingsInBulk - outlook", error);
                if (callback) {
                    callback(false);
                }
            }
            else {

                var newInteractions = buildObjForNewMeetingInteractions(userPro,meetings);

                interactionManagementObj.updateNewInteractionsCollection(common.castToObjectId(userPro._id.toString()),userPro,newInteractions);

                if (nonExistingMeetings.length > 0) {
                    loggerGoogle.info("Non Existing Meetings - ", nonExistingMeetings.length);
                    var len = nonExistingMeetings.length;

                    var meetingInteractions = [],refIdArr = [],userIds = [];
                    for (var i = 0; i < len; i++) {
                        meetingInteractions.push(createMeetingInteractionsObjects(nonExistingMeetings[i], null, false));
                        refIdArr.push(nonExistingMeetings[i].invitationId)
                    }

                    var interactionsToSave = [];

                    _.each(meetingInteractions,function (meetingInteractions) {
                        var interactionsForUsers = _.uniq(_.compact(_.pluck(meetingInteractions, "userId")))
                        _.each(interactionsForUsers,function (userId) {
                            var userDetails = usersDictionary[userId];
                            _.each(meetingInteractions,function (el) {
                                el["userId"] = el.userId?common.castToObjectId(el.userId):null
                                el["ownerId"] = userDetails._id
                                el["ownerEmailId"] = userDetails.emailId;
                                el["companyId"] = userPro.companyId?common.castToObjectId(String(userPro.companyId)):null;
                                interactionsToSave.push(_.clone(el))
                            });
                        });
                    });

                    interactionManagementObj.insertInteractions(common.castToObjectId(String(userId)),interactionsToSave,userPro,function () {
                        if(callback){
                            callback(true)
                        }
                    });
                } else {
                    if(callback){
                        callback(true)
                    }
                }

                meetingClassObj.updateScheduleTimeSlotsBulk(meetings,function (forMeetingInteractionUpdate) {
                    interactionManagementObj.updateInteractionDateById(forMeetingInteractionUpdate,usersEmailIdObj,function (result) {
                    })
                });
            }
        });
    });
}

function google_relatas_execute(calendarEmail,meeting,meetings,lastLoginDate,userId){
    meetingClassObj.updateCreateMeeting(calendarEmail,meeting,meetings,lastLoginDate,function(invitation){
        if (invitation && common.checkRequired(invitation.toList) && invitation.toList.length) {
            storeCreateMeetingInteractions(invitation,null,false);
            addParticipantsAsContacts(invitation);
            //Update last interacted date for each contact after meetings interaction have been added.
            //updateLastInteractedDate(userId);
        }
    })
}

function getInteractionsCopy(user,interactions){
    _.each(interactions,function (el) {
        el["ownerId"] = user._id;
        el["ownerEmailId"] = user.emailId;
    });

    return _.flatten(interactions);
}

/* New for create interactions on meeting create. //TODO this function maybe Deprecated.Verify and delete function */
function storeCreateMeetingInteractions(meeting,suggestedUserId,isSuggested){
    var allMeetingInteractions = [];
    var date = common.checkRequired(meeting.scheduledDate) ? new Date(meeting.scheduledDate) : new Date();
    var trackInfo = {action:"request"};
    if(isSuggested){
        trackInfo = {action:"re scheduled"};
        date = meeting.suggestedBy.suggestedDate ? new Date(meeting.suggestedBy.suggestedDate) : new Date()
    }
    var senderI = common.getInteractionObject(meeting.senderId,null,null,meeting.senderName,null,null,meeting.senderPicUrl,null,null,null,null,"sender","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.senderEmailId,meeting.invitationId,'google',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null);
    allMeetingInteractions.push(senderI);
    var emailIdList = [meeting.senderEmailId];
    if(meeting.selfCalendar && meeting.toList && meeting.toList.length>0){
        for(var i=0; i<meeting.toList.length; i++){
            emailIdList.push(meeting.toList[i].receiverEmailId);
            allMeetingInteractions.push(common.getInteractionObject(meeting.toList[i].receiverId,null,null,meeting.toList[i].receiverFirstName,meeting.toList[i].receiverLastName,null,null,null,null,null,null,"receiver","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.toList[i].receiverEmailId,meeting.invitationId,'google',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null));
        }
    }
    else if(meeting.to && common.checkRequired(meeting.to.receiverEmailId)){
        emailIdList.push(meeting.to.receiverEmailId);
        allMeetingInteractions.push(common.getInteractionObject(meeting.to.receiverId,null,null,meeting.to.receiverName,null,null,null,null,null,null,null,"receiver","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.to.receiverEmailId,meeting.invitationId,'google',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null));
    }

    userManagements.selectUserProfilesCustomQuery({emailId:{$in:emailIdList}},function(error,users){
        if(!error && users && users.length > 0){
            for(var inter=0; inter<allMeetingInteractions.length; inter++){
                for(var user=0; user<users.length; user++){
                    if (allMeetingInteractions[inter].emailId == users[user].emailId) {
                        allMeetingInteractions[inter].userId = users[user]._id;
                        allMeetingInteractions[inter].firstName = users[user].firstName;
                        allMeetingInteractions[inter].lastName = users[user].lastName;
                        allMeetingInteractions[inter].publicProfileUrl = users[user].publicProfileUrl;
                        allMeetingInteractions[inter].profilePicUrl = users[user].profilePicUrl;
                        allMeetingInteractions[inter].companyName = users[user].companyName;
                        allMeetingInteractions[inter].designation = users[user].designation;
                        allMeetingInteractions[inter].location = users[user].location;
                        allMeetingInteractions[inter].mobileNumber = users[user].mobileNumber;
                        allMeetingInteractions[inter].emailId = users[user].emailId;
                    }
                }
            }
        }

        if(allMeetingInteractions.length > 0){
            var senderUser = {_id:null,emailId:meeting.senderEmailId,forMeeting:true};
            if(common.checkRequired(meeting.senderId)){
                senderUser = {_id:common.castToObjectId(meeting.senderId),emailId:meeting.senderEmailId,forMeeting:true};
            }
//
            if(isSuggested && common.checkRequired(suggestedUserId)){
                for(var s=0; s<allMeetingInteractions.length; s++){
                    if(allMeetingInteractions[s].userId == suggestedUserId){
                        allMeetingInteractions[s].action = 'sender';
                        senderI = allMeetingInteractions[s];
                        senderUser = {_id:common.castToObjectId(suggestedUserId),emailId:allMeetingInteractions[s].emailId,forMeeting:true};
                    }
                    else allMeetingInteractions[s].action = 'receiver';
                }
            }

            // //var senderUser = {_id:common.castToObjectId(meeting.senderId),emailId:meeting.senderEmailId,forMeeting:true};
            if(common.checkRequired(senderUser._id)){
                interactionManagementObj.addInteractionsBulk(senderUser,allMeetingInteractions,[],function(){});
            }

            createInteractionsReceivers(allMeetingInteractions,senderI);
        }
    });
}

GoogleCalendar.prototype.createMeetingsInteractionsObjects_ = function(meetings,suggestedUserId,isSuggested){
    return createMeetingInteractionsObjects(meetings,suggestedUserId,isSuggested)
}

/*Create Meeting's Interactions object for Bulk Operations. Naveen 12 May 16*/
function createMeetingInteractionsObjects(meeting,suggestedUserId,isSuggested){
    var allMeetingInteractions = [];
    var date = common.checkRequired(meeting.scheduleTimeSlots[0].start.date) ? new Date(meeting.scheduleTimeSlots[0].start.date) : new Date();
    var trackInfo = {action:"request"};
    if(isSuggested){
        trackInfo = {action:"re scheduled"};
        date = meeting.suggestedBy.suggestedDate ? new Date(meeting.suggestedBy.suggestedDate) : new Date()
    }
    // var senderI = common.getInteractionObject(userId,user_twitter_id,user_twitter_name,firstName,lastName,publicProfileUrl,profilePicUrl,companyName,designation,location,mobileNumber,action,interactionType,subType,emailId,refId,source,title,description,interactionDate,endDate,trackId,trackInfo,emailContentId,googleAccountEmailId);
    var senderI = common.getInteractionObject(meeting.senderId,null,null,meeting.senderName,null,meeting.publicProfileUrl,meeting.senderPicUrl,null,meeting.designation,meeting.location,meeting.mobileNumber,"sender","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.senderEmailId,meeting.invitationId,'google',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null);
    allMeetingInteractions.push(senderI);
    var emailIdList = [meeting.senderEmailId];
    if(meeting.selfCalendar && meeting.toList && meeting.toList.length>0){
        for(var i=0; i<meeting.toList.length; i++){
            emailIdList.push(meeting.toList[i].receiverEmailId);
            allMeetingInteractions.push(common.getInteractionObject(meeting.toList[i].receiverId,null,null,meeting.toList[i].receiverFirstName,meeting.toList[i].receiverLastName,meeting.toList[i].publicProfileUrl,meeting.toList[i].picUrl,null,meeting.toList[i].designation,meeting.toList[i].location,meeting.toList[i].mobileNumber,"receiver","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.toList[i].receiverEmailId,meeting.invitationId,'google',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null));
        }
    }
    else if(meeting.to && common.checkRequired(meeting.to.receiverEmailId)){
        emailIdList.push(meeting.to.receiverEmailId);
        allMeetingInteractions.push(common.getInteractionObject(meeting.to.receiverId,null,null,meeting.to.receiverName,null,null,null,null,null,null,null,"receiver","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.to.receiverEmailId,meeting.invitationId,'google',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null));
    }

    return allMeetingInteractions;
}

function createInteractionsReceivers(allMeetingInteractions,senderI){
    for(var i=0; i<allMeetingInteractions.length; i++){
        if(allMeetingInteractions[i].action == 'receiver' && common.checkRequired(allMeetingInteractions[i].userId)){
            var user = {_id:common.castToObjectId(allMeetingInteractions[i].userId),emailId:allMeetingInteractions[i].emailId,forMeeting:true}
            interactionManagementObj.addInteractionsBulk(user,[allMeetingInteractions[i],senderI],[],function(){});
        }
    }
}

//Prototype for creating meetings -- Mobile
GoogleCalendar.prototype.google_relatas_execute_ = function(calendarEmail,meeting){
    google_relatas_execute(calendarEmail,meeting)
}

function generateMeetingWithGEvent(event,googleEmailId,officeEmailId){
    
    var meeting = {
        selfCalendar:true,
        suggested:false,
        readStatus:false,
        isGoogleMeeting:true,
        toList:[],
        scheduleTimeSlots:[],
        participants:[],
        docs:[]
    };
    var emailIdArr = [];

    if(common.checkRequired(event)){
        if(event.id.substr(0,12) != 'relatasevent' && event.id.substr(0,6) != 'todoid'){

            if(event.id.substr(0,7) == 'relatas' || event.id.substr(0,5) == 'relat'){
                return {meeting:null,emailIdArr:emailIdArr}
            }
            else{
                var organizer;
                if(common.checkRequired(event.organizer)){
                    organizer = event.organizer.email == googleEmailId ? officeEmailId : event.organizer.email;

                    meeting.senderId = '';
                    meeting.senderName = common.checkRequired(event.organizer.displayName) ? event.organizer.displayName : '';
                    meeting.senderEmailId = organizer;
                    meeting.senderPicUrl = '';

                    meeting.participants.push({emailId:organizer});
                    emailIdArr.push(organizer)
                }
                else if(common.checkRequired(event.creator)){
                    organizer = event.creator.email == googleEmailId ? officeEmailId : event.creator.email;

                    meeting.senderId = '';
                    meeting.senderName = common.checkRequired(event.creator.displayName) ? event.creator.displayName : '';
                    meeting.senderEmailId = organizer;
                    meeting.senderPicUrl = '';
                    emailIdArr.push(organizer)
                    meeting.participants.push({emailId:organizer})
                }

                meeting.invitationId = event.id;
                meeting.googleEventId = event.id;
                meeting.scheduledDate = new Date(event.created);
                var isAccepted = false;
                if(common.checkRequired(event.attendees) && event.attendees.length > 0){
                    for(var attendee = 0; attendee<event.attendees.length; attendee++){
                        if(common.checkRequired(event.attendees[attendee])){
                            if(event.attendees[attendee].organizer){
                                meeting.senderId = '',
                                    meeting.senderName = common.checkRequired(event.attendees[attendee].displayName) ? event.attendees[attendee].displayName : '',
                                    meeting.senderEmailId = event.attendees[attendee].email == googleEmailId ? officeEmailId : event.attendees[attendee].email
                                meeting.senderPicUrl = '';
                                if(meeting.senderEmailId != organizer){
                                    emailIdArr.push(meeting.senderEmailId)
                                    meeting.participants.push({emailId:meeting.senderEmailId})
                                }
                            }
                            else{
                                var to = {
                                    receiverId:'',
                                    receiverFirstName:common.checkRequired(event.attendees[attendee].displayName) ? event.attendees[attendee].displayName : '',
                                    receiverLastName:'',
                                    receiverEmailId:event.attendees[attendee].email == googleEmailId ? officeEmailId : event.attendees[attendee].email,
                                    isAccepted:event.attendees[attendee].responseStatus == "accepted",
                                    canceled:event.attendees[attendee].responseStatus == "declined"
                                }

                                if(to.receiverEmailId != organizer){
                                    emailIdArr.push(to.receiverEmailId)
                                    meeting.participants.push({emailId:to.receiverEmailId})
                                }

                                meeting.toList.push(to)
                                if(!isAccepted)
                                    isAccepted = event.attendees[attendee].responseStatus == "accepted"
                            }
                        }
                    }
                }

                var slot =  {

                    _id: new customObjectId(),
                    isAccepted: isAccepted,
                    start: {
                        date: new Date(event.start.dateTime || event.start.date)
                    },
                    end: {
                        date: new Date(event.end.dateTime || event.end.date)
                    },
                    title: event.summary,
                    location: event.location,
                    suggestedLocation: '',
                    description: event.description
                };

                if(slot.location){
                    slot.locationType = common.setMeetingLocationType(slot.location)?common.setMeetingLocationType(slot.location):"In-Person"
                }

                meeting.scheduleTimeSlots.push(slot);
                meeting.readStatus = isAccepted;

                if(meeting.scheduleTimeSlots.length > 0){
                    return {meeting:meeting,emailIdArr:emailIdArr}
                }
                else return {meeting:null,emailIdArr:emailIdArr}
            }
        }
        else return {meeting:null,emailIdArr:emailIdArr}
    }
    else return {meeting:null,emailIdArr:emailIdArr}
}

//Prototype for Mobile Sync. -- Rajiv.
GoogleCalendar.prototype.generateMeetingWithGEvent_ = function(event,googleEmailId,officeEmailId){
    return generateMeetingWithGEvent(event,googleEmailId,officeEmailId)
}

function addInteractionsMeetingNext(intArr,interactionSender,interactionReceiver){
    if(common.checkRequired(interactionSender.userId) && common.checkRequired(interactionReceiver.userId) && interactionSender.userId.toString() == interactionReceiver.userId.toString()){
        var rIn = [];
        for(var i=0; i<intArr.length; i++){
            if(intArr[i].action == 'sender'){
                if(rIn.length == 0){
                    rIn.push(intArr[i])
                }
            }
        }

        interactionManagementObj.addInteractionsMeeting(common.castToObjectId(interactionSender.userId),rIn,function(isSuccess){
            if(!isSuccess){
                logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting self ',interactionSender.emailId,interactionSender.refId);
            }
        })
    }
    else{
        if(common.checkRequired(interactionSender.userId)){
            interactionManagementObj.addInteractionsMeeting(common.castToObjectId(interactionSender.userId),intArr,function(isSuccess){
                if(!isSuccess){
                    logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting ',interactionSender.emailId,interactionSender.refId);
                }
            })
        }
        if(common.checkRequired(interactionReceiver.userId)){
            interactionManagementObj.addInteractionsMeeting(common.castToObjectId(interactionReceiver.userId),intArr,function(isSuccess){
                if(!isSuccess){
                    logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting ',interactionReceiver.emailId,interactionReceiver.refId);
                }
            })
        }
    }
}

function addParticipantsAsContacts(meeting){
    var senderContact = {
        personId:meeting.senderId,
        personName:meeting.senderName,
        personEmailId:meeting.senderEmailId,
        count:1,
        lastInteracted:new Date(),
        addedDate: new Date(),
        verified:false,
        relatasContact:true,
        relatasUser:false,
        source:'relatas',
        mobileNumber:null
    };

    if(meeting.selfCalendar){

        if(meeting.toList && meeting.toList.length > 0){

            for(var i=0; i<meeting.toList.length; i++){

                if(common.checkRequired(meeting.toList[i].receiverEmailId) && meeting.toList[i].receiverEmailId != meeting.senderEmailId && meeting.toList[i].receiverId != meeting.senderId){

                    var name = '';
                    if(common.checkRequired(meeting.toList[i].receiverFirstName)){
                        name += meeting.toList[i].receiverFirstName+' ';
                    }
                    if(common.checkRequired(meeting.toList[i].receiverLastName)){
                        name += meeting.toList[i].receiverLastName
                    }
                    if(common.checkRequired(name)){
                        var contact = {
                            personId:meeting.toList[i].receiverId,
                            personName:meeting.toList[i].receiverName,
                            personEmailId:meeting.toList[i].receiverEmailId,
                            count:1,
                            lastInteracted:new Date(),
                            addedDate: new Date(),
                            verified:false,
                            relatasContact:true,
                            relatasUser:false,
                            source:'relatas',
                            mobileNumber:null
                        };
                        if(common.checkRequired(meeting.senderId)){
                            addContact(meeting.senderId,contact)
                        }
                    }
                }
                if(common.checkRequired(meeting.toList[i].receiverId)){
                    addContact(meeting.toList[i].receiverId,senderContact)
                }
            }
        }
    }
    else if(common.checkRequired(meeting.to) && common.checkRequired(meeting.to.receiverEmailId) && meeting.to.receiverId != meeting.senderId && meeting.to.receiverEmailId != meeting.senderEmailId){
        if(common.checkRequired(meeting.to.receiverName)){
            var contact2 = {
                personId:meeting.to.receiverId,
                personName:meeting.to.receiverName,
                personEmailId:meeting.to.receiverEmailId,
                count:1,
                lastInteracted:new Date(),
                addedDate: new Date(),
                verified:false,
                relatasContact:true,
                relatasUser:false,
                source:'relatas',
                mobileNumber:null
            };
            if(common.checkRequired(meeting.senderId)){
                addContact(meeting.senderId,contact2)
            }
        }
        if(common.checkRequired(meeting.to.receiverId)){
            addContact(meeting.to.receiverId,senderContact)
        }
    }
}

function addContact(userId,contact){
    userManagements.findUserProfileByEmailIdWithCustomFields(contact.personEmailId,{emailId:1,firstName:1,lastName:1,mobileNumber:1,location:1,designation:1,companyName:1,skypeId:1},function(error,user){
        if(error){

        }
        else if(user && common.checkRequired(user._id)){
            contact.personId = user._id
            contact.personName = user.firstName+' '+user.lastName
            contact.companyName = user.companyName
            contact.designation = user.designation
            contact.mobileNumber = user.mobileNumber
            contact.location = user.location
            contact.skypeId = user.skypeId
            contact.relatasUser = true
        }
        contactObj.addSingleContact(userId,contact)
    })
}

function updateLastInteractedDate(userId){

    interactionManagementObj.getLastInteractedDate(userId,function(contacts){
        userManagements.updateLastInteractedDate(userId,contacts,function(result){
            if(result){
                logger.info("Updated last interacted date for user - ", userId)
            }
        })
    });
}

module.exports = GoogleCalendar;
