
var emailSender = require('../public/javascripts/emailSender');
var userManagement = require('../dataAccess/userManagementDataAccess');
var meetingManagement = require('../dataAccess/meetingManagement');
var commonClass = require('./commonUtility');
var winstonLog = require('../common/winstonLog');
var moment = require('moment-timezone');
var emailSenders = new emailSender();
var common = new commonClass();
var meetingClassObj = new meetingManagement();
var userManagements = new userManagement();
var logLib = new winstonLog();
var logger =logLib.getWinston();

function MongoEventHandler(){

}

MongoEventHandler.prototype.emitEvent = function(doc,event){

    logger.info('<----------- TODO time expire event triggered. ------------->'+new Date().toISOString());
    if(event.data.type == 'todo'){
        if(common.checkRequired(doc) && common.checkRequired(doc.toDo) && doc.toDo.length > 0){
            meetingClassObj.populateMeetingToDoInfo(doc.toDo,function(newTodo){

                if(newTodo){
                    for(var i=0; i<newTodo.length; i++){
                        if(event.data.id.toString() == newTodo[i]._id.toString()){
                            sendEmailNotification(newTodo[i]);
                            break;
                        }
                    }
                }
            });
        }
    }
};

function sendEmailNotification(todo){
    if(common.checkRequired(todo)){
        if(common.checkRequired(todo.createdBy)){
            // mail to creator
            var timezone = common.checkRequired(todo.createdBy.timezone) ? todo.createdBy.timezone.name : 'UTC';
            var name = todo.createdBy.firstName+' '+todo.createdBy.lastName;
            var start = moment(todo.dueDate).tz(timezone || 'UTC');
            var date = start.format("Do MMMM");
            var time = start.format("h:mm a z");
            if(todo.status != "complete"){

                var notifySenderNotComplete = {
                    toName:name,
                    subject:'Relatas: Task Time Expired',
                    emailId:todo.createdBy.emailId,
                    mailContent:'This is to let you know that the task <b>"'+todo.actionItem+'"</b> set by <b>"'+name+'"</b> with task deadline<b>"'+date+' : '+time+'"</b> has expired and the task remains OPEN.'
                };
                emailSenders.sendMeetingCancelOrDeletionMail(notifySenderNotComplete,"Task Time Expired");
            }
            else if(todo.status == "complete"){
                var notifySenderComplete = {
                    toName:name,
                    subject:'Relatas: Task Time Expired',
                    emailId:todo.createdBy.emailId,
                    mailContent:'This is to let you know that the task <b>"'+todo.actionItem+'"</b> set by <b>"'+name+'"</b> with task deadline<b>"'+date+' : '+time+'"</b> has been COMPLETED.'
                };
                emailSenders.sendMeetingCancelOrDeletionMail(notifySenderComplete,"Task Time Expired");
            }

            // mail to receiver
            var timezone2 = common.checkRequired(todo.assignedTo.timezone) ? todo.assignedTo.timezone.name : 'UTC';
            var name2 = todo.assignedTo.firstName+' '+todo.assignedTo.lastName;
            var name2SetBy = todo.createdBy.firstName+' '+todo.createdBy.lastName;
            var start2 = moment(todo.dueDate).tz(timezone2 || 'UTC');
            var date2 = start2.format("Do MMMM");
            var time2 = start2.format("h:mm a z");
            if(todo.status != "complete"){

                var notifySenderNotComplete2 = {
                    toName:name2,
                    subject:'Relatas: Task Time Expired',
                    emailId:todo.assignedTo.emailId,
                    mailContent:'This is to let you know that the task <b>"'+todo.actionItem+'"</b> set by <b>"'+name2SetBy+'"</b> with task deadline<b>"'+date2+' : '+time2+'"</b> has expired and the task remains OPEN.'
                };
                emailSenders.sendMeetingCancelOrDeletionMail(notifySenderNotComplete2,"Task Time Expired");
            }
            else if(todo.status == "complete"){
                var notifySenderComplete2 = {
                    toName:name2,
                    subject:'Relatas: Task Time Expired',
                    emailId:todo.assignedTo.emailId,
                    mailContent:'This is to let you know that the task <b>"'+todo.actionItem+'"</b> set by <b>"'+name2SetBy+'"</b> with task deadline<b>"'+date2+' : '+time2+'"</b> has been COMPLETED.'
                };
                emailSenders.sendMeetingCancelOrDeletionMail(notifySenderComplete2,"Task Time Expired");
            }
        }
    }
}

function getUserProfile(id,callback){
    userManagements.findUserProfileByIdWithCustomFields(id,{firstName:1,lastName:1,emailId:1,timezone:1},function(error,user){
        callback(user);
    })
}

module.exports = MongoEventHandler;