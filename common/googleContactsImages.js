/**
 * Created by naveen on 9/6/16.
 */

var fs = require('fs');
var request=require('request');
var commonUtility = require('../common/commonUtility');
var common = new commonUtility();


function GoogleContactImage(){

}

GoogleContactImage.prototype.getContactProfileImageFromGoogle = function (url,access_token, imageName,callback) {
    if (url != 'images/default.png' && url != '/images/default.png') {
        request.get({
            url: url,
            headers: {
                'Authorization': 'Bearer ' + access_token,
                'GData-Version': '3.0'
            },
            encoding: 'binary'
        }, function (err, response, body) {

            if(!err && body != null && body != undefined && !common.contains(body,'html') && !common.contains(body,'title')){
                fs.writeFile('./public/profileImages/' + imageName, body, 'binary', function (err) {
                    if (err && callback){
                        callback(err)
                    }
                    else if(callback){
                        callback(true)
                    }
                });
            }
            else if(callback){
                callback(err)
            }
        });
    }
}

GoogleContactImage.prototype.sendContactImage = function (res,emailId) {
    res.download('./public/profileImages/'+emailId,function(error){
        if(error){
            res.send((emailId.substr(0,2)).toUpperCase())
        }
    });
}

module.exports = GoogleContactImage;