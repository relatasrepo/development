var momentTZ = require('moment-timezone');
var moment = require("moment");

var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var userManagement = require('../dataAccess/userManagementDataAccess');
var calendarPasswordManagement = require('../dataAccess/calendarPasswordManagement')
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var OutLookAPIModule = require('../common/outlookRecursiveModule');
var interactionManagement = require('../dataAccess/interactionManagement');
var contactClass = require('../dataAccess/contactsManagementClass');
var meetingManagement = require('../dataAccess/meetingManagement');

var meetingClassObj = new meetingManagement();
var common = new commonUtility();
var interactionManagementObj = new interactionManagement();
var contactObj = new contactClass();
var userManagements = new userManagement();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var endPoint = '';

var logLib = new winstonLog();
var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();
var _ = require("lodash");

function OpenSlots(){

};

OpenSlots.prototype.findEmailSendingPreferredTimeSlots = function(userId,callback){ //TODO sunil -store in user profile
    findEmailSendingPreferredTimeSlots(userId, callback)
};

OpenSlots.prototype.findUserMeetingOpenSlots = function(userId,callback){
    findUserMeetingOpenSlots(userId,callback);
};

OpenSlots.prototype.findUserMeetingOpenSlotsWithSlotDuration = function(userId,dateMin,dateMax,slotDuration,uptoDays,forDay,timezone,callback){
    findUserMeetingOpenSlotsWebLead(userId,dateMin,dateMax,slotDuration,uptoDays,forDay,timezone,callback);
};

OpenSlots.prototype.findRelatasPreferredSlots = function(userId,callback){
    findRelatasPreferredSlots(userId,callback);
};

OpenSlots.prototype.findAndStoreEmailSentOutFrequency = function(userId,callback){
    findAndStoreEmailSentOutFrequency(userId,callback);
};

function findUserMeetingOpenSlotsWebLead(userId,dateMin,dateMax,slotDuration,uptoDays,forDay,timezone,callback) {

    slotDuration = slotDuration?slotDuration:30; // Default 30 minutes

    todayDateMinMaxTimeZoneAndServiceLogin(userId,function(error, date){
        if(error){
            loggerError.info("ERROR: findUserMeetingOpenSlots() in userOpenSlot.js " + error)
            callback([])
        }
        else {
            //
            // if(timezone){
            //     date.timezone = timezone
            // }

            getBusySlots(userId, dateMin,dateMax, date.timezone, date.serviceLogin, function (busySlots) {
                
                getFreeSlotsWebLead(busySlots, slotDuration, date.timezone, true, true,uptoDays,forDay,function (freeslots) {
                    callback(freeslots)
                });
            });
        }
    });
}

function findAndStoreEmailSentOutFrequency(userId,callback){

    userManagements.findUserProfileByIdWithCustomFields(userId, {"emailSentOutFrequencyUpdatedOn" :1,"emailSentOutFrequency":1}, function (error, user) {
        
        if (error || !user) {
            callback(error, false)
            loggerError.info("ERROR: findAndStoreEmailSentOutFrequency() in userOpenSlot.js " + error)
        }
        else if (!user.emailSentOutFrequencyUpdatedOn || (moment(user.emailSentOutFrequencyUpdatedOn) < moment().subtract(180,'days')) || !user.emailSentOutFrequency || (user.emailSentOutFrequency && user.emailSentOutFrequency.length == 0)) {
            findEmailSendingPreferredTimeSlots(userId, function (error, emailFrequency) {
                var formatted = [];
                emailFrequency.forEach(function (a) {
                    formatted.push({hours24: a.hours24, meridiem: a.meridiem, frequency: a.frequency})
                });
                userManagements.updateEmailSentOutFrequency(userId, formatted, function (err, result) {
                    callback(err, result)
                });
            });
        }
        else {
            callback(null, 'upto date')
        }
    });
}

function findUserMeetingOpenSlots(userId,callback) {
    todayDateMinMaxTimeZoneAndServiceLogin(userId,function(error, date){
        if(error){
            loggerError.info("ERROR: findUserMeetingOpenSlots() in userOpenSlot.js " + error)
            callback([])
        }
        else
        {
            getBusySlots(userId, date.min, date.max, date.timezone, date.serviceLogin, function (busySlots) {
                getFreeSlots(busySlots, 30, date.timezone, true, true, function (freeslots) {
                    callback(freeslots)
                });
            });
        }
    });
}

function findEmailSendingPreferredTimeSlots(userId, callback) {
    interactionManagementObj.getEmailSentTimeToFindPreferredSlots(common.castToObjectId(userId),function (error, interactions) {
        if(error){
            callback(error,[])
            loggerError.info("ERROR: findEmailSendingPreferredTimeSlots() in userOpenSlot.js " + error)
        }
        else{
            // interactions.forEach(function(a){
            //     a.utcHour = a._id;
            //     delete a._id;
            // });
            // callback(error,interactions)

            userManagements.findUserProfileByIdWithCustomFields(userId, {"timezone" :1}, function (error, user) {
                if(error){
                    callback(error, [])
                    loggerError.info("ERROR: findEmailSendingPreferredTimeSlots() in userOpenSlot.js " + error)
                }
                else{
                    var timezone = 'UTC';
                    if(user && user.timezone && user.timezone.name){
                        timezone = user.timezone.name
                    }

                    interactions.forEach(function(a){
                        a.formate = momentTZ(a.interactionDate);
                        a.time = a.formate.tz(timezone).format('hh:mm a');
                        a.hours = a.formate.tz(timezone).format('hh');
                        a.period = a.formate.tz(timezone).format('a');
                        a.hourWithPeriod = a.formate.tz(timezone).format('hh a');
                        a.hours24 = a.formate.tz(timezone).format('HH')
                    });

                    var groupedByHour = _
                        .chain(interactions)
                        .groupBy('hourWithPeriod')
                        .map(function (value, key) {
                            return {
                                hourWithPeriod: key,
                                hours:_.unique(_.pluck(value,'hours')),
                                meridiem:_.unique(_.pluck(value,'period')),
                                hours24:_.unique(_.pluck(value,'hours24')),
                                interaction:value
                            }
                        })
                        .value();

                    groupedByHour.forEach(function(a){
                        a.hours = a.hours[0];
                        a.meridiem = a.meridiem[0];
                        a.frequency = a.interaction.length;
                        a.hours24 = a.hours24[0];
                        a.timeRange = a.hours + '-'+ (Number(a.hours) + 1)
                        // delete a.interaction;
                    });

                    groupedByHour.sort(function(a,b){
                        if(b.frequency != a.frequency) {
                            return b.frequency - a.frequency;
                        }
                        else{
                            return Number(a.hours24) - Number(b.hours24);
                        }
                    })
                    callback(error,groupedByHour);
                }
            });
        }
    })
}

function getFreeSlots(busyDates,slotRange,timezone, getAll,withSlotDates,callback){
    var current = momentTZ().tz(timezone);

    var inputStart,inputEnd;
    inputStart = momentTZ().tz(timezone);
    inputStart.hours(0)
    inputStart.minutes(0)
    inputStart.seconds(0)

    inputEnd = inputStart.clone();
    inputEnd.hours(23)
    inputEnd.minutes(59)
    inputEnd.seconds(59)

    var freeSlots = {};
    var slotRangeNew = getSlotRange(slotRange,timezone);

    var daysRange = moment.range(inputStart.format()+'/'+inputEnd.format());
    var busyRanges = busySlotsToRanges(busyDates);

    daysRange.by(slotRangeNew, function(moment) {
        var end = moment.clone();
        end.minutes(end.minutes()+slotRange);

        var range = moment.range(moment.format()+'/'+end.format());
        var overlaped = false;
        var overlappedIndex = null;
        for(var i=0; i<busyRanges.length; i++){
            if(busyRanges[i].range.overlaps(range)){
                overlaped = true;
                overlappedIndex = i;
            }
        }
        var m = momentTZ(moment).tz(timezone);
        var me = momentTZ(end).tz(timezone);
        if(!m.isBefore(current)){
            if(!overlaped){
                var f = m.format("MM-DD-YYYY");
                if(common.checkRequired(freeSlots[f])){
                    // freeSlots[f][m.format('a')].slotCount++;
                    if(common.checkRequired(inputStart)){
                        freeSlots[f][m.format('a')].slots.push({start: m.format(),end:me.format(),isBlocked:false});
                    }
                }
                else{
                    freeSlots[f] = {
                        date: m.format(),
                        am:{slotCount:0,slots:[]},
                        pm:{slotCount:0,slots:[]}
                    };
                    // freeSlots[f][m.format('a')].slotCount++;
                    if(common.checkRequired(inputStart)){
                        freeSlots[f][m.format('a')].slots.push({start: m.format(),end:me.format(),isBlocked:false});
                    }
                }
            }
            else if((getAll && overlappedIndex != null) || (withSlotDates && overlappedIndex != null)){
                var f_new = m.format("MM-DD-YYYY");
                if(common.checkRequired(freeSlots[f_new])){
                    // freeSlots[f_new][m.format('a')].slotCount++;
                    if(common.checkRequired(inputStart) || withSlotDates){
                        freeSlots[f_new][m.format('a')].slots.push({
                            start: m.format(),end:me.format(),
                            title:busyDates[overlappedIndex].title,
                            isBlocked:true
                        });
                    }
                }
                else{
                    freeSlots[f_new] = {
                        date: m.format(),
                        am:{slotCount:0,slots:[]},
                        pm:{slotCount:0,slots:[]}
                    };
                    // freeSlots[f_new][m.format('a')].slotCount++;
                    if(common.checkRequired(inputStart) || withSlotDates){
                        freeSlots[f_new][m.format('a')].slots.push({
                            start: m.format(),end:me.format(),
                            title:busyDates[overlappedIndex].title,
                            isBlocked:true
                        });
                    }
                }
            }
        }
    });

    var slots = _
        .chain(freeSlots)
        .map(function (value, key) {
            return {
                key: key,
                slots:value
            }
        })
        .value();

    callback(slots);
}

function getFreeSlotsWebLead(busyDates,slotRange,timezone,getAll,withSlotDates,uptoDays,forDay,callback){
    var current = momentTZ().tz(timezone);

    var inputStart,inputEnd;
    inputStart = momentTZ(forDay).tz(timezone);
    inputStart.hours(0)
    inputStart.minutes(0)
    inputStart.seconds(0)

    inputEnd = inputStart.clone();
    inputEnd.hours(23)
    inputEnd.minutes(59)
    inputEnd.seconds(59)

    var freeSlots = {};
    var slotRangeNew = getSlotRange(slotRange,timezone);

    var daysRange = moment.range(inputStart.format()+'/'+inputEnd.format());

    var busyRanges = busySlotsToRanges(busyDates);

    daysRange.by(slotRangeNew, function(moment) {
        var end = moment.clone();
        end.minutes(end.minutes()+slotRange);

        var range = moment.range(moment.format()+'/'+end.format());
        var overlaped = false;
        var overlappedIndex = null;
        for(var i=0; i<busyRanges.length; i++){
            if(busyRanges[i].range.overlaps(range)){
                overlaped = true;
                overlappedIndex = i;
            }
        }
        var m = momentTZ(moment).tz(timezone);
        var me = momentTZ(end).tz(timezone);
        if(!m.isBefore(current)){
            if(!overlaped){
                var f = m.format("MM-DD-YYYY");
                if(common.checkRequired(freeSlots[f])){
                    // freeSlots[f][m.format('a')].slotCount++;
                    if(common.checkRequired(inputStart)){
                        freeSlots[f][m.format('a')].slots.push({start: m.format(),end:me.format(),isBlocked:false});
                    }
                }
                else{
                    freeSlots[f] = {
                        date: m.format(),
                        am:{slotCount:0,slots:[]},
                        pm:{slotCount:0,slots:[]}
                    };
                    // freeSlots[f][m.format('a')].slotCount++;
                    if(common.checkRequired(inputStart)){
                        freeSlots[f][m.format('a')].slots.push({start: m.format(),end:me.format(),isBlocked:false});
                    }
                }
            }
            else if((getAll && overlappedIndex != null) || (withSlotDates && overlappedIndex != null)){
                var f_new = m.format("MM-DD-YYYY");
                if(common.checkRequired(freeSlots[f_new])){
                    // freeSlots[f_new][m.format('a')].slotCount++;
                    if(common.checkRequired(inputStart) || withSlotDates){
                        freeSlots[f_new][m.format('a')].slots.push({
                            start: m.format(),end:me.format(),
                            title:busyDates[overlappedIndex].title,
                            isBlocked:true
                        });
                    }
                }
                else{
                    freeSlots[f_new] = {
                        date: m.format(),
                        am:{slotCount:0,slots:[]},
                        pm:{slotCount:0,slots:[]}
                    };
                    // freeSlots[f_new][m.format('a')].slotCount++;
                    if(common.checkRequired(inputStart) || withSlotDates){
                        freeSlots[f_new][m.format('a')].slots.push({
                            start: m.format(),end:me.format(),
                            title:busyDates[overlappedIndex].title,
                            isBlocked:true
                        });
                    }
                }
            }
        }
    });

    var slots = _
        .chain(freeSlots)
        .map(function (value, key) {
            return {
                key: key,
                slots:value
            }
        })
        .value();

    callback = callback?callback:forDay
    callback(slots);
}

function findRelatasPreferredSlots(userId,callback) {
    userManagements.findUserProfileByIdWithCustomFields(userId, {"emailSentOutFrequency" :1}, function (error, user){
        if(error || !user || !user.emailSentOutFrequency){
            if(error) {
                loggerError.info("ERROR: findRelatasPreferredSlots() in userOpenSlot.js " + error)
            }
            callback([]);
        }
        else if(user.emailSentOutFrequency.length > 0){
            var emailFrequency = user.emailSentOutFrequency;
            findUserMeetingOpenSlots(userId,function(freeSlots){
                todayDateMinMaxTimeZoneAndServiceLogin(userId,function(error,date){

                    //TODO IMPORTANT - If no slots are found then the insights should still come up on the dashboard anyway.

                    if(error){
                        loggerError.info("ERROR: findRelatasPreferredSlots() in userOpenSlot.js " + error)
                        callback([]);
                    }
                    else {
                        emailFrequency.sort(function(a,b){
                            if(b.frequency != a.frequency) {
                                return b.frequency - a.frequency;
                            }
                            else{
                                return Number(a.hours24) - Number(b.hours24);
                            }
                        })
                        // callback(emailFrequency, freeSlots);
                        var preferringSlots = [];
                        var priority = 0;
                        emailFrequency.forEach(function (emailSlot) {
                            var slotStart, slotEnd;
                            var nexthour = Number(emailSlot.hours24) + 1;
                            slotStart = momentTZ().tz(date.timezone);
                            slotStart.hours(emailSlot.hours24)
                            slotStart.minutes(0)
                            slotStart.seconds(0)

                            slotEnd = slotStart.clone();
                            slotEnd.hours(nexthour)
                            slotEnd.minutes(0)
                            slotEnd.seconds(0)

                            var emailSentoutRange = moment.range(slotStart.format()+'/'+slotEnd.format());

                            if(emailSlot.meridiem == 'am'){
                                freeSlots[0].slots.am.slots.forEach(function(openSlot){

                                    if(!openSlot.isBlocked) {
                                        var Mstart = momentTZ(openSlot.start).tz(date.timezone);
                                        var Mend = momentTZ(openSlot.end).tz(date.timezone);

                                        var range = moment.range(Mstart.format() + '/' + Mend.format());
                                        var overlaped = false;

                                        if (emailSentoutRange.overlaps(range)) {
                                            overlaped = true;
                                        }

                                        if(overlaped){
                                            priority++;
                                            var m = momentTZ(Mstart).tz(date.timezone);
                                            var me = momentTZ(Mend).tz(date.timezone);
                                            preferringSlots.push({
                                                start: m.format(),
                                                end:me.format(),
                                                priority:priority,
                                                EmailSentFrequency:emailSlot.frequency
                                            })
                                        }
                                    }
                                })
                            }
                            else if(emailSlot.meridiem == 'pm'){
                                freeSlots[0].slots.pm.slots.forEach(function(openSlot){
                                    if(!openSlot.isBlocked) {
                                        var Mstart = momentTZ(openSlot.start).tz(date.timezone);
                                        var Mend = momentTZ(openSlot.end).tz(date.timezone);

                                        var range = moment.range(Mstart.format() + '/' + Mend.format());
                                        var overlaped = false;

                                        if (emailSentoutRange.overlaps(range)) {
                                            overlaped = true;
                                        }

                                        if(overlaped){

                                            priority++;
                                            var m = momentTZ(Mstart).tz(date.timezone);
                                            var me = momentTZ(Mend).tz(date.timezone);
                                            preferringSlots.push({
                                                start: m.format(),
                                                end:me.format(),
                                                priority:priority,
                                                EmailSentFrequency:emailSlot.frequency
                                            });
                                        }
                                    }
                                })
                            }
                        });
                        callback(preferringSlots)
                    }
                });
            })
        }
        else{
            callback([]);
        }
    });
}

function busySlotsToRanges(slots){
    var ranges = []
    for(var i=0; i<slots.length; i++){
        ranges.push(
            {
                range:moment.range(slots[i].start+'/'+slots[i].end),
            }

        )
    }

    return ranges;
}

function getSlotRange(mins,timezone){

    var start = momentTZ().tz(timezone)
    start.hours(0)
    start.minutes(0)
    start.seconds(0)
    var end   = start.clone();
    end.minutes(mins)

    return moment.range(start.format()+'/'+end.format());

}

function getBusySlots(userId,endDate,timeMax,timezone,findOutlookOrGoogle,callback){

    var busySlots = [];
    getBusyMeetingsFomMeetings(userId,endDate,timeMax,findOutlookOrGoogle,function(meetings){
        if(meetings != null && meetings != undefined && meetings.length > 0){
            for(var i=0; i<meetings.length; i++){
                if(common.checkRequired(meetings[i].scheduleTimeSlots) && meetings[i].scheduleTimeSlots.length > 0){
                    var isAccepted = false;
                    var slot;
                    for(var t=0;t<meetings[i].scheduleTimeSlots.length;t++){
                        if(meetings[i].scheduleTimeSlots[t].isAccepted){
                            isAccepted = true;
                            slot = t;
                        }
                    }

                    if(isAccepted && common.checkRequired(slot)){
                        var obj = {
                            start: momentTZ(meetings[i].scheduleTimeSlots[slot].start.date).tz(timezone).format(),
                            end:momentTZ(meetings[i].scheduleTimeSlots[slot].end.date).tz(timezone).format()
                        };

                        busySlots.push(obj);
                    }
                    else{
                        for(var s=0; s<meetings[i].scheduleTimeSlots.length; s++){
                            var obj2 = {
                                start: momentTZ(meetings[i].scheduleTimeSlots[s].start.date).tz(timezone).format(),
                                end:momentTZ(meetings[i].scheduleTimeSlots[s].end.date).tz(timezone).format()
                            };

                            busySlots.push(obj2);
                        }
                    }
                }
            }
        }

        callback(busySlots)
    });
}

function getBusyMeetingsFomMeetings(userId,startDate,timeMax,findOutlookOrGoogle,callback){
    meetingClassObj.userMeetingsByDate(userId,startDate,timeMax,{scheduleTimeSlots:1},findOutlookOrGoogle,function(meetings){
        callback(meetings);
    })
}

function todayDateMinMaxTimeZoneAndServiceLogin(userId, callback) {
    var timezone;
    myUserCollection.find({ _id: userId }, { timezone: 1,serviceLogin:1 }, function(err, user) {
        if (err || user.length == 0) {
            loggerError.info("ERROR: todayDateMinMaxTimeZoneAndServiceLogin() in userOpenSlot.js " + err)
            callback(err, null);
        }
        else {
            if (user[0].timezone && user[0].timezone.name)
                timezone = user[0].timezone.name;
            else
                timezone = 'UTC';
            var dateMin = momentTZ().tz(timezone);
            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)
            var dateMax = momentTZ().tz(timezone);

            dateMax.hour(23)
            dateMax.minute(59)
            dateMax.second(59)
            dateMax.millisecond(0)
            callback(null, { min: dateMin, max: dateMax, timezone: timezone, serviceLogin:user[0].serviceLogin})
        }
    });
}

function getRangeDays(days,timezone){
    days--;
    var start = momentTZ().tz(timezone)
    start.hours(0)
    start.minutes(0)
    start.seconds(0)
    var end   = start.clone();
    end.hours(23)
    end.minutes(59)
    end.seconds(59)
    end.date(end.date()+days);

    return moment.range(start.format()+'/'+end.format());

}

module.exports = OpenSlots;