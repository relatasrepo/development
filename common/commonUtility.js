var queryString = require('querystring');
var jwt = require('jwt-simple');
var moment = require('moment-timezone');
var fs = require('fs');
// var lwip = require('lwip');
var mongoose = require('mongoose');
var useragent = require('express-useragent');
var _ = require("lodash")
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var customObjectId = require('mongodb').ObjectID;
var request = require('request');
var async = require('async');

var moment2 = require('moment');
    require('moment-range');

var userManagement = require('../dataAccess/userManagementDataAccess');
var profileManagementClass = require('../dataAccess/profileManagementClass');
var calendarPasswordManagement = require('../dataAccess/calendarPasswordManagement');
var messageDataAccess = require('../dataAccess/messageManagement');
var interactions = require('../dataAccess/interactionManagement');
var userLog = require('../dataAccess/userLogManagement');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var companyClass = require('../dataAccess/corporateDataAccess/companyModelClass');
var errorClass = require('../errors/errorClass');
var errorMessages = require('../errors/errorMessage');
var contactClass = require('../dataAccess/contactsManagementClass');
var appConfiguration = require('../databaseSchema/userManagementSchema').appConfiguration;
var inavlidEmailListCollection = require('../databaseSchema/userManagementSchema').invalidEmailList;
var portfoliosCollection = require('../databaseSchema/portfolios').portfolios;
var opportunityManagement = require('../dataAccess/opportunitiesManagement');
var admin = require('../config/firebaseConfiguration');

var calendarPasswordManagementsObj = new calendarPasswordManagement();
var contactObj = new contactClass();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var appCredential = new appCredentials();
var profileManagementClassObj = new profileManagementClass();
var interaction = new interactions();
var company = new companyClass();
var messageAccess = new messageDataAccess();
var log = new userLog();
var emailSenders = new emailSender();
var userManagements = new userManagement();
var oppManagementObj = new opportunityManagement();

var statusCodes = errorMessagesObj.getStatusCodes();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var domainName = domain.domainName;

var logLib = new winstonLog();
var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();
var linkedinErrorLog = logLib.getWinstonLinkedInError();
var Linkedin = require('node-linkedin')(authConfig.LINKEDIN_CONSUMER_KEY, authConfig.LINKEDIN_CONSUMER_SECRET, authConfig.LINKEDIN_REDIRECT_URL);
var excludeDomains = appCredential.getExcludeDomains();
var redis = require('redis');
var redisClient = redis.createClient();

//var ObjectId = require('mongodb').ObjectID;

var invalidEmails = [/techgig/i, /team/i,/info@/i, /support/i, /admin/i, /hello/i, /no-reply/i, /noreply/i, /no-reply/i, /help/i, /mailer-daemon/i, /mail-noreply/i, /alert/i, /calendar-notification/i, /eBay/i, /flipkartletters/i, /pinterest/i, /dobambam.com/i, /notify/i, /offers@/i, /info@relatas.in/i, /facebookmail/i, /verify/i, /ship-confirm/i, /order-update/i, /auto-confirm/i, /ship@/i, /automatic/i, /updates/i, /update/i, /notice/i, /aws-loft/i, /scheduling/i, /schedule/i, /contact@/i, /editor@/i, /survey@/i, /.love@/i, /community/i, /coordinator/i, /bounce/i, /postmaster@/i, /newsletter/i,/admission@/i,
    /aws/i,
    /care@/i,
    /communications1/i,
    /community/i,
    /coordinator/i,
    /customerservice/i,
    /onlinesbicard/i,
    /statements/i,
    /welcome/i,
    /wisdom@/i,
    /wrangler/i,
    /information@/i,
    /administration@/i,
    /notification/i,
    /promo@/i,
    /@calendar-server.bounces.google.com/i,
    /calendar/i,
    /developer@/i,
    /linkedin@/i,
    /editorial@/i,
    /editors@/i,
    /.sale@/i,
    /@reply/i
]

var interactionsDataForPast = 30;
var REMAIND_ME_NO_RESPONSE_AFTER_DAYS = 7;
var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';

function CommonUtility(){
    this.interactionDataFor = function(callback){
        callback(interactionsDataForPast);
    };

    this.remaindMeAfterDays = function(callback){
        callback(REMAIND_ME_NO_RESPONSE_AFTER_DAYS);
    };

    this.getInvalidEmailList = function(){
        return invalidEmails;
    }
}

CommonUtility.prototype.insertLog = function(profile,loginPage,req){
    var source = req.headers['user-agent'],
        ua = useragent.parse(source);
    updateProfileWithLastLoginDate(profile._id);

    var obj = {
        userId:profile._id,
        emailId:profile.emailId,
        loginPage:loginPage,
        userIp:getClientIpFromReq(req),
        userAgent:{
            browser: ua.Browser,
            version: ua.Version,
            operatingSystem: ua.OS,
            platform: ua.Platform,
            source:ua.source,
            isMobile:ua.isMobile,
            isTablet: ua.isTablet,
            isIPad: ua.isiPad,
            isIPod: ua.isiPod,
            isIPhone: ua.isiPhone,
            isAndroid: ua.isAndroid,
            isBlackberry: ua.isBlackberry,
            isOpera: ua.isOpera,
            isIE: ua.isIE,
            isIECompatibilityMode: ua.isIECompatibilityMode,
            isSafari: ua.isSafari,
            isFirefox: ua.isFirefox,
            isWebkit: ua.isWebkit,
            isChrome: ua.isChrome,
            isKonqueror: ua.isKonqueror,
            isOmniWeb: ua.isOmniWeb,
            isSeaMonkey: ua.isSeaMonkey,
            isFlock: ua.isFlock,
            isAmaya: ua.isAmaya,
            isEpiphany: ua.isEpiphany,
            isDesktop: ua.isDesktop,
            isWindows: ua.isWindows,
            isLinux: ua.isLinux,
            isLinux64: ua.isLinux64,
            isMac: ua.isMac,
            isBada: ua.isBada,
            isSamsung: ua.isSamsung,
            isRaspberry: ua.isRaspberry,
            isBot: ua.isBot,
            isCurl: ua.isCurl,
            isAndroidTablet: ua.isAndroidTablet,
            isWinJs: ua.isWinJs,
            isKindleFire: ua.isKindleFire,
            isSilk: ua.isSilk,
            SilkAccelerated: ua.SilkAccelerated
        }
    };
    log.insertLog(obj);
};

CommonUtility.prototype.findAllPortfoliosWithAccess = function(userEmailIds,callback){

    portfoliosCollection.find({"list.headEmailId" : {$in:userEmailIds}}).exec(function (err,portfolios) {
        callback(err,portfolios)
    })
}

CommonUtility.prototype.insertMobileLog = function(userId,req){

    var obj = {
        userId:userId,
        emailId:null,
        loginPage:"mobile-landing",
        userIp:getClientIpFromReq(req)
    };
    log.insertLog(obj);
};

CommonUtility.prototype.penultimateLoginDate = updateProfileWithLastLoginDate

function updateProfileWithLastLoginDate(userId){
    var date = new Date()

    userManagements.getLastLogin(userId,function (err,penultimateLastLoginDate) {
        userManagements.updatePenultimateLogin(userId,new Date(penultimateLastLoginDate[0].lastLoginDate),function (ep,fp,mp){
            userManagements.updateLastLogin(userId,date,function(e,f,m){

            });
        });
    });
}

CommonUtility.prototype.getLoginStatus = function(req){
    if(req.isAuthenticated()){
        return {loginSignUp:'style=display:none',logout:'style=display:block'}
    }
    else{
        return {loginSignUp:'style=display:block',logout:'style=display:none'}
    }
};

function appendProfileToInteraction(user,interaction){

    interaction.userId = user._id;
    interaction.emailId = user.emailId;
    interaction.firstName = user.firstName;
    interaction.lastName =user.lastName;
    interaction.publicProfileUrl = user.publicProfileUrl;
    interaction.profilePicUrl = user.profilePicUrl;


    if(checkRequired(user.companyName)){
        interaction.companyName = user.companyName;
    }

    if(checkRequired(user.designation)){
        interaction.designation = user.designation;
    }

    if(checkRequired(user.location)){
        interaction.location = user.location;
    }

    return interaction
}

function storeInteractionPr(details){
    userManagements.findUserProfileByIdWithCustomFields(details.userId,{emailId:1,mobileNumber:1,designation:1,companyName:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,location:1},function(error,profile){
        if(checkRequired(profile)){
            details.emailId = profile.emailId;
            if(checkRequired(profile.mobileNumber)){
                details.mobileNumber = profile.mobileNumber;
            }

            details = appendProfileToInteraction(profile,details);
            interaction.createInteraction(details);
        }
        else{
            details.emailId = details.emailId || '';
            interaction.createInteraction(details);
        }
    })
}

CommonUtility.prototype.storeInteraction_new = function(userId,interactionsArr){

    var emailIdArr = [];
    for(var i=0; i<interactionsArr.length; i++){
        emailIdArr.push(interactionsArr[i].emailId)
    }
    if(emailIdArr.length > 0){
        emailIdArr = this.removeDuplicate_id(emailIdArr,true);
        userManagements.selectUserProfilesCustomQuery({emailId:{$in:emailIdArr}},function(error,users){
            if(error){
                loggerError.info('Error in storeInteraction_new():CommonUtility ',error);
            }
            if(users != null && users != undefined && users.length > 0){
                users.forEach(function(user){
                    for(var i=0; i<interactionsArr.length; i++){
                        if(interactionsArr[i].emailId == user.emailId){
                            interactionsArr[i].userId = user._id;
                            interactionsArr[i].firstName = user.firstName;
                            interactionsArr[i].lastName = user.lastName;
                            interactionsArr[i].publicProfileUrl = user.publicProfileUrl;
                            interactionsArr[i].profilePicUrl = user.profilePicUrl;
                            interactionsArr[i].companyName = user.companyName;
                            interactionsArr[i].designation = user.designation;
                            interactionsArr[i].location = user.location;
                            interactionsArr[i].emailId = user.emailId;
                            interactionsArr[i].mobileNumber = user.mobileNumber;
                        }
                    }
                })
            }
            interaction.createInteraction_new(userId,interactionsArr);
        })
    }
    else interaction.createInteraction_new(userId,interactionsArr);
};

CommonUtility.prototype.storeInteraction = function(details){
    storeInteractionPr(details)
};

CommonUtility.prototype.isRelatasAdmin = function(req, res, callback){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user, tokenSecret);
    var userId = decoded.id;
    userManagements.findUserProfileByIdWithCustomFields(castToObjectId(userId),{admin:1}, function(error, user) {
        if (checkRequired(user)) {
            if (user.admin == true) {
                callback(true);
            } else callback(false);
        } else callback(false);
    })
}

CommonUtility.prototype.storeInteractionReceiver = function(details){
    userManagements.findUserProfileByEmailIdWithCustomFields(details.emailId,{emailId:1,mobileNumber:1,designation:1,companyName:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,location:1},function(error,profile){
        if(checkRequired(profile)){
            details.emailId = profile.emailId;
            details.userId = profile._id;
            if(checkRequired(profile.mobileNumber)){
                details.mobileNumber = profile.mobileNumber;
            }

            details = appendProfileToInteraction(profile,details);
            interaction.createInteraction(details);
        }else{
            interaction.createInteraction(details);
        }
    })
};

CommonUtility.prototype.storeInteractionDirectly = function(details,user){
    details = appendProfileToInteraction(user,details);
    interaction.createInteraction(details);
};

CommonUtility.prototype.storeInteractionByMobileNumber = function(details){
    if(checkRequired(details.mobileNumber)){
        userManagements.selectUserProfileCustomQuery({mobileNumber:details.mobileNumber},{emailId:1,mobileNumber:1,designation:1,companyName:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,location:1},function(error,user){
            if(checkRequired(user)){
                details = appendProfileToInteraction(user,details);
            }
            interaction.createInteraction(details);
        })
    }else interaction.createInteraction(details);
};

CommonUtility.prototype.getValidUniqueUrl = function(uniqueName){
    var url = domainName+'/'+getValidUniqueName(uniqueName);
    return url;
}

CommonUtility.prototype.getValidUniqueName = function(uniqueName){
    var unique = getValidUniqueName(uniqueName);
    return unique;
}

CommonUtility.prototype.removeDuplicate_id = function(arr,withoutId){
    var newArr = removeDuplicates_id(arr,withoutId);
    return newArr;
}

CommonUtility.prototype.checkRequired = function(data){
    return checkRequired(data);
}

CommonUtility.prototype.getSubDomain = function(req){
    return isSubDomain(req);
};

CommonUtility.prototype.makeSquareImage = function(path,oPath,type,extension,callback){
    fs.readFile(path+'.'+type, function(error, image) {
        if(error) {
            callback(false, path);
        } else {
            callback(true,oPath+'.'+type);
        }
    });


    /* lwip.open(path,type, function(err2, image) {
        if(err2){
            callback(false,path)
        }
        else{
            image.scale(3.40, function (error, image) {
                if(error){
                    callback(false,path)
                }
                else{
                    var p = path+'.'+type;
                    if(extension){
                        p = path;
                    }
                    image.writeFile(p, function(err){
                        // check err...
                        // done.
                        if(err){
                            logger.info('Error in getting image from third party server '+err);
                            callback(false,path)
                        }
                        else{
                            logger.info('Saving image in server success (image from thirdParty servers)');
                            fs.unlinkSync(path);
                            callback(true,oPath+'.'+type);
                        }
                    });
                }
            })
        }
    }) */
};

CommonUtility.prototype.checkImageExtension = function(userProfile,callback){

    if(callback){
        callback()
    }
};

CommonUtility.prototype.checkImageWidthAndHeight = function(path,type,callback){
    /* lwip.open(path,type,function(err2, image) {

        if(image){

            if(image.width() >= 240 && image.height() >= 240){
                callback(true)
            }
            else{
                callback(false)
            }
        }else callback(true)
    }) */
};

function isSubDomain(req){
    var host = req.headers.host;

    host = checkRequired(host) ? host : ''
    if(excludeDomains.indexOf(host.trim()) == -1){
        host = host.split('.');

        var result = false;

        //Only for creating a company with subdoman and domain as the same. Last modified 08 March 2016 by Naveen
        if(appCredential.getMainHost() == host[0]){
            return result = host[0]
        }

        for(var i=0; i<host.length; i++){
            if(host[i] == appCredential.getMainHost()){
                result = true;
                if(i == 0) return false;

                if(i > 0){
                    var j = i-1;
                    if(host[j] != 'www' && host[j] != undefined && host[j] != null){

                        return host[j]

                    }else return false;
                }else return false;
            }
        }

        if(!result) return false;
    }
    else return false;
}

CommonUtility.prototype.checkUserDomain = function(req,res,next) {

    if (req.isAuthenticated()) {

        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user, tokenSecret);
        var userId = decoded.id;
        company.findOrCreateCompanyByUrl(getCompanyObj(isSubDomain(req),req),{url:1},function(companyProfile){

            userManagements.findUserProfileByIdWithCustomFields(userId, {companyId: 1, corporateUser: 1}, function (error, user) {
                if (!error && checkRequired(user)) {
                    var domain = isSubDomain(req);
                    // var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
                    var protocol = 'http' + (req.connection.encrypted ? 's' : 's') + '://';

                    if(_.includes(req.headers.host,"localhost")){
                        protocol = 'http://';
                    }

                    if (domain) {
                        if (!user.corporateUser) {

                            var url = protocol + appCredential.getDomain().mainDomain+'/external/login';
                            url = url + '?' + queryString.stringify({token: req.user, goTo: req.url});

                            req.logOut();
                            return res.redirect(url);

                        }
                        else if(user.corporateUser){

                            if(user.companyId.toString() == companyProfile._id.toString()){
                                next();
                            }
                            else{
                                company.findCompanyByIdWithProjection(user.companyId,{url:1},function(companyInfo){
                                    if(checkRequired(companyInfo)){
                                        var url2 = protocol + companyInfo.url+'/external/login';
                                        url2 = url2 + '?' + queryString.stringify({token: req.user, goTo: req.url});

                                        req.logOut();
                                        return res.redirect(url2);
                                    }else next();
                                });
                            }
                        }
                    }
                    else{
                        if (user.corporateUser) {
                            company.findCompanyByIdWithProjection(user.companyId,{url:1},function(companyInfo){
                                if(checkRequired(companyInfo)){
                                    var url3 = protocol + companyInfo.url+'/external/login';
                                    url3 = url3 + '?' + queryString.stringify({token: req.user, goTo: req.url});

                                    req.logOut();
                                    return res.redirect(url3);
                                }else next();
                            });
                        }
                        else{
                            next();
                        }
                    }
                } else next();
            });
        });

    }else next();
};

CommonUtility.prototype.getCompanyObjByDomain = function(companyName,domain){
    return {
        url:companyName+'.'+domain.mainDomain,
        companyName:companyName
    };
};

function getCompanyObj(companyName,req){
    var company = {
        url:req.headers.host?req.headers.host.toLowerCase():req.headers.host,
        companyName:companyName?companyName.toLowerCase():companyName
    }

    return company;
}

function getValidUniqueName(uniqueName){
    var url = domainName.split('/');
    var patt = new RegExp(url[2]);
    if(patt.test(uniqueName)){
        url = uniqueName.split('/');
        for (var i = url.length - 1; i >= 0; i--) {
            if (url[i] == 'u') {
                uniqueName = url[i+1];
            }
        }
        return uniqueName;
    }else {
        return uniqueName;
    }
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined || data == "undefined") {
        return false;
    }
    else{
        return true;
    }
}

CommonUtility.prototype.removeDuplicates_emailId = function(arr){
    return removeDuplicates_emailId(arr);
}

CommonUtility.prototype.removeDuplicates_field = function(arr,field){
    return removeDuplicates_field(arr,field);
}

function removeDuplicates_field(arr,field){

    var end = arr.length;

    for(var i = 0; i < end; i++)
    {
        for(var j = i + 1; j < end; j++)
        {
            if (arr[i] && arr[j] && arr[i][field] == arr[j][field])
            {
                var shiftLeft = j;
                for(var k = j+1; k < end; k++, shiftLeft++)
                {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

function removeDuplicates_emailId(arr){

    var end = arr.length;

    for(var i = 0; i < end; i++)
    {
        for(var j = i + 1; j < end; j++)
        {
            if (arr[i].emailId == arr[j].emailId)
            {
                var shiftLeft = j;
                for(var k = j+1; k < end; k++, shiftLeft++)
                {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

function removeDuplicates_id(arr,withoutId){

    var end = arr.length;

    for(var i = 0; i < end; i++)
    {
        for(var j = i + 1; j < end; j++)
        {
            if (!withoutId ? arr[i]._id == arr[j]._id : arr[i] == arr[j])
            {
                var shiftLeft = j;
                for(var k = j+1; k < end; k++, shiftLeft++)
                {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

CommonUtility.prototype.getUserProfileAndStatus = function(userId,req,callback){

    userManagements.selectUserProfileWithContactCount(userId,
        {
            contactsLength: { $size: "$contacts" },
            publicProfileUrl:1,
            firstName:1,
            lastName:1,
            profilePicUrl:1,
            linkedin:1,
            identityShare:1,
            dashboardPopup:1,
            companyName:1,
            designation:1,
            mobileNumber:1,
            timezone:1,
            serviceLogin:1,
            tokenReset:1
        },
        function(error,userProfile) {
            if (error || !checkRequired(userProfile)) {
                callback(false);
            }
            else{
                var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
                var domain = protocol+req.headers.host;
                var obj = {};
                var userUrl = domain+'/'+getValidUniqueName(userProfile.publicProfileUrl);
                var uniqueName = getValidUniqueName(userProfile.publicProfileUrl);
                obj.uniqueUrl = userUrl;
                obj.firstName = userProfile.firstName;
                obj.uniqueName = uniqueName.toLowerCase();
                obj.description =  "I've created my Unique Identity on Relatas ("+userUrl+"). Relatas is a new way to interact with your contacts with Unified Communication, Secure Cloud Scheduling and Intelligent Document Sharing";
                if(userProfile.profilePicUrl){
                    if(userProfile.profilePicUrl.charAt(0) == '/' || userProfile.profilePicUrl.charAt(0) == 'h'){
                        obj.image = domain+''+userProfile.profilePicUrl;
                    }else  obj.image = domain+'/'+userProfile.profilePicUrl;
                }else obj.image = domain+'/images/default.png';
                getDashboardProgressStatus(userProfile,function(statusInfo){
                    obj.statusInfo = JSON.stringify(statusInfo);
                    callback(obj);
                })
            }
        });
};

function getDashboardProgressStatus(profile,callback){
    var obj = {};
    obj.accountCreated = true;
    obj.completeProfile = checkMandatoryFields(profile);
    obj.addLinkedinProfile = checkLinkedInProfile(profile.linkedin);
    obj.syncContacts = checkSyncContacts(profile);
    obj.identityShare = checkShareIdentity(profile.identityShare);
    obj.dashboardPopup = profile.dashboardPopup == true;
    messageAccess.getOneMessageInteraction(profile._id,function(message){
        obj.sendEmailToYourContacts = checkRequired(message);
        callback(obj);
    })
}

function checkMandatoryFields(userProfile) {
    if(checkRequired(userProfile.firstName) && checkRequired(userProfile.lastName) &&
        checkRequired(userProfile.publicProfileUrl) && checkRequired(userProfile.emailId) &&
        checkRequired(userProfile.designation) && checkRequired(userProfile.companyName) &&
        checkRequired(userProfile.mobileNumber) && checkRequired(userProfile.timezone)) {
        return true;
    }else return false;
}

function checkLinkedInProfile(linkedin){
    if(checkRequired(linkedin) && checkRequired(linkedin.id)) {
        return true;

    }else return false;
}

function checkShareIdentity(identityShare){
    if (identityShare) {
        return true;
    }else return false;
}

function checkSyncContacts(userProfile){

    if (userProfile.contactsLength > 0) {
        if (userProfile.serviceLogin != 'linkedin') {
            return true;
        }else {
            if (userProfile.tokenReset) {
                return true;
            }else return false;
        }
    }else return false;
}

function getClientIp(req) {
    var ipAddress;
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        ipAddress = req.connection.remoteAddress;
    }
    return ipAddress;
}

function getClientIpFromReq(req) {
    var forwardedIpsStr = req.headers.userRemoteIP?req.headers.userRemoteIP:null;
    return forwardedIpsStr?forwardedIpsStr.ip:null;
}

CommonUtility.prototype.getClientIp = getClientIp;

CommonUtility.prototype.getClientIp2 = getClientIpFromReq;

CommonUtility.prototype.generateWidgetKey = function(emailId,callback){
    userManagements.findUserProfileByEmailIdWithCustomFields(emailId,{emailId:1,createdDate:1},function(error,user){
        if(checkRequired(user)){
            var secrete = new Date().toISOString();
            var token = jwt.encode({ id: user._id}, secrete);
            var obj = {key:token,secrete:secrete};
            userManagements.updateWidgetKey(user._id,obj,function(result){
                if(result){
                    callback(obj);
                }else callback(false);
            })
        }
    })
};

CommonUtility.prototype.sendMeetingInvitationMailToSender = function(data,suggest){
    // if(appCredential.getServerEnvironment() != 'LIVE'){
    var now = moment().format()
    if(checkRequired(data.senderId) && checkRequired(data.receiverId)){
        data.interactionUrl = domainName+'/interactions/summary/'+data.senderId;
        interaction.totalInteractionsWithOneUserCountAndLastInteracted(data.senderId,data.receiverEmailId,this.castToObjectId,now,function(interactionsCount){

            if(checkRequired(interactionsCount) && interactionsCount.length > 0 && checkRequired(interactionsCount[0])){

                var start = moment(interactionsCount[0].lastInteracted).tz(data.timezone || 'UTC');

                data.lastInteractedOn = start.format("DD-MMM-YYYY");
                data.totalInteractions = interactionsCount[0].count || 0;
                data.commonConnections = 'Not Found';
                emailSenders.sendInvitationMailToSender_NewTemplate(data,suggest);
            }
            else{

                data.lastInteractedOn = "none";
                data.totalInteractions = 0;
                data.commonConnections = 'Not Found';
                emailSenders.sendInvitationMailToSender_NewTemplate(data,suggest);
            }
        });
    }
    else{
        data.lastInteractedOn = "none";
        data.totalInteractions = 0;
        data.commonConnections = 'Not Found';
        emailSenders.sendInvitationMailToSender_NewTemplate(data,suggest);
    }

};

function fetchCompanyFromEmail(email){

    if(checkRequired(email)){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            word = word.toLowerCase();
            return removableTextList.indexOf(word.trim()) > -1
        })
        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
    } else {
        return null
    }
}

CommonUtility.prototype.fetchCompanyFromEmail = fetchCompanyFromEmail

CommonUtility.prototype.isNumber = isNumber

CommonUtility.prototype.validateEmail = validateEmail

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

CommonUtility.prototype.sendMeetingInvitationMailToReceiver = function(data,suggest){
    // if(appCredential.getServerEnvironment() != 'LIVE'){
    var now = moment().format();
    if(checkRequired(data.senderId) && checkRequired(data.receiverId)){
        data.interactionUrl = domainName+'/interactions/summary/'+data.senderId;
        interaction.totalInteractionsWithOneUserCountAndLastInteracted(data.senderId,data.receiverEmailId,this.castToObjectId,now,function(interactionsCount){

            if(checkRequired(interactionsCount) && interactionsCount.length > 0 && checkRequired(interactionsCount[0])){
                var start = moment(interactionsCount[0].lastInteracted).tz(data.timezone || 'UTC');

                data.lastInteractedOn = start.format("DD-MMM-YYYY");
                data.totalInteractions = interactionsCount[0].count || 0;
                data.commonConnections = 'Not Found';
                emailSenders.sendInvitationMailToReceiver_NewTemplate(data,suggest);
            }
            else{
                data.lastInteractedOn = "none";
                data.totalInteractions = 0;
                data.commonConnections = 'Not Found';
                emailSenders.sendInvitationMailToReceiver_NewTemplate(data,suggest);
            }
        });
    }
    else{
        data.lastInteractedOn = "none";
        data.totalInteractions = 0;
        data.commonConnections = 'Not Found';
        emailSenders.sendInvitationMailToReceiver_NewTemplate(data,suggest);
    }
};

// Function to get common connections
function getCommonConnections(authProfile,PublicProfile,callback){

    var linkedinAuthUser = Linkedin.init(authProfile.linkedin.token);
    linkedinAuthUser.people.id(PublicProfile.linkedin.id, ['id','relation-to-viewer:(related-connections:(id,first-name,last-name,siteStandardProfileRequest,pictureUrl))'], function(err, data) {

        if(checkRequired(data)){
            if(data.errorCode == 0 || data.errorCode == '0'){

                linkedinErrorLog.info('error in getting linkedin common connections of '+authProfile.emailId+' error '+JSON.stringify(data));
                callback('',false);
            }
            else{

                if(checkRequired(data.relationToViewer) && checkRequired(data.relationToViewer.relatedConnections) && checkRequired(data.relationToViewer.relatedConnections.values) && data.relationToViewer.relatedConnections.values.length > 0) {

                    var connections = data.relationToViewer.relatedConnections.values;
                    var comConnectionsLength = connections.length >= 1 ? 1 : connections.length;
                    var commonConnectionTable = '<table>';
                    var commonConnectionImages ='<tr>';
                    var commonConnectionNames = '<tr>';
                    for(var i=0; i<comConnectionsLength; i++){
                        var linkedinUrl = connections[i].siteStandardProfileRequest.url;
                        var firstName = connections[i].firstName || '';
                        var lastName = connections[i].lastName || '';
                        var title = firstName+' '+lastName;
                        var iUrl = connections[i].pictureUrl || 'http://relatas.com/images/default.png';

                        commonConnectionImages += '<td class="social-connection-item">';
                        commonConnectionImages += '<a target="_blank" href='+linkedinUrl+'><div class="profile-picture-small social-connection-profile-picture">';
                        commonConnectionImages += '<img class="profile-pic-img-small" style="border-radius: 55%;" id="lImage1" src='+iUrl+' title='+title.replace(/\s/g, '&nbsp;')+'><br></div></a>';
                        commonConnectionImages += '</td>';
                        commonConnectionNames += '<td class="social-connection-item"><span title='+title.replace(/\s/g, '&nbsp;')+'>'+getTextLength(title,11)+'</span></td>';
                    }
                    commonConnectionImages += '</tr>';
                    commonConnectionNames += '</tr>';
                    commonConnectionTable += commonConnectionImages+commonConnectionNames+'</table>';
                    callback(commonConnectionTable,comConnectionsLength > 0);
                }
                else{
                    callback('',false);
                }
            }
        }
    })
}

function getTextLength(text,maxLength){

    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

// Router to get common connections
function validateLinkedinUser(userId,publicUserId,callback){
    // This is to get authenticated user profile

    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,linkedin:1,firstName:1,lastName:1},function(error,authenticatedUserProfile){

        if(error){

            callback('',false)
        }
        else if(checkRequired(authenticatedUserProfile) && checkRequired(authenticatedUserProfile.linkedin) && checkRequired(authenticatedUserProfile.linkedin.id)){
            // This is to get Public profile

            userManagements.findUserProfileByIdWithCustomFields(publicUserId,{emailId:1,linkedin:1,firstName:1,lastName:1},function(error,publicUserProfile){
                if(error){

                    callback('',false)
                }
                else if(checkRequired(publicUserProfile) && checkRequired(publicUserProfile.linkedin) && checkRequired(publicUserProfile.linkedin.id)){

                    getCommonConnections(authenticatedUserProfile,publicUserProfile,callback);
                }
                else{

                    callback('',false)
                }
            });
        }
        else{
            callback('',false)
        }
    });
}

CommonUtility.prototype.getUserId = function(user){
    return jwt.decode(user ,"alpha").id;
};

CommonUtility.prototype.getCommitDayTimeStart = getCommitDayTimeEnd

function getCommitDayTimeEnd(companyDetails,timezone) {

    var dateTime = moment().day("Monday");
    if(companyDetails && companyDetails.commitDay){
        dateTime = moment().day(companyDetails.commitDay)
    }

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    dateTime = moment(dateTime).tz(timezone)

    if(companyDetails && companyDetails.commitHour){
        dateTime = dateTime.hour(parseInt(companyDetails.commitHour))
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    } else {
        dateTime = dateTime.hour(18)
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    }

    if(new Date(dateTime)< new Date()){
        dateTime = moment(dateTime).add(1,'week') //well past commit time
    }

    return new Date(dateTime);
}

CommonUtility.prototype.getThisWeekCommitCutoffDate = getThisWeekCommitCutoffDate

function getThisWeekCommitCutoffDate(companyDetails,timezone) {

    var dateTime = moment().day("Monday");
    if(companyDetails && companyDetails.commitDay){
        dateTime = moment().day(companyDetails.commitDay)
    }

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    dateTime = moment(dateTime).tz(timezone)

    if(companyDetails && companyDetails.commitHour){
        dateTime = dateTime.hour(parseInt(companyDetails.commitHour))
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    } else {
        dateTime = dateTime.hour(18)
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    }

    return new Date(dateTime);
}

//Rajiv -- for opening web pages on mobile, get user ID this way. Pass the req data. Refer storeMobileSessionSpecInfo for what objects are stored by mobile.
CommonUtility.prototype.getUserIdFromMobileOrWeb = function(req){
    if(req.session.mobileSession){
        return jwt.decode(req.session.user,MOBILE_TOKEN_SECRETE).id;
    }else{
        if(req.headers.token){
            return jwt.decode(req.headers.token,MOBILE_TOKEN_SECRETE).id;
        }else{
            return jwt.decode(req.user,"alpha").id
        }
    }
}

CommonUtility.prototype.getUserFromSession = function(req){
    return req.session.profile
}

CommonUtility.prototype.decodeToken = function(token,secrete){
    try{
        return jwt.decode(token ,secrete,true).id;
    }catch(e){
        return null;
    }
}

function decodeToken(token,secrete){
    try{
        return jwt.decode(token,secrete,true).id;
    }catch(e){
        return null;
    }
}

CommonUtility.prototype.decodeStatusToken = function(token,secrete){
    try{
        return jwt.decode(token ,secrete,true);
    }catch(e){
        return null;
    }
}

CommonUtility.prototype.generateToken = function(tokenSecret,userId){
    return jwt.encode({id:userId}, tokenSecret);
};

CommonUtility.prototype.generateTokenWithObject = function(tokenSecret,obj){
    return jwt.encode(obj, tokenSecret);
};

CommonUtility.prototype.getRandomNumber = function(min, max){

    var offset = min;
    var range = (max - min) + 1;
    return Math.floor( Math.random() * range) + offset;

};

CommonUtility.prototype.makeFavoriteContacts = function(userId){
    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,isFavoriteUpdated:1,timezone:1,corporateUser:1,firstName:1,lastName:1,companyId:1},function(err,user){
        // if(!user.isFavoriteUpdated) {

        userManagements.getTeamMembers(user.companyId, function(err, teamData) {
            teamData.push({_id:castToObjectId(userId),emailId:user.emailId,firstName:user.firstName,lastName:user.lastName});

            var teamMembers = _.pluck(teamData,"emailId");

            interaction.getInteractionsToAddFavoriteContacts(user._id, function (interactions) {
                var emailIds = _.pluck(interactions, "_id")
                oppManagementObj.allOpenOpportunityContacts(user._id,function(err,oppo) {
                    var oppoEmails = _.pluck(oppo, "contactEmailId");
                    var concatArray = _.uniq(emailIds.concat(oppoEmails))
                    var finalEmailArray = [];
                    var company = fetchCompanyFromEmail(user.emailId)
                    var companyReg = new RegExp(company, 'i');
                    if(user.corporateUser && company){
                        concatArray.forEach(function(a){
                            if(!companyReg.test(a)){
                                finalEmailArray.push(a);
                            }
                        })
                    }
                    else{
                        finalEmailArray = concatArray;
                    }

                    finalEmailArray = _.difference(finalEmailArray,teamMembers);

                    profileManagementClassObj.updateContactsToFavorite(userId, finalEmailArray, function (result) {

                    });
                });
            });
        })

        // }
    });
};

CommonUtility.prototype.updateInteractions = function(userId){
    updateUserInteractions(userId);
}

function updateUserInteractions(userId){
    userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0,google:0,linkedin:0,twitter:0,facebook:0},function(err,user){
        if(checkRequired(user)){
            var updateObj = {
                userId:userId,
                emailId:user.emailId,
                firstName:user.firstName,
                lastName:user.lastName,
                publicProfileUrl:user.publicProfileUrl,
                profilePicUrl:user.profilePicUrl
            };

            if(checkRequired(user.mobileNumber)){
                updateObj.mobileNumber = user.mobileNumber;
            }

            if(checkRequired(user.companyName)){
                updateObj.companyName = user.companyName;
            }

            if(checkRequired(user.designation)){
                updateObj.designation = user.designation;
            }

            if(checkRequired(user.location)){
                updateObj.location = user.location;
            }

        }
        interaction.updateInteractionWithProfile(userId,user.emailId,user.mobileNumber,updateObj);
    });
}

CommonUtility.prototype.contains = function(str, subStr){
    return str.indexOf(subStr) != -1;
};

CommonUtility.prototype.isLoggedInUserBoolean = function(req){
    if(req.headers.token){
        return true;
    }else{
        return req.isAuthenticated();
    }
}

CommonUtility.prototype.isLoggedInUser = function(req, res, next){
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        return next();
    }else
    {
        // if they aren't redirect them to the home page
        // req.logOut();
        return res.redirect('/');
    }
};

CommonUtility.prototype.checkArray = function (value) {
    return Array.isArray(value);
}

CommonUtility.prototype.isLoggedInUserToGoNext = function(req, res, next){
    // if user is authenticated in the session, carry on

    if (req.isAuthenticated()){
        return next();
    }else {
        // return res.send(errorObj.generateErrorResponse({status:statusCodes.AUTHORIZATION_ERROR_CODE,key:'LOGIN_REQUIRED'}));
        // return res.send("<p ng-init='sessionTimeoutReload()'></p>");
        // req.logOut();
        // return res.redirect("/")
        return res.redirect('/');
    }
};

//MOBILE SESSION INFORMATION STORE
CommonUtility.prototype.storeMobileSessionSpecInfo = function(req,callBack){

    var user
    if(req.headers.token){
        user = req.headers.token;
        req.session.user = user;
    }
    if(req.headers.mobilesession){
        req.session.mobileSession = true
    }

    req.session.save();
    callBack(true)

}

CommonUtility.prototype.isLoggedInUserOrMobile = function(req,res,next){
    if(req.headers.token){
        var userId = decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE)
        if(checkRequired(userId)){
            return next();
        }else{
            // req.logOut();
            return res.redirect('/');
        }
    }else if(req.session.mobileSession){
        var userId1 = decodeToken(req.session.user,MOBILE_TOKEN_SECRETE)
        if(checkRequired(userId1)){
            return next();
        }else{
            // req.logOut();
            return res.redirect('/');
        }
    }
    else{
        if (req.isAuthenticated()){
            return next();
        }else
        {
            // req.logOut();
            return res.redirect('/');
        }
    }
}

CommonUtility.prototype.getPosition = function(companyName,designation){
    var position = '';

    if(this.checkRequired(companyName) && this.checkRequired(designation)){
        position = designation+', '+companyName
    }
    else if(this.checkRequired(companyName) && !this.checkRequired(designation)){
        position = companyName
    }
    else if(!this.checkRequired(companyName) && this.checkRequired(designation)){
        position = designation
    }

    return position;
};

CommonUtility.prototype.getLocation = function(location,currentLocation){
    var locationNew = '';
    if(this.checkRequired(location)){
        locationNew = location;
    }
    else if(this.checkRequired(currentLocation) && this.checkRequired(currentLocation.city)){
        locationNew = currentLocation.city;
    }
    return locationNew;
};

CommonUtility.prototype.getTimezone = function(timezone){
    if(this.checkRequired(timezone) && this.checkRequired(timezone.name)){
        return timezone.name;
    }
    else return 'UTC';
};

CommonUtility.prototype.castToObjectId = function(id){
    return mongoose.Types.ObjectId(id)
};

function castToObjectId(id){
    return mongoose.Types.ObjectId(id)
}

CommonUtility.prototype.castListToObjectIds = function(ids){
    return _.map(ids, function(id){
        return castToObjectId(id)
    })
};

CommonUtility.prototype.replaceProfileInSession = function(userId,req,callback){
    userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,user){
        if(error){
            logger.info('Error in replaceProfileInSession():CommonUtility  user: '+userId ,error);
        }

        if(user){
            req.session.profile = user;
            callback(user)
        }
        else callback(null);
    })
};

CommonUtility.prototype.getProfileOrStoreProfileInSession = function(userId,req,callback){

    if(req.session && req.session.profile){
        callback(req.session.profile)
    } else {
        userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,user){
            if(error){
                logger.info('Error in getProfileOrStoreProfileInSession():CommonUtility  user: '+userId ,error);
            }

            if(checkRequired(user)){
                req.session.profile = user;
                callback(user)
            }
        });
    }
};

CommonUtility.prototype.updateContactMultiCollectionLevel = function(id,isEmailId){
    var q;
    if(isEmailId){
        q = {emailId:id}
    }
    else q = {_id:id};

    userManagements.selectUserProfileCustomQuery(q,{emailId:1,firstName:1,lastName:1,companyName:1,designation:1,mobileNumber:1,skypeId:1,location:1,linkedin:1,facebook:1,twitter:1},function(error,user){
        if(error){
            logger.info('Error in updateContactMultiCollectionLevel():CommonUtility ',error,q);
        }
        if(checkRequired(user)){
            user = JSON.parse(JSON.stringify(user));

            if(checkRequired(user.facebook) && checkRequired(user.facebook.name)){
                user.facebookUserName = user.facebook.name;
            }

            if(checkRequired(user.twitter) && checkRequired(user.twitter.userName)){
                user.twitterUserName = user.twitter.userName;
            }

            contactObj.updateContactsCollectionLevelMulti(user);
        }
    })
};

function contains(str, subStr){
    return str.indexOf(subStr) != -1;
}

CommonUtility.prototype.sendCalendarPasswordRequest = function(user,publicUser,callback){
    if(this.checkRequired(user) && this.checkRequired(publicUser)){
        if(this.checkRequired(user._id) && this.checkRequired(user.firstName) && this.checkRequired(user.emailId) && this.checkRequired(publicUser._id) && this.checkRequired(publicUser.firstName) && this.checkRequired(publicUser.emailId)){

            var obj = {
                requestFrom:{
                    userId:user._id,
                    emailId:user.emailId
                },
                requestTo:{
                    userId:publicUser._id,
                    emailId:publicUser.emailId
                }
            };
            calendarPasswordManagementsObj.saveRequest(obj,function(error,status,calendarPasswordReq){

                if(status == 'success'){
                    if(checkRequired(calendarPasswordReq)){
                        emailSenders.sendCalendarPasswordRequestMail({
                            toEmailId:publicUser.emailId,
                            toName:publicUser.firstName
                        });

                        var interaction = {};
                        interaction.userId = calendarPasswordReq.requestTo.userId;
                        interaction.emailId = calendarPasswordReq.requestTo.emailId;
                        interaction.firstName = calendarPasswordReq.requestTo.firstName;
                        interaction.action = 'receiver'
                        interaction.type = 'calendar-password';
                        interaction.subType = 'calendar-password';
                        interaction.refId = calendarPasswordReq._id;
                        interaction.source = 'relatas';
                        interaction.title = 'Calendar password request';
                        interaction.description = '';

                        var interactionNew = {};
                        interactionNew.userId = calendarPasswordReq.requestFrom.userId;
                        interactionNew.emailId = calendarPasswordReq.requestFrom.emailId;
                        interactionNew.firstName = calendarPasswordReq.requestFrom.firstName;
                        interactionNew.action = 'sender';
                        interactionNew.type = 'calendar-password';
                        interactionNew.subType = 'calendar-password';
                        interactionNew.refId = calendarPasswordReq._id;
                        interactionNew.source = 'relatas';
                        interactionNew.title = 'Calendar password request';
                        interactionNew.description = '';

                        storeAcceptMeetingInteractions(interactionNew,interaction);
                        callback(true)
                    }
                    else callback(false)
                }
                else callback(false)
            })
        }
        else callback(false)
    }
    else callback(false);
};

CommonUtility.prototype.getValidUniqueNameWithIdentity = function(rIdentity){
    rIdentity = rIdentity.toLowerCase();
    rIdentity = rIdentity.replace(/\s/g,'');
    return rIdentity;
};

function getInteractionObject(userId,user_twitter_id,user_twitter_name,firstName,lastName,publicProfileUrl,profilePicUrl,companyName,designation,location,mobileNumber,action,interactionType,subType,emailId,refId,source,title,description,interactionDate,endDate,trackId,trackInfo,emailContentId,googleAccountEmailId, toCcBcc, emailThreadId,toList,ccList,hasUnsubscribe){

    var obj = {
        userId:checkRequired(userId) ? userId : null,
        user_twitter_id:checkRequired(user_twitter_id) ? user_twitter_id : null,
        user_twitter_name:checkRequired(user_twitter_name) ? user_twitter_name : null,
        firstName:checkRequired(firstName) ? firstName : null,
        lastName:checkRequired(lastName) ? lastName : null,
        publicProfileUrl:checkRequired(publicProfileUrl) ? publicProfileUrl : null,
        profilePicUrl:checkRequired(profilePicUrl) ? profilePicUrl : null,
        emailId:checkRequired(emailId) ? emailId.toLowerCase() : null,
        // emailId:checkRequired(emailId) ? emailId : null,
        //companyName:checkRequired(companyName) ? companyName : null,
        companyName: checkRequired(emailId) ? fetchCompanyFromEmail(emailId) : null,
        designation:checkRequired(designation) ? designation : null,
        location:checkRequired(location) ? location : null,
        mobileNumber:checkRequired(mobileNumber) ? mobileNumber : null,
        action:action,
        interactionType:interactionType,
        subType:subType,
        refId:refId,
        source:checkRequired(source) ? source : null,
        title:checkRequired(title) ? title : null,
        description:checkRequired(description) ? description : null,
        interactionDate:checkRequired(interactionDate) ? new Date(interactionDate) : new Date(),
        endDate:checkRequired(endDate) ? new Date(endDate) : null,
        createdDate:new Date(),
        trackId:checkRequired(trackId) ? trackId : null,
        emailContentId:checkRequired(emailContentId) ? emailContentId : null,
        googleAccountEmailId:checkRequired(googleAccountEmailId) ? googleAccountEmailId : null,
        toCcBcc:checkRequired(toCcBcc) ? toCcBcc : null,
        emailThreadId:checkRequired(emailThreadId) ? emailThreadId : null,
        to:toList?toList:[],
        cc:ccList?ccList:[],
        hasUnsubscribe:hasUnsubscribe?true:false
    };
    if(checkRequired(trackInfo)){
        obj.trackInfo = trackInfo;
    }
    return obj;
}

CommonUtility.prototype.getInteractionObject = function(userId,user_twitter_id,user_twitter_name,firstName,lastName,publicProfileUrl,profilePicUrl,companyName,designation,location,mobileNumber,action,interactionType,subType,emailId,refId,source,title,description,interactionDate,endDate,trackId,trackInfo,emailContentId,googleAccountEmailId, toCcBcc,threadId,toList,ccList,hasUnsubscribe){
    return getInteractionObject(userId,user_twitter_id,user_twitter_name,firstName,lastName,publicProfileUrl,profilePicUrl,companyName,designation,location,mobileNumber,action,interactionType,subType,emailId,refId,source,title,description,interactionDate,endDate,trackId,trackInfo,emailContentId,googleAccountEmailId, toCcBcc,threadId,toList,ccList,hasUnsubscribe);
};

function storeAcceptMeetingInteractions(interactionSender,interactionReceiver){
    userManagements.findUserProfileByIdWithCustomFields(interactionSender.userId,{emailId:1,firstName:1,lastName:1,companyName:1,designation:1,mobileNumber:1,publicProfileUrl:1,profilePicUrl:1,skypeId:1,location:1,twitter:1},function(error,user){
        var feed1 = interactionSender;
        var feed2 = interactionReceiver;
        if(user){
            feed1 = getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,interactionSender.action,interactionSender.type,interactionSender.subType,user.emailId,interactionSender.refId,'relatas',interactionSender.title,interactionSender.description,interactionSender.interactionDate,interactionSender.endDate,interactionSender.trackId,interactionSender.trackInfo,null);
        }
        else feed1 = getInteractionObject(interactionSender.userId,null,null,interactionSender.firstName,interactionSender.lastName,interactionSender.publicProfileUrl,interactionSender.profilePicUrl,interactionSender.companyName,interactionSender.designation,interactionSender.location,interactionSender.mobileNumber,interactionSender.action,interactionSender.type,interactionSender.subType,interactionSender.emailId,interactionSender.refId,'relatas',interactionSender.title,interactionSender.description,interactionSender.interactionDate,interactionSender.endDate,interactionSender.trackId,interactionSender.trackInfo,null);

        userManagements.findUserProfileByIdWithCustomFields(interactionReceiver.userId,{emailId:1,firstName:1,lastName:1,companyName:1,designation:1,mobileNumber:1,publicProfileUrl:1,profilePicUrl:1,skypeId:1,location:1,twitter:1},function(error,user2){
            if(user2){
                feed2 = getInteractionObject(user2._id,null,null,user2.firstName,user2.lastName,user2.publicProfileUrl,user2.profilePicUrl,user2.companyName,user2.designation,user2.location,user2.mobileNumber,interactionReceiver.action,interactionReceiver.type,interactionReceiver.subType,user2.emailId,interactionReceiver.refId,'relatas',interactionReceiver.title,interactionReceiver.description,interactionReceiver.interactionDate,interactionReceiver.endDate,interactionReceiver.trackId,interactionReceiver.trackInfo,null);
            }
            else feed2 = getInteractionObject(interactionReceiver.userId,null,null,interactionReceiver.firstName,interactionReceiver.lastName,interactionReceiver.publicProfileUrl,interactionReceiver.profilePicUrl,interactionReceiver.companyName,interactionReceiver.designation,interactionReceiver.location,interactionReceiver.mobileNumber,interactionReceiver.action,interactionReceiver.type,interactionReceiver.subType,interactionReceiver.emailId,interactionReceiver.refId,'relatas',interactionReceiver.title,interactionReceiver.description,interactionReceiver.interactionDate,interactionReceiver.endDate,interactionReceiver.trackId,interactionReceiver.trackInfo,null);

            var intArr = [feed1,feed2];
            addInteractionsMeetingNext(intArr,interactionSender,interactionReceiver);
        })
    })
}

function addInteractionsMeetingNext(intArr,interactionSender,interactionReceiver){
    if(checkRequired(interactionSender.userId) && checkRequired(interactionReceiver.userId) && interactionSender.userId.toString() == interactionReceiver.userId.toString()){
        var rIn = [];
        for(var i=0; i<intArr.length; i++){
            if(intArr[i].action == 'sender'){
                if(rIn.length == 0){
                    rIn.push(intArr[i])
                }
            }
        }

        interaction.addInteractionsMeeting(castToObjectId(interactionSender.userId),rIn,function(isSuccess){
            if(!isSuccess){
                logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting self ',interactionSender.emailId,interactionSender.refId);
            }
        })
    }
    else{
        if(checkRequired(interactionSender.userId)){
            interaction.addInteractionsMeeting(castToObjectId(interactionSender.userId),intArr,function(isSuccess){
                if(!isSuccess){
                    logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting ',interactionSender.emailId,interactionSender.refId);
                }
            })
        }
        if(checkRequired(interactionReceiver.userId)){
            interaction.addInteractionsMeeting(castToObjectId(interactionReceiver.userId),intArr,function(isSuccess){
                if(!isSuccess){
                    logger.info('Meeting interactions failed addInteractionsMeetingNext():Meeting ',interactionReceiver.emailId,interactionReceiver.refId);
                }
            })
        }
    }
}

CommonUtility.prototype.formatInteractionTypeGraphData = function(typeCounts){

    if(typeCounts[0] && typeCounts[0].typeCounts.length > 0){
        var obj = {
            _id:null,
            totalCount:typeCounts[0].totalCount,
            maxCount:typeCounts[0].maxCount,
            typeCounts:[]
        };

        for(var i=0; i<typeCounts[0].typeCounts.length; i++){
            if(['call','sms','facebook','twitter','linkedin'].indexOf(typeCounts[0].typeCounts[i]._id) != -1){
                obj.typeCounts.push({_id:typeCounts[0].typeCounts[i]._id,count:typeCounts[0].typeCounts[i].count})
            }

            // Meeting
            if(['google-meeting','meeting','calendar-password','task'].indexOf(typeCounts[0].typeCounts[i]._id) != -1){
                var exists = false;
                var index;
                for(var j=0; j<obj.typeCounts.length; j++){
                    if(obj.typeCounts[j]._id == 'meeting'){
                        exists = true;
                        obj.typeCounts[j].count += typeCounts[0].typeCounts[i].count
                    }
                }
                if(!exists){
                    obj.typeCounts.push({_id:'meeting',count:typeCounts[0].typeCounts[i].count})
                }
            }

            if(['meeting-comment','email','document-share','message'].indexOf(typeCounts[0].typeCounts[i]._id) != -1){
                var exists2 = false;
                for(var k=0; k<obj.typeCounts.length; k++){
                    if(obj.typeCounts[k]._id == 'email'){
                        obj.typeCounts[k].count += typeCounts[0].typeCounts[i].count
                        exists2 = true;
                    }
                }
                if(!exists2){
                    obj.typeCounts.push({_id:'email',count:typeCounts[0].typeCounts[i].count})
                }
            }
        }
        return obj.typeCounts.length ? [obj] : [];
    }
    else return []
};

CommonUtility.prototype.getAccountObj = getAccountObj;

function getAccountObj(companyId,ownerId,ownerEmailId,emailIds,callback) {

    var accounts = [],names=[];
    var updateObj = {};

    _.each(emailIds,function (el) {
        var name = fetchCompanyFromEmail(el);
        if(name){
            names.push(name);

            accounts.push({
                companyId:castToObjectId(companyId),
                ownerId:castToObjectId(ownerId),
                ownerEmailId:ownerEmailId,
                name:fetchCompanyFromEmail(el),
                createdDate:new Date(),
                access:[{
                    emailId:ownerEmailId
                }]
            })
        }
    });

    updateObj = {
        names:names,
        accounts:accounts
    }

    callback(updateObj);
}

CommonUtility.prototype.getCompanyObj = getCompanyObj;

CommonUtility.prototype.getCompanyHead = function(userId,companyId,callback){
    var query = {companyId:companyId,orgHead:true}, projection = {emailId:1,firstName:1}
    userManagements.selectUserProfileCustomQuery(query, projection,function (err,companyHead) {
        userManagements.selectUserProfileCustomQuery({_id:userId}, {emailId:1,firstName:1},function (err,reportingManager) {
            callback(companyHead,reportingManager)
        });
    });
};

CommonUtility.prototype.getAdminAndReportingManagers = function(userId1,userId2,callback){
    userManagements.selectUserProfilesCustomQuery({_id:{$in:[userId1,userId2]}}, {emailId:1,firstName:1,hierarchyParent:1},function (err,reportingManagers) {

        var hierarchyParents = _.pluck(reportingManagers,"hierarchyParent");
        var userIds = [];

        _.each(hierarchyParents,function (el) {
            if(el){
                userIds.push(castToObjectId(el))
            }
        })

        userManagements.selectUserProfilesCustomQuery({_id:{$in:userIds}}, {emailId:1,firstName:1},function (err,reportingManagersDetails) {
            callback(err,reportingManagersDetails)
        });
    });
};

CommonUtility.prototype.getCompanyLoginService = function(url,callback){

    company.findCompanyByUrl({url:url},{_id:1},function(companyProfile){
        if(companyProfile && companyProfile._id) {
            userManagements.selectUserProfileCustomQuery({companyId:companyProfile._id,serviceLogin:{$exists:true}}, {serviceLogin:1},function (err,serviceLogin) {
                callback(err,serviceLogin)
            });
        } else {
            callback(null,null)
        }
    });
};

CommonUtility.prototype.getGoogleAccountByGoogleAccountEmailId = function(google,googleAccountEmailId){
    if(!this.checkRequired(googleAccountEmailId)){
        return google[0];
    }

    var account = null;
    for(var i=0; i<google.length; i++){
        if(google[i].emailId == googleAccountEmailId){
            account = google[i];
        }
    }
    if(account == null){
        account = google[0];
    }
    return account;
};

CommonUtility.prototype.getCompanyObjDB = function(req,callback){

    var common = this;
    var domain = common.getSubDomain(req);
    if(domain && common.isLoggedInUserBoolean(req)){
        var userId = common.getUserId(req.user);
        company.findOrCreateCompanyByUrl(common.getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                userManagements.findUserProfileByIdWithCustomFields(userId,{corporateUser:1,companyId:1},function(error,user){
                    if(!error && user){
                        if(user.corporateUser && user.companyId.toString() == companyProfile._id.toString()){
                            req.session.companyId = companyProfile._id;
                            callback({isLoggedIn : common.isLoggedInUserBoolean(req),companyId:companyProfile._id.toString(),company:JSON.stringify(companyProfile),companyName:companyProfile.companyName})
                        }
                        else callback({})
                    }
                    else callback({})
                })
            }
            else callback({})
        });
    }
    else callback({})
};

CommonUtility.prototype.validateCorporateAdmin = function(companyId,userId,callback){
    userManagements.selectUserProfileCustomQuery({_id:userId,companyId:companyId,corporateAdmin:true},{emailId:1,timezone:1},function(error,user){
        if(error){
            callback(false)
        }
        else if(checkRequired(user) && checkRequired(user.emailId)){
            callback(true,user)
        }
        else callback(false)
    })
};
CommonUtility.prototype.getRandomString = function(randomStringLength) {
    var randomString = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < randomStringLength; i++)
        randomString += possible.charAt(Math.floor(Math.random() * possible.length));

    return randomString;
}

CommonUtility.prototype.getReqHttp = function(req){
    return 'http' + (req.connection.encrypted ? 's' : '') + '://';
};

CommonUtility.prototype.getAppConfiguration = function(entityId,callback){

    var query =  getAppConfigQuery(entityId);
    query.exec(function(err,appConfigData){
        if(err){
            callback(err);
        }
        else {
            callback(appConfigData);
        }
    });
};

CommonUtility.prototype.getNewGoogleToken = function (token,refreshToken,callback) {
    var tokenProvider = new GoogleTokenProvider({
        refresh_token: refreshToken,
        client_id:     authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        access_token: token
    });

    tokenProvider.getToken(function (err, token) { // start of token provider
        if (err) {
            callback(false)
        }
        else{
            callback(token)
        }
    })
};

CommonUtility.prototype.getNumString = function(num){
    return num < 10 ? '0'+num : ''+num
}

CommonUtility.prototype.buildOutlookContactObject = function (contact) {

    var contactImageLink = null;

    if(contact.id){
        contactImageLink = "https://graph.microsoft.com/v1.0/me/contacts/"+contact.id+"/photo";
    }

    var fullName;

    var cEmailId = null;
    if(contact.emailAddresses.length>0 && contact.emailAddresses[0].address){
        cEmailId = contact.emailAddresses[0].address.trim().toLowerCase()
    }

    if(checkRequired(contact.givenName && contact.surname)){
        fullName = contact.givenName + ' '+ contact.surname
    } else if(checkRequired(contact.givenName)){
        fullName = contact.givenName
    } else if(checkRequired(contact.displayName)){
        fullName = contact.displayName
    } else {
        fullName = cEmailId
    }

    return {
        personId:null,
        personName:fullName,
        personEmailId:cEmailId,
        birthday:contact.birthday || null,
        companyName:contact.companyName || null,
        designation:contact.jobTitle || null,
        count:0,
        addedDate:new Date(),
        verified:false,
        relatasUser:false,
        relatasContact:true,
        mobileNumber:contact.mobilePhone?contact.mobilePhone:null,
        location:contact.officeLocation?contact.officeLocation:null,
        source:'outlook-login',
        account:{
            name:cEmailId?fetchCompanyFromEmail(cEmailId):null,
            selected:false,
            updatedOn:new Date()
        },
        contactImageLink:contactImageLink || null
    }
};

CommonUtility.prototype.buildOutlookInteractionObject = function (userProfile,interaction,interactionWithEmailId,action,userId,senderName,toCcBcc,to,cc) {

    var fullName = senderName.split(" ");

    return {
        ownerId:castToObjectId(userProfile._id),
        ownerEmailId:userProfile.emailId,
        userId:userId?castToObjectId(userId):null,
        user_twitter_id:checkRequired(interaction.user_twitter_id) ? interaction.user_twitter_id : null,
        user_twitter_name:checkRequired(interaction.user_twitter_name) ? interaction.user_twitter_name : null,
        firstName:fullName[0],
        lastName:fullName[1],
        publicProfileUrl:checkRequired(interaction.publicProfileUrl) ? interaction.publicProfileUrl : null,
        profilePicUrl:checkRequired(interaction.profilePicUrl) ? interaction.profilePicUrl : null,
        emailId:checkRequired(interactionWithEmailId) ? interactionWithEmailId.toLowerCase() : null,
        companyName: checkRequired(interactionWithEmailId) ? fetchCompanyFromEmail(interactionWithEmailId) : null,
        designation:checkRequired(interaction.designation) ? interaction.designation : null,
        location:checkRequired(interaction.location) ? interaction.location : null,
        mobileNumber:checkRequired(interaction.mobileNumber) ? interaction.mobileNumber : null,
        action:action,
        interactionType:'email',
        subType:'email',
        refId:interaction.id || null,
        source:'outlook-login',
        title:checkRequired(interaction.subject) ? interaction.subject : null,
        description:checkRequired(interaction.description) ? interaction.description : null,
        interactionDate:checkRequired(interaction.receivedDateTime) ? new Date(interaction.receivedDateTime) : new Date(),
        endDate:checkRequired(interaction.endDate) ? new Date(interaction.endDate) : null,
        createdDate:new Date(),
        trackId:checkRequired(interaction.trackId) ? interaction.trackId : null,
        emailContentId:checkRequired(interaction.id) ? interaction.id : null,
        googleAccountEmailId:checkRequired(interaction.googleAccountEmailId) ? interaction.googleAccountEmailId : null,
        toCcBcc:checkRequired(toCcBcc) ? toCcBcc : null,
        emailThreadId:checkRequired(interaction.conversationId) ? interaction.conversationId : null,
        to:to,
        cc:cc
    };
};

CommonUtility.prototype.buildOutlookMeetingObject = function (userProfile,meetingInstance) {

    var toList = [];
    var attendees = [];
    var participants = [{emailId:userProfile.emailId}];

    if(meetingInstance.attendees.length>0){
        _.each(meetingInstance.attendees,function (participant) {

            if(participant.emailAddress.address){
                attendees.push(participant.emailAddress.address.toLowerCase());
            }

            attendees.push(meetingInstance.organizer.emailAddress.address.toLowerCase())

            //To fetch Receiver's Id.
            if(meetingInstance.organizer.emailAddress.address && meetingInstance.organizer.emailAddress.address.toLowerCase() != participant.emailAddress.address.toLowerCase()){
                var fullName = participant.emailAddress.name.split(" ");
                var toListObj = {
                    receiverId: null,
                    receiverFirstName: fullName[0],
                    receiverLastName: fullName[1],
                    receiverEmailId: participant.emailAddress.address.toLowerCase(),
                    receiverPicUrl:null,
                    mobileNumber :null,
                    location:null,
                    publicProfileUrl:null,
                    designation:null
                }

                toList.push(toListObj);
            }

            participants.push({emailId: participant.emailAddress.address?participant.emailAddress.address.toLowerCase():null})
        });
    }

    participants = _.uniq(participants,'emailId');

    var isAccepted = false;
    if(meetingInstance.responseStatus.response == 'organizer'){
        isAccepted = false
    } else if(meetingInstance.responseStatus.response = 'accepted') {
        isAccepted = true;
    }

    var scheduleTimeSlots= [];

    var location = meetingInstance.location.displayName || ''

    if(meetingInstance.location.address){

        var street = meetingInstance.location.address.street?meetingInstance.location.address.street+', ':''
        var city = meetingInstance.location.address.city?meetingInstance.location.address.city+', ':''
        var state = meetingInstance.location.address.state?meetingInstance.location.address.state+', ':''
        var country = meetingInstance.location.address.countryOrRegion?meetingInstance.location.address.countryOrRegion+', ':''

        location = location + street + city + state + country
    }

    scheduleTimeSlots.push({
        _id: new customObjectId(),
        isAccepted: isAccepted,
        start: {
            date: new Date(meetingInstance.start.dateTime)|| null
        },
        end: {
            date: new Date(meetingInstance.end.dateTime)|| null
        },
        title: meetingInstance.subject || null,
        location: location,
        suggestedLocation: null,
        locationType: "In-Person",
        description: meetingInstance.bodyPreview || ''
    });

    var meeting = {
        invitationId:meetingInstance.id,
        suggested : false,
        readStatus : false,
        scheduledDate :meetingInstance.createdDateTime?new Date(meetingInstance.createdDateTime):null,
        tasks : null,
        toDo : null,
        meetingLocation : {
            region : meetingInstance.location.address?meetingInstance.location.address.state:null,
            city : meetingInstance.location.address?meetingInstance.location.address.city:null,
            country : meetingInstance.location.address?meetingInstance.location.address.countryOrRegion:null
        },
        comments : [],
        iCalUId:meetingInstance.iCalUId,
        participants:participants,
        toList:toList,
        scheduleTimeSlots:scheduleTimeSlots,
        officeEventId:meetingInstance.officeEventId,
        isOfficeOutlookMeeting: true,
        isGoogleMeeting:false,
        lastModified:new Date(meetingInstance.lastModifiedDateTime),
        senderEmailId:meetingInstance.organizer.emailAddress.address.toLowerCase(),
        senderName:meetingInstance.organizer.emailAddress.name,
        deleted:meetingInstance.isCancelled,
        selfCalendar:true,
        mobileNumber :null,
        location:null,
        publicProfileUrl:null,
        designation:null,
        LIUoutlookEmailId:userProfile.emailId
        // recurrence:{
        //     isRecurring:isRecurring?isRecurring:false,
        //     date:isRecurring? new Date():false
        // }
    };

    return {meeting:meeting,attendees:attendees}
};

CommonUtility.prototype.getInvalidEmailListFromDb = function (callback) {
    inavlidEmailListCollection.findOne({type:"invalidEmail"}, function (err, lists) {
        if(!err){
            callback(lists)
        } else {
            callback("NO_LISTS_FOUND")
        }
    });
}

CommonUtility.prototype.isValidEmail = function (email, invalidList) {
    var ok = true;
    if(invalidList) {
        invalidList.invalid.forEach(function (tester) {
            if (tester.test(email)) {
                ok = false;
                return false;
            }
        });
    }
    return ok;
}

CommonUtility.prototype.insertValidOrInvalidEmail = function (list,type,callback) {

    var pushQuery = {};
    if(type == 'invalid') {
        pushQuery = {invalid: {$each: list}};
    }
    else if(type == 'exception'){
        pushQuery = {exception: {$each: list}};
    }
    if(pushQuery) {
        inavlidEmailListCollection.update({type: "invalidEmail"}, {$addToSet: pushQuery}, {upsert: true}, function (err, res) {
            if (!err) {
                callback(res)
            } else {
                callback(false)
            }
        });
    }
    else{
        callback(false)
    }
};

CommonUtility.prototype.deleteValidOrInvalidEmail = function (list,type,callback) {
    var query = {};
    if(type == 'invalid'){
        query = {$pullAll: {invalid:list}}
    }
    else if(type == 'exception'){
        query = {$pullAll: {exception:list}}
    }
    if(query) {
        inavlidEmailListCollection.update({type: "invalidEmail"}, query, {upsert: true}, function (err, res) {
            if (!err) {
                callback(res)
            } else {
                callback(false)
            }
        });
    }
    else{
        callback(false)
    }
};

CommonUtility.prototype.loginUserAndRedirectToApp = function (userId,redirectUrl,profile,message,res,req) {

    var self = this;

    var tokenSecret ="alpha";
    var token = jwt.encode({ id: userId.toString()}, tokenSecret);

    req.logIn(token, function(err){
        self.insertLog(profile,message,req);
        res.redirect(redirectUrl);
    });
}

CommonUtility.prototype.createMeetingInteractionsObjects = function (meeting,isSuggested,source,timezone){

    // var date = checkRequired(meeting.scheduleTimeSlots[0].start.date) ? moment(meeting.scheduleTimeSlots[0].start.date).tz(timezone).format() : new Date();

    var allMeetingInteractions = [];
    var date = checkRequired(meeting.scheduleTimeSlots[0].start.date) ? meeting.scheduleTimeSlots[0].start.date : new Date();
    var trackInfo = {action:"request"};
    if(isSuggested){
        trackInfo = {action:"re scheduled"};
        date = meeting.suggestedBy.suggestedDate ? new Date(meeting.suggestedBy.suggestedDate) : new Date()
    }
    // var senderI = getInteractionObject(userId,user_twitter_id,user_twitter_name,firstName,lastName,publicProfileUrl,profilePicUrl,companyName,designation,location,mobileNumber,action,interactionType,subType,emailId,refId,source,title,description,interactionDate,endDate,trackId,trackInfo,emailContentId,googleAccountEmailId);
    var senderI = getInteractionObject(meeting.senderId,null,null,meeting.senderName,null,meeting.publicProfileUrl,meeting.senderPicUrl,null,meeting.designation,meeting.location,meeting.mobileNumber,"sender","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.senderEmailId,meeting.invitationId,source,meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null);
    allMeetingInteractions.push(senderI);
    var emailIdList = [meeting.senderEmailId];
    if(meeting.selfCalendar && meeting.toList && meeting.toList.length>0){
        for(var i=0; i<meeting.toList.length; i++){
            emailIdList.push(meeting.toList[i].receiverEmailId);
            allMeetingInteractions.push(getInteractionObject(meeting.toList[i].receiverId,null,null,meeting.toList[i].receiverFirstName,meeting.toList[i].receiverLastName,meeting.toList[i].publicProfileUrl,meeting.toList[i].picUrl,null,meeting.toList[i].designation,meeting.toList[i].location,meeting.toList[i].mobileNumber,"receiver","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.toList[i].receiverEmailId,meeting.invitationId,source,meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null));
        }
    }
    else if(meeting.to && checkRequired(meeting.to.receiverEmailId)){
        emailIdList.push(meeting.to.receiverEmailId);
        allMeetingInteractions.push(getInteractionObject(meeting.to.receiverId,null,null,meeting.to.receiverName,null,null,null,null,null,null,null,"receiver","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.to.receiverEmailId,meeting.invitationId,'google',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null));
    }

    return allMeetingInteractions;
}

CommonUtility.prototype.createRecurringMeetingInteractionsObjects = function (meeting,isSuggested,source){
    var allMeetingInteractions = [];
    var date = null;

    var trackInfo = {action:"request"};

    _.each(meeting.scheduleTimeSlots,function (slot) {
        date = new Date(slot.start.date)
    });

    // var senderI = getInteractionObject(userId,user_twitter_id,user_twitter_name,firstName,lastName,publicProfileUrl,profilePicUrl,companyName,designation,location,mobileNumber,action,interactionType,subType,emailId,refId,source,title,description,interactionDate,endDate,trackId,trackInfo,emailContentId,googleAccountEmailId);
    var senderI = getInteractionObject(meeting.senderId,null,null,meeting.senderName,null,meeting.publicProfileUrl,meeting.senderPicUrl,null,meeting.designation,meeting.location,meeting.mobileNumber,"sender","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.senderEmailId,meeting.invitationId,source,meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null);
    allMeetingInteractions.push(senderI);
    var emailIdList = [meeting.senderEmailId];
    if(meeting.selfCalendar && meeting.toList && meeting.toList.length>0){
        for(var i=0; i<meeting.toList.length; i++){
            emailIdList.push(meeting.toList[i].receiverEmailId);
            allMeetingInteractions.push(getInteractionObject(meeting.toList[i].receiverId,null,null,meeting.toList[i].receiverFirstName,meeting.toList[i].receiverLastName,meeting.toList[i].publicProfileUrl,meeting.toList[i].picUrl,null,meeting.toList[i].designation,meeting.toList[i].location,meeting.toList[i].mobileNumber,"receiver","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.toList[i].receiverEmailId,meeting.invitationId,source,meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null));
        }
    }
    else if(meeting.to && checkRequired(meeting.to.receiverEmailId)){
        emailIdList.push(meeting.to.receiverEmailId);
        allMeetingInteractions.push(getInteractionObject(meeting.to.receiverId,null,null,meeting.to.receiverName,null,null,null,null,null,null,null,"receiver","meeting",meeting.scheduleTimeSlots[0].locationType,meeting.to.receiverEmailId,meeting.invitationId,'google',meeting.scheduleTimeSlots[0].title,meeting.scheduleTimeSlots[0].description,date,null,null,trackInfo,null));
    }

    return allMeetingInteractions;
}

CommonUtility.prototype.validateOutlookAccount = function (outlookObj) {
    if(outlookObj && outlookObj.length>0){
        return true;
    } else {
        return false;
    }
}

CommonUtility.prototype.rmDuplicatesByFields = function(arr,field1,field2){
    return rmDuplicateByFields(arr,field1,field2)
}

CommonUtility.prototype.getRecurringMeetingObjects = function(meeting,day,date) {

    _.each(meeting.scheduleTimeSlots,function (slot) {

        var mS = moment2(date).add(day, 'days');
        var mE = moment2(date).add(day, 'days');

        var mTs = moment2(slot.start.date)
        var mTe = moment2(slot.end.date);

        var sHour = mTs.get('hour');
        var sMin = mTs.get('minute');
        var sSec = mTs.get('second');
        var sMs = mTs.get('millisecond');
        var eHour = mTe.get('hour');
        var eMin = mTe.get('minute');
        var eSec = mTe.get('second');
        var eMs = mTe.get('millisecond');

        var start = mS.set({
            'hour':sHour,
            'minute':sMin,
            'second':sSec,
            'millisecond':sMs
        });

        var end = mE.set({
            'hour':eHour,
            'minute':eMin,
            'second':eSec,
            'millisecond':eMs
        });

        slot.start.date = new Date(start)
        slot.end.date = new Date(end)
    });

    meeting.recurrenceId = meeting.invitationId+'recurringmeeting'+day

    return meeting;

}

function rmDuplicateByFields(arr,field1,field2){

    var end = arr.length;

    for(var i = 0; i < end; i++) {
        for(var j = i + 1; j < end; j++) {
            if (arr[i] && arr[j] && (arr[i][field1] == arr[j][field1]) && (arr[i][field2] == arr[j][field2])) {


                var shiftLeft = j;
                for(var k = j+1; k < end; k++, shiftLeft++)
                {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

CommonUtility.prototype.mergeSimilarContact = function(arr){
    return mergeContactFields(arr)
};

CommonUtility.prototype.getLocationGoogleAPI = function(location,callback){
    var loc = location.replace(/[^\w\s]/gi, '');

    request('https://maps.googleapis.com/maps/api/geocode/json?address='+loc,function(e,r,b){

        if(!e) {
            try{
                var gLoc = JSON.parse(b);
            }
            catch(e){
                logger.info("Exception Google location Search searchByAddress() ",e, 'search content ',loc);
            }
            callback(gLoc)

        } else {
            callback(false)
        }
    });
}

CommonUtility.prototype.clone = clone;

CommonUtility.prototype.isMeetingLocationInPerson = function (location) {

    var skypeCheck = "skype:",
        callCheck = "call:",
        phoneCheck = "phone";

    location = location.toLowerCase();
    location = location.replace(/[&\/\\#,+()$~%.'"*?<>{}]/g, '');

    if(!isNumber(location)){

        if(location.indexOf(skypeCheck) !== -1 || location.indexOf(callCheck) !== -1 || location.indexOf(phoneCheck) !== -1){
            return false;
        } else {
            return true;
        }

    } else {
        return false;
    }
}

CommonUtility.prototype.setMeetingLocationType = function (location) {

    var skypeCheck = "skype",
        callCheck = "call",
        phoneCheck = "phone";

    location = location.toLowerCase();
    location = location.replace(/[&\/\\#,+()$~%.'"*?<>{}]/g, '');

    if(!isNumber(location)){

        if(location.indexOf(skypeCheck) !== -1){
            return 'Skype';
        } else if(location.indexOf(callCheck) !== -1 || location.indexOf(phoneCheck) !== -1){
            return 'Phone';
        } else {
            return false;
        }

    } else {
        return 'Phone';
    }
}

CommonUtility.prototype.createMessageObject = function (rawData) {

    var obj = {
        conversationId: new customObjectId(),
        messageOwner: {
            fullName: rawData.userName,
            emailId: rawData.emailId,
            userId:rawData.userId
        },
        text: rawData.text,
        emailId: rawData.emailId,
        date: new Date()
    }

    return obj;

};

CommonUtility.prototype.sendErrorResponse = function (res,data,message) {
    res.send({
        "SuccessCode": 0,
        "Message": message?message:"",
        "ErrorCode": 1,
        "Data":data?data:''
    });
}

CommonUtility.prototype.getContactObj = function (email) {
    var contactObj2 = {
        _id: new customObjectId(),
        personId: null,
        personName: email,
        personEmailId: email,
        birthday: null,
        companyName: null,
        designation: null,
        addedDate: new Date(),
        verified: false,
        relatasUser: false,
        relatasContact: true,
        mobileNumber: null,
        location: null,
        source: 'web2lead',
        account: {
            name: fetchCompanyFromEmail(email),
            selected: false,
            updatedOn: new Date()
        }
    }
    return contactObj2
}

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

function mergeContactFields(arr){
    var end = arr.length;
    for(var i=0;i<end;i++) {
        for (var j = i + 1; j < end; j++) {
            if (arr[i] && arr[j]) {
                var removeNext = false;
                if (arr[i].personEmailId == arr[j].personEmailId) {
                    if ((arr[i].mobileNumber == null) && (arr[j].mobileNumber != null)) {
                        arr[i].mobileNumber = arr[j].mobileNumber
                        //remove arr[j]
                        removeNext = true
                    } else if ((arr[i].mobileNumber != null) && (arr[j].mobileNumber == null)) {
                        //remove arr[j]
                        removeNext = true
                    }
                } else if (arr[i].mobileNumber == arr[j].mobileNumber) {
                    if ((arr[i].personEmailId == null) && (arr[j].personEmailId != null)) {
                        arr[i].personEmailId = arr[j].personEmailId
                        //remove arr[j]
                        removeNext = true
                    } else if ((arr[i].personEmailId != null) && (arr[j].personEmailId == null)) {
                        //remove arr[j]
                        removeNext = true

                    }
                }

                if (removeNext) {
                    var shiftLeft = j;//1
                    for (var k = j + 1; k < end; k++, shiftLeft++) {
                        arr[shiftLeft] = arr[k];//move i+2 to i+1 removing duplicate.
                    }
                    end--;
                    j--;
                }
            }
        }
    }
    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    return whitelist;

}

function getAppConfigQuery(entityId){
    var query = appConfiguration.findOne({entityId:entityId});
    return query;
};

CommonUtility.prototype.numberWithCommas = function(x,ins) {
    if(ins){
        return numberWithCommas_ins(x);
    } else {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

CommonUtility.prototype.nl2br = function (str, isXhtml) {

    if (typeof str === 'undefined' || str === null) {
        return ''
    }

    var breakTag = (isXhtml || typeof isXhtml === 'undefined') ? '<br ' + '/>' : '<br>'

    return (str + '')
        .replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2')
}

CommonUtility.prototype.validateCorporateEmailId = validateCorporateEmailId

function validateCorporateEmailId(emailId, companyObject) {
    var emailDomains = companyObject.emailDomains;
    var eArr = emailId.split('@');
    var isValid = false;

    for (var domain = 0; domain < emailDomains.length; domain++) {

        if (eArr[1] == emailDomains[domain]) {
            isValid = true;
        }
    }
    return isValid;
}

CommonUtility.prototype.getCompanyObjAuth = getCompanyObjAuth;

CommonUtility.prototype.validObject = function(object){

    if(Object.keys(object).length === 0){
        return false;
    } else {
        return true;
    }
}

function getCompanyObjAuth(req,user,callback) {

    var domain = this.getSubDomain(req); //LM by Naveen 07 Dec 2016
    var domain2 = fetchCompanyFromEmail(user.emailId);

    if(!domain2){
        domain2 = domain
    }

    domain2 = domain2?domain2.toLowerCase():domain2;

    if (domain2) {
        company.findCompanyByUrl(getCompanyObj2(domain2, req), {
            url: 1,
            emailDomains: 1,
            companyName: 1
        }, function (companyProfile) {

            if(companyProfile){
                req.session.companyId = companyProfile._id;
                req.session.companyName = companyProfile.companyName;
                callback(companyProfile)
            } else {
                callback(null)
            }
        });
    } else callback(null)
}

function getCompanyObj2(companyName, req) {

    var domain = appCredential.getDomain();
    var url = companyName+'.'+domain.mainDomain;

    var company = {
        // url: req.headers.host,
        url: url?url.toLowerCase():url,
        companyName: companyName
    };

    return company;
}

CommonUtility.prototype.companyFiscalYearStartMonth = function (companyId,callback) {
    company.findCompanyByIdWithProjection({_id:castToObjectId(companyId)},{fyMonth:1},function (result) {
        callback(result.fyMonth)
    });
}

CommonUtility.prototype.monthNames = function (){
    return ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
}

CommonUtility.prototype.getUserListForQuery = function (req){

    var userId = this.getUserIdFromMobileOrWeb(req);
    var hierarchyList = []

    if (req.query.userIdList)
        hierarchyList = req.query.userIdList.split(",")
    else {
        hierarchyList = [userId]
    }

    hierarchyList = this.castListToObjectIds(hierarchyList);

    return hierarchyList;
}

CommonUtility.prototype.getContactsListForQuery = function (req){

    var contactsList = [];

    if (req.query.contacts){
        contactsList = req.query.contacts.split(",")
    }

    var contactsLowerCase = [];

    _.each(contactsList,function (el) {
        contactsLowerCase.push(el.toLowerCase())
    });

    return contactsLowerCase;
}

CommonUtility.prototype.getQuarterYear = function (allQuarters,startOfQuarter,fyMonth,timezone,fyStartDate) {
    var qtrString = getQtrString(allQuarters,startOfQuarter,timezone,fyStartDate);
    return qtrString+""+moment(startOfQuarter).year();
}

CommonUtility.prototype.sendNotification = function(payload, webToken, mobileToken, companyId, userId, userEmailId, callback) {
    webToken = checkRequired(webToken) ? webToken : "123"; // "123 is random id on not availibility of actual firebase token, which makes firebase to return undefined messageId"
    mobileToken = checkRequired(mobileToken) ? mobileToken : "123"; // "123 is random id on not availibility of actual firebase token, which makes firebase to return undefined messageId"

    async.parallel([function(callback) {
        var webPayload = JSON.parse(JSON.stringify(payload));
        webPayload.notification.icon = webPayload.data.icon ? webPayload.data.icon : '/images/relatasIcon.png';
        webPayload.notification.click_action = webPayload.data.click_action || '';

        admin.messaging().sendToDevice(webToken, webPayload, {})
            .then((response) => {
                var webMsgId = response.results[0].messageId;
                callback(null, webMsgId)
            })
            .catch((error) => {
                console.log("Error while sending notification:", error);
                callback(null, '');
            });

    }, function(callback) {
        admin.messaging().sendToDevice(mobileToken, payload, {})
            .then((response) => {
                var mobileMsgId = response.results[0].messageId;
                callback(null, mobileMsgId)
            })
            .catch((error) => {
                console.log("Error while sending notification:", error);
                callback(null, '');
            });

    }], function(error, msgIds) {
        storeNotificationData([{
            messageIds: [msgIds[0], msgIds[1]],
            payloadData: JSON.stringify(payload),
            title: payload.notification.title,
            body: payload.notification.body,
            url: payload.data.click_action,
            notificationType: "realtime",
            category: payload.data.category
        }], companyId, userId, userEmailId, function() {

            callback();
        })
    })
}

function storeNotificationData(notificationsData, companyId, userId, emailId, callback) {
    var notificationObjs = [];
    var dayString = moment().format("DDMMYYYY");

    _.each(notificationsData, function(notification) {
        if(notification && notification.messageIds) {

            if( checkRequired(notification.messageIds[0]) ) {
                notificationObjs.push({
                    messageId: notification.messageIds[0],
                    emailId: emailId,
                    userId: userId,
                    companyId: companyId,
                    data: notification.payloadData,
                    sentDate: new Date(),
                    category: notification.category,
                    notificationTitle: notification.title,
                    notificationBody: notification.body,
                    url: notification.url,
                    notificationType: notification.notificationType,
                    dayString: dayString,
                    dataReferenceType: "notification",
                    dataFor: "web",
                    accessed: false
                });
            }

            if( checkRequired(notification.messageIds[1]) ) {
                notificationObjs.push({
                    messageId: notification.messageIds[1],
                    emailId: emailId,
                    userId: userId,
                    companyId: companyId,
                    data: notification.payloadData,
                    sentDate: new Date(),
                    category: notification.category,
                    dataFor: "mobile",
                    notificationTitle: notification.title,
                    notificationBody: notification.body,
                    url: notification.url,
                    notificationType: notification.notificationType,
                    dayString: dayString,
                    dataReferenceType: "notification",
                    accessed: false
                });
            }
        }
    })

    messageAccess.insertNotifications(notificationObjs, function() {
        callback();
    })
}

function getQtrString(allQuarters,startOfQuarter,timezone,fyStartDt){

    var prevQtrs = buildQuarters(moment(fyStartDt).subtract(1,"year"),timezone);
    var nextQtrs = buildQuarters(moment(fyStartDt).add(1,"year"),timezone);

    var qtrString = null;
    for( var key in allQuarters){
        if(new Date(startOfQuarter)>=new Date(allQuarters[key].start) && new Date(startOfQuarter)<=new Date(allQuarters[key].end)){
            qtrString = key
        }
    }

    //If not found in the current quarter, find in the next qtr
    if(!qtrString){
        for( var key in nextQtrs){
            if(new Date(startOfQuarter)>=new Date(nextQtrs[key].start) && new Date(startOfQuarter)<=new Date(nextQtrs[key].end)){
                qtrString = key
            }
        }
    }

    //If not found in the current and next quarter, find in the prev qtr
    if(!qtrString){
        for( var key in prevQtrs){
            if(new Date(startOfQuarter)>=new Date(prevQtrs[key].start) && new Date(startOfQuarter)<=new Date(prevQtrs[key].end)){
                qtrString = key
            }
        }
    }

    return qtrString;
}

function buildQuarters(fyStartDate,timezone) {

    var quarters = [],
        twelve_months_for_loop = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November"],
        months = [],
        qtrYears = [],
        qtrObj = {};

    months.push(fyStartDate);

    _.each(twelve_months_for_loop,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    quarters.push({start:moment(months[0]).tz(timezone).startOf('month').format(),end:moment(months[2]).tz(timezone).endOf('month').format(),qtr:"quarter1"})
    quarters.push({start:moment(months[3]).tz(timezone).startOf('month').format(),end:moment(months[5]).tz(timezone).endOf('month').format(),qtr:"quarter2"})
    quarters.push({start:moment(months[6]).tz(timezone).startOf('month').format(),end:moment(months[8]).tz(timezone).endOf('month').format(),qtr:"quarter3"})
    quarters.push({start:moment(months[9]).tz(timezone).startOf('month').format(),end:moment(months[11]).tz(timezone).endOf('month').format(),qtr:"quarter4"})

    _.each(quarters,function (el) {
        qtrObj[el.qtr] = el
    });

    return qtrObj;
}

CommonUtility.prototype.userFiscalYear = function (userId,callback) {

    var redisKey = String(userId)+"fiscalYear";

    checkCacheFiscalYear(redisKey,function (data) {
        if(data){
            callback(null,data.fyMonth,data.startEnd,data.quarters,data.timezone,data.user,data.company)
        } else {
            userManagements.getUserCompanyDetails(castToObjectId(userId),function (err,company,user) {
                if(!err && company && user){

                    var monthNames = ["January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"];

                    if (err || !company || !company.fyMonth) {
                        var fyMonth = "April"; //Default
                    } else {
                        fyMonth = company.fyMonth
                    }

                    var currentYr = new Date().getFullYear()
                    var currentMonth = new Date().getMonth()

                    var toDate = null;
                    var fromDate = new Date(moment().startOf('month'));
                    fromDate.setMonth(monthNames.indexOf(fyMonth))

                    if(currentMonth<monthNames.indexOf(fyMonth)){
                        fromDate.setFullYear(currentYr-1)
                    }

                    toDate = moment(fromDate).add(11, 'month');
                    toDate = moment(toDate).endOf('month');

                    var timezone = user.timezone && user.timezone.name?user.timezone.name:"UTC";

                    var obj = {
                        fyMonth:fyMonth,
                        startEnd:{start:moment(new Date(fromDate)).tz(timezone).format(),end: moment(new Date(toDate)).tz(timezone).format()},
                        quarters:setQuarter(fyMonth,timezone,fromDate,toDate),
                        timezone:timezone,
                        user:user,
                        company:company
                    }

                    cacheFiscalYear(redisKey,obj);

                    callback(err,
                        company.fyMonth,
                        {start:moment(fromDate).tz(timezone).format(),end: moment(toDate).tz(timezone).format()},
                        setQuarter(fyMonth,timezone,fromDate,toDate),
                        timezone,
                        user,company)
                } else {
                    callback(err,null,null)
                }
            })
        }
    })
}



function cacheFiscalYear(redisKey,data){
    redisClient.setex(redisKey, 3600, JSON.stringify(data));
}

function checkCacheFiscalYear(redisKey,callback) {
    redisClient.get(redisKey, function (error, cacheResult) {
        try {
            var data = JSON.parse(cacheResult);
            callback(data)
            // callback(null)
        } catch (err) {
            callback(null)
        }
    });
}

CommonUtility.prototype.getQuarterRange = getQuarterRange

function getQuarterRange(quarter) {

    var start = moment().quarter(quarter).startOf('quarter');
    var end = moment().quarter(quarter).endOf('quarter');

    return {start:start, end:end};
};

function setQuarter(startMonth,timezone,fyStartDate,fyEndDate) {

    var qtrObj = {};

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    var twelve_months_for_loop = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November"];

    var months = [];
    months.push(fyStartDate)

    _.each(twelve_months_for_loop,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    qtrObj.quarter1 = {start:moment(moment(months[0]).startOf('month')).tz(timezone).format(),end:moment(moment(months[2]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter2 = {start:moment(moment(months[3]).startOf('month')).tz(timezone).format(),end:moment(moment(months[5]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter3 = {start:moment(moment(months[6]).startOf('month')).tz(timezone).format(),end:moment(moment(months[8]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter4 = {start:moment(moment(months[9]).startOf('month')).tz(timezone).format(),end:moment(moment(months[11]).endOf('month')).tz(timezone).format()}

    var currentMonth = moment().month()
    var currentQuarter = "quarter4"
    if(startMonth =='January'){

        if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    if(startMonth =='April'){

        if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    return qtrObj
}

CommonUtility.prototype.sizeOfObject = sizeOfObject;

CommonUtility.prototype.bytesToSize = bytesToSize;

function sizeOfObject( object ) {

    var objectList = [];
    var stack = [ object ];
    var bytes = 0;

    while ( stack.length ) {
        var value = stack.pop();

        if ( typeof value === 'boolean' ) {
            bytes += 4;
        }
        else if ( typeof value === 'string' ) {
            bytes += value.length * 2;
        }
        else if ( typeof value === 'number' ) {
            bytes += 8;
        }
        else if
        (
            typeof value === 'object'
            && objectList.indexOf( value ) === -1
        )
        {
            objectList.push( value );

            for( var i in value ) {
                stack.push( value[ i ] );
            }
        }
    }
    return bytes;
}

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};

CommonUtility.prototype.getDateForDay = function (timezone) {

    var start = moment().tz(timezone).startOf('isoweek')
    var end = moment().tz(timezone).endOf('isoweek');
    var now = start
        ,dayDateObj ={}
        ,days = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];

    while (now <= end) {
        dayDateObj[days[now.isoWeekday()-1]] = new Date(now)
        now.add(1,'days');
    }

    return dayDateObj;
}

CommonUtility.prototype.enumerateDaysBetweenDates = enumerateDaysBetweenDates;

function enumerateDaysBetweenDates(startDate, endDate) {

    var start = moment(startDate)
    var end = moment(endDate)

    var now = start, dates = [];

    while (now <= end) {
        dates.push(now.format("MMM Do YY"));
        now.add('days', 1);
    }
    return dates;
};

CommonUtility.prototype.getMonthsBetweenDates = getMonthsBetweenDates

function getMonthsBetweenDates(from,to) {
    var startDate = moment(from).add(2, 'days');
    var endDate = moment(to).add(2, 'days');

    var result = [];

    if (endDate.isBefore(startDate)) {
        throw "End date must be greated than start date."
    }

    var currentDate = startDate.clone();

    while (currentDate.isBefore(endDate)) {
        result.push(currentDate.format("MMM YYYY"));
        currentDate.add(1, 'month');
    }

    return result;
}

function getMonthDateBetweenDates(from,to) {
    var startDate = moment(from).add(2, 'days');
    var endDate = moment(to).add(2, 'days');

    var result = [];

    if (endDate.isBefore(startDate)) {
        throw "End date must be greated than start date."
    }

    var currentDate = startDate.clone();

    while (currentDate.isBefore(endDate)) {
        result.push(currentDate.format());
        result.push(currentDate.format());
        currentDate.add(1, 'month');
    }

    return result;
}

CommonUtility.prototype.roundOff = roundOff

CommonUtility.prototype.getUserAccessControl = getUserAccessControl

function getUserAccessControl(userIds,callback) {

    userManagements.getUserOppOwnerDetails(userIds,function (errO,oppOwnershipDetails) {
        company.findCompanyById(oppOwnershipDetails[0].companyId,function (companyDetails) {

            var regionAccess = companyDetails.geoLocations.length>0?oppOwnershipDetails[0].regionOwner:null;
            var productAccess = companyDetails.productList.length>0?oppOwnershipDetails[0].productTypeOwner:null;
            var verticalAccess = companyDetails.verticalList.length>0?oppOwnershipDetails[0].verticalOwner:null;
            var businessUnits = companyDetails.businessUnits.length>0?oppOwnershipDetails[0].businessUnits:null;
            var netGrossMarginReq = companyDetails.netGrossMargin?companyDetails.netGrossMargin:false;
            var companyTargetAccess = oppOwnershipDetails[0].companyTargetAccess?oppOwnershipDetails[0].companyTargetAccess:false;

            callback(regionAccess,productAccess,verticalAccess,oppOwnershipDetails[0].companyId,netGrossMarginReq,companyTargetAccess,companyDetails,businessUnits)
        });
    });
}

CommonUtility.prototype.checkNGMRequirement = function (companyId,callback) {

    company.findCompanyById(companyId,function (companyDetails) {
        var netGrossMarginReq = companyDetails && companyDetails.netGrossMargin?companyDetails.netGrossMargin:false;
        callback(netGrossMarginReq)
    });
}

function roundOff(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

CommonUtility.prototype.getUserHierarchy = getUserHierarchy

CommonUtility.prototype.getAllCompanyUsers = getAllCompanyUsers

CommonUtility.prototype.calculateNGM = function(opps,ping) {

    var opportunitiesList = [];

    _.each(opps,function (op) {

        op = JSON.parse(JSON.stringify(op))

        if(!op.amountNonNGM){
            op.amountNonNGM = op.amount;

            if(op.netGrossMargin || op.netGrossMargin == 0){
                var amountWithNGM = op.netGrossMargin*op.amount

                if(amountWithNGM){
                    amountWithNGM = amountWithNGM/100
                }

                op.amount = amountWithNGM;
            }
        }

        opportunitiesList.push(op)
    })

    opps = opportunitiesList;
    return opps;
}

function getUserHierarchy(userId,callback){
    userManagements.findUserProfileByIdWithCustomFields(userId, { companyId: 1, companyName: 1, hierarchyPath: 1, emailId:1,firstName:1,lastName:1 }, function(error, data) {
        userManagements.getUserHierarchyWithoutLiuWithNames(userId, function(err, teamData) {
            teamData.push({_id:castToObjectId(userId),emailId:data.emailId,firstName:data.firstName,lastName:data.lastName});
            callback(err,teamData)
        })
    });
}

function getAllCompanyUsers(userId,callback){

    userManagements.findUserProfileByIdWithCustomFields(userId, { companyId: 1, companyName: 1, hierarchyPath: 1, emailId:1,firstName:1,lastName:1 }, function(error, data) {
        userManagements.getTeamMembers(data.companyId, function(err, teamData) {
            callback(err,teamData)
        })
    });
}

function setTimezone(moment, timezone) {

    var a = moment.toArray(); // year,month,date,hours,minutes,seconds as an array

    moment = moment.tz(timezone);

    moment.year(a[0])
        .month(a[1])
        .date(a[2])
        .hours(a[3])
        .minutes(a[4])
        .seconds(a[5])
        .milliseconds(a[6]);

    return moment; // for chaining
};

CommonUtility.prototype.getEmailIdDomain = function(emailId){
    return emailId.replace(/.*@/, "");
}

module.exports = CommonUtility;
