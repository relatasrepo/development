var winstonModule = require('winston');

function Winston(){

}
/*
 * winston is a logging library using this we logging current
 * action in relatas - application
 */
var logger = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Relatas_login.log', maxsize:512000 })
    ]
});

var loggerMobileError = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Relatas_mobile_error.log', maxsize:512000 })
    ]
});

var loggerError = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Relatas_error.log', maxsize:512000 })
    ]
});

var linkedinErrorLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Relatas_linkedin_error.log', maxsize:512000 })
    ]
});

var tasksErrorLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Tasks_class_error.log', maxsize:512000 })
    ]
});

var interactionsErrorLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Interactions_class_error.log', maxsize:512000 })
    ]
});

var meetingsErrorLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Meetings_class_error.log', maxsize:512000 })
    ]
});

var messagesErrorLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Messages_class_error.log', maxsize:512000 })
    ]
});

var AWSErrorLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/AWS_class_error.log', maxsize:512000 })
    ]
});

var GmailErrorLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Gmail_class_error.log', maxsize:512000 })
    ]
});

var FilesErrorLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Files_operations_error.log', maxsize:512000 })
    ]
});

var corporateLogger = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Relatas_corporate.log', maxsize:512000 })
    ]
});

var corporateErrorLogger = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Relatas_corporate.log', maxsize:512000 })
    ]
});

var adminChangeLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/admin_changes.log', maxsize:512000})
    ]
});

var twitterLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/twitter_error.log', maxsize:512000})
    ]
});

var actionItemsErrorLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/action_items.log', maxsize:512000})
    ]
});

var dailyAgendaMailErrorLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/Daily_agenda_mail_error.log', maxsize:512000 })
    ]
});

var damNoAccessToken = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/DAM_no_access_token_error.log', maxsize:512000 })
    ]
});

var damSuccessUpdate = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/DAM_success_access_token.log', maxsize:512000 })
    ]
});

var damUserFound = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/User_Found_DAM.log', maxsize:512000 })
    ]
});

var redisLog = new (winstonModule.Logger)({
    transports: [
        new (winstonModule.transports.Console)(),
        new (winstonModule.transports.File)({ filename: 'logs/redis_keys.log', maxsize:512000})
    ]
});

Winston.prototype.getWinston = function(){
    return logger;
}

Winston.prototype.getRedisLog = function(){
    return redisLog;
}

Winston.prototype.getWinstonError = function(){
    return loggerError;
}

Winston.prototype.getWinstonLinkedInError = function(){
    return linkedinErrorLog;
}

Winston.prototype.getCorporateLogger = function(){
    return corporateLogger;
}

Winston.prototype.getCorporateErrorLogger = function(){
    return corporateErrorLogger;
}

Winston.prototype.getAdminChangesLogger = function(){
    return adminChangeLog;
}

Winston.prototype.getMobileErrorLogger = function(){
    return loggerMobileError;
}

Winston.prototype.getTasksErrorLogger = function(){
    return tasksErrorLog;
}

Winston.prototype.getInteractionsErrorLogger = function(){
    return interactionsErrorLog;
}

Winston.prototype.getMeetingsErrorLogger = function(){
    return meetingsErrorLog;
}

Winston.prototype.getMessageErrorLogger = function(){
    return messagesErrorLog;
}

Winston.prototype.getAWSErrorLogger = function(){
    return AWSErrorLog;
}

Winston.prototype.getFilesErrorLogger = function(){
    return FilesErrorLog;
}

Winston.prototype.getGmailErrorLogger = function(){
    return GmailErrorLog;
}

Winston.prototype.getTwitterErrorLogger = function(){
    return tasksErrorLog;
}

Winston.prototype.getActionErrorLogger = function(){
    return actionItemsErrorLog;
}

Winston.prototype.getDynamicLogger = function(fileName){
    var dynamicLogger = new (winstonModule.Logger)({
        transports: [
            new (winstonModule.transports.Console)(),
            new (winstonModule.transports.File)({ filename: 'logs/'+fileName })
        ]
    });
    return dynamicLogger;
};

Winston.prototype.getDailyAgendaMailErrorLogger = function(){
    return dailyAgendaMailErrorLog;
}

Winston.prototype.getDAMAcessTokenErrorLogger = function(){
    return damNoAccessToken;
}

Winston.prototype.getDAMSuccessLogger = function(){
    return damSuccessUpdate;
}

Winston.prototype.getDAMUserFoundLogger = function(){
    return damUserFound;
}

module.exports = Winston;