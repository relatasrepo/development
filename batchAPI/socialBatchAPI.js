
var request = require('request');
var userManagement = require('../dataAccess/userManagementDataAccess');
var winstonLog = require('../common/winstonLog');
var twitterM = require('../common/twitter');
var linkedinM = require('../common/linkedin');
var User = require('../databaseSchema/userManagementSchema').User;
var emailSender = require('../public/javascripts/emailSender');

var logLib = new winstonLog();
var logger = logLib.getWinston();
var twitterObj = new twitterM();
var linkedinObj = new linkedinM();
var emailSenders = new emailSender();
var userManagementObj = new userManagement();

var userIdListTwitter = [];

var userIdListLinkedin = [];

function BatchAPI(){
    var isTwitterRunning = false;
    var isLinkedinRunning = false;
    this.runTwitterBatch = function(){

            userManagementObj.getUserIdListAddedTwitter(function(userIdList){
                console.log("userIdList\n"+userIdList.toString());
                if(userIdList && userIdList.length > 0){
                    userIdListTwitter = userIdList;
                    isTwitterRunning = true;
                    runBatchWithUserIdList(userIdListTwitter.shift(),function(){
                        isTwitterRunning = false;
                        userIdListTwitter = [];
                        logger.info('Twitter batch completed runTwitterBatch():BatchAPI');
                        // callback()
                    });
                }
                else {
                    logger.info('NO users with twitter added runTwitterBatch():BatchAPI');
                }
            })
    };

    this.runLinkedinBatch = function(){
            userManagementObj.getUserIdListAddedLinkedin(function(userIdList){
                if(userIdList && userIdList.length > 0){
                    userIdListLinkedin = userIdList;
                    isLinkedinRunning = true;
                    runBatchWithUserIdListLinkedin(userIdListLinkedin.shift(),function(){
                        isLinkedinRunning = false;
                        userIdListLinkedin = [];
                        logger.info('Linkedin batch completed runLinkedinBatch():BatchAPI');
                        // callback()
                    });
                }
                else {
                    logger.info('NO users with linkedin added runLinkedinBatch():BatchAPI');
                }
            })
    };

    this.runLocationMigration = function(){
        logger.info('runLocationMigration started **********------------*************')
        var stream = User.find({location:{$exists:true,$nin:[null,'',' ']}},{_id:1,skypeId:1,mobileNumber:1,emailId:1,location:1,firstName:1,lastName:1,companyName:1,designation:1}).batchSize(100).stream();
        var bulk = User.collection.initializeUnorderedBulkOp();
        stream.on('data', function (doc) {
            processUserLocation(doc,function(location,latLangObj){

                if(location != null){
                    var updateObj = {};
                    updateObj["contacts.$.location"]=location;
                    if(checkRequired(doc.firstName) && checkRequired(doc.lastName)){
                        updateObj["contacts.$.personName"]=doc.firstName+' '+doc.lastName;
                    }
                    if(checkRequired(doc._id)){
                        updateObj["contacts.$.personId"]=doc._id;
                    }
                    if(checkRequired(doc.emailId)){
                        updateObj["contacts.$.personEmailId"]=doc.emailId;
                    }
                    if(checkRequired(doc.companyName)){
                        updateObj["contacts.$.companyName"]=doc.companyName;
                    }
                    if(checkRequired(doc.designation)){
                        updateObj["contacts.$.designation"]=doc.designation;
                    }
                    if(checkRequired(doc.skypeId)){
                        updateObj["contacts.$.skypeId"]=doc.skypeId;
                    }
                    if(checkRequired(doc.mobileNumber)){
                        updateObj["contacts.$.mobileNumber"]=doc.mobileNumber;
                    }
                    updateObj["contacts.$.relatasUser"]=true;

                    var profileUpdateObj = {location:location};
                    if(checkRequired(latLangObj) && checkRequired(latLangObj.lat) && checkRequired(latLangObj.lng)){
                        profileUpdateObj = {location:location,locationLatLang:{latitude:latLangObj.lat,longitude:latLangObj.lng}};
                    }

                    bulk.find({_id:doc._id}).update({$set:profileUpdateObj});
                    bulk.find({"contacts.personEmailId":doc.emailId}).update({$set:updateObj});
                    logger.info('User ',doc.emailId, 'runLocationMigration completed-------------------');
                }
                else{
                    logger.info('User ',doc.emailId, 'runLocationMigration completed no location-------------------');
                }

                stream.resume();
            });
            stream.pause();
        });
        stream.on('error', function (err) {
            logger.info('Error runLocationMigration ',err);
            // handle err
            emailSenders.sendUncaughtException(err,JSON.stringify(err),err.stack," uncaughtException runLocationMigration()");
        });

        stream.on('close', function () {
            // all done
            bulk.execute(function(err, result) {
                if(err){
                    logger.info('Error in runLocationMigrationBulk():socialBatchAPI ',err )
                }
                else{
                    result = result.toJSON();
                    logger.info('runLocationMigrationBulk():socialBatchAPI results ',result);
                }
                bulk = User.collection.initializeUnorderedBulkOp();
            });

            emailSenders.sendUncaughtException('-----------** DONE **-----------','DONE','DONE'," Completed runLocationMigration()");
        })
    };
}

function runBatchWithUserIdList(userId,callback){
    if(userId){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1,companyName:1,designation:1,mobileNumber:1,publicProfileUrl:1,profilePicUrl:1,skypeId:1,location:1,twitter:1},function(error,user){
            if(error){
                logger.info('Error in runBatchWithUserIdList():BatchAPI ',error,userId);
                runBatchWithUserIdList(userIdListTwitter.shift(),callback);
            }
            else if(user){
                twitterObj.twitterFeedToInteractions(user.twitter,user,function(isSuccess){
                    console.log("Social TFI:\n\n"+"user.twitter\n\n"+user.twitter+"\n\nuser:\n\n"+user);
                    if(!isSuccess){
                        logger.info('Twitter batch failed for user ',user.emailId);
                    }else logger.info('Twitter batch success for user ',user.emailId);
                    runBatchWithUserIdList(userIdListTwitter.shift(),callback);
                })
            }
            else {
                logger.info('NO USER in runBatchWithUserIdList():BatchAPI ',error,userId);
                runBatchWithUserIdList(userIdListTwitter.shift(),callback);
            }
        });
    }
    else callback();
}

function runBatchWithUserIdListLinkedin(userId,callback){
    if(userId){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1,companyName:1,designation:1,mobileNumber:1,publicProfileUrl:1,profilePicUrl:1,skypeId:1,location:1,linkedin:1},function(error,user){
            if(error){
                logger.info('Error in runBatchWithUserIdListLinkedin():BatchAPI ',error,userId);
                runBatchWithUserIdListLinkedin(userIdListLinkedin.shift(),callback);
            }
            else if(user){
                linkedinObj.importLinkedInFeedToInteractions(user,user.linkedin,new Date(),function(isSuccess){
                    if(!isSuccess){
                        logger.info('Linkedin batch failed for user ',user.emailId);
                    }else logger.info('Linkedin batch success for user ',user.emailId);
                    runBatchWithUserIdListLinkedin(userIdListLinkedin.shift(),callback);
                });
            }
            else {
                logger.info('NO USER in runBatchWithUserIdListLinkedin():BatchAPI ',error,userId);
                runBatchWithUserIdListLinkedin(userIdListLinkedin.shift(),callback);
            }
        });
    }
    else callback();
}

function processUserLocation(user,callback){
    setTimeout(function(){
        var reg = new RegExp('area','i');
        var location = user.location;

        if(reg.test(location)){
            location = location.replace(reg,'');
        }

        request.get('http://maps.googleapis.com/maps/api/geocode/json?address='+location,function(err,res,data){
            if(err){
                logger.info("Error Google location Search processUserLocation():socialBatchAPI ",err, 'search content ',user.location);
                callback(null)
            }
            else {

                try {
                    data = JSON.parse(data);
                }
                catch (e) {
                    logger.info("Exception Google location Search processUserLocation():socialBatchAPI ", e, 'search content ', user.location);
                }
                if(data && data.results && data.results.length > 0 && data.results[0] && data.results[0].formatted_address) {
                    callback(data.results[0].formatted_address,data.results[0].geometry.location);
                }
                else callback(null)
            }
        })
    },500)
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

module.exports = BatchAPI;