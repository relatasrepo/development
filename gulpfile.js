var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
// var uglify = require('gulp-uglify');
var uglify = require('gulp-uglifyes');
var merge = require('merge-stream');

gulp.task('insights_rel_js', function() {

    var srcs = [
        'public/javascripts/reportsV2/index.js'
        , 'public/javascripts/todayInsights/today.js'
        , 'public/javascripts/common/common.js'
        , 'public/javascripts/common/leftMenuControllerRelatasApp.js'
        , 'public/javascripts/commits/commit.js'
        , 'public/javascripts/opportunity/services.js'
        , 'public/javascripts/common/activityLog.js'
        , 'public/javascripts/tasks/index.js'
        , 'public/javascripts/tasks/services.js'
    ]
        , dest = 'public/javascripts/gulp/insights';


    var lib_srcs = [
        'public/javascripts/schedule/jquery.js'
        ,'public/javascripts/schedule/angular.min.js'
        ,'public/javascripts/schedule/bootstrap.min.js'
        ,'public/javascripts/moment.js'
        ,'public/javascripts/moment-timezone.js'
        ,'public/javascripts/schedule/slide/toastr.min.js'
        ,'public/javascripts/datePicker/jquery.datetimepicker.js'
        ,'public/javascripts/common/d3.min.js'
        ,'public/javascripts/reports/d3-sankey.js'
    ];

    return merge(non_lib_insights(srcs,dest),lib_insights(lib_srcs,dest));

});

function lib_insights(srcs,dest){

    return gulp.src(srcs)
        .pipe(concat('lib_script_original.js'))
        .pipe(gulp.dest(dest))
        .pipe(rename('lib_script.js'))
        .pipe(uglify())
        .pipe(gulp.dest(dest));
}

function non_lib_insights(srcs,dest){

    return gulp.src(srcs)
        .pipe(concat('scripts_original.js'))
        .pipe(gulp.dest(dest))
        .pipe(rename('script.js'))
        // .pipe(uglify())
        .pipe(gulp.dest(dest));
}

gulp.task('insights_rel_css', function() {

    var srcs = [
        'public/stylesheets/css/toastr.css'
        ,'public/stylesheets/css/bootstrap.min.css'
        ,'public/stylesheets/librariesBundle.css'
        ,'public/stylesheets/css/styles.css'
        ,'public/chartist/chartist.min.css'
        ,'public/chartist//chartist-plugin-tooltip.css'
    ]
        , dest = 'public/css/gulp/insights';

    return gulp.src(srcs)
        .pipe(concat('styles_rel.css'))
        .pipe(gulp.dest(dest))
});

gulp.task('insights_rel_css', function() {

    var srcs = [
        'public/stylesheets/css/bootstrap.min.css'
        ,'public/stylesheets/librariesBundle.css'
        ,'public/stylesheets/css/styles.css'
        ,'public/chartist/chartist.min.css'
        ,'public/chartist//chartist-plugin-tooltip.css'
    ]
        , dest = 'public/css/gulp/insights';

    return gulp.src(srcs)
        .pipe(concat('styles_rel.css'))
        .pipe(gulp.dest(dest))
});
