var chaiHttp = require('chai-http');
var chai = require('chai');
chai.use(chaiHttp);
var server = require('../../app');

var expect = require('chai').expect
  , isThere = require("is-there")
  , pulseRouteFilePath = __dirname + "/../../routes/webRest/pulse.js"
  , pulseRoute

describe("Routes", function(){
  describe("pulse.js", function(){
    it("should be a file", function(){
      expect(isThere(pulseRouteFilePath)).to.be.true
    });
  });

  describe("Pulse Route", function(){

    describe("/pulse/team/summary", function(){
      it("should get list of team summary", function(){
        chai.request(server)
            .get('/pulse/team/summary')
            .end(function (err, res) {
              res.should.have.status(200);
              res.body.should.be.a('array');
              res.body.length.should.be.eql(0);
              done();
            })
      })

    })

  })
})
