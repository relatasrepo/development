var expect = require('chai').expect
  , isThere = require("is-there")
  , interactionManagementFilePath = __dirname + "/../../dataAccess/interactionManagement.js"
  , interactionManagement

describe("Data Access Layer", function(){
  describe("interactionManagement.js", function(){
    it("should be a file", function(){
      expect(isThere(interactionManagementFilePath)).to.be.true 
    }) 
  }) 

  describe("Interaction Management", function(){
    before(function(){
      interactionManagement = new (require(interactionManagementFilePath))()
    })

    describe("getInteractionsCountByCompany", function(){
      it("should be a function", function(){
        expect(typeof interactionManagement.getUsersAccountInteractionGrowth).to.equal('function')
      })

      it("should require mandatory fields", function(){
        var userIds
            ,date
            ,callback
      })
    })
  })
})
