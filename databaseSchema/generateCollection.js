/**
 * Created by naveen on 12/10/17.
 */

var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var moment = require('moment-timezone');
var _ = require("lodash");
var appCredentials = require('../config/relatasConfiguration');

var appCredential = new appCredentials();

function CreateCollection() {

}

CreateCollection.prototype.createOppCommitCollection = function (companyId,callback) {

    var collectionName = "commit_"+String(companyId)+"_"+moment().year();

    this.checkCollectionExists(collectionName,function (status) {
        if(status){
            callback(true)
        } else {
            callback(false)
        }
    })

}

CreateCollection.prototype.checkCollectionExists = function (collectionName,callback) {

    var dbUrl = 'mongodb://localhost:27017/Sep';
    var pwdRequired = false;
    var dbPasswords = appCredential.getDBPasswords();

    if (appCredential.getServerEnvironment() == 'DEV') {
        dbUrl = dbPasswords.relatasDB.dbConnectionUrl;
        pwdRequired = true;
    } else if (appCredential.getServerEnvironment() == 'LIVE') {
        dbUrl = 'mongodb://172.31.38.246:27017';
    } else if (appCredential.getServerEnvironment() == 'STAGING') {
        dbUrl = 'mongodb://localhost:27017/Relatas';
    }

    console.log(dbUrl)

    MongoClient.connect(dbUrl, function(err, db) {

        if(!err && db){
            if(pwdRequired){
                db.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {
                    if(!err && res){
                        listCollections(db,collectionName,callback)
                    } else {
                        callback(false)
                    }

                });
            } else {
                listCollections(db,collectionName,callback)
            }

        } else {
            callback(false)
        }
    });
}

function listCollections(db,collectionName,callback){

    db.listCollections().toArray(function(err, collections) {
        var collectionExists = false;

        if(!err && collections && collections.length>0){
            _.each(collections,function (co) {
                if(co.name == collectionName){
                    collectionExists = true;
                }
            })
        }

        if(!collectionExists){
            db.createCollection(collectionName, function(err, result) {
                callback(true);
                db.close();
            })

        } else {
            callback(true);
            db.close();
        }
    });
}

CreateCollection.prototype.getCommitModel = function (companyId) {

    var collectionName = "commit_"+String(companyId)+"_"+moment().year();

    var schema = new mongoose.Schema({
        opportunityNames: [],
        opportunityIds: [],
        date: {type:Date},
        source: {type:String},
        commitFor:{
            name:{type:String,
                enum:['week', 'month', 'quarter']
            },
            value: {type:String},
            quarterRange: {
                start: {type:Date},
                end: {type:Date}
            },
            year:{type:String}
        },
        amount:{type:Number},
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            default: null
        }
    });

    return mongoose.model(collectionName, schema, collectionName)
}

module.exports = CreateCollection;
