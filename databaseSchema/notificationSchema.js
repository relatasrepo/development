/**
 * Created by naveen on 13/11/17.
 */

/*
 Notifications Collection Schema
 */

(function () {
    var notificationSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    notificationSchema = new mongoose.Schema( {
        ownerEmailId:String,
        ownerId:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        notificationsList:[{
            module:{type:String, default:'tasks', enum:['meeting', 'weeklyReview', 'insights', 'tasks']},
            count:{
                type:Number,
                default:1
            }
        }],
        count:{
            type:Number,
            default:1
        }
    });
    exports.notifications = mongoose.model('notifications', notificationSchema, 'notifications');

}).call(this);
