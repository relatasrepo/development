
(function () {

    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , accessControlOpportunitiesSchema = new Schema({
        companyId: {type: mongoose.Schema.Types.ObjectId, required: true},

        userId: {type: mongoose.Schema.Types.ObjectId, required: true},

        allACOpportunities:[],
        orgHierarchyACOpportunities:[],
        revenueACOpportunities:[],
        productACOpportunities:[],
        businessUnitACOpportunities:[],
        regionACOpportunities:[],
        solutionACOpportunities:[],
        typeACOpportunities:[],
        itACOpportunities:[], //Internal team access opps. There will be no corresponding RMS or TMs for these opps.
        verticalACOpportunities:[],

        allACReportingManagers:[],
        orgHierarchyACReportingManagers:[],
        revenueHierarchyACReportingManagers:[],
        productHierarchyACReportingManagers:[],
        businessUnitHierarchyACReportingManagers:[],
        regionHierarchyACReportingManagers:[],
        solutionHierarchyACReportingManagers:[],
        typeHierarchyACReportingManagers:[],
        verticalHierarchyACReportingManagers:[],

        allACTeamMembers:[],
        orgHierarchyACTeamMembers:[],
        revenueHierarchyACTeamMembers:[],
        productHierarchyACTeamMembers:[],
        businessUnitHierarchyACTeamMembers:[],
        regionHierarchyACTeamMembers:[],
        solutionHierarchyACTeamMembers:[],
        typeHierarchyACTeamMembers:[],
        verticalHierarchyACTeamMembers:[]


    });

    exports.accessControlOpportunities = mongoose.model('accessControlOpportunities', accessControlOpportunitiesSchema, 'accessControlOpportunities');

}).call(this);