(function () {

    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , documentTemplateSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        createdDate:{type:Date},
        documentTemplateName:{type:String, default:null},
        documentTemplateType:{ type:String, default:null},
        documentTemplateUiId:{type:Number, default:1},
        isDocumentCreated:{ type:Boolean, default:false},
        numberOfDocumentsCreated:{ type:Number, default:0},
        isDocTemplateDeactivated:{ type:Boolean, default:false},
        deactivatedDate:{type:Date},
        isDefaultTemplate:{ type:Boolean, default:false},
        isTemplateVersionControlled:{ type:Boolean, default:false},

        docTemplateElementList:[
                { type:String, default:null}
            ],

        docTemplateAttrList:[{
            attributeId:{ type:Number, default:0},
            attributeName:{type:String, default:null, required: true},
            attributeType:{ type:String, default:null},
            attributeValue:{ type:String, default:null},
            attributeFormula:{ type:String, default:null},
            isSystemReferenced:{type:Boolean,default:false},
            systemReferencedCollection:{ type:String, default:null},
            systemReferencedAttributeName:{ type:String, default:null},
            isAttributeMandatory:{type:Boolean,default:false},
            isAttributeCustomerVisible:{type:Boolean,default:false},
            repeatOnEveryPage:{type:Boolean,default:false}
        }],

        docTemplateTableList:[ {
            tableName:{type:String, required:true},

            tableColumns: [ {
                columnId:{type:Number, default:0},
                columnName:{type:String, default:null},
                columnType:{type:String, default:null},
                columnValue:{ type:String, default:null},
                columnPosition:{type:Number,default:0},
                columnWidth:{type:Number,default:0},
                columnFormula:{type:String, default:null},
                isSystemReferenced:{type:Boolean,default:false},
                systemReferencedCollection:{ type:String, default:null},
                systemReferencedAttributeName:{ type:String, default:null},
                isColumnMandatory:{type:Boolean,default:false},
                isColumnCustomerVisible:{type:Boolean,default:false},
            } ]
        }]
    });

    exports.documentTemplates = mongoose.model('documentTemplate', documentTemplateSchema, 'documentTemplate');

}).call(this);
