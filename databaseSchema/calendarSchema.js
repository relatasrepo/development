/**
 * Created by elavarasan on 20/10/15.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var calendar=new Schema({
    _list:{type:Schema.ObjectId,ref:'CalendarList'},
    invitationId: { type:String, default:null},
    suggested: Boolean,
    senderId: { type:String, default:null},
    senderName: { type:String, default:null},
    senderEmailId: { type:String, default:null, required: true, trim: true},
    senderPicUrl: { type:String, default:null},
    isSenderPrepared:Boolean,
    readStatus: Boolean,
    scheduledDate: Date,
    googleEventId: { type:String, default:null},
    officeEventId: { type:String, default:null},
    iCalUID:{ type:String, default:null},
    updateCount: Number,
    lastActionUserId: { type:String, default:null},
    to: {
        receiverId: { type:String, default:null},
        receiverName: { type:String, default:null},
        receiverEmailId: { type:String, default:null},
        canceled:Boolean,
        canceledDate: Date,
        isPrepared:Boolean
    },
    suggestedBy: {
        userId: { type:String, default:null},
        suggestedDate: Date
    },
    selfCalendar: {type:Boolean, default:false},
    isGoogleMeeting: {type:Boolean, default:false},
    isOfficeOutlookMeeting: {type:Boolean, default:false},
    toList: [
        {
            receiverId: { type:String, default:null},
            receiverFirstName: { type:String, default:null},
            receiverLastName: { type:String, default:null},
            receiverEmailId: { type:String, default:null},
            isAccepted: Boolean,
            canceled:Boolean,
            canceledDate: Date,
            acceptedDate: Date,
            isPrepared:Boolean,
            isResource:{type:Boolean,default:false}
        }
    ],
    docs: [
        {
            documentId: { type:String, default:null},
            documentName: { type:String, default:null},
            documentUrl: { type:String, default:null},
            addedBy:{type: mongoose.Schema.Types.ObjectId,ref: 'User',default:null},
            addedOn: Date
        }
    ],
    icsFile: {
        awsKey: { type:String, default:null},
        url: { type:String, default:null}
    },
    scheduleTimeSlots: [
        {
            isAccepted: Boolean,
            acceptedDate: Date,
            messages: [{
                messageDate: Date,
                message: { type:String, default:null},
                userId: { type:String, default:null}
            }],
            start: {
                date: Date
            },
            end: {
                date: Date
            },
            title: { type:String, default:null},
            location: { type:String, default:null},
            suggestedLocation: { type:String, default:null},
            locationType: { type:String, default:null},
            description: { type:String, default:null}
        }
    ],
    participants: [
        {
            emailId: String
        }
    ],
    comments:[
        {
            messageDate: Date,
            message: { type:String, default:null},
            userId: { type:String, default:null}
        }
    ],

    meetingLocation: {
        country: { type:String, default:null},
        city: { type:String, default:null},
        region: { type:String, default:null},
        latitude: Number,
        longitude: Number
    },
    deleted: Boolean,
    deletedDate:Date,
    lastModified: Date,
    toDo:[
        {
            actionItem:{ type:String, default:null},
            assignedTo:{ type:String, default:null},
            createdBy:{ type:String, default:null},
            createdDate:Date,
            googleEventId:{ type:String, default:null},
            status:{type:String,default:'notStarted'},
            dueDate:Date,
            updatedDate:Date,
            updateCount:{type:Number,default:1}
        }
    ],
    tasks:[{type: mongoose.Schema.Types.ObjectId,ref: 'tasks'}]

});
module.exports=mongoose.model('Calendar',calendar);
