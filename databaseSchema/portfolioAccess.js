/*
 Relatas : Portfolio Access Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , portfolioAccessSchema = new Schema({
        companyId: {type: mongoose.Schema.Types.ObjectId, required: true},
        type:{type:String,default:true},
        name: {type:String,default:null},
        isHead:{type:Boolean,default:false},
        ownerEmailId:{type:String,default:null},
        targets: [{
            fy:{
                start:{type:Date},
                end:{type:Date}
            },
            values:[{
                date:{type:Date, default:null},
                amount:{type:Number,default:0}
            }]
        }],
        accessLevel:[{
            type:{type:String,default:null},
            values:[]
        }]
    });

    exports.portfolioAccess = mongoose.model('portfolioAccess', portfolioAccessSchema, 'portfolioAccess');

}).call(this);
