(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , accountSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        ownerId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        createdDate:{type:Date},
        lastInteractedDate:{type:Date},
        name:{type:String},
        ownerEmailId:{type:String},
        costOfDelivery:{type:Number,default:0},
        costOfSales:{type:Number,default:0},
        access:[{
            emailId:{ type:String, default:null}
        }],
        contacts:[],
        accountAccessRequested:[{
            emailId:{type:String},
            date:{type:Date}
        }],
        hierarchy: [{
            fullName: {type:String, default: null},
            designation: {type:String, default: null},
            emailId: {type:String, default: null},
            relationshipType: {type:String, default: null},
            type: {type:String, default: null},
            hierarchyParent: {type:String, default: null},
            hierarchyPath: {type:String, default: null},
            orgHead: {type:Boolean, default: null}
        }],
        source:{ type:String, default:null},
        partner:{ type:Boolean, default:false},
        important:{ type:Boolean, default:false},
        reportingHierarchyAccess:{ type:Boolean, default:true},
        target:[{
            rawStartDate:{type: Date,default:null},
            rawEndDate:{type: Date,default:null},
            amount:{type: Number,default:0},
            year:{type: String,default:null},
            costOfDelivery:{type:Number,default:0},
            costOfSales:{type:Number,default:0}
        }]
    });

    exports.accounts = mongoose.model('accounts', accountSchema, 'accounts');

}).call(this);
