/*
 Relatas : opportunityLog Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , oppCommitSchema = new Schema({
        opportunities: [{
            opportunityId:{type:String},
            opportunityName:{type:String},
            amount:{type:Number},
            closeDate:{type:Date},
            stage:{type:String}
        }],
        date: {type:Date},
        commitWeekYear: {type:String},
        monthYear: {type:String},
        quarterYear: {type:String},
        commitForDate: {type:Date},
        week:{
            userCommitAmount:{type:Number},
            relatasCommitAmount:{type:Number}
        },
        month:{
            userCommitAmount:{type:Number},
            relatasCommitAmount:{type:Number}
        },
        quarter:{
            userCommitAmount:{type:Number},
            relatasCommitAmount:{type:Number}
        },
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            default: null
        },
        commitFor:{
            type:String,
            enum:['week', 'month','quarter']
        },
        userEmailId:{type:String}
    });

    exports.oppCommits = mongoose.model('oppCommits', oppCommitSchema, 'oppCommits');

}).call(this);
