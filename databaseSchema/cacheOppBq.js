(function () {
    var cacheOppBqSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    cacheOppBqSchema = new mongoose.Schema({
        companyId: {type: mongoose.Schema.Types.ObjectId, required: true},
        userId: {type: mongoose.Schema.Types.ObjectId, required: true},
        userEmailId: { type:String, default:null},
        createdByEmailId: { type:String, default:null},
        contactEmailId: { type:String, default:null},
        salesforceContactId: { type:String, default:null},
        opportunityId: {type: String, unique:true},
        sourceOpportunityId: {type: String},
        opportunityName: { type:String, default:null},
        createdDate: {type: Date, default: null},
        closeDate: {type: Date, default: null},
        amount: { type:Number, default:0 },
        netGrossMargin: { type:Number, default:100 },
        isClosed: {type:Boolean, default:false },
        isWon: {type:Boolean, default:false },
        stageName: { type:String, default:null},
        mobileNumber: { type:String, default:null},
        source: { type:String, default:null},
        relatasStage:{ type:String, default:null},
        productType:{ type:String, default:null},
        competitor:{ type:String, default:null},
        sourceType:{ type:String, default:null},
        vertical:{ type:String, default:null},
        businessUnit: { type:String, default:null },
        type: { type:String, default:null },
        solution: { type:String, default:null },
        currency: { type:String, default:null },
        xr: { type:Number, default:1 },
        listOfItems:[{
            name: { type:String, default:null },
            businessUnit: { type:String, default:null },
            solution: { type:String, default:null },
            currency: { type:String, default:null },
            xr: { type:Number, default:1 },
            amount: { type:Number, default:0 },
            netGrossMargin: { type:Number, default:100 },
        }],
        BANT:{
            budget:{type:Boolean, default:false},
            authority:{type:Boolean, default:false},
            need:{type:Boolean, default:false},
            time:{type:Boolean, default:false}
        },
        interactionCount:{type:Number,default:0},
        interactionsInProgress:{type:Number,default:0},
        lastStageUpdated:{
            fromStage:{type:String, default:null},
            date:{type:Date, default:null}
        },
        partners:[{
            interactionCount:{type:Number,default:0},
            interactionsInProgress:{type:Number,default:0},
            fullName:{ type:String, default:null},
            personId:{ type: String, default:null},
            contactId:{ type: String, default:null},
            emailId:{ type:String, default:null}
        }],
        decisionMakers:[{
            interactionCount:{type:Number,default:0},
            interactionsInProgress:{type:Number,default:0},
            fullName:{ type:String, default:null},
            personId:{ type: String, default:null},
            contactId:{ type: String, default:null},
            emailId:{ type:String, default:null}
        }],
        influencers:[{
            interactionCount:{type:Number,default:0},
            interactionsInProgress:{type:Number,default:0},
            fullName:{ type:String, default:null},
            personId:{ type: String, default:null},
            contactId:{ type: String, default:null},
            emailId:{ type:String, default:null}
        }],
        geoLocation:{
            town:{ type:String, default:null},
            zone:{ type:String, default:null}
        },
        notes:[{
            createdDate:{type:Date, default:null},
            text:{type:String, default:null}
        }],
        closeReasons:[],
        closeReasonDescription:{ type:String, default:null},
        interactionCount: {type:Number}

    });
    exports.cacheOppBq = mongoose.model('cacheOppBq', cacheOppBqSchema, 'cacheOppBq');
}).call(this);