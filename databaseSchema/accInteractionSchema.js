(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , accInteractionSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: false
        },
        ownerId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        accountName:{type:String},
        ownerEmailId:{type:String},
        interactions:{type:Number},
        firstInteractedDate:{type:Date}
    });

    exports.accInteraction = mongoose.model('accInteraction', accInteractionSchema, 'accInteraction');

}).call(this);
