/*
 Relatas : insightsAdminReports Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , insightsAdminReportsSchema = new Schema({
        losingTouch:{type: Number},
        losingTouchImpact:{type: Number},
        dealsAtRisk:{type: Number},
        allInteractions:{type: Number},
        oppInteractions:{type: Number},
        dealsAtRiskImpact:{type: Number},
        dealsAtRiskAction:[],
        totalRegUsers:{type: Number},
        totalWebLogins:{type: Number},
        totalMobLogins:{type: Number},
        uniqueMobLogins:{type: Number},
        uniqueWebLogins:{type: Number},
        mobActiveUsers:{type: Number},
        totalActiveUsers:{type: Number},
        pipelineVelocity:{
            expectedPipeline:{type:Number},
            shortfall:{type:Number},
            totalCount:{type:Number},
            totalAmount:{type:Number},
            wonCount:{type:Number},
            wonAmount:{type:Number},
            currentOopPipeline:[],
            currentQuarter: {type: String},
            currentTargets:[],
            oppNextQ:[],
            staleOpps:[]
        },
        oppREConversion:{type: Number},
        date: {type: Date},
        dayString: {type: String},
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        ownerId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        ownerEmailId: {
            type: String,
            required: true
        }
    });

    exports.insightsAdminReports = mongoose.model('insightsAdminReports', insightsAdminReportsSchema, 'insightsAdminReports');

}).call(this);
