(function() {

    var notificationsSchema, mongoose;
    mongoose = require('../node_modules/mongoose');
    var Schema = mongoose.Schema;

    notificationsSchema = new Schema({
        messageId: {type:String, default:null },
        userId: { type:mongoose.Schema.Types.ObjectId, default:null },
        emailId: { type:String, default:null },
        companyId:{
            type:mongoose.Schema.Types.ObjectId,
            ref: 'Company',
            default: null
        },
        notificationTitle: {type:String, default:null },
        notificationBody: {type:String, default:null },
        url: {type:String, default:null },
        data: { type:String, default:null },
        sentDate: { type:Date, default:null },
        openDate: { type:Date, default:null },
        dayString: {type:String, default: null},
        category: { type:String, default:null },
        accessed: { type:Boolean, default:false },
        dataFor:{
            type:String,
            default: null,
            enum:['mobile', 'web']
        },
        dataReferenceType: {
            type:String,
            default: null,
            enum:['notification', 'war']
        },
        abstractReportFor: {
            type:String,
            default: 'user',
            enum:['manager', 'user']
        },
        notificationType: {
            type:String,
            default: null,
            enum:['daily', 'weekly', 'realtime']
        },
        achievement: {
            target: {type:Number, default:0},
            won: {type:Number, default:0},
            lost: {type:Number, default:0},
            commit: {type:Number, default:0},
            commitPipeline: {type:Number, default:0},
            totalCommitOpps: {type:Number, default:0},
            commitOppsValue: {type:Number, default:0},
            commitStage: {type:String, default:null}
        },
        futureOpps: {
            closingInDays: {type:Number, default:1},
            count: {type:Number, default:0},
            amount: {type:Number, default:0},
            opportunityIds: []
        },
        dealsAtRisk: {
            count: {type:Number, default:0},
            amount: {type:Number, default:0},
            deals: []
        },
        staleOpps: {
            count: {type:Number, default:0},
            amount: {type:Number, default:0},
            opportunityIds: []
        },
        tasks: {
            count: {type:Number},
            tasks: []
        },
        meetings: {
            count: {type:Number},
            meetings: []
        },
        losingTouchContacts: {
            totalContacts: {type:Number},
            contacts:[]
        },
        pastOpps: {
            count: {type:Number, default:0},
            wonCount: {type:Number, default:0},
            lostCount: {type:Number, default:0},
            amount: {type:Number, default:0},
            lostAmount: {type:Number, default:0},
            wonAmount: {type:Number, default:0},
            opportunityIds: []
        }

    })

    exports.Notifications = mongoose.model('Notifications', notificationsSchema, 'notifications');

}).call(this);