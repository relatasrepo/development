/**
 * Created by naveen on 13/11/17.
 */

/*
 Tasks Collection Schema
 */

(function () {
    var TasksSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    TasksSchema = new mongoose.Schema({
        taskName:String,
        assignedTo:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        assignedToEmailId:String,
        createdBy:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        participants:[{
            emailId:String,
            responseRequired:{type:Boolean,default:false}
        }],
        createdByEmailId:String,
        createdDate:{
            type:Date,
            default: new Date()
        },
        googleEventId:String,
        status:{
            type:String,
            default:'notStarted',  /* notStarted, inProgress, complete */
            enum:['notStarted', 'inProgress', 'complete']
        },
        dueDate:{type:Date,default:null},
        closeDate:{type:Date,default:null},
        updatedDate:{type:Date,default:null},
        taskFor:{type:String, default:'other', enum:['meeting', 'weeklyReview', 'mobile', 'call', 'opportunities', 'other']},
        refId:{type:String,default:null},
        threadId:{type:String,default:null},
        description:String,
        priority:{type:String, default:'high', enum:['high', 'medium', 'low']},
        updateCount:{
            type:Number,
            default:1
        }
    });
    exports.tasks = mongoose.model('tasks', TasksSchema, 'tasks');

}).call(this);
