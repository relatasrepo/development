/*
 Relatas : marketingMaterial Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , marketingMaterialSchema = new Schema({
        entityType:{
            type:String,
            enum:["video", "image"]
        },
        urls:[{
            order:{type:Number},
            title:{type:String},
            description:{type:String},
            url:{type:String},
            originalUrl:{type:String},
            dateAdded:{type: Date},
            isDefault:{type:Boolean,default:false}
        }]

    });

    exports.marketingMaterial = mongoose.model('marketingMaterial', marketingMaterialSchema, 'marketingMaterial');

}).call(this);