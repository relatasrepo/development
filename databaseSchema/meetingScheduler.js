(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , meetingSchedulerSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        isDeleted:{type:Boolean,default:false},
        uploadedDate:{type:Date},
        meetingDate:{type:Date},
        toEmailId:{type:String},
        fromEmailId:{type:String},
        location:{
            address:{ type:String, default:null},
            lat:{ type:Number, default:null},
            lng:{ type:Number, default:null}
        },
        contacts:[],
        status:{enum:['declined', 'confirmed', 'postponed']},
        account:{type:String},
        notes:[{
            text:{type:String},
            fromEmailId:{type:String},
            date:{type:Date}
        }]
    });

    exports.meetingScheduler = mongoose.model('meetingScheduler', meetingSchedulerSchema, 'meetingScheduler');

}).call(this);
