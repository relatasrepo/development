(function () {

    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , documentSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },

        documentCreatorId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'userSchema',
            required: true
        },

        documentTemplateId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'documentTemplates',
            required: true
        },

        documentCreatedBy:{type:String, required:true},
        documentCreatedDate:{type:Date},
        documentUpdatedDate:{type:Date},
        documentTemplateType:{ type:String, default:null},
        documentTemplateName:{type:String, default:null},
        documentName:{type:String, required:true},
        isTemplateVersionControlled:{ type:Boolean, default:false},
        documentVersion:{type:Number, default:1},
        currentDocumentVersion:{type:Number, default:1},
        documentStage:{type:String, default:'Draft', enum:['Draft', 'Sent', 'Accepted']},
        
        oppReferenceIds: [{
            type: String,
            default:null
        }],

        documentElementList:[
            { type:String, default:null}
        ],

        documentAttrList:[{
            attributeId:{ type:Number, default:0},
            attributeName:{type:String, default:null},
            attributeType:{ type:String, default:null},
            attributeValue:{ type:String, default:null},
            attributeFormula:{ type:String, default:null},
            isSystemReferenced:{type:Boolean,default:false},
            systemReferencedCollection:{ type:String, default:null},
            systemReferencedAttributeName:{ type:String, default:null},
            isAttributeMandatory:{type:Boolean,default:false},
            isAttributeCustomerVisible:{type:Boolean,default:false},
            repeatOnEveryPage:{type:Boolean,default:false}
        }],

        documentTableList:[ {
            tableName:{type:String, required:true},

            tableColumns: [ {
                columnId:{type:Number, default:0},
                columnName:{type:String, default:null},
                columnType:{type:String, default:null},
                columnValue:{ type:String, default:null},
                columnPosition:{type:Number,default:0},
                columnWidth:{type:Number,default:0},
                columnFormula:{type:String, default:null},
                isSystemReferenced:{type:Boolean,default:false},
                systemReferencedCollection:{ type:String, default:null},
                systemReferencedAttributeName:{ type:String, default:null},
                isColumnMandatory:{type:Boolean,default:false},
                isColumnCustomerVisible:{type:Boolean,default:false},
            } ]
        }]
    });

    exports.documentModel = mongoose.model('document', documentSchema, 'document');

}).call(this);