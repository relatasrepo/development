(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , newCompaniesInteractionSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: false
        },
        ownerId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        accountName:{type:String},
        monthYear:{type:String},
        interactionDate:{type:Date},
        ownerEmailId:{type:String},
        emailId:{type:String}
    });

    exports.newCompaniesInteraction = mongoose.model('newCompaniesInteraction', newCompaniesInteractionSchema, 'newCompaniesInteraction');

}).call(this);
