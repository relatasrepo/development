/*
 Relatas : opportunityLog Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , oppLogSchema = new Schema({
        opportunityName: {type:String},
        opportunityId: {type:String},
        action: {type:String},
        type: {type:String},
        oldValue: {type:String},
        newValue: {type:String},
        date: {type:Date},
        fromUserId: {type: mongoose.Schema.Types.ObjectId, required: true},
        fromEmailId: { type:String,default:null},
        toUserId: {type: mongoose.Schema.Types.ObjectId, required: true},
        toEmailId: { type:String,default:null},
        transferredBy: { type:String,default:null},
        notes: { type:String,default:null}
    });

    exports.opportunityLog = mongoose.model('opportunityLog', oppLogSchema, 'opportunityLog');

}).call(this);
