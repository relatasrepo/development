/*
Product Recommendation Schema
*/

(function() {
    var mlInsightsSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    mlInsightsSchema = new mongoose.Schema({
        mlInsightsDate:{type:Date},
        companyName:{type:String, default:null},
        rlUserEmailId:{type:String, default:null},
        accountDomainName:{type:String, default:null},
        oppsWonCount:{type:Number, default:0},
        oppsLostCount:{type:Number, default:0},
        oppsPipeLineCount:{type:Number, default:0},
        interactionsCount:{type:Number, default:0},
        oppsTotalCount:{type:Number, default:0},
        maxAmount:{type:Number, default:0},
        minAmount:{type:Number, default:0},
        recommendedProduct:{type:String, default:null},
        recommendedAmount:{type:Number, default:0},
        recommendedUser:{type:String, default:null},
        nSparkPositiveSentiments:{type: Number, default: 0},
        nSparkNegativeSentiments:{type: Number, default: 0},
        sparkIntent:{type: String, default: null},
        nSignedDocumentsSent:{type: Number, default: 0},
        nImportantDocumentsRequested:{type: Number, default: 0},
        nQuestionAskedAndNotAnswered:{type: Number, default: 0},
        ignore:{type: Boolean, default: false},
        converted:{type: Boolean, default: false}
    });

    exports.rrsProducts = mongoose.model('rrsProducts', mlInsightsSchema, 'rrsProducts');

}).call(this);