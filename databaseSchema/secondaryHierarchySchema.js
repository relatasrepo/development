
(function () {

    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , secondaryHierarchySchema = new Schema({
        companyId: {type: mongoose.Schema.Types.ObjectId, required: true},
        secondaryHierarchyName:{ type:String, default:null},
        secondaryHierarchyType:{
            type:String,
            default:null,
            enum:['Revenue', 'Product','BusinessUnit', 'Region', 'Vertical', 'Solution', 'Type']
        },
        userId: {type: mongoose.Schema.Types.ObjectId, required: true},
        ownerEmailId:{type:String},
        hierarchyParent: {type: Schema.Types.ObjectId, ref: 'User', default:null},
        hierarchyPath:{type:String, default:null},
        orgHead:{type:Boolean,default:false},
        secondaryHierarchyItemsList:[],
        hasReportee:{type:Boolean,default:false},
        secondaryHierarchyMonthlyTargets:[{
            date:{type: Date,default:null},
            target:{type: Number,default:0},
            monthYear:{type: String,default:null}
        }],

        secondaryHierarchyMonthlyAchievements:[{
            date:{type: Date,default:null},
            achievements:{type: Number,default:0},
            monthYear:{type: String,default:null}
        }]
    });

    exports.secondaryHierarchy = mongoose.model('secondaryHierarchy', secondaryHierarchySchema, 'secondaryHierarchy');

}).call(this);