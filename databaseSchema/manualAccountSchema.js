(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , manualAccountSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        createdDate:{type:Date},
        name:{type:String},
        group:{type:String},
        domain:{type:String},
        branch:{type:String},
        address:{type:String},
        phone:{type:String},
        contacts:[]
    });

    exports.manualAccounts = mongoose.model('manualAccounts', manualAccountSchema, 'manualAccounts');

}).call(this);
