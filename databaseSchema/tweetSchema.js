/*
 Relatas : Tweet Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , tweetFeedSchema = new Schema({
        // entityType:{
        //     type:String,
        //     enum:["contact", "company"]
        // },
        tweet: {type:String},
        twitterUserName: {type:String},
        tweetId: {type:String},
        tweetedDate:{type: Date},
        userLocation:{type: String}
    });

    exports.twitterfeed = mongoose.model('twitterfeed', tweetFeedSchema, 'twitterfeed');

}).call(this);
