/*
 Relatas : Social Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , socialFeedSchema = new Schema({
        entityType:{
            type:String,
            enum:["user", "company"]
        },
        entityId: {
            type: mongoose.Schema.Types.ObjectId
        },
        entityName: {
          type: String
        },
        rssFeed: {
          type: String
        },
        emailId: {type: String},
        newsFeeds:[
          {
            guid: {type: String},
            title: {type: String},
            description: {type: String},
            publishedDate: {type: Date},
            companyName: {type: String}
          }
        ]

    })
    exports.socialfeed = mongoose.model('socialfeed', socialFeedSchema, 'socialfeed');

}).call(this);
