(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , dashboardInsightSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: false
        },
        ownerId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        ownerEmailId:{type:String},
        dayString:{type:String},
        forTeam:{type:Boolean,default:false},
        date:{type:Date},
        oppWon:{
            amount:{type:Number},
            count:{type:Number}
        },
        oppLost:{
            amount:{type:Number},
            count:{type:Number}
        },
        stale:{
            amount:{type:Number},
            count:{type:Number}
        },
        reasonsWon:[{
            name:{type:String},
            oppAmount:{type:Number},
            oppCount:{type:Number},
            reasonCount:{type:Number}
        }],
        reasonsLost:[{
            name:{type:String},
            oppAmount:{type:Number},
            oppCount:{type:Number},
            reasonCount:{type:Number}
        }],
        dealsAtRisk:{
            amount:{type:Number},
            count:{type:Number},
            dealsRiskAsOfDate:{type:Date}
        },
        renewalOpen:{
            amount:{type:Number},
            count:{type:Number}
        },
        accountsWon:[{
            amount:{type:Number},
            name:{type:String}
        }],
        productsWon:[{
            amount:{type:Number},
            name:{type:String}
        }],
        typesWon:[{
            amount:{type:Number},
            count:{type:Number},
            name:{type:String}
        }],
        sourcesWon:[{
            amount:{type:Number},
            name:{type:String}
        }],
        regionsWon:[{
            amount:{type:Number},
            name:{type:String}
        }],
        conversionRate: [{
            created:[],
            closed:[],
        }],
        newCompaniesInteracted: [{
            monthYear:{type:String},
            sortDate:{type:Date},
            accountDetails:[], // List of accounts
        }],
        pipelineVelocity:[{
            month:{type:String},
            target:{type:String},
            won:{type:String},
            pipeline:{type:String},
            lost:{type:String},
        }],
        pipelineFunnel:[{
            _id:{type:String},
            totalAmount:{type:Number},
            opps:[],
        }],
        oppsInteractions: {
            interactions:[],
            opportunities: []
        },
        topOppByOppThisQtr: {
            interactions:[],
            opportunities: []
        },
        pipelineFlow:{
            metaData:[],
            oppStages:[],
            qStart:{type:Date},
            qEnd:{type:Date},
            commitStage:{type:String}
        },
        targets:[],
        currentInsights: []
      });

    exports.dashboardInsight = mongoose.model('dashboardInsight', dashboardInsightSchema, 'dashboardInsight');

}).call(this);