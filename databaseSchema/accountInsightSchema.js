(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , accountInsightSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: false
        },
        forSection: {type:String,default:"dashboard"},
        dayString:{type:String},
        date:{type:Date},
        accountsInteractions:{
            interactions: [{
                axis:{type:String},
                value:{type:Number},
                name:{type:String},
                interactionsCount:{type:Number},
                usersWithAccess:[]
            }],
            opportunities: [{
                axis:{type:String},
                value:{type:Number},
                name:{type:String},
                oppCount:{type:Number},
                oppAmount:{type:Number},
                usersWithAccess:[]
            }]
        },
        topAccByOppThisQtr: {
            opportunities: [{
                axis:{type:String},
                value:{type:Number},
                name:{type:String},
                oppCount:{type:Number},
                oppAmount:{type:Number},
                usersWithAccess:[]
            }]
        }
      });

    exports.accountInsight = mongoose.model('accountInsight', accountInsightSchema, 'accountInsight');

}).call(this);