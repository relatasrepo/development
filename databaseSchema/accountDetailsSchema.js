(function () {

    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , accountDetailsSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        createdDate:{type:Date},
        accountName:{type:String},
        accountType:{ type:String, default:null},
        accountWebSite:{ type:String, default:null},
        accountAddressDelails:[{
            accBranchName:{type: String,default:null},
            accBranchAddress:{type: String,default:null},
            accBranchPhone:{type: Number,default:0},
            accBranchFax:{type: String,default:null}
        }],
        accountGSTN:{type: String,default:null},
        accountPAN:{type: String,default:null},
        accountCIN:{type: String,default:null},
        accountURL:{type: String,default:null},

    });

    exports.accountDetails = mongoose.model('accountDetails', accountDetailsSchema, 'accountsDetails');

}).call(this);
