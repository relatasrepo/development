/*
 Relatas : Portfolios Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , portfoliosSchema = new Schema({
        companyId: {type: mongoose.Schema.Types.ObjectId, required: true},
        name:{type:String},
        required:{type:Boolean,default:false},
        fromPortfolios:{type:Boolean,default:true},
        list:[{
            name: {type:String,default:null},
            head:{type:String,default:null},
            headEmailId:{type:String,default:null},
            targets: [{
                fy:{
                    start:{type:Date},
                    end:{type:Date}
                },
                values:[{
                    date:{type:Date, default:null},
                    amount:{type:Number,default:0}
                }]
            }]
        }]
    });

    exports.portfolios = mongoose.model('portfolios', portfoliosSchema, 'portfolios');

}).call(this);
