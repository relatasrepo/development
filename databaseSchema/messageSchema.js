/*
 Relatas : Message Schema
 */

/*
* Schema design
*
*  @param {Conversation ID} conversationId  - The conversation ID which will be used as a conversation thread.
*
*  @param {Message Reference Type} messageReferenceType  - The reference to another collection's object.
*  For eg. 'opportunities' will be for conversations on the opportunities. This will carry another property.
*
*  @param {Message Reference ID} messageReferenceId  - The messageReferenceId is of type Object ID.
*  For eg. ObjectId("58465077dfd36f6e2aef527a") of a document ID of an opportunity.
*
*  @param {Message Owner}messageOwner - Details of the owner of this message. This will be a Relatas user
*
*  @param {Text} text - The message itself
*
*  @param {Email ID} emailId - The other person in this conversation. Will be Relatas user for now- 06 Dec 2016
*
*  @param {userId} userId - The userId of other person in this conversation. Reference to user collection
*
*  @param {Date} date - Message created date
*
* */

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
    , messageSchema = new Schema({
        conversationId: {type: Schema.Types.ObjectId},
        messageReferenceType: {
            type:String,
            default:null,
            enum:["opportunities","tasks"]
        },
        messageReferenceId: {type: Schema.Types.ObjectId, default:null},
        messageOwner: {
            fullName: {type:String},
            emailId: {type:String}
        },
        text: {type:String},
        emailId: {type:String,default:null},
        userId:{type: Schema.Types.ObjectId, default:null},
        date: {type: Date},
        otherParticipants:[]
    });

    exports.messages = mongoose.model('messages', messageSchema, 'messages');

}).call(this);