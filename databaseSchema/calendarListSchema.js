/**
 * Created by elavarasan on 20/10/15.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var calendarList = new Schema({
    name:{type:String,required:true},
    color:{type:String,required:true},
    userId:{type:Schema.Types.ObjectId},
    email:{type:String},
    description:{type:String},
    calendar:[{type:Schema.ObjectId,ref:'Calendar'}]
});
module.exports = mongoose.model('CalendarList', calendarList);