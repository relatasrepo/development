(function () {

    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , customerProfileSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },

        customerProfileTemplateId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'customerProfileTemplateModel',
            required: false
        },

        accountId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'accounts',
            required: false
        },

        // customerProfileCreatedBy:{type:String, required:true},
        customerProfileCreatedDate:{type:Date},
        customerProfileUpdatedDate:{type:Date},
        customerProfileTemplateType:{ type:String, default:null},
        customerProfileTemplateName:{type:String, default:null},
        customerProfileName:{type:String, required:true},

        customerProfileElementList:[
            { type:String, default:null}
        ],

        customerProfileAttrList:[{
            attributeId:{ type:Number, default:0},
            attributeName:{type:String, default:null, required: true},
            attributeType:{ type:String, default:null},
            attributeValue:{ type:String, default:null},
            attributeFormula:{ type:String, default:null},
            isSystemReferenced:{type:Boolean,default:false},
            systemReferencedCollection:{ type:String, default:null},
            systemReferencedAttributeName:{ type:String, default:null},
            isAttributeMandatory:{type:Boolean,default:false},
            isAttributeCustomerVisible:{type:Boolean,default:false},
            repeatOnEveryPage:{type:Boolean,default:false}
        }],

        customerProfileTableList:[ {
            tableName:{type:String, required:true},

            tableColumns: [ {
                columnId:{type:Number, default:0},
                columnName:{type:String, default:null},
                columnType:{type:String, default:null},
                columnValue:{ type:String, default:null},
                columnPosition:{type:Number,default:0},
                columnWidth:{type:Number,default:0},
                columnFormula:{type:String, default:null},
                isSystemReferenced:{type:Boolean,default:false},
                systemReferencedCollection:{ type:String, default:null},
                systemReferencedAttributeName:{ type:String, default:null},
                isColumnMandatory:{type:Boolean,default:false},
                isColumnCustomerVisible:{type:Boolean,default:false},
            } ]
        }]
    });

    exports.customerProfileModel = mongoose.model('customerProfile', customerProfileSchema, 'customerProfile');

}).call(this);