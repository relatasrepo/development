(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , oppMetaDataSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        userEmailId:{type:String},
        month:{type:Number},
        year:{type:Number},
        data:[{
            stageName:{type:String},
            oppIds:[],
            amount:{type:Number},
            amountWithNgm:{type:Number},
            numberOfOpps:{type:Number},
            regions:[],
            reasons:[],
            verticals:[],
            products:[],
            accounts:[],
            types:[],
            sources:[],
            solutions:[],
            businessUnits:[]
        }]
    });

    exports.oppMetaData = mongoose.model('oppMetaData', oppMetaDataSchema, 'oppMetaData');

}).call(this);
