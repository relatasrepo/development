
(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , bqMetaDataSchema = new Schema({
        lastUpdatedDate:{type: String},
        collectionName:{type: String}
    });

    exports.bqMetaData = mongoose.model('bqMetaData', bqMetaDataSchema, 'bqMetaData');

}).call(this);
