/*
 Relatas : userRelation Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , userRelationSchema = new Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, required: true},
        userEmailId: { type:String, default:null},
        monthYear:{type:String,default:null},
        important:{
            contacts:{type:Number,default:0},
            interactionCount:{type:Number,default:0},
            contactsAndInteractions:[]
        },
        partners:{
            contacts:{type:Number,default:0},
            interactionCount:{type:Number,default:0},
            contactsAndInteractions:[]
        },
        decisionMakers:{
            contacts:{type:Number,default:0},
            interactionCount:{type:Number,default:0},
            contactsAndInteractions:[]
        },

        influencers:{
            contacts:{type:Number,default:0},
            interactionCount:{type:Number,default:0},
            contactsAndInteractions:[]
        },
        others:{
            contacts:{type:Number,default:0},
            interactionCount:{type:Number,default:0},
            contactsAndInteractions:[]
        }
    })
    exports.userRelation = mongoose.model('userRelation', userRelationSchema, 'userRelation');

}).call(this);
