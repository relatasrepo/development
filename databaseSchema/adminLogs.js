/*
 Relatas : adminLogs Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , adminLogsSchema = new Schema({
        companyId: {type: mongoose.Schema.Types.ObjectId, required: true},
        type:{type:String},
        action:{type:String},
        byEmailId:{type:String},
        currentSelection:{type:String},
        date:{type:Date}
    });

    exports.adminLogs = mongoose.model('adminLogs', adminLogsSchema, 'adminLogs');

}).call(this);
