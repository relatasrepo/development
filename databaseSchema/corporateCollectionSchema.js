/*
 Relatas : Corporate Schema
 */

(function() {

    var mongoose = require('mongoose');
    var Schema = mongoose.Schema;

    var corporateSchema = new Schema({
        userId:{type: Schema.Types.ObjectId, ref: 'User'},
        firstName:String,
        lastName:String,
        emailId: {type: String, ref: 'User'},
        companyId: {type: Schema.Types.ObjectId, ref: 'Company'},
        parent: {type: Schema.Types.ObjectId, ref: 'User'},
        path:String,
        active:Boolean,
        keySent:Boolean,
        key:String,
        createdDate:Date
    });

    var corporatelogoSchema = new Schema({
        companyUrl: {type: String, required: true},
        logoUrl: {type: String},
        createdAt: { type: Date }
    });
    corporatelogoSchema.pre('save', function(next){
        now = new Date()
        this.createdAt = now
        next()
    });
    
    var companySchema = new Schema({
        companyName:{type: String,default:null},
        website:{type: String,default:null},
        emailDomains:[{type: String,default:null}],
        description:{type:String,default:null},
        presentationTitle:{type:String,default:null},
        presentationType:{type:String,default:null},
        url:{type: String,required:true},
        socialInfo:{
           facebookPage:{type: String,default:null},
           linkedinPage:{type: String,default:null},
           twitterPage:{type: String,default:null}
        },
        latestNews:[
            {
                news:{type: String,default:null},
                addedOn:{type:Date, default:null},
                readMore:{type:String,default:null}
            }
        ],
        admin:[
            {
                adminId:{type: Schema.Types.ObjectId, ref: 'User',default:null},
                adminType:{type: String,default:null},
                addedOn:{type:Date, default:null},
                updatedOn:{type:Date, default:null}
            }
        ],
        videos:[
            {
                url: {type: String,default:null},
                awsKey: {type: String,default:null},
                sourceType:{type:String,default:null},
                uploadedOn: {type:Date, default:null},
                videoName: {type: String,default:null},
                uploadedBy:{type: Schema.Types.ObjectId, ref: 'User',default:null}
            }
        ],  
        images:[
            {
                url: {type: String,default:null},
                awsKey: {type: String,default:null},
                uploadedOn: {type:Date, default:null},
                imageName: {type: String,default:null},
                uploadedBy:{type: Schema.Types.ObjectId, ref: 'User',default:null}
            }
        ],
        quickStats:{
           website:{type:String,default:null},
           foundedIn:{type:String,default:null},
           headQuarters:{type:String,default:null},
           industry:{type:String,default:null},
           companySize:{type:String,default:null},
           usp:{type:String,default:null}
        },
        documents:[
            {
                url: {type: String,default:null},
                awsKey: {type: String,default:null},
                uploadedOn: {type:Date, default:null},
                documentName: {type: String,default:null},
                documentId:{type: Schema.Types.ObjectId, ref: 'documents',default:null},
                uploadedBy:{type: Schema.Types.ObjectId, ref: 'User',default:null}
            }
        ],
        slides:[
            {
                url: {type: String,default:null},
                sourceType:{type:String,default:null},
                uploadedOn: {type:Date, default:null},
                slideName: {type: String,default:null},
                uploadedBy:{type: Schema.Types.ObjectId, ref: 'User',default:null}
            }
        ],
        logo:{
            url: {type: String,default:null},
            awsKey: {type: String,default:null},
            uploadedOn: {type:Date, default:null},
            imageName: {type: String,default:null},
            uploadedBy:{type: Schema.Types.ObjectId, ref: 'User',default:null}
        },
        contacts:[
            {
                userId:{type: Schema.Types.ObjectId, ref: 'User',default:null},
                firstName:{type: String,default:null},
                lastName:{type: String,default:null},
                designation:{type: String,default:null},
                uniqueName:{type: String,default:null},
                emailId:{type: String,default:null},
                contactFor:{type: String,default:null},
                addedOn:{type:Date, default:null},
                addedBy:{type: Schema.Types.ObjectId, ref: 'User',default:null}
            }
        ],
        logFile:{type: String,default:null},
        backgroundImage:{type: String,default:null},
        createdDate:{type:Date, default:null},
        supportTeam:[],
        geoLocations:[{
            region: {type:String,default:null},
            head:{type:String,default:null},
            headEmailId:{type:String,default:null},
            targets: [{
                fy:{
                    start:{type:Date},
                    end:{type:Date}
                },
                values:[{
                    date:{type:Date, default:null},
                    amount:{type:Number,default:0}
                }]
            }]
        }],
        productList:[{
            name: {type:String,default:null},
            head:{type:String,default:null},
            headEmailId:{type:String,default:null},
            targets: [{
                fy:{
                    start:{type:Date},
                    end:{type:Date}
                },
                values:[{
                    date:{type:Date, default:null},
                    amount:{type:Number,default:0}
                }]
            }]
        }],
        sourceList:[{
            name: {type:String}
        }],
        verticalList:[{
            name: {type:String,default:null},
            head:{type:String,default:null},
            headEmailId:{type:String,default:null},
            targets: [{
                fy:{
                    start:{type:Date},
                    end:{type:Date}
                },
                values:[{
                    date:{type:Date, default:null},
                    amount:{type:Number,default:0}
                }]
            }]
        }],
        solutionList:[{
            name: {type:String,default:null}
        }],
        businessUnits:[{
            name: {type:String,default:null},
            head:{type:String,default:null},
            headEmailId:{type:String,default:null},
            targets: [{
                fy:{
                    start:{type:Date},
                    end:{type:Date}
                },
                values:[{
                    date:{type:Date, default:null},
                    amount:{type:Number,default:0}
                }]
            }]
        }],
        documentCategoryList:[{
            name: {type:String,default:null},
            active: {type:Boolean}
        }],
        typeList:[{
            name: {type:String,default:null},
            isDefaultRenewalType:{type:Boolean},
            isTypeRenewal:{type:Boolean}
        }],
        customerSetting:[{
            name: {type:String,default:null},
            mandatory:{type:Boolean,default:false}
        }],
        opportunityStages:[{
            name: {type:String,default:null},
            commitStage: {type:Boolean,default:false},
            weight: {type:Number,default:0},
            order: {type:Number,default:0},
            isWon: {type:Boolean,default:false},
            isLost: {type:Boolean,default:false},
            isStart: {type:Boolean,default:false},
            isEnd: {type:Boolean,default:false}
        }],
        closeReasons:[{
            name: {type:String}
        }],
        currency:[{
            name:{type:String,default:"United States Dollar"},
            symbol:{type:String,default:"USD"},
            xr:{type:Number,default:1},
            isPrimary:{type:Boolean,default:false},
            isDeactivated:{type:Boolean,default:false}
        }],
        mailSubject:{type:String,default:null},
        mailOutgoingBody:{type:String,default:"Thanks for reaching out to us. We will contact back shortly."},
        welcomeTitle:{type:String,default:null},
        successReply:{type:String,default:null},
        failureReply:{type:String,default:null},
        meetingTitle:{type:String,default:null},
        majorColor:{type:String,default:"#03a2ea"},
        textColor:{type:String,default:"#ffffff"},
        commitDay:{type:String,default:"Monday"},
        commitHour:{type:Number,default:18},
        monthCommitCutOff:{type:Number,default:10},
        qtrCommitCutOff:{type:Number,default:15},
        opportunitySettings:{
            regionRequired:{type:Boolean,default: true},
            verticalRequired:{type:Boolean,default: false},
            sourceRequired:{type:Boolean,default: false},
            solutionRequired:{type:Boolean,default: false},
            productRequired:{type:Boolean,default: false},
            typeRequired:{type:Boolean,default: false},
            businessUnitRequired:{type:Boolean,default: false},
            renewalStatusRequired:{type:Boolean,default: false}
        },
        netGrossMargin:{type:Boolean,default: false},
        notificationForOrgHead:{type:Boolean,default: false},
        notificationForReportingManager:{type:Boolean,default: false},
        documentNumber:{type:Number,default:1},
        salesForceSettings:{
            clientId:{type:String,default: null},
            clientSecret:{type:String,default: null}
        },
        fyMonth:{type:String,default: "April"},
        accountTypes:[{
            name:{type:String},
        }]
    });

    companySchema.pre("save",function(next) {

        if(this.documentCategoryList.length == 0) {
            this.documentCategoryList.push({
                name: "Sales",
                active: true
            },{
                name: "Marketing",
                active: true
            })
        }

        if (this.closeReasons.length == 0){
            this.closeReasons.push({
                name: "Budget"
            },{
                name: "Competition"
            },{
                name: "Relationship"
            },{
                name: "Solution"
            },{
                name: "Time"
            },{
                name: "Others"
            });
        }

        if (this.typeList.length == 0){
            this.typeList.push({
                name: "Renewal",
                isDefaultRenewalType: true,
                isTypeRenewal:true
            },{
                name: "New",
                isTypeRenewal:false
            },{
                name: "Upgrade",
                isTypeRenewal:false
            });
        }

        if (this.customerSetting.length == 0){
            this.customerSetting.push({
                name: "Decision Makers",
                mandatory:false
            },{
                name: "Influencers",
                mandatory:false
            },{
                name: "Partners/Resellers",
                mandatory:false
            });
        }

        if (this.businessUnits.length == 0){
            this.businessUnits.push({
                name: "Security"
            },{
                name: "Networking"
            },{
                name: "Others"
            });
        }

        if (this.sourceList.length == 0){
            this.sourceList.push({
                name: "Web"
            },{
                name: "Organic Search"
            },{
                name: "Call"
            },{
                name: "Email"
            },{
                name: "Referral"
            },{
                name: "Direct"
            },{
                name: "Paid Search"
            },{
                name: "Blog"
            },{
                name: "Social Media"
            },{
                name: "Events"
            },{
                name: "Webinar"
            });
        }

        if (this.geoLocations.length == 0){
            this.geoLocations.push({
                region: "North"
            },{
                region: "East"
            },{
                region: "South"
            },{
                region: "West"
            });
        }

        if (this.currency.length == 0){
            this.currency.push({
                name:"United States Dollar",
                symbol:"USD",
                xr:1,
                isPrimary:true,
                isDeactivated:false
            });
        }

        if (this.opportunityStages.length == 0){
            this.opportunityStages.push({
                name: "Prospecting",
                commitStage:false,
                isWon: false,
                isStart: true,
                isEnd: false,
                isLost: false,
                weight:20,
                order:1
            },{
                name: "Evaluation",
                commitStage:false,
                isStart: false,
                isEnd: false,
                weight:40,
                isWon: false,
                isLost: false,
                order:2
            },{
                name: "Proposal",
                commitStage:true,
                isStart: false,
                isEnd: false,
                weight:80,
                isWon: false,
                isLost: false,
                order:3
            },{
                name: "Close Won",
                commitStage:false,
                isStart: false,
                isEnd: true,
                isWon: true,
                isLost: false,
                weight:100,
                order:4
            },{
                name: "Close Lost",
                commitStage:false,
                isStart: false,
                isEnd: true,
                isWon: false,
                isLost: true,
                weight:100,
                order:5
            });
        }

        next();
    });

    exports.corporateModel = mongoose.model('Corporate', corporateSchema,'corporate');
    exports.companyModel = mongoose.model('Company', companySchema,'company');
    exports.corporatelogoModel = mongoose.model('Corporatelogo', corporatelogoSchema,'corporatelogo');

}).call(this);
