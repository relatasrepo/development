/* Relatas : Email Template Schema*/

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , emailTemplateSchema = new Schema({
        // entityType:{
        //     type:String,
        //     enum:["contact", "company"]
        // },
        tweet: {type:String},
        twitterUserName: {type:String},
        tweetId: {type:String},
        tweetedDate:{type: Date},
        userLocation:{type: String}
    });

    exports.twitterfeed = mongoose.model('emailtemplates', emailTemplateSchema, 'emailtemplates');

}).call(this);
