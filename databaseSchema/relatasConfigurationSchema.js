(function() {
    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , relatasConfigSchema = new Schema({
            latestMobileAppVersion: {type:Number, default:0}
        });
    
    exports.relatasConfig= mongoose.model('relatasConfiguration', relatasConfigSchema, 'relatasConfiguration');

}).call(this);