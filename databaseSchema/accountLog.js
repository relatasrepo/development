(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , accountLogSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        date:{type:Date},
        accountName:{type:String},
        fromEmailId:{type:String},
        updateValue:{type:String},
        updateFor:{type:String},
        type: {enum:['accountAccessRevoked', 'accountAccessGranted', 'accountTransfer']}
    });

    exports.accountsLog = mongoose.model('accountsLogs', accountLogSchema, 'accountsLogs');

}).call(this);
