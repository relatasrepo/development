(function () {
    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , roleSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        roleName:{type:String},
        users:[]
    });

    exports.orgRoles = mongoose.model('orgRoles', roleSchema, 'orgRoles');

}).call(this);
