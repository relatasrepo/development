/*
 Relatas : Tweet Schema
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , contactSchema = new Schema({
        ownerId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        ownerEmailId: {type: String},
        personId: { type:String, default:null},
        personName: { type:String, default:null},
        personEmailId: {
            type: String,
            lowercase: true
        },
        location:{ type:String, default:null},
        lat:{ type:Number, default:null},
        lng:{ type:Number, default:null},
        companyName: { type:String, default:null},
        designation: { type:String, default:null},
        skypeId: { type:String, default:null},
        mobileNumber: { type:String, default:null},
        birthday:{ type:String, default:null},
        source:{ type:String, default:null},
        contactRelation:{
            prospect_customer:{type:String,default:null,enum:[null,'prospect','customer','contact','lead']},
            influencer:{type:Boolean,default:false},
            decision_maker:{type:Boolean,default:false},
            partner:{type:Boolean,default:false},
            cxo:{type:Boolean,default:false}
        },
        stageDate:{
            contact:{type: Date,default:null},
            lead:{type: Date,default:null},
            prospect:{type: Date,default:null},
            customer:{type: Date,default:null}
        },
        relationshipStrength_updated:{type:Number,default:0},
        count: {type:Number,default:0},
        twitterUserName:{ type:String, default:null},
        facebookUserName:{ type:String, default:null},
        linkedinUserName:{ type:String, default:null},
        lastInteracted: {type:Date,default:null},
        addedDate: Date,
        favorite: { type:Boolean, default:false},
        doNotTrackForFavorite: { type:Boolean, default:false},
        verified: { type:Boolean, default:false},
        relatasUser: { type:Boolean, default:false},
        relatasContact: { type:Boolean, default:false},
        notes:[
            {
                createdDate:Date,
                text:{ type:String, default:null}
            }
        ],
        losingTouchRecommendation:{
            date:{type:Date,default:null}
        },
        account:{
            name:{type:String,default:null},
            selected:{type:Boolean,default:false},
            showToReportingManager:{type:Boolean,default:true},
            updatedOn:{type:Date,default:null},
            value:{
                inProcess:{type:Number,default:null},
                won:{type:Number,default:null},
                lost:{type:Number,default:null}
            },
            ignore:{type:Date,default:null},
            twitterUserName:{type:String,default:null},
            watchThisAccount: {type:Boolean,default:false}
        },
        hashtag:[{type:String, default:null}],
        notificationStartTime: Date,
        respLimit: Date,
        respSentTime: Date,
        ignoreContact:{type:String,default:null},
        remindToConnect: {type:Date,default:null},
        watchThisContact: {type:Boolean,default:false},
        contactImageLink:{type:String,default:null},
        isSalesforceContact:{type:Boolean,default:false},
        inactive:{type:Boolean,default:false},
        interactionActivity:{
            received:{type:Number,default:0},
            sent:{type:Number,default:0},
            ownerFeedback:{type:Number,default:0}
        }
    });

    exports.contacts = mongoose.model('contacts', contactSchema, 'contacts');

}).call(this);
