(function () {

    var mongoose = require('../node_modules/mongoose')
        , Schema = mongoose.Schema
        , customerProfileTemplateSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        createdDate:{type:Date},
        modifiedDate:{type:Date},
        customerProfileTemplateName:{type:String, default:null},
        customerProfileTemplateType:{ type:String, default:null},
        customerProfileTemplateUiId:{type:Number, default:1},
        numCustomerProfilesCreated:{ type:Number, default:false},
        isTemplateDeactivated:{ type:Boolean, default:false},
        deactivatedDate:{type:Date},
        isDefaultTemplate:{ type:Boolean, default:false},
        isCustomerProfileCreated:{ type:Boolean, default:false},

        customerProfileTemplateElementList:[
            { type:String, default:null}
        ],

        customerProfileTemplateAttrList:[{
            attributeId:{ type:Number, default:0},
            attributeName:{type:String, default:null, required: true},
            attributeType:{ type:String, default:null},
            attributeValue:{ type:String, default:null},
            isSystemReferenced:{type:Boolean,default:false},
            systemReferencedCollection:{ type:String, default:null},
            systemReferencedAttributeName:{ type:String, default:null},
            isAttributeMandatory:{type:Boolean,default:false},
            isAttributeCustomerVisible:{type:Boolean,default:false},
            isAttributeAdminOnly:{type:Boolean,default:false}
        }],

        customerProfileTemplateTableList:[ {
            tableName:{type:String, required:true},

            tableColumns: [ {
                columnId:{type:Number, default:0},
                columnName:{type:String, default:null},
                columnType:{type:String, default:null},
                columnValue:{ type:String, default:null},
                columnPosition:{type:Number,default:0},
                columnWidth:{type:Number,default:0},
                columnFormula:{type:String, default:null},
                isSystemReferenced:{type:Boolean,default:false},
                systemReferencedCollection:{ type:String, default:null},
                systemReferencedAttributeName:{ type:String, default:null},
                isColumnMandatory:{type:Boolean,default:false},
                isColumnCustomerVisible:{type:Boolean,default:false},
                isColumnAdminOnly:{type:Boolean,default:false}

            } ]
        }]
    });

    exports.customerProfileTemplateModel = mongoose.model('customerProfileTemplate', customerProfileTemplateSchema, 'customerProfileTemplate');

}).call(this);
