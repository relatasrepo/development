/*
 Relatas : User Management Schema
 */

/*
 User Collection Schema
 */
(function () {

    var userSchema, mongoose;
    mongoose = require('../node_modules/mongoose');
    var Schema = mongoose.Schema;
    userSchema = new Schema({
        firstName: { type:String, default:null},
        lastName: { type:String, default:null},
        userType:{
            type:String,
            default:null,
            enum:['registered', 'not-registered','enterprise-demo', 'mobile-app',null]
        },
        corporateUser:{ type:Boolean, default:false},
        resource:{ type:Boolean, default:false},
        companyId:{type: Schema.Types.ObjectId, ref: 'Company', default:null},
        hierarchyParent: {type: Schema.Types.ObjectId, ref: 'User', default:null},
        hierarchyPath:{type:String, default:null},
        corporateAdmin:{type:Boolean, default:false},
        admin:{ type:Boolean, default:false},
        publicProfileUrl: {
            type: String,
            lowercase: true,
            required: true,
            trim: true
        },
        profilePicUrl: { type:String, default:null},
        designation: { type:String, default:null},
        registeredUser: Boolean,
        companyName: {type:String, default:null,trim: true},
        linkedinId: { type:String, default:null},
        googleId: { type:String, default:null},
        emailId: {
            type: String,
            lowercase: true,
            required: true,
            trim: true,
            unique:true
        },
        mobileNumber: { type:String, default:null},
        password: { type:String, default:null},
        skypeId: { type:String, default:null},
        location: { type:String, default:null},
        locationLatLang: { // global map location
            latitude: { type:String, default:null},
            longitude: { type:String, default:null}
        },
        timezone: {
            name: { type:String, default:null},
            zone: { type:String, default:null},
            updated: { type:Boolean, default:false}
        },
        birthday: {
            day: Number,
            month: Number,
            year: Number
        },
        linkedin: {
            id: { type:String, default:null},
            token: { type:String, default:null},
            emailId: { type:String, default:null},
            name: { type:String, default:null},
            lastSync: { type:Date, default:null}
        },
        google: [{
            id: { type:String, default:null},
            token: { type:String, default:null},
            refreshToken: { type:String, default:null},
            emailId: { type:String, default:null},
            name: { type:String, default:null},
            addedOn:Date,
            updatedOn:Date
        }],
        outlook: [{
            id: { type:String, default:null},
            token: { type:String, default:null},
            refreshToken: { type:String, default:null},
            emailId: { type:String, default:null},
            name: { type:String, default:null},
            addedOn:Date,
            isPrimary:{type:Boolean,default:true}
        }],
        facebook: {
            id: { type:String, default:null},
            token: { type:String, default:null},
            emailId: { type:String, default:null},
            name: { type:String, default:null},
            lastSync: { type:Date, default:null}
        },
        twitter: {
            id: { type:String, default:null},
            token: { type:String, default:null},
            refreshToken: { type:String, default:null},
            displayName: { type:String, default:null},
            userName: { type:String, default:null},
            sinceId: { type:String, default:null},
            lastSync: { type:Date, default:null}
        },
        salesforce:{
            id: { type:String, default:null},
            instanceUrl: { type:String, default:null},
            refreshToken: { type:String, default:null},
            userInfoUrl:{type:String, default:null},
            emailId:{type:String, default:null},
            lastSync: { type:Date, default:null}
        },
        contacts: [
            {
                personId: { type:String, default:null},
                personName: { type:String, default:null},
                personEmailId: {
                    type: String,
                    lowercase: true
                },
                location:{ type:String, default:null},
                lat:{ type:Number, default:null},
                lng:{ type:Number, default:null},
                companyName: { type:String, default:null},
                designation: { type:String, default:null},
                skypeId: { type:String, default:null},
                mobileNumber: { type:String, default:null},
                birthday:{ type:String, default:null},
                source:{ type:String, default:null},
                contactRelation:{
                    prospect_customer:{type:String,default:null,enum:[null,'prospect','customer','contact','lead']},
                    influencer:{type:Boolean,default:false},
                    decision_maker:{type:Boolean,default:false},
                    cxo:{type:Boolean,default:false},
                    partner:{type:Boolean,default:false}
                },
                stageDate:{
                    contact:{type: Date,default:null},
                    lead:{type: Date,default:null},
                    prospect:{type: Date,default:null},
                    customer:{type: Date,default:null}
                },
                relationshipStrength_updated:{type:Number,default:0},
                count: {type:Number,default:0},
                twitterUserName:{ type:String, default:null},
                facebookUserName:{ type:String, default:null},
                linkedinUserName:{ type:String, default:null},
                lastInteracted: {type:Date,default:null},
                addedDate: Date,
                favorite: { type:Boolean, default:false},
                doNotTrackForFavorite: { type:Boolean, default:false},
                verified: { type:Boolean, default:false},
                relatasUser: { type:Boolean, default:false},
                relatasContact: { type:Boolean, default:false},
                notes:[
                    {
                        createdDate:Date,
                        text:{ type:String, default:null}
                    }
                ],
                losingTouchRecommendation:{
                    date:{type:Date,default:null}
                },
                account:{
                    name:{type:String,default:null},
                    selected:{type:Boolean,default:false},
                    showToReportingManager:{type:Boolean,default:true},
                    updatedOn:{type:Date,default:null},
                    value:{
                        inProcess:{type:Number,default:null},
                        won:{type:Number,default:null},
                        lost:{type:Number,default:null}
                    },
                    ignore:{type:Date,default:null},
                    twitterUserName:{type:String,default:null},
                    watchThisAccount: {type:Boolean,default:false}
                },
                hashtag:[{type:String, default:null}],
                notificationStartTime: Date,
                respLimit: Date,
                respSentTime: Date,
                ignoreContact:{type:String,default:null},
                remindToConnect: {type:Date,default:null},
                watchThisContact: {type:Boolean,default:false},
                contactImageLink:{type:String,default:null},
                isSalesforceContact:{type:Boolean,default:false},
                inactive:{type:Boolean,default:false}
            }
        ],
        officeCalendar:{
            emailId:{ type:String, default:null},
            token_type: { type:String, default:null},
            expires_in: { type:String, default:null},
            expires_on: { type:String, default:null},
            not_before: { type:String, default:null},
            resource: { type:String, default:null},
            access_token: { type:String, default:null},
            refresh_token: { type:String, default:null},
            refresh_token_expires_in: { type:String, default:null},
            scope: { type:String, default:null},
            id_token: { type:String, default:null},
            addedDate:Date,
            updatedDate:Date
        },
        currentLocation: { // location taken while user signup
            country: { type:String, default:null},
            city: { type:String, default:null},
            region: { type:String, default:null},
            latitude: Number,
            longitude: Number
        },
        calendarAccess: {
            calendarType: { type:String, default:null},
            timeOne: {
                start: { type:String, default:null},
                end: { type:String, default:null}
            },
            timeTwo: {
                start: { type:String, default:null},
                end: { type:String, default:null}
            }
        },
        workHours: {
            start: { type:String, default:null},
            end: { type:String, default:null}
        },
        workHoursWeek:{
            weekdayHours:{
                days:[{type: Number,enum:[0,1,2,3,4,5,6],default:[0,1,2,3,4,5,6]}],
                start: { type:String, default:"9"}, // 0 - 24 (0 = 12AM, 24 = 11:59PM)
                end: { type:String, default:"17"} // 0 - 24 (0 = 12AM, 24 = 11:59PM)
            },
            weekendHours:{
                days:[{type: Number,enum:[0,1,2,3,4,5,6],default:[0,1,2,3,4,5,6]}],
                start: { type:String, default:"9"}, // 0 - 24 (0 = 12AM, 24 = 11:59PM)
                end: { type:String, default:"17"} // 0 - 24 (0 = 12AM, 24 = 11:59PM)
            }
        },
        notification:{
            notificationOn:{type:String,default:'every-month',enum:['never','every-month','every-week']}
        },
        mobileFirebaseToken: {type: String , default: null},
        webFirebaseSettings: {
            firebaseToken: {type: String , default: null},
            notificationEnabled: {type: Boolean, default: false}
        },
        notificationSettings: {
            documentView: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
            commitPending: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
            tasksForToday: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
            losingTouch: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
            oppClosingToday: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
            dealsAtRiskForUser: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
            dealsAtRiskForManager: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
            oppClosingThisWeek: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
            weeklyCommit: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
            targetWonLostManager: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
            documentSharingStatus: {
                web: {type: Boolean, default: true},
                mobile: {type: Boolean, default: true}
            },
        },
        profileDescription: { type:String, default:null},
        profilePrivatePassword: { type:String, default:null},
        resetPasswordToken: { type:String, default:null},
        serviceLogin: { type:String, default:null},
        sendDailyAgenda: { type:Boolean, default:true},
        profileDeactivated: { type:Boolean, default:false},
        createdDate: Date,
        registeredDate: Date,
        partialFill: Boolean,
        identityShare: Boolean,
        tokenReset: Boolean,
        firstLogin:Boolean,
        zoneNumber: {type:Number, default:2},
        lastLoginDate:Date,
        penultimateLoginDate:Date,
        lastAgendaSentDate:Date,
        lastDAMSentDate:{type:Date,default:null},
        dashboardPopup:{ type:Boolean, default:false},
        isMobileNumberVerified:{ type:Boolean, default:false},
        openActionItemAfterCall:{ type:Boolean, default:false},
        mobileNumberOTP:{ type:String, default:null},
        widgetKey:{
            key:{ type:String, default:null},
            secrete:{ type:String, default:null}
        },
        isFavoriteUpdated:{type:Boolean,default:false},
        lastAccountUpdated:{type:Date},
        contactsUpload:{
            lastUploaded:{type:Date},
            isUploaded:{type:Boolean,default:false}
        },
        isMobileUser : {type: Date ,default:null}, // the first time
        insightsRemindIn : {type:String,default:null},
        lastMobileSyncDate:{type:Date,default:null},
        lastDataSyncDate:{type:Date,default:null},
        lastOppInteractionsSyncDate:{type:Date,default:null},
        mobileAppVersion:{type:String,default:null},
        mobileOnBoardingDate:{type:Date,default:null},
        mobileLastLoginDate:{type:Date,default:null},
        mobileNumberWithoutCC:{type:String,default:null}, //mobile number without country code.
        countryCode:{type:String,default:null},
        lastDataSync:{type:Date,default:null},
        emailSentOutFrequencyUpdatedOn:{type:Date,default:null},
        insightsBuilt:{type:Boolean,default: false},
        orgHead:{type:Boolean,default: false},
        companyTargetAccess:{type:Boolean,default: false},
        verticalOwner:[],
        productTypeOwner:[],
        regionOwner:[],
        businessUnits:[],
        teamListOwner:[],
        emailSentOutFrequency:[
            {
                hours24: {type: String, default: null},
                meridiem: {type: String, default: null},
                frequency: {type: Number, default: 0}
            }
        ],
        actionableMailSetting:{type: String, default: "all"},
        dashboardSetting:{
            responsePending:{type:Boolean,default: false},
            followUp:{type:Boolean,default: false},
            meetingFollowUp:{type:Boolean,default: false},
            remindToConnect:{type:Boolean,default: false},
            travellingToLocation:{type:Boolean,default: false},
            losingTouch:{type:Boolean,default: false}
        },
        matrixAccess: [{
            type:{type: String, default: null},
            list:[]
        }]
    });

    userSchema.pre("save",function(next) {
        if (this.workHoursWeek.weekdayHours.days.length == 0){
            this.workHoursWeek.weekdayHours.days.push(0,1,2,3,4,5,6);
        }

        if (this.workHoursWeek.weekendHours.days.length == 0){
            this.workHoursWeek.weekendHours.days.push(0,1,2,3,4,5,6);
        }

        next();
    });

    exports.User = mongoose.model('User', userSchema, 'user');

}).call(this);

/*
 Meeting Collection Schema
 */
(function () {

    var meetingScheduleSchema, mongoose;
    mongoose = require('../node_modules/mongoose');
    var calendarV2=require('./calendarSchema');
    var calendarList=require('./calendarListSchema');
    meetingScheduleSchema = new mongoose.Schema({
        invitationId: { type:String, default:null},
        suggested: Boolean,
        senderId: { type:String, default:null},
        senderName: { type:String, default:null},
        senderEmailId: { type:String, default:null, required: true, trim: true},
        senderPicUrl: { type:String, default:null},
        isSenderPrepared:Boolean,
        readStatus: Boolean,
        scheduledDate: Date,
        googleEventId: { type:String, default:null},
        officeEventId: { type:String, default:null},
        iCalUID:{ type:String, default:null},
        updateCount: Number,
        lastActionUserId: { type:String, default:null},
        to: {
            receiverId: { type:String, default:null},
            receiverName: { type:String, default:null},
            receiverEmailId: { type:String, default:null},
            canceled:Boolean,
            canceledDate: Date,
            isPrepared:Boolean
        },
        suggestedBy: {
            userId: { type:String, default:null},
            suggestedDate: Date
        },
        selfCalendar: {type:Boolean, default:false},
        isGoogleMeeting: {type:Boolean, default:false},
        isOfficeOutlookMeeting: {type:Boolean, default:false},
        toList: [
            {
                receiverId: { type:String, default:null},
                receiverFirstName: { type:String, default:null},
                receiverLastName: { type:String, default:null},
                receiverEmailId: { type:String, default:null},
                isAccepted: Boolean,
                canceled:Boolean,
                canceledDate: Date,
                acceptedDate: Date,
                isPrepared:Boolean,
                isResource:{type:Boolean, default:false},
                isPrimary:{type:Boolean, default:false}
            }
        ],
        docs: [
            {
                documentId: { type:String, default:null},
                documentName: { type:String, default:null},
                documentUrl: { type:String, default:null},
                addedBy:{type: mongoose.Schema.Types.ObjectId,ref: 'User',default:null},
                addedOn: Date
            }
        ],
        icsFile: {
            awsKey: { type:String, default:null},
            url: { type:String, default:null}
        },
        scheduleTimeSlots: [
            {
                isAccepted: Boolean,
                acceptedDate: Date,
                messages: [{
                    messageDate: Date,
                    message: { type:String, default:null},
                    userId: { type:String, default:null}
                }],
                start: {
                    date: Date
                },
                end: {
                    date: Date
                },
                title: { type:String, default:null},
                location: { type:String, default:null},
                suggestedLocation: { type:String, default:null},
                locationType: { type:String, default:null},
                description: { type:String, default:null}
            }
        ],
        participants: [
            {
                emailId: String
            }
        ],
        comments:[
            {
                messageDate: Date,
                message: { type:String, default:null},
                userId: { type:String, default:null}
            }
        ],
        meetingLocation: {
            country: { type:String, default:null},
            city: { type:String, default:null},
            region: { type:String, default:null},
            latitude: Number,
            longitude: Number
        },

        upLoadedMeetingLocation: {
            country: { type:String, default:null},
            city: { type:String, default:null},
            region: { type:String, default:null},
            latitude: Number,
            longitude: Number
        },

        deleted: Boolean,
        deletedDate:Date,
        lastModified: Date,
        toDo:[
            {
                actionItem:{ type:String, default:null},
                assignedTo:{ type:String, default:null},
                createdBy:{ type:String, default:null},
                createdDate:Date,
                googleEventId:{ type:String, default:null},
                status:{type:String,default:'notStarted'},
                dueDate:Date,
                updatedDate:Date,
                updateCount:{type:Number,default:1}
            }
        ],
        tasks:[{type: mongoose.Schema.Types.ObjectId,ref: 'tasks'}],
        recurrenceId:{ type:String, default:null},
        LIUoutlookEmailId:{type:String, default:null},
        actionItemSlot:{type:Boolean, default:false},
        actionItemSlotType:{type:String, default:null},
        partialMeeting:{type:Boolean, default:false} //true if the non relatas user sets up meeting
    });

    exports.scheduleInvitation = mongoose.model('scheduleInvitation', meetingScheduleSchema, 'meeting');

}).call(this);

/*
 Document Collection Schema
 */
(function () {
    var documentSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    documentSchema = new mongoose.Schema({
        sharedBy: {
            userId: String,
            companyId: String,
            companyName: String,
            companyLogoUrl: String,
            emailId: String,
            firstName: String,
            lastName: String,
            profilePicUrl: String
        },
        sharedWith: [
            {
                accessed:{type:Boolean, default:false},
                userId: String,
                emailId: String,
                firstName: String,
                lastName: String,
                subject: String,
                message: String,
                accessStatus: {type:Boolean, default:true},
                accessCount: {type:Number, default:0},
                linkAccessCount: {type:Number, default:0},
                firstAccessed: Date,
                lastAccessed: Date,
                sharedOn: Date,
                expiryDate: Date,
                documentPassword: String,
                expirySec: Number,
                accessInfo: {
                    pagesRead: Number,
                    pageReadTimings: [
                        {
                            pageNumber: Number,
                            Time: Number,
                            lastAccessed: Date
                        }
                    ]
                }
            }
        ],
        awsKey: String,
        documentName: String,
        documentCategory: String,
        documentUrl: String,
        access: String,
        sharedDate: Date,
        updatedDate: Date,
        isDeleted: {type:Boolean, default:false}
    });
    exports.documents = mongoose.model('documents', documentSchema, 'documents');

}).call(this);

/*
 Mass mail count Collection Schema
 */
(function () {
    var massMailCountSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    massMailCountSchema = new mongoose.Schema({

        userId: String,
        count: Number,
        date: Date,
        createdDate: Date

    });
    exports.massMailCount = mongoose.model('massMailCount', massMailCountSchema, 'massMailCount');

}).call(this);

/*
 Message Collection Schema
 */
(function () {
    var messageSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    messageSchema = new mongoose.Schema({

        senderId: String,
        receiverId: String,
        receiverEmailId: String,
        subject: String,
        message: String,
        headerImage: Boolean,
        image: {
            url: String,
            uploadedOn: Date,
            name: String
        },
        type: String,
        sentOn: Date,
        readStatus:Boolean,
        redOn:Date

    });
    exports.message = mongoose.model('message', messageSchema, 'message');

}).call(this);

/*
 Calendar Password Collection Schema
 */
(function () {
    var calendarPasswordManagementSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    calendarPasswordManagementSchema = new mongoose.Schema({

        readStatus: {type:Boolean, default:false},
        isAccepted: {type:Boolean, default:false},
        requestDate: {type:Date, default:null},
        actionDate: {type:Date, default:null},
        requestFrom: {
            userId: {type:String, default:null},
            emailId:{type:String, default:null}
        },
        requestTo: {
            userId: {type:String, default:null},
            emailId:{type:String, default:null}
        }

    });
    exports.calendarPasswordManagement = mongoose.model('calendarPasswordManagement', calendarPasswordManagementSchema, 'calendarPasswordManagement');

}).call(this);

/*
 Events Collection Schema
 */
(function () {
    var eventSchema, mongoose;
    mongoose = mongoose = require('../node_modules/mongoose');

    eventSchema = new mongoose.Schema({
        eventName: String,
        locationName: String,
        eventLocation: String,
        eventLocationUrl: String,
        eventDescription: String,
        startDateTime: Date,
        endDateTime: Date,
        eventCategory: String,
        accessType: String,
        tags: String,
        eventMap: String,
        createdDate: Date,
        lastUpdated: Date,
        speakerName: String,
        speakerDesignation: String,
        createdBy: {
            userId: String,
            userEmailId: String,
            userName: String
        },
        image: {
            imageUrl: String,
            imageName: String,
            awsKey: String,
            addedOn: Date
        },
        docs: [
            {
                documentId: String,
                documentName: String,
                documentUrl: String,
                addedOn: Date
            }
        ]
    });
    exports.event = mongoose.model('event', eventSchema, 'event');

}).call(this);

/*
 Interactions Collection Schema
 */

(function () {
  var userInteractionsSchema, mongoose;
  mongoose = require('../node_modules/mongoose');

  userInteractionsSchema = new mongoose.Schema(
    {
      ownerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
      },
      ownerEmailId: {type: String},
          userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            default: null
          },
          emailId: {
            type: String,
            default: null
          },
          firstName: {type: String, default: null},
          lastName: {type: String, default: null},
          companyName: {type: String, default: null},
          designation: {type: String, default: null},
          profilePicUrl: {type: String, default: null},
          publicProfileUrl: {type: String, default: null},
          location: {type: String, default: null},
          interactionDate: {type: Date, default: null},
          mobileNumber: {type: String, default: null},
          endDate: {type: Date, default: null},
          action: {
            type: String,
            enum:['sender', 'receiver'],
            default: null
          },
          interactionType: {type: String, default: null},
          subType: {type: String, default: null},
          ignore:{type:Boolean, default:false},
          insightsIgnore:{
              responsePending:{type:Boolean, default:false}
          },
          refId: {
            type: String,
            required: true
          },
          parentRefId: {type: String, default: null},
          createdDate: {type: Date, default: null},
          source: {type: String, default: null},
          title: {type: String, default: null},
          postURL: {type: String, default: null},
          description: {type: String, default: null},
          trackId: {type: String, default: null},
          trackInfo: {
            action:{type:String,default:null,enum:['request','confirmed','re scheduled']},
            lastOpenedOn: {type: Date, default: null},
            status:{type:Boolean,default:false}, // for task
            isRed:{type:Boolean, default:false}, // Emails sent from relatas with track open and red
            trackOpen:{type:Boolean,default:false}, // Emails sent from relatas with track open
            trackResponse:{type:Boolean,default:false}, // Emails sent from relatas with track response
            gotResponse:{type:Boolean,default:false}, // Emails sent from relatas with track response and got response
            trackDocument:{type:Boolean,default:false}, // Emails sent from relatas with track doc open
              emailOpens:{type:Number,default:0} // Number of times email opened
          },
          user_twitter_id: {type: String, default: null},
          user_twitter_name: {type: String, default: null},
          user_facebook_id: {type: String, default: null},
          emailContentId:{type:String,default:null}, // Actual id of email
          googleAccountEmailId:{type:String,default:null},
          toCcBcc:{type:String,default:null},
          emailThreadId:{type:String,default:null}, // thread id of email - content id and thread id might be same if email thread have single mail
        futureEvents:{
            dates:[{type: Date, default: null}],
            suggestions:[{type: String, default: null}]
        },
        to:[{emailId: { type:String, default:null}}],
        cc:[{emailId: { type:String, default:null}}],
        eActionTaken:{type: Boolean, default: false},
        summary:{type: String, default: null},
        importance:{type: Number, default: 0},
        duration:{type: Number, default: 0},
        actions:{type: String, default: null},
        sentiment:{type: String, default: null},
        emailClassified:{type: Boolean, default: false},
        hasUnsubscribe:{type: Boolean, default: false},
        notImportant:{type: Boolean, default: false},
        companyId:{
            type:mongoose.Schema.Types.ObjectId,
            ref: 'Company',
            default: null
        }
    });
  exports.interactions = mongoose.model('interactions', userInteractionsSchema, 'interactions');

}).call(this);

/*
 Enterprice Demo User Collection Schema
 */
(function () {
    var enterPriseDemoUserSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    enterPriseDemoUserSchema = new mongoose.Schema(
        {
            fullName:String,
            emailId:String,
            companyName:String,
            phoneNumber:String,
            skypeId:String,
            createdDate:Date,
            type:String

        });
    exports.enterpriseSignUp = mongoose.model('enterpriseSignUp', enterPriseDemoUserSchema, 'enterpriseSignUp');

}).call(this);

/*
 Mobile App Launch Collection Schema
 */
(function () {
    var mobileAppLaunchSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    mobileAppLaunchSchema = new mongoose.Schema(
        {
            emailId:String,
            iosApp:Boolean,
            androidApp:Boolean,
            windowsApp:Boolean,
            createdDate:Date

        });
    exports.mobileAppLaunch = mongoose.model('mobileAppLaunch', mobileAppLaunchSchema, 'mobileAppLaunch');

}).call(this);

/*
 Relationship Cartoon Collection Schema
 */

(function () {
    var relationshipCartoonSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    relationshipCartoonSchema = new mongoose.Schema(
        {
            email:String,
            name:String,
            gender:String,
            emailNumbers:String,
            callNumbers:String,
            meetingNumbers:String

        });
    exports.relationshipCartoon = mongoose.model('relationshipCartoon', relationshipCartoonSchema, 'relationshipCartoon');

}).call(this);

/*
 Subscribe To Events Collection Schema
 */
(function () {
    var subscribeToEventsSchema, mongoose;
    mongoose = mongoose = require('../node_modules/mongoose');

    subscribeToEventsSchema = new mongoose.Schema({
        userId: String,
        createdDate: Date,
        lastUpdated: Date,
        eventTypes: [
            {
                eventType: String,
                status: Boolean
            }
        ],
        locations: [
            {
                location: String,
                status: Boolean
            }
        ],
        eventsList: [
            {
                eventId: String,
                googleEvent: Boolean,
                googleEventId: String,
                isDeleted: Boolean,
                deletedDate:Date,
                sequenceCount: Number,
                confirmed: Boolean,
                confirmDate: Date
            }
        ]
    });
    exports.subscribeToEvents = mongoose.model('subscribeToEvents', subscribeToEventsSchema, 'subscribeToEvents');

}).call(this);

/*
Admin Collection Schema
 */

(function () {
   var adminSchema, mongoose;
   mongoose = require('../node_modules/mongoose');

   adminSchema = new mongoose.Schema({
       userId:String,
       expire:Date,
       token:String
   });

    adminSchema.index({ expire: 1}, { expireAfterSeconds: 60 });
    exports.adminCollection = mongoose.model('adminCollection', adminSchema, 'adminCollection');

}).call(this);


/*
 User Login Details Collection Schema
 */
(function () {
    var userLogSchema, mongoose;
     mongoose = require('../node_modules/mongoose');

    userLogSchema = new mongoose.Schema({
        userId: String,
        emailId:String,
        loginDate:Date,
        loginPage:String,
        userIp:String,
        adminActionTaken:String,
        userAgent:{
            browser: String,
            version: String,
            operatingSystem: String,
            platform: String,
            source:String,
            isMobile:Boolean,
            isTablet: Boolean,
            isIPad: Boolean,
            isIPod: Boolean,
            isIPhone: Boolean,
            isAndroid: Boolean,
            isBlackberry: Boolean,
            isOpera: Boolean,
            isIE: Boolean,
            isIECompatibilityMode: Boolean,
            isSafari: Boolean,
            isFirefox: Boolean,
            isWebkit: Boolean,
            isChrome: Boolean,
            isKonqueror: Boolean,
            isOmniWeb: Boolean,
            isSeaMonkey: Boolean,
            isFlock: Boolean,
            isAmaya: Boolean,
            isEpiphany: Boolean,
            isDesktop: Boolean,
            isWindows: Boolean,
            isLinux: Boolean,
            isLinux64: Boolean,
            isMac: Boolean,
            isBada: Boolean,
            isSamsung: Boolean,
            isRaspberry: Boolean,
            isBot: Boolean,
            isCurl: Boolean,
            isAndroidTablet: Boolean,
            isWinJs: Boolean,
            isKindleFire: Boolean,
            isSilk: Boolean,
            SilkAccelerated: Boolean
        }
    });
    exports.userlog = mongoose.model('userlog', userLogSchema, 'userlog');

}).call(this);

/*
 Relationship strength Collection Schema
 */
(function () {
    var relationshipStrength, mongoose;
    mongoose = require('../node_modules/mongoose');

    relationshipStrength = new mongoose.Schema(
        {
            minRatio:Number,
            maxRatio:Number

        });
    exports.relationshipStrength = mongoose.model('relationshipStrength', relationshipStrength, 'relationshipStrength');

}).call(this);

(function () {

    var facebookStatusSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    facebookStatusSchema = new mongoose.Schema({
        _id: String,
        userId: String,
        userEmailId: String,
        userFirstName: String,
        dateModified: Date,
        facebook: [
            {
                id: String,
                story: String,
                picture: String,
                link: String,
                name: String,
                caption: String,
                icon: String,
                type: String,
                status_type: String,
                object_id: String,
                created_time: String,
                updated_time: String,
                is_hidden: Boolean,
                message: String,
                message_tags: Object,
                source: String,
                from: {
                    id: String,
                    name: String
                },
                properties: [
                    {
                        name: String,
                        text: String,
                        href: String
                    }
                ],
                actions: [
                    {
                        name: String,
                        link: String
                    }
                ],
                privacy: {
                    description: String,
                    value: String,
                    friends: String,
                    networks: String,
                    allow: String,
                    deny: String
                },
                call_to_action: {
                    context: Object
                },
                application: {
                    name: String,
                    id: String
                },
                comments: {
                    data: [
                        {
                            id: String,
                            from: {
                                id: String,
                                name: String
                            },
                            message: String,
                            can_remove: Boolean,
                            created_time: String,
                            like_count: Number,
                            user_likes: Boolean
                        }
                    ],
                    paging: {
                        cursors: {
                            after: String,
                            before: String
                        }
                    }
                }
            }
        ]
    });

    exports.facebookStatus = mongoose.model('facebookStatus', facebookStatusSchema, 'facebook');
}).call(this);

(function () {
    var twitterStatusSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    twitterStatusSchema = new mongoose.Schema({
        _id: String,
        userId: String,
        userEmailId: String,
        userFirstName: String,
        dateModified: Date,
        twitter: [
            {
                created_at: String,
                id: Number,
                id_str: String,
                text: String,
                source: String,
                truncated: Boolean,
                in_reply_to_status_id: Number,
                in_reply_to_status_id_str: Number,
                in_reply_to_user_id: Number,
                in_reply_to_user_id_str: String,
                in_reply_to_screen_name: String,
                retweet_count: Number,
                favorite_count: Number,
                possibly_sensitive: Boolean,
                withheld_copyright: Boolean,
                withheld_in_countries: [{type: String}],
                withheld_scope: String,
                favorited: Boolean,
                retweeted: Boolean,
                lang: String,
                geo: {
                    coordinates: [{type: Number}],
                    type: String
                },
                coordinates: {
                    coordinates: [{type: Number}],
                    type: String
                },
                contributors: {
                    id: Number,
                    id_str: String,
                    screen_name: String
                },
                scopes: {
                    followers: Boolean
                },
                place: {
                    country: String,
                    country_code: String,
                    full_name: String,
                    id: String,
                    name: String,
                    place_type: String,
                    url: String,
                    polylines: [],
                    geometry: {
                        coordinates: [],
                        type: String
                    },
                    attributes: {
                        street_address: String,
                        locality: String,
                        region: String,
                        iso3: String,
                        postal_code: String,
                        phone: String,
                        url: String,
                        "app:id": String,
                        twitter: String
                    },
                    bounding_box: {
                        coordinates: [
                            [
                                [{type: Number}],
                                [{type: Number}],
                                [{type: Number}],
                                [{type: Number}]
                            ]
                        ],
                        type: String
                    }
                },
                user: {
                    id: Number,
                    id_str: String,
                    name: String,
                    screen_name: String,
                    location: String,
                    description: String,
                    url: String,
                    protected: Boolean,
                    followers_count: Number,
                    friends_count: Number,
                    listed_count: Number,
                    created_at: String,
                    favourites_count: Number,
                    utc_offset: Number,
                    time_zone: String,
                    geo_enabled: Boolean,
                    verified: Boolean,
                    statuses_count: Number,
                    lang: String,
                    contributors_enabled: Boolean,
                    is_translator: Boolean,
                    is_translation_enabled: Boolean,
                    profile_background_color: String,
                    profile_background_image_url: String,
                    profile_background_image_url_https: String,
                    profile_background_tile: Boolean,
                    profile_image_url: String,
                    profile_image_url_https: String,
                    profile_link_color: String,
                    profile_sidebar_border_color: String,
                    profile_sidebar_fill_color: String,
                    profile_text_color: String,
                    profile_use_background_image: Boolean,
                    default_profile: Boolean,
                    default_profile_image: Boolean,
                    following: Boolean,
                    follow_request_sent: Boolean,
                    notifications: Boolean

                }
            }
        ]
    });

    exports.twitterStatus = mongoose.model('twitterStatus', twitterStatusSchema, 'twitter');
}).call(this);

// Mongo DB schema for user social info (to store latest statuses from Linkedin)
(function () {

    var linkedinStatusSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    linkedinStatusSchema = new mongoose.Schema({
        _id: String,
        userId: String,
        userEmailId: String,
        userFirstName: String,
        dateModified: Date,
        linkedin: [
            {
                isCommentable: Boolean,
                isLikable: Boolean,
                isLiked: Boolean,
                numLikes: Number,
                timestamp: Number,
                updateComments: {
                    _total: Number
                },
                updateContent: {
                    person: {
                        apiStandardProfileRequest: {
                            headers: {
                                _total: Number,
                                values: [{
                                    name: String,
                                    value: String
                                }]
                            },
                            url: String
                        },
                        currentShare: {
                            author: {
                                firstName: String,
                                id: String,
                                lastName: String
                            },
                            comment: String,
                            content: {
                                description: String,
                                eyebrowUrl: String,
                                shortenedUrl: String,
                                submittedImageUrl: String,
                                submittedUrl: String,
                                thumbnailUrl: String,
                                title: String
                            },
                            id: String,
                            source: {
                                serviceProvider: {
                                    name: String
                                },
                                serviceProviderShareId: String
                            },
                            timestamp: Number,
                            visibility: {
                                code: String
                            }
                        },
                        firstName: String,
                        headline: String,
                        id: String,
                        lastName: String,
                        pictureUrl: String,
                        siteStandardProfileRequest: {
                            url: String
                        }
                    }
                },
                updateKey: String,
                updateType: String
            }
        ]
    });

    exports.linkedinStatus = mongoose.model('linkedinStatus', linkedinStatusSchema, 'linkedin');

}).call(this);

//App Configuration Schema. This schema stores all the special features the user/company has signed up for.

(function () {
    var appConfigurationSchema, mongoose;

    mongoose = require('../node_modules/mongoose');
    var Schema = mongoose.Schema;

    appConfigurationSchema = new Schema(
        {
            entityId:{type: Schema.Types.ObjectId},
            feature: []
        });
    exports.appConfiguration = mongoose.model('appConfiguration', appConfigurationSchema, 'appConfiguration');

}).call(this);

//Algo Seed Values Schema. This schema stores all the algorithms and values for insights.

(function () {
    var algoSeedValueSchema, mongoose;

    mongoose = require('../node_modules/mongoose');
    var Schema = mongoose.Schema;

    algoSeedValueSchema = new Schema(
        {
            documentName: { type:String, default:null}
        }, {strict: false});
    exports.algoSeedValues = mongoose.model('algoSeedValues', algoSeedValueSchema, 'algoSeedValues');

}).call(this);

/*
losing touch collection schema
*/

(function () {
    var losingTouchSchema, mongoose;
     mongoose = require('../node_modules/mongoose');

    losingTouchSchema = new mongoose.Schema({
        userEmailId: { type:String, default:null},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User', unique:true, required: true},
        lastUpdatedDate: Date,
        historicalData:[{
            date:{type: Date, default: null},
            count:{type: Number, default: 0}
        }],
        contacts: [
            {
                personEmailId: { type:String, default:null},
                mobileNumber: { type:String, default:null},
                score: { type:Number, default:0 },
                actionTaken: { type:Boolean, default:false }
            }
        ]
    });
    exports.losingTouch = mongoose.model('losingTouch', losingTouchSchema, 'losingTouch');

}).call(this);

/*
insights action taken collection schema
*/// $http.post("/messages/send/email/single/web", obj)
            //     .success(function(response) {
            //         if (response.SuccessCode) {
            //             $("#send-email-but").removeClass("disabled");
            //             $(".send-email-but").removeClass("disabled");
            //             $scope.resetFields();
            //             $(".compose-email-inline").slideToggle(200);
            //             toastr.success(response.Message);
            //         } else {
            //             $("#send-email-but").addClass("disabled")
            //             toastr.error(response.Message);
            //         }
            //     });

(function () {
    var insightsActionTakenSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    insightsActionTakenSchema = new mongoose.Schema({
        userId: String,
        userEmailId: String,
        createdDate: Date,
        contact:
        {
            userId: String,
            name: String,
            personEmailId: String,
            mobileNumber: String
        },
        actionType: String,
        suggestion: String,
        insightSource: String,
        suggestionIndexValue: Number,
        self_hierarchy: String
    });
    exports.insightsActionTaken = mongoose.model('insightsActionTaken', insightsActionTakenSchema, 'insightsActionTaken');

}).call(this);

/*Invalid email list*/
(function () {
    var invalidEmailListSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    invalidEmailListSchema = new mongoose.Schema({
        type:String,
        invalid:[],
        exception:[]
    });
    exports.invalidEmailList = mongoose.model('invalidEmailList', invalidEmailListSchema, 'invalidEmailList');
}).call(this);

/*User Events Track on Relatas*/

(function () {
    var actionItemsSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    actionItemsSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
        userEmailId: {type: String, default: null},
        itemCreatedDate: {type: Date, default: null},
        // personId: {type: String, default: null},
        // personName: {type: String, default: null},
        // personEmailId: {type: String, default: null},
        // personMobileNumber: {type: String, default: null},
        actionTakenSource: {type: String, default: null},
        actionTaken: {type: Boolean, default: null},
        actionTakenType: {type: String, default: null},
        actionTakenDate: {type: Date, default: null},
        itemType: {
            type: String,
            default: null,
            enum:['meeting','mailFollowUp','mailResponsePending','remindToConnect','tweets','news','MLanalysed','travellingToLocation','peopleAtMeetingLocation','losingTouch']
        },
        documentRecordFrom:{type: String, default: null},
        documentRecordId:{type: mongoose.Schema.Types.ObjectId, required: true},
        documentRecordIdString:{type: String,default:null},
        meetingId:{type: String},
        emailThreadId: {type: String, default: null}, // using only for itemType - mailResponsePending
        eAction:{type: String, default: null},
        eActionTaken:{type: Boolean, default: null},
        summary:{type: String, default: null},
        importance:{type: Boolean, default: null},
        action:[],
        sentiment:[] //using for important mails as recode may consists both response pending and important mail
    });

    exports.actionItems = mongoose.model('actionItems', actionItemsSchema, 'actionItems');
}).call(this);

(function () {
    var opportunitiesSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    opportunitiesSchema = new mongoose.Schema({
        companyId: {type: mongoose.Schema.Types.ObjectId, required: true},
        userId: {type: mongoose.Schema.Types.ObjectId, required: true},
        userEmailId: { type:String, default:null},
        createdByEmailId: { type:String, default:null},
        contactEmailId: { type:String, default:null},
        salesforceContactId: { type:String, default:null},
        opportunityId: {type: String, unique:true},
        sourceOpportunityId: {type: String},
        opportunityName: { type:String, default:null},
        createdDate: {type: Date, default: null},
        closeDate: {type: Date, default: null},
        amount: { type:Number, default:0 },
        netGrossMargin: { type:Number, default:100 },
        isClosed: {type:Boolean, default:false },
        isWon: {type:Boolean, default:false },
        stageName: { type:String, default:null},
        mobileNumber: { type:String, default:null},
        source: { type:String, default:null},
        relatasStage:{ type:String, default:null},
        productType:{ type:String, default:null},
        competitor:{ type:String, default:null},
        sourceType:{ type:String, default:null},
        vertical:{ type:String, default:null},
        businessUnit: { type:String, default:null },
        type: { type:String, default:"New" },
        solution: { type:String, default:null },
        currency: { type:String, default:null },
        xr: { type:Number, default:1 },
        listOfItems:[{
            name: { type:String, default:null },
            businessUnit: { type:String, default:null },
            solution: { type:String, default:null },
            currency: { type:String, default:null },
            xr: { type:Number, default:1 },
            amount: { type:Number, default:0 },
            netGrossMargin: { type:Number, default:100 },
        }],
        BANT:{
            budget:{type:Boolean, default:false},
            authority:{type:Boolean, default:false},
            need:{type:Boolean, default:false},
            time:{type:Boolean, default:false}
        },
        interactionCount:{type:Number,default:0},
        interactionsInProgress:{type:Number,default:0},
        lastStageUpdated:{
            fromStage:{type:String, default:null},
            date:{type:Date, default:null}
        },
        partners:[{
            interactionCount:{type:Number,default:0},
            interactionsInProgress:{type:Number,default:0},
            fullName:{ type:String, default:null},
            personId:{ type: String, default:null},
            contactId:{ type: String, default:null},
            emailId:{ type:String, default:null}
        }],
        decisionMakers:[{
            interactionCount:{type:Number,default:0},
            interactionsInProgress:{type:Number,default:0},
            fullName:{ type:String, default:null},
            personId:{ type: String, default:null},
            contactId:{ type: String, default:null},
            emailId:{ type:String, default:null}
        }],
        influencers:[{
            interactionCount:{type:Number,default:0},
            interactionsInProgress:{type:Number,default:0},
            fullName:{ type:String, default:null},
            personId:{ type: String, default:null},
            contactId:{ type: String, default:null},
            emailId:{ type:String, default:null}
        }],
        geoLocation:{
            town:{ type:String, default:null},
            zone:{ type:String, default:null},
            lat:{ type:Number, default:null},
            lng:{ type:Number, default:null},
        },
        notes:[{
            createdDate:{type:Date, default:null},
            text:{type:String, default:null}
        }],
        masterData: [{
            type:{type:String, default:null},
            data:[]
        }],
        closeReasons:[],
        closeReasonDescription:{ type:String, default:null},
        usersWithAccess:[
            {
                emailId:{type:String},
                accessGroup:{type:String}
            }
        ],
        usersAndInteractions:[
            {
                ownerEmailId:{type:String},
                emailId:{type:String},
                count:{type:Number}
            }
        ],
        accounts:[
            {
                group:{type:String},
                name:{type:String}
            }
        ],
        renewed:{
            createdDate: {type: Date, default: null},
            closeDate: {type: Date, default: null},
            amount: { type:Number, default:0 },
            netGrossMargin: { type:Number, default:100 }
        },
        renewalStatusSet:{type:Boolean, default:false},
        roadBlockForOpportunity: {type:Boolean, default:false },
        negativeSentiment: {type:Boolean, default:false },
        respondedToCustomer: {type:Boolean, default:false }

    });
    exports.opportunities = mongoose.model('opportunities', opportunitiesSchema, 'opportunities');
}).call(this);

(function () {
    var opportunitiesTargetSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    opportunitiesTargetSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, required: true},
        userEmailId: { type:String, default:null},
        salesTarget:[{
            date:{type: Date,default:null},
            target:{type: Number,default:0},
            monthYear:{type: String,default:null}
        }],
        fy:{
            start:{type: Date,default:null},
            end:{type: Date,default:null}
        },
        isCompanyTarget:{type:Boolean, default:false}
    });
    exports.opportunitiesTarget = mongoose.model('opportunitiesTarget', opportunitiesTargetSchema, 'opportunitiesTarget');
}).call(this);

(function () {
    var dealsAtRiskMetaSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    dealsAtRiskMetaSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, required: true},
        userEmailId: { type:String, default:null},
        date:{type:Date,default:null},
        count:{type:Number,default:0},
        totalDealValueAtRisk:{type:Number,default:0},
        deals:[],
        opportunityIds:[],
        averageRisk:{type:String,default:null},
        totalDeals:{type:Number,default:0}
    });
    exports.dealsAtRiskMeta = mongoose.model('dealsAtRiskMeta', dealsAtRiskMetaSchema, 'dealsAtRiskMeta');
}).call(this);

(function() {
    var todayInsightsSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    todayInsightsSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, required: true},
        userEmailId: { type:String, default:null},
        date:{type:Date,default:null},
        recommendedToMeetCount:{type:Number,default:0},
        UpcomingMeetinsCount:{type:Number,default:0},
        upcomingTasksCount:{type:Number,default:0},
        overdueTasksCount:{type:Number,default:0},
        staleOppsCount:{type:Number,default:0},
        dealsAtRiskCount:{type:Number,default:0},
        impMailsResponseCount:{type:Number,default:0},
        totalDealValueAtRisk:{type:Number,default:0},
        totalStaleOppsValue:{type:Number,default:0},
        dealClosingOppsIds:[],
        staleOppsIds:[],
    });
    exports.todayInsights = mongoose.model('todayInsights', todayInsightsSchema, 'todayInsights');
    
}).call(this);

(function () {
    var pipelineMetaSchema, mongoose;
    mongoose = require('../node_modules/mongoose');

    pipelineMetaSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, required: true},
        userEmailId: { type:String, default:null},
        date:{type:Date,default:null},
        totalPipeline:{type:Number,default:0},
        opportunities:[]
    });
    exports.pipelineMeta = mongoose.model('pipelineMeta', pipelineMetaSchema, 'pipelineMeta');
}).call(this);
