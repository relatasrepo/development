(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , masterDataSchema = new Schema({
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companyModel',
            required: true
        },
        createdDate:{type:Date},
        name:{type:String},
        oppLinkMandatory:{type:Boolean,default:false},
        importantHeaders:[],
        relatasAccount:{type:String},
        data:[]
    });

    exports.masterData = mongoose.model('masterData', masterDataSchema, 'masterData');

}).call(this);
