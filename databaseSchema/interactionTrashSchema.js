/*
 Relatas : New interactions Schema. 2 Nov 2017.
 */

(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , interactionSchema = new Schema(
        {
            ownerId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User',
                required: true
            },
            ownerEmailId: {type: String},
            emailId: {
                type: String,
                default: null
            },
            firstName: {type: String, default: null},
            lastName: {type: String, default: null},
            companyName: {type: String, default: null},
            designation: {type: String, default: null},
            profilePicUrl: {type: String, default: null},
            publicProfileUrl: {type: String, default: null},
            location: {type: String, default: null},
            interactionDate: {type: Date, default: null},
            mobileNumber: {type: String, default: null},
            endDate: {type: Date, default: null},
            action: {
                type: String,
                enum:['sender', 'receiver'],
                default: null
            },
            interactionType: {type: String, default: null},
            subType: {type: String, default: null},
            ignore:{type:Boolean, default:false},
            insightsIgnore: {
                responsePending:{type:Boolean, default:false}
            },
            refId: {
                type: String,
                required: true
            },
            parentRefId: {type: String, default: null},
            createdDate: {type: Date, default: null},
            source: {type: String, default: null},
            title: {type: String, default: null},
            postURL: {type: String, default: null},
            description: {type: String, default: null},
            trackId: {type: String, default: null},
            trackInfo: {
                action:{type:String,default:null,enum:['request','confirmed','re scheduled']},
                lastOpenedOn: {type: Date, default: null},
                status:{type:Boolean,default:false}, // for task
                isRed:{type:Boolean, default:false}, // Emails sent from relatas with track open and red
                trackOpen:{type:Boolean,default:false}, // Emails sent from relatas with track open
                trackResponse:{type:Boolean,default:false}, // Emails sent from relatas with track response
                gotResponse:{type:Boolean,default:false}, // Emails sent from relatas with track response and got response
                trackDocument:{type:Boolean,default:false}, // Emails sent from relatas with track doc open
                emailOpens:{type:Number,default:0} // Number of times email opened
            },
            user_twitter_id: {type: String, default: null},                 
            user_twitter_name: {type: String, default: null},
            user_facebook_id: {type: String, default: null},
            emailContentId:{type:String,default:null}, // Actual id of email
            googleAccountEmailId:{type:String,default:null},
            toCcBcc:{type:String,default:null},
            emailThreadId:{type:String,default:null}, // thread id of email - content id and thread id might be same if email thread have single mail
            futureEvents:{
                dates:[{type: Date, default: null}],
                suggestions:[{type: String, default: null}]
            },
            to:[{emailId: { type:String, default:null}}],
            cc:[{emailId: { type:String, default:null}}],
            eActionTaken:{type: Boolean, default: false},
            summary:{type: String, default: null},
            importance:{type: Number, default: 0},
            duration:{type: Number, default: 0},
            actions:{type: String, default: null},
            sentiment:{type: String, default: null},
            emailClassified:{type: Boolean, default: false},
            hasUnsubscribe:{type: Boolean, default: false},
            notImportant:{type: Boolean, default: false},
            sparkSentiment:{type: String, default: null},
            sparkIntent:{type: String, default: null},
            checkIfSignedDocumentSent:{type: Boolean, default: false},
            checkIfImportantDocumentRequested:{type: Boolean, default: false},
            checkIfQuestionAskedAndNotAnswered:{type: Boolean, default: false},
            companyId:{
                type:mongoose.Schema.Types.ObjectId,
                ref: 'Company',
                default: null
            }
        });

    exports.interactionsTrash = mongoose.model('interactionsTrash', interactionSchema, 'interactionsTrash');

}).call(this);
