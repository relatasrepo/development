
(function () {

    var mongoose = require('../node_modules/mongoose')
      , Schema = mongoose.Schema
      , revenueHierarchySchema = new Schema({
        companyId: {type: mongoose.Schema.Types.ObjectId, required: true},
        userId: {type: mongoose.Schema.Types.ObjectId, required: true},
        ownerEmailId:{type:String},
        hierarchyParent: {type: Schema.Types.ObjectId, ref: 'User', default:null},
        hierarchyPath:{type:String, default:null},
        orgHead:{type:Boolean,default:false}
        // monthlyTargets:[{
        //     date:{type: Date,default:null},
        //     target:{type: Number,default:0},
        //     monthYear:{type: String,default:null}
        // }]
    });

    exports.revenueHierarchy = mongoose.model('revenueHierarchy', revenueHierarchySchema, 'revenueHierarchy');

}).call(this);
