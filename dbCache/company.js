/**
 * Created by naveen on 7/10/17.
 */

var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
// var myUserCollection = require('../../databaseSchema/userManagementSchema').User;

var _ = require("lodash")
// var mongoose = require('mongoose');
var moment = require('moment-timezone');
var winstonLog = require('../common/winstonLog');

var redis = require('redis');
var redisClient = redis.createClient();

var logLib = new winstonLog();
var redisLogs = logLib.getRedisLog();

function CompanyManagement() {

}

CompanyManagement.prototype.getStageWeights = function (companyId,callback) {
    var key = "oppWeights"+String(companyId);

    getCache(key,function (cacheWeights) {
        if(cacheWeights){
            callback(cacheWeights)
        } else {
            companyCollection.findOne({_id:companyId},{opportunityStages:1},function (err,weights) {
                if(!err && weights){
                    var hashMap = getHashMap(weights.opportunityStages);
                    setCache(key,hashMap)
                    callback(hashMap)
                } else {
                    callback(null)
                }
            })
        }
    })
}

CompanyManagement.prototype.clearOppCache = function (companyId,callback) {

    var key1 = "oppStages"+String(companyId);
    var key2 = "oppWeights"+String(companyId);

    redisClient.del(key1);
    redisClient.del(key2);

    if(callback){
        callback()
    }
}

CompanyManagement.prototype.clearExceptionalAccessCache = function (id,callback) {

    var key1 = "oppUserAccessControl"+String(id);
    var key2 = "userProfile"+String(id);

    redisClient.del(key1);
    redisClient.del(key2);

    if(callback){
        callback()
    }
}

CompanyManagement.prototype.clearCompanyHierarchyCache = function (companyId,callback) {

    var key = "getTeam"+String(companyId)

    redisClient.del(key);

    if(callback){
        callback()
    }
}

CompanyManagement.prototype.getOppStages = function (companyId,callback) {
    var key = "oppStages"+String(companyId);
    var key2 = "commitDayHour"+String(companyId);

    getCache(key,function (cacheWeights) {
        getCache(key2,function (cacheCommitDayHour) {
            if(cacheWeights && cacheCommitDayHour){
                callback(cacheWeights,cacheCommitDayHour)
            } else {
                companyCollection.findOne({_id:companyId},{opportunityStages:1,commitDay:1,commitHour:1,monthCommitCutOff:1,qtrCommitCutOff:1,currency:1},function (err,data) {
                    if(!err && data){

                        if(data.qtrCommitCutOff && data.qtrCommitCutOff == 40 && moment().daysInMonth() == 31){
                            data.qtrCommitCutOff = 41;
                        }

                        if(data.qtrCommitCutOff && data.qtrCommitCutOff == 50 && moment().daysInMonth() == 31){
                            data.qtrCommitCutOff = 51;
                        }

                        setCache(key,data.opportunityStages)
                        setCache(key2,data)
                        callback(data.opportunityStages,data)
                    } else {
                        callback(null)
                    }
                })
            }
        })
    })
}

CompanyManagement.prototype.getManualAccountsForGroup = function (companyId,group,callback) {
    var key = String(companyId)+group;
    getCache(key,function (data) {
        callback(data)
    })
}

function getHashMap(array){
    var hashMap = {};

    _.each(array,function (el) {
        hashMap[el.name] = el.weight;
    })

    return hashMap;
}

CompanyManagement.prototype.setCache = function(key,data){
    setCache(key,data)
}

function setCache(key,data){
    //Storing for 30 min
    redisClient.setex(key, 1800, JSON.stringify(data));
}

function getCache(key,callback){
    redisLogs.info("Company Cache", key);

    if(!_.includes(key, 'null')){
        redisClient.get(key, function (error, result) {
            try {
                var data = JSON.parse(result);
                // callback(data)
                callback(null)
            } catch (err) {
                callback(null)
            }
        });
    } else {
        callback(null)
    }
}

module.exports = CompanyManagement;
