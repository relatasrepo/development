/**
 * Created by sudhakar on 2/8/14.
 *
 *Updated for Facebook
 **/

function config(){

}

var domain = {
    domainName:'https://showcase.relatas.com',
    domainNameForUniqueName :'https://showcase.relatas.com',
    mainDomain : 'showcase.relatas.com'
}

var serverPort = {
    PORT:3131
}

var server = 'DEV';
var mainHost = 'relatas';
var excludeDomains = ['showcase.relatas.com'];

var authCredentials = {
    //Production use
    FACEBOOK_APP_ID : '1060038077361225',
    FACEBOOK_APP_SECRET : 'b3326ffb494fddff51fdde3fecdaba87',
    FACEBOOK_REDIRESCT_URL : '/auth/facebook/callback',

    GOOGLE_API_KEY:'AIzaSyAUTHh1JnArfd-PG2GYpkYBFuyxPeS_J4g',
    GOOGLE_CLIENT_ID : '574261059240-elq9ub1pei1n1unfo1cpt2snvvk9d2c7.apps.googleusercontent.com',
    GOOGLE_CLIENT_SECRET : 'm2IjAQddqhQLueOOm-adbJ4Q',
    GOOGLE_REDIRECT_URL : 'https://showcase.relatas.com/auth/callback',
    GOOGLE_REDIRECT_URL_MOBILE : 'urn:ietf:wg:oauth:2.0:oob',

    LINKEDIN_CONSUMER_KEY : '7502h1aqcmbyes',
    LINKEDIN_CONSUMER_SECRET : '4K5wa8MV6u9QDNW1',
    LINKEDIN_REDIRECT_URL : '/oauth/linkedin/callback',

    TWITTER_CONSUMER_KEY : 'cZzGk24sqjrUlnHpGpMcjfwnH',
    TWITTER_CONSUMER_SECRET : 'tFsBPLl7PdpyBbMT4kmQM79YRVM2Y2pAOzMKAGBavKQxJNZFzI',
    TWITTER_REDIRECT_URL  : 'https://showcase.relatas.com/auth/twitter/callback',

    OFFICE365_CLIENT_ID : '1a86f939-b628-404d-9f83-1eb14a4a6f24',
    OFFICE365_CLIENT_SECRETE : 'nrmqRfKWkFq9P0/XI9hudOUIftJNWqKHo5RdHP4ZOiU=',
    OFFICE365_REDIRECT_URL : 'https://exampledev.relatas.com/office365/auth/callback',
    OFFICE365_RESOURCE : 'https://outlook.office365.com/',
    OFFICE365_TENANT : 'd1212ba6-7d4d-4cfc-a103-d85039e72353',

    linkedinScopes: ['r_emailaddress','r_basicprofile','w_share'],

    OPEN_WEATHER_MAP_API_KEY: 'c6ae4fae8df46cef177a44a534616717',
    FULL_CONTACT_API_KEY:'8d60a159a549115e',

    OUTLOOK_clientID: "39b2e488-e080-431b-ac51-860a35f36ea8",
    OUTLOOK_clientSecret: "QGDewHVg9GCieFnRt38mWHi",
    OUTLOOK_site: "https://login.microsoftonline.com/common",
    OUTLOOK_authorizationPath: "/oauth2/v2.0/authorize",
    OUTLOOK_tokenPath: "/oauth2/v2.0/token",
    OUTLOOK_redirectURI:"https://showcase.relatas.com/authorizeTest/",

    SALESFORCE_clientID:"3MVG9ZL0ppGP5UrDXpgR_PgGDQeqX7Ru49YYPbCZfD_sR_py9P28zRzeHCB0UvTYMYarlulLbt6R0gBULvQVf",
    SALESFORCE_clientSecret:"8242087627542216559"

}

var relatasSocialAccounts = {
    twitter:"https://twitter.com/relatasHQ",
    facebook:"https://www.facebook.com/relatasHQ",
    linkedin:"https://www.linkedin.com/company/relatas"
}

var relatasHeaders = {
    relatasHeader  : domain.domainName+'/images/mailer/Relatas_header.jpg',
    relatasTwitter  : domain.domainName+'/images/mailer/relatas_mail_twitter.png',
    relatasFacebook : domain.domainName+'/images/mailer/relatas_mail_facebook.png',
    relatasLinkedin : domain.domainName+'/images/mailer/relatas_mail_linkedin.png',
    relatasGrayLogo : domain.domainName+'/images/mailer/RelatasGrayLogo.png'
}

var awsCredentials = {
    "accessKeyId": "AKIAJWM7LTFKNYORWRAQ",
    "secretAccessKey": "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",
    "region": "us-east-1",
    "bucket":'relatas-dev-docstorage',
    "icsBucket":"relatas-dev-icsfilestorage",
    "imageBucket":"relatas-dev-imagestorage",
    "companyVideosBucket":"relatas-dev-company-videos",
    "companyLogosBucket":"relatas-dev-company-logos"
}

var dbPasswords = {
    relatasDB:{
        user:"devRelatasAdmin",
        password:"vcxz7890",
        // dbConnectionUrl:"mongodb://172.31.47.181/Relatas?poolSize=100"
        dbConnectionUrl:"mongodb://10.150.0.8/Relatas_masked_227_Apr_04?poolSize=100"
    }
}

var machineLearningQueues = {
    processQueue : "devProcessQueue",
    completedQueue : "devCompletedQueue"
}

config.prototype.getQueues = function(){
    return machineLearningQueues;
}

config.prototype.getDBPasswords = function(){
    return dbPasswords;
}

config.prototype.getDomain = function(){
    return domain;
}

config.prototype.getAuthCredentials = function(){
    return authCredentials;
}

config.prototype.getServerPort = function(){
    return serverPort;
}

config.prototype.getAwsCredentials = function(){
    return awsCredentials
}

config.prototype.getRelatasSocialAccounts = function(){
    return relatasSocialAccounts
}

config.prototype.getRelatasHeaders = function(){
    return relatasHeaders
}

config.prototype.getServerEnvironment = function(){
    return server;
}

config.prototype.getMainHost = function(){
    return mainHost;
}

config.prototype.getExcludeDomains = function(){
    return excludeDomains;
}

var invalidSearchContent = [ /calendar-notification/i,/feedproxy/i, /techgig/i, /team/i,/info/i,/blog/i, /support/i, /admin/i, /hello/i, /no-reply/i, /noreply/i, /reply/i, /help/i, /mailer-daemon/i, /googlemail.com/i, /mail-noreply/i, /alert/i, /calendar-notification/i, /eBay/i, /flipkartletters/i, /pinterest/i, /dobambam.com/i, /notify/i, /offers/i, /iicicibank/i, /indiatimes/i, /info@relatas.in/i, /facebookmail/i,
    /appstore/i, /ship-confirm/i, /order-update/i, /auto-confirm/i, /ship/i, /automatic/i, /updates/i, /update/i, /notice/i, /finance/i, /aws-loft/i, /scheduling/i, /schedule/i, /payment/i, /contact/i, /bizdev/i, /editor/i, /survey/i, /ceo/i, /chair/i, /love/i, /community/i, /sale/i, /coordinator/i, /bounce/i, /postmaster/i, /newsletter/i, /marketing/i, /billing/i];

config.prototype.getBroadCastEmailList = function(){
    return invalidSearchContent;
}

module.exports = config

