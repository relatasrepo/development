var MongoClient = require('mongodb').MongoClient
  , url = "mongodb://localhost";

function init(){

  MongoClient.connect(url, function(err, client){

    var db = client.db('iccWC2019');//This will create a db automatically if it doesn't exists.

    var teamCollection = db.collection("team");

    console.log("Db is connected!");

    var player = {
      name: "Virat Kohli",
      country: "India",
      canBat: true,
      canBowl: true,
      isCaptain: true,
      age: 30
    };

    insertTeamRecord(player,teamCollection,function(err,result){
      console.log(err)
      console.log(result);
    })

  });
}

1.Create/ a db named  - use iccWC2019
2. Insert a player into a collection called team. The below command will automactically create the collection if it doesn't exist.
db.team.insert({
  name: "Virat Kohli",
  country: "India",
  canBat: true,
  canBowl: true,
  isCaptain: true,
  age: 30
})

Insert few players from different countries and ages.

3. Find a player by name

db.team.find({name:"Virat Kohli"})

4. Find all players whose age is above 25
db.team.find({age:{$gte:25}}).count()


Assigment:
1. Write a query to find all players who are below 25 or any age group
2. Find count of players from any given country
3. Find count of players who can only bat
4. Find count of players who can bat from a particular country
