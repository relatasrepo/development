/**
 * Created by sudhakar on 2/8/14.
 */
function config(){

}

var domain = {
    domainName:'https://staging.relatas.com',
    domainNameForUniqueName :'https://staging.relatas.com',
    mainDomain : 'staging.relatas.com',
    nonEnvDomain : 'relatas.com'
}

var serverPort = {
    PORT:3131
}

var server = 'STAGING';
var mainHost = 'relatas';
var excludeDomains = ['staging.relatas.com'];

var authCredentials = {
    //Production use
    FACEBOOK_APP_ID : '1060038077361225',
    FACEBOOK_APP_SECRET : 'b3326ffb494fddff51fdde3fecdaba87',
    FACEBOOK_REDIRESCT_URL : '/auth/facebook/callback',

GOOGLE_SERVER_API_KEY:'AIzaSyB6pmq1cRWmGiNeX2KpqxermAOfr6RopIk', 
    GOOGLE_API_KEY:'AIzaSyB6pmq1cRWmGiNeX2KpqxermAOfr6RopIk',
    GOOGLE_CLIENT_ID : '1071111038885-pgvtj892eqgbp2hhvfoua6pgktmjd1cd.apps.googleusercontent.com',
    GOOGLE_CLIENT_SECRET : 'oEZWvlDfdI7UMAadZcBNltTx',
    GOOGLE_REDIRECT_URL : 'http://staging.relatas.com/auth/callback',

    LINKEDIN_CONSUMER_KEY : '7502h1aqcmbyes',
    LINKEDIN_CONSUMER_SECRET : '4K5wa8MV6u9QDNW1',
    LINKEDIN_REDIRECT_URL : '/oauth/linkedin/callback',

    TWITTER_CONSUMER_KEY : 'cZzGk24sqjrUlnHpGpMcjfwnH',
    TWITTER_CONSUMER_SECRET : 'tFsBPLl7PdpyBbMT4kmQM79YRVM2Y2pAOzMKAGBavKQxJNZFzI',
    TWITTER_REDIRECT_URL  : 'http://staging.relatas.com/auth/twitter/callback',
 
OUTLOOK_clientID: "2300ccc1-7db1-4971-a5d9-fd12e6b7b54b",
    OUTLOOK_clientSecret: "kGkwyDKgwZAp6c5QfG1umCS",
    OUTLOOK_site: "https://login.microsoftonline.com/common",
    OUTLOOK_authorizationPath: "/oauth2/v2.0/authorize",
    OUTLOOK_tokenPath: "/oauth2/v2.0/token",
    OUTLOOK_redirectURI:"https://staging.relatas.com/authorizeTest/",

    linkedinScopes: ['r_emailaddress', 'r_basicprofile', 'w_share'],

    OPEN_WEATHER_MAP_API_KEY: 'c6ae4fae8df46cef177a44a534616717',
    FULL_CONTACT_API_KEY:'8d60a159a549115e',
SALESFORCE_clientID:"3MVG9ZL0ppGP5UrDXpgR_PgGDQeqX7Ru49YYPbCZfD_sR_py9P28zRzeHCB0UvTYMYarlulLbt6R0gBULvQVf",
SALESFORCE_clientSecret:"8242087627542216559"
}

var relatasSocialAccounts = {
    twitter:"https://twitter.com/relatasHQ",
    facebook:"https://www.facebook.com/relatasHQ",
    linkedin:"https://www.linkedin.com/company/relatas"
}

var relatasHeaders = {
    relatasHeader  : domain.domainName+'/images/mailer/Relatas_header.jpg',
    relatasTwitter  : domain.domainName+'/images/mailer/relatas_mail_twitter.png',
    relatasFacebook : domain.domainName+'/images/mailer/relatas_mail_facebook.png',
    relatasLinkedin : domain.domainName+'/images/mailer/relatas_mail_linkedin.png',
    relatasGrayLogo : domain.domainName+'/images/mailer/RelatasGrayLogo.png'
}

var awsCredentials = {
    "accessKeyId": "AKIAJWM7LTFKNYORWRAQ",
    "secretAccessKey": "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",
    "region": "us-east-1",
    "bucket":'relatas-dev-docstorage',
    "icsBucket":"relatas-dev-icsfilestorage",
    "imageBucket":"relatas-dev-imagestorage",
    "companyVideosBucket":"relatas-dev-company-videos",
    "companyLogosBucket":"relatas-dev-company-logos"
}
/*
var dbPasswords = {
    relatasDB:{
        user:"stuserrel",
        password:"stuserpass",
 dbConnectionUrl:"mongodb:/localhost/Relatas?poolSize=100"
    }
}
*/

var dbPasswords = {
    relatasDB:{
        user:"reladmin",
        password:"Hennur123",
 dbConnectionUrl:"mongodb://10.150.0.8/Relatas_masked?poolSize=100"
    }
}

config.prototype.getDBPasswords = function(){
    return dbPasswords;
}

config.prototype.getDomain = function(){
    return domain;
}

config.prototype.getAuthCredentials = function(){
    return authCredentials;
}

config.prototype.getServerPort = function(){
    return serverPort;
}

config.prototype.getAwsCredentials = function(){
    return awsCredentials
}

config.prototype.getRelatasSocialAccounts = function(){
    return relatasSocialAccounts
}

config.prototype.getRelatasHeaders = function(){
    return relatasHeaders
}

config.prototype.getServerEnvironment = function(){
    return server;
}

config.prototype.getMainHost = function(){
    return mainHost;
}

config.prototype.getExcludeDomains = function(){
    return excludeDomains;
}

var invalidSearchContent = [ /calendar-notification/i,/feedproxy/i, /techgig/i, /team/i,/info/i, /support/i, /admin/i, /hello/i, /no-reply/i, /noreply/i, /no-reply/i, /help/i, /mailer-daemon/i, /googlemail.com/i, /mail-noreply/i, /alert/i, /calendar-notification/i, /eBay/i, /flipkartletters/i, /pinterest/i, /dobambam.com/i, /notify/i, /offers/i, /iicicibank/i, /indiatimes/i, /info@relatas.in/i, /facebookmail/i];

config.prototype.getBroadCastEmailList = function(){
    return invalidSearchContent;
}

var machineLearningQueues = {
    processQueue : "stageProcessQueue",
    completedQueue : "stageCompletedQueue"
}

config.prototype.getQueues = function(){
    return machineLearningQueues;
}

module.exports = config
