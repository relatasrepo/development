var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var request = require('request');
var gcal = require('google-calendar');
var simple_unique_id = require('simple-unique-id');
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;

var userManagement = require('../dataAccess/userManagementDataAccess');
var eventDataAccess = require('../dataAccess/eventDataAccess');
var validations = require('../public/javascripts/validation');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var googleCalendarAPI = require('../common/googleCalendar');
var commonUtility = require('../common/commonUtility');

var appCredential = new appCredentials();
var userManagements = new userManagement();
var common = new commonUtility();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var domainName = domain.domainName;

var logLib = new winstonLog();
var logger =logLib.getWinston()
var googleCalendar = new googleCalendarAPI();

var googleMapUrl = []

//router.get('/events',common.checkUserDomain,function(req,res){
//    try {
//        res.render('events',common.getLoginStatus(req));
//        logger.info('User entered in to events page');
//    }catch (ex) {
//        logger.error('Error in rendering events page', ex.stack);
//        throw ex;
//    }
//});

//router.get('/createAnEvent',common.checkUserDomain,function(req,res){
//    try {
//        res.render('createEvent',common.getLoginStatus(req));
//        logger.info('User entered in to createEvent page');
//    }catch (ex) {
//        logger.error('Error in rendering createEvent page', ex.stack);
//        throw ex;
//    }
//});

//router.get('/events/dashboard/today',function(req,res){
//    var tokenSecret = "alpha";
//    var decoded = jwt.decode(req.user ,tokenSecret);
//    var userId = decoded.id;
//    getEventsByDate(userId,2,4,function(events){
//        res.send(events)
//    })
//});

function getEventsByDate(userId,start,end,callback){
    var eventAccess = new  eventDataAccess();
    eventAccess.getAllSubscribedEvents(userId,2,4,function(events){
        callback(events)
    });
}

//router.get('/events/calendar/get',function(req,res){
//    var tokenSecret = "alpha";
//    var decoded = jwt.decode(req.user ,tokenSecret);
//    var userId = decoded.id;
//    getEventsByDate(userId,90,90,function(events){
//        res.send(events)
//    })
//});

//router.get('/getEventMessage',function(req,res){
//    if(checkRequired(req.session.eventMessage)){
//        res.send( req.session.eventMessage)
//    }else res.send(null)
//})
//
//router.get('/clearEventMessage',function(req,res){
//    if(checkRequired(req.session.eventMessage)){
//        delete req.session.eventMessage;
//        res.send(true)
//    }else res.send(true)
//})

router.get('/event/:eventId',common.checkUserDomain,function(req,res){
    try {
        var eventId = req.params.eventId;
        req.session.finalPageUrl = '/event/'+eventId;
        var eventAccess = new  eventDataAccess();
        if(checkRequired(eventId)){
            req.session.eventId = eventId;
            getEventById(eventId,function(error,event){
                if(error || !checkRequired(event)){
                    logger.info('An error occurred while searching event by id '+error);
                    res.render('eventDetails',common.getLoginStatus(req));
                }
                else{
                    var obj ={}
                    obj.title = event.eventName;
                    obj.url = domainName+'/event/'+eventId;
                    if(event.image){
                        obj.image = event.image.imageUrl || 'https://relatas-live-imagestorage.s3.amazonaws.com/defaultEventImageGray.png';
                    }else obj.image = 'https://relatas-live-imagestorage.s3.amazonaws.com/defaultEventImageGray.png';
                    obj.description = event.speakerDesignation || event.eventName;
                    var status = common.getLoginStatus(req);
                    obj.loginSignUp = status.loginSignUp;
                    obj.logout = status.logout;
                    res.render('eventDetails',obj);
                    logger.info('User entered in to eventDetails page');
                }
            })
        } else  res.render('eventDetails');
    }catch (ex) {
        logger.error('Error in rendering eventDetails page', ex.stack);
        throw ex;
    }
});

//router.get('/getRequestedEvent',function(req,res){
//    var eventAccess = new  eventDataAccess();
//
//    var eventId = req.session.eventId;
//    if(checkRequired(eventId)){
//        getEventById(eventId,function(error,event){
//            if(error){
//                logger.info('An error occurred while searching event by id '+error);
//                res.send(false)
//            }
//            else{
//                res.send(event);
//            }
//        })
//    }else res.send(false);
//});

router.get('/event/getById/:eventId',function(req,res){
    if(req.params.eventId){
        getEventById(req.params.eventId,function(error,event){
            res.send(event)
        })
    }else res.send(null)
})

function getEventById(eventId,back){
    var eventAccess = new  eventDataAccess();
    eventAccess.getEventById(eventId,back);
}

router.get('/event/addToCalendar/:eventId',isLoggedInUser,function(req,res){
    var eventAccess = new  eventDataAccess();
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var eventId = req.params.eventId;
    if(checkRequired(userId) && checkRequired(eventId)){
    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1},function(error,profile){
        if(checkRequired(profile) && googleCalendar.validateUserGoogleAccount(profile)){
            eventAccess.getUserSubscribeToEventsDetails(userId,function(error,SDetails){
                if(error){
                    res.send(false);
                }
                else if(checkRequired(SDetails)){
                    addEventToCalendar(req,res,userId,eventId,true,false);
                }else{
                    createBasicSubscribeToEventsAcc(userId,function(flag){
                        if(flag){
                            addEventToCalendar(req,res,userId,eventId,true,false);
                        }
                        else{
                            res.send(false);
                        }
                    })
                }
            })
        }else res.send(false);
    })
    }
    else{
        res.send(false);
    }
});

router.get('/event/addToCalendar/:eventId/:userId',function(req,res){
    var eventAccess = new  eventDataAccess();
    var userId = req.params.userId;
    var eventId = req.params.eventId;
    if(checkRequired(userId) && checkRequired(eventId)){
        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1},function(error,profile){
            if(checkRequired(profile) && googleCalendar.validateUserGoogleAccount(profile)){
                eventAccess.getUserSubscribeToEventsDetails(userId,function(error,SDetails){
                    if(error){
                        var msg1 = {message:"An error occurred while adding event to calendar. Please try again.",type:'error'};
                        req.session.eventMessage = msg1;
                        res.redirect('/event/'+eventId);
                    }
                    else if(checkRequired(SDetails)){
                        addEventToCalendar(req,res,userId,eventId,true,true);
                    }else{
                        createBasicSubscribeToEventsAcc(userId,function(flag){
                            if(flag){
                                addEventToCalendar(req,res,userId,eventId,true,true);
                            }
                            else{
                                var msg2 = {message:"An error occurred while adding event to calendar. Please try again.",type:'error'};
                                req.session.eventMessage = msg2;
                                res.redirect('/event/'+eventId);
                            }
                        })
                    }
                })
            }else{
                var msg4 = {message:"Adding event to calendar failed. Please make sure that you have added your google account to your Relatas profile.",type:'error'};
                req.session.eventMessage = msg4;
                res.redirect('/event/'+eventId);
            }
        })
    }
    else{
        var msg3 = {message:"An error occurred while adding event to calendar. Please try again.",type:'error'};
        req.session.eventMessage = msg3;
        res.redirect('/event/'+eventId);
    }
});

function addEventToCalendar(req,res,userId,eventId,srvp,mail){
    var eventAccess = new  eventDataAccess();
    eventAccess.getEventById(eventId,function(error,event){
        if(error){
            logger.info('An error occurred while searching event by id '+error);
            if(mail){
                var msg1 = {message:"An error occurred while adding event to calendar. Please try again.",type:'error'};
                req.session.eventMessage = msg1;
                res.redirect('/event/'+eventId);
            }else
            res.send(false)
        }
        else{
            if(checkRequired(event)){
                updateUserSubscribeToAccWithEvenList(userId,[event],false,true,true,srvp,false);
                setTimeout(function(){
                    if(mail){
                        var msg2 = {message:"Event successfully added to your calendar.",type:'success'};

                        req.session.eventMessage = msg2;
                        res.redirect('/event/'+eventId);
                    }else
                    res.send(true);
                },1000)
            }
            else{
                if(mail){
                    var msg3 = {message:"An error occurred while adding event to calendar. Please try again.",type:'error'};
                    req.session.eventMessage = msg3;
                    res.redirect('/event/'+eventId);
                }else
                res.send(false);
            }
        }
    })
}

router.post('/saveEventDetails',function(req,res){
    req.session.saveEventDetails=req.body;
    res.send(true);
});

router.get('/getSavedEventDetails',function(req,res){
    var details = req.session.saveEventDetails;
    res.send(details)
});

router.get('/clearSavedEventDetails',function(req,res){
    req.session.saveEventDetails = null;
    res.send(true)
});

router.get('/allEventsLocations',function(req,res){
    var eventAccess = new  eventDataAccess();
    eventAccess.getAllEventsLocations(function(error,locations){

        res.send(locations)
    })
})

router.get('/getAllEvents',function(req,res){
    var eventAccess = new  eventDataAccess();
    eventAccess.getAllPublicEvents(function(error,eventList){
        res.send(eventList);
    })
});

router.post('/getAllSubscribedEvents',function(req,res){
    var eventAccess = new  eventDataAccess();

    var ids = JSON.parse(req.body.list);
    eventAccess.getEventsByArrayOfId(ids,function(error,eList){
        res.send(eList)
    })
});

router.get('/events/nonConfirmedEvents',function(req,res){
    var eventAccess = new  eventDataAccess();
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    eventAccess.getNonConfirmedEvents(userId,function(error,result){
        res.send(result);
    })
});

router.post('/createEvent',isLoggedInUser,function(req,res){
    try{
        var userLogInfo = getUserLogInfo(req);
        userLogInfo.action = 'request for creating Event';
        var eventDetails = req.body;
        var validation = new validations();
        var eventAccess = new  eventDataAccess();

        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret);
        var userId = decoded.id;
        eventDetails.userId = userId;

        eventDetails.docs = JSON.parse(eventDetails.docs)
        eventDetails.image = JSON.parse(eventDetails.image)

        if(eventDetails.locationType == 'offline'){
            var locationUrl = eventDetails.eventLocationUrl.toString();
            // Location url checking
            if(locationUrl.indexOf("/maps/place/")>-1){
                locationUrl = locationUrl.split('@');

                if(checkRequired(locationUrl[1])){
                    var latLangArr = locationUrl[1].split(',');
                    var lat = parseFloat(latLangArr[0]);
                    var lang = parseFloat(latLangArr[1]);
                    if(checkRequired(lat) && checkRequired(lang)){

                        var connectionOptions = {
                            url:'http://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lang+''
                        };

                        request(connectionOptions, function ( error, r, status ) {

                            status = JSON.parse(status)

                            if(checkRequired(status)){
                                if(checkRequired(status.results)){
                                    if(checkRequired(status.results[0])){
                                        if(checkRequired(status.results[0].address_components)){
                                            if(checkRequired(status.results[0].address_components[0])){
                                                var addressArr = status.results[0].address_components;

                                                var city;
                                                var country;
                                                for(var i=0; i<addressArr.length; i++){
                                                    if(addressArr[i].types[0] == "administrative_area_level_2"){
                                                        city = addressArr[i].long_name;

                                                        if(checkRequired(city)){
                                                            city = city.split(" ")
                                                            if(city.length > 2){
                                                                city = city[0]+' '+city[1];
                                                            }
                                                            else city = addressArr[i].long_name;
                                                        }
                                                    }
                                                    if(!checkRequired(city)){
                                                        if(addressArr[i].types[0] == "administrative_area_level_1"){
                                                            city = addressArr[i].long_name;
                                                            if(checkRequired(city)){
                                                                city = city.split(" ")
                                                                if(city.length > 2){
                                                                    city = city[0]+' '+city[1];
                                                                }
                                                                else city = city[0]
                                                            }
                                                        }
                                                    }
                                                    if(!checkRequired(city)){
                                                        if(addressArr[i].types[0] == "locality"){
                                                            city = addressArr[i].long_name;
                                                        }
                                                    }


                                                    if(addressArr[i].types[0] == "country"){
                                                        country = addressArr[i].short_name;
                                                    }

                                                }

                                                if(checkRequired(country) && checkRequired(city)){
                                                    eventDetails.eventLocation = country+' - '+city;
                                                    if(checkRequired(eventDetails.creatorId) && eventDetails.eventId){
                                                        updateEvent(req,res,eventDetails,userId,userLogInfo);

                                                    }
                                                    else{
                                                        createEvent(req,res,eventDetails,userId,userLogInfo);

                                                    }


                                                }else res.send('invalidUrl')
                                            }else res.send('invalidUrl')
                                        }else res.send('invalidUrl')
                                    }else res.send('invalidUrl')
                                }else res.send('invalidUrl')
                            }else res.send('invalidUrl')
                        })
                    }else res.send('invalidUrl')
                }else res.send('invalidUrl')
            }
            else res.send('invalidUrl')
        }
        else{
            eventDetails.eventLocation = 'Online';
            if(checkRequired(eventDetails.creatorId) && eventDetails.eventId){

                updateEvent(req,res,eventDetails,userId,userLogInfo);
            }
            else{

                createEvent(req,res,eventDetails,userId,userLogInfo);
            }
        }
    }
    catch(e){
     logger.info(e);
    }
});

function createEvent(req,res,eventDetails,userId,userLogInfo){

    var validation = new validations();
    var eventAccess = new  eventDataAccess();

    validation.validateEventData(eventDetails,function(status){
        if(status == true){
            var event = constructEventDetails(eventDetails,true)

            eventAccess.createEvent(event,function(error,event,message){
                userLogInfo.functionName = 'createEvent in createEvent router'
                if(error || event == null){
                    userLogInfo.error= error;
                    userLogInfo.info = 'Error in mongo db while creating event';
                    logger.info('Error in mongo',userLogInfo);
                    res.send(false)
                }
                else{
                    if(event.accessType == 'public'){
                        checkUserSubscribeDetails(event,userId,false,true,false,false)
                    }
                    else if(event.accessType == 'private'){
                        checkUserSubscribeDetails(event,userId,true,true,false,false)
                    }
                    res.send(event)
                }
            })
        }
        else res.send(status)
    })
}

function updateEvent(req,res,eventDetails,userId,userLogInfo){

    var validation = new validations();
    var eventAccess = new  eventDataAccess();
    if(checkRequired(eventDetails.creatorId) && eventDetails.eventId){
        var creatorId = eventDetails.creatorId;
        var eventId = eventDetails.eventId;
        validation.validateEventData(eventDetails,function(status){
            if(status == true){
                var event = constructEventDetails(eventDetails,false)
                event.eventId = eventId;
                event.creatorId = creatorId;

                eventAccess.updateEvent(event,function(error,isUpdated){
                    userLogInfo.functionName = 'updating in createEvent router'
                    if(error){
                        userLogInfo.error= error;
                        userLogInfo.info = 'Error in mongo db while updating event';
                        logger.info('Error in mongo',userLogInfo);
                        res.send(false)
                    }
                    else if(isUpdated){
                        if(event.accessType == 'public'){
                            event._id = eventId;
                            checkUserSubscribeDetails(event,userId,false,false,true,false)
                        }
                        else if(event.accessType == 'private'){
                            event._id = eventId;
                            checkUserSubscribeDetails(event,userId,true,false,true,false)
                        }
                        res.send(true)
                    }
                    else{
                        res.send(false)
                    }
                })
            }
            else res.send(status)
        })
    }
    else {
        res.send(4010);
    }
}

function checkUserSubscribeDetails(event,userId,isPrivate,add,update,addCall){
    var eventAccess = new  eventDataAccess();
    if(checkRequired(userId) && checkRequired(event)){
        eventAccess.getUserSubscribeToEventsDetails(userId,function(error,SDetails){
            if(error){
                updateAllSubscribedUserWithEvent(event,userId,isPrivate,add,update,addCall)
            }
            else if(checkRequired(SDetails)){
                updateAllSubscribedUserWithEvent(event,userId,isPrivate,add,update,addCall)
               // addEventToCalendar(req,res,userId,event)
            }else{
                createBasicSubscribeToEventsAcc(userId,function(flag){
                    updateAllSubscribedUserWithEvent(event,userId,isPrivate,add,update,addCall)
                })
            }
        })

    }
    else{

    }
}

function updateAllSubscribedUserWithEvent(event,userId,isPrivate,add,update,addCall){
    var eventAccess = new  eventDataAccess();
    var allUsers = [{userId:userId}]
    var allEvents = [event];

    if(isPrivate){
        updateUserSubscribeToAccWithEvenList(allUsers[0].userId,allEvents,add,update,addCall,false,true);
    }else{
        eventAccess.getSubscribedUsersByEventTypeAndAllLocations(event.eventCategory,function(error,users1){

            allUsers = allUsers.concat(users1)
            eventAccess.getSubscribedUsersByEventTypeAndLocation(event.eventCategory,event.eventLocation,function(error,users2){

                allUsers = allUsers.concat(users2)
                if(add){
                    if(checkRequired(allUsers[0])){

                        allUsers = removeDuplicates(allUsers);

                        for(var i=0; i<allUsers.length; i++){

                            updateUserSubscribeToAccWithEvenList(allUsers[i].userId,allEvents,add,update,addCall,false,true);
                        }

                    }
                }
                else{
                    eventAccess.getAllUsersAddedThisEvent(event._id,function(error,list){

                        if(checkRequired(list)){
                            if(checkRequired(list[0])) {
                                allUsers = allUsers.concat(list);
                            }
                        }
                        if(checkRequired(allUsers[0])){

                            allUsers = removeDuplicates(allUsers);

                            for(var i=0; i<allUsers.length; i++){

                                updateUserSubscribeToAccWithEvenList(allUsers[i].userId,allEvents,add,update,addCall,false,true);
                            }

                        }
                    })
                }

            })
        })
    }
}

router.get('/getUpComingPublicEvents',function(req,res){
    var eventAccess = new  eventDataAccess();
    eventAccess.getUpComingPublicEvents(function(error,events){
        if(events){
            res.send(events);
        }
        else{
            res.send(false);
        }
    })
});

router.post('/subscribeToEvents',isLoggedInUser,function(req,res){
    var subscribeDetails = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var eventAccess = new  eventDataAccess();
   if(checkRequired(subscribeDetails)){
       if(checkRequired(subscribeDetails.ArrString) && checkRequired(userId)){
           subscribeDetails = JSON.parse(subscribeDetails.ArrString)

           var SObject = {
               userId:userId,
               eventTypes:subscribeDetails.eventTypes,
               locations:subscribeDetails.locations
           }

           eventAccess.getUserSubscribeToEventsDetails(userId,function(error,SDetails){
               if(SDetails == false){
                   res.send(false);
               }
               else if(checkRequired(SDetails)){
                    eventAccess.updateSubscribeToEvents(SObject,function(error,isUpdated){
                        if(isUpdated){
                            addSubscribedEventsToUserSubscribeToAcc(userId)
                        }
                        setTimeout(function(){
                            res.send(isUpdated)
                        },1000)
                    })
               }
               else{
                    eventAccess.createSubscribeToEvents(SObject,function(error,details){
                        if(checkRequired(details)){
                            addSubscribedEventsToUserSubscribeToAcc(userId)
                            setTimeout(function(){
                                res.send(true);
                            },1000);
                        }
                        else{
                            res.send(false);
                        }
                    })
               }
           })
       }
       else res.send(false);
   }
   else res.send(false);
})

router.get('/getUserSubscribeToEventsDetails',isLoggedInUser,function(req,res){
    var eventAccess = new  eventDataAccess();
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    eventAccess.getUserSubscribeToEventsDetails(userId,function(error,SDetails){

            res.send(SDetails);

    })
});

router.get('/getAllUsersConfirmedEvent/:eventId',function(req,res){

    var eventId = req.params.eventId;

    getAllUsersConfirmedByEventId(eventId,function(list){
        res.send(list);
    })
});

function getAllUsersConfirmedByEventId(eventId,callback){
    var eventAccess = new  eventDataAccess();

    eventAccess.getAllUsersConfirmedThisEvent(eventId,function(error,list){
        callback(list);
    })
}

function getUserListById(list,callback){

    userManagements.getBasicUserInfoByArrayOfId(list,function(error,userList){
        callback(userList)
    })
}

router.get('/event/rsvp/:eventId/:userId',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    if(userId == req.params.userId){
      res.render('rsvpUserList');
    }else res.send("Access Denied");
});

router.get('/event/rsvp/:eventId/:userId/getList',function(req,res){
    var eventId = req.params.eventId;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    if(userId == req.params.userId){
       getAllUsersConfirmedByEventId(eventId,function(list){
            if(list){
                if(list.length > 0){
                   var userIds = [];
                   for(var i=0; i<list.length; i++){
                       userIds.push(list[i].userId);
                   }
                   getUserListById(userIds,function(userList){
                       res.send(userList);
                   })
                }
            }
       })
    }else res.send("Access Denied");
});


router.get('/event/getGoogleId/:eventId',isLoggedInUser,function(req,res){
    var eventAccess = new  eventDataAccess();
    var eventId = req.params.eventId;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(checkRequired(eventId) && checkRequired(userId)){
        var event = {
            _id:eventId
        }
        eventAccess.getSingleEventFromSubscribedList(userId,event, function (error, listObj, e) {
             res.send(listObj);
        })
    }
    else{
        res.send(false);
    }
});

router.get('/event/edit/:eventId',isLoggedInUser,function(req,res){
    var eventId = req.params.eventId;
    if(checkRequired(eventId)){
        req.session.editEventId = eventId;
        res.render('createEvent');
    }
    else{
        res.redirect('/events');
    }
});

router.get('/getRequestedEventEdit',isLoggedInUser,function(req,res){
    var eventAccess = new  eventDataAccess();
    var eventId = req.session.editEventId;
    if(checkRequired(eventId)){
        eventAccess.getEventById(eventId,function(error,event){
            if(error){
                logger.info('An error occurred while searching event by id '+error);
                res.send(false)
            }
            else{
                res.send(event);
            }
        })
    }
});

router.get('/event/delete/:eventId',isLoggedInUser,function(req,res){
    var eventAccess = new  eventDataAccess();
    var eventId = req.params.eventId;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    eventAccess.deleteEvent(userId,eventId,function(error,isSuccess){
        if(isSuccess){
            deleteEventFromAllUsersGoogleCalendar(eventId,req,res)
        }

        setTimeout(function(){
            res.send(isSuccess);
        },200)

    })

});

router.get('/event/removeFromCalendar/:eventId',isLoggedInUser,function(req,res){
    var eventAccess = new  eventDataAccess();
    var eventId = req.params.eventId;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var googleEventId = 'relatasevent'+eventId;
    if(checkRequired(userId) && checkRequired(eventId) && checkRequired(googleEventId)){
        removeEventFromGoogleCalendar(googleEventId,eventId,userId,req,res)
    }
    else res.send(false);
});

// Function to develop event details JSON
function constructEventDetails(eventData,action){
  var event = {
      eventName:  eventData.eventName,
      locationName:  eventData.locationName,
      eventLocation : eventData.eventLocation,
      eventLocationUrl:eventData.eventLocationUrl,
      startDateTime: eventData.startDateTime,
      endDateTime:eventData.endDateTime,
      eventCategory: eventData.eventCategory,
      accessType: eventData.accessType,
      tags:eventData.tags,
      eventDescription:eventData.eventDescription,
      createdDate:eventData.createdDate,
      speakerName:eventData.speakerName,
      speakerDesignation:eventData.speakerDesignation,
      createdBy:{
          userId:eventData.userId,
          userEmailId:eventData.userEmailId,
          userName:eventData.userName
      }
  }
   if(action){
       event.docs = eventData.docs
       if(checkRequired(eventData.image)){
           event.image = eventData.image[0];
       }
   }else{
       if(checkRequired(eventData.docs)){
           if(checkRequired(eventData.docs[0])){
               event.docs = eventData.docs
           }
       }
       if(checkRequired(eventData.image)){
           if(checkRequired(eventData.image[0])){
               event.image = eventData.image[0];
           }

       }
   }


  return event;
}

function addSubscribedEventsToUserSubscribeToAcc(userId){
    var eventAccess = new  eventDataAccess();
        eventAccess.getSubscribedEventTypeAndLocation(userId,function(error,SDetails){
            if(SDetails == false){

            }
            else{
                if(checkRequired(SDetails.eventTypes[0]) && checkRequired(SDetails.locations[0])){
                    var eventTypes = SDetails.eventTypes;
                    var locations = SDetails.locations;
                    var allEvents = [];

                        for(var i=0; i<eventTypes.length; i++){
                            if(SDetails.all == true){
                                eventAccess.getEventsByType(userId,eventTypes[i].eventType,function(error,details){
                                    if(details){

                                        allEvents = allEvents.concat(details);

                                    }
                                })
                            }
                            else{
                                for(var j=0; j<locations.length; j++){
                                    eventAccess.getEventsByTypeAndLocation(userId,eventTypes[i].eventType,locations[j].location,function(error,details){
                                        if(details){
                                            allEvents = allEvents.concat(details);

                                        }

                                    })
                                }
                            }
                        }
                    setTimeout(function(){
                        updateUserSubscribeToAccWithEvenList(userId,allEvents,true,false,false,false,true);

                    },100)

                }
            }
        })
}

function updateUserSubscribeToAccWithEvenList(userId,allEvents,add,update,addCall,srvp,fromUpdate){ //allEvents is an array

    var eventAccess = new  eventDataAccess();
      if(checkRequired(allEvents[0]) && checkRequired(userId)){
          var eventsList = [];
          for(var i=0; i<allEvents.length; i++){

              allEvents[i].userId = userId;
              if(checkRequired(allEvents[i]._id) && checkRequired(allEvents[i].userId)){

                  eventAccess.updateSubscribeAccWithEventList(userId,allEvents[i],function(error,isSuccess,e){

                        if(isSuccess == 'exist' && checkRequired(e)){

                                eventAccess.getSingleEventFromSubscribedList(userId,e, function (error, listObj, event) {

                                    if (checkRequired(listObj)) {
                                        if (checkRequired(listObj.eventsList)) {
                                            if (checkRequired(listObj.eventsList[0])) {
                                                if (listObj.eventsList[0].confirmed == true) {
                                                    if(fromUpdate == true){
                                                        srvp = true;
                                                    }

                                                }
                                            }
                                        }
                                    }
                                    if (checkRequired(listObj)) {
                                        if (checkRequired(listObj.eventsList)) {
                                            if (checkRequired(listObj.eventsList[0])) {
                                                if (listObj.eventsList[0].googleEvent == false) {
                                                     event.userId = userId;
                                                    var sequence1 = listObj.eventsList[0].sequenceCount || 1;
                                                   createEventInGoogleCalendar(event,add,update,sequence1,srvp)
                                                }
                                                else{
                                                    if(listObj.eventsList[0].isDeleted && update == true && addCall == true){
                                                        event.userId = userId;
                                                        var sequence2 = listObj.eventsList[0].sequenceCount || 1;

                                                        createEventInGoogleCalendar(event,add,update,sequence2,srvp);
                                                    }
                                                    else if (listObj.eventsList[0].googleEvent == true ){
                                                           if(update && !listObj.eventsList[0].isDeleted){
                                                               event.userId = userId;
                                                               var sequence3 = listObj.eventsList[0].sequenceCount || 1;
                                                               createEventInGoogleCalendar(event,add,update,sequence3,srvp)
                                                           }else{

                                                           }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                })
                        }
                        else if(isSuccess == true && checkRequired(e)){

                                eventAccess.getSingleEventFromSubscribedList(userId,e, function (error, listObj, event) {

                                    if (checkRequired(listObj)) {
                                        if (checkRequired(listObj.eventsList)) {
                                            if (checkRequired(listObj.eventsList[0])) {
                                                if (listObj.eventsList[0].googleEvent == false) {

                                                    event.userId = userId;
                                                    createEventInGoogleCalendar(event,true,false,1,srvp)
                                                }
                                            }
                                        }
                                    }
                                })
                        }
                  })
              }
          }
      }
}

function createEventInGoogleCalendar(event,create,update,count,srvp){
    var eventDetails = event;
    //  var date = new Date();
    var userId = event.userId;
    var eventAccess = new  eventDataAccess();
    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1},function(error,userProfile) {

        if (error) {
            logger.error('Error occurred while searching authenticated user profile in the DB');

        }
        else{
            var userInfo=userProfile;
            if (!googleCalendar.validateUserGoogleAccount(userInfo)) {

            }
            else{
                var tokenProvider = new GoogleTokenProvider({
                    refresh_token: userInfo.google[0].refreshToken,
                    client_id:     authConfig.GOOGLE_CLIENT_ID,
                    client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                    access_token: userInfo.google[0].token
                });
                tokenProvider.getToken(function (err, token) {
                    if (err) {
                        logger.info('Error in token provider'+err);
                    }
                    var location = eventDetails.eventLocation ;

                    var id = eventDetails._id || simple_unique_id.generate('googleRelatasEvent');
                    var google_calendar = new gcal.GoogleCalendar(token);
                    var calendarId='primary';
                    var request_body = {

                        summary:eventDetails.eventName,
                        attendees:[
                            {
                                email:userInfo.google[0].emailId,
                                displayName:userInfo.firstName
                            }
                        ],
                        end:{
                            dateTime:new Date(eventDetails.endDateTime)
                        },
                        start:{
                            dateTime:new Date(eventDetails.startDateTime)
                        },
                        description:eventDetails.eventDescription || eventDetails.eventName,
                        location:location

                    };

                    if(create){
                        request_body.id = 'relatasevent'+id;
                        google_calendar.events.insert(calendarId,request_body,function(err,result){

                            if(err){

                                logger.error('creating event in google failed '+JSON.stringify(err));

                            }
                            else{
                                if(srvp){

                                }
                                var googleEventId = result.id;
                                if(checkRequired(googleEventId) && checkRequired(eventDetails._id) && checkRequired(userId)){

                                    eventAccess.updateSingleEventInSubscribedLisWithGoogleEventId(userId,eventDetails._id,googleEventId,1,srvp);
                                }
                            }
                        });
                    }
                    else if(update){

                        count += 1;

                        var eventId = 'relatasevent'+id
                        request_body.sequence=count;
                        google_calendar.events.update(calendarId,eventId,request_body,function(err,result){

                            if(err){
                                logger.info('Error in Google event update '+JSON.stringify(err));

                            }
                            else{
                                if(srvp){

                                }
                                var googleEventId = result.id;
                                if(checkRequired(googleEventId) && checkRequired(eventDetails._id) && checkRequired(userId)){
                                    eventAccess.updateSingleEventInSubscribedLisWithGoogleEventId(userId,eventDetails._id,googleEventId,count+1,srvp);
                                }
                            }
                        });
                    }
                }) // end of token provider
            }
        }
    });
}

function removeEventFromGoogleCalendar(googleEventId,eventId,userId,req,res,response,deletion){

    var eventAccess = new  eventDataAccess();
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for removing event in google calendar';

        userManagements.findUserProfileByIdWithCustomFields(userId,{google:1,emailId:1,firstName:1},function(error,userProfile){
            userLogInfo.functionName = 'selectUserProfile in remove event from google calendar';
            if(error){
                userLogInfo.error= error ;
                userLogInfo.info = 'Removing event from google failed, Error in mongo db while searching user profile using id';
                logger.info('Error in mongo',userLogInfo);
                if(response == false){

                }
                else{
                    res.send(false)
                }
            }
            else{

                var userInfo = userProfile;
                if(!googleCalendar.validateUserGoogleAccount(userInfo)){
                    logger.info('No Google account to remove event from google calendar',userLogInfo);
                    if(response == false){

                    }
                    else{
                        res.send(false)
                    }
                }
                else{
                    var tokenProvider = new GoogleTokenProvider({
                        refresh_token: userInfo.google[0].refreshToken,
                        client_id:     authConfig.GOOGLE_CLIENT_ID,
                        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                        access_token: userInfo.google[0].token
                    });
                    tokenProvider.getToken(function (err, token) {
                        if (err) {
                            userLogInfo.error = err;
                            userLogInfo.info = 'Error while refreshing token';

                            logger.info('Error in token provider module',userLogInfo);
                        }

                        var google_calendar = new gcal.GoogleCalendar(token);
                        var calendarId='primary';

                        google_calendar.events.delete(calendarId,googleEventId,function(err,result){

                            if(err){
                                userLogInfo.error = err;
                                userLogInfo.info = 'Error while deleting google event for user '+userProfile.firstName;

                                logger.info('Error in Google event delete',userLogInfo);

                                if(response == false){

                                }else
                                res.send(false);
                            }
                            else{
                                userLogInfo.info = 'Deleting event in google success'
                                logger.info('Deleting event in google success',userLogInfo);
                                if(response == false){
                                    if(deletion == true){
                                        eventAccess.removeEventFromUserSubscribeList(userId,eventId,function(error,isRemoved){

                                        })
                                    }
                                }
                                else{
                                    eventAccess.updateSingleEventInSubscribedLisWithGoogleEventDeleted(userId,eventId,function(error,isDeleted){

                                        if(response == false){

                                        }else
                                        res.send(true);
                                    })
                                }

                            }
                        });
                    }) // end of token provider
                }

            }
        })
}

function deleteEventFromAllUsersGoogleCalendar(eventId,req,res){
    var eventAccess = new  eventDataAccess();
    var googleEventId = 'relatasevent'+eventId;
    eventAccess.getAllUsersAddedThisEvent(eventId,function(error,list){
         if(checkRequired(list)){
             if(checkRequired(list[0])){
                 for(var i=0; i<list.length; i++){
                        removeEventFromGoogleCalendar(googleEventId,eventId,list[i].userId,req,res,false,true);
                 }
             }
         }
    })
}

function createBasicSubscribeToEventsAcc(userId,callback){
    var eventAccess = new  eventDataAccess();

      var SObject = {
          userId:userId,
          eventTypes:[],
          locations:[]
      }

    eventAccess.createSubscribeToEvents(SObject,function(error,details){
        if(checkRequired(details)){
            callback(true);
        }
        else{
            callback(false);
        }
    })
}

function removeDuplicates(arr){

    var end = arr.length;

    for(var i = 0; i < end; i++)
    {
        for(var j = i + 1; j < end; j++)
        {
            if(arr[i].userId == arr[j].userId)
            {
                var shiftLeft = j;
                for(var k = j+1; k < end; k++, shiftLeft++)
                {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

function getUserLogInfo(req){
    var userIp = getClientIp(req);
    var userLogInfo;
    if(req.session.userLogInfo){
        userLogInfo = req.session.userLogInfo;
    }
    else{
        userLogInfo = {
            userName:'',
            userIp:userIp,
            loginType:'',
            info:'',
            cause:'',
            action : ''
        }
        req.session.userLogInfo = userLogInfo;
    }
    return userLogInfo;
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
};

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }else
    {

        return res.send('loginRequired');
    }
}


function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

module.exports = router;