/*
 * relatas-application framework
 *
 */
var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var passport = require('passport');
var request = require('request');
var simple_unique_id = require('simple-unique-id');
var twitter = require('twitter');
var Twit = require('twit');
var fbapi = require('facebook-api');
var FB = require('fb');
var fs = require('fs');
var geoip = require('geoip-lite');
var moment = require('moment-timezone');

var userManagement = require('../dataAccess/userManagementDataAccess');
var emailSender = require('../public/javascripts/emailSender');
var validations = require('../public/javascripts/validation');
var appCredentials = require('../config/relatasConfiguration');
var commonUtility = require('../common/commonUtility');
var meetingSupportClass = require('../common/meetingSupportClass');
var winstonLog = require('../common/winstonLog');
var contactClass = require('../dataAccess/contactsManagementClass');

var contactObj = new contactClass();
var meetingSupportClassObj = new meetingSupportClass();
var common = new commonUtility();
var validation = new validations();
var emailSenders = new emailSender();
var appCredential = new appCredentials();
var userManagements = new userManagement();
var logLib = new winstonLog();

var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var logger =logLib.getWinston();
var linkedinErrorLog = logLib.getWinstonLinkedInError();
var domainName = domain.domainName;

var Linkedin = require('node-linkedin')(authConfig.LINKEDIN_CONSUMER_KEY, authConfig.LINKEDIN_CONSUMER_SECRET, authConfig.LINKEDIN_REDIRECT_URL);

router.get('/sendInviteLinkedinStatus',function(req,res){
    var details = {
        sendInviteLinkedinStatus:req.session.sendInviteLinkedinStatus,
        identity: req.session.uIdentity
    }
    if(req.session.sendInviteLinkedinStatus == 'userExist'){
        req.session.sendInviteLinkedinStatus = 'n'
        req.session.uIdentity = ''
    }
    res.send(details);
});

router.get('/schedulePage/isSefCalendar',isLoggedIn,function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(userId == req.session.publicProfileUserId){

        res.send(true);
    }else res.send(false);
})

router.get('/removeTimeslots',function(req,res){
    req.session.timeSlots = [];
    req.session.meetingDetails = null;
    req.session.doc = null;
    req.session.showMeeting = false;
    // req.session.isCalendarAuthorised = false;
    req.session.sendInviteLinkedinStatus = 'n'
    res.send(true)
});

router.get('/getContactByUserId/:userId/:otherUserId',function(req,res){

    var otherUserId = req.params.otherUserId;
    var userId = req.params.userId;
    userManagements.getSingleContactByUserId(userId,otherUserId,function(contact){
        if(checkRequred(contact) && checkRequred(contact.contacts) && checkRequred(contact.contacts[0])){
            res.send(true)
        }else res.send(false);
    })
})

router.get('/newInvitationsAsSender',isLoggedIn,function(req,res){

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    userManagements.newInvitationsAsSender(userId,function(eList){
        res.send(eList);
    })
});

// Router to get new invitations count
router.get('/newInvitationsAsReceiver',function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for new invitations with user id';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.newInvitationsAsReceiver(userId,function(error,invitations,message){
        userLogInfo.functionName = 'newInvitations in newInvitationsAsReceiver/:userId router';
        if(error){
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching new invitations using id';
            logger.info('Error in mongo',userLogInfo);
            res.send(invitations);
        }
        else{
            res.send(invitations)
        }
    });
});

// Router to get new invitations count
router.get('/newInvitations/:userId',function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for new invitations with user id';
    var userId = req.params.userId;

    if(userId == 'authenticated'){
        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret);
         userId = decoded.id;
    }

    userManagements.newInvitations(userId,function(error,invitations,message){
        userLogInfo.functionName = 'newInvitations in newInvitations/:userId router';
        if(error){
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching new invitations using id';
            logger.info('Error in mongo',userLogInfo);
            res.send(invitations);
        }
        else{
            res.send(invitations);

        }
    });
});

router.post('/sendInvitation',isLoggedInUserToSendInvitation,function(req,res){

    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request to send new invitation';
    var iDetails = req.body

    var invitationDetails = constructInvitationDetails(iDetails);

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    req.session.awsKey = invitationDetails.awsKey;

    userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,userProfile) {
        userLogInfo.functionName = 'selectUserProfile in sendInvitation router'
        if (error) {
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching user profile using id';
            logger.info('Error in mongo',userLogInfo);
            userProfile = null;
            res.send('error');

        }
        else{
            invitationDetails.invitationId = simple_unique_id.generate('invitationSecrete');
            invitationDetails.senderId = userProfile._id;
            invitationDetails.senderName = userProfile.firstName+" "+userProfile.lastName;
            invitationDetails.senderEmailId = userProfile.emailId;
            invitationDetails.senderPicUrl = userProfile.profilePicUrl;

            invitationDetails.participants.push({
                emailId:userProfile.emailId
            });

            validation.validateInvitationDetails(invitationDetails,function(status){
                if (status == true) {
                    userManagements.scheduleInvitation(invitationDetails, function(error, invitationData, message){
                        userLogInfo.functionName = 'scheduleInvitation in sendInvitation router'
                        if(error){
                            userLogInfo.error= error;
                            userLogInfo.info = 'Error in mongo db while storing new invitation';
                            logger.info('Error in mongo',userLogInfo);
                            res.send('error');
                        }
                        else if (!invitationData) {
                            res.send('error');
                        }
                        else{
                            req.session.timeSlots = [];
                            req.session.meetingDetails = null;
                            req.session.doc = null;
                            req.session.showMeeting = false;
                            req.session.sendInviteLinkedinStatus = 'n'
                            meetingSupportClassObj.sendNewMeetingRequestEmail(invitationData.invitationId,userId);
        //********* Confirmation email to Receiver  *************//
                           /* var maxSlot =  invitationData.scheduleTimeSlots.reduce(function (a, b) { return new Date(a.start.date) < new Date(b.start.date) ? a : b; });
                            var timezone = iDetails.receiverTimezone || iDetails.senderTimezone || 'UTC';
                            var start = moment(maxSlot.start.date).tz(timezone);
                            var date = start.format("DD-MMM-YYYY");
                            var time = start.format("h:mm a z");
                            var mLocation = maxSlot.locationType+':'+maxSlot.location;
                            var mAgenda = maxSlot.description;
                            var mTitle = maxSlot.title;
                            var companyName = userProfile.companyName || '';
                            var designation = userProfile.designation || '';

                            var data = {
                                invitationId:invitationData.invitationId,
                                receiverEmailId:invitationData.to.receiverEmailId,
                                receiverName:invitationData.to.receiverName,
                                senderName:invitationData.senderName,
                                senderId:invitationData.senderId,
                                receiverId:invitationData.to.receiverId,
                                mDate:date,
                                mTime:time,
                                mLocation:mLocation,
                                mAgenda:mAgenda,
                                mTitle:mTitle,
                                senderLocation:userProfile.location || '',
                                timezone:timezone,
                                senderCompany:designation+', '+companyName,
                                senderPicUrl:domainName +'/getImage/'+invitationData.senderId
                            };
                            common.sendMeetingInvitationMailToReceiver(data,false);

        //********* Confirmation email to Sender  *************
                            sendNewTemplateToSender(invitationData,maxSlot,iDetails.senderTimezone,true);*/

                            var aws = req.session.awsKey;
                            storeDocumentInfo(invitationData,aws,userLogInfo);
                            var contact = {
                                personId:invitationData.to.receiverId || '',
                                personName:invitationData.to.receiverName,
                                personEmailId:invitationData.to.receiverEmailId || '',
                                count:1,
                                lastInteracted:new Date(),
                                addedDate: new Date(),
                                verified:false,
                                relatasContact:true,
                                relatasUser:true
                            };
                            addContact(userId,contact,userLogInfo);
                            res.send('success');
                        }
                    })
                }
                else res.send(status);
            })
        }
    });
});

function sendNewTemplateToSender(invitationData,maxSlot,senderTimezone,defaultEmail){
    userManagements.findUserProfileByIdWithCustomFields(invitationData.to.receiverId,{contacts:0,google:0,linkedin:0,twitter:0,officeCalendar:0,currentLocation:0,calendarAccess:0},function(error,receiver){
        if(common.checkRequired(receiver)){
            var timezone2 = senderTimezone || 'UTC';
            var start = moment(maxSlot.start.date).tz(timezone2);
            var date = start.format("DD-MMM-YYYY");
            var time = start.format("h:mm a z");
            var mLocation = maxSlot.locationType+':'+maxSlot.location;
            var mAgenda = maxSlot.description;
            var mTitle = maxSlot.title;
            var companyName = receiver.companyName || '';
            var designation = receiver.designation || '';

            var data = {
                invitationId:invitationData.invitationId,
                receiverEmailId:invitationData.senderEmailId,
                receiverName:invitationData.senderName,
                senderName:invitationData.to.receiverName,
                senderId:invitationData.to.receiverId,
                receiverId:invitationData.senderId,
                mDate:date,
                mTime:time,
                mLocation:mLocation,
                mAgenda:mAgenda,
                mTitle:mTitle,
                senderLocation:receiver.location || '',
                timezone:timezone2,
                senderCompany:designation+', '+companyName,
                senderPicUrl:domainName +'/getImage/'+receiver._id
            };
            common.sendMeetingInvitationMailToSender(data);
        }
        else{
            if(defaultEmail)
                emailSenders.sendInvitationMailToSender(invitationData);
        }
    });
}

router.post('/sendInvitationSelf',isLoggedInUserToSendInvitation,function(req,res){

    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request to send new self invitation';
    var iDetails = req.body;

    constructInvitationDetailsSelf(iDetails,function( invitationDetails ){

        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret);
        var userId = decoded.id;

        req.session.awsKey = invitationDetails.awsKey;
        userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,userProfile) {
            userLogInfo.functionName = 'selectUserProfile in sendInvitationSelf router'
            if (error) {
                userLogInfo.error= error;
                userLogInfo.info = 'Error in mongo db while searching user profile using id';
                logger.info('Error in mongo',userLogInfo);
                userProfile = null;
                res.send('error');
            }
            else{
                invitationDetails.invitationId = simple_unique_id.generate('invitationSecrete');
                invitationDetails.senderId = userProfile._id;
                invitationDetails.senderName = userProfile.firstName+" "+userProfile.lastName;
                invitationDetails.senderEmailId = userProfile.emailId;
                invitationDetails.senderPicUrl = userProfile.profilePicUrl;
                var isExist = false;
                for(var i=0; i<invitationDetails.participants.length; i++){
                    if(invitationDetails.participants[i].emailId == userProfile.emailId){
                        isExist = true;
                    }
                }
                if(!isExist){
                    invitationDetails.participants.push({
                        emailId:userProfile.emailId
                    })
                }

                userManagements.scheduleInvitationSelf(invitationDetails, function(error, invitationData, message){
                    userLogInfo.functionName = 'scheduleInvitation in sendInvitation router'
                    if(error){
                        userLogInfo.error= error;
                        userLogInfo.info = 'Error in mongo db while storing new invitation';
                        logger.info('Error in mongo',userLogInfo);
                        res.send('error');
                    }
                    else if (!invitationData) {
                        res.send('error');
                    }
                    else{
                        req.session.timeSlots = [];
                        req.session.meetingDetails = null;
                        req.session.doc = null;
                        req.session.showMeeting = false;
                        req.session.sendInviteLinkedinStatus = 'n';
                        meetingSupportClassObj.sendNewMeetingRequestEmail(invitationData.invitationId,userId);
                        var maxSlot =  invitationData.scheduleTimeSlots.reduce(function (a, b) { return new Date(a.start.date) < new Date(b.start.date) ? a : b; });

                        var companyName = userProfile.companyName || '';
                        var designation = userProfile.designation || '';

                        for(var i=0; i<invitationData.toList.length; i++){

                            var receiverNameF = invitationData.toList[i].receiverFirstName || '';
                            var receiverNameL = invitationData.toList[i].receiverLastName || '';
                            var rfn = receiverNameF+' '+receiverNameL;
                            var senderName = invitationData.senderName;
                            var data = {
                                invitationId:invitationData.invitationId,
                                receiverEmailId:invitationData.toList[i].receiverEmailId,
                                senderId:invitationData.senderId,
                                receiverId:invitationData.toList[i].receiverId,
                                receiverName:rfn,
                                senderName:senderName,
                                senderLocation:userProfile.location || '',
                               // timezone:timezone,
                                senderCompany:designation+', '+companyName,
                                senderPicUrl:domainName +'/getImage/'+invitationData.senderId
                            };
                            updateContactsByEmail(invitationData.senderId,data);
                            //common.sendMeetingInvitationMailToReceiver(data,false);
                        }
                        /*if(invitationData.toList.length == 1 && common.checkRequired(invitationData.toList[0].receiverId)){
                            var nameF = invitationData.toList[0].receiverFirstName || '';
                            var nameL = invitationData.toList[0].receiverLastName || '';
                            var fn = nameF+' '+nameL;
                            invitationData.to = {
                                receiverName:fn,
                                receiverId:invitationData.toList[0].receiverId,
                                receiverEmailId:invitationData.toList[0].receiverEmailId
                            };
                            sendNewTemplateToSender(invitationData,maxSlot,iDetails.senderTimezone,false);
                        }
                        else{
                            invitationData.to.receiverName = '';
                            invitationData.selfCalendar = true;
                            for(var j=0; j<invitationData.toList.length; j++){
                                invitationData.to.receiverName += invitationData.toList[j].receiverEmailId;
                                if(j == invitationData.toList.length-1){

                                }else invitationData.to.receiverName += '<br>'
                            }
                            emailSenders.sendInvitationMailToSender(invitationData);
                        }*/

                        var aws = req.session.awsKey;
                        storeDocumentInfoSelf(invitationData,aws,userLogInfo);
                        res.send('success');
                    }
                })
            }
        });
    });
});

// Router to send invitation using email
router.post('/sendInvitationWithMail',function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request to send new invitation with email';
    var iDetails = req.body

    var ipAddr = getClientIp(req);
    var geo = geoip.lookup(ipAddr.toString());
    var currentLocation = getCurrentLocation(geo);

    var senderName = iDetails.senderName;
    var senderEmailId = iDetails.senderEmailId;
    var invitationDetails = constructInvitationDetails(iDetails);

    invitationDetails.invitationId = simple_unique_id.generate('invitationSecrete');
    invitationDetails.meetingLocation = currentLocation;
    invitationDetails.senderId = null;

    invitationDetails.senderName = senderName;
    invitationDetails.senderEmailId = senderEmailId;
    invitationDetails.senderPicUrl = null;

    invitationDetails.participants.push({
        emailId:senderEmailId
    });

    validation.validateInvitationDetailsEmail(invitationDetails,function(status){

        if (status == true) {
            userManagements.scheduleInvitation(invitationDetails, function(error, invitationData, message){
                userLogInfo.functionName = 'scheduleInvitation in sendInvitationWithMail router'
                if(error){
                    userLogInfo.error= error;
                    userLogInfo.info = 'Error in mongo db while storing new invitation with email';
                    logger.info('Error in mongo',userLogInfo);
                    res.send('error');
                }
                else if (!invitationData) {
                    res.send('error');
                }
                else{
                    req.session.timeSlots = [];
                    req.session.meetingDetails = null;
                    req.session.doc = null;
                    req.session.showMeeting = false;

                    req.session.sendInviteLinkedinStatus = 'n'
                    emailSenders.sendInvitationMailToSender(invitationData);
                    emailSenders.sendInvitationMailToReceiver(invitationData);

                    res.send('success');
                }
            })
        }
        else res.send(status);
    })
});

router.post('/showMeetingTrue',function(req,res){
    var details = req.body;

    req.session.showMeeting = details.showMeeting

    res.send(true)
});

router.get('/getShowMeeting',function(req,res){

    if (req.session.showMeeting) {
        res.send(true)
    }
    else res.send(false)
});

router.post('/storeMeetingDetails',function(req,res){
    // var meetingDetails = req.body;
    req.session.meetingDetails = req.body;

    res.send(true);
});

router.get('/getMeetingDetails',function(req,res){
    res.send(req.session.meetingDetails);
});

router.get('/isCalendarAuthorised',function(req,res){

    if(req.session.isCalendarAuthorised == undefined){
        res.send(null);
    }else res.send(req.session.isCalendarAuthorised);
});

router.post('/loginFromPublicProfile',function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for login from schedule page';
    userLogInfo.routerName = 'loginFromPublicProfile ';
    var details = req.body;

    userManagements.isValidRelatasUser(details.emailId, details.password, function (err, user, message) {
        userLogInfo.functionName = 'isValidRelatasUser in loginFromPublicProfile router'
        if (err) {
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching user with email';
            logger.info('Error in mongo',userLogInfo);
            res.send(false);
        }
        if (!user) {
            userLogInfo.info = 'Email id or password miss match'
            logger.error('user credentials is failed in password LocalStrategy',userLogInfo);
            res.send(false);
        }
        else{
            var tokenSecret ="alpha";
            var token = jwt.encode({ id: user._id}, tokenSecret);
            var user = token;
            req.session.dashboardMsg = true;
            req.session.userLoginFlag = true;
            req.logIn(user, function(err)
            {
                common.insertLog(user,'schedule-page',req);
                if (err) {
                    userLogInfo.info = 'Email & Password login fail';

                    userLogInfo.error = err;
                    logger.info('Email login failed',userLogInfo);
                    res.send(false);
                }
                userLogInfo.info = 'Email & Password login success';
                logger.info('user credentials is success in password authentication',userLogInfo);

                res.send(true);
            });
        }
    });
});

router.get('/isLoggedInUser',isLoggedIn, function(req,res){
    res.send(true);
});



router.post('/storeDocDetailsInSession',function(req,res){
    var doc = {
        documentName: req.body.docName,
        documentUrl: req.body.docUrl
    }
    req.session.doc = doc;

    res.send(true);
});

router.get('/fileName',function(req,res){

    if(req.session.doc){
        res.send(req.session.doc)
    }
    else res.send(false)
});

router.post('/timeSlots',function(req,res){

    if (!req.session.timeSlots) {
        req.session.timeSlots = []
        req.session.timeSlots.push(req.body);

        res.send(true);
    }else{

        if (req.session.timeSlots.length <3) {
            req.session.timeSlots.push(req.body);
            res.send(true);
        }
        else{
            res.send(false)
        }
    }
});



router.post('/updateTimeSlots',function(req,res){
    var details = req.body;
    var id = details.id.split(' ')
    var slotNo = parseInt(id[1])
    var times = req.session.timeSlots
    times[slotNo-1] = details
    req.session.timeSlots = times;

    res.send(true)
});

router.post('/schedule/slot/remove',function(req,res){
    var data = req.body;
    var slotId = data.slotId;
    var newSlots = [];
    var idss = ['_ 1 !','_ 2 #','_ 3 $']
    if(common.checkRequired(slotId)){
        var times = req.session.timeSlots;
        for(var i=0;i<times.length;i++){
            if(times[i].id != slotId){
                newSlots.push(times[i])
            }
        }
        for(var j=0; j<newSlots.length; j++){
            newSlots[j].id = idss[j]
        }
        req.session.timeSlots = newSlots;
        res.send(true);
    }
    else res.send(false);
});

router.post('/removeTimeSlots',function(req,res){
    var count  ;
    var t = []

    var idss = ['_ 1 !','_ 2 #','_ 3 $']
    var details = req.body;
    var id = details.id.split(' ')
    var slotNo = parseInt(id[1])
    var times = req.session.timeSlots
    times[slotNo-1] = null
    for(var i=0;i<times.length;i++){
        if(times[i]==null){
            count = i
        }
        else{
            t.push(times[i])
        }
    }
    for(var j=0;j<t.length;j++){
        t[j].id = idss[j]
    }

    req.session.timeSlots = t;

    res.send(true)
});

router.get('/getTimeSlots',function(req,res){

    res.send(req.session.timeSlots)
});

router.get('/clearTimeSlots',function(req,res){
    req.session.timeSlots = [];
    req.session.meetingDetails = null;
    req.session.doc = null;
    req.session.showMeeting = false;
    // req.session.isCalendarAuthorised = false;
    req.session.sendInviteLinkedinStatus = 'n'
    res.send(true)
});


// Router to get common connections
router.post('/getCommonConnections',isLoggedIn, function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request to get linkedin common connections';
    userLogInfo.routerName = 'getCommonConnections ';

    var publicUserId = req.body.id;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    // This is to get authenticated user profile
    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,linkedin:1,firstName:1,lastName:1},function(error,authenticatedUserProfile){
        userLogInfo.functionName = 'selectUserProfile in getCommonConnections router'
        if(error){
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching user profile using id';
            logger.info('Error in mongo',userLogInfo);
            res.send({message:'No common connections found'})

        }
        else if(checkRequred(authenticatedUserProfile) && checkRequred(authenticatedUserProfile.linkedin) && checkRequred(authenticatedUserProfile.linkedin.id)){

                // This is to get Public profile
                userManagements.findUserProfileByIdWithCustomFields(publicUserId,{emailId:1,linkedin:1,firstName:1,lastName:1},function(error,publicUserProfile){
                    userLogInfo.functionName = 'selectUserProfile to find authenticated user profile in getCommonConnections router'
                    if(error){
                        userLogInfo.error= error;
                        userLogInfo.info = 'Error in mongo db while searching user profile using id';
                        logger.info('Error in mongo',userLogInfo);
                        res.send({message:'No common connections found'})
                    }
                    else if(checkRequred(publicUserProfile) && checkRequred(publicUserProfile.linkedin) && checkRequred(publicUserProfile.linkedin.id)){

                            getCommonConnections(authenticatedUserProfile,publicUserProfile,req,res);
                    }
                    else{
                        var name = 'User';
                        if(checkRequred(publicUserProfile) && checkRequred(publicUserProfile.firstName)){
                            name = publicUserProfile.firstName;
                        }

                        logger.info(name+' has not added his LinkedIn profile on Relatas')
                        res.send({message:name+' has not added his LinkedIn profile on Relatas'})
                    }
                });
        }
        else{
            logger.info(authenticatedUserProfile.emailId+' has not added his LinkedIn profile on Relatas');
            res.send({message:'You have not added your LinkedIn profile on Relatas'});
        }
    });
});

// Router to get latest post using linkedin user token
router.get('/linkedinStatus/:userId',isLoggedIn, function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request to get linkedin status';
    var publicUserId = req.params.userId;

    // This is to get authenticated user profile
    userManagements.findUserProfileByIdWithCustomFields(publicUserId,{emailId:1,linkedin:1,firstName:1,lastName:1},function(error,publicUserProfile){
        userLogInfo.functionName = 'selectUserProfile in linkedinStatus router'
        if (error) {
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching user profile using id';
            logger.info('Error in mongo',userLogInfo);
            res.send({message:'No latest post found on linkedin'})
        }
        else{
            if(checkRequred(publicUserProfile)){
                if (checkRequred(publicUserProfile.linkedin.token)) {
                    var token = publicUserProfile.linkedin.token

                    // Options to get loggedin user status
                    var connectionOptions = {
                        url: 'https://api.linkedin.com/v1/people/~/network/updates?scope=self&count=30&start=0',
                        headers: { 'x-li-format': 'json' },
                        qs: { oauth2_access_token: token }
                    };

                    request(connectionOptions, function ( error, r, status ) {
                        var statusUpdate = JSON.parse(status)

                        if ( r.statusCode != 200 ) {
                            userLogInfo.error= error;
                            userLogInfo.info = 'Error in linkedin api, for lates status';
                            logger.info('Error in linkedin api',userLogInfo);
                            res.send({message:'No latest post found on linkedin'})
                        }
                        else{

                            if (checkRequred(statusUpdate.values) && !statusUpdate.values.length == 0) {

                                var status = {
                                    id:publicUserProfile._id,
                                    userId:publicUserProfile._id,
                                    userEmailId:publicUserProfile.emailId,
                                    userFirstName:publicUserProfile.firstName,
                                    linkedin:statusUpdate.values
                                }

                                userManagements.storeLinkedinStatus(status,function(error,isSuccess,message){
                                    if(error){
                                        userLogInfo.error= error;
                                        userLogInfo.info = 'Error in mongo db while storing linkedin status';
                                        logger.info('Error in mongo',userLogInfo);

                                    }

                                })
                                res.send(statusUpdate.values[0])
                            }
                        }
                    })
                }
            }

        }
    })
});

router.get('/linkedinLike/:updateKey',isLoggedIn,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request to like linkedin post';
    var updateKey = req.params.updateKey;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    // This is to get authenticated user profile
    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,linkedin:1},function(error,authenticatedUserProfile){
        if (error) {
            logger.info('error while reading token from DB (in linkedin like)')
            res.send({message:'error while reading token from DB'})
        }
        else{
            if(checkRequred(authenticatedUserProfile)){
                if (checkRequred(authenticatedUserProfile.linkedin.token)) {
                    var token = authenticatedUserProfile.linkedin.token
                    var likeOptions = {
                        url: 'http://api.linkedin.com/v1/people/~/network/updates/key='+updateKey+'/is-liked',
                        headers: { 'x-li-format': 'json' },
                        qs: { oauth2_access_token: token } ,
                        body:{
                            isLiked:true
                        }
                    };

                    request.put(likeOptions, function ( error, r, status ) {

                        if ( r.statusCode != 201 ) {
                            logger.info(error)
                            //res.send({message:'Error in like '})
                            res.send(false)
                        }
                        else{
                            res.send(true)
                        }
                    })
                }
                else res.send({message:'Your linkedin credentials is invalid'})
            }

        }
    })
});

router.post('/linkedinComment',function(req,res){

});

router.post('/linkedinPro',isLoggedIn,function(req,res){

    var userId = req.body.id;

    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,linkedin:1,firstName:1,lastName:1},function(error,authenticatedUserProfile){
        if (error) {
            linkedinErrorLog.info('error while reading token from DB '+error);
            res.send({message:'error while reading token from DB'})
        }
        else{
            if(checkRequred(authenticatedUserProfile)){
                if (checkRequred(authenticatedUserProfile.linkedin.token)) {
                    var linkedIn = Linkedin.init(authenticatedUserProfile.linkedin.token);
                    linkedIn.people.me(['first-name','last-name','headline','educations'], function(err, profile) {
                       if(err || !checkRequred(profile)){
                           linkedinErrorLog.info('error in getting linkedin profile of 1 '+authenticatedUserProfile.emailId+' error '+JSON.stringify(profile));
                           res.send({message:'Error in getting your linkedin connections'})
                       }
                       else{
                           if(!checkRequred(profile.educations)){
                               linkedinErrorLog.info('error in getting linkedin profile of 2 '+authenticatedUserProfile.emailId+' error '+JSON.stringify(profile));
                               res.send({message:'Error in getting your linkedin profile'})

                           }
                           else{
                               res.send(profile);
                           }
                       }
                    });
                }
                else res.send({message:'Linkedin credentials is invalid'})
            }
        }
    })
});

router.post('/getTwitterInfo', isLoggedIn, function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request to get twitter info';

    var publicUserId = req.body.id;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
   // This is to get Public profile
    userManagements.findUserProfileByIdWithCustomFields(publicUserId,{emailId:1,twitter:1,firstName:1,lastName:1},function(error,publicUserProfile){
        userLogInfo.functionName = 'selectUserProfile public user profile in getTwitterInfo router'
        if (error) {
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching user profile using id';
            logger.info('Error in mongo',userLogInfo);
            res.send({message:'No latest post found on Twitter'})
        }
        else
        if(checkRequred(publicUserProfile)){
            if (checkRequred(publicUserProfile.twitter.id)) {

                getTwitterInfo(publicUserProfile,req,res);

            }
            else{
                if (req.body.requestFrom) {
                    logger.info('Sender does not have twitter account')
                    res.send({message:'Sender does not have twitter account'})
                }
                else{
                    logger.info('Public profile user does not have Twitter account')
                    res.send({message:'No Twitter account found for public user'})
                }
            }
        }
    });


});

router.post('/getFacebookInfo',isLoggedIn, function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request to get facebook info';
    var publicUserId = req.body.id;

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    // This is to get authenticated user profile
    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,facebook:1,firstName:1,lastName:1},function(error,authenticatedUserProfile){
        userLogInfo.functionName = 'selectUserProfile in getFacebookInfo router';
        if (error) {
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching user profile using id';
            logger.info('Error in mongo',userLogInfo);
            res.send({message:'No latest post found on Facebook'})
        }
        else
        if(checkRequred(authenticatedUserProfile)){
            if (checkRequred(authenticatedUserProfile.facebook.id)) {

                // This is to get Public profile
                userManagements.findUserProfileByIdWithCustomFields(publicUserId,{emailId:1,facebook:1,firstName:1,lastName:1},function(error,publicUserProfile){
                    userLogInfo.functionName = 'selectUserProfile for public user profile in getFacebookInfo router';
                    if (error) {
                        userLogInfo.error= error;
                        userLogInfo.info = 'Error in mongo db while searching user profile using id';
                        logger.info('Error in mongo',userLogInfo);
                        res.send({message:'No latest post found on Facebook'})
                    }
                    else
                    if(checkRequred(publicUserProfile)){
                        if (checkRequred(publicUserProfile.facebook.id)) {

                            getFacebookInfo(authenticatedUserProfile,publicUserProfile,req,res);

                        }
                        else{
                            if (req.body.requestFrom) {
                                logger.info('Sender does not have facebook account')
                                res.send({message:'Sender does not have facebook account'})
                            }
                            else{
                                logger.info('Public profile user does not have facebook account')
                                res.send({message:'No facebook account found for public user'})
                            }
                        }
                    }

                });
            }
            else{
                if (req.body.requestFrom) {
                    logger.info('Authenticated user does not have facebook account')
                    res.send({message:'No facebook account associated with your relatas account'})
                }
                else{
                    logger.info('Authenticated user does not have facebook account')
                    res.send({message:'No facebook account associated with your relatas account'})
                }
            }
        }

    });
});

// Router to get facebook feeds
router.post('/facebookStatus',isLoggedIn, function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request to get facebook status';
    var userId = req.body.id

    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,facebook:1,firstName:1,lastName:1},function(error,publicUserProfile){
        userLogInfo.functionName = 'selectUserProfile in facebookStatus router';
        if (error) {
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching user profile using id';
            logger.info('Error in mongo',userLogInfo);
            res.send({message:'No latest post found on Facebook'})
        }
        else{
            if(checkRequred(publicUserProfile)){
                if (checkRequred(publicUserProfile.facebook.token)) {
                    var token = publicUserProfile.facebook.token
                    FB.api('me/feed?access_token='+token+'&limit=30', function (result) {
                        if(!result || result.error) {
                            userLogInfo.error = result.error
                            logger.info('error occurred infacebook FB api',userLogInfo);
                            res.send({message:'No latest post found on Facebook'})
                        }
                        else{
                            if (!checkRequred(result.data) && !checkRequred(result.data[0])) {
                                res.send({message:'No latest post found on Facebook'})
                            }
                            else{
                                var status={
                                    id:publicUserProfile._id,
                                    userId:publicUserProfile._id,
                                    userEmailId:publicUserProfile.emailId,
                                    userFirstName:publicUserProfile.firstName,
                                    facebook:result.data
                                }
                                userManagements.storeFacebookStatus(status,function(error,isSuccess,message){
                                    if(error){
                                        userLogInfo.error= error;
                                        userLogInfo.info = 'Error in mongo db while storing Facebook status';
                                        logger.info('Error in mongo',userLogInfo);

                                    }
                                })
                                res.send(result.data[0])
                            }
                        }
                    })
                }
                else res.send({message:'Requested facebook credentials is invalid'})
            }

        }
    })
});


function isLoggedIn(req, res, next){
    // if user is authenticated in the session, carry on

    if (req.isAuthenticated()){

        return next();
    }else
    {
        res.send(false);
    }
}

function isLoggedInUserToSendInvitation(req, res, next) {



    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }else
    {

        // if they aren't send false to login

        res.send('loginRequired');
    }
}

// Function to develop invitation details JSON
function constructInvitationDetails(data){
    var invitationDetails = {
        senderId:'',
        senderName:'',
        senderEmailId:'',
        scheduledDate:data.scheduledDate,
        awsKey:data.awsKey,
        to:{
            receiverId:data.receiverId,
            receiverName:data.receiverName,
            receiverEmailId:data.receiverEmailId
        },
        scheduleTimeSlots:[],
        docs:[],
        participants:[
            {
                emailId:data.receiverEmailId
            }
        ]
    };

    if(checkRequred(data.documentName) && checkRequred(data.documentUrl)){
        invitationDetails.docs.push({
            documentName:data.documentName,
            documentUrl:data.documentUrl
        })
    }

    if (checkRequred(data.start1)) {
        var slotOne = {
            start:{
                date      : data.start1
            },
            end:{
                date      : data.end1
            },
            title:data.title1,
            location:data.location1,
            locationType:data.locationType1,
            description:data.description1
        }
        invitationDetails.scheduleTimeSlots.push(slotOne);
    }

    if (checkRequred(data.start2)) {
        var slotTwo = {
            start:{
                date      : data.start2
            },
            end:{
                date      : data.end2
            },
            title:data.title2,
            location:data.location2,
            locationType:data.locationType2,
            description:data.description2
        }
        invitationDetails.scheduleTimeSlots.push(slotTwo);
    }

    if (checkRequred(data.start3)) {
        var slotThree = {
            start:{
                date      : data.start3
            },
            end:{
                date      : data.end3
            },
            title:data.title3,
            location:data.location3,
            locationType:data.locationType3,
            description:data.description3
        }
        invitationDetails.scheduleTimeSlots.push(slotThree);
    }

    return invitationDetails;
}

// Function to develop invitation details JSON
function constructInvitationDetailsSelf(data,callback){

    var invitationDetails = {
        senderId:'',
        senderName:'',
        senderEmailId:'',
        scheduledDate:data.scheduledDate || new Date(),
        awsKey:data.awsKey,
        toList:[],
        scheduleTimeSlots:[],
        docs:[],
        participants:[]
    }
    var one,two,three;
    var toList = [];

    if(data.toList){
        toList =  JSON.parse(data.toList);

        for(var j=0; j<toList.length; j++){
            getUserListObj(toList[j].receiverEmailId,function(obj){
                invitationDetails.toList.push(obj);
            })
        }

        if(toList.length == 1){
            one = true;
            two = true;
            three = true;
        }else{
            one = true;
            two = false;
            three = false;
        }
    }

    for(var i=0; i<toList.length; i++){
        var participant = {
            emailId:toList[i].receiverEmailId
        }
        invitationDetails.participants.push(participant);
    }

    if(checkRequred(data.documentName) && checkRequred(data.documentUrl)){
        invitationDetails.docs.push({
            documentName:data.documentName,
            documentUrl:data.documentUrl
        })
    }

    if (checkRequred(data.start1) && one) {
        var slotOne = {
            start:{
                date      : data.start1
            },
            end:{
                date      : data.end1
            },
            title:data.title1,
            location:data.location1,
            locationType:data.locationType1,
            description:data.description1
        }
        invitationDetails.scheduleTimeSlots.push(slotOne);
    }

    if (checkRequred(data.start2) && two) {
        var slotTwo = {
            start:{
                date      : data.start2
            },
            end:{
                date      : data.end2
            },
            title:data.title2,
            location:data.location2,
            locationType:data.locationType2,
            description:data.description2
        }
        invitationDetails.scheduleTimeSlots.push(slotTwo);
    }

    if (checkRequred(data.start3) && three) {
        var slotThree = {
            start:{
                date      : data.start3
            },
            end:{
                date      : data.end3
            },
            title:data.title3,
            location:data.location3,
            locationType:data.locationType3,
            description:data.description3
        }
        invitationDetails.scheduleTimeSlots.push(slotThree);
    }
    setTimeout(function(){
        callback(invitationDetails);
    },toList.length*150)
}

function getUserListObj(receiverEmailId,callback){

    userManagements.findUserProfileByEmailIdWithCustomFields(receiverEmailId,{firstName:1,lastName:1,emailId:1,timezone:1},function(error,user){

        if(error || !checkRequred(user)){
            callback({
                receiverEmailId   :receiverEmailId,
                isAccepted:false
            })
        }else{
            var lObj = {
                receiverId        : user._id,
                receiverFirstName : user.firstName,
                receiverLastName  : user.lastName,
                receiverEmailId   : user.emailId,
                isAccepted:false
            }
            if (user.timezone) {
                if (user.timezone.name) {
                    lObj.timezone = user.timezone.name
                }
            }
            callback(lObj);
        }
    })
}

// Function to get common connections
function getCommonConnections(authProfile,PublicProfile,req,res){

    var linkedinAuthUser = Linkedin.init(authProfile.linkedin.token);
    linkedinAuthUser.people.id(PublicProfile.linkedin.id, ['id','public-profile-url','first-name', 'last-name','current-share','headline','positions','relation-to-viewer:(related-connections:(id,first-name,last-name,siteStandardProfileRequest,pictureUrl))'], function(err, data) {

        if(checkRequred(data)){
            if(data.errorCode == 0 || data.errorCode == '0'){
                linkedinErrorLog.info('error in getting linkedin common connections of '+authProfile.emailId+' error '+JSON.stringify(data));
                res.send({message:'No common connections found'});
            }
            else{
                res.send(data)
            }
        }
    });
}

function getTwitterInfo(PublicProfile,req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request to get latest twitter status';

    userLogInfo.functionName = 'getTwitterInfo in getTwitterInfo router';
    var tweets = [];

    try{
        var T = new Twit({
            consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
            consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
            access_token:        PublicProfile.twitter.token,
            access_token_secret:  PublicProfile.twitter.refreshToken
        })

        T.get('statuses/user_timeline', function(err, data, response) {


            if (err) {
                userLogInfo.error = err;
                userLogInfo.info = ''
                logger.info('error occurred while requesting latest tweets, in Twit module',userLogInfo);
                res.send({message:'No latest tweet found'})

            }
            else{
                if (!checkRequred(data) && !checkRequred(data[0])) {
                    res.send({message:'No latest tweet found'});
                }
                else{
                    var status = {
                        id: PublicProfile._id,
                        userId: PublicProfile._id,
                        userEmailId: PublicProfile.emailId,
                        userFirstName: PublicProfile.firstName,
                        twitter:data.length <= 30 ? data : getThirtyTweets(data)
                    }

                    userManagements.storeTwitterStatus(status,function(error,isSuccess,message){
                        userLogInfo.functionName += ' inner function storeTwitterStatus'
                        if(error){
                            userLogInfo.error= error;
                            userLogInfo.info = 'Error in mongo db while storing twitter tweets';
                            logger.info('Error in mongo',userLogInfo);
                        }
                    })

                    if (data.length <=5) {
                        res.send(data);
                    }
                    else{
                        for(var i=0; i<data.length; i++){
                            tweets.push(data[i]);
                            if (i == 5) {
                                tweets.push(data[i]);
                                res.send(tweets)
                            }
                        }
                    }
                }
            }
        })
    }
    catch(e){
        userLogInfo.error = e;
        logger.info('Exception',userLogInfo)
    }
}

function getFacebookInfo(authProfile,PublicProfile,req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request to get Facebook common friends';

    userLogInfo.functionName = 'getFacebookInfo in getFacebookInfo router';
    var authFriends,publicFriends;
    var commonFriends = []

    try{

        FB.api('me/friends?access_token=' + authProfile.facebook.token, function (result) {


            if(!result || result.error) {
                userLogInfo.error = result.error
                logger.info('error occurred in facebook FB api, logged in user',userLogInfo);

                res.send({message:'No friend list found on Facebook'})
            }
            else{
                authFriends = result.data;

                FB.api('me/friends?access_token=' + PublicProfile.facebook.token, function (result) {

                    if(!result || result.error) {
                        userLogInfo.error = result.error
                        logger.info('error occurred in facebook FB api, public user',userLogInfo);

                        res.send({message:'No friend list found on Facebook'})
                    }
                    else{
                        publicFriends = result.data;
                        if (authFriends == null || authFriends == undefined || authFriends[0] == undefined) {
                            res.send({message:'No friend list found on Facebook'})
                        }
                        else if (publicFriends == null || publicFriends == undefined || publicFriends[0] == undefined) {
                            res.send({message:'No friend list found on Facebook'})
                        }
                        else{
                            authFriends.forEach(function(authFriend){
                                publicFriends.forEach(function(publicFriend){

                                    if (authFriend.id == publicFriend.id) {
                                        if (commonFriends.length <=2) {
                                            commonFriends.push(authFriend)
                                        }
                                    }
                                })
                            })
                            res.send(commonFriends)
                        }
                    }
                })
            }
        });
    }
    catch(e){
        userLogInfo.error = e;
        logger.info('Exception',userLogInfo);
    }

}

// Function to store document info
function storeDocumentInfoSelf(invitation,aws,userLogInfo){
    userLogInfo.functionName = 'storeDocumentInfo in sendInvitation router';

    if(checkRequred(invitation.docs[0])){
        if(checkRequred(invitation.docs[0].documentName)){

            var docDetails = {
                sharedDate: invitation.scheduledDate,
                awsKey:aws,
                sharedBy:{
                    userId:invitation.senderId,
                    emailId:invitation.senderEmailId,
                    firstName:invitation.senderName
                },
                sharedWith:[],
                documentName:invitation.docs[0].documentName,
                documentUrl:invitation.docs[0].documentUrl
            };

            for(var i=0; i<invitation.toList.length; i++){
                docDetails.sharedWith.push({
                    emailId:invitation.toList[i].receiverEmailId,
                    sharedOn:invitation.scheduledDate,
                    accessStatus:true
                })
            }

            storeDocumentFinal(docDetails,invitation,aws,userLogInfo);
        }
    }

}

// Function to store document info
function storeDocumentInfo(invitation,aws,userLogInfo){
    userLogInfo.functionName = 'storeDocumentInfo in sendInvitation router';

    if(checkRequred(invitation.docs[0])){
        if(checkRequred(invitation.docs[0].documentName)){

            var docDetails = {
                sharedDate: invitation.scheduledDate,
                awsKey:aws,
                sharedBy:{
                    userId:invitation.senderId,
                    emailId:invitation.senderEmailId,
                    firstName:invitation.senderName
                },
                sharedWith:[
                    {
                        userId:invitation.to.receiverId,
                        emailId:invitation.to.receiverEmailId,
                        firstName:invitation.to.receiverName ,
                        sharedOn:invitation.scheduledDate,
                        accessStatus:true
                    }
                ],
                documentName:invitation.docs[0].documentName,
                documentUrl:invitation.docs[0].documentUrl
            };
            storeDocumentFinal(docDetails,invitation,aws,userLogInfo);

        }
    }

}

function storeDocumentFinal(docDetails,invitation,aws,userLogInfo){

    userManagements.storeDocument(docDetails,function(error,document,message){

        userLogInfo.functionName = 'storeDocument in storeDocumentInfo in sendInvitation router';
        if(error){
            userLogInfo.error = error;
            logger.info('Storing document info failed ',userLogInfo)
        }
        else if(document != null || document != undefined){
            if(checkRequred(document.sharedWith)){
                if(checkRequred(document.sharedWith[0])){

                    var interaction = {};
                    interaction.userId = document.sharedBy.userId;
                    interaction.action = 'sender';
                    interaction.type = 'document-share';
                    interaction.subType = 'document-share';
                    interaction.refId = document._id;
                    interaction.source = 'relatas';
                    interaction.title = document.documentName;
                    interaction.description = '';
                    common.storeInteraction(interaction);

                    for(var i=0; i<document.sharedWith.length; i++){
                        if(checkRequred(document.senderId) && document.sharedWith[i].userId){
                            updateReceiverContacts(document.senderId,document.sharedWith[i].userId);
                            updateReceiverContacts(document.sharedWith[i].userId,document.senderId);
                        }
                        var interactionReceiver = {};
                        interactionReceiver.userId = document.sharedWith[i].userId || '';
                        interactionReceiver.emailId = document.sharedWith[i].emailId;
                        interactionReceiver.action = 'receiver';
                        interactionReceiver.type = 'document-share';
                        interactionReceiver.subType = 'document-share';
                        interactionReceiver.refId = document._id;
                        interactionReceiver.source = 'relatas';
                        interactionReceiver.title = document.documentName;
                        interactionReceiver.description = '';
                        common.storeInteraction(interactionReceiver);
                    }
                }
            }
            userManagements.updateInvitationWithDocId(invitation.invitationId,invitation.docs[0]._id,document,function(error,isUpdated,message){
                if(error){
                    userLogInfo.error = error;
                    logger.info('updating invitation with doc id fail ',userLogInfo);
                }
            })
        };
    });
}


function updateContactsByEmail(senderId,receiverObj){

    userManagements.findUserProfileByEmailIdWithCustomFields(receiverObj.receiverEmailId,{emailId:1,firstName:1,lastName:1,mobileNumber:1},function(error,profile) {

        if(error){

        }else
        if(profile != null || profile != undefined){

            var contact1 = {
                personId:profile._id,
                personName:profile.firstName+' '+profile.lastName,
                personEmailId:profile.emailId,
                count:1,
                lastInteracted:new Date(),
                addedDate: new Date(),
                verified:true,
                relatasContact:true,
                relatasUser:true,
                mobileNumber:profile.mobileNumber
            }
            updateContact(senderId,contact1);
        }else{
            var contact2 = {
                personId:'',
                personName:receiverObj.receiverName,
                personEmailId:receiverObj.receiverEmailId,
                count:1,
                lastInteracted:new Date(),
                addedDate: new Date(),
                verified:true,
                relatasContact:true,
                relatasUser:false
            }
            updateContact(senderId,contact2);
        }
    });
}

//Function to add Contact
function updateContact(userId,contact){
    contactObj.addSingleContact(userId,contact)
}

function updateReceiverContacts(senderId,receiverId){

    userManagements.findUserProfileByIdWithCustomFields(senderId,{emailId:1,firstName:1,lastName:1,mobileNumber:1},function(error,profile) {

        if(error){

        }else
        if(profile != null || profile != undefined){

            var contact1 = {
                personId:profile._id,
                personName:profile.firstName+' '+profile.lastName,
                personEmailId:profile.emailId,
                count:1,
                lastInteracted:new Date(),
                addedDate: new Date(),
                verified:true,
                relatasContact:true,
                relatasUser:true,
                mobileNumber:profile.mobileNumber
            }
            updateContact(receiverId,contact1);
        }
    });
}

//Function to add Contact
function addContact(userId,contact,userLogInfo){
    contactObj.addSingleContact(userId,contact)
}

function getThirtyTweets(data){
    var tweets =[];
    for(var i=0; i<30; i++){
        if(data[i]){
            tweets.push(data[i])
        }

    }

    return tweets;
}


function getUserLogInfo(req){
    var userIp = getClientIp(req);
    var userLogInfo;
    if(req.session.userLogInfo){
        userLogInfo = req.session.userLogInfo;
    }
    else{
        userLogInfo = {
            userName:'',
            userIp:userIp,
            loginType:'',
            info:'',
            cause:'',
            action : ''
        }
        req.session.userLogInfo = userLogInfo;
    }
    return userLogInfo;
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
};


function getCurrentLocation(geo){
    if(checkRequred(geo)){
        if(checkRequred(geo.country) && checkRequred(geo.city) && checkRequred(geo.region) && checkRequred(geo.ll[0]) && checkRequred(geo.ll[1])){
            var location = {
                country:geo.country,
                city:geo.city,
                region:geo.region,
                latitude:geo.ll[0],
                longitude:geo.ll[1]
            }
            return location;
        }
        else  return null;
    }
    else return null;


}

function checkRequred(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

module.exports = router;