var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var request = require('request');
var _ = require("lodash");

var userManagement = require('../dataAccess/userManagementDataAccess');
var messageDataAccess = require('../dataAccess/messageManagement');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');

var appCredential = new appCredentials();
var messageAccess = new messageDataAccess();
var userManagements = new userManagement();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();

var logLib = new winstonLog();
var common = new commonUtility();
var logger =logLib.getWinston()

router.get('/notifications',isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('notifications/index');
});

router.get('/war/user', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("war/warUser");
});

router.get('/messages/unRead/:reqFor',isLoggedInUserRequest,function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    getMessagesUnRead(userId,req.params.reqFor,function(obj){
        res.send(obj);
    })
});

router.get('/messages/unRead/:reqFor/:userId',isLoggedInUserRequest,function(req,res){
    var userId = req.params.userId;
    getMessagesUnRead(userId,req.params.reqFor,function(obj){
        res.send(obj);
    })
});

router.put('/update/war/email/setting/:settingName', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagements.updateWarEmailSettings(userId, req.params.settingName, function(error, result) {
        if(error) {
            res.send({
                SuccessCode: 0, 
                ErrorCode: 1,
                Message: "Error in updating war email settings"
            })
        } else {
            res.send({
                SuccessCode: 1, 
                ErrorCode: 0,
                Message: "War Settings updated successfully"
            })
        }
    })

});

router.post('/update/notification/settings',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    var settingName = req.body.settingName;
    var status = req.body.status;
    var updateFor = req.body.updateFor;

    userManagements.updateNotificationSettings(userId, settingName, status, updateFor, function(error, result) {
        if(error) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Something went wrong in updating notification setting"
            })
        } else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Message: "Notification settings update successfull"
            })
        }
    })
});

router.post('/get/notification/data', common.isLoggedInUserOrMobile, function(req,res) {
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagements.findUserProfileByIdWithCustomFields(userId, {emailId:1, webFirebaseSettings:1}, function(error, user) {
        if(!error && user) {
            messageAccess.getNotificationData(req.body, user.emailId, function(error, result) {
                if(!error && result) {
                    
                    if(req.body.messageId) {
                        parseNotificationData(result, function(parsedData) {
                            res.send(parsedData)
                        })
                    } else {
                        res.send(result);
                    }
                    
                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Message: "Error in fetching notification data"
                    })
                }
            });
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Error in fetching notification data"
            })
        }
    })
});

router.post('/update/notification/open/date', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagements.findUserProfileByIdWithCustomFields(userId, {emailId:1}, function(error, user) {
        if(!error && user) {
            messageAccess.updateNotificationOpenDate(req.body, user.emailId, function(error, result) {
                if(!error && result) {
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                    })
                    
                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Message: "Notification open date update failed"
                    })
                }
            });
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Error in finding profile"
            })
        }
    })
})

router.post('/get/notification/data/for/report', common.isLoggedInUserOrMobile, function(req,res) {
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagements.findUserProfileByIdWithCustomFields(userId, {emailId:1, webFirebaseSettings:1}, function(error, user) {
        if(!error && user) {
            messageAccess.getNotificationDataForReport(req.body, user.emailId, function(error, result) {
                if(!error && result) {
                    res.send(result);
                    
                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Message: "Error in fetching notification data"
                    })
                }
            });
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Error in fetching notification data"
            })
        }
    })
})

router.get('/get/notifications/today', common.isLoggedInUserOrMobile, function(req,res) {
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagements.findUserProfileByIdWithCustomFields(userId, {emailId:1, webFirebaseSettings:1, companyId:1}, function(error, user) {
        if(!error && user) {
            messageAccess.getNotificationDataForToday(req.query, user.emailId, user.companyId, function(error, result) {
                if(!error && result) {
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        data: {
                            notifications: _.uniq(result, 'notificationTitle'),
                            notificationDate: new Date()
                        }
                    });

                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Message: "Error in fetching notification data",
                        data: {
                            notifications: [],
                            notificationDate: new Date()
                        }
                    })
                }
            });
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Error in fetching notification data",
                data: {
                    notifications: [],
                    notificationDate: new Date()
                }
            })
        }
    })
})



router.post('/get/war/data', common.isLoggedInUserOrMobile, function(req,res) {
    var userId = common.getUserIdFromMobileOrWeb(req);

    messageAccess.getWarData(req.body,  function(error, result) {
        if(!error && result) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: result
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Error in fetching notification data"
            })
        }
    });
})

router.post('/get/war/data/for/report', common.isLoggedInUserOrMobile, function(req,res) {
    var userId = common.getUserIdFromMobileOrWeb(req);

    messageAccess.getWarData(req.body,  function(error, result) {
        if(!error && result) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: result
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Error in fetching notification data"
            })
        }
    });
})

function parseNotificationData(result, callback) {
    var jsonData = JSON.parse(result.data);

    switch(result.category) {
        case "tasksForToday":
            callback({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: {
                    total: jsonData.length,
                    tasks: jsonData
                }
            });
            break;
        case "oppClosingToday":
            callback({
                SuccessCode: 1,
                ErrorCode: 0,
                data: [{
                    _id: result.emailId,
                    opportunities: jsonData
                }]
            });
            break;
        case "dealsAtRiskForUser":
            var response = {
                SuccessCode: 1,
                ErrorCode: 0,
            }
            response.deals = jsonData[0] ? jsonData[0].deals : [];
            callback(response);
            break;
        case "documentSharingStatus":
            callback({
                SuccessCode: 1,
                ErrorCode: 0,
                documents: jsonData
            });
            break;
        case "documentView":
            callback({
                SuccessCode: 1,
                ErrorCode: 0,
                documents: [jsonData]
            });
            break;
        case "oppClosingThisWeek":
            callback({
                SuccessCode: 1,
                ErrorCode: 0,
                data: [{
                    _id: result.emailId,
                    opportunities: jsonData
                }]
            });
            break;
        case "weeklyCommit":
        case "targetWonLostManager":
            callback({
                SuccessCode: 1,
                ErrorCode: 0,
                data: jsonData
            });
            break;
        default:
            callback({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Error in fetching notification data"
            });
    }

}

function getMessagesUnRead(userId,reqFor,callback){
    if(reqFor  == 'count'){
        messageAccess.getUnReadMessageCount(userId,['message'],function(count){
            callback(""+count);
        })
    }else if(reqFor  == 'messages'){
        messageAccess.getUnReadMessages(userId,['message'],function(messages){
            if(common.checkRequired(messages) && common.checkRequired(messages.userIdArr) && common.checkRequired(messages.userIdArr.length > 0) && common.checkRequired(messages.messages) && common.checkRequired(messages.messages.length > 0)){

                var userManagements = new userManagement();
                var fields = {
                    firstName:1,
                    lastName:1,
                    emailId:1
                }
                userManagements.getUserListByIdRequestedFields(messages.userIdArr,fields,function(users){
                    if(common.checkRequired(users) && common.checkRequired(users.length)){
                        messages.users = users;
                        callback(messages);
                    }else callback(false);
                })
            }else callback(false);

        })
    }else{
        callback(false);
    }
}

router.get('/messages/update/readStatus/:messageId',function(req,res){
    var messageId = req.params.messageId;
    messageAccess.updateMessageReadStatus(messageId);
    res.send(true);
});

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        return next();
    }else
    {
        return res.redirect('/');
    }
}

// route middleware to make sure a user is logged in
function isLoggedInUserRequest(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        return next();
    }else
    {
        res.send(false);
    }
}

module.exports = router;
