/*
* This file contains all routes related to document viewer page
*
* */

var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple')
var request=require('request');
var _ = require('lodash');

var userManagement = require('../dataAccess/userManagementDataAccess');
var companyClass = require('../dataAccess/corporateDataAccess/companyModelClass');
var appCredentials = require('../config/relatasConfiguration');
var commonUtility = require('../common/commonUtility');
var interactionManagement = require('../dataAccess/interactionManagement');
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var messageDataAccess = require('../dataAccess/messageManagement');

var interactionsObj = new interactionManagement();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var common = new commonUtility();
var userManagements = new userManagement();
var company = new companyClass();
var momentTZ = require('moment-timezone');
var moment = require("moment");
var messageAccess = new messageDataAccess();

var winstonLog = require('../common/winstonLog');

var logLib = new winstonLog();
var logger =logLib.getWinston()

router.get('/documentViewer/old',common.checkUserDomain,function(req,res){
    logger.info('User entered into document viewer page');
    res.render('documentViewer',common.getLoginStatus(req));
});

router.get('/render/document',common.checkUserDomain,function(req,res){
    res.render('documentTracking/documentRenderer',common.getLoginStatus(req));
});

router.get('/user/authentication',common.checkUserDomain,function(req,res){
    res.render('documentTracking/passwordAuthentication',common.getLoginStatus(req));
});

router.get('/document/link/expired',common.checkUserDomain,function(req,res){
    res.render('documentTracking/documentLinkExpired',common.getLoginStatus(req));
});

router.get('/view/shared/document',common.checkUserDomain,function(req,res){
    res.render('documentTracking/documentView',common.getLoginStatus(req));
});

router.get('/readDocument/:docId',common.checkUserDomain,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for analytics';
    var docId = req.params.docId;
    req.session.nonUserEmailId = '';
    // req.session.readDocumentId = docId;
    // req.session.sharedWithObjId = '';
    res.redirect('/render/document?docId='+docId);
});

router.get('/readDocument/:docId/:objId',common.checkUserDomain,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for analytics';
    var docId = req.params.docId;
    var sharedUserObjId = req.params.objId;
    req.session.nonUserEmailId = '';
    // req.session.readDocumentId = docId;
    // req.session.sharedWithObjId = sharedUserObjId;
    userManagements.updateDocumentLinkAccessCount(docId, sharedUserObjId, function(error, result) {
    })
    // res.redirect('/render/document/'+docId+'/'+sharedUserObjId);
    res.redirect('/render/document?docId='+docId+'&'+'sharedId='+sharedUserObjId);
});

router.get('/accessDocByEmail/:emailId/:docId',function(req,res){
    
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for access doc by email';

    var docId = req.params.docId;
    var emailId = req.params.emailId;
    if(checkRequired(docId) && checkRequired(emailId)){

        req.session.readDocumentId = req.params.docId;
        req.session.nonUserEmailId = emailId;
        userManagements.findDocumentById(docId,function(error,document,message){
            userLogInfo.functionName = 'findDocumentById in access doc by email router';
            if(error){
                userLogInfo.error = error;
                userLogInfo.info = 'Error in finding document with doc id';
                logger.error('Error in mongo',userLogInfo);
                res.redirect('/');
            }
            else{
                if(checkRequired(document)){
                    if(checkRequired(document.sharedWith[0])){
                        var shareWith = document.sharedWith;
                        for(var i=0; i<shareWith.length; i++){
                            if(shareWith[i].emailId == emailId){
                                var name = shareWith[i].firstName;
                                if(checkRequired(name)){
                                    var firstName = name.split(' ')[0];
                                    var lastName = name.split(' ')[1];
                                    if(checkRequired(firstName)){
                                        req.session.nonUserFirstName = firstName;
                                    }
                                    if(checkRequired(lastName)){
                                        req.session.nonUserLastName = firstName;
                                    }

                                }

                            }
                            if(i == shareWith.length-1){
                                res.redirect('/documentViewer');
                            }
                        }
                    } else  res.redirect('/');
                } else  res.redirect('/');

            }
        });

    }
    else  res.redirect('/');
});

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}
router.get('/nonUserInfo/docViewer',function(req,res){
    if(req.session.nonUserEmailId){
        var emailId = req.session.nonUserEmailId;
        var firstName = req.session.nonUserFirstName;
        var lastName = req.session.nonUserLastName;
        var resObj = {
            emailId:emailId || '',
            firstName:firstName || '',
            lastName:lastName || ''
        }
        res.send(resObj)
    }
    else{
        res.send(true)
    }
});

router.get('/load/document/:docId/:objId?', function(req, res){
    var documentId = req.params.docId;
    var sharedWithObjId = req.params.objId;

    if( documentId != null &&  documentId != undefined ) {
        if(sharedWithObjId != null && sharedWithObjId != undefined && sharedWithObjId != "") {
            userManagements.findDocumentBySharedObjId(documentId, sharedWithObjId, function(error, document, msg) {
                if(error || document == null) {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Message:'Link Expired'
                    })
                } else {
                    userManagements.findUserProfileByIdWithCustomFields(document.sharedBy.userId, {emailId:1,firstName:1,lastName:1, profilePicUrl:1, profileDeactivated:1, companyName:1, companyId: 1}, function(error, result) {
                        if(result && !error && !result.profileDeactivated) {
                            var companyId = result.companyId || document.sharedBy.companyId;
    
                            company.findCompanyById(common.castToObjectId(companyId),function (companyDetails) {
                                companyDetails = companyDetails || {logo:{url:""}};
        
                                var sharedWithDetails;
                                document.sharedWith.forEach(function(shared){
                                    if(shared._id == sharedWithObjId) {
                                        sharedWithDetails = shared;
                                    }
                                });
            
                                if(Object.keys(sharedWithDetails).length > 0) {
                                    document.sharedWith = [];
                                    document.sharedWith.push(sharedWithDetails);
                                    document.sharedBy.companyLogoUrl = companyDetails.logo.url;
                                    document.sharedBy.firstName = result.firstName;
                                    document.sharedBy.lastName = result.lastName;
                                    document.sharedBy.profilePicUrl = result.profilePicUrl;
            
                                    res.send({
                                        SuccessCode: 1,
                                        ErrorCode: 0,
                                        Data: document,
                                        isOwner: false
                                    })
                                } else {
                                    res.send({
                                        SuccessCode: 0,
                                        ErrorCode: 1,
                                        Message: 'Link expired'
                                    })
                                }
                            })
                        } else {
                            res.send({
                                SuccessCode: 0,
                                ErrorCode: 1,
                                Message: 'Shared by user profile not found'
                            })
                        }
                    })
                }
            })
        }
        else {
            userManagements.findDocumentById(documentId, function(error, document, msg) {
                if(error) {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Message: msg.message
                    })
                } else {
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data: document,
                        isOwner: true
                    })
                }
            })
        }

    } else {
        res.send({
            SuccessCode: 0,
            ErrorCode: 1,
            Message: 'Document not exist'
        })
    }
});

router.get('/loadDocument',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for loadDocument';

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    if( req.session.readDocumentId != null &&  req.session.readDocumentId != undefined){
        var docId = req.session.readDocumentId;


        userManagements.checkDocumentAuthorization(userId,docId,function(error,document,message){

            userLogInfo.functionName = 'checkDocumentAuthorization in loadDocument router';
            if(error){
                userLogInfo.error = error;
                userLogInfo.info = 'Error in finding document authorization';
                logger.error('Error in mongo',userLogInfo);
                res.send('notAuthorised');
            }else
            if(document == false){
                userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1},function(error,userData){
                    if(error || !checkRequired(userData)){
                        res.send('notAuthorised');
                    }else{
                        userManagements.checkDocumentAuthorizationByEmail(userData.emailId,docId,function(error2,document2,message){
                            if(error2 || !checkRequired(document2)){
                                res.send('notAuthorised');
                            }else{
                                var obj = {
                                    userId:userData._id,
                                    firstName:userData.firstName+' '+userData.lastName,
                                    emailId:userData.emailId
                                }
                                userManagements.updateDocShareWithEmailId(docId,userId,obj,function(error,isUpdated,msg){
                                    res.send(document2)
                                })
                            }
                        })
                    }
                })
            }
            else{
                res.send(document);
            }
        });
    }
    else{
        res.send(false);
    }
});

router.post('/updateReadStatusOfDocument',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for update document status';
    var readStatus = req.body;
    if(readStatus.count == true || readStatus.count == 'true'){
        var userId = common.getUserId(req.user)
        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1},function(error,user){
            if(common.checkRequired(user)){
                    interactionsObj.updateInteractionOpenedTaskBulk(readStatus.documentId,false,common.castToObjectId(userId),common.castToObjectId(readStatus.lUserId),true,function(isSuccess){
                })
            }
        })
    }

    userManagements.updateDocumentReadInfo(readStatus,function(error,isUpdated,message){
        userLogInfo.functionName = 'updateDocumentReadInfo in updateReadStatusOfDocument router';
        if(error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in updating read status';
            logger.error('Error in mongo',userLogInfo);
        }
    });
    res.send(true);
});

router.post('/update/document/page/read/status', function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for update document status';
    var readStatus = req.body.data;

    userManagements.updateDocumentReadInfoV2(readStatus,function(error,isUpdated,message){
        userLogInfo.functionName = 'updateDocumentReadInfo in updateReadStatusOfDocument router';

        if(error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in updating read status';
            logger.error('Error in mongo',userLogInfo);
        }
    });
    res.send(true);
});

router.post('/update/document/access/count', function(req,res){
    var userLogInfo = getUserLogInfo(req);
    var docId = req.body.docId;
    var sharedUserObjId = req.body.sharedUserObjId;

    userManagements.findDocumentBySharedObjId(docId, sharedUserObjId, function(error, document, message) {
        if(!error && document && document.sharedWith[0]) {
            var sharedWithUser = _.find(document.sharedWith, function(sharedWith) {
                if(sharedWith._id == sharedUserObjId) {
                    return sharedWith;
                }
            });

            userManagements.findUserProfileByEmailIdWithCustomFields(document.sharedBy.emailId, {mobileFirebaseToken:1, webFirebaseSettings:1, companyId:1, _id:1, emailId:1, timezone:1}, function(error, user) {
                var timezone = user.timezone && user.timezone.name ? user.timezone.name : "Asia/Kolkata";
                var accessedTime = moment().tz(timezone).format("hh:mm a");

                if(!error && user) {
                    var payload = {
                        notification: {
                            title: document.documentName,
                            body: sharedWithUser.firstName + " " + sharedWithUser.lastName + " has viewed your document at " + accessedTime,
                            click_action: "MY_ACTION",
                        },
                        data: {
                            category: 'documentView',
                            documentId: String(document._id),
                            sharedWithId: String(sharedWithUser._id),
                            dayString: moment().format("DDMMYYYY"),
                            click_action: "/document/index?docId="+ String(document._id) + "&sharedId="+ String(sharedWithUser._id)
                        }
                    };
    
                    common.sendNotification(payload, user.webFirebaseSettings.firebaseToken, user.mobileFirebaseToken, user.companyId, user._id, user.emailId, function() {
                    })
                }
                
            })
        }
    })

    userManagements.updateDocumentAccessCountV2(docId, sharedUserObjId, function(error,isUpdated) {
        res.send(true);
    });
});

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        return next();
    }else
    {
        // if they aren't redirect them to the home page
        return res.redirect('/');
    }
}

function getUserLogInfo(req){
    var userIp = getClientIp(req);
    var userLogInfo;
    if(req.session.userLogInfo){
        userLogInfo = req.session.userLogInfo;
    }
    else{
        userLogInfo = {
            userName:'',
            userIp:userIp,
            loginType:'',
            info:'',
            cause:'',
            action : ''
        }
        req.session.userLogInfo = userLogInfo;
    }
    return userLogInfo;
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;
    }
    return ipAddress;
};

module.exports = router;