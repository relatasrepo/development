var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var jwt = require('jwt-simple');
var geoip = require('geoip-lite');
var fs = require('fs');
var request=require('request');

var userManagement = require('../dataAccess/userManagementDataAccess');
var emailSender = require('../public/javascripts/emailSender');
var validations = require('../public/javascripts/validation');
var validation = new validations();
var winstonLog = require('../common/winstonLog');
var appCredentials = require('../config/relatasConfiguration');
var commonUtility = require('../common/commonUtility');
var googleCalendar = require('../common/googleCalendar');
var IPLocatorClass = require('../common/ipLocator');
var adminClass = require('../dataAccess/corporateDataAccess/adminModelClass');

var logLib = new winstonLog();
var common = new commonUtility();
var googleCalendarObj = new googleCalendar();
var logger =logLib.getWinston()

var admin = new adminClass();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var emailSenders = new emailSender();
var IPLocator = new IPLocatorClass();
var userManagements = new  userManagement();

var domainName = domain.domainNameForUniqueName;

router.get('/fromPage/:pageName',function(req,res){
    req.session.nextPage = req.params.pageName;
    res.send(true);
});

router.get('/preOnBoarding',isLoggedInUser,function(req,res){
    res.render('postSignUp');
});

router.get('/onBoarding/:userId/:url',function(req,res){

    if(common.checkRequired(req.params) && common.checkRequired(req.params.userId) && common.checkRequired(req.params.url)){

        getReturnObj(req.params.userId,req,function(returnedObj){

            var url = 'https://chart.googleapis.com/chart?cht=p3&chs=420x210';
            var arr = JSON.parse(req.params.url);

            var arrNum = [];
            var arrNames = [];
            var arrColor = [];

            for(var i=1;i<arr.length;i++){
                arrNames.push(arr[i][0]);
                arrNum.push(arr[i][1]);
                arrColor.push(arr[i][2]);
            }

            if(arrNum.length > 0 && arrNames.length > 0){
                url += '&chd=t:'+arrNum.join(',');
                url += '&chl='+arrNames.join('|');
                url += '&chco='+arrColor.join(',');
            }

            returnedObj.image = url;
            returnedObj.uniqueUrl = returnedObj.uniqueUrl+'/'+req.params.url;
            res.render('postSignUp3',returnedObj);
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/onBoarding',isLoggedInUser,function(req,res){


        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret);
        var userId = decoded.id;

        getReturnObj(userId,req,function(returnedObj){
            res.render('postSignUp3',returnedObj);
        });

});

function getReturnObj(userProfile,req,callback){
    var obj = {};
    var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';

    obj.uniqueUrl = protocol+req.headers.host+'/onBoarding/'+userProfile;
    obj.uniqueUrl2 = protocol+req.headers.host+'/onBoarding/'+userProfile;
    obj.ur = protocol+req.headers.host+'/onBoarding';
    obj.description =  "Relatas  just showed me how productive I am. Have you checked your productivity at www.relatas.com";
    obj.image = 'https://chart.googleapis.com/chart?cht=p3&chd=t:60,40&chs=250x100&chl=Hello|World';
    callback(obj);
}

router.get('/finalPage',function(req,res){
    if( req.session.finalPage == 'meetingProcess'){
        req.session.sendInviteLinkedinStatus = 'userExist';
        res.redirect(req.session.calUrl || '/');
    }else
    if(req.session.finalPage == 'documentViewer'){
        res.redirect('/documentViewer');
    }
    else if(req.session.finalPage == 'events'){
        res.redirect('/events');
    }
    else if(req.session.finalPage == 'createEvent'){
        res.redirect('/createAnEvent');
    }
    else if(req.session.finalPage == 'viewEventDetails' || req.session.finalPage == 'meetingDetails'){
        res.redirect(req.session.finalPageUrl);
    }
    else{
        res.redirect('/');
    }
})

function getLoginPageName(req){
    if( req.session.finalPage == 'meetingProcess'){
       return 'schedule-page'
    }else
    if(req.session.finalPage == 'documentViewer'){
      return 'document-viewer-page'
    }
    else if(req.session.finalPage == 'events'){
       return 'events-page'
    }
    else if(req.session.finalPage == 'createEvent'){
        return 'create-event-page'
    }
    else if(req.session.finalPage == 'viewEventDetails'){
       return 'event-details-page'
    }
    else if(req.session.finalPage == 'meetingDetails'){
        return 'meeting-page'
    }
    else{
       return 'landing-page'
    }
}

router.get('/join',function(req,res){
    logger.info('join');
    try
    {
        logger.info('User entering into '+req.session.nextPage);
        res.render(req.session.nextPage);
    }
    catch (ex)
    {
        logger.info('Error loading user '+req.session.nextPage+'! '+ ex.stack);
    }
});

router.get('/emailLogin',function(req,res){
    req.session.linkedinUser = ''
    req.session.googleUser = ''
    req.session.facebookUser = ''
    req.session.twitterUser = ''
    req.session.profileImageUrl = ''
    req.session.saveDetails = ''
    req.session.finalPage = 'dashboard';
        //req.session.nextPage = 'emailLoginPersonalDetailsPage';
        req.session.nextPage = 'emailLoginProfileStep1';
        res.redirect('/join');
});

router.get('/DocumentEmailLogin',function(req,res){
    req.session.linkedinUser = ''
    req.session.googleUser = ''
    req.session.facebookUser = ''
    req.session.twitterUser = ''
    req.session.profileImageUrl = ''
    req.session.saveDetails = ''
    req.session.finalPage = 'documentViewer';

    req.session.nextPage = 'emailLoginProfileStep1';
    res.redirect('/join');
});

router.get('/emailLoginEventsPage',function(req,res){
    req.session.linkedinUser = ''
    req.session.googleUser = ''
    req.session.facebookUser = ''
    req.session.twitterUser = ''
    req.session.profileImageUrl = ''
    req.session.saveDetails = ''
    req.session.finalPage = 'events';

    req.session.nextPage = 'emailLoginProfileStep1';
    res.redirect('/join');
});

router.get('/emailLoginCreateEventPage',function(req,res){
    req.session.linkedinUser = ''
    req.session.googleUser = ''
    req.session.facebookUser = ''
    req.session.twitterUser = ''
    req.session.profileImageUrl = ''
    req.session.saveDetails = ''
    req.session.finalPage = 'createEvent';

    req.session.nextPage = 'emailLoginProfileStep1';
    res.redirect('/join');
});


router.get('/emailLoginViewEventDetailsPage',function(req,res){
    req.session.linkedinUser = ''
    req.session.googleUser = ''
    req.session.facebookUser = ''
    req.session.twitterUser = ''
    req.session.profileImageUrl = ''
    req.session.saveDetails = ''
    req.session.finalPage = 'viewEventDetails';

    req.session.nextPage = 'emailLoginProfileStep1';
    res.redirect('/join');
})

router.get('/emailLoginProfileStep2',function(req,res){
    res.render('emailLoginProfileStep2');
})
router.get('/emailLoginProfileStep3',function(req,res){
   res.render('emailLoginProfileStep3');
})

router.get('/socialLoginProfileStep2',function(req,res){
    res.render('socialLoginProfileStep2');
})
router.get('/socialLoginProfileStep3',function(req,res){
    res.render('socialLoginProfileStep3');
})

router.get('/googleLoginProfileStep2',function(req,res){
    res.render('googleLoginProfileStep2');
})
router.get('/googleLoginProfileStep3',function(req,res){
    res.render('googleLoginProfileStep3');
})

router.get('/googleLogin',function(req,res){
    req.session.linkedinUser = ''
    req.session.googleUser = ''
    req.session.facebookUser = ''
    req.session.twitterUser = ''

    req.session.nextPage = 'googleLoginPersonalDetailsPage';
    res.redirect('/join');
});

router.get('/googleLoginSocialAccountPage',function(req,res){
    req.session.nextPage = 'googleLoginSocialAccountPage';
    res.redirect('/join');
});

router.get('/socialLogin',function(req,res){
    req.session.nextPage = 'socialLoginProfileStep1';
    res.redirect('/join');
})

router.get('/socialLoginSocialAccountPage',function(req,res){
    req.session.nextPage = 'socialLoginSocialAccountPage';
    res.redirect('/join');
});

router.get('/meetingProcessSignUp',function(req,res){
    req.session.linkedinUser = ''
    req.session.googleUser = ''
    req.session.facebookUser = ''
    req.session.twitterUser = ''
    req.session.finalPage = 'meetingProcess';
    req.session.nextPage = 'emailLoginPersonalDetailsPage';
    res.redirect('/join');

});


router.post('/checkUrl',function(req,res){
       var uniqueName = req.body.publicProfileUrl;

       var url=domainName+'/u/'+uniqueName.toLowerCase();

       userManagements.isExistingProfile(url,uniqueName,function(error,isExist){
            if (isExist == true) {
                  res.send(false)
            }
            else{
                res.send(true)
            } 
       })
});

router.post('/checkUrl/linkedin',function(req,res){
    var uniqueName = req.body.publicProfileUrl;

    var url=domainName+'/u/'+uniqueName.toLowerCase();

    userManagements.findUserPublicProfile(url,uniqueName,function(error,profile){
        if (error) {
            res.send(false)
        }
        else if(checkRequred(profile)){
             if(profile.id == req.body.userId){
                 res.send(true);
             }else res.send(false);
        }else  res.send(true);
    })
});

router.get('/checkEmail/:emailId',function(req,res){
        var emailId = req.params.emailId;

    userManagements.isExistingUser(emailId,function(error,userProfile,message){
          if(error){
              logger.info('Error in db while checking email exist or not '+error);
              res.send(false);
          }
          else{
              if(message.message == 'existing user'){
                  res.send(false);
              }
              else{
                  res.send(true);
              }
          }
    })
});

router.get('/checkEmail/:emailId/:userId',function(req,res){
    var emailId = req.params.emailId;
    var userId = req.params.userId;

    userManagements.isExistingUser(emailId,function(error,userProfile,message){
        if(error){
            logger.info('Error in db while checking email exist or not '+error);
            res.send(false);
        }
        else{
            if(message.message == 'existing user'){
                if(userProfile._id == userId){
                    res.send(true)
                }else res.send(false);
            }
            else{
                res.send(true);
            }
        }
    })
});

router.get('/getPartialAccountDetails',function(req,res){
     var partialFillAcc = req.session.partialFillAcc;

      if(checkRequred(partialFillAcc)){
         userManagements.findUserProfileByEmailIdWithCustomFields(partialFillAcc.emailId,{contacts:0},function(error,user){
              if(error){
                  res.send(false);
              }
              else{
                  var u = JSON.parse(JSON.stringify(user));
                  u.scheduleFlow = req.session.scheduleFlow;
                  u.onSuccess = req.session.onSuccess;
                  res.send(u);
              }
         })
      }else res.send(false);
});

router.post('/createPartialAccount',function(req,res){
        var user = req.body;
     if(checkRequred(user.firstName) && checkRequred(user.lastName) && checkRequred(user.emailId)){
       user.public = true;
       user.registeredUser = false;
       user.partialFill = true;
         req.session.partialFillAcc = user;

         userManagements.addUser(user,function(error,userDataTobeSend,message){
             if(error || !checkRequred(userDataTobeSend)){
                 res.send(false);
             }else{
                 res.send(true);
             }
         })
     }else{
         res.send(false);
     }
 });

router.post('/updatePartialAcc/google',function(req,res){

    req.session.fromOnBoarding = req.body.fromOnBoarding;
    req.session.interactionsWritten = false;//This is for showing the loading your interactions message for only onboarding users. Will be set to true in GoogleCalendar.js
    req.session.save();

    var userId = req.body.userId;
    validation.RegistrationThirdParty(req.body,function(status){

        if(status == true){
            var userProfiles = getUserInfo(req,res,'google');
            userManagements.updateProfileObject(userProfiles,function(error,result){
                if(error || result == 0){
                    res.send(false);
                }
                else{
                    var pathUpdate = {companyId:req.body.companyId,userId:req.body.userId,parentId:req.body.reportingManager};
                    if(common.checkRequired(pathUpdate.parentId) && common.checkRequired(pathUpdate.companyId)){
                        admin.updateAssigned(pathUpdate,function(isSuccess){
                        })
                    }
                    userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,profile){
                        if(error || !checkRequred(profile)){
                            res.send(false);
                        }
                        else{

                            //Send mail to confirm Registration
                            var emailSenders = new emailSender();
                            emailSenders.sendRegistrationConfirmationMail(profile);
                            common.updateContactMultiCollectionLevel(userId,false);
                            req.session.profileImageUrl = ''
                            logger.info('generating token for user');
                            var tokenSecret ="alpha";

                            var user = jwt.encode({ id: profile._id}, tokenSecret);
                            logger.info('generating token for user done');
                            req.session.dashboardMsg = true;
                            req.session.userLoginFlag = true;
                            req.session.partialFillAcc = null;
                            req.session.profileImageUrl = '';
                            req.logIn(user, function(err){
                                
                                if(!err){
                                    common.insertLog(profile,'partial-account-created',req);
                                    updateCurrentLocation(common.getClientIp2(req),profile._id);
                                    var fromOnboarding = true;

                                    afterTime(profile,userId,res,fromOnboarding,req);
                                    // Manually save session before redirect. See bug https://github.com/expressjs/session/pull/69
                                    req.session.save(function(){
                                        //afterTime(req,profile,userId);
                                        res.send(true);
                                    });
                                }
                                else res.send(false);
                            });
                        }
                    })
                }
            })
        }else res.send(status);
    });
});

function afterTime(profile,userId,res,fromOnboarding,req){
    setTimeout(function(){
        googleCalendarObj.getGoogleMeetingInteractions(profile._id,null,null,fromOnboarding,req,profile.serviceLogin);
    },1000)
}

router.get('/update/login/relatas/by/id',function(req,res){
    if(common.checkRequired(req.query.userId) && req.query.pass == '099k1a0540'){
        var tokenSecret ="alpha";

        var token = jwt.encode({ id: req.query.userId}, tokenSecret);
        req.logIn(token, function(err){
            setTimeout(function(){
                res.redirect('/');
            },500)

        })
    }else res.send('un authorised');
});

router.post('/updatePartialAcc/linkedin',function(req,res){

    var userId = req.body.userId;
    validation.RegistrationThirdParty(req.body,function(status){
        if(status == true){
            var userProfiles = getUserInfo(req,res,'linkedin');
            userManagements.updateProfileObject(userProfiles,function(error,result){
                if(error || result == 0){
                    res.send(false);
                }else{
                    userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,profile){
                        if(error || !checkRequred(profile)){
                            res.send(false);
                        }else{
                            updateCurrentLocation(common.getClientIp2(req),profile._id);
                            var emailSenders = new emailSender();
                            emailSenders.sendRegistrationConfirmationMail(profile);
                            req.session.profileImageUrl = ''
                            logger.info('generating token for user linkedin');
                            var tokenSecret ="alpha";

                            var token = jwt.encode({ id: profile._id}, tokenSecret);
                            logger.info('generating token for user done linkedin');
                            var user=token;
                            req.session.partialFillAcc = null;
                            req.logIn(user, function(err){
                                if(!err){
                                    req.session.dashboardMsg = true;
                                    req.session.userLoginFlag = true;
                                    var pageName = getLoginPageName(req);
                                    common.insertLog(profile,pageName,req);
                                        req.session.profileImageUrl = ''
                                        if(req.session.finalPage == 'documentViewer'){
                                            if(req.session.nonUserEmailId){
                                                var userData = {
                                                    userId:profile._id,
                                                    emailId:profile.emailId,
                                                    firstName:profile.firstName+' '+profile.lastName,
                                                    sharedOn:new Date(),
                                                    accessStatus:true
                                                }
                                                var docId =  req.session.readDocumentId;
                                                var userId = profile._id;
                                                updateDocWithNewUserId(userData,docId,req,res)
                                                res.send(true)
                                            } else res.send(true);
                                        }
                                        else if(req.session.finalPage == 'meetingDetails'){
                                            var invitationId = req.session.meetingId;
                                            var meetingUserData = {
                                                receiverId:profile._id,
                                                receiverEmailId:profile.emailId,
                                                receiverFirstName:profile.firstName,
                                                receiverLastName:profile.lastName,
                                                isAccepted:false
                                            }
                                            updateInvitationToList(req,res,invitationId,meetingUserData);
                                        }else res.send(true);
                                }
                                else res.send(false);
                            });
                        }
                    })
                }
            })
        }else res.send(status);
    });
});


router.post('/updatePartialAcc/relatas',function(req,res){

    var userId = req.body.userId;
    validation.Registration(req.body,function(status){
        if(status == true){
            var userProfiles = getUserInfo(req,res,'relatas');
            userManagements.updateProfileObject(userProfiles,function(error,result){
                if(error || result == 0){
                    res.send(false);
                }else{
                    userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,profile){
                        if(error || !checkRequred(profile)){
                            res.send(false);
                        }else{
                            updateCurrentLocation(common.getClientIp2(req),profile._id);
                            var emailSenders = new emailSender();
                            emailSenders.sendRegistrationConfirmationMail(profile);
                            req.session.profileImageUrl = ''
                            logger.info('generating token for user email');
                            var tokenSecret ="alpha";

                            var token = jwt.encode({ id: profile._id}, tokenSecret);
                            logger.info('generating token for user done email');
                            var user=token;
                            req.session.partialFillAcc = null;
                               req.logIn(user, function(err){
                                   req.session.dashboardMsg = true;
                                   req.session.userLoginFlag = true;
                                   var pageName = getLoginPageName(req);
                                   common.insertLog(profile,pageName,req);
                                 res.send(true)
                              });

                         }
                    })
                }
            })
        }else res.send(status);
    });
});

function getTokenResetFlag(google){
    if(checkRequred(google) && google.length >0){
        return true;
    }else return false;
}

function getUserInfo(req,res,type){
    var userProfiles = req.body;

    if(userProfiles.profilePicUrl.charAt(0) == 'h' && type != 'relatas'){
        storeProfileImg(userProfiles.profilePicUrl,userProfiles.screenName);
        userProfiles.profilePicUrl = '/profileImages/'+userProfiles.screenName;
    }
    userProfiles.partialFill = false;
    userProfiles.tokenReset = true;
    userProfiles.userType = "registered"
    userProfiles.registeredUser = true;
    userProfiles.registeredDate = new Date();

    return userProfiles;
}

router.post('/registerAccount', function(req, res)
{
    try
    {
        validation.Registration(req.body,function(status){

          if(status == true){
                 logger.info('validation completed for registering account');

              var userProfiles =  getUserInfo(req,res,'relatas');
              var docView = userProfiles.docView;
              var meetingProcess = userProfiles.meetingProcess;
              var meetingDetails = userProfiles.meetingDetails;

                userManagements.addUser(userProfiles,function(error,userDataTobeSend,message)
                {
                  logger.info('callback from add user');
                  if (error || userDataTobeSend == null) {
                        logger.info('Error occured while writing user data in DB (emailLogin)');
                        res.redirect('error');
                   }
                  if (userDataTobeSend == 'exist') {
                     logger.info('user existing in DB email login');
                         res.send(false);
                    }
                  else if(userDataTobeSend.serviceLogin=="relatas"){
                           if(userDataTobeSend.alreadyRegistered){
                        }
                        else{
                             logger.info('sending confirmation mail to user emailLogin');
                             var emailSenders = new emailSender();
                             emailSenders.sendRegistrationConfirmationMail(userDataTobeSend);
                            }
                  }
                  if (userDataTobeSend == 'exist') {
                       logger.info('user existing in DB email login 2');
                       res.send(false);
                  }
                  else{
                      updateCurrentLocation(common.getClientIp2(req),userDataTobeSend._id);
                      req.session.profileImageUrl = ''
                        logger.info('generating token for user  emailLogin');
                        var tokenSecret ="alpha"; 
                        
                        var token = jwt.encode({ id: userDataTobeSend._id}, tokenSecret);  
                        logger.info('generating token for user done emailLogin');
                        var user=token;
                        req.logIn(user, function(err){


                               if (err) {
                                    return next(err);
                               } 
                               else{
                                   req.session.dashboardMsg = true;
                                   req.session.userLoginFlag = true;
                                   var pageName = getLoginPageName(req);
                                   common.insertLog(userDataTobeSend,pageName,req);
                                   if(meetingProcess){
                                       req.session.finalPage = 'meetingProcess'
                                   }
                                   if(docView){
                                       req.session.finalPage = 'documentViewer'
                                   }
                                   if(meetingDetails){
                                       req.session.finalPage = 'meetingDetails'
                                   }
                                   req.session.profileImageUrl = ''
                                   if(req.session.finalPage == 'documentViewer'){

                                       if(req.session.nonUserEmailId){
                                           var userData = {
                                               userId:userDataTobeSend._id,
                                               emailId:userDataTobeSend.emailId,
                                               firstName:userDataTobeSend.firstName+' '+userDataTobeSend.lastName,
                                               sharedOn:new Date(),
                                               accessStatus:true
                                           }

                                           var docId =  req.session.readDocumentId;
                                           var userId = userDataTobeSend._id;
                                           updateDocWithNewUserId(userData,docId,req,res)
                                           res.send(true);
                                       } else res.send(true);
                                   }
                                   else if(req.session.finalPage == 'meetingDetails'){
                                       var invitationId = req.session.meetingId;
                                       var meetingUserData = {
                                           receiverId:userDataTobeSend._id,
                                           receiverEmailId:userDataTobeSend.emailId,
                                           receiverFirstName:userDataTobeSend.firstName,
                                           receiverLastName:userDataTobeSend.lastName,
                                           isAccepted:false
                                       }
                                       updateInvitationToList(req,res,invitationId,meetingUserData);
                                   }else res.send(true)
                               }
                         });
                   }
                });
            }
            else{
                logger.info("user credential is invalid");
               
                res.send(status);
                
            }
     
      });
  }
    //TODO: send the correct error message!:
  catch(err)
    {
      logger.info("Exception occurred in emailLogin "+err);
       throw  err;
    }
});

function updateCurrentLocation(IP,userId) {
    IPLocator.lookup(IP, userId, function (location) {

    });
}

function updateInvitationToList(req,res,invitationId,userData){

    userManagements.updateInvitationToList(invitationId,userData,function(error,isUpdated,message){
        logger.info(message)
        res.send(true);
    })
}

router.post('/storeSession',function(req,res){
  req.session.storeSession=req.body;
  res.send(true);
})

router.post('/saveDetails',function(req,res){
      req.session.saveDetails=req.body;
     res.send(true)
});

router.get('/getSavedDetails',function(req,res){
     var details = req.session.saveDetails;
     res.send(details)
});

router.get('/clearSavedDetails',function(req,res){
    req.session.saveDetails = null;
    res.send(true)
});

function EncryptionValue(data){
  var shaAlgorithm = crypto.createHash('sha256');
    shaAlgorithm.update(data);
    var hashedData = shaAlgorithm.digest('hex');
    return hashedData; 
}



router.post('/registerAccountThirdParty', function(req, res)
{
  
    try
    {
        validation.RegistrationThirdParty(req.body,function(status){
           
            if(status == true){
              logger.info('validation completed');

                var userProfiles = req.body;

                var ipAddr = common.getClientIp2(req);
                var geo = geoip.lookup(ipAddr.toString());
                var currentLocation = getCurrentLocation(geo);
                userProfiles.currentLocation = currentLocation;

                if(!checkRequred(userProfiles.location)){
                    userProfiles.location = checkRequred(currentLocation) ? currentLocation.city : '';
                }
                if(userProfiles.profilePicUrl.charAt(0) == 'h'){
                    storeProfileImg(userProfiles.profilePicUrl,userProfiles.screenName);
                    userProfiles.profilePicUrl = '/profileImages/'+userProfiles.screenName;
                }

                var dataGoogle=JSON.parse(userProfiles.google);
                var dataLinkedin =JSON.parse(userProfiles.linkedin);
                var dataFacebook = JSON.parse(userProfiles.facebook);
                var dataTwitter = JSON.parse(userProfiles.twitter);

                userProfiles.google=dataGoogle;
                userProfiles.linkedin=dataLinkedin;
                userProfiles.facebook=dataFacebook;
                userProfiles.twitter=dataTwitter;


                userManagements.addUserThirdParty(userProfiles,function(error,userDataTobeSend,message)
                {
                  if (error) {
                        logger.info('Error occured while writing user data in DB (socialLogin)');
                        res.redirect('/error');
                   }
                    
                  if (message.message == 'userExist') {
                        res.send('<html><head><script>alert("User already exist");window.location.replace("/")</script></head></html>');
                  }
                  else{
                     var emailSenders = new emailSender();
                     emailSenders.sendRegistrationConfirmationMail(userDataTobeSend);
                     req.session.loginService ='';
                      var tokenSecret ="alpha"; 
                     var token = jwt.encode({ id: userDataTobeSend._id}, tokenSecret);  
                    
                     var user=token;
                     req.logIn(user, function(err){

                       if (err) {
                           return next(err);
                       }
                       else{
                           req.session.dashboardMsg = true;
                           req.session.userLoginFlag = true;
                           var pageName = getLoginPageName(req);
                           common.insertLog(userDataTobeSend,pageName,req);
                           req.session.profileImageUrl = ''
                           if(req.session.finalPage == 'documentViewer'){
                               if(req.session.nonUserEmailId){
                                   var userData = {
                                       userId:userDataTobeSend._id,
                                       emailId:userDataTobeSend.emailId,
                                       firstName:userDataTobeSend.firstName+' '+userDataTobeSend.lastName,
                                       sharedOn:new Date(),
                                       accessStatus:true
                                   }

                                   var docId =  req.session.readDocumentId;
                                   var userId = userDataTobeSend._id;
                                   updateDocWithNewUserId(userData,docId,req,res)
                                   res.send(true);
                               } else res.send(true);
                           }
                           else res.send(true)
                       }

                    }) 
                     
                }
                    
              });
            }
            else{
                logger.info("user credential is invalid");
                
                res.send(status);
               
            }

      });
   }
        //TODO: send the correct error message!:
    catch(err)
    {
        logger.info('Error occurred in user linkedin Registration '+err);
        throw  err;
    }
});



// register through google

router.post('/registerAccountThirdPartyGoogle', function(req, res)
{

    try
    {
        validation.RegistrationThirdParty(req.body,function(status){

            if(status == true){
                logger.info('validation completed');

                var userProfiles = req.body;

                var ipAddr = common.getClientIp2(req);
                var geo = geoip.lookup(ipAddr.toString());
                var currentLocation = getCurrentLocation(geo);
                userProfiles.currentLocation = currentLocation;

                if(!checkRequred(userProfiles.location)){
                    userProfiles.location = checkRequred(currentLocation) ? currentLocation.city : '';
                }

                storeProfileImg(userProfiles.profilePicUrl,userProfiles.screenName);
                userProfiles.profilePicUrl = '/profileImages/'+userProfiles.screenName;

                var dataGoogle=JSON.parse(userProfiles.google);
                var dataLinkedin =JSON.parse(userProfiles.linkedin);
                var dataFacebook = JSON.parse(userProfiles.facebook);
                var dataTwitter = JSON.parse(userProfiles.twitter);

                userProfiles.google=dataGoogle;
                userProfiles.linkedin=dataLinkedin;
                userProfiles.facebook=dataFacebook;
                userProfiles.twitter=dataTwitter;

                if(userProfiles.profilePrivatePassword != ''){
                    userProfiles.profilePrivatePassword = EncryptionValue(userProfiles.profilePrivatePassword);
                }

                userManagements.addUserThirdParty(userProfiles,function(error,userDataTobeSend,message)
                {
                    if (error) {
                        logger.info('Error occured while writing user data in DB (googleLogin)');
                        res.redirect('/error');
                    }
               if(req.session.nonUserEmailId){
                                   var userData = {
                                       userId:userDataTobeSend._id,
                                       emailId:userDataTobeSend.emailId,
                                       firstName:userDataTobeSend.firstName+' '+userDataTobeSend.lastName,
                                       sharedOn:new Date(),
                                       accessStatus:true
                                   }

                                   var docId =  req.session.readDocumentId;
                                   var userId = userDataTobeSend._id;
                                   updateDocWithNewUserId(userData,docId)
                               }
                    if (message.message == 'userExist') {
                        res.send('<html><head><script>alert("User already exist");window.location.replace("/")</script></head></html>');
                    }
                    else{
                        var emailSenders = new emailSender();
                        emailSenders.sendRegistrationConfirmationMail(userDataTobeSend);
                        req.session.loginService ='';
                        var tokenSecret ="alpha";
                        var token = jwt.encode({ id: userDataTobeSend._id}, tokenSecret);

                        var user=token;
                        req.logIn(user, function(err){

                            if (err) {
                                return next(err);
                            }else{
                                req.session.dashboardMsg = true;
                                req.session.userLoginFlag = true;
                                var pageName = getLoginPageName(req);
                                common.insertLog(userDataTobeSend,pageName,req);
                              if(req.session.finalPage == 'documentViewer'){
                                    if(req.session.nonUserEmailId){
                                        var userData = {
                                            userId:userDataTobeSend._id,
                                            emailId:userDataTobeSend.emailId,
                                            firstName:userDataTobeSend.firstName+' '+userDataTobeSend.lastName,
                                            sharedOn:new Date(),
                                            accessStatus:true
                                        }

                                        var docId =  req.session.readDocumentId;
                                        var userId = userDataTobeSend._id;
                                        updateDocWithNewUserId(userData,docId,req,res)
                                        res.send(true);
                                    }else res.send(true);

                                }
                                else res.send(true)
                            }
                        })

                    }

                });
            }
            else{
                logger.info("user credential is invalid");

                res.send(status);

            }

        });
    }
        //TODO: send the correct error message!:
    catch(err)
    {
        logger.info('Error occurred in user linkedin Registration '+err);
        throw  err;
    }
});

router.post('/uploadImage',function(req,res){

    var fstream;


    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename) {

        var currentTime = new Date();
                var fileNameUnique =''+currentTime.getFullYear()+''+currentTime.getMonth()+''+currentTime.getDate()+''+currentTime.getHours()+''+currentTime.getMinutes()+''+ filename;

                var imagePath = '/profileImages/'+fileNameUnique;
                fstream = fs.createWriteStream('./public/profileImages/' + fileNameUnique);
                file.pipe(fstream);
                fstream.on('close', function () {
                    req.session.profileImageUrl = imagePath;

                    res.redirect('back');
                });
    });
});

router.get('/getProfileImageUrl',function(req,res){
    res.send(req.session.profileImageUrl)
})

router.post('/uploadLinkedinImage',function(req,res){
    req.session.profileImageUrl = 'profileImages/'+ req.body.name;
       storeProfileImg(req.body.url,req.body.name)
    setTimeout(function(){
        res.send(true)
    },1000)

});

function storeProfileImg(url, imageName, isLinkedinImage, userId) {

    if (url != 'images/default.png' && url != '/images/default.png') {
        request.get({url: url, encoding: 'binary'}, function (err, response, body) {

            if(!err && body != null && body != undefined && !common.contains(body,'html') && !common.contains(body,'title')){
                fs.writeFile('./public/profileImages/' + imageName, body, 'binary', function (err) {
                    if (err){
                        logger.info('Error in getting image from third party server ',err);
                    }
                    else{
                        logger.info('Saving image in server success (image from thirdParty servers)');
                    }
                });
            }
            else{
                logger.info('Error in getting image from third party emailRegistration: router ', err);
            }
        });
    }
}

router.get('/getLocation',function(req,res){
    var ipAddr = common.getClientIp2(req);
    var geo = geoip.lookup(ipAddr.toString());
    var currentLocation = getCurrentLocation(geo);
    res.send(currentLocation);
});

router.post('/settings/update/reporting/manager',common.isLoggedInUserToGoNext,function (req,res) {

    var userId = common.getUserId(req.user);
    var pathUpdate = {companyId:req.body.companyId,userId:userId,parentId:req.body.reportingManager};
    
    if(common.checkRequired(pathUpdate.parentId) && common.checkRequired(pathUpdate.companyId)){
        admin.updateAssigned(pathUpdate,function(isSuccess){
            res.send(true)
        })
    } else {
        res.send(false)
    }
});

function updateDocWithNewUserId(userData,docId,req,res){

    userManagements.updateDocumentSharedWith(docId,userData,function(error,isUpdated,message){
            logger.info(message.message);
    })
}


function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
};

function getCurrentLocation(geo){
    if(checkRequred(geo)){
       if(checkRequred(geo.country) && checkRequred(geo.city) && checkRequred(geo.region) && checkRequred(geo.ll[0]) && checkRequred(geo.ll[1])){
           var location = {
               country:geo.country,
               city:geo.city,
               region:geo.region,
               latitude:geo.ll[0],
               longitude:geo.ll[1]
           }
           return location;
       }
        else  return null;
    }
    else return null;
}

function checkRequred(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}


// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        return next();
    }else
    {
        // if they aren't redirect them to the home page
        return res.redirect('/');
    }
}

module.exports = router;
