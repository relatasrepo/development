
var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var request = require('request');
var passport = require('passport');
var csv = require('ya-csv');
var fs = require('fs');
var xlsx = require('xlsx');
var xlsjs = require('xlsjs');
var schedule = require('node-schedule');
var mongoose = require('mongoose');

var userManagement = require('../dataAccess/userManagementDataAccess');
var massMailCountManagement = require('../dataAccess/massMailCountManagement');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var checkDocumentExpired = require('../dataAccess/changeDocumentExpiry');
var messageDataAccess = require('../dataAccess/messageManagement');
var googleCalendarApi = require('../common/agendaMail');
var commonUtility = require('../common/commonUtility');
var linkedinM = require('../common/linkedin');
var twitterM = require('../common/twitter');
var facebookM = require('../common/facebook');
var contactClass = require('../dataAccess/contactsManagementClass');

var contactObj = new contactClass();
var emailSenderObj = new emailSender();
var linkedinObj = new linkedinM();
var twitterObj = new twitterM();
var facebookObj = new facebookM();
var GCAIP = new googleCalendarApi();
var appCredential = new appCredentials();
var userManagements = new userManagement();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var domainName = domain.domainName;
var logLib = new winstonLog();
var common = new commonUtility();

var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();

var documentExpireRule = new schedule.RecurrenceRule();
documentExpireRule.minute = 1;
var documentExpireRuleObject = schedule.scheduleJob(documentExpireRule, function(){
    updateExpiredDocuments();
});


//console.log(appCredential.getServerEnvironment());
//
//if(appCredential.getServerEnvironment() == 'LOCAL1'){
//    logger.info('Schedule is Runnng');
//    var sendDailyAgendaRuleZone1 = new schedule.RecurrenceRule(); //00:45 AM IST
//    sendDailyAgendaRuleZone1.hour = 19;  //19:15 UTC
//    sendDailyAgendaRuleZone1.minute = 15;
//    var sendDailyAgendaRuleObjectZone1 = schedule.scheduleJob(sendDailyAgendaRuleZone1, function(){
//        logger.info('GCAPI IS RUNNING..... zone 1 -- 00:45 AM IST');
//        loggerError.info('GCAPI IS RUNNING..... zone 1 -- 00:45 AM IST');
//        //GCAIP.sendDailyCalendarEventsAgenda(1);
//    });
//
//    var sendDailyAgendaRuleZone2 = new schedule.RecurrenceRule(); //05:45 AM IST
//    sendDailyAgendaRuleZone2.hour = 0;  //0:15
//    sendDailyAgendaRuleZone2.minute = 15;
//    var sendDailyAgendaRuleObjectZone2 = schedule.scheduleJob(sendDailyAgendaRuleZone2, function(){
//        logger.info('GCAPI IS RUNNING..... zone 2 -- 05:45 AM IST');
//        loggerError.info('GCAPI IS RUNNING..... zone 2 -- 05:45 AM IST');
//        //GCAIP.sendDailyCalendarEventsAgenda(2);
//    });
//
//    var sendDailyAgendaRuleZone3 = new schedule.RecurrenceRule(); //10:45 AM IST
//    sendDailyAgendaRuleZone3.hour = 5;  // 5:15
//    sendDailyAgendaRuleZone3.minute = 15;
//
//    var naveen = new Date();
//    var test = new Date(naveen.setMinutes(naveen.getMinutes() + 1));
//    console.log(new Date(naveen.setMinutes(naveen.getMinutes() + 1)))
//
//    var sendDailyAgendaRuleObjectZone3 = schedule.scheduleJob(new Date(), function(){
//        logger.info('GCAPI IS RUNNING..... zone 3 -- 10:45 AM IST');
//        loggerError.info('GCAPI IS RUNNING..... zone 3 -- 10:45 AM IST');
//        GCAIP.sendDailyCalendarEventsAgenda(3);
//    });
//
//    var sendDailyAgendaRuleZone4 = new schedule.RecurrenceRule(); //5:00 PM IST
//    sendDailyAgendaRuleZone4.hour = 11;  // 11:30
//    sendDailyAgendaRuleZone4.minute = 30;
//    var sendDailyAgendaRuleObjectZone4 = schedule.scheduleJob(sendDailyAgendaRuleZone4, function(){
//        logger.info('GCAPI IS RUNNING..... zone 4 -- 5:00 PM IST');
//        loggerError.info('GCAPI IS RUNNING..... zone 4 -- 5:00 PM IST');
//        //GCAIP.sendDailyCalendarEventsAgenda(4);
//    });
//}

function updateExpiredDocuments(){
    var expiry = new checkDocumentExpired();
    expiry.checkExpiry();
}

router.get('/getImage/:userId/:date',function(req,res){
    var userId = req.params.userId;
    senImage(userId,req,res);
});

router.get('/getImage/:userId',function(req,res){
    var userId = req.params.userId;
    senImage(userId,req,res);
});

function senImage(userId,req,res){

    res.send(404)
    // userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,profilePicUrl:1,publicProfileUrl:1},function(errorUser,userProfile) {
    //
    //     if(errorUser || !checkRequred(userProfile)){
    //         res.send(404)
    //        // res.download('./public/images/default.png');
    //     }else{
    //
    //         if(checkRequred(userProfile.profilePicUrl) && userProfile.profilePicUrl.charAt(0) == 'h'){
    //             storeProfileImg(userProfile.profilePicUrl,common.getValidUniqueName(userProfile.publicProfileUrl),userId,req,res);
    //         }else{
    //             //common.checkImageExtension(userProfile);
    //             var url = checkRequred(userProfile.profilePicUrl) ? userProfile.profilePicUrl.split('/') : null;
    //             var u;
    //             if(checkRequred(url)){
    //                 u = url[url.length-2]+'/'+url[url.length-1]
    //             }
    //             if(checkRequred(userProfile.profilePicUrl) && userProfile.profilePicUrl != "/images/default.png"){
    //
    //                 res.download('./public/'+u || 'images/default.png',function(error){
    //                     if(error){
    //                         res.send(404)
    //                         //res.download('./public/images/default.png');
    //                     }
    //                 });
    //             }
    //             else res.send(404)
    //         }
    //     }
    // })
}

function storeProfileImg(url,imageName,userId,req,res){

    if(url != 'images/default.png'){

        request.get({url: url, encoding: 'binary'}, function (err, response, body) {
            if(!err && body != null && body != undefined && !common.contains(body,'html') && !common.contains(body,'title')){
                fs.writeFile('./public/profileImages/'+imageName, body, 'binary', function(err) {
                    if(err){
                        logger.info('Error in getting image from third party server '+err);
                        res.download('./public/images/default.png');
                    }
                    else{

                        logger.info('Saving image in server success (image from thirdParty servers)');
                        userManagements.updateProfileImage(userId,'/profileImages/'+imageName,function(error,isUpdated,msg){
                            userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,profilePicUrl:1},function(err,pr){
                                common.checkImageExtension(pr,function(){
                                    senImage(userId,req,res)
                                });
                            })
                        });
                    }
                });
            }
            else{
                logger.info('Error in getting image from third party old:contacts:router ', err);
            }
        });
    } else {
        res.send(404)
    }
};

router.get('/importLinkedinContacts',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for import linkedin contacts';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,lonkedin:1,firstName:1,lastName:1},function(error,userProfile) {
        userLogInfo.functionName = 'selectUserProfile in importLinkedinContacts router'
        if (error) {
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching user profile using id';
            logger.info('Error in mongo',userLogInfo);
            userProfile = null;
            res.send('error');

        }
        else {
            if(checkRequred(userProfile.linkedin)){
                if(checkRequred(userProfile.linkedin.token)){
                    var authProfileOptions = {
                        url: 'https://api.linkedin.com/v1/people/~/connections:(headline,first-name,last-name,picture-url)',
                        headers: { 'x-li-format': 'json' },
                        qs: { oauth2_access_token: userProfile.linkedin.token }
                    };

                    request(authProfileOptions, function (error, r, user1Connections) {
                        if (checkRequred(r)) {

                            if (r.statusCode != 200) {
                                userLogInfo.error = error;
                                userLogInfo.info = ''
                                logger.info('error occurred while requesting linkedin common connections, in authProfileOptions contacts', userLogInfo);
                                res.send({message: 'No common connections found on linkedin'})

                            }
                            else {
                                var  authUserConnections = JSON.parse(user1Connections);

                            }
                        }
                    })
                } else res.send('noAccount')
            } else res.send('noAccount')
        }
    });
});

router.get('/contacts/filter/total',function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(userId)){
        var emailsArr = []
        userId = mongoose.Types.ObjectId(userId);
        userManagements.getRelatasUserContacts(userId,function(con1,emails){

            if(common.checkRequired(emails) && emails.length > 0){
               emailsArr = emails;
            }

            userManagements.getNonRelatasContacts(userId,emailsArr,function(con2,emails){
                if(common.checkRequired(emails) && emails.length > 0){
                    emailsArr = emails;
                }
                userManagements.getRelatasContactsNoUser(userId,emails,function(con3,emails){
                    userManagements.totalContactsFindOne(userId,function(error,user,message){
                        if(common.checkRequired(user) && common.checkRequired(user.contacts) && user.contacts.length > 0) {
                            //res.send(user.contacts);
                            res.send({relatas:con1,linkedin:con2,nonRelatas:con3,total:user.contacts.length});
                        }
                        else{
                            res.send({relatas:con1,linkedin:con2,nonRelatas:con3});
                        }
                    })

                });
            });
        });
    }else res.send([]);
});

router.get('/contacts/filter/relatasUser',function(req,res){
   var userId = common.getUserId(req.user);
    if(common.checkRequired(userId)){
        userManagements.getRelatasUserContacts(mongoose.Types.ObjectId(userId),function(con){
            res.send(con)
        });
    }else res.send([]);
});

router.get('/contacts/filter/nonrelatascontacts',function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(userId)){
        userManagements.getNonRelatasContacts(mongoose.Types.ObjectId(userId),function(con){
            res.send(con)
        });
    }else res.send([]);
});

router.get('/contacts/filter/nonrelatasUsercontacts',function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(userId)){
        userManagements.getRelatasContactsNoUser(mongoose.Types.ObjectId(userId),function(con){
            res.send(con)
        });
    }else res.send([]);
});

router.get('/getTotalContacts',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for total contacts ';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.totalContactsFindOne(userId,function(error,user,message){
        userLogInfo.functionName = 'totalContacts in totalContacts router';
        if(error){
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching contacts';
            logger.info('Error in mongo',userLogInfo);
            res.send('error');
        }
        else{
            if(common.checkRequired(user) && common.checkRequired(user.contacts) && user.contacts.length > 0) {
               res.send(user.contacts);
            }
            else{
                res.send(false);
            }
        }
    });
});

router.get('/uploadContactStatusMsg',function(req,res){
    var msg = req.session.uploadContactsStatusMsg;
    req.session.uploadContactsStatusMsg = null;
    if(checkRequred(msg)){
        req.session.uploadContactsStatusMsg = null;
        res.send(msg);
    }
    else res.send(true)
})

router.get('/clearUploadContactStatusMsg',function(req,res){

    req.session.uploadContactsStatusMsg = null;

    res.send(true)
});

router.post('/uploadCSVFile',isLoggedInUser,function(req,res){
    req.session.uploadContactsStatusMsg = '';
    var fstream;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {

        if(filename.split('.')[filename.split('.').length-1] == 'csv' || filename.split('.')[filename.split('.').length-1] == 'CSV'){
            parseAndStoreCsvContacts(fieldname, file, filename,userId,userManagements,fstream,req,res)
        }
        else if(filename.split('.')[filename.split('.').length-1] == 'xlsx' || filename.split('.')[filename.split('.').length-1] == 'XLSX'){
            parseAndStoreXlsxContacts(fieldname, file, filename,userId,userManagements,fstream,req,res)
        }
        else if(filename.split('.')[filename.split('.').length-1] == 'xls' || filename.split('.')[filename.split('.').length-1] == 'XLS'){
            parseAndStoreXlsContacts(fieldname, file, filename,userId,userManagements,fstream,req,res)
        }
        else{
            req.session.uploadContactsStatusMsg ={msg:'<span>Please upload a valid csv or xlsx file. Please click <a target="_blank" href="https://medium.com/relatas/mass-email-linkedin-contacts-in-5-steps/">here</a> for correct format.</span>', status:'error'} ;

                res.redirect('back');

        }
    })
});

function goBack(req,res){
    req.session.uploadContactsStatusMsg = {msg:'<span>Uploaded file format is not supported. Please click <a target="_blank" href="https://medium.com/relatas/mass-email-linkedin-contacts-in-5-steps/">here</a> for correct format.</span>', status:'error'};
    res.redirect('back');
}

function parseAndStoreCsvContacts(fieldname, file, filename,userId,userManagements,fstream,req,res){
    var contactsArr = [];
    var dataArray = [];
    fstream = fs.createWriteStream('./public/' + filename);
    file.pipe(fstream);

    fstream.on('close', function () {
        var reader = csv.createCsvFileReader('./public/' + filename, { columnsFromHeader: true });
        reader.addListener('data', function(data) {

            dataArray.push(data)
            var firstName = data["First Name"] || '';
            var lastName = data["Last Name"] || '';
            var name = firstName+' '+lastName


            var emailId = data["E-mail Address"]
            var companyName = data["Company"]
            var designation = data["Job Title"]
            var mobilePhone = data["Mobile Phone"]
            var primaryPhone = data["Primary Phone"]
            var mobile = checkRequred(mobilePhone) ? mobilePhone.toString() : checkRequred(primaryPhone) ? primaryPhone.toString() :'';
            if(checkRequred(emailId) && checkRequred(firstName)){
                var contact = {
                    personId:'',
                    personName:name,
                    personEmailId:emailId.trim().toLowerCase(),
                    companyName:companyName || '',
                    designation:designation || '',
                    mobileNumber:mobile,
                    count:0,
                    addedDate:new Date(),
                    verified:false,
                    relatasUser:false,
                    relatasContact:false,
                    source:'linkedin'
                }
                contactsArr.push(contact)
            }
        });
        reader.addListener('end', function() {

            if(checkRequred(contactsArr[0])){
                var contacts = removeDuplicates(contactsArr)

               for(var i in contacts){
                   contactObj.addSingleContact(userId,contacts[i])
                }

                req.session.uploadContactsStatusMsg = {msg:'<span>Contacts successfully uploaded.</span>', status:'success'};
                fs.unlink('./public/' + filename)
                res.redirect('back');
            }
            else{
                fs.unlink('./public/' + filename)
                parseCSVStringToJson(dataArray,req,res,userId)

            }
        })

    });
}

function parseCSVStringToJson(CSVStringArray,req,res,userId){
    var data = [];

        if(checkRequred(CSVStringArray[0])) {
            var eachKey;
            for (var eKey in CSVStringArray[0]) {
                eachKey = eKey.split('\t')
            }
            for (var i = 0; i < CSVStringArray.length; i++) {
                for (var key in CSVStringArray[i]) {

                    var a = CSVStringArray[i][key];
                    var valuesArray = a.split('\t');
                    var obj = {

                    }
                    for (var j = 0; j < eachKey.length; j++) {
                        if (checkRequred(eachKey[j])) {

                            obj[eachKey[j]] = valuesArray[j];

                        }
                    }
                    data.push(obj)

                }
            }
          var contactsArr = [];
            if(checkRequred(data[0])){
                for (var l = 0; l < data.length; l++) {
                    var firstName = data[l]["First Name"] || '';
                    var lastName = data[l]["Last Name"] || '';
                    var name = firstName + ' ' + lastName


                    var emailId = data[l]["E-mail Address"]
                    var companyName = data[l]["Company"]
                    var designation = data[l]["Job Title"]
                    var mobilePhone = data[l]["Mobile Phone"]
                    var primaryPhone = data[l]["Primary Phone"]
                    var mobile = checkRequred(mobilePhone) ? mobilePhone.toString() : checkRequred(primaryPhone) ? primaryPhone.toString() : '';
                    if (checkRequred(emailId) && checkRequred(firstName)) {
                        var contact = {
                            personId: '',
                            personName: name,
                            personEmailId: emailId.trim().toLowerCase(),
                            companyName: companyName || '',
                            designation: designation || '',
                            mobileNumber: mobile,
                            count: 0,
                            addedDate: new Date(),
                            verified: false,
                            relatasUser: false,
                            relatasContact: false,
                            source:'linkedin'
                        }
                        contactsArr.push(contact)
                    }
                    if(l == data.length-1){
                        var contacts = removeDuplicates(contactsArr)

                        for(var m in contacts){
                            contactObj.addSingleContact(userId,contacts[m])
                        }
                        req.session.uploadContactsStatusMsg = {msg:'<span>Contacts successfully uploaded.</span>', status:'success'};
                        res.redirect('back');
                    }
                }
            }
            else{
                req.session.uploadContactsStatusMsg = {msg:'<span>Uploaded file format is not supported. Please click <a target="_blank" href="https://medium.com/relatas/mass-email-linkedin-contacts-in-5-steps/">here</a> for correct format.</span>', status:'error'};
                res.redirect('back');
            }
        }
        else{
            req.session.uploadContactsStatusMsg = {msg:'<span>Uploaded file format is not supported. Please click <a target="_blank" href="https://medium.com/relatas/mass-email-linkedin-contacts-in-5-steps/">here</a> for correct format.</span>', status:'error'};
            res.redirect('back');
        }
}

function parseAndStoreXlsxContacts(fieldname, file, filename,userId,userManagements,fstream,req,res){
    var contactsArr = [];

    var currentTime = new Date();
    var fileNameUnique =''+currentTime.getFullYear()+''+currentTime.getMonth()+''+currentTime.getDate()+''+currentTime.getHours()+''+currentTime.getMinutes()+''+ filename;


    fstream = fs.createWriteStream('./public/' + filename);
    file.pipe(fstream);


    fstream.on('close', function () {
      //  var reader = csv.createCsvFileReader('./public/' + filename, { columnsFromHeader: true });
      var out = xlsx.readFile('./public/' + filename);

        var result = {};
        out.SheetNames.forEach(function(sheetName) {
            var roa = xlsx.utils.sheet_to_row_object_array(out.Sheets[sheetName]);
            if(roa.length > 0){
                result[sheetName] = roa;
            }
        });

        var contactsArr = [];
       if(checkRequred(result)){
           if(checkRequred(result.Sheet1)){
               if(checkRequred(result.Sheet1[0])){
                   var data = result.Sheet1;
                   for (var i=0; i<data.length; i++){
                       var firstName = data[i]["First Name"] || '';
                       var lastName = data[i]["Last Name"] || '';
                       var name = firstName+' '+lastName


                       var emailId = data[i]["Email Id"]
                        var skypeId = data[i]["Skype Id"]
                       var mobilePhone = data[i]["Mobile Number"]

                       if(checkRequred(emailId) && checkRequred(firstName)){
                           var contact = {
                               personId:'',
                               personName:name,
                               personEmailId:emailId.trim().toLowerCase(),
                               mobileNumber:mobilePhone || '',
                               skypeId:skypeId || '',
                               count:0,
                               addedDate:new Date(),
                               verified:false,
                               relatasUser:false,
                               relatasContact:true,
                               source:'linkedin'
                           }
                           contactsArr.push(contact)
                       }
                   }

                   if(checkRequred(contactsArr[0])){
                       var contacts = removeDuplicates(contactsArr)

                       for(var j in contacts){
                           contactObj.addSingleContact(userId,contacts[j])
                       }

                       req.session.uploadContactsStatusMsg = {msg:'<span>Contacts successfully uploaded.</span>', status:'success'};
                       fs.unlink('./public/' + filename)
                       res.redirect('back');
                   }
                   else{
                       req.session.uploadContactsStatusMsg = {msg:'<span>Uploaded file format is not supported. Please click <a target="_blank" href="https://medium.com/relatas/mass-email-linkedin-contacts-in-5-steps/">here</a> for correct format.</span>', status:'error'};
                       fs.unlink('./public/' + filename)
                       res.redirect('back');
                   }
               }else{
                   fs.unlink('./public/' + filename)
                   goBack(req,res)
               }
           }else{
               fs.unlink('./public/' + filename)
               goBack(req,res)
           }
       }else{
           fs.unlink('./public/' + filename)
           goBack(req,res)
       }

    });
}

function parseAndStoreXlsContacts(fieldname, file, filename,userId,userManagements,fstream,req,res){
    var contactsArr = [];

    var currentTime = new Date();
    var fileNameUnique =''+currentTime.getFullYear()+''+currentTime.getMonth()+''+currentTime.getDate()+''+currentTime.getHours()+''+currentTime.getMinutes()+''+ filename;


    fstream = fs.createWriteStream('./public/' + filename);
    file.pipe(fstream);


    fstream.on('close', function () {
        //  var reader = csv.createCsvFileReader('./public/' + filename, { columnsFromHeader: true });
        var out = xlsjs.readFile('./public/' + filename);

        var result = {};
        out.SheetNames.forEach(function(sheetName) {
            var roa = xlsjs.utils.sheet_to_row_object_array(out.Sheets[sheetName]);
            if(roa.length > 0){
                result[sheetName] = roa;
            }
        });

        var contactsArr = [];
        if(checkRequred(result)){
            if(checkRequred(result.Sheet1)){
                if(checkRequred(result.Sheet1[0])){
                    var data = result.Sheet1;
                    for (var i=0; i<data.length; i++){

                        var firstName = data[i]["First Name"] || '';
                        var lastName = data[i]["Last Name"] || '';
                        var name = firstName+' '+lastName
                        var emailId = data[i]["Email Id"]
                        var skypeId = data[i]["Skype Id"]
                        var mobilePhone = data[i]["Mobile Number"]

                        if(checkRequred(emailId) && checkRequred(firstName)){
                            var contact = {
                                personId:'',
                                personName:name,
                                personEmailId:emailId.trim().toLowerCase(),
                                mobileNumber:mobilePhone || '',
                                skypeId:skypeId || '',
                                count:0,
                                addedDate:new Date(),
                                verified:false,
                                relatasUser:false,
                                relatasContact:true,
                                source:'linkedin'
                            }
                            contactsArr.push(contact)
                        }
                    }

                    if(checkRequred(contactsArr[0])){
                        var contacts = removeDuplicates(contactsArr)

                        for(var j in contacts){
                            contactObj.addSingleContact(userId,contacts[j])
                        }

                        req.session.uploadContactsStatusMsg = {msg:'<span>Contacts successfully uploaded.</span>', status:'success'};
                        fs.unlink('./public/' + filename)
                        res.redirect('back');
                    }
                    else{
                        req.session.uploadContactsStatusMsg = {msg:'<span>Uploaded file format is not supported. Please click <a target="_blank" href="https://medium.com/relatas/mass-email-linkedin-contacts-in-5-steps/">here</a> for correct format.</span>', status:'error'};
                        fs.unlink('./public/' + filename)
                        res.redirect('back');
                    }
                }else{
                    fs.unlink('./public/' + filename)
                    goBack(req,res)
                }
            }else{
                fs.unlink('./public/' + filename)
                goBack(req,res)
            }
        }else{
            fs.unlink('./public/' + filename)
            goBack(req,res)
        }
    });
}

router.post('/sendEmailMessage',isLoggedInUser,function(req,res){
       var msgObj = req.body;
       var subject = msgObj.subject;
       var emailSenders = new emailSender();
       var messageAccess = new messageDataAccess();
       var massMailCountManagements = new massMailCountManagement();
       var jsonMsg = JSON.parse(msgObj.data);

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.findUserProfileByIdWithCustomFields(userId,{firstName:1,lastName:1,emailId:1,timezone:1,companyName:1,designation:1,location:1},function(err,user){
        if(common.checkRequired(user)){

            var senderName_new = user.firstName+' '+user.lastName;

            var cName = user.companyName || '';
            var des = user.designation || '';
            var comma = (common.checkRequired(cName) && common.checkRequired(des)) ? ', ' : ' ';
            var senderInfo = des+comma+cName;
            var senderLocation = user.location;

            if(checkRequred(jsonMsg[0]) && checkRequred(userId)){


                var params = {
                    "template_name": "R_MailMass",
                    "template_content": [
                        {
                            "name": "R_MailMass",
                            "content": "example content"
                        }
                    ],
                    "message": {
                        "subject": subject || "Relatas: Message",
                        "from_email": "no-reply@relatas.co.in",
                        "from_name": jsonMsg[0].senderName || "Relatas",
                        "to": [

                        ],

                        "merge": true,

                        "merge_vars": [

                        ]

                    }
                }

                var interactionFlag = true;
                for (var i=0; i<jsonMsg.length; i++){
                    //var trackUrl = 'http://showcase.relatas.com/track/email/open/'+jsonMsg[i].receiverEmailId;
                    var image = '';
                    if(jsonMsg[i].headerImage == true || jsonMsg[i].headerImage == 'true'){
                        image += '<img style="width:100%; height:100px !important;" src="'+jsonMsg[i].imageUrl+'">';
                    }
                    var recp = {
                        "email":jsonMsg[i].receiverEmailId
                    }
                    var mergvars = {
                        "rcpt":jsonMsg[i].receiverEmailId,
                        "vars": [
                            {
                                "name": "senderName",
                                "content":  senderName_new
                            },
                            {
                                "name": "senderPic",
                                "content":  domainName+'/getImage/'+userId
                            },
                            {
                                "name": "senderInfo",
                                "content": senderInfo
                            },
                            {
                                "name": "senderLocation",
                                "content": senderLocation
                            },
                            {
                                "name": "receiverName",
                                "content": jsonMsg[i].receiverName || ''
                            },
                            {
                                "name": "message",
                                "content":  jsonMsg[i].message
                            }
                        ]
                    };

                    params.message.to.push(recp)
                    params.message.merge_vars.push(mergvars)
                    var message = {
                        senderId:userId,
                        receiverId:jsonMsg[i].userId || '',
                        receiverEmailId:jsonMsg[i].receiverEmailId,
                        subject:subject,
                        type:'message',
                        headerImage:jsonMsg[i].headerImage ? true: false,
                        imageUrl:jsonMsg[i].imageUrl,
                        imageName:jsonMsg[i].imageName,
                        message:jsonMsg[i].message
                    };
                    messageAccess.createMessage(message,function(message){

                        if(message){

                            var interaction = {};
                            interaction.userId = message.senderId;
                            interaction.action = 'sender';
                            interaction.type = 'message';
                            interaction.subType = 'message';
                            interaction.refId = message._id;
                            interaction.source = 'relatas';
                            interaction.title = message.subject;
                            interaction.description = message.message;
                            common.storeInteraction(interaction);

                            var interactionReceiver = {};
                            interactionReceiver.userId = message.receiverId || '';
                            interactionReceiver.emailId = message.receiverEmailId;
                            interactionReceiver.action = 'receiver';
                            interactionReceiver.type = 'message';
                            interactionReceiver.subType = 'message';
                            interactionReceiver.refId = message._id;
                            interactionReceiver.source = 'relatas';
                            interactionReceiver.title = message.subject;
                            interactionReceiver.description = message.message;
                            common.storeInteractionReceiver(interactionReceiver);
                        }
                    });
                    generateContact(jsonMsg[i],userId);
                    if(checkRequred(jsonMsg[i].userId)){
                        updateReceiverContacts(userId,jsonMsg[i].userId);
                    }
                }
                var mmc = jsonMsg.length || 0;

                massMailCountManagements.incrementMassMailCount(userId,mmc,function(error,isUpdated){

                });
                setTimeout(function(){
                    emailSenders.sendEmailMessage(params)

                    res.send(true);
                },100)

            }else{
                res.send(false);
            }
        }
        else{
            res.send(false);
        }
    });

/*
if(checkRequred(jsonMsg[0]) && checkRequred(userId)){


    var params = {
        "template_name": "MassMail",
        "template_content": [
            {
                "name": "MassMail",
                "content": "example content"
            }
        ],
        "message": {
            "subject": subject || "Relatas: Message",
            "from_email": "no-reply@relatas.co.in",
            "from_name": jsonMsg[0].senderName || "Relatas",
            "to": [

            ],

            "merge": true,

            "merge_vars": [

            ]

        }
    }

    var interactionFlag = true;
    for (var i=0; i<jsonMsg.length; i++){

        var image = '';
        if(jsonMsg[i].headerImage == true || jsonMsg[i].headerImage == 'true'){
            image += '<img style="width:100%; height:100px !important;" src="'+jsonMsg[i].imageUrl+'">';
        }
        var recp = {
            "email":jsonMsg[i].receiverEmailId
        }
        var mergvars = {
            "rcpt":jsonMsg[i].receiverEmailId,
            "vars": [
                {
                    "name": "header",
                    "content":  image
                },
                {
                    "name": "senderName1",
                    "content": jsonMsg[i].senderName
                },
                {
                    "name": "senderName2",
                    "content": jsonMsg[i].senderName
                },
                {
                    "name": "userMsg",
                    "content":  jsonMsg[i].message
                }
            ]
        }

        params.message.to.push(recp)
        params.message.merge_vars.push(mergvars)
        var message = {
               senderId:userId,
               receiverId:jsonMsg[i].userId || '',
               receiverEmailId:jsonMsg[i].receiverEmailId,
               subject:subject,
               type:'message',
               headerImage:jsonMsg[i].headerImage ? true: false,
               imageUrl:jsonMsg[i].imageUrl,
               imageName:jsonMsg[i].imageName,
               message:jsonMsg[i].message
        };
        messageAccess.createMessage(message,function(message){

            if(message){

                    var interaction = {};
                    interaction.userId = message.senderId;
                    interaction.action = 'sender';
                    interaction.type = 'message';
                    interaction.subType = 'message';
                    interaction.refId = message._id;
                    interaction.source = 'relatas';
                    interaction.title = message.subject;
                    interaction.description = message.message;
                    common.storeInteraction(interaction);

                var interactionReceiver = {};
                interactionReceiver.userId = message.receiverId || '';
                interactionReceiver.emailId = message.receiverEmailId;
                interactionReceiver.action = 'receiver';
                interactionReceiver.type = 'message';
                interactionReceiver.subType = 'message';
                interactionReceiver.refId = message._id;
                interactionReceiver.source = 'relatas';
                interactionReceiver.title = message.subject;
                interactionReceiver.description = message.message;
                common.storeInteractionReceiver(interactionReceiver);
            }
        });
        generateContact(jsonMsg[i],userId);
        if(checkRequred(jsonMsg[i].userId)){
            updateReceiverContacts(userId,jsonMsg[i].userId);
        }
      }
    var mmc = jsonMsg.length || 0;

    massMailCountManagements.incrementMassMailCount(userId,mmc,function(error,isUpdated){

    });
     setTimeout(function(){
         emailSenders.sendEmailMessage(params)

         res.send(true);
       },100)

    }else{
    res.send(false);
  }
*/
});

router.get('/validateMMC',function(req,res){

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var massMailCountManagements = new massMailCountManagement();
      if(checkRequred(userId)){
          var mmc = 0;
          massMailCountManagements.incrementMassMailCount(userId,mmc,function(error,isUpdated){
              res.send(true);
          })
      }else res.send(false);
});

function generateContact(jsonMsg,userId){

    var type = jsonMsg.relatasContact;
    var relatasContact = false;
    if(type == 'true' || type == true){
        relatasContact = true;
    }
    var emails = jsonMsg.receiverEmailId;
    if(checkRequred(emails)){
        userManagements.findUserProfileByEmailIdWithCustomFields(emails,{emailId:1,firstName:1,lastName:1,mobileNumber:1},function(error,profile){
            if(error){
            }else
            if(profile != null || profile != undefined){

                var contact1 = {
                    personId:profile._id,
                    personName:profile.firstName+' '+profile.lastName,
                    personEmailId:profile.emailId,
                    count:1,
                    lastInteracted:new Date(),
                    addedDate: new Date(),
                    verified:true,
                    relatasContact:true,
                    relatasUser:true,
                    mobileNumber:profile.mobileNumber
                }
                updateContact(userId,contact1);
            }
            else{
                if(checkRequred(jsonMsg.receiverName)){
                    var relatasContact = false;
                    if(type == 'true' || type == true){
                        relatasContact = true;
                    }
                    var contact2 = {
                        personId:jsonMsg.userId || '',
                        personName:jsonMsg.receiverName,
                        personEmailId:jsonMsg.receiverEmailId || '',
                        count:1,
                        lastInteracted:new Date(),
                        addedDate: new Date(),
                        verified:true,
                        relatasContact:relatasContact,
                        relatasUser:checkRequred(jsonMsg.userId) ? true : false
                    }
                    updateContact(userId,contact2);
                }
            }
        });
    }

}

//Function to add Contact
function updateContact(userId,contact){
    contactObj.addSingleContact(userId,contact)
}

router.post('/deleteContact',isLoggedInUser,function(req,res){
        var contact = req.body;

        if(checkRequred(contact.userId) && checkRequred(contact.contactId)){
            userManagements.deleteContact(contact.userId,contact.contactId,function(error,isSuccess){
                if(isSuccess){
                    res.send(true)
                }
                else res.send(false)
            })
        }
        else{
            res.send(false)
        }
});

router.post('/createContact',isLoggedInUser,function(req,res){
    var contactDetails = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    if(checkRequred(userId) && checkRequred(contactDetails.firstName) && checkRequred(contactDetails.lastName) && checkRequred(contactDetails.emailId)){

        userManagements.findUserProfileByEmailIdWithCustomFields(contactDetails.emailId,{firstName:1,lastName:1,emailId:1,mobileNumber:1,companyName:1,designation:1},function(error,profile){
            if(error){
            }else
            if(profile != null || profile != undefined){

                var contactObj = {
                    personId:profile._id,
                    personName:profile.firstName+' '+profile.lastName,
                    personEmailId:profile.emailId,
                    companyName:profile.companyName,
                    designation:profile.designation,
                    count:0,
                    addedDate: new Date(),
                    verified:true,
                    relatasContact:true,
                    relatasUser:true,
                    mobileNumber:profile.mobileNumber
                }
                addSingleContact(userId,contactObj,req,res)
            }
            else{
                var contactObj = {
                    personId:'',
                    personName:contactDetails.firstName+' '+contactDetails.lastName,
                    personEmailId:contactDetails.emailId.toLowerCase(),
                    count:0,
                    addedDate:new Date(),
                    verified:false,
                    relatasUser:false,
                    relatasContact:true
                }
                addSingleContact(userId,contactObj,req,res)
            }
        });
    }
    else res.send(false)
})

router.get('/getMassMailCount',isLoggedInUser,function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var massMailCountManagements = new massMailCountManagement();
    massMailCountManagements.getMassMailCountAccount(userId,function(error,status,accountDetails){
            if(error){
                res.send('error');
            }
            else{
                res.send(accountDetails);
            }
    })
});

router.post('/contacts/nonInteracted/emails',function(req,res){
    var strArr = req.body;
    if(checkRequred(strArr) && checkRequred(strArr.arrStr)){
        req.session.nonInteractedArrStr = strArr.arrStr;

        res.send(true);
    }else res.send(false);
});

function addSingleContact(userId,contactObjU,req,res){

    contactObj.addSingleContact(userId,contactObjU,function(error,isAdded,message){
        if(isAdded == 'exist'){
            res.send(isAdded)
        }
        else if(isAdded == true){
            res.send(true)
        }
        else res.send(false)
    })
}

function updateReceiverContacts(senderId,receiverId){

    userManagements.findUserProfileByIdWithCustomFields(senderId,{firstName:1,lastName:1,emailId:1,mobileNumber:1},function(error,profile) {

        if(error){

        }else
        if(profile != null || profile != undefined){

            var contact1 = {
                personId:profile._id,
                personName:profile.firstName+' '+profile.lastName,
                personEmailId:profile.emailId,
                count:1,
                lastInteracted:new Date(),
                addedDate: new Date(),
                verified:true,
                relatasContact:true,
                relatasUser:true,
                mobileNumber:profile.mobileNumber
            }
            updateContact(receiverId,contact1);
        }
    });
}

function getUserLogInfo(req){
    var userIp = getClientIp(req);
    var userLogInfo;
    if(req.session.userLogInfo){
        userLogInfo = req.session.userLogInfo;
    }
    else{
        userLogInfo = {
            userName:'',
            userIp:userIp,
            loginType:'',
            info:'',
            cause:'',
            action : ''
        }
        req.session.userLogInfo = userLogInfo;
    }
    return userLogInfo;
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
};

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }else
    {

        // if they aren't redirect them to the home page

        return res.redirect('/');
    }
}


function checkRequred(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

function removeDuplicates(arr){

    var end = arr.length;

    for(var i = 0; i < end; i++)
    {
        for(var j = i + 1; j < end; j++)
        {
            if(arr[i].personEmailId == arr[j].personEmailId)
            {
                var shiftLeft = j;
                for(var k = j+1; k < end; k++, shiftLeft++)
                {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

router.get('/profile/get/total/email/id',function(req,res){
    userManagements.findUserProfileByEmailIdWithCustomFields(req.query.emailId,{},function(er,pr){
        res.send(pr)
    })
});

/* NEW API'S  */

router.get('/favorite/social',isLoggedInUser,function(req,res){
    res.render('favoriteSocial');
});

router.get('/contacts/filter/favorite/web',isLoggedInUser,function(req,res){
    var userId = common.getUserId(req.user);
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

    contactObj.getAllFavoriteContactsWithProfile(mongoose.Types.ObjectId(userId),null,function(error,contacts,ids){
        var total = contacts.length;
        var results = contacts.splice(skip,limit);
        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data":{
                total:total,
                skipped:skip,
                limit:limit,
                returned:results.length,
                contacts:results,
                userIdArr:ids
            }
        });
    })
});

router.post('/contacts/favorite/social/feed/web',isLoggedInUser,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.body.userIdArr)){
        var arr = JSON.parse(req.body.userIdArr);

        var q = {
            _id:{$in:arr},
            $or: [
                {
                    linkedin: {
                        $exists: true
                    },
                    "linkedin.id": {
                        $exists: true,
                        $ne: ''
                    }
                },
                {
                    facebook: {
                        $exists: true
                    },
                    "facebook.id": {
                        $exists: true,
                        $ne: ''
                    }
                },
                {
                    twitter: {
                        $exists: true
                    },
                    "twitter.id": {
                        $exists: true,
                        $ne: ''
                    }
                }
            ]
        };

        userManagements.selectUserProfilesCustomQueryWithLimit(q,{firstName:1,lastName:1,emailId:1,linkedin:1,facebook:1,twitter:1},10,function(err,userList){

            if(common.checkRequired(userList) && userList.length > 0){

                /*getFeeds(userList.shift(),function(feed){
                    res.send(feed)
                })*/
                var total = [];

                feedsForAllUsers(userList,function(feed){
                    res.send(feed)
                });

                function feedsForAllUsers(users,callback){

                    getFeeds(users.shift(),function(feed){
                        if(feed){
                            total.push(feed)
                            feedsForAllUsers(users,callback)
                        }
                        else callback(total)
                    })
                }
            }
            else res.send(false);
        })
    }
    else res.send(false);
});


function getFeeds(user,callback){

    if(common.checkRequired(user)){

        linkedinObj.getUserFeeds(user.linkedin,6,0,function(err,lFeed){
            twitterObj.getTwitterFeed(user.twitter,true,6,function(err,tFeed){
                facebookObj.getFacebookFeeds(user.facebook,true,6,function(err,fFeed){
                    var feeds = {
                        linkedin:lFeed,
                        twitter:tFeed,
                        facebook:fFeed,
                        user:user
                    };
                    callback(feeds)
                })
            })
        })
    }
    else callback(false)
}

router.get('/social/feed/linkedin/feed/update/web',isLoggedInUser,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.query.updateKey)){
        userManagements.findUserProfileByIdWithCustomFields(userId,{linkedin:1},function(error,user){
            if(common.checkRequired(user)){
                switch (req.query.action){
                    case 'like': linkedinObj.likeFeed(user.linkedin,req.query.updateKey,function(error,isSuccess){
                        res.send(isSuccess);
                    });
                        break;
                    case 'comment':'';
                        break;
                    default : res.send(false)
                }
            }
            else{
                res.send(false)
            }
        })
    }
    else res.send(false);
});

router.get('/get/dups/contacts/:emailId',function(req,res){
    contactObj.removeDuplicateContacts(req.params.emailId,function(er,re){
        res.send({error:er,data:re});
    });
});

router.get('/get/noId/contacts/:emailId',function(req,res){
    contactObj.getContactsDoesNotHaveId(req.params.emailId,function(er,re){
        res.send({error:er,data:re});
    });
});

module.exports = router;