var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var mongoose = require('mongoose');
var Twit = require('twit');
var myUserCollection = require('../../databaseSchema/userManagementSchema').User;

var commonUtility = require('../../common/commonUtility');
var contactClass = require('../../dataAccess/contactsManagementClass');
var contactSupportClass = require('../../common/contactsSupport');
var mobileAuth = require('../../common/mobileAuthValidation');
var appCredentials = require('../../config/relatasConfiguration');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var interactions = require('../../dataAccess/interactionManagement');
var WinstonLog = require('../../common/winstonLog');

var logLib = new WinstonLog();
var logger = logLib.getWinston();
var errorObj = new errorClass();
var mobileAuthObj = new mobileAuth();
var appCredential = new appCredentials();
var common = new commonUtility();
var interactionObj = new interactions();
var validation = new validations();
var userManagementObj = new userManagement();
var errorMessagesObj = new errorMessages();
var contactObj = new contactClass();
var contactSupport = new contactSupportClass();

var statusCodes = errorMessagesObj.getStatusCodes();
var authConfig = appCredential.getAuthCredentials();

var Linkedin = require('node-linkedin')(authConfig.LINKEDIN_CONSUMER_KEY, authConfig.LINKEDIN_CONSUMER_SECRET, authConfig.LINKEDIN_REDIRECT_URL);
var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';

router.get('/contacts/filter/all',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)){

        var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
        var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                contactObj.getAllContactsWithProfile(user._id,function(err,contacts){
                    if(err){
                        res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                    }
                    else {
                        var total = contacts.length;
                        var results = contacts.splice(skip,limit);
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":{
                                total:total,
                                skipped:skip,
                                limit:limit,
                                returned:results.length,
                                contacts:results
                            }
                        });
                    }
                })
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/contacts/filter/new',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var date = moment().tz(timezone);
                date.date(date.date() - 7);
                date = date.format();

                var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

                contactObj.getNewContactsAdded(user._id,date,function(err,contacts){

                    if(err){
                        res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                    }else
                    if(contacts.length > 0){
                        if(req.query.returndata == 'count'){
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{
                                    count:contacts[0].count || 0
                                }
                            });
                        }
                        else{
                            contactObj.populateUserProfileContacts(contacts[0].contacts,function(pContacts,isPopulated){
                                pContacts.sort(function(a,b){
                                    return new Date(b.addedDate) - new Date(a.addedDate);
                                });
                                var total = pContacts.length;
                                var results = pContacts.splice(skip,limit);

                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":{
                                        total:total,
                                        skipped:skip,
                                        limit:limit,
                                        returned:results.length,
                                        contacts:results
                                    }
                                });
                            })
                        }
                    }
                    else{
                        if(req.query.returndata == 'count'){
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{
                                    count: 0
                                }
                            });
                        }
                        else{
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{
                                    total:0,
                                    skipped:skip,
                                    limit:limit,
                                    returned:0,
                                    contacts:[]
                                }
                            });
                        }
                    }
                })
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});
//use Losing/touch/info/? from web rest insights. -- Rajiv
router.get('/contacts/filter/losingtouch/mobile',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)) {
        userManagementObj.findUserProfileByIdWithCustomFields(userId, {
            emailId: 1,
            timezone: 1
        }, function (error, user) {
            if (common.checkRequired(user)) {
                var timezone;
                if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                    timezone = user.timezone.name;
                } else timezone = 'UTC';

                var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
                var dateMin = moment().tz(timezone);
                dateMin.date(dateMin.date() - 15)
                dateMin.hour(0)
                dateMin.minute(0)
                dateMin.second(0)
                dateMin.millisecond(0)

                contactObj.getAllFavoriteContactsEmailIds(user._id,user.emailId,function(emailIdArr){
                    if(emailIdArr.length > 0){
                        interactionObj.losingTouchByDateORDays(userId,emailIdArr,dateMin,function(losingTouch){
                            losingTouch = common.removeDuplicates_emailId(losingTouch);
                            if(req.query.returndata == 'count'){
                                sendResponseCount(req,res,losingTouch.count)
                            }
                            else{
                                var total = losingTouch.length;
                                var results = losingTouch.splice(skip,limit);

                                sendResponseData(req,res,{
                                    total:total,
                                    skipped:skip,
                                    limit:limit,
                                    returned:results.length,
                                    contacts:results
                                })
                            }
                        })
                    }
                    else{
                        if(req.query.returndata == 'count'){
                            sendResponseCount(req,res,0)
                        }
                        else{
                            sendResponseData(req,res,{
                                total:0,
                                skipped:skip,
                                limit:limit,
                                returned:0,
                                contacts:[]
                            })
                        }
                    }
                });

            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

function sendResponseCount(req,res,count){
    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data":{
            count: count
        }
    });
}

function sendResponseData(req,res,data){
    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data":data
    });
}

router.get('/contacts/filter/recentlyInteracted',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE)
    if(common.checkRequired(userId)){

        var skip = common.checkRequired(req.query.skip)?req.query.skip:0
        var limit = common.checkRequired(req.query.limit)?parseInt(req.query.limit):15

        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(err,userData){
            if(common.checkRequired(userData)){
                    contactSupport.getRecentlyInteractedContacts(common.castToObjectId(userId),skip,limit,req.query.searchContent,null,function(result){
                        res.send(result)
                    })
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
})

router.get('/contacts/filter/favorite',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var date = moment().tz(timezone);
                date.date(date.date() - 7);
                date = date.format();

                var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

                contactObj.getAllFavoriteContactsWithProfile(user._id,null,function(error,contacts,ids){
                    var total = contacts.length;
                    var results = contacts.splice(skip,limit);
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":{
                            total:total,
                            skipped:skip,
                            limit:limit,
                            returned:results.length,
                            contacts:results
                        }
                    });
                })
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.post('/contacts/filter/common', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        if (common.checkRequired(req.body.userId) && common.checkRequired(req.body.cUserId) && common.checkRequired(req.body.idType)) {

            var isEmailId = req.body.idType
            var limit = req.body.limit ? req.body.limit : 12
            var result = []
            var projectContact = {

                "_id": 1,
                "emailId": 1,
                "contacts._id": 1,
                "contacts.personId": 1,
                "contacts.personName": 1,
                "contacts.personEmailId": 1,
                "contacts.companyName": 1,
                "contacts.designation": 1,
                "contacts.mobileNumber": 1
                // "contacts.contactRelation":1,
                // "contacts.favorite":1,
                // "contacts.twitterUserName":1,
                // "contacts.addedDate":1,
                // "contacts.source":1,
                // "contacts.hashtag":1
            };

            var q = {}

            if (isEmailId) {
                q = {emailId: {$in: [req.body.userId, req.body.cUserId]}}
            }
            else {
                q = {_id: {$in: [req.body.userId, req.body.cUserId]}}
            }
            myUserCollection.aggregate([{$match: q}, {$project: projectContact}], function (error, contacts) {
                if (!error && contacts.length == 2) {
                    if (contacts[0]._id.toString() == userId.toString()) {
                        var uIndex = 0;
                        var cIndex = 1;
                    }
                    else {
                        var uIndex = 1;
                        var cIndex = 0;
                    }
                    contactObj.getCommonConnectionsNew(contacts[uIndex].emailId, contacts[cIndex].emailId, contacts[uIndex].contacts, contacts[cIndex].contacts, limit, function (contactsArr) {
                        result = contactsArr

                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {
                                //skipped:skip,
                                limit: limit,
                                returned: result.length,
                                contacts: result
                            }
                        });
                    })
                } else {
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": {
                            //skipped:skip,
                            limit: limit,
                            returned: result.length,
                            contacts: result
                        }
                    });

                }
            })
        } else res.send(errorObj.generateErrorResponse({
            status: statusCodes.INVALID_REQUEST_CODE,
            key: 'REQUEST_BODY_MISSED'
        }));
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

router.get('/contacts/linkedin/get/biography',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(req.query.id,{emailId:1,timezone:1,linkedin:1},function(error,user){
                if(common.checkRequired(user)){

                        if (common.checkRequired(user.linkedin) && common.checkRequired(user.linkedin.token)) {
                            var linkedIn = Linkedin.init(user.linkedin.token);
                            linkedIn.people.me(['first-name','last-name','headline','educations','summary'], function(err, profile) {
                                if(err || !common.checkRequired(profile)){
                                    logger.info('error in getting linkedin profile of 1 '+user.emailId+' error '+JSON.stringify(profile));
                                    res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'LINKEDIN_ERROR'}));
                                }
                                else{

                                        res.send({
                                            "SuccessCode": 1,
                                            "Message": "",
                                            "ErrorCode": 0,
                                            "Data":{
                                                linkedinProfile:profile
                                            }
                                        });

                                }
                            });
                        }
                        else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_LINKEDIN_ACCOUNT'}));

                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/contacts/twitter/get/statuses',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(req.query.id,{emailId:1,timezone:1,twitter:1},function(error,user){
                if(common.checkRequired(user)){

                    if (common.checkRequired(user.twitter) && common.checkRequired(user.twitter.token) && common.checkRequired(user.twitter.refreshToken)) {
                        var T = new Twit({
                            consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
                            consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
                            access_token:        user.twitter.token,
                            access_token_secret:  user.twitter.refreshToken
                        });

                        T.get('statuses/user_timeline', function(err, data, response) {

                            if (err) {

                                logger.info('error occurred while requesting latest tweets, in Twit module '+user.emailId);
                                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'TWITTER_ERROR'}));
                            }
                            else{
                                if (!common.checkRequired(data) && !common.checkRequired(data[0])) {
                                    res.send({
                                        "SuccessCode": 0,
                                        "Message": "No Data Found",
                                        "ErrorCode": 5001,
                                        "Error": "NO_TWEETS_FOUND",
                                        "Data": {}
                                    });
                                }
                                else{
                                    data = data.splice(0,30);
                                    var status = {
                                        id: user._id,
                                        userId: user._id,
                                        userEmailId: user.emailId,
                                        userFirstName: user.firstName,
                                        twitter:data
                                    };

                                    userManagementObj.storeTwitterStatus(status,function(error,isSuccess,message){
                                        if(error){
                                            logger.info(error);
                                        }
                                    });

                                    res.send(
                                        {
                                            "SuccessCode": 1,
                                            "Message": "",
                                            "ErrorCode": 0,
                                            "Data": {
                                                "tweets": data.splice(0,5)
                                            }
                                        }
                                    )
                                }
                            }
                        })
                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_TWITTER_ACCOUNT'}));

                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

/* CONTACTS UPDATE */

router.post('/contacts/update/favorite',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if(common.checkRequired(req.body.contactId)){
                userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                    if(common.checkRequired(user)){
                        var status = common.checkRequired(req.body.favorite) ? req.body.favorite : false;
                        contactObj.updateFavorite(user._id,common.castToObjectId(req.body.contactId),status,function(err,isSuccess){
                           if(err){
                               res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
                           }
                           else if(isSuccess){
                               res.send({
                                   "SuccessCode": 1,
                                   "Message": "",
                                   "ErrorCode": 0,
                                   "Data":{}
                               });
                           }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
                        })
                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                });
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_CONTACT_ID_FOUND'}));
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));

    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.post('/contacts/update/note',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if(common.checkRequired(req.body.contactId) && common.checkRequired(req.body.noteText)){
                userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                    if(common.checkRequired(user)){
                        contactObj.insertNote(user._id,req.body.contactId,req.body.noteText,function(err,isSuccess){

                            if(err){
                                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
                            }
                            else if(isSuccess){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":{}
                                });
                            }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
                        })
                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                });
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_CONTACT_ID_OR_NOTE_TEXT_FOUND'}));
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));

    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/fetch/favorite/contacts',common.isLoggedInUserOrMobile,function(req,res){

    var searchBy = req.query.searchBy;
    var userId = common.getUserIdFromMobileOrWeb(req);

    if(searchBy){

        contactObj.getFavoriteContactsByMobileOrEmailId(common.castToObjectId(userId),searchBy,function (err,contacts) {

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": contacts
            });

        });
    } else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}))

});

module.exports = router;