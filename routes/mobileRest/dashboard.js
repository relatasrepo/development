var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var mongoose = require('mongoose');

var commonUtility = require('../../common/commonUtility');
var contactClass = require('../../dataAccess/contactsManagementClass');
var mobileAuth = require('../../common/mobileAuthValidation');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var interactions = require('../../dataAccess/interactionManagement');
var meetingClass = require('../../dataAccess/meetingManagement');
var notificationSupportClass = require('../../common/notificationSupportClass');
var googleCalendarAPI = require('../../common/googleCalendar');
var insightsManagement = require('../../dataAccess/insightsManagement');
var _ = require("lodash");

var notificationSupportClassObj = new notificationSupportClass();
var googleCalendar = new googleCalendarAPI();
var errorObj = new errorClass();
var mobileAuthObj = new mobileAuth();
var common = new commonUtility();
var interactionObj = new interactions();
var validation = new validations();
var userManagementObj = new userManagement();
var errorMessagesObj = new errorMessages();
var contactObj = new contactClass();
var meetingsObj = new meetingClass();
var insightsManagementObj = new insightsManagement();

var statusCodes = errorMessagesObj.getStatusCodes();

var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';

function getGoogleDetails(userId){
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,lastLoginDate:1},function(error,user) {
        if (common.checkRequired(user)) {
            googleCalendar.getGoogleContactsEmailsEventsOffice365user(user);
        }
    })
}

router.get('/dashboard/status/count/mobile',mobileAuthObj.isAuthenticatedMobile,function(req,res){

    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){

        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,location:1,currentLocation:1},function(error,user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';
                //getGoogleDetails(user._id); did this get Google Interactions?:/
                var date = new Date().toISOString();

                var dateMin = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);
                var dateMax = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);

                dateMin.date(dateMin.date())
                dateMin.hour(0)
                dateMin.minute(0)
                dateMin.second(0)
                dateMin.millisecond(0)

                dateMax.date(dateMax.date())
                dateMax.hour(23)
                dateMax.minute(59)
                dateMax.second(59)
                dateMax.millisecond(59)

                dateMin = dateMin.format()
                dateMax = dateMax.format()

                interactionObj.interactionsByDateMinMaxDashboard(userId,dateMin,dateMax,function(confirmedMeetings){
                    getPendingMeetingsByDate(userId,dateMin,dateMax,function(pendingMeetings){
                        var count = 0;
                        if(common.checkRequired(confirmedMeetings) && confirmedMeetings.length > 0){
                            count += confirmedMeetings.length;
                        }
                        if(common.checkRequired(pendingMeetings) && pendingMeetings.length > 0){
                            count += pendingMeetings.length;
                        }
                        getTopFiveInteractionsCount(userId,user,count,timezone,req,res)
                    });
                })
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));

});

router.get('/dashboard/mobile/count/peopleconnections',mobileAuthObj.isAuthenticatedMobile,function(req,res){

    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE)
    if(common.checkRequired(userId)) {
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{timezone :1}, function (err, user) {
            if (err || user.length == 0)
                res.send(errorObj.generateErrorResponse({
                    status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                    key: 'SOMETHING_WENT_WRONG'
                }))
            else {
                if (user.timezone && user.timezone.name)
                    timezone = user.timezone.name;
                else
                    timezone = 'UTC';
                var dateMin = moment().tz(timezone);
                dateMin.hour(0)
                dateMin.minute(0)
                dateMin.second(0)
                dateMin.millisecond(0)
                var dateMax = moment().tz(timezone);
                dateMax.hour(23)
                dateMax.minute(59)
                dateMax.second(59)
                dateMax.millisecond(0)
                insightsManagementObj.peopleToConnect(common.castToObjectId(userId), dateMin, dateMax, timezone, function (err, result) {
                    if (!err && result) {
                        if (result.length>0)
                            res.send({
                                SuccessCode: 1,
                                ErrorCode: 0,
                                Data: {peopleToConnect: result.length}
                            })
                        else
                            res.send({
                                SuccessCode: 1,
                                ErrorCode: 0,
                                Data: result
                            })

                    } else {
                        res.send(errorObj.generateErrorResponse({
                            status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                            key: 'SOMETHING_WENT_WRONG'
                        }))
                    }
                });
            }
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'TOKEN_NOT_PROVIDED'}))
})



function getPendingMeetingsByDate(userId,dateMin,dateMax,callback){

    meetingsObj.newInvitationsByDateTimezone(userId,new Date(dateMin),new Date(dateMax),function(meetings){
        if(common.checkRequired(meetings) && meetings.length > 0){
            callback(meetings)
        }else callback([])
    })
}

//TODO:The below methods are currently not used by mobile. They may not return correct results. Have to be refactored. -- Rajiv 3005
function getTopFiveInteractionsCount(userId,user,meetingCount,timezone,req,res){

    var date = moment().tz(timezone);
    var start = date.clone();
    var end = date.clone();

    start.date(start.date() - 7);

    var data = {
         todayMeetingsCount:meetingCount,
        //currently only using to get meetings count. Removed extra code for better performance
        interactionsCountTopFive : 0,
        newContactsCount : 0,
        losingTouchCount : 0,
        notificationsCount : 0
    }

    sendSuccessResponse(req,res,data)
    /*interactionObj.interactionIndividualTopFive_new(userId,user.emailId,start.format(),end.format(),function(interactions){
        if(common.checkRequired(interactions) && interactions.length > 0){

                var count = 0;
                var length = interactions.length > 5 ? 5 : interactions.length;
                for(var c=0; c<length; c++){
                    count += interactions[c].count || 0
                }

                data.interactionsCountTopFive = count;

            getNewContactsCount(userId,user,data,timezone,req,res)
        }
        else{
            data.interactionsCountTopFive = 0;

            getNewContactsCount(userId,user,data,timezone,req,res)
        }
    })*/
}

function getNewContactsCount(userId,user,data,timezone,req,res){

    var date = moment().tz(timezone);
    date.date(date.date() - 7);
    date = date.format();

    contactObj.getNewContactsAdded(user._id,date,function(err,contacts){
        if(common.checkRequired(contacts) && contacts.length > 0){
            data.newContactsCount = contacts[0].count || 0;
        }
        else data.newContactsCount = 0;

        getLoosingTouchCount(userId,user,data,timezone,req,res)
    })
}

function getLoosingTouchCount(userId,user,data,timezone,req,res){

    var dateMin = moment().tz(timezone);
    dateMin.date(dateMin.date() - 15)
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    contactObj.getAllFavoriteContactsEmailIds(user._id,user.emailId,user.mobileNumber,function(emailIdArr,mobileNumberArr){
        if(emailIdArr.length > 0 ){
            interactionObj.losingTouchByDateORDays(userId,emailIdArr,dateMin,mobileNumberArr,function(losingTouch){
                if(common.checkRequired(losingTouch) && losingTouch.length > 0){
                    losingTouch = common.removeDuplicates_emailId(losingTouch);
                    data.losingTouchCount = losingTouch.length;
                    getNotificationCount(userId,user,data,timezone,req,res)
                }
                else{
                    data.losingTouchCount = 0;
                    getNotificationCount(userId,user,data,timezone,req,res)
                }
            })
        }
        else{
            data.losingTouchCount = 0;
            getNotificationCount(userId,user,data,timezone,req,res)
        }
    });
}

function getNotificationCount(userId,user,data,timezone,req,res){
    var givenDate = moment().tz(timezone);
    var dateMin = givenDate.clone();     //min date
    var dateMax = givenDate.clone();     //max date

    //dateMin.date(dateMin.date()-1)
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    //dateMax.date(dateMax.date()+1)
    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(59)
    dateMax.millisecond(59)
    dateMin = dateMin.format()
    dateMax = dateMax.format()

    notificationSupportClassObj.getNotificationsCount(userId, dateMin, timezone,function(count){
        data.notificationsCount = count;
        sendSuccessResponse(req,res,data)
    });
}

function sendSuccessResponse(req,res,data){
    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data":data
    });
}

module.exports = router;