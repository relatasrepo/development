var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var notificationSupportClass = require('../../common/notificationSupportClass');
var mobileAuth = require('../../common/mobileAuthValidation');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var meetingManagement = require('../../dataAccess/meetingManagement');
var eventDataAccess = require('../../dataAccess/eventDataAccess');
var calendarPasswordManagement = require('../../dataAccess/calendarPasswordManagement');

var notificationSupportClassObj = new notificationSupportClass();
var errorObj = new errorClass();
var mobileAuthObj = new mobileAuth();
var common = new commonUtility();
var validation = new validations();
var userManagementObj = new userManagement();
var errorMessagesObj = new errorMessages();

var statusCodes = errorMessagesObj.getStatusCodes();

var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';

router.get('/notifications/get', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        userManagementObj.findUserProfileByIdWithCustomFields(userId, {
            emailId: 1,
            timezone: 1
        }, function (error, user) {
            if (common.checkRequired(user)) {
                var timezone;
                if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                    timezone = user.timezone.name;
                } else timezone = 'UTC';
                var givenDate = moment().tz(timezone);
                var dateMin = givenDate.clone();     //min date
                var dateMax = givenDate.clone();     //max date

                //dateMin.date(dateMin.date()-1)
                dateMin.hour(0)
                dateMin.minute(0)
                dateMin.second(0)
                dateMin.millisecond(0)

                //dateMax.date(dateMax.date()+1)
                dateMax.hour(23)
                dateMax.minute(59)
                dateMax.second(59)
                dateMax.millisecond(59)
                dateMin = dateMin.format()
                dateMax = dateMax.format()
                if (req.query.returndata == 'count') {
                    notificationSupportClassObj.getNotificationsCount(userId, dateMin, timezone,function(count){
                        sendResponseCount(req, res, count)
                    });
                }
                else {
                    notificationSupportClassObj.getNotificationsList(userId, dateMin, timezone, function(list){
                        sendResponseList(req, res, list)
                    })
                }
            } else res.send(errorObj.generateErrorResponse({
                status: statusCodes.PROFILE_ERROR_CODE,
                key: 'ERROR_FETCHING_PROFILE'
            }));
        })
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

function sendResponseCount(req, res, count) {
    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data": {
            count: count
        }
    });
}

function sendResponseList(req, res, list) {
    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data": {
            total: list.length,
            notifications: list
        }
    });
}

module.exports = router;