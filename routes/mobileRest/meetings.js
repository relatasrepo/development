var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var AWS = require('aws-sdk');

var commonUtility = require('../../common/commonUtility');
var meetingSupportClass = require('../../common/meetingSupportClass');
var mobileAuth = require('../../common/mobileAuthValidation');
var appCredentials = require('../../config/relatasConfiguration');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var interactions = require('../../dataAccess/interactionManagement');
var meetingClass = require('../../dataAccess/meetingManagement');
var winstonLog = require('../../common/winstonLog');
var googleCalendarAPI = require('../../common/googleCalendar');

var errorObj = new errorClass();
var emailSenders = new emailSender();
var mobileAuthObj = new mobileAuth();
var appCredential = new appCredentials();
var common = new commonUtility();
var interactionObj = new interactions();
var validation = new validations();
var userManagementObj = new userManagement();
var errorMessagesObj = new errorMessages();
var meetingsObj = new meetingClass();
var meetingSupportClassObj = new meetingSupportClass();
var googleCalendar = new googleCalendarAPI();
var logLib = new winstonLog();

var logger = logLib.getWinston();
var statusCodes = errorMessagesObj.getStatusCodes();
var domain = appCredential.getDomain();
var domainName = domain.domainName;
var awsData = appCredential.getAwsCredentials();

AWS.config.update(awsData);
AWS.config.region = awsData.region;

var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';

router.post('/meetings/action/accept/mobile', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            
            var message = req.body.message?req.body.message:"";
            
            userManagementObj.findUserProfileByIdWithCustomFields(userId, {
                google: 1,emailId:1,firstName:1,lastName:1,timezone:1,serviceLogin:1
            }, function (error, user) {
                if (common.checkRequired(user)) {
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }
                    else if(common.checkRequired(req.body.timezone)){
                        timezone = req.body.timezone
                    }
                    else timezone = 'UTC';

                    if(common.checkRequired(req.body.invitationId) && common.checkRequired(req.body.slotId)){
                        meetingSupportClassObj.validateUserAcceptance(req.body.invitationId,userId,req.body.slotId,req.body.message,function(isValid,error,obj){
                            if(isValid){
                                if(common.checkRequired(obj)){
                                    obj.timezone = timezone;
                                    getUserPrimaryGoogleEmailId(obj.senderId,function(emailId){
                                        if(common.checkRequired(emailId)){
                                            obj.senderPrimaryEmail = emailId;
                                        }
                                        //meetingSupportClassObj.confirmMeeting_new(obj,userId,user.serviceLogin,function(isSuccess,error){
                                        meetingSupportClassObj.confirmMeeting(obj,userId,message,function(isSuccess,error){

                                            if(isSuccess){
                                                meetingSupportClassObj.updateMeetingInvitation(obj,userId,function(result,err){
                                                    if(result){
                                                        uploadICSFileToAWS(result,obj,userId,req,res);
                                                    }else {
                                                        res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: err}))
                                                    }
                                                })
                                            }
                                            else{
                                                if(error == 'ERROR_FETCHING_PROFILE'){
                                                    res.send(errorObj.generateErrorResponse({status: statusCodes.PROFILE_ERROR_CODE, key: error}))
                                                }else {
                                                    res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: error}))
                                                }
                                            }
                                        })
                                    });
                                }else {
                                    res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'ACCEPT_MEETING_FAILED'}))
                                }
                            } else{
                                if(error == 'UN_AUTHORISED_ACCESS'){
                                    res.send(errorObj.generateErrorResponse({status: statusCodes.AUTHORIZATION_ERROR_CODE, key: error}))
                                }else  {
                                    res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: error}))
                                }
                            }
                        })
                    } else {
                        res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'SLOT_ID_OR_INVITATION_ID_MISSED'}))
                    }

                } else {
                    res.send(errorObj.generateErrorResponse({
                        status: statusCodes.PROFILE_ERROR_CODE,
                        key: 'ERROR_FETCHING_PROFILE'
                    }));
                }
            })
        } else {
            res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}))
        }
    } else {
        res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
    }
});

router.get('/meetings/get/byId/mobile',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.invitationId)){
            userManagementObj.findInvitationById(req.query.invitationId,function(error,invitation,message){
                if(invitation){

                    if(invitation.senderId == userId){
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {
                                meeting:invitation
                            }
                        });
                    }else{

                        var exist = false;
                        var canceled = false;
                        for(var i=0; i<invitation.toList.length; i++){
                            if(invitation.toList[i].receiverId == userId){
                                if(invitation.toList[i].canceled){
                                    canceled = true;
                                }else
                                    exist = true;
                            }
                        }

                        if(invitation.to){
                            if(invitation.to.receiverId == userId){
                                if(invitation.to.canceled){
                                    canceled = true;
                                }else
                                    exist = true;
                            }
                        }
                        if(canceled){
                            res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'MEETING_CANCELLED'}));
                        }else
                        if(exist){
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data": {
                                    meeting:invitation
                                }
                            });
                        }else{
                            userManagementObj.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(profile){

                                if(profile){
                                    userManagementObj.findInvitationWithEmail(invitation.invitationId,profile.emailId,function(error,invi){
                                        if(invi){
                                            var obj = {
                                                receiverId:profile._id,
                                                receiverEmailId:profile.emailId,
                                                receiverFirstName:profile.firstName,
                                                receiverLastName:profile.lastName,
                                                isAccepted:false
                                            }

                                            updateInviToList(req,res,obj,invi);
                                        }else res.send(errorObj.generateErrorResponse({status: statusCodes.AUTHORIZATION_ERROR_CODE, key: 'UN_AUTHORISED_ACCESS'}));
                                    })
                                }else res.send(errorObj.generateErrorResponse({status: statusCodes.AUTHORIZATION_ERROR_CODE, key: 'UN_AUTHORISED_ACCESS'}));
                            })
                        }
                    }
                }
                else
                    res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'FAILED_TO_FETCH_MEETING_DETAILS'}))
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

/* invitationId & message */
router.post('/meetings/action/cancel/mobile',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)){
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if(common.checkRequired(req.body.invitationId)){
                userManagementObj.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,user){
                    if(common.checkRequired(user)){

                        var timezone;
                        if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                            timezone = user.timezone.name;
                        }else timezone = 'UTC';

                        meetingSupportClassObj.cancelMeeting(req.body.invitationId,userId,req.body.message,timezone,user,function(isSuccess,error){
                            if(isSuccess){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":{}
                                });
                            }
                            else{
                                res.send({
                                    "SuccessCode": 0,
                                    "Message": error,
                                    "ErrorCode": 1,
                                    "Data":{}
                                });
                            }
                        })

                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                })
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVITATION_ID_NOT_FOUND'}));

        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));

    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

/* invitationId, message(comment), new times and location details */
router.post('/meetings/action/edit/mobile',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)) {
        if (common.checkRequired(req.body) && validation.isEmptyObject(req.body)) {
            var status = validation.validateMeetingNewTimeLocation(req.body);
            if(status.status == 5000){
                res.send(errorObj.generateErrorResponse(status));
            }
            else {

                var invitationDetails = req.body;

                userManagementObj.findInvitationById(invitationDetails.invitationId,function(error,invi,msg){
                    if(common.checkRequired(invi)){
                        var accepted = false;
                        var multipleSuggest = false;
                        var lastLocation;
                        var lastUser = invi.lastActionUserId || userId;
                        var count = invi.updateCount || 0;
                        count = count > 0 ? count-1 : count;
                        var gEventId = invi.googleEventId;
                        gEventId = gEventId ? gEventId : 'relat'+getNumString(count)+invi.invitationId;
                        var maxSlot;
                        for(var i=0; i<invi.scheduleTimeSlots.length; i++){
                            if(invi.scheduleTimeSlots[i].isAccepted){
                                accepted = true;
                                maxSlot = invi.scheduleTimeSlots[i]
                            }
                        }

                        if(!common.checkRequired(maxSlot)){
                            maxSlot = invi.scheduleTimeSlots.reduce(function (a, b) {
                                return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                            });
                        }

                        var newInvitationDetails = {
                            comment:req.body.comment,
                            invitationId:invi.invitationId,
                            lastLocation:maxSlot.location,
                            scheduleTimeSlots:[
                                {
                                    start:{
                                        date:new Date(invitationDetails.newSlotDetails.startDateTime)
                                    },
                                    end:{
                                        date:new Date(invitationDetails.newSlotDetails.endDateTime)
                                    },
                                    title:maxSlot.title,
                                    location:maxSlot.location,
                                    suggestedLocation:invitationDetails.newSlotDetails.suggestedLocation,
                                    locationType:common.checkRequired(invitationDetails.newSlotDetails.locationType) ? invitationDetails.newSlotDetails.locationType : maxSlot.locationType,
                                    description:maxSlot.description
                                }
                            ]
                        }

                        if(invi.selfCalendar){
                            if(invi.toList.length > 1){
                                multipleSuggest = true;
                            }
                            /*updateReceiverContacts(invi.senderId,invi.toList[0].receiverId);
                             updateReceiverContacts(invi.toList[0].receiverId,invi.senderId);*/
                        }
                        else{
                            /*updateReceiverContacts(invi.senderId,invi.to.receiverId);
                             updateReceiverContacts(invi.to.receiverId,invi.senderId);*/
                        }

                        updateInvitationToListFalse(invi);
                        if(accepted){
                            if(common.checkRequired(invi.icsFile) && common.checkRequired(invi.icsFile.awsKey)){
                                meetingSupportClassObj.deleteICSFile(invi.icsFile.awsKey)
                            }

                            if(invi.selfCalendar){
                                googleCalendar.removeEventFromGoogleCalendarForMeeting(gEventId,lastUser,function(flag){
                                    updateInvitation(newInvitationDetails,userId,multipleSuggest,maxSlot,req,res);
                                });
                            }
                            else{
                                googleCalendar.removeEventFromGoogleCalendarForMeeting(gEventId,lastUser,function(flag){
                                    updateInvitation(newInvitationDetails,userId,multipleSuggest,maxSlot,req,res);
                                });
                            }
                        }
                        else{
                            updateInvitation(newInvitationDetails,userId,multipleSuggest,maxSlot,req,res);
                        }
                    }
                    else{
                        res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'FAILED_TO_FETCH_MEETING_DETAILS'}));
                    }
                })
            }
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

/* invitationId & late message */
router.post('/meetings/action/message/late/mobile',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)){
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if(common.checkRequired(req.body.invitationId) && common.checkRequired(req.body.message)){
                userManagementObj.findUserProfileByIdWithCustomFields(userId,{firstName:1,lastName:1,emailId:1,timezone:1,companyName:1,designation:1,location:1},function(error,user){
                    if(common.checkRequired(user)){

                        var timezone;
                        if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                            timezone = user.timezone.name;
                        }else timezone = 'UTC';
                        userManagementObj.findInvitationById(req.body.invitationId,function(error,meeting,msg){
                            if(common.checkRequired(meeting)){
                                if(meetingSupportClassObj.validateUserAuthorizationForMeeting(meeting,userId)){

                                    var maxSlot;
                                    for(var s=0; s<meeting.scheduleTimeSlots.length; s++){
                                        if(meeting.scheduleTimeSlots[s].isAccepted){
                                            maxSlot = meeting.scheduleTimeSlots[s];
                                        }
                                    }

                                    if(!common.checkRequired(maxSlot)){
                                        maxSlot = meeting.scheduleTimeSlots.reduce(function (a, b) {
                                            return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                                        });
                                    }

                                    if(meeting.selfCalendar){
                                        for(var l=0; l<meeting.toList.length; l++){
                                            if(common.checkRequired(meeting.toList[l].receiverId) && meeting.toList[l].receiverId != userId){
                                                sendLateMessageToParticipant(meeting.toList[l].receiverId,maxSlot,user,req.body.message);
                                            }
                                        }
                                    }
                                    else{
                                        if(common.checkRequired(meeting.to.receiverId) && meeting.to.receiverId != userId){
                                            sendLateMessageToParticipant(meeting.to.receiverId,maxSlot,user,req.body.message);
                                        }
                                    }

                                    if(meeting.senderId != userId){
                                        sendLateMessageToParticipant(meeting.senderId,maxSlot,user,req.body.message);
                                    }

                                    res.send({
                                        "SuccessCode": 1,
                                        "Message": "",
                                        "ErrorCode": 0,
                                        "Data":{}
                                    });
                                }else res.send(errorObj.generateErrorResponse({status:statusCodes.AUTHORIZATION_ERROR_CODE,key:'UN_AUTHORISED_ACCESS'}));
                            }else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'FAILED_TO_FETCH_MEETING_DETAILS'}));
                        })

                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                })
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_DATA_RECEIVED'}));

        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));

    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

function getNumString(num){
    return num < 10 ? '0'+num : ''+num
}

function updateInvitationToListFalse(invitation){
    if(invitation.selfCalendar){
        var toList = invitation.toList;
        var userId = invitation.toList[0].receiverId;
        for(var i=0; i<toList.length; i++){
            toList[i].isAccepted = false;
            toList[i].canceled = false;
        }
        userManagementObj.updateInvitationToListNotAccepted(invitation.invitationId,toList,function(error,isUpdate,message){
            logger.info(message.message);
        })
    }
    else{
        meetingsObj.updateMeetingToNotAccepted(invitation.invitationId,function(success){

        })
    }
}

function updateInvitation(invitationDetails,userId,multipleSuggest,maxSlot,req,res){
    var date = new Date();

    var invitationObj = {
        readStatus:false,
        suggested:true,
        scheduleTimeSlots:invitationDetails.scheduleTimeSlots,
        lastModified:date,
        suggestedBy:{
            userId:userId,
            suggestedDate:date
        }
    }

    if(!common.checkRequired(invitationObj.scheduleTimeSlots[0].suggestedLocation)){
        invitationObj.scheduleTimeSlots[0].location = invitationDetails.lastLocation;
        invitationObj.scheduleTimeSlots[0].suggestedLocation = invitationDetails.lastLocation;

    }
    else{
        invitationObj.scheduleTimeSlots[0].location = invitationObj.scheduleTimeSlots[0].suggestedLocation;
    }

    var comment = {
        messageDate: new Date(),
        message: invitationDetails.comment,
        userId: userId
    };

    meetingsObj.updateInvitationSuggest(invitationDetails.invitationId,invitationObj,comment,function(error,isUpdated,message){
        if (isUpdated) {
            logger.info(message.message);
            interactionObj.removeInteractions(invitationDetails.invitationId);

            var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
            var mUrl = protocol+req.headers.host+'/meeting/'+invitationDetails.invitationId;
            meetingSupportClassObj.sendSuggestMeetingEmail(invitationDetails.invitationId,maxSlot,invitationDetails.comment);
            //sendSuggestInvitationMailToSuggestedUser(invitationDetails.invitationId,userId,maxSlot,mUrl,invitationDetails.comment);
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{}
            });
        }
        else{
            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_MEETING_FAILED'}));
        }
    });
}

function updateInviToList(req,res,obj,invi){
    userManagementObj.updateInvitationToList(invi.invitationId,obj,function(error,isUpdated,message){
        logger.info(message);
        res.send(invi);
    })
}

function getUserPrimaryGoogleEmailId(userId,callback){
    if(common.checkRequired(userId)){

        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,officeCalendar:1,google:1,serviceLogin:1},function(error,user){
            if(common.checkRequired(user)){
                if(user.serviceLogin == 'office' && common.checkRequired(user.officeCalendar)){
                    callback(user.officeCalendar.emailId);
                }
                else if(googleCalendar.validateUserGoogleAccount(user)){
                    callback(user.google[0].emailId);
                }
                else callback(null);
            }
        })

    }
    else callback(null)
}

function uploadICSFileToAWS(icsFile,obj,userId,req,res){

    if(common.checkRequired(obj.icsFile) && common.checkRequired(obj.icsFile.awsKey)){

        obj.file = icsFile;
        meetingSupportClassObj.sendAcceptInvitationEmail(obj,userId);

        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data":{}
        });
    }
    else{

        obj.file = icsFile;
        var s3bucket = new AWS.S3({ params: {Bucket: awsData.icsBucket} });
        var url = 'https://' + awsData.icsBucket + '.s3.amazonaws.com/';
        var key = obj.invitationId+'.ics';
        var params = {Key: key, ContentType: 'text/calendar', Body: icsFile};

        s3bucket.putObject(params, function(err, data) {
            if (err) {

                logger.info('Uploading ICS file to AWS failed '+err)
                meetingSupportClassObj.sendAcceptInvitationEmail(obj,userId);
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{}
                });
            } else {

                obj.icsAwsKey = key;
                obj.icsUrl = url+key;
                meetingSupportClassObj.updateMeetingWithIcs(obj,function(isSuccess,error){

                     if(error){
                         logger.info('Updating meeting with ICS file failed '+error);
                     }
                    meetingSupportClassObj.sendAcceptInvitationEmail(obj,userId);
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":{}
                    });
                })
            }
        });
    }
}

function sendSuggestInvitationMailToSuggestedUser(invitationId,userId,maxSlotOld,mUrl,message){
    userManagementObj.findInvitationById(invitationId,function(error,meeting,msg){
        if(common.checkRequired(meeting)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId, {
                emailId: 1,
                timezone: 1,
                firstName:1,
                lastName:1
            }, function (error, user) {
                if (common.checkRequired(user)) {

                    var timezone;
                    if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                        timezone = user.timezone.name;
                    } else timezone = 'UTC';

                    var dataToSend_sender = {};
                    var senderName = user.firstName+' '+user.lastName;

                    dataToSend_sender.emailId = user.emailId;
                    dataToSend_sender.toName = senderName;

                    //dataToSend_sender.subject =  "Relatas : Your revised suggested times to "++" has been sent"
                    var maxSlotNew = meeting.scheduleTimeSlots.reduce(function (a, b) {
                        return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                    });
                    var participants = "";
                    var multiple = false;
                    if(meeting.senderId == userId){
                        if(meeting.selfCalendar){
                            if(meeting.toList.length > 1){
                                multiple = true;
                                for(var p=0; p<meeting.toList.length; p++){
                                    var fName =  meeting.toList[p].receiverFirstName || '';
                                    var lName =  meeting.toList[p].receiverLastName || '';
                                    participants += fName+" "+lName;
                                    if(meeting.toList.length-1 != p){
                                        participants += ","
                                    }
                                }
                                dataToSend_sender.subject =" Relatas : Your revised suggested times has been sent";
                            }
                            else{
                                var fName2 =  meeting.toList[0].receiverFirstName || '';
                                var lName2 =  meeting.toList[0].receiverLastName || '';
                                participants += fName2+" "+lName2;
                                dataToSend_sender.subject =" Relatas : Your revised suggested times to "+participants+" has been sent";
                            }
                        }
                        else{
                            participants = meeting.to.receiverName;
                            dataToSend_sender.subject =" Relatas : Your revised suggested times to "+participants+" has been sent";
                        }
                    }
                    else{
                        participants = meeting.senderName;
                        dataToSend_sender.subject =" Relatas : Your revised suggested times to "+participants+" has been sent";
                    }

                    var startOld = moment(maxSlotOld.start.date).tz(timezone || 'UTC');
                    var dateOld = startOld.format("DD MMM YYYY");
                    var timeOld = startOld.format("h:mm a z");
                    var locationOld = maxSlotOld.locationType+" : "+maxSlotOld.location;

                    var startNew = moment(maxSlotNew.start.date).tz(timezone || 'UTC');
                    var dateNew = startNew.format("DD MMM YYYY");
                    var timeNew = startNew.format("h:mm a z");
                    var locationNew = maxSlotNew.locationType+" : "+maxSlotNew.location;

                    var names = multiple ? 'they' : participants;

                    dataToSend_sender.mailContent = "<span style='color:rgb(89,89,89);'>Your suggested time changes to <b>"+participants+"</b> has been sent successfully. We will let you know when "+names+" accepts this meeting. You may check your current past meeting <a href="+mUrl+">here</a> to accept/ reschedule this meeting.</span><br>Old time: "+dateOld+"  |  "+timeOld+"  |  "+locationOld+"<br><b>New time Location</b>: "+dateNew+"  |  "+timeNew+"  |  "+locationNew;

                    if(common.checkRequired(message)){
                        dataToSend_sender.mailContent += "<span style='color:rgb(89,89,89);'><br><b>Message from "+senderName+"</b> <br>"+message+"</span>";
                    }
                    emailSenders.sendMeetingCancelOrDeletionMail(dataToSend_sender,"New suggested meeting time");
                }
            });
        }
    })
}

function sendLateMessageToParticipant(userId,maxSlot,senderPr,message){

    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,firstName:1,lastName:1},function(error,userDetails) {
        if (common.checkRequired(userDetails)) {
            var dataToSend_sender = {}
            var timezone;
            if(common.checkRequired(userDetails.timezone) && common.checkRequired(userDetails.timezone.name)){
                timezone = userDetails.timezone.name;
            }else if(common.checkRequired(senderPr.timezone) && common.checkRequired(senderPr.timezone.name)){
                timezone = senderPr.timezone.name;
            }else timezone = 'UTC';

            var dataToSend = {};
            dataToSend.emailId = userDetails.emailId;
            dataToSend.subject = ' Relatas Meeting Late Message';
            dataToSend.senderName = senderPr.firstName+' '+senderPr.lastName;

            var cName = senderPr.companyName || '';
            var des = senderPr.designation || '';
            var comma = (common.checkRequired(cName) && common.checkRequired(des)) ? ', ' : ' ';
            dataToSend.senderInfo = des+comma+cName;
            dataToSend.senderLocation = senderPr.location;
            dataToSend.receiverName = userDetails.firstName+' '+userDetails.lastName;
            dataToSend.meetingTitle = maxSlot.title;

            var start = moment(maxSlot.start.date).tz(timezone || 'UTC');
            var date = start.format("DD-MMM-YYYY");
            var time = start.format("h:mm a z");
            dataToSend.mDate = date;
            dataToSend.mTime = time;
            dataToSend.mLocation = maxSlot.locationType+' : '+maxSlot.location;
            dataToSend.lateMessage = common.checkRequired(message) ? message : 'No message';
            dataToSend.senderPic = domainName+'/getImage/'+senderPr._id;

            emailSenders.sendMeetingLateMessage(dataToSend);
        }
    })
}

module.exports = router;