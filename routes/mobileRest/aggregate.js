var express = require('express');
var router = express.Router();
var request = require('request');
var fs = require('fs');
var jwt = require('jwt-simple');
var gmailApi = require('node-gmail-api');
var _ = require("lodash");
var winstonLog = require('../../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getWinston();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var aggregateSupport = require('../../common/aggregateSupport');
var mobileAuth = require('../../common/mobileAuthValidation');
var appCredentials = require('../../config/relatasConfiguration');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactionManagement = require('../../dataAccess/interactionManagement')
var meetingManagement = require('../../dataAccess/meetingManagement');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var googleCal = require('../../common/googleCalendar')
var async = require('async');
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var rabbitmq = require('../../common/rabbitmq');

var googleCalObj = new googleCal();
var errorObj = new errorClass();
var mobileAuthObj = new mobileAuth();
var appCredential = new appCredentials();
var common = new commonUtility();
var aggregateSupportObj = new aggregateSupport();
var validation = new validations();
var userManagementObj = new userManagement();
var emailSenderObj = new emailSender();
var errorMessagesObj = new errorMessages();
var meetingClassObj = new meetingManagement();
var interactionObj = new interactionManagement();
var rabbitmqObj = new rabbitmq();

var authConfig = appCredential.getAuthCredentials();
var google = require('node-google-api')(authConfig.GOOGLE_CLIENT_ID);

var statusCodes = errorMessagesObj.getStatusCodes();

var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';

router.post('/aggregate/interactions/add',mobileAuthObj.isAuthenticatedMobile,function(req,res){

    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
// router.get('/aggregate/interactions/add',common.isLoggedInUserOrMobile,function(req,res){
    // var userId = common.getUserIdFromMobileOrWeb(req)

    // // req.body = {
    // //     data:[{mobileNumber:"007",name:"Naveen paul APIs",emailId:"Naveenpaul.markunda@gmail.com"},
    // //         {mobileNumber:"007",name:"Naveen paul APIs",emailId:"naveenPaul.markunda@gmail.com"},
    // //         {mobileNumber:"007",name:"Naveen R APIs",emailId:"naveenpaul@relatas.com"},
    // //         {mobileNumber:"007",name:"Jimmy Test APIs",emailId:'jimmytestacc@gmail.com'}],
    // //     historyType:"contacts"
    // // }
    //
    //
    // console.log(req.body)

    if(common.checkRequired(userId)){
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,mobileNumber:1,mobileNumberWithoutCC:1,countryCode:1,designation:1,companyName:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,location:1},function(error,user){
                if(common.checkRequired(user)){

                    if(common.checkRequired(req.body.timezone) && common.checkRequired(req.body.timezone.name) && common.checkRequired(req.body.timezone.zone)){
                        userManagementObj.updateTimezone(user._id,req.body.timezone,function(error,isSuccess,msg){
                            if(error || !isSuccess){
                                emailSenderObj.sendUncaughtException('TIMEZONE failed to update',req.body.data.length,JSON.stringify(req.body.timezone),'TIMEZONE '+user.emailId)
                            }
                        })
                    }

                    switch(req.body.historyType){
                        case 'call':aggregateSupportObj.parseAndStoreMobileHistory(req.body.data,user,'call',function(status){
                                        sendDefaultStatusMessage(req,res,status)
                                    });
                            break;
                        case 'sms':aggregateSupportObj.parseAndStoreMobileHistory(req.body.data,user,'sms',function(status){
                            sendDefaultStatusMessage(req,res,status)
                                    });
                            break;
                        //commented parts are for mobile, and currently do not integrate to relatas properly. -- Rajiv 04032016
                        case 'email':
                            //aggregateSupportObj.parseAndStoreMobileHistory(req.body.data,user,'email',function(status){

                            sendDefaultStatusMessage(req,res,null)
                                    //});
                            break;
                        case 'calendar':
                            //aggregateSupportObj.parseAndStoreMobileHistory(req.body.data,user,'calendar',function(status){

                            sendDefaultStatusMessage(req,res,null)
                                        //});
                            break;
                        case 'contacts':
                            aggregateSupportObj.parseAndStoreMobileHistory(req.body.data,user,'contacts',function(status){

                            sendDefaultStatusMessage(req,res,status)
                        });
                            break;
                        default : res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_HISTORY_TYPE'}));
                    }
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

function sendDefaultStatusMessage(req,res,status){
    if(status){
        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data":{}
        });
    }
    else{
        res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_HISTORY_FOUND'}));
    }
}

//Adding Mobile Data sync. API in new file doesn't work.
router.get('/sync/data/now',common.isLoggedInUserOrMobile,function(req,res){
// router.get('/sync/data/now',mobileAuthObj.isAuthenticatedMobile,function(req,res){
//     userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    var userId = common.getUserIdFromMobileOrWeb(req);
    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,serviceLogin:1,google:1,outlook:1},function (err,userProfile) {

        if (!err && userProfile) {
            if(userProfile.google.length>0 && userProfile.outlook.length === 0){
                //Google user
                if(common.checkRequired(userId)){
                    if(common.checkRequired(req.query.isOnBoarding)){
                        var isOnBoarding = req.query.isOnBoarding
                        mBackgroundMeetingAndEmailSync(userId,isOnBoarding,res)
                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:"MISSING_QUERY_PARAMETER"}))
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST,key:'MISSING_TOKEN_HEADER'}))

            } else {

                //Outlook user
                // This API is in /routes/office365.js
                res.redirect('/sync/outlook/data?isOnBoarding='+req.query.isOnBoarding);
            }

        } else {
            res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:"MISSING_QUERY_PARAMETER"}))
        }
    });

});

function mBackgroundMeetingAndEmailSync(userId,isOnBoarding,res){
    var currentDate = new Date();
    findUserProfile(userId,function(err,data){
        if(data){
            var lastLoginDate = data.lastLoginDate;
            var lastMobileSyncDate = data.lastMobileSyncDate
            var toSyncFromDate
            var googleArray = data.google;

            var lastLogin;
            var lastSync;


/*
            if (lastMobileSyncDate != null){
                var lastLogin = new Date(lastLoginDate);
                var lastSync = new Date(lastMobileSyncDate);


                if(lastLogin>=lastSync) {
                    toSyncFromDate = lastLogin
                }else {
                    toSyncFromDate = lastSync
                }
            }else {
                toSyncFromDate = new Date(lastLoginDate)
            }*/
            if(isOnBoarding){
                if(lastLoginDate == null) {
                    toSyncFromDate = null
                }else{
                    if(lastMobileSyncDate!=null){
                        lastLogin = new Date(lastLoginDate)
                        lastSync = new Date(lastMobileSyncDate)
                        if(lastLogin>=lastSync) {
                            toSyncFromDate = lastLogin
                        }else {
                            toSyncFromDate = lastSync
                        }
                    }else{
                        toSyncFromDate = new Date(lastLoginDate)
                    }
                }
            }else{
                if (lastMobileSyncDate != null){
                    lastLogin = new Date(lastLoginDate);
                    lastSync = new Date(lastMobileSyncDate);
                    if(lastLogin>=lastSync) {
                        toSyncFromDate = lastLogin
                    }else {
                        toSyncFromDate = lastSync
                    }
                }else {
                    toSyncFromDate = new Date(lastLoginDate)
                }
            }
            // console.log("SyncDate (can be null for onBoarding new Users, or it is a date) :",toSyncFromDate)

            async.each(googleArray,function(googleDataFromArray,callBack){
                getEmailsAndMeetingsWrapper(data,googleDataFromArray,toSyncFromDate,true,data.emailId,callBack)
            },function(updatedAll){

                var obj = {
                    'userId': String(userId),
                    'afterDate': moment(toSyncFromDate).format('YYYY-MM-DD'),
                    'beforeDate': moment().format('YYYY-MM-DD')
                };
                logger.info("Add interactions bulk EMAIL Success and send rabitmq message aggregate",data.emailId);

                rabbitmqObj.producer(obj,function(result){
                    if(result)
                        logger.info("Add OBJ to processQueue Success ",obj);
                    else
                        logger.info("Failed to Add OBJ to processQueue ",obj);
                });

                callbackDone(data,currentDate,isOnBoarding,updatedAll,res)
            })
        }else{
            res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'UPDATE_FAILED'}))
        }
    })
}

function findUserProfile(userId ,callBack){
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,lastLoginDate:1,lastMobileSyncDate:1,
        mobileOnBoardingDate:1,createdDate:1,google:1,firstName:1,lastName:1,companyName:1,designation:1
        ,mobileNumber:1,publicProfileUrl:1,profilePicUrl:1,skypeId:1,location:1,registeredDate:1},function(error,user){
            callBack(error,user)
    })
}

function getEmailsAndMeetingsWrapper (user, googlePackage, toSyncFromDate,office,officeEmail,callBack){
    var access_tokenOld = googlePackage.token
    var googleEmail = googlePackage.emailId

    var tokenProvider = new GoogleTokenProvider({
        refresh_token: googlePackage.refreshToken,
        client_id:     authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        access_token: access_tokenOld
    });
    tokenProvider.getToken(function (err, token) { // start of token provider
        if (err || !token) {
        }

        if (token != null) {
            getUserEmailList(user, token, toSyncFromDate, office, officeEmail, googleEmail, true, function () {
            })
            getCalendarMeetings(googleEmail, token, toSyncFromDate, office, officeEmail, googleEmail, callBack)
        }
    })
}

function getCalendarMeetings(calendarEmail,access_token,toSyncFromDate,office,primaryGoogleId,googleEmailId,callBack){

    var endDate = new Date();
    var timeMax = new Date();
    if(toSyncFromDate!=null) {
        endDate = new Date(toSyncFromDate);
    }
    endDate.setDate(endDate.getDate() - 2);
    timeMax.setDate(timeMax.getDate() + 15);
    var endMonth = endDate.getMonth()+1;
    var maxMonth = timeMax.getMonth()+1;


    google.build(function(api){
        api.calendar.events.list(  // calendar event list start
            {
                calendarId: 'primary',
                access_token:access_token, // You have to supply the OAuth token for the current user
                singleEvents:true,
                maxResults:2499,
                timeMin:endDate.getFullYear()+'-'+endMonth+'-'+endDate.getDate()+'T00:00:00.000Z',
                timeMax:timeMax.getFullYear()+'-'+maxMonth+'-'+timeMax.getDate()+'T00:00:00.000Z'
            },
            function(events) {
                if (events.items == undefined || events.items == null || events.items == '') {
                    //If there are no meetings scheduled then log in the user.
                    callBack(true);
                }
                else{
                    events = events.items;
                    googleMeetings_Relatas(calendarEmail,events,office,primaryGoogleId,googleEmailId,true,function(updatedAll){
                        callBack(updatedAll);
                    });
                }
            }
        )})
}

function googleMeetings_Relatas(calendarEmail,events,office,officeEmailId,googleEmailId,toMeeting,callback){
    if(common.checkRequired(events) && events.length > 0 && toMeeting){
        var meetingsArr = [];
        var emailIdArr = [];
        for(var i=0; i<events.length; i++){
            var obj = googleCalObj.generateMeetingWithGEvent_(events[i],googleEmailId,officeEmailId)
            if(obj.emailIdArr.length > 0 && obj.meeting != null){
                meetingsArr.push(obj.meeting);
                emailIdArr = emailIdArr.concat(obj.emailIdArr);
            }
        }

        if(meetingsArr.length > 0 && emailIdArr.length > 0){
            emailIdArr = common.removeDuplicate_id(emailIdArr,true);

            userManagementObj.selectUserProfilesCustomQuery({emailId:{$in:emailIdArr}},{firstName:1,lastName:1,emailId:1,profilePicUrl:1},function(error,users){
                if(!error && users.length > 0){
                    for(var user=0; user<users.length; user++){
                        for(var meeting=0; meeting<meetingsArr.length > 0; meeting++){
                            if(meetingsArr[meeting].senderEmailId == users[user].emailId){
                                meetingsArr[meeting].senderEmailId = users[user].emailId
                                meetingsArr[meeting].senderId = users[user]._id.toString()
                                meetingsArr[meeting].senderName = users[user].firstName+' '+users[user].lastName
                                meetingsArr[meeting].senderPicUrl = users[user].profilePicUrl
                                meetingsArr[meeting].mobileNumber = users[user].mobileNumber
                                meetingsArr[meeting].location = users[user].location
                                meetingsArr[meeting].publicProfileUrl = users[user].publicProfileUrl
                                meetingsArr[meeting].designation = users[user].designation
                            }
                            if(common.checkRequired(meetingsArr[meeting].toList) && meetingsArr[meeting].toList.length > 0){
                                for(var i=0; i<meetingsArr[meeting].toList.length; i++){
                                    if(meetingsArr[meeting].toList[i].receiverEmailId == users[user].emailId){
                                        meetingsArr[meeting].toList[i].receiverId = users[user]._id.toString()
                                        meetingsArr[meeting].toList[i].receiverFirstName = users[user].firstName
                                        meetingsArr[meeting].toList[i].receiverLastName = users[user].lastName
                                        meetingsArr[meeting].toList[i].receiverEmailId = users[user].emailId
                                        meetingsArr[meeting].toList[i].picUrl = users[user].profilePicUrl
                                        meetingsArr[meeting].toList[i].mobileNumber = users[user].mobileNumber
                                        meetingsArr[meeting].toList[i].location = users[user].location
                                        meetingsArr[meeting].toList[i].publicProfileUrl = users[user].publicProfileUrl
                                        meetingsArr[meeting].toList[i].designation = users[user].designation
                                    }
                                }
                            }
                        }
                    }
                }
                google_relatas_final(calendarEmail,users._id,meetingsArr,callback);
            })
        }
        else callback()
    }
    else callback();
}

/*function google_relatas_final(calendarEmail,userId,meetings,callback){
    calendarlist.findOne({email:calendarEmail,userId:userId}).exec(function(err,list) {
        if(list===null){
            var c=new calendarlist({'name':'Primary','userId':userId,'email':calendarEmail,'color':'red'});
            c.save(function(e) {
                if(!e){
                    logger.info('list sucessfully created'+userId);
                    for(var i=0; i<meetings.length; i++){
                        googleCalObj.google_relatas_execute_(calendarEmail,meetings[i]);
                    }
                    if(callback) callback()
                }
            });
        }else{
            for(var i=0; i<meetings.length; i++){
                googleCalObj.google_relatas_execute_(calendarEmail,meetings[i]);
            }
            if(callback) callback()
        }
    });
}*/
function google_relatas_final(calendarEmail,userId,meetings,callback) {

    //TODO update interactions collection and participants as contacts.
    // This function gets an array of refId that have to be added to the interactions collection.
    meetingClassObj.bulkMeetingsUpdate(calendarEmail, meetings, function (nonExistingMeetings) {
        if (nonExistingMeetings.length > 0) {
            var len = nonExistingMeetings.length;

            var meetingInteractions = [];
            for (var i = 0; i < len; i++) {
                meetingInteractions.push(googleCalObj.createMeetingsInteractionsObjects_(nonExistingMeetings[i], null, false));
            }

            var meetingsLen = meetingInteractions.length;
            var updatedAll = true //set as false if an update fails. Don't update Sync Date if false

            if (meetingsLen > 0) {
                for (var j = 0; j < meetingsLen; j++) {
                    var addInteractionsForUsers = _.compact(_.pluck(meetingInteractions[j], "userId"));
                    var kLen = addInteractionsForUsers.length;
                    for (var k = 0; k < kLen; k++) {
                        interactionObj.updateMeetingsAsInteractions(common.castToObjectId(addInteractionsForUsers[k]), meetingInteractions[j], function (result) {
                            if (result) {
                                logger.info("Meeting Interactions Successfully updated for - ", addInteractionsForUsers[k])
                            } else {
                                updatedAll = false;
                                logger.info("Meeting Interactions Not updated for - ", addInteractionsForUsers[k])
                            }
                        });
                    }
                }
            }
            if (callback) {
                callback(updatedAll)
            }
        }else{
            callback(true)
        }


    });
}

function callbackDone(user,lastSyncDate,isOnBoarding,hasUpdatedAll,res){
    var updateLastSync = true
    if(isOnBoarding){
        // updateMobileOnBoard Date  only for non relatas users onBoarding the first time..along with partial Profile Date ( created Date)
        if(user.createdDate)
        if(user.mobileOnBoardingDate == null){
            userManagementObj.updateMobileOnBoardDate(user._id,lastSyncDate,function(err,result){
                if(err){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'UPDATE_FAILED'}));
                    updateLastSync = false
                }else {
                    // console.log("MobileOnBoard date updated.")
                    if(user.createdDate==null || user.createdDate=="") {
                        userManagementObj.updatePartialProfileDate(user._id, lastSyncDate, function (err,result) {
                            if(err){
                                res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'UPDATE_FAILED'}));
                                updateLastSync = false
                            }else{
                                // console.log("Created Date updated")
                                if(user.registeredDate == null || user.registeredDate == ""){
                                    userManagementObj.updateRegisteredDateUser(user._id,lastSyncDate,function(err,result){
                                        if(err) {
                                            res.send(errorObj.generateErrorResponse({
                                                status: statusCodes.UPDATE_FAILED,
                                                key: 'UPDATE_FAILED'
                                            }));
                                            updateLastSync = false
                                        }else{
                                            // console.log("RegisteredDate updated")
                                        }
                                    })
                                }

                            }
                        })
                    }
                }
            })
        }
    }
    if(updateLastSync) {
        if(hasUpdatedAll){
        userManagementObj.updateMobileSyncDate(user._id, lastSyncDate, function (err, result) {
            if (err) {
                res.send(errorObj.generateErrorResponse({status: statusCodes.UPDATE_FAILED, key: 'UPDATE_FAILED'}))
            } else {
                res.send({
                    "SuccessCode": 1,
                    "Message": "ALL_INTERACTIONS_WERE_SYNCED",
                    "ErrorCode": 0,
                    "Data": {
                        "isOnBoarding":isOnBoarding,
                        "lastSyncDate":lastSyncDate
                    }
                });
            }
        })
    }else{
        res.send({
            "SuccessCode": 1,
            "Message": "NOT_ALL_INTERACTIONS_WERE_SYNCED",
            "ErrorCode": 0,
            "Data": {
                "isOnBoarding":isOnBoarding,
                "lastSyncDateAttempt":lastSyncDate
            }
        })
    }
    }
}

function getUserEmailList (user,access_token,lastLoginDate,office,officeEmail,googleEmail,isCallback,callback) {

//Add email interactions for the past 90 days for new users. For registered users by last login date
    var interactionsDirect = [];
    var interactionsOther = [];
    var refIdArr = [];
    var emailIdArr = [];

    var filter = '';
    var after = new Date();
    var before = new Date();
    var now = new Date();

        if(common.checkRequired(lastLoginDate)){
            after = new Date(lastLoginDate);
            before = new Date();
            after.setDate(after.getDate() - 1);
            before.setDate(before.getDate() + 1);
            var monthAfter2 = after.getMonth()+1;
            var monthBefore2 = before.getMonth()+1;
            filter = 'after:'+after.getFullYear()+'/'+monthAfter2+'/'+after.getDate()+' before:'+before.getFullYear()+'/'+monthBefore2+'/'+before.getDate();

        }
        else{
            after.setDate(after.getDate() - 90);
            before.setDate(before.getDate() + 2);
            var monthAfter3 = after.getMonth()+1;
            var monthBefore3 = before.getMonth()+1;
            filter = 'after:'+after.getFullYear()+'/'+monthAfter3+'/'+after.getDate()+' before:'+before.getFullYear()+'/'+monthBefore3+'/'+before.getDate();

        }

    if(before > after){
        var gmail = new gmailApi(access_token);

        var s = gmail.messages(filter);

        s.on('end',function(){

            googleCalObj.mapUserDataToInteractions_(user,interactionsDirect,interactionsOther,refIdArr,emailIdArr);

            if(isCallback){
                callback()
            }
        });

        s.on('error',function(a){
            // console.log("error",a)
            googleCalObj.mapUserDataToInteractions_(user,interactionsDirect,interactionsOther,refIdArr,emailIdArr);
            if(isCallback){
                callback()
            }
        });

        s.on('data', function (d) {
            if(common.checkRequired(d) && common.checkRequired(d.payload) && common.checkRequired(d.payload.headers)){
                if(d.payload.headers.length > 0){
                    var headers = d.payload.headers;
                    var email = parseEmailHeader(d.id,headers);
                    if(common.checkRequired(email) && common.checkRequired(email.id)){
                        var obj = generateAndStoreInteraction(user,email,office,officeEmail,googleEmail);
                        interactionsDirect = interactionsDirect.concat(obj.interactionDirect);
                        interactionsOther = interactionsOther.concat(obj.interactionOther);
                        refIdArr.push(obj.refId);
                        emailIdArr = emailIdArr.concat(obj.emailIdArr);
                    }
                }
            }
        })
    }
    else{
        if(isCallback){
            callback()
        }
    }
}

function parseEmailHeader(id,headers){

    var email = {emailContentId:id};
    for(var i=0; i<headers.length; i++){
        if(headers[i].name == 'From' || headers[i].name == 'from'){
            email.from = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'To' || headers[i].name == 'to'){
            email.To = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Subject' || headers[i].name == 'subject'){
            email.subject = headers[i].value;
        }
        if(headers[i].name == 'Cc' || headers[i].name == 'cc'){
            email.Cc = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Bcc' || headers[i].name == 'bcc'){
            email.Bcc = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Date' || headers[i].name == 'date'){
            email.date = new Date(headers[i].value);
        }
        if(headers[i].name == 'Message-Id' || headers[i].name == 'Message-ID'){
            email.id = headers[i].value;
        }
    }

    var isValid = true;
    if(email.to && email.to.length > 0){
        for(var to in email.to){
            isValid = !matchInArray(email.to[to].split('@')[0])
        }
    }
    if(email.from && email.from.length > 0){
        for(var from in email.from){
            isValid = !matchInArray(email.from[from].split('@')[0])
        }
    }
    if(email.Cc && email.Cc.length > 0){
        for(var Cc in email.Cc){
            isValid = !matchInArray(email.Cc[Cc].split('@')[0])
        }
    }
    if(email.Bcc && email.Bcc.length > 0){
        for(var Bcc in email.Bcc){
            isValid = !matchInArray(email.Bcc[Bcc].split('@')[0])
        }
    }

    return isValid ? email : null;
}

function matchInArray(stringSearch){
    var position = String(common.getInvalidEmailList()).search(stringSearch);
    return (position > -1);
}

function extractEmails (text)
{
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

function generateAndStoreInteraction(user,email,office,officeEmail,googleEmail){

    var interactionDirect = [];
    var interactionOther = [];
    var interactionRefId = [];
    var emailIdArr = [];
    var isSender = false;

    if(common.checkRequired(email.from)){
        for(var from=0; from<email.from.length; from++){
            var emailIdFrom = email.from[from] ? email.from[from] : '';
            if(office && googleEmail == emailIdFrom){
                emailIdFrom = officeEmail;
            }

            if(emailIdFrom == user.emailId){
                isSender = true;
                interactionDirect.push(common.getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'sender','email','email',user.emailId,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
            else{
                emailIdArr.push(emailIdFrom);
                interactionOther.push(common.getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'sender','email','email',emailIdFrom,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
        }
    }

    if(common.checkRequired(email.To)){
        for(var to=0; to<email.To.length; to++){
            var emailIdTo = email.To[to] ? email.To[to] : '';
            if(office && googleEmail == emailIdTo){
                emailIdTo = officeEmail;
            }

            if(emailIdTo == user.emailId){
                interactionDirect.push(common.getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','email','email',user.emailId,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
            else if(isSender){
                emailIdArr.push(emailIdTo);
                interactionOther.push(common.getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'receiver','email','email',emailIdTo,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
        }
    }

    if(common.checkRequired(email.Cc)){
        for(var cc=0; cc<email.Cc.length; cc++){
            var emailIdCc = email.Cc[cc] ? email.Cc[cc] : '';
            if(office && googleEmail == emailIdCc){
                emailIdCc = officeEmail;
            }

            if(emailIdCc == user.emailId){
                interactionDirect.push(common.getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','email','email',user.emailId,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
            else if(isSender){
                emailIdArr.push(emailIdCc);
                interactionOther.push(common.getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'receiver','email','email',emailIdCc,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
        }
    }
    if(common.checkRequired(email.Bcc)){
        for(var bcc=0; bcc<email.Bcc.length; bcc++){
            var emailIdBcc = email.Bcc[bcc] ? email.Bcc[bcc] : '';
            if(office && googleEmail == emailIdBcc){
                emailIdBcc = officeEmail;
            }

            if(emailIdBcc == user.emailId){
                interactionDirect.push(common.getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','email','email',user.emailId,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
            else if(isSender){
                emailIdArr.push(emailIdBcc);
                interactionOther.push(common.getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'receiver','email','email',emailIdBcc,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
        }
    }

    return {
        interactionDirect:interactionDirect,
        interactionOther:interactionOther,
        interactionRefId:interactionRefId,
        refId:email.id,
        emailIdArr:emailIdArr
    }
}

module.exports = router;