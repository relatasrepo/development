var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var taskManagementClass = require('../../dataAccess/taskManagementClass');
var mobileAuth = require('../../common/mobileAuthValidation');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var interactions = require('../../dataAccess/interactionManagement');
var calendarPasswordManagement = require('../../dataAccess/calendarPasswordManagement');
var googleCalendarAPI = require('../../common/googleCalendar');
var winstonLog = require('../../common/winstonLog');
var taskSupportClass = require('../../common/taskSupportClass');

var taskSupportClassObj = new taskSupportClass();
var taskManagementClassObj = new taskManagementClass();
var errorObj = new errorClass();
var interactionsObj = new interactions();
var mobileAuthObj = new mobileAuth();
var common = new commonUtility();
var validation = new validations();
var userManagementObj = new userManagement();
var googleCalendar = new googleCalendarAPI();
var errorMessagesObj = new errorMessages();

var statusCodes = errorMessagesObj.getStatusCodes();

var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';


router.post('/task/create/new',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)){
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            var status = validation.validateTask(req.body);
            if(status.status == 5000){
                res.send(errorObj.generateErrorResponse(status));
            }
            else{
                if(common.checkRequired(req.body.assignedToEmailId) && !validation.validateEmailFlag(req.body.assignedToEmailId)){
                    return res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_EMAIL_ID'}));
                }

                common.getProfileOrStoreProfileInSession(userId,req,function(user) {
                    if (common.checkRequired(user)) {
                        var task = req.body;
                        task.createdBy = userId;
                        task.taskFor = "mobile";
                        task.createdByEmailId = user.emailId;
                        taskSupportClassObj.createNewTask(task,userId,function(response){
                            res.send(response);
                        })
                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                });
            }

        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.post('/remainder/reply/call',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,google:1,firstName:1},function(error,user){
            if(common.checkRequired(user)){
                var replyObj = {
                    title:''
                }
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.post('/tasks/get/meeting',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.invitationId)){
            taskManagementClassObj.getTasks('meeting',req.query.invitationId,function(error,tasks){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                }
                else{
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":{
                            tasks:tasks
                        }
                    });
                }
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.post('/tasks/get/call',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.callId)){
            taskManagementClassObj.getTasks('call',req.query.callId,function(error,tasks){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                }
                else{
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":{
                            tasks:tasks
                        }
                    });
                }
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.post('/tasks/get/meeting/by/date',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.dateTime)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user) {
                if (common.checkRequired(user)) {
                    var timezone;
                    if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                        timezone = user.timezone.name;
                    } else timezone = 'UTC';

                    var date = req.body.dateTime;

                    if(common.checkRequired(date)){
                        date = new Date(date).toISOString();
                    }


                    var dateMin = common.checkRequired(date) ? moment(date).tz(timezone) : moment().tz(timezone);
                    var dateMax = common.checkRequired(date) ? moment(date).tz(timezone) : moment().tz(timezone);

                    dateMin.date(dateMin.date())
                    dateMin.hour(0)
                    dateMin.minute(0)
                    dateMin.second(0)
                    dateMin.millisecond(0)

                    dateMax.date(dateMax.date())
                    dateMax.hour(23)
                    dateMax.minute(59)
                    dateMax.second(59)
                    dateMax.millisecond(59)

                    dateMin = dateMin.format()
                    dateMax = dateMax.format()
                    taskManagementClassObj.getTasksForDate('meeting',user._id,dateMin,dateMax,function(error,tasks){
                        if(error){
                            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                        }
                        else{
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{
                                    tasks:tasks
                                }
                            });
                        }
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.post('/task/update/status', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);

    if (common.checkRequired(userId)) {
        if (common.checkRequired(req.body) && validation.isEmptyObject(req.body)) {

            var data = req.body;
            if (common.checkRequired(data.taskId) && common.checkRequired(data.status)) {
                if (data.status == 'notStarted' || data.status == 'inProgress' || data.status == 'complete'){
                    taskManagementClassObj.updateTaskStatus(userId,data, function ( isSuccess) {
                      if (isSuccess) {
                          interactionsObj.updateOpenTrackWithReferenceId(data.taskId);
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data": {}
                            });
                        } else res.send(errorObj.generateErrorResponse({
                            status: statusCodes.UPDATE_FAILED,
                            key: 'INVALID_DATA_RECEIVED'
                        }));
                    })
                } else res.send(errorObj.generateErrorResponse({
                    status: statusCodes.INVALID_REQUEST_CODE,
                    key: 'INVALID_STATUS'
                }));
            } else res.send(errorObj.generateErrorResponse({
                status: statusCodes.INVALID_REQUEST_CODE,
                key: 'TASK_ID_OR_STATUS_NOT_FOUND'
            }))
        } else res.send(errorObj.generateErrorResponse({
            status: statusCodes.INVALID_REQUEST_CODE,
            key: 'NO_REQUEST_BODY'
        }));
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

module.exports = router;
