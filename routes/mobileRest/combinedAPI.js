var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');
var mongoose = require('mongoose');
var Twit = require('twit');

var commonUtility = require('../../common/commonUtility');
var linkedinClass = require('../../common/linkedin');
var twitterClass = require('../../common/twitter');
var facebookClass = require('../../common/facebook');
var weatherJS = require('../../common/weather');
var ipLocatorJS = require('../../common/ipLocator');
var mobileAuth = require('../../common/mobileAuthValidation');
var appCredentials = require('../../config/relatasConfiguration');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var interactions = require('../../dataAccess/interactionManagement');
var taskManagementClass = require('../../dataAccess/taskManagementClass');
var contactClass = require('../../dataAccess/contactsManagementClass');
var meetingClass = require('../../dataAccess/meetingManagement');
var myUserCollection = require('../../databaseSchema/userManagementSchema').User;
var async = require('async')

var errorObj = new errorClass();
var linkedinClassObj = new linkedinClass();
var twitterClassObj = new twitterClass();
var facebookClassbj = new facebookClass();
var weatherObj = new weatherJS();
var mobileAuthObj = new mobileAuth();
var appCredential = new appCredentials();
var common = new commonUtility();
var interactionObj = new interactions();
var validation = new validations();
var userManagementObj = new userManagement();
var errorMessagesObj = new errorMessages();
var ipLocatorObj = new ipLocatorJS();
var contactObj = new contactClass();
var taskManagementClassObj = new taskManagementClass();
var meetingsObj = new meetingClass();

var statusCodes = errorMessagesObj.getStatusCodes();
var authConfig = appCredential.getAuthCredentials();

var Linkedin = require('node-linkedin')(authConfig.LINKEDIN_CONSUMER_KEY, authConfig.LINKEDIN_CONSUMER_SECRET, authConfig.LINKEDIN_REDIRECT_URL);

var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';

/* INCOMING-CALL SINGLE SERVER CALL */
/*
router.get('/incoming/call/get/insights',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var isEmailId = false;
                    if(common.checkRequired(req.query.idType)){
                        if(req.query.idType == 'emailId'){
                            isEmailId = true;
                        }
                    }
                    else{
                        return res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
                    }

                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';
                    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 1;
                    getLastInteraction(userId,user.emailId,req.query.id,req.query.idType,limit,timezone,req,res)

                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});
*/
/*

function getLastInteraction(userId,emailId,cUserId,idType,limit,timezone,req,res){
    var date = moment().tz(timezone);
    var data = {};

    interactionObj.lastInteractionWithContact(userId,cUserId,idType,date.format(),limit,function(interactions){
        if(common.checkRequired(interactions) && interactions.length > 0){
           data.lastInteractions = interactions;
        }else
           data.lastInteractions = [];

        getTasksAssigned(userId,emailId,cUserId,idType == 'emailId',timezone,data,req,res)
    })
}*/

/*function getTasksAssigned(userId,emailId,cUserId,isEmailId,timezone,data,req,res){
    var date = moment().tz(timezone)
    date.hour(0)
    date.minute(0)
    date.second(0)
    date.millisecond(0)
    date = date.format()

    taskManagementClassObj.getTasksForDateAllGreater(userId,cUserId,isEmailId,date,function(error,tasks){
        if(common.checkRequired(tasks) && tasks.length > 0){
            data.tasks = tasks;
        }else
            data.tasks = [];

        nextInteraction(userId,emailId,cUserId,isEmailId,timezone,data,req,res)
    })
}

function nextInteraction(userId,emailId,cUserId,isEmailId,timezone,data,req,res){
    var date = moment().tz(timezone);
    interactionObj.nextInteractionWithContact(userId,cUserId,isEmailId,date.format(),function(interactions){
        if(common.checkRequired(interactions) && interactions.length > 0){
            data.nextInteraction = interactions;
        }else
            data.nextInteraction = [];

        getCommonConnections(userId,emailId,cUserId,isEmailId,timezone,data,req,res)
    })
}

function getCommonConnections(userId,emailId,cUserId,isEmailId,timezone,data,req,res){
    if(isEmailId){
        contactObj.getCommonContactsWithEmail(mongoose.Types.ObjectId(userId),emailId,cUserId,[],function(contacts){
            data.common = contacts.splice(0,4);
            sendResponseData(req,res,data);
        })
    }
    else{
        contactObj.getCommonContacts(mongoose.Types.ObjectId(userId),mongoose.Types.ObjectId(cUserId),false,function(contacts){
            data.common = contacts.splice(0,4);
            sendResponseData(req,res,data);
        })
    }
}*/

function sendResponseData(req,res,data){
    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data":data
    });
}

/* CONTACT-INFO SINGLE SERVER CALL */
//deprecated, Use /contact/info/get/interactions instead -- Rajiv
/*
router.get('/contact/info/get/insights',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            var isEmailId = false;
            if(common.checkRequired(req.query.idType)){
                if(req.query.idType == 'emailId'){
                    isEmailId = true;
                }
            }
            else{
                return res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
            }

            userManagementObj.findUserProfileByIdWithCustomFields(userId,{_id:1,    emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';
                    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 1;
                    contactObj.getPersonalNote(user._id,req.query.id,isEmailId,function(data){
                        console.log(data)
                        var dataSend = {
                            interactionContent:{}
                        };

                        if(common.checkRequired(data) && data.length > 0 && common.checkRequired(data[0]) && common.checkRequired(data[0].contacts)){
                            dataSend.favorite = data[0].contacts.favorite || false;
                            dataSend.notes = data[0].contacts.notes || []
                        }
                        getLastThreeInteractions(userId,user.emailId,req.query.id,isEmailId,timezone,dataSend,req,res)
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});
*/

//contact detail info new for mobile. -- Rajiv, will work with mobile Number also.
router.get('/contact/info/get/profile/interactions',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE)
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)||common.checkRequired(req.query.mobileNumber)){
            var idType;
            var type;
            if(!common.checkRequired(req.query.idType)){
                res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}))
            }else{
                idType = req.query.idType
                if(idType=="mobileNumber")
                    type = "mobileNumber"
                if(req.query.idType == "userId")
                    type = "userId"
                if(idType=="emailId")
                    type = "emailId"
            }

            userManagementObj.findUserProfileByIdWithCustomFields(userId,{_id:1,emailId:1,timezone:1},function(err,user){
                if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';
                    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 3;
                    contactObj.getNoteForUser(user._id,req,type,function(data){
                        var dataSend = {};
                        if(common.checkRequired(data) && data.length > 0 && common.checkRequired(data[0]) && common.checkRequired(data[0].contacts)){
                            dataSend.favorite = data[0].contacts.favorite || false;
                            dataSend.notes = data[0].contacts.notes || []
                        }
                        getLastNInteractionsNew(user,timezone,dataSend,type,limit,req,res)
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}))
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/incoming/call/get/lastinteraction',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE)
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.mobileNumber) && common.checkRequired(req.query.idType)){


            userManagementObj.findUserProfileByIdWithCustomFields(userId,{_id:1,emailId:1,mobileNumber:1},function(err,user){

                var idType = req.query.idType
                var mobileNumber = req.query.mobileNumber
                var reqId = req.query.id

                interactionObj.lastInteractionWithUserMobile(userId,reqId,idType,req,function(dataInteractions){
                    var data = {}
                    data.lastInteraction = dataInteractions
                    sendResponseData(req,res,data)
            })
            })


        }else res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:'QUERY_PARAMETER_MISSED'}))
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.AUTHORIZATION_ERROR_CODE,key:'INVALID_TOKEN'}))
})



router.get('/contact/get/profile/nextInteraction',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE)
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id) && common.checkRequired(req.query.idType) && common.checkRequired(req.query.limit)) {
            var idType;
            var type;
            var isEmailId = false;
            var limit = req.query.limit?req.query.limit:1
            if (!common.checkRequired(req.query.idType)) {
                res.send(errorObj.generateErrorResponse({
                    status: statusCodes.INVALID_REQUEST_CODE,
                    key: 'QUERY_PARAMETER_MISSED'
                }))
            } else {

                idType = req.query.idType
                if (idType == "mobileNumber")
                    type = "mobileNumber"
                if (req.query.idType == "userId")
                    type = "userId"
                if (idType == "emailId"){
                    type = "emailId"
                    isEmailId = true
                }
            }
            userManagementObj.findUserProfileByIdWithCustomFields(userId, {
                _id: 1,
                emailId: 1,
                timezone: 1
            }, function (err, user) {
                if(err){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.ERROR_FETCHING_PROFILE,key:'ERROR_FETCHING_PROFILE'}))
                }else{
                    console.log(limit)
                    nextInteractionInfo(user._id,user.emailId,req.query.id,isEmailId,null,null,limit,req,function(meetings){
                        var data = {}
                        data.nextInteraction = meetings
                        sendResponseData(req,res,data)
                    })
                }
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}))
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
})

//Added on 05042016
var getLastNInteractionsNew = function(user,timezone,data,type,limitReq,req,response){
    var dateMin = moment().subtract(15, "days").toDate()
    var dateMax = moment().toDate()

    var isEmailId = false;
    if(type == "emailId")
        isEmailId = true;

    async.parallel( [function(callBackInteractions) {
        interactionObj.getInteractionsForContact(user, req, dateMin, dateMax, type, function (err, res) {

            // console.log(JSON.stringify(res,null,1));

            if(err)
                console.log(err)
            if(res.length>0 /*&& res[0].length>0*/){
                var limit = req.query.limit ?req.query.limit:3
                data.totalInteractions = res[0].totalCount
                data.emailCount = res[0].emailArray.length
                data.meetingCount = res[0].meetingArray.length
                data.callCount = res[0].callArray.length
                data.smsCount = res[0].smsArray.length
                var interactions = {}
                interactions.emails=res[0].emailArray.splice(0,limit)
                interactions.meetings = res[0].meetingArray.splice(0,limit)
                interactions.calls = res[0].callArray.splice(0,limit)
                interactions.sms =res[0].smsArray.splice(0,limit)
                data.interactions = interactions
            }
            else{
                data.totalInteractions = 0
                data.emailCount = 0
                data.meetingCount = 0
                data.callCount = 0
                data.smsCount = 0

                var interactions = {}
                interactions.emails=[]
                interactions.meetings = []
                interactions.calls = []
                interactions.sms = []

                data.interactions = interactions
            }
            callBackInteractions()

        })
    },function(callBackTasks){

        getTasksAssignedInfo(user._id,user.emailId,req.query.id,isEmailId,timezone,data,limitReq,req,function(err,tasks){

            if(common.checkRequired(tasks) && tasks.length > 0){
                data.tasks = tasks;
            }else
                data.tasks = [];
            callBackTasks()
        })
    }, function (callBackNextMeeting) {
        nextInteractionInfo(user._id,user.emailId,req.query.id,isEmailId,timezone,data,limitReq,req,function(meetings){
            if(meetings.length>0)
                data.nextInteraction = meetings.slice(0,1)
            else{
                data.nextInteraction = []
            }
            callBackNextMeeting()
        })
    },/* function (callBackCommonConnections) {
        getCommonConnectionsInfo(user._id,user.emailId,req.query.id,isEmailId,timezone,data,limitReq,req,function(contactsArr){
            data.common = contactsArr
            callBackCommonConnections()
        })

    }*/],function(){
        response.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data":data})
    })
}


function getTasksAssignedInfo(userId,emailId,cUserId,isEmailId,timezone,data,limit,req,callBack){
    var date = moment().tz(timezone)
    date.hour(0)
    date.minute(0)
    date.second(0)
    date.millisecond(0)
    date = date.format()
    taskManagementClassObj.getTasksForDateAllGreater(userId,cUserId,isEmailId,date,function(error,tasks){
        callBack(error,tasks)
    })
}

function nextInteractionInfo(userId,emailId,cUserId,isEmailId,timezone,data,limit,req,callBack){
    var dateMin = new Date()
    var cUserID = cUserId
    if(isEmailId) {
        userManagementObj.findUserProfileByEmailId(cUserId, function (err, resultUser) {
            if(resultUser && resultUser._id)
                cUserID = common.castToObjectId(resultUser._id)
            meetingsObj.getNextMeetingWithContact(emailId,cUserID, dateMin, function (meetings) {
                callBack(meetings)
            })
        })
    }else{
        meetingsObj.getNextMeetingWithContact(emailId,cUserID,dateMin,function(meetings){
            callBack(meetings)
        })
    }
}

function getCommonConnectionsInfo(userId,emailId,cUserId,isEmailId,timezone,data,limit,req,callback){
    var q = {}
    if(isEmailId) {
        q = {emailId: {$in: [emailId, cUserId]}}
    }else {
        q = {_id: {$in: [userId, cUserId]}}
    }


    var projectContact = {

        "_id": 1,
        "emailId": 1,
        "contacts._id": 1,
        "contacts.personId": 1,
        "contacts.personName": 1,
        "contacts.personEmailId": 1,
        "contacts.companyName": 1,
        "contacts.designation": 1,
        "contacts.mobileNumber": 1
        // "contacts.contactRelation":1,
        // "contacts.favorite":1,
        // "contacts.twitterUserName":1,
        // "contacts.addedDate":1,
        // "contacts.source":1,
        // "contacts.hashtag":1
    };


    myUserCollection.aggregate([{$match: q}, {$project: projectContact}], function (error, contacts) {
        if (!error && contacts.length == 2) {
            if (contacts[0]._id.toString() == userId.toString()) {
                var uIndex = 0;
                var cIndex = 1;
            }
            else {
                var uIndex = 1;
                var cIndex = 0;
            }
            contactObj.getCommonConnectionsNew(contacts[uIndex].emailId, contacts[cIndex].emailId, contacts[uIndex].contacts, contacts[cIndex].contacts, limit, function (contactsArr) {
                callback(contactsArr)
            })
        }
    })

    /*    contactObj.findProfileWithFieldsAsArray(emailId,{},"emailId",function(user){
            contactObj.findProfileWithFieldsAsArray(cUserId,{},"emailId",function(cUser){
                if(cUser.length>0) {
                    contactObj.getCommonConnectionsNew(user[0].emailId, cUser[0].emailId, user[0].contacts, cUser[0].contacts, limit, function (contactsArr) {
                        callback(contactsArr)
                    })
                }else{
                    callback([])
                }
            })
        })
    }
    else{
        contactObj.findProfileWithFieldsAsArray(userId,{contacts:1},"userId",function(userContacts){
            contactObj.findProfileWithFieldsAsArray(cUserId,{contacts:1},"userId",function(cUserContacts){
                if(cUserContacts.length>0) {
                    contactObj.getCommonConnectionsNew(userContacts[0].emailId, cUserContacts[0].emailId, userContacts[0].contacts, cUserContacts[0].contacts, 5, function (contactsArr) {
                        callback(contactsArr)
                    })
                }else{
                    callback([])
                }
            })
        })
    }*/
}

/* CONTACT-INFO-SOCIAL SINGLE SERVER CALL */
router.get('/contact/info/social/insights',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(req.query.id,{emailId:1,timezone:1,linkedin:1,twitter:1},function(error,user){
                if(common.checkRequired(user)){

                    if (common.checkRequired(user.linkedin) && common.checkRequired(user.linkedin.token)) {
                        var linkedIn = Linkedin.init(user.linkedin.token);
                        linkedIn.people.me(['first-name','last-name','headline','educations','summary'], function(err, profile) {
                            if(err || !common.checkRequired(profile)){
                                logger.info('error in getting linkedin profile of 1 '+user.emailId+' error '+JSON.stringify(profile));
                                //res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'LINKEDIN_ERROR'}));
                                getTweets(user, req, res, {error:'LINKEDIN_ERROR',linkedinProfile:profile})
                            }
                            else{
                                getTweets(user, req, res, {error:null,linkedinProfile:profile})
                            }
                        });
                    }
                    else{
                        getTweets(user, req, res, {error:'NO_LINKEDIN_ACCOUNT',linkedinProfile:null})
                    }

                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

function getTweets(user, req, res, linkedinInfo) {

    if (common.checkRequired(user.twitter) && common.checkRequired(user.twitter.token) && common.checkRequired(user.twitter.refreshToken)) {
        var T = new Twit({
            consumer_key: authConfig.TWITTER_CONSUMER_KEY,
            consumer_secret: authConfig.TWITTER_CONSUMER_SECRET,
            access_token: user.twitter.token,
            access_token_secret: user.twitter.refreshToken
        });

        T.get('statuses/user_timeline', function (err, data, response) {

            if (err) {

                logger.info('error occurred while requesting latest tweets, in Twit module ' + user.emailId);

                res.send(
                    {
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": {
                            "tweetInfo": {error:'TWITTER_ERROR',tweets:[]},
                            "inkedinInfo":linkedinInfo
                        }
                    }
                )
            }
            else {
                if (!common.checkRequired(data) && !common.checkRequired(data[0])) {

                    res.send(
                        {
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {
                                "tweetInfo": {error:'NO_TWEETS_FOUND',tweets:[]},
                                "inkedinInfo":linkedinInfo
                            }
                        }
                    )
                }
                else {
                    data = data.splice(0, 30);
                    var status = {
                        id: user._id,
                        userId: user._id,
                        userEmailId: user.emailId,
                        userFirstName: user.firstName,
                        twitter: data
                    };

                    userManagementObj.storeTwitterStatus(status, function (error, isSuccess, message) {
                        if (error) {
                            logger.info(error);
                        }
                    });

                    res.send(
                        {
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {
                                "tweetInfo": {error:null,tweets:data.splice(0, 2)},
                                "inkedinInfo":linkedinInfo
                            }
                        }
                    )
                }
            }
        })
    }
    else{
        res.send(
            {
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": {
                    "tweetInfo": {error:'NO_TWITTER_ACCOUNT',tweets:[]},
                    "inkedinInfo":linkedinInfo
                }
            }
        )
    }
}

// ------------------------------------------------------------------------------------------
//modified by Rajiv
router.get('/incoming/call/get/insights/new',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);

    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.mobileNumber)){
            var search = new RegExp(req.query.mobileNumber.trim(),'i');
            var q = {mobileNumber:{$eq:req.query.mobileNumber}}

            common.getProfileOrStoreProfileInSession(userId,req,function(user){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';
                var limit = common.checkRequired(req.query.limitBy) ? parseInt(req.query.limitBy) : 5;

                userManagementObj.selectUserProfileCustomQuery(q,{publicProfileUrl:1,profilePicUrl:1,firstName:1,lastName:1,emailId:1,companyName:1,designation:1,location:1,currentLocation:1,timezone:1,serviceLogin:1,mobileNumber:1},function(error,cUser){

                    if(common.checkRequired(cUser) && cUser.firstName!=null){
                        getLastInteraction2(userId,user.emailId,cUser,'userId',limit,timezone,req,res)
                    }
                    else {
                        getLastInteraction2(common.castToObjectId(userId),user.emailId,req.query.mobileNumber.trim(),'mobileNumber',limit,timezone,req,res)
                    }
                })
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});
//modified by Rajiv -- 04032016
function getLastInteraction2(userId,emailId,cUserId,idType,limit,timezone,req,res){
    var date = new Date().toISOString()
    var data = {};

    var cUserIs
    var rUserId
    if(idType == 'userId')
        cUserIs = common.castToObjectId(cUserId._id)
        rUserId = cUserIs

    if(idType == 'mobileNumber'){
        cUserIs = cUserId
    }

    interactionObj.lastInteractionWithContactMobile(common.castToObjectId(userId),emailId,cUserIs,idType,date,limit,function(interactions,totalCount){

        if(common.checkRequired(interactions) && interactions.length > 0){
            data.lastInteractions = interactions;
            data.interactionCount = totalCount;
        }else {
            data.lastInteractions = [];
        }
        /*date.hour(0)
        date.minute(0)
        date.second(0)
        date.millisecond(0)
        date = date.format()*/

        //taskManagementClassObj.getTasksForDateAllGreater(userId,cUserId,idType,date,function(error,tasks){
        //    if(common.checkRequired(tasks) && tasks.length > 0){
        //        data.tasks = tasks;
        //    }else
        //        data.tasks = [];

            /*if(data.tasks.length > 0){
                data.nextInteraction = null;
                sendResponseData(req,res,data);
            }
            else{*/
                var date = moment().tz(timezone);
                meetingsObj.getNextMeetingWithContact(emailId,rUserId,date,function(invitations){

                    data.nextInteraction = invitations.slice(0,1);
                    sendResponseData(req,res,data)
                    })
                    //}
                })
            //}
        //})
    }

router.get('/weather/current/temp',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){

            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,location:1,currentLocation:1},function(error,user){
                if(common.checkRequired(user)){
                   var lat,lan, location;
                   if(common.checkRequired(user.currentLocation) && common.checkRequired(user.currentLocation.latitude) && common.checkRequired(user.currentLocation.longitude)){
                        lat = user.currentLocation.latitude;
                        lan = user.currentLocation.longitude;
                   }

                    if(common.checkRequired(user.location)){
                        location = user.location;
                    }


                    if(common.checkRequired(location)){
                        weatherObj.getForecast(location,function(error,forecast,query){
                            if(common.checkRequired(forecast) && common.checkRequired(forecast)){
                                res.send(forecast)
                            }
                            else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'NO_LOCATION_DETAILS_FOUND'}));
                        });
                    }else{
                        //emailSenderObj.sendUncaughtException('IP',ip,user.emailId,'IP ADDR')
                        var ip = null;
                        
                        ipLocatorObj.lookup(ip,userId,function(locationNew){
                            var lNew;
                            console.log(locationNew)
                            if(common.checkRequired(locationNew) && common.checkRequired(locationNew.city) && common.checkRequired(locationNew.country)){
                                lNew = locationNew.city;
                                if(common.checkRequired(locationNew.country)){
                                    lNew += ', '+locationNew.country
                                }
                            }

                            //emailSenderObj.sendUncaughtException('Addr',lNew,JSON.stringify(locationNew),'ADDR')
                            if(common.checkRequired(lNew)){
                                weatherObj.getForecast(lNew,function(error,forcast,query){
                                    if(common.checkRequired(forcast)){
                                        res.send(forcast)
                                    }
                                    else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'NO_LOCATION_DETAILS_FOUND'}));
                                });
                            }
                            else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'NO_LOCATION_DETAILS_FOUND'}));
                        })
                    }

                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/contact/info/social/linkedin/feed',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(req.query.id,{linkedin:1},function(error,user){
                if(common.checkRequired(user)){
                    if (common.checkRequired(user.linkedin) && common.checkRequired(user.linkedin.token)) {

                        linkedinClassObj.getUserFeeds(user.linkedin,1,0,function(isSuccess,feed){
                            res.send(feed);
                        })
                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_LINKEDIN_ACCOUNT'}));
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/contact/info/social/twitter/feed',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(req.query.id,{twitter:1},function(error,user){
                if(common.checkRequired(user)){
                    if (common.checkRequired(user.twitter) && common.checkRequired(user.twitter.token)) {

                        twitterClassObj.getTwitterFeed(user.twitter,true,1,function(isSuccess,feed){
                            res.send(feed);
                        })
                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_TWITTER_ACCOUNT'}));
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/contact/info/social/facebook/feed',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(req.query.id,{facebook:1},function(error,user){
                if(common.checkRequired(user)){
                    if (common.checkRequired(user.facebook) && common.checkRequired(user.facebook.token)) {
                        facebookClassbj.getFacebookFeeds(user.facebook,true,1,function(isSuccess,feed){
                            res.send(feed);
                        })
                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_FACEBOOK_ACCOUNT'}));
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

module.exports = router;