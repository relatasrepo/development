var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');

var commonUtility = require('../../common/commonUtility');
var mobileAuth = require('../../common/mobileAuthValidation');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var relatasConfigManagement = require('../../dataAccess/relatasConfigurationManagement');
var errorMessages = require('../../errors/errorMessage');
var meetingManagement = require('../../dataAccess/meetingManagement');
var eventDataAccess = require('../../dataAccess/eventDataAccess');
var calendarPasswordManagement = require('../../dataAccess/calendarPasswordManagement');
var profileManagementClass = require('../../dataAccess/profileManagementClass');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');

var profileManagementClassObj = new profileManagementClass();
var errorObj = new errorClass();
var mobileAuthObj = new mobileAuth();   
var common = new commonUtility();
var validation = new validations();
var userManagementObj = new userManagement();
var relatasConfigManagementObj = new relatasConfigManagement();
var errorMessagesObj = new errorMessages();
var company = new companyClass();

var statusCodes = errorMessagesObj.getStatusCodes();

var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';

router.get('/profile/get/basic/:id', mobileAuthObj.isAuthenticatedMobile, function (req, res) {

    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        var profileUserId;
        if(req.params.id == 'me'){
            profileUserId = userId
        }
        else profileUserId = req.params.id;
        userManagementObj.findUserProfileByIdWithCustomFields(profileUserId, {
            contacts: 0,
            google: 0,
            linkedin:0,
            facebook:0,
            twitter:0,
            admin:0,
            officeCalendar:0,
            currentLocation:0,
            calendarAccess:0,
            workHours:0,
            widgetKey:0,
            profilePrivatePassword:0
        }, function (error, user) {
            console.log("User from profile get basic:", user);
            
            if(error){
                res.send(errorObj.generateErrorResponse({
                    status: statusCodes.PROFILE_ERROR_CODE,
                    key: 'ERROR_FETCHING_PROFILE'
                }));
            }else
            if (common.checkRequired(user)) {
                common.checkImageExtension(user);
                user.publicProfileUrl = common.getValidUniqueName(user.publicProfileUrl);
                user["companyDetails"] = {};

                    if(user.companyId){
                        company.findCompanyById(user.companyId,function (companyDetails) {
                            user["companyDetails"] = companyDetails;
                            sendResponseList(req, res, user)
                        })
                    } else {
                        sendResponseList(req, res, user)
                    }
            } else {
                res.send(errorObj.generateErrorResponse({
                    status: statusCodes.PROFILE_ERROR_CODE,
                    key: 'USER_PROFILE_NOT_FOUND'
                }));
            }
        })
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

router.get('/profile/get/full/:id', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        var profileUserId;
        if(req.params.id == 'me'){
            profileUserId = userId
        }
        else profileUserId = req.params.id;
        userManagementObj.findUserProfileByIdWithCustomFields(profileUserId, {
            contacts: 0,
            google: 0,
            admin:0,
            currentLocation:0,
            calendarAccess:0,
            workHours:0,
            widgetKey:0,
            profilePrivatePassword:0
        }, function (error, user) {
            if(error){
                res.send(errorObj.generateErrorResponse({
                    status: statusCodes.PROFILE_ERROR_CODE,
                    key: 'ERROR_FETCHING_PROFILE'
                }));
            }else
            if (common.checkRequired(user)) {
                common.checkImageExtension(user);
                user.publicProfileUrl = common.getValidUniqueName(user.publicProfileUrl)
                user["companyDetails"] = {};

                    if(user.companyId){
                        company.findCompanyById(user.companyId,function (companyDetails) {
                            user["companyDetails"] = companyDetails;
                            sendResponseList(req, res, user)
                        })
                    } else {
                        sendResponseList(req, res, user)
                    }
            } else {
                res.send(errorObj.generateErrorResponse({
                    status: statusCodes.PROFILE_ERROR_CODE,
                    key: 'USER_PROFILE_NOT_FOUND'
                }));
            }
        })
    }else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

router.get('/profile/get/by/mobile/:number', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {

        if(common.checkRequired(req.params.number)){
            var num = req.params.number;
            var num0 = req.params.number;
            //check if leading number is a zero and try again
            if(num.charAt(0)=='0' && num.charAt(1)!='0'){
                num0 = num.substring(1)
            }

            var q = {$or:[{mobileNumber:{$eq:num}},{mobileNumberWithoutCC:{$eq:num}},{mobileNumberWithoutCC:{$eq:num0}}]}

            userManagementObj.selectUserProfileCustomQuery(q, {
                contacts: 0,
                google: 0,
                linkedin:0,
                facebook:0,
                twitter:0,
                admin:0,
                officeCalendar:0,
                currentLocation:0,
                calendarAccess:0,
                workHours:0,
                widgetKey:0,
                profilePrivatePassword:0
            }, function (error, user) {
                if(error){
                    res.send(errorObj.generateErrorResponse({
                        status: statusCodes.PROFILE_ERROR_CODE,
                        key: 'ERROR_FETCHING_PROFILE'
                    }));
                }else
                if (common.checkRequired(user)) {

                    common.checkImageExtension(user);
                    user.publicProfileUrl = common.getValidUniqueName(user.publicProfileUrl)
                    user["companyDetails"] = {};

                    if(user.companyId){
                        company.findCompanyById(user.companyId,function (companyDetails) {
                            user["companyDetails"] = companyDetails;
                            sendResponseList(req, res, user)
                        })
                    } else {
                        sendResponseList(req, res, user)
                    }
                } else {

                    res.send(errorObj.generateErrorResponse({
                        status: statusCodes.PROFILE_ERROR_CODE,
                        key: 'USER_PROFILE_NOT_FOUND'
                    }));
                }
            })
        }else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_DATA_RECEIVED'}));
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

router.post('/profile/update/full/mobile',function(req,res){
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{contacts:0,google:0,linkedin:0,twitter:0,facebook:0,officeCalendar:0,calendarAccess:0},function(err,user){
                if (common.checkRequired(user)) {

                    var reqObj = req.body;
                    var updateObj = {};
                    var isValidUpdate = false;
                    var updateInteractions = false;

                    if(common.checkRequired(reqObj.firstName)){
                        isValidUpdate = true;
                        updateInteractions = true;
                        updateObj.firstName = reqObj.firstName;
                    }

                    if(common.checkRequired(reqObj.lastName)){
                        isValidUpdate = true;
                        updateInteractions = true;
                        updateObj.lastName = reqObj.lastName;
                    }


                    if(common.checkRequired(reqObj.skypeId)){
                        isValidUpdate = true;
                        updateObj.skypeId = reqObj.skypeId;
                    }

                    if(common.checkRequired(reqObj.mobileNumber)){
                        isValidUpdate = true;
                        updateInteractions = true;
                        updateObj.mobileNumber = reqObj.mobileNumber;
                    }

                    if(common.checkRequired(reqObj.isMobileNumberVerified)){
                        isValidUpdate = true;
                        updateObj.isMobileNumberVerified = reqObj.isMobileNumberVerified;
                    }

                    if(common.checkRequired(reqObj.mobileNumberOTP)){
                        isValidUpdate = true;
                        updateObj.mobileNumberOTP = reqObj.mobileNumberOTP;
                    }

                    if(common.checkRequired(reqObj.birthday) && common.checkRequired(reqObj.birthday.day) && common.checkRequired(reqObj.birthday.month)  && common.checkRequired(reqObj.birthday.year)){
                        isValidUpdate = true;
                        updateObj.birthday = reqObj.birthday;
                    }

                    if(common.checkRequired(reqObj.sendDailyAgenda)){
                        isValidUpdate = true;
                        updateObj.sendDailyAgenda = reqObj.sendDailyAgenda;
                    }

                    if(common.checkRequired(reqObj.location)){
                        isValidUpdate = true;
                        updateInteractions = true;
                        updateObj.location = reqObj.location;
                    }

                    if(reqObj.openActionItemAfterCall == true || reqObj.openActionItemAfterCall == 'true'){
                        isValidUpdate = true;
                        updateObj.openActionItemAfterCall = true;
                    }
                    else{
                        isValidUpdate = true;
                        updateObj.openActionItemAfterCall = false;
                    }

                    /*
                     "timezone" : {
                     "name" : "Asia/Kolkata",
                     "zone" : "+05:30"
                     }
                     */

                    if(common.checkRequired(reqObj.timezone) && common.checkRequired(reqObj.timezone.name) && common.checkRequired(reqObj.timezone.zone)){
                        isValidUpdate = true;
                        reqObj.timezone.updated = true;
                        updateObj.timezone = reqObj.timezone;
                        updateObj.zoneNumber = getZoneNumber(reqObj.timezone.zone);
                    }


                    if(isValidUpdate){
                        profileManagementClassObj.updateProfileCustomQuery(userId,updateObj,function(isSuccess){
                            if(isSuccess){
                                if(updateInteractions){
                                    common.updateInteractions(userId);
                                }
                                common.updateContactMultiCollectionLevel(userId,false);
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data": {}
                                });
                            }
                            else {
                                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_UPDATE_PROFILE'}));
                            }
                        })
                    }
                    else{
                        res.send(errorObj.generateErrorResponse({
                            status: statusCodes.UPDATE_FAILED,
                            key: 'INVALID_DATA_RECEIVED'
                        }));
                    }

                } else res.send(errorObj.generateErrorResponse({
                    status: statusCodes.PROFILE_ERROR_CODE,
                    key: 'ERROR_FETCHING_PROFILE'
                }));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
    }else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

function getZoneNumber(zone){

    if(zone){
        var zoneStr = zone.toString();
        var num = parseInt(zoneStr.substring(0, 3));

        if(zoneStr.charAt(0) == '+'){

            if(num >= 6 && num <= 11){
                return 1;
            }else if(num >= 0 && num <= 5){
                return 2;
            }else return 0;
        }else if(zoneStr.charAt(0) == '-'){

            if(num >= -6 && num <= 0){
                return 3;
            }else if(num >= -11 && num <= -6){
                return 4;
            }else return 0;
        }else return 0;
    }else return 0;
}

function sendResponseList(req, res, profile) {
    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data": {
            profile:profile,
            companyDetails: profile.companyDetails
        }
    });
}

/* UPDATE PROFILE */

router.post('/profile/update/google/primary', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        var status = validation.validateGoogleAccount(req.body);
        if(status.status == 4010){
            res.send(errorObj.generateErrorResponse(status));
        }
        else{
            userManagementObj.updateGooglePrimaryAcc(userId,req.body,function(error,result){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
                }
                else if(result){
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": {
                        }
                    });
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
            })
        }
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

router.post('/profile/update/mobileNumber', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if(common.checkRequired(req.body.mobileNumber) && common.checkRequired(req.body.countryCode)){
                userManagementObj.updateMobileNumber(userId,req.body.countryCode,req.body.mobileNumber,function(error,result){
                    if(error){
                        res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
                    }
                    else if(result){
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {}
                        });
                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
                })
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'PHONE_NUMBER_NOT_FOUND'}));
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

//METHOD TO VERIFY OTP AND UPDATE FIELD ISMOBILEVERIFIED
router.post('/profile/update/verify/mobileNumberAndOTP',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if(common.checkRequired(req.body.mobileNumber)){
                if(common.checkRequired(req.body.code) && common.checkRequired(req.body.toVerify)){
                    userManagementObj.verifyMobileNumberWithOTP(userId,req.body.code,req.body.toVerify,function(err,result){
                        if(err){
                            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
                        }
                        else if (result){
                            res.send({
                                "SuccessCode": 1,
                                "Message": "MOBILE_NUMBER_VERIFIED",
                                "ErrorCode": 0,
                                "Data": {}
                            });
                        }
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_OTP_CODE_OR_TOVERIFY_FIELD_FOUND'}))
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'MOBILE_NUMBER_NOT_FOUND'}))
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
    }   else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

/*router.post('/profile/update/isMobileVerified', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if(common.checkRequired(req.body.mobileNumberOTP)){
                userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,isMobileNumberVerified:1,mobileNumberOTP:1,mobileNumber:1},function(error,user){
                    if(common.checkRequired(user)){
                        if(common.checkRequired(user.mobileNumber)){
                            if(user.mobileNumberOTP == req.body.mobileNumberOTP){
                                var updateObj = {
                                    isMobileNumberVerified:true
                                };
                                profileManagementClassObj.updateProfileCustomQuery(userId,updateObj,function(updateResult){
                                    if(updateResult){
                                        res.send({
                                            "SuccessCode": 1,
                                            "Message": "",
                                            "ErrorCode": 0,
                                            "Data": {}
                                        });
                                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
                                });
                            }
                            else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_OTP_RECEIVED'}));
                        }
                        else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'MOBILE_NUMBER_NOT_FOUND'}));
                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                });
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_OTP_RECEIVED'}));

        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});*/

router.get('/profile/update/remove/linkedin', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
       userManagementObj.updateLinkedin(userId,'','','','',function(error,isSuccess,msg){
           if(error){
               res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
           }
           else if(isSuccess){
               res.send({
                   "SuccessCode": 1,
                   "Message": "",
                   "ErrorCode": 0,
                   "Data": {}
               });
           }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
       })
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

router.get('/profile/update/remove/facebook', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        userManagementObj.updateFacebook(userId,'','','','',function(error,isSuccess,msg){
            if(error){
                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
            }
            else if(isSuccess){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": {}
                });
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
        })
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

router.get('/profile/update/remove/twitter', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        userManagementObj.updateTwitter(userId,'','','','','',function(error,isSuccess,msg){
            if(error){
                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
            }
            else if(isSuccess){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": {}
                });
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
        })
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

router.post('/profile/update/add/linkedin', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        var status = validation.validateLinkedinAccount(req.body);
        if(status.status == 5000){
            res.send(errorObj.generateErrorResponse(status));
        }
        else{
            userManagementObj.updateLinkedin(userId,req.body.id,req.body.token,req.body.emailId,req.body.name,function(error,isSuccess,msg){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
                }
                else if(isSuccess){
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": {}
                    });
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
            })
        }
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

router.post('/profile/update/add/facebook', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        var status = validation.validateFacebookAccount(req.body);
        if(status.status == 5000){
            res.send(errorObj.generateErrorResponse(status));
        }
        else{
            userManagementObj.updateFacebook(userId,req.body.id,req.body.token,req.body.emailId,req.body.name,function(error,isSuccess,msg){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
                }
                else if(isSuccess){
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": {}
                    });
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
            })
        }
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

router.post('/profile/update/add/twitter', mobileAuthObj.isAuthenticatedMobile, function (req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        var status = validation.validateTwitterAccount(req.body);
        if(status.status == 5000){
            res.send(errorObj.generateErrorResponse(status));
        }
        else{
            userManagementObj.updateTwitter(userId,req.body.id,req.body.token,req.body.refreshToken,req.body.userName,req.body.displayName,function(error,isSuccess,msg){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
                }
                else if(isSuccess){
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": {}
                    });
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
            })
        }
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

//added Datemarker for when the app was first installed
router.post('/profile/update/mobile/firstuse',function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE)
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.body.emailId)){
            var emailId = req.body.emailId
            userManagementObj.findUserProfileByEmailId(emailId,function(err,mData){
                if(err)
                    res.send(errorObj.generateErrorResponse({status :statusCodes.SOMETHING_WENT_WRONG_CODE,key : 'SOMETHING_WENT_WRONG'}))
                if(mData != null || mData != undefined ) {
                    if (mData.isMobileUser == null) {
                        userManagementObj.updateFirstUseOfMobile(emailId, function (err, updated) {
                            if (err)
                                res.send(errorObj.generateErrorResponse({
                                    status: statusCodes.UPDATE_FAILED,
                                    key: 'UPDATE_FAILED'
                                }))
                            else {
                                console.log(updated)
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data": {}
                                })
                            }
                        })
                    }
                    else{
                    res.send({
                            "SuccessCode": 1,
                            "Message": "Already a Relatas Mobile User",
                            "ErrorCode": 0,
                            "Data": {}
                    })
                }
                }
            })
        }else res.send(errorObj.generateErrorResponse({status: statusCodes.MANDATORY_FIELD_ERROR_CODE,key: 'MANDATORY_FIELD_MISSING' }))
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}))
}) 

router.post('/profile/update/app-version', mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token)
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.body.versionName)){
            userManagementObj.updateMobileAppVersionName(userId,req.body.versionName,function(err,result){
                if(err){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'UPDATE_FAILED'}))
                }else{
                    res.send({
                        "SuccessCode": 1,
                        "Message": "App Version Updated on "+ new Date(),
                        "ErrorCode": 0,
                        "Data": {}})
                }
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:'MANDATORY_FIELD_MISSING'}))
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}))
})

router.get('/profile/update/mobile/sync/date', mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token)
    if(common.checkRequired(userId)){
        userManagementObj.updateMobileSyncDate(userId, new Date(),function(err,result){
            if(err){
                res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'UPDATE_FAILED'}))
            }else{
                res.send({
                    "SuccessCode": 1,
                    "Message": "App Synced on "+ new Date(),
                    "ErrorCode": 0})
            }
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}))
})

router.get('/get/mobile/app/latest/version', mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token)
    if(common.checkRequired(userId)){
        relatasConfigManagementObj.getLatestMobileAppVersion(function(err, result) {
            if(err) {
                res.send({
                    "SuccessCode": 0,
                    "ErrorCode": 1
                })
            } else if(result) {
                res.send({
                    "SuccessCode": 1,
                    "ErrorCode": 0,
                    "Data": result
                })
            }
        })

    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}))
})

router.post('/profile/update/mobile/sync/date', mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token)
    if(common.checkRequired(userId)){
        userManagementObj.updateMobileSyncDate(userId, new Date(),function(err,result){
            if(err){
                res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'UPDATE_FAILED'}))
            }else{
                res.send({
                    "SuccessCode": 1,
                    "Message": "App Synced on "+ new Date(),
                    "ErrorCode": 0,
                    "Data": {}})
            }
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}))
})

router.post('/profile/update/mobile/last/login', mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token)
    if(common.checkRequired(userId)){
        userManagementObj.updateMobileLastLoginDate(userId, req.body.appVersion, function(err,result){
            if(err){
                res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'UPDATE_FAILED'}))
            }else{

                common.insertMobileLog(userId,req)

                res.send({
                    "SuccessCode": 1,
                    "Message": "Last login date:"+ new Date(),
                    "ErrorCode": 0,
                    "Data": {}})
            }
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}))
})


router.post('/profile/update/preferences',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token)
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.body.postCallScreenEnabled)){
            
            var updateObj = {}

            var data = {
                "SuccessCode":1,
                "Message":"",
                "ErrorCode":0,
                "Data":{}
            }

            if(common.checkRequired(req.body.timezone)) {
                userManagementObj.updateTimezone(userId, req.body.timezone, function (err, isSuccess, msg) {
                    data.Data.timezone = msg
                })
            }
            

            updateObj.isOpenActionItem = req.body.postCallScreenEnabled
            updateObj.agenda = req.body.agendaMail
            updateObj.location = req.body.location

            if(updateObj.agenda!="never"){
                updateObj.agendaMailEnabled = true
            }else{
                updateObj.agendaMailEnabled = false
            }

            userManagementObj.updateMobilePreferences(userId,updateObj,function(err,result){
                if(err){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'DB_UPDATE_FAILED'}))
                }else{
                    res.send(data)
                }
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:'REQUEST_BODY_MISSING'}))
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}))
})

router.get('/profile/get/preferences/mobile',mobileAuthObj.isAuthenticatedMobile,function(req,res){

    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{timezone:1,notification:1,openActionItemAfterCall:1,location:1},function(err,user){
            var data = {}
            if(err){
                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'PROFILE_NOT_FOUND'}))
            }else {
                data.openActionItems = user.openActionItemAfterCall
                var notification = {}
                notification= user.notification
                data.dailyAgenda = notification.notificationOn
                data.location = user.location
                data.timezone = user.timezone

                res.send({
                    "SuccessCode":1,
                    "Message":"",
                    "ErrorCode":0,
                    "Data":data
                })
            }
        })

    }else res.send(errorObj.generateErrorResponse({status:statusCodes.AUTHORIZATION_ERROR_CODE,key:"INVALID_TOKEN"}))

})

router.get('/get/latest/mobile/version',mobileAuthObj.isAuthenticatedMobile,function(req,res){

    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){

    }else res.send(errorObj.generateErrorResponse({status:statusCodes.AUTHORIZATION_ERROR_CODE,key:"INVALID_TOKEN"}))

})

router.post('/profile/update/profilepic',function(req, res) {
    var userId = common.decodeToken(req.headers.token, MOBILE_TOKEN_SECRETE);
    if (common.checkRequired(userId)) {
        var fstream;
        if(common.checkRequired(req.busboy)){
            req.pipe(req.busboy);
            req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {

                if(common.contains(mimetype,'image')){
                    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,publicProfileUrl:1},function(error, userData){
                        if(error || userData == null){
                            res.send(errorObj.generateErrorResponse({
                                status: statusCodes.PROFILE_ERROR_CODE,
                                key: 'ERROR_FETCHING_PROFILE'
                            }));
                        }
                        else{
                            var extension = filename.split('.')[filename.split('.').length-1];
                            var uniqueName = common.getValidUniqueName(userData.publicProfileUrl);
                            var imagePath = '/profileImages/'+uniqueName+'.'+extension || 'jpg';

                            fstream = fs.createWriteStream('./public/profileImages/' + uniqueName+'.'+extension || 'jpg');
                            file.pipe(fstream);
                            fstream.on('close', function () {
                                userManagementObj.updateProfileImage(userId,imagePath,function(error,isUpdated,message){
                                    if(error){
                                        res.send(errorObj.generateErrorResponse({status: statusCodes.PROFILE_ERROR_CODE, key: 'FAILED_TO_UPDATE_PROFILE_PIC'}));
                                    }
                                    else if(isUpdated){
                                        res.send({
                                            "SuccessCode": 1,
                                            "Message": "",
                                            "ErrorCode": 0,
                                            "Data": {

                                            }
                                        });
                                    }
                                    else res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG'}));
                                });
                            });
                        }
                    });
                }
                else{
                    res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_IMAGE_FILE_TYPE'}));
                }
            });
        } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'NO_FILES_RECEIVED'}));
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'}));
});

module.exports = router;