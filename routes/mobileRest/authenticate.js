var express = require('express');
var router = express.Router();
var request = require('request');
var fs = require('fs');

var commonUtility = require('../../common/commonUtility');
var appCredentials = require('../../config/relatasConfiguration');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var winstonLog = require('../../common/winstonLog');
var googleCalendarAPI = require('../../common/googleCalendar');
var IPLocatorClass = require('../../common/ipLocator');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');

var logLib = new winstonLog();
var logger = logLib.getWinston();
var errorObj = new errorClass();
var googleCalendar = new googleCalendarAPI();
var appCredential = new appCredentials();
var common = new commonUtility();
var validation = new validations();
var userManagementObj = new userManagement();
var emailSenderObj = new emailSender();
var errorMessagesObj = new errorMessages();
var IPLocator = new IPLocatorClass();
var company = new companyClass();


var statusCodes = errorMessagesObj.getStatusCodes();

var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';

/* BEFORE SIGN-UP VALIDATION */
router.post('/authenticate/profile/exist',function(req,res){

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        if(common.checkRequired(req.body.emailId)){
            userManagementObj.findUserProfileByEmailIdWithCustomFields(req.body.emailId,{contacts:0,linkedin:0,facebook:0,twitter:0,officeCalendar:0,calendarAccess:0,widgetKey:0,workHours:0},function(error,user){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                }
                else if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';
                    common.checkImageExtension(user);
                    //common.makeFavoriteContacts(user._id.toString(),user.isFavoriteUpdated,timezone,user.lastLoginDate);
                    common.insertLog(user,'mobile-existing-profile',req);
                    user.publicProfileUrl = common.getValidUniqueName(user.publicProfileUrl)
                    user.uniqueName = user.publicProfileUrl;

                    user["companyDetails"] = {};

                    if(user.companyId){
                        company.findCompanyById(user.companyId,function (companyDetails) {
                            user["companyDetails"] = companyDetails;

                            var successExist = {
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "ProfileExist":true,
                                "Data":{
                                    token:common.generateToken(MOBILE_TOKEN_SECRETE,user._id),
                                    profile:user,
                                    companyDetails: user.companyDetails
                                }
                            };
                            res.send(successExist)
                        })
                    } else {
                        var successExist = {
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "ProfileExist":true,
                            "Data":{
                                token:common.generateToken(MOBILE_TOKEN_SECRETE,user._id),
                                profile:user,
                                companyDetails: user.companyDetails
                            }
                        };
                        res.send(successExist)
                    }
                }
                else{
                    var successNotExist = {
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "profileExist":false,
                        "Data":{

                        }
                    };
                    res.send(successNotExist)
                }
            });
        }else {
            res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'EMAIL_ID_NOT_FOUND_IN_REQUEST'}));  
        } 
    }
    else {
        res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
    }
});

router.post('/authenticate/profile/uniquename/exist',function(req,res){
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        if(common.checkRequired(req.body.uniqueName)){
            var protocol = 'http://';
            var domain = protocol+req.headers.host;
            var urls = [req.body.uniqueName,domain+'/'+req.body.uniqueName];
            userManagementObj.selectUserProfileCustomQuery({publicProfileUrl:{$in:urls}},{emailId:1},function(error,user){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                }
                else if(common.checkRequired(user)){
                    var successExist = {
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "UniqueNameExist":true,
                        "Data":{
                        }
                    };
                    res.send(successExist)
                }
                else{
                    var successNotExist = {
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "UniqueNameExist":false,
                        "Data":{
                        }
                    };
                    res.send(successNotExist)
                }
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'UNIQUE_NAME_NOT_FOUND_IN_REQUEST'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

/* STORING PROFILE WITH VALIDATIONS */
router.post('/authenticate/profile/create',function(req,res){

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        var status = validation.validateMobileSignUpProfileMandatoryFields(req.body,req.body.loginType);
        if(status.status == 4010){
            res.send(errorObj.generateErrorResponse(status));
        }else{

            var pr = req.body;
            pr.public = true;
            pr.registeredUser = true;
            pr.publicProfileUrl = req.body.uniqueName;
            pr.mobileNumber = req.body.countryCode+req.body.phoneNumber;
            pr.mobileNumberWithoutCC = req.body.phoneNumber;
            pr.countryCode = req.body.countryCode;
            pr.isMobileNumberVerified = req.body.isMobileNumberVerified;
            pr.mobileNumberOTP = common.checkRequired(req.body.mobileNumberOTP) ? req.body.mobileNumberOTP : '';
            pr.corporateUser = false;
            pr.profilePicUrl = common.checkRequired(pr.profilePicUrl) ? pr.profilePicUrl : '/images/default.png';
            pr.registeredDate = new Date();
            pr.location = req.body.location;

            if(req.body.loginType && req.body.loginType === 'OUTLOOK'){
                pr.outlook = [
                    {
                        id:pr.account.id,
                        token:pr.account.token,
                        refreshToken:pr.account.refreshToken,
                        emailId:pr.account.emailId,
                        name:pr.account.name
                    }
                ];

                pr.serviceLogin = "outlook"
            } else {
                pr.google = [
                    {
                        id:pr.account.id,
                        token:pr.account.token,
                        refreshToken:pr.account.refreshToken,
                        emailId:pr.account.emailId,
                        name:pr.account.name
                    }
                ];
                pr.serviceLogin = 'google';
            }

            pr.linkedin = {
                id:'',
                token:'',
                emailId:'',
                name:''
            };
            pr.facebook = {
                id:'',
                token:'',
                emailId:'',
                name:''
            };
            pr.twitter = {
                id:'',
                token:'',
                refreshToken:'',
                displayName:'',
                userName:''
            };
            //emailSenderObj.sendUncaughtException('',pr.profilePicUrl,req.body.emailId,'PROFILE-CREATE');
            //TODO:Replace with successful onboard email.
            userManagementObj.addUserThirdParty(pr,function(error,profile,msg){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_SAVING_PROFILE'}));
                }
                else{
                    var timezone;
                    if(common.checkRequired(profile.timezone) && common.checkRequired(profile.timezone.name)){
                        timezone = profile.timezone.name;
                    }else timezone = 'UTC';

                    profile.publicProfileUrl = common.getValidUniqueName(profile.publicProfileUrl);

                    updateCurrentLocation(common.getClientIp2(req),profile._id);
                    if(msg.message != 'userExist'){
                        emailSenderObj.sendRegistrationConfirmationMail_new(profile);
                        if(common.checkRequired(profile.profilePicUrl) && profile.profilePicUrl.charAt(0) == 'h'){
                            storeProfileImg(profile.profilePicUrl,profile.publicProfileUrl,false,profile._id);
                        }
                    }
                    common.insertLog(profile,'mobile-new-profile',req);
                    //googleCalendar.getGoogleContactsEmailsEventsOffice365user(profile);
                    //common.makeFavoriteContacts(profile._id.toString(),profile.isFavoriteUpdated,timezone,profile.lastLoginDate);
                    //Error while creating a profile. Refering to old interaction collection. Redundant as emails etc are gotten after profile creation. 
                    var success = {
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":{
                            token:common.generateToken(MOBILE_TOKEN_SECRETE,profile._id),
                            profile:profile
                        }
                    };
                    res.send(success);
                }
            });
        }
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

function storeProfileImg(url, imageName, isLinkedinImage, userId) {
    if (url != 'images/default.png' && url != '/images/default.png') {
        request.get({url: url, encoding: 'binary'}, function (err, response, body) {
            if(!err && body != null && body != undefined && !common.contains(body,'html') && !common.contains(body,'title')){
                fs.writeFile('./public/profileImages/' + imageName, body, 'binary', function (err) {
                    if (err){
                        logger.info('Error in getting image from third party server ' + err);
                    }
                    else{
                        logger.info('Saving image in server success (image from thirdParty servers)');
                        if (isLinkedinImage) {
                            common.makeSquareImage('./public/profileImages/' + imageName, '/profileImages/' + imageName, 'jpg', false, function (isSuccess, path) {
                                userManagementObj.updateProfileImage(userId, path, function (error, isUpdated, msg) {
                                    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,profilePicUrl:1},function(err,pr){
                                        common.checkImageExtension(pr);
                                    })
                                });
                            })
                        }
                        else {
                            userManagementObj.updateProfileImage(userId, '/profileImages/' + imageName, function (error, isUpdated, msg) {

                            });
                        }
                    }
                });
            }
            else{
                logger.info('Error in getting image from third party mobile: authenticate:router ', err);
            }
        });
    }
}

router.post('/authenticate/authcode/exchange',function(req,res){
    if(common.checkRequired(req.body.code)){
        exchangeAuthCodeForToken(req.body.code,function(tokens){

            if(common.checkRequired(tokens)){
                tokens = JSON.parse(tokens)
            }

            if(common.checkRequired(tokens)){

                if(tokens.error){
                    res.send({
                        "SuccessCode": 0,
                        "Message": tokens.error_description || '',
                        "ErrorCode": 5000,
                        "Error":tokens.error || '',
                        "Data":{

                        }
                    });
                }
                else if(common.checkRequired(tokens.access_token)){
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":{
                            tokens:tokens
                        }
                    });
                }else res.send({
                        "SuccessCode": 0,
                        "Message": "Something Went Wrong",
                        "ErrorCode": 5000,
                        "Error":"SOMETHING_WENT_WRONG",
                        "Data":{}
                    });
            }else res.send({
                    "SuccessCode": 0,
                    "Message": "Something Went Wrong",
                    "ErrorCode": 5000,
                    "Error":"SOMETHING_WENT_WRONG",
                    "Data":{}
                });
        })
    }else res.send({
            "SuccessCode": 0,
            "Message": "Auth Code Not Found",
            "ErrorCode": 5000,
            "Error":"BAD_REQUEST",
            "Data":{}
        });
});


router.post('/profile/update/partial',function(req,res){

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)) {

        var emailId = req.body.emailId?req.body.emailId:null;

        if(emailId){
            userManagementObj.findUserProfileByEmailId(emailId,function (err,user) {

                if(!err && !user.partialFill){

                    res.send({
                        "SuccessCode": 0,
                        "ErrorCode": 1,
                        "Message": "PROFILE_ALREADY_EXISTS",
                        "Data": []
                    })

                } else {

                    req.body.partialFill = false;
                    req.body.registeredUser = true;

                    userManagementObj.updateUserProfile(req.body,function (result) {
                        if(result){
                            res.send({
                                "SuccessCode": 1,
                                "ErrorCode": 0,
                                "Message": "PROFILE_REGISTERED",
                                "Data": []
                            })
                        } else {
                            res.send({
                                "SuccessCode": 0,
                                "ErrorCode": 1,
                                "Message": "PROFILE_NOT_REGISTERED",
                                "Data": []
                            })
                        }

                    })
                }

            })

        } else {
            res.send({
                "SuccessCode": 0,
                "ErrorCode": 1,
                "Message": "PROFILE_NOT_REGISTERED",
                "Data": []
            })
        }
    } else {

        res.send({
            "SuccessCode": 0,
            "ErrorCode": 1,
            "Message": "BODY_NOT_FOUND",
            "Data": []
        })

    }
})

function exchangeAuthCodeForToken(code,callback){
      var authConfig = appCredential.getAuthCredentials();

      var url1 = 'https://accounts.google.com/o/oauth2/token';

      var payload = {
        grant_type: 'authorization_code',
        code: code,
        client_id: authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        redirect_uri:authConfig.GOOGLE_REDIRECT_URL_MOBILE
    };

    request.post(url1, { form: payload }, function(error, response, body) {
        callback(body)
    });

}

function updateCurrentLocation(IP,userId) {
    IPLocator.lookup(IP, userId, function (location) {

    });
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
}

module.exports = router;
