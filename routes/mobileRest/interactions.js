var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');
var _ = require("lodash");

var commonUtility = require('../../common/commonUtility');
var mobileAuth = require('../../common/mobileAuthValidation');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var interactions = require('../../dataAccess/interactionManagement');
var meetingClass = require('../../dataAccess/meetingManagement');
var taskManagementClass = require('../../dataAccess/taskManagementClass');

var errorObj = new errorClass();
var mobileAuthObj = new mobileAuth();
var common = new commonUtility();
var interactionObj = new interactions();
var validation = new validations();
var userManagementObj = new userManagement();
var errorMessagesObj = new errorMessages();
var meetingsObj = new meetingClass();
var taskManagementClassObj = new taskManagementClass();

var statusCodes = errorMessagesObj.getStatusCodes();

var MOBILE_TOKEN_SECRETE = 'azXSdcVF1032fvCDsxZA';

router.get('/interactions/topfive',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var date = moment().tz(timezone);
                var start = date.clone();
                var end = date.clone();

                start.date(start.date() - 7);

                var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

                interactionObj.interactionIndividualTopFive_new(userId,user.emailId,start.format(),end.format(),function(interactions){

                    if(common.checkRequired(interactions) && interactions.length > 0){
                        interactions = common.removeDuplicates_emailId(interactions);
                        if(common.checkRequired(req.query) && req.query.returndata == 'count'){
                            var count = 0;
                            var length = interactions.length > 5 ? 5 : interactions.length
                            for(var c=0; c<length; c++){
                                count += interactions[c].count
                            }
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{
                                    count:count
                                }
                            });
                        }
                        else{

                            var total = interactions.length;
                            var results = interactions.splice(skip,limit);
                            sendSuccessResponse(req,res,{  total:total,
                                skipped:skip,
                                limit:limit,
                                returned:results.length,
                                contacts:results});
                        }
                    }else sendSuccessResponse(req,res, { total:0,
                        skipped:skip,
                        limit:limit,
                        returned:0,
                        contacts:[]})
                })
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });

    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

/*router.get('/interactions/subtype/contact',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';

                    var dateMin = moment().tz(timezone);
                    var dateMax = moment().tz(timezone);

                    dateMin.date(dateMin.date()-15)
                    dateMin.hour(0)
                    dateMin.minute(0)
                    dateMin.second(0)
                    dateMin.millisecond(0)

                    dateMax.date(dateMax.date())
                    dateMin = dateMin.format()
                    dateMax = dateMax.format()

                    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
                    interactionObj.interactionWithContactBySubtypes(userId,req.query.id,dateMin,dateMax,function(interactions){
                        if(common.checkRequired(interactions) && interactions.length > 0){
                            var total = 0;
                            for(var i=0; i<interactions.length; i++){
                                total += interactions[i].count
                            }
                            sendSuccessResponse(req,res,{totalInteractions:total,interactionsCountByType:interactions});
                        }else
                            sendSuccessResponse(req,res,{totalInteractions:0,interactionsCountByType:[]});
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/interactions/last/contact',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';

                    var date = moment().tz(timezone);

                    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 1;
                    interactionObj.lastInteractionWithContact(userId,req.query.id,false,date.format(),limit,function(interactions){
                        if(common.checkRequired(interactions) && interactions.length > 0){
                            sendSuccessResponse(req,res,{interactions:interactions});
                        }else
                        sendSuccessResponse(req,res,{});
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/interactions/next/contact',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';

                    var date = moment().tz(timezone);

                    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
                    interactionObj.nextInteractionWithContact(userId,req.query.id,false,date.format(),function(interactions){
                        if(common.checkRequired(interactions) && interactions.length > 0){
                            sendSuccessResponse(req,res,{interaction:interactions[0]});
                        }else
                            sendSuccessResponse(req,res,{});
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});
*/
router.get('/tasks/pending/contact',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';

                    var date = moment().tz(timezone);

                    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
                   meetingsObj.getPendingToDoItemsWithContact(userId,req.query.id,date.format(),function(tasks){
                       if(tasks.length > 0){
                           tasks.sort(function(a,b){
                               return new Date(b.dueDate) - new Date(a.dueDate);
                           });
                           sendSuccessResponse(req,res,{total:tasks.length,tasks:tasks})
                       }else
                        sendSuccessResponse(req,res,{total:0,tasks:[]})
                   });
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

/*
router.get('/interactions/last/contact/call',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';

                    var date = moment().tz(timezone);

                    interactionObj.lastCallInteractionWithContact(userId,req.query.id,date.format(),function(interactions){
                        if(common.checkRequired(interactions) && interactions.length > 0){
                            sendSuccessResponse(req,res,{interaction:interactions[0]});
                        }else
                            sendSuccessResponse(req,res,{});
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});
*/

router.post('/interactions/today/meetings',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){

            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,location:1,currentLocation:1},function(error,user){
                if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';

                    var date = req.body.dateTime;

                    //emailSenderObj.sendUncaughtException('',req.body.dateTime,JSON.stringify(req.body),'TODAY_MEETINGS 1 '+user.emailId)
                    if(common.checkRequired(date)){
                        date = new Date(date).toISOString();
                    }
                    //emailSenderObj.sendUncaughtException('',date,JSON.stringify(req.body),'TODAY_MEETINGS 2 '+user.emailId)
                    var dateMin = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);
                    var dateMax = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);

                    dateMin.date(dateMin.date())
                    dateMin.hour(0)
                    dateMin.minute(0)
                    dateMin.second(0)
                    dateMin.millisecond(0)

                    dateMax.date(dateMax.date())
                    dateMax.hour(23)
                    dateMax.minute(59)
                    dateMax.second(59)
                    dateMax.millisecond(59)

                    dateMin = dateMin.format()
                    dateMax = dateMax.format()
                    //emailSenderObj.sendUncaughtException('',dateMin,dateMax,'TODAY_MEETINGS 3 '+user.emailId)
                    interactionObj.interactionsByDateMinMaxDashboard(userId,dateMin,dateMax,function(confirmedMeetings){
                        getPendingMeetingsByDate(userId,dateMin,dateMax,function(pendingMeetings){
                            getTodayTasks(userId,dateMin,dateMax,function(tasks){
                                var meetings = [];
                                if(common.checkRequired(confirmedMeetings) && confirmedMeetings.length > 0){
                                    pendingMeetings = pendingMeetings.concat(confirmedMeetings);

                                    pendingMeetings.sort(function (o1, o2) {
                                        return new Date(o1.scheduleTimeSlots[0].start.date) < new Date(o2.scheduleTimeSlots[0].start.date) ? -1 : new Date(o1.scheduleTimeSlots[0].start.date) > new Date(o2.scheduleTimeSlots[0].start.date) ? 1 : 0;
                                    });

                                    if(req.body.mobile){
                                        _.each(pendingMeetings,function (meeting) {
                                            if(!meeting.actionItemSlotType){
                                                meetings.push(meeting)
                                            }
                                        });
                                    } else {
                                        meetings = todayMeetings;
                                    }

                                    sendSuccessResponse(req,res,{meetings:meetings,tasks:tasks},true,user);
                                }
                                else{
                                    if(pendingMeetings.length > 0){
                                        pendingMeetings.sort(function (o1, o2) {
                                            return new Date(o1.scheduleTimeSlots[0].start.date) < new Date(o2.scheduleTimeSlots[0].start.date) ? -1 : new Date(o1.scheduleTimeSlots[0].start.date) > new Date(o2.scheduleTimeSlots[0].start.date) ? 1 : 0;
                                        });
                                    }

                                    if(req.body.mobile){
                                        _.each(pendingMeetings,function (meeting) {
                                            if(!meeting.actionItemSlotType){
                                                meetings.push(meeting)
                                            }
                                        });
                                    } else {
                                        meetings = todayMeetings;
                                    }
                                    
                                    sendSuccessResponse(req,res,{meetings:meetings,tasks:tasks},true,user);
                                }
                            });
                        });
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

function getPendingMeetingsByDate(userId,dateMin,dateMax,callback){
    meetingsObj.newInvitationsByDateTimezone(userId,new Date(dateMin),new Date(dateMax),function(meetings){
        if(common.checkRequired(meetings) && meetings.length > 0){
            callback(meetings)
        }else callback([])
    })
}

function getTodayTasks(userId,dateMin,dateMax,callback){
    taskManagementClassObj.getTasksForDateAll(userId,dateMin,dateMax,function(error,tasks){
        if(common.checkRequired(tasks) && tasks.length > 0){
            callback(tasks)
        }else callback([])
    })
}

function sendSuccessResponse(req,res,data,images,user){
    if(images){
        fs.readdir('./public/images/app_images/drawable-hdpi/', function(er,files){
             if(common.checkRequired(files) && files.length > 0){
                 var randomNum = common.getRandomNumber(0,files.length-1);

                 if(req.session.randomFileNum == randomNum){

                     if(randomNum < files.length-1){
                         randomNum += 1;
                     }
                     else if(randomNum == files.length-1 ){
                         randomNum = randomNum - 1;
                     }
                 }

                 req.session.randomFileNum = randomNum;
                 data.image = files[randomNum];
             }

                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":data
                });
        });
    }else
    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data":data
    });
}

/*router.get('/test/local/contact',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){

            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';

                    var dateMin = moment().tz(timezone);
                    dateMin.date(dateMin.date() - 180)
                    dateMin.hour(0)
                    dateMin.minute(0)
                    dateMin.second(0)
                    dateMin.millisecond(0)

                    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 1;
                    interactionObj.getMoreThanFiveInteractedEmails(userId,dateMin.format(),function(interactions){
                        res.send({count:interactions.length,interactions:interactions})
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/test/local/interaction/top/five/type/count',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){

        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var date = moment().tz(timezone);
                var start = date.clone();
                var end = date.clone();

                start.date(start.date() - 7);
                var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

                interactionObj.interactionIndividualTopFive_new_withTypeCount(userId,user.emailId,start.format(),end.format(),function(interactions){
                    if(common.checkRequired(interactions) && interactions.length > 0){
                        if(common.checkRequired(req.query) && req.query.returndata == 'count'){
                            var count = 0;
                            var length = interactions.length > 5 ? 5 : interactions.length
                            for(var c=0; c<length; c++){
                                count += interactions[c].count
                            }
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{
                                    count:count
                                }
                            });
                        }
                        else{

                            var total = interactions.length;
                            var results = interactions.splice(skip,limit);
                            sendSuccessResponse(req,res,{  total:total,
                                skipped:skip,
                                limit:limit,
                                returned:results.length,
                                contacts:results});

                        }
                    }else sendSuccessResponse(req,res, { total:0,
                        skipped:skip,
                        limit:limit,
                        returned:0,
                        contacts:[]})
                })
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});*/

/*
router.get('/interactions/subtype/contact/new',mobileAuthObj.isAuthenticatedMobile,function(req,res){
    var userId = common.decodeToken(req.headers.token,MOBILE_TOKEN_SECRETE);
    if(common.checkRequired(userId)){
        if(common.checkRequired(req.query.id)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';

                    var dateMin = moment().tz(timezone);
                    var dateMax = moment().tz(timezone);

                    dateMin.date(dateMin.date()-15)
                    dateMin.hour(0)
                    dateMin.minute(0)
                    dateMin.second(0)
                    dateMin.millisecond(0)

                    dateMax.date(dateMax.date())
                    dateMin = dateMin.format()
                    dateMax = dateMax.format()

                    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
                    interactionObj.lastInteractionsWithSubTypeCountOneUser(userId,req.query.id,false,dateMin,dateMax,function(interactions){
                        res.send(interactions);
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});
*/

module.exports = router;