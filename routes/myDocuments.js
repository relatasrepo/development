var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var fs = require('fs');
var request = require('request')
var moment = require('moment-timezone');
var AWS = require('aws-sdk');
var _ = require("lodash")

var userManagement = require('../dataAccess/userManagementDataAccess');
var validations = require('../public/javascripts/validation');
var emailSender = require('../public/javascripts/emailSender');
var massMailCountManagement = require('../dataAccess/massMailCountManagement');
var messageDataAccess = require('../dataAccess/messageManagement');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var officeOutlookApi = require('../common/officeOutlookAPI');
var meetingSupportClass = require('../common/meetingSupportClass');
var googleCalendarAPI = require('../common/googleCalendar');
var Gmail = require('../common/gmail');
var contactClass = require('../dataAccess/contactsManagementClass');
var companyModelClass = require('../dataAccess/corporateDataAccess/companyModelClass');
var appCredentials = require('../config/relatasConfiguration');
var interactions = require('../dataAccess/interactionManagement');
var errorClass = require('../errors/errorClass');
var errorMessages = require('../errors/errorMessage');

var contactObj = new contactClass();
var companyCollection = new companyModelClass();
var meetingSupportClassObj = new meetingSupportClass();
var common = new commonUtility();
var userManagements = new userManagement();
var emailSenders = new emailSender();
var officeOutlook = new officeOutlookApi();
var massMailCountManagements = new massMailCountManagement();
var messageAccess = new messageDataAccess();
var appCredential = new appCredentials();
var validation = new validations();
var interactionObj = new interactions();
var gmailObj = new Gmail();
var googleCalendar = new googleCalendarAPI();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();

var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

var logLib = new winstonLog();
var logger =logLib.getWinston()

router.get('/docs/old',isLoggedInUser,common.checkUserDomain,function(req,res){
      try{
          logger.info('User entered in to my documents page');
          res.render('myDocuments');
      }
      catch (ex){
          logger.info('Error occurred while rendering my documents page '+ex);
      }
});

router.get('/analytics/:docId/:userId',isLoggedInUser,function(req,res){

    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for analytics';

    var docId = req.params.docId;
    var userId = req.params.userId;
    userManagements.findAnalytics(docId,userId,function(error,document,message){
        userLogInfo.functionName = 'findAnalytics in analytics router';
        if(error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in finding document analytics';
            logger.error('Error in mongo',userLogInfo);
            res.send(false)
        }
        else
        if(document == false || document == null){
            res.send(false)
        }
        else{
        document.sharedWith.forEach(function(shared){

                if(shared.userId == userId){
                    if(shared.accessInfo.pageReadTimings[0]){
                        var pageInfo = shared.accessInfo.pageReadTimings;

                        res.send(shared);
                    }
                    else{
                        res.send(false)
                    }
                }
        });
        }
    });
});

router.get('/analytics/v2/:docId/:objId',common.isLoggedInUserOrMobile,function(req,res){

    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for analytics';

    var docId = req.params.docId;
    var sharedUserObjId = req.params.objId;

    userManagements.findAnalyticsBySharedUserObjId(docId,sharedUserObjId,function(error,document,message){
        userLogInfo.functionName = 'findAnalytics in analytics router';
        if(error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in finding document analytics';
            logger.error('Error in mongo',userLogInfo);

            res.send({
                SuccessCode: 0,
                ErrorCode: 1, 
                Message: message.message
            })
        }
        else if(document == false || document == null){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1, 
                Message: 'Document is null'
            })
        }
        else{
            document.sharedWith.forEach(function(shared){
                if(shared._id == sharedUserObjId){
                    if(shared.accessInfo.pageReadTimings[0]){
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0, 
                            Analytics: shared
                        })
                    }
                    else{
                        res.send({
                            SuccessCode: 0,
                            ErrorCode: 1, 
                            Message: 'Document is not Accessed'
                        })
                    }
                }
        });
        }
    });
});

router.get('/analytics/:docId/byEmail/:emailId',common.isLoggedInUserToGoNext,function(req,res){

    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for analytics';

    var docId = req.params.docId;
    var emailId = req.params.emailId;
    userManagements.findAnalytics_email(docId,emailId,function(error,document,message){
        userLogInfo.functionName = 'findAnalytics in analytics router';
        if(error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in finding document analytics';
            logger.error('Error in mongo',userLogInfo);
            res.send(false)
        }
        else
        if(document == false || document == null){
            res.send(false)
        }
        else{
            document.sharedWith.forEach(function(shared){

                if(shared.emailId == emailId){
                    if(shared.accessInfo.pageReadTimings[0]){
                        //var pageInfo = shared.accessInfo.pageReadTimings;

                        res.send(shared);
                    }
                    else{
                        res.send(false)
                    }
                }
            });
        }
    });
});

router.get('/changeAccess/:docId/:userId/:status',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for changeAccess to document';

    var docId = req.params.docId;
    var userId = req.params.userId;
    var status =req.params.status;

    userManagements.updateDocAccess(docId,userId,status,function(error,isUpdated,message){
        userLogInfo.functionName = 'updateDocAccess in changeAccess router';
        if(error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in updating document access';
            logger.error('Error in mongo',userLogInfo);
            res.send(isUpdated);
        }
        else{
            res.send(isUpdated);
        }

    });
});

router.get('/change/access/status/:docId/:objId/:status',common.isLoggedInUserOrMobile,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for changeAccess to document';

    var docId = req.params.docId;
    var objId = req.params.objId;
    var status =req.params.status;

    userManagements.updateDocAccessBySharedWithObjId(docId,objId,status,function(error,isUpdated,message){
        userLogInfo.functionName = 'updateDocAccess in changeAccess router';
        if(error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in updating document access';
            logger.error('Error in mongo',userLogInfo);
            res.send(isUpdated);
        }
        else{
            res.send(isUpdated);
        }

    });
});

router.get('/deleteDocument/:docId/:awsKey',common.isLoggedInUserOrMobile, function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for deleteDocument';
    var docId = req.params.docId;
    var awsKey = req.params.awsKey;
    var awsCredentials = appCredential.getAwsCredentials()
    var s3bucket;

    AWS.config.update(awsCredentials);
    AWS.config.region = awsCredentials.region;
    s3bucket = new AWS.S3({params: {Bucket: awsCredentials.bucket}});

    var params = {
        Bucket: awsCredentials.bucket,
        Key: awsKey
    }

    s3bucket.deleteObject(params, function(error, data) {
        if(error) {
            userLogInfo.error = error;
        }
    })

    userManagements.deleteDocument(docId,function(error,isDeleted,message){
        userLogInfo.functionName = 'deleteDocument in deleteDocument router';
        if(error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in deleting document';
            logger.error('Error in mongo',userLogInfo);
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Error in deleting document"
            });
        }
        else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Message: "Document Deleted Successfully"
            });
        }
    })
});

router.get('/documents/sharedWithMeNonRead',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.documentsSharedWithMeNonRead(userId,function(error,docs,msg){
        res.send(docs);
    });
});

router.get('/documents/sharedWithMeNonRead/count',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.documentsSharedWithMeNonReadCount(userId,function(count){

        res.send(count+'');
    });
});

router.get('/documents/sharedWithMeNonRead/count/:userId',function(req,res){

    var userId = req.params.userId;

    userManagements.documentsSharedWithMeNonReadCount(userId,function(count){
        res.send(count+'');
    });
});

router.get('/documents/sharedDocRed',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.sharedDocRed(userId,function(error,docs,msg){
        res.send(docs);
    });
});

router.get('/documents/sharedDocRed/count',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.sharedDocRedCount(userId,function(count){
        res.send(count+'');
    });
});

router.get('/documents/sharedDocRed/count/:userId',function(req,res){

    var userId = req.params.userId;

    userManagements.sharedDocRedCount(userId,function(count){
        res.send(count+'');
    });
});

router.post('/documents/updateAccessed',function(req,res){

    var userId = req.body.userId;
    var docId = req.body.docId;
    userManagements.updateAccessed(docId,userId,function(error,info){

        res.send(true)
    });
});

router.post('/shareDocument',isLoggedInUser,function(req,res){

    var shareEmails = req.body.shareEmails;
    shareDocument(req,res,shareEmails,true);

});

router.post('/share/document',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    var sharedWith = {
                        emailId: req.body.shareWithEmail,
                        firstName: req.body.firstName,
                        lastName: req.body.lastName,
                        sharedOn: req.body.sharedOn,
                        accessStatus: true,
                        message: req.body.userMsg,
                        subject: req.body.emailSubject,
                        documentPassword: common.getRandomString(6)
                    };

    if(req.body.expiryDate)
        sharedWith.expiryDate = req.body.expiryDate;

    userManagements.shareDocumentV2(req.body.documentId, sharedWith, function(error, result, msg) {
        if(result) {
            userManagements.getDocumentSharedWithDetails(req.body.documentId, sharedWith.emailId, function(error, sharedDocument, msg) {
                if(error) {
                    sendDocSharingStatus(false, "Error in getting shared document", res);
                } else {
                    userManagements.findUserProfileByIdWithCustomFields(userId, {companyId:1}, function(error, user) {
                        if(error) {
                            sendDocSharingStatus(false, "Error in fetching user profile", res);

                        } else {
                            companyCollection.findCompanyByIdWithProjection(user.companyId, {url:1}, function(companyInfo) {
                                
                                sendEmailWithDocLink(userId, sharedDocument[0], companyInfo.url, function(response) {
                                    if(response.SuccessCode) {
                                        
                                    }
                                    res.send(response);
                                })
                            })
                        }
                    })
                }
            });

        } else {
            sendDocSharingStatus(false, "Error in sharing document", res);
        }
    })
});

router.post('/resend/document', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    var documentId = req.body.documentId;
    var sharedWithEmailId = req.body.sharedWithEmailId;

    userManagements.getDocumentSharedWithDetails(documentId, sharedWithEmailId, function(error, sharedDocument, msg) {
        if(error) {
            sendDocSharingStatus(false, "Error in getting shared document", res);
        } else {
                userManagements.findUserProfileByIdWithCustomFields(userId, {companyId:1}, function(error, user) {
                    if(error) {
                        sendDocSharingStatus(false, "Error in fetching user profile", res);

                    } else {
                        companyCollection.findCompanyByIdWithProjection(user.companyId, {url:1}, function(companyInfo) {
                            
                            sendEmailWithDocLink(userId, sharedDocument[0], companyInfo.url, function(response) {
                                res.send(response);
                            })
                        })
                    }
                })
            }
    });
});

router.post('/setDocExpiry',common.isLoggedInUserOrMobile,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for set document expiry';
    var expObj = req.body

    userManagements.updateDocExpiry(expObj,function(error,isUpdated,message){
        userLogInfo.functionName = 'setting document expiry in setDocExpiry router';
        if(error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in updating document expiry';
            logger.error('Error in mongo',userLogInfo);
            res.send(isUpdated);
        }
        else {
            res.send(isUpdated);
        }
    })
});

router.get('/getDoc/:docId',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for get doc by id expiry';
        var docId = req.params.docId;

    if(checkRequired(docId)){
        userManagements.findDocumentById(docId,function(error,document,message){
            userLogInfo.functionName = 'find document by id in /getDoc/:docId router';
            if(error){
                userLogInfo.error = error;
                userLogInfo.info = 'Error in finding doc by id';
                logger.error('Error in mongo',userLogInfo);
                res.send(false);
            }
            else {
                res.send(document);
            }
        })
    }
})

function sendDocSharingStatus(isSuccess, message, res) {
    if(isSuccess) {
        res.send({
            SuccessCode: 1,
            ErrorCode: 0,
        })
    } else {
        res.send({
            SuccessCode: 0,
            ErrorCode: 1,
            Message: message
        })
    }
}

function sendEmailWithDocLink(userId, sharedDocument, companyUrl, callback) {
    var documentUrl = 'https://'+companyUrl + '/readDocument/'+ sharedDocument._id+'/'+sharedDocument.sharedWith[0]._id;
    var pdfIconUrl = 'https://'+companyUrl + '/images/pdf_icon.png'

    var emailObj = {
        email_cc: null,
        receiverEmailId: sharedDocument.sharedWith[0].emailId,
        receiverName: sharedDocument.sharedWith[0].firstName + ' ' + sharedDocument.sharedWith[0].lastName,
        message: sharedDocument.sharedWith[0].message,
        subject: sharedDocument.sharedWith[0].subject,
        docTrack: true,
        trackViewed: true,
        remind: null,
        respSentTime: moment().format(),
        isLeadTrack: false,
        newMessage: true,
        remind: false
    }
    
    emailObj.message = emailObj.message +
    "<div style='margin-top:20px;'><div style='display:inline-block; margin-right: 20px;'> " +
        "<a href=" + documentUrl +" >" +
            "<img src=" + pdfIconUrl + " style='display:inline-block;width:40px'/> " +
        "</a>" +
    "</div> " +
    "<div style='display:inline-block; width:80%'>" +
        // "<p style='margin:0px'><b>" + $scope.sharedDocument.documentName + "</b></p>" +
        "<p style='margin:0px'> " + 
            "<a style='text-decoration:none; color:#222; font-weight: bold;' href=" + documentUrl +" >" +
                "<span>" + sharedDocument.documentName + "</span>" +
            "</a> </p>" +
        "<p style='margin:0px'><b>Link: </b>" + documentUrl + "</p>" +
        "<p style='margin:0px'><b>Security Code: </b>" + 
            sharedDocument.sharedWith[0].documentPassword; +
        "</p>" +
    "</div></div>";

    sendEmailSingleUser(userId, emailObj, callback);

}

function shareDocument(req,res,email,responseFlag){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for shareDocument';
    var validate = new validations();

    var docId = req.body.docId;
    var shareEmails = email;
    var userMsg = req.body.userMsg;
    var sharedOn = req.body.sharedOn;
    var personName = req.body.personName || '';
    var subject = req.body.subject;
    var type = req.body.relatasContact;
    var resend = req.body.resend || false;
    var createMsg = req.body.createMsg || false;
    var headerImage = req.body.headerImage || false;
    var imageUrl = req.body.imageUrl;
    var imageName = req.body.imageName;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var message = '';
    if(checkRequired(userMsg)){
        message = userMsg;
    }

    var expiryDate = req.body.expiryDate == 'noDate' ? false :  req.body.expiryDate;
    var expSec = req.body.expirySec == 'noSec'? false : req.body.expirySec;

    // shareEmails +=',abc';

    //var emails = shareEmails.split(',');
    var emails = shareEmails;
    var shareDetails = {
        docId:docId,
        emails:[]
    };
    var nonUser;

    // for (var i=0;i<emails.length-1;i++){
    validate.validateEmail(emails,function(isValid){

        if(isValid){
            userManagements.findUserProfileByEmailIdWithCustomFields(emails,{emailId:1,firstName:1,lastName:1,mobileNumber:1},function(error,profile){

                if(error){
                    if(responseFlag == true){
                        res.send(false);
                    }
                }else
                if(profile != null || profile != undefined){

                    if(expiryDate == false){
                        shareDetails.emails.push({
                            userId:profile._id,
                            emailId:profile.emailId,
                            firstName:profile.firstName+' '+profile.lastName,
                            sharedOn:sharedOn,
                            accessStatus:true,
                            message:message,
                            subject:subject
                        });
                    }else{
                        shareDetails.emails.push({
                            userId:profile._id,
                            emailId:profile.emailId,
                            firstName:profile.firstName+' '+profile.lastName,
                            expiryDate:expiryDate,
                            expirySec:expSec,
                            sharedOn:sharedOn,
                            accessStatus:true,
                            message:message,
                            subject:subject
                        });
                    }

                    var contact = {
                        personId:profile._id,
                        personName:profile.firstName+' '+profile.lastName,
                        personEmailId:profile.emailId,
                        count:1,
                        lastInteracted:new Date(),
                        addedDate: new Date(),
                        verified:true,
                        relatasContact:true,
                        relatasUser:true,
                        mobileNumber:profile.mobileNumber
                    }
                   // updateContact(userId,contact);
                    setTimeout(function(){

                        var sharedWithDetails = shareDetails;
                        var message2 = message;
                        userManagements.shareDocument(shareDetails,function(error,isSuccess,msg){
                            userLogInfo.functionName = 'shareDocument in shareDocument router';
                            if(error || isSuccess == false){
                                userLogInfo.error = error;
                                userLogInfo.info = 'Error in sharing document';
                                logger.error('Error in mongo',userLogInfo);
                                if(responseFlag == true) {
                                    res.send(isSuccess);
                                }
                            }
                            else{
                                userManagements.findDocumentById(docId,function(error,doc,me){
                                    var interaction = {};
                                    interaction.userId = userId;
                                    interaction.action = 'sender';
                                    interaction.type = 'document-share';
                                    interaction.subType = 'document-share';
                                    interaction.refId =  docId;
                                    interaction.source = 'relatas';
                                    interaction.description = message || '';

                                    var interactionReceive = {};
                                    interactionReceive.userId = contact.personId || '';
                                    interactionReceive.action = 'receiver';
                                    interactionReceive.emailId = contact.personEmailId;
                                    interactionReceive.type = 'document-share';
                                    interactionReceive.subType = 'document-share';
                                    interactionReceive.refId = docId;
                                    interactionReceive.source = 'relatas';
                                    interactionReceive.description = message || '';

                                    if(!error && doc){
                                        interaction.title = doc.documentName;
                                        interactionReceive.title = doc.documentName;
                                    }
                                    //common.storeInteractionReceiver(interactionReceive);
                                    //common.storeInteraction(interaction);
                                    meetingSupportClassObj.storeAcceptMeetingInteractions(interaction,interactionReceive)
                                });


                                //if(isSuccess != 'exist'){
                                    updateContact(userId,contact);
                                updateReceiverContacts(userId,contact.personId);
                                if(resend || createMsg){
                                    var messageObj = {
                                        senderId:userId,
                                        receiverId:contact.personId || '',
                                        receiverEmailId:contact.personEmailId,
                                        subject:subject,
                                        type: resend ? 'resendDoc' : 'messageDoc',
                                        headerImage:headerImage,
                                        imageUrl:imageUrl,
                                        imageName:imageName,
                                        message:message2
                                    }
                                    messageAccess.createMessage(messageObj,function(message){
                                        if(message){

                                            var interaction = {};
                                            interaction.userId = message.senderId;
                                            interaction.action = 'sender';
                                            interaction.type = 'message';
                                            interaction.subType = 'message';
                                            interaction.refId = message._id;
                                            interaction.source = 'relatas';
                                            interaction.title = message.subject;
                                            interaction.description = message.message;
                                        // common.storeInteraction(interaction);

                                            var interactionReceiver = {};
                                            interactionReceiver.userId = message.receiverId || '';
                                            interactionReceiver.emailId = message.receiverEmailId;
                                            interactionReceiver.action = 'receiver';
                                            interactionReceiver.type = 'message';
                                            interactionReceiver.subType = 'message';
                                            interactionReceiver.refId = message._id;
                                            interactionReceiver.source = 'relatas';
                                            interactionReceiver.title = message.subject;
                                            interactionReceiver.description = message.message;
                                        // common.storeInteractionReceiver(interactionReceiver);
                                            meetingSupportClassObj.storeAcceptMeetingInteractions(interaction,interactionReceiver)
                                        }
                                    });
                                }
                                //}
                                userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1},function(error,profile){
                                    if(!error && profile != null){
                                        var dataToSend = {
                                            emailId:sharedWithDetails.emails[0].emailId,
                                            receivedByFirstName:sharedWithDetails.emails[0].firstName,
                                            url:'/readDocument/'+docId,
                                            sharedByEmail:profile.emailId,
                                            sharedByFirstName:profile.firstName+' '+profile.lastName,
                                            userMsg:userMsg,
                                            subject:subject,
                                            headerImage:headerImage,
                                            imageUrl:imageUrl,
                                            imageName:imageName

                                         }
                                            if(req.body.mmcCount){
                                                massMailCountManagements.incrementMassMailCount(userId,1,function(error,isUpdated){

                                                })
                                            }

                                        emailSenders.sendShareDocMailToRelatasUser(dataToSend)
                                        if(responseFlag == true) {
                                            res.send(isSuccess);
                                        }

                                    }
                                    else{
                                        if(responseFlag == true) {
                                            res.send(isSuccess);
                                        }
                                    }
                                })

                            }
                        });
                    },100)
                }
                else{

                    nonUser = emails;
                    if(expiryDate == false){
                        shareDetails.emails.push({
                            emailId:emails,
                            firstName:personName || '',
                            sharedOn:sharedOn,
                            accessStatus:true,
                            message:message,
                            subject:subject
                        });
                    }else{
                        shareDetails.emails.push({
                            emailId:emails,
                            expiryDate:expiryDate,
                            firstName:personName || '',
                            expirySec:expSec,
                            sharedOn:sharedOn,
                            accessStatus:true,
                            message:message,
                            subject:subject
                        });
                    }
                    var contact2;
                   if(checkRequired(personName)){
                       var relatasContact = false;
                       if(type == 'true' || type == true){
                           relatasContact = true;
                       }
                        contact2 = {
                           personId:'',
                           personName:personName,
                           personEmailId:emails || '',
                           count:1,
                           lastInteracted:new Date(),
                           addedDate: new Date(),
                           verified:true,
                           relatasContact:relatasContact,
                           relatasUser:false
                       }
                   }
                    sendMailToNonRelatasUser(shareDetails,userId,docId,userMsg,req,res,userLogInfo,responseFlag,subject,contact2,resend,createMsg,headerImage,imageUrl,imageName)
                }
            });
        }
        else{
            if(responseFlag == true) {
                res.send(false);
            }
        }
    });

    //  }
}

function sendEmailSingleUser(userId, reqBody, callback) {

    if(checkRequired(reqBody) && validation.isEmptyObject(reqBody)) {
        var status = validation.validateSendMessageFields(reqBody);
        if (status.status == 5000) {
            logger.info('Email Validation error status 5000',status,reqBody);
            callback(errorObj.generateErrorResponse(status,errorMessagesObj.getMessage("SEND_EMAIL_FAILED")))
        }
        else {
            var data = reqBody;

            userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1,outlook:1},function(error,user){

                if(checkRequired(user)){

                    if(user.google.length == 0 && user.outlook.length>0){
                        _.each(user.outlook,function (outlookObj) {
                            if(user.emailId == outlookObj.emailId){
                                sendOutlookMail(outlookObj.refreshToken,data,user,callback);
                            }
                        });

                    } else {

                        var dataToSend = {
                            email_cc: data.email_cc,
                            receiverEmailId: data.receiverEmailId,
                            receiverId: checkRequired(data.receiverId) ? data.receiverId : '',
                            receiverName: data.receiverName,
                            senderId: user._id,
                            senderEmailId: user.emailId,
                            senderName: user.firstName+' '+user.lastName,
                            message: data.message,
                            subject: data.subject,
                            trackViewed: true, //data.trackViewed,
                            docTrack: data.docTrack,
                            remind: data.remind,
                            refId: data.refId,
                            updateReplied: data.updateReplied || false
                        };

                        sendEmail(dataToSend,user,callback);
                    }
                    //Update respSentTime
                    if(checkRequired(data.respSentTime)){
                        contactObj.updateResponseTimeStamp(userId,data.receiverEmailId,data.respSentTime,data.isLeadTrack);
                    }

                    //Add email IDs to contact list after getting the receiver's UserID. Using for Prospect Leads feature

                    if(checkRequired(data.assignedTimeISO)){
                        userManagements.getUserByEmail(data.receiverEmailId,function(err,relatasProfile){

                            var contactUserId = relatasProfile._id;

                            for(var i=0;i<data.prospectEmailLeads.length;i++){

                                var obj = {};
                                obj.personEmailId = data.prospectEmailLeads[i];
                                obj.notificationStartTime = data.assignedTimeISO;
                                obj.respLimit = data.deadLineISO;
                                obj.contactRelation = {prospect_customer:'prospect'};
                                obj.hashtagThis = true;

                                contactObj.addSingleContact(contactUserId,obj);
                            }

                        });
                    }

                }else callback(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("SEND_EMAIL_FAILED")));
            });
        }
    }
    else callback(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("SEND_EMAIL_FAILED")));
} 

function sendOutlookMail(refreshToken,mailBody,user,callback) {

    var mail = {
        "Comment": mailBody.message
    }

    officeOutlook.getNewAccessTokenWithoutStore(refreshToken,function (newToken) {

        if(mailBody.newMessage){
            sendNewOutlookMail(newToken.access_token,mailBody,user,callback)
        } else {

            mail = {
                "Comment": mailBody.originalBody
            }

            var id = mailBody.refId?mailBody.refId:mailBody.id;

            officeOutlook.createReplyEmailMSGraphAPI(newToken.access_token,mail,id,function (error,response) {

                if(!error && response){

                    var draftMessage = JSON.parse(response);

                    var content = "<html><body><div>"+mailBody.originalBody+"</div></body></html>" +draftMessage.body.content
                    var recipients = [];

                    if(mailBody.email_cc && mailBody.email_cc.length>0){

                        _.each(mailBody.email_cc,function (emailId) {
                            
                            recipients.push({
                                "EmailAddress": {
                                    "Address": emailId.trim().toLowerCase(),
                                }
                            })
                        });
                    }

                    var updateDraft = {
                        "body": {
                            "contentType": "html",
                            "content": content
                        },
                        "ccRecipients":recipients
                    }

                    officeOutlook.updateDraftMSGraphAPI(newToken.access_token,draftMessage.id,updateDraft,function (err,updateRes) {

                        officeOutlook.sendDraftMSGraphAPI(newToken.access_token,draftMessage.id,function (errSend,sent) {
                            if(!errSend && response){

                                setTimeout(function(){
                                    updateOutlookMailsMSGraphAPI(newToken.access_token,user);
                                },2000)

                                callback({
                                    "SuccessCode": 1,
                                    "Message": errorMessagesObj.getMessage("SEND_EMAIL_SUCCESS"),
                                    "ErrorCode": 0,
                                    "Data": {}
                                });
                            } else {
                                logger.info('Error send Email ',error,response);
                                callback({
                                    "SuccessCode": 0,
                                    "Message": errorMessagesObj.getMessage("SEND_EMAIL_FAILED"),
                                    "ErrorCode": 1,
                                    "Data": {}
                                });
                            }
                        })
                    })
                } else {
                    logger.info('Error send Email ',error,response);
                    callback({
                        "SuccessCode": 0,
                        "Message": errorMessagesObj.getMessage("SEND_EMAIL_FAILED"),
                        "ErrorCode": 1,
                        "Data": {}
                    });
                }
            });
        }
    });
};

function updateOutlookMailsMSGraphAPI(refreshToken,user) {
    officeOutlook.getEmailsMSGraphAPI(refreshToken,user,function (mails) {
        officeOutlook.getMailsAsInteractions(mails, user, function (interactions, emailIdArr, referenceIds) {
            if(interactions && interactions.length>0){
                officeOutlook.mapInteractionsEmailIdToRelatasUsers(interactions,emailIdArr,function (mappedInteractions) {

                    if(mappedInteractions && mappedInteractions.length>0){
                        interactionObj.updateEmailInteractions(user,mappedInteractions,referenceIds,function(isSuccess){

                            if(isSuccess){
                                logger.info("Emails successfully saved for -",user.emailId)
                            } else {
                                logger.info("Emails unsuccessfully saved for -",user.emailId)
                            }
                        });

                        //Update these interactions's 2nd party as contacts if they don't already exist.
                        addInteractedContacts(user,mappedInteractions);
                    }
                });
            }
        });
    });
}

function sendNewOutlookMail(refreshToken,mailBody,user,callback) {

    var recipients = []
    if(mailBody.email_cc && mailBody.email_cc.length>0){

        _.each(mailBody.email_cc,function (emailId) {
            recipients.push({
                "EmailAddress": {
                    "Address": emailId
                }
            })
        });
    }
    
    var mail = {
        "Message": {
            "Subject": mailBody.subject,
            "Body": {
                "ContentType": "html",
                "Content": mailBody.message
            },
            "ToRecipients": [
                {
                    "EmailAddress": {
                        "Address": mailBody.receiverEmailId
                    }
                }
            ],
            "ccRecipients":recipients
        },
        "SaveToSentItems": "true"
    };

    officeOutlook.sendEmailMSGraphAPI(refreshToken,mail,function (error,response) {

        if(!error && response){

            setTimeout(function(){
                updateOutlookMailsMSGraphAPI(refreshToken,user);
            },2000)

            callback({
                "SuccessCode": 1,
                "Message": errorMessagesObj.getMessage("SEND_EMAIL_SUCCESS"),
                "ErrorCode": 0,
                "Data": {}
            });
        } else {
            logger.info('Error send Email ',error,response);
            callback({
                "SuccessCode": 0,
                "Message": errorMessagesObj.getMessage("SEND_EMAIL_FAILED"),
                "ErrorCode": 1,
                "Data": {}
            });
        }
    });
}

function sendEmail(dataToSend,user,callback){

    if(googleCalendar.validateUserGoogleAccount(user)){
        gmailObj.sendEmail(user._id,user.google[0].token,user.google[0].refreshToken,user.google[0].emailId,dataToSend,function(error,response){
            if(error){
                logger.info('Error send Email ',error,response,dataToSend);

                if(callback){
                    callback({
                        "SuccessCode": 0,
                        "Message": errorMessagesObj.getMessage("SEND_EMAIL_FAILED"),
                        "ErrorCode": 1,
                        "Data": {}
                    });
                }

            }
            else{
                if(dataToSend.updateReplied){
                    var obj = {
                        emailId1:dataToSend.receiverEmailId,
                        emailId2:dataToSend.senderEmailId,
                        refId:dataToSend.refId,
                        interactionDate:new Date()
                    };
                    interactionObj.updateInteractionReplied(obj);
                }

                if(callback){
                    callback({
                        "SuccessCode": 1,
                        "Message": errorMessagesObj.getMessage("SEND_EMAIL_SUCCESS"),
                        "ErrorCode": 0,
                        "Data": {}
                    });
                }
            }
        })
    }
    else callback(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_GOOGLE_ACCOUNTS'},errorMessagesObj.getMessage("SEND_EMAIL_FAILED")));
}

//Function to add Contact
function updateContact(userId,contact){
    contactObj.addSingleContact(userId,contact)
}

function sendMailToNonRelatasUser(shareDetails,userId,docId,userMsg,req,res,userLogInfo,responseFlag,subject,contact,resend,createMsg,headerImage,imageUrl,imageName){

        // var userId = emailArr[0].userId;

    userManagements.shareDocument(shareDetails,function(error,isSuccess,message){
        userLogInfo.functionName = 'shareDocument fun in sendMailToNonRelatasUser fun ';
        if(error || isSuccess == false){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in sharing document';
            logger.error('Error in mongo',userLogInfo);
            if(responseFlag == true) {
                res.send(isSuccess);
            }
        }
        else{
           // if(isSuccess != 'exist'){
           //     if(checkRequired(contact)){
            userManagements.findDocumentById(docId,function(error,doc,message){
                var interaction = {};
                interaction.userId = userId;
                interaction.action = 'sender';
                interaction.type = 'document-share';
                interaction.subType = 'document-share';
                interaction.refId =  docId;
                interaction.source = 'relatas';
                interaction.description = userMsg;

                var interactionReceive = {};
                interactionReceive.userId = contact.personId || '';
                interactionReceive.action = 'receiver';
                interactionReceive.emailId = contact.personEmailId;
                interactionReceive.type = 'document-share';
                interactionReceive.subType = 'document-share';
                interactionReceive.refId = docId;

                if(!error && doc){
                    interaction.title = doc.documentName;
                    interactionReceive.title = doc.documentName;
                 }
               // common.storeInteraction(interaction);
               // common.storeInteractionReceiver(interactionReceive);
                meetingSupportClassObj.storeAcceptMeetingInteractions(interaction,interactionReceive)
            });


            updateContact(userId,contact);
            if(resend || createMsg){
                var message = {
                    senderId:userId,
                    receiverId:contact.personId || '',
                    receiverEmailId:contact.personEmailId,
                    subject:subject,
                    type: resend ? 'resendDoc' : 'messageDoc',
                    headerImage:headerImage,
                    imageUrl:imageUrl,
                    imageName:imageName,
                    message:userMsg
                }
                messageAccess.createMessage(message,function(message){
                    if(message){

                            var interaction = {};
                            interaction.userId = message.senderId;
                            interaction.action = 'sender';
                            interaction.type = 'message';
                            interaction.subType = 'message';
                            interaction.refId = message._id;
                            interaction.source = 'relatas';
                            interaction.title = message.subject;
                            interaction.description = message.message;
                        //  common.storeInteraction(interaction);

                        var interactionReceiver = {};
                        interactionReceiver.userId = message.receiverId || '';
                        interactionReceiver.emailId = message.receiverEmailId;
                        interactionReceiver.action = 'receiver';
                        interactionReceiver.type = 'message';
                        interactionReceiver.subType = 'message';
                        interactionReceiver.refId = message._id;
                        interactionReceiver.source = 'relatas';
                        interactionReceiver.title = message.subject;
                        interactionReceiver.description = message.message;
                    // common.storeInteractionReceiver(interactionReceiver);
                        meetingSupportClassObj.storeAcceptMeetingInteractions(interaction,interactionReceiver)
                    }
                });
            }
            //  }
        // }
            userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1},function(error,profile){
                if(!error && profile != null){

                    var dataToSend = {
                        emailId:shareDetails.emails[0].emailId,
                        url:'accessDocByEmail/'+shareDetails.emails[0].emailId+'/'+docId,
                        sharedWithName:shareDetails.emails[0].firstName,
                        sharedByEmail:profile.emailId,
                        sharedByFirstName:profile.firstName+' '+profile.lastName,
                        userMsg:userMsg,
                        subject:subject,
                        headerImage:headerImage,
                        imageUrl:imageUrl,
                        imageName:imageName
                    }

                    if(req.body.mmcCount){
                        massMailCountManagements.incrementMassMailCount(userId,1,function(error,isUpdated){

                        })
                    }

                    emailSenders.sendShareDocMailToNonRelatasUser(dataToSend)
                    if(responseFlag == true) {
                        res.send(true)
                    }
                }
            });
        }
    })
}

function updateReceiverContacts(senderId,receiverId){

    userManagements.findUserProfileByIdWithCustomFields(senderId,{emailId:1,firstName:1,lastName:1,mobileNumber:1},function(error,profile) {
        if(error){

        }else
        if(profile != null || profile != undefined){

            var contact1 = {
                personId:profile._id,
                personName:profile.firstName+' '+profile.lastName,
                personEmailId:profile.emailId,
                count:1,
                lastInteracted:new Date(),
                addedDate: new Date(),
                verified:true,
                relatasContact:true,
                relatasUser:true,
                mobileNumber:profile.mobileNumber
            }
            updateContact(receiverId,contact1);
        }
    });
}

function addInteractedContacts(userProfile,mappedInteractions) {

    var interactedContacts = [];

    _.each(mappedInteractions,function (interaction) {

        var personName;
        if(interaction.firstName && interaction.lastName){
            personName = interaction.firstName+' '+interaction.lastName
        } else if(interaction.firstName){
            personName = interaction.firstName
        }else if(interaction.lastName){
            personName = interaction.lastName
        }

        if(interaction.emailId) {
            var contactObj = {
                personId: interaction.userId ? interaction.userId.toString() : null,
                personName: personName,
                personEmailId: interaction.emailId.toLowerCase(),
                birthday: null,
                companyName: interaction.companyName ? interaction.companyName : null,
                designation: null,
                addedDate: new Date(),
                verified: false,
                relatasUser: interaction.userId ? true : false,
                relatasContact: true,
                mobileNumber: interaction.mobileNumber ? interaction.mobileNumber : null,
                location: interaction.location ? interaction.location : null,
                source: 'outlook-email-interactions',
                account: {
                    name: common.fetchCompanyFromEmail(interaction.emailId),
                    selected: false,
                    updatedOn: new Date()
                }
            }
            interactedContacts.push(contactObj)
        }
    });

    var contacts = _.uniq(interactedContacts,'personEmailId');
    var contactsEmailIdArr = _.pluck(interactedContacts,'personEmailId');
    var mobileNumbers = [];

    contactObj.addContactNotExist(common.castToObjectId(userProfile._id.toString()),userProfile.emailId,contacts,mobileNumbers,contactsEmailIdArr,'outlook',false);
}

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        return next();
    }else
    {
        // if they aren't redirect them to the home page
        return res.redirect('/');
    }
}

function getUserLogInfo(req){
    var userIp = getClientIp(req);
    var userLogInfo;
    if(req.session.userLogInfo){
        userLogInfo = req.session.userLogInfo;
    }
    else{
        userLogInfo = {
            userName:'',
            userIp:userIp,
            loginType:'',
            info:'',
            cause:'',
            action : ''
        }
        req.session.userLogInfo = userLogInfo;
    }
    return userLogInfo;
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
};


function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}



module.exports = router;