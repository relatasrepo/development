var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var moment = require('moment-timezone');

var userManagement = require('../dataAccess/userManagementDataAccess');
var taskManagementClass = require('../dataAccess/taskManagementClass');
var emailSender = require('../public/javascripts/emailSender');
var validations = require('../public/javascripts/validation');
var appCredentials = require('../config/relatasConfiguration');
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');
var googleCalendarAPI = require('../common/googleCalendar');

var common = new commonUtility();
var validation = new validations();
var emailSenders = new emailSender();
var appCredential = new appCredentials();
var userManagementObj = new userManagement();
var taskManagementClassObj = new taskManagementClass();
var logLib = new winstonLog();
var googleCalendar = new googleCalendarAPI();

var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var logger =logLib.getWinston();

router.post('/web/task/create/new',function(req,res){

});

module.exports = router;
