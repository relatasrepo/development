 /*
 * relatas-application framework
 *  
 * exports  Edit profile 
 */

 // initializing  the required module

 var express = require('express');
 var router = express.Router();
 var passport = require('passport');
 var jwt = require('jwt-simple');
 var crypto = require('crypto');
 var fs = require('fs');
 var request=require('request');

 var userManagement = require('../dataAccess/userManagementDataAccess');
 var emailSender = require('../public/javascripts/emailSender');
 var corporateClass = require('../dataAccess/corporateDataAccess/corporateModelClass');

 var userManagements = new userManagement();
 var winstonLog = require('../common/winstonLog');
 var commonUtility = require('../common/commonUtility');
 var appCredentials = require('../config/relatasConfiguration');

 var logLib = new winstonLog();
 var corporate = new corporateClass();
 var logger =logLib.getWinston();
 var common = new commonUtility();
 var appCredential = new appCredentials();
 var authConfig = appCredential.getAuthCredentials();
 var domain = appCredential.getDomain();
 var domainName = domain.domainNameForUniqueName;

// Old Routes
//router.get('/editProfile', isLoggedIn,common.checkUserDomain, function(req,res){
//	 try {
//          res.render('editProfile');
//          logger.info('authorized user entering to the editProfilePage Page');
//       }
//    catch (ex) {
//         logger.error('authenticated is invalid to landing editProfilePage page', ex.stack);
//         throw ex;
//      }
//});

 router.get('/isLinedinUpdate',function(req,res){
     var response = req.session.linkedinUpdated;
     req.session.linkedinUpdated = false;
     res.send(response)
 })

router.get('/userProfileEdit', isLoggedIn,function(req, res){ 
   
   try {
        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret); 
        var userId = decoded.id; 
        
        userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,userProfile) {
         
          if (error) {
                   logger.error('Error occurred while searching authenticated user profile in the DB');
                   userProfile = null;
                   res.send(userProfile);
          }
          else{
                  logger.info('transfering authenticated user profile information');
                 res.send(userProfile);
          }
      }); 
  }
  catch (ex) {
         logger.error('authentication is failed while user transfering data ', ex.stack); 
         throw ex;
    }  
});

 router.post('/updateLocationLatLang',function(req,res){
     var latLang = req.body;

     if(checkRequiredField(latLang.userId) && checkRequiredField(latLang.latitude) && checkRequiredField(latLang.longitude)){
         var obj = {
             latitude:''+latLang.latitude,
             longitude:''+latLang.longitude
         };

         userManagements.updateLocationLatLang(latLang.userId,obj,function(error,isUpdated,message){
              logger.info(message.message);
              res.send(true);
         });
     }else res.send(false);
 });

router.post('/edit', isLoggedIn, function(req,res){

  try{
	    var userProfiles = req.body;
        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret); 
        var userId = decoded.id;

        userManagements.findUserProfileByIdWithCustomFields(userId,
            {
                emailId:1,
                profilePrivatePassword:1,
                profileDescription:1,
                firstName:1,
                lastName:1,
                publicProfileUrl:1,
                serviceLogin:1,
                designation:1,
                companyName:1,
                mobileNumber:1,
                skypeId:1,
                location:1,
                birthday:1
            },function(error, userData){
          if (userData == null || userData == undefined || userData == '') {
               res.send('<html><head><script>alert("An error occurred, your profile not updated");window.location.replace("/")</script></head></html>');
          }
          else{
                if(checkRequiredField(userProfiles.profilePicUrl) && userProfiles.profilePicUrl.charAt(0) == 'h'){
                    storeProfileImg(userProfiles.profilePicUrl,userProfiles.publicProfileUrl,userId);
                }
                updateProfilePrivatePassword(userId,userProfiles.profilePrivatePassword,userData.profilePrivatePassword);
                updateProfileDescription(userId,userProfiles.profileDescription,userData.profileDescription);
                updateCalendarAccess(userId,userProfiles);
                updateFirstName(userId,userProfiles.firstName, userData.firstName);
                updateEmailId(userId,userProfiles.emailId, userData.emailId);
                updateRelatasIdentity(userId,userProfiles.publicProfileUrl,userData.publicProfileUrl);
                updateDailyAgendaField(userId,userProfiles.sendDailyAgenda);
                var workHoursObj = {
                         start:userProfiles.workHoursStart,
                         end:userProfiles.workHoursEnd
                }
                updateWorkHours(userId,workHoursObj);

                if(userData.serviceLogin == 'relatas'){
                    updatePassword(userId,userProfiles.password);
                }

              if(checkRequiredField(userProfiles.tzName)){
                  var zone = {};
                  zone.name = userProfiles.tzName;
                  zone.zone = userProfiles.tzZone;
                  zone.updated = true;
                  updateTimezone(userId,zone);
              }

                userManagements.updateRegistereStatus(userId,function(error,isUpdated,msg){
                    logger.info(msg.message);
                });


             if (!compare(userProfiles.lastName, userData.lastName)) {

                   userManagements.updateLastName(userId,userProfiles.lastName,function(error,isUpdated,message){
                  	if (isUpdated) {
                       logger.info(message.message);
                  	}else logger.info(message.message);
                  });
             }

             if (!compare(userProfiles.designation, userData.designation)) {

                   userManagements.updateDesignation(userId,userProfiles.designation,function(error,isUpdated,message){
                  	if (isUpdated) {
                       logger.info(message.message);
                  	}else logger.info(message.message);
                  });
             }

             if (!compare(userProfiles.companyName, userData.companyName)) {

                   userManagements.updateCompanyName(userId,userProfiles.companyName,function(error,isUpdated,message){
                  	if (isUpdated) {
                       logger.info(message.message);
                  	}else logger.info(message.message);
                  });
             }

             //if (!compare(userProfiles.mobile, userData.mobileNumber)) {

                  userManagements.updateMobileNumber(userId,userProfiles.mobile,function(error,isUpdated,message){
                  	if (isUpdated) {
                       logger.info(message.message);
                  	}else logger.info(message.message);
                  });
             //}

             if (!compare(userProfiles.skypeId, userData.skypeId)) {

                  userManagements.updateSkypeId(userId,userProfiles.skypeId,function(error,isUpdated,message){
                  	if (isUpdated) {
                       logger.info(message.message);
                  	}else logger.info(message.message);
                  });
             }

             if (!compare(userProfiles.location, userData.location)) {

                  userManagements.updateLocation(userId,userProfiles.location,function(error,isUpdated,message){
                  	if (isUpdated) {
                       logger.info(message.message);
                  	}else logger.info(message.message);
                  });
             }

             if (!compare(userProfiles.day,userData.birthday.day) || !compare(userProfiles.month,userData.birthday.month) || !compare(userProfiles.year,userData.birthday.year)) {

                 userManagements.updateBirthday(userId,userProfiles.day,userProfiles.month,userProfiles.year,function(error,isUpdated,message,result){

                     if (isUpdated) {
                       logger.info(message.message);
                    }else logger.info(message.message);
                 });
             }

                var emailSenderObj = new emailSender();
                var dataTosend = {
                      emailId:userProfiles.emailId,
                      firstName:userProfiles.firstName
                }

              if(checkRequiredField(userProfiles.firstName) && checkRequiredField(userProfiles.lastName) && userProfiles.emailId){
                 var corporateObj = {
                     userId:userData._id,
                     emailId:userProfiles.emailId,
                     firstName:userProfiles.firstName,
                     lastName:userProfiles.lastName
                 }
                 corporate.updateCorporateAcc(corporateObj);
              }

             emailSenderObj.sendEditProfileConfirmationMail(dataTosend);
             common.updateInteractions(userId);
             common.updateContactMultiCollectionLevel(userId,false);
              userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,user){
                  if(error){
                      logger.info('Error in replaceProfileInSession():CommonUtility  user: '+userId ,error);
                  }

                  if(user){
                      req.session.profile = user;
                  }
              });
             req.session.editProfileSession = '';
             res.redirect('/editProfile');
           }

        });
   }
   catch(e){
        logger.info('Error occurred while updating, in edit profile router '+e);
  }
});


 router.post('/updateEmailProfile',isLoggedIn,function(req,res){
     var userProfiles = req.body;

     var tokenSecret = "alpha";
     var decoded = jwt.decode(req.user ,tokenSecret);
     var userId = decoded.id;

     userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,profileDescription:1,designation:1,companyName:1,mobileNumber:1,skypeId:1,birthday:1,location:1},function(error, userData) {
         if (userData == null || userData == undefined || userData == '') {
             res.send('<html><head><script>alert("An error occurred, your profile not updated");window.location.replace("/")</script></head></html>');
         }
         else {

             updateProfileDescription(userId,userProfiles.profileDescription,userData.profileDescription);
             updateCalendarAccess(userId,userProfiles);
             var workHoursObj = {
                 start:userProfiles.workHoursStart,
                 end:userProfiles.workHoursEnd
             }
             updateWorkHours(userId,workHoursObj);
             var password = '';

                 if(userProfiles.profilePrivatePassword == ''){
                     password = '';
                 }
                 else{
                     password = EncryptionValue(userProfiles.profilePrivatePassword);
                 }

                 userManagements.updateProfilePrivatePassword(userId,password,function(error,isUpdated,message){
                     logger.info(message.message);
                 });


             if (!compare(userProfiles.designation, userData.designation)) {

                 userManagements.updateDesignation(userId,userProfiles.designation,function(error,isUpdated,message){
                     if (isUpdated) {
                         logger.info(message.message);
                     }else logger.info(message.message);
                 });
             }

             if (!compare(userProfiles.companyName, userData.companyName)) {

                 userManagements.updateCompanyName(userId,userProfiles.companyName,function(error,isUpdated,message){
                     if (isUpdated) {
                         logger.info(message.message);
                     }else logger.info(message.message);
                 });
             }

             if (!compare(userProfiles.mobile, userData.mobileNumber)) {

                 userManagements.updateMobileNumber(userId,userProfiles.mobile,function(error,isUpdated,message){
                     if (isUpdated) {
                         logger.info(message.message);
                     }else logger.info(message.message);
                 });
             }

             if (!compare(userProfiles.skypeId, userData.skypeId)) {

                 userManagements.updateSkypeId(userId,userProfiles.skypeId,function(error,isUpdated,message){
                     if (isUpdated) {
                         logger.info(message.message);
                     }else logger.info(message.message);
                 });
             }

             if (!compare(userProfiles.location, userData.location)) {

                 userManagements.updateLocation(userId,userProfiles.location,function(error,isUpdated,message){
                     if (isUpdated) {
                         logger.info(message.message);
                     }else logger.info(message.message);
                 });
             }

             if (!compare(userProfiles.day,userData.birthday.day) || !compare(userProfiles.month,userData.birthday.month) || !compare(userProfiles.year,userData.birthday.year)) {

                 userManagements.updateBirthday(userId,userProfiles.day,userProfiles.month,userProfiles.year,function(error,isUpdated,message,result){

                     if (isUpdated) {
                         logger.info(message.message);
                     }else logger.info(message.message);
                 });
             }
             res.send(true)
         }
     })
 });

function updateDailyAgendaField(userId,flag){

    userManagements.updateSendDailyAgenda(userId,flag,function(error,isUpdated,message){
        logger.info(message.message);
    })
}

function updateWorkHours(userId,obj){

    userManagements.updateWorkHours(userId,obj,function(error,isUpdated,message){
        logger.info(message.message);
    })
}

function updateProfilePrivatePassword(userId,profilePrivatePassword,userData){

    var password;
    if (!compare(profilePrivatePassword,'@pass@')) {
        if(profilePrivatePassword == ''){
            password = '';
        }
        else{
            password = EncryptionValue(profilePrivatePassword);
        }

       userManagements.updateProfilePrivatePassword(userId,password,function(error,isUpdated,message){
           logger.info(message.message);
       });
    }
};

 function updateProfileDescription(userId,profileDescription,userData){

     userManagements.updateProfileDescription(userId,profileDescription,function(error,isUpdated,message){
         logger.info(message.message);
     });
 }

 function updateCalendarAccess(userId,userInfo){

     var profileType = {
         timeOne:{
             start:userInfo.startTime1 || '',
             end:userInfo.endTime1 || ''
         },
         timeTwo:{
             start:userInfo.startTime2 || '',
             end:userInfo.endTime2 || ''
         }
     }
     if(userInfo.public == true || userInfo.public == 'true'){
         profileType.calendarType = 'public';
     }else profileType.calendarType = 'selectPublic';
     userManagements.updateCalendarAccess(userId,profileType,function(error,isUpdated,message){
            logger.info(message.message);
     });
 }

 function updateFirstName(userId,firstNameNew,firstNameOld){
     if (!compare(firstNameNew, firstNameOld)) {

         userManagements.updateFirstName(userId,firstNameNew,function(error,isUpdated,message){
             logger.info(message.message);
         });
     }
 }

 function updateTimezone(userId,zone){

         userManagements.updateTimezone(userId,zone,function(error,isUpdated,message){
             logger.info(message.message);
         });
 }

 function updateEmailId(userId,eNew,eOld){
        if(!compare(eNew,eOld)){
            userManagements.updateEmailId(userId,eNew,function(error,isUpdated,message){
                logger.info(message.message);
            })
        }
 }

 function updateRelatasIdentity(userId,rIdentityNew,rIdentityOld){

    if(!compare(rIdentityNew,rIdentityOld)){
         userManagements.updateRelatasIdentity(userId,rIdentityNew,function(error,isUpdated,msg){
             logger.info(msg.message);
         })
    }

 }

 function updatePassword(userId,password){
     if(!compare(password,'@defaultPass@')){
          userManagements.updateProfilePassword(userId,password,function(error,isUpdated,msg){
                logger.info(msg.message)
          });
     }
 }

 function storeProfileImg(url,imageName,userId){
     if(url != 'images/default.png'){
         imageName = common.getValidUniqueName(imageName)
         if(common.checkRequired(imageName)){
             request.get({url: url, encoding: 'binary'}, function (err, response, body) {

                 if(!err && body != null && body != undefined && !common.contains(body,'html') && !common.contains(body,'title')){
                     fs.writeFile('./public/profileImages/'+imageName, body, 'binary', function(err) {
                         if(err){
                             logger.info('Error in getting image from third party server '+err);
                         }
                         else{
                             logger.info('Saving image in server success (image from thirdParty servers)');
                             userManagements.updateProfileImage(userId,'/profileImages/'+imageName,function(error,isUpdated,msg){

                             });
                         }
                     });
                 }
                 else{
                     logger.info('Error in getting image from third party old:contacts:router ', err);
                 }
             });
         }
     }
 };

 function EncryptionValue(data){
     var shaAlgorithm = crypto.createHash('sha256');
     shaAlgorithm.update(data);

     var hashedData = shaAlgorithm.digest('hex');
     return hashedData;
 }

 router.get('/removeLinkedinAccount',isLoggedIn,function(req,res){

     var tokenSecret = "alpha";
     var decoded = jwt.decode(req.user ,tokenSecret);
     var userId = decoded.id;

    userManagements.updateLinkedin(userId,'','','','',function(error,isUpdated,message){
          logger.info(message.message);
        res.send(isUpdated)
    });
 });

 router.get('/removeFacebookAccount',isLoggedIn,function(req,res){

     var tokenSecret = "alpha";
     var decoded = jwt.decode(req.user ,tokenSecret);
     var userId = decoded.id;

     userManagements.updateFacebook(userId,'','','','',function(error,isUpdated,message){
         logger.info(message.message);
         res.send(isUpdated)
     });
 });

 router.get('/removeTwitterAccount',isLoggedIn,function(req,res){

     var tokenSecret = "alpha";
     var decoded = jwt.decode(req.user ,tokenSecret);
     var userId = decoded.id;

     userManagements.updateTwitter(userId,'','','','','',function(error,isUpdated,message){
         logger.info(message.message);
         res.send(isUpdated)
     });
 });

 router.get('/removeGoogleAccount/:googleId',function(req,res){

     var tokenSecret = "alpha";
     var decoded = jwt.decode(req.user ,tokenSecret);
     var userId = decoded.id;
        var googleId = req.params.googleId;
     userManagements.removeGoogleAccountById(userId,googleId,function(error,isUpdated,message){
         logger.info(message.message);
         res.send(isUpdated);
     })

 });


 router.post('/profileImg',isLoggedIn,function(req, res) {
     var fstream;
     var tokenSecret = "alpha";
     var decoded = jwt.decode(req.user ,tokenSecret);
     var userId = decoded.id;
     req.pipe(req.busboy);
     req.busboy.on('file', function (fieldname, file, filename) {

         userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,publicProfileUrl:1},function(error, userData){
             if(error || userData == null){
                 logger.info('Error in mongo db while searching user profile using id in profileImg router editprofile '+error);
                 res.redirect('back');
             }
             else{
                 var extension = filename.split('.')[filename.split('.').length-1];
                 var uniqueName = common.getValidUniqueName(userData.publicProfileUrl);
                 var imagePath = '/profileImages/'+uniqueName+'.'+extension || 'jpg';

                 fstream = fs.createWriteStream('./public/profileImages/' + uniqueName+'.'+extension || 'jpg');
                 file.pipe(fstream);
                 fstream.on('close', function () {
                     userManagements.updateProfileImage(userId,imagePath,function(error,isUpdated,message){
                         if(error){
                             logger.info('Error in mongo while updating profile image edit profile '+error);
                             res.redirect('back');
                         }
                         else{
                             res.redirect('back');
                         }
                     });
                 });
             }
         });

     });
 });



 router.post('/uploadProfile', isLoggedIn,function(req, res) {

     // get the id of the logged in user
     var tokenSecret = "alpha";
     var decoded = jwt.decode(req.user ,tokenSecret);
     var userId = decoded.id;

     // get the current server time
     var currentTime = new Date();
     var fileNameUniq = userId + '_' + currentTime.getTime();
     var tmp_path = req.files.userPhoto.path;
     // set where the file should actually exists - in this case it is in the "images" directory
     var target_path = './public/profileImages/'+ fileNameUniq + req.files.userPhoto.name;

     // move the file from the temporary location to the intended location
     var send_path = 'profileImages/'+ fileNameUniq + req.files.userPhoto.name;
     fs.rename(tmp_path, target_path, function(err) {
         if (err){
             res.send(false);
         }
         // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files
         fs.unlink(tmp_path, function() {
             if (err){
                 logger.error('client enter the resource not found page 404');
                 res.send(false);
             } else{
                 userManagements.updateProfileImage(userId,send_path);
                 res.send({ path: send_path });
             }
         });
     });

 });


 router.get('/storeDatabaseData',function(req,res){
     req.session.databaseData = req.body;
     res.send(true);
 });

router.post('/storeEditProfileSession',function(req,res){
	  req.session.editProfileSession = req.body;
	  res.send(true);
});

router.get('/getEditProfileSession',function(req,res){
                 
	  res.send(req.session.editProfileSession);
});

 router.get('/removeEditProfileSession',function(req,res){
     req.session.editProfileSession = '';
     res.send(null);
 });

router.get('/getFacebookDatabaseData',function(req,res){
        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret); 
        var userId = decoded.id; 
        
        userManagements.findUserProfileByIdWithCustomFields(userId,{facebook:1},function(error,userProfile) {
          if (error) {
                   userProfile = null;
                   res.send(userProfile);
          }
          else{
                 res.send(userProfile.facebook);
          }
      }); 
	
});

router.get('/getLinkedinDatabaseData',function(req,res){
  var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret); 
        var userId = decoded.id;
        userManagements.findUserProfileByIdWithCustomFields(userId,{linkedin:1},function(error,userProfile) {
          if (error) {
                   userProfile = null;
                   res.send(userProfile);
          }
          else{
                 res.send(userProfile.linkedin);
          }
      }); 
	
});

router.get('/getTwitterDatabaseData',function(req,res){
        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret); 
        var userId = decoded.id; 
        
        userManagements.findUserProfileByIdWithCustomFields(userId,{twitter:1},function(error,userProfile) {
          if (error) {
                   userProfile = null;
                   res.send(userProfile);
          }
          else{
                 res.send(userProfile.twitter);
          }
      }); 

});

router.get('/getGoogleDatabaseData',function(req,res){
	    var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret); 
        var userId = decoded.id; 

        userManagements.findUserProfileByIdWithCustomFields(userId,{google:1},function(error,userProfile) {
          if (error) {
                   userProfile = null;
                   res.send(userProfile);
          }
          else{
                 res.send(userProfile.google);
          }
      }); 
});

 router.get('/checkEmailEdit/:emailId',isLoggedIn,function(req,res){
     var tokenSecret = "alpha";
     var decoded = jwt.decode(req.user ,tokenSecret);
     var userId = decoded.id;
     var emailId = req.params.emailId;

     userManagements.findUserProfileByEmailIdWithCustomFields(emailId,{emailId:1},function(error,profile,message){
             if(error){
                 res.send('error')
             }
             else if(profile == null){
                 res.send(true)
             }
             else{
                 if(profile._id == userId){
                     res.send(true)
                 }
                 else res.send(false)
             }
     });

 });

 router.post('/checkUrlEdit',isLoggedIn,function(req,res){
     var uniqueName = req.body.publicProfileUrl;
     var url = domainName+'/u/'+uniqueName.toLowerCase();
     var tokenSecret = "alpha";
     var decoded = jwt.decode(req.user ,tokenSecret);
     var userId = decoded.id;

     logger.info('Checking url existing or not in edit')

     userManagements.findUserPublicProfile(url,uniqueName,function(error,profile){
         if(error){
             res.send('error')
         }else
         if (profile == false) {

             res.send(true)
         }
         else if(profile != null){
             if(userId == profile._id){
                 res.send(true)
             }
             else{
                 res.send(false)
             }
         }
         else res.send('error')
     })
 });


 function checkRequiredField(data){
     if (data == '' || data == null || data == undefined) {
         return false;
     }
     else{
         return true;
     }
 }

 // route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
  
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        return next();//next(); 
      }else
      {
    // if they aren't redirect them to the home page
     return res.redirect('/');
  }
}

function compare(string1, string2){
	 if (string1 == string2) {
         return true;
	 }
	 else return false;
}

module.exports = router;
