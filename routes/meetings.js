
var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var request = require('request');
var mongoose = require('mongoose');
var moment = require('moment-timezone');

var userManagement = require('../dataAccess/userManagementDataAccess');
var meetingManagement = require('../dataAccess/meetingManagement');
var interactionManagement = require('../dataAccess/interactionManagement');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var messageDataAccess = require('../dataAccess/messageManagement');
var meetingSupportClass = require('../common/meetingSupportClass');
var commonUtility = require('../common/commonUtility');
var googleCalendarAPI = require('../common/googleCalendar');
var mongoScheduler = require('../common/mongoScheduler');
var mongoSchedulerHandler = require('../common/mongoSchedulerHandler');

var common = new commonUtility();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var interactions = new interactionManagement();
var userManagements = new userManagement();
var meetingSupportClassObj = new meetingSupportClass();
var meetingClassObj = new meetingManagement();
var messageAccess = new messageDataAccess();
var emailSenders = new emailSender();
var googleCalendar = new googleCalendarAPI();
var mongoScheduleObj = new mongoScheduler();
mongoScheduleObj.startScheduler(new mongoSchedulerHandler());

var logLib = new winstonLog();
var logger =logLib.getWinston();

router.get('/interactions/summary/:user2Id',isLoggedInUser,common.checkUserDomain,function(req,res){
    try {
        var interactionsWithUserId = req.params.user2Id;
        if(common.checkRequired(interactionsWithUserId)){
            req.session.interactionsWithUserId = interactionsWithUserId;
            res.render('interactionDetails');
            logger.info('User entered in to interactionDetails page');
        }
        else res.redirect('/');

    }catch (ex) {
        logger.error('Error in rendering interactionDetails page', ex.stack);
        throw ex;
    }
});

router.get('/interactions/profileIntelligence',isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('profileIntelligence');
});

router.get('/getInteractionWithUserId',function(req,res){
    var userId = req.session.interactionsWithUserId;
    res.send(userId);
});

router.get('/getInteractions/meetings/summary',isLoggedInUser,function(req,res){

        var notLoggedInUser = req.session.interactionsWithUserId;
        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret);
        var userId = decoded.id;
    if(common.checkRequired(userId) && common.checkRequired(notLoggedInUser)){
        userManagements.getMeetingInteractionsSummary(userId,notLoggedInUser,function(list1,list2,list3,list4){
            list1 = list1.concat(list2,list3,list4);

            res.send(list1)

        })
    }
    else res.send(false)
});

router.get('/getInteractions/documents/summary',isLoggedInUser,function(req,res){

    var notLoggedInUser = req.session.interactionsWithUserId;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(common.checkRequired(userId) && common.checkRequired(notLoggedInUser)){
        userManagements.getDocumentsInteractionsSummary(userId,notLoggedInUser,function(list1,list2){
            list1 = list1.concat(list2);

            res.send(list1)

        })
    }
    else res.send(false)
});

/***** CURRENTLY NOT USING *****/
router.get('/getInteractions/messages/summary',isLoggedInUser,function(req,res){

    var notLoggedInUser = req.session.interactionsWithUserId;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(common.checkRequired(userId) && common.checkRequired(notLoggedInUser)){
        messageAccess.getMessageInteractions(userId,notLoggedInUser,function(messages){
            res.send(messages)
        })
    }
    else res.send(false)
});

router.get('/getMessages/summary',isLoggedInUser,function(req,res){

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(common.checkRequired(userId)){
        messageAccess.getMessagesSent(userId,function(num){
            res.send(num+'');
        })
    }
    else res.send(0)
});

router.get('/abc/interactions',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    interactions.totalInteractionsIndividual(userId,null,null,function(){
        res.send(true);
    })
});

router.get('/interactionsCount/individual/:userId',function(req,res){
    var userId = req.params.userId;
    interactions.totalInteractionsIndividual(userId,null,null,function(interactions){
         res.send(interactions);
    })
});

router.get('/interactionsCount/:otherUserId',function(req,res){

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var otherUser = req.params.otherUserId;

    userManagements.getMeetingInteractionsCount(userId,otherUser,function(meetingsCount){
        userManagements.getDocumentsInteractionsCount(userId,otherUser,function(documentsCount){
              messageAccess.getMessageInteractionsCount(userId,otherUser,function(messagesCount){
                  var totalCount = meetingsCount+documentsCount+messagesCount;
                  totalCount = ''+totalCount;
                  res.send(totalCount);
              })
        })
    })
});

router.get('/interactions/count/lastInteractionDate',isLoggedInUser,function(req,res){
      var userId = common.getUserId(req.user);
    interactions.totalInteractionsSelfCountAndLastInteracted(userId,function(interactionReport){
        res.send(interactionReport);
    })
});

router.get('/interactions/count/lastFiveInteractions',isLoggedInUser,function(req,res){
    var userId = common.getUserId(req.user);
    interactions.totalInteractionsSelfLastFiveInteracted(userId,function(interactionReport){
        res.send(interactionReport);
    })
});


router.get('/meetings/action/delete',function(req,res){

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    if(common.checkRequired(userId) && common.checkRequired(req.query.mId)){
        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                meetingSupportClassObj.cancelMeeting(req.query.mId,userId,"",timezone,function(isSuccess,error){
                    if(isSuccess){
                        res.send(true);
                    }
                    else{
                        res.send(false);
                    }
                })

            }
            else res.send(false);
        });
        /*userManagements.deleteMeeting(userId,req.query.mId,function(result){
            if(result != false && common.checkRequired(result)){
                interactions.removeInteractions(result.invitationId);
                var dataToSend_sender = {};
                dataToSend_sender.emailId = result.senderEmailId;
                dataToSend_sender.toName = result.senderName;
                var maxSlot = result.scheduleTimeSlots.reduce(function (a, b) {
                    return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                });
                var canceledByUserName = result.senderName;
                var participants = result.senderName;
                if(result.selfCalendar){
                    for(var par=0; par<result.toList.length; par++){
                        var rFName = result.toList[par].receiverFirstName || '';
                        var rLName = result.toList[par].receiverLastName || '';
                        var name = rFName+' '+rLName;
                        participants += ', '+name;
                    }
                }
                else{
                    var rName = result.to.receiverName || '';
                    participants += ', '+rName;
                }

                dataToSend_sender.subject = 'Relatas: Your meeting canceled';
                var start = moment(maxSlot.start.date).tz(req.query.timezone || 'UTC');
                var date = start.format("Do MMMM");
                var time = start.format("h:mm a z");
                var mailContent = "The meeting <b>'"+maxSlot.title+"'</b> dated <b>'"+date+" : "+time+"'</b> with the participants <b>'"+participants+"'</b> has been cancelled by <b>'"+canceledByUserName+"'</b>. If you wish to reschedule please log into Relatas.com";
                dataToSend_sender.mailContent = "The meeting <b>'"+maxSlot.title+"'</b> dated <b>'"+date+" : "+time+"'</b> with the participants <b>'"+participants+"'</b> has been cancelled by <b>'you'</b>. If you wish to reschedule please log into Relatas.com";
                emailSenders.sendMeetingCancelOrDeletionMail(dataToSend_sender,"Meeting Canceled");

                if(result.selfCalendar){
                    for(var i=0; i<result.toList.length; i++){
                        var dataToSend_receiver = {};
                        var rFName2 = result.toList[i].receiverFirstName || '';
                        var rLName2 = result.toList[i].receiverLastName || '';

                        dataToSend_receiver.subject = 'Relatas: Your meeting canceled';
                        dataToSend_receiver.toName = rFName2+' '+rLName2;
                        dataToSend_receiver.emailId = result.toList[i].receiverEmailId;
                        dataToSend_receiver.mailContent = mailContent;
                        emailSenders.sendMeetingCancelOrDeletionMail(dataToSend_receiver,"Meeting Canceled");
                    }
                }
                else{
                    var rName2 = result.to.receiverName || '';
                    emailSenders.sendMeetingCancelOrDeletionMail({
                        subject:'Relatas: Your meeting canceled',
                        toName:rName2,
                        emailId:result.to.receiverEmailId,
                        mailContent:mailContent
                    },"Meeting Canceled");
                }

                res.send(true);
            }else
            res.send(false);
        })*/
    }
    else res.send(false);
});

router.post('/meetings/action/cancel',isLoggedInUser,function(req,res){

    var body = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    if(common.checkRequired(userId) && common.checkRequired(body.invitationId)){

        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                meetingSupportClassObj.cancelMeeting(body.invitationId,userId,body.message,timezone,function(isSuccess,error){
                    if(isSuccess){
                        res.send(true);
                    }
                    else{
                        res.send(false);
                    }
                })

            }
            else res.send(false);
        });
        /*userManagements.cancelMeeting(userId,body.invitationId,body.selfCalendar,body.message,function(result){
            if(common.checkRequired(result)){
                interactions.removeUserCanceledInteractions(result.invitationId,userId);
                var dataToSend_sender = {};
                var dataToSend_receiver = {};
                dataToSend_sender.emailId = result.senderEmailId;
                dataToSend_sender.toName = result.senderName;
                var maxSlot = result.scheduleTimeSlots.reduce(function (a, b) {
                    return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                });
                var canceledByUserName = '';
                var participants = result.senderName;
                if(result.selfCalendar){
                    for(var par=0; par<result.toList.length; par++){
                        var rFName = result.toList[par].receiverFirstName || '';
                        var rLName = result.toList[par].receiverLastName || '';
                        var name = rFName+' '+rLName;
                        participants += ', '+name;
                        if(userId == result.toList[par].receiverId){
                            canceledByUserName = name;
                            dataToSend_receiver.toName = name;
                            dataToSend_receiver.emailId = result.toList[par].receiverEmailId
                        }
                    }
                }
                else{
                    var rName = result.to.receiverName || '';
                    participants += ', '+rName;
                    if(userId == result.to.receiverId){
                        canceledByUserName = rName;
                        dataToSend_receiver.toName = rName;
                        dataToSend_receiver.emailId = result.to.receiverEmailId

                    }
                }

                dataToSend_sender.subject = 'Relatas: Your meeting canceled';
                dataToSend_receiver.subject = 'Relatas: Your meeting canceled';
                var start = moment(maxSlot.start.date).tz(body.timezone || 'UTC');
                var date = start.format("Do MMMM");
                var time = start.format("h:mm a z");


                var mailContent = "The meeting <b>'"+maxSlot.title+"'</b> dated <b>'"+date+" : "+time+"'</b> with the participants <b>'"+participants+"'</b> has been cancelled by <b>'"+canceledByUserName+"'</b>. If you wish to reschedule please log into Relatas.com";
                 dataToSend_sender.mailContent = mailContent;
                 dataToSend_receiver.mailContent = "The meeting <b>'"+maxSlot.title+"'</b> dated <b>'"+date+" : "+time+"'</b> with the participants <b>'"+participants+"'</b> has been cancelled by <b>'you'</b>. If you wish to reschedule please log into Relatas.com";
                 if(common.checkRequired(req.body.message)){
                     dataToSend_sender.mailContent = dataToSend_sender.mailContent+". Message from <b>"+canceledByUserName+"</b>: '"+req.body.message+"'.";
                     dataToSend_receiver.mailContent =  dataToSend_receiver.mailContent+". Message from <b>"+canceledByUserName+"</b>: '"+req.body.message+"'.";
                 }
                 emailSenders.sendMeetingCancelOrDeletionMail(dataToSend_sender,"Meeting Canceled");
                 emailSenders.sendMeetingCancelOrDeletionMail(dataToSend_receiver,"Meeting Canceled");
                res.send(true);
            }else
                res.send(result);
        })*/
    }
    else res.send(false);
});

router.get('/meetings/todo/pending',function(req,res){
    if(common.checkRequired(req.query.filter) && common.checkRequired(req.query.timezone)){
        var givenDate = moment().tz(req.query.timezone);
        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret);
        var userId = decoded.id;
        var dateMin = givenDate.clone();     //min date
        var dateMax = givenDate.clone();     //max date

        dateMin.date(dateMin.date()-1)
        dateMin.hour(0)
        dateMin.minute(0)
        dateMin.second(0)
        dateMin.millisecond(0)

        dateMax.date(dateMax.date()+1)
        dateMax.hour(23)
        dateMax.minute(59)
        dateMax.second(59)
        dateMax.millisecond(59)
        dateMin = dateMin.format()
        dateMax = dateMax.format()

        if(common.checkRequired(req.query.returnData == 'count')){
            meetingClassObj.getPendingToDoItemsCount(userId,dateMin,function(count){
                if(common.checkRequired(count)){
                    res.send(count.length+'')
                }
                else res.send('0');
            })
        }
        else{
            meetingClassObj.getPendingToDoItems(userId,dateMin,dateMax,req.query.filter,function(meetings){
                res.send(meetings);
            });
        }
    }
});

router.post('/meeting/todo/add',function(req,res){
    var todoSrt = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
    var domain = protocol+req.headers.host;
    if(common.checkRequired(todoSrt) && common.checkRequired(todoSrt.items) && common.checkRequired(todoSrt.invitationId)){
        var todoList = JSON.parse(todoSrt.items);
        var creatorName = todoSrt.creatorName;

        if(todoList.length > 0){
            var oldTodo = [];
            var newTodo = [];
            var statusTodo = [];

            for(var s=0; s<todoList.length; s++){
                if(todoList[s].owner){
                    oldTodo.push(todoList[s]);
                }else if(todoList[s].receiver){
                    statusTodo.push(todoList[s])
                }else if(!common.checkRequired(todoList[s]._id)){
                    newTodo.push(todoList[s])
                }
            }

            var meetingObj = JSON.parse(todoSrt.meetingObj);
            if(oldTodo.length > 0){
                updateOldToDo(oldTodo,meetingObj,userId,todoSrt.acceptedSlot, todoSrt.timezone, domain + '/meeting/' + todoSrt.invitationId);
            }

            if(statusTodo.length > 0){
                for(var st=0; st<statusTodo.length; st++){
                    updateToDoStatus(statusTodo[st],meetingObj,userId,todoSrt.acceptedSlot, todoSrt.timezone, domain + '/meeting/' + todoSrt.invitationId);
                }
            }

            var newArrList = statusTodo.concat(oldTodo);
            sendNotifyEmailBundleTasks(newArrList,userId,meetingObj,todoSrt,domain,creatorName);

            if (newTodo.length > 0) {
                var result = groupBy(newTodo, function (item) {
                    return [item.assignedTo];
                });

                newTodo.forEach(function (todoItem, index) {
                    if (common.checkRequired(todoItem._id)) {
                        createOrUpdateTodo(todoItem, false, todoSrt.invitationId, userId, meetingObj, todoSrt.acceptedSlot, todoSrt.timezone, domain + '/meeting/' + todoSrt.invitationId);
                    } else {
                        todoItem._id = mongoose.Types.ObjectId();
                        todoItem.createdBy = userId;
                        createOrUpdateTodo(todoItem, true, todoSrt.invitationId, userId, meetingObj, todoSrt.acceptedSlot, todoSrt.timezone, domain + '/meeting/' + todoSrt.invitationId);
                    }
                });

                if (common.checkRequired(result) && result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        sendToDoNotifyEmailToReceiver(userId, result[i], meetingObj, todoSrt.acceptedSlot, todoSrt.timezone, domain + '/meeting/' + todoSrt.invitationId, creatorName)
                    }
                }

                userManagements.findUserProfileByIdWithCustomFields(userId, {
                    emailId: 1,
                    firstName: 1,
                    lastName: 1,
                    timezone: 1
                }, function (error, user) {
                    if (common.checkRequired(user)) {
                        sendNotifyEmailForTodo(newTodo, user, meetingObj, todoSrt.timezone, todoSrt.acceptedSlot, domain + '/meeting/' + todoSrt.invitationId, creatorName,false,true);
                    }
                });
            }

            res.send(true);
        }
        else res.send(false)
    }
    else res.send(false)
});

/**/

function sendNotifyEmailBundleTasks(TodoArr,userId,meetingObj,todoSrt,domain,creatorName){
    var newList = [];

    for(var l=0; l<TodoArr.length; l++){
        if(TodoArr[l].owner && TodoArr[l].receiver){

        }
        else if(TodoArr[l].receiver){

        }else{
            newList.push(TodoArr[l])
        }
    }

    if (newList.length > 0) {
        var result = groupBy(newList, function (item) {
            return [item.assignedTo];
        });
        for (var i = 0; i < result.length; i++) {
            sendToDoNotifyEmailToReceiver(userId, result[i], meetingObj, todoSrt.acceptedSlot, todoSrt.timezone, domain + '/meeting/' + todoSrt.invitationId, creatorName,true)
        }
    }

    userManagements.findUserProfileByIdWithCustomFields(userId, {
        emailId: 1,
        firstName: 1,
        lastName: 1,
        timezone: 1
    }, function (error, user) {
        if (common.checkRequired(user)) {
            sendNotifyEmailForTodo(TodoArr, user, meetingObj, todoSrt.timezone, todoSrt.acceptedSlot, domain + '/meeting/' + todoSrt.invitationId, creatorName,true,true);
        }
    });
}

function updateOldToDo(oldTodo,meetingObj,userId,acceptedSlot,timezone,meetingUrl){
    for(var t=0; t<oldTodo.length; t++){
        createOrUpdateTodo(oldTodo[t],false,meetingObj.invitationId,userId,meetingObj,acceptedSlot,timezone,meetingUrl);
    }
}

function updateToDoStatus(statusToDo,meetingObj,userId,acceptedSlot,timezone,meetingUrl){
    meetingClassObj.updateMeetingToDoStatus(statusToDo,meetingObj.invitationId,function(success){
        if(success){

            if(statusToDo.deuDateUpdated){
                mongoScheduleObj.updateSchedule(new mongoose.Types.ObjectId(statusToDo._id),{after:new Date(statusToDo.dueDate).toISOString()});
                googleCalendar.updateToDoEvent(statusToDo.createdBy,statusToDo,meetingObj.invitationId,statusToDo.updateCount,statusToDo.googleEventId,function(success){

                });
            }
        }
    })
}

function arrayFromObject(obj) {
    var arr = [];
    for (var i in obj) {
        arr.push(obj[i]);
    }
    return arr;
}

function groupBy(list, fn) {
    var groups = {};
    for (var i = 0; i < list.length; i++) {
        var group = JSON.stringify(fn(list[i]));
        if (group in groups) {
            groups[group].push(list[i]);
        } else {
            groups[group] = [list[i]];
        }
    }
    return arrayFromObject(groups);
}

/**/

router.post('/meetings/todo/delete',function(req,res){
    var reqData = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    if(common.checkRequired(reqData.toDoId) && common.checkRequired(reqData.invitationId) && common.checkRequired(reqData.googleEventId)){
        googleCalendar.deleteToDoEvent(userId,reqData.googleEventId,function(isSuccess){

            if(isSuccess){

                meetingClassObj.removeToDoItem(userId,reqData.invitationId,reqData.toDoId,function(isRemoved){
                    if(isRemoved){
                        mongoScheduleObj.removeSchedule(new mongoose.Types.ObjectId(reqData.toDoId));
                    }
                    res.send(isRemoved);
                });

            }else res.send(false);
        })
    }
    else if(!common.checkRequired(reqData.googleEventId) && common.checkRequired(reqData.toDoId) && common.checkRequired(reqData.invitationId)){
        meetingClassObj.removeToDoItem(userId,reqData.invitationId,reqData.toDoId,function(isRemoved){
            res.send(isRemoved);
        })
    }
    else{
        res.send(false);
    }
});

router.post('/meetings/todo/update/status',function(req,res){
    var reqData = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(common.checkRequired(reqData.toDoId) && common.checkRequired(reqData.invitationId) && common.checkRequired(reqData.status)){
        reqData.userId = userId;
        meetingClassObj.updateMeetingToDoStatus(reqData,function(isUpdated){
            res.send(isUpdated);
        })
    }else res.send(false);
});

function createOrUpdateTodo(toDoObj,isNew,invitationId,userId,meetingObj,acceptedSlot,timezone,meetingUrl){
    meetingClassObj.addOrUpdateMeetingToDo(toDoObj,isNew,invitationId,function(isSuccess,notify,isUpdate,todoDoc,updateGoogleEvent){

         if(isSuccess && notify && !isUpdate){
             // newly inserted
             scheduleTODOExpire({invitationId:invitationId,toDoId:toDoObj._id});
             googleCalendar.createToDoEvent(userId,toDoObj,invitationId,function(success){

             });
         }
         else if(isSuccess && isUpdate && notify){

             if(updateGoogleEvent){
                 mongoScheduleObj.updateSchedule(new mongoose.Types.ObjectId(toDoObj._id),{after:new Date(toDoObj.dueDate).toISOString()});
                 googleCalendar.updateToDoEvent(userId,toDoObj,invitationId,toDoObj.updateCount,toDoObj.googleEventId,function(success){

                 });
             }
         }
    })
}

function sendToDoNotifyEmailToReceiver(userId,toDoObjArr,meetingObj,acceptedSlot,timezone,meetingUrl,creatorName,isUpdate){
    if(common.checkRequired(toDoObjArr) && toDoObjArr.length > 0 && toDoObjArr[0].assignedTo != userId){
        userManagements.findUserProfileByIdWithCustomFields(toDoObjArr[0].assignedTo,{emailId:1,firstName:1,lastName:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                sendNotifyEmailForTodo(toDoObjArr,user,meetingObj,timezone,acceptedSlot,meetingUrl,creatorName,isUpdate,false);
            }
        })
    }
}

function sendNotifyEmailForTodo(toDoObjArr,user,meetingObj,timezone,acceptedSlot,meetingUrl,creatorName,isUpdate,isSender){
   if(common.checkRequired(toDoObjArr) && toDoObjArr.length > 0){
       var timezoneProfile;
       if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
           timezoneProfile = user.timezone.name;
       }
       else timezoneProfile = timezone;

       var maxSlot;
       if(acceptedSlot == 'none'){
           maxSlot = meetingObj.scheduleTimeSlots.reduce(function (a, b) {
               return new Date(a.start.date) < new Date(b.start.date) ? a : b;
           });
       }
       else{
           maxSlot = meetingObj.scheduleTimeSlots[acceptedSlot];
       }
       var name = user.firstName+' '+user.lastName;
       var a = toDoObjArr.length > 1 ? "tasks" : "task";
       var b = isUpdate ? "updated" : "assigned";
       var mailContent = "";

       mailContent = !isSender ?  creatorName+" "+b+" the following "+a+" to "+name+" for the meeting <b>'"+maxSlot.title+"'</b>.<br>" : "Following "+b+" tasks to recipients for the meeting <b>'"+maxSlot.title+"'</b> has been sent:<br>";

       for(var i=0; i<toDoObjArr.length; i++){
           var start = moment(toDoObjArr[i].dueDate).tz(timezoneProfile || 'UTC');
           var date = start.format("Do MMMM");
           var time = start.format("h:mm a z");
           mailContent += (i+1)+". "+toDoObjArr[i].actionItem+" - "+date+" : "+time+"<br>"
       }
       mailContent += "For more details please visit the <a href="+meetingUrl+">meeting page</a>.";

       var notifyObj3 = {
           toName:user.firstName+' '+user.lastName,
           subject:'Relatas: Meeting ToDo',
           emailId:user.emailId,
           mailContent:mailContent
       };
       emailSenders.sendMeetingCancelOrDeletionMail(notifyObj3,"Meeting ToDo");
   }
}

function scheduleTODOExpire(details){

    meetingClassObj.findOneCustomQuery({invitationId:details.invitationId,"toDo._id":details.toDoId},{"toDo.$":1},function(doc){

        if(common.checkRequired(doc) && common.checkRequired(doc.toDo) && doc.toDo.length > 0){

            var scheduleObj = {
                invitationId:details.invitationId,
                eventName:doc.toDo[0]._id,
                collectionName:'meeting',
                after:new Date(doc.toDo[0].dueDate).toISOString(),
                data:{
                    type:'todo',
                    id:doc.toDo[0]._id
                }
            };

            mongoScheduleObj.scheduleMeetingTimeExpireEvent(scheduleObj);
        }
    })
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
}

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }else
    {

        return res.redirect('/');
    }
}

module.exports = router;