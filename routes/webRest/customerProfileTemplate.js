//**************************************************************************************
// File Name            : customerProfileTemplate
// Functionality        : Router for customerProfileTemplates(Quotation/Purchase Order/Invoice)
// History              : First Version
//
//
//**************************************************************************************

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var _ = require("lodash");
var fs = require('fs');

var commonUtility = require('../../common/commonUtility');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var contactClass = require('../../dataAccess/contactsManagementClass');
var rabbitmq = require('../../common/rabbitmq');
var customerProfileTemplatesManagementClass = require('../../dataAccess/customerProfileTemplateManagement');
var customerProfileTemplatesCollection = require('../../databaseSchema/customerProfileTemplateSchema').customerProfileTemplateModel;

var customerProfileTemplatesManagementClassObj = new customerProfileTemplatesManagementClass();
var common = new commonUtility();
var rabbitmqObj = new rabbitmq();
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var statusCodes = errorMessagesObj.getStatusCodes();

// ui routes
router.get('/customerProfileTemplates/show/all',common.isLoggedInUserToGoNext, function(req, res){
    res.render('customerProfiles/corporateAdmin/showAllCustomerProfileTemplates');
});

router.get('/customerProfileTemplates/create/new',common.isLoggedInUserToGoNext, function(req, res){
    res.render('customerProfiles/corporateAdmin/createCustomerProfileTemplate');
});

router.get('/customerProfile/create/new',common.isLoggedInUserToGoNext, function(req, res){
    res.render('customerProfiles/corporateUser/createCustomerProfile');
});


router.post("/customerProfileTemplate/create/new",  function (req, res) {
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var companyId = common.castToObjectId( userProfile.companyId);

    console.log("Backend received for customerProfileTemplate",req.body)
    customerProfileTemplatesManagementClassObj.createNewCustomerProfileTemplate(companyId, req.body, function(error, savedTemplate) {
        if(error) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        } else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Template: savedTemplate
            })
        }
    })
});

router.post("/customerProfileTemplates/updateAll",  function (req, res){
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);
    var objCustomerProfileTemplateId = common.castToObjectId( req.body.customerProfileTemplateId);

    console.log("in updateAll",req.body.customerProfileTemplateId)

    customerProfileTemplatesManagementClassObj.updateCustomerProfileTemplateAll(objCompanyId,
        objCustomerProfileTemplateId,
        req.body.customerProfileTemplateType,
        req.body.customerProfileTemplateName,
        req.body.customerProfileTemplateElementList,
        req.body.isCustomerProfileTemplateDeactivated,
        req.body.customerProfileTemplateUiId,
        req.body.customerProfileTemplateAttrList,
        function(err, result) {
            if(err) {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                })
            } else {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                })
            }
        });
});

router.post("/customerProfileTemplates/updateIsCustomerProfileTemplateDeactivated",  function (req, res){
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    var objCompanyId = common.castToObjectId(companyId);
    var templateType = req.body.customerProfileTemplateType;
    var objCustomerProfileTemplateId = common.castToObjectId(req.body.templateId);
    var isDeactivated = req.body.isTemplateDeactivated;

    customerProfileTemplatesManagementClassObj.updateIsTemplateDeactivated(
                objCompanyId,
                templateType,
                objCustomerProfileTemplateId,
                isDeactivated,
                function(err, result) {
                    if(err) {
                        res.send({
                            SuccessCode: 0,
                            ErrorCode: 1,
                        })
                    } else {
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                        })
                    }
                });
});

router.post("/customerProfileTemplates/updateCustomerProfileTemplateAsDefault",  function (req, res){
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    var objCompanyId = common.castToObjectId(companyId);
    var templateType = req.body.customerProfileTemplateType;
    var objCustomerProfileTemplateId = common.castToObjectId(req.body.templateId);
    var isDefault = req.body.isDefault;

    customerProfileTemplatesManagementClassObj.updateCustomerProfileTemplateAsDefault(objCompanyId,
                templateType,
                objCustomerProfileTemplateId,
                isDefault,
                function(err, result) {
                    if(err) {
                        res.send({
                            SuccessCode: 0,
                            ErrorCode: 1,
                        })
                    } else {
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                        })
                    }
                });
});

router.get('/customerProfileTemplates/get/all/data', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    var objCompanyId = common.castToObjectId(companyId);

    console.log("In ")
    customerProfileTemplatesManagementClassObj.getAllCustomerProfileTemplates(
                objCompanyId,
                function(err, customerProfileTemplates){
                if(err) {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1
                    })
                }
                else {
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data: customerProfileTemplates

                    })
                }
            });
});

router.post('/customerProfileTemplates/getById', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);
    var objCustomerProfileTemplateId = common.castToObjectId(req.body.customerProfileTemplateId);

    console.log("Back end /customerProfileTemplates/getById ",objCustomerProfileTemplateId )
    customerProfileTemplatesManagementClassObj.getCustomerProfileTemplateById(
                objCompanyId,
                objCustomerProfileTemplateId,
                function(err, customerProfileTemplates){
                    if(err) {
                        res.send({
                            SuccessCode: 0,
                            ErrorCode: 1
                        })
                    }
                    else {
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            Data: customerProfileTemplates

                        })
                    }
                });
});

router.get('/customerProfileTemplates/getCustomerProfileTemplateTypes', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);

    customerProfileTemplatesManagementClassObj.getAllCustomerProfileTemplateTypes(
                objCompanyId,
                function(err,templateTypes) {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    DataTypes: templateTypes
                });
            });
});

router.post('/customerProfileTemplates/getAllCustomerProfileTemplateNamesByType', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);
    var templateType = req.body.customerProfileTemplateType;

    customerProfileTemplatesManagementClassObj.getAllCustomerProfileTemplateNamesByType(
        objCompanyId,
        templateType,
        function(err,templateNames) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                TemplateNames: templateNames
            });
        });
});

router.post('/customerProfileTemplates/getAllCustomerProfileTemplateAttributes', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);
    var templateType = req.body.customerProfileTemplateType;
    var templateName = req.body.customerProfileTemplateName;

    console.log("getAllCustomerProfileTemplateAttributes", templateType, templateName );
    customerProfileTemplatesManagementClassObj.getAllCustomerProfileTemplateAttributeNames(
        objCompanyId,
        templateType,
        templateName,
        function(err, templateAttributes) {
            console.log("Template templateAttributes", templateAttributes )
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                TemplateAttributes: templateAttributes
            });
        });
});
module.exports = router;