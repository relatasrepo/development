
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');
var request=require('request');
var _ = require("lodash");
var gcal = require('google-calendar');
var simple_unique_id = require('simple-unique-id');

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactions = require('../../dataAccess/interactionManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var contactManagementClass = require('../../dataAccess/contactManagement');
var contactClassSupport = require('../../common/contactsSupport');
var appCredentials = require('../../config/relatasConfiguration');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var ipLocatorJS = require('../../common/ipLocator');
var winstonLog = require('../../common/winstonLog');
var googleContactImage = require('../../common/googleContactsImages');
var SocialManagement = require("../../dataAccess/socialManagement")
var opportunityManagement = require('../../dataAccess/opportunitiesManagement');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var MessageManagement = require('../../dataAccess/messageManagementV2');
var taskSupportClass = require('../../common/taskSupportClass');
var officeOutlookApi = require('../../common/officeOutlookAPI');

var oppManagementObj = new opportunityManagement();
var ipLocatorObj = new ipLocatorJS();
var validation = new validations();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var contactSupportObj = new contactClassSupport();
var appCredential = new appCredentials();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionObj = new interactions();
var googleContactImageObj = new googleContactImage();
var socialManagement = new SocialManagement();
var company = new companyClass();
var messageManagementObj = new MessageManagement();
var contactManagementObj = new contactManagementClass();
var taskSupportClassObj = new taskSupportClass();
var officeOutlook = new officeOutlookApi();
var logLib = new winstonLog();
var logger =logLib.getWinston();

var statusCodes = errorMessagesObj.getStatusCodes();

// var memwatch = require('memwatch-next');
//memwatch.setup();
/* 
memwatch.on('leak', function(info) {
    logger.info('Memory leak detected: ', info)
});

memwatch.on('stats', function(stats) {
    logger.info('Memory stats detected: ', stats);
});
 */
router.get('/contacts/left/bar/template',function(req,res){
   res.render('secondSeries/leftContactsList');
});

router.get('/contacts/left/bar/template/v2',function(req,res){
   res.render('secondSeries/leftContactsListV2');
});

router.get('/contacts/list',function(req,res){
   res.render('secondSeries/leftContactsListPrototype');
});

router.get('/contacts/landing', common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render("contact/landing");
});

router.get('/contact/connect',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('contact/connect',{isLoggedIn : common.isLoggedInUserBoolean(req),page:"contactConnect"});
});

router.get('/contact/selected', common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render("contact/contact");
});

router.get('/contact/create/template', common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render("contact/createContact");
});

router.get('/contact/create/template/v2', common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render("contact/createContactV2");
});

router.get('/contacts/all', common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render("contact/contactPrototype");
});

router.get('/contacts/filter/common/web', common.isLoggedInUserOrMobile, function (req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);

    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

    var fetchWithMobile;
    if(req.query.fetchWithMobile == 'yes'){
        fetchWithMobile = true
    } else {
        fetchWithMobile = false
    }

    if (common.checkRequired(req.query.id) && req.query.id != 'null') {
        contactSupportObj.getCommonContacts_web(userId, req.query.id, skip, limit, fetchWithMobile,function (response) {
            res.send(response)
        })

    } else {
        res.send(errorObj.generateErrorResponse({
            status: statusCodes.MESSAGE,
            key: 'NO_COMMON_CONNECTIONS_FOUND'
        }));
    }
});

router.get('/contacts/filter/user/location',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if (common.checkRequired(user)) {
            user._id = common.castToObjectId(userId);
            var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
            var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
            var ip = ipLocatorObj.getIP(req);

            ipLocatorObj.lookup(ip,null,function(locationObj){
                if(locationObj){
                    var location = common.getLocation(null,locationObj);

                    if(common.checkRequired(location)){
                        interactionIndividualCountByLocation(userId,user,skip,limit,location,locationObj,req,res,user.emailId)
                    }
                    else{
                        var locationUser = common.getLocation(user.location,user.currentLocation);
                        user._id = common.castToObjectId(userId);
                        if(common.checkRequired(locationUser)){
                            interactionIndividualCountByLocation(userId,user,skip,limit,locationUser,null,req,res,user.emailId)
                        }
                        else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'NO_LOCATION_DETAILS_FOUND'},errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION")));
                    }
                }
                else{
                    var locationUser2 = common.getLocation(user.location,user.currentLocation);
                    if(common.checkRequired(locationUser2)){
                        interactionIndividualCountByLocation(userId,user,skip,limit,locationUser2,null,req,res,user.emailId)
                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'NO_LOCATION_DETAILS_FOUND'},errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION")));
                }
            })
        }
        else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION")));
    });
});

function interactionIndividualCountByLocation(userId,user,skip,limit,location,locationObj,req,res,emailId){
    var timezone;
    if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
        timezone = user.timezone.name;
    } else timezone = 'UTC';

    var date = moment().tz(timezone).format()
    contactObj.getContactsMatchedLocation(common.castToObjectId(userId),emailId,location,null,function(eror,contacts,userIdList,emailIdList){
        if(emailIdList && emailIdList.length > 0){
            interactionObj.interactionIndividualCountByLocation(common.castToObjectId(userId),user.emailId,emailIdList,location,null,date,true,function(contacts){
                var total = contacts.length;
                var results = contacts.splice(skip,limit);
                var resObj = {
                };

                if(total == 0){
                    resObj = {
                        "SuccessCode": 0,
                        "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION"),
                        "ErrorCode": 1,
                        "Data":{
                            total:total,
                            skipped:skip,
                            limit:limit,
                            returned:results.length,
                            contacts:results,
                            location:location,
                            timezone:timezone
                        }
                    };
                }
                else resObj = {
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        total:total,
                        skipped:skip,
                        limit:limit,
                        returned:results.length,
                        contacts:results,
                        location:location,
                        timezone:timezone
                    }
                };

                if(!common.checkRequired(locationObj)){
                    resObj.Data.isNewLocation = false;
                }
                else if(isUserCurrentLocation(user,locationObj)){
                    resObj.Data.isNewLocation = false;
                }
                else{
                    resObj.Data.isNewLocation = true;
                }
                res.send(resObj);
            });
        }
        else {
            res.send({
                "SuccessCode": 0,
                "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION"),
                "ErrorCode": 1,
                "Data":{
                    total:0,
                    skipped:skip,
                    limit:limit,
                    returned:0,
                    contacts:[],
                    location:location,
                    timezone:timezone
                }})
        }
    });
}

router.get('/contacts/filter/common/companyName/web', common.isLoggedInUserToGoNext, function (req, res) {
    var userId = common.getUserId(req.user);
    if (common.checkRequired(req.query.companyName)) {
        var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
        var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

        var fetchWithMobile;
        if(req.query.fetchWithMobile == 'yes'){
            fetchWithMobile = true
        } else {
            fetchWithMobile = false
        }

        contactSupportObj.getCommonContactsByEmailIdWithinCompany(userId,req.query.emailId,req.query.fetchWith,req.query.companyName,fetchWithMobile,function(contacts){
            var total = contacts.length;
            var results = contacts.splice(skip, limit);

            if(results.length > 0){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": {
                        total: total,
                        skipped: skip,
                        limit: limit,
                        returned: results.length,
                        contacts: results
                    }
                });
            }
            else res.send(errorObj.generateErrorResponse({
                status: statusCodes.MESSAGE,
                key: 'NO_COMMON_CONNECTIONS_FOUND'
            }));
        })

    } else res.send(errorObj.generateErrorResponse({
        status: statusCodes.MESSAGE,
        key: 'NO_COMMON_CONNECTIONS_FOUND'
    }));
});

router.get('/contacts/filter/common/companyName/web/v2', common.isLoggedInUserOrMobile, function (req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);

    if (['DEV', 'LOCAL','LOCAL1'].indexOf(appCredential.getServerEnvironment()) != - 1 && (String(userId) == "541c83d89ff44f767a23c546" || String(userId) == "5979e51467f719c418d669ca")) {

        var results = [
            {
                "_id": "5836a82e4430bca33f4221e9",
                "personId": {
                    "_id": "5836a82e4430bca33f4221e9",
                    "publicProfileUrl": "naveenpaul3",
                    "mobileNumber": null,
                    "companyName": "ExampleDev",
                    "designation": "VP Sales",
                    "profilePicUrl": "/profileImages/naveenpaul3.jpg",
                    "lastName": "Jones",
                    "firstName": "Neil"
                },
                "personName": "Neil Jones",
                "personEmailId": "naveenpaul@relatas.com",
                "companyName": "ExampleDev",
                "designation": "Dev",
                "mobileNumber": null,
                "contactRelation": {
                    "decisionmaker_influencer": null,
                    "prospect_customer": null
                },
                "favorite": false,
                "twitterUserName": null,
                "facebookUserName": null,
                "linkedinUserName": null,
                "addedDate": "2016-12-17T07:32:25.562Z",
                "source": "google-login",
                "hashtag": ["Something"],
                "ownerId": "541c83d89ff44f767a23c546"
            },{
                "_id": "56f660482f7b2a1976c24ea5",
                "personId": {
                    "_id": "56f660482f7b2a1976c24ea5",
                    "publicProfileUrl": "ajitmoily1",
                    "mobileNumber": null,
                    "companyName": "ExampleDev",
                    "designation": "VP Engineering",
                    "profilePicUrl": "/profileImages/ajitmoily1.jpeg",
                    "lastName": "Doily",
                    "firstName": "Bill"
                },
                "personName": "Bill Doily",
                "personEmailId": "ajitmoily@relatas.com",
                "companyName": "ExampleDev",
                "designation": "Dev",
                "mobileNumber": null,
                "contactRelation": {
                    "decisionmaker_influencer": null,
                    "prospect_customer": null
                },
                "favorite": false,
                "twitterUserName": null,
                "facebookUserName": null,
                "linkedinUserName": null,
                "addedDate": "2016-12-17T07:32:25.562Z",
                "source": "google-login",
                "hashtag": ["Something"],
                "ownerId": "541c83d89ff44f767a23c546"
            },{
                "_id": "56f660482f7b2a1976c24ea5",
                "personId": {
                    "_id": "56f660482f7b2a1976c24ea5",
                    "publicProfileUrl": "ajitmoily1",
                    "mobileNumber": null,
                    "companyName": "ExampleDev",
                    "designation": "VP Engineering",
                    "profilePicUrl": "/profileImages/ajitmoily1.jpeg",
                    "lastName": "Doily",
                    "firstName": "Bill"
                },
                "personName": "Bill Doily",
                "personEmailId": "ajitmoily@relatas.com",
                "companyName": "ExampleDev",
                "designation": "Dev",
                "mobileNumber": null,
                "contactRelation": {
                    "decisionmaker_influencer": null,
                    "prospect_customer": null
                },
                "favorite": false,
                "twitterUserName": null,
                "facebookUserName": null,
                "linkedinUserName": null,
                "addedDate": "2016-12-17T07:32:25.562Z",
                "source": "google-login",
                "hashtag": ["Something"],
                "ownerId": "541c83d89ff44f767a23c546"
            }
        ]

        var newArray = common.removeDuplicates_field(results, 'personEmailId')

        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data": {
                total: 2,
                skipped: 0,
                limit: 12,
                returned: newArray.length,
                contacts: newArray
            }
        });

    } else {

        if (common.checkRequired(req.query.companyName)) {
            var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
            var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

            var fetchWithMobile;
            if(req.query.fetchWithMobile == 'yes'){
                fetchWithMobile = true
            } else {
                fetchWithMobile = false
            }

            contactSupportObj.getCommonContactsByEmailIdWithinCompany(userId,req.query.emailId,req.query.fetchWith,req.query.companyName,fetchWithMobile,function(contacts){
                var total = contacts.length;
                var results = contacts.splice(skip, limit);

                if(results.length > 0){

                    var newArray = common.removeDuplicates_field(results, 'personEmailId')
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": {
                            total: total,
                            skipped: skip,
                            limit: limit,
                            returned: newArray.length,
                            contacts: newArray
                        }
                    });
                }
                else res.send(errorObj.generateErrorResponse({
                    status: statusCodes.MESSAGE,
                    key: 'NO_COMMON_CONNECTIONS_FOUND'
                }));
            })

        } else res.send(errorObj.generateErrorResponse({
            status: statusCodes.MESSAGE,
            key: 'NO_COMMON_CONNECTIONS_FOUND'
        }));
    }

});

function isUserCurrentLocation(user,currentLocation){
    if(common.checkRequired(user) && common.checkRequired(user.currentLocation)){
        if(user.currentLocation.city == currentLocation.city){
            return true;
        }else return false;
    }else return false;
}

router.get('/contacts/filter/losingtouch/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if (common.checkRequired(user)) {
            var timezone;
            if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                timezone = user.timezone.name;
            } else timezone = 'UTC';

            var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
            var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
            contactSupportObj.getContactsLosingTouch(common.castToObjectId(userId),user,timezone,'list',skip,limit,true,function(resObj){
                res.send(resObj);
            })
        }
        else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });
});

var contactTypes = [
    'all',
    'losing_touch',
    'favorite',
    'recently_added',
    'prospect',
    'customer',
    'decision_maker',
    'influencer',
    'search'
];

router.get('/contacts/delete',common.isLoggedInUserOrMobile,function(req,res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var contacts = []
    
    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{contacts:0},function(error,userProfile){
        var companyId = userProfile ? common.castToObjectId(userProfile.companyId) : null;
        if(req.query.contacts){
            contacts = req.query.contacts.split(',')
        }
    
        if(contacts.length>0){
            contactObj.dntContactsRemove(common.castToObjectId(userId),common.castListToObjectIds(contacts),function (err,result) {
                res.send({
                    err:err,
                    result:result
                })
            })
        } else {
            res.send({
                err:"NO CONTACTS",
                result:null
            })
        }
    })

});

router.get('/contacts/start/tracking',common.isLoggedInUserOrMobile,function(req,res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    var contacts = [];
    
    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{contacts:0},function(error,userProfile){
        var companyId = userProfile ? common.castToObjectId(userProfile.companyId) : null;
        
        if(req.query.contacts){
            contacts = req.query.contacts.split(',')
        }
    
        if(contacts.length>0){
            contactObj.startTracking(common.castToObjectId(userId),contacts,function (err,result) {
                res.send({
                    err:err,
                    result:result
                })
            })
        } else {
            res.send({
                err:err,
                result:null
            })
        }
    })
    

});

router.get('/contacts/filter/custom/web',common.isLoggedInUserOrMobile,function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var type = 'recentlyInteracted';
    
    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{contacts:0},function(error,userProfile){
        var companyId = userProfile?common.castToObjectId(userProfile.companyId):null;
        if(common.checkRequired(req.query.filterBy)){
            type = req.query.filterBy;
        }
        else {
            // return error
            return res.send(errorObj.generateErrorResponse({
                status: statusCodes.INVALID_REQUEST_CODE,
                key: 'INVALID_CONTACT_TYPE'
            }));
        }
    
        var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
        var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
    
        switch (type){
            case 'all':contactSupportObj.getAllContacts(common.castToObjectId(userId),skip,limit,function(error,contacts){
    
                if(error){
                    res.send(errorObj.generateErrorResponse(error));
                }
                else{
                    res.send(contacts)
                }
            });
                break;
            case 'search':
                var searchByEmailId = req.query.searchByEmailId == 'yes';
                contactSupportObj.searchUserContacts(req.query.searchContent,searchByEmailId,userId,skip,limit,function(error,results){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_IN_SEARCH'}));
                }
                else{
                    res.send(results);
                }
            });
                break;
            case 'losing_touch':
                common.getProfileOrStoreProfileInSession(userId,req,function(user){
                    if (common.checkRequired(user)) {
                        var timezone;
                        if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                            timezone = user.timezone.name;
                        }else timezone = 'UTC';
    
                        contactSupportObj.getContactsLosingTouch(userId,user,timezone,'list',skip,limit,false,function(resObj){
                            res.send(resObj);
                        })
                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                });
                break;
            case 'favorite':
                contactSupportObj.getFavoriteContacts(common.castToObjectId(userId),skip,limit,req.query.searchContent,function(resObj){
                res.send(resObj);
            });
                break;
            case 'recentlyInteracted':
                contactSupportObj.getRecentlyInteractedContacts(common.castToObjectId(userId),skip,limit,req.query.searchContent,companyId,function(resObj){
                    res.send(resObj);
                });
                break;
            case 'hashtag':
                contactSupportObj.getHashtagContacts(common.castToObjectId(userId),skip,limit,req.query.searchContent,function(resObj){
                    res.send(resObj);
                });
                break;
            case 'dnt':
                contactObj.getDntContacts(common.castToObjectId(userId),skip,limit,req.query.searchContent,function(resObj){
                    res.send(resObj);
                });
                break;
            case 'recently_added':
                common.getProfileOrStoreProfileInSession(userId,req,function(user){
                    if (common.checkRequired(user)) {
    
                        var timezone;
                        if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                            timezone = user.timezone.name;
                        }else timezone = 'UTC';
                        contactSupportObj.getNewContacts(userId,user,timezone,'list',skip,limit,function(error,resObj){
                            if(error){
                                res.send(errorObj.generateErrorResponse(error));
                            }
                            else
                                res.send(resObj);
                        })
                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                });
                break;
            default:
                var arrTypes = [];
                if(common.contains(type,',')){
                    arrTypes = type.split(',')
                }
                else arrTypes = [type]
                contactSupportObj.getContactsByType(common.castToObjectId(userId),arrTypes,null,skip,limit,req.query.searchContent,function(resObj){
                    res.send(resObj);
                });
                break;
        }
    })
    
});

router.get('/contacts/type/interacted/count',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user) {
        if (common.checkRequired(user)) {

            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';

            contactSupportObj.getCountsOfContactsByTypeInteracted(userId,timezone,function(emails,obj){
                req.session.past7DayInteractedContactTyeEmails = emails;
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":obj
                });
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });
});

router.get('/contacts/type/interacted/list',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var type = '';
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

    if(common.checkRequired(req.query.contactType)){
        if(contactTypes.indexOf(req.query.contactType) == -1){
            // return error
            return res.send(errorObj.generateErrorResponse({
                status: statusCodes.INVALID_REQUEST_CODE,
                key: 'INVALID_CONTACT_TYPE'
            }));
        }
        else{
            type = req.query.contactType;
        }
    }

    if(common.checkRequired(type)){
        if(common.checkRequired(req.session.past7DayInteractedContactTyeEmails) && req.session.past7DayInteractedContactTyeEmails.length > 0){
            contactSupportObj.getContactsByType(common.castToObjectId(userId),[type],req.session.past7DayInteractedContactTyeEmails,skip,limit,null,function(resObj){
                res.send(resObj);
            });
        }
        else{

            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user) {
                if (common.checkRequired(user)) {

                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';


                    var dateMin = moment().tz(timezone);
                    dateMin.date(dateMin.date() - 7)
                    dateMin.hour(0)
                    dateMin.minute(0)
                    dateMin.second(0)
                    dateMin.millisecond(0)

                    interactionObj.getInteractedEmailsDate(userId,dateMin,function(emails){
                        if(emails.length > 0){
                            req.session.past7DayInteractedContactTyeEmails = emails;
                            contactSupportObj.getContactsByType(common.castToObjectId(userId),[type],emails,skip,limit,null,function(resObj){
                                res.send(resObj);
                            });
                        }
                        else{
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":[]
                            })
                        }
                    })

                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }
    }
    else{
        res.send(errorObj.generateErrorResponse({
            status: statusCodes.INVALID_REQUEST_CODE,
            key: 'INVALID_CONTACT_TYPE'
        }));
    }
});

router.post('/contacts/update/reltionship/partner',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
    var type = req.body.type;
    var value = req.body.value;

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        contactObj.updateRelationshipPartner(common.castToObjectId(userId),common.castToObjectId(req.body.contactId),req.body.relationKey,type,value,function(err,isSuccess){
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{}
            });
        });

    } else {
        res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")))
    }
});

router.post('/contacts/update/cxo',common.isLoggedInUserToGoNext,function(req,res) {
    var userId = common.getUserId(req.user);

    contactObj.updateCXO(common.castToObjectId(userId),common.castToObjectId(req.body.contactId),req.body.value,function(err,isSuccess){
        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data":{}
        });
    });

});

router.post('/contacts/update/reltionship/type',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        if(common.checkRequired(req.body.contactId)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var type = req.body.type;
                    contactObj.updateRelationshipType(user._id,common.castToObjectId(req.body.contactId),req.body.relationKey,type,function(err,isSuccess){
                        if(err){
                            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
                        }
                        else if(isSuccess){
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{}
                            });
                        }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_CONTACT_ID_FOUND'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
});

router.post('/opportunities/contacts/update/reltionship/type',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        if(common.checkRequired(req.body.contactId)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var type = req.body.type;
                    var relation = req.body.relation;
                    var value = req.body.value;

                    if(type === null && req.body.relationKey === 'decisionmaker_influencer'){
                        oppManagementObj.checkOpportunitiesForPeople(common.castToObjectId(userId),null,null,relation,req.body.contactId,function(error,opportunities){
                            if(opportunities.length == 0){
                                contactObj.updateRelationshipType(user._id,common.castToObjectId(req.body.contactId),req.body.relationKey,type,value,function(err,isSuccess){
                                    if(err){
                                        res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
                                    }
                                    else if(isSuccess){
                                        res.send({
                                            "SuccessCode": 1,
                                            "Message": "",
                                            "ErrorCode": 0,
                                            "Data":{}
                                        });
                                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
                                })
                            } else {
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":{}
                                });
                            }
                        });

                    } else {
                        contactObj.updateRelationshipType(user._id,common.castToObjectId(req.body.contactId),req.body.relationKey,type,value,function(err,isSuccess){
                            if(err){
                                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
                            }
                            else if(isSuccess){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":{}
                                });
                            }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
                        })
                    }
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_CONTACT_ID_FOUND'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
});

router.post('/contacts/update/relationship/type/by/email',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        if(common.checkRequired(req.body.email)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var type = req.body.type;
                    contactObj.updateRelationshipTypeByEmail(user._id,req.body.email,req.body.relationKey,type,function(err,isSuccess){
                        if(err){
                            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
                        }
                        else if(isSuccess){
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{}
                            });
                        }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_CONTACT_ID_FOUND'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("UPDATE_CONTACT_FAVORITE_FAILED")));
});

router.post('/contacts/update/favorite/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if(common.checkRequired(req.body.contactId)){
                userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                    if(common.checkRequired(user)){
                        var status = common.checkRequired(req.body.favorite) ? req.body.favorite : false;
                        contactObj.updateFavorite(user._id,common.castToObjectId(req.body.contactId),status,function(err,isSuccess){
                            if(err){
                                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
                            }
                            else if(isSuccess){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":{}
                                });
                            }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
                        })
                    }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                });
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_CONTACT_ID_FOUND'}));
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

router.post('/contacts/update/relationship/strength/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if(common.checkRequired(req.body.contactId) && common.checkRequired(req.body.relationshipStrength_updated)) {
                contactObj.updateRelationshipStrength(userId, common.castToObjectId(req.body.contactId), req.body.relationshipStrength_updated, function (err, isSuccess) {
                    if (err) {
                        res.send(errorObj.generateErrorResponse({
                            status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                            key: 'UPDATE_DB_ERROR'
                        }));
                    }
                    else if (isSuccess) {
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {}
                        });
                    } else res.send(errorObj.generateErrorResponse({
                        status: statusCodes.UPDATE_FAILED,
                        key: 'INVALID_DATA_RECEIVED'
                    }));
                })
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_CONTACT_ID_FOUND'}));
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

router.get('/contacts/get/profile/:idType/:id/contactspage',common.isLoggedInUserToGoNext,function(req,res){
    var id = req.params.id;
    var idType = req.params.idType;
    var userId = common.getUserId(req.user);
    req.session.interactionPage = null;
    if(!common.checkRequired(req.session.interactionPage)){
        req.session.interactionPage = {};
    }
    //req.session.interactionPage[id] = null;
    if(common.checkRequired(id) && common.checkRequired(idType) && (idType == 'userId' || idType == 'emailId')){
        var sessionData = req.session.interactionPage[id];
        if(common.checkRequired(sessionData)){
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":sessionData
            });
        }
        else{
            var data = {
                profile:null,
                lastInteraction:null,
                contact:null
            };
            common.getProfileOrStoreProfileInSession(userId,req,function(user){
                if (common.checkRequired(user)) {
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';
                    var date = moment().tz(timezone).format();
                    interactionObj.lastInteractionWithContact(userId,id,idType == 'emailId',date,1,function(interactions){

                        if(common.checkRequired(interactions) && interactions.length > 0){
                            data.lastInteraction = interactions[0];
                        }else
                            data.lastInteraction = null;

                        var query;
                        if(idType == 'emailId'){
                            query = {emailId:id};
                        }
                        else if(idType == 'userId'){
                            query = {_id:id};
                        }
                        else{
                            return res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}));
                        }

                        userManagementObj.selectUserProfileCustomQuery(query,{firstName:1,lastName:1,emailId:1,publicProfileUrl:1,location:1,currentLocation:1,mobileNumber:1,companyName:1,designation:1,profilePicUrl:1},function(error,user){
                            contactSupportObj.getSingleContactRelation(userId,id,idType == 'emailId',!common.checkRequired(user), null,function(obj){
                                if(common.checkRequired(user)){

                                    data.profile = JSON.parse(JSON.stringify(user));
                                    data.profile.userUrl = common.getValidUniqueUrl(user.publicProfileUrl);
                                }
                                data.contact = obj;

                                req.session.interactionPage[id] = data;
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":data
                                });
                            });
                        })
                    })
                }
                else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}));
});

router.get('/contacts/get/profile/:idType/:id',common.isLoggedInUserToGoNext,function(req,res){
    var id = req.params.id;
    var idType = req.params.idType;
    var userId = common.getUserId(req.user);
    req.session.interactionPage = null;
    if(!common.checkRequired(req.session.interactionPage)){
        req.session.interactionPage = {};
    }
    //req.session.interactionPage[id] = null;
    if(common.checkRequired(id) && common.checkRequired(idType) && (idType == 'userId' || idType == 'emailId')){
        var sessionData = req.session.interactionPage[id];
        if(common.checkRequired(sessionData)){
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":sessionData
            });
        }
        else{
            var data = {
                profile:null,
                lastInteraction:null,
                contact:null
            };
            common.getProfileOrStoreProfileInSession(userId,req,function(user){
                if (common.checkRequired(user)) {
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';
                    var date = moment().tz(timezone).format();


                        var query;
                        if(idType == 'emailId'){
                            query = {emailId:id};
                        }
                        else if(idType == 'userId'){
                            query = {_id:id};
                        }
                        else{
                            return res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}));
                        }

                        userManagementObj.selectUserProfileCustomQuery(query,{firstName:1,lastName:1,emailId:1,publicProfileUrl:1,location:1,currentLocation:1,mobileNumber:1,companyName:1,designation:1,profilePicUrl:1,serviceLogin:1},function(error,user){
                            contactSupportObj.getSingleContactRelation(userId,id,idType == 'emailId',!common.checkRequired(user), null,function(obj){
                                if(common.checkRequired(user)){

                                    data.profile = JSON.parse(JSON.stringify(user));
                                    data.profile.userUrl = common.getValidUniqueUrl(user.publicProfileUrl);
                                }
                                data.contact = obj;
                                var dateMin = moment().tz(timezone);
                                dateMin.date(dateMin.date() - 7);
                                dateMin = dateMin.format();
                                var dateMax = moment().tz(timezone);
                                interactionObj.interactionWithContactByTypes(userId,id,false,dateMin,dateMax.format(),null,user.serviceLogin,function(typeCounts){
                                    if(common.checkRequired(typeCounts) && typeCounts.length > 0){
                                        data.interactionTypes = typeCounts.typeCounts
                                    }
                                    else data.interactionTypes = [];
                                    req.session.interactionPage[id] = data;
                                    res.send({
                                        "SuccessCode": 1,
                                        "Message": "",
                                        "ErrorCode": 0,
                                        "Data":data
                                    });
                                });
                            });
                        })

                }
                else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}));
});

router.get('/contacts/details/:participantEmailId/relationship',common.isLoggedInUserToGoNext,function(req,res){

    var participantEmailId = req.params.participantEmailId;
    var userId = common.getUserId(req.user);
    var mobileNumber = req.query.mobileNumber

    if(common.checkRequired(participantEmailId)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,twitter:1,facebook:1,linkedin:1,serviceLogin:1},function(error,user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var showSetup = true;
                if((common.checkRequired(user.linkedin) && common.checkRequired(user.linkedin.id)) || (common.checkRequired(user.twitter) && common.checkRequired(user.twitter.id)) || (common.checkRequired(user.facebook) && common.checkRequired(user.facebook.id))){
                    showSetup = false;
                }

                var data = {showSocialSetup:showSetup};
                if(req.query.fetchProfile == 'yes'){
                    userManagementObj.findUserProfileByEmailIdWithCustomFields(participantEmailId,{publicProfileUrl:1},function(error,pUser){
                        data.profile = pUser;

                        getRelationship(userId,user.emailId,participantEmailId,data,timezone,req,res,mobileNumber,user.serviceLogin);
                    })
                }
                else getRelationship(userId,user.emailId,participantEmailId,data,timezone,req,res,mobileNumber,user.serviceLogin);
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });
    }
    else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'EMAIL_ID_NOT_FOUND_IN_REQUEST'}));
});

router.post('/contacts/update/value/type/contactId',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        contactObj.updateContactValueContactId(common.castToObjectId(userId),req.body,function(result){
            if(!result){
                res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}))
            } else {
                res.send(result);
            }
        })
    }
});

router.post('/contacts/update/value/type',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        contactObj.updateContactValue(common.castToObjectId(userId),req.body,function(result){
            if(!result){
                res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}))
            } else {
                res.send(result);
            }
        })
    }
});

router.post('/contacts/create',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,userProfile) {
            contactObj.createContact(common.castToObjectId(userId),userProfile.emailId,req.body,function(err,result){
                var account = common.fetchCompanyFromEmail(req.body.personEmailId)
                if(account && account.toLowerCase() != "other"){
                    contactManagementObj.updateAccountWhenContactAdded(common.castToObjectId(userId));
                }
                if(!result){
                    res.send(false)
                } else {
                    res.send(true);
                }
            })
        })
    }
});

router.post('/contacts/remindtoconnect',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        contactObj.updateRemindToConnect(common.castToObjectId(userId),req.body,function(result){
            if(!result){
                res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}))
            } else {
                userManagementObj.getUserBasicProfile(common.castToObjectId(userId),function (err,user) {

                    var task ={
                        taskName:"Connect with: "+req.body.contact,
                        assignedToEmailId:user.emailId,
                        createdBy:user._id,
                        createdByEmailId:user.emailId,
                        dueDate:new Date(req.body.remind),
                        createdDate:new Date(),
                        taskFor:"other",
                        priority:"high",
                        description:"Reminder set to connect with: "+req.body.contact,
                        refId:null,
                        participants:[{
                            emailId:user.emailId
                        }]
                    }

                    var meetingDetails = {
                        title:"Connect with: "+req.body.contact,
                        end:new Date(moment(req.body.remind).add(30,"min")),
                        start:new Date(req.body.remind),
                        description:"Reminder set to connect with: "+req.body.contact,
                        locationType:"",
                        location:"",
                    }

                    taskSupportClassObj.createNewTask_v2(task,user,false,function(err,response){
                        updateCalendars(user.google.length>0 && user.outlook.length == 0,meetingDetails,user,function(err_cal,result){
                        });
                        res.send(result);
                    });
                });
            }
        })
    }
});

function updateCalendars(isGoogle,meetingDetails,user,callback){

    var id = simple_unique_id.generate('googleRelatas');
    var gId = 'relat'+common.getNumString(0)+id;

    var request_body = {
        "id": gId,
        "summary": meetingDetails.title,
        "end": {
            "dateTime": meetingDetails.end
        },
        "start": {
            "dateTime": meetingDetails.start
        },
        "description": meetingDetails.description,
        "location": meetingDetails.locationType +" : "+meetingDetails.location,
        "locationType": meetingDetails.locationType,
        "locationDetails": meetingDetails.location,
        "relatasEvent": true,
        "attendees": [{
            email:user.emailId,
            displayName:user.firstName+' '+user.lastName,
            responseStatus: 'accepted'
        }]
    };

    if(isGoogle){
        createGoogleEvent(user,request_body,callback)
    } else {
        writeMeetingOnOutlookCalendar(user.outlook[0].refreshToken, meetingDetails, user.timezone, callback)
    }
}

function createGoogleEvent(user,request_body,callback) {

    common.getNewGoogleToken(user.google[0].token,user.google[0].refreshToken,function (newAccessToken) {
        if (newAccessToken) {
            var google_calendar = new gcal.GoogleCalendar(newAccessToken);
            google_calendar.events.insert('primary', request_body,{sendNotifications:true}, function (err, result) {
                callback(err, result);
            });
        } else {
            callback(false,false);
        }
    });
};

function writeMeetingOnOutlookCalendar(refreshToken,meetingDetails,timezone,callback){

    var attendees = [];

    _.each(meetingDetails.toList,function (attendee) {
        attendees.push({
            "EmailAddress": {
                "Address": attendee.receiverEmailId,
                "Name": attendee.receiverFirstName +' '+ attendee.receiverLastName
            },
            "Type": "Required"
        })
    });

    var meeting = {
        "Subject": meetingDetails.title,
        "Body": {
            "ContentType": "Text",
            "Content": meetingDetails.description
        },
        "Start": {
            "DateTime": meetingDetails.start,
            "TimeZone": timezone.name?timezone.name:'UTC'
        },
        "End": {
            "DateTime": meetingDetails.end,
            "TimeZone": timezone.name?timezone.name:'UTC'
        },
        "Attendees": attendees,
        "Location":{
            "DisplayName":meetingDetails.location+":"+meetingDetails.locationType
        }
    };

    officeOutlook.getNewAccessTokenWithoutStore(refreshToken,function (newToken) {
        officeOutlook.writeMeetingOnCalendarMSGraphAPI(newToken.access_token,meeting,callback);
    });
}

router.post('/save/contact/details',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){

        var updateObj = {
            'contacts.$.personName':req.body.personName?req.body.personName:null,
            'contacts.$.designation':req.body.designation?req.body.designation:null,
            'contacts.$.companyName':req.body.companyName?req.body.companyName:null,
            'contacts.$.location':req.body.location?req.body.location:null,
            'contacts.$.lat':req.body.lat?req.body.lat:null,
            'contacts.$.lng':req.body.lng?req.body.lng:null,
            'contacts.$.mobileNumber':req.body.mobileNumber?req.body.mobileNumber:null,
            'contacts.$.personEmailId':req.body.personEmailId?req.body.personEmailId:null
        }

        var personEmailId = req.body.personEmailId?req.body.personEmailId:null;

        var contactId = req.body.contactId?common.castToObjectId(req.body.contactId):null;

        var matchByEmail = req.body.matchByEmail;

        if(personEmailId || req.body.contactId){

            contactObj.updateContactDetails(common.castToObjectId(userId),personEmailId,contactId,matchByEmail,updateObj,function(err,result){
                if(!result){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SAVE_DOCUMENT_ERROR'},errorMessagesObj.getMessage("UPDATE_CONTACT_DETAILS_FAILED")));
                } else {

                    contactObj.updateLatLngContactsCollection(common.castToObjectId(userId),personEmailId,
                        req.body.mobileNumber,
                        req.body.lat,
                        req.body.lng,
                        req.body.location,
                        req.body.designation)

                    var account = common.fetchCompanyFromEmail(personEmailId)
                    if(account && account.toLowerCase() != "other"){
                        contactManagementObj.updateAccountWhenContactAdded(common.castToObjectId(userId));
                    }

                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":null
                    });
                }
            })

        } else {
            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SAVE_DOCUMENT_ERROR'},errorMessagesObj.getMessage("UPDATE_CONTACT_DETAILS_FAILED")));
        }
    }
});

router.get('/contacts/values/:participantEmailId',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var contactEmailId = req.params.participantEmailId;
    var contactMobile = req.query.mobile;

    contactObj.getContactValues(common.castToObjectId(userId),contactEmailId,contactMobile,function(result){
        if(result){
            res.send(result)

        } else {
            res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}))
        }
    })

});

router.get('/getContactImage/:emailId/:contactImageLink',common.isLoggedInUserToGoNext,function(req,res){
    var emailId = req.params.emailId;
    
    var contactImageLink = decodeURIComponent(req.params.contactImageLink);
    var userId = common.getUserId(req.user);

    if(emailId){
        fs.access('./public/profileImages/'+emailId, fs.F_OK, function(err) {
            if (!err) {
                googleContactImageObj.sendContactImage(res,emailId);
            } else if(contactImageLink && contactImageLink != 'null' || !req.params.contactImageLink){

                res.send((emailId.substr(0,2)).toUpperCase())

                common.getProfileOrStoreProfileInSession(userId,req,function(error,userProfile) {
                    if(userProfile && userProfile.google.length>0){
                        common.getNewGoogleToken(userProfile.google[0].token,userProfile.google[0].refreshToken,function (access_token) {
                            if(access_token){
                                googleContactImageObj.getContactProfileImageFromGoogle(contactImageLink,access_token, emailId,function (result) {
                                    //Download the image now and re-use it next time.

                                })
                            }
                        });
                    }
                })
            } else{
                res.send((emailId.substr(0,2)).toUpperCase())
            }
        });
    } else {
        res.send((emailId.substr(0,2)).toUpperCase())
    }
});

router.get('/save/twitterContactImage',common.isLoggedInUserToGoNext,function (req,res) {

    var emailId = req.query.emailId
    var url = req.query.imageUrl
    var imageName = emailId

    fs.access('./public/profileImages/'+emailId, fs.F_OK, function(err) {
        if (!err) {
            googleContactImageObj.sendContactImage(res,emailId);
        } else {
            //Save image
            request.get({url:url, encoding: 'binary'},function (err, response, body) {

                fs.writeFile('./public/profileImages/' + imageName, body, 'binary', function (err) {

                    if (err){
                        res.send((imageName.substr(0,2)).toUpperCase())
                    }
                    else {
                        googleContactImageObj.sendContactImage(res,emailId);
                    }
                });
            });
        }
    })
})

function getRelationship(userId,emailId,participantEmailId,data,timezone,req,res,mobileNumber,serviceLogin){

    var isEmail = true;

    if(!validateEmail(participantEmailId)) {
        isEmail = false
    }

    contactObj.getSingleContactRelation(userId,participantEmailId,isEmail,false, mobileNumber, function(contact){

        if(common.checkRequired(contact)){
            data.relation = contact;
            getInteractionsByTypeCount(userId,participantEmailId,data,timezone,req,res,mobileNumber,serviceLogin)
        }
        else{
            data.relation = null;
            getInteractionsByTypeCount(userId,participantEmailId,data,timezone,req,res,mobileNumber,serviceLogin)
        }
    })
}

function getInteractionsByTypeCount(userId,participantEmailId,data,timezone,req,res,mobileNumber,serviceLogin){

    var dateMin = moment().tz(timezone);
    dateMin.date(dateMin.date() - 7);
    dateMin.hours(0)
    dateMin.minutes(0)
    dateMin.seconds(0)
    dateMin = dateMin.format();
    var dateMax = moment().tz(timezone).format();
    interactionObj.interactionWithContactByTypes(common.castToObjectId(userId),participantEmailId,false,null,dateMax,mobileNumber,serviceLogin,function(typeCounts){

        if(common.checkRequired(typeCounts) && typeCounts.length > 0){
            data.interactionTypes = common.formatInteractionTypeGraphData(typeCounts)
        }
        else data.interactionTypes = [];

        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data":data
        });
    })
}

router.get('/contact/connections/interactions', common.isLoggedInUserToGoNext, function (req, res) {

    var userId = common.getUserId(req.user);

    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

    var fetchWithMobile = false;
    var id = req.query.id
    var twitterUserName = req.query.twitterUserName
    var date = new Date()
    var userIdOrEmailId = "emailId";

    if(!common.validateEmail(id)){
        userIdOrEmailId = ''
    }

    if (common.checkRequired(req.query.id) && req.query.id != 'null') {
        contactSupportObj.getCommonContacts_web(userId, id, skip, limit, fetchWithMobile,function (response) {

            var ccEmailIds = _.pluck(response.Data.contacts,'personEmailId')
            
            interactionObj.lastInteractionWithContact(common.castToObjectId(userId),ccEmailIds,"emailId",date,1,function(ccLastInteraction){

                interactionObj.lastInteractionWithContact(common.castToObjectId(userId),id,userIdOrEmailId,date,1,function(interactions){

                    if(ccLastInteraction && ccLastInteraction[0]){
                        _.each(response.Data.contacts,function (contact) {
                            if(contact.personEmailId === ccLastInteraction[0].emailId){
                                var splitName = contact.personName.split(" ")
                                ccLastInteraction[0]["firstName"] = splitName[0]
                                ccLastInteraction[0]["lastName"] = splitName[1]
                            }
                        })
                    }

                    if(interactions[0] && interactions[0].type){
                        interactions[0].type = interactions[0].type ==="meeting-comment"?"email":interactions[0].type;
                    }

                    socialManagement.fetchTweet(twitterUserName, function (err, data) {

                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":{
                                commonConnections:response.SuccessCode?response.Data.total:0,
                                lastInteraction:interactions?interactions:null,
                                latestTweet:data?data:null,
                                profile:req.session.profile,
                                ccLastInteraction:ccLastInteraction
                            }
                        })
                    });
                });
            });
        });
    } else {
        res.send(errorObj.generateErrorResponse({
            status: statusCodes.MESSAGE,
            key: 'NO_CONTACT_FOUND'
        }));
    }
});

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

router.get('/check/empty/object', common.isLoggedInUserToGoNext, function (req, res) {

    var obj1 = {};
    var obj2 = {a:1};

    res.send({
        obj1:common.validObject(obj1),
        obj2:common.validObject(obj2)
    })

})

router.get('/contact/get/all/data', common.isLoggedInUserToGoNext, function (req, res) {

    var participantEmailId = req.query.participantEmailId && req.query.participantEmailId !='null'?req.query.participantEmailId:null;
    var companyId = req.query.companyId && req.query.companyId !='null'?req.query.companyId:null;
    var userId = common.getUserId(req.user);
    var mobileNumber = req.query.mobileNumber

    common.getProfileOrStoreProfileInSession(userId,req,function(user){

        if (common.checkRequired(participantEmailId) || common.checkRequired(mobileNumber)) {
            var timezone = 'UTC';
            if (common.checkRequired(req.query.timezone)) {
                timezone = req.query.timezone;
            }

            if (req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name) {
                timezone = req.session.profile.timezone.name
            }

            var dateMin = moment().tz(timezone).format();
            getLIUData(userId,companyId,function (companyDetails) {
                interactionObj.getInteractionsByDateTimeline(common.castToObjectId(userId), participantEmailId, true, 5, dateMin, mobileNumber,user.serviceLogin, function (interactions) {
                    
                    oppManagementObj.getOpportunitiesByContact(common.castToObjectId(userId),participantEmailId,mobileNumber,function(error,opportunities){
                        messageManagementObj.getNotesForContact(common.castToObjectId(userId),participantEmailId,function (err,notes) {
                            if(error){
                                res.send(false)
                            } else{
                                var totalPipelineVal = 0;
                                var tpvLost = 0;
                                var tpvWon = 0;
                                _.each(opportunities,function (opp) {
                                    if(!opp.isClosed || !opp.isWon){
                                        totalPipelineVal += opp.amount;
                                    }

                                    if(opp.isWon){
                                        tpvWon += opp.amount;
                                    }

                                    if(!opp.isWon && opp.isClosed){
                                        tpvLost += opp.amount;
                                    }
                                });

                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":{
                                        interactions:interactions,
                                        opportunities:opportunities,
                                        notes:notes,
                                        timezone:timezone,
                                        companyDetails:companyDetails
                                    }
                                })
                            }
                        })
                    })
                })
            });

        } else {
            res.send(false)
        }  
    })
    
});

function checkPrevMailHasBeenReplied (messages,userEmailId) {

    var data = _
        .chain(messages)
        .groupBy("emailId")
        .map(function(value, key) {
            var sent = 0,received = 0,hasReplied = false;

            value.sort(function (o2, o1) {
                return new Date(o1.interactionDate) > new Date(o2.interactionDate) ? 1 : new Date(o1.interactionDate) < new Date(o2.interactionDate) ? -1 : 0;
            });

            var lastButOneSender = null;

            if(value && value.length>1){
                lastButOneSender = value[1]?value[1]:null
            }

            if(value && value.length == 1){
                lastButOneSender = value[0]
            }

            _.each(value,function (val) {
                if((val.emailId == userEmailId && action == "sender") || val.emailId != userEmailId && action == "receiver"){
                    sent++
                } else {
                    received++
                }
            })

            if(lastButOneSender && lastButOneSender.emailId == userEmailId && lastButOneSender.action == "sender"){
                hasReplied = true
            }

            if(lastButOneSender && lastButOneSender.emailId != userEmailId && lastButOneSender.action == "receiver"){
                hasReplied = true
            }

            return {
                emailThreadId:key,
                hasReplied:hasReplied,
                lastButOneSender:lastButOneSender,
                cEmailIds:lastButOneSender.emailId,
                received:received,
                sent:sent
            };

            // return {
            //     emailThreadId:key,
            //     lastButOneSender:lastButOneSender,
            //     data:value
            // }

        })
        .value();

    return data;
}

router.get('/contact/get/details', common.isLoggedInUserToGoNext, function (req, res){

    var emailId = req.query.emailId;
    var userEmailId = req.query.userEmailId;
    var userId = common.getUserId(req.user);
    userId = common.castToObjectId(userId)

    if(userEmailId){
        userId = userEmailId
    }

    if(emailId && userId){

        contactSupportObj.searchUserContact(emailId,true,userId,0,1,function(error,results) {
            if (error) {
                res.send(errorObj.generateErrorResponse({
                    status: statusCodes.PROFILE_ERROR_CODE,
                    key: 'ERROR_IN_SEARCH'
                }));
            }
            else {
                res.send(results);
            }
        });
    } else {
        res.send(errorObj.generateErrorResponse({
            status: statusCodes.PROFILE_ERROR_CODE,
            key: 'ERROR_IN_SEARCH'
        }));
    }

});

router.get('/contact/set/contactname', common.isLoggedInUserToGoNext, function (req, res){

    var companyId = req.query.companyId;

    userManagementObj.getTeamMembers(common.castToObjectId(companyId),function (err,team) {
        var teamUserIds = _.pluck(team, "emailId");

        contactObj.getContactsWithLIUEmailId(teamUserIds,function (err,data) {

            contactObj.updatePersonNameForMultiUsers(data,function (err,data) {
                res.send({
                    err:data
                });
            });
        });
    });

});

router.get('/contact/script/set/important', common.isLoggedInUserToGoNext, function (req, res){

    oppManagementObj.getAllOpportunities(function (err,opps) {
        var userGroup = _
            .chain(opps)
            .groupBy('userEmailId')
            .map(function(value, key) {
                var obj = {};
                obj["emailId"] = key;
                var contacts = [];
                _.each(value,function (el) {
                    contacts.push(_.pluck(el.partners,"emailId"))
                    contacts.push(_.pluck(el.influencers,"emailId"))
                    contacts.push(_.pluck(el.decisionMakers,"emailId"))
                    contacts.push(el.contactEmailId)
                });

                obj["contacts"] = _.union(_.flatten(contacts))
                return obj
            }).value();

        var userGroupForRelationship = _
            .chain(opps)
            .groupBy('userEmailId')
            .map(function(value, key) {
                var obj = {};
                obj["emailId"] = key;
                var influencers = [];
                var dms = [];
                var partners = [];
                var customers = [];
                var prospects = [];

                _.each(value,function (el) {

                    partners.push(_.pluck(el.partners,"emailId"))
                    influencers.push(_.pluck(el.influencers,"emailId"))
                    dms.push(_.pluck(el.decisionMakers,"emailId"))

                    if(el.stageName === 'Close Won' || el.stageName === 'Closed Won'){
                        customers.push(el.contactEmailId)
                    } else {
                        prospects.push(el.contactEmailId)
                    }
                });

                prospects = prospects.filter( function( el ) {
                    return customers.indexOf( el ) < 0;
                });

                obj["contacts"] = {
                    partners:_.union(_.flatten(partners)),
                    dms:_.union(_.flatten(dms)),
                    influencers:_.union(_.flatten(influencers)),
                    customers:_.union(_.flatten(customers)),
                    prospects:_.union(_.flatten(prospects))
                }
                return obj
            }).value();

        contactObj.updateRelationshipForMultiUsers(userGroupForRelationship,function (err,result) {

            res.send({
                opp:opps.length,
                err:err,
                relationship:result,
            })
        });
    });

});

router.post('/contact/add/to/invalid/list', common.isLoggedInUserToGoNext, function (req, res){

    var temp = req.body.list;
    var list = [];
    if(temp.length > 0) {
        temp.forEach(function (a) {
            if(a != "") {
                list.push(new RegExp(a, 'i'));
            }
        });

        common.insertValidOrInvalidEmail(list, req.body.type, function (result) {
            res.send(result)
        });
    }
    else{
        res.send(false)
    }

});

function getLIUData(userId,companyId,callback){
    if(companyId){
        company.findCompanyById(companyId,function(companyInfo){
            callback(companyInfo)
        });
    } else {
        callback(null)
    }
}

module.exports = router;
