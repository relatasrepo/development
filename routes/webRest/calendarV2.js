/**
 * Created by elavarasan on 20/10/15.
 */
var express = require('express');
var calendar = require('../../databaseSchema/calendarListSchema');
var events = require('../../databaseSchema/calendarSchema');
var scheduleInvitation = require('../../databaseSchema/userManagementSchema').scheduleInvitation;
var commonUtility = require('../../common/commonUtility');
var common=new commonUtility();
var router = express.Router();
router.route('/v2/calendar')
    .get(function(req,res){
        calendar.findOne({email:'arasan1289@gmail.com'},function (err, calendarList) {
            if (!err) {
                console.log(calendarList);
                return res.send(calendarList);
            } else {
                res.statusCode = 500;
                console.log(res.send({error: err}));
            }
        });
    })
    .post(function (req, res) {
        var calendarList = new calendar(req.body);
        calendarList.save(function (err, calendarList) {
            if (!err) {
                res.send(calendarList)
            } else {
                res.statusCode = 500;
                return res.send({error: err});
            }
        })
    });

router.get('/v2/calendar/:startDate',common.isLoggedInUserToGoNext,function (req, res) {
        var userId=common.getUserId(req.user);
        var s=Number(req.params.startDate);
        var startDate=new Date(s);
        var endDate=new Date(s);
        endDate.setDate(startDate.getDate()+7);
        var ds=startDate.getDate(),ms=startDate.getMonth()+1,ys=startDate.getFullYear();
        var de=endDate.getDate(),me=endDate.getMonth()+1,ye=endDate.getFullYear();
        calendar.find({'userId':userId}).exec(function(err,lists){
            if(err){
                res.statusCode=500;
                console.log(err);
            }
            events.find({
                'scheduleTimeSlots.start.date': {
                    $gte:new Date(ys+'-'+ms+'-'+ds),
                    $lte:new Date(ye+'-'+me+'-'+de)
                },
                $or:[{'to.receiverId':userId},{'senderId':userId}],
                deleted:{$ne:true}
            },function(e,events){
                if(!e){
                    res.send({'lists':lists,events:events});
                }else{
                    res.statusCode = 500;
                    console.log(e);
                }
            })
        });
    });

router.route('/v2/calendar/:id')
    .get(function (req, res) {
        return calendar.findById(req.params.id).populate('calendar').exec(function (err, calendarList) {
            if (!calendarList) {
                res.statusCode = 404;
                return res.send({error: 'Not found'});
            }
            if (!err) {
                return res.send(calendarList);
            } else {
                res.statusCode = 500;
                console.log('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({error: 'Server error'});
            }
        })
    })
    .put(function (req, res) {
        return calendar.findById(req.params.id, function (err, calendarList) {
            if (!calendarList) {
                res.statusCode = 404;
                return res.send({error: 'Not found'});
            }
            for (var prop in req.body) {
                calendarList[prop] = req.body[prop];
            }
            return calendarList.save(function (err, updatedList) {
                if (!err) {
                    res.statusCode = 201;
                    res.send(updatedList);
                } else {
                    res.statusCode = 500;
                    res.send({error: 'Server error'});
                }
            });
        });
    });
router.route('/v2/calendar/:id/events')
    .get(function (req, res) {
        events.find({_list: req.params.id}, function (err, events) {
            if (!err) {
                res.send(events);
            } else {
                res.statusCode = 500;
                res.send({error: err});
            }
        })
    })
    .post(function (req, res) {
        var event = new events(req.body);
        event._list = req.params.id;
        event.save(function (err, event) {
            if (!err) {
                calendar.findById(req.params.id,function(err,calendarList){
                    if(!err){
                        calendarList.calendar.push(event._id);
                        calendarList.save(function(err,list){
                            if(!err){
                                res.send(event);
                            }
                            else{
                                res.statusCode = 500;
                                res.send({error: err});
                            }
                        })
                    }
                })
            } else {
                res.statusCode = 500;
                res.send({error: err});
            }
        })
    });
router.route('/v2/calendar/changeList')
    .post(function (req, res) {
        events.findById(req.body.eventId,function(err,event){
            event._list=req.body.listId;
            event.save(function(err,event){
                if(!err){
                    res.send(event);
                }else{
                    res.statusCode = 500;
                    res.send({error: err});
                }
            })
        })
    });
router.route('/v2/calendar/events/:id')
    .get(function(req,res){
        events.findById(req.params.id,function(err,event){
            if(!err){
                res.send(event);
            }else{
                res.statusCode = 500;
                res.send({error: err});
            }
        })
    })
    .put(function(req,res){
       events.findById(req.params.id,function(err,event){
           if (!event) {
               res.statusCode = 404;
               return res.send({error: 'Not found'});
           }
           for (var prop in req.body) {
               event[prop] = req.body[prop];
           }
           event.save(function (err, updatedevent) {
               if (!err) {
                   res.statusCode = 201;
                   res.send(updatedevent);
               } else {
                   res.statusCode = 500;
                   res.send({error: err});
               }
           });
       })
    });
module.exports = router;