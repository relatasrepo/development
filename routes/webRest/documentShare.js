//**************************************************************************************
// File Name            : documents
// Functionality        : Router for documents Share
// History              : First Version -> renamed from documents in routes/webRest
//
//
//**************************************************************************************

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var documentManagement = require('../../dataAccess/documentManagement');
var interactions = require('../../dataAccess/interactionManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var messageDataAccess = require('../../dataAccess/messageManagement');
var aws = require('../../common/aws');
var winstonLog = require('../../common/winstonLog');
var documentSupport = require('../../common/documentSupport');
var meetingSupportClass = require('../../common/meetingSupportClass');
var validations = require('../../public/javascripts/validation');
var emailSender = require('../../public/javascripts/emailSender');
var massMailCountManagement = require('../../dataAccess/massMailCountManagement');

var massMailCountManagements = new massMailCountManagement();
var validation = new validations();
var emailSenderObj = new emailSender();
var AWSObj = new aws();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var documentSupportObj = new documentSupport();
var meetingSupportClassObj = new meetingSupportClass();
var interactionObj = new interactions();
var common = new commonUtility();
var userManagementObj = new userManagement();
var documentManagementObj = new documentManagement();
var contactObj = new contactClass();
var messageDataAccessObj = new messageDataAccess();
var logLib = new winstonLog();

var logger =logLib.getFilesErrorLogger();
var statusCodes = errorMessagesObj.getStatusCodes();

function extractEmails (text)
{
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

router.post('/document/share/new',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var invitationId = req.query.invitationId;
    var participants = req.query.participants;
    if(common.checkRequired(participants)){
        participants = extractEmails(participants);
        if(!common.checkRequired(participants))
            participants = []

    }else participants = [];

    if(common.checkRequired(req.busboy)){
        req.pipe(req.busboy);
        req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {

            if(common.contains(mimetype,'pdf')){
                file.fileRead = [];

                file.on('data', function(chunk) {
                    // Push chunks into the fileRead array
                    this.fileRead.push(chunk);
                });

                file.on('error', function(err) {
                    logger.info('Error in /document/upload/new: documents web router ',err);
                    res.send(errorObj.generateErrorResponse({status: statusCodes.FILE_ERROR_CODE, key: 'FILE_UPLOAD_ERROR'}));
                });

                file.on('end', function() {

                    // Concat the chunks into a Buffer
                    var finalBuffer = Buffer.concat(this.fileRead);
                    var now = moment();
                    var timeStamp = now.year() + ':' + now.month() + ':' + now.date() + ':' + now.hour() + ':' + now.minute() + ':' + now.second();
                    var dIdentity = timeStamp + '_' + filename;

                    var docNameTimestamp = dIdentity.replace(/\s/g, '-');
                    AWSObj.uploadFileToAWS(finalBuffer,filename,docNameTimestamp,"bucket",mimetype,function(error,response){
                        if(error){
                            res.send(error);
                        }
                        else if(common.checkRequired(response) && common.checkRequired(response.awsKey) && common.checkRequired(response.fileName)&& common.checkRequired(response.fileUrl)){
                            if(common.checkRequired(invitationId)){
                                meetingSupportClassObj.getParticipantListForDocument(userId,invitationId,function(newParticipants){
                                    participants = participants.concat(newParticipants);
                                    participants = common.removeDuplicate_id(participants,true);
                                    storeDocument(userId,response,participants,true,invitationId,req,res);
                                })
                            }
                            else{
                                storeDocument(userId,response,participants,false,null,req,res);
                            }
                        }
                        else res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG'}));
                    });
                    //res.send('')
                });
            }
            else{
                res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_IMAGE_FILE_TYPE'}));
            }
        });
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'NO_FILES_RECEIVED'}));
});

function storeDocument(userId,response,participants,isMeeting,invitationId,req,res){
    documentSupportObj.storeNewDocument(userId,{documentName:response.fileName,documentUrl:response.fileUrl,awsKey:response.awsKey},participants,isMeeting,invitationId,function(error,result){
        if(error){
            res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SAVE_DOCUMENT_ERROR'}));
        }
        else{
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": result
            });
        }
    })
}

// document share types = 'message', 'meeting', 'people'
router.post('/document/share/existing',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        if(common.checkRequired(req.body.documentId)){
            documentManagementObj.documentByIdCustomFields(req.body.documentId,{documentName:1,documentUrl:1,awsKey:1},function(error,doc){
                if(error){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                }
                else if(common.checkRequired(doc)){
                    if(req.body.shareIn =='meeting'){
                        if(common.checkRequired(req.body.invitationId)){
                            meetingSupportClassObj.getParticipantListForDocument(userId,req.body.invitationId,function(newParticipants){
                                var participants = [];

                                if(common.checkRequired(newParticipants) && newParticipants.length > 0){
                                    participants = newParticipants;
                                }
                                var docInfoForMeeting =  {
                                    documentId: doc._id,
                                    documentName: doc.documentName,
                                    documentUrl: doc.documentUrl,
                                    addedBy:userId,
                                    addedOn: new Date()
                                };
                                shareInMeeting(userId,participants,req.body.invitationId,doc,docInfoForMeeting,req,res);
                            })
                        }
                        else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVITATION_ID_NOT_FOUND'}));
                    }
                    else if(common.checkRequired(req.body.shareWith) && req.body.shareWith.length > 0){
                        if(req.body.shareIn =='people'){
                            documentSupportObj.shareDocument(userId,doc._id,req.body.shareWith,true,req.body.message,req.body.subject,req.body.headerImage,req.body.imageUrl,req.body.imageName,function(error,doc){
                                if(error){
                                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                                }
                                else res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data": {}
                                });
                            })
                        }
                        else if(req.body.shareIn =='message'){
                            documentSupportObj.shareDocument(userId,doc._id,req.body.shareWith,false,req.body.message,req.body.subject,req.body.headerImage,req.body.imageUrl,req.body.imageName,function(error,doc,shareWith){
                                if(error){
                                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                                }
                                else if(common.checkRequired(doc)){
                                    var document = {
                                        documentId:doc._id,
                                        documentName:doc.documentName,
                                        documentUrl:doc.documentUrl
                                    };
                                    sendMessagesWithDoc(userId,shareWith,document,req.body.message,req.body.subject,req.body.resend,req.body.headerImage,req.body.imageUrl,req.body.imageName);
                                    res.send({
                                        "SuccessCode": 1,
                                        "Message": "",
                                        "ErrorCode": 0,
                                        "Data": {}
                                    });
                                }
                            })
                        }
                        else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_SHARE_IN_TYPE'}));
                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'DOCUMENT_SHARE_LIST_NOT_FOUND'}));
                }
                else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'DOCUMENT_NOT_FOUND'}));
            });
        }
        else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'DOCUMENT_ID_NOT_FOUND'}));
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

function shareInMeeting(userId,participants,invitationId,doc,docInfoForMeeting,req,res){
    if(participants.length > 0){
        userManagementObj.selectUserProfilesCustomQuery({emailId:{$in:participants}},{emailId:1,firstName:1,lastName:1},function(error,users){
            if(error){
                logger.info('Error in shareInMeeting():document web router ',error);
                res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG'}));
            }
            else{
                var shareWith = [];
                for(var j=0; j<users.length; j++){
                    shareWith.push({
                        userId:users[j]._id,
                        emailId:users[j].emailId,
                        firstName:users[j].firstName+' '+users[j].lastName,
                        accessStatus:true,
                        sharedOn:new Date()
                    })
                }
                if(participants.length != shareWith.length){
                    for(var k=0; k<participants.length > 0; k++){
                        var isExist = false;
                        for(var l=0; l<shareWith.length; l++){
                            if(shareWith[l].emailId == participants[k]){
                                isExist = true;
                            }
                        }
                        if(!isExist){
                            shareWith.push({
                                userId:'',
                                emailId:participants[k],
                                firstName:'',
                                accessStatus:true,
                                sharedOn:new Date()
                            })
                        }
                    }
                }
                documentSupportObj.shareDocument(userId,doc._id,shareWith,true,req.body.message,req.body.subject,req.body.headerImage,req.body.imageUrl,req.body.imageName,function(error,doc){
                    if(error){
                        res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SHARING_DOCUMENT_FAILED'}));
                    }
                    else{
                        documentSupportObj.addDocumentToMeeting(invitationId,doc,docInfoForMeeting,shareWith,function(error,doc){
                            if(error){
                                res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'UPDATE_MEETING_DOC_FAILED'}));
                            }
                            else res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data": {}
                            });
                        })
                    }
                })
            }
        })
    }
    else res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG'}));
}

function sendMessagesWithDoc(userId,shareWith,doc,message,subject,resend,headerImage,imageUrl,imageName){
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{firstName:1,lastName:1,emailId:1},function(err,user){
        if(common.checkRequired(user)){
            for(var i=0; i<shareWith.length; i++){
                var contact = {
                    personId:shareWith[i].userId,
                    personName:shareWith[i].firstName,
                    personEmailId:shareWith[i].emailId,
                    count:1,
                    lastInteracted:new Date(),
                    addedDate: new Date(),
                    verified:true,
                    relatasContact:true,
                    relatasUser:true
                };

                var interaction = {};
                interaction.userId = userId;
                interaction.action = 'sender';
                interaction.type = 'document-share';
                interaction.subType = 'document-share';
                interaction.refId =  doc.documentId;
                interaction.title = doc.documentName;
                interaction.source = 'relatas';
                interaction.description = message || '';

                var interactionReceive = {};
                interactionReceive.userId = contact.personId || '';
                interactionReceive.action = 'receiver';
                interactionReceive.emailId = contact.personEmailId;
                interactionReceive.type = 'document-share';
                interactionReceive.subType = 'document-share';
                interactionReceive.refId = doc.documentId;
                interactionReceive.title = doc.documentName;
                interactionReceive.source = 'relatas';
                interactionReceive.description = message || '';

                common.storeInteractionReceiver(interactionReceive);
                common.storeInteraction(interaction);

                updateContact(userId,contact);
                if(common.checkRequired(contact.personId)){
                    updateReceiverContacts(userId,contact.personId);
                }

                var dataToSend = {
                    emailId:contact.personEmailId,
                    receivedByFirstName:contact.personName || '',
                    sharedWithName:contact.personName || '',
                    url:'/readDocument/'+doc.documentId,
                    sharedByEmail:user.emailId,
                    sharedByFirstName:user.firstName+' '+user.lastName,
                    userMsg:message,
                    subject:subject,
                    headerImage:headerImage,
                    imageUrl:imageUrl,
                    imageName:imageName

                };

                if(!common.checkRequired(contact.personId)){
                    dataToSend.url = 'accessDocByEmail/'+contact.personEmailId+'/'+doc.documentId
                    emailSenderObj.sendShareDocMailToNonRelatasUser(dataToSend)
                }
                else{
                    emailSenderObj.sendShareDocMailToRelatasUser(dataToSend)
                }
                createMessage(userId,contact,message,subject,resend,headerImage,imageUrl,imageName)
            }
            massMailCountManagements.incrementMassMailCount(userId,shareWith.length,function(error,isUpdated){

            })
        }
    })
}

function createMessage(userId,contact,message,subject,resend,headerImage,imageUrl,imageName){
    if(common.checkRequired(message)){
        var message = {
            senderId:userId,
            receiverId:contact.personId || '',
            receiverEmailId:contact.personEmailId,
            subject:subject,
            type: resend ? 'resendDoc' : 'messageDoc',
            headerImage:headerImage,
            imageUrl:imageUrl,
            imageName:imageName,
            message:message
        }
        messageDataAccessObj.createMessage(message,function(message){
            if(message){

                var interaction = {};
                interaction.userId = message.senderId;
                interaction.action = 'sender';
                interaction.type = 'message';
                interaction.subType = 'message';
                interaction.refId = message._id;
                interaction.source = 'relatas';
                interaction.title = message.subject;
                interaction.description = message.message;
                common.storeInteraction(interaction);

                var interactionReceiver = {};
                interactionReceiver.userId = message.receiverId || '';
                interactionReceiver.emailId = message.receiverEmailId;
                interactionReceiver.action = 'receiver';
                interactionReceiver.type = 'message';
                interactionReceiver.subType = 'message';
                interactionReceiver.refId = message._id;
                interactionReceiver.source = 'relatas';
                interactionReceiver.title = message.subject;
                interactionReceiver.description = message.message;
                common.storeInteractionReceiver(interactionReceiver);
            }
        });
    }
}

//Function to add Contact
function updateContact(userId,contact){
    contactObj.addSingleContact(userId,contact)
}

function updateReceiverContacts(senderId,receiverId){

    userManagementObj.findUserProfileByIdWithCustomFields(senderId,{emailId:1,firstName:1,lastName:1,mobileNumber:1},function(error,profile) {
        if(error){

        }else
        if(profile != null || profile != undefined){

            var contact1 = {
                personId:profile._id,
                personName:profile.firstName+' '+profile.lastName,
                personEmailId:profile.emailId,
                count:1,
                lastInteracted:new Date(),
                addedDate: new Date(),
                verified:true,
                relatasContact:true,
                relatasUser:true,
                mobileNumber:profile.mobileNumber
            }
            updateContact(receiverId,contact1);
        }
    });
}

module.exports = router;