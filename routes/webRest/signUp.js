
var express = require('express');
var router = express.Router();

var validations = require('../../public/javascripts/validation');
var emailSender = require('../../public/javascripts/emailSender');
var commonUtility = require('../../common/commonUtility');
var profileManagementClass = require('../../dataAccess/profileManagementClass');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var appCredentials = require('../../config/relatasConfiguration');
var contactClass = require('../../dataAccess/contactsManagementClass');
var winstonLog = require('../../common/winstonLog');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');

var company = new companyClass();
var contactObj = new contactClass();
var validation = new validations();
var common = new commonUtility();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var profileManagementClassObj = new profileManagementClass();
var appCredential = new appCredentials();
var emailSenderObj = new emailSender();

var statusCodes = errorMessagesObj.getStatusCodes();


router.get('/onboarding/new',function(req,res){
    res.render('onboarding/onboarding',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/retail/signup',function(req,res){
    res.render('onboarding/retailSignup',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/onboarding/enterprise',function(req,res){
    var companyId = req.session.partialFillAcc.companyId;

    if(companyId){
        company.findCompanyByIdWithProjection(common.castToObjectId(companyId),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                res.render('onboarding/onboardingEnterprise',{isLoggedIn : common.isLoggedInUserBoolean(req),companyId:companyProfile._id.toString(),company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
            }
            else res.render('error');
        });
    } else {
        res.render('error');
    }
});

router.post('/profile/update/onboardEnterprise',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        var profile = req.body;
        var dataToSend = {
            emailId:profile.emailId,
            firstName:profile.firstName
        }

        profileManagementClassObj.updateProfileCustomQuery(userId,profile,function(isUpdated){
            emailSenderObj.sendEditProfileConfirmationMail(dataToSend);
            common.updateContactMultiCollectionLevel(userId,false);
            res.send({
                "SuccessCode": isUpdated ? 1 : 0,
                "Message": isUpdated ? errorMessagesObj.getMessage("UPDATE_PROFILE_SUCCESS") : errorMessagesObj.getMessage("UPDATE_PROFILE_FAILED"),
                "ErrorCode": isUpdated ? 0 : 1,
                "Data": {}
            });
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("UPDATE_PROFILE_FAILED")));
});

router.get('/onboarding/get/contacts/accounts',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var domain = common.getSubDomain(req);
        contactObj.getAllContactsUser_without_populate_emailId_id(common.castToObjectId(userId),function(emailAccounts,names){
            if(emailAccounts != null){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{emailAccounts:emailAccounts,allAccountNames:names,corporate:domain ? true : false}
                });
            }
            else{
                res.send({
                    "SuccessCode": 0,
                    "Message": errorMessagesObj.getMessage("NO_ACCOUNTS_CONTACTS"),
                    "ErrorCode": 1,
                    "Data": {corporate:domain ? true : false}
                });
            }
        });

});

router.post('/onboarding/update/selected/accounts',common.isLoggedInUserToGoNext,function(req,res){
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        var userId = common.getUserId(req.user)
        if(req.body.length > 0){
            contactObj.updateContactsAccountSelectedBulk(common.castToObjectId(userId),req.body,common.castToObjectId,false);
        }
        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data": {}
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

router.get('/onboarding/get/company/users',function(req,res){
    if(common.checkRequired(req.query.companyId) && req.session.partialFillAcc && req.session.partialFillAcc._id){
        profileManagementClassObj.getUsersByCompanyId(req.session.partialFillAcc._id,req.query.companyId,function(users){
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": users
            });
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

router.get('/settings/get/company/users',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.query.companyId)){
        profileManagementClassObj.getUsersByCompanyId(common.castToObjectId(userId),req.query.companyId,function(users){
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": users
            });
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

module.exports = router;