/* * Created by naveen on 12/10/17.
 */

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var _ = require("lodash");
var async = require("async")

var userManagement = require('../../dataAccess/userManagementDataAccess');
var commonUtility = require('../../common/commonUtility');
var opportunityManagement = require('../../dataAccess/opportunitiesManagement');
var OppCommitManagement = require('../../dataAccess/oppCommitAccess');
var OppWeightsClass = require('../../dbCache/company.js')
var DealsAtRiskManagement = require('../../dataAccess/dealsAtRiskManagement');
var AccessControlOpportunities = require('../../dataAccess/accessControlOpportunitiesManagement');
var SecondaryHierarchy = require('../../dataAccess/secondaryHierarchyManagement');

var common = new commonUtility();
var userManagementObj = new userManagement();
var oppManagementObj = new opportunityManagement();
var oppWeightsObj = new OppWeightsClass();
var oppCommitObj = new OppCommitManagement();
var dealsAtRiskObj = new DealsAtRiskManagement();
var accessControlOpportunitiesObj = new AccessControlOpportunities();
var secondaryHierarchyObj = new SecondaryHierarchy();

router.get("/team/commit/review", common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserId(req.user);

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(user.corporateUser){
            res.render('commits/index',{isLoggedIn : common.isLoggedInUserBoolean(req)});
            // res.render('pulse/reviewCommit',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('accounts/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/commits/v2', common.isLoggedInUserToGoNext, function(req, res) {
    // res.render("pulse/reviewCommit");
    // res.render("commits/index");
});

router.post('/review/meeting/update/commit/value/weekly', common.isLoggedInUserOrMobile, function(req, res) {

    var companyId;
    var userId = common.getUserIdFromMobileOrWeb(req);

    common.getProfileOrStoreProfileInSession(userId,req,function(userProfile){
        companyId = userProfile ? userProfile.companyId : null

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {
    
            var commitStage = "Proposal"; // default.
            if (oppStages) {
                _.each(oppStages, function (st) {
                    if (st.commitStage) {
                        commitStage = st.name;
                    }
                });
            }
    
            var commitDate = common.getThisWeekCommitCutoffDate(companyDetails);
    
            if(req.body.committingForNext){
                commitDate = moment(commitDate).add(1,"week")
            };
    
            var commitWeekYear = moment(commitDate).week()+""+moment(commitDate).year()
    
            var commitValues = {
                weekRelatasCommitAmount:parseFloat(req.body.week)
            }
    
            common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {
                oppCommitObj.updateCommitWeekly(common.castToObjectId(userId),commitWeekYear,commitStage,commitValues,fyRange.start,fyRange.end,timezone,commitDate,function (err,commits) {
                    sendSuccess(err,res,commits)
                });
            });
        });
    })

});

router.post('/review/meeting/update/commit/value/monthly', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    var companyId;

    common.getProfileOrStoreProfileInSession(userId,req,function(userProfile){
        companyId = userProfile ? userProfile.companyId : null

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {
    
            var commitStage = "Proposal"; // default.
            if (oppStages) {
                _.each(oppStages, function (st) {
                    if (st.commitStage) {
                        commitStage = st.name;
                    }
                });
            }
    
            var monthCommitCutOff = 10
    
            if(companyDetails && companyDetails.monthCommitCutOff){
                monthCommitCutOff = companyDetails.monthCommitCutOff
            }
    
            var startOfMonth = moment(moment().startOf('month')).add(monthCommitCutOff,"day");
    
            if(new Date()> new Date(startOfMonth) || req.body.committingForNext){ // Cutofftime
                startOfMonth = moment(startOfMonth).add(1,'month')
            }
    
            var endOfMonth = moment(startOfMonth).add(1,'month'),
                monthYear = moment(startOfMonth).month()+""+moment(startOfMonth).year();
    
            common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {
                common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
                    oppCommitObj.updateCommitMonth(common.castToObjectId(userId),userProfile.emailId,monthYear,commitStage,req.body.commitValue,startOfMonth,endOfMonth,timezone,startOfMonth,netGrossMarginReq,common.calculateNGM,function (err,commits) {
                        sendSuccess(err,res,commits)
                    })
                })
            });
        });
    })

});

router.post('/review/meeting/update/commit/value/quarterly', common.isLoggedInUserOrMobile, function(req, res) {
    var companyId;
    var userId = common.getUserIdFromMobileOrWeb(req);

    common.getProfileOrStoreProfileInSession(userId,req,function(userProfile){
        companyId = userProfile ? userProfile.companyId : null

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {
    
            var commitStage = "Proposal"; // default.
            if (oppStages) {
                _.each(oppStages, function (st) {
                    if (st.commitStage) {
                        commitStage = st.name;
                    }
                });
            }
    
            var qtrCommitCutOff = 15
    
            if(companyDetails && companyDetails.qtrCommitCutOff){
                qtrCommitCutOff = companyDetails.qtrCommitCutOff
            }
    
            common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {
    
                var startOfQuarter = allQuarters[allQuarters.currentQuarter].start,
                    endOfQuarter = allQuarters[allQuarters.currentQuarter].end;
    
                startOfQuarter = moment(startOfQuarter).tz(timezone).add(15,"day")
                endOfQuarter = moment(endOfQuarter).tz(timezone).add(15,"day")
    
                if(qtrCommitCutOff){
                    startOfQuarter = moment(allQuarters[allQuarters.currentQuarter].start).tz(timezone).add(qtrCommitCutOff,"day")
                    endOfQuarter = moment(allQuarters[allQuarters.currentQuarter].end).tz(timezone).add(qtrCommitCutOff,"day")
                }
    
                if(new Date()> new Date(startOfQuarter) || req.body.committingForNext){ // Cutofftime
                    startOfQuarter = moment(startOfQuarter).add(3,'month')
                    endOfQuarter = moment(endOfQuarter).add(3,'month')
                }
    
                var quarterYear = common.getQuarterYear(allQuarters,startOfQuarter,fyMonth,timezone,fyRange.start);
    
                common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
                    oppCommitObj.updateCommitQuarter(common.castToObjectId(userId),userProfile.emailId,commitStage,req.body.commitValue,startOfQuarter,endOfQuarter,timezone,startOfQuarter,netGrossMarginReq,common.calculateNGM,quarterYear,function (err,commits) {
                        sendSuccess(err,res,commits)
                    });
                });
            });
        });
    })

});

router.get('/review/meeting/pipeline', common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;

    var userId = req.query.userId?req.query.userId:common.getUserId(req.user);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var qEnd = allQuarters[allQuarters.currentQuarter].end;

        var monthsInQuarters = [];

        monthsInQuarters.push(moment(qStart).tz(timezone).format('MMMM'))
        monthsInQuarters.push(moment(qStart).tz(timezone).add(1,"month").format('MMMM'))
        monthsInQuarters.push(moment(qEnd).tz(timezone).format('MMMM'))

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if(oppStages){
                _.each(oppStages,function (st) {
                    if(st.commitStage){
                        commitStage = st.name;
                    }
                });
            }

            var weekStartDateTime = common.getCommitDayTimeStart(companyDetails);

            if(req.query.startDate){
                weekStartDateTime = new Date(req.query.startDate)
            }

            var nextWeekMoment = weekStartDateTime;
            var currentWeek = moment(nextWeekMoment).week(); //Next Week :-D
            var prevWeekYear = moment().week()+""+moment().year() //Current week

            var currentWeekYear = currentWeek+""+moment(nextWeekMoment).year()

            async.parallel([
                function (callback) {
                    getOpportunitiesInStage(common.castToObjectId(userId),fyRange.start,fyRange.end,commitStage,callback)
                },
                function (callback) {
                    oppCommitObj.getCommitsByWeekRange(common.castToObjectId(userId),prevWeekYear,callback)
                },
                function (callback) {
                    oppCommitObj.getCommitsByWeekRange(common.castToObjectId(userId),currentWeekYear,callback)
                }
            ], function (errors,data) {
                common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
                    res.send({
                        fyRange:fyRange,
                        commitStage:commitStage,
                        qStart:qStart,
                        qEnd:qEnd,
                        monthsInQuarters:monthsInQuarters,
                        opps:data[0] && netGrossMarginReq?calculateNGM(data[0]):data[0],
                        commitsPrev:checkForNoPrevCommits(data[1],netGrossMarginReq),
                        commitsCurrent:checkForNoCurrentCommits(data[2],data[0],commitStage,qStart,qEnd,netGrossMarginReq)
                    })
                })
            });
        })
    })
})

router.get('/user/get/portfolios', common.isLoggedInUserOrMobile, function(req, res){
    
    var userId = common.getUserIdFromMobileOrWeb(req);

    common.getProfileOrStoreProfileInSession(userId,req,function(userProfile){
        var companyId = userProfile?userProfile.companyId:null;

        common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone,user_profile,companyDetails) {

            var qStart = allQuarters[allQuarters.currentQuarter].start;
            var qEnd = allQuarters[allQuarters.currentQuarter].end;

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, orgHierarchyData){
                var userEmailIds = [userProfile.emailId];
                var fullNameDetails = {};

                if(!err && orgHierarchyData && orgHierarchyData.length>0){
                    _.each(orgHierarchyData,function(org){
                        userEmailIds.push(org.emailId);
                        fullNameDetails[org.emailId] = org.firstName +" "+org.lastName
                    });
                }

                userEmailIds = _.uniq(userEmailIds);

                userManagementObj.getPortfolios(common.castToObjectId(companyId),userEmailIds,userId,function(err,portfolios){

                    var amountWonObj = {}

                    if(portfolios && portfolios.owner && portfolios.owner.length>0){

                        var users = [];

                        _.each(portfolios.owner,function(po){
                            users = users.concat(po.users)
                        });

                        _.each(portfolios.reportees,function(po){
                            _.each(po.users,function(user){
                                users = users.concat(user)
                            })
                        })

                        users = _.uniq(users,"_id")

                        async.eachSeries(users, function(user, next) {

                            getOppsByPortfolio(user, companyId,qStart,qEnd,primaryCurrency,currenciesObj, function(amount) {
                                amountWonObj[user._id] = amount;
                                next();
                            })
                        }, function(error3,data) {

                            var commitsForUsers = [];
                            if(req.query.getHeads){
                                var pQuery= []
                            }

                            _.each(portfolios.owner,function(po){

                                if(req.query.getHeads){
                                    pQuery.push({
                                        type:po._id.type,
                                        name:po._id.name
                                    });
                                }

                                _.each(po.users,function(user){

                                    commitsForUsers.push(user.ownerEmailId);

                                    user.wonAmtQtr = amountWonObj[user._id]?amountWonObj[user._id].qtr:0;
                                    user.wonAmtMonth = amountWonObj[user._id]?amountWonObj[user._id].month:0;
                                    user.wonAmtWeek = amountWonObj[user._id]?amountWonObj[user._id].week:0;
                                    user.qStart = qStart;
                                    user.qEnd = qEnd;
                                })
                            })

                            _.each(portfolios.reportees,function(po){
                                _.each(po.users,function(user){
                                    commitsForUsers.push(user.ownerEmailId);
                                    user.wonAmtQtr = amountWonObj[user._id]?amountWonObj[user._id].qtr:0;
                                    user.wonAmtMonth = amountWonObj[user._id]?amountWonObj[user._id].month:0;
                                    user.wonAmtWeek = amountWonObj[user._id]?amountWonObj[user._id].week:0;
                                    user.qStart = qStart;
                                    user.qEnd = qEnd;
                                })
                            });

                            portfolios.qStart = qStart;
                            portfolios.qEnd = qEnd;
                            portfolios.fullNameDetails = fullNameDetails;

                            var startOfMonth = moment().startOf('month');
                            var endOfMonth = moment(startOfMonth).add(1,'month'),
                                monthYear = moment(startOfMonth).month()+""+moment(startOfMonth).year();

                            var quarterYearDate = moment(qStart).add(3,"month");
                            var currentWeekYear = moment().week()+""+moment().year()
                            var startOfQuarter = moment(qStart).add(15,"day")
                            var quarterYear = common.getQuarterYear(allQuarters,startOfQuarter,fyMonth,timezone,fyRange.start);
                            var userIds = [];

                            userManagementObj.getTeamMembers(common.castToObjectId(companyId),function (err,allUsers) {

                                var fullNameDetailsTeam = {};

                                var allUserIds = {};
                                _.each(allUsers,function (user) {

                                    fullNameDetailsTeam[user.emailId] = user.firstName +" "+user.lastName
                                    allUserIds[user.emailId] = {
                                        _id:user._id,
                                        emailId:user.emailId,
                                    };
                                });
                                commitsForUsers = _.uniq(commitsForUsers);

                                _.each(commitsForUsers,function(emailId){
                                    userIds.push(allUserIds[emailId]._id)
                                });

                                var findCommitQuery = {
                                    userId:{$in:userIds},
                                    $or:[{
                                        quarterYear:quarterYear,
                                        commitFor:'quarter'
                                    },{
                                        monthYear:monthYear,
                                        commitFor:'month'
                                    },{
                                        commitWeekYear:currentWeekYear,
                                        commitFor:'week'
                                    }]
                                };

                                oppCommitObj.getCommitsByCustomQuery(findCommitQuery,function (err,commits) {

                                    var commitObj = {};
                                    if(commits && commits.length>0){
                                        _.each(commits,function(co){
                                            commitObj[String(co.userId)+co.commitFor] = co
                                        });
                                    }

                                    _.each(portfolios.owner,function(po){
                                        _.each(po.users,function(user){
                                            var userId = allUserIds[user.ownerEmailId]._id;
                                            user.commitQtr = commitObj[userId+"quarter"]?commitObj[userId+"quarter"].quarter.userCommitAmount:0;
                                            user.commitMonth = commitObj[userId+"month"]?commitObj[userId+"month"].month.userCommitAmount:0;
                                            user.commitWeek = commitObj[userId+"week"]?commitObj[userId+"week"].week.userCommitAmount:0;
                                        })
                                    })

                                    _.each(portfolios.reportees,function(po){
                                        _.each(po.users,function(user){
                                            var userId = allUserIds[user.ownerEmailId]._id;

                                            user.commitQtr = commitObj[userId+"quarter"]?commitObj[userId+"quarter"].quarter.userCommitAmount:0;
                                            user.commitMonth = commitObj[userId+"month"]?commitObj[userId+"month"].month.userCommitAmount:0;
                                            user.commitWeek = commitObj[userId+"week"]?commitObj[userId+"week"].week.userCommitAmount:0;
                                        })
                                    });

                                    if(req.query.getHeads){
                                        userManagementObj.getPortfolioHeads({companyId:common.castToObjectId(companyId),
                                            isHead:true,
                                            $or:pQuery},function(err,portfoliosWithHead){
                                            portfolios.commits = commitObj

                                            var portfoliosWithHeadObj = {};
                                            var portfolioHeads = {};
                                            if(portfoliosWithHead && portfoliosWithHead.length>0){
                                                _.each(portfoliosWithHead,function(po){
                                                    portfolioHeads[po.ownerEmailId] = po.name +" - "+po.type;

                                                    portfoliosWithHeadObj[po.type+po.name] = {
                                                        name: fullNameDetailsTeam[po.ownerEmailId],
                                                        emailId:po.ownerEmailId
                                                    };
                                                })
                                            }
                                            portfolios.portfoliosWithHeadObj = portfoliosWithHeadObj
                                            portfolios.portfolioHeads = portfolioHeads
                                            res.send(portfolios);
                                        })
                                    } else {
                                        portfolios.commits = commitObj
                                        res.send(portfolios);
                                    }
                                })
                            })
                        })
                    } else {
                        res.send(null)
                    }
                });
            })
        })
    })
})

function getOppsByPortfolio(user,companyId,start,end,primaryCurrency,currenciesObj,callback){

    var query = {};
    var portfolioAcc = null;
    var portfolioHead = null;

    if(user){

        var regionAccess = [],
            productAccess = [],
            businessUnits = [],
            verticalAccess = [];

        if(user.type == "Products"){
            productAccess.push(user.name)

            if(user.isHead){
                portfolioHead = [{productType:user.name}]
            }
        }

        if(user.type == "Verticals"){
            verticalAccess.push(user.name)
            if(user.isHead){
                portfolioHead = [{vertical:user.name}]
            }
        }

        if(user.type == "Business Units"){
            businessUnits.push(user.name);

            if(user.isHead){
                portfolioHead = [{businessUnit:user.name}]
            }
        }

        if(user.type == "Regions"){
            regionAccess.push(user.name)
            if(user.isHead){
                portfolioHead = [{"geoLocation.zone":user.name}]
            }
        }

        _.each(user.accessLevel,function(ac){

            if(ac.type == "Products"){
                productAccess = ac.values;
            }

            if(ac.type == "Verticals"){
                verticalAccess = ac.values;
            }

            if(ac.type == "Business Units"){
                businessUnits = ac.values;
            }

            if(ac.type == "Regions"){
                regionAccess = ac.values;
            }

        });

        if(portfolioHead){

            query = {
                companyId:companyId,
                closeDate:{$gte: new Date(start),$lte: new Date(end)},
                relatasStage: "Close Won",
                $and: portfolioHead
            }
        } else {

            query = {
                companyId:companyId,
                closeDate:{$gte: new Date(start),$lte: new Date(end)},
                relatasStage: "Close Won",
                $and: accessControlSettings(regionAccess, productAccess, verticalAccess, companyId, businessUnits)
            }
        }

    }

    oppManagementObj.getOppByCustomQuery(query,null,function (errOpps,opps) {
        var amount = 0;
        var obj = {
            week:0,
            month:0,
            qtr:0
        }

        var monthStart = moment().startOf("month");
        var monthEnd = moment().endOf("month");

        if(!errOpps && opps && opps.length>0){
           _.each(opps,function(op){

               op.amountNonNGM = op.amount;
               if(op.netGrossMargin || op.netGrossMargin == 0){
                   var amountWithNGM = op.netGrossMargin*op.amount

                   if(amountWithNGM){
                       amountWithNGM = amountWithNGM/100
                   }

                   op.amount = amountWithNGM;
                   op.convertedAmt = op.amount;
                   op.convertedAmtWithNgm = op.amountWithNgm

                   if(op.currency && op.currency !== primaryCurrency){

                       if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                           op.convertedAmt = op.amount/currenciesObj[op.currency].xr
                       }

                       if(op.netGrossMargin || op.netGrossMargin == 0){
                           op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                       }

                       op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

                   }
               }

               if(new Date(op.closeDate)>= (new Date(start)) && new Date(op.closeDate) <= (new Date(end))){
                   obj.qtr = obj.qtr+parseFloat(op.convertedAmt);
               }

               if(new Date(op.closeDate)>= (new Date(monthStart)) && new Date(op.closeDate) <= (new Date(monthEnd))){
                   obj.month = obj.month+parseFloat(op.convertedAmt);
               }

               if(new Date(op.closeDate)>= (new Date(moment().startOf('isoweek'))) && new Date(op.closeDate) <= (new Date(moment().endOf('isoweek')))){
                   obj.week = obj.week+parseFloat(op.convertedAmt);
               }

           });
        }
    callback(obj)
    });
}

router.get('/review/commits/week', common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);

    common.getProfileOrStoreProfileInSession(userId,req,function(userProfile){
        var companyId = userProfile?userProfile.companyId:null;

        common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

            var qStart = allQuarters[allQuarters.currentQuarter].start;
            var qEnd = allQuarters[allQuarters.currentQuarter].end;
    
            oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {
    
                var commitStage = "Proposal"; // default.
                if(oppStages){
                    _.each(oppStages,function (st) {
                        if(st.commitStage){
                            commitStage = st.name;
                        }
                    });
                }
    
                var nextEditCutOffDate = common.getCommitDayTimeStart(companyDetails);
                var prevEditCutOffDate = moment(nextEditCutOffDate).subtract(1,"week")
    
                var weekYear = moment().week()+""+moment().year()
                var editAccess = false,
                    nextCommitExists = true;
    
                if(req.query.startDate){
                    weekYear = moment(req.query.startDate).week()+""+moment(req.query.startDate).year()
                    editAccess = new Date(req.query.startDate)<=new Date(nextEditCutOffDate) && new Date(req.query.startDate)>new Date(prevEditCutOffDate)
                }
    
                async.parallel([
                    function (callback) {
                        getAllOpportunitiesInCommitStage(common.castToObjectId(userId),commitStage,callback)
                    },
                    function (callback) {
                        oppCommitObj.getCommitsByWeekRange(common.castToObjectId(userId),weekYear,callback)
                    }
                ], function (errors,data) {
                    common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
    
                        var oppsInCommitStage = data[0];
                        if(netGrossMarginReq){
                            oppsInCommitStage = calculateNGM(oppsInCommitStage)
                        }
    
                        res.send({
                            netGrossMarginReq:netGrossMarginReq,
                            editAccess:editAccess,
                            commitWeek:req.query.startDate?req.query.startDate:moment().toISOString(),
                            fyRange:fyRange,
                            commitStage:commitStage,
                            qStart:qStart,
                            qEnd:qEnd,
                            opps:oppsInCommitStage,
                            commitsCurrentWeek:checkForNoWeekCommits(data[1],oppsInCommitStage,commitStage,editAccess,netGrossMarginReq)
                        })
                    })
                });
            })
        })
    })

})

router.get('/review/commits/week/team', common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;

    var userId = req.query.userId?req.query.userId:common.getUserId(req.user);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var qEnd = allQuarters[allQuarters.currentQuarter].end;

        var userIds = []
        if(req.query.hierarchylist){
            userIds = req.query.hierarchylist.split(',')
        } else {
            userIds.push(userId)
        }

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if(oppStages){
                _.each(oppStages,function (st) {
                    if(st.commitStage){
                        commitStage = st.name;
                    }
                });
            }

            var nextEditCutOffDate = common.getCommitDayTimeStart(companyDetails);
            var prevEditCutOffDate = moment(nextEditCutOffDate).subtract(1,"week")
            var weekYear = moment().isoWeek()+""+moment().year()
            var editAccess = false

            if(req.query.startDate){
                weekYear = moment(req.query.startDate).week()+""+moment(req.query.startDate).year()
                editAccess = new Date(req.query.startDate)<=new Date(nextEditCutOffDate) && new Date(req.query.startDate)>new Date(prevEditCutOffDate)
            }

            async.parallel([
                function (callback) {
                    getAllOpportunitiesInCommitStageMultiUsers(userIds,commitStage,callback)
                },
                function (callback) {
                    oppCommitObj.getCommitsByWeekRangeMultiUsers(common.castListToObjectIds(userIds),weekYear,callback)
                }
            ], function (errors,data) {
                common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {

                    var oppsInCommitStage = data[0];
                    if(netGrossMarginReq){
                        oppsInCommitStage = calculateNGM(oppsInCommitStage)
                    }

                    secondaryHierarchyObj.getUserHierarchy(common.castToObjectId(userId), req.query.shType, function(err_r, sh_data){
                        res.send({
                            sh_users:_.pluck(sh_data,"userId"),
                            editAccess:false,
                            commitWeek:req.query.startDate?req.query.startDate:moment().toISOString(),
                            fyRange:fyRange,
                            commitStage:commitStage,
                            qStart:qStart,
                            qEnd:qEnd,
                            commitsCurrentRaw:req.query.hierarchylist && data[1] && data[1].rawData?data[1] && data[1].rawData:null,
                            opps:oppsInCommitStage,
                            commitsCurrentWeek:checkForNoWeekCommits(data[1].flattenedData,oppsInCommitStage,commitStage,editAccess,netGrossMarginReq),
                        })
                    });
                })
            });
        })
    })
})

router.get('/review/opp/insights', common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req);
    var userId = req.query.userId?req.query.userId:common.getUserId(req.user)
    var userIds = []
    if(req.query.hierarchylist && req.query.contacts && req.query.opportunityId){
        userIds = req.query.hierarchylist.split(',');
        userIds = _.compact(userIds);
        userIds = common.castListToObjectIds(userIds);
        var contacts = req.query.contacts.split(',');
        var opportunityId = req.query.opportunityId;
        var closeDate = req.query.closeDate?new Date(req.query.closeDate):null;

        if(!req.query.createdDate){
            req.query.createdDate = moment().subtract(90,"days");
        }

        async.parallel([
            function (callback) {
                oppManagementObj.getInteractionActivity(userIds,contacts,closeDate,callback)
            },
            function (callback) {
                dealsAtRiskObj.findAtRiskOpp(userIds,opportunityId,callback)
            },
            function (callback) {
                oppManagementObj.getInteractionCount(userIds,contacts,new Date(req.query.createdDate),closeDate,callback)
            },
            function (callback) {
                oppManagementObj.getOppByCustomQuery({opportunityId:opportunityId},null,callback)
            }
        ], function (errors,data) {

            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                err: errors,
                Data:{
                    opp:data[3][0],
                    interactionsCount:data[2],
                    interactions:data[0],
                    insights:dealsAtRiskInsights(data[1],data[3][0],data[2])
                }
            })
        });
    } else {
        res.send({
            SuccessCode: 0,
            ErrorCode: 1,
            Data:null
        })
    }
})

function dealsAtRiskInsights(deals,opp,interactionsCount){

    var insights = [];

    if(opp.relatasStage !== "Close Won" && opp.relatasStage !== "Close Lost" && opp.lastStageUpdated && opp.lastStageUpdated.date && (moment().diff(moment(opp.lastStageUpdated.date),'day'))>45){
        insights.push({
            text:"Too much time in the "+ opp.stageName+ " stage("+moment().diff(moment(opp.lastStageUpdated.date),'day')+" days)",
            risk: 'high'
        });
    } else if(opp.relatasStage == "Close Won" || opp.relatasStage == "Close Lost"){
        insights.push({
            text:"Days to closure "+"("+Math.abs(moment(opp.createdDate).diff(moment(opp.closeDate),'day'))+" days)",
            risk: 'low'
        })
    } else {
        if(opp.lastStageUpdated && opp.lastStageUpdated.date){
            insights.push({
                text:"Stage last updated "+"("+moment().diff(moment(opp.lastStageUpdated.date),'day')+" days)",
                risk: 'low'
            })
        }
    }

    if(opp.influencers.length>0){
        insights.push({
            text:"Influencer identified",
            risk: 'low'
        })
    } else {
        insights.push({
            text:"Influencer not identified",
            risk: 'high'
        })
    }

    if(opp.decisionMakers.length>0){
        insights.push({
            text:"Decision Makers identified",
            risk: 'low'
        })
    } else {
        insights.push({
            text:"Decision Makers not identified",
            risk: 'high'
        })
    }

    if(deals[0] && deals[0].deals){
        // if(deals[0].deals.skewedTwoWayInteractions || interactionsCount == 0){
        //     insights.push({
        //         text:"Recent interactions activity below normal",
        //         risk: 'high'
        //     })
        // } else {
        //     insights.push({
        //         text:"Recent interactions activity is normal",
        //         risk: 'low'
        //     })
        // }

        if(deals[0].deals.metDecisionMaker_infuencer){
            insights.push({
                text:"Met Decision Makers/Influencers",
                risk: 'low'
            })
        } else {
            insights.push({
                text:"Did not meet Decision Makers/Influencers",
                risk: 'high'
            })
        }

        // if(deals[0].deals.ltWithOwner || interactionsCount == 0){
        //     insights.push({
        //         text:"Losing touch with primary contact",
        //         risk: 'high'
        //     })
        // } else {
        //     insights.push({
        //         text:"In touch with primary contact",
        //         risk: 'low'
        //     })
        // }
    }

    return insights;
}

router.get('/review/commits/month', common.isLoggedInUserOrMobile, function(req, res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    if(userId instanceof Array){
        userId = userId[0];
    }

    common.getProfileOrStoreProfileInSession(userId,req,function(userProfile){
        var companyId = userProfile?userProfile.companyId:null;

        common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {
            
            var qStart = allQuarters[allQuarters.currentQuarter].start;
            var qEnd = allQuarters[allQuarters.currentQuarter].end;
    
            oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {
    
                var commitStage = "Proposal"; // default.
                if(oppStages){
                    _.each(oppStages,function (st) {
                        if(st.commitStage){
                            commitStage = st.name;
                        }
                    });
                }
    
                var monthCommitCutOff = 10;
    
                if(companyDetails && companyDetails.monthCommitCutOff){
                    monthCommitCutOff = companyDetails.monthCommitCutOff
                }
    
                var cutOffDateTime = new Date(new Date(new Date().setDate(monthCommitCutOff)).setHours(23, 59, 0))
    
                var startOfMonth = moment(moment().startOf('month')).add(monthCommitCutOff,"day");
                var editAccess = new Date()<= cutOffDateTime
    
                if(req.query.startDate){
                    startOfMonth = moment(moment(req.query.startDate).startOf('month')).add(monthCommitCutOff,"day")
                    endOfMonth = moment(startOfMonth).add(1,'month');
                    editAccess = new Date()<= new Date(startOfMonth)
                } else {
                    if(new Date()>new Date(startOfMonth)){
                        // startOfMonth = moment(moment(moment().add(1,"month")).startOf('month')).add(monthCommitCutOff,"day")
                    }
                }
    
                startOfMonth = moment(startOfMonth).startOf('month')
                var endOfMonth = moment(startOfMonth).add(1,'month')
    
                var monthYear = moment(startOfMonth).month()+""+moment(startOfMonth).year();
    
                async.parallel([
                    function (callback) {
                        getOpportunitiesInStage(common.castToObjectId(userId),startOfMonth,endOfMonth,commitStage,callback)
                    },
                    function (callback) {
                        oppCommitObj.getCommitsByMonthRange(common.castToObjectId(userId),monthYear,callback)
                    }
                ], function (errors,data) {
                    common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
    
                        var currentDate = req.query.startDate?req.query.startDate:moment().toISOString()
                        currentDate = new Date(moment(moment(currentDate).startOf('month')).add(monthCommitCutOff,"day"));
    
                        res.send({
                            editAccess:editAccess,
                            commitMonth:new Date(startOfMonth),
                            currentDate:currentDate,
                            cutOffDate:cutOffDateTime,
                            fyRange:fyRange,
                            commitStage:commitStage,
                            qStart:qStart,
                            qEnd:qEnd,
                            opps:data[0] && netGrossMarginReq?calculateNGM(data[0]):data[0],
                            commitCurrentMonth:checkForNoCurrentMonthCommits(data[1],data[0],commitStage,startOfMonth,endOfMonth,netGrossMarginReq,monthYear,userProfile,currentDate)
                        })
                    })
                });
            })
        })
    })

})

router.get('/review/next/commits', common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;
    var userId = common.getUserIdFromMobileOrWeb(req);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var quarterYearDate = moment(qStart).add(3,"month");

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var monthCommitCutOff = 10

            if(companyDetails && companyDetails.monthCommitCutOff){
                monthCommitCutOff = companyDetails.monthCommitCutOff
            }

            var startOfMonth = moment(moment(moment().startOf('month')).add(monthCommitCutOff,"day")).add(1,"month");
            var monthYear = moment(startOfMonth).month()+""+moment(startOfMonth).year();

            var findQuery = {
                userId:common.castToObjectId(userId),
                monthYear:monthYear,
                commitFor:'month'
            };

            var weekDate = moment(common.getThisWeekCommitCutoffDate(companyDetails,timezone)).add(1,"week");
            var commitWeekYear = moment(weekDate).week()+""+moment(weekDate).year()

            if(req.query.range){
                if(req.query.range.toLowerCase() == 'week'){
                    findQuery = {
                        userId:common.castToObjectId(userId),
                        commitWeekYear:commitWeekYear,
                        commitFor:'week'
                    };
                } else if(req.query.range.toLowerCase() == 'quarter'){
                    findQuery = {
                        userId:common.castToObjectId(userId),
                        quarterYear:common.getQuarterYear(allQuarters,quarterYearDate,fyMonth,timezone,fyRange.start),
                        commitFor:'quarter'
                    };
                }
            }

            oppCommitObj.getCommitsByCustomRange(findQuery,function (err,commits) {
                if(!err && commits){
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data: commits
                    })
                } else {

                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data:null
                    })
                }
            })
        })
    })
})

router.get('/review/commits/month/team', common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;
    var userId = req.query.userId?req.query.userId:common.getUserId(req.user);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var qEnd = allQuarters[allQuarters.currentQuarter].end;

        var userIds = []
        if(req.query.hierarchylist){
            userIds = req.query.hierarchylist.split(',')
        } else {
            userIds.push(userId)
        }

        userIds = common.castListToObjectIds(userIds);

        secondaryHierarchyObj.getUserHierarchy(common.castToObjectId(userId), req.query.shType, function(err_r, sh_data){
            accessControlOpportunitiesObj.getAllAccessControlOpportunities(common.castToObjectId(companyId), common.castToObjectId(userId), function (err,allACOpps){
                oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

                    var commitStage = "Proposal"; // default.
                    if(oppStages){
                        _.each(oppStages,function (st) {
                            if(st.commitStage){
                                commitStage = st.name;
                            }
                        });
                    }

                    var userEmailIds = req.query.userEmailId.split(",")

                    var sh_users = _.pluck(sh_data,"userId");

                    var monthCommitCutOff = 10

                    if(companyDetails && companyDetails.monthCommitCutOff){
                        monthCommitCutOff = companyDetails.monthCommitCutOff
                    }

                    var startOfMonth = moment(moment().startOf('month')).add(monthCommitCutOff,"day");

                    if(new Date()>new Date(startOfMonth)){
                        // startOfMonth = moment(moment(moment().add(1,"month")).startOf('month')).add(monthCommitCutOff,"day")
                    }

                    var endOfMonth = moment(startOfMonth).add(1,'month')

                    if(req.query.startDate){
                        startOfMonth = moment(moment(req.query.startDate).startOf('month')).add(monthCommitCutOff,"day")
                        endOfMonth = moment(startOfMonth).add(1,'month');
                    }

                    var monthYear = moment(startOfMonth).month()+""+moment(startOfMonth).year();

                    var accessControlQuery = null;
                    if(req.query.portfolios){
                        var portfolios_raw = req.query.portfolios.split(",");

                        if(portfolios_raw && portfolios_raw.length>0){
                            var productAccess = [];
                            var regionAccess = [];
                            var verticalAccess = [];
                            var businessUnits = [];

                            _.each(portfolios_raw,function (po) {
                                var splits  = po.split("_type_");
                                if(splits[1] == "productType"){
                                    productAccess.push(splits[0])
                                }
                                if(splits[1] == "vertical"){
                                    verticalAccess.push(splits[0])
                                }
                                if(splits[1] == "businessUnit"){
                                    businessUnits.push(splits[0])
                                }
                                if(splits[1] == "region"){
                                    regionAccess.push(splits[0])
                                }
                            })
                        }

                        accessControlQuery = portfolioAccess(regionAccess,productAccess,verticalAccess,companyId,businessUnits);
                    }

                    async.parallel([
                        function (callback) {
                            if(req.query.shType == "Revenue"){
                                userIds = userIds.concat(common.castListToObjectIds(sh_users));
                            }
                            getOpportunitiesInStageMultiUsers(userIds,startOfMonth,endOfMonth,commitStage,allACOpps,companyId,callback,userEmailIds,accessControlQuery)
                        },
                        function (callback) {
                            oppCommitObj.getCommitsByMonthRangeMultiUsers(userIds,monthYear,callback)
                        }
                    ], function (errors,data) {
                        common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {

                            var currentDate = req.query.startDate?req.query.startDate:moment().toISOString()
                            currentDate = new Date(moment(moment(currentDate).startOf('month')).add(monthCommitCutOff,"day"))
                            var oppsIds = [],
                                oppsUserIds = [];

                            _.each(data[0],function (op) {
                                if(op){
                                    oppsUserIds.push(common.castToObjectId(op.userId));
                                    oppsIds.push(op.opportunityId);
                                }
                            });

                            getDealsAtRiskForOpps(oppsUserIds,oppsIds,function (errD,dealsAtRisk) {

                                res.send({
                                    sh_users:sh_users,
                                    dealsAtRisk:dealsAtRisk,
                                    commitMonth:new Date(startOfMonth),
                                    currentDate:currentDate,
                                    cutOffDate:new Date(moment(moment().startOf('month')).add(monthCommitCutOff,"day")),
                                    fyRange:fyRange,
                                    commitStage:commitStage,
                                    qStart:qStart,
                                    qEnd:qEnd,
                                    opps:data[0] && netGrossMarginReq?calculateNGM(data[0]):data[0],
                                    commitsCurrentRaw:data[1].rawData,
                                    commitCurrentMonth:checkForNoCurrentMonthCommits(data[1].flattenedData,data[0],commitStage,startOfMonth,endOfMonth,netGrossMarginReq,monthYear,userProfile,currentDate)
                                })
                            })
                        })
                    });
                })
            });
        });
    })
})

router.get('/review/commits/quarter/team', common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;
    var userId = req.query.userId?req.query.userId:common.getUserId(req.user);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var qEnd = allQuarters[allQuarters.currentQuarter].end;

        var userIds = []
        if(req.query.hierarchylist){
            userIds = req.query.hierarchylist.split(',')
        } else {
            userIds.push(userId)
        }

        userIds = common.castListToObjectIds(userIds)

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if(oppStages){
                _.each(oppStages,function (st) {
                    if(st.commitStage){
                        commitStage = st.name;
                    }
                });
            }

            var monthCommitCutOff = 10

            if(companyDetails && companyDetails.monthCommitCutOff){
                monthCommitCutOff = companyDetails.monthCommitCutOff
            }

            var qtrCommitCutOff = 15

            if(companyDetails && companyDetails.qtrCommitCutOff){
                qtrCommitCutOff = companyDetails.qtrCommitCutOff
            }

            var startOfQuarter = allQuarters[allQuarters.currentQuarter].start,
                endOfQuarter = allQuarters[allQuarters.currentQuarter].end;

            startOfQuarter = moment(startOfQuarter).tz(timezone).add(15,"day")
            endOfQuarter = moment(endOfQuarter).tz(timezone).add(15,"day")

            if(qtrCommitCutOff){
                startOfQuarter = moment(allQuarters[allQuarters.currentQuarter].start).tz(timezone).add(qtrCommitCutOff,"day")
                endOfQuarter = moment(allQuarters[allQuarters.currentQuarter].end).tz(timezone).add(qtrCommitCutOff,"day")
            }

            if(req.query.startDate){
                startOfQuarter = moment(req.query.startDate).tz(timezone)
                endOfQuarter = moment(startOfQuarter).tz(timezone).add(3,"month")
            }

            var quarterYear = common.getQuarterYear(allQuarters,startOfQuarter,fyMonth,timezone,fyRange.start);

            var accessControlQuery = null;
            if(req.query.portfolios){
                var portfolios_raw = req.query.portfolios.split(",");

                if(portfolios_raw && portfolios_raw.length>0){
                    var productAccess = [];
                    var regionAccess = [];
                    var verticalAccess = [];
                    var businessUnits = [];

                    _.each(portfolios_raw,function (po) {
                        var splits  = po.split("_type_");
                        if(splits[1] == "productType"){
                            productAccess.push(splits[0])
                        }
                        if(splits[1] == "vertical"){
                            verticalAccess.push(splits[0])
                        }
                        if(splits[1] == "businessUnit"){
                            businessUnits.push(splits[0])
                        }
                        if(splits[1] == "region"){
                            regionAccess.push(splits[0])
                        }
                    })
                }

                accessControlQuery = portfolioAccess(regionAccess,productAccess,verticalAccess,companyId,businessUnits);
            }

            var userEmailIds = req.query.userEmailId.split(",")

            async.parallel([
                function (callback) {
                    getOpportunitiesInStageMultiUsers(userIds,startOfQuarter,endOfQuarter,commitStage,[],companyId,callback,userEmailIds,accessControlQuery)
                },
                function (callback) {
                    oppCommitObj.getCommitsByQuarterRangeMultiUsers(userIds,quarterYear,callback)
                }
            ], function (errors,data) {
                common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {

                    var currentDate = req.query.startDate?req.query.startDate:moment().toISOString()

                    var oppsIds = [],
                        oppsUserIds = [];

                    _.each(data[0],function (op) {
                        if(op){
                            oppsUserIds.push(common.castToObjectId(op.userId));
                            oppsIds.push(op.opportunityId);
                        }
                    });

                    secondaryHierarchyObj.getUserHierarchy(common.castToObjectId(userId), req.query.shType, function(err_r, sh_data){
                        getDealsAtRiskForOpps(oppsUserIds,oppsIds,function (errD,dealsAtRisk) {
                            res.send({
                                sh_users:_.pluck(sh_data,"userId"),
                                dealsAtRisk:dealsAtRisk,
                                commitQuarter:{
                                    start: new Date(startOfQuarter),
                                    end: new Date(endOfQuarter)
                                },
                                currentDate:currentDate,
                                fyRange:fyRange,
                                commitStage:commitStage,
                                qStart:qStart,
                                qEnd:qEnd,
                                opps:data[0] && netGrossMarginReq?calculateNGM(data[0]):data[0],
                                commitsCurrentRaw:data[1].rawData,
                                commitCurrentQuarter:checkForNoCurrentQuarterCommits(data[1].flattenedData,data[0],commitStage,startOfQuarter,endOfQuarter,netGrossMarginReq,userProfile,currentDate,quarterYear)
                            })
                        });
                    })
                })
            });
        })
    })
})

router.get('/review/get/quarter/cutoff', common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;
    var userId = req.query.userId?req.query.userId:common.getUserId(req.user);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var qtrCommitCutOff = 15

            if(companyDetails && companyDetails.qtrCommitCutOff){
                qtrCommitCutOff = companyDetails.qtrCommitCutOff
            }

            var startOfQuarter = allQuarters[allQuarters.currentQuarter].start,
                endOfQuarter = allQuarters[allQuarters.currentQuarter].end;

            startOfQuarter = moment(startOfQuarter).add(15, "day")
            endOfQuarter = moment(endOfQuarter).add(15, "day")

            if (qtrCommitCutOff) {
                startOfQuarter = moment(allQuarters[allQuarters.currentQuarter].start).add(qtrCommitCutOff, "day")
                endOfQuarter = moment(allQuarters[allQuarters.currentQuarter].end).add(qtrCommitCutOff, "day")
            }

            res.send({
                startOfQuarter:allQuarters[allQuarters.currentQuarter].start,
                endOfQuarter:allQuarters[allQuarters.currentQuarter].end,
                startOfQuarterCutOff:startOfQuarter,
                endOfQuarterCutOff:endOfQuarter
            })
        });
    });
});

router.get('/review/get/all/commit/cutoff', common.isLoggedInUserOrMobile, function(req, res){
    var userId = req.query.userId?req.query.userId:common.getUserIdFromMobileOrWeb(req);

    common.getProfileOrStoreProfileInSession(userId,req,function(userProfile){
        var companyId = userProfile?userProfile.companyId:null;

        common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {
            oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {
    
                var monthCommitCutOff = 10
    
                if(companyDetails && companyDetails.monthCommitCutOff){
                    monthCommitCutOff = companyDetails.monthCommitCutOff
                }
    
                var qtrCommitCutOff = 15
    
                if(companyDetails && companyDetails.qtrCommitCutOff){
                    qtrCommitCutOff = companyDetails.qtrCommitCutOff
                }
    
                var startOfQuarter = allQuarters[allQuarters.currentQuarter].start,
                    endOfQuarter = allQuarters[allQuarters.currentQuarter].end;
    
                startOfQuarter = moment(startOfQuarter).add(15, "day")
                endOfQuarter = moment(endOfQuarter).add(15, "day")
    
                if (qtrCommitCutOff) {
                    startOfQuarter = moment(allQuarters[allQuarters.currentQuarter].start).add(qtrCommitCutOff, "day")
                    endOfQuarter = moment(allQuarters[allQuarters.currentQuarter].end).add(qtrCommitCutOff, "day")
                }
    
                res.send({
                    quarter :{
                        startOfQuarter:startOfQuarter,
                        endOfQuarter:endOfQuarter
                    },
                    month:new Date(new Date(new Date().setDate(monthCommitCutOff)).setHours(23, 59, 0)),
                    week:common.getThisWeekCommitCutoffDate(companyDetails,timezone),
                    timezone:timezone,
                    startOfQuarter:allQuarters[allQuarters.currentQuarter].start,
                    endOfQuarter:allQuarters[allQuarters.currentQuarter].end
                })
            });
        });
    })
});

router.get('/review/commits/quarter', common.isLoggedInUserOrMobile, function(req, res){
    
    var userId = common.getUserIdFromMobileOrWeb(req);
    if(userId instanceof Array){
        userId = userId[0];
    }

    common.getProfileOrStoreProfileInSession(userId,req,function(userProfile){
        var companyId = userProfile?userProfile.companyId:null;

        common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {
    
            oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {
    
                var commitStage = "Proposal"; // default.
                if(oppStages){
                    _.each(oppStages,function (st) {
                        if(st.commitStage){
                            commitStage = st.name;
                        }
                    });
                }
    
                var qtrCommitCutOff = 15
    
                if(companyDetails && companyDetails.qtrCommitCutOff){
                    qtrCommitCutOff = companyDetails.qtrCommitCutOff
                }
    
                var startOfQuarter = allQuarters[allQuarters.currentQuarter].start,
                    endOfQuarter = allQuarters[allQuarters.currentQuarter].end;
    
                startOfQuarter = moment(startOfQuarter).tz(timezone).add(15,"day")
                endOfQuarter = moment(endOfQuarter).tz(timezone).add(15,"day")
    
                if(qtrCommitCutOff){
                    startOfQuarter = moment(allQuarters[allQuarters.currentQuarter].start).tz(timezone).add(qtrCommitCutOff,"day")
                    endOfQuarter = moment(allQuarters[allQuarters.currentQuarter].end).tz(timezone).add(qtrCommitCutOff,"day")
                }
    
                var editAccess = new Date()<= new Date(startOfQuarter)
    
                if(req.query.startDate){
                    startOfQuarter = moment(req.query.startDate).tz(timezone)
                    endOfQuarter = moment(startOfQuarter).tz(timezone).add(3,"month")
                    editAccess = new Date()<= new Date(startOfQuarter);
                }
    
                var quarterYear = common.getQuarterYear(allQuarters,startOfQuarter,fyMonth,timezone,fyRange.start);
    
                async.parallel([
                    function (callback) {
                        getOpportunitiesInStage(common.castToObjectId(userId),null,null,commitStage,callback)
                    },
                    function (callback) {
                        oppCommitObj.getCommitsByQuarterRange(common.castToObjectId(userId),quarterYear,callback)
                    }
                ], function (errors,data) {
                    common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
                        var currentDate = req.query.startDate?req.query.startDate:moment().toISOString()
    
                        var oppsIds = [],
                            oppsUserIds = [];
    
                        _.each(data[0],function (op) {
                            if(op){
                                oppsUserIds.push(common.castToObjectId(op.userId));
                                oppsIds.push(op.opportunityId);
                            }
                        });
    
                        getDealsAtRiskForOpps(oppsUserIds,oppsIds,function (errD,dealsAtRisk) {
                            res.send({
                                dealsAtRisk:dealsAtRisk,
                                editAccess:editAccess,
                                commitQuarter:{
                                    start: new Date(allQuarters[allQuarters.currentQuarter].start),
                                    end: new Date(allQuarters[allQuarters.currentQuarter].end)
                                },
                                cutOffDate:new Date(moment(allQuarters[allQuarters.currentQuarter].start).tz(timezone).add(qtrCommitCutOff,"day")),
                                currentDate:currentDate,
                                fyRange:fyRange,
                                commitStage:commitStage,
                                qStart:allQuarters[allQuarters.currentQuarter].start,
                                qEnd:allQuarters[allQuarters.currentQuarter].end,
                                opps:data[0] && netGrossMarginReq?calculateNGM(data[0]):data[0],
                                commitCurrentQuarter:checkForNoCurrentQuarterCommits(data[1],data[0],commitStage,startOfQuarter,endOfQuarter,netGrossMarginReq,userProfile,currentDate)
                            })
                        });
                    })
                });
            })
        })
    })

})

router.get('/review/pipeline/status', common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;

    var userId = req.query.userId?req.query.userId:common.getUserId(req.user);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var qEnd = allQuarters[allQuarters.currentQuarter].end;

        var monthsInQuarters = [];

        monthsInQuarters.push(moment(qStart).tz(timezone).format('MMMM'))
        monthsInQuarters.push(moment(qStart).tz(timezone).add(1,"month").format('MMMM'))
        monthsInQuarters.push(moment(qEnd).tz(timezone).format('MMMM'))

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if(oppStages){
                _.each(oppStages,function (st) {
                    if(st.commitStage){
                        commitStage = st.name;
                    }
                });
            }

            var currentWeek = moment().week();
            var prevWeekYear = moment().week()-1+""+moment().year()
            if(currentWeek == 1){
                prevWeekYear = moment().weeksInYear()+""+moment().year()-1
            }

            var currentWeekYear = currentWeek+""+moment().year()

            async.parallel([
                function (callback) {
                    getOpportunitiesInStage(common.castToObjectId(userId),fyRange.start,fyRange.end,commitStage,callback)
                },
                function (callback) {
                    oppCommitObj.getCommitsByWeekRange(common.castToObjectId(userId),prevWeekYear,callback)
                },
                function (callback) {
                    oppCommitObj.getCommitsByWeekRange(common.castToObjectId(userId),currentWeekYear,callback)
                }
            ], function (errors,data) {
                common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
                    res.send({
                        fyRange:fyRange,
                        commitStage:commitStage,
                        qStart:qStart,
                        qEnd:qEnd,
                        monthsInQuarters:monthsInQuarters,
                        opps:data[0] && netGrossMarginReq?calculateNGM(data[0]):data[0],
                        commitsPrev:checkForNoPrevCommits(data[1],netGrossMarginReq),
                        commitsCurrent:checkForNoCurrentCommits(data[2],data[0],commitStage,qStart,qEnd,netGrossMarginReq)
                    })
                })
            });
        })
    })
})

router.get('/review/meeting/team/pipeline', common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;

    var userId = req.query.userId?req.query.userId:common.getUserId(req.user);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var qEnd = allQuarters[allQuarters.currentQuarter].end;

        var monthsInQuarters = [];

        monthsInQuarters.push(moment(qStart).tz(timezone).format('MMMM'))
        monthsInQuarters.push(moment(qStart).tz(timezone).add(1,"month").format('MMMM'))
        monthsInQuarters.push(moment(qEnd).tz(timezone).format('MMMM'))

        var userIds = []
        if(req.query.hierarchylist){
            userIds = req.query.hierarchylist.split(',')
        } else {
            userIds.push(userId)
        }

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if(oppStages){
                _.each(oppStages,function (st) {
                    if(st.commitStage){
                        commitStage = st.name;
                    }
                });
            }
            var weekStartDateTime = common.getCommitDayTimeStart(companyDetails);
            var weekEndDateTime = moment(weekStartDateTime).add(1,'week');

            var nextWeekMoment = weekStartDateTime;
            var currentWeek = moment(nextWeekMoment).week();
            var prevWeekYear = moment().week()+""+moment().year()
            // if(currentWeek == 1){
            //     prevWeekYear = moment().weeksInYear()+""+moment().year()-1
            // }

            var currentWeekYear = currentWeek+""+moment(nextWeekMoment).year()

            async.parallel([
                function (callback) {
                    getOpportunitiesInStageMultiUsers(userIds,fyRange.start,fyRange.end,commitStage,[],companyId,callback)
                },
                function (callback) {
                    oppCommitObj.getCommitsByWeekRangeMultiUsers(common.castListToObjectIds(userIds),prevWeekYear,callback)
                },
                function (callback) {
                    oppCommitObj.getCommitsByWeekRangeMultiUsers(common.castListToObjectIds(userIds),currentWeekYear,callback)
                }
            ], function (errors,data) {
                common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {

                    var commitsPrev = emptyCommit(),
                        commitsCurrent = emptyCommit();

                    if(req.query.hierarchylist && data[1] && data[1].flattenedData){
                        commitsPrev = checkForNoPrevCommits(data[1].flattenedData,netGrossMarginReq)
                    }

                    if(req.query.hierarchylist && data[2] && data[2].flattenedData){
                        commitsCurrent = checkForNoCurrentCommits(data[2].flattenedData,data[0],commitStage,qStart,qEnd,netGrossMarginReq)
                    }

                    var opps = []

                    if(req.query.hierarchylist && data[0]){
                        opps = netGrossMarginReq?calculateNGM(data[0]):data[0]
                    }

                    res.send({
                        fyRange:fyRange,
                        commitStage:commitStage,
                        qStart:qStart,
                        qEnd:qEnd,
                        monthsInQuarters:monthsInQuarters,
                        commitsPrevRaw:req.query.hierarchylist && data[1] && data[1].rawData?data[1] && data[1].rawData:null,
                        commitsCurrentRaw:req.query.hierarchylist && data[2] && data[2].rawData?data[2] && data[2].rawData:null,
                        commitsPrev:commitsPrev,
                        commitsCurrent:commitsCurrent,
                        opps:opps
                    })
                })
            });
        })
    })
});

router.get('/review/pipeline/movement', common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;
    var userId = common.getUserId(req.user);
    var range = req.query.range?req.query.range:"Month";

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        if(req.query.opportunityIds){
            var oppIds = req.query.opportunityIds.split(',');
            var query = {
                opportunityId: {$in:oppIds}
            }

            var projection = {
                opportunityId:1,
                stageName:1,
                createdDate:1,
                closeDate:1,
                amount:1,
                netGrossMargin:1,
                currency:1
            }

            oppManagementObj.getOppByCustomQuery(query,projection,function (err,opps) {

                if(!err && opps && opps.length>0){
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data: calculateNGM(opps)
                    })
                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data:[]
                    })
                }
            });

        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data:[]
            })
        }
    });
});

function emptyCommit() {
    return {
        "quarter": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "month": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "week": {
            "userCommitAmount": 0,
            "relatasCommitAmount": 0
        },
        "opportunities": []
    }
}

function checkForNoPrevCommits(data,netGrossMarginReq){

    if(!data){
        return {
            "quarter": {
                "userCommitAmount": 0,
                "relatasCommitAmount": 0
            },
            "month": {
                "userCommitAmount": 0,
                "relatasCommitAmount": 0
            },
            "week": {
                "userCommitAmount": 0,
                "relatasCommitAmount": 0
            },
            "opportunities": []
        }
    } else {
        var opportunities = [];
        if(data.opportunities.length>0 && netGrossMarginReq){

            _.each(data.opportunities,function (el) {
                el = JSON.parse(JSON.stringify(el))
                el.opps = calculateNGM(el.opps)

                opportunities.push({opps:el.opps})
            })

            data.opportunities = opportunities

        }

        return data
    }
}

function checkForNoWeekCommits(data,oppsInCommitStage,commitStage,editAccess,netGrossMarginReq){

    if(!data){
        var filler = {
            "quarter": {
                "userCommitAmount": 0,
                "relatasCommitAmount": 0
            },
            "month": {
                "userCommitAmount": 0,
                "relatasCommitAmount": 0
            },
            "week": {
                "userCommitAmount": 0,
                "relatasCommitAmount": 0
            },
            "opportunities": []
        }

        if(editAccess){
            filler.opportunities = oppsInCommitStage
        }

        return filler;

    } else {

        if(data.opportunities.length>0){
            data.opportunities = _.flatten(_.pluck(data.opportunities,"opps"));
        }

        if(netGrossMarginReq) {
            data.opportunities = calculateNGM(data.opportunities)
        }

        data.opportunities = data.opportunities.filter(function (el) {
            if(el.stageName == commitStage || el.relatasStage == commitStage){
                return el;
            }
        });

        if(editAccess){
            data.opportunities = oppsInCommitStage
        }

        return data
    }
}

function checkForNoCurrentMonthCommits(data,currentOpps,commitStage,dateMin,dateMax,netGrossMarginReq,monthYear,userProfile,currentDate){

    if(!data){

        if(currentOpps.length>0 && netGrossMarginReq){
            currentOpps = JSON.parse(JSON.stringify(currentOpps))
            currentOpps = calculateNGM(currentOpps)
        }

        return {
            opportunities: currentOpps,
            date: null,
            commitWeekYear: null,
            monthYear: monthYear,
            commitForDate: dateMin,
            week:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            month:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            quarter:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            userId: userProfile._id,
            commitFor:'month',
            userEmailId:userProfile.emailId
        }
    } else {

        if(new Date()<= new Date(dateMin)){
            data.opportunities = currentOpps;
        }

        if(data.opportunities.length>0 && netGrossMarginReq){
            data.opportunities = calculateNGM(data.opportunities)
        }

        return data
    }
}

function checkForNoCurrentQuarterCommits(data,currentOpps,commitStage,dateMin,dateMax,netGrossMarginReq,userProfile,currentDate,quarterYear){

    if(!data){

        if(currentOpps.length>0 && netGrossMarginReq){
            currentOpps = JSON.parse(JSON.stringify(currentOpps))
            currentOpps = calculateNGM(currentOpps)
        }

        return {
            opportunities: currentOpps,
            date: null,
            commitWeekYear: null,
            monthYear: null,
            quarterYear:quarterYear,
            commitForDate: dateMin,
            week:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            month:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            quarter:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            userId: userProfile._id,
            commitFor:'quarter',
            userEmailId:userProfile.emailId
        }
    } else {

        if(new Date()<= new Date(dateMin)){
            data.opportunities = currentOpps;
        }

        if(data.opportunities.length>0 && netGrossMarginReq){
            data.opportunities = calculateNGM(data.opportunities)
        }

        return data
    }
}

function checkForNoCurrentCommits(data,currentOpps,commitStage,qStart,qEnd,netGrossMarginReq){

    if(!data){
        var thisWeekCommitVal = 0
            ,thisMonthCommitVal = 0
            ,thisQtrCommitVal = 0,
            startOfMonth = moment().startOf('month'),
            endOfMonth = moment().endOf('month')

        _.each(currentOpps,function (op) {

            if(op.relatasStage == commitStage){
                if(new Date(op.closeDate)>= (new Date(qStart)) && new Date(op.closeDate) <= (new Date(qEnd))){
                    thisQtrCommitVal = thisQtrCommitVal+parseFloat(op.amount);
                }

                if(new Date(op.closeDate)>= (new Date(startOfMonth)) && new Date(op.closeDate) <= (new Date(endOfMonth))){
                    thisMonthCommitVal = thisMonthCommitVal+parseFloat(op.amount);
                }

                if(new Date(op.closeDate)>= (new Date(moment().startOf('isoweek'))) && new Date(op.closeDate) <= (new Date(moment().endOf('isoweek')))){
                    thisWeekCommitVal = thisWeekCommitVal+parseFloat(op.amount);
                }
            }

        });

        return {
            "quarter": {
                "userCommitAmount": 0,
                "relatasCommitAmount": thisQtrCommitVal
            },
            "month": {
                "userCommitAmount": 0,
                "relatasCommitAmount": thisMonthCommitVal
            },
            "week": {
                "userCommitAmount": 0,
                "relatasCommitAmount": thisWeekCommitVal
            },
            "opportunities": []
        }
    } else {

        if(data.opportunities.length>0 && netGrossMarginReq){

            _.each(data.opportunities,function (el) {
                el = JSON.parse(JSON.stringify(el))
                calculateNGM(el.opps)
            })
        }

        return data
    }
}

router.get('/review/all/opps', common.isLoggedInUserOrMobile, function(req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile ? userProfile.companyId : null;

    var userId = req.query.userId ? req.query.userId : common.getUserId(req.user);
    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
            oppManagementObj.getOppForUserById(common.castToObjectId(userId),netGrossMarginReq,function (err,opps) {

                if(err){
                    res.send({
                        fyRange:fyRange,
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data:[]
                    })
                } else {
                    res.send({
                        fyRange:fyRange,
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data:opps
                    })
                }
            })
        })
    });

});

router.post('/review/past/commits', common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;

    var userId = req.body.userId?req.body.userId:common.getUserId(req.user);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if(oppStages){
                _.each(oppStages,function (st) {
                    if(st.commitStage){
                        commitStage = st.name;
                    }
                });
            };

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            var weekStartDateTime = common.getCommitDayTimeStart(companyDetails);
            var weekEndDateTime = moment(weekStartDateTime).add(1,'week');

            var nextWeekMoment = weekStartDateTime;
            var dateMin = req.body.dateMin?moment(req.body.dateMin).tz(timezone):moment().tz(timezone).subtract(3,"months").endOf('isoweek'),
                dateMax = req.body.dateMax?moment(req.body.dateMax).tz(timezone): moment(nextWeekMoment).tz(timezone)

            oppCommitObj.getCommitsByDateRange(common.castToObjectId(userId),dateMin,dateMax,function (err,data) {

                var weekYears = getWeekYearBetweenTwoDates(dateMin,dateMax,timezone);
                var weekYearsList = getWeekYearBetweenTwoDates(moment(fyRange.start),moment(fyRange.end),timezone);
                var weekYearsObj = {};

                _.each(weekYearsList,function (wy) {
                    weekYearsObj[wy.commitWeekYear] = wy
                })

                var allWeeks = _.pluck(weekYears,"commitWeekYear");

                var filler = [];

                var nonExistingWeeksCommits = _.difference(allWeeks,_.pluck(data,"commitWeekYear"))

                _.each(nonExistingWeeksCommits,function (wy) {
                    _.each(weekYears,function (el) {
                        if(wy == el.commitWeekYear){
                            filler.push(el)
                        }
                    });
                });

                var allData = filler.concat(data);

                allData.sort(function (o1, o2) {
                    return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
                });

                var query = {
                    userId:common.castToObjectId(userId),
                    $or:[{relatasStage:commitStage},{stageName:commitStage}]
                };

                var portfolioAcc = null;
                var portfolioHead = null;

                if(req.body.selectedPortfolio){

                    var regionAccess = [],
                        productAccess = [],
                        businessUnits = [],
                        verticalAccess = [];

                    if(req.body.selectedPortfolio.type == "Products"){
                        productAccess.push(req.body.selectedPortfolio.name);

                        if(req.body.selectedPortfolio.isHead){
                            portfolioHead = [{productType:req.body.selectedPortfolio.name}]
                        }
                    }

                    if(req.body.selectedPortfolio.type == "Verticals"){
                        verticalAccess.push(req.body.selectedPortfolio.name);
                        if(req.body.selectedPortfolio.isHead){
                            portfolioHead = [{vertical:req.body.selectedPortfolio.name}]
                        }
                    }

                    if(req.body.selectedPortfolio.type == "Business Units"){
                        businessUnits.push(req.body.selectedPortfolio.name)
                        if(req.body.selectedPortfolio.isHead){
                            portfolioHead = [{businessUnit:req.body.selectedPortfolio.name}]
                        }
                    }

                    if(req.body.selectedPortfolio.type == "Regions"){
                        regionAccess.push(req.body.selectedPortfolio.name)
                        if(req.body.selectedPortfolio.isHead){
                            portfolioHead = [{"geoLocation.zone":req.body.selectedPortfolio.name}]
                        }
                    }

                    _.each(req.body.selectedPortfolio.accessLevel,function(ac){

                        if(ac.type == "Products"){
                            productAccess = ac.values;
                        }

                        if(ac.type == "Verticals"){
                            verticalAccess = ac.values;
                        }

                        if(ac.type == "Business Units"){
                            businessUnits = ac.values;
                        }

                        if(ac.type == "Regions"){
                            regionAccess = ac.values;
                        }

                    });

                    portfolioAcc = accessControlSettings(regionAccess,
                        productAccess,
                        verticalAccess,
                        companyId,
                        businessUnits)

                    if(portfolioHead){
                        query = {
                            companyId:common.castToObjectId(companyId),
                            relatasStage:commitStage,
                            $and:portfolioHead
                        }
                    } else {
                        query = {
                            relatasStage:commitStage,
                            $or:[
                                {$and: portfolioAcc}
                            ]
                        }
                    }
                }

                oppManagementObj.getOppByCustomQuery(query,null,function (errOpps,oppsInCommitStage) {

                    var startWk = new Date(moment().tz(timezone).startOf('isoweek'));
                    var endWk = new Date(moment().tz(timezone).endOf('isoweek'));

                    var opps = [];
                    _.each(opps,function (op) {
                        if(new Date(op.closeDate)>=new Date(startWk) && new Date(op.closeDate)<=new Date(endWk)){
                            opps.push(op)
                        }
                    })

                    oppManagementObj.oppConversionByWeek(common.castToObjectId(userId),dateMin,dateMax,primaryCurrency,currenciesObj,common.castToObjectId(companyId),portfolioAcc,portfolioHead,function (err,oppsConversion) {
                        
                        var conversionData = oppConversionFillers(oppsConversion,allWeeks,weekYearsObj,timezone);

                        var allDataWeekYears = _.pluck(allData,"commitWeekYear");
                        var allWeekYears = _.flatten(_.pluck(conversionData.created,"weekYear"),_.pluck(conversionData.oppsWon,"weekYear"),allDataWeekYears)

                        var extendAllData = _.difference(allWeekYears,allDataWeekYears);
                        var fillersTwo = [];

                        _.each(extendAllData,function (el) {

                            var year = el.substr(-4);
                            var week = el.split(year)[0]

                            var date = moment().tz(timezone).week(week).year(year);
                            date = new Date(moment(date).subtract(1,"week"))

                            fillersTwo.push({
                                "quarter": {
                                    "userCommitAmount": 0,
                                    "relatasCommitAmount": 0
                                },
                                "month": {
                                    "userCommitAmount": 0,
                                    "relatasCommitAmount": 0
                                },
                                "week": {
                                    "userCommitAmount": 0,
                                    "relatasCommitAmount": 0
                                },
                                "opportunities": [],
                                noCommits:true,
                                date:new Date(date),
                                commitWeekYear:moment(date).week()+""+moment(date).year()
                            })
                        });

                        removeNextWeekFromAllSourcesAndRespond(err,res,weekYears,conversionData,fillersTwo,allData,commitStage,errOpps,opps,allWeekYears,dateMin,dateMax,companyId,fyRange,weekStartDateTime,timezone,weekYearsObj,oppsInCommitStage)
                    })
                })
            })
        })
    })
})

router.post('/get/portfolio/summary', common.isLoggedInUserOrMobile, function(req, res) {

    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile ? userProfile.companyId : null;
    var userId = req.body.userId ? req.body.userId : common.getUserId(req.user);

    common.userFiscalYear(common.castToObjectId(userId), function (err, fyMonth, fyRange, allQuarters, timezone) {

        oppWeightsObj.getOppStages(common.castToObjectId(companyId), function (oppStages, companyDetails, monthCommitCutOff) {

            var commitStage = "Proposal"; // default.
            if (oppStages) {
                _.each(oppStages, function (st) {
                    if (st.commitStage) {
                        commitStage = st.name;
                    }
                });
            }

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            var won = 0,
                pipeline = 0;

            if(req.body.selectedPortfolio){

                var portfolioHead = null
                var regionAccess = [],
                    productAccess = [],
                    businessUnits = [],
                    verticalAccess = [];

                if(req.body.selectedPortfolio.type == "Products"){
                    productAccess.push(req.body.selectedPortfolio.name);

                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{productType:req.body.selectedPortfolio.name}]
                    }
                }

                if(req.body.selectedPortfolio.type == "Verticals"){
                    verticalAccess.push(req.body.selectedPortfolio.name);
                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{vertical:req.body.selectedPortfolio.name}]
                    }
                }

                if(req.body.selectedPortfolio.type == "Business Units"){
                    businessUnits.push(req.body.selectedPortfolio.name)
                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{businessUnit:req.body.selectedPortfolio.name}]
                    }
                }

                if(req.body.selectedPortfolio.type == "Regions"){
                    regionAccess.push(req.body.selectedPortfolio.name)
                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{"geoLocation.zone":req.body.selectedPortfolio.name}]
                    }
                }

                _.each(req.body.selectedPortfolio.accessLevel,function(ac){

                    if(ac.type == "Products"){
                        productAccess = ac.values;
                    }

                    if(ac.type == "Verticals"){
                        verticalAccess = ac.values;
                    }

                    if(ac.type == "Business Units"){
                        businessUnits = ac.values;
                    }

                    if(ac.type == "Regions"){
                        regionAccess = ac.values;
                    }

                });

                var portfolioAcc = accessControlSettings(regionAccess,
                    productAccess,
                    verticalAccess,
                    companyId,
                    businessUnits)

                var start = moment().startOf("month");
                var end = moment().endOf("month");

                if(req.body.range == "Quarter"){
                    start = allQuarters[allQuarters.currentQuarter].start;
                    end = allQuarters[allQuarters.currentQuarter].end;
                }

                if(req.body.range == "Week"){
                    start = moment().startOf("week");
                    end = moment().endOf("week");
                }

                var query = {
                    companyId:common.castToObjectId(companyId),
                    relatasStage:{$in:[commitStage,"Close Won"]},
                    closeDate:{$gte: new Date(start),$lte: new Date(end)},
                    $or:[{$and: portfolioAcc}]
                }

                if(portfolioHead){

                    query = {
                        companyId:common.castToObjectId(companyId),
                        relatasStage:{$in:[commitStage,"Close Won"]},
                        closeDate:{$gte: new Date(start),$lte: new Date(end)},
                        $and:portfolioHead
                    }
                }

                oppManagementObj.getOppByCustomQuery(query,null,function (errOpps,opps) {

                    if(!errOpps && opps && opps.length>0){
                        _.each(opps,function(op){
                            op.amountNonNGM = op.amount;
                            if(op.netGrossMargin || op.netGrossMargin == 0){
                                var amountWithNGM = op.netGrossMargin*op.amount

                                if(amountWithNGM){
                                    amountWithNGM = amountWithNGM/100
                                }

                                op.amount = amountWithNGM;
                                op.convertedAmt = op.amount;
                                op.convertedAmtWithNgm = op.amountWithNgm

                                if(op.currency && op.currency !== primaryCurrency){

                                    if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                                        op.convertedAmt = op.amount/currenciesObj[op.currency].xr
                                    }

                                    if(op.netGrossMargin || op.netGrossMargin == 0){
                                        op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                                    }

                                    op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

                                }
                            }

                            if(op.relatasStage == "Close Won"){
                                won = won + op.convertedAmt;
                            }

                            if(op.relatasStage == commitStage){
                                pipeline = pipeline + op.convertedAmt;
                            }
                        });
                    }

                    res.send({
                        won:won,
                        pipeline:pipeline
                    });
                });
            } else {
                res.send({
                    won:won,
                    pipeline:pipeline
                });
            }
        })
    })
});

router.post('/review/monthly/commits/history', common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;
    var userId = req.body.userId?req.body.userId:common.getUserId(req.user);
    var hierarchyType = req.body.hierarchyType;

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails,monthCommitCutOff) {

            var commitStage = "Proposal"; // default.
            if(oppStages){
                _.each(oppStages,function (st) {
                    if(st.commitStage){
                        commitStage = st.name;
                    }
                });
            }

            if(!monthCommitCutOff){
                monthCommitCutOff = 10
            }

            var startOfMonth = moment(moment().startOf('month')).add(monthCommitCutOff,"day"),
                endOfMonth = moment(startOfMonth).add(1,'month'),
                monthYear = moment(startOfMonth).month()+""+moment(startOfMonth).year(),
                dateMin = moment().subtract(11,"month");

            endOfMonth = moment(endOfMonth).endOf("day");

            var query = {
                userId:common.castToObjectId(userId),
                $or:[{relatasStage:commitStage},{stageName:commitStage}]
            }

            if(req.body.userEmailId){
                query = {
                    $or:[{userId:common.castToObjectId(userId),$or:[{relatasStage:commitStage},{stageName:commitStage}]},
                        {"usersWithAccess.emailId":req.body.userEmailId,$or:[{relatasStage:commitStage},{stageName:commitStage}]}
                        ]
                }
            }

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            var portfolioAcc = null;
            var portfolioHead = null;

            if(req.body.selectedPortfolio){

                var regionAccess = [],
                    productAccess = [],
                    businessUnits = [],
                    verticalAccess = [];

                if(req.body.selectedPortfolio.type == "Products"){
                    productAccess.push(req.body.selectedPortfolio.name);

                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{productType:req.body.selectedPortfolio.name}]
                    }
                }

                if(req.body.selectedPortfolio.type == "Verticals"){
                    verticalAccess.push(req.body.selectedPortfolio.name);
                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{vertical:req.body.selectedPortfolio.name}]
                    }
                }

                if(req.body.selectedPortfolio.type == "Business Units"){
                    businessUnits.push(req.body.selectedPortfolio.name)
                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{businessUnit:req.body.selectedPortfolio.name}]
                    }
                }

                if(req.body.selectedPortfolio.type == "Regions"){
                    regionAccess.push(req.body.selectedPortfolio.name)
                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{"geoLocation.zone":req.body.selectedPortfolio.name}]
                    }
                }

                _.each(req.body.selectedPortfolio.accessLevel,function(ac){

                    if(ac.type == "Products"){
                        productAccess = ac.values;
                    }

                    if(ac.type == "Verticals"){
                        verticalAccess = ac.values;
                    }

                    if(ac.type == "Business Units"){
                        businessUnits = ac.values;
                    }

                    if(ac.type == "Regions"){
                        regionAccess = ac.values;
                    }

                });

                portfolioAcc = accessControlSettings(regionAccess,
                    productAccess,
                    verticalAccess,
                    companyId,
                    businessUnits)

                if(portfolioHead){
                    query = {
                        companyId:common.castToObjectId(companyId),
                        relatasStage:commitStage,
                        $and:portfolioHead
                    }
                } else {
                    query = {
                        relatasStage:commitStage,
                        $or:[
                            {$and: portfolioAcc}
                        ]
                    }
                }
            }

            oppManagementObj.getOppByCustomQuery(query,null,function (errOpps,oppsInCommitStage) {

                var oppIds = [];
                var oppsInCommitStageThisMonth = [];
                _.each(oppsInCommitStage,function (op) {
                    oppIds.push(op.opportunityId)
                    if(new Date(op.closeDate)>=new Date(startOfMonth) && new Date(op.closeDate)<=new Date(endOfMonth)){
                        oppsInCommitStageThisMonth.push(op)
                    }
                });

                oppCommitObj.getPastMonthlyCommits(common.castToObjectId(userId),userProfile.emailId,monthYear,oppsInCommitStageThisMonth,function (err,data) {
                    oppManagementObj.oppConversionByMonthForPortfolios(common.castToObjectId(userId),dateMin,new Date(moment().endOf("day")),primaryCurrency,currenciesObj,portfolioAcc,common.castToObjectId(companyId),portfolioHead,function (err,oppsConversion) {
                        common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {

                            var datesForTargets = [],
                                allDates = [];
                            _.each(data,function (el) {
                                datesForTargets.push({
                                    monthYear:el.monthYear,
                                    date:el.date
                                });
                                allDates.push(el.date);
                            });

                            getTargetsByMonth([common.castToObjectId(userId)],datesForTargets,allDates,function (err3,targets) {
                                getDealsAtRiskForOpps([common.castToObjectId(userId)],oppIds,function (errRisk,dealsAtRisk) {
                                    res.send({
                                        dealsAtRisk:dealsAtRisk,
                                        targets:targets,
                                        oppsConversion:oppsConversion,
                                        commits:data.slice(Math.max(data.length - 12, 0)),
                                        commitStage:commitStage,
                                        dateMin:dateMin,
                                        dateMax:new Date(),
                                        fyRange:fyRange,
                                        timezone:timezone,
                                        netGrossMarginReq:netGrossMarginReq,
                                        oppsInCommitStageThisMonth:oppsInCommitStageThisMonth,
                                        oppsInCommitStage:oppsInCommitStage,
                                        qtrStart:allQuarters[allQuarters.currentQuarter].start,
                                        qtrEnd:allQuarters[allQuarters.currentQuarter].end,
                                    })
                                })
                            })
                        })
                    })
                })
            });
        })
    })
});

function getDealsAtRiskForOpps(userIds,opportunityIds,callback){
    if(opportunityIds && opportunityIds.length>0){
        dealsAtRiskObj.findAtRiskOppIds(userIds,opportunityIds,callback)
    } else {
        callback(null,[])
    }
}

router.post('/review/quarterly/commits/history', common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;
    var userId = req.body.userId?req.body.userId:common.getUserId(req.user);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if(oppStages){
                _.each(oppStages,function (st) {
                    if(st.commitStage){
                        commitStage = st.name;
                    }
                });
            }

            var qtrCommitCutOff = 15

            if(companyDetails && companyDetails.qtrCommitCutOff){
                qtrCommitCutOff = companyDetails.qtrCommitCutOff
            }

            var startOfQuarter = allQuarters[allQuarters.currentQuarter].start,
                endOfQuarter = allQuarters[allQuarters.currentQuarter].end;

            startOfQuarter = moment(startOfQuarter).tz(timezone).add(15,"day")
            endOfQuarter = moment(endOfQuarter).tz(timezone).add(15,"day")

            if(qtrCommitCutOff){
                startOfQuarter = moment(allQuarters[allQuarters.currentQuarter].start).tz(timezone).add(qtrCommitCutOff,"day")
                endOfQuarter = moment(allQuarters[allQuarters.currentQuarter].end).tz(timezone).add(qtrCommitCutOff,"day")
            }

            var query = {
                userId:common.castToObjectId(userId),
                $or:[{relatasStage:commitStage},{stageName:commitStage}]
            }

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            var portfolioAcc = null;
            var portfolioHead = null;

            if(req.body.selectedPortfolio){

                var regionAccess = [],
                    productAccess = [],
                    businessUnits = [],
                    verticalAccess = [];

                if(req.body.selectedPortfolio.type == "Products"){
                    productAccess.push(req.body.selectedPortfolio.name);

                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{productType:req.body.selectedPortfolio.name}]
                    }
                }

                if(req.body.selectedPortfolio.type == "Verticals"){
                    verticalAccess.push(req.body.selectedPortfolio.name);
                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{vertical:req.body.selectedPortfolio.name}]
                    }
                }

                if(req.body.selectedPortfolio.type == "Business Units"){
                    businessUnits.push(req.body.selectedPortfolio.name)
                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{businessUnit:req.body.selectedPortfolio.name}]
                    }
                }

                if(req.body.selectedPortfolio.type == "Regions"){
                    regionAccess.push(req.body.selectedPortfolio.name)
                    if(req.body.selectedPortfolio.isHead){
                        portfolioHead = [{"geoLocation.zone":req.body.selectedPortfolio.name}]
                    }
                }

                _.each(req.body.selectedPortfolio.accessLevel,function(ac){

                    if(ac.type == "Products"){
                        productAccess = ac.values;
                    }

                    if(ac.type == "Verticals"){
                        verticalAccess = ac.values;
                    }

                    if(ac.type == "Business Units"){
                        businessUnits = ac.values;
                    }

                    if(ac.type == "Regions"){
                        regionAccess = ac.values;
                    }

                });

                portfolioAcc = accessControlSettings(regionAccess,
                    productAccess,
                    verticalAccess,
                    companyId,
                    businessUnits)

                if(portfolioHead){
                    query = {
                        companyId:common.castToObjectId(companyId),
                        relatasStage:commitStage,
                        $and:portfolioHead
                    }
                } else {
                    query = {
                        relatasStage:commitStage,
                        $or:[
                            {$and: portfolioAcc}
                        ]
                    }
                }
            }

            oppManagementObj.getOppByCustomQuery(query,null,function (errOpps,oppsInCommitStage) {

                var oppsInCommitStageThisQtr = [],oppIds = [];
                _.each(oppsInCommitStage,function (op) {
                    oppIds.push(op.opportunityId);
                    if(new Date(op.closeDate)>=new Date(allQuarters[allQuarters.currentQuarter].start) && new Date(op.closeDate)<=new Date(allQuarters[allQuarters.currentQuarter].end)){
                        oppsInCommitStageThisQtr.push(op)
                    }
                });

                oppCommitObj.getPastQuarterlyCommits(common.castToObjectId(userId),userProfile.emailId,allQuarters,oppsInCommitStageThisQtr,timezone,fyRange.start,function (err,data) {

                    var allDates = [],
                    qtrRanges = [];

                    _.each(data,function (el) {
                        qtrRanges.push({
                            quarterYear:el.quarterYear,
                            qtrStart:el.qtrStart,
                            qtrEnd:el.qtrEnd
                        });

                        allDates.push(new Date(moment(el.qtrStart)));
                        allDates.push(new Date(moment(el.qtrStart).add(1,"month")));
                        allDates.push(new Date(moment(el.qtrStart).add(2,"month")));
                    });

                    oppManagementObj.oppConversionByPastQuarter(common.castToObjectId(userId),qtrRanges,primaryCurrency,currenciesObj,allQuarters,portfolioAcc,portfolioHead,common.castToObjectId(companyId),function (err,oppsConversion) {
                        common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {

                            var dateMin = new Date(Math.min.apply(null,allDates));
                            var dateMax = new Date(Math.max.apply(null,allDates));

                            if(dateMax< new Date(fyRange.end)){
                                dateMax = new Date(fyRange.end);
                            }

                            oppManagementObj.getTargetForUserByQtrs([common.castToObjectId(userId)],dateMin,dateMax,function (err3,targets) {

                                if(portfolioAcc && req.body.selectedPortfolio.targets && req.body.selectedPortfolio.targets[0]){
                                    _.each(req.body.selectedPortfolio.targets[0].values,function (va) {
                                        va.target = va.amount;
                                    })
                                    targets = req.body.selectedPortfolio.targets[0].values;
                                }

                                if(targets && targets.length>0){
                                    _.each(targets,function (tr) {
                                        _.each(data,function (el) {
                                            if(new Date(tr.date) >= new Date(el.qtrStart) && new Date(tr.date) <= new Date(el.qtrEnd)){
                                                el.target = el.target+tr.target
                                            }
                                        })
                                    })
                                }

                                getDealsAtRiskForOpps([common.castToObjectId(userId)],oppIds,function (errRisk,dealsAtRisk) {

                                    res.send({
                                        dealsAtRisk:dealsAtRisk,
                                        targets:targets,
                                        oppsConversion:oppsConversion,
                                        commits:data,
                                        commitStage:commitStage,
                                        dateMin:fyRange.start,
                                        dateMax:new Date(),
                                        fyRange:fyRange,
                                        timezone:timezone,
                                        oppsInCommitStage:oppsInCommitStage,
                                        netGrossMarginReq:netGrossMarginReq,
                                        qtrStart:allQuarters[allQuarters.currentQuarter].start,
                                        qtrEnd:allQuarters[allQuarters.currentQuarter].end
                                    })
                                });
                            })

                        })
                    })
                })
            })
        })
    })
})

function removeNextWeekFromAllSourcesAndRespond(err,res,weekYears,conversionData,fillersTwo,allData,commitStage,errOpps,opps,allWeekYears,dateMin,dateMax,companyId,fyRange,weekStartDateTime,timezone,weekYearsObj,oppsInCommitStage) {

    var nextWeekMoment = weekStartDateTime;
    var maxWeekYear = moment(nextWeekMoment).week()+""+moment(nextWeekMoment).year() // Current week year

    var commits = fillersTwo && fillersTwo.length>0?allData.concat(fillersTwo):allData

    commits = commits.filter(function (el) {
        return new Date(el.date) <= new Date(nextWeekMoment)
    })

    allWeekYears = allWeekYears.filter(function (el) {
        return new Date(el.date) <= new Date(nextWeekMoment)
    })

    if(conversionData.created && conversionData.created.length>0){
        conversionData.created = conversionData.created.filter(function (el) {
            if(parseInt(el.weekYear.slice(-4))< parseInt(maxWeekYear.slice(-4)) || parseInt(el.weekYear) <= parseInt(maxWeekYear)){
                return el
            }
        })
    }

    if(conversionData.oppsWon && conversionData.oppsWon.length>0){
        conversionData.oppsWon = conversionData.oppsWon.filter(function (el) {
            if(parseInt(el.weekYear.slice(-4))< parseInt(maxWeekYear.slice(-4)) || parseInt(el.weekYear) <= parseInt(maxWeekYear)){
                return el
            }
        })
    }

    common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {

        if(commits && commits.length>0 && netGrossMarginReq){
            _.each(commits,function (el) {
                var oppsInCommitStage = [];
                if(el.opportunities){
                    _.each(el.opportunities,function (oppGrp) {
                        oppGrp = JSON.parse(JSON.stringify(oppGrp))
                        oppGrp.opps = calculateNGM(oppGrp.opps)
                        _.each(oppGrp.opps,function (op) {
                            if(op.relatasStage == commitStage || op.stageName == commitStage){
                                oppsInCommitStage.push(op)
                            }
                        })
                    });
                }
                var startOfWeek = moment(buildDateObj(el.commitWeekYear,timezone)).startOf("week"),
                    endOfWeek = moment(startOfWeek).endOf("week");

                var weekCommit = 0;
                _.each(oppsInCommitStage,function (op) {
                    if(new Date(op.closeDate)>= (new Date(startOfWeek)) && new Date(op.closeDate) <= (new Date(endOfWeek))){
                        weekCommit = weekCommit+op.amount
                    }
                })

                el.opportunities = oppsInCommitStage;
                el.week.relatasCommitAmount = weekCommit;
            })
        } else {
            _.each(commits,function (el) {

                var oppsInCommitStage = [];
                if(el.opportunities){
                    _.each(el.opportunities,function (oppGrp) {
                        oppGrp = JSON.parse(JSON.stringify(oppGrp))
                        _.each(oppGrp.opps,function (op) {
                            if(op.relatasStage == commitStage || op.stageName == commitStage){
                                oppsInCommitStage.push(op)
                            }
                        })
                    });
                }

                var startOfWeek = moment(buildDateObj(el.commitWeekYear,timezone)).startOf("isoWeek"),
                    endOfWeek = moment(startOfWeek).endOf("isoWeek");

                var weekCommit = 0;
                _.each(oppsInCommitStage,function (op) {
                    if(new Date(op.closeDate)>= (new Date(startOfWeek)) && new Date(op.closeDate) <= (new Date(endOfWeek))){
                        weekCommit = weekCommit+op.amount
                    }
                })

                el.opportunities = oppsInCommitStage;
                el.week.relatasCommitAmount = weekCommit;

                el.opportunities = oppsInCommitStage;
            })
        }

        sendSuccess(err,res,
            {
                oppsConversion:conversionData,
                commits:commits,
                commitStage:commitStage,
                oppsInStageClosingThisWeek:!errOpps && opps && opps.length>0 && netGrossMarginReq?calculateNGM(JSON.parse(JSON.stringify(opps)),"Ping"):[],
                allWeeks:allWeekYears,
                dateMin:dateMin,
                dateMax:dateMax,
                fyRange:fyRange,
                oppsInCommitStage:oppsInCommitStage,
                timezone:timezone
            })
    });
}

function oppConversionFillers(oppsConversion,allWeeks,weekYearsObj,timezone){

    var nonExistingWeeksWon = allWeeks;

    if(oppsConversion){

        if(oppsConversion.oppsWon){
            nonExistingWeeksWon = _.difference(allWeeks,_.pluck(oppsConversion.oppsWon,"weekYear"))
            _.each(nonExistingWeeksWon,function (el) {
                oppsConversion.oppsWon.push({
                    weekYear:el,
                    count:0,
                    date:weekYearsObj[el]?weekYearsObj[el].date:buildDateObj(el,timezone)
                })
            })
        } else {
            oppsConversion.oppsWon = [];

            _.each(nonExistingWeeksWon,function (el) {
                oppsConversion.oppsWon.push({
                    weekYear:el,
                    count:0,
                    date:weekYearsObj[el]?weekYearsObj[el].date:buildDateObj(el,timezone)
                })
            })
        }

        if(oppsConversion.created){

            nonExistingWeeksWon = _.difference(allWeeks,_.pluck(oppsConversion.created,"weekYear"))
            _.each(nonExistingWeeksWon,function (el) {
                oppsConversion.created.push({
                    weekYear:el,
                    count:0,
                    date:weekYearsObj[el]?weekYearsObj[el].date:buildDateObj(el,timezone)
                })
            })
        } else {
            oppsConversion.created = [];

            _.each(nonExistingWeeksWon,function (el) {
                oppsConversion.created.push({
                    weekYear:el,
                    count:0,
                    date:weekYearsObj[el]?weekYearsObj[el].date:buildDateObj(el,timezone)
                })
            })
        }
    }

    var finalLengthCheck = [];

    if(oppsConversion && oppsConversion.created && oppsConversion.oppsWon){
        if(oppsConversion.created.length != oppsConversion.oppsWon.length){

            if(oppsConversion.created.length>oppsConversion.oppsWon.length){
                finalLengthCheck = _.difference(_.pluck(oppsConversion.created,"weekYear"),_.pluck(oppsConversion.oppsWon,"weekYear"))

                _.each(finalLengthCheck,function (el) {
                    oppsConversion.oppsWon.push({
                        weekYear:el,
                        count:0,
                        date:weekYearsObj[el]?weekYearsObj[el].date:buildDateObj(el,timezone)
                    })

                    allWeeks.push(el)
                })

            } else {
                finalLengthCheck = _.difference(_.pluck(oppsConversion.oppsWon,"weekYear"),_.pluck(oppsConversion.created,"weekYear"))

                _.each(finalLengthCheck,function (el) {
                    oppsConversion.created.push({
                        weekYear:el,
                        count:0,
                        date:weekYearsObj[el]?weekYearsObj[el].date:buildDateObj(el,timezone)
                    })
                    allWeeks.push(el)
                })
            }
        }
    }

    return oppsConversion;
}

function buildDateObj(weekYear,timezone) {
    var year = weekYear.substr(-4);
    var week = weekYear.split(year)[0]
    return new Date(moment().tz(timezone).week(week).year(year));
}

router.get('/review/opps/at/risk',common.isLoggedInUserOrMobile,function (req,res) {

    var userId = req.query.userId ? req.query.userId : common.getUserIdFromMobileOrWeb(req);
    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {
        dealsAtRiskObj.findDealsAtRiskTodayForUser(common.castToObjectId(userId),timezone,function (err,dealsAtRisk) {
            sendSuccess(err,res,dealsAtRisk)
        })
    })
});

router.get('/review/all/my/access/opps',common.isLoggedInUserOrMobile,function (req,res) {

    var userId = req.query.userId ? req.query.userId : common.getUserIdFromMobileOrWeb(req);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){

        oppWeightsObj.getOppStages(common.castToObjectId(user.companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if (oppStages) {
                _.each(oppStages, function (st) {
                    if (st.commitStage) {
                        commitStage = st.name;
                    }
                });
            }

            common.getUserAccessControl([common.castToObjectId(userId)],function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq,companyTargetAccess,companyDetails,businessUnits) {

                var userEmailIds = req.query.userEmailId.split(",");

                if(req.query.portfolios){
                    var portfolios_raw = req.query.portfolios.split(",");

                    if(portfolios_raw && portfolios_raw.length>0){
                        var pAccess = [],
                            rAccess = [],
                            vAccess = [],
                            bUnits = [];

                        _.each(portfolios_raw,function (po) {
                            var splits  = po.split("_type_");
                            if(splits[1] == "productType"){
                                pAccess.push(splits[0])
                            }
                            if(splits[1] == "vertical"){
                                vAccess.push(splits[0])
                            }
                            if(splits[1] == "businessUnit"){
                                bUnits.push(splits[0])
                            }
                            if(splits[1] == "region"){
                                rAccess.push(splits[0])
                            }
                        })
                    }

                    var pmQuery = portfolioAccess(rAccess,pAccess,vAccess,user.companyId,bUnits);
                }

                var userIds = req.query.userIds?req.query.userIds.split(","):[userId];
                userIds = common.castListToObjectIds(userIds);

                var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,user.companyId,businessUnits);

                userEmailIds = _.compact(userEmailIds);

                var query = {
                    userId:{$in:userIds},
                    companyId:common.castToObjectId(user.companyId),
                    stageName:commitStage
                };

                if(userEmailIds && userEmailIds.length>0){
                    query = {
                        $or:[{userId:{$in:userIds}},
                            {"usersWithAccess.emailId":{$in:userEmailIds}},
                            {$and:accessControlQuery}
                            ],
                        companyId:common.castToObjectId(user.companyId),
                        stageName:commitStage
                    };
                }

                var aggregateQ = [{
                    $match:query
                }];

                if(req.query.portfolios){
                    aggregateQ.push({
                        $match:{
                            $and:pmQuery
                        }
                    })
                }

                oppManagementObj.getOppByCustomAggregateQuery(aggregateQ,function (errO,opps) {

                    if(opps && opps.length>0){

                        var userIds = [],
                            oppsIds = [];
                        _.each(opps,function (op) {
                            if(!op.createdDate){
                                op.createdDate = op._id.getTimestamp()
                            };

                            oppsIds.push(op.opportunityId);
                            userIds.push(common.castToObjectId(op.userId));
                        });

                        getDealsAtRiskForOpps(userIds,oppsIds,function (errD,dealsAtRisk) {
                            sendSuccess(errO,res,{opps:opps,dealsAtRisk:dealsAtRisk})
                        })
                    } else {
                        sendSuccess(errO,res,[])
                    }
                })

            });
        });
    })

});

function getWeekYearBetweenTwoDates(dateMin,dateMax,timezone){

    var diff = dateMax.diff(dateMin, 'week')
    var weekYears = [];

    function generate(diff){

        var date = new Date(moment(moment(dateMin).tz(timezone).add(diff,"week")).add(6,"h"))

        if(diff>0){
            weekYears.push({
                "quarter": {
                    "userCommitAmount": 0,
                    "relatasCommitAmount": 0
                },
                "month": {
                    "userCommitAmount": 0,
                    "relatasCommitAmount": 0
                },
                "week": {
                    "userCommitAmount": 0,
                    "relatasCommitAmount": 0
                },
                "opportunities": [],
                noCommits:true,
                date: date,
                commitWeekYear:moment(date).week()+""+moment(date).year()
            });
            diff--;
            generate(diff)
        } else {
            return false;
        }
    }

    generate(diff);

    return weekYears;

}

function getOpportunitiesInStage(userId,dateMin,dateMax,stage,callback){

    var query = {
        userId:userId
    }

    if(stage){
        query["$or"] = [{relatasStage:stage},{stageName:stage}]
    }

    oppManagementObj.getOppByCustomQuery(query,null,function (err,opps) {

        if(!err && opps && opps.length>0){

            _.each(opps,function (op) {
                if(!op.createdDate){
                    op.createdDate = op._id.getTimestamp()
                }
            })

            callback(err,opps)
        } else {
            callback(err,[])
        }

    });
}

function getAllOpportunitiesInCommitStage(userId,stage,callback){

    var query = {
        userId:userId
    }

    if(stage){
        query["$or"] = [{relatasStage:stage},{stageName:stage}]
    }

    oppManagementObj.getOppByCustomQuery(query,null,function (err,opps) {

        if(!err && opps && opps.length>0){

            _.each(opps,function (op) {
                if(!op.createdDate){
                    op.createdDate = op._id.getTimestamp()
                }
            })

            callback(err,opps)
        } else {
            callback(err,[])
        }

    });
}

function getOpportunitiesInStageMultiUsers(userIds,dateMin,dateMax,stage,allACOpps,companyId,callback,userEmailIds,accessControlQuery){

    var query = {
        userId:{$in:userIds}
    }

    if(userEmailIds && userEmailIds.length>0){
        query = {
            $or:[{userId:{$in:userIds}},
                {"usersWithAccess.emailId":{$in:userEmailIds}}]
        }
    }

    if(stage){
        query.stageName = stage
    }

    if(accessControlQuery){
        query.$and = accessControlQuery
    }

    async.parallel([
        function (callback) {
            oppManagementObj.getOppByCustomQuery(query,null,callback);
        },
        function (callback) {

            if(allACOpps && allACOpps.length>0) {
                var queryAllAccess = {
                    companyId:common.castToObjectId(companyId),
                    opportunityId:{$in:allACOpps}
                }

                if(accessControlQuery){
                    queryAllAccess.$and = accessControlQuery
                }

                oppManagementObj.getOppByCustomQuery(queryAllAccess,null,callback);
            } else {
                callback(null,[])
            }
        }
    ], function (err,data) {
        var opps = [];
        if(!err && data && data.length>0){
            opps = data[0].concat(data[1]);
        }

        var onlyCommitStageOpps = [];
        var oppsIds = [];

        if(opps && opps.length>0){
            _.each(opps,function (op) {
                if(op.relatasStage === stage || op.stageName === stage) {
                    oppsIds.push(op.opportunityId);
                    if(!op.createdDate){
                        op.createdDate = op._id.getTimestamp()
                    }
                    onlyCommitStageOpps.push(op);
                }
            });

            callback(err,_.uniq(onlyCommitStageOpps,'opportunityId'));
        } else {
            callback(err,[]);
        }
    });
}

function getAllOpportunitiesInCommitStageMultiUsers(userIds,stage,callback){

    var query = {
        userId:{$in:userIds}
    }

    if(stage){
        query["$or"] = [{relatasStage:stage},{stageName:stage}]
    }

    oppManagementObj.getOppByCustomQuery(query,null,function (err,opps) {

        if(!err && opps && opps.length>0){

            _.each(opps,function (op) {
                if(!op.createdDate){
                    op.createdDate = op._id.getTimestamp()
                }
            })
            callback(err,opps)
        } else {
            callback(err,[])
        }

    });
}

function sendSuccess(err,res,data) {

    if(err){
        res.send({
            SuccessCode: 0,
            ErrorCode: 1,
            Data:[]
        })
    } else {
        res.send({
            SuccessCode: 1,
            ErrorCode: 0,
            Data:data
        })
    }
}

function calculateNGM(opps) {
    return common.calculateNGM(opps)
}

function setCurrenciesObj(primaryCurrency,currenciesObj,companyDetails){
    companyDetails.currency.forEach(function (el) {
        currenciesObj[el.symbol] = el;
        if(el.isPrimary){
            primaryCurrency = el.symbol;
        }
    });

    return primaryCurrency;
}

function getTargetsByMonth(hierarchyList,dateRange,allDates,callback) {

    var dateMin = new Date(Math.min.apply(null,allDates));
    var dateMax = new Date(Math.max.apply(null,allDates));
    // dateMax = new Date(moment(dateMax).add(1,'month'));

    oppManagementObj.getTargetForUserByMonths(hierarchyList,dateMin,moment(dateMax).endOf("month"),function (err, targets) {

        var targetObj = {};

        _.each(targets, function (tr) {
            // tr.date = new Date(moment(tr.date).add(1,"day"));
            var monthYear = String(moment(tr.date).month()+""+moment(tr.date).year())
            tr.monthYear = monthYear;
            targetObj[monthYear] = tr;
        });

        var nonExistingTargets = [];
        _.each(dateRange,function (dt) {
            if(!targetObj[dt.monthYear]){
                nonExistingTargets.push({
                    userId:hierarchyList[0],
                    monthYear: dt.monthYear,
                    date: dt.date,
                    target:0
                })
            }
        })

        var flTargets = _.uniq(targets.concat(nonExistingTargets),'monthYear');

        flTargets.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        callback(err,flTargets.slice(Math.max(flTargets.length - 12, 0)))
        // callback(err,flTargets)
    });
}

function portfolioAccess(regionAccess,productAccess,verticalAccess,companyId,buAccess) {

    var accessControlQuery = [];

    if(regionAccess && regionAccess.length>0 && companyId){
        accessControlQuery.push({"geoLocation.zone":{$in:regionAccess}})
    }

    if(productAccess && productAccess.length>0 && companyId){
        accessControlQuery.push({"productType":{$in:productAccess}})
    }

    if(verticalAccess && verticalAccess.length>0 && companyId){
        accessControlQuery.push({vertical:{$in:verticalAccess}})
    }

    if(buAccess && buAccess.length>0 && companyId){
        accessControlQuery.push({businessUnit:{$in:buAccess}})
    }

    return accessControlQuery;

}

function accessControlSettings(regionAccess,productAccess,verticalAccess,companyId,businessUnits) {

    var accessControlQuery = [];

    if(regionAccess && regionAccess.length>0 && companyId){
        accessControlQuery.push({"geoLocation.zone":{$in:regionAccess}})
    } else {
        accessControlQuery.push({"geoLocation.zone":"no_access"})
    }

    if(productAccess && productAccess.length>0 && companyId){
        accessControlQuery.push({"productType":{$in:productAccess}})
    } else {
        accessControlQuery.push({"productType":"no_access"})
    }

    if(verticalAccess && verticalAccess.length>0 && companyId){
        accessControlQuery.push({vertical:{$in:verticalAccess}})
    } else {
        accessControlQuery.push({vertical:"no_access"})
    }

    if(businessUnits && businessUnits.length>0 && companyId){
        accessControlQuery.push({businessUnit:{$in:businessUnits}})
    } else {
        accessControlQuery.push({businessUnit:"no_access"})
    }

    accessControlQuery.push({companyId:companyId})

    return accessControlQuery;

}

module.exports = router;
