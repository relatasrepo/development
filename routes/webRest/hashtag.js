/**
 * Created by naveen on 12/3/15.
 */

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactions = require('../../dataAccess/interactionManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var contactClassSupport = require('../../common/contactsSupport');
var appCredentials = require('../../config/relatasConfiguration');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var ipLocatorJS = require('../../common/ipLocator');
var Gmail = require('../../common/gmail');
var fullContact = require('../../common/fullContact');
var opportunityManagement = require('../../dataAccess/opportunitiesManagement');
var _ = require("lodash");

var oppManagementObj = new opportunityManagement();
var gmailObj = new Gmail();
var ipLocatorObj = new ipLocatorJS();
var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var contactSupportObj = new contactClassSupport();
var appCredential = new appCredentials();
var meetingClassObj = new meetingManagement();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionObj = new interactions();

var statusCodes = errorMessagesObj.getStatusCodes();
var domain = appCredential.getDomain();

/*Post Hashtags*/
router.post('/api/hashtag/new',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        if(common.checkRequired(req.body.contactId)){

            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
                if(common.checkRequired(user)){
                    var status = common.checkRequired(req.body.hashtag);
                    var hashtag = req.body.hashtag;
                    contactObj.addHashtag(user._id,common.castToObjectId(req.body.contactId),hashtag,function(err,isSuccess){
                        if(err){
                            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
                        }
                        else if(isSuccess){
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{}
                            });
                        }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_CONTACT_ID_FOUND'}));
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

/*Delete Hashtags*/
router.get('/api/hashtag/delete',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
        if(common.checkRequired(req.query.contactId)){
            var hashtag = req.query.hashtag;
            userManagementObj.deleteHashtagMgmt(userId,common.castToObjectId(req.query.contactId),hashtag, function(err, isSuccess){

                if(isSuccess===false){
                    res.send({
                        "SuccessCode": 0
                    });
                }
                else {
                    res.send({
                        "SuccessCode": 1
                    });
                }
            })
        }
});

/*Delete Hashtags for opportunities*/
router.get('/api/opportunities/hashtag/delete',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
        if(common.checkRequired(req.query.contactId)){
            var hashtag = req.query.hashtag;
            var type = req.query.type;

            var contactEmailId = req.query.contactEmailId ? req.query.contactEmailId : null;
            var contactMobile = req.query.contactMobile ? req.query.contactMobile : null;

            oppManagementObj.checkOpportunitiesForPeople(common.castToObjectId(userId),contactEmailId,contactMobile,type,req.query.contactId,function(error,opportunities){
                
                if(opportunities.length == 0){
                    // res.send({
                    //     "SuccessCode": 1
                    // });
                    userManagementObj.deleteHashtagMgmt(userId,common.castToObjectId(req.query.contactId),hashtag, function(err, isSuccess){

                        if(isSuccess===false){
                            res.send({
                                "SuccessCode": 0
                            });
                        }
                        else {
                            res.send({
                                "SuccessCode": 1
                            });
                        }
                    })
                } else {
                    res.send({
                        "SuccessCode": 1
                    });
                }
            });
        }
});

module.exports = router;