
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactions = require('../../dataAccess/interactionManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var contactClassSupport = require('../../common/contactsSupport');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var profileManagementClass = require('../../dataAccess/profileManagementClass');

var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var contactSupportObj = new contactClassSupport();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionObj = new interactions();

var statusCodes = errorMessagesObj.getStatusCodes();

router.get('/search/contact/header',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 10;
    if(common.checkRequired(req.query.searchContent) && common.checkRequired(req.query.filterBy)){
        var filterBy = req.query.filterBy;
        if(common.contains(filterBy,',')){
            filterBy = filterBy.split(',');
        }
        else filterBy = [filterBy];

        contactSupportObj.searchUserContacts_for_search_page(req.query.searchContent,userId,skip,limit,filterBy,function(error,results){
            if(error){
                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_IN_SEARCH'}));
            }
            else{
                res.send(results);
            }
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'SEARCH_TEXT_NOT_FOUND_IN_REQUEST'}));
});

//Search only for Hashtag
router.get('/search/hashtag/header',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 10;
    if(common.checkRequired(req.query.searchContent) && common.checkRequired(req.query.filterBy)){
        var filterBy = req.query.filterBy;
        if(common.contains(filterBy,',')){
            filterBy = filterBy.split(',');
        }
        else filterBy = [filterBy];

        contactSupportObj.searchUserHashtag_for_search_page(req.query.searchContent,userId,skip,limit,filterBy,function(error,results){
            if(error){
                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_IN_SEARCH'}));
            }
            else{
                res.send(results);
            }
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'SEARCH_TEXT_NOT_FOUND_IN_REQUEST'}));
});

router.post('/search/connect/best/path',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(req.body.contactEmailId && req.body.connected && req.body.connected.length > 0){
        contactObj.getSingleContact(userId,req.body.contactEmailId,function(error,contact) {
            if (error) {
                logger.info('Error 1 in /people/connect/best/path/to/:emailId user: ' + req.user, error);
                res.send(errorObj.generateErrorResponse({
                    status: statusCodes.PROFILE_ERROR_CODE,
                    key: 'ERROR_IN_FIND_PATH'
                }));
            }
            else if (common.checkRequired(contact)) {
                res.send(errorObj.generateErrorResponse({
                    status: statusCodes.PROFILE_ERROR_CODE,
                    key: 'USER_ALREADY_CONNECTED'
                }));
            }
            else {
                userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,companyName:1},function(error,user){
                    if(common.checkRequired(user)){
                        var timezone;
                        if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                            timezone = user.timezone.name;
                        }else timezone = 'UTC';

                        var userIdsObject = [];
                        for(var i=0; i<req.body.connected.length; i++){
                            if(userId != req.body.connected[i])
                                userIdsObject.push(common.castToObjectId(req.body.connected[i]))
                        }

                        interactionObj.bestPathToConnection(common.castToObjectId(userId),userIdsObject,[req.body.contactEmailId],function(result){

                            if(result && result.length > 0){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "emails":[],
                                    "timezone":timezone,
                                    "Data":result})
                            }
                            else res.send(errorObj.generateErrorResponse({
                                status: statusCodes.MESSAGE,
                                key: 'NO_COMMON_CONNECTIONS_FOUND'
                            }));

                        })
                    }
                    else res.send(errorObj.generateErrorResponse({
                        status: statusCodes.PROFILE_ERROR_CODE,
                        key: 'ERROR_FETCHING_PROFILE'
                    }));
                })
            }
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'EMAIL_ID_NOT_FOUND_IN_REQUEST'}));
});

router.get('/search/connect/best/path/to/common',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.query.contactEmailId)){
        var notInEmailIds = [];
        if(common.checkRequired(req.query.notIn)){
            if(common.contains(req.query.notIn,',')){
                notInEmailIds = req.query.notIn.split(',');
            }
            else notInEmailIds = [req.query.notIn];
        }
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                contactObj.getCommonContactsWithEmail(common.castToObjectId(userId),user.emailId,req.query.contactEmailId,notInEmailIds,function(contacts,emailIdArr){
                    if(emailIdArr.length > 0){
                        var timezone;
                        if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                            timezone = user.timezone.name;
                        }else timezone = 'UTC';

                        var dateMin = moment().tz(timezone);
                        dateMin.date(dateMin.date() - 90);
                        dateMin = dateMin.format();
                        var dateMax = moment().tz(timezone).format();
                        interactionObj.getInteractedWithCountList_top_limit(common.castToObjectId(userId),emailIdArr,dateMin,dateMax,2,function(interactions){

                            var bestPaths = []
                            if(interactions.length > 0){
                                for(var i=0; i<interactions.length; i++){
                                    for(var j=0; j<contacts.length; j++){
                                        if(interactions[i]._id == contacts[j].personEmailId){
                                            contacts[j].count = interactions[i].count;
                                            contacts[j].lastInteracted = interactions[i].lastInteracted;
                                            bestPaths.push(contacts[j]);
                                        }
                                    }
                                }
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "timezone":timezone,
                                    "Data":bestPaths})
                            }
                            else {
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "timezone":timezone,
                                    "Data":contacts.slice(0,2)});
                                // send 2 random common contacts
                            }
                        })
                    }
                    else res.send(errorObj.generateErrorResponse({
                        status: statusCodes.MESSAGE,
                        key: 'NO_COMMON_CONNECTIONS_FOUND'
                    }));
                })
            }
            else res.send(errorObj.generateErrorResponse({
                status: statusCodes.PROFILE_ERROR_CODE,
                key: 'ERROR_FETCHING_PROFILE'
            }));
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'EMAIL_ID_NOT_FOUND_IN_REQUEST'}));
});

router.get('/search/connect/best/path/to/company',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.query.contactEmailId)){
        contactObj.getSingleContact(userId,req.query.contactEmailId,function(error,contact){
            if(error){
                logger.info('Error 1 in /people/connect/best/path/to/:emailId user: '+req.user,error);
                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_IN_FIND_PATH'}));
            }
            else if(common.checkRequired(contact)){
                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'USER_ALREADY_CONNECTED'}));
            }
            else{
                userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,companyName:1},function(error,user){
                    if(common.checkRequired(user)){
                        contactObj.getCommonContactsWithEmailWithinCompany(common.castToObjectId(userId),user.emailId,req.query.contactEmailId,user.companyName,function(contacts,emailIdArr){
                            if(emailIdArr.length > 0){
                                var timezone;
                                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                                    timezone = user.timezone.name;
                                }else timezone = 'UTC';

                                var dateMin = moment().tz(timezone);
                                dateMin.date(dateMin.date() - 90);
                                dateMin = dateMin.format();
                                var dateMax = moment().tz(timezone).format();

                                interactionObj.getInteractedWithCountList_top_limit(common.castToObjectId(userId),emailIdArr,dateMin,dateMax,2,function(interactions){

                                    var bestPaths = [];
                                    var emails = [];
                                    if(interactions.length > 0){
                                        for(var i=0; i<interactions.length; i++){
                                            for(var j=0; j<contacts.length; j++){
                                                if(interactions[i]._id == contacts[j].personEmailId){
                                                    contacts[j].count = interactions[i].count;
                                                    contacts[j].lastInteracted = interactions[i].lastInteracted;
                                                    bestPaths.push(contacts[j]);
                                                    emails.push(interactions[i]._id)
                                                }
                                            }
                                        }
                                        res.send({
                                            "SuccessCode": 1,
                                            "Message": "",
                                            "ErrorCode": 0,
                                            "emails":emails,
                                            "timezone":timezone,
                                            "Data":bestPaths})
                                    }
                                    else {
                                        for(var k=0; k<contacts.length; k++){
                                            if(k < 2){
                                                bestPaths.push(contacts[k])
                                                emails.push(contacts[i].personEmailId)
                                            }
                                            else break;
                                        }
                                        res.send({
                                            "SuccessCode": 1,
                                            "Message": "",
                                            "ErrorCode": 0,
                                            "emails":emails,
                                            "timezone":timezone,
                                            "Data":bestPaths})
                                        // send 2 random common contacts
                                    }
                                })
                            }
                            else res.send(errorObj.generateErrorResponse({
                                status: statusCodes.MESSAGE,
                                key: 'NO_COMMON_CONNECTIONS_FOUND'
                            }));
                        })
                    }
                    else res.send(errorObj.generateErrorResponse({
                        status: statusCodes.PROFILE_ERROR_CODE,
                        key: 'ERROR_FETCHING_PROFILE'
                    }));
                });
            }
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'EMAIL_ID_NOT_FOUND_IN_REQUEST'}));
});

router.get('/search/contacts/mobile/api',common.isLoggedInUserOrMobile,function(req,res) {

    var userId = common.getUserIdFromMobileOrWeb(req);

    contactSupportObj.getContactsWithLastInteractedDate(common.castToObjectId(userId),req.query.searchContent,function(error,contacts){

        var resObj = {contacts:contacts}

        if(!error && contacts && contacts.length>0) {

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":resObj
            })
        } else {
            resObj = {contacts:[]}

            res.send({
                "SuccessCode": 1,
                "Message": "Contact not found",
                "ErrorCode": 0,
                "Data":resObj
            })
        }
    });
});

module.exports = router;
