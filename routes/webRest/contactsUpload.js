
var express = require('express');
var router = express.Router();
var csv = require('ya-csv');
var fs = require('fs');
var xlsx = require('xlsx');
var xlsjs = require('xlsjs');

var userManagement = require('../../dataAccess/userManagementDataAccess');
var profileManagementClass = require('../../dataAccess/profileManagementClass');
var winstonLog = require('../../common/winstonLog');
var commonUtility = require('../../common/commonUtility');
var contactClass = require('../../dataAccess/contactsManagementClass');
var errorMessages = require('../../errors/errorMessage');
var aggregateSupport = require('../../common/aggregateSupport');

var aggregateSupportObj = new aggregateSupport();
var errorMessagesObj = new errorMessages();
var contactObj = new contactClass();
var userManagements = new userManagement();
var logLib = new winstonLog();
var common = new commonUtility();
var profileManagementClassObj = new profileManagementClass();

var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();


router.post('/contacts/upload/file',common.isLoggedInUserToGoNext,function(req,res){
    req.session.uploadContactsStatusMsg = '';
    var fstream;
    var userId = common.getUserId(req.user);

    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        if(filename.split('.')[filename.split('.').length-1] == 'csv' || filename.split('.')[filename.split('.').length-1] == 'CSV'){
            parseAndStoreCsvContacts(fieldname, file, filename,userId,fstream,req,res)
        }
        else if(filename.split('.')[filename.split('.').length-1] == 'xlsx' || filename.split('.')[filename.split('.').length-1] == 'XLSX'){
            parseAndStoreXlsxContacts(fieldname, file, filename,userId,fstream,req,res)
        }
        else if(filename.split('.')[filename.split('.').length-1] == 'xls' || filename.split('.')[filename.split('.').length-1] == 'XLS'){
            parseAndStoreXlsContacts(fieldname, file, filename,userId,fstream,req,res)
        }
        else{
            res.send({
                "SuccessCode": 0,
                "Message": errorMessagesObj.getMessage("CSV_XLS_FILES_NOT_VALID"),
                "ErrorCode": 1,
                "Data":{}
            });
        }
    })
});

function goBack(req,res){
    req.session.uploadContactsStatusMsg = {msg:'<span>Uploaded file format is not supported. Please click <a target="_blank" href="https://medium.com/relatas/mass-email-linkedin-contacts-in-5-steps/">here</a> for correct format.</span>', status:'error'};
    res.redirect('back');
}

function parseAndStoreCsvContacts(fieldname, file, filename,userId,fstream,req,res){
    var contactsArr = [];
    var emailIdArr = [];
    var dataArray = [];

    fstream = fs.createWriteStream('./public/' + filename);
    file.pipe(fstream);

    fstream.on('close', function () {

        var reader = csv.createCsvFileReader('./public/' + filename, { columnsFromHeader: true });
        reader.addListener('data', function(data) {

            dataArray.push(data)
            var firstName = data["First Name"] || null;
            var lastName = data["Last Name"] || null;
            var name = firstName+' '+lastName


            var emailId = data["E-mail Address"]
            var companyName = data["Company"]
            var designation = data["Job Title"]
            var mobilePhone = data["Mobile Phone"]
            var primaryPhone = data["Primary Phone"]
            var mobile = common.checkRequired(mobilePhone) ? mobilePhone.toString() : common.checkRequired(primaryPhone) ? primaryPhone.toString() :null;
            if(common.checkRequired(emailId) && common.checkRequired(firstName)){
                emailIdArr.push(emailId.trim().toLowerCase());
                var contact = {
                    personId:null,
                    personName:name,
                    personEmailId:emailId.trim().toLowerCase(),
                    companyName:companyName || null,
                    designation:designation || null,
                    mobileNumber:mobile,
                    count:0,
                    addedDate:new Date(),
                    verified:false,
                    relatasUser:false,
                    relatasContact:false,
                    source:'linkedin'
                };

                contactsArr.push(contact)
            }
        });
        reader.addListener('end', function() {

            fs.unlink('./public/' + filename);
            if(contactsArr.length > 0){
                addContactsFinal(userId,contactsArr,emailIdArr,req,res)
            }
            else{
                fs.unlink('./public/' + filename)
                parseCSVStringToJson(dataArray,req,res,userId)
            }
        })

    });
}

function parseCSVStringToJson(CSVStringArray,req,res,userId){
    var data = [];

    if(common.checkRequired(CSVStringArray[0])) {
        var eachKey;
        for (var eKey in CSVStringArray[0]) {
            eachKey = eKey.split('\t')
        }
        for (var i = 0; i < CSVStringArray.length; i++) {
            for (var key in CSVStringArray[i]) {

                var a = CSVStringArray[i][key];
                var valuesArray = a.split('\t');
                var obj = {

                }
                for (var j = 0; j < eachKey.length; j++) {
                    if (common.checkRequired(eachKey[j])) {

                        obj[eachKey[j]] = valuesArray[j];

                    }
                }
                data.push(obj)

            }
        }
        var contactsArr = [];
        var emailIdArr = [];

        if(common.checkRequired(data[0])){
            for (var l = 0; l < data.length; l++) {
                var firstName = data[l]["First Name"] || null;
                var lastName = data[l]["Last Name"] || null;
                var name = firstName + ' ' + lastName

                var emailId = data[l]["E-mail Address"]
                var companyName = data[l]["Company"]
                var designation = data[l]["Job Title"]
                var mobilePhone = data[l]["Mobile Phone"]
                var primaryPhone = data[l]["Primary Phone"]
                var mobile = common.checkRequired(mobilePhone) ? mobilePhone.toString() : common.checkRequired(primaryPhone) ? primaryPhone.toString() : null;
                if (common.checkRequired(emailId) && common.checkRequired(firstName)) {
                    emailIdArr.push(emailId.trim().toLowerCase())
                    var contact = {
                        personId: null,
                        personName: name,
                        personEmailId: emailId.trim().toLowerCase(),
                        companyName: companyName || null,
                        designation: designation || null,
                        mobileNumber: mobile,
                        count: 0,
                        addedDate: new Date(),
                        verified: false,
                        relatasUser: false,
                        relatasContact: false,
                        source:'linkedin'
                    }
                    contactsArr.push(contact)
                }
                if(l == data.length-1){
                    if(contactsArr.length > 0){
                        addContactsFinal(userId,contactsArr,emailIdArr,req,res)
                    }
                    else res.send({
                        "SuccessCode": 0,
                        "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
                        "ErrorCode": 1,
                        "Data":{}
                    });
                }
            }
        }
        else{
            res.send({
                "SuccessCode": 0,
                "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
                "ErrorCode": 1,
                "Data":{}
            });
        }
    }
    else{
        res.send({
            "SuccessCode": 0,
            "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
            "ErrorCode": 1,
            "Data":{}
        });
    }
}

function parseAndStoreXlsxContacts(fieldname, file, filename,userId,fstream,req,res){
    var contactsArr = [];

    var currentTime = new Date();
    var fileNameUnique =''+currentTime.getFullYear()+''+currentTime.getMonth()+''+currentTime.getDate()+''+currentTime.getHours()+''+currentTime.getMinutes()+''+ filename;

    fstream = fs.createWriteStream('./public/' + filename);
    file.pipe(fstream);


    fstream.on('close', function () {
        //  var reader = csv.createCsvFileReader('./public/' + filename, { columnsFromHeader: true });
        var out = xlsx.readFile('./public/' + filename);

        var result = {};
        out.SheetNames.forEach(function(sheetName) {
            var roa = xlsx.utils.sheet_to_row_object_array(out.Sheets[sheetName]);
            if(roa.length > 0){
                result[sheetName] = roa;
            }
        });

        var contactsArr = [];
        var emailIdArr = [];
        if(common.checkRequired(result)){
            if(common.checkRequired(result.Sheet1)){
                if(common.checkRequired(result.Sheet1[0])){
                    var data = result.Sheet1;
                    for (var i=0; i<data.length; i++){
                        var firstName = data[i]["First Name"] || null;
                        var lastName = data[i]["Last Name"] || null;
                        var name = firstName+' '+lastName


                        var emailId = data[i]["Email Id"]
                        var skypeId = data[i]["Skype Id"]
                        var mobilePhone = data[i]["Mobile Number"]

                        if(common.checkRequired(emailId) && common.checkRequired(firstName)){
                            emailIdArr.push(emailId.trim().toLowerCase())
                            var contact = {
                                personId:null,
                                personName:name,
                                personEmailId:emailId.trim().toLowerCase(),
                                mobileNumber:mobilePhone || null,
                                skypeId:skypeId || null,
                                count:0,
                                addedDate:new Date(),
                                verified:false,
                                relatasUser:false,
                                relatasContact:true,
                                source:'linkedin'
                            };
                            contactsArr.push(contact)
                        }
                    }
                    fs.unlink('./public/' + filename)
                    if(contactsArr.length > 0){
                        addContactsFinal(userId,contactsArr,emailIdArr,req,res)
                    }
                    else{
                        res.send({
                            "SuccessCode": 0,
                            "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
                            "ErrorCode": 1,
                            "Data":{}
                        });
                    }
                }else{
                    fs.unlink('./public/' + filename)
                    res.send({
                        "SuccessCode": 0,
                        "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
                        "ErrorCode": 1,
                        "Data":{}
                    });
                }
            }else{
                fs.unlink('./public/' + filename)
                res.send({
                    "SuccessCode": 0,
                    "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
                    "ErrorCode": 1,
                    "Data":{}
                });
            }
        }else{
            fs.unlink('./public/' + filename)
            res.send({
                "SuccessCode": 0,
                "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
                "ErrorCode": 1,
                "Data":{}
            });
        }

    });
}

function parseAndStoreXlsContacts(fieldname, file, filename,userId,fstream,req,res){

    var currentTime = new Date();

    fstream = fs.createWriteStream('./public/' + filename);
    file.pipe(fstream);


    fstream.on('close', function () {
        //  var reader = csv.createCsvFileReader('./public/' + filename, { columnsFromHeader: true });
        var out = xlsjs.readFile('./public/' + filename);

        var result = {};
        out.SheetNames.forEach(function(sheetName) {
            var roa = xlsjs.utils.sheet_to_row_object_array(out.Sheets[sheetName]);
            if(roa.length > 0){
                result[sheetName] = roa;
            }
        });

        var contactsArr = [];
        var emailIdArr = [];
        if(common.checkRequired(result)){
            if(common.checkRequired(result.Sheet1)){
                if(common.checkRequired(result.Sheet1[0])){
                    var data = result.Sheet1;
                    for (var i=0; i<data.length; i++){

                        var firstName = data[i]["First Name"] || null;
                        var lastName = data[i]["Last Name"] || null;
                        var name = firstName+' '+lastName
                        var emailId = data[i]["Email Id"]
                        var skypeId = data[i]["Skype Id"]
                        var mobilePhone = data[i]["Mobile Number"]

                        if(common.checkRequired(emailId) && common.checkRequired(firstName)){
                            emailIdArr.push(emailId.trim().toLowerCase());
                            var contact = {
                                personId:null,
                                personName:name,
                                personEmailId:emailId.trim().toLowerCase(),
                                mobileNumber:mobilePhone || null,
                                skypeId:skypeId || null,
                                count:0,
                                addedDate:new Date(),
                                verified:false,
                                relatasUser:false,
                                relatasContact:true,
                                source:'linkedin'
                            };
                            contactsArr.push(contact)
                        }
                    }
                    fs.unlink('./public/' + filename)
                    if(contactsArr.length > 0){
                        addContactsFinal(userId,contactsArr,emailIdArr,req,res)
                    }
                    else{
                        fs.unlink('./public/' + filename)
                        res.send({
                            "SuccessCode": 0,
                            "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
                            "ErrorCode": 1,
                            "Data":{}
                        });
                    }
                }else{
                    fs.unlink('./public/' + filename)
                    res.send({
                        "SuccessCode": 0,
                        "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
                        "ErrorCode": 1,
                        "Data":{}
                    });
                }
            }else{
                fs.unlink('./public/' + filename)
                res.send({
                    "SuccessCode": 0,
                    "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
                    "ErrorCode": 1,
                    "Data":{}
                });
            }
        }else{
            fs.unlink('./public/' + filename)
            res.send({
                "SuccessCode": 0,
                "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_FILE_FORMAT_NOT_SUPPORTED"),
                "ErrorCode": 1,
                "Data":{}
            });
        }
    });
}

function addContactsFinal(userId,contactsArr,emailIdArr,req,res){
    contactsArr = common.removeDuplicates_field(contactsArr,'personEmailId');
    userManagements.findUserProfileByIdWithCustomFields(userId,{skypeId:1,firstName:1,lastName:1,emailId:1,publicProfileUrl:1,profilePicUrl:1,companyName:1,designation:1,location:1,mobileNumber:1},function(error,user){
        if(user && common.checkRequired(user._id)){
            aggregateSupportObj.mapUserContactsWithProfile_(user,[],emailIdArr,contactsArr,false);
            var obj =  {contactsUpload:{
                lastUploaded:new Date(),
                isUploaded:true
            }};
            profileManagementClassObj.updateProfileCustomQuery(userId,obj,function(){});
        }
        res.send({
            "SuccessCode": 1,
            "Message": errorMessagesObj.getMessage("UPLOAD_CONTACTS_SUCCESS"),
            "ErrorCode": 0,
            "Data":{}
        });
    });
}

module.exports = router;