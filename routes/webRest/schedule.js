var express = require('express');
var router = express.Router();
var moment = require('moment');
require('moment-range');
var moment_t = require('moment-timezone');
var simple_unique_id = require('simple-unique-id');

var gcal = require('google-calendar');
var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var profileManagementClass = require('../../dataAccess/profileManagementClass');
var contactClass = require('../../dataAccess/contactsManagementClass');
var linkedinM = require('../../common/linkedin');
var appCredentials = require('../../config/relatasConfiguration');
var twitterM = require('../../common/twitter');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var emailSender = require('../../public/javascripts/emailSender');
var meetingSupportClass = require('../../common/meetingSupportClass');
var calendarPasswordManagement = require('../../dataAccess/calendarPasswordManagement');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var winstonLog = require('../../common/winstonLog');
var IPLocatorClass = require('../../common/ipLocator');
var officeOutlookApi = require('../../common/officeOutlookAPI');
var _ = require("lodash");
var userOpenSlots = require('../../common/userOpenSlots');

var company = new companyClass();
var meetingSupportClassObj = new meetingSupportClass();
var profileManagementClassObj = new profileManagementClass();
var emailSenderObj = new emailSender();
var calendarPasswordManagementObj = new calendarPasswordManagement();
var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var linkedinObj = new linkedinM();
var appCredential = new appCredentials();
var twitterObj = new twitterM();
var meetingClassObj = new meetingManagement();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var IPLocator = new IPLocatorClass();
var officeOutlook = new officeOutlookApi();
var userOpenSlotsObj = new userOpenSlots();

var logLib = new winstonLog();
var logger =logLib.getWinston();
var statusCodes = errorMessagesObj.getStatusCodes();
var domain = appCredential.getDomain();

function getSlotRange(mins,timezone){

    var start = moment_t().tz(timezone)
    start.hours(0)
    start.minutes(0)
    start.seconds(0)
    var end   = start.clone();
    end.minutes(mins)

    return moment.range(start.format()+'/'+end.format());

}

function getRangeDays(days,timezone){
    days--;
    var start = moment_t().tz(timezone)
    start.hours(0)
    start.minutes(0)
    start.seconds(0)
    var end   = start.clone();
    end.hours(23)
    end.minutes(59)
    end.seconds(59)
    end.date(end.date()+days);

    return moment.range(start.format()+'/'+end.format());

}

function busySlotsToRanges(slots){

    var ranges = [];
    for(var i=0; i<slots.length; i++){
        ranges.push(
            {
                range:moment.range(slots[i].start+'/'+slots[i].end),
                title:slots[i].title
            }

        )
    }
    return ranges;
}

function getFreeSlots(busyDates,slotRange,uptoDays,timezone,dateTime, getAll,withSlotDates,calendarStartTime,calendarEndTime){

    var current = moment_t().tz(timezone);
    if(common.checkRequired(dateTime))
        dateTime = dateTime.replace(' ','+');

    var inputStart,inputEnd;
    if(common.checkRequired(dateTime)){
        inputStart = moment_t(dateTime).tz(timezone);
        if(moment_t().tz(timezone).format("MM-DD-YYYY") != inputStart.format("MM-DD-YYYY")){
            inputStart.hours(0)
            inputStart.minutes(0)
            inputStart.seconds(0)
        }

        inputEnd = inputStart.clone();
        inputEnd.hours(23)
        inputEnd.minutes(59)
        inputEnd.seconds(59)
    }

    var freeSlots = {};
    // if(common.checkRequired(busyDates) && busyDates.length > 0){
    var slotRangeNew = getSlotRange(slotRange,timezone);

    var daysRange;
    if(common.checkRequired(inputStart) && common.checkRequired(inputEnd)){
        daysRange = moment.range(inputStart.format()+'/'+inputEnd.format());
    }else daysRange = getRangeDays(uptoDays,timezone);

    var busyRanges = busySlotsToRanges(busyDates);

    daysRange.by(slotRangeNew, function(moment) {
        var end = moment.clone();
        end.minutes(end.minutes()+slotRange);

        var range = moment.range(moment.format()+'/'+end.format());
        var overlaped = false;
        var overlappedIndex = null;
        for(var i=0; i<busyRanges.length; i++) {
            if (busyRanges[i].range.overlaps(range)) {
                overlaped = true;
                overlappedIndex = i;
            }
        }

            var m = moment_t(moment).tz(timezone);
            var me = moment_t(end).tz(timezone);

            if(!m.isBefore(current)){
                if(!overlaped){
                    var f = m.format("MM-DD-YYYY");

                    // minutesOfDay(now) > minutesOfDay(end);
                    if(common.checkRequired(freeSlots[f])){

                        freeSlots[f][m.format('a')].slotCount++;
                        if(common.checkRequired(inputStart)){
                            freeSlots[f][m.format('a')].slots.push({start: m.format(),end:me.format(),isBlocked:false});
                        }

                    }
                    else{
                        freeSlots[f] = {
                            date: m.format(),
                            am:{slotCount:0,slots:[]},
                            pm:{slotCount:0,slots:[]}
                        };

                        freeSlots[f][m.format('a')].slotCount++;
                        if(common.checkRequired(inputStart)){
                            freeSlots[f][m.format('a')].slots.push({start: m.format(),end:me.format(),isBlocked:false});
                        }
                    }
                }
                else if((getAll && overlappedIndex != null) || (withSlotDates && overlappedIndex != null)){
                    var f_new = m.format("MM-DD-YYYY");
                    if(common.checkRequired(freeSlots[f_new])){
                        //freeSlots[f_new][m.format('a')].slotCount++;
                        if(common.checkRequired(inputStart) || withSlotDates){

                            freeSlots[f_new][m.format('a')].slots.push({
                                start: m.format(),end:me.format(),
                                title:busyDates[overlappedIndex].title,
                                isTitleExists:common.checkRequired(busyDates[overlappedIndex].title),
                                isBlocked:true
                            });
                        }

                    }
                    else{
                        freeSlots[f_new] = {
                            date: m.format(),
                            am:{slotCount:0,slots:[]},
                            pm:{slotCount:0,slots:[]}
                        };

                        //freeSlots[f_new][m.format('a')].slotCount++;
                        if(common.checkRequired(inputStart) || withSlotDates){
                            freeSlots[f_new][m.format('a')].slots.push({
                                start: m.format(),end:me.format(),
                                title:busyDates[overlappedIndex].title,
                                isTitleExists:common.checkRequired(busyDates[overlappedIndex].title),
                                isBlocked:true
                            });

                        }
                    }
                }
        }
    });
    //  }
    return freeSlots;
}

router.get('/:uniqueName/profile/web',function(req,res){
    if(common.checkRequired(req.params.uniqueName)){
        var userId;
        if(common.isLoggedInUserBoolean(req)){
            userId = common.getUserId(req.user);
        }
        var query;
        if (common.checkRequired(userId)) {
            query = {
                $or: [
                    {_id: userId},
                    {publicProfileUrl: domain.domainName + '/' + req.params.uniqueName},
                    {publicProfileUrl: domain.domainName + '/u/' + req.params.uniqueName},
                    {publicProfileUrl: req.params.uniqueName}
                ],
                publicProfileUrl: {$exists: true}
            }
        }
        else {

            query = {
                $or: [
                    {publicProfileUrl: domain.domainName + '/' + req.params.uniqueName},
                    {publicProfileUrl: domain.domainName + '/u/' + req.params.uniqueName},
                    {publicProfileUrl: req.params.uniqueName}
                ],
                publicProfileUrl: {$exists: true}
            }
        }

        userManagementObj.selectUserProfilesCustomQuery(query,{publicProfileUrl:1,profilePicUrl:1,firstName:1,lastName:1,emailId:1,companyName:1,designation:1,location:1,currentLocation:1,profilePrivatePassword:1,timezone:1},function(error,users){

            var obj = {
                logged_in_user: { "Error": "LOGIN_REQUIRED"},
                other_user:{ "Error": "UNIQUE_NAME_NOT_EXISTS"}
            };

            if(common.checkRequired(users) && users.length > 0){

                for(var i=0; i<users.length; i++){
                    if(users[i]._id.toString() == userId){
                        obj.logged_in_user = users[i];
                    }
                    else{
                        if(common.checkRequired(req.session.publicUser)){
                            req.session.publicUser = users[i];
                            //req.session.publicUserObj = objObj
                        }

                        obj.other_user = users[i];
                    }
                }
            }

            if(!obj.other_user.Error){
                obj.isCalendarLocked = common.checkRequired(obj.other_user.profilePrivatePassword)
                obj.other_user.profilePrivatePassword = null
            }
            else obj.isCalendarLocked = false;

            if(!obj.other_user.Error && !obj.logged_in_user.Error){
                obj.self = obj.logged_in_user._id == obj.other_user._id;
                if(obj.self)
                    obj.other_user = { "Error": "SELF_ACCESS"}
            }else  obj.self = false;

            if(obj.other_user.Error){
                if(req.params.uniqueName == common.getValidUniqueName(obj.logged_in_user.publicProfileUrl)){
                    obj.self = true;
                    obj.other_user = { "Error": "SELF_ACCESS"}
                }else obj.self = false;
            }

            if(req.session[req.params.uniqueName] == true){
                obj.isCalendarLocked = false;
            }

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":obj
            });

        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'UNIQUE_NAME_NOT_FOUND_IN_REQUEST'}));
});

function getBothUserInfo(req,res,callback){
    var userId = common.getUserId(req.user);
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{linkedin:1},function(err,userL) {
        if(err){
            callback('error',null);
        }
        else if (common.checkRequired(userL)) {
            var users = {loggedInUser:userL};
            var query;
            if (common.checkRequired(req.query.id)) {
                query = {_id: req.query.id}
            }
            else if (common.checkRequired(req.query.uniqueName)) {
                query = {$or: [{publicProfileUrl: domain.domainName + '/' + req.query.uniqueName},{publicProfileUrl: domain.domainName + '/u/' + req.query.uniqueName}, {publicProfileUrl: req.query.uniqueName}]}
            }
            if(common.checkRequired(query)) {
                userManagementObj.selectUserProfileCustomQuery(query, {
                    linkedin: 1,
                    twitter: 1
                }, function (err, userO) {
                    if(err){
                        callback('error',null);
                    }
                    else if (common.checkRequired(userO)) {
                        users.otherUser = userO;
                        callback(false,users)
                    }
                    else callback('USER_PROFILE_NOT_FOUND',null)
                })
            }
        }
        else callback('USER_PROFILE_NOT_FOUND',null)
    })
}

// query parameter id( which is userId) or uniqueName
router.get('/schedule/social/contacts/common/web',common.isLoggedInUserToGoNext,function(req,res) {
    var userId = common.getUserId(req.user);
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 10;
    if(common.checkRequired(req.query.id) || common.checkRequired(req.query.uniqueName)){
        getBothUserInfo(req,res,function(error,users){
            if(error || !common.checkRequired(users)){
                if(error == 'USER_PROFILE_NOT_FOUND'){
                    res.send(errorObj.generateErrorResponse({
                        status: statusCodes.PROFILE_ERROR_CODE,
                        key: 'USER_PROFILE_NOT_FOUND'
                    }));
                }
                else {
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                }
            }
            else{
                if(users.loggedInUser._id.toString() != users.otherUser._id.toString()){
                    var project = {
                        _id:"$contacts._id",
                        personId: "$contacts.personId",
                        personName: "$contacts.personName",
                        personEmailId:"$contacts.personEmailId"
                    };
                    contactObj.getCommonContacts(common.castToObjectId(users.loggedInUser._id.toString()),common.castToObjectId(users.otherUser._id.toString()),project,function(contacts) {

                        var total = contacts.length;
                        var results = contacts.splice(skip,limit);
                        //total = results.length;
                        if(results.length > 0){
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":results,
                                "Total":total
                            });
                        }
                        else res.send(errorObj.generateErrorResponse({
                            status: statusCodes.MESSAGE,
                            key: 'NO_COMMON_CONNECTIONS_FOUND'
                        }));
                    })
                }
                else res.send(errorObj.generateErrorResponse({
                    status: statusCodes.MESSAGE,
                    key: 'NO_COMMON_CONNECTIONS_FOUND'
                }));
            }
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
});

// query parameter id( which is userId) or uniqueName
router.get('/schedule/social/linkedin/web',common.isLoggedInUserToGoNext,function(req,res) {
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.query.id) || common.checkRequired(req.query.uniqueName)){
        getBothUserInfo(req,res,function(error,users){
            if(error || !common.checkRequired(users)){
                if(error == 'USER_PROFILE_NOT_FOUND'){
                    res.send(errorObj.generateErrorResponse({
                        status: statusCodes.PROFILE_ERROR_CODE,
                        key: 'USER_PROFILE_NOT_FOUND'
                    }));
                }
                else {
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                }
            }
            else{
                if(users.loggedInUser._id.toString() != users.otherUser._id.toString()){
                    linkedinObj.getOtherUserProfileWithoutCommonConnections(users.otherUser.linkedin,function(errorL,details){
                        if(errorL){
                            if(errorL == 'NO_LINKEDIN_ACCOUNT'){
                                res.send(errorObj.generateErrorResponse({
                                    status: statusCodes.PROFILE_ERROR_CODE,
                                    key: 'NO_LINKEDIN_ACCOUNT'
                                }));
                            }
                            else res.send(errorObj.generateErrorResponse({
                                status: statusCodes.MESSAGE,
                                key: 'LINKEDIN_ERROR'
                            }));
                        }
                        else res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":details
                        });
                    })
                }
                else res.send(errorObj.generateErrorResponse({
                    status: statusCodes.MESSAGE,
                    key: 'NO_LINKEDIN_INFO_FOUND'
                }));
            }
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
});

// query parameter id( which is userId) or uniqueName
router.get('/schedule/social/twitter/web',common.isLoggedInUserToGoNext,function(req,res) {
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.query.id) || common.checkRequired(req.query.uniqueName)){
        getBothUserInfo(req,res,function(error,users){
            if(error || !common.checkRequired(users)){
                if(error == 'USER_PROFILE_NOT_FOUND'){
                    res.send(errorObj.generateErrorResponse({
                        status: statusCodes.PROFILE_ERROR_CODE,
                        key: 'USER_PROFILE_NOT_FOUND'
                    }));
                }
                else {
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                }
            }
            else{
                if(users.loggedInUser._id.toString() != users.otherUser._id.toString()){
                    if(common.isLoggedInUserBoolean(req)){
                        twitterObj.getTwitterFeed_Top_Trending_post_B(users.otherUser.twitter,true,1,function(errorT,tweets){
                            if(errorT){
                                if(errorT == 'NO_TWITTER_ACCOUNT'){
                                    res.send(errorObj.generateErrorResponse({
                                        status: statusCodes.PROFILE_ERROR_CODE,
                                        key: 'NO_TWITTER_ACCOUNT'
                                    }));
                                }
                                else res.send(errorObj.generateErrorResponse({
                                    status: statusCodes.MESSAGE,
                                    key: 'TWITTER_ERROR'
                                }));
                            }
                            else{
                                if(common.checkRequired(tweets) && tweets.length > 0){
                                    res.send({
                                        "SuccessCode": 1,
                                        "Message": "",
                                        "ErrorCode": 0,
                                        "Data":tweets
                                    });
                                }
                                else res.send(errorObj.generateErrorResponse({
                                    status: statusCodes.MESSAGE,
                                    key: 'NO_TWITTER_INFO_FOUND'
                                }));
                            }
                        })
                    }
                    else res.send(errorObj.generateErrorResponse({
                        status: statusCodes.MESSAGE,
                        key: 'NO_TWITTER_INFO_FOUND'
                    }));
                }
                else res.send(errorObj.generateErrorResponse({
                    status: statusCodes.MESSAGE,
                    key: 'NO_TWITTER_INFO_FOUND'
                }));
            }
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
});

// query parameter id( which is userId) or uniqueName
router.get('/schedule/social/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.query.id) || common.checkRequired(req.query.uniqueName)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{linkedin:1},function(err,userL){
            if(common.checkRequired(userL)){
                var query;
                if(common.checkRequired(req.query.id)){
                    query = {_id:req.query.id}
                }
                else if(common.checkRequired(req.query.uniqueName)){
                    query = {$or:[{publicProfileUrl:domain.domainName+'/'+req.query.uniqueName},{publicProfileUrl:req.query.uniqueName}]}
                }

                if(common.checkRequired(query)){
                    userManagementObj.selectUserProfileCustomQuery(query,{linkedin:1,twitter:1},function(err,userO){
                        if(common.checkRequired(userO)){
                            if(userL._id.toString() != userO._id.toString()){
                                contactObj.getCommonContacts(userL._id,userO._id,false,function(contacts){
                                    var total = contacts.length;
                                    var results = contacts.splice(0,10);
                                    linkedinObj.getOtherUserProfileWithCommonConnections(userL.linkedin,userO.linkedin,function(errorL,details){

                                        if(errorL){
                                            details = {Error:"LINKEDIN_ERROR"}
                                        }
                                        if(common.isLoggedInUserBoolean(req)){
                                            twitterObj.getTwitterFeed(userO.twitter,true,1,function(errorT,tweets){
                                                // store social info in redis
                                                if(errorT){
                                                    tweets = {Error:errorT}
                                                }

                                                res.send({
                                                    "SuccessCode": 1,
                                                    "Message": "",
                                                    "ErrorCode": 0,
                                                    "Data":{linkedin:details,twitter:tweets,relatas_common_contacts:results}
                                                });
                                            })
                                        }
                                        else res.send({
                                            "SuccessCode": 1,
                                            "Message": "",
                                            "ErrorCode": 0,
                                            "Data":{linkedin:details,twitter:{Error:"TWITTER_ERROR"},relatas_common_contacts:results}
                                        });
                                    })
                                });
                            }
                            else res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{linkedin:{Error:"LINKEDIN_ERROR"},twitter:{Error:"TWITTER_ERROR"},relatas_common_contacts:results}
                            });
                        }
                        else res.send(errorObj.generateErrorResponse({
                            status: statusCodes.PROFILE_ERROR_CODE,
                            key: 'USER_PROFILE_NOT_FOUND'
                        }));
                    })
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));

            }
            else res.send(errorObj.generateErrorResponse({
                status: statusCodes.PROFILE_ERROR_CODE,
                key: 'USER_PROFILE_NOT_FOUND'
            }));
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
});

router.get('/schedule/slots/available/mapcalendar',function(req,res){
    if(req.session.publicUserObj){
        if(common.checkRequired(req.query.timezone)){
            req.session.publicUserObj.timezone = req.query.timezone;
        }else req.session.publicUserObj.timezone = 'UTC';
    }

    userCalendarTimings(common.castToObjectId(req.query.id),function (error,userProfile) {

        if(common.checkRequired(req.query) && common.checkRequired(req.query.id)){

            var withSlotDates = req.query.withSlotDates == 'yes';
            var lUserId = req.user?common.getUserId(req.user):req.query.id;
            var oUserId = req.query.id;
            var slotDuration = common.checkRequired(req.query.slotDuration) ? parseInt(req.query.slotDuration) : 30
            var daysDuration = common.checkRequired(req.query.daysDuration) ? parseInt(req.query.daysDuration) : 14
            var timezone = req.query.timezone;
            var slotDate = common.checkRequired(req.query.slotDate) ? req.query.slotDate : null;

            var givenDate = moment_t().tz(timezone);
            var dateMin = givenDate.clone();     //min date
            var dateMax = givenDate.clone();     //max date

            dateMax.date(dateMax.date()+daysDuration);

            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)

            dateMax.hour(23)
            dateMax.minute(59)
            dateMax.second(59)
            dateMax.millisecond(59)
            dateMin = dateMin.format()
            dateMax = dateMax.format()

            var findOutlookOrGoogle = 'google';

            if(userProfile.outlook.length>0 && userProfile.google.length === 0){
                findOutlookOrGoogle = 'outlook';
            }
            
            getBusySlots(lUserId,lUserId, oUserId, dateMin,dateMax,timezone,findOutlookOrGoogle,function(busySlots,timezone){

                busySlots = updateCalendarSettings(busySlots,userProfile.workHoursWeek.weekdayHours.start,userProfile.workHoursWeek.weekdayHours.end,timezone);

                // var freeSlots = getFreeSlots(busySlots,slotDuration,daysDuration,timezone,slotDate, slotDate != null,withSlotDates);//why was slotDate !=null being checked?
                var freeSlots = getFreeSlots(busySlots,slotDuration,daysDuration,timezone,slotDate, true,withSlotDates);
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":freeSlots
                });
            });
        }
        else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
    });
});

router.get('/rweblead/schedule/slots/available',function(req,res){
    if(req.session.publicUserObj){
        if(common.checkRequired(req.query.timezone)){
            req.session.publicUserObj.timezone = req.query.timezone;
        }else req.session.publicUserObj.timezone = 'UTC';
    }

    // var timezone = req.query.timezone?req.query.timezone:"UTC";
    var timezone = "UTC";

    if(new Date(req.query.selectedDay) != "Invalid Date"){

        var selectedDate = moment_t(new Date(req.query.selectedDay)).tz(timezone).format("MM-DD-YYYY");
        userCalendarTimings(common.castToObjectId(req.query.id),function (error,userProfile) {

            if(common.checkRequired(req.query) && common.checkRequired(req.query.id)){
                
                var start = userProfile.workHoursWeek.weekdayHours.start?userProfile.workHoursWeek.weekdayHours.start:10;
                var end = userProfile.workHoursWeek.weekdayHours.end?userProfile.workHoursWeek.weekdayHours.end:7+12;

                var userTimezone = userProfile.timezone && userProfile.timezone.name?userProfile.timezone.name:"UTC";

                var lUserId = req.user?common.getUserId(req.user):req.query.id;
                var slotDuration = common.checkRequired(req.query.slotDuration) ? parseInt(req.query.slotDuration) : 30
                var daysDuration = common.checkRequired(req.query.daysDuration) ? parseInt(req.query.daysDuration) : 14

                var givenDate = moment_t(req.query.selectedDay).tz(timezone);
                var givenDate2 = moment_t(req.query.selectedDay).tz(userTimezone);
                var dateMin = givenDate.clone();     //min date
                var dateMax = givenDate.clone();     //max date

                var calendarStartTime = givenDate2.clone();
                var calendarEndTime = givenDate2.clone();

                dateMax.date(dateMax.date()+daysDuration);

                dateMin.hour(0)
                dateMin.minute(0)
                dateMin.second(0)
                dateMin.millisecond(0)

                calendarStartTime.hours(start)
                calendarStartTime.minutes(0)
                calendarStartTime.seconds(0)

                calendarEndTime.hours(0)
                calendarEndTime.minutes(0)
                calendarEndTime.seconds(0)
                
                calendarEndTime.add(end,"hours")

                dateMax.hour(23)
                dateMax.minute(59)
                dateMax.second(59)
                dateMax.millisecond(59)
                dateMin = dateMin.format()
                dateMax = dateMax.format()

                userOpenSlotsObj.findUserMeetingOpenSlotsWithSlotDuration(lUserId,dateMin,dateMax,slotDuration,daysDuration,selectedDate,timezone,function (freeSlots) {

                    freeSlots = _.indexBy(freeSlots, 'key');
                    var slotsForSelectedDay = freeSlots[selectedDate];
                    
                    if(slotsForSelectedDay){

                        var openSlots = [];
                        var allSlots = [];
                        var allSlots2 = [];
                        var option = '';

                        allSlots = allSlots.concat(slotsForSelectedDay.slots.am.slots);
                        allSlots = allSlots.concat(slotsForSelectedDay.slots.pm.slots);

                        _.each(allSlots,function (slot) {

                            allSlots2.push(moment_t(slot.start).tz(req.query.timezone).format())
                            // if(!slot.isBlocked && new Date(slot.start)>new Date(calendarStartTime) && new Date(slot.start)<new Date(calendarEndTime)){

                            if(!slot.isBlocked && moment_t(slot.start).tz(userTimezone).format()>calendarStartTime.tz(userTimezone).format() && moment_t(slot.start).tz(userTimezone).format()<calendarEndTime.tz(userTimezone).format()){
                                // option = option+'<option>'+formatToAmPm(new Date(slot.start))+'</option>'
                                option = option+'<option>'+moment_t(slot.start).tz(req.query.timezone).format("h:mm A")+'</option>'
                                openSlots.push(formatToAmPm(new Date(slot.start)))
                            } else {
                                // option = option+'<option disabled>'+formatToAmPm(new Date(slot.start))+'</option>'
                                option = option+'<option disabled>'+moment_t(slot.start).tz(req.query.timezone).format("h:mm A")+'</option>'
                            }
                        });

                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":openSlots,
                            "html":option,
                            "slots":allSlots,
                            "allSlots2":allSlots2,
                            "start":start,
                            "end":end
                        });

                    } else {
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":[],
                            "html":'<option>No free slots</option>'
                        });
                    }
                });
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
        });
    } else {
        res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}))
    }
});

function updateCalendarSettings(busySlots,start,end,timezone) {

    start = start?start:10;
    end = end?end:6;

    for(var k=1;k<15;k++){

        var calendarStartTime = moment_t().tz(timezone).add(k-1,'days');
        var calendarEndTime = moment_t().tz(timezone).add(k-1,'days');

        calendarStartTime.hours(start)
        calendarStartTime.minutes(0)
        calendarStartTime.seconds(0)

        calendarEndTime.hours(end)
        calendarEndTime.minutes(0)
        calendarEndTime.seconds(0)

        var lastNight = moment_t().tz(timezone).add(k-1,'days').startOf('day');
        var thisNight = moment_t().tz(timezone).add(k-1,'days').endOf('day');

        busySlots.push({
            start:lastNight.format(),
            end:calendarStartTime.format()
        });

        busySlots.push({
            start:calendarEndTime.format(),
            end:thisNight.format()
        });
    }

    return busySlots;
}

// query parameter id, timezone
router.get('/schedule/slots/available',function(req,res){

    if(req.session.publicUserObj){
        if(common.checkRequired(req.query.timezone)){
            req.session.publicUserObj.timezone = req.query.timezone;
        }else req.session.publicUserObj.timezone = 'UTC';
    }

    var withSlotDates = req.query.withSlotDates == 'yes';
    var userId, lUserId = false, gotoGoogle = false;

    if(common.checkRequired(req.query) && common.checkRequired(req.query.id)){
        userId = req.query.id
    }
    else if(common.isLoggedInUserBoolean(req)){
        userId = common.getUserId(req.user);
        lUserId = true
    }

    var slotDuration = common.checkRequired(req.query.slotDuration) ? parseInt(req.query.slotDuration) : 30
    var daysDuration = common.checkRequired(req.query.daysDuration) ? parseInt(req.query.daysDuration) : 14

    userCalendarTimings(common.castToObjectId(userId),function (err,userProfile) {

        if(common.checkRequired(userId)){

            if(req.query.gotoGoogle == 'true'){
                gotoGoogle = true;
            }

            var freeSlotsSession;
            if(lUserId){
                freeSlotsSession = req.session.freeBusyLoggedIn;
            }
            else{
                freeSlotsSession = req.session.freeBusyNonLoggedIn;
            }

            if(common.checkRequired(freeSlotsSession)){
                getBusyNext(userId,req.query.timezone,gotoGoogle,function(slots,timezone){

                    slots = updateCalendarSettings(slots,userProfile.workHoursWeek.weekdayHours.start,userProfile.workHoursWeek.weekdayHours.end,timezone);

                    var freeSlots = getFreeSlots(slots,slotDuration,daysDuration,timezone,req.query.slotDate,true,withSlotDates);

                if(lUserId){
                    req.session.freeBusyLoggedIn = freeSlots;
                }
                else{
                    req.session.freeBusyNonLoggedIn = freeSlots;
                }

                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":freeSlots
                    });
                })
            }
            else{
                getBusyNext(userId,req.query.timezone,true,function(slots,timezone){

                    slots = updateCalendarSettings(slots,userProfile.workHoursWeek.weekdayHours.start,userProfile.workHoursWeek.weekdayHours.end,timezone);

                    var freeSlots = getFreeSlots(slots,slotDuration,daysDuration,timezone,req.query.slotDate,true,withSlotDates);

                if(lUserId){
                    req.session.freeBusyLoggedIn = freeSlots;
                }
                else{
                    req.session.freeBusyNonLoggedIn = freeSlots;
                }

                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":freeSlots
                    });
                })
            }
        }
        else{
            res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'QUERY_PARAMETER_MISSED'}));
        }

    });
});

function getBusyNext(userId,timezoneInput,fromGoogle,callback){

    userManagementObj.findUserProfileByIdWithCustomFields(userId,{contacts:0,linkedin:0,twitter:0,facebook:0,officeCalendar:0},function(error,user){

        if(common.checkRequired(user)){
            var timezone;
            if(common.checkRequired(timezoneInput)){
                timezone = timezoneInput;
            }
            else if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }
            else timezone = 'UTC';

            var givenDate = moment_t().tz(timezone);
            var endDate = givenDate.clone();     //min date
            var timeMax = givenDate.clone();     //max date

            timeMax.date(timeMax.date()+14);

            endDate.hour(0)
            endDate.minute(0)
            endDate.second(0)
            endDate.millisecond(0)

            timeMax.hour(23)
            timeMax.minute(59)
            timeMax.second(59)
            timeMax.millisecond(59)
            endDate = endDate.format()
            timeMax = timeMax.format()

            if(common.validateOutlookAccount(user.outlook)) {

                _.each(user.outlook,function (outlookObj) {
                    if (user.emailId == outlookObj.emailId) {
                        officeOutlook.getNewAccessTokenWithoutStore(outlookObj.refreshToken,function (newAccessToken) {
                            officeOutlook.getCalendarEventsMSGraphAPI(newAccessToken.access_token,user,function (calendarEvents) {
                                officeOutlook.getMeetingObjectFromOutlookCalendar(user,calendarEvents,function (meetings,recurringEventsIdArray) {

                                    officeOutlook.insertMeetingsInBulk(user.emailId,meetings,false,function (insertResults) {

                                        //The previous method checks for meetings already stored.
                                        //As recurring meetings have the same event ID, they will be rejected.
                                        //To overcome this, add these recurring meetings using a separate method insertRecurringMeetingsInBulk().

                                        var recurringMeetingsArr = [];

                                        _.each(recurringEventsIdArray,function (rEvent) {
                                            _.each(meetings,function (meeting) {

                                                var start = new Date(rEvent.recurrence.range.startDate);
                                                var end = new Date(rEvent.recurrence.range.endDate);
                                                var today = new Date();

                                                if(rEvent.id == meeting.invitationId && start<today && today<end){

                                                    var startD = moment(rEvent.recurrence.range.startDate);
                                                    var endD = moment(rEvent.recurrence.range.endDate);

                                                    var counter = endD.diff(startD, "days")

                                                    for(var i=1;i<counter+1;i++){
                                                        var rm = {};
                                                        rm = common.getRecurringMeetingObjects(meeting,i)
                                                        //Clone object
                                                        var newObject = JSON.parse(JSON.stringify(rm));
                                                        recurringMeetingsArr.push(newObject)
                                                    }
                                                }
                                            });
                                        });

                                        _.each(recurringMeetingsArr,function (rm) {
                                            _.each(rm.scheduleTimeSlots,function (s) {
                                                s.start.date = new Date(s.start.date)
                                                s.end.date = new Date(s.end.date)
                                            })
                                        });

                                        if(recurringMeetingsArr.length>0 && insertResults){
                                            officeOutlook.insertRecurringMeetingsInBulk(user.emailId,recurringMeetingsArr)
                                        }
                                    });
                                    var findOutlookOrGoogle = 'outlook';
                                    getBusySlots(user._id.toString(),userId,null,endDate,timeMax,timezone,findOutlookOrGoogle,callback)
                                });
                            });
                        })
                    }
                });

            } else if(fromGoogle){
                var findOutlookOrGoogle = 'google'
                googleCalendar.getGoogleCalendarEventsByTimezone(user,timezone,endDate,timeMax,user.emailId,function(events,timeZoneCalendar){
                    getBusySlots(user._id.toString(),userId,null,endDate,timeMax,timezone,findOutlookOrGoogle,callback)
                });
            }
            else {
                getBusySlots(user._id.toString(),userId,null,endDate,timeMax,timezone,findOutlookOrGoogle,callback)
            }
        }
        else callback(false)
    })
}

function getBusyMeetingsFomMeetings(userId,userId1, userId2, endDate,timeMax,timezone,findOutlookOrGoogle,callback){
    if(common.checkRequired(userId1) && common.checkRequired(userId2)){
        var isFirst = userId == userId1;

        meetingClassObj.userMeetingsByDate_map_sender_receiver(userId1,userId2,endDate,timeMax,findOutlookOrGoogle,function(meetings1,meetings2){
            var ids = [];
            var m = [];
            m = isFirst ? meetings1 : meetings2;
            for(var i=0; i<m.length; i++){
                ids.push(m[i].invitationId);
            }
            meetings1 = meetings1.concat(meetings2);
            if(meetings1.length > 0) meetings1 = common.removeDuplicates_field(meetings1,'invitationId');

            callback(meetings1, ids);
        })
    }
    else{
        meetingClassObj.userMeetingsByDate(userId1 || userId2,endDate,timeMax,{scheduleTimeSlots:1},findOutlookOrGoogle,function(meetings){
            callback(meetings);
        })
    }
}

function getBusySlots(userId,userId1,userId2,endDate,timeMax,timezone,findOutlookOrGoogle,callback){

    var busySlots = [];
    getBusyMeetingsFomMeetings(userId,userId1,userId2,endDate,timeMax,timezone,findOutlookOrGoogle,function(meetings, ids){

        if(meetings != null && meetings != undefined && meetings.length > 0){
            for(var i=0; i<meetings.length; i++){
                if(common.checkRequired(meetings[i].scheduleTimeSlots) && meetings[i].scheduleTimeSlots.length > 0){
                    var isAccepted = false;
                    var slot;
                    for(var t=0;t<meetings[i].scheduleTimeSlots.length;t++){
                        if(meetings[i].scheduleTimeSlots[t].isAccepted){
                            isAccepted = true;
                            slot = t;
                        }
                    }

                    if(isAccepted && common.checkRequired(slot)){
                        var obj = {
                            start: moment_t(meetings[i].scheduleTimeSlots[slot].start.date).tz(timezone).format(),
                            end:moment_t(meetings[i].scheduleTimeSlots[slot].end.date).tz(timezone).format()
                        };
                        if(ids){
                            if(ids.indexOf(meetings[i].invitationId) != -1)
                                obj.title = meetings[i].scheduleTimeSlots[slot].title;
                        }

                        busySlots.push(obj);
                    }
                    else{
                        for(var s=0; s<meetings[i].scheduleTimeSlots.length; s++){
                            var obj2 = {
                                start: moment_t(meetings[i].scheduleTimeSlots[s].start.date).tz(timezone).format(),
                                end:moment_t(meetings[i].scheduleTimeSlots[s].end.date).tz(timezone).format()
                            };

                            if(ids){
                                if(ids.indexOf(meetings[i].invitationId) != -1)
                                    obj2.title = meetings[i].scheduleTimeSlots[s].title;
                            }

                            busySlots.push(obj2);
                        }
                    }
                }
            }
        }

        callback(busySlots,timezone)
    });
}

router.get('/u/:uniqueName',function(req,res,next){
    res.redirect('/'+req.params.uniqueName) ;
});

router.get("/tester/hellor",function(req,res){
    userManagementObj.getTimeZone()
    res.send("tester");
});

router.get('/:uniqueName',function(req,res,next){

    if(common.checkRequired(req.params.uniqueName)){
        if(req.params.uniqueName.substring(0,2) == '[['){
            return res.send(402);
        }

        if (common.checkRequired(req.session.publicUser) && req.session.new_uniqueName == req.params.uniqueName) {
            req.session.publicUserObj.isLoggedIn = common.isLoggedInUserBoolean(req);
            processScheduleMeeting(req,res,req.session.publicUser,req.session.publicUserObj)
        }
        else{

            var tz = req.session.Obj ? req.session.Obj.timezone : null
            req.session.Obj = {timezone:common.checkRequired(tz) ? tz : 'UTC'}

            req.session.is_cal_password_sent = false;
            req.session.filled_meeting_details = null;
            var query = {$or:[{publicProfileUrl:domain.domainName+'/'+req.params.uniqueName},{publicProfileUrl:domain.domainName+'/u/'+req.params.uniqueName},{publicProfileUrl:req.params.uniqueName}]}

            userManagementObj.selectUserProfileCustomQuery(query,{publicProfileUrl:1,profilePicUrl:1,firstName:1,lastName:1,emailId:1,companyName:1,designation:1,location:1,currentLocation:1,profilePrivatePassword:1,timezone:1,corporateUser:1,companyId:1},function(error,user){
                if(common.checkRequired(user)){

                    var obj = getReturnObj(user);
                    obj.isLoggedIn = common.isLoggedInUserBoolean(req);

                    var domainUrl = common.getSubDomain(req);

                    if(domainUrl){

                        if(user.corporateUser){
                            company.findOrCreateCompanyByUrl(common.getCompanyObj(domainUrl,req),{url:1},function(companyProfile){

                                if(common.checkRequired(companyProfile) && user.companyId.toString() == companyProfile._id.toString()){

                                    logger.info('User requested with a valid public url 1');
                                    if(common.isLoggedInUserBoolean(req)){
                                        userManagementObj.findUserProfileByIdWithCustomFields(common.getUserId(req.user),{timezone:1},function(err,userL){

                                            if(common.checkRequired(userL)){
                                                obj.timezone = common.getTimezone(userL.timezone)
                                            }
                                            else obj.timezone = 'UTC'

                                            req.session.new_uniqueName = req.params.uniqueName;
                                            req.session.publicUser = user
                                            req.session.publicUserObj = obj
                                            processScheduleMeeting(req,res,user,obj)
                                        })
                                    }
                                    else{

                                        if(common.checkRequired(user.timezone.updated)){
                                            obj.timezone = user.timezone.name;
                                        }
                                        else obj.timezone = 'UTC';
                                        req.session.new_uniqueName = req.params.uniqueName;
                                        req.session.publicUser = user
                                        req.session.publicUserObj = obj
                                        processScheduleMeeting(req,res,user,obj)
                                    }
                                }
                                else{
                                    logger.info('User requested with invalid public url of corporate user 2');
                                    //Even if the meeting is setup with another corporate user, don't redirect user from their subdomain to other subdomain. Last edited 17 Feb 2016 by Naveen
                                    if(common.isLoggedInUserBoolean(req)){
                                        userManagementObj.findUserProfileByIdWithCustomFields(common.getUserId(req.user),{timezone:1},function(err,userL){

                                            if(common.checkRequired(userL)){
                                                obj.timezone = common.getTimezone(userL.timezone)
                                            }
                                            else obj.timezone = 'UTC'

                                            req.session.new_uniqueName = req.params.uniqueName;
                                            req.session.publicUser = user
                                            req.session.publicUserObj = obj
                                            processScheduleMeeting(req,res,user,obj)
                                        })
                                    }
                                    else{

                                        if(common.checkRequired(user.timezone.updated)){
                                            obj.timezone = user.timezone.name;
                                        }
                                        else obj.timezone = 'UTC';
                                        req.session.new_uniqueName = req.params.uniqueName;
                                        req.session.publicUser = user
                                        req.session.publicUserObj = obj
                                        processScheduleMeeting(req,res,user,obj)
                                    }
                                    //res.render('identityNotFound');
                                }
                            })
                        }
                        else{
                            logger.info('User requested with invalid public url of corporate user 3');

                            //Even if the meeting is setup with another corporate user, don't redirect user from their subdomain to other subdomain. Last edited 17 Feb 2016 by Naveen
                            if(common.isLoggedInUserBoolean(req)){
                                userManagementObj.findUserProfileByIdWithCustomFields(common.getUserId(req.user),{timezone:1},function(err,userL){

                                    if(common.checkRequired(userL)){
                                        obj.timezone = common.getTimezone(userL.timezone)
                                    }
                                    else obj.timezone = 'UTC'

                                    req.session.new_uniqueName = req.params.uniqueName;
                                    req.session.publicUser = user
                                    req.session.publicUserObj = obj
                                    processScheduleMeeting(req,res,user,obj)
                                })
                            }
                            else{

                                if(common.checkRequired(user.timezone.updated)){
                                    obj.timezone = user.timezone.name;
                                }
                                else obj.timezone = 'UTC';
                                req.session.new_uniqueName = req.params.uniqueName;
                                req.session.publicUser = user
                                req.session.publicUserObj = obj
                                processScheduleMeeting(req,res,user,obj)
                            }

                            //res.redirect(domain.domainName+'/'+req.params.uniqueName);
                            //res.render('identityNotFound');
                        }
                    }
                    else{
                        if(!user.corporateUser){

                            logger.info('User requested with a valid public url 4');
                            if(common.isLoggedInUserBoolean(req)){

                                userManagementObj.findUserProfileByIdWithCustomFields(common.getUserId(req.user),{timezone:1},function(err,userL){

                                    if(common.checkRequired(userL)){

                                        obj.timezone = common.getTimezone(userL.timezone)

                                    }
                                    else obj.timezone = 'UTC'

                                    req.session.new_uniqueName = req.params.uniqueName;
                                    req.session.publicUser = user
                                    req.session.publicUserObj = obj
                                    processScheduleMeeting(req,res,user,obj)
                                })
                            }
                            else{

                                userManagementObj.findUserPublicProfile(req.params.uniqueName,{timezone:1},function(err,userL){

                                    if(userL.timezone.zone) {

                                        obj.timezone = userL.timezone.name;
                                        req.session.new_uniqueName = req.params.uniqueName;
                                        req.session.publicUser = user
                                        req.session.publicUserObj = obj
                                        processScheduleMeeting(req,res,user,obj)
                                    } else {
                                        obj.timezone = 'UTC';
                                        req.session.new_uniqueName = req.params.uniqueName;
                                        req.session.publicUser = user
                                        req.session.publicUserObj = obj
                                        processScheduleMeeting(req,res,user,obj)
                                    }
                                });
                            }

                        }
                        else{
                            logger.info('User requested with invalid public url 4');

                            //Even if the meeting is setup with a corporate user, don't redirect user from retail subdomain to corporate subdomain. Last edited 17 Feb 2016 by Naveen
                            if(common.isLoggedInUserBoolean(req)){
                                userManagementObj.findUserProfileByIdWithCustomFields(common.getUserId(req.user),{timezone:1},function(err,userL){

                                    if(common.checkRequired(userL)){
                                        obj.timezone = common.getTimezone(userL.timezone)
                                    }
                                    else obj.timezone = 'UTC'

                                    req.session.new_uniqueName = req.params.uniqueName;
                                    req.session.publicUser = user
                                    req.session.publicUserObj = obj
                                    processScheduleMeeting(req,res,user,obj)
                                })
                            }
                            else{

                                if(common.checkRequired(user.timezone.updated)){
                                    obj.timezone = user.timezone.name;
                                }
                                else obj.timezone = 'UTC';
                                req.session.new_uniqueName = req.params.uniqueName;
                                req.session.publicUser = user
                                req.session.publicUserObj = obj
                                processScheduleMeeting(req,res,user,obj)
                            }
                        }
                    }


                }else res.render('identityNotFound');
            })
        }

    } else res.render('identityNotFound');
});

router.get('/schedule/update/timezone/session',function(req,res){
    req.session.publicUserObj.timezone = req.query.timezone;
    res.send(true);
});

router.get('/schedule/get/timezone/session',function(req,res){

    var uName = req.query.userName;

    if(uName){
     userManagementObj.getUserTzByUname(uName,function (err,timezone) {

         if(!err && timezone[0] && timezone[0].timezone && timezone[0].timezone.name){
             res.send(timezone[0].timezone.name)
         } else {
             res.send("UTC")
         }
     })
    } else {
        res.send(req.session.publicUserObj.timezone);
    }
});

router.delete('/schedule/remove/session',function(req,res){
    req.session.filled_meeting_details = null;
    req.session.publicUserObj = null;
    req.session.server_messages = null;
});

router.get('/schedule/go/back',function(req,res){
    req.session.filled_meeting_details = null;
    req.session.slotDate = null;
    req.session.timezone_schedule = null;
    req.session.slotDuration = null;
    req.session.meridiem = null;
    req.session.publicUserObj.meridiemUpperCase = null;
    req.session.publicUserObj.meridiem = null;
    req.session.publicUserObj.slotDate = null;
    req.session.publicUserObj.s = null;
    req.session.publicUserObj.slotDuration = null;

    req.session.freeBusyNonLoggedIn = null;

    res.send(true);

});

function processScheduleMeeting(req,res,user,obj){
    if(common.checkRequired(req.query.s)){
        if(common.checkRequired(req.session.timezone_schedule)){
            obj.timezone = req.session.timezone_schedule;
            req.session.timezone_schedule = null;
        }

        if(common.checkRequired(req.session.filled_meeting_details)){
            obj.filled_meeting_details = req.session.filled_meeting_details;
        }

        if(common.checkRequired(req.query.slotDuration) && ['15','30','60'].indexOf(req.query.slotDuration) != -1){
            obj.slotDuration = req.query.slotDuration;
        }else obj.slotDuration = '30';

        if(common.checkRequired(req.query.slotDate)){
            obj.slotDate = req.query.slotDate;
            obj.slotDate = obj.slotDate.replace(' ','+');
        }

        if(common.checkRequired(req.query.meridiem)){
            obj.meridiem = req.query.meridiem;
            obj.meridiemUpperCase = req.query.meridiem.toUpperCase();
        }

        if(!obj.isLoggedIn){
            obj.disableIcons = 'disable-icons'
            obj.disableNav  =   'wrapper'

        }else obj.disableIcons = ''
        obj.disableNav    =''

        obj.s = req.query.s;
        req.session.Obj = obj;
        res.redirect('/'+req.params.uniqueName)
    }
    else{
        if(req.session.isMessageRed == true){
            req.session.server_messages = null;
        }
        else req.session.isMessageRed = true;
        var obj2 = req.session.Obj || {};

        if(obj && obj.userId){
            for(var key in obj){
                obj2[key] = obj[key];
            }
        }
        common.getCompanyObjDB(req, function (companyDetails) {
            obj.companyDetails = companyDetails;

            switch (obj2.s) {
                case 'calendar' :
                    res.render('schedule/usercalendar', obj2);
                    break;
                case 'selectedSlot':
                    res.render('schedule/select_slot', obj2);
                    break;
                case 'fillMeetingInfo':
                    res.render('schedule/meeting_details_info', obj2);
                    break;
                case 'login':
                    res.render('schedule/meeting_invite', obj2);
                    break;
                case 'success':
                    req.session.Obj = {timezone:obj2.timezone}
                    res.render('schedule/confirmation', obj2);
                    break;
                default :
                    res.render('schedule/schedule_landing', obj2);
            }
        });
    }

}

function getReturnObj(userProfile){
    var obj = {};
    var userUrl = common.getValidUniqueUrl(userProfile.publicProfileUrl);
    obj.userId=userProfile._id.toString();
    obj.uniqueUrl = userUrl;
    obj.uniqueName = common.getValidUniqueName(userProfile.publicProfileUrl);
    obj.description =  "I've created my Unique Identity on Relatas ("+userUrl+"). Relatas is a new way to interact with your contacts with Unified Communication, Secure Cloud Scheduling and Intelligent Document Sharing";
    obj.image = '/getImage/'+userProfile._id;
    obj.name = userProfile.firstName+' '+userProfile.lastName;
    obj.firstName = userProfile.firstName;
    obj.position = common.getPosition(userProfile.companyName,userProfile.designation);
    obj.location = common.getLocation(userProfile.location,userProfile.currentLocation);
    obj.isCalendarLocked = common.checkRequired(userProfile.profilePrivatePassword);
    //obj.timezone = common.getTimezone(userProfile.timezone);
    return obj;
}

router.post('/schedule/new/meeting',function(req,res){

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){

        req.session.filled_meeting_details = req.body;
        var status = validation.validateCreateMeetingFields(req.body,false);
        if(status.status == 4010){
            res.send(errorObj.generateErrorResponse(status));
        }
        else if(!common.isLoggedInUserBoolean(req)){
            req.session.filled_meeting_details = req.body;
            res.send(errorObj.generateErrorResponse({status:statusCodes.AUTHORIZATION_ERROR_CODE,key:'LOGIN_REQUIRED'}));
        }
        else{
            var userId = common.getUserId(req.user);

            var meetingDetails = req.body;
            if(meetingDetails.selfCalendar){
                if(!common.checkRequired(meetingDetails.toList) || meetingDetails.toList.length <= 0){
                    return res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:'MEETING_TO_LIST_NOT_FOUND'}));
                }

                else scheduleMeetingNext(req,res,userId,null,meetingDetails,true);
            }
            else if(common.checkRequired(meetingDetails.publicProfileUrl)){

                if (common.checkRequired(req.session.publicUser)) {
                    scheduleMeetingNext(req,res,userId,req.session.publicUser,meetingDetails,false)
                }
                else {
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                }

            }
            else return res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:'UNIQUE_NAME_NOT_FOUND'}));
        }
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:'MEETING_INFO_NOT_FOUND'}));
});

router.post('/rweblead/schedule/new/meeting',function(req,res){

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        var data = req.body;
        var meetingDetails = data.meetingDetails;

        var status = validation.validateCreateMeetingFields(meetingDetails,false);
        if(status.status == 4010){
            res.send(errorObj.generateErrorResponse(status));
        }
        var userId = data.userId;
        if(!common.checkRequired(meetingDetails.toList) || meetingDetails.toList.length <= 0){
            return res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:'MEETING_TO_LIST_NOT_FOUND'}));
        } else scheduleMeetingNext(req,res,userId,null,meetingDetails,true,true);

    }else res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:'MEETING_INFO_NOT_FOUND'}));
});

function scheduleMeetingNext(req,res,userId,receiverObj,meetingDetails,isSelfCalendar,isWeb2Lead){

    userManagementObj.findUserProfileByIdWithCustomFields(userId,{firstName:1,lastName:1,emailId:1,profilePicUrl:1,google:1,outlook:1,timezone:1},function(err,sender){
        if(common.checkRequired(sender)){
            var senderObj = {
                senderId:sender._id,
                senderEmailId:sender.emailId,
                senderName:sender.firstName+' '+sender.lastName,
                senderPicUrl:sender.profilePicUrl,
                timezone:sender.timezone && sender.timezone.name?sender.timezone.name:"UTC"
            };

            meetingDetails.senderSelectedTimezone = senderObj.timezone;

            meetingDetails.start = moment_t(meetingDetails.start).tz(senderObj.timezone).format();
            meetingDetails.end = moment_t(meetingDetails.end).tz(senderObj.timezone).format();

            var type = isSelfCalendar ? 'selfCalendar' : 'normal';
            
            meetingSupportClassObj.scheduleNewMeeting(senderObj,receiverObj,type,meetingDetails,req,isWeb2Lead,function(response){

                if(response == false){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                }
                else{

                    if(sender.google.length == 0 && sender.outlook.length>0){
                        _.each(sender.outlook,function (outlookObj) {
                            if(sender.emailId == outlookObj.emailId){
                                writeMeetingOnOutlookCalendar(outlookObj.refreshToken,meetingDetails,sender.timezone,function (err,resultOutlookAPI) {
                                    if(!err && resultOutlookAPI){

                                        //Update Meeting's Id. This is to prevent duplications when the user Logs-in again.
                                        updateOutlookMeetingInvitationId(response,resultOutlookAPI);

                                        if(isWeb2Lead){
                                            var relationKey = 'prospect_customer';
                                            var email = meetingDetails.toList[0].receiverEmailId;

                                            userManagementObj.addContactFromGoogleInteractions(sender.emailId,common.getContactObj(email),function (result) {
                                                contactObj.updateRelationshipTypeByEmail(common.castToObjectId(userId), email, relationKey, 'lead',function (err,result) {
                                                    contactObj.addHashtag(common.castToObjectId(userId), email, "web2lead", function(err,result){
                                                    })
                                                });
                                            });
                                        }

                                        req.session.isMessageRed = false;
                                        req.session.server_messages = {message:errorMessagesObj.getMessage("MEETING_INVITATION_SUCCESS"), type:'success'};

                                        res.send({
                                            "SuccessCode": 1,
                                            "Message": errorMessagesObj.getMessage("MEETING_INVITATION_SUCCESS"),
                                            "ErrorCode": 0,
                                            "Data":{}
                                        });
                                    } else {
                                        res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                                    }
                                });
                            }
                        });

                    } else {

                        var count = 0;
                        var id = simple_unique_id.generate('googleRelatas');
                        var gId = 'relat'+common.getNumString(count)+id;
                        var attendees = [];

                        attendees.push({
                            email:sender.emailId,
                            displayName:sender.firstName+' '+sender.lastName,
                            responseStatus: 'accepted'
                        });

                        _.each(meetingDetails.toList,function (r) {
                            var rec = {
                                email : r.receiverEmailId,
                                displayName : r.receiverFirstName +' '+r.receiverLastName
                            };

                            attendees.push(rec);
                        });

                        var request_body = {
                            "id": gId,
                            "summary": meetingDetails.title,
                            "end": {
                                "dateTime": new Date(meetingDetails.end)
                            },
                            "start": {
                                "dateTime": new Date(meetingDetails.start)
                            },
                            "description": meetingDetails.description,
                            "location": meetingDetails.locationType +" : "+meetingDetails.location,
                            "locationType": meetingDetails.locationType,
                            "locationDetails": meetingDetails.location,
                            "relatasEvent": true,
                            "attendees": attendees
                        };

                        //request_body.sequence = 3;

                        createGoogleEvent(sender,request_body,function (err,result) {
                            
                            if(!err && result){

                                updateGoogleMeetingInvitationId(response,result)

                                req.session.isMessageRed = false;
                                req.session.server_messages = {message:errorMessagesObj.getMessage("MEETING_INVITATION_SUCCESS"), type:'success'};

                                if(isWeb2Lead){
                                    var relationKey = 'prospect_customer';
                                    var email = meetingDetails.toList[0].receiverEmailId;

                                    userManagementObj.addContactFromGoogleInteractions(sender.emailId,common.getContactObj(email),function (result) {
                                        contactObj.updateRelationshipTypeByEmail(common.castToObjectId(userId), email, relationKey, 'lead',function (err,result) {
                                            contactObj.addHashtag(common.castToObjectId(userId), email, "web2lead", function(err,result){
                                            })
                                        });
                                    });
                                }

                                res.send({
                                    "SuccessCode": 1,
                                    "Message": errorMessagesObj.getMessage("MEETING_INVITATION_SUCCESS"),
                                    "ErrorCode": 0,
                                    "Data":{}
                                });

                            } else {
                                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                            }
                        });
                    }
                }
            })
        }
        else{
            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
        }
    });
}

function createGoogleEvent(user,request_body,callback) {

    common.getNewGoogleToken(user.google[0].token,user.google[0].refreshToken,function (newAccessToken) {
        if (newAccessToken) {
            var google_calendar = new gcal.GoogleCalendar(newAccessToken);
            google_calendar.events.insert('primary', request_body,{sendNotifications:true}, function (err, result) {
                callback(err, result);
            });
        } else {
            callback(false,false);
        }
    });
};

function updateOutlookMeetingInvitationId(meetingRelatas,resultOutlookAPI) {

    var outlookMeetingObj = JSON.parse(resultOutlookAPI);
    var relatasMeetingId = meetingRelatas.invitationId;

    meetingClassObj.updateInvitationId(outlookMeetingObj.id,relatasMeetingId,false);
}

function updateGoogleMeetingInvitationId(meetingRelatas,googleMeetingObj) {

    var relatasMeetingId = meetingRelatas.invitationId;

    meetingClassObj.updateGoogleEventId(googleMeetingObj.id,relatasMeetingId,false);
}

function writeMeetingOnOutlookCalendar(refreshToken,meetingDetails,timezone,callback){

    var attendees = [];

    _.each(meetingDetails.toList,function (attendee) {
        attendees.push({
            "EmailAddress": {
                "Address": attendee.receiverEmailId,
                "Name": attendee.receiverFirstName +' '+ attendee.receiverLastName
            },
            "Type": "Required"
        })
    });

    var meeting = {
        "Subject": meetingDetails.title,
        "Body": {
            "ContentType": "Text",
            "Content": meetingDetails.description
        },
        "Start": {
            "DateTime": meetingDetails.start,
            "TimeZone": timezone.name?timezone.name:'UTC'
        },
        "End": {
            "DateTime": meetingDetails.end,
            "TimeZone": timezone.name?timezone.name:'UTC'
        },
        "Attendees": attendees,
        "Location":{
            "DisplayName":meetingDetails.location+":"+meetingDetails.locationType
        }
    };

    officeOutlook.getNewAccessTokenWithoutStore(refreshToken,function (newToken) {
        officeOutlook.writeMeetingOnCalendarMSGraphAPI(newToken.access_token,meeting,callback);
    });
}

router.post('/schedule/new/meeting/withoutlogin',function(req,res){

    var senderSelectedTimezone = req.body.timezone;
    var mDetails = req.session.filled_meeting_details;
    mDetails.senderEmailId = req.body.senderEmailId.toLowerCase();
    mDetails.senderFirstName = req.body.senderFirstName;
    mDetails.senderLastName = req.body.senderLastName;
    var uName = mDetails.senderFirstName+" "+mDetails.senderLastName;
    uName = uName.toLowerCase();

    req.body = mDetails;

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)) {
        var status = validation.validateCreateMeetingFields(req.body, true);
        if (status.status == 4010) {
            res.send(errorObj.generateErrorResponse(status));
        }
        else {
            var meetingDetails = req.body;

            if(common.checkRequired(meetingDetails.publicProfileUrl)){
                var userObj = {
                    firstName: mDetails.senderFirstName,
                    lastName: mDetails.senderLastName,
                    profilePicUrl:'/images/default.png',
                    publicProfileUrl: uName.replace(/[^a-zA-Z0-9]/g,''),
                    designation: '',
                    companyName: '',
                    location: '',
                    public: true,
                    emailId: mDetails.senderEmailId,
                    day: 0,
                    month: 0,
                    year: 0,
                    registeredUser: false,
                    partialFill: true,
                    serviceLogin: 'relatas',
                    currentLocation: null,
                    userType:'not-registered',
                    timezone:{name:senderSelectedTimezone}
                };

                if (common.checkRequired(req.session.publicUser)) {
                    userManagementObj.findUserProfileByEmailIdWithCustomFields(mDetails.senderEmailId,{userType:1,firstName:1,lastName:1,emailId:1,registeredUser:1,publicProfileUrl:1,partialFill:1,google:1,outlook:1},function(error,user){
                        if(common.checkRequired(user) && user.userType != "not-registered"){
                            scheduleNewMeetingWithoutLoginNext(req,res,meetingDetails,user,senderSelectedTimezone);
                        }
                        else if(common.checkRequired(user) && user.userType == "not-registered"){
                            req.session.partialProfile = true;
                            req.session.save();

                            scheduleNewMeetingWithoutLoginNext(req,res,meetingDetails,user,senderSelectedTimezone);
                        }
                        else{
                            //userObj.publicProfileUrl = userObj.publicProfileUrl+new Date().getMilliseconds();
                            // create profile

                            var ipAddress = common.getClientIp2(req);

                            userManagementObj.addUserSchedule(userObj,true,updateCurrentLocation,ipAddress,function(err,user,message){
                                if(common.checkRequired(user)){

                                    if(user.partialFill){
                                        //Send Email for Partial profile creation
                                        userObj.partialFill = user.partialFill;
                                        userObj._id = user._id;
                                        emailSenderObj.sendPartialRegistrationMail(userObj);
                                    }
                                    req.session.partialProfile = true;
                                    req.session.save();
                                    scheduleNewMeetingWithoutLoginNext(req,res,meetingDetails,user,senderSelectedTimezone);
                                }
                                else{
                                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                                }
                            })
                        }
                    });
                }
                else {
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                }
            }
            else return res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:'UNIQUE_NAME_NOT_FOUND'}));
        }
    }
});

function scheduleNewMeetingWithoutLoginNext(req,res,meetingDetails,user,senderSelectedTimezone){

    var count = 0;
    var id = simple_unique_id.generate('googleRelatas');
    var gId = 'relat'+common.getNumString(count)+id;
    var attendees = [];

    attendees.push({
        email:user.emailId,
        displayName:user.firstName+' '+user.lastName,
        responseStatus: 'accepted'
    });

    _.each(meetingDetails.toList,function (r) {
        var rec = {
            email : r.receiverEmailId,
            displayName : r.receiverFirstName +' '+r.receiverLastName
        };

        attendees.push(rec);
    });

    var request_body = {
        "id": gId,
        "summary": meetingDetails.title,
        "end": {
            "dateTime": meetingDetails.end
        },
        "start": {
            "dateTime": meetingDetails.start
        },
        "description": meetingDetails.description,
        "location": meetingDetails.locationType +" : "+meetingDetails.location,
        "locationType": meetingDetails.locationType,
        "locationDetails": meetingDetails.location,
        "relatasEvent": true,
        "attendees": attendees
    };

    var nonRegisteredTimeZoneAbbr = senderSelectedTimezone;

    var senderObj = {
        senderId:user._id,
        senderEmailId:user.emailId,
        senderName:user.firstName+' '+user.lastName,
        senderPicUrl:user.profilePicUrl || '/images/default.png',
        senderSelectedTimezone:nonRegisteredTimeZoneAbbr
    };

    var type = meetingDetails.selfCalendar ? 'selfCalendar' : 'normal';
    meetingSupportClassObj.scheduleNewMeeting(senderObj,req.session.publicUser,type,meetingDetails,req,false,function(response){

        if(response){

            if(req.session.partialProfile){
                req.session.isMessageRed = false;
                req.session.server_messages = {
                    message:errorMessagesObj.getMessage("MEETING_INVITATION_SUCCESS"),
                    type:'success',
                    messageNewProfile:errorMessagesObj.getMessage(null,!user.registeredUser,user.publicProfileUrl,user._id)
                };
                res.send({
                    "SuccessCode": 1,
                    "Message": errorMessagesObj.getMessage("MEETING_INVITATION_SUCCESS"),
                    "ErrorCode": 0,
                    "Data":response
                });
            }
            else if(user.google.length>0 && user.outlook.length == 0){
                createGoogleEvent(user,request_body,function (err,result) {
                    if(!err){
                        updateGoogleMeetingInvitationId(response,result)
                        req.session.isMessageRed = false;
                        req.session.server_messages = {
                            message:errorMessagesObj.getMessage("MEETING_INVITATION_SUCCESS"),
                            type:'success',
                            messageNewProfile:errorMessagesObj.getMessage(null,!user.registeredUser,user.publicProfileUrl,user._id)
                        };
                        res.send({
                            "SuccessCode": 1,
                            "Message": errorMessagesObj.getMessage("MEETING_INVITATION_SUCCESS"),
                            "ErrorCode": 0,
                            "Data":response
                        });

                    } else {
                        res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                    }
                });

            } else {
                _.each(user.outlook,function (outlookObj) {
                    if (user.emailId == outlookObj.emailId) {
                        writeMeetingOnOutlookCalendar(outlookObj.refreshToken, meetingDetails, user.timezone, function (err, resultOutlookAPI) {

                            if (!err && resultOutlookAPI) {

                                req.session.isMessageRed = false;
                                req.session.server_messages = {
                                    message:errorMessagesObj.getMessage("MEETING_INVITATION_SUCCESS"),
                                    type:'success',
                                    messageNewProfile:errorMessagesObj.getMessage(null,!user.registeredUser,user.publicProfileUrl,user._id)
                                };
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": errorMessagesObj.getMessage("MEETING_INVITATION_SUCCESS"),
                                    "ErrorCode": 0,
                                    "Data":response
                                });

                                //Update Meeting's Id. This is to prevent duplications when the user Logs-in again.
                                updateOutlookMeetingInvitationId(response, resultOutlookAPI)
                            } else {
                                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                            }
                        });
                    } else {
                        res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                    }
                });
            }
        }
        else{
            //req.session.server_messages = {message:errorMessagesObj.getMessage("MEETING_INVITATION_FAILED"), type:'success'};
            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
        }
    })
}

router.post('/schedule/session/saveMeetingDetails/new',function(req,res){
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)) {
        req.session.filled_meeting_details = req.body;
        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data":{}
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

router.post('/schedule/session/saveMeetingDetails',function(req,res){
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)) {
        var status = validation.validateCreateMeetingFields(req.body,false);
        if(status.status == 4010){
            res.send(errorObj.generateErrorResponse(status));
        }
        else{
            req.session.filled_meeting_details = req.body;
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{}
            });
        }
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

router.get('/schedule/session/getSavedMeetingDetails',function(req,res){
    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data":common.checkRequired(req.session.filled_meeting_details) ? req.session.filled_meeting_details : null
    });
});

router.get('/schedule/calendar/lock/request',common.isLoggedInUserToGoNext,function(req,res){
    common.getProfileOrStoreProfileInSession(common.getUserId(req.user),req,function(user){
        common.sendCalendarPasswordRequest(user,req.session.publicUser,function(result){
            if(result){
                req.session.is_cal_password_sent = true;
                res.send({
                    "SuccessCode": 1,
                    "Message": errorMessagesObj.getMessage("CALENDAR_PASSWORD_REQUEST_SENT"),
                    "ErrorCode": 0,
                    "Data":{}
                });
            }
            else res.send({
                "SuccessCode": 0,
                "Message": errorMessagesObj.getMessage("CALENDAR_PASSWORD_REQUEST_FAILED"),
                "ErrorCode": 0,
                "Data":{}
            });
        });
    });
});

router.get('/schedule/header',function(req,res){
    res.render('schedule/header',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/schedule/headerV2',function(req,res){
    res.render('schedule/headerV2',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/schedule/schedule_header',function(req,res){
    res.render('schedule/schedule_header',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/schedule/profile_banner',function(req,res){
    res.render('schedule/profile_banner',req.session.publicUserObj);
});

router.get('/schedule/meeting_details_info',function(req,res){
    res.render('schedule/meeting_details_info');

});

router.get('/schedule/usercalendar',function(req, res){
    res.render('schedule/usercalendar')
});

router.get('/schedule/send_meeting',function(req, res){
    var data = req.session.filled_meeting_details
    data.slotDuration = req.session.publicUserObj.slotDuration;
    res.render('schedule/send_meeting',data);
});

router.get('/series/commonConnection&InviteMeeting',function(req, res){
    res.render('series/commonConnection&InviteMeeting');
});

router.get('/schedule/calendarlock',function(req, res){

    var isLoggedIn = common.isLoggedInUserBoolean(req);
    var hideButton = '';
    if(req.session.is_cal_password_sent == true || !isLoggedIn){
        hideButton = 'hide';
    }
    var cal_loc_message_2 = '';
    if(req.session.is_cal_password_sent == true){
        cal_loc_message_2 = 'Request has been sent to unlock the calendar';
    }
    else if(isLoggedIn){
        cal_loc_message_2 = 'Please send a request to '+req.session.publicUserObj.firstName+' to get access to the calendar.'
    }else cal_loc_message_2 = req.session.publicUserObj.firstName+' shares his calendar with people who are signed in.'


    var locObj = {
        disabled:req.session.is_cal_password_sent ? 'disabled' : '',
        cal_loc_message_1:'The calendar is locked.',
        cal_loc_message_2:cal_loc_message_2,
        hide_but:hideButton,
        hide_login_but:isLoggedIn ? 'hide' : '',
        cal_pass_req_url_outlook:isLoggedIn ? '#' : '/authenticate/office365Auth/new?onSuccess='+req.session.publicUserObj.uniqueName+'&onFailure='+req.session.publicUserObj.uniqueName+'&action=cal_pass_request',
        cal_pass_req_url_google:isLoggedIn ? '#' : '/authenticate/google/web?onSuccess='+req.session.publicUserObj.uniqueName+'&onFailure='+req.session.publicUserObj.uniqueName+'&action=cal_pass_request'
    };
    res.render('schedule/calendarlock',locObj);
});

router.get('/schedule/index',function(req, res){
    res.render('schedule/index')
});

router.get('/contacts/remove/invalid',function(req,res){
    contactObj.removeInvalidContacts(function(data){
        res.send(data);
    });

});

router.get('/schedule/get/message', function (req, res) {

    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data":req.session.server_messages
    });

});

router.delete('/schedule/remove/message', function (req, res) {
    //  req.session.isMessageRed = true;
    res.send(true);
});

router.post('/schedule/action/calendarPasswordResponse',common.isLoggedInUserToGoNext,function(req,res){
    var resObj = req.body;
    var userId = common.getUserId(req.user);
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,profilePrivatePassword:1,publicProfileUrl:1},function(error,user){
        if(user && common.checkRequired(user._id)){
            resObj.name = user.firstName;
            resObj.emailId = user.emailId;
            resObj.publicProfileUrl = user.publicProfileUrl;
            resObj.profilePrivatePassword = user.profilePrivatePassword;
            if(common.checkRequired(resObj.requestedByName) && common.checkRequired(resObj.requestedByEmailId) && common.checkRequired(user.publicProfileUrl) && common.checkRequired(user.profilePrivatePassword) && common.checkRequired(user.emailId) && common.checkRequired(resObj.name) && common.checkRequired(resObj.requestId) && common.checkRequired(resObj.accepted)){
                var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
                var host = req.headers.host;
                resObj.url = protocol+host+'/u/'+resObj.publicProfileUrl+'/'+resObj.profilePrivatePassword;
                calendarPasswordManagementObj.updateCalendarRequest(resObj,function(error,isUpdated){
                    if(error){
                        res.send({
                            "SuccessCode": 0,
                            "Message": errorMessagesObj.getMessage("CALENDAR_PASSWORD_APPROVE_FAILED"),
                            "ErrorCode": 1,
                            "Data":{}
                        });
                    }
                    else if(isUpdated){
                        emailSenderObj.sendCalendarPasswordMail(resObj);
                        emailSenderObj.sendCalendarPasswordSentMail(resObj);
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":{}
                        });
                    }
                    else{
                        res.send({
                            "SuccessCode": 0,
                            "Message": errorMessagesObj.getMessage("CALENDAR_PASSWORD_APPROVE_FAILED"),
                            "ErrorCode": 1,
                            "Data":{}
                        });
                    }
                })
            }
            else{
                res.send({
                    "SuccessCode": 0,
                    "Message": errorMessagesObj.getMessage("CALENDAR_PASSWORD_APPROVE_FAILED"),
                    "ErrorCode": 1,
                    "Data":{}
                });
            }
        }
        else res.send({
            "SuccessCode": 0,
            "Message": errorMessagesObj.getMessage("CALENDAR_PASSWORD_APPROVE_FAILED"),
            "ErrorCode": 1,
            "Data":{}
        });
    });
});

router.post('/schedule/freeslot/meetingroom',function(req,res){
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body) && common.checkRequired(req.body.companyId) && common.checkRequired(req.body.start) && common.checkRequired(req.body.end)){
        profileManagementClassObj.getMeetingRoomIdListWithCompanyId(common.castToObjectId(req.body.companyId),function(data){
            if(data && data.length > 0){
                var list = data[0].list;
                meetingClassObj.findMeetingRoomSlotInfo(list,req.body.start,req.body.end,function(meetingRooms){
                    if(meetingRooms && meetingRooms.length > 0){
                        var rooms = []
                        for(var j=0; j<data[0].rooms.length; j++){
                            if(meetingRooms[0].rooms.indexOf(data[0].rooms[j]._id.toString()) == -1){
                                rooms.push(data[0].rooms[j])
                            }
                        }

                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":rooms
                        });
                    }
                    else res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":data[0].rooms
                    });
                })
            }
            else res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":[]
            });
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.MANDATORY_FIELD_ERROR_CODE,key:'MEETING_INFO_NOT_FOUND'}));
});

function jsonToQueryString(json) {
    return '?' +
        Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
}

router.get('/schedule/partialProfile/next/onboarding',function(req,res){
    if(common.checkRequired(req.query.userId)){
        userManagementObj.findUserProfileByIdWithCustomFields(req.query.userId,{contacts:0},function(error,user){
            if(error){
                res.render('error');
            }
            else{
                req.session.uIdentity = common.getValidUniqueName(user.publicProfileUrl);
                req.session.partialFillAcc = user;
                return res.redirect('/onboarding/new');
                //return res.redirect(appCredential.getDomain().domainName+'/onboarding/new');
            }
        })
    }
    else res.render('error');
});

router.get('/schedule/meeting/location/error',common.isLoggedInUserToGoNext,function(req,res){
    res.send(req.session.locationStatus)
});

router.get('/test/meetings',common.isLoggedInUserToGoNext,function (req,res) {
    var userId = common.getUserId(req.user);
    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId),{firstName:1,lastName:1,emailId:1,profilePicUrl:1,google:1,outlook:1,timezone:1},function(err,sender){

        var count = 0;
        var id = simple_unique_id.generate('googleRelatas');
        var gId = 'relat'+common.getNumString(count)+id;

        var request_body = {
            "id": gId,
            "summary": "Google to Google LIU",
            "end": {
                "dateTime": "2016-07-25T12:30:00.000Z"
            },
            "start": {
                "dateTime": "2016-07-25T12:00:00.000Z"
            },
            "description": "Hello, Jimmy.\n\nLet's catch up over a cuppa.",
            "location": "Phone: 34567890",
            "locationType": "Phone",
            "locationDetails": "34567890",
            "relatasEvent": true,
            "attendees": [
                {
                    "email": "naveenpaul.markunda@gmail.com",
                    "displayName": "Naveen",
                    "responseStatus": "accepted"
                },
                {
                    "email": "jimmytestacc@gmail.com",
                    "displayName": "Jimmy Paul",
                    "responseStatus": "accepted"
                }
            ]
        }

        //request_body.sequence = 3;
        common.getNewGoogleToken(sender.google[0].token,sender.google[0].refreshToken,function (newAccessToken) {
            if(newAccessToken){

                var google_calendar = new gcal.GoogleCalendar(newAccessToken);
                google_calendar.events.insert('primary',request_body, function (err, result) {
                    res.send(result)
                });
            } else{
                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
            }
        });
    });
});

function getClientIp(req) {
    var ipAddress;
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        ipAddress = req.connection.remoteAddress;
    }
    return ipAddress;
}

function userCalendarTimings(userId,callback) {

    userManagementObj.findUserProfileByIdWithCustomFields(userId,{workHoursWeek:1,google:1,outlook:1,timezone:1},function(err,user){
        callback(err,user)
    });
}

function updateCurrentLocation(IP, userId) {
    IPLocator.lookup(IP, userId, function (location) {
    });
}

var minutesOfDay = function(m){
    return m.minutes() + m.hours() * 60;
}

function formatToAmPm(inputDate) {
    var hours = inputDate.getHours();
    var minutes = inputDate.getMinutes();
    var ampm = hours < 12? " AM" : (hours=hours%12," PM");
    hours = hours == 0? 12 : hours < 10? ("0" + hours) : hours;
    minutes = minutes < 10 ? ("0" + minutes) : minutes;
    return hours + ":" + minutes + ampm;
}

module.exports = router;
