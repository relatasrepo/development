
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs')

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactions = require('../../dataAccess/interactionManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var Gmail = require('../../common/gmail');
var winstonLog = require('../../common/winstonLog');
var emailSender = require('../../public/javascripts/emailSender');
var massMailCountManagement = require('../../dataAccess/massMailCountManagement');
var officeOutlookApi = require('../../common/officeOutlookAPI');
var _ = require("lodash");

var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionObj = new interactions();
var gmailObj = new Gmail();
var logLib = new winstonLog();
var officeOutlook = new officeOutlookApi();

var statusCodes = errorMessagesObj.getStatusCodes();
var logger =logLib.getMobileErrorLogger();

router.get('/message/get/email/single/web',common.isLoggedInUserOrMobile,function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    
    if(common.checkRequired(req.query.emailContentId)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,outlook:1},function(error,user){

            if(googleCalendar.validateUserGoogleAccount(user)){

                var googleAccount = common.getGoogleAccountByGoogleAccountEmailId(user.google,req.query.googleAccountEmailId);
                gmailObj.getEmail(req.query.emailContentId,googleAccount.token,googleAccount.refreshToken,googleAccount.emailId,function(error,response){

                    if(error){
                        res.send({
                            "SuccessCode": 0,
                            "Message": errorMessagesObj.getMessage("GET_EMAIL_BODY_FAILED"),
                            "ErrorCode": 1,
                            "Data": {}
                        });
                    }
                    else{

                        if(response && response.payload && response.payload.headers && response.payload.headers.length > 0 && !response.payload.parts){

                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data": convertMailToHtml(response)
                            });

                        } else if(response && response.payload && response.payload.parts && response.payload.parts.length > 0){
                            var body = {};
                            for(var i=0; i<response.payload.parts.length; i++){
                                if(response.payload.parts[i].mimeType == "text/html"){
                                    body = response.payload.parts[i].body;
                                }
                            }

                            if(body && !body.data){
                                for(var j=0; j<response.payload.parts.length; j++){
                                    if(response.payload.parts[j].parts && response.payload.parts[j].parts.length > 0){
                                        response.payload.parts[j].parts.forEach(function(part){
                                            if(part.mimeType == "text/html"){
                                                body = part.body;
                                            }
                                        })
                                    }
                                }
                            }

                            if(body && body.data){
                                body.data = new Buffer(body.data, 'base64').toString()
                                
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data": body
                                });
                            }
                            else {
                                res.send({
                                    "SuccessCode": 0,
                                    "Message": errorMessagesObj.getMessage("GET_EMAIL_BODY_FAILED"),
                                    "ErrorCode": 1,
                                    "Data": {}
                                });
                            }
                        }
                        else res.send({
                            "SuccessCode": 0,
                            "Message": errorMessagesObj.getMessage("GET_EMAIL_BODY_FAILED"),
                            "ErrorCode": 1,
                            "Data": {}
                        });
                    }
                })
            }
            else{

                if(user.outlook && user.outlook.length>0){
                    _.each(user.outlook,function (outlookObj) {
                        if(user.emailId == outlookObj.emailId){
                            getOutlookMail(outlookObj.refreshToken,req.query.emailContentId,function (err,response) {
                                if(!err && response){
                                    var body = {};
                                    body.data= response
                                    res.send({
                                        "SuccessCode": 1,
                                        "Message": "",
                                        "ErrorCode": 0,
                                        "Data": body
                                    });
                                } else{
                                    res.send({
                                        "SuccessCode": 0,
                                        "Message": errorMessagesObj.getMessage("GET_EMAIL_BODY_FAILED"),
                                        "ErrorCode": 1,
                                        "Data": {}
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.send({
                        "SuccessCode": 0,
                        "Message": errorMessagesObj.getMessage("GET_EMAIL_BODY_FAILED"),
                        "ErrorCode": 1,
                        "Data": {}
                    });
                }
            }
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'EMAIL_CONTENT_ID_NOT_FOUND_IN_REQUEST'}));
});

function convertMailToHtml(response){
    var body = {};
    for(var i=0; i<response.payload.headers.length; i++){
        if(response.payload.headers[i].mimeType == "text/html"){
            body = response.payload.headers[i].body;
        }
    }

    if(body && !body.data){
        if(response.payload.body){
            body = response.payload.body
        }
    }

    if(body && body.data) {
        body.data = new Buffer(body.data, 'base64').toString()
    }

    return body
}

function getOutlookMail(refreshToken,emailContentId,callback) {
    // var userRegistrationToken;
    officeOutlook.getNewAccessTokenWithoutStore(refreshToken,function (newToken) {
        officeOutlook.getEmailByIdMSGraphAPI(newToken.access_token,emailContentId,callback)
    });
};

router.post('/messages/send/email/single/web',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)) {
        var status = validation.validateSendMessageFields(req.body);
        if (status.status == 5000) {
            logger.info('Email Validation error status 5000',status,req.body);
            res.send(errorObj.generateErrorResponse(status,errorMessagesObj.getMessage("SEND_EMAIL_FAILED")));
        }
        else {
            var data = req.body;

            data.message = data.message.replace(/(?:\r\n|\r|\n)/g, '<br>');

            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1,outlook:1},function(error,user){

                if(common.checkRequired(user)){

                    if(user.google.length == 0 && user.outlook.length>0){
                        _.each(user.outlook,function (outlookObj) {
                            if(user.emailId == outlookObj.emailId){
                                sendOutlookMail(outlookObj.refreshToken,data,user,res);
                            }
                        });

                    } else {

                        var dataToSend = {
                            email_cc:data.email_cc,
                            receiverEmailId:data.receiverEmailId,
                            receiverId:common.checkRequired(data.receiverId) ? data.receiverId : '',
                            receiverName:data.receiverName,
                            senderId:user._id,
                            senderEmailId:user.emailId,
                            senderName:user.firstName+' '+user.lastName,
                            message:data.message,
                            subject:data.subject,
                            trackViewed:true, //data.trackViewed,
                            docTrack:data.docTrack,
                            remind:data.remind,
                            refId:data.refId,
                            updateReplied:data.updateReplied || false
                        };

                        sendEmail(dataToSend,user,req,res);
                    }
                    //Update respSentTime
                    if(common.checkRequired(data.respSentTime)){
                        contactObj.updateResponseTimeStamp(userId,data.receiverEmailId,data.respSentTime,data.isLeadTrack);
                    }

                    //Add email IDs to contact list after getting the receiver's UserID. Using for Prospect Leads feature

                    if(common.checkRequired(data.assignedTimeISO)){
                        userManagementObj.getUserByEmail(data.receiverEmailId,function(err,relatasProfile){

                            var contactUserId = relatasProfile._id;

                            for(var i=0;i<data.prospectEmailLeads.length;i++){

                                var obj = {};
                                obj.personEmailId = data.prospectEmailLeads[i];
                                obj.notificationStartTime = data.assignedTimeISO;
                                obj.respLimit = data.deadLineISO;
                                obj.contactRelation = {prospect_customer:'prospect'};
                                obj.hashtagThis = true;

                                contactObj.addSingleContact(contactUserId,obj);
                            }

                        });
                    }

                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("SEND_EMAIL_FAILED")));
            });
        }
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("SEND_EMAIL_FAILED")));
});

router.post('/rweblead/messages/send/email/single/web',function(req,res){

    var userId = req.body.userId;
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)) {
        var status = validation.validateSendMessageFields(req.body);
        if (status.status == 5000) {
            logger.info('Email Validation error status 5000',status,req.body);
            res.send(errorObj.generateErrorResponse(status,errorMessagesObj.getMessage("SEND_EMAIL_FAILED")));
        }
        else {
            var data = req.body;

            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1,outlook:1,designation:1,publicProfileUrl:1,companyName:1},function(error,user){

                if(common.checkRequired(user)){
                    var signature = user.firstName +' '+ user.lastName+'\n'+user.designation+', '+user.companyName+'\n'+'www.relatas.com/'+user.publicProfileUrl;

                    data.email_cc = [user.emailId];
                    data.message = data.message+'\n\n\n'+signature;
                    if(user.google.length == 0 && user.outlook.length>0){
                        _.each(user.outlook,function (outlookObj) {
                            if(user.emailId == outlookObj.emailId){
                                sendOutlookMail(outlookObj.refreshToken,data,user,res);

                                var relationKey = 'prospect_customer';
                                var email = data.receiverEmailId;

                                userManagementObj.addContactFromGoogleInteractions(user.emailId,common.getContactObj(email),function (result) {
                                    contactObj.updateRelationshipTypeByEmail(common.castToObjectId(userId), email, relationKey, 'lead',function (err,result) {
                                        contactObj.addHashtag(common.castToObjectId(userId), email, "web2lead", function(err,result){
                                        })
                                    });
                                });
                            }
                        });

                    } else {

                        var dataToSend = {
                            email_cc:data.email_cc,
                            receiverEmailId:data.receiverEmailId,
                            receiverId:common.checkRequired(data.receiverId) ? data.receiverId : '',
                            receiverName:data.receiverName,
                            senderId:user._id,
                            senderEmailId:user.emailId,
                            senderName:user.firstName+' '+user.lastName,
                            message:data.message,
                            subject:data.subject,
                            trackViewed:true, //data.trackViewed,
                            docTrack:data.docTrack,
                            remind:data.remind,
                            refId:data.refId,
                            updateReplied:data.updateReplied || false
                        };

                        sendEmail(dataToSend,user,req,res);

                        var relationKey = 'prospect_customer';
                        var email = data.receiverEmailId;

                        userManagementObj.addContactFromGoogleInteractions(user.emailId,common.getContactObj(email),function (result) {
                            contactObj.updateRelationshipTypeByEmail(common.castToObjectId(userId), email, relationKey, 'lead',function (err,result) {
                                contactObj.addHashtag(common.castToObjectId(userId), email, "web2lead", function(err,result){
                                })
                            });
                        });
                    }

                }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("SEND_EMAIL_FAILED")));
            });
        }
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("SEND_EMAIL_FAILED")));
});

function sendOutlookMail(refreshToken,mailBody,user,res) {

    var mail = {
        "Comment": mailBody.message
    }

    officeOutlook.getNewAccessTokenWithoutStore(refreshToken,function (newToken) {

        if(mailBody.newMessage){
            sendNewOutlookMail(newToken.access_token,mailBody,user,res)
        } else {

            mail = {
                "Comment": mailBody.originalBody
            }

            var id = mailBody.refId?mailBody.refId:mailBody.id;

            officeOutlook.createReplyEmailMSGraphAPI(newToken.access_token,mail,id,function (error,response) {

                if(!error && response){

                    var draftMessage = JSON.parse(response);

                    var content = "<html><body><div>"+mailBody.originalBody+"</div></body></html>" +draftMessage.body.content
                    var recipients = [];

                    if(mailBody.email_cc && mailBody.email_cc.length>0){

                        _.each(mailBody.email_cc,function (emailId) {
                            
                            recipients.push({
                                "EmailAddress": {
                                    "Address": emailId.trim().toLowerCase(),
                                }
                            })
                        });
                    }

                    var updateDraft = {
                        "body": {
                            "contentType": "html",
                            "content": content
                        },
                        "ccRecipients":recipients
                    }

                    officeOutlook.updateDraftMSGraphAPI(newToken.access_token,draftMessage.id,updateDraft,function (err,updateRes) {

                        officeOutlook.sendDraftMSGraphAPI(newToken.access_token,draftMessage.id,function (errSend,sent) {
                            if(!errSend && response){

                                setTimeout(function(){
                                    updateOutlookMailsMSGraphAPI(newToken.access_token,user);
                                },2000)

                                res.send({
                                    "SuccessCode": 1,
                                    "Message": errorMessagesObj.getMessage("SEND_EMAIL_SUCCESS"),
                                    "ErrorCode": 0,
                                    "Data": {}
                                });
                            } else {
                                logger.info('Error send Email ',error,response);
                                res.send({
                                    "SuccessCode": 0,
                                    "Message": errorMessagesObj.getMessage("SEND_EMAIL_FAILED"),
                                    "ErrorCode": 1,
                                    "Data": {}
                                });
                            }
                        })
                    })
                } else {
                    logger.info('Error send Email ',error,response);
                    res.send({
                        "SuccessCode": 0,
                        "Message": errorMessagesObj.getMessage("SEND_EMAIL_FAILED"),
                        "ErrorCode": 1,
                        "Data": {}
                    });
                }
            });
        }
    });
};

function updateOutlookMailsMSGraphAPI(refreshToken,user) {
    officeOutlook.getEmailsMSGraphAPI(refreshToken,user,function (mails) {
        officeOutlook.getMailsAsInteractions(mails, user, function (interactions, emailIdArr, referenceIds) {
            if(interactions && interactions.length>0){
                officeOutlook.mapInteractionsEmailIdToRelatasUsers(interactions,emailIdArr,function (mappedInteractions) {

                    if(mappedInteractions && mappedInteractions.length>0){
                        interactionObj.updateEmailInteractions(user,mappedInteractions,referenceIds,function(isSuccess){

                            if(isSuccess){
                                logger.info("Emails successfully saved for -",user.emailId)
                            } else {
                                logger.info("Emails unsuccessfully saved for -",user.emailId)
                            }
                        });

                        //Update these interactions's 2nd party as contacts if they don't already exist.
                        addInteractedContacts(user,mappedInteractions);
                    }
                });
            }
        });
    });
}

function sendNewOutlookMail(refreshToken,mailBody,user,res) {

    var recipients = []
    if(mailBody.email_cc && mailBody.email_cc.length>0){

        _.each(mailBody.email_cc,function (emailId) {
            recipients.push({
                "EmailAddress": {
                    "Address": emailId
                }
            })
        });
    }
    
    var mail = {
        "Message": {
            "Subject": mailBody.subject,
            "Body": {
                "ContentType": "html",
                "Content": mailBody.message
            },
            "ToRecipients": [
                {
                    "EmailAddress": {
                        "Address": mailBody.receiverEmailId
                    }
                }
            ],
            "ccRecipients":recipients
        },
        "SaveToSentItems": "true"
    };

    officeOutlook.sendEmailMSGraphAPI(refreshToken,mail,function (error,response) {

        if(!error && response){

            setTimeout(function(){
                updateOutlookMailsMSGraphAPI(refreshToken,user);
            },2000)

            res.send({
                "SuccessCode": 1,
                "Message": errorMessagesObj.getMessage("SEND_EMAIL_SUCCESS"),
                "ErrorCode": 0,
                "Data": {}
            });
        } else {
            logger.info('Error send Email ',error,response);
            res.send({
                "SuccessCode": 0,
                "Message": errorMessagesObj.getMessage("SEND_EMAIL_FAILED"),
                "ErrorCode": 1,
                "Data": {}
            });
        }
    });
}

function addInteractedContacts(userProfile,mappedInteractions) {

    var interactedContacts = [];

    _.each(mappedInteractions,function (interaction) {

        var personName;
        if(interaction.firstName && interaction.lastName){
            personName = interaction.firstName+' '+interaction.lastName
        } else if(interaction.firstName){
            personName = interaction.firstName
        }else if(interaction.lastName){
            personName = interaction.lastName
        }

        if(interaction.emailId) {
            var contactObj = {
                personId: interaction.userId ? interaction.userId.toString() : null,
                personName: personName,
                personEmailId: interaction.emailId.toLowerCase(),
                birthday: null,
                companyName: interaction.companyName ? interaction.companyName : null,
                designation: null,
                addedDate: new Date(),
                verified: false,
                relatasUser: interaction.userId ? true : false,
                relatasContact: true,
                mobileNumber: interaction.mobileNumber ? interaction.mobileNumber : null,
                location: interaction.location ? interaction.location : null,
                source: 'outlook-email-interactions',
                account: {
                    name: common.fetchCompanyFromEmail(interaction.emailId),
                    selected: false,
                    updatedOn: new Date()
                }
            }
            interactedContacts.push(contactObj)
        }
    });

    var contacts = _.uniq(interactedContacts,'personEmailId');
    var contactsEmailIdArr = _.pluck(interactedContacts,'personEmailId');
    var mobileNumbers = [];

    contactObj.addContactNotExist(common.castToObjectId(userProfile._id.toString()),userProfile.emailId,contacts,mobileNumbers,contactsEmailIdArr,'outlook',false);

}

function sendEmail(dataToSend,user,req,res){

    if(googleCalendar.validateUserGoogleAccount(user)){
        gmailObj.sendEmail(user._id,user.google[0].token,user.google[0].refreshToken,user.google[0].emailId,dataToSend,function(error,response){
            if(error){
                logger.info('Error send Email ',error,response,dataToSend);

                if(res){

                    res.send({
                        "SuccessCode": 0,
                        "Message": errorMessagesObj.getMessage("SEND_EMAIL_FAILED"),
                        "ErrorCode": 1,
                        "Data": {}
                    });
                }

            }
            else{
                if(dataToSend.updateReplied){
                    var obj = {
                        emailId1:dataToSend.receiverEmailId,
                        emailId2:dataToSend.senderEmailId,
                        refId:dataToSend.refId,
                        interactionDate:new Date()
                    };
                    interactionObj.updateInteractionReplied(obj);
                }

                if(res){

                    res.send({
                        "SuccessCode": 1,
                        "Message": errorMessagesObj.getMessage("SEND_EMAIL_SUCCESS"),
                        "ErrorCode": 0,
                        "Data": {}
                    });
                }
            }
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_GOOGLE_ACCOUNTS'},errorMessagesObj.getMessage("SEND_EMAIL_FAILED")));
}

router.post('/messages/send/email/multiple/web',common.isLoggedInUserToGoNext,function(req,res){

    var subject = req.body.subject;
    var message = req.body.message;
    var recipients = req.body.recipients;
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        if(common.checkRequired(recipients) && recipients.length > 0){
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{firstName:1,lastName:1,emailId:1,google:1,officeCalendar:1},function(error,user){
                if(common.checkRequired(user)){
                    if(googleCalendar.validateUserGoogleAccount(user)){
                        googleCalendar.getNewGoogleTokenNew(user.google[0].token,user.google[0].refreshToken,function(token){
                            if(token){
                                mapRecipientsWithProfiles(user,recipients,token,message,subject,req,res)
                            }
                        })
                    }
                    else if(common.checkRequired(user.officeCalendar) && common.checkRequired(user.officeCalendar.access_token) && common.checkRequired(user.officeCalendar.refresh_token)){

                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'EITHER_GOOGLE_OR_OFFICE_ACCOUNT_NOT_FOUND'}));
                }
                else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            });
        }
        else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'RECIPIENTS_NOT_FOUND'}));
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

function mapRecipientsWithProfiles(user,recipients,token,message,subject,req,res){
    var success = 0;
    var failed = 0;

    for(var i=0; i<recipients.length; i++){
        if(validation.validateEmailFlag(recipients[i].receiverEmailId)){
            recipients[i].senderName = user.firstName+' '+user.lastName;
            recipients[i].senderEmailId = user.emailId;
            var name = recipients[i].receiverName || '';
            recipients[i].message = "Hi "+name+"<br>"+message;
            recipients[i].subject = subject;
            success++;
            gmailObj.sendEmailWithToken(user._id,token,user.google[0].emailId,recipients[i],function(){

            })
        }
        else failed++;
    }
    res.send({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data": {
            success:success,
            failed:failed
        }
    });
}

module.exports = router;
