var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var DealsAtRiskManagement = require('../../dataAccess/dealsAtRiskManagement');
var oppManagement = require('../../dataAccess/opportunitiesManagement');
var UserKlass = require('../../databaseSchema/userManagementSchema').User;
var profileManagementClass = require('../../dataAccess/profileManagementClass');
var interactions = require('../../dataAccess/interactionManagement');
var accounts = require('../../dataAccess/accountManagement');
var MessageManagement = require('../../dataAccess/messageManagementV2');
var AccessManagement = require('../../accessControl/accountAccess');
var contactClass = require('../../dataAccess/contactsManagementClass');
var RevenueHierarchy = require('../../dataAccess/revenueHierarchy');
var SecondaryHierarchy = require('../../dataAccess/secondaryHierarchyManagement');

var ProductHierarchy = require('../../dataAccess/productHierarchy');
var ContactManagement = require('../../dataAccess/contactManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var contactClassSupport = require('../../common/contactsSupport');
var OrgHierarchy = require('../../dataAccess/orgHierarchy');
var OppCommitManagement = require('../../dataAccess/oppCommitAccess');
var OppWeightsClass = require('../../dbCache/company.js')

var contactSupportObj = new contactClassSupport();
var accAccessObj = new AccessManagement();
var profileManagementClassObj = new profileManagementClass();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionObj = new interactions();
var accountsObj = new accounts();
var oppObj = new oppManagement();
var dealsAtRiskObj = new DealsAtRiskManagement();
var messageManagementObj = new MessageManagement();
var _ = require("lodash")
var async = require("async")
var statusCodes = errorMessagesObj.getStatusCodes();
var revenueHierarchyObj = new RevenueHierarchy();
var productHierarchyObj = new ProductHierarchy();
var secondaryHierarchyObj = new SecondaryHierarchy();

var orgHierarchyObj = new OrgHierarchy();
var contactManagementObj = new ContactManagement();
var oppCommitObj = new OppCommitManagement();
var oppWeightsObj = new OppWeightsClass();

var redis = require('redis');
var redisClient = redis.createClient();

router.get('/company/user/all/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user)
    var userProfile = common.getUserFromSession(req);
    var timezone = "Asia/Kolkata"
    if(userProfile && userProfile.timezone && userProfile.timezone.name){
        timezone = userProfile.timezone.name;
    }

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        revenueHierarchyObj.getUserHierarchy(common.castToObjectId(userId),function (rErr,revenueHierarchyData) {
            userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, orgHierarchyData){

                if(!revenueHierarchyData){
                    revenueHierarchyData = [user]
                }

                if(!rErr){

                    var revenueHierarchyDataObj = {},
                        orgHierarchyDataObj = {},
                        allHierarchyDataObj = {};
                    if(revenueHierarchyData.length>0){
                        _.each(revenueHierarchyData,function (el) {
                            el.emailId = el.ownerEmailId
                            revenueHierarchyDataObj[el.ownerEmailId] = el;

                            if(el.hierarchyPath){
                                allHierarchyDataObj[el.ownerEmailId] = el.hierarchyPath;
                            }
                        })
                    }

                    if(orgHierarchyData.length>0){
                        _.each(orgHierarchyData,function (el) {
                            orgHierarchyDataObj[el.emailId] = el;
                            if(allHierarchyDataObj[el.emailId]){
                                allHierarchyDataObj[el.emailId] = allHierarchyDataObj[el.emailId]+el.hierarchyPath
                            } else {
                                allHierarchyDataObj[el.emailId] = el.hierarchyPath;
                            }
                        })
                    }

                    _.each(revenueHierarchyData,function (el) {
                        if(orgHierarchyDataObj[el.emailId] && orgHierarchyDataObj[el.emailId].hierarchyPath){
                            el.hierarchyPath = el.hierarchyPath+orgHierarchyDataObj[el.emailId].hierarchyPath
                        }
                    })

                    _.each(orgHierarchyData,function (el) {
                        if(revenueHierarchyDataObj[el.emailId] && revenueHierarchyDataObj[el.emailId].hierarchyPath){
                            el.hierarchyPath = el.hierarchyPath+revenueHierarchyDataObj[el.emailId].hierarchyPath
                        }
                    })

                    var allHierarchyData = [];

                    if(orgHierarchyData && orgHierarchyData.length>0 && revenueHierarchyData && revenueHierarchyData.length>0){
                        allHierarchyData = orgHierarchyData.concat(revenueHierarchyData)
                    } else if(orgHierarchyData && orgHierarchyData.length>0){
                        allHierarchyData = orgHierarchyData;
                    } else if(revenueHierarchyData && revenueHierarchyData.length>0){
                        allHierarchyData = revenueHierarchyData;
                    }

                    allHierarchyData = _.uniq(allHierarchyData,"emailId")

                    userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

                        var listOfMembers = [];

                        _.each(team,function (tm) {
                            _.each(allHierarchyData,function (el) {
                                if(el.emailId == tm.emailId){

                                    el._id = tm._id;
                                    el.emailId = tm.emailId;
                                    el.firstName = tm.firstName;
                                    el.lastName = tm.lastName;
                                    el.designation = tm.designation;
                                }
                            })
                        })

                        _.each(allHierarchyData,function (el) {
                            _.each(allHierarchyData,function (hi) {
                                if(_.includes(hi.hierarchyPath,el._id)){
                                    listOfMembers.push({
                                        userId:el._id,
                                        userEmailId:el.emailId,
                                        teammate:hi
                                    })
                                }
                            })
                        });

                        var data = getChildren(listOfMembers)
                        var tmChildrenObj = data.obj;
                        _.each(allHierarchyData,function (el) {
                            el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                        })

                        if(req.query.forCommits){
                            getTargetsAndAchievements(allHierarchyData,common.castToObjectId(user.companyId),timezone,function (err,targetsAchievementCommits) {
                                var resObj = {
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    listOfMembers:data.array,
                                    "sessionHierarchy":req.session.hierarchy,
                                    "Data": targetsAchievementCommits,
                                    companyMembers:team
                                };

                                res.send(resObj)
                            });

                        } else {

                            var resObj = {
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                listOfMembers:data.array,
                                "Data": allHierarchyData,
                                companyMembers:team
                            };

                            res.send(resObj)
                        }

                        // var resObj = {
                        //     "SuccessCode": 1,
                        //     "Message": "",
                        //     "ErrorCode": 0,
                        //     "Data": allHierarchyData,
                        //     "companyMembers":team,
                        //     "listOfMembers":data.array
                        // };
                        // res.send(resObj)
                    })
                } else {
                    res.send({
                        "SuccessCode": 0,
                        "Message": "",
                        "ErrorCode": 1,
                        "Data": [],
                        "companyMembers":[],
                        "listOfMembers":[]
                    })
                }
            });
        })
    });
});


// router.get('/company/access/control/update/user/opportunities', common.isLoggedInUserToGoNext, function(req, res){
//     var userId = common.getUserId(req.user)
//     var userProfile = common.getUserFromSession(req);
//     var timezone = "Asia/Kolkata"
//     if(userProfile && userProfile.timezone && userProfile.timezone.name){
//         timezone = userProfile.timezone.name;
//     }
//
//     common.getProfileOrStoreProfileInSession(userId,req,function(user){
//         revenueHierarchyObj.getUserHierarchy(common.castToObjectId(userId),function (rErr,revenueHierarchyData) {
//             userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, orgHierarchyData){
//
//                 if(!revenueHierarchyData){
//                     revenueHierarchyData = [user]
//                 }
//
//                 if(!rErr){
//
//                     var revenueHierarchyDataObj = {},
//                         orgHierarchyDataObj = {},
//                         allHierarchyDataObj = {};
//                     if(revenueHierarchyData.length>0){
//                         _.each(revenueHierarchyData,function (el) {
//                             el.emailId = el.ownerEmailId
//                             revenueHierarchyDataObj[el.ownerEmailId] = el;
//
//                             if(el.hierarchyPath){
//                                 allHierarchyDataObj[el.ownerEmailId] = el.hierarchyPath;
//                             }
//                         })
//                     }
//
//                     if(orgHierarchyData.length>0){
//                         _.each(orgHierarchyData,function (el) {
//                             orgHierarchyDataObj[el.emailId] = el;
//                             if(allHierarchyDataObj[el.emailId]){
//                                 allHierarchyDataObj[el.emailId] = allHierarchyDataObj[el.emailId]+el.hierarchyPath
//                             } else {
//                                 allHierarchyDataObj[el.emailId] = el.hierarchyPath;
//                             }
//                         })
//                     }
//
//                     _.each(revenueHierarchyData,function (el) {
//                         if(orgHierarchyDataObj[el.emailId] && orgHierarchyDataObj[el.emailId].hierarchyPath){
//                             el.hierarchyPath = el.hierarchyPath+orgHierarchyDataObj[el.emailId].hierarchyPath
//                         }
//                     })
//
//                     _.each(orgHierarchyData,function (el) {
//                         if(revenueHierarchyDataObj[el.emailId] && revenueHierarchyDataObj[el.emailId].hierarchyPath){
//                             el.hierarchyPath = el.hierarchyPath+revenueHierarchyDataObj[el.emailId].hierarchyPath
//                         }
//                     })
//
//                     var allHierarchyData = [];
//
//                     if(orgHierarchyData && orgHierarchyData.length>0 && revenueHierarchyData && revenueHierarchyData.length>0){
//                         allHierarchyData = orgHierarchyData.concat(revenueHierarchyData)
//                     } else if(orgHierarchyData && orgHierarchyData.length>0){
//                         allHierarchyData = orgHierarchyData;
//                     } else if(revenueHierarchyData && revenueHierarchyData.length>0){
//                         allHierarchyData = revenueHierarchyData;
//                     }
//
//                     allHierarchyData = _.uniq(allHierarchyData,"emailId")
//
//                     userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {
//
//                         var listOfMembers = [];
//
//                         _.each(team,function (tm) {
//                             _.each(allHierarchyData,function (el) {
//                                 if(el.emailId == tm.emailId){
//
//                                     el._id = tm._id;
//                                     el.emailId = tm.emailId;
//                                     el.firstName = tm.firstName;
//                                     el.lastName = tm.lastName;
//                                     el.designation = tm.designation;
//                                 }
//                             })
//                         })
//
//                         _.each(allHierarchyData,function (el) {
//                             _.each(allHierarchyData,function (hi) {
//                                 if(_.includes(hi.hierarchyPath,el._id)){
//                                     listOfMembers.push({
//                                         userId:el._id,
//                                         userEmailId:el.emailId,
//                                         teammate:hi
//                                     })
//                                 }
//                             })
//                         });
//
//                         var data = getChildren(listOfMembers)
//                         var tmChildrenObj = data.obj;
//                         _.each(allHierarchyData,function (el) {
//                             el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
//                         })
//
//                         if(req.query.forCommits){
//                             getTargetsAndAchievements(allHierarchyData,common.castToObjectId(user.companyId),timezone,function (err,targetsAchievementCommits) {
//                                 var resObj = {
//                                     "SuccessCode": 1,
//                                     "Message": "",
//                                     "ErrorCode": 0,
//                                     listOfMembers:data.array,
//                                     "sessionHierarchy":req.session.hierarchy,
//                                     "Data": targetsAchievementCommits,
//                                     companyMembers:team
//                                 };
//
//                                 res.send(resObj)
//                             });
//
//                         } else {
//
//                             var resObj = {
//                                 "SuccessCode": 1,
//                                 "Message": "",
//                                 "ErrorCode": 0,
//                                 listOfMembers:data.array,
//                                 "Data": allHierarchyData,
//                                 companyMembers:team
//                             };
//
//                             res.send(resObj)
//                         }
//
//                         // var resObj = {
//                         //     "SuccessCode": 1,
//                         //     "Message": "",
//                         //     "ErrorCode": 0,
//                         //     "Data": allHierarchyData,
//                         //     "companyMembers":team,
//                         //     "listOfMembers":data.array
//                         // };
//                         // res.send(resObj)
//                     })
//                 } else {
//                     res.send({
//                         "SuccessCode": 0,
//                         "Message": "",
//                         "ErrorCode": 1,
//                         "Data": [],
//                         "companyMembers":[],
//                         "listOfMembers":[]
//                     })
//                 }
//             });
//         })
//     });
// });