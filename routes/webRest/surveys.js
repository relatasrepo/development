/**
* Created by naveen on 1/27/16.
*/

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');

var userManagement = require('../../dataAccess/userManagementDataAccess');
var commonUtility = require('../../common/commonUtility');
var relationshipCartoon = require('../../dataAccess/userManagementDataAccess');
//var relationshipCartoon = require('../../dataAccess/relationshipCartoon');
var contactClass = require('../../dataAccess/contactsManagementClass');
var contactClassSupport = require('../../common/contactsSupport');
var appCredentials = require('../../config/relatasConfiguration');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var ipLocatorJS = require('../../common/ipLocator');
var Gmail = require('../../common/gmail');
var fullContact = require('../../common/fullContact');

var gmailObj = new Gmail();
var ipLocatorObj = new ipLocatorJS();
var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var contactSupportObj = new contactClassSupport();
var appCredential = new appCredentials();
var meetingClassObj = new meetingManagement();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var relationshipCartoonObj = new relationshipCartoon();

var statusCodes = errorMessagesObj.getStatusCodes();
var domain = appCredential.getDomain();

router.post('/relationship/cartoon/data',function(req, res){

    console.log("Do we come here");
    console.log(JSON.stringify(req));

    relationshipCartoonObj.save(data,function(err,isSuccess){
            if(err){
                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
            }
            else if(isSuccess){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{}
                });
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
        })

});



module.exports = router;