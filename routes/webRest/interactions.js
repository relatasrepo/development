
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var meetingSupportClass = require('../../common/meetingSupportClass');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var interactions = require('../../dataAccess/interactionManagement');
var winstonLog = require('../../common/winstonLog');
var googleCalendarAPI = require('../../common/googleCalendar');
var contactClass = require('../../dataAccess/contactsManagementClass');
var contactClassSupport = require('../../common/contactsSupport');
var fullContact = require('../../common/fullContact');
var facebookM = require('../../common/facebook');
var twitterM = require('../../common/twitter');
var linkedinM = require('../../common/linkedin');
var calendarPasswordManagement = require('../../dataAccess/calendarPasswordManagement');

var calendarPasswordManagementObj = new calendarPasswordManagement();
var facebookObj = new facebookM();
var twitterObj = new twitterM();
var linkedinObj = new linkedinM();
var emailSenderObj = new emailSender();
var fullContactObj = new fullContact();
var contactSupportObj = new contactClassSupport();
var errorObj = new errorClass();
var common = new commonUtility();
var interactionObj = new interactions();
var validation = new validations();
var userManagementObj = new userManagement();
var errorMessagesObj = new errorMessages();
var googleCalendar = new googleCalendarAPI();
var logLib = new winstonLog();
var contactObj = new contactClass();

var logger = logLib.getWinston();
var statusCodes = errorMessagesObj.getStatusCodes();
var _ = require("lodash");

router.get('/interactions',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('secondSeries/interactions',{isLoggedIn : common.isLoggedInUserBoolean(req)})
});


router.get('/account/interactions',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('secondSeries/interactionsV2',{isLoggedIn : common.isLoggedInUserBoolean(req)})
});

router.get('/interactions/relation/strength',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,companyName:1,timezone:1},function(error,user){
        if(common.checkRequired(user)){
            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';

            var dateMin = moment().tz(timezone);
            dateMin.date(dateMin.date() - 90);
            dateMin = dateMin.format();
            var dateMax = moment().tz(timezone).format();
            interactionObj.calculateRelationshipStrength(user._id,common.castToObjectId("54993a7e89ce034a0b99f374"),dateMin,dateMax,function(result){
                res.send(result)
            })
        }
    });
});

router.get('/interactions/with/:idType/:id',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    var id = req.params.id;
    var idType = req.params.idType;

    if(common.checkRequired(id) && common.checkRequired(idType) && (idType == 'userId' || idType == 'emailId')){
        res.render('web/interactions');
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}));
});

router.get('/interactions/timeline/:participantEmailId/files',common.isLoggedInUserToGoNext,function(req,res){
    var participantEmailId = req.params.participantEmailId;
    var userId = common.getUserId(req.user);
    if(common.checkRequired(participantEmailId)){
        var timezone = 'UTC';
        if(common.checkRequired(req.query.timezone)){
            timezone = req.query.timezone;
        }
        var dateMin = moment().tz(timezone).format();
        interactionObj.documentsSharedWithContact(common.castToObjectId(userId),participantEmailId,5,dateMin,function(shareDocInteractions){
            if(shareDocInteractions.length > 0){

                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "timezone":timezone,
                    "Data": shareDocInteractions
                });
            }
            else{
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "timezone":timezone,
                    "Data": []
                });
            }
        })
    }
    else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'EMAIL_ID_NOT_FOUND_IN_REQUEST'}));
});

router.get('/interactions/timeline/:participantEmailId/interactions', common.isLoggedInUserOrMobile, function (req, res) {

    var participantEmailId = req.params.participantEmailId;
    var userId = common.getUserIdFromMobileOrWeb(req);
    var mobileNumber = req.query.mobileNumber

    if (common.checkRequired(participantEmailId)) {
        common.remaindMeAfterDays(function(remaindDays){
            var timezone = 'UTC';
            if(common.checkRequired(req.query.timezone)){
                timezone = req.query.timezone;
            }

            if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                timezone = req.session.profile.timezone.name
            }

            var serviceLogin = req.session && req.session.profile?req.session.profile.serviceLogin:null

            var dateMin = moment().tz(timezone).format();

            interactionObj.getInteractionsByDateTimeline(common.castToObjectId(userId), participantEmailId, true, 5, dateMin,mobileNumber,serviceLogin,function (interactions) {

                if (interactions.length > 0) {
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "remaindDays":remaindDays,
                        "timezone":timezone,
                        "userId":userId,
                        "Data": interactions
                    });
                }
                else {
                    res.send({
                        "SuccessCode": 0,
                        "Message": "",
                        "ErrorCode": 1,
                        "remaindDays":remaindDays,
                        "timezone":timezone,
                        "userId":userId,
                        "Data": []
                    });
                }
            });

        })
    }
    else res.send(errorObj.generateErrorResponse({
        status: statusCodes.INVALID_REQUEST_CODE,
        key: 'EMAIL_ID_NOT_FOUND_IN_REQUEST'
    }));
});

router.get('/interactions/timeline/:participantEmailId/tasks', common.isLoggedInUserToGoNext, function (req, res) {
    var participantEmailId = req.params.participantEmailId;
    var userId = common.getUserId(req.user);
    if (common.checkRequired(participantEmailId)) {
        var timezone = 'UTC';
        if(common.checkRequired(req.query.timezone)){
            timezone = req.query.timezone;
        }
        var dateMin = moment().tz(timezone).format();
        interactionObj.getInteractionsByDateTimeLineTasks(common.castToObjectId(userId), participantEmailId, true, 5,dateMin, function (interactions) {
            if (interactions.length > 0) {
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "timezone":timezone,
                    "Data": interactions
                });
            }
            else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "timezone":timezone,
                    "Data": {}
                });
            }
        });
    }
    else res.send(errorObj.generateErrorResponse({
        status: statusCodes.INVALID_REQUEST_CODE,
        key: 'EMAIL_ID_NOT_FOUND_IN_REQUEST'
    }));
});

router.get('/interactions/timeline/:participantEmailId/social', common.isLoggedInUserToGoNext, function (req, res) {
    var participantEmailId = req.params.participantEmailId;
    var userId = common.getUserId(req.user);
    if (common.checkRequired(participantEmailId)) {
        var timezone = 'UTC';
        if(common.checkRequired(req.query.timezone)){
            timezone = req.query.timezone;
        }

        if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
            timezone = req.session.profile.timezone.name
        }

        var dateMin = moment().tz(timezone).format();
        interactionObj.getInteractionsByDateTimeLineSocialUpdates(common.castToObjectId(userId), participantEmailId, true, 5,dateMin, function (interactions) {
            if (interactions.length > 0) {
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "timezone":timezone,
                    "userId":userId,
                    "Data": interactions
                });
            }
            else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "timezone":timezone,
                    "userId":userId,
                    "Data": {}
                });
            }
        });
    }
    else res.send(errorObj.generateErrorResponse({
        status: statusCodes.INVALID_REQUEST_CODE,
        key: 'EMAIL_ID_NOT_FOUND_IN_REQUEST'
    }));
});

router.get('/interactions/contacts/landing/counts/total',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user) {
        if (common.checkRequired(user)) {

            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';

            var dateMin = moment().tz(timezone);
            var dateMax = moment().tz(timezone);
            dateMin.date(dateMin.date() - 7)
            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)

            interactionObj.getInteractionsCountByCompanyLocationTypeDate(userId,dateMin.format(),dateMax.format(),[],[],function(data){
                if(common.checkRequired(data.interactionsByLocation) && data.interactionsByLocation.length > 0){
                   req.session.interactionsByLocation = data.interactionsByLocation;

                }
                if(common.checkRequired(data.interactionsByCompany) && data.interactionsByCompany.length > 0){
                    req.session.interactionsByCompany = data.interactionsByCompany;

                }

                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": data
                });
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });
});

router.post('/interactions/contacts/landing/counts/total/click',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
    if(common.checkRequired(req.body.type) && (req.body.type == 'location' || req.body.type == 'company')){
        if(common.checkRequired(req.body.name)){
            switch(req.body.type){
                case 'location':if(common.checkRequired(req.session.interactionsByLocation) && req.session.interactionsByLocation.length > 0){
                    var data1 = req.session.interactionsByLocation;
                    var emails1;
                    for(var i=0; i<data1.length; i++){

                        if(data1[i]._id == req.body.name){
                            emails1 = data1[i].emailIdArr;
                        }
                    }

                    if(common.checkRequired(emails1) && emails1.length > 0){
                        contactObj.getContactsInEmailIdArrUserId(common.castToObjectId(userId),emails1,function(error,contacts1){
                            if(error){
                               res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                            }
                            else{
                                var total = contacts1.length;
                                var results = contacts1.splice(skip,limit);

                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":{
                                        total:total,
                                        skipped:skip,
                                        limit:limit,
                                        returned:results.length,
                                        contacts:results
                                    }
                                });
                            }
                        })
                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                    break;
                case 'company':if(common.checkRequired(req.session.interactionsByLocation) && req.session.interactionsByLocation.length > 0){
                    var data2 = req.session.interactionsByLocation;
                    var emails2;
                    for(var j=0; j<data2.length; j++){
                        if(data2[i]._id == req.body.name){
                            emails2 = data2[j].emailIdArr;
                        }
                    }
                    if(common.checkRequired(emails2) && emails2.length > 0){
                        contactObj.getContactsInEmailIdArrUserId(common.castToObjectId(userId),emails1,function(error,contacts1){
                            if(error){
                                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                            }
                            else{
                                var total = contacts1.length;
                                var results = contacts1.splice(skip,limit);

                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":{
                                        total:total,
                                        skipped:skip,
                                        limit:limit,
                                        returned:results.length,
                                        contacts:results
                                    }
                                });
                            }
                        })
                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                }else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
                break;
            }
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_DATA_RECEIVED'}));
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TYPE'}));
});

router.get('/interactions/get/days/summary/:idType/:id', common.isLoggedInUserToGoNext,function(req,res){

    var id = req.params.id;
    var idType = req.params.idType;
    var userId = common.getUserId(req.user);
    var mobileNumber = req.query.mobileNumber
    
    common.getProfileOrStoreProfileInSession(userId,req,function(user){

        if(common.checkRequired(user)){
            common.remaindMeAfterDays(function(remaindDays){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';
                var dateMin = moment().tz(timezone);
                var dateMax = moment().tz(timezone);

                dateMin.date(dateMin.date() - 7);
                dateMin.hours(0);
                dateMin.minutes(0);
                dateMin.seconds(0);
                var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 200;

                interactionObj.totalInteractionsIndividualSummaryDate(common.castToObjectId(userId), id, idType == 'emailId',null,null,dateMax,mobileNumber,user.serviceLogin,function (interactions) {

                    var total = interactions.length;
                    var results = interactions.splice(skip,limit);
                    var dataSet = {
                        Data:results,
                        total: total,
                        skipped: skip,
                        limit: limit,
                        returned: results.length
                    };
                    res.send({
                        "SuccessCode": results.length > 0 ? 1 : 0,
                        "Message": results.length > 0 ? "" : errorMessagesObj.getMessage("NO_INTERACTIONS_FOUND_PAST_SEVEN_DAY"),
                        "ErrorCode": results.length > 0 ? 0 : 1,
                        "Data": dataSet,
                        "timezone":timezone,
                        "remaindDays":remaindDays
                    });
                });

            })
        }
        else res.send(errorObj.generateErrorResponse({
            status: statusCodes.PROFILE_ERROR_CODE,
            key: 'USER_PROFILE_NOT_FOUND'
        }));
    });
});

router.get('/interactions/get/all/summary/:idType/:id', common.isLoggedInUserToGoNext, function (req, res) {

    var id = req.params.id;
    var idType = req.params.idType;
    var userId = common.getUserId(req.user);
    var social = req.query;
    var mobileNumber;
    if(req.query.mobileNumber == 'none' || req.query.mobileNumber == 'null' || req.query.mobileNumber == null){

    }
    else mobileNumber = req.query.mobileNumber;

    //req.session.fullContactSession = null;
    if (common.checkRequired(id) && common.checkRequired(idType) && (idType == 'userId' || idType == 'emailId')) {
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{facebook:1,emailId:1,mobileNumber:1,designation:1,companyName:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,location:1},function(error,user){
            if(error){
                logger.info('Error in findUserProfileByIdWithCustomFields():Interactions router  user: '+userId ,error);
            }

            if(common.checkRequired(user)){
                //facebookObj.importFacebookFeedToInteractions(user.facebook,user);
                if (common.checkRequired(user.facebook.id))
                    facebookObj.FacebookFeedToInteractions(user.facebook, user, function (isSuccess) {
                        if (!isSuccess) {
                            userManagementObj.updateFacebook(user._id, null, null, null, null, function (error, isUpdated, message) {
                            });
                        } else {
                        }
                    });

            }

            else callback(null);
        });

        contactSupportObj.getContactSocialDetails(userId,id,req,function(obj){

            if(!common.checkRequired(mobileNumber) && common.checkRequired(obj) && common.checkRequired(obj.mobileNumber)){
                mobileNumber = obj.mobileNumber;
            }

            interactionObj.totalInteractionsIndividualSummary(userId, id, idType == 'emailId', true,mobileNumber, function (interactions) {
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": interactions
                });
            });
        });
    }
    else res.send(errorObj.generateErrorResponse({
        status: statusCodes.INVALID_REQUEST_CODE,
        key: 'INVALID_URL_PARAMETER'
    }));
});

// router.get('/social/feed/facebook/feed/update/web',common.isLoggedInUserToGoNext,function(req,res){
//     console.log("This");
//     var userId = common.getUserId(req.user);

//     if(common.checkRequired(req.query.postId)){
//         userManagementObj.findUserProfileByIdWithCustomFields(userId,{facebook:1},function(error,user){
//             if(common.checkRequired(user)){
//                 switch (req.query.action){
//                     case 'like': facebookObj.likeFacebookFeed(user.facebook,req.query.postId,function(error,isSuccess){
//                         if(error){
//                             if(error == 'no_account'){
//                                 res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_FACEBOOK_ACCOUNT'}));
//                             }
//                             else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'FACEBOOK_ERROR_LIKE'}));
//                         }
//                         else if(isSuccess){
//                             res.send({
//                                 "SuccessCode": 1,
//                                 "Message": "",
//                                 "ErrorCode": 0,
//                                 "Data": {}
//                             });
//                         }else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
//                     });
//                         break;
//                     case 'comment':
//                         if(common.checkRequired(req.query.comment)){
//                             facebookObj.commentOnFacebookFeed(user.facebook,req.query.postId,req.query.comment,function(error,isSuccess){
//                                 if(error){
//                                     if(error == 'no_account'){
//                                         res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_FACEBOOK_ACCOUNT'}));
//                                     }
//                                     else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'FACEBOOK_ERROR_COMMENT'}));
//                                 }
//                                 else if(isSuccess){
//                                     res.send({
//                                         "SuccessCode": 1,
//                                         "Message": "",
//                                         "ErrorCode": 0,
//                                         "Data": {}
//                                     });
//                                 }else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}));
//                             });
//                         }
//                         else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'FACEBOOK_COMMENT_FOUND'}));
//                         break;
//                     default : res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_ACTION'}));
//                 }
//             }
//             else{
//                 if(error)
//                     res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
//                 else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'USER_PROFILE_NOT_FOUND'}));
//             }
//         })
//     }
//     else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'FACEBOOK_POST_ID_FOUND'}));
// });

router.put('/social/feed/facebook/like',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.query.objectId)){
        facebookObj.likeFBObject(userId,req.query.objectId,function(error,isSuccess){
            if(error)
                res.send({
                    "SuccessCode": 0,
                    "error": error
                });
            else if(isSuccess===true)
                res.send({
                    "SuccessCode": 1
                });
            else
                res.send({
                    "SuccessCode": 0
                });
        })
    }    
})

router.put('/social/feed/facebook/comment',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.query.objectId) && common.checkRequired(req.query.msg)){
        facebookObj.commentOnFBObject(userId,req.query.objectId,req.query.msg,function(error,isSuccess){
            if(error)
                res.send({
                    "SuccessCode": 0,
                    "error": error
                });
            else if(isSuccess===true)
                res.send({
                    "SuccessCode": 1
                });
            else
                res.send({
                    "SuccessCode": 0
                });
        })
    }    
})

router.put('/social/feed/facebook/share',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.query.objectId)){
        facebookObj.shareFBObject(userId,req.query.objectId,function(error,isSuccess){
            if(error)
                res.send({
                    "SuccessCode": 0,
                    "error": error
                });
            else if(isSuccess===true)
                res.send({
                    "SuccessCode": 1
                });
            else
                res.send({
                    "SuccessCode": 0
                });
        })
    }    
})

router.put('/social/feed/linkedin/share',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.query.url)){
        res.send(req.query.url);
        linkedinObj.shareOnLinkedIn(userId,req.query.url,function(error,isSuccess){
            if(error)
                res.send({
                    "SuccessCode": 0,
                    "error": error
                });
            else if(isSuccess===true)
                res.send({
                    "SuccessCode": 1
                });
            else
                res.send({
                    "SuccessCode": 0
                });
        })
    }
})

router.get('/social/feed/twitter/tweet/update/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.query.postId)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{twitter:1,emailId:1},function(error,user){
            if(common.checkRequired(user)){
                switch (req.query.action){
                    case 'favorite': twitterObj.favoriteTwitterFeed(user.twitter,req.query.postId,function(error,isSuccess,errObj){
                        if(error){
                            if(error == 'no_account'){
                                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_TWITTER_ACCOUNT'},errorMessagesObj.getMessage("TWITTER_ERROR_FAVORITE")));
                            }
                            else{
                                var message = errorMessagesObj.getMessage("TWITTER_ERROR_FAVORITE");
                                if(common.checkRequired(errObj) && common.checkRequired(errObj.message)){
                                    message = errObj.message;
                                }
                                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'TWITTER_ERROR_FAVORITE'},message));
                            }
                        }
                        else if(isSuccess){
                            
                            interactionObj.updateInteractionOpenedTaskBulk(req.query.postId,true,user.emailId,req.query.emailId,true,function(isSuccess){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": errorMessagesObj.getMessage("TWITTER_FAVORITE_SUCCESS"),
                                    "ErrorCode": 0,
                                    "Data": {}
                                });
                            });
                        }else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'},errorMessagesObj.getMessage("TWITTER_ERROR_FAVORITE")));
                    });
                        break;
                    case 're-tweet':twitterObj.reTweetFeed(user.twitter,req.query.postId,function(error,isSuccess,errObj){
                        if(error){
                            if(error == 'no_account'){
                                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_TWITTER_ACCOUNT'},errorMessagesObj.getMessage("TWITTER_ERROR_RE_TWEET")));
                            }
                            else{
                                var message = false;
                                if(common.checkRequired(errObj) && common.checkRequired(errObj.message)){
                                    message = errObj.message;
                                }
                                res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'TWITTER_ERROR_RE_TWEET'},errorMessagesObj.getMessage("TWITTER_ERROR_RE_TWEET")));
                            }
                        }
                        else if(isSuccess){

                            userManagementObj.getTwoUsersByEmail(user.emailId,req.query.emailId,function (err,users,exists) {
                                var tweetObj = [];

                                _.each(users,function (u) {
                                    if(u.emailId == user.emailId){
                                        tweetObj.push(getInteractionObject(u,'sender','twitter','re-tweet',user.emailId,req.query.postId,'relatas',req.query.status,false));
                                    } else if(u.emailId == req.query.emailId) {
                                        tweetObj.push(getInteractionObject(u,'receiver','twitter','re-tweet',req.query.emailId,req.query.postId,'relatas',req.query.status,false));
                                    } else{
                                        //non Relatas User
                                        tweetObj.push(getInteractionObject(null,'receiver','twitter','re-tweet',req.query.emailId,req.query.postId,'relatas',req.query.status,true));
                                    }
                                });

                                interactionObj.addTwitterInteractions(common.castToObjectId(tweetObj[0].userId),tweetObj,function (response) {
                                    logger.info("added Twitter Interaction for - " , tweetObj[0].userId)
                                    if(tweetObj.length>1){
                                        interactionObj.addTwitterInteractions(common.castToObjectId(tweetObj[1].userId),tweetObj,function (response) {
                                            logger.info("added Twitter Interaction for -  ", tweetObj[1].userId)
                                        });
                                    }
                                });
                            });

                            interactionObj.updateInteractionOpenedTaskBulk(req.query.postId,true,user.emailId,req.query.emailId,true,function(isSuccess){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": errorMessagesObj.getMessage("TWITTER_RE_TWEET_SUCCESS"),
                                    "ErrorCode": 0,
                                    "Data": {}
                                });
                            });
                        }else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'},errorMessagesObj.getMessage("TWITTER_ERROR_RE_TWEET")));
                    });
                        break;
                    case 'reply':if(common.checkRequired(req.query.status)){
                        twitterObj.replyToTweet(user.twitter,req.query.postId,req.query.status,function(error,isSuccess,errObj){
                            if(error){
                                if(error == 'no_account'){
                                    res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'NO_TWITTER_ACCOUNT'},errorMessagesObj.getMessage("TWITTER_ERROR_REPLAY")));
                                }
                                else{
                                    var message = false;
                                    if(common.checkRequired(errObj) && common.checkRequired(errObj.message)){
                                        message = errObj.message;
                                    }
                                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'TWITTER_ERROR_REPLAY'},errorMessagesObj.getMessage("TWITTER_ERROR_REPLAY")));
                                }
                            }
                            else if(isSuccess){

                                userManagementObj.getTwoUsersByEmail(user.emailId,req.query.emailId,function (err,users,exists) {
                                    var tweetObj = [];
                                    
                                    _.each(users,function (u) {
                                        if(u.emailId == user.emailId){
                                            tweetObj.push(getInteractionObject(u,'sender','twitter','reply',user.emailId,req.query.postId,'relatas',req.query.status,false));
                                        } else if(u.emailId == req.query.emailId) {
                                            tweetObj.push(getInteractionObject(u,'receiver','twitter','reply',req.query.emailId,req.query.postId,'relatas',req.query.status,false));
                                        } else{
                                            //non Relatas User
                                            tweetObj.push(getInteractionObject(null,'receiver','twitter','reply',req.query.emailId,req.query.postId,'relatas',req.query.status,true));
                                        }
                                    });

                                    interactionObj.addTwitterInteractions(common.castToObjectId(tweetObj[0].userId),tweetObj,function (response) {
                                        logger.info("added Twitter Interaction for - " , tweetObj[0].userId)
                                        if(tweetObj.length>1){
                                            interactionObj.addTwitterInteractions(common.castToObjectId(tweetObj[1].userId),tweetObj,function (response) {
                                                logger.info("added Twitter Interaction for -  ", tweetObj[1].userId)
                                            });
                                        }
                                    });
                                });

                                interactionObj.updateInteractionOpenedTaskBulk(req.query.postId,true,user.emailId,req.query.emailId,true,function(isSuccess){
                                    res.send({
                                        "SuccessCode": 1,
                                        "Message": errorMessagesObj.getMessage("TWITTER_REPLAY_SUCCESS"),
                                        "ErrorCode": 0,
                                        "Data": {}
                                    });
                                })
                            }else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'},errorMessagesObj.getMessage("TWITTER_ERROR_REPLAY")));
                        });
                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'TWEET_STATUS_NOT_FOUND'}));
                        break;
                    default : res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_ACTION'}));
                }
            }
            else{
                if(error)
                    res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
                else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'USER_PROFILE_NOT_FOUND'}));
            }
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'TWEET_ID_NOT_FOUND'}));
});

router.get('/interactions/twitter/fetch',common.isLoggedInUserToGoNext,function(req,res){
    userManagementObj.findUserProfileByIdWithCustomFields(common.getUserId(req.user),{emailId:1,firstName:1,lastName:1,companyName:1,designation:1,mobileNumber:1,skypeId:1,location:1,twitter:1},function(err,user){
        if(user){
            twitterObj.twitterFeedToInteractions(user.twitter,user,function(isSuccess){
                res.send(isSuccess);
            });
        }
        else{
            res.send(false);
        }
    });
});

router.post('/interactions/update/interaction/ignore',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.body.interactionId)){
        interactionObj.updateInteractionToIgnore(common.castToObjectId(userId),req.body.interactionId,req.body.isCalPassReq == true,function(isSuccess){
            if(req.body.isCalPassReq){
                var resObj = {requestId:req.body.requestId,accepted:false};
                calendarPasswordManagementObj.updateCalendarRequest(resObj,function(error,isUpdated){

                })
            }
            res.send({
                "SuccessCode": isSuccess ? 1 : 0,
                "Message": isSuccess ? errorMessagesObj.getMessage("INTERACTION_IGNORE_UPDATE_SUCCESS") : errorMessagesObj.getMessage("INTERACTION_IGNORE_UPDATE_FAILED"),
                "ErrorCode": isSuccess ? 0 : 1,
                "Data": {}
            })
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INTERACTION_ID_NOT_FOUND_IN_REQUEST'},errorMessagesObj.getMessage("INTERACTION_IGNORE_UPDATE_FAILED")));
});

router.get('/track/email/open/:emailId/track/id/:trackId/user/:userId',function(req,res){

    // var ip = common.getClientIp2(req)
    //
    // emailSenderObj.sendUncaughtException('','Track IP address of email',ip,ip)

    if(common.checkRequired(req.params.emailId) && common.checkRequired(req.params.trackId) && common.checkRequired(req.params.userId)){
       interactionObj.updateInteractionOpened(common.castToObjectId(req.params.userId),req.params.emailId,req.params.trackId,function(isSuccess){
           if(isSuccess){
               //emailSenderObj.sendUncaughtException('','Email track updated',JSON.stringify(req.params),'Track info update Success');
           }
           else emailSenderObj.sendUncaughtException('','Email track info update failed',JSON.stringify(req.params),'Track info update failed');
       })
    }
    else{
        emailSenderObj.sendUncaughtException('','Invalid email track info received',JSON.stringify(req.params),'INVALID_EMAIL_TRACK_DATA');
    }
    res.download('./public/images/transparent_1px.gif');
});

function findUserAddedTwitter(user){
    if(common.checkRequired(user) && common.checkRequired(user.twitter) && common.checkRequired(user.twitter.userName)){
        return true;
    }
    else return false;
}

function findUserAddedFacebook(user){
    if(common.checkRequired(user) && common.checkRequired(user.facebook) && common.checkRequired(user.facebook.id)){
        return true;
    }
    else return false;
}

router.get('/interactions/connected/via/linkedin',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var contactEmailId = req.query.emailId;
    if(common.checkRequired(contactEmailId)){
        interactionObj.linkedinConnectedVia(common.castToObjectId(userId),contactEmailId,function(error,result){
            if(result && result.length > 0 && result[0] && result[0].types && result[0].types.length > 0){
                if(result[0].types.indexOf("linkedin") != -1){
                    res.send({
                        "SuccessCode": 0,
                        "Message": "",
                        "ErrorCode": 1,
                        "Data": {connectedVia:{linkedin:true}}
                    });
                }
                else res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": {connectedVia:{linkedin:false}}
                });
            }
            else res.send({
                "SuccessCode": 0,
                "Message": "",
                "ErrorCode": 1,
                "Data": {connectedVia:{linkedin:false}}
            });
        })
    }
    else res.send({
        "SuccessCode": 0,
        "Message": "",
        "ErrorCode": 1,
        "Data": {connectedVia:{linkedin:false}}
    });
});

router.get('/interactions/get/last/interacted/details',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var contactEmailId = req.query.emailId;

    if(common.checkRequired(contactEmailId)){

        interactionObj.getLastInteractedDateByEmailList(common.castToObjectId(userId),[contactEmailId],function(error,result){

            contactObj.getContactsByEmailId(common.castToObjectId(userId),[contactEmailId],function (err,contacts) {

                if(result){
                    
                    _.each(result,function (el) {
                        _.each(contacts,function (co) {
                            
                            if(co.personEmailId === el._id){
                                el.personName = co.personName
                            }
                        });
                    });
                    
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": result
                    });
                }
                else res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": []
                });
            })
        })
    }
    else res.send({
        "SuccessCode": 0,
        "Message": "",
        "ErrorCode": 1,
        "Data": []
    });
});

router.get('/interactions/connected/via/twitter',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var contactEmailId = req.query.emailId;
    if(common.checkRequired(contactEmailId)){
        var query = {$or:[{_id:userId},{emailId:contactEmailId}]};
        userManagementObj.selectUserProfilesCustomQuery(query,{emailId:1,twitter:1},function(error,users){
            if(error){
                sendErrorConnectedVia(req,res)
            }
            else if(users && users.length > 0){
                var loggedInUser = null;
                var cUser = null;
                for(var i=0; i<users.length; i++){
                    if(users[i]._id.toString() == userId){
                        loggedInUser = JSON.parse(JSON.stringify(users[i]));
                    }
                    else cUser = JSON.parse(JSON.stringify(users[i]));
                }
                if(!common.checkRequired(cUser)){
                    cUser = {emailId:contactEmailId,isTwitter:false}
                }
                else cUser.isTwitter = findUserAddedTwitter(cUser);

                loggedInUser.isTwitter = findUserAddedTwitter(loggedInUser);

                if(cUser.isTwitter && loggedInUser.isTwitter){
                    // both added
                    validateTwitterRelation(loggedInUser.twitter,cUser.twitter.userName,req,res)

                }
                else if(cUser.isTwitter && !loggedInUser.isTwitter){

                    // contact user added
                    fullContactObj.getPersonByEmail(null,loggedInUser.emailId,req,function(obj){
                        if(common.checkRequired(obj) && common.checkRequired(obj.twitterUserName)){
                            validateTwitterRelation(cUser.twitter,obj.twitterUserName,req,res);
                        }
                        else sendErrorConnectedVia(req,res)
                    })
                }
                else if(!cUser.isTwitter && loggedInUser.isTwitter){
                    // loggedin user added
                    if(common.checkRequired(req.query.twitterUserName)){
                        validateTwitterRelation(loggedInUser.twitter,req.query.twitterUserName,req,res);
                    }
                    else fullContactObj.getPersonByEmail(null,cUser.emailId,req,function(obj){
                        if(common.checkRequired(obj) && common.checkRequired(obj.twitterUserName)){
                            validateTwitterRelation(loggedInUser.twitter,obj.twitterUserName,req,res);
                        }
                        else sendErrorConnectedVia(req,res)
                    })
                }
                else sendErrorConnectedVia(req,res)
            }
            else sendErrorConnectedVia(req,res)
        })
    }
    else sendErrorConnectedVia(req,res)

});

function validateTwitterRelation(twitter,screenName,req,res){
    twitterObj.getTwitterFriendship(twitter,screenName,function(error,data){
        if(error){
            sendErrorConnectedVia(req,res)
        }
        else if(data && data.length > 0){
            if(data[0].connections && data[0].connections.length > 0 && data[0].connections.indexOf('none') == -1 && (data[0].connections.indexOf('following') != -1 || data[0].connections.indexOf('followed_by') != -1)){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": {connectedVia:{twitter:true}}
                })
            }
            else sendErrorConnectedVia(req,res)
        }
        else sendErrorConnectedVia(req,res)
    })
}

function sendErrorConnectedVia(req,res){
    res.send({
        "SuccessCode": 0,
        "Message": "",
        "ErrorCode": 1,
        "Data": {connectedVia:{twitter:false}}
    })
}

function sendErrorConnectedViaFB(req,res){
    res.send({
        "SuccessCode": 0,
        "Message": "",
        "ErrorCode": 1,
        "Data": {connectedVia:{facebook:false}}
    })
}

router.get('/interactions/connected/via/facebook',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var contactEmailId = req.query.emailId;
    if(common.checkRequired(contactEmailId)){
        var query = {$or:[{_id:userId},{emailId:contactEmailId}]};
        userManagementObj.selectUserProfilesCustomQuery(query,{emailId:1,facebook:1},function(error,users){
            if(error){
                sendErrorConnectedViaFB(req,res)
            }
            else if(users && users.length > 0){
                var loggedInUser = null;
                var cUser = null;
                for(var i=0; i<users.length; i++){
                    if(users[i]._id.toString() == userId){
                        loggedInUser = JSON.parse(JSON.stringify(users[i]));
                    }
                    else cUser = JSON.parse(JSON.stringify(users[i]));
                }
                if(!common.checkRequired(cUser)){
                    cUser = {emailId:contactEmailId,isfacebook:false}
                }
                else cUser.isfacebook = findUserAddedFacebook(cUser);

                loggedInUser.isfacebook = findUserAddedFacebook(loggedInUser);

                if(cUser.isfacebook && loggedInUser.isfacebook){
                    // both added
                    facebookObj.findIsFbFriend(loggedInUser.facebook,cUser.facebook.id,function(data){
                        if(data && data.data && data.data.length > 0){
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data": {connectedVia:{facebook:true}}
                            })
                        }
                        else sendErrorConnectedViaFB(req,res)
                    })
                }
                else sendErrorConnectedViaFB(req,res)
            }
            else sendErrorConnectedViaFB(req,res)
        })
    }
    else sendErrorConnectedViaFB(req,res)
});

router.get('/insights',common.isLoggedInUser,common.checkUserDomain,function(req,res){

    var userId = common.getUserId(req.user)

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render("reportsV2/index",{isLoggedIn : common.isLoggedInUserBoolean(req)});
            //res.render('customer/landing',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/insights/achievements',common.isLoggedInUser,common.checkUserDomain,function(req,res){

    var userId = common.getUserId(req.user)

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render("reportsV2/index",{isLoggedIn : common.isLoggedInUserBoolean(req)});
            //res.render('customer/landing',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/insights/v4',common.isLoggedInUser,common.checkUserDomain,function(req,res){

    var userId = common.getUserId(req.user)

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render("reports/index",{isLoggedIn : common.isLoggedInUserBoolean(req)});
            //res.render('customer/landing',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/insights/v3',common.isLoggedInUser,common.checkUserDomain,function(req,res){

    var userId = common.getUserId(req.user)

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render("insights/insightsV2",{isLoggedIn : common.isLoggedInUserBoolean(req)});
            //res.render('customer/landing',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

function getInteractionObject(userInfo,action,interactionType,subType,emailId,refId,source,title,description,nonRelatas){

    if(!nonRelatas){
        var obj = {
            userId:checkRequired(userInfo._id) ? common.castToObjectId(userInfo._id) : null,
            user_twitter_id:checkRequired(userInfo.twitter.user_twitter_id) ? userInfo.twitter.user_twitter_id : null,
            user_twitter_name:checkRequired(userInfo.twitter.user_twitter_name) ? userInfo.twitter.user_twitter_name : null,
            firstName:checkRequired(userInfo.firstName) ? userInfo.firstName : null,
            lastName:checkRequired(userInfo.lastName) ? userInfo.lastName : null,
            publicProfileUrl:checkRequired(userInfo.publicProfileUrl) ? userInfo.publicProfileUrl : null,
            profilePicUrl:checkRequired(userInfo.profilePicUrl) ? userInfo.profilePicUrl : null,
            emailId:checkRequired(emailId) ? emailId.toLowerCase() : null,
            companyName: checkRequired(emailId) ? fetchCompanyFromEmail(emailId) : null,
            designation:checkRequired(userInfo.designation) ? userInfo.designation : null,
            location:checkRequired(userInfo.location) ? userInfo.location : null,
            mobileNumber:checkRequired(userInfo.mobileNumber) ? userInfo.mobileNumber : null,
            action:action,
            interactionType:interactionType,
            subType:subType,
            refId:refId,
            source:checkRequired(source) ? source : null,
            title:checkRequired(title) ? title : null,
            description:checkRequired(description) ? description : null,
            interactionDate: new Date(),
            endDate: null,
            createdDate:new Date(),
            trackId: null,
            emailContentId: null,
            googleAccountEmailId: null
        };
    } else if(nonRelatas) {
        var obj = {
            userId:null,
            user_twitter_id:checkRequired(userInfo.twitter.user_twitter_id) ? userInfo.twitter.user_twitter_id : null,
            user_twitter_name:checkRequired(userInfo.twitter.user_twitter_name) ? userInfo.twitter.user_twitter_name : null,
            firstName:checkRequired(userInfo.firstName) ? userInfo.firstName : null,
            lastName:checkRequired(userInfo.lastName) ? userInfo.lastName : null,
            publicProfileUrl:checkRequired(userInfo.publicProfileUrl) ? userInfo.publicProfileUrl : null,
            profilePicUrl:checkRequired(userInfo.profilePicUrl) ? userInfo.profilePicUrl : null,
            emailId:checkRequired(emailId) ? emailId : null,
            companyName: checkRequired(emailId) ? fetchCompanyFromEmail(emailId) : null,
            designation:checkRequired(userInfo.designation) ? userInfo.designation : null,
            location:checkRequired(userInfo.location) ? userInfo.location : null,
            mobileNumber:checkRequired(userInfo.mobileNumber) ? userInfo.mobileNumber : null,
            action:action,
            interactionType:interactionType,
            subType:subType,
            refId:refId,
            source:checkRequired(source) ? source : null,
            title:checkRequired(title) ? title : null,
            description:checkRequired(description) ? description : null,
            interactionDate: new Date(),
            endDate: null,
            createdDate:new Date(),
            trackId: null,
            emailContentId: null,
            googleAccountEmailId: null
        };
    }

    return obj;
}

function fetchCompanyFromEmail(email){
    var domain = email.substring(email.lastIndexOf("@") +1)
        , words = domain.split(".")
        , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
        , removableTextList = ["com", "org", "edu", "in"]

    _.remove(words, function(word){
        return removableTextList.indexOf(word.trim()) > -1
    })
    words = _.sortBy(words, function(word){return -(word.length);})
    return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

module.exports = router;