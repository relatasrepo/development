/**
 * Created by naveen on 1/27/16.
 */

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var _ = require("lodash");
var async = require("async")
var fs = require('fs');
var json2xls = require('json2xls');

var dealsAtRiskMeta = require('../../databaseSchema/userManagementSchema').dealsAtRiskMeta;
var masterData = require('../../databaseSchema/masterDataSchema').masterData;
var dashboardInsight = require('../../databaseSchema/dashboardInsightSchema').dashboardInsight;
var accountInsight = require('../../databaseSchema/accountInsightSchema').accountInsight;
var bqMetaData = require('../../databaseSchema/bqMetaDatASchema').bqMetaData;
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactionManagement = require('../../dataAccess/interactionManagement');
var commonUtility = require('../../common/commonUtility');
var errorMessages = require('../../errors/errorMessage');
var opportunityManagement = require('../../dataAccess/opportunitiesManagement');
var OppWeightsClass = require('../../dbCache/company.js')
var RevenueHierarchy = require('../../dataAccess/revenueHierarchy');
var SecondaryHierarchy = require('../../dataAccess/secondaryHierarchyManagement');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var OppCommitManagement = require('../../dataAccess/oppCommitAccess');

var common = new commonUtility();
var userManagementObj = new userManagement();

var errorMessagesObj = new errorMessages();
var interactionManagementObj = new interactionManagement();
var oppManagementObj = new opportunityManagement();
var company = new companyClass();
var oppWeightsObj = new OppWeightsClass();
var redis = require('redis');
var redisClient = redis.createClient();
var revenueHierarchyObj = new RevenueHierarchy();
var secondaryHierarchyObj = new SecondaryHierarchy();
var oppCommitObj = new OppCommitManagement();

var winstonLog = require('../../common/winstonLog');
var logLib = new winstonLog();
var redisLogs = logLib.getRedisLog();

var statusCodes = errorMessagesObj.getStatusCodes();

router.get('/menu/vertical', function(req, res) {
    res.render("menus/top_left_menu");
});

router.get('/menu/horizontal', function(req, res) {
    res.render("menus/top_top_menu");
});

router.get('/reports/opportunities', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("reports/opportunities");
});

router.get('/reports/opportunities/edit/popup', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("reports/oppPopup");
});

router.get('/reports/achievements', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("reports/achievements");
});

router.get('/reports/dashboard', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("reports/dashboard");
});

router.get('/reports/region/template', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("reports/region");
});

router.get('/reports/exceptional/access', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("reports/ea");
});

router.get('/reports/left/nav', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("reportsV2/nav");
});

router.get('/reports/allcrossfilters', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("reportsV2/allCrossFilters");
});

router.get('/reports/commit', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("reportsV2/commit");
});

router.get('/commits/commit', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("commits/commit");
});

router.get('/reports/account', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("reports/account");
});

router.get('/reports/today', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("todayInsights/today");
});

router.get('/reports/dashboard/insights', common.isLoggedInUserToGoNext, function(req, res) {

    var userId = common.getUserId(req.user);

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var qEnd = allQuarters[allQuarters.currentQuarter].end;

        common.getProfileOrStoreProfileInSession(common.castToObjectId(userId),req,function(userProfile) {
            var companyId = userProfile ? userProfile.companyId : null;

            var query = {
                ownerId:common.castToObjectId(userId),
                forTeam: false
            }

            var projection = {
                pipelineFlow:1,
                oppWon:1,
                oppLost:1,
                stale:1,
                targets:1,
                reasonsWon:1,
                reasonsLost:1,
                dealsAtRisk:1,
                pipelineVelocity:1,
                renewalOpen:1
            }

            if(req.query.section && req.query.section == "second"){
                projection = {
                    pipelineVelocity:1,
                    accountsWon:1,
                    oppsInteractions:1,
                    targets:1,
                    topOppByOppThisQtr:1
                }
            }

            if(req.query.section && req.query.section == "third"){
                projection = {
                    newCompaniesInteracted:1,
                    conversionRate:1,
                    pipelineFunnel:1,
                    typesWon:1,
                    sourcesWon:1,
                    productsWon:1,
                    targets:1,
                    currentInsights:1
                }
            }

            if(req.query.emailId && req.query.emailId.toLowerCase() == "all") {
                query = {
                    ownerId:common.castToObjectId(userId),
                    forTeam: true
                }
            }

            if(req.query.emailId && req.query.emailId.toLowerCase() != "all") {
                query = {
                    ownerEmailId:req.query.emailId,
                    forTeam: false
                }
            }

            query.date = {$lte: new Date(qEnd),$gte: new Date(qStart)}

            if(req.query.qStart && req.query.qEnd){
                query.date = {$lte: new Date(moment(req.query.qEnd).endOf("day")),$gte: new Date(req.query.qStart)}
            }

            dashboardInsight.findOne(query,projection).sort({date:-1}).limit(1).exec(function (err,data) {

                if(!err && data){
                    res.send(data)
                } else {
                    res.send(false)
                }
            })
        });
    });

});

router.get('/reports/dashboard/account/insights', common.isLoggedInUserToGoNext, function(req, res) {

    var userId = common.getUserId(req.user);

    common.getProfileOrStoreProfileInSession(common.castToObjectId(userId),req,function(userProfile) {
        var companyId = userProfile ? userProfile.companyId : null;

        var query = {
            companyId: common.castToObjectId(companyId)
        }

        if(req.query.qStart && req.query.qEnd){
            query.date = {$lte: new Date(moment(req.query.qEnd).endOf("day")),$gte: new Date(req.query.qStart)}
        }
        var filterAccess = []
        if(req.query.emailId){
            filterAccess = req.query.emailId.split(",")
        } else {
            filterAccess.push(userProfile.emailId)
        }

        accountInsight.findOne(query).sort({date:-1}).limit(1).lean().exec(function (err,data) {

            if(!err && data){
                var oppWithAccess = [],
                    intsWithAccess = [],
                    topOppWithAccess = [],
                    topIntsWithAccess = [];

                if(data.accountsInteractions && data.accountsInteractions.opportunities){
                    oppWithAccess = data.accountsInteractions.opportunities.filter(function (op) {
                        if(containsAny(op.usersWithAccess,filterAccess)){
                            return op;
                        }
                    })
                }

                if(data.accountsInteractions && data.accountsInteractions.interactions){
                    intsWithAccess = data.accountsInteractions.interactions.filter(function (op) {
                        if(containsAny(op.usersWithAccess,filterAccess)){
                            return op;
                        }
                    });
                }

                if(data.topAccByOppThisQtr && data.topAccByOppThisQtr.opportunities) {
                    topOppWithAccess = data.topAccByOppThisQtr.opportunities.filter(function (op) {
                        if(containsAny(op.usersWithAccess,filterAccess)){
                            return op;
                        }
                    })
                }

                if(data.topAccByOppThisQtr && data.topAccByOppThisQtr.interactions){

                    topIntsWithAccess = data.topAccByOppThisQtr.interactions.filter(function (op) {
                        if(containsAny(op.usersWithAccess,filterAccess)){
                            return op;
                        }
                    });
                }

                data.accountsInteractions.opportunities = oppWithAccess;
                data.accountsInteractions.interactions = intsWithAccess;

                if(!data.topAccByOppThisQtr){
                    data.topAccByOppThisQtr = {}
                }

                data.topAccByOppThisQtr.opportunities = topOppWithAccess;
                data.topAccByOppThisQtr.interactions = topIntsWithAccess;

                res.send(data)
            } else {
                res.send({
                    accountsInteractions:null
                })
            }
        })
    });

});

function containsAny(source,target) {
    if(source){
        var result = source.filter(function(item){ return target.indexOf(item) > -1});
        return (result.length > 0);
    } else {
        return false;
    }
}

router.post('/reports/get/opportunities', common.isLoggedInUserToGoNext, function(req, res) {

    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile ? userProfile.companyId : null;
    var userId = common.getUserId(req.user);
    var forUserEmailId = null;
    var filter = null;
    if(req.body.forUserEmailId){
        forUserEmailId = req.body.forUserEmailId;
    }

    if(req.body.filters){
        filter = req.body.filters
    }

    var from = moment().subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    oppWeightsObj.getOppStages(common.castToObjectId(companyId), function (oppStages, companyDetails) {

        common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {

            var qStart = allQuarters[allQuarters.currentQuarter].start;
            var qEnd = allQuarters[allQuarters.currentQuarter].end;

            var commitStage = "Proposal"; // default.
            if (oppStages) {
                _.each(oppStages, function (st) {
                    if(st.commitStage) {
                        commitStage = st.name;
                    }
                });
            }

            if(filter && filter.length>0){
                _.each(filter,function (fl) {
                    if(fl.type === "closeDate"){
                        fl.start = moment(fl.start).startOf("month");
                        fl.end = moment(fl.end).endOf("month");
                        qStart = fl.start;
                        qEnd = fl.end;
                    }
                })
            }

            var currenciesObj = {};
            var primaryCurrency = "USD"

            if(companyDetails){
                _.each(companyDetails.currency,function (co) {
                    currenciesObj[co.symbol] = co;
                    if(co.isPrimary){
                        primaryCurrency = co.symbol;
                    }
                })
            }

            var forUsers = [forUserEmailId];
            if(!forUserEmailId && req.body.allUserEmailId){
                forUsers = req.body.allUserEmailId
            } else if(req.body.forUserEmailId) {
                forUsers = [req.body.forUserEmailId]
            }

            var forUserIds = [userId];
            if(req.body.allUserId){
                forUserIds = req.body.allUserId
            }

            var startMonthYear = {
                month:moment(qStart).month(),
                year:moment(qStart).year()
            }

            var endMonthYear = {
                month:moment(qEnd).month()+1,
                year:moment(qEnd).year()
            }

            // 0 - Jan
            // 1 - Feb
            // 2 - Mar
            fyRange.start = new Date(moment(fyRange.start).subtract(1,"day"))
            fyRange.end = new Date(moment(fyRange.end).add(1,"day"))

            async.parallel([
                function (callback) {
                    oppManagementObj.getMetaDataForMonth(common.castToObjectId(companyId),forUsers,startMonthYear,endMonthYear,qStart,qEnd,primaryCurrency,currenciesObj,callback)
                },
                function (callback) {
                    getOppsForThisQuarter(common.castToObjectId(companyId),common.castToObjectId(userId),forUserEmailId,filter,fyMonth,fyRange,allQuarters,forUsers,req.body.selectedPortfolio,callback)
                },
                function (callback) {

                    var emailIds = userProfile.emailId;

                    if(!emailIds){
                        emailIds = [forUserEmailId];
                    }
                    dealsAtRiskMeta.find({userEmailId:{$in:emailIds},date:{$gte:new Date(from),$lte:new Date(to)}},{opportunityIds:0}).sort({date:-1}).limit(1).exec(function (err,deals) {
                        callback(err,deals)
                    })
                },
                function (callback) {
                    var userIds = [];
                    if(req.body.forUserId){
                        userIds = [common.castToObjectId(req.body.forUserId)]
                    } else {
                        userIds = common.castListToObjectIds(req.body.allUserId)
                    }

                    oppManagementObj.getTargetsForAllUsers(userIds,qStart,qEnd,fyRange.start,fyRange.end,callback);
                }
            ], function (errors,data) {

                if(!errors && data && data[0]){

                    res.send({
                        oppStages:_.pluck(oppStages,"name"),
                        opps:data[1]?data[1]:[],
                        commitStage:commitStage,
                        metaData:data[0],
                        dealsAtRisk:convertCurrencyForDealsAtRisk(data[2],primaryCurrency,currenciesObj),
                        targets:data[3],
                        qStart:qStart,
                        qEnd:qEnd
                    })
                }
            });

        });
    });
});

router.post('/reports/get/opportunities/v2', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var forUserEmailId = null;
    var filter = null;
    if(req.body.forUserEmailId){
        forUserEmailId = req.body.forUserEmailId;
    }

    if(req.body.filters){
        filter = req.body.filters
    }

    var from = moment().subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    common.getProfileOrStoreProfileInSession(userId,req,function(userProfile){
        var companyId = userProfile ? userProfile.companyId : null;

        oppWeightsObj.getOppStages(common.castToObjectId(companyId), function (oppStages, companyDetails) {
    
            common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {
    
                var qStart = allQuarters[allQuarters.currentQuarter].start;
                var qEnd = allQuarters[allQuarters.currentQuarter].end;
    
                var commitStage = "Proposal"; // default.
                if (oppStages) {
                    _.each(oppStages, function (st) {
                        if(st.commitStage) {
                            commitStage = st.name;
                        }
                    });
                }

                var qtrDateFilterExists = false;
    
                if(filter && filter.length>0){
                    _.each(filter,function (fl) {
                        if(fl.type === "closeDate"){
                            qtrDateFilterExists = true;
                            if(!fl.includeDateRange) {
                                fl.start = moment(moment(fl.start).add(1,"day")).startOf("month");
                                fl.end = moment(moment(fl.end).subtract(1,"day")).endOf("month");
                            }
                            qStart = fl.start;
                            qEnd = fl.end;
                        }
                    })
                }
    
                var currenciesObj = {};
                var primaryCurrency = "USD"
    
                if(companyDetails){
                    _.each(companyDetails.currency,function (co) {
                        currenciesObj[co.symbol] = co;
                        if(co.isPrimary){
                            primaryCurrency = co.symbol;
                        }
                    })
                }
    
                var forUsers = [forUserEmailId];
                if(!forUserEmailId && req.body.allUserEmailId){
                    forUsers = req.body.allUserEmailId
                } else if(req.body.forUserEmailId) {
                    forUsers = [req.body.forUserEmailId]
                }
    
                // 0 - Jan
                // 1 - Feb
                // 2 - Mar
                fyRange.start = new Date(moment(fyRange.start).subtract(1,"day"))
                fyRange.end = new Date(moment(fyRange.end).add(1,"day"))

                masterData.find({companyId:common.castToObjectId(companyId)},{importantHeaders:1}).lean().exec(function (er,masterData) {

                    secondaryHierarchyObj.getUserHierarchyByEmailId(forUsers,req.body.selectedHierarchy,function (err_sh,sh) {

                        if(sh){

                            if(!filter){
                                filter = []
                            }

                            if(!qtrDateFilterExists){
                                filter = filter.concat([{type: "closeDate",start:new Date(qStart),end: new Date(qEnd)}])
                            }

                            filter = filter.concat(sh);
                        }

                        async.parallel([
                            function (callback) {
                                getOppsForThisQuarter(common.castToObjectId(companyId),common.castToObjectId(userId),forUserEmailId,filter,fyMonth,fyRange,allQuarters,forUsers,req.body.selectedPortfolio,callback)
                            },
                            function (callback) {

                                if(forUserEmailId){
                                    var query = {
                                        ownerEmailId:forUserEmailId,
                                        forTeam: false
                                    }
                                    query.date = {$lte: new Date(qEnd),$gte: new Date(qStart)};
                                    dashboardInsight.findOne(query,{dealsAtRisk:1}).sort({date:-1}).limit(1).exec(function (err,data) {
                                        if(err || (data && !data.dealsAtRisk)){
                                            data = {
                                                dealsAtRisk:{
                                                    amount:0,
                                                    count:0,
                                                    dealsRiskAsOfDate:""
                                                }
                                            }
                                        }
                                        callback(err,data)
                                    });
                                } else {
                                    callback(null,{
                                        dealsAtRisk:{
                                            amount:0,
                                            count:0,
                                            dealsRiskAsOfDate:""
                                        }
                                    });
                                }
                            }
                        ], function (errors,data) {

                            if(!errors && data && data[0]){

                                res.send({
                                    oppStages:_.pluck(oppStages,"name"),
                                    opps:data[0]?data[0]:[],
                                    commitStage:commitStage,
                                    qStart:qStart,
                                    qEnd:qEnd,
                                    dealsAtRisk:data && data[1]?data[1].dealsAtRisk:null,
                                    masterData:masterData
                                })
                            }
                        });
                    });
                })
    
            });
        });
    })

});

router.post('/reports/download/opportunities/v2', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var forUserEmailId = null;
    var filter = null;
    if(req.body.forUserEmailId){
        forUserEmailId = req.body.forUserEmailId;
    }

    if(req.body.filters){
        filter = req.body.filters
    }

    var from = moment().subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    common.getProfileOrStoreProfileInSession(userId,req,function(userProfile){
        var companyId = userProfile ? userProfile.companyId : null;

        oppWeightsObj.getOppStages(common.castToObjectId(companyId), function (oppStages, companyDetails) {
    
            common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {
    
                var qStart = allQuarters[allQuarters.currentQuarter].start;
                var qEnd = allQuarters[allQuarters.currentQuarter].end;
    
                var commitStage = "Proposal"; // default.
                if (oppStages) {
                    _.each(oppStages, function (st) {
                        if(st.commitStage) {
                            commitStage = st.name;
                        }
                    });
                }

                var qtrDateFilterExists = false;
    
                if(filter && filter.length>0){
                    _.each(filter,function (fl) {
                        if(fl.type === "closeDate"){
                            qtrDateFilterExists = true;
                            if(!fl.includeDateRange) {
                                fl.start = moment(fl.start).startOf("month");
                                fl.end = moment(fl.end).endOf("month");
                            }
                            qStart = fl.start;
                            qEnd = fl.end;
                        }
                    })
                }
    
                var currenciesObj = {};
                var primaryCurrency = "USD"
    
                if(companyDetails){
                    _.each(companyDetails.currency,function (co) {
                        currenciesObj[co.symbol] = co;
                        if(co.isPrimary){
                            primaryCurrency = co.symbol;
                        }
                    })
                }
    
                var forUsers = [forUserEmailId];
                if(!forUserEmailId && req.body.allUserEmailId){
                    forUsers = req.body.allUserEmailId
                } else if(req.body.forUserEmailId) {
                    forUsers = [req.body.forUserEmailId]
                }
    
                // 0 - Jan
                // 1 - Feb
                // 2 - Mar
                fyRange.start = new Date(moment(fyRange.start).subtract(1,"day"))
                fyRange.end = new Date(moment(fyRange.end).add(1,"day"))

                masterData.find({companyId:common.castToObjectId(companyId)},{importantHeaders:1}).lean().exec(function (er,masterData) {

                    secondaryHierarchyObj.getUserHierarchyByEmailId(forUsers,req.body.selectedHierarchy,function (err_sh,sh) {

                        if(sh){

                            if(!filter){
                                filter = []
                            }

                            if(!qtrDateFilterExists){
                                filter = filter.concat([{type: "closeDate",start:new Date(qStart),end: new Date(qEnd)}])
                            }

                            filter = filter.concat(sh);
                        }

                        async.parallel([
                            function (callback) {
                                getOppsForThisQuarter(common.castToObjectId(companyId),common.castToObjectId(userId),forUserEmailId,filter,fyMonth,fyRange,allQuarters,forUsers,req.body.selectedPortfolio,callback)
                            }
                        ], function (errors,data) {

                            var importantHeaders = [];

                            if(!errors && data && data[0]){

                                if(masterData && masterData.length>0){
                                    _.each(masterData,function (ma) {
                                        if(ma.importantHeaders){
                                            _.each(ma.importantHeaders,function (ih) {
                                                if(ih.isImportant){
                                                    importantHeaders.push(ih.name);
                                                }
                                            });
                                        }
                                    })
                                }

                                formatOppsForDownload(data[0],currenciesObj,primaryCurrency,importantHeaders,function (opps) {

                                    var xls = json2xls(opps);
                                    var fileName = 'Opps-'+moment().format("DDMMMMYY")+"_"+String(userId)+".xlsx"
                                    fs.writeFileSync('public/'+fileName, xls, 'binary');

                                    res.send({
                                        link:fileName
                                    });

                                    // res.xls('Opps-'+moment().format("DDMMMMYY"), opps);
                                    // res.send({
                                    //     opps:opps,
                                    //     commitStage:commitStage,
                                    //     qStart:qStart,
                                    //     qEnd:qEnd,
                                    //     masterData:masterData
                                    // })
                                })
                            }
                        });
                    });
                })
    
            });
        });
    })

});

function formatOppsForDownload(opps,currenciesObj,primaryCurrency,importantHeaders,callback) {
    
    var importantHeadersObj = {};
    
    importantHeaders = _.sortBy(importantHeaders,function (el) {
        return el.toLowerCase()
    });

    _.each(importantHeaders,function (el) {
        importantHeadersObj[el] = true;
    });

    var formattedOpps = [];
    
    _.each(opps,function (op) {

        if(op.closeReasons && op.closeReasons.length>0){
            op.closeReasonsString = "";
            _.each(op.closeReasons,function (cr,index) {
                if(index === 0){
                    op.closeReasonsString = cr;
                } else {
                    op.closeReasonsString = op.closeReasonsString+","+cr;
                }
            })
        }

        convertAmount(op,primaryCurrency,currenciesObj)

        if(op.masterData && op.masterData.length>0){
            _.each(op.masterData,function (ma) {
                _.each(ma.data,function (da) {

                    for(var key in da){
                        if(importantHeadersObj[key]){

                            if(!op.masterDataFormat){
                                op.masterDataFormat = [];
                            }

                            if(da[key]){
                                op.masterDataFormat.push({
                                    key:key,
                                    value:da[key]
                                })
                                op[key] = da[key];
                            } else {
                                op[key] = "";
                                op.masterDataFormat.push({
                                    key:key,
                                    value:""
                                })
                            }
                        }
                    }
                });
            })
        }

        if(op.masterDataFormat && op.masterDataFormat.length>0){

        } else if((!op.masterDataFormat || op.masterDataFormat.length === 0) && importantHeaders.length>0){
            op.masterDataFormat = [];
            _.each(importantHeaders,function (ih) {
                op.masterDataFormat.push({
                    key:ih,
                    value:""
                });
            });
        };

        var nonExistingImpHeaders = _.xor(importantHeaders,_.map(op.masterDataFormat,'key'));

        if(nonExistingImpHeaders && nonExistingImpHeaders.length>0){
            _.each(nonExistingImpHeaders,function (ih) {
                op.masterDataFormat.push({
                    key:ih,
                    value:""
                })
            });
        };

        formattedOpps.push(formatOppForDownload(op,currenciesObj))
    });

    callback(formattedOpps)
};

function convertAmount(el,primaryCurrency,currenciesObj) {

    el.amountWithNgm = parseFloat(el.amount);

    if(el.netGrossMargin || el.netGrossMargin == 0){
        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
    }

    el.convertedAmt = el.amount;
    el.convertedAmtWithNgm = el.amountWithNgm

    if(el.currency && el.currency !== primaryCurrency){

        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
        }

        if(el.netGrossMargin || el.netGrossMargin == 0){
            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
        }

        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))

    }
}

function formatOppForDownload(el,currenciesObj) {
    var acc = common.fetchCompanyFromEmail(el.contactEmailId);
    var obj = {
        "Opp Name": el.opportunityName,
        "Opp Owner": el.userEmailId,
        "Contact (selling to)": el.contactEmailId,
        Account: acc?acc:"Others",
        Currency: el.currency?el.currency:"USD",
        Amount: el.amount,
        Margin:el.netGrossMargin,
        Bottomline: parseFloat(el.amountWithNgm.toFixed(2)),
        "Exchange Rate": currenciesObj[el.currency] && currenciesObj[el.currency].xr?currenciesObj[el.currency].xr:1,
        "Top Line (Primary Currency)": el.convertedAmt,
        "Bottom Line (Primary Currency)": parseFloat(el.convertedAmtWithNgm.toFixed(2)),
        Stage: el.stageName,
        "Close Date": moment(new Date(el.closeDate)).format("DD MMM YYYY"),
        // "Close Date": formatThisDate(el.closeDate),
        Product:el.productType,
        BU: el.businessUnit,
        Solution:el.solution,
        Type:el.type,
        Region:el.geoLocation?el.geoLocation.zone:"",
        City:el.geoLocation?el.geoLocation.town:"",
        Source:el.sourceType,
        Vertical:el.vertical,
        Partners: _.pluck(el.partners,"emailId").join(","),
        "Created Date": el.createdDate?moment(el.createdDate).format("DD MMM YYYY"):moment(el._id.getTimestamp()).format("DD MMM YYYY"),
        "Created By": el.createdByEmailId,
        "Opportunity Id": el.opportunityId
    }

    if(el.masterDataFormat && el.masterDataFormat.length>0){
        _.each(el.masterDataFormat,function (ma) {
            var mdObj = {};
            mdObj[ma.key] = ma.value;
            // obj = {...obj,...mdObj};
            obj = _.merge(obj,mdObj);
        })
    }

    return obj;
}

function formatThisDate(date){
    var newDate = new Date(date);
    var str = newDate.getDate()+" "+(parseInt(newDate.getMonth())+1)+" "+newDate.getFullYear()
    return str.toString()
}

function convertCurrencyForDealsAtRisk(dealsAtRisk,primaryCurrency,currenciesObj) {

    _.each(dealsAtRisk,function (de) {

        var totalDealValueAtRisk = 0;
        _.each(de.deals,function (el) {

            el.amountWithNgm = el.amount;

            if(el.netGrossMargin || el.netGrossMargin == 0){
                el.amountWithNgm = (el.amount*el.netGrossMargin)/100
            }

            el.convertedAmt = el.amount;
            el.convertedAmtWithNgm = el.amountWithNgm

            if(el.currency && el.currency !== primaryCurrency){

                if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                    el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                }

                if(el.netGrossMargin || el.netGrossMargin == 0){
                    el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                }

                el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))

            }

            totalDealValueAtRisk = totalDealValueAtRisk+parseFloat(el.convertedAmtWithNgm);
        });

        de.totalDealValueAtRisk = totalDealValueAtRisk;
    })

    return dealsAtRisk
}

router.get('/reports/download/opportunities', common.isLoggedInUserToGoNext, function(req, res) {

    var userId = common.getUserId(req.user);
    var filterKey = String(userId)+"filtersApplied";
    var fileName = 'opps_'+moment().toString()+'.xlsx'

    var projection = {
        influencers:0,
        decisionMakers:0,
        usersWithAccess:0,
        notes:0,
        lastStageUpdated:0,
        BANT:0,
        isClosed:0,
        isWon:0,
        salesforceContactId:0,
        mobileNumber:0
    }

    userManagementObj.getUserBasicProfile(common.castToObjectId(userId),function (err,userProfile) {

        var companyId = userProfile ? userProfile.companyId : null;
        var userEmailIdKey = String(companyId)+"userEmailId"

        oppWeightsObj.getOppStages(common.castToObjectId(companyId), function (oppStages, companyDetails) {

            common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {

                var qStart = allQuarters[allQuarters.currentQuarter].start;
                var qEnd = allQuarters[allQuarters.currentQuarter].end;

                getCache(filterKey,function (filtersApplied) {
                    getCache(userEmailIdKey,function (userEmailId) {
                        getCache(String(userId)+"forUsers",function (forUsers) {

                            var findQuery = {
                                companyId:companyId
                            }

                            if(forUsers){
                                findQuery.userEmailId = {$in:forUsers}
                                findQuery.closeDate = {$lte: new Date(qEnd),$gte: new Date(qStart)}
                            } else  if(filtersApplied){
                                findQuery["$and"] = buildQuery(filtersApplied)
                            } else if(userEmailId){
                                findQuery.userEmailId = userEmailId
                                findQuery.closeDate = {$lte: new Date(qEnd),$gte: new Date(qStart)}
                            } else {
                                findQuery.userEmailId = userProfile.emailId
                                findQuery.closeDate = {$lte: new Date(qEnd),$gte: new Date(qStart)}
                            }

                            var currenciesObj = {};
                            var primaryCurrency = "USD"

                            if(companyDetails){
                                _.each(companyDetails.currency,function (co) {
                                    currenciesObj[co.symbol] = co;
                                    if(co.isPrimary){
                                        primaryCurrency = co.symbol;
                                    }
                                })
                            }

                            oppManagementObj.getOppByCustomQuery(findQuery,projection,function (err,opps) {
                                // oppManagementObj.getOppByCustomQuery(findQuery,null,function (err,opps) {

                                var oppsFormatted = [];

                                _.each(opps,function (el) {

                                    var acc = common.fetchCompanyFromEmail(el.contactEmailId);

                                    el.amountWithNgm = parseFloat(el.amount);

                                    if(el.netGrossMargin || el.netGrossMargin == 0){
                                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                                    }

                                    el.convertedAmt = el.amount;
                                    el.convertedAmtWithNgm = el.amountWithNgm

                                    if(el.currency && el.currency !== primaryCurrency){

                                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                                        }

                                        if(el.netGrossMargin || el.netGrossMargin == 0){
                                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                                        }

                                        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))

                                    }

                                    oppsFormatted.push({
                                        "Opp Name": el.opportunityName,
                                        "Opp Owner": el.userEmailId,
                                        "Contact (selling to)": el.contactEmailId,
                                        Account: acc?acc:"Others",
                                        Currency: el.currency?el.currency:"USD",
                                        Amount: el.amount,
                                        Margin:el.netGrossMargin,
                                        Bottomline: parseFloat(el.amountWithNgm.toFixed(2)),
                                        "Exchange Rate": currenciesObj[el.currency] && currenciesObj[el.currency].xr?currenciesObj[el.currency].xr:1,
                                        "Top Line (Primary Currency)": el.convertedAmt,
                                        "Bottom Line (Primary Currency)": parseFloat(el.convertedAmtWithNgm.toFixed(2)),
                                        Stage: el.stageName,
                                        "Close Date": moment(el.closeDate).format("DD MMM YYYY"),
                                        Product:el.productType,
                                        BU: el.businessUnit,
                                        Solution:el.solution,
                                        Type:el.type,
                                        Region:el.geoLocation?el.geoLocation.zone:"",
                                        City:el.geoLocation?el.geoLocation.town:"",
                                        Source:el.sourceType,
                                        Vertical:el.vertical,
                                        Partners: _.pluck(el.partners,"emailId").join(","),
                                        "Created Date": el.createdDate?moment(el.createdDate).format("DD MMM YYYY"):moment(el._id.getTimestamp()).format("DD MMM YYYY"),
                                        "Created By": el.createdByEmailId,
                                        "Opportunity Id": el.opportunityId
                                    })
                                });

                                redisClient.del(filterKey);
                                res.xls(fileName, oppsFormatted);
                            })
                        });
                    })
                })
            })
        });
    })

});

router.get('/reports/download/opportunities/all/access', common.isLoggedInUserToGoNext, function(req, res) {

    var userId = common.getUserId(req.user);
    var filterKey = String(userId)+"filtersApplied";
    var fileName = 'OppsAllAccess_'+moment().toString()+'.xlsx'

    var projection = {
        influencers:0,
        decisionMakers:0,
        usersWithAccess:0,
        notes:0,
        lastStageUpdated:0,
        BANT:0,
        isClosed:0,
        isWon:0,
        salesforceContactId:0,
        mobileNumber:0
    }

    userManagementObj.getUserBasicProfile(common.castToObjectId(userId),function (err,userProfile) {

        var companyId = userProfile ? userProfile.companyId : null;

        revenueHierarchyObj.getUserHierarchy(common.castToObjectId(userId),function (rErr,revenueHierarchyData) {
            userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, orgHierarchyData){

                var teamUserIds = [];

                _.each(revenueHierarchyData,function (el) {
                    teamUserIds.push(el.ownerEmailId)
                })

                _.each(orgHierarchyData,function (el) {
                    teamUserIds.push(el.emailId)
                })

                teamUserIds = _.uniq(teamUserIds)

                getUserAccessControl([common.castToObjectId(userId)],userId,function (regionAccess,productAccess,verticalAccess,companyId) {
                    oppWeightsObj.getOppStages(common.castToObjectId(companyId), function (oppStages, companyDetails) {

                        var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId);

                        var findQuery = {
                            companyId:common.castToObjectId(companyId),
                            $or:[
                                {
                                    userEmailId:{$in:teamUserIds}
                                },
                                {
                                    $and:accessControlQuery
                                },{
                                    "usersWithAccess.emailId":{$in:[userProfile.emailId]}
                                }
                            ]
                        }

                        var currenciesObj = {};
                        var primaryCurrency = "USD"

                        if(companyDetails){
                            _.each(companyDetails.currency,function (co) {
                                currenciesObj[co.symbol] = co;
                                if(co.isPrimary){
                                    primaryCurrency = co.symbol;
                                }
                            })
                        }

                        oppManagementObj.getOppByCustomQuery(findQuery,projection,function (err,opps) {
                            // oppManagementObj.getOppByCustomQuery(findQuery,null,function (err,opps) {

                            var oppsFormatted = [];

                            _.each(opps,function (el) {

                                var acc = common.fetchCompanyFromEmail(el.contactEmailId);

                                el.amountWithNgm = parseFloat(el.amount);

                                if(el.netGrossMargin || el.netGrossMargin == 0){
                                    el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                                }

                                if(el.closeReasons && el.closeReasons.length>0){
                                    el.closeReasonsString = "";
                                    _.each(el.closeReasons,function (cr,index) {
                                        if(index == 0){
                                            el.closeReasonsString = cr;
                                        } else {
                                            el.closeReasonsString = el.closeReasonsString+","+cr;
                                        }
                                    })
                                }

                                el.convertedAmt = el.amount;
                                el.convertedAmtWithNgm = el.amountWithNgm

                                if(el.currency && el.currency !== primaryCurrency){

                                    if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                                        el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                                    }

                                    if(el.netGrossMargin || el.netGrossMargin == 0){
                                        el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                                    }

                                    el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))

                                }

                                oppsFormatted.push({
                                    "Opp Name": el.opportunityName,
                                    "Opp Owner": el.userEmailId,
                                    "Contact (selling to)": el.contactEmailId,
                                    Account: acc?acc:"Others",
                                    Currency: el.currency?el.currency:"USD",
                                    Amount: el.amount,
                                    Margin:el.netGrossMargin,
                                    Bottomline: parseFloat(el.amountWithNgm.toFixed(2)),
                                    "Exchange Rate": currenciesObj[el.currency] && currenciesObj[el.currency].xr?currenciesObj[el.currency].xr:1,
                                    "Top Line (Primary Currency)": el.convertedAmt,
                                    "Bottom Line (Primary Currency)": parseFloat(el.convertedAmtWithNgm.toFixed(2)),
                                    Stage: el.stageName,
                                    "Close Date": moment(el.closeDate).format("DD MMM YYYY"),
                                    // "Close Date": formatThisDate(el.date),
                                    Product:el.productType,
                                    BU: el.businessUnit,
                                    Solution:el.solution,
                                    Type:el.type,
                                    Region:el.geoLocation?el.geoLocation.zone:"",
                                    City:el.geoLocation?el.geoLocation.town:"",
                                    Source:el.sourceType,
                                    Vertical:el.vertical,
                                    Partners: _.pluck(el.partners,"emailId").join(","),
                                    "Created Date": el.createdDate?moment(el.createdDate).format("DD MMM YYYY"):moment(el._id.getTimestamp()).format("DD MMM YYYY"),
                                    "Created By": el.createdByEmailId,
                                    "Opportunity Id": el.opportunityId,
                                    "Close Reasons": el.closeReasonsString,
                                    "Close Reasons Description": el.closeReasonDescription
                                })
                            });

                            res.xls(fileName, oppsFormatted);
                        })
                    });
                });
            })
        })
    })


});

router.get('/reports/current/insights', common.isLoggedInUserToGoNext, function(req, res){

    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile ? userProfile.companyId : null;
    var userId = common.getUserId(req.user);
    var forUserEmailId = null;
    if(req.query.forUserEmailId){
        forUserEmailId = req.query.forUserEmailId.split(",");
    }

    var projection = {
        usersWithAccess:0,
        notes:0,
        lastStageUpdated:0,
        BANT:0,
        isClosed:0,
        isWon:0,
        salesforceContactId:0,
        mobileNumber:0
    }

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {

        oppWeightsObj.getOppStages(common.castToObjectId(companyId), function (oppStages, companyDetails) {

            var currenciesObj = {};
            var primaryCurrency = "USD"
            var qStart = allQuarters[allQuarters.currentQuarter].start;
            var qEnd = allQuarters[allQuarters.currentQuarter].end;

            var minDate = moment().subtract(90,"days")

            if(new Date(qStart)< new Date(minDate)){
                minDate = qStart;
            }

            var query = {
                userEmailId:{$in:forUserEmailId},
                closeDate:{$lte: new Date(moment().add(90,"days")),$gte: new Date(minDate)}
            }

            if(companyId){
                query = {
                    // companyId:companyId,
                    userEmailId:{$in:forUserEmailId},
                    closeDate:{$lte: new Date(moment().add(90,"days")),$gte: new Date(minDate)}
                }
            }

            var threeMonthsEndDate = moment().add(90,"days");

            if(companyDetails){
                _.each(companyDetails.currency,function (co) {
                    currenciesObj[co.symbol] = co;
                    if(co.isPrimary){
                        primaryCurrency = co.symbol;
                    }
                })
            }

            getAllCompanyUsers(common.castToObjectId(companyId),function (errT,teamData) {
                var companyMembers = [];

                var companyMembersObj = {};
                _.each(teamData,function (el) {
                    companyMembersObj[el.emailId] = el;
                    companyMembers.push(el._id)
                })

                oppManagementObj.getOppByCustomQuery(query,projection,function (err,opps) {

                    oppManagementObj.staleOpportunitiesCount(forUserEmailId,null,function (err,staleOpps) {

                        var contactsForInteractions = [];
                        var interactionsForWonOpp = 0,
                            maxInteractionsForWonOpp = 0,
                            daysToCloseOpp = 0,
                            maxDaysToCloseOpp = 0,
                            minDaysToCloseOpp = 0,
                            wonOpps = 0,
                            oppsClosingThisQtr = 0,
                            renewalOppsNext90Days = 0,
                            renewalOppsAmountNext90Days = 0,
                            newOppsAdded = 0,
                            allOppsAmountThisQuarter = 0,
                            wonAmount = 0,
                            allDealSizes = [],
                            intPerThousandAmountWon = [],
                            allDealCloseDays = [];

                        if(!err && opps && opps.length>0){
                            _.each(opps,function (op) {

                                var oppCreatedDate = op.createdDate?op.createdDate:op._id.getTimestamp();
                                op.amountWithNgm = op.amount;

                                if(op.netGrossMargin || op.netGrossMargin == 0){
                                    op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                                }

                                op.convertedAmt = op.amount;
                                op.convertedAmtWithNgm = op.amountWithNgm

                                if(op.currency && op.currency !== primaryCurrency){

                                    if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                                        op.convertedAmt = op.amount/currenciesObj[op.currency].xr
                                    }

                                    if(op.netGrossMargin || op.netGrossMargin == 0){
                                        op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                                    }

                                    op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

                                }

                                if(new Date(oppCreatedDate)>= new Date(qStart) && new Date(oppCreatedDate) <= new Date(qEnd)) {
                                    newOppsAdded++
                                }

                                if(new Date(op.closeDate)>= new Date(qStart) && new Date(op.closeDate) <= new Date(qEnd)) {
                                    oppsClosingThisQtr++
                                }

                                if(new Date(op.closeDate)>= new Date() && new Date(op.closeDate)<= new Date(threeMonthsEndDate) && op.type && op.type.toLowerCase() == "renewal"){
                                    renewalOppsAmountNext90Days = renewalOppsAmountNext90Days+parseFloat(op.convertedAmtWithNgm);
                                    renewalOppsNext90Days++
                                }

                                if(op.relatasStage === "Close Won" && new Date(op.closeDate)>= new Date(qStart) && new Date(op.closeDate) <= new Date(qEnd)){

                                    var allInteractions = 0;
                                    wonOpps++;
                                    if(op.interactionCount){
                                        interactionsForWonOpp = interactionsForWonOpp+op.interactionCount;
                                        allInteractions = op.interactionCount;
                                    }

                                    wonAmount = wonAmount+op.convertedAmtWithNgm;

                                    var createCloseDiff = moment(op.closeDate).diff(moment(oppCreatedDate), 'days');

                                    if(createCloseDiff == 0){
                                        createCloseDiff = 1;
                                    }

                                    allDealCloseDays.push(createCloseDiff)

                                    daysToCloseOpp = daysToCloseOpp+createCloseDiff;

                                    if(op.influencers && op.influencers.length>0){
                                        _.each(op.influencers,function (el) {
                                            if(el.interactionCount){
                                                interactionsForWonOpp = interactionsForWonOpp+el.interactionCount
                                                allInteractions = allInteractions+el.interactionCount
                                            }
                                        })
                                    }

                                    if(op.decisionMakers && op.decisionMakers.length>0){
                                        _.each(op.decisionMakers,function (el) {
                                            if(el.interactionCount){
                                                interactionsForWonOpp = interactionsForWonOpp+el.interactionCount
                                                allInteractions = allInteractions+el.interactionCount
                                            }
                                        })
                                    }

                                    if(op.partners && op.partners.length>0){
                                        _.each(op.partners,function (el) {
                                            if(el.interactionCount){
                                                interactionsForWonOpp = interactionsForWonOpp+el.interactionCount
                                                allInteractions = allInteractions+el.interactionCount
                                            }
                                        })
                                    }

                                    if(allInteractions>maxInteractionsForWonOpp){
                                        maxInteractionsForWonOpp = allInteractions
                                    }

                                    if(allInteractions && op.convertedAmtWithNgm){
                                        // var amount = op.convertedAmtWithNgm/1000;
                                        var amount = op.convertedAmtWithNgm;
                                        intPerThousandAmountWon.push(allInteractions/amount);
                                    }

                                    allDealSizes.push(op.convertedAmtWithNgm)
                                }
                            })
                        }

                        var avgDaysToCloseOpp = 0,
                            avgDealSize = 0,
                            avgInteractionsPerAmountWon = 0,
                            newOppsCreatedThisQuarter = 0;

                        if(daysToCloseOpp && wonOpps){
                            avgDaysToCloseOpp = parseFloat((daysToCloseOpp/wonOpps).toFixed(2));
                        }

                        if(interactionsForWonOpp && wonAmount){
                            avgInteractionsPerAmountWon = parseFloat(((interactionsForWonOpp/(wonAmount/1000))).toFixed(2));
                            // avgInteractionsPerAmountWon = parseFloat((interactionsForWonOpp/wonAmount).toFixed(2));
                        }

                        if(wonAmount && wonOpps){
                            avgDealSize = parseFloat((wonAmount/wonOpps).toFixed(2));
                        }

                        minDaysToCloseOpp = _.min(allDealCloseDays)
                        maxDaysToCloseOpp = _.max(allDealCloseDays)

                        res.send({
                            SuccessCode:1,
                            Data:{
                                avgDealSize:avgDealSize,
                                maxDealSize:_.max(allDealSizes),
                                minDealSize:_.min(allDealSizes),
                                staleOpps:staleOpps,
                                wonAmount:wonAmount,
                                minIntPerThousandAmountWon:_.min(intPerThousandAmountWon),
                                maxIntPerThousandAmountWon:_.max(intPerThousandAmountWon),
                                maxInteractionsForWonOpp:maxInteractionsForWonOpp,
                                avgInteractionsPerAmountWon:avgInteractionsPerAmountWon,
                                renewalOppsNext90Days:renewalOppsNext90Days,
                                renewalOppsAmountNext90Days:renewalOppsAmountNext90Days,
                                avgDaysToCloseOpp:avgDaysToCloseOpp,
                                maxDaysToCloseOpp:maxDaysToCloseOpp?maxDaysToCloseOpp:1,
                                minDaysToCloseOpp:minDaysToCloseOpp?minDaysToCloseOpp:1,
                                wonOpps:wonOpps,
                                interactionsForWonOpp:interactionsForWonOpp,
                                daysToCloseOpp:daysToCloseOpp,
                                opps:opps
                            }
                        })
                    });

                })
            });
        })
    })

})

router.get('/reports/region', common.isLoggedInUserToGoNext, function(req, res) {

    var userId = common.getUserId(req.user);

    common.getProfileOrStoreProfileInSession(common.castToObjectId(userId),req,function(userProfile) {
        var companyId = userProfile ? userProfile.companyId : null;

        var forUserEmailId = [userProfile.emailId];
        if (req.query.forUserEmailId) {
            forUserEmailId = req.query.forUserEmailId.split(",");
        }

        var projection = {
            usersWithAccess:0,
            notes:0,
            lastStageUpdated:0,
            BANT:0,
            isClosed:0,
            isWon:0,
            salesforceContactId:0,
            masterData:0,
            renewed:0,
            closeReasonDescription:0,
            decisionMakers:0,
            influencers:0,
            partners:0,
            closeReasons:0,
            mobileNumber:0
        }

        var findQuery = {
            companyId:common.castToObjectId(companyId),
            userEmailId:{$in:forUserEmailId}
        }

        if(!req.query.startDate && !req.query.endDate){
            findQuery.closeDate = {
                $gte: new Date(moment().subtract(90,'days')),
            }
        }

        if(req.query.startDate && req.query.endDate){
            findQuery.closeDate = {
                $gte: new Date(req.query.startDate),
                $lte: new Date(moment(req.query.endDate).endOf("day"))
            }
        }

        if(req.query.startDate && !req.query.endDate){
            findQuery.closeDate = {
                $gte: new Date(req.query.startDate)
            }
        }

        if(!req.query.startDate && req.query.endDate){
            findQuery.closeDate = {
                $lte: new Date(req.query.endDate)
            }
        }

        if(req.query.stage){
            findQuery.relatasStage = req.query.stage;
        };

        oppManagementObj.getOppByCustomQuery(findQuery,projection,function (err,opps) {

            var groupedOpps = [];

            if(!err && opps && opps.length>0){
                groupedOpps = _
                    .chain(opps)
                    .groupBy(function (op) {
                        var loc = "0/0";

                        if(op.geoLocation && (op.geoLocation.lat || op.geoLocation.lat<0)){
                            loc = parseFloat(op.geoLocation.lat.toFixed(4))+"/"+parseFloat(op.geoLocation.lng.toFixed(4))
                        }

                        return loc;
                    })
                    .map(function(values, key) {
                        var pipeline = 0,
                            won = 0,
                            city = "India",
                            lost = 0,
                            total = 0,
                            minWon = Infinity,
                            maxWon = -Infinity;

                        if(values.length>0){
                            _.each(values,function (va) {
                                if(values[0].geoLocation && values[0].geoLocation.town){
                                    city = values[0].geoLocation.town;
                                }

                                if(va.relatasStage == "Close Won"){
                                    won = won+va.amount
                                    if(va.amount>maxWon){
                                        maxWon = va.amount
                                    }

                                } else if(va.relatasStage == "Close Lost"){
                                    lost = lost+va.amount

                                    if(va.amount<minWon){
                                        minWon = va.amount
                                    }
                                } else {
                                    pipeline = pipeline+va.amount
                                };

                                total = total+va.amount;
                            })
                        }

                        return {
                            loc: key,
                            // city: city,
                            city: key,
                            lat: parseFloat(key.split("/")[0]),
                            lon: parseFloat(key.split("/")[1]),
                            Pipeline: pipeline,
                            Won: won,
                            Lost: lost,
                            total: total,
                            maxWon:maxWon,
                            minWon:minWon,
                            opps:values
                        }
                    })
                    .value();
            }

            res.send(groupedOpps);
        });
    })

});

router.get('/reports/contact/details', common.isLoggedInUserToGoNext, function(req, res) {

    var userId = common.getUserId(req.user);

    common.getProfileOrStoreProfileInSession(common.castToObjectId(userId),req,function(userProfile) {
        var companyId = userProfile ? userProfile.companyId : null;

        var emailIds = [];
        var forUserEmailId = [userProfile.emailId];

        if (req.query.emailIds) {
            emailIds = req.query.emailIds.split(",");
        }
        if (req.query.forUserEmailId) {
            forUserEmailId = req.query.forUserEmailId.split(",");
        }

        async.parallel([
            function (callback) {
                oppManagementObj.opportunitiesForContactsAndUsers(common.castToObjectId(companyId),forUserEmailId,emailIds,callback)
            },
            // function (callback) {
            //     interactionManagementObj.lastInteractionDateForUsersContacts(common.castToObjectId(companyId),forUserEmailId,emailIds,callback)
            // }
            ], function (errors,data) {

            var obj = {},interactions = [],opps = [];
            if(!errors){
                opps = data[0];
                interactions = data[1];
                _.each(opps,function (op) {
                    obj[op._id.owner+op._id.emailId] = {
                        amount:op.amount,
                        lastInteractedDate:""
                    }
                });

                // _.each(interactions,function (int) {
                //     if(!obj[int._id.owner+int._id.emailId]){
                //         obj[int._id.owner+int._id.emailId] = {}
                //     }
                //     obj[int._id.owner+int._id.emailId].lastInteractedDate = int.lastInteractedDate;
                // });
            }

            res.send(obj);
        });
    })
})

router.get('/reports/contacts', common.isLoggedInUserToGoNext, function(req, res) {

    var userId = common.getUserId(req.user);

    common.getProfileOrStoreProfileInSession(common.castToObjectId(userId),req,function(userProfile) {
        var companyId = userProfile ? userProfile.companyId : null;

        var forUserEmailId = [userProfile.emailId];
        if (req.query.forUserEmailId) {
            forUserEmailId = req.query.forUserEmailId.split(",");
        }

        var projection = {
            lat:1,
            lng:1,
            personEmailId:1,
            location:1,
            ownerEmailId:1,
            contactRelation:1,
            designation:1,
            mobileNumber:1
        }

        var findQ = { ownerEmailId: {$in:forUserEmailId }}

        if(req.query.contactType && req.query.contactType.toLowerCase() != "cxo" && req.query.contactType.toLowerCase() != "influencer" && req.query.contactType.toLowerCase() != "decision maker"){
            findQ["contactRelation.prospect_customer"] = req.query.contactType.toLowerCase();
        }

        if(req.query.contactType && req.query.contactType.toLowerCase() == "contact"){
            findQ["contactRelation.prospect_customer"] = {$nin:["prospect","lead","customer"]};
        }

        if(req.query.contactType && req.query.contactType.toLowerCase() == "influencer"){
            findQ["contactRelation.influencer"] = true;
        }

        if(req.query.contactType && req.query.contactType.toLowerCase() == "decision maker"){
            findQ["contactRelation.decision_maker"] = true;
        }

        if(req.query.contactType && req.query.contactType.toLowerCase() == "cxo"){
            findQ["contactRelation.cxo"] = true;
        }

        userManagementObj.getContacts(findQ,projection,function (err,users) {

            var groupedOpps = [];
            var contacts = [];

            if(!err && users && users.length>0){
                users.forEach(function (co) {
                    if((co.lat || co.lat<0)){
                        contacts.push({
                            lat:parseFloat(co.lat.toFixed(4)),
                            lng:parseFloat(co.lng.toFixed(4)),
                            location:co.location,
                            ownerEmailId:co.ownerEmailId,
                            personEmailId:co.personEmailId,
                            contactRelation:co.contactRelation,
                            designation:co.designation,
                            mobileNumber:co.mobileNumber
                        });
                    }
                });

                if(!err && contacts && contacts.length>0){
                    groupedOpps = _
                        .chain(contacts)
                        .groupBy(function (op) {
                            var loc = "0/0";

                            if((op.lat || op.lat<0)){
                                loc = op.lat+"/"+op.lng
                            }

                            return loc;
                        })
                        .map(function(values, key) {

                            return {
                                loc: key,
                                city: key,
                                lat: parseFloat(key.split("/")[0]),
                                lon: parseFloat(key.split("/")[1]),
                                contacts:values,
                                count:values.length
                            }
                        })
                        .value();
                }
            }

            res.send(groupedOpps);
        });
    })

});

router.get('/opp/last/cached', common.isLoggedInUserToGoNext, function(req, res){

    bqMetaData.findOne({collectionName:"opportunities"}).sort({_id:-1}).limit(1).exec(function(err,result){

        var date = ""
        if(!err && result && result.lastUpdatedDate){
            date = "Data as of "+moment(result.lastUpdatedDate).format("DD MMM YYYY h:mm:ss A");
        }

        res.send(date);
    });
});

function getAllCompanyUsers(companyId,callback){
    userManagementObj.getTeamMembers(companyId, function(err, teamData) {
        callback(err,teamData)
    })
}

function buildQuery(filtersApplied){

    var groupedFilter = _
        .chain(filtersApplied)
        .groupBy('type')
        .map(function(value, key) {
            var obj = {};

            if(key === "geoLocation"){
                obj[key+".zone"] = {$in:_.pluck(value,"name")}
            } else if(key === "account"){
                obj["contactEmailId"] = {$in:_.map(value,function (el) {
                        if(el.name !== "Others"){return new RegExp(el.name, "i");}
                    })
                }
            } else if(key === "closeDate"){
                obj["closeDate"] = {
                    $gte: new Date(value[0].start),
                    $lte: new Date(value[0].end)
                }

            } else {
                obj[key] = {$in:_.pluck(value,"name")}
            }
            return obj;
        })
        .value();

    return groupedFilter;
}

function getOppsForThisQuarter(companyId,userId,userEmailId,filters,fyMonth,fyRange,allQuarters,forUsers,selectedPortfolio,callback) {

    var qStart = allQuarters[allQuarters.currentQuarter].start;
    var qEnd = allQuarters[allQuarters.currentQuarter].end;
    var listOfUsers = [userEmailId]
    var redisKey = String(companyId)+"oppsForReports";
    var filterKey = String(userId)+"filtersApplied"
    var userEmailIdKey = String(companyId)+"userEmailId"

    if(filters){

        var usersExist = false;
        _.each(filters,function (fl) {
            if(fl.type === "userEmailId"){
                usersExist = true;
            }
        })

        if(!usersExist) {
            if(forUsers && forUsers.length>0){
                _.each(forUsers,function (user) {
                    filters.push({
                        userEmailId:user,
                        name:user,
                        type:"userEmailId"
                    })
                })
            } else {

                filters.push({
                    userId:common.castToObjectId(userId),
                    name:common.castToObjectId(userId),
                    type:"userId"
                })
            }
        }

        setCache(filterKey,filters); //This is for download.

        oppManagementObj.getOppByFilters(companyId,filters,qStart,qEnd,selectedPortfolio,userEmailId,function (err,result) {
            if(err){
                console.log('Error in getOppByFilters():getOppByFilters:opportunitiesManagement ',err );
            }
            //Cache for report downloads;
            // setCache(String(companyId)+"oppsForDownload",result);
            callback(null,result)
        });
    } else {
        setCache(userEmailIdKey,userEmailId);
        getCache(redisKey,function (data) {
            if(data){

                if(userEmailId){
                    callback(null,data[userEmailId])
                } else {
                    var opps = []
                    for(var key in data){
                        if(_.includes(forUsers,key)){
                            opps.push(data[key])
                        }
                    }

                    setCache(String(userId)+"forUsers",forUsers);
                    callback(null,_.flatten(opps))
                }
            } else {

                oppManagementObj.getOppForSingleByDate(companyId,forUsers,qStart,qEnd,selectedPortfolio,function (err,result) {
                    if(err){
                        console.log('Error in getOppForSingleByDate():getOppForSingleByDate:opportunitiesManagement ',err );
                    }

                    if(!err && result && result.length>0){
                        callback(null,_.flatten(result))

                    } else {
                        callback(null,[])
                    }
                });
            }
        })
    }
}

function setCache(key,data){
    //Storing for 45 min
    redisClient.setex(key, 2700, JSON.stringify(data));
}

function getCache(key,callback){

    redisLogs.info("Reports ",key);

    if(!_.includes(key, 'null')){
        redisClient.get(key, function (error, result) {
            try {
                var data = JSON.parse(result);
                // callback(data)
                callback(null)
            } catch (err) {
                callback(null)
            }
        });
    } else {
        callback(null)
    }

}

function accessControlSettings(regionAccess,productAccess,verticalAccess,companyId) {

    var accessControlQuery = [];
    
    if(regionAccess && regionAccess.length>0 && companyId){
        accessControlQuery.push({"geoLocation.zone":{$in:regionAccess}})
    } else {
        accessControlQuery.push({"geoLocation.zone":"no_access"})
    }

    if(productAccess && productAccess.length>0 && companyId){
        accessControlQuery.push({"productType":{$in:productAccess}})
    } else {
        accessControlQuery.push({"productType":"no_access"})
    }

    if(verticalAccess && verticalAccess.length>0 && companyId){
        accessControlQuery.push({vertical:{$in:verticalAccess}})
    } else {
        accessControlQuery.push({vertical:"no_access"})
    }

    if(companyId){
        accessControlQuery.push({companyId:companyId})
    }

    return accessControlQuery;

}

function getUserAccessControl(userIds,liu,callback) {

    var key = "oppUserAccessControl"+String(liu);

    getCache(key,function (data) {

        if(data){
            getAccessDetails(data.oppOwnershipDetails,data.companyDetails,callback)
        } else {
            userManagementObj.getUserOppOwnerDetails(userIds,function (errO,oppOwnershipDetails) {

                if(oppOwnershipDetails[0].companyId){

                    company.findCompanyById(oppOwnershipDetails[0].companyId,function (companyDetails) {
                        setCache(key,{oppOwnershipDetails:oppOwnershipDetails,companyDetails:companyDetails})
                        getAccessDetails(oppOwnershipDetails,companyDetails,callback)
                    });
                } else {
                    callback(null,null,null,null,false,false)
                }
            });
        }

    })
}

function getAccessDetails(oppOwnershipDetails,companyDetails,callback) {

    var regionAccess = companyDetails.geoLocations && companyDetails.geoLocations.length>0?oppOwnershipDetails[0].regionOwner:null;
    var productAccess = companyDetails.productList && companyDetails.productList.length>0?oppOwnershipDetails[0].productTypeOwner:null;
    var verticalAccess = companyDetails.verticalList && companyDetails.verticalList.length>0?oppOwnershipDetails[0].verticalOwner:null;
    var netGrossMarginReq = companyDetails.netGrossMargin?companyDetails.netGrossMargin:false;
    var companyTargetAccess = oppOwnershipDetails[0].companyTargetAccess?oppOwnershipDetails[0].companyTargetAccess:false;

    callback(regionAccess,productAccess,verticalAccess,oppOwnershipDetails[0].companyId,netGrossMarginReq,companyTargetAccess,companyDetails)
}

router.post('/reports/big/query', common.isLoggedInUserToGoNext, function(req, res){

    var userId = common.getUserId(req.user);
    common.getProfileOrStoreProfileInSession(common.castToObjectId(userId),req,function(userProfile) {

        var companyId = userProfile ? common.castToObjectId(userProfile.companyId) : null;

        common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters) {
            oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

                var userEmailIds = [userProfile.emailId];
                var userIds = [userProfile._id];
                if(req.body.selectedPortfolio){
                    userEmailIds = _.map(req.body.selectedPortfolio,"ownerEmailId");
                    userIds = _.map(req.body.selectedPortfolio,"userId");
                } else if(req.body.userEmailIds) {
                    userEmailIds = req.body.userEmailIds;
                    userIds = req.body.userIds;
                }

                var primaryCurrency = "USD";
                var currenciesObj = {};

                companyDetails.currency.forEach(function (el) {
                    currenciesObj[el.symbol] = el;
                    if(el.isPrimary){
                        primaryCurrency = el.symbol;
                    }
                });

                var qtrStart = allQuarters[allQuarters.currentQuarter].start;
                var qtrEnd = allQuarters[allQuarters.currentQuarter].end;

                if(req.body.start){
                    qtrStart = req.body.start
                }

                if(req.body.end){
                    qtrEnd = req.body.end
                }

                oppManagementObj.loadBQData(companyId
                    ,userEmailIds
                    ,common.castListToObjectIds(userIds)
                    ,new Date(qtrStart)
                    ,new Date(qtrEnd)
                    ,primaryCurrency
                    ,currenciesObj
                    ,new Date(fyRange.start)
                    ,new Date(fyRange.end)
                    ,oppCommitObj
                    ,req.body.selectedPortfolio
                    ,req.body.allPortfolios
                    ,req.body.usersDictionary
                    ,userProfile
                    ,function(err,data){
                    res.send({
                        data:data,
                        allQuarters: {
                            start:qtrStart,
                            end:qtrEnd,
                        },
                        fyRange:fyRange,
                        err:err
                    })
                });
            })
        })
    });
});

module.exports = router;
