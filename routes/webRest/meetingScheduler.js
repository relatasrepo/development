
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var _ = require("lodash");
var async = require("async")

var CommonUtility = require('../../common/commonUtility');
var meetingScheduler = require('../../databaseSchema/meetingScheduler').meetingScheduler;

var common = new CommonUtility();

router.get('/meetings/scheduler', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("meetingScheduler/index");
});

router.post('/meetings/scheduler/save', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(req.body && req.body.length>0){

            var data = req.body.map(function (el) {

                var notes = [];
                if(el.notes && el.notes.length>0){
                    notes.push({
                        text: el.notes,
                        fromEmailId: user.emailId,
                        date: new Date()
                    })
                };

                var meetingDate = moment(String(el.date));
                if(!meetingDate.isValid()){
                    meetingDate = moment(el.date,"DD/MM/YYYY")
                }

                console.log(meetingDate);

                return {
                    companyId: common.castToObjectId(user.companyId),
                    fromEmailId: user.emailId,
                    location: {
                        address: el.address,
                        lat: null,
                        lng: null
                    },
                    contacts: el.contact.split(","),
                    meetingDate: new Date(meetingDate),
                    uploadedDate: new Date(),
                    toEmailId: el.toEmailId,
                    account: el.account,
                    status:null,
                    notes:notes
                };
            });

            console.log(JSON.stringify(data,null,1));

            meetingScheduler.collection.insert(data,function (err,result) {
                if(!err && result){
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": null
                    })
                } else {
                    res.send({
                        "SuccessCode": 0,
                        "Message": "",
                        "ErrorCode": 1,
                        "Data": null
                    })
                }
            });
        } else {
            res.send({
                "SuccessCode": 0,
                "Message": "",
                "ErrorCode": 1,
                "Data": null
            })
        }
    });

});

router.post('/meetings/scheduler/get/today', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){

        var query = {
            companyId:common.castToObjectId(user.companyId),
            isDeleted:{$ne:true},
            meetingDate: {
                $gte: new Date(moment().startOf("day")),
                $lte: new Date(moment().endOf("day")),
            },
            $or:[{fromEmailId: user.emailId},{toEmailId: user.emailId}]
        };

        meetingScheduler.find(query).exec(function (err,meetings) {
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": meetings
            })
        })
    });

});

router.post('/meetings/scheduler/delete', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){

        var bulk = meetingScheduler.collection.initializeUnorderedBulkOp();
        _.each(req.body._ids,function (el) {
            bulk.find({
                companyId:common.castToObjectId(user.companyId),
                _id:common.castToObjectId(el)}).update({$set:{isDeleted:true}});
        });

        bulk.execute(function(err, result) {

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": result
            })
        });
    });

});

router.post('/meetings/scheduler/get/by/filters', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){

        var query = {
            companyId:common.castToObjectId(user.companyId),
            isDeleted:{$ne:true},
            meetingDate: {
                $gte: new Date(moment().startOf("day")),
                $lte: new Date(moment().endOf("day")),
            },
            $or:[{fromEmailId: user.emailId},{toEmailId: user.emailId}]
        };

        if(req.body && req.body.length>0){
            query = {
                companyId:common.castToObjectId(user.companyId),
                isDeleted:{$ne:true}
            };

            _.each(req.body,function (el) {

                if(el.type == "meetingDate"){
                    if(el.startDate && el.endDate){
                        query.meetingDate = {
                            $gte: new Date(moment(el.startDate).startOf("day")),
                            $lte: new Date(moment(el.endDate).endOf("day")),
                        }
                    } else if(el.startDate && !el.endDate){
                        query.meetingDate = {
                            $gte: new Date(moment(el.startDate).startOf("day"))
                        }
                    } else if(!el.startDate && el.endDate){
                        query.meetingDate = {
                            $lte: new Date(moment(el.endDate).endOf("day"))
                        }
                    } else {
                        query.meetingDate = {
                            $gte: new Date(moment().startOf("day")),
                            $lte: new Date(moment().endOf("day")),
                        }
                    }
                }

                if(el.type == "toEmailId"){
                    query.toEmailId = {$in:_.compact(el.value)}
                } else {
                    query.$or = [{fromEmailId: user.emailId},{toEmailId: user.emailId}]
                }

                if(el.type == "status"){
                    query.status = {$in:el.value}
                }
            });
        }

        meetingScheduler.find(query).exec(function (err,meetings) {
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": meetings
            })
        })
    });

});

router.post('/meetings/scheduler/update', common.isLoggedInUserOrMobile, function(req, res) {

    if(req.body && req.body._id){

        var findQuery = {
            companyId:common.castToObjectId(req.body.companyId),
            _id:common.castToObjectId(req.body._id)
        };

        var updateQuery = {
            $set:{
                status:req.body.status,
                notes:req.body.notes,
                "location.lat": req.body.lat,
                "location.lng": req.body.lng
            }
        };

        meetingScheduler.update(findQuery,updateQuery).exec(function (err,results) {
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": results
            })
        })
    } else {
        res.send({
            "SuccessCode": 0,
            "Message": "",
            "ErrorCode": 1,
            "Data": null
        })
    }

});

router.get('/meetings/scheduler/templates', common.isLoggedInUserToGoNext, function(req, res) {
    var fileName = 'meetingsSchedulerTemplates.xlsx';
    var format = [];

    format.push({
        "toEmailId": "Meeting Owner",
        "account": "Customer account",
        "contact": "Comma separated contact emailIds/phone numbers.",
        "address": "Customer address",
        "date": "Meeting date. Format: DD/MM/YYYY",
        "status": "Meeting status. Can be blank",
        "notes": "Meeting notes. Can be blank",
    });

    res.xls(fileName, format);

});


module.exports = router;