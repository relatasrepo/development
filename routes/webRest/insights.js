/**
 * Created by naveen on 1/27/16.
 */

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');
var jwt = require('jwt-simple');
var _ = require("lodash");
var async = require("async")

var allUsers = require('../../databaseSchema/userManagementSchema').User;
var dealsAtRiskMeta = require('../../databaseSchema/userManagementSchema').dealsAtRiskMeta;
var insightsAdminReports = require('../../databaseSchema/insightsAdminReports').insightsAdminReports;
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactionManagement = require('../../dataAccess/interactionManagement');
var commonUtility = require('../../common/commonUtility');
var insightsManagement = require('../../dataAccess/insightsManagement');
var ActionItemsManagement = require('../../dataAccess/actionItemsManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var opportunityManagement = require('../../dataAccess/opportunitiesManagement');
var userOpenSlots = require('../../common/userOpenSlots');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var OppWeightsClass = require('../../dbCache/company.js')
var OppCommitManagement = require('../../dataAccess/oppCommitAccess');

var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var meetingClassObj = new meetingManagement();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionManagementObj = new interactionManagement();
var insightsManagementObj = new insightsManagement();
var actionItemsManagementObj = new ActionItemsManagement();
var oppManagementObj = new opportunityManagement();
var userOpenSlotsObj = new userOpenSlots();
var company = new companyClass();
var oppWeightsObj = new OppWeightsClass();
var oppCommitObj = new OppCommitManagement();

var winstonLog = require('../../common/winstonLog');
var logLib = new winstonLog();

var statusCodes = errorMessagesObj.getStatusCodes();

router.get('/insights/losing/touch/details', /*common.isLoggedInUserToGoNext*/ common.isLoggedInUserOrMobile, function(req, res) {

    if(req.headers.token) {
        common.storeMobileSessionSpecInfo(req,function(){
            res.render("insights/insightsLosingTouchDetails");
        })
    }else{
        res.render("insights/insightsLosingTouchDetails");
    }

});

router.get('/insights/company/relationship/risk/details', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("insights/insightsCRRDetails");
});

router.get('/insights/your/responses/pending/details', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("insights/insightsResponsePendingDetails");
});

router.get('/insights/mail/action', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("insights/insightsMailAction");
});

router.get('/insights/mails', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("insights/insightsMail");
});

router.get('/insights/mailsTable', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("insights/mailsTable");
});

router.get('/insights/actionBoard', common.isLoggedInUserToGoNext, function(req, res) {
    res.render("insights/actionBoard");
});

router.get('/people/to/connect', common.isLoggedInUserOrMobile, function(req, res) {
    if(req.headers.token){
        common.storeMobileSessionSpecInfo(req,function(){
            res.render("insights/peopleToConnect")
        })
    }else{
        res.render("insights/peopleToConnect");
    }
});

router.get('/insights/mail/actions/meta', common.isLoggedInUserOrMobile, function(req, res) {

    var userId;
    userId = common.getUserIdFromMobileOrWeb(req)
    var from = moment().subtract(14, "days")

    interactionManagementObj.getNumberOfImportantMails(common.castToObjectId(userId),from,function (err,mails) {

        if(!err && mails && mails.length>0){

            var filteredMails = filterByOnlyRecentInteractionThread(mails)

            var threadIds = _.pluck(filteredMails,"emailThreadId");

            var threadsHaveLatestReplies = [],tempMails = [];

            interactionManagementObj.checkLiuRespondedToLastEmail(common.castToObjectId(userId),from,threadIds,function (err,prevEmails) {

                var important = 0,onlyToMe = 0,followUp = 0,positive = 0,negative = 0;
                var emailContentIds = [];

                _.each(prevEmails,function (el) {
                    if(el.emailId != el.ownerEmailId && el.action == "receiver" ){
                        threadsHaveLatestReplies.push(el.emailThreadId)
                    }
                })

                _.each(filteredMails,function (el) {
                    if(!_.includes(threadsHaveLatestReplies,el.emailThreadId)){
                        tempMails.push(el)
                    }
                })

                _.each(tempMails,function (el) {

                    emailContentIds.push(el.emailContentId);

                    if(el.trackInfo && el.trackInfo.trackResponse){
                        followUp++;
                    }

                    if(el.toCcBcc == 'me'){
                        onlyToMe++;
                    }

                    if((el.eAction && el.eAction == 'important') || el.importance>0){
                        important++;
                    }

                    if(el.sentiment == "Positive"){
                        positive++;
                    }

                    if(el.sentiment == "Negative"){
                        negative++;
                    }
                })

                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data:{
                        important:important,
                        onlyToMe:onlyToMe,
                        followUp:followUp,
                        positive:positive,
                        negative:negative,
                        emailContentIds:emailContentIds
                    }
                })
            });
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }

    });
});

function filterByOnlyRecentInteractionThread(interactions){
    var filteredInteractions = _
        .chain(interactions)
        .groupBy('emailThreadId')
        .map(function(value, key) {
            return _.max(value,"interactionDate")
        })
        .value();

    return filteredInteractions;
}

router.post('/insights/mail/update/not/important',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var emailId = req.body.emailId;
    var refId = req.body.refId;

    interactionManagementObj.ignoreMail(common.castToObjectId(userId),emailId,refId,function (err,results) {
        if (!err && results) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    });

});

router.get('/insights/mail/actions', common.isLoggedInUserOrMobile, function(req, res) {

    var userId;
    userId = common.getUserIdFromMobileOrWeb(req)

    var limit = 10;
    var skip = req.query.skip?parseInt(req.query.skip):0;
    var from = moment().subtract(14, "days")
    var filter = req.query.filter?req.query.filter:null;
    var emailContentIds = []
    if(filter){
        emailContentIds = req.query.emailContentIds.split(",");
    }
    
    interactionManagementObj.getMlAnalyzedMails(common.castToObjectId(userId),skip,limit,from,filter,function (err,interactions) {

        if(!err && interactions && interactions.length>0){

            var contacts = [],tempMails = [];
            var filteredMails = filterByOnlyRecentInteractionThread(interactions)

            var threadIds = _.pluck(filteredMails,"emailThreadId");

            var threadsHaveLatestReplies = [];

            interactionManagementObj.checkLiuRespondedToLastEmail(common.castToObjectId(userId),from,threadIds,function (err,prevEmails) {
                _.each(prevEmails,function (el) {
                    if(el.emailId != el.ownerEmailId && el.action == "receiver" ){
                        threadsHaveLatestReplies.push(el.emailThreadId)
                    }
                })

                _.each(filteredMails,function (el) {
                    if(!_.includes(threadsHaveLatestReplies,el.emailThreadId)){
                        tempMails.push(el)
                    }
                })
                
                if(filter){
                    tempMails = tempMails.filter(function (el) {
                        return _.includes(emailContentIds,el.emailContentId)
                    })
                }

                //Pagination
                var totalRecords = tempMails.length,
                    pageSize = 10,
                    pageCount = Math.round(totalRecords/pageSize),
                    currentPage = 1,
                    paginatedInteractions = [],
                    finalList = []

                var interactionsCount = [
                    {
                        "_id": null,
                        "count": totalRecords
                    }
                ]

                //split list into groups
                while (tempMails.length > 0) {
                    paginatedInteractions.push(tempMails.splice(0, pageSize));
                }

                //set current page if specified as get variable (eg: /?page=2)

                if (typeof skip !== 'undefined' && skip !== 0) {
                    skip = parseInt(String(skip).substring(0,1))
                    currentPage = currentPage+skip;
                }

                finalList = paginatedInteractions[+currentPage - 1];

                _.each(finalList,function (el) {
                    contacts.push(el.emailId)

                    if(el.to && el.to.length>0){
                        var emailIds = _.pluck(el.to,"emailId")
                        contacts = contacts.concat(emailIds)
                    }

                    if(el.toList && el.toList.length>0){
                        var emailIds2 = _.pluck(el.toList,"emailId")
                        contacts = contacts.concat(emailIds2)
                    }

                    if(el.cc && el.cc.length>0){
                        var emailIdsCc = _.pluck(el.cc,"emailId")
                        contacts = contacts.concat(emailIdsCc)
                    }
                });

                contactObj.getContactsByEmailId(common.castToObjectId(userId),_.uniq(contacts),function (errC,contacts) {

                    var contactsObj = {};

                    _.each(contacts,function (el) {
                        contactsObj[el.personEmailId] = el;
                    });

                    var interactionsArray = [];

                    _.each(finalList,function (el) {

                        if(el.to && el.to.length>0){

                            _.each(el.to,function (to) {
                                if(contactsObj[to.emailId]){
                                    to.contact = contactsObj[to.emailId]
                                }
                            })
                        }

                        if(el.toList && el.toList.length>0){

                            _.each(el.toList,function (to) {
                                if(contactsObj[to.emailId]){
                                    to.contact = contactsObj[to.emailId]
                                }
                            })
                        }

                        if(el.cc && el.cc.length>0){

                            _.each(el.cc,function (cc) {
                                if(contactsObj[cc.emailId]){
                                    cc.contact = contactsObj[cc.emailId]
                                }
                            })
                        }

                        if(contactsObj[el.emailId]){
                            el.contact = contactsObj[el.emailId]
                        }

                        if(el.trackInfo && el.trackInfo.trackResponse){
                            el.type = "Follow up"
                        }

                        if(el.toCcBcc == 'me'){
                            el.type = "Only to me"
                        }

                        if((el.eAction && el.eAction == 'important') || el.importance>0){
                            el.type = "Important"
                        }

                        if(el.type == "Follow up"){
                            interactionsArray.push(el)
                        } else {
                            interactionsArray.push(el)
                        }

                        if(el.to && el.to.length>0){
                            el.to = el.to.concat(el.toList)
                        } else {
                            el.to = el.toList;
                        }

                    });

                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data:interactionsArray,
                        interactionsCount:interactionsCount,
                        pageCount:pageCount
                    })
                })
            })

        } else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data:[]
            })
        }

    })
});

router.post('/people/to/connect/ignore', common.isLoggedInUserOrMobile, function(req, res) {

    var userId;
    userId = common.getUserIdFromMobileOrWeb(req)

    var contactId = common.castToObjectId(req.body.contactId);
    var interactionId = null;
    if(req.body.update == 'notify'){
        interactionId = common.castToObjectId(req.body.interactionId)
    }
    insightsManagementObj.peopleToConnectIgnore(common.castToObjectId(userId), contactId, req.body.update, interactionId, function(err, result) {
        if (!err && result) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })
})

router.post('/insights/your/response/pending/ignore', common.isLoggedInUserOrMobile, function(req, res) {

    var userId;
    userId = common.getUserIdFromMobileOrWeb(req)
    var interactionId = null;
    var emailThreadId = null;
    if(req.body.emailType == "single")
        interactionId = common.castToObjectId(req.body.interactionId)
    else
        emailThreadId = req.body.emailThreadId;

    insightsManagementObj.yourResponsePendingIgnore(common.castToObjectId(userId), interactionId, emailThreadId, function(err, result) {
        if (!err && result) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })
})

router.get('/insights/contacts/by/companies', common.isLoggedInUserOrMobile, function(req, res) {

    var uId = common.getUserIdFromMobileOrWeb(req);
    var userId = common.castToObjectId(uId);
    var companies = [];
    if(typeof req.query.companies == 'string')
        companies.push(req.query.companies);
    else
        companies = req.query.companies;
    allUsers.aggregate(
        [{$match:{_id:userId}},
        {$unwind:"$contacts"},
        {$match: {"contacts.account.name": {$in:companies} }},
        {$project:{
            name:"$contacts.personName",
            company:"$contacts.account.name",
            contactId:"$contacts._id",
            value:"$contacts.account.value.inProcess"
        }
        }]
        ,function(err, result){
            if (!err && result) {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data:result
                })
            } else {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                    Data:[]
                })
            }
    });
});

router.get('/people/to/connect/info', common.isLoggedInUserOrMobile, function(req, res) {
    var timezone;
    var userId;
    userId = common.getUserIdFromMobileOrWeb(req)

    allUsers.find({ _id: common.castToObjectId(userId)}, { timezone: 1 }, function(err, user) {
        if (err || user.length == 0)
            res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
        else {
            if (user[0].timezone && user[0].timezone.name)
                timezone = user[0].timezone.name;
            else
                timezone = 'UTC';
            var dateMin = moment().tz(timezone);
            dateMin.hour(0);
            dateMin.minute(0);
            dateMin.second(0);
            dateMin.millisecond(0);
            var dateMax = moment().tz(timezone);

            dateMax.hour(23);
            dateMax.minute(59);
            dateMax.second(59);
            dateMax.millisecond(0);
            
            insightsManagementObj.peopleToConnect(common.castToObjectId(userId), dateMin, dateMax, timezone, function(err, result) {
                if (!err && result) {
                    if (req.query.infoFor == 'dashboard')
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            Data: { peopleToConnect: result.length }
                        })
                    else
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            Data: result
                        })

                } else {
                    res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                }
            });
        }
    })
});

router.get('/insights/losing/touch/info/', common.isLoggedInUserOrMobile, function(req, res) {

    var limit = req.query.limit;
    var threshold = 50;
    var userId;

    var insightsFor = []
    insightsFor = req.query.hierarchyList.split(',')

    userId = common.getUserIdFromMobileOrWeb(req)
    var docName = "losingTouch";


    getAllCompanyUsers(common.castToObjectId(userId),null,null,function (err,teamData) {

        var companyMembers = _.pluck(teamData,"_id");

        common.getProfileOrStoreProfileInSession(common.castToObjectId(userId), req, function(userProfile) {
            insightsManagementObj.getLosingTouchSelf(common.castListToObjectIds(insightsFor), docName, companyMembers,function(err,aggregatedNumbers) {

                insightsManagementObj.getLosingTouchFullDetails(common.castListToObjectIds(insightsFor), threshold, limit, function(result) {

                    var dateMin = new Date();
                    var contactsPersonIdList = _.map(
                        _.where(result, { relatasUser: true }),
                        function(data) {
                            return data.personId;
                        }
                    );

                    meetingClassObj.userNextWeekTravelingTo(contactsPersonIdList, dateMin, function(meetings) {
                        var meetingsLoc = meetings.map(function(value) {
                            var tempArr = [];

                            if (value.toList.length > 0) {
                                if (contactsPersonIdList)
                                    if (contactsPersonIdList.indexOf(value.toList[0].receiverId) > -1) {
                                        var obj = {
                                            meetingLocation: value.scheduleTimeSlots[0].location,
                                            personId: value.toList[0].receiverId,
                                            startTime: value.scheduleTimeSlots[0].start.date
                                        }
                                        tempArr.push(obj)
                                        //return obj;
                                    }
                            } else {
                                if (contactsPersonIdList)
                                    if (contactsPersonIdList.indexOf(value.senderId) > -1) {
                                        var obj = {
                                            meetingLocation: value.scheduleTimeSlots[0].location,
                                            personId: value.senderId,
                                            startTime: value.scheduleTimeSlots[0].start.date
                                        }
                                        tempArr.push(obj)
                                    }
                            }
                            return tempArr;
                        });

                        var userTempLoc = userProfile.location.split(',');

                        var obj = _.map(result, function(a) {
                            return _.extend(a, _.find(meetingsLoc, function(b) {
                                a.userBaseLocation = [];
                                a.userBaseLocation.push(userTempLoc[0]);
                                a.userBaseLocation.push(userProfile.currentLocation.city);
                                a.userFullName = userProfile.firstName + ' ' + userProfile.lastName;

                                return a.personId === b.personId;
                            }))
                        });

                        var emailIdArr = _.compact(_.pluck(result, 'personEmailId'));
                        var tempLen = result.length;
                        var mobileNumberArr = [];

                        for(var i=0;i<tempLen;i++){
                            if(!result[i].personEmailId && result[i].contactMobileNumber){
                                var mobileNumberTemp = result[i].contactMobileNumber.replace(/[^a-zA-Z0-9]/g,'')
                                mobileNumberArr.push(mobileNumberTemp)
                            }
                        }

                        var ltContacts = _.map(_.groupBy(result,"ownerEmailId"),function (val,user) {
                            return {
                                ownerEmailId:user,
                                contacts:_.pluck(val,"personEmailId")
                            }
                        });

                        contactObj.getContactsFavoriteCountMultipleUsers(common.castListToObjectIds(insightsFor),function (errUsersContacts,usersContactsList) {
                            // contactObj.getImportantContactsLtWith(common.castListToObjectIds(insightsFor),ltContacts,function (errUsersContacts,usersContactsList) {

                            interactionManagementObj.getLastInteractedDateByEmailAndMobileList(common.castToObjectId(userId), emailIdArr,mobileNumberArr, function(lastInteraction) {

                                var obj2 = _.map(obj, function(a) {
                                    return _.extend(a, _.find(lastInteraction, function(b) {
                                        if(a.personEmailId && b.emailId) {
                                            return b.emailId === a.personEmailId;
                                        } else if(a.contactMobileNumber && b.mobileNumber){
                                            return b.mobileNumber === a.contactMobileNumber;
                                        }
                                    }))
                                });

                                var rules = getRulesObj(obj2);
                                if(rules.length > 0) {
                                    insightsManagementObj.getRules(rules, function (suggestions) {

                                        var teamMembers = _.pluck(teamData,"emailId");
                                        var cleanArray = []

                                        _.each(suggestions,function (sg) {
                                            if(!_.contains(teamMembers, sg.contactEmailId)){
                                                cleanArray.push(sg)
                                            }
                                        });

                                        var data = [];

                                        var totalFavTemp = 0;

                                        _.each(cleanArray,function (co) {
                                            _.each(usersContactsList,function (el) {
                                                if(co.ownerEmailId == el._id){
                                                    _.each(el.contacts,function (item) {
                                                        if(item.personEmailId == co.contactEmailId){
                                                            totalFavTemp = el.contacts.length
                                                            data.push(co)
                                                        }
                                                    })
                                                }
                                            });
                                        });

                                        res.send({
                                            SuccessCode: 1,
                                            ErrorCode: 0,
                                            Data: {
                                                contacts: data,
                                                aggregatedNumbers: aggregatedNumbers
                                            },
                                            aggregatedNumbers: aggregatedNumbers,
                                            totalFavTemp:totalFavTemp
                                        });

                                    })
                                }
                                else{
                                    res.send({
                                        SuccessCode: 0,
                                        ErrorCode: 1,
                                        Data: {
                                            contacts: [],
                                            aggregatedNumbers: {selfTotalCount: 0, selfLosingTouch: 0}
                                        }
                                    });
                                }
                            });
                        });
                    });
                });
            });
        });
    });
})

router.get('/insights/losing/touch/info/by/relation', common.isLoggedInUserOrMobile, function(req, res) {

    var limit = req.query.limit;
    var threshold = 50;
    var insightsFor = [];
    var userId = common.getUserIdFromMobileOrWeb(req);
    var docName = "losingTouch";

    if (req.query.userIds)
        insightsFor = req.query.userIds.split(',')
    else {
        insightsFor = [userId]
    }

    getAllCompanyUsers(common.castToObjectId(userId),null,null,function (err,teamData) {

        var companyMembers = _.pluck(teamData,"_id");

        common.getProfileOrStoreProfileInSession(common.castToObjectId(userId), req, function(userProfile) {
            insightsManagementObj.getLosingTouchSelf(common.castListToObjectIds(insightsFor), docName, companyMembers,function(err,aggregatedNumbers) {

                insightsManagementObj.getLosingTouchFullDetails(common.castListToObjectIds(insightsFor), threshold, limit, function(result) {

                    var dateMin = new Date();
                    var contactsPersonIdList = _.map(
                        _.where(result, { relatasUser: true }),
                        function(data) {
                            return data.personId;
                        }
                    );

                    meetingClassObj.userNextWeekTravelingTo(contactsPersonIdList, dateMin, function(meetings) {
                        var meetingsLoc = meetings.map(function(value) {
                            var tempArr = [];

                            if (value.toList.length > 0) {
                                if (contactsPersonIdList)
                                    if (contactsPersonIdList.indexOf(value.toList[0].receiverId) > -1) {
                                        var obj = {
                                            meetingLocation: value.scheduleTimeSlots[0].location,
                                            personId: value.toList[0].receiverId,
                                            startTime: value.scheduleTimeSlots[0].start.date
                                        }
                                        tempArr.push(obj)
                                    }
                            } else {
                                if (contactsPersonIdList)
                                    if (contactsPersonIdList.indexOf(value.senderId) > -1) {
                                        var obj = {
                                            meetingLocation: value.scheduleTimeSlots[0].location,
                                            personId: value.senderId,
                                            startTime: value.scheduleTimeSlots[0].start.date
                                        }
                                        tempArr.push(obj)
                                    }
                            }
                            return tempArr;
                        });

                        var userTempLoc = userProfile.location?userProfile.location.split(','):"no_location_set";

                        var obj = _.map(result, function(a) {
                            return _.extend(a, _.find(meetingsLoc, function(b) {
                                a.userBaseLocation = [];
                                a.userBaseLocation.push(userTempLoc[0]);
                                a.userBaseLocation.push(userProfile.currentLocation.city);
                                a.userFullName = userProfile.firstName + ' ' + userProfile.lastName;

                                return a.personId === b.personId;
                            }))
                        });

                        var emailIdArr = _.compact(_.pluck(result, 'personEmailId'));
                        var tempLen = result.length;
                        var mobileNumberArr = [];

                        for(var i=0;i<tempLen;i++){
                            if(!result[i].personEmailId && result[i].contactMobileNumber){
                                var mobileNumberTemp = result[i].contactMobileNumber.replace(/[^a-zA-Z0-9]/g,'')
                                mobileNumberArr.push(mobileNumberTemp)
                            }
                        }

                        contactObj.getContactsFavoriteCountMultipleUsers(common.castListToObjectIds(insightsFor),function (errUsersContacts,usersContactsList) {
                        // contactObj.getImportantContactsLtWith(common.castListToObjectIds(insightsFor),ltContacts,function (errUsersContacts,usersContactsList) {

                            interactionManagementObj.getLastInteractedDateByEmailAndMobileList(common.castToObjectId(userId), emailIdArr,mobileNumberArr, function(lastInteraction) {

                                var obj2 = _.map(obj, function(a) {
                                    return _.extend(a, _.find(lastInteraction, function(b) {
                                        if(a.personEmailId && b.emailId) {
                                            return b.emailId === a.personEmailId;
                                        } else if(a.contactMobileNumber && b.mobileNumber){
                                            return b.mobileNumber === a.contactMobileNumber;
                                        }
                                    }))
                                });

                                var rules = getRulesObj(obj2);

                                if(rules.length > 0) {
                                    insightsManagementObj.getRules(rules, function (suggestions) {

                                        var teamMembers = _.pluck(teamData,"emailId");
                                        var cleanArray = [];
                                        var cleanArray2 = [];

                                        _.each(suggestions,function (sg) {
                                            if(!_.contains(teamMembers, sg.contactEmailId)){
                                                cleanArray.push(sg)
                                                cleanArray2.push(sg.contactEmailId)
                                            }
                                        });


                                        var data = [];

                                        var totalFavTemp = 0;

                                        _.each(cleanArray,function (co) {
                                            _.each(usersContactsList,function (el) {
                                                if(co.ownerEmailId == el._id){
                                                    _.each(el.contacts,function (item) {
                                                        if(item.personEmailId == co.contactEmailId){
                                                            totalFavTemp = el.contacts.length
                                                            data.push(co)
                                                        } else {
                                                            data.push(co)
                                                        }
                                                    })
                                                }
                                            });
                                        });

                                        data = _.uniq(data,"contactEmailId");
                                        data = _.uniq(data,"contactMobileNumber");

                                        res.send({
                                            SuccessCode: 1,
                                            ErrorCode: 0,
                                            Data: cleanArray,
                                            aggregatedNumbers: aggregatedNumbers,
                                            totalFavTemp:totalFavTemp
                                        });

                                    })
                                }
                                else{
                                    res.send({
                                        SuccessCode: 0,
                                        ErrorCode: 1,
                                        Data: {
                                            contacts: [],
                                            aggregatedNumbers: {selfTotalCount: 0, selfLosingTouch: 0}
                                        }
                                    });
                                }
                            });
                        });
                    });
                });
            });
        });
    });

})

router.get('/company/members', common.isLoggedInUserOrMobile, function(req, res) {

    var userId;
    userId = common.getUserIdFromMobileOrWeb(req)

    userManagementObj.findUserProfileByIdWithCustomFields(userId, { companyId: 1, companyName: 1, hierarchyPath: 1, emailId:1 }, function(error, data) {
        userManagementObj.getUserHierarchyWithoutLiu(userId, function(err, teamData) {

            if (!err) {

                var teamMembers = _.pluck(teamData, "_id");

                allUsers.find({ companyId: data.companyId, corporateUser: true }, { _id: 1, emailId: 1 }, function(err, allUsers) {

                    var company = _.reduce(allUsers, function(o, v) {
                        o[v.emailId] = v._id;
                        return o;
                    }, {});

                    if(teamMembers.length<=0){
                        teamMembers.push(userId)
                    }

                    if(Object.keys(company).length<=0){
                        company[data.emailId] = userId
                    }

                    var obj = {
                        userId: userId,
                        company: data.companyName,
                        companyId: data.companyId,
                        teamMembers: teamMembers,
                        companyMembers: company
                    }

                    if (!err) {
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            Data: obj
                        })
                    }
                })
            }
        });
    })
});

router.get('/insights/rules/', common.isLoggedInUserToGoNext, function(req, res) {

    var rulesFor2 = JSON.parse(req.query.rulesFor);
    var rulesFor = req.query.rulesFor;

    insightsManagementObj.getRules(rulesFor, function(result) {
        res.send('OK');
    })
});

router.get('/insights/losing/touch/', common.isLoggedInUserOrMobile, function(req, res) {
    var docName = "losingTouch";
    var companyMembers
    if(req.query.companyMembers.length>1){
        companyMembers= req.query.companyMembers.split(",");
        companyMembers = common.castListToObjectIds(companyMembers);
    }else{
        companyMembers = req.query.companyMembers
        companyMembers = common.castToObjectId(companyMembers)
    }

    var insightsFor = [common.getUserIdFromMobileOrWeb(req)]

    if(req.query.hierarchyList){
        if(req.query.hierarchyList.length>1){
            insightsFor= req.query.hierarchyList.split(',');
        }else{
            insightsFor = req.query.hierarchyList
        }
    }

    insightsManagementObj.getLosingTouchSelf(common.castListToObjectIds(insightsFor), docName, companyMembers, function(err, result) {
        contactObj.getContactsCount(common.castListToObjectIds(insightsFor), function(count) {
            if(result && count[0])
                result.contactsCount = count[0].contactsCount;
            else if(result)
                result.contactsCount = 0;

            if(!req.session.notifications){
                req.session.notifications = {};
            } else {
                req.session.notifications.losingTouch = result
            }

            if (!err && result) {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data: result
                })
            } else {
                res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
            }
        })
    });
});

router.get('/insights/company/relationship/risk', common.isLoggedInUser, function(req, res) {

    var userId = common.getUserId(req.user)

    var usersCompany = req.session.companyName;

    var userIdList = [];
    if (typeof req.query.userIds === 'string')
        userIdList.push(common.castToObjectId(req.query.userIds))
    else
        userIdList = common.castListToObjectIds(req.query.userIds)

    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1},function(error,user) {

        usersCompany = common.fetchCompanyFromEmail(user.emailId);
        insightsManagementObj.companyRelationRisk(userIdList, req.query.detailsFor,usersCompany,function(err, result) {
            if (!err && result) {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data: result
                })
            } else {
                res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
            }
        })
    });

});

router.get('/insights/your/responses/pending', common.isLoggedInUserOrMobile, function(req, res) {
    var timezone;
    var uId = common.getUserId(req.user);
    var userId = common.castToObjectId(uId);

    todayDateMinMax(userId, function(err, date) {
        if (err || !date)
            res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
        else {
            var days = req.query.days;
            var minDate = moment(date.min).subtract(days, "days").toDate()
            var maxDate = moment(date.max).subtract(0, "days").toDate()
            contactObj.getEmailContacts(userId, function(error, emails){
                if(error)
                    res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                else {
                    var contactEmails = _.compact(_.pluck(emails, 'personEmailId'));

                    insightsManagementObj.yourResponsePending(userId, minDate, maxDate, 'thread', contactEmails, function(error, emailThreads){
                        if(error)
                            res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                        else{
                            var allResponsePendingEmails = [];
                            var totalEmails = 0;
                            var responded = 0;
                            if(emailThreads.length > 0){
                                totalEmails += emailThreads.length;
                                for(var i=0; i<emailThreads.length;i++){
                                    if(emailThreads[i].interaction[0].action == 'sender')
                                        allResponsePendingEmails.push(emailThreads[i].interaction[0]);
                                }
                                responded = totalEmails - allResponsePendingEmails.length;
                            }
                            allResponsePendingEmails.forEach(function(a){
                                a.mailType = 'thread';
                            })
                            
                            insightsManagementObj.yourResponsePending(userId, minDate, maxDate, null, contactEmails, function(error, unstructuredEmail) {
                                if (error)
                                    res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                                else {

                                    var temp = [];
                                    unstructuredEmail.forEach(function(a){
                                        if(a.interactions.length == 1 && a.interactions[0].action == 'sender'){
                                            temp.push(a.interactions[0])
                                        }
                                        else if(a.interactions.length > 1)
                                        {
                                            var mailsToStore = [];
                                            for (var i = 0; i < a.interactions.length; i++) {
                                                if(a.interactions[i].action == 'receiver'){
                                                    responded++;
                                                    mailsToStore = [];
                                                }
                                                else if(a.interactions[i].action == 'sender'){
                                                    mailsToStore.push(a.interactions[i]);
                                                }
                                            }

                                            temp = temp.concat(mailsToStore);
                                        }
                                    });
                                    temp.forEach(function(a){
                                        a.mailType = 'single';
                                    })
                                    allResponsePendingEmails = allResponsePendingEmails.concat(temp);

                                    var emailIds = _.compact(_.pluck(allResponsePendingEmails,'personEmailId'));
                                    emailIds = _.uniq(emailIds)
                                    contactObj.getContactsByEmailId(userId, emailIds, function(err, contacts){
                                        if(err)
                                            res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                                        else{
                                            var responsePending = _.map(allResponsePendingEmails, function(a) {
                                                return _.extend(a, _.find(contacts, function(b) {
                                                    return b.personEmailId === a.personEmailId;
                                                }))
                                            });
                                            userManagementObj.findUserProfileByIdWithCustomFields(userId, { companyId: 1, companyName: 1, hierarchyPath: 1, emailId:1 }, function(err, uerDetail){
                                                allUsers.aggregate([{$match:{ companyId: uerDetail.companyId, corporateUser: true }},{$project: { _id:0,personEmailId: "$emailId"}}], function(err, allUsers) {
                                                    allUsers.forEach(function(a){
                                                        a.internal = true;
                                                    })
                                                    responsePending = _.map(responsePending, function(a) {
                                                        return _.extend(a, _.find(allUsers, function(b) {
                                                            return b.personEmailId === a.personEmailId;
                                                        }))
                                                    });

                                                    responsePending.forEach(function(a){
                                                        a.interactionDateSort = a.interactionDate;
                                                        a.interactionDate = a.interactionDate ? moment(a.interactionDate).format("DD MMM YYYY") : null;
                                                        a.company = a.companyNameFromInteraction ? a.companyNameFromInteraction : a.companyNamefromUser;
                                                        if(a.company && uerDetail.companyName && a.company.toLowerCase() == uerDetail.companyName.toLowerCase()) {
                                                            a.internal = true;
                                                        }
                                                    })

                                                    var responsePendingExternal =  responsePending.filter(function(a){
                                                        if(!a.internal)
                                                            return true;
                                                    })

                                                    var responsePendingInternal = responsePending.filter(function(a){
                                                        if(a.internal)
                                                            return true;
                                                    })
                                                    
                                                    if(req.query.infoFor == 'graph') {
                                                        res.send({
                                                            SuccessCode: 1,
                                                            ErrorCode: 0,
                                                            Data: getResultObject(req.query.infoFor, req.query.filter, req.query.company, {
                                                                internal: responsePendingInternal,
                                                                external: responsePendingExternal,
                                                                responded: responded
                                                            })
                                                            //Data: {internal:responsePendingInternal, external:responsePendingExternal, responded:responded}
                                                        })
                                                    }
                                                    else{
                                                        var data = getResultObject(req.query.infoFor, req.query.filter, req.query.company, {internal:responsePendingInternal, external:responsePendingExternal, responded:responded})
                                                        var suggestions = data.explain;
                                                        var totalRecords = suggestions.length,
                                                            pageSize = 10,
                                                            pageCount = Math.round(totalRecords/pageSize),
                                                            currentPage = 1,
                                                            suggestionsArrays = [],
                                                            suggestionsList = [];

                                                        suggestions.sort(function(a, b){
                                                            return b.interactionDateSort - a.interactionDateSort;
                                                        })
                                                        
                                                        //split list into groups
                                                        while (suggestions.length > 0) {
                                                            suggestionsArrays.push(suggestions.splice(0, pageSize));
                                                        }

                                                        //set current page if specifed as get variable (eg: /?page=2)
                                                        if (typeof req.query.getPage !== 'undefined') {
                                                            currentPage = +req.query.getPage;
                                                        }

                                                        suggestionsList = suggestionsArrays[+currentPage - 1];

                                                        res.send({
                                                            SuccessCode: 1,
                                                            ErrorCode: 0,
                                                            Data: {explain:suggestionsList ? suggestionsList : [], companies:data.companies},
                                                            pageSize: pageSize,
                                                            totalList: totalRecords,
                                                            pageCount: pageCount,
                                                            currentPage: currentPage
                                                        });
                                                    }
                                                });
                                            })
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            })
        }
    });
});

function getResultObject(infoFor, filter, company, data){
    if(infoFor == 'graph')
        return data;
    else if(infoFor == 'detailsPage' && filter == 'internal') {
        return {explain:data.internal, companies: []};
    }
    else if(infoFor == 'detailsPage' && filter == 'external' && ( company == 'all' || !company ) ) {

        return {explain:data.external, companies: getMailsByCompany(data.external, null)};
    }
    else if(infoFor == 'detailsPage' && filter == 'internalAndExternal') {
        var both = data.internal.concat(data.external);
        return {explain:both, companies: getMailsByCompany(data.external, null)};
    }
    else if(infoFor == 'detailsPage' && filter == 'external' && company){ //data for selected company
        return  {explain:getMailsByCompany(data.external, company), companies:getMailsByCompany(data.external, null)};
    }

}

function getMailsByCompany(data, company){
    var selectedCompany = [];
    var companies = _
        .chain(data)
        .groupBy('company')
        .map(function(value, key) {
            return {
                company: key,
                details: value
            }
        })
        .value();
    companies.forEach(function(a){
        a.total = a.details.length;
        if(a.company == 'null') {
            a.company = 'other';
            a.total = 0;
            a.originalTotal = a.details.length;
        }
        if(!company)
            delete a.details;
        if(company && company.toLowerCase() == a.company.toLocaleLowerCase())
            selectedCompany = a.details;
    })
    companies.sort(function(a,b){
        return b.total > a.total;
    })
    var topFive = [];
    topFive.push({company:'all', total:data.length})
    topFive = topFive.concat(companies.slice(0,5));
    topFive.forEach(function(a){
        if(a.company == 'other')
            a.total = a.originalTotal;
        delete a.originalTotal;
    })

    topFive.sort(function(a,b){
        return b.total > a.total;
    })

    if(!company)
        return topFive;
    if(company)
        return selectedCompany;

}

router.get('/insights/company/relationship/risk/company', common.isLoggedInUser, function(req, res) {
    var userIdList = [];
    var usersCompany = req.session.companyName;
    if (typeof req.query.userIds === 'string')
        userIdList.push(common.castToObjectId(req.query.userIds))
    else
        userIdList = common.castListToObjectIds(req.query.userIds)

    var uId = common.getUserId(req.user);
    var userId = common.castToObjectId(uId);

    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1},function(error,user) {

        usersCompany = common.fetchCompanyFromEmail(user.emailId);
        insightsManagementObj.companyRelationRiskCompany(userIdList,usersCompany, function(err, result) {
            if (!err && result) {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data: result
                })
            } else {
                res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
            }
        })
    });

});

router.get('/insights/company/relationship/risk/detail/self', common.isLoggedInUser, function(req, res) {
    var hierarchyList = [];
    var userId = common.getUserId(req.user)
    hierarchyList.push(userId)
    hierarchyList = common.castListToObjectIds(hierarchyList)
    var usersCompany = req.session.companyName;
    req.session.hierarchy = hierarchyList
    req.session.save();

    var userIdList = [];
    if (typeof req.query.userIds === 'string')
        userIdList.push(common.castToObjectId(req.query.userIds))
    else
        userIdList = common.castListToObjectIds(req.query.userIds)

    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1},function(error,user) {

        usersCompany = common.fetchCompanyFromEmail(user.emailId);

        insightsManagementObj.companyRelationRiskDetailSelf(userIdList, req.query.limit,usersCompany, function(err, result) {

            if (!err && result.length>0) {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data: result
                })
            } else {
                res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
            }
        });
    });

});

router.get('/insights/company/relationship/risk/detail/hierarchy', common.isLoggedInUser, function(req, res) {
    var userIdList = [];
    if (typeof req.query.userIds === 'string')
        userIdList.push(common.castToObjectId(req.query.userIds))
    else
        userIdList = common.castListToObjectIds(req.query.userIds)
    insightsManagementObj.companyRelationRiskDetailHierarchy(userIdList, req.query.limit, function(err, result) {
        if (!err && result) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: result
            })
        } else {
            res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
        }
    })
});

router.post('/insights/ignore/contact', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    var contactId = req.body.contactId;
    var contactEmailId = req.body.contactEmailId;
    var ignoreFor = req.body.ignoreFor;
    var insightsSection = req.body.insightsSection;

    insightsManagementObj.ignoreContact(userId, contactId, contactEmailId, ignoreFor, insightsSection, function(err, results) {
        res.send(results)
    });
});

router.post('/insights/action/taken', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    allUsers.find({ _id: userId }, { emailId: 1 }, function(error, user) {
        insightsManagementObj.insightsAction(user, req.body.actionTaken, function(err, result) {
            if (!err && result) {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0
                })
            } else {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1
                })
            }
        })
    })
})

router.post('/insights/contact/inProcess', common.isLoggedInUser, function(req, res) {
    var userId = common.getUserId(req.user);

    insightsManagementObj.contactInProcess(userId, req.body.contact, req.body.value, req.body.contactId, function(err, result) {
        if (!err && result) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })
})

router.post('/insights/contact/relation', common.isLoggedInUser, function(req, res) {
    var userId = common.getUserId(req.user);
    var cid = null;
    if (req.body.contactId)
        cid = common.castToObjectId(req.body.contactId);
    insightsManagementObj.contactRelation(userId, req.body.contact, req.body.value, cid, function(err, result) {
        if (!err && result) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })
})

router.get('/insights/revenue/risk/graph', common.isLoggedInUserToGoNext, function(req, res) {
    var hierarchy = req.query.hierarchyList.split(',');
    var getFor = req.query.getFor
    var explain = false;
    var companyMembers = req.query.companyMembers.split(",");

    companyMembers = common.castListToObjectIds(companyMembers);

    insightsManagementObj.getInteractionGrowth(common.castListToObjectIds(hierarchy), getFor, explain, companyMembers, function(results, totalContactValue, valueAtRisk, companyRevenueRisk) {

        var topTenCompanies = _.pluck(results,'company');
        req.session.revenueRiskList = topTenCompanies;

        if(!req.session.notifications){
            req.session.notifications = {};

            req.session.notifications.revenueAtRisk = {
                allContactsValue: totalContactValue,
                valueAtRisk: valueAtRisk
            }
        } else {

            req.session.notifications.revenueAtRisk = {
                allContactsValue: totalContactValue,
                valueAtRisk: valueAtRisk
            }
        }

        req.session.save();

        if (results) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: results,
                allContactsValue: totalContactValue,
                valueAtRisk: valueAtRisk,
                companyRevenueRisk: companyRevenueRisk
            });
        } else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: [],
                allContactsValue: 0,
                valueAtRisk: 0,
                companyRevenueRisk: 0
            });
        }
    });
});

router.get('/insights/revenue/risk/details', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("insights/insightsRevenueRiskDetails");
});

router.get('/insights/revenue/risk/all/data', common.isLoggedInUserToGoNext, function(req, res) {

    var hierarchy = req.query.hierarchyList.split(',');
    var getFor = req.query.getFor;
    var explain = true;

    var userId = common.getUserId(req.user);

    common.getProfileOrStoreProfileInSession(userId, req, function(userProfile) {

        insightsManagementObj.getRevenueRiskDetails(common.castListToObjectIds(hierarchy), getFor, explain, function(data) {
            
            if (data.length > 0) {
                var dateMin = new Date();
                var contactsPersonIdList = _.map(
                    _.where(data, { relatasUser: true }),
                    function(data) {
                        return data.personId.toString();
                    }
                );

                var emailIdsArr = _.compact(_.pluck(data,'personEmailId'))

                meetingClassObj.userNextWeekTravelingTo(contactsPersonIdList, dateMin, function(meetings) {
                    interactionManagementObj.getLastInteractedDateForAllContacts(common.castListToObjectIds(hierarchy),emailIdsArr, function(lastInteraction) {

                        if (meetings.length > 0) {

                            var meetingsLoc = meetings.map(function(value) {
                                var tempArr = [];

                                if (value.toList.length > 0) {
                                    if (contactsPersonIdList)
                                        if (contactsPersonIdList.indexOf(value.toList[0].receiverId) > -1) {
                                            var obj = {
                                                meetingLocation: value.scheduleTimeSlots[0].location,
                                                personId: value.toList[0].receiverId,
                                                startTime: value.scheduleTimeSlots[0].start.date
                                            }
                                            tempArr.push(obj)
                                        }
                                } else {
                                    if (contactsPersonIdList)
                                        if (contactsPersonIdList.indexOf(value.senderId) > -1) {
                                            var obj = {
                                                meetingLocation: value.scheduleTimeSlots[0].location,
                                                personId: value.senderId,
                                                startTime: value.scheduleTimeSlots[0].start.date
                                            }
                                            tempArr.push(obj)
                                        }
                                }
                                return tempArr;
                            });

                            var userTempLoc = userProfile.location.split(',');

                            var obj = _.map(data, function(a) {
                                return _.extend(a, _.find(meetingsLoc, function(b) {
                                    a.userBaseLocation = [];
                                    a.userBaseLocation.push(userTempLoc[0]);
                                    a.userBaseLocation.push(userProfile.currentLocation.city);
                                    a.userFullName = userProfile.firstName + ' ' + userProfile.lastName;

                                    return a.personId === b.personId;
                                }))
                            });

                            var obj2 = _.map(obj, function(a) {
                                return _.extend(a, _.find(lastInteraction, function(b) {
                                    return b._id === a.personEmailId;
                                }))
                            });

                            var rules = getRulesObj(obj2)

                            insightsManagementObj.getRules(rules, function(suggestions) {

                                if(req.query.limit === 'true' && req.session.revenueRiskList && req.session.revenueRiskList.length>0) {

                                    var matches = [],
                                        i, j;

                                    for (i = 0; i < suggestions.length; i++) {
                                        if (-1 != (j = req.session.revenueRiskList.indexOf(suggestions[i].company)))
                                            matches.push(suggestions[i]);
                                    }

                                    var obj = _.uniq(matches, function (item) {
                                        return item.company;
                                    });

                                    if (obj.length >= 10) {
                                        obj.length = 10;
                                    } else {
                                        obj.length = obj.length;
                                    }

                                    res.send({
                                        SuccessCode: 1,
                                        ErrorCode: 0,
                                        Data: obj
                                    });
                                } else {

                                    var totalRecords = suggestions.length,
                                        pageSize = 10,
                                        pageCount = Math.round(totalRecords/pageSize),
                                        currentPage = 1,
                                        suggestionsArrays = [],
                                        suggestionsList = [];

                                    //split list into groups
                                    while (suggestions.length > 0) {
                                        suggestionsArrays.push(suggestions.splice(0, pageSize));
                                    }

                                    //set current page if specifed as get variable (eg: /?page=2)
                                    if (typeof req.query.getPage !== 'undefined') {
                                        currentPage = +req.query.getPage;
                                    }

                                    suggestionsList = suggestionsArrays[+currentPage - 1];

                                    res.send({
                                        SuccessCode: 1,
                                        ErrorCode: 0,
                                        Data: suggestionsList,
                                        pageSize: pageSize,
                                        totalList: totalRecords,
                                        pageCount: pageCount,
                                        currentPage: currentPage
                                    });
                                }
                            })
                        } else {

                            var obj2 = _.map(data, function(a) {
                                return _.extend(a, _.find(lastInteraction, function(b) {
                                    return a.personEmailId === b._id;
                                }))
                            });

                            var rules = getRulesObj(obj2)

                            insightsManagementObj.getRules(rules, function(suggestions) {

                                if(req.query.limit === 'true' && req.session.revenueRiskList && req.session.revenueRiskList.length>0){

                                    var matches = [],
                                        i, j;

                                    for (i=0; i < suggestions.length; i++) {
                                        if (-1 != (j = req.session.revenueRiskList.indexOf(suggestions[i].company)))
                                            matches.push(suggestions[i]);
                                    }

                                    var obj = _.uniq(matches, function(item) {
                                        return item.company;
                                    });

                                    if(obj.length>=10){
                                        obj.length = 10;
                                    } else {
                                        obj.length = obj.length;
                                    }

                                    res.send({
                                        SuccessCode: 1,
                                        ErrorCode: 0,
                                        Data: obj
                                    });

                                } else {

                                    var totalRecords = suggestions.length,
                                        pageSize = 10,
                                        pageCount = Math.round(totalRecords/pageSize),
                                        currentPage = 1,
                                        suggestionsArrays = [],
                                        suggestionsList = [];

                                    //split list into groups
                                    while (suggestions.length > 0) {
                                        suggestionsArrays.push(suggestions.splice(0, pageSize));
                                    }

                                    //set current page if specifed as get variable (eg: /?page=2)
                                    if (typeof req.query.getPage !== 'undefined') {
                                        currentPage = +req.query.getPage;
                                    }

                                    suggestionsList = suggestionsArrays[+currentPage - 1];

                                    res.send({
                                        SuccessCode: 1,
                                        ErrorCode: 0,
                                        Data: suggestionsList,
                                        pageSize: pageSize,
                                        totalList: totalRecords,
                                        pageCount: pageCount,
                                        currentPage: currentPage
                                    });
                                }
                            })
                        }
                    })
                });
            } else {
                res.send({
                    SuccessCode: false,
                    ErrorCode: 0,
                    Data: [],
                    Message: "No Revenue at Risk Companies",
                    topTenCompanies: req.session.revenueRiskList
                });
            }
        });
    })
});

router.get('/insights/update/init', common.isLoggedInUserToGoNext, function(req, res) {
    var userId = common.getUserId(req.user);
    var list = req.query.list

    insightsManagementObj.getTopTenInteractedContactsByCompany(common.castToObjectId(userId), list, function(data) {

        if (data && data.length > 0) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: data
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
            })
        }
    });
});

router.get('/insights/get/insights/reminder', common.isLoggedInUserToGoNext, function(req, res) {
    var userId = common.getUserId(req.user);
    insightsManagementObj.getInsightsReminder(common.castToObjectId(userId), function(remind) {
        res.send(remind)
    });
});

router.post('/insights/init/save', common.isLoggedInUserToGoNext, function(req, res) {

    var userId = common.getUserId(req.user);
    var remind = req.body.insightsRemindIn;

    insightsManagementObj.updateRemindInsights(common.castToObjectId(userId), remind, function(result) {
        res.send(result)
    });
})

router.get('/get/companies/list', common.isLoggedInUserToGoNext, function(req, res) {
    res.send(req.session.revenueRiskList)
})

router.post('/session/hierarchy/selection', common.isLoggedInUserToGoNext, function(req, res) {

    //var getFor = req.query.getFor;
    //
    //req.session.sessionGetFor = getFor;
    //req.session.save();

    res.send(true)
});

router.get('/team/reports',common.isLoggedInUserOrMobile, function(req, res) {
    // res.render("insights/interactionsOpportunities");
    var userId = common.getUserId(req.user)

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render("insights/interactionsOpportunities",{isLoggedIn : common.isLoggedInUserBoolean(req)});
            //res.render('customer/landing',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
})

router.get('/insights/v2',common.isLoggedInUserOrMobile, function(req, res) {
    // res.render("insights/interactionsOpportunities");
    var userId = common.getUserId(req.user)

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render("insights/insightsLanding",{isLoggedIn : common.isLoggedInUserBoolean(req)});
            //res.render('customer/landing',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
})

router.get('/insights/admin/report',common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserId(req.user)
    common.getProfileOrStoreProfileInSession(userId,req,function(user){

        var companyId = req.query.companyId?common.castToObjectId(req.query.companyId):common.castToObjectId(user.companyId);

        var query = {
            ownerEmailId:req.query.emailId?req.query.emailId:user.emailId,
            forTeam:false,
            date: {$lte: new Date(),$gte: req.query.startDate?new Date(moment(req.query.startDate).startOf("day")):new Date(moment().subtract(1,"week"))}
        }

        if(req.query.emailId == "all"){
            query = {
                companyId:companyId,
                forTeam:true,
                date: {$lte: new Date(),$gte: req.query.startDate?new Date(moment(req.query.startDate).startOf("day")):new Date(moment().subtract(1,"week"))}
            }
        }

        insightsAdminReports.aggregate([
            {
                $match:query
            },
            {
                $project:{
                    "pipelineVelocity.currentOopPipeline":0,
                    "pipelineVelocity.currentTargets":0,
                    "pipelineVelocity.oppNextQ":0,
                    "pipelineVelocity.staleOpps":0
                }
            },
            {
                $sort:{
                    "date":1
                }
            },
            // {
            //     $group: {
            //         _id: null,
            //         first: { $first: "$$ROOT" },
            //         last: { $last: "$$ROOT" }
            //     }
            // }
        ]).exec(function (err,results) {
            var insights = [];
            if(!err && results){

                _.each(results,function (el) {
                    if(el.first){
                        insights.push(el.first)
                    }

                    if(el.last){
                        insights.push(el.last)
                    }
                })
            }

            company.findCompanyById(companyId,function (companyInfo) {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data: results,
                    companyDetails:companyInfo
                });
            })
        })
    })
})

router.get('/insights/interactions/by/type/for/team',common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req)

    var minDate =  common.checkRequired(req.query.fromDate)?moment(new Date()).subtract(req.query.fromDate, "days").toDate():null;
    var MaxDate = new Date();

    getTeamUserIds(userId,minDate,MaxDate,function (err,teamUserIds) {
        var teamMembers = [];
        var external = false;

        if(req.query.external == "true"){
            external = true;
            teamMembers = _.pluck(teamUserIds,"emailId")
        } else if(req.query.external == "false"){
            external = false;
            teamMembers = _.pluck(teamUserIds,"emailId")
        } else {
            teamMembers = []
        }

        interactionManagementObj.getInteractionsCountByTypeForUsers(teamUserIds,minDate,MaxDate,teamMembers,external,function (err,interactions) {

            if(!err && interactions && interactions.length>0){
                _.each(teamUserIds,function (tm) {
                   _.each(interactions,function (interaction) {
                       if(interaction._id.ownerEmailId == tm.emailId){
                           interaction._id["name"] = tm.firstName +" " +tm.lastName
                       }
                   })
                });
                res.send({
                    interactions:interactions,
                    teamMembers:teamUserIds
                })
            } else {
                res.send([])
            }
        });
    });
});

router.get('/insights/opportunities/for/team',common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req)
    var days = req.query.fromDate?req.query.fromDate:90;
    var minDate = new Date();
    var maxDate = moment(new Date()).add(days, "days").toDate()
    var pastMaxDate = moment(new Date()).subtract(days, "days").toDate()

    var noLimit = false;

    if(typeof days == "string"){
        noLimit = true;
    }

    getTeamUserIds(userId,minDate,maxDate,function (err,teamUserIds) {
        var userIds = common.castListToObjectIds(_.pluck(teamUserIds,'_id'));

        oppManagementObj.allOpenOpportunitiesForUsers(userIds,null,null,function (err1,openOpp) {
            oppManagementObj.allOpenOpportunitiesForUsersByDateRange(userIds,minDate,maxDate,function (err2,openOppNext60days) {
                oppManagementObj.closedOpportunitiesForUsersByDateRange(userIds,minDate,pastMaxDate,function (err3,closedOppPast60days) {

                    var allOpenOpportunities = [];
                    var openOpportunitiesNext60days = [];
                    var closedOpportunitiesPast60days = [];
                    if(!err1 && openOpp && openOpp.length){
                        allOpenOpportunities = formatOpportunities(openOpp,'allOpen')
                    }

                    if(!err2 && openOppNext60days && openOppNext60days.length){
                        openOpportunitiesNext60days = formatOpportunities(openOppNext60days,'openNext60')
                    }

                    if(!err3 && closedOppPast60days && closedOppPast60days.length){
                        closedOpportunitiesPast60days = formatOpportunities(closedOppPast60days,'closedPast60')
                    }

                    // var usersAndContacts = allOpenOpportunities.forInteractions.concat(openOpportunitiesNext60days.forInteractions);
                    // usersAndContacts = usersAndContacts.concat(closedOpportunitiesPast60days.forInteractions);
                    
                    var dateRange = common.enumerateDaysBetweenDates(pastMaxDate,minDate)

                    interactionManagementObj.interactionsForMultiUsersContacts(userIds,minDate,pastMaxDate,common,function(intErr1,interactionsNext60daysOpp){
                        res.send({
                            allOpenOpportunities:allOpenOpportunities.oppByUserAndCompany,
                            openOppNext60days:openOpportunitiesNext60days.oppByUserAndCompany,
                            closedOppPast60days:closedOpportunitiesPast60days.oppByUserAndCompany,
                            interactionsNext60daysOpp:interactionsNext60daysOpp,
                            teamData:teamUserIds,
                            dateRange:dateRange
                        });
                    });
                });
            });
        });
    });
});

router.get('/insights/interactions/by/relationship',common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req)
    var days = req.query.fromDate?req.query.fromDate:90;
    var minDate = new Date();
    var pastMaxDate = moment(new Date()).subtract(days, "days").toDate()
    
    if(req.query.userId){
        userId = req.query.userId
    }

    var noLimit = false;
    
    if(typeof days == "string"){
        noLimit = true;
    }

    userManagementObj.getUserConatctsByRelationship(common.castToObjectId(userId), function(error, data) {

        var interactionsByRelationship = {
            others:0,
            leads:0,
            prospects:0,
            customers:0,
            all:0
        }

        interactionManagementObj.interactionsForContacts(common.castToObjectId(userId),minDate,pastMaxDate,null,function (err,interactions) {

            _.each(interactions,function (el) {
                interactionsByRelationship.all = interactionsByRelationship.all+el.count
            })

            _.each(data,function (contact) {
                _.each(interactions,function (interaction) {
                    if(contact.emailId == interaction._id.contactEmailId && contact.contactRelation){

                        if(contact.contactRelation.prospect_customer == 'prospect'){
                            interactionsByRelationship.prospects = interactionsByRelationship.prospects+interaction.count
                        } else if(contact.contactRelation.prospect_customer == 'lead'){
                            interactionsByRelationship.leads = interactionsByRelationship.prospects+interaction.count
                        } else if(contact.contactRelation.prospect_customer == 'contact'){
                            interactionsByRelationship.others = interactionsByRelationship.prospects+interaction.count
                        } else if(contact.contactRelation.prospect_customer == 'customer'){
                            interactionsByRelationship.customers = interactionsByRelationship.prospects+interaction.count
                        } else {
                            interactionsByRelationship.others = interactionsByRelationship.prospects+interaction.count
                        }

                        // interactionsByRelationship.all = interactionsByRelationship.all+interaction.count

                    }
                })
            });

            res.send(interactionsByRelationship)

        })
    });
});

function getTeamUserIds(userId,minDate,MaxDate,callback){
    userManagementObj.findUserProfileByIdWithCustomFields(userId, { companyId: 1, companyName: 1, hierarchyPath: 1, emailId:1,firstName:1,lastName:1 }, function(error, data) {
        userManagementObj.getUserHierarchyWithoutLiuWithNames(userId, function(err, teamData) {
            teamData.push({_id:common.castToObjectId(userId),emailId:data.emailId,firstName:data.firstName,lastName:data.lastName});
            callback(err,teamData)
        })
    });
}

function getAllCompanyUsers(userId,minDate,MaxDate,callback){

    userManagementObj.findUserProfileByIdWithCustomFields(userId, { companyId: 1, companyName: 1, hierarchyPath: 1, emailId:1,firstName:1,lastName:1 }, function(error, data) {
        userManagementObj.getTeamMembers(data.companyId, function(err, teamData) {
            callback(err,teamData)
        })
    });
}

function getRulesObj(arr) {
    var rulesfor = [];
    var len = arr.length;
    for (var i = 0; i < len; i++) {

        var now = new Date();
        var lastInteractionDate;

        //Reject those objects which do not have all the values required for getting rules. i.e objects length lesser than 4
        if (Object.keys(arr[i]).length > 6) {

            if (arr[i].lastInteracted) {

                lastInteractionDate = new Date(arr[i].lastInteracted);
                var timeDiff = Math.abs(lastInteractionDate.getTime() - now.getTime());
                var hoursAgo = Math.floor(timeDiff / 1000 / 60 / 60);
                var daysAgo = Math.ceil(timeDiff / (1000 * 3600 * 24));

                if (arr[i].lastInteractedType === 'email') {
                    rulesfor.push({
                        name: arr[i].ownerFirstName + ' ' + arr[i].ownerLastName,
                        contactName: arr[i].fullName,
                        contactLocation: arr[i].baseLocation || null,
                        myLocationNextWeek: arr[i].userBaseLocation ? arr[i].userBaseLocation.join() : null,
                        lastInteractionHours: hoursAgo,
                        lastInteractionDays: daysAgo,
                        lastInteractionDate: lastInteractionDate,
                        lastInteractionType: arr[i].lastInteractedType,
                        action: arr[i].action,
                        interactionGrowth: arr[i].interactionGrowth || null,
                        contactEmailId: arr[i].personEmailId,
                        contactMobileNumber: arr[i].contactMobileNumber,
                        designation: arr[i].designation,
                        company: arr[i].company,
                        lastInteracted: moment(arr[i].lastInteracted).format("DD MMM YYYY") || null,
                        contactValue: arr[i].contactValue,
                        publicProfileUrl: arr[i].publicProfileUrl,
                        contactId: arr[i].contactId,
                        contactRelation: arr[i].contactRelation,
                        personId: arr[i].personId,
                        contactImageLink: arr[i].contactImageLink || null,
                        ownerFullName: arr[i].ownerFirstName + ' ' + arr[i].ownerLastName,
                        ownerId: arr[i].ownerId,
                        ownerEmailId: arr[i].ownerEmailId,
                        twitterRefId: arr[i].refId || null,
                        twitterTitle: arr[i].title || null,
                        description: arr[i].description || null,
                        emailContentId: arr[i].emailContentId || null,
                        refId: arr[i].refId || null,
                        title: arr[i].title || null,
                        trackInfo: arr[i].trackInfo || null,
                        trackId: arr[i].trackId || null
                    });
                } else {
                    rulesfor.push({
                        name: arr[i].ownerFirstName + ' ' + arr[i].ownerLastName,
                        contactName: arr[i].fullName,
                        contactLocation: arr[i].baseLocation || null,
                        myLocationNextWeek: arr[i].userBaseLocation ? arr[i].userBaseLocation.join() : null,
                        lastInteractionHours: hoursAgo,
                        lastInteractionDays: daysAgo,
                        lastInteractionDate: lastInteractionDate,
                        lastInteractionType: arr[i].lastInteractedType,
                        interactionGrowth: arr[i].interactionGrowth || null,
                        contactEmailId: arr[i].personEmailId,
                        contactMobileNumber: arr[i].contactMobileNumber,
                        designation: arr[i].designation,
                        company: arr[i].company,
                        lastInteracted: moment(arr[i].lastInteracted).format("DD MMM YYYY") || null,
                        contactValue: arr[i].contactValue,
                        publicProfileUrl: arr[i].publicProfileUrl,
                        contactId: arr[i].contactId,
                        contactRelation: arr[i].contactRelation,
                        personId: arr[i].personId,
                        contactImageLink: arr[i].contactImageLink || null,
                        ownerFullName: arr[i].ownerFirstName + ' ' + arr[i].ownerLastName,
                        ownerId: arr[i].ownerId,
                        ownerEmailId: arr[i].ownerEmailId,
                        twitterRefId: arr[i].refId || null,
                        twitterTitle: arr[i].title || null,
                        description: arr[i].description || null,
                        emailContentId: arr[i].emailContentId || null,
                        refId: arr[i].refId || null,
                        title: arr[i].title || null,
                        trackInfo: arr[i].trackInfo || null,
                        trackId: arr[i].trackId || null
                    });
                }
            } else {

                if (arr[i].lastInteractedType === 'email') {
                    rulesfor.push({
                        name: arr[i].userFullName || null,
                        contactName: arr[i].personEmailId || '',
                        contactLocation: arr[i].baseLocation || '',
                        myLocationNextWeek: arr[i].userBaseLocation ? arr[i].userBaseLocation.join() : null,
                        lastInteractionHours: null,
                        lastInteractionDays: null,
                        lastInteractionDate: null,
                        lastInteractionType: arr[i].lastInteractedType,
                        action: arr[i].action,
                        interactionGrowth: arr[i].interactionGrowth || null,
                        contactEmailId: arr[i].personEmailId,
                        contactMobileNumber: arr[i].contactMobileNumber,
                        designation: arr[i].designation || null,
                        company: arr[i].company || "Others",
                        lastInteracted: arr[i].lastInteracted || null,
                        contactValue: arr[i].contactValue || null,
                        publicProfileUrl: arr[i].publicProfileUrl || null,
                        contactId: arr[i].contactId || null,
                        contactImageLink: arr[i].contactImageLink || null,
                        contactRelation: arr[i].contactRelation || null,
                        personId: arr[i].personId || null,
                        ownerFullName: arr[i].ownerFirstName + ' ' + arr[i].ownerLastName,
                        ownerId: arr[i].ownerId || null,
                        ownerEmailId: arr[i].ownerEmailId || null,
                        twitterRefId: arr[i].refId || null,
                        twitterTitle: arr[i].title || null,
                        description: arr[i].description || null,
                        emailContentId: arr[i].emailContentId || null,
                        refId: arr[i].refId || null,
                        title: arr[i].title || null,
                        trackInfo: arr[i].trackInfo || null,
                        trackId: arr[i].trackId || null
                    });
                } else {

                    rulesfor.push({
                        name: arr[i].userFullName || null,
                        contactName: arr[i].personEmailId || arr[i].fullName || '',
                        contactLocation: arr[i].baseLocation || '',
                        myLocationNextWeek: arr[i].userBaseLocation ? arr[i].userBaseLocation.join() : null,
                        lastInteractionHours: null,
                        lastInteractionDays: null,
                        lastInteractionDate: null,
                        lastInteractionType: arr[i].lastInteractedType || null,
                        interactionGrowth: arr[i].interactionGrowth || null,
                        contactEmailId: arr[i].personEmailId,
                        contactMobileNumber: arr[i].contactMobileNumber,
                        designation: arr[i].designation || null,
                        company: arr[i].company || "Others",
                        lastInteracted: arr[i].lastInteracted?moment(arr[i].lastInteracted).format("DD MMM YYYY"): null,
                        contactValue: arr[i].contactValue || '',
                        publicProfileUrl: arr[i].publicProfileUrl || null,
                        contactId: arr[i].contactId || null,
                        contactImageLink: arr[i].contactImageLink || null,
                        contactRelation: arr[i].contactRelation || null,
                        personId: arr[i].personId || null,
                        ownerFullName: arr[i].ownerFirstName + ' ' + arr[i].ownerLastName,
                        ownerId: arr[i].ownerId || null,
                        ownerEmailId: arr[i].ownerEmailId || null,
                        twitterRefId: arr[i].refId || null,
                        twitterTitle: arr[i].title || null,
                        description: arr[i].description || null,
                        emailContentId: arr[i].emailContentId || null,
                        refId: arr[i].refId || null,
                        title: arr[i].title || null,
                        trackInfo: arr[i].trackInfo || null,
                        trackId: arr[i].trackId || null
                    });
                }
            }
        }
    }
    return rulesfor;
}

module.exports = router;

function todayDateMinMax(userId, callback) {
    var timezone;
    allUsers.find({ _id: userId }, { timezone: 1 }, function(err, user) {
        if (err || user.length == 0)
            callback(err, null);
        else {
            if (user[0].timezone && user[0].timezone.name)
                timezone = user[0].timezone.name;
            else
                timezone = 'UTC';
            var dateMin = moment().tz(timezone);
            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)
            var dateMax = moment().tz(timezone);

            dateMax.hour(23)
            dateMax.minute(59)
            dateMax.second(59)
            dateMax.millisecond(0)
            callback(null, { min: dateMin, max: dateMax })
        }
    });
}

//This version caches the result. Naveen 06 Oct 2016
router.get('/insights/losing/touch/info/v2', common.isLoggedInUserOrMobile, function(req, res) {

    var limit = req.query.limit;
    var threshold = 50;
    var userId;

    var insightsFor = []
    insightsFor = req.query.hierarchyList.split(',')

    userId = common.getUserIdFromMobileOrWeb(req)
    var docName = "losingTouch";

    getAllCompanyUsers(common.castToObjectId(userId),null,null,function (err,teamData) {

        var companyMembers = _.pluck(teamData,"_id");

        common.getProfileOrStoreProfileInSession(common.castToObjectId(userId), req, function(userProfile) {
            insightsManagementObj.getLosingTouchSelf(common.castListToObjectIds(insightsFor), docName, companyMembers,function(err,aggregatedNumbers) {

                insightsManagementObj.getLosingTouchFullDetails(common.castListToObjectIds(insightsFor), threshold, limit, function(result) {

                    var dateMin = new Date();
                    var contactsPersonIdList = _.map(
                        _.where(result, { relatasUser: true }),
                        function(data) {
                            return data.personId;
                        }
                    );

                    meetingClassObj.userNextWeekTravelingTo(contactsPersonIdList, dateMin, function(meetings) {
                        var meetingsLoc = meetings.map(function(value) {
                            var tempArr = [];

                            if (value.toList.length > 0) {
                                if (contactsPersonIdList)
                                    if (contactsPersonIdList.indexOf(value.toList[0].receiverId) > -1) {
                                        var obj = {
                                            meetingLocation: value.scheduleTimeSlots[0].location,
                                            personId: value.toList[0].receiverId,
                                            startTime: value.scheduleTimeSlots[0].start.date
                                        }
                                        tempArr.push(obj)
                                        //return obj;
                                    }
                            } else {
                                if (contactsPersonIdList)
                                    if (contactsPersonIdList.indexOf(value.senderId) > -1) {
                                        var obj = {
                                            meetingLocation: value.scheduleTimeSlots[0].location,
                                            personId: value.senderId,
                                            startTime: value.scheduleTimeSlots[0].start.date
                                        }
                                        tempArr.push(obj)
                                    }
                            }
                            return tempArr;
                        });

                        var userTempLoc = userProfile.location.split(',');

                        var obj = _.map(result, function(a) {
                            return _.extend(a, _.find(meetingsLoc, function(b) {
                                a.userBaseLocation = [];
                                a.userBaseLocation.push(userTempLoc[0]);
                                a.userBaseLocation.push(userProfile.currentLocation.city);
                                a.userFullName = userProfile.firstName + ' ' + userProfile.lastName;

                                return a.personId === b.personId;
                            }))
                        });

                        var emailIdArr = _.compact(_.pluck(result, 'personEmailId'));
                        var tempLen = result.length;
                        var mobileNumberArr = [];

                        for(var i=0;i<tempLen;i++){
                            if(!result[i].personEmailId && result[i].contactMobileNumber){
                                var mobileNumberTemp = result[i].contactMobileNumber.replace(/[^a-zA-Z0-9]/g,'')
                                mobileNumberArr.push(mobileNumberTemp)
                            }
                        }

                        var ltContacts = _.map(_.groupBy(result,"ownerEmailId"),function (val,user) {
                            return {
                                ownerEmailId:user,
                                contacts:_.pluck(val,"personEmailId")
                            }
                        });

                        contactObj.getContactsFavoriteCountMultipleUsers(common.castListToObjectIds(insightsFor),function (errUsersContacts,usersContactsList) {
                            // contactObj.getImportantContactsLtWith(common.castListToObjectIds(insightsFor),ltContacts,function (errUsersContacts,usersContactsList) {

                            interactionManagementObj.getLastInteractedDateByEmailAndMobileList(common.castToObjectId(userId), emailIdArr,mobileNumberArr, function(lastInteraction) {

                                var obj2 = _.map(obj, function(a) {
                                    return _.extend(a, _.find(lastInteraction, function(b) {
                                        if(a.personEmailId && b.emailId) {
                                            return b.emailId === a.personEmailId;
                                        } else if(a.contactMobileNumber && b.mobileNumber){
                                            return b.mobileNumber === a.contactMobileNumber;
                                        }
                                    }))
                                });

                                var rules = getRulesObj(obj2);
                                if(rules.length > 0) {
                                    insightsManagementObj.getRules(rules, function (suggestions) {

                                        var teamMembers = _.pluck(teamData,"emailId");
                                        var cleanArray = []

                                        _.each(suggestions,function (sg) {
                                            if(!_.contains(teamMembers, sg.contactEmailId)){
                                                cleanArray.push(sg)
                                            }
                                        });

                                        var data = [];

                                        var totalFavTemp = 0;

                                        _.each(cleanArray,function (co) {
                                            _.each(usersContactsList,function (el) {
                                                if(co.ownerEmailId == el._id){
                                                    _.each(el.contacts,function (item) {
                                                        if(item.personEmailId == co.contactEmailId){
                                                            totalFavTemp = el.contacts.length
                                                            data.push(co)
                                                        }
                                                    })
                                                }
                                            });
                                        });

                                        res.send({
                                            SuccessCode: 1,
                                            ErrorCode: 0,
                                            Data: data,
                                            aggregatedNumbers: aggregatedNumbers,
                                            totalFavTemp:totalFavTemp
                                        });

                                    })
                                }
                                else{
                                    res.send({
                                        SuccessCode: 0,
                                        ErrorCode: 1,
                                        Data: {
                                            contacts: [],
                                            aggregatedNumbers: {selfTotalCount: 0, selfLosingTouch: 0}
                                        }
                                    });
                                }
                            });
                        });
                    });
                });
            });
        });
    });
});

function processSessionStoreLosingTouch(req,res) {

    var suggestions = req.session.losingTouch;

    var pageSize;

    if (typeof req.query.pageLimit != 'undefined') {
        pageSize = +req.query.pageLimit;
    } else {
        pageSize = 10
    }

    var totalRecords = suggestions.length,
        pageCount = Math.round(totalRecords/pageSize),
        currentPage = 1,
        suggestionsArrays = [];

    //split list into groups
    while (suggestions.length > 0) {
        suggestionsArrays.push(suggestions.splice(0, pageSize));
    }

    //set current page if specified as get variable (eg: /?page=2)
    if (typeof req.query.getPage != 'undefined') {
        currentPage = +req.query.getPage;
    }

    suggestionsList = suggestionsArrays[+currentPage - 1];

    res.send({
        SuccessCode: 1,
        ErrorCode: 0,
        Data: {
            contacts: suggestionsList,
        },
        pageSize: pageSize,
        totalList: totalRecords,
        pageCount: pageCount,
        currentPage: currentPage
    });

}

function formatOpportunities(openOpp,status) {

    var arrayOpp = [];
    var oppByUserAndCompany = []

    _.each(openOpp,function (opp) {

        var obj = {
            userId:opp._id.userId,
            userEmailId:opp._id.userEmailId,
            contactEmailId:opp._id.contactEmailId,
            company:opp._id.contactEmailId?common.fetchCompanyFromEmail(opp._id.contactEmailId):"others",
            amount:opp.total
        }
        
        if(status == 'closedPast60'){
            obj["closedDate"] = opp._id.$dayOfYear
        }

        arrayOpp.push(obj);
    });

    var oppGroup = _
        .chain(arrayOpp)
        .groupBy('company')
        .map(function(value, key) {
            return {
                company: key,
                details: value
            }
        })
        .value();

    _.each(oppGroup,function (company) {
        var oppGroup2 = _
            .chain(company.details)
            .groupBy('userEmailId')
            .map(function(value, key) {

                var obj = {};
                obj["amount"] = 0;
                obj["closedDates"] = [];
                _.each(value,function (oppValues) {
                    obj["userEmailId"] = key

                    if(status == 'allOpen'){
                        obj["allOpenAmount"] = obj["amount"]+oppValues.amount
                    } else if(status == 'closedPast60'){
                        obj["closedPast60Amount"] = obj["amount"]+oppValues.amount
                        obj["closedDates"].push({
                            company:oppValues.contactEmailId,
                            closedDate:oppValues.closedDate
                        })
                    } else {
                        obj["openNext60Amount"] = obj["amount"]+oppValues.amount
                    }

                    obj["company"] = oppValues.company
                });

                return obj;

            })
            .value();
        oppByUserAndCompany.push(oppGroup2)
    });

    return {
        forInteractions:arrayOpp,
        oppByUserAndCompany:oppByUserAndCompany
    }
}

router.get('/insights/current/month/stats',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = []
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;

    if(req.query.userIds)
        hierarchyList = req.query.userIds.split(",")
    else{
        hierarchyList = [userId]
    }
    
    hierarchyList = common.castListToObjectIds(hierarchyList);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var qEnd = allQuarters[allQuarters.currentQuarter].end;

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if (oppStages) {
                _.each(oppStages, function (st) {
                    if (st.commitStage) {
                        commitStage = st.name;
                    }
                });
            }

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            var dateMin = moment().tz(timezone).startOf('month');
            var dateMax = moment().tz(timezone).endOf('month');

            if(req.query.startDate){
                dateMin = moment(req.query.startDate).tz(timezone).startOf('month')
                dateMax = moment(dateMin).tz(timezone).endOf('month');
            }

            if(req.query.viewMode && (req.query.viewMode == "qtr" || req.query.viewMode == "Quarter")){

                if(req.query.startDate){
                    qStart = moment(req.query.startDate).tz(timezone).startOf('month')
                }
                qEnd = moment(moment(qStart).tz(timezone).endOf('month')).add(2,"month")
            }

            common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
                async.parallel([
                    function (callback) {
                        getOppsStatusDataByDateRange(hierarchyList,dateMin,dateMax,commitStage,false,primaryCurrency,currenciesObj,fyRange.start,fyRange.end,callback)
                    },
                    function (callback) {
                        getOppsStatusDataByDateRange(hierarchyList,qStart,qEnd,commitStage,false,primaryCurrency,currenciesObj,fyRange.start,fyRange.end,callback)
                    },
                    function (callback) {
                        oppCommitByDateRange(hierarchyList[0],moment().tz(timezone).startOf('isoweek'),moment().tz(timezone).endOf('isoweek'),netGrossMarginReq,callback)
                    }
                ], function (errors,data) {
                    if(!errors && data){

                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            result: '',
                            Data:{
                                month:data[0]?data[0]:null,
                                qtr:data[1]?data[1]:null,
                                commits:data[2]
                            }
                        })
                    } else {
                        res.send({
                            SuccessCode: 0,
                            ErrorCode: 1,
                            Data:[]
                        })
                    }

                });
            });
        })
    })
});

router.get('/insights/current/fy',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = []
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;

    if(req.query.userIds)
        hierarchyList = req.query.userIds.split(",")
    else{
        hierarchyList = [userId]
    }

    hierarchyList = common.castListToObjectIds(hierarchyList);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
                async.parallel([
                    function (callback) {
                        getOppsStatusDataByDateRangeWon(hierarchyList,fyRange.start,fyRange.end,'Close Won',false,primaryCurrency,currenciesObj,fyRange.start,fyRange.end,callback)
                    }
                ], function (errors,data) {
                    if(!errors && data){

                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            result: '',
                            Data:data[0]?data[0]:null
                        })
                    } else {
                        res.send({
                            SuccessCode: 0,
                            ErrorCode: 1,
                            Data:[]
                        })
                    }

                });
            });
        })
    })
});

router.get('/insights/team/current/month/stats',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = []
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;

    if(req.query.userIds)
        hierarchyList = req.query.userIds.split(",")
    else{
        hierarchyList = [userId]
    }

    hierarchyList = common.castListToObjectIds(hierarchyList);

    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var qEnd = allQuarters[allQuarters.currentQuarter].end;

        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages) {

            var commitStage = "Proposal"; // default.
            if (oppStages) {
                _.each(oppStages, function (st) {
                    if (st.commitStage) {
                        commitStage = st.name;
                    }
                });
            }

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            var dateMin = moment().tz(timezone).startOf('month');
            var dateMax = moment().tz(timezone).endOf('month');

            common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
                async.parallel([
                    function (callback) {
                        getOppsStatusDataByDateRange(hierarchyList,dateMin,dateMax,commitStage,null,primaryCurrency,currenciesObj,fyRange.start,fyRange.end,callback)
                    },
                    function (callback) {
                        getOppsStatusDataByDateRange(hierarchyList,qStart,qEnd,commitStage,null,primaryCurrency,currenciesObj,fyRange.start,fyRange.end,callback)
                    }
                ], function (errors,data) {
                    if(!errors && data){

                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            result: '',
                            Data:{
                                month:req.query.userIds && data[0]?mergeMultiUserCommits(data[0]):fillerObjMonthlyStats(),
                                qtr:req.query.userIds && data[1]?mergeMultiUserCommits(data[1]):fillerObjMonthlyStats(),
                                // commits:data[2]
                            }
                        })
                    } else {
                        res.send({
                            SuccessCode: 0,
                            ErrorCode: 1,
                            Data:[]
                        })
                    }

                });
            });
        })
    })
});

function fillerObjMonthlyStats() {

    return {
        "userEmailId": null,
        "userId": null,
        "pipeline": 0,
        "lost": 0,
        "won": 0,
        "probability": 0,
        "target": 0
    }
}

function mergeMultiUserCommits(data) {

    var obj = {
        userEmailId: null,
        userId: null,
        pipeline: 0,
        lost: 0,
        won: 0,
        probability: 0,
        target: 0
    }

    _.each(data,function (el) {
        obj.pipeline = obj.pipeline+el.pipeline
        obj.lost = obj.lost+el.lost
        obj.won = obj.won+el.won
        obj.probability = obj.probability+el.probability
        obj.target = obj.target+el.target
    });

    return [obj];
}

function oppCommitByDateRange(userId,dateMin,dateMax,netGrossMarginReq,callback){
    oppCommitObj.getCommitsByDateRange(userId,dateMin,dateMax,function (err,data) {

        if(!err && data && data.length>0 && netGrossMarginReq){
            _.each(data.opportunities,function (el) {
                el = JSON.parse(JSON.stringify(el))
                common.calculateNGM(el.opps)
            })
        }

        callback(err,data)
    })
}

function getOppsStatusDataByDateRange(hierarchyList,dateMin,dateMax,commitStage,netGrossMarginReq,primaryCurrency,currenciesObj,fyStart,fyEnd,callback){
    
    oppManagementObj.getTargetForUserByMonthRange(hierarchyList,dateMin,dateMax,null,null,null,fyStart,fyEnd,function (err,targets) {

        var targetsDictionary = {};
        _.each(targets,function (tr) {
            targetsDictionary[tr._id] = tr.target?tr.target:0
        });

        oppManagementObj.opportunitiesForUsersByDate(hierarchyList,dateMin,dateMax,null,null,null,null,netGrossMarginReq,null,function (err,opps) {

            var allUsersValues = [],
            fillAndExtendData = [];

            if(!err && opps.length>0){
                _.each(opps,function (op) {
                    var pipeline = 0,won = 0,lost = 0,userId = null;
                    _.each(op.opportunities,function (el) {
                        userId = el.userId;
                        el.amountWithNgm = el.amount;

                        if(el.netGrossMargin){
                            el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                        }

                        el.convertedAmt = el.amount;
                        el.convertedAmtWithNgm = el.amountWithNgm

                        if(el.currency && el.currency !== primaryCurrency){
                            if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                                el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                            }

                            if(el.netGrossMargin){
                                el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                            } else {
                                el.convertedAmtWithNgm = el.convertedAmt;
                            }

                            el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))
                        }

                        if(_.includes(el.stageName,"Close Won") || _.includes(el.stageName,"Closed Won")){
                            won = won+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        } else if(_.includes(el.stageName,"Close Lost") || _.includes(el.stageName,"Closed Lost")){
                            lost = lost+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        } else {
                            pipeline = pipeline+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        }
                    });

                    var prob = 100;
                    if(lost>0){
                        prob = 0
                    }

                    allUsersValues.push({
                        userEmailId:op._id,
                        userId:userId,
                        pipeline:pipeline,
                        lost:lost,
                        won:won,
                        probability:prob,
                        target:targetsDictionary[userId]
                    });
                });

                var allUsersValuesObj = {};
                _.each(allUsersValues,function (el) {
                    allUsersValuesObj[el.userId] = el;
                });

                _.each(hierarchyList,function (el) {
                    if(allUsersValuesObj[el]){
                        fillAndExtendData.push(allUsersValuesObj[el])
                    } else {

                        fillAndExtendData.push({
                            userEmailId:null,
                            userId:el,
                            pipeline:0,
                            lost:0,
                            won:0,
                            probability:0,
                            target:targetsDictionary[el]
                        })
                    }
                })

            } else {

                _.each(hierarchyList,function (hi) {
                    fillAndExtendData.push({
                        userEmailId:null,
                        userId:hi,
                        pipeline:0,
                        lost:0,
                        won:0,
                        probability:0,
                        target:targetsDictionary[hi]
                    })
                })
            }

            callback(err,fillAndExtendData)
        })
    })
}

function getOppsStatusDataByDateRangeWon(hierarchyList,dateMin,dateMax,commitStage,netGrossMarginReq,primaryCurrency,currenciesObj,fyStart,fyEnd,callback){

    oppManagementObj.getTargetForUserByMonthRange(hierarchyList,dateMin,dateMax,null,null,null,fyStart,fyEnd,function (err,targets) {

        var targetsDictionary = {};
        _.each(targets,function (tr) {;
            targetsDictionary[tr._id] = tr.target?tr.target:0
        });

        oppManagementObj.opportunitiesForUsersByDateWon(hierarchyList,dateMin,dateMax,null,null,null,null,netGrossMarginReq,null,function (err,opps) {

            var allUsersValues = [],
            fillAndExtendData = [],
                oppsObj = {};

            if(!err && opps.length>0){
                _.each(opps,function (op) {
                    var pipeline = 0,won = 0,lost = 0,userId = null;
                    _.each(op.opportunities,function (el) {
                        userId = el.userId;
                        el.amountWithNgm = el.amount;

                        if(el.netGrossMargin){
                            el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                        }

                        el.convertedAmt = el.amount;
                        el.convertedAmtWithNgm = el.amountWithNgm

                        if(el.currency && el.currency !== primaryCurrency){
                            if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                                el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                            }

                            if(el.netGrossMargin){
                                el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                            } else {
                                el.convertedAmtWithNgm = el.convertedAmt;
                            }

                            el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))
                        }

                        if(_.includes(el.stageName,"Close Won") || _.includes(el.stageName,"Closed Won")){
                            won = won+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        } else if(_.includes(el.stageName,"Close Lost") || _.includes(el.stageName,"Closed Lost")){
                            lost = lost+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        } else {
                            pipeline = pipeline+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        }
                    });

                    var prob = 100;
                    if(lost>0){
                        prob = 0
                    }

                    allUsersValues.push({
                        userEmailId:op._id,
                        userId:userId,
                        pipeline:pipeline,
                        lost:lost,
                        won:won,
                        probability:prob,
                        target:targetsDictionary[userId]
                    });
                });

                var allUsersValuesObj = {};
                _.each(allUsersValues,function (el) {
                    allUsersValuesObj[el.userId] = el;
                });

                _.each(hierarchyList,function (el) {
                    if(allUsersValuesObj[el]){
                        fillAndExtendData.push(allUsersValuesObj[el])
                    } else {

                        fillAndExtendData.push({
                            userEmailId:null,
                            userId:el,
                            pipeline:0,
                            lost:0,
                            won:0,
                            probability:0,
                            target:targetsDictionary[el]
                        })
                    }
                })

            } else {

                _.each(hierarchyList,function (hi) {
                    fillAndExtendData.push({
                        userEmailId:null,
                        userId:hi,
                        pipeline:0,
                        lost:0,
                        won:0,
                        probability:0,
                        target:targetsDictionary[hi]
                    })
                })
            }

            _.each(fillAndExtendData,function (op) {
                oppsObj[op.userId] = {
                    userEmailId:op.userEmailId,
                    userId:op.userId,
                    pipeline:op.pipeline,
                    lost:op.lost,
                    won:op.won,
                    probability:op.prob,
                    target:op.target
                };
            });

            var results = [];

            _.each(targets,function (tr) {

                if(oppsObj[tr._id]){
                    results.push({
                        _id:tr._id,
                        userId:tr._id,
                        target:tr.target,
                        pipeline:oppsObj[tr._id].pipeline,
                        lost:oppsObj[tr._id].lost,
                        won: oppsObj[tr._id].won,
                        probability: oppsObj[tr._id].probability
                    })
                } else {
                    results.push({
                        _id:tr._id,
                        userId:tr._id,
                        target:tr.target,
                        pipeline : 0,
                        lost : 0,
                        won : 0,
                        probability : 0
                    })
                }
            });

            callback(err,results)
        })
    })
}

router.get('/insights/losing/touch/by/relation',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);

    var hierarchyList = []

    if (req.query.userIds)
        hierarchyList = req.query.userIds.split(",")
    else {
        hierarchyList = [userId]
    }

    hierarchyList = common.castListToObjectIds(hierarchyList);

    insightsManagementObj.getLosingTouchContactsGroup(hierarchyList,null,function (ltErr,ltContacts) {

        if(!ltErr && ltContacts){
            contactObj.getImportantContactsLtWith(hierarchyList,ltContacts,function (err,usersContacts) {

                if(!err && usersContacts){

                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        result: '',
                        Data:usersContacts
                    })
                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data:[]
                    })
                }
            });
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data:[]
            })
        }
    });
});

router.get('/insights/deals/closing/soon',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);

    var hierarchyList = [];

    if(req.query.userIds)
        hierarchyList = req.query.userIds.split(",")
    else{
        hierarchyList = [userId]
    }

    hierarchyList = common.castListToObjectIds(hierarchyList);

    var basedOnAccessControl = false;
    if(req.query.accessControl && req.query.accessControl == 'true'){
        basedOnAccessControl = true;
    }

    getUserAccessControl(hierarchyList,function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq) {

        if (!basedOnAccessControl) {
            regionAccess = null, productAccess = null, verticalAccess = null;
        }

        oppManagementObj.getOpportunitiesByByDate(hierarchyList,null,null,null,regionAccess,productAccess,verticalAccess,netGrossMarginReq,common.castToObjectId(companyId),function (err,opps) {

            var existingOpps = _.pluck(opps,"_id")

            oppManagementObj.getOpportunitiesByByDate(hierarchyList,new Date(),null,existingOpps,regionAccess,productAccess,verticalAccess,netGrossMarginReq,common.castToObjectId(companyId),function (err,oppsAll) {

                if(!err && opps && oppsAll){
                    var today = moment().today;

                    _.each(opps,function (op) {
                        if(op.lastStageUpdated){
                            op.daysSinceStageUpdated = -1*(moment(new Date(op.lastStageUpdated.date)).diff(today, 'days'));
                        } else {
                            op.daysSinceStageUpdated = -1*(moment(new Date(op.createdDate)).diff(today, 'days'));
                        }
                    })

                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data:opps,
                        oppsAll:oppsAll.length
                    })
                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data:[]
                    })
                }
            })
        });
    });

});

router.get('/insights/deals/at/risk',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = []
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;

    if (req.query.userIds)
        hierarchyList = req.query.userIds.split(",")
    else {
        hierarchyList = [userId]
    }

    hierarchyList = common.castListToObjectIds(hierarchyList);

    var basedOnAccessControl = false;
    if(req.query.accessControl && req.query.accessControl == 'true'){
        basedOnAccessControl = true;
    }

    var dateMin = moment().add(1, "days");
    var dateMax = moment().add(1, "days");

    if(req.query.from){
        dateMin = new Date(req.query.from)
    }

    dateMin.date(1);
    dateMin.hour(0);
    dateMin.minute(0);
    dateMin.second(0);
    dateMin.millisecond(0);

    dateMax.date(31);
    dateMax.hour(23);
    dateMax.minute(59);
    dateMax.second(0);
    dateMax.millisecond(0);
    
    //Cal. 6 metrics to decide if the deals are @ risk
    //1.Prev Avg interactions. Done.
    //2.DMs/Infls exist?. Done.
    //2.DMs/Infls meetings?. Done.
    //3.Check Stage. Done.
    //4.Check when stage last changed
    //5.LT with opp owner contact. Done.
    //6.Interactions within company. Done.
    //7.Check for stale opp. Done.
    //8.Check 2-way interactions between liu & opp owner. Done

    if(basedOnAccessControl){

        //Show the deals at risk bases on the batch program.
        oppBasedOnAccess(common.castToObjectId(userId),function (err,deals) {

            if(!err && deals && deals[0]){
                res.send({
                    averageRisk:deals[0].averageRisk,
                    deals:deals[0].deals
                });
            } else {
                res.send({
                    averageRisk:0,
                    deals:[]
                });
            }
        })

    } else {

        oppWeightsObj.getStageWeights(common.castToObjectId(companyId),function (oppWeights) {

            insightsManagementObj.dealsAtRiskWeights(function (err,weights) {
                //This function preAverageInteractionsVsOpps() gets interactions data for prev successful deals closure.
                previousAverageInteractionsVsOpps(hierarchyList,function (avgInteractionsVsOpps) {
                    //get current open deals for this month
                    getOpenDeals(hierarchyList,dateMin,dateMax,basedOnAccessControl,function (err1,opps){
                        // 1. interactionsWithImportantContacts() a) get interactions with these contacts until now,
                        // b) This function returns data if DMs/Infls have been met
                        // 2. Checks if DMs/Infls exist
                        interactionsWithImportantContacts(opps,dateMin,dateMax,function (data,currentInteractions,forLtQuery,interactionsByCompany) {
                            getLosingTouch(hierarchyList,forLtQuery,function (err1,ltContacts) {
                                getTwoWayInteractions(hierarchyList,forLtQuery,function (err2,twoWayInt) {

                                    calculateRisk(data,weights.weightages,oppWeights,avgInteractionsVsOpps,interactionsByCompany,ltContacts,currentInteractions,null,twoWayInt,function (deals,averageRisk) {

                                        //test. Remove this
                                        var userEmailId = ''
                                        _.each(deals,function (de) {
                                            if(de.riskMeter>=averageRisk){
                                                userEmailId = de.userEmailId
                                            }
                                        });

                                        res.send({
                                            averageRisk:averageRisk,
                                            deals:deals,
                                            weights:weights,
                                            interactionsByCompany:interactionsByCompany,
                                            ltContacts:ltContacts,
                                            avgInteractionsVsOpps:avgInteractionsVsOpps,
                                            currentInteractions:currentInteractions
                                        });
                                    });
                                })
                            });
                        });
                    });
                });
            });
        })
    }
});

router.get('/insights/deals/at/risk/team/meta',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = []

    if (req.query.userIds)
        hierarchyList = req.query.userIds.split(",")
    else {
        hierarchyList = [userId]
    }

    hierarchyList = common.castListToObjectIds(hierarchyList);

    var from = moment(new Date()).subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    dealsAtRiskMeta.find({userId:{$in:hierarchyList},date:{$gte:new Date(from),$lte:new Date(to)}},function (err,dealsAtRisk) {

        if(!err && dealsAtRisk && dealsAtRisk.length){
            res.send(dealsAtRisk)
        } else {
            res.send([])
        }
    });
    
});

router.get('/insights/accounts/interaction/growth',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = []

    if (req.query.userIds)
        hierarchyList = req.query.userIds.split(",")
    else {
        hierarchyList = [userId]
    }

    hierarchyList = common.castListToObjectIds(hierarchyList);

    var date = moment().subtract(180, "days")

    interactionManagementObj.getUsersAccountInteractionGrowth(hierarchyList,date,function (err,interactionAccounts) {

        if(!err && interactionAccounts){

            var accounts = [];
            var filler = [];
            var monthsPrev = req.query.monthsPrev?req.query.monthsPrev:6;

            for(var i = 0;i<monthsPrev;i++){
                var date = new Date(moment().subtract(i, "month").toDate())
                var obj = {
                    monthYear:monthNames[date.getUTCMonth()]+" "+date.getUTCFullYear(),
                    sortDate:date,
                    accountDetails:[]
                };

                filler.push(obj)
            }

            _.each(interactionAccounts,function (el) {
                accounts.push({
                    account:common.fetchCompanyFromEmail(el._id.account),
                    addedDate:el.addedDate,
                    emailId:el._id.emailId
                })
            })

            var accountsDetails = _
                .chain(accounts)
                .groupBy('account')
                .map(function(value, key) {

                    value.sort(function (o1, o2) {
                        return o1.addedDate > o2.addedDate ? 1 : o1.addedDate < o2.addedDate ? -1 : 0;
                    });

                    var monthYear = monthNames[moment(new Date(value[0].addedDate)).month()] +" "+moment(new Date(value[0].addedDate)).year()

                    return {
                        account:key,
                        emailId:value[0].emailId,
                        addedDate:value[0].addedDate,
                        monthYear:monthYear
                    }
                })
                .value();

            var finalData = _
                .chain(accountsDetails)
                .groupBy('monthYear')
                .map(function(value, key) {

                    return {
                        monthYear: key,
                        sortDate:value[0].addedDate,
                        accountDetails: value
                    }
                })
                .value();

            // Find values that are in filler but not in arrayValues
            var fillerResults = filler.filter(function(obj) {
                return !finalData.some(function(obj2) {
                    return obj.monthYear == obj2.monthYear;
                });
            });

            var data = finalData.concat(fillerResults)

            data.sort(function (o1, o2) {
                return new Date(o1.sortDate) > new Date(o2.sortDate) ? 1 : new Date(o1.sortDate) < new Date(o2.sortDate) ? -1 : 0;
            });

            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: data
            })

        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data:[]
            })
        }
    });
});

router.get('/insights/opp/growth',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = []

    if (req.query.userIds)
        hierarchyList = req.query.userIds.split(",")
    else {
        hierarchyList = [userId]
    }

    hierarchyList = common.castListToObjectIds(hierarchyList);

    var basedOnAccessControl = false;
    if(req.query.accessControl && req.query.accessControl == 'true'){
        basedOnAccessControl = true;
    }

    var date = moment().subtract(6, "months");

    getUserAccessControl(hierarchyList,function (regionAccess,productAccess,verticalAccess,companyId) {

        if(!basedOnAccessControl){
            regionAccess = null,productAccess = null,verticalAccess = null;
        }
        
        oppManagementObj.oppCreatedByMonth(hierarchyList,new Date(date),regionAccess,productAccess,verticalAccess,common.castToObjectId(companyId),function (err,opps) {

            if (!err && opps) {

                var filler = [];
                var monthsPrev = req.query.monthsPrev?req.query.monthsPrev:6;

                for(var i = 0;i<monthsPrev;i++){
                    var prevDate = new Date(moment().subtract(i, "month").toDate())

                    var obj = {
                        monthYear:monthNames[prevDate.getUTCMonth()].substring(0,3)+" "+prevDate.getUTCFullYear(),
                        sortDate:prevDate,
                        count:0
                    };

                    filler.push(obj)
                }

                var dataCreated = [],dataClosed = [];

                _.each(opps,function (op) {
                    var obj = {};

                    if(!op.createdDate){
                        op.createdDate = op._id.getTimestamp()
                    }

                    // if(Closed.test(op.stageName) || closed.test(op.stageName) || Close.test(op.stageName) || close.test(op.stageName) || op.isClosed){
                    if(op.stageName && (op.stageName.toLowerCase() == "closed won" || op.stageName.toLowerCase() == "close won")){
                        dataClosed.push({
                            emailId:op.userEmailId,
                            monthYear:monthNames[moment(new Date(op.closeDate)).month()].substring(0,3) +" "+moment(new Date(op.closeDate)).year(),
                            sortDate:op.closeDate
                        })
                    }

                    obj["monthYear"] = monthNames[moment(new Date(op.createdDate)).month()].substring(0,3) +" "+moment(new Date(op.createdDate)).year();
                    obj["emailId"] = op.userEmailId
                    obj["sortDate"] = op.createdDate
                    dataCreated.push(obj)
                });

                var created = _
                    .chain(dataCreated)
                    .groupBy('monthYear')
                    .map(function(value, key) {

                        return {
                            monthYear: key,
                            sortDate:value[0].sortDate,
                            count: value.length
                        }
                    })
                    .value();

                var closed = _
                    .chain(dataClosed)
                    .groupBy('monthYear')
                    .map(function(value, key) {

                        return {
                            monthYear: key,
                            sortDate:value[0].sortDate,
                            count: value.length
                        }
                    })
                    .value();

                //Find values that are in filler but not in arrayValues
                var fillerResults = filler.filter(function(obj) {
                    return !created.some(function(obj2) {
                        return obj.monthYear == obj2.monthYear;
                    });
                });

                var data = created.concat(fillerResults)

                //Find values that are in filler but not in arrayValues
                var fillerResultsClose = filler.filter(function(obj) {
                    return !closed.some(function(obj2) {
                        return obj.monthYear == obj2.monthYear;
                    });
                });

                var data2 = closed.concat(fillerResultsClose)

                var nonExistingMonthsClose = data.filter(function(obj) {
                    return !data2.some(function(obj2) {
                        return obj.monthYear == obj2.monthYear;
                    });
                });

                _.each(nonExistingMonthsClose,function (el) {
                    data2.push({
                        monthYear:el.monthYear,
                        sortDate:el.sortDate,
                        count:0
                    })
                });

                var nonExistingMonthsOpen = data2.filter(function(obj) {
                    return !data.some(function(obj2) {
                        return obj.monthYear == obj2.monthYear;
                    });
                });

                _.each(nonExistingMonthsOpen,function (el) {
                    data.push({
                        monthYear:el.monthYear,
                        sortDate:el.sortDate,
                        count:0
                    })
                });

                var prevSixMonthDate = moment().subtract(7,"month");

                data = data.filter(function (fl) {
                    if(new Date(fl.sortDate)>= new Date(prevSixMonthDate)){
                        return fl;
                    }
                })

                data2 = data2.filter(function (fl) {
                    if(new Date(fl.sortDate)>= new Date(prevSixMonthDate)){
                        return fl;
                    }
                })

                data.sort(function (o1, o2) {
                    return new Date(o1.sortDate) > new Date(o2.sortDate) ? 1 : new Date(o1.sortDate) < new Date(o2.sortDate) ? -1 : 0;
                });

                data2.sort(function (o1, o2) {
                    return new Date(o1.sortDate) > new Date(o2.sortDate) ? 1 : new Date(o1.sortDate) < new Date(o2.sortDate) ? -1 : 0;
                });

                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data:data.slice(Math.max(data.length - 7, 1)),
                    dataClosed:data2.slice(Math.max(data2.length - 7, 1))
                })
            } else {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                    Data:[]
                })
            }
        })
    });

});

router.get('/interactions/update/test',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);

    oppManagementObj.updateInteractionsForAllOpenOpps(common.castToObjectId(userId),function (err,interactions) {
        res.send({
            err:err,
            interactions:interactions
        })
    })
});

router.get('/get/open/slots',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    userOpenSlotsObj.findRelatasPreferredSlots(common.castToObjectId(userId.toString()), function (openSlots) {
        
        _.each(openSlots,function (os) {
            os.startTimeFormatted = moment(os.start).tz("Asia/Kolkata").format("ddd, hA")
        });
        
        res.send(openSlots)
    })

});

router.get('/insights/pipeline/velocity',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);

    if(req.query.userId) {
        userId = req.query.userId
    }

    var quarters = [
        {
            quarter:1,
            startMonth:0,
            endMonth:2
        },{
            quarter:2,
            startMonth:3,
            endMonth:5
        },{
            quarter:3,
            startMonth:6,
            endMonth:8
        },{
            quarter:4,
            startMonth:9,
            endMonth:11
        }
    ];

    var now = new Date();
    var currentMonth = now.getMonth();

    var dateMax = new Date();
    var dateMin = new Date();

    var currentStartDate = new Date();
    var currentEndDate = new Date();

    var pastStartDate = new Date();
    var pastEndDate = new Date();

    var nextQuarterEndDate = new Date(),nextQuarterStartDate = new Date();

    var currentQuarter = 1; // Init to Jan - March.
    dateMin.setMonth(0)
    dateMax.setMonth(2)

    currentStartDate.setMonth(0)
    currentEndDate.setMonth(2)

    nextQuarterStartDate.setMonth(3)
    nextQuarterEndDate.setMonth(5)

    _.each(quarters,function (q,i,list) {

        if(q.startMonth <= currentMonth && q.endMonth >= currentMonth){

            currentQuarter = q
            currentStartDate.setMonth(q.startMonth)
            currentEndDate.setMonth(q.endMonth)

            var prevQ = list[i-1];
            var nextQ = list[i+1]?list[i+1]:list[0]; // Pick the next quarter. If current quarter is the last quarter, then pick the 1st quarter for next year

            if(prevQ){
                pastStartDate.setMonth(prevQ.startMonth)
                pastEndDate.setMonth(prevQ.endMonth)
            } else {
                pastStartDate.setMonth(new Date(moment(currentStartDate).subtract(2,"month")).getMonth)
                pastEndDate.setMonth(new Date(moment(currentEndDate).subtract(2,"month")).getMonth)
            }
            nextQuarterStartDate.setMonth(nextQ.startMonth)
            nextQuarterEndDate.setMonth(nextQ.endMonth)
        }

    });

    currentStartDate.setDate(1);
    currentStartDate.setHours(0);
    currentStartDate.setMinutes(0);
    currentStartDate.setSeconds(1);

    currentEndDate = new Date(moment(moment(currentStartDate).add(2,"months")).endOf("month"));

    nextQuarterStartDate.setDate(1);
    nextQuarterStartDate.setHours(0);
    nextQuarterStartDate.setMinutes(1);
    nextQuarterStartDate.setSeconds(1);

    nextQuarterEndDate.setDate(31);
    nextQuarterEndDate.setHours(23);
    nextQuarterEndDate.setMinutes(59);
    nextQuarterEndDate.setSeconds(59);

    pastStartDate.setDate(1);
    pastStartDate.setHours(0);
    pastStartDate.setMinutes(1);
    pastStartDate.setSeconds(59);

    pastEndDate.setDate(31);
    pastEndDate.setHours(23);
    pastEndDate.setMinutes(59);
    pastEndDate.setSeconds(59);

    var basedOnAccessControl = false;
    if(req.query.accessControl && req.query.accessControl == 'true'){
        basedOnAccessControl = true;
    }
    
    var targetsFor = [common.castToObjectId(userId)];
    if(req.query.companyId && basedOnAccessControl){
        targetsFor = [common.castToObjectId(req.query.companyId)];
    }

    if(new Date(nextQuarterStartDate) < new Date(currentStartDate)){
        nextQuarterStartDate = moment(currentStartDate).add(3,"months")
        nextQuarterEndDate = moment(currentEndDate).add(3,"months")
    }

    getUserAccessControl([common.castToObjectId(userId)],function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq,companyTargetAccess) {

        if (!basedOnAccessControl) {
            regionAccess = null, productAccess = null, verticalAccess = null;
        }

        if(companyTargetAccess && basedOnAccessControl){
            targetsFor = [common.castToObjectId(companyId)]
        }

        common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {
            oppManagementObj.getTargetForUserByMonthRange([common.castToObjectId(userId)],pastStartDate,pastEndDate,regionAccess,productAccess,verticalAccess,fyRange.start,fyRange.end,function (errT,pastTargets) {
                oppManagementObj.getTargetForUserByMonthRange(targetsFor,currentStartDate,currentEndDate,regionAccess,productAccess,verticalAccess,fyRange.start,fyRange.end,function (errF,currentTargets) {
                    oppManagementObj.getAllOpportunitiesByStageName(common.castToObjectId(userId),pastStartDate,pastEndDate,regionAccess,productAccess,verticalAccess,netGrossMarginReq,common.castToObjectId(companyId),function (err,oppPipeline) {
                        oppManagementObj.getAllOpportunitiesByStageName(common.castToObjectId(userId),currentStartDate,currentEndDate,regionAccess,productAccess,verticalAccess,netGrossMarginReq,common.castToObjectId(companyId),function (err,currentOopPipeline) {
                            oppManagementObj.opportunitiesForUsersByDate([common.castToObjectId(userId)],nextQuarterStartDate,nextQuarterEndDate,null,regionAccess,productAccess,verticalAccess,null,common.castToObjectId(companyId),function (fErr,oppNextQ) {
                                oppManagementObj.staleOpportunities(common.castToObjectId(userId),regionAccess,productAccess,verticalAccess,common.castToObjectId(companyId),function (staleErr,staleOpps) {

                                    if(!err && !fErr){

                                        var totalCount = 0,totalAmount = 0,wonCount = 0,wonAmount = 0;

                                        if(oppPipeline.length>0){

                                            _.each(oppPipeline,function (el) {
                                                if(el._id == "Close Won"){
                                                    wonAmount = el.sumOfAmount
                                                    wonCount = el.countOfOpps
                                                }

                                                totalAmount = totalAmount+el.sumOfAmount;
                                                totalCount = totalCount+el.countOfOpps;
                                            });
                                        }

                                        if(wonAmount === 0){
                                            wonAmount = 1;
                                        }

                                        var ft = currentTargets && currentTargets[0]?currentTargets[0].target:0;

                                        var expectedPipeline = (ft*totalAmount)/wonAmount;

                                        res.send({
                                            SuccessCode: 1,
                                            ErrorCode: 0,
                                            Data:{
                                                expectedPipeline:expectedPipeline?parseInt(expectedPipeline):expectedPipeline,
                                                totalCount:totalCount,
                                                totalAmount:totalAmount,
                                                wonCount:wonCount,
                                                wonAmount:wonAmount,
                                                currentOopPipeline:currentOopPipeline,
                                                currentQuarter: monthNames[currentQuarter.startMonth] +' - '+monthNames[currentQuarter.endMonth]+' '+ dateMax.getFullYear(),
                                                pastTargets:pastTargets,
                                                currentTargets:currentTargets,
                                                oppNextQ:oppNextQ,
                                                staleOpps:staleOpps
                                            }
                                        })

                                    } else {

                                        res.send({
                                            SuccessCode: 0,
                                            ErrorCode: 1,
                                            Data:[]
                                        })
                                    }
                                });

                            })
                        })
                    });

                })
            })
        })
    });
});

router.get('/test/insights/building',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);

    actionItemsManagementObj.kickOffActionItemsMailProcess(userId,new Date(moment().subtract(5, "days").toDate()),req,function (results) {
        res.send(results)
    })

});

function calculateRisk(opps,weights,oppWeights,avgInteractionsVsOpps,interactionsByCompany,ltContacts,currentInteractions,stages,twoWayInt,callback){

    var today = moment().today;
    var avgIntPerDealCloseCache = 0;

    var twoWayIntDictionary = {};
    
    _.each(twoWayInt,function (el) {
        twoWayIntDictionary[el._id.ownerEmailId+el._id.contact+el._id.action] = el.count
    });

    _.each(opps,function (op) {
        op.interactionsInCompanyWeight = 0;
        op.decisionMaker_infuencerExistWeight = 0;
        op.metDecisionMaker_infuencerWeight = 0;
        op.losingTouchWithOwnerWeight = 0;
        op.oppStageWeight = 0;
        op.stageLastModifiedWeight = 0;
        op.averageInteractionsPerDealWeight = 0;
        op.staleOppWeight = 0;
        op.twoWayInteractionsWeight = 0;
        op.avgInWithOwnerWeight = 0;

        var totalCompaniesInteracted = 0;

        _.each(op.contacts,function (co) {
            if(common.fetchCompanyFromEmail(op.contactEmailId)){
                totalCompaniesInteracted++;
            }
        });

        var riskMeter = 0;
        if(op.influencersExist || op.decisionMakersExist){
        } else {
            riskMeter = riskMeter+weights.decisionMaker_infuencerExist

            op.decisionMaker_infuencerExistWeight = weights.decisionMaker_infuencerExist
        }

        if(avgInteractionsVsOpps && avgInteractionsVsOpps[op.userEmailId]){
            if(avgInteractionsVsOpps[op.userEmailId].companiesInteracted >= totalCompaniesInteracted){
                op.interactionsInCompanyWeight = weights.interactionsInCompany
                riskMeter = riskMeter+weights.interactionsInCompany
            }
        }

        if(currentInteractions && currentInteractions[op.userEmailId+op.contactEmailId]){

            var meetingCount = 0;
            _.each(op.dmsInfls,function (el) {
                if(currentInteractions[op.userEmailId+el] && currentInteractions[op.userEmailId+el].meetingCount>0){
                    meetingCount++;
                }
            });

            if(meetingCount>0){
                op.metDecisionMaker_infuencer = true;
            } else {
                riskMeter = riskMeter+weights.metDecisionMaker_infuencer
                op.metDecisionMaker_infuencer = false;
                op.metDecisionMaker_infuencerWeight = weights.metDecisionMaker_infuencer
            }

        } else {
            riskMeter = riskMeter+weights.metDecisionMaker_infuencer
            op.metDecisionMaker_infuencerWeight = weights.metDecisionMaker_infuencer
            op.metDecisionMaker_infuencer = false;
        }

        var daysSince = 90; // Default more than 90 days
        var daysSinceOppCreated = -1*(moment(new Date(op.createdDate)).diff(today, 'days'));

        if(op.lastStageUpdated && op.lastStageUpdated.date){
            daysSince = -1*(moment(new Date(op.lastStageUpdated.date)).diff(today, 'days'));
            if(daysSince>45){
                riskMeter = riskMeter+weights.stageLastModified;
                op.stageLastModifiedWeight = weights.stageLastModified
            } else if(daysSince<45){
                op.daysSinceStageUpdated = -1*(moment(new Date(op.lastStageUpdated.date)).diff(today, 'days'));
            }
        } else {

            daysSince = daysSinceOppCreated;
            if(daysSinceOppCreated>45){
                riskMeter = riskMeter+weights.stageLastModified;
                op.stageLastModifiedWeight = weights.stageLastModified
            }
        }

        op.daysSinceStageUpdated = daysSince;

        if(oppWeights && oppWeights[op.stageName]){
            riskMeter = riskMeter+(oppWeights[op.stageName]/100)*weights.oppStage;
            op.oppStageWeight = (oppWeights[op.stageName]/100)*weights.oppStage
        } else {

            if(op.stageName == "Prospecting"){
                riskMeter = riskMeter+0.10*weights.oppStage;
                op.oppStageWeight = 0.10*weights.oppStage
            }else if(op.stageName == "Evaluation"){
                riskMeter = riskMeter+0.3*weights.oppStage;
                op.oppStageWeight = 0.3*weights.oppStage
            } else if(op.stageName == "Proposal"){
                riskMeter = riskMeter+0.6*weights.oppStage;
                op.oppStageWeight = 0.6*weights.oppStage
            }
        }

        if(ltContacts && ltContacts[op.userEmailId+op.contactEmailId]) {
            riskMeter = riskMeter+weights.losingTouchWithOwner
            op.losingTouchWithOwnerWeight = weights.losingTouchWithOwner
            op.ltWithOwner = true
        } else {
            op.ltWithOwner = false
        }
        
        //Check if the average interactions are being maintained for the contact owner.
        // Or else the risk will be high. If there are no interactions in the past 30 days, risk is high by default.

        if(avgInteractionsVsOpps && avgInteractionsVsOpps[op.userEmailId] && currentInteractions && currentInteractions[op.userEmailId+op.contactEmailId]){
            // var avgIntPerDealClose = (avgInteractionsVsOpps[op.userEmailId].oppWonClosed/avgInteractionsVsOpps[op.userEmailId].count)*100;

            var avgIntPerDealClose = 0;
            if(!avgIntPerDealCloseCache){
                avgIntPerDealCloseCache = (avgInteractionsVsOpps[op.userEmailId].count/avgInteractionsVsOpps[op.userEmailId].oppWonClosed);
                avgIntPerDealClose = avgIntPerDealCloseCache
                
            } else {
                avgIntPerDealClose = avgIntPerDealCloseCache
            }

            var currentIntCount = currentInteractions[op.userEmailId+op.contactEmailId].allCount
            if(currentIntCount<avgIntPerDealClose){
                riskMeter = riskMeter+weights.averageInteractionsPerDeal;

                op.averageInteractionsPerDealWeight = weights.averageInteractionsPerDeal;

                op.averageInteractionsPerDeal = false
            } else {
                op.averageInteractionsPerDeal = true
            }
            
        } else {
            riskMeter = riskMeter+weights.averageInteractionsPerDeal
            op.averageInteractionsPerDealWeight = weights.averageInteractionsPerDeal;
        }

        //Two way interactions
        var liuInit = 0,oppOwnerInit = 0;

        if(twoWayIntDictionary && twoWayIntDictionary[op.userEmailId+op.contactEmailId+'receiver']){
            liuInit = twoWayIntDictionary[op.userEmailId+op.contactEmailId+'receiver']
        }

        if(twoWayIntDictionary && twoWayIntDictionary[op.userEmailId+op.contactEmailId+'sender']){
            oppOwnerInit = twoWayIntDictionary[op.userEmailId+op.contactEmailId+'sender']
        }

        op.skewedTwoWayInteractions = false;

        if(oppOwnerInit/liuInit<0.2){
            op.skewedTwoWayInteractions = true;
            riskMeter = riskMeter+weights.twoWayInteractions;
            op.twoWayInteractionsWeight = weights.twoWayInteractions;
        }


        if(new Date(op.closeDate)< new Date()){
            riskMeter = riskMeter+weights.staleOpp
            op.staleOppWeight = weights.staleOpp;
        }

        op.riskMeter = common.roundOff(riskMeter,2) ;
        // op.riskMeter = riskMeter ;

    });

    var riskMeterValues = _.pluck(opps,"riskMeter");
    var maxRiskValue = _.max(riskMeterValues)
    var minRiskValue = _.min(riskMeterValues)
    var num = (minRiskValue+maxRiskValue)/2;
    var averageRisk = _.sum(riskMeterValues)/riskMeterValues.length;

    callback(opps,averageRisk)

}

function getTwoWayInteractions(hierarchyList,forQuery,callback){
    interactionManagementObj.getTwoWayInteractionsCount(forQuery,null,null,function (err,intCount) {
        callback(err,intCount)
    })
}

function getLosingTouch(hierarchyList,userContactGroup,callback){

    insightsManagementObj.getLosingTouchForUsersContacts(hierarchyList,userContactGroup,function (err,ltContacts) {

        if(!err && ltContacts && ltContacts.length>0){
            var ltContactsDictionary = {};
            _.each(ltContacts,function (co) {
                ltContactsDictionary[co._id.ownerEmailId+co._id.emailId] = true
            })
            callback(err,ltContactsDictionary)
        } else {
            callback(err,[])
        }

    })
}

function interactionsWithImportantContacts(opps,dateMin,dateMax,callback) {

    var opportunitiesWithInsights = [];
    var forInteractionsQuery = [];
    var forInteractionsQuery2 = [];

    _.each(opps,function (op) {
        var uniqContacts = [],uniqDmsInfls = [],oppOwner = [],oppCreatedDates = [];
        var opportunities = [];
        _.each(op.opportunities,function (el) {
            if(_.includes(el.stageName,"Close Won") || _.includes(el.stageName,"won")){

            } else {

                var contacts = [],dmsInfls = [];

                contacts.push(el.contactEmailId)
                oppOwner.push(el.contactEmailId)
                // contacts.push(_.pluck(el.partners,"emailId"))
                contacts.push(_.pluck(el.decisionMakers,"emailId"))
                contacts.push(_.pluck(el.influencers,"emailId"))

                dmsInfls.push(_.pluck(el.decisionMakers,"emailId"))
                dmsInfls.push(_.pluck(el.influencers,"emailId"))

                uniqContacts.push(contacts)
                uniqDmsInfls.push(dmsInfls)

                oppCreatedDates.push(el.createdDate?el.createdDate:el._id.getTimestamp());

                var company = common.fetchCompanyFromEmail(el.contactEmailId);
                opportunities.push({
                    _id:el._id,
                    opportunityId:el.opportunityId,
                    opportunityName:el.opportunityName,
                    closeDate:el.closeDate,
                    formattedCloseDate:moment(el.closeDate).format("DD MMM YYYY"),
                    createdDate:el.createdDate?el.createdDate:el._id.getTimestamp(),
                    userEmailId:el.userEmailId,
                    contactEmailId:el.contactEmailId,
                    amount:el.amount,
                    stageName:el.stageName,
                    sourceType:el.sourceType,
                    productType:el.productType,
                    partnersExist:el.partners?el.partners.length>0:false,
                    influencersExist:el.influencers?el.influencers.length>0:false,
                    decisionMakersExist:el.decisionMakers?el.decisionMakers.length>0:false,
                    decisionMakers:el.decisionMakers,
                    influencers:el.influencers,
                    partners:el.partners,
                    netGrossMargin:el.netGrossMargin,
                    lastStageUpdated:el.lastStageUpdated?el.lastStageUpdated:null,
                    contacts:_.flattenDeep(contacts),
                    dmsInfls:_.flattenDeep(dmsInfls),
                    company:company?company:"Others"
                });

                forInteractionsQuery2.push({
                    userEmailId:el.userEmailId,
                    createdDate:el.createdDate?el.createdDate:el._id.getTimestamp(),
                    contacts:_.flattenDeep(contacts)
                })
            }
        });

        forInteractionsQuery.push({
            userEmailId:op._id,
            contacts:_.uniq(_.flattenDeep(uniqContacts)),
            uniqDmsInfls:_.uniq(_.flattenDeep(uniqDmsInfls)),
            oppOwner:_.uniq(oppOwner),
            oppCreatedDates:oppCreatedDates
        });

        var obj = {};
        obj[op._id] = opportunities
        opportunitiesWithInsights.push(opportunities)
    });

    interactionManagementObj.getInteractionCountByGroupByUserContacts(forInteractionsQuery,dateMin,dateMax,function (err,interactions) {

        var interactionsByContactAndUser = {},interactionsByCompanyAndUser = {},meetingCount = 0,allCount = 0;
        _.each(interactions,function (el) {

            if(el._id.interactionType == "meeting"){
                meetingCount = meetingCount+el.count
            }

            allCount = allCount+el.count;

            interactionsByContactAndUser[el._id.ownerEmailId+el._id.emailId] = {
                meetingCount:meetingCount,
                allCount:allCount,
                contactEmailId:el._id.emailId
            };
        });

        interactionManagementObj.getInteractionCountByCompany(forInteractionsQuery,dateMin,dateMax,common,function (err,interactionsByCompany) {

            _.each(interactionsByCompany,function (el) {

                if(interactionsByCompanyAndUser[el._id.ownerEmailId+common.fetchCompanyFromEmail(el._id.emailId)]){
                    interactionsByCompanyAndUser[el._id.ownerEmailId+common.fetchCompanyFromEmail(el._id.emailId)] = el.count+interactionsByCompanyAndUser[el._id.ownerEmailId+common.fetchCompanyFromEmail(el._id.emailId)];
                } else {
                    interactionsByCompanyAndUser[el._id.ownerEmailId+common.fetchCompanyFromEmail(el._id.emailId)] = el.count;
                }
            });
            
            callback(_.flatten(opportunitiesWithInsights),
                interactionsByContactAndUser,
                forInteractionsQuery,
                interactionsByCompanyAndUser)
        });
    });

}

function getOpenDeals(hierarchyList,dateMin,dateMax,basedOnAccessControl,callback) {

    getUserAccessControl(hierarchyList,function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq) {

        if(!basedOnAccessControl){
            regionAccess = null,productAccess = null,verticalAccess = null;

            oppManagementObj.getOpenOpportunitiesForUsers(hierarchyList,dateMin,dateMax,null,regionAccess,productAccess,verticalAccess,netGrossMarginReq,common.castToObjectId(companyId),function (err,opps) {
                callback(err,opps)
            });

        }
    });
}

function oppBasedOnAccess(userId,callback) {

    var from = moment(new Date()).subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    getAllCompanyUsers(userId,null,null,function (tmErr,teamData) {
        getUserAccessControl([userId],function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq) {

            var companyMembers = _.pluck(teamData,"_id");
            var accessControlQuery = [];
            if(regionAccess){
                accessControlQuery.push({"deals.geoLocation.zone":{$in:regionAccess}})
            }

            if(productAccess){
                accessControlQuery.push({"deals.productType":{$in:productAccess}})
            }

            if(verticalAccess){
                accessControlQuery.push({"deals.vertical":{$in:verticalAccess}})
            }

            var match = {
                $and:accessControlQuery
            }

            dealsAtRiskMeta.aggregate([
                {
                    $match:{userId:{$in:companyMembers},date:{$gte:new Date(from),$lte:new Date(to)}}
                },{
                    $unwind:"$deals"
                }
                ,{
                    $match:match
                },{
                    $group:{
                        _id:null,
                        deals:{$push:"$deals"},
                        averageRisk:{$push:{
                            averageRisk:"$averageRisk",
                            userEmailId:"$userEmailId",
                            totalDeals:"$totalDeals"
                        }}
                    }
                }
            ],function(error, result){
                if(error){
                    callback(error,null);
                }
                else{
                    callback(null,result);
                }
            });

            // dealsAtRiskMeta.find({userId:{$in:companyMembers},date:{$gte:new Date(from),$lte:new Date(to)}},{deals:1,averageRisk:1},function (err,dealsAtRisk) {
            //     callback(err,dealsAtRisk)
            // });

        });
    })
}

function previousAverageInteractionsVsOpps(hierarchyList,callback) {
    oppManagementObj.successFullClosures(hierarchyList,function (err,opps) {

        var usersAndContacts = [],usersAndContactsDictionary = {},usersInteractionsDictionary = {};
        _.each(opps,function (op) {
            var contacts = [], minDate = [], maxDate = [];

            _.each(op.contacts,function (co) {
                contacts.push(co.mainContact)
                // contacts.push(_.pluck(co.partners,"emailId"))
                contacts.push(_.pluck(co.decisionMakers,"emailId"))
                contacts.push(_.pluck(co.influencers,"emailId"))
                minDate.push(new Date(co._id.getTimestamp()))
                maxDate.push(new Date(co.closeDates))
            });

            contacts = _.uniq(_.flatten(contacts));

            usersAndContacts.push({
                userEmailId:op._id,
                oppWonClosed:op.count,
                contacts:contacts,
                minDate:getMinOfArray(minDate),
                maxDate:getMaxOfArray(maxDate)
            });

            var companiesInteracted = 0;
            _.each(contacts,function (co) {
                if(common.fetchCompanyFromEmail(co)){
                    companiesInteracted++
                }
            })

            usersAndContactsDictionary[op._id] = {
                contacts:contacts,
                oppWonClosed:op.count,
                companiesInteracted:companiesInteracted
            }

        });

        interactionManagementObj.getInteractionCountByUsersAndContacts(usersAndContacts,null,null,function (err,interactions) {

            _.each(interactions,function (el) {

                el.oppWonClosed = usersAndContactsDictionary[el._id].oppWonClosed
                el.contactsInteracted = usersAndContactsDictionary[el._id].contacts.length
                el.companiesInteracted = usersAndContactsDictionary[el._id].companiesInteracted
                usersInteractionsDictionary[el._id] = el
            });

            callback(usersInteractionsDictionary)
        })
    })
}

function getMaxOfArray(numArray) {
    return Math.max.apply(null, numArray);
}

function getMinOfArray(numArray) {
    return Math.min.apply(null, numArray);
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function getUserAccessControl(userIds,callback) {

    userManagementObj.getUserOppOwnerDetails(userIds,function (errO,oppOwnershipDetails) {
        company.findCompanyById(oppOwnershipDetails[0].companyId,function (companyDetails) {

            var regionAccess = companyDetails.geoLocations.length>0?oppOwnershipDetails[0].regionOwner:null;
            var productAccess = companyDetails.productList.length>0?oppOwnershipDetails[0].productTypeOwner:null;
            var verticalAccess = companyDetails.verticalList.length>0?oppOwnershipDetails[0].verticalOwner:null;
            var netGrossMarginReq = companyDetails.netGrossMargin?companyDetails.netGrossMargin:false;
            var companyTargetAccess = oppOwnershipDetails[0].companyTargetAccess?oppOwnershipDetails[0].companyTargetAccess:false;

            callback(regionAccess,productAccess,verticalAccess,oppOwnershipDetails[0].companyId,netGrossMarginReq,companyTargetAccess)
        });
    });
}
