
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var commonUtility = require('../../common/commonUtility');
var OrgRoles = require('../../dataAccess/teamRoles');
var _ = require("lodash");

var common = new commonUtility();
var orgRolesObj = new OrgRoles();

var redis = require('redis');
var redisClient = redis.createClient();

router.get('/organisation/get/roles',common.isLoggedInUserOrMobile,function(req,res){

    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile ? userProfile.companyId : null;
    var userId = common.getUserIdFromMobileOrWeb(req);

    orgRolesObj.getAllRoles(common.castToObjectId(companyId),function (err,result) {
        res.send({
            Data:result
        });
    })
});

module.exports = router;