var express = require('express');
var router = express.Router();

router.get('/fetch/some/data', function(req, res) {
    req.dbPath.model('User').find({}).exec(function (err,data) {
        res.send({
            err:err,
            data:data
        })
    });
})

module.exports = router;