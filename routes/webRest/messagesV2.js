/**
 * Created by naveen on 5/12/16.
 */
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');
var jwt = require('jwt-simple');
var _ = require("lodash");
var request = require('request');

var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactionManagement = require('../../dataAccess/interactionManagement');
var MessageManagement = require('../../dataAccess/messageManagementV2');
var commonUtility = require('../../common/commonUtility');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');

var customObjectId = require('mongodb').ObjectID;
var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var messageManagementObj = new MessageManagement();

var winstonLog = require('../../common/winstonLog');
var logLib = new winstonLog();
var logger = logLib.getMessageErrorLogger();

var statusCodes = errorMessagesObj.getStatusCodes();

router.post('/message/create/by/id', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagementObj.getUserBasicProfile(common.castToObjectId(userId),function (err,profile) {

        var conversationId = req.body.conversationId?req.body.conversationId:new customObjectId();
        var messageReferenceId = req.body.messageReferenceId?req.body.messageReferenceId:userId;
        var messageReferenceType = req.body.messageReferenceType?req.body.messageReferenceType:null;
        var text = common.nl2br(req.body.text);
        var cEmailId = req.body.cEmailId?req.body.cEmailId:null;

        var messageOwner =  {
            fullName: profile.firstName+" "+profile.lastName,
            emailId: profile.emailId,
            userId:common.castToObjectId(userId)
        };

        var message = {
            conversationId:conversationId,
            messageReferenceType: messageReferenceType,
            messageReferenceId:common.castToObjectId(messageReferenceId),
            messageOwner: messageOwner,
            text: text,
            emailId: cEmailId,
            userId:null,
            date: new Date()
        };

        messageManagementObj.createMessage(message,function (err,result) {

            if(!err && result.result.ok){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":message
                });
            } else {
                logger.info("Error in saving message",err);
                common.sendErrorResponse(res,errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }));
            }
        });
    });
});

router.post('/message/reply', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagementObj.getUserBasicProfile(common.castToObjectId(userId),function (err,profile) {

        var conversationId = req.body.conversationId?req.body.conversationId:new customObjectId();
        var messageReferenceId = null;
        var messageReferenceType = req.body.messageReferenceType?req.body.messageReferenceType:null;
        var text = common.nl2br(req.body.text);
        var emailId = req.body.emailId?req.body.emailId:null;

        var messageOwner =  {
            fullName: profile.firstName+" "+profile.lastName,
            emailId: profile.emailId,
            userId:common.castToObjectId(userId)
        };

        var message = {
            conversationId:conversationId,
            messageReferenceType: messageReferenceType,
            messageReferenceId:common.castToObjectId(messageReferenceId),
            messageOwner: messageOwner,
            text: text,
            emailId: emailId,
            userId:null,
            date: new Date(),
            otherParticipants:req.body.participants
        };

        messageManagementObj.createMessage(message,function (err,result) {

            if(!err && result.result.ok){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":message
                });
            } else {
                logger.info("Error in saving message",err);
                common.sendErrorResponse(res,errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }));
            }
        });
    });
});

router.get('/get/messages/by/reference/id', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var messageReferenceId = req.query.messageReferenceId;
    var messageReferenceType = req.query.messageReferenceType;

    messageManagementObj.getConversationByReferenceId(common.castToObjectId(userId),messageReferenceId,messageReferenceType,function (err,result) {
        if(!err && result){
            var timezone = "UTC";
            if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                timezone = req.session.profile.timezone.name
            }

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":result,
                "timezone":timezone
            });
        } else {
            logger.info("Error in saving message",err);
            common.sendErrorResponse(res,errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }));
        }
    });
});

router.get('/get/messages/by/conversation/id', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var conversationId = req.query.conversationId;

    messageManagementObj.getConversationByConversationId(null,conversationId,null,function (err,result) {
        if(!err && result){
            var timezone = "UTC";
            if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                timezone = req.session.profile.timezone.name
            }

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":result,
                "timezone":timezone
            });
        } else {
            logger.info("Error in saving message",err);
            common.sendErrorResponse(res,errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }));
        }
    });
});

router.get('/get/messages/by/email/id', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var emailId = req.query.emailId;
    var messageReferenceType = req.query.messageReferenceType;

    messageManagementObj.getConversationByEmailId(common.castToObjectId(userId),emailId,messageReferenceType,function (err,result) {
        if(!err && result){
            var timezone = "UTC";
            if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                timezone = req.session.profile.timezone.name
            }

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":result,
                "timezone":timezone
            });
        } else {
            logger.info("Error in saving message",err);
            common.sendErrorResponse(res,errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }));
        }
    });
});

router.get('/get/messages/all', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);

    var message = common.createMessageObject();
    message["messageReferenceId"] = messageReferenceId;

    messageManagementObj.getAllConversations(messageReferenceId,function (err,result) {

    });
});

module.exports = router;