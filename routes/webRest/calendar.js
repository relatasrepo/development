
var express = require('express');
var router = express.Router();
var moment = require('moment');
             require('moment-range');
var moment_t = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactions = require('../../dataAccess/interactionManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var appCredentials = require('../../config/relatasConfiguration');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var contactClassSupport = require('../../common/contactsSupport');
var taskManagementClass = require('../../dataAccess/taskManagementClass');
var eventDataAccess = require('../../dataAccess/eventDataAccess');

var eventAccessObj = new  eventDataAccess();
var taskManagementClassObj = new taskManagementClass();
var contactSupportObj = new contactClassSupport();
var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var appCredential = new appCredentials();
var meetingClassObj = new meetingManagement();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionObj = new interactions();

var statusCodes = errorMessagesObj.getStatusCodes();
var domain = appCredential.getDomain();


router.post('/calendar/data/week',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        common.getProfileOrStoreProfileInSession(userId,req,function(user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(req.body.timezone)){
                    timezone = req.body.timezone;
                }
                else if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                    timezone = user.timezone.name;
                }
                else timezone = 'UTC';
                var dateMin;
                var dateMax;
                if(common.checkRequired(req.body.startDate) && common.checkRequired(req.body.endDate)){
                    dateMin = moment_t(req.body.startDate).tz(timezone);
                    dateMax = moment_t(req.body.endDate).tz(timezone);
                    getCalenderDataWithDate(userId,dateMin,dateMax,user.emailId,req,res);
                }
                else{
                    res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'START_END_DATES_MISSED'}));
                }
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        })
    }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'}));
});

function getCalenderDataWithDate(userId,dateMin,dateMax,emailId,req,res){
    var data = {
        counts:{
            meetings:0,
            tasks:0,
            people:0,
            busyHours:0
        }
    };
    var projectionMeetings = {
        invitationId:1,
        senderId:1,
        senderName:1,
        senderEmailId:1,
        senderPicUrl:1,
        "to.receiverId":1,
        "to.receiverEmailId":1,
        "toList.receiverId":1,
        "toList.receiverEmailId":1,
        suggested:1,
        suggestedBy:1,
        "scheduleTimeSlots.isAccepted":1,
        "scheduleTimeSlots.title":1,
        "scheduleTimeSlots.location":1,
        "scheduleTimeSlots.locationType":1,
        "scheduleTimeSlots.end":1,
        "scheduleTimeSlots.start":1
    };
    meetingClassObj.newInvitationsByDateWithProjection(userId,dateMin,dateMax,projectionMeetings,function(meetings){
        if(common.checkRequired(meetings) && meetings.length){
            data.pendingMeetings = meetings;
            data.counts.meetings = meetings.length;
        }
        else{
            data.pendingMeetings = [];
        }
        interactionObj.interactionsByDateMinMaxDashboardWithoutTasks(userId,dateMin,dateMax,projectionMeetings,function(acceptedMeeting){
            if(common.checkRequired(acceptedMeeting) && acceptedMeeting.length){
                data.acceptedMeetings = acceptedMeeting;
                data.counts.meetings += acceptedMeeting.length;
            }else data.acceptedMeetings = [];
            taskManagementClassObj.getTasksForDateAll(userId,dateMin,dateMax,function(error,tasks){
                if(common.checkRequired(tasks) && tasks.length){
                    data.tasks = tasks;
                    data.counts.tasks = tasks.length;
                }else data.tasks = [];
                eventAccessObj.getAllSubscribedEventsDate(userId,dateMin,dateMax,{eventName:1,locationName:1,startDateTime:1,endDateTime:1},function(events){
                    if(common.checkRequired(events) && events.length){
                        data.events = events;
                    }else data.events = [];

                    if(data.acceptedMeetings.length > 0){
                        data.counts.busyHours = getBusyHours(data.acceptedMeetings);
                        var all = data.acceptedMeetings.concat(data.pendingMeetings);
                        data.counts.people = getPeopleInvolved(all,emailId);
                    }

                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":data
                    });
                })
            })
        })
    })
}

function getPeopleInvolved(meetings,emailId){
    var emailIdArr = [];
    for(var i=0; i<meetings.length; i++){
        if(meetings[i].senderEmailId != emailId){
            emailIdArr.push(meetings[i].senderEmailId)
        }
        if(meetings[i].selfCalendar){
            for(var j=0; j<meetings[i].toList.length; j++){
                if(meetings[i].toList[j].receiverEmailId != emailId){
                    emailIdArr.push(meetings[i].toList[j].receiverEmailId);
                }
            }
        }
        else{
            if(meetings[i].to.receiverEmailId != emailId){
                emailIdArr.push(meetings[i].to.receiverEmailId);
            }
        }
    }
    if(emailIdArr.length > 0){
        emailIdArr = common.removeDuplicate_id(emailIdArr,true);
    }
    return emailIdArr.length;
}

function getBusyHours(meetings){
    var busyMinutes = 0;
    for(var i=0; i<meetings.length; i++){
        if(common.checkRequired(meetings[i].scheduleTimeSlots) && meetings[i].scheduleTimeSlots.length > 0){
            for(var j=0; j<meetings[i].scheduleTimeSlots.length; j++){
                if(meetings[i].scheduleTimeSlots[j].isAccepted == true){
                    var r = moment.range(moment_t(meetings[i].scheduleTimeSlots[j].start.date).format()+'/'+moment_t(meetings[i].scheduleTimeSlots[j].end.date).format());
                    busyMinutes += r.diff('minutes');
                }
            }
        }
    }
    return Math.round(busyMinutes/60);
}

/*Calendar, Settings, Help routes*/

router.get('/calendar/new', common.isLoggedInUser,common.checkUserDomain,function(req,res){
        res.render("calendar/calendar");
});

//
//router.get('/notifications/new',common.isLoggedInUser,function(req,res){
//    res.render('notifications/notifications',{isLoggedIn : common.isLoggedInUserBoolean(req)});
//});

module.exports = router;
