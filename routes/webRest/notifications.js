/**
 * Created by naveen on 10/29/15.
 */

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var interactions = require('../../dataAccess/interactionManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var interactionObj = new interactions();
var common = new commonUtility();
var contactObj = new contactClass();

// Router to get notifications page

router.get('/notifications/new',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('notifications/notifications',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

function getLoosingTouchCount(userId,user,timezone,callback){

    var dateMin = moment().tz(timezone);
    dateMin.date(dateMin.date() - 15)
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    contactObj.getAllFavoriteContactsEmailIds(user._id,user.emailId,function(emailIdArr){
        if(emailIdArr.length > 0){
            interactionObj.losingTouchCountByDate(common.castToObjectId(userId),emailIdArr,dateMin,function(losingTouch){
                if(common.checkRequired(losingTouch) && losingTouch > 0) {
                    callback(losingTouch)
                }
                else{
                    callback(0)
                }
            })
        }
        else{
            callback(0)
        }
    });
}

module.exports = router;