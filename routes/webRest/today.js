
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');
var request = require('request');
var _ = require("lodash");
var async = require('async')
var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactions = require('../../dataAccess/interactionManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var contactClassSupport = require('../../common/contactsSupport');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var taskManagementClass = require('../../dataAccess/taskManagementClass');
var oppsManagementClass = require('../../dataAccess/opportunitiesManagement');
var eventDataAccess = require('../../dataAccess/eventDataAccess');
var weatherJS = require('../../common/weather');
var twitterJS = require('../../common/twitter');
var linkedinJS = require('../../common/linkedin');
var ipLocatorJS = require('../../common/ipLocator');
var notificationSupportClass = require('../../common/notificationSupportClass');
var winstonLog = require('../../common/winstonLog');
var ActionItemsManagement = require('../../dataAccess/actionItemsManagement');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');

var companyClassObj = new companyClass();
var notificationSupportClassObj = new notificationSupportClass();
var weatherObj = new weatherJS();
var twitterObj = new twitterJS();
var linkedinObj = new linkedinJS();
var ipLocatorObj = new ipLocatorJS();
var taskManagementClassObj = new taskManagementClass();
var oppsManagementClassObj = new oppsManagementClass();
var eventDataAccessObj = new eventDataAccess();
var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var contactSupportObj = new contactClassSupport();
var meetingClassObj = new meetingManagement();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionObj = new interactions();
var actionItemsManagementObj = new ActionItemsManagement();
var logLib = new winstonLog();
var logger =logLib.getWinston();

var statusCodes = errorMessagesObj.getStatusCodes();

// var io = require('socket.io');

// var memwatch = require('memwatch-next');
//memwatch.setup();

/* memwatch.on('leak', function(info) {
    logger.info('Memory leak detected: ', info)
});

memwatch.on('stats', function(stats) {
    logger.info('Memory stats detected: ', stats);
}); */

function getLoginInfo(req){
    var obj = {
        isLoggedIn :common.isLoggedInUserBoolean(req)
    };
    if(!obj.isLoggedIn){
        obj.disableIcons = 'disable-icons'
        obj.disableNav  =   'wrapper'

    }else obj.disableIcons = ''
    obj.disableNav    =''

    return obj;
}

router.get('/today/simple',common.isLoggedInUser,common.checkUserDomain,function(req, res){
    res.render('todaySeries/todaySimple',getLoginInfo(req));
});

router.get('/today/landing',common.isLoggedInUser,common.checkUserDomain,function(req, res){
    res.render('todaySeries/todayLanding',getLoginInfo(req));
});

router.get('/today/details/:invitationId',common.isLoggedInUser,common.checkUserDomain,function(req, res){
    res.render('todaySeries/todayDetails',getLoginInfo(req));
});

router.get('/schedule/confirmation',function(req, res){
    res.render('schedule/confirmation')
});

router.get('/schedule/ad',function(req, res){
    res.render('schedule/ad')
});

router.get('/schedule/meeting_invite',function(req, res){
    res.render('schedule/meeting_invite')
});

router.get('/schedule/send_req_without_login',function(req, res){
    res.render('schedule/send_req_without_login',req.session.publicUserObj)
});

router.get('/schedule/left_menubar',function(req, res){
    res.render('schedule/left_menubar')
});

router.get('/schedule/header_no_mid',function(req,res){
    res.render('schedule/header_no_mid',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/schedule/ad',function(req, res){
    res.render('schedule/ad');
});

router.get('/todaySeries/leftMeetingBar',function(req, res){
    res.render('todaySeries/leftMeetingBar');
});

router.get('/todaySeries/meetingRightaside',function(req, res){
    res.render('todaySeries/meetingRightaside');
});

router.get('/today/dashboard/v2',common.isLoggedInUser,function(req, res){
    res.render('todaySeries/dashboard3a');
});

router.get('/today/dashboard',common.isLoggedInUser,function(req, res){
    var userId = common.getUserId(req.user);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            actionItemsManagementObj.kickOffActionItemsMailProcess(userId,user.lastLoginDate,req,function(result){
                res.render('todaySeries/dashboard3av2');
            });
            //res.render('customer/landing',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/today/insights/all',common.isLoggedInUser,function(req, res){
    var userId = common.getUserId(req.user);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            // actionItemsManagementObj.kickOffActionItemsMailProcess(userId,user.lastLoginDate,req,function(result){
            // });
            res.render('todaySeries/todayInsightsAll');
            //res.render('customer/landing',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/today/left/section',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(common.checkRequired(user)){
            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';
            var data = {userId:userId,timezone:timezone};
            getTodayPendingMeeting(userId,data,timezone,req,res,user.serviceLogin,user);
        }
        else res.send(errorObj.generateErrorResponse({
            status: statusCodes.PROFILE_ERROR_CODE,
            key: 'USER_PROFILE_NOT_FOUND'
        }));
    });
});

router.post('/get/all/meetings',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    var user = common.getUserFromSession(req);

    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,timezone:1,location:1,currentLocation:1,serviceLogin:1,google:1,outlook:1,dashboardSetting:1},function(error,user){

        if(common.checkRequired(user)){
            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';

            var fromDate = req.body.fromDate;
            var toDate = req.body.toDate;

            var dateMin, dateMax;

            //emailSenderObj.sendUncaughtException('',req.body.dateTime,JSON.stringify(req.body),'TODAY_MEETINGS 1 '+user.emailId)
            if(common.checkRequired(fromDate) && common.checkRequired(toDate)){
                fromDate = new Date(fromDate).toISOString();
                toDate = new Date(toDate).toISOString();

                dateMin = moment(fromDate).tz(timezone);
                dateMax = moment(toDate).tz(timezone);
                
            } else {
                dateMin = moment().tz(timezone);
                dateMax = moment().tz(timezone);
            }

            dateMin.date(dateMin.date())
            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)

            dateMax.date(dateMax.date())
            dateMax.hour(23)
            dateMax.minute(59)
            dateMax.second(59)
            dateMax.millisecond(59)

            dateMin = dateMin.format();
            dateMax = dateMax.format();


            var findOutlookOrGoogle = user.serviceLogin;

            getPendingMeetingsByDate(userId,dateMin,dateMax,findOutlookOrGoogle,function(todayMeetings){

                var meetings = [];

                if(req.body.mobile){
                    _.each(todayMeetings,function (meeting) {
                        if(!meeting.actionItemSlotType){
                            meetings.push(meeting)
                        }
                    });
                } else {
                    meetings = todayMeetings;
                }

                setCorrectLocationType(meetings);

                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data: meetings,
                    userId: userId
                });

            });

        }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });    
});

router.post('/upload/meeting/location',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,timezone:1,location:1,currentLocation:1,serviceLogin:1,google:1,outlook:1,dashboardSetting:1},function(error,user){

        if(common.checkRequired(user)){

            var invitationId = req.body.invitationId;

            var locationObj = {
                country: req.body.country,
                city: req.body.city,
                region: req.body.region,
                latitude: req.body.latitude,
                longitude: req.body.longitude
            }

            uploadMeetingLocation(invitationId, locationObj, function(status){

                if(status) {
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Message: "Location uploaded"
                    });
                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1
                    });
                }

            });

        }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });    
});

router.get('/today/insights/meta', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,location:1,currentLocation:1,serviceLogin:1,companyId:1, google:1,outlook:1,dashboardSetting:1},function(error,user){
    
    companyClassObj.findCompanyByIdWithProjection(user.companyId, {contacts:0}, function(company) {
        var primaryCurrency = "USD",
        currenciesObj = {}

        company.currency.forEach(function (el) {
            currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                primaryCurrency = el.symbol;
            }
        });

        async.parallel([
            function(callback) {
                taskManagementClassObj.getUpcomingTasks(user.emailId, callback);
            },
            function(callback) {
                taskManagementClassObj.getOverdueTasks(user.emailId, callback);
            },
            function(callback) {
                oppsManagementClassObj.getDealsAtRisk([user.emailId], primaryCurrency, currenciesObj,callback);
            },
            function(callback) {
                oppsManagementClassObj.getStaleOpportunitiesCount(user.emailId, user.companyId, callback);
            }, 
            function(callback) {
                getImportantMails(userId, callback);
            }, 
            function(callback) {
                getUpcomingMeetingsForTwoDays(userId,user, callback);
            },
            // function(callback) {
            //     getRecommendedToMeet(user, userId, callback);
            // }
        ], function(err, data) {
            if(err) {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                    Data:[]
                })
            } else {
                formatTodayInsights(data, function(formattedData) {
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data:formattedData
                    })
                })
            }

        })

    })
    })

});

function formatTodayInsights(data, callback) {
    var formattedData = {};

    formattedData.upcomingTasks = data[0].upcomingTasks;
    formattedData.overdueTasks = data[1].overdueTasks;
    formattedData.dealsClosingSoon = data[2];
    formattedData.staleOpportunities = data[3].staleOpportunities;
    formattedData.mailInsights = data[4];
    formattedData.upcomingMeetings = data[5].upcomingMeetings
    // formattedData.peopleRecommendedToMeet = data[6].peopleRecommendedToMeet
    callback(formattedData);
} 

function getImportantMails(userId, callback) {
    var from = moment().subtract(14, "days");

    interactionObj.getNumberOfImportantMails(common.castToObjectId(userId),from,function (err,mails) {

        if(!err && mails && mails.length>0){

            var filteredMails = filterByOnlyRecentInteractionThread(mails)

            var threadIds = _.pluck(filteredMails,"emailThreadId");

            var threadsHaveLatestReplies = [],tempMails = [];

            interactionObj.checkLiuRespondedToLastEmail(common.castToObjectId(userId),from,threadIds,function (err,prevEmails) {

                var important = 0,onlyToMe = 0,followUp = 0,positive = 0,negative = 0;
                var emailContentIds = [];

                _.each(prevEmails,function (el) {
                    if(el.emailId != el.ownerEmailId && el.action == "receiver" ){
                        threadsHaveLatestReplies.push(el.emailThreadId)
                    }
                })

                _.each(filteredMails,function (el) {
                    if(!_.includes(threadsHaveLatestReplies,el.emailThreadId)){
                        tempMails.push(el)
                    }
                })

                _.each(tempMails,function (el) {

                    emailContentIds.push(el.emailContentId);

                    if(el.trackInfo && el.trackInfo.trackResponse){
                        followUp++;
                    }

                    if(el.toCcBcc == 'me'){
                        onlyToMe++;
                    }

                    if((el.eAction && el.eAction == 'important') || el.importance>0){
                        important++;
                    }

                    if(el.sentiment == "Positive"){
                        positive++;
                    }

                    if(el.sentiment == "Negative"){
                        negative++;
                    }
                })

                callback(err, {
                    important:important,
                    onlyToMe:onlyToMe,
                    followUp:followUp,
                    positive:positive,
                    negative:negative,
                    emailContentIds:emailContentIds
                })
                
            });
        } else {
            callback(err, {
                important:0,
                onlyToMe:0,
                followUp:0,
                positive:0,
                negative:0,
                emailContentIds:[]
            })
        }

    });
}

function filterByOnlyRecentInteractionThread(interactions){
    var filteredInteractions = _
        .chain(interactions)
        .groupBy('emailThreadId')
        .map(function(value, key) {
            return _.max(value,"interactionDate")
        })
        .value();

    return filteredInteractions;
}

function getUpcomingMeetingsForTwoDays(userId,user, callback) {
    if(common.checkRequired(user)){
        var timezone;
        if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
            timezone = user.timezone.name;
        }else timezone = 'UTC';

        var fromDate = moment().add(1, "days").format();
        var toDate = moment().add(2, "days").format();

        var dateMin, dateMax;

        //emailSenderObj.sendUncaughtException('',req.body.dateTime,JSON.stringify(req.body),'TODAY_MEETINGS 1 '+user.emailId)
        if(common.checkRequired(fromDate) && common.checkRequired(toDate)){
            fromDate = new Date(fromDate).toISOString();
            toDate = new Date(toDate).toISOString();

            dateMin = moment(fromDate).tz(timezone);
            dateMax = moment(toDate).tz(timezone);
            
        } else {
            dateMin = moment().tz(timezone);
            dateMax = moment().tz(timezone);
        }

        dateMin.date(dateMin.date())
        dateMin.hour(0)
        dateMin.minute(0)
        dateMin.second(0)
        dateMin.millisecond(0)

        dateMax.date(dateMax.date())
        dateMax.hour(23)
        dateMax.minute(59)
        dateMax.second(59)
        dateMax.millisecond(59)

        dateMin = dateMin.format();
        dateMax = dateMax.format();

        var findOutlookOrGoogle = user.serviceLogin;

        getPendingMeetingsByDate(userId,dateMin,dateMax,findOutlookOrGoogle,function(todayMeetings){

            var meetings = [];

            _.each(todayMeetings,function (meeting) {
                if(!meeting.actionItemSlotType){
                    meetings.push(meeting)
                }
            });

            setCorrectLocationType(meetings);
            callback(null, {upcomingMeetings:meetings.length})

        });

    }else {
        callback(null, {upcomingMeetings:0})
    }
}

function getRecommendedToMeet(user, userId, callback) {
    var dateMin = moment().add(1, "days");
    var dateMax = moment().add(3, "days");
    var userIdArr = [];

    userIdArr.push(userId);

    dateMin.date(dateMin.date())
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    dateMax.date(dateMax.date())
    dateMax.hour(0)
    dateMax.minute(0)
    dateMax.second(0)
    dateMax.millisecond(0)

    var meetingsLocations = [];
    var participants = [];

    var outlookOrGoogle = null;
        
        if(user.google.length>0){
            outlookOrGoogle = 'google'
        } else if(user.outlook.length>0) {
            outlookOrGoogle = 'outlook'
        }

        meetingClassObj.userNexTwoWeeksTravelingTo(userIdArr, dateMin,dateMax,outlookOrGoogle,function (meetings) {

            if (meetings.length > 0) {
                _.each(meetings, function (meeting) {
                    _.each(meeting.scheduleTimeSlots, function (slot) {

                        _.each(meeting.participants, function (participant) {

                            if(user.emailId !== participant.emailId){
                                participants.push(participant.emailId)
                            }
                        });

                        if(common.isMeetingLocationInPerson(slot.location)){

                            meetingsLocations.push({
                                location: slot.location,
                                invitationId: meeting.invitationId,
                                date: slot.start.date,
                                participants:participants
                            });

                        }
                    });
                });

                callback(null, {peopleRecommendedToMeet:participants.length})

            } else {
                callback(null, {peopleRecommendedToMeet:0})
            }
        });

}

function getTodayPendingMeeting(userId,data,timezone,req,res,findOutlookOrGoogle,user){
    var dateMin = moment().tz(timezone)
    // moment().add(1, "days")
    // dateMin.hour(0)
    // dateMin.minute(0)
    // dateMin.second(0)
    // dateMin.millisecond(0)

    // var dateMin = moment().add(1, "days");
    // // moment().add(1, "days")


    // Getting today's meetings was removed based on Sudip's suggestion
    // Reference: https://relatas.atlassian.net/projects/WB/issues/WB-341?filter=allopenissues
    // -Naveen - 23 sep 2016
    
    // data.pendingMeetings = errorMessagesObj.getMessage('NEW_INVITATIONS_NOT_FOUND');
    // getTodayConfirmedMeetings(userId,data,timezone,req,res,findOutlookOrGoogle)

    meetingClassObj.userNewMeetingsByDate(userId,dateMin.format(),findOutlookOrGoogle,function(meetings){

        if(common.checkRequired(meetings) && meetings.length > 0){
            setCorrectLocationType(meetings);
            data.pendingMeetings = meetings;
            getTodayConfirmedMeetings(userId,data,timezone,req,res,findOutlookOrGoogle,user)
        }
        else{
            data.pendingMeetings = errorMessagesObj.getMessage('NEW_INVITATIONS_NOT_FOUND');
            getTodayConfirmedMeetings(userId,data,timezone,req,res,findOutlookOrGoogle,user)
        }
    });
}

function getTodayConfirmedMeetings(userId,data,timezone,req,res,findOutlookOrGoogle,user){

    var dateMin = moment().tz(timezone);
    var dateMax = moment().tz(timezone);
    dateMin.date(dateMin.date())
    /*dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)*/

    //dateMax.date(dateMax.date())
    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(59)
    dateMax.millisecond(59)

    dateMin = dateMin.format()
    dateMax = dateMax.format()
    meetingClassObj.todayMeetings(userId,dateMin,dateMax,findOutlookOrGoogle,function(confirmedMeetings){
        if(common.checkRequired(confirmedMeetings) && confirmedMeetings.length > 0){
            setCorrectLocationType(confirmedMeetings)
            data.confirmedMeetings = confirmedMeetings;
            getTodayTasks(userId,dateMin,dateMax,timezone,data,req,res,user)
        }
        else{
            data.confirmedMeetings = errorMessagesObj.getMessage('CONFIRMED_MEETINGS_NOT_FOUND');
            getTodayTasks(userId,dateMin,dateMax,timezone,data,req,res,user)
        }
    })
}

function getTodayTasks(userId,dateMin,dateMax,timezone,data,req,res,user){
    taskManagementClassObj.getPendingTasksByDate(userId,dateMin,dateMax,function(tasks){
        if(common.checkRequired(tasks) && tasks.length > 0){
           data.pendingTasks = tasks;
            getFutureMeetings(userId,timezone,data,req,res,user)
        }
        else{
            data.pendingTasks = errorMessagesObj.getMessage('NO_TASKS_FOUND');
            getFutureMeetings(userId,timezone,data,req,res,user)
        }
    })
}

function getFutureMeetings(userId,timezone,data,req,res,user){
    var dateMax = moment().tz(timezone);

    dateMax.date(dateMax.date() + 1);
    dateMax.hours(0)
    dateMax.minutes(0)

    meetingClassObj.upcomingMeetings(userId,dateMax,user,function(upComingMeetings){

        if(common.checkRequired(upComingMeetings) && upComingMeetings.length > 0){
            setCorrectLocationType(upComingMeetings)
            data.upcomingMeetings = upComingMeetings;
        }
        else{
            data.upcomingMeetings = errorMessagesObj.getMessage('UPCOMING_MEETINGS_NOT_FOUND');
        }

        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data": data
        });
    });
}

router.get('/today/weather/forecast/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    getWeather(userId,req,res);
});

router.get('/today/weather/forecast/web/by/location', common.isLoggedInUserToGoNext, function(req, res) {
    var lNew = req.query.location;
    if (common.checkRequired(lNew)) {
        weatherObj.getForecast(lNew, function(error, forcast, query) {

            if (common.checkRequired(forcast)) {
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": forcast
                });
            } else res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'NO_LOCATION_DETAILS_FOUND' }));
        });
    } else res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'NO_LOCATION_DETAILS_FOUND' }));
});

function getWeather(userId,req,res){

    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,location:1,currentLocation:1},function(error,user){
        if(common.checkRequired(user)){

            var lat,lan, location = common.getLocation(user.location,user.currentLocation);

            if(common.checkRequired(location)){
                weatherObj.getForecast(location,function(error,forcast,query){
                    if(common.checkRequired(forcast) && common.checkRequired(forcast)){
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":forcast
                        });
                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'NO_LOCATION_DETAILS_FOUND'}));
                });
            }
            else{

                var ip = ipLocatorObj.getIP(req);
                ipLocatorObj.lookup(ip,null,function(locationNew){
                    var lNew;

                    if(common.checkRequired(locationNew) && common.checkRequired(locationNew.city) && common.checkRequired(locationNew.country)){
                        lNew = locationNew.city;
                        if(common.checkRequired(locationNew.country)){
                            lNew += ', '+locationNew.country
                        }
                    }

                    if(common.checkRequired(lNew)){

                        weatherObj.getForecast(lNew,function(error,forcast,query){
                            if(common.checkRequired(forcast)){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":forcast
                                });
                            }
                            else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'NO_LOCATION_DETAILS_FOUND'}));
                        });
                    }
                    else res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'NO_LOCATION_DETAILS_FOUND'}));
                })
            }
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });
}

router.post('/today/top/section/info',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var profile = req.session.profile;
    var date;
    if(common.checkRequired(req.body.dateTime)){
        date = moment(req.body.dateTime)
    }
    var timezone;
    if(common.checkRequired(profile)){

        if(common.checkRequired(profile.timezone) && common.checkRequired(profile.timezone.name)){
            timezone = profile.timezone.name;
        }else timezone = 'UTC';

        var dateMin = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);
        var dateMax = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);

        dateMin.hour(0)
        dateMin.minute(0)
        dateMin.second(0)
        dateMin.millisecond(0)

        dateMax.hour(23)
        dateMax.minute(59)
        dateMax.second(59)
        dateMax.millisecond(59)

        dateMin = dateMin.format()
        dateMax = dateMax.format()

        getTodayMeetingCount(userId,{},dateMin,dateMax,req,res,profile.serviceLogin)
    }
    else{
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,user){
            if(common.checkRequired(user)){
                req.session.profile = user;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var dateMin = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);
                var dateMax = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);

                dateMin.hour(0)
                dateMin.minute(0)
                dateMin.second(0)
                dateMin.millisecond(0)

                dateMax.hour(23)
                dateMax.minute(59)
                dateMax.second(59)
                dateMax.millisecond(59)

                dateMin = dateMin.format()
                dateMax = dateMax.format()

                getTodayMeetingCount(userId,{},dateMin,dateMax,req,res,req.session.profile.serviceLogin)
            }
            else{
                logger.info('Error in /today/top/section/info user: '+req.user ,error);
                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            }
        })
    }
});

function getTodayMeetingCount(userId,data,dateMin,dateMax,req,res,serviceLogin,callback){

    var findOutlookOrGoogle = serviceLogin;
    meetingClassObj.userMeetingsByDate(userId,dateMin,dateMax,{scheduleTimeSlots:1},findOutlookOrGoogle,function(meetings){

        if(common.checkRequired(meetings) && meetings.length > 0){

            var tempArr = [];
            for(var i =0;i<meetings.length;i++){

                for(var j=0;j<meetings[i].scheduleTimeSlots.length;j++){
                    if(meetings[i].scheduleTimeSlots[j].locationType == "In-Person"){
                        data.location = meetings[i].scheduleTimeSlots[j].location;
                    }

                    if(meetings[i].scheduleTimeSlots[j].title !== 'Relatas Insights responsePending' && meetings[i].scheduleTimeSlots[j].title !== 'Relatas Insights losingTouch' && meetings[i].scheduleTimeSlots[j].title !== 'Relatas Insights followUp' && meetings[i].scheduleTimeSlots[j].title !== 'Relatas Insights meetingFollowUp' && meetings[i].scheduleTimeSlots[j].title !== 'Relatas Insights travellingToLocation' && meetings[i].scheduleTimeSlots[j].title !== 'Relatas Insights remindToConnect'){
                        tempArr.push(1)
                    }
                }
            }

            data.todayMeetings = tempArr.length;

            if(callback){
                callback(meetings.length)
            }else
            getTodayTasksCount(userId,dateMin,dateMax,data,req,res)
        }
        else{
            data.todayMeetings = 0;
            if(callback){
                callback(0)
            }else
            getTodayTasksCount(userId,dateMin,dateMax,data,req,res)
        }
    })
}

function getTodayTasksCount(userId,dateMin,dateMax,data,req,res){
    taskManagementClassObj.getTasksForDateAll(userId,dateMin,dateMax,function(error,tasks){
        if(common.checkRequired(tasks) && tasks.length > 0){
            data.todayTasks = tasks.length;
            getRandomImage(data,req,res)
        }
        else{
            data.todayTasks = 0;
            getRandomImage(data,req,res)
        }
    })
}

function getRandomImage(data,req,res){

    fs.readdir('./public/images/app_images/drawable-hdpi/', function(er,files){

        if(common.checkRequired(files) && files.length > 0){
            var randomNum = common.getRandomNumber(0,files.length-1);

            if(req.session.randomFileNum == randomNum){

                if(randomNum < files.length-1){
                    randomNum += 1;
                }
                else if(randomNum == files.length-1 ){
                    randomNum = randomNum - 1;
                }
            }

            req.session.randomFileNum = randomNum;
            data.image = files[randomNum];
        }

        function getFiles (dir, files_){
            files_ = files_ || [];
            var files = fs.readdirSync(dir);
            for (var i in files){
                var name = dir + '/' + files[i];
                if (fs.statSync(name).isDirectory()){
                    getFiles(name, files_);
                } else {
                    files_.push(name);
                }
            }
            return files_;
        }

        var images = getFiles('./public/images/app_images/drawable-hdpi');
        var dateDiff = new Date() - new Date(2015,10,01);

        var todayDay = new Date();

        data.imageNew = todayDay.getDate()+'.jpg';

        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data":data
        });
    });
}

router.post('/today/agenda/section/web',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,timezone:1,location:1,currentLocation:1,serviceLogin:1,google:1,outlook:1,dashboardSetting:1},function(error,user){
        if(common.checkRequired(user)){
            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';

            var date = req.body.dateTime;

            //emailSenderObj.sendUncaughtException('',req.body.dateTime,JSON.stringify(req.body),'TODAY_MEETINGS 1 '+user.emailId)
            if(common.checkRequired(date)){
                date = new Date(date).toISOString();
            }
            //emailSenderObj.sendUncaughtException('',date,JSON.stringify(req.body),'TODAY_MEETINGS 2 '+user.emailId)
            var dateMin = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);
            var dateMax = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);

            dateMin.date(dateMin.date())
            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)

            dateMax.date(dateMax.date())
            dateMax.hour(23)
            dateMax.minute(59)
            dateMax.second(59)
            dateMax.millisecond(59)

            dateMin = dateMin.format();
            dateMax = dateMax.format();

            var findOutlookOrGoogle = user.serviceLogin;

            getPendingMeetingsByDate(userId,dateMin,dateMax,findOutlookOrGoogle,function(todayMeetings){

                var meetings = [];

                if(req.body.mobile){
                    _.each(todayMeetings,function (meeting) {
                        if(!meeting.actionItemSlotType){
                            meetings.push(meeting)
                        }
                    });
                } else {
                    meetings = todayMeetings;
                }

            setCorrectLocationType(meetings);

            getTodayTasks2(userId,dateMin,dateMax,function(tasks){
                if(meetings.length > 0){
                    getTodayEvents(req,res,userId,dateMin,dateMax,{meetings:meetings,tasks:tasks,userId:userId})
                }
                else{
                    getTodayEvents(req,res,userId,dateMin,dateMax,{meetings:meetings,tasks:tasks,userId:userId})
                }
            });
        });

        }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });
});

router.post('/today/agenda/section/web/v2',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,timezone:1,location:1,currentLocation:1,serviceLogin:1,google:1,outlook:1,dashboardSetting:1},function(error,user){
        if(common.checkRequired(user)){
            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';

            var date = req.body.dateTime;

            //emailSenderObj.sendUncaughtException('',req.body.dateTime,JSON.stringify(req.body),'TODAY_MEETINGS 1 '+user.emailId)
            if(common.checkRequired(date)){
                date = new Date(date).toISOString();
            }
            //emailSenderObj.sendUncaughtException('',date,JSON.stringify(req.body),'TODAY_MEETINGS 2 '+user.emailId)
            var dateMin = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);
            var dateMax = common.checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);

            dateMin.date(dateMin.date())
            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)

            dateMax.date(dateMax.date())
            dateMax.hour(23)
            dateMax.minute(59)
            dateMax.second(59)
            dateMax.millisecond(59)

            dateMin = dateMin.format();
            dateMax = dateMax.format();

            var findOutlookOrGoogle = user.serviceLogin;

            getPendingMeetingsByDateV2(userId,dateMin,dateMax,findOutlookOrGoogle,function(todayMeetings){

                var meetings = [];

                if(req.body.mobile){
                    _.each(todayMeetings,function (meeting) {
                        if(!meeting.actionItemSlotType){
                            meetings.push(meeting)
                        }
                    });
                } else {
                    meetings = todayMeetings;
                }

            setCorrectLocationType(meetings);
        });

        }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });
});

function getPendingMeetingsByDate(userId,dateMin,dateMax,findOutlookOrGoogle,callback){
    var projection = {"recurrenceId":1,invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1,actionItemSlot:1,actionItemSlotType:1,designation:1,companyName:1}
    
    meetingClassObj.userMeetingsByDate(userId,new Date(dateMin),new Date(dateMax),projection,findOutlookOrGoogle,function(meetings){
        if(common.checkRequired(meetings) && meetings.length > 0){
            callback(meetings)
        }else callback([])
    })
}

function uploadMeetingLocation(invitationId,upLoadedLocationObj,callback){
    
    meetingClassObj.updateUploadedLocation(invitationId, upLoadedLocationObj, function(err, result){
        if(!err && result) {
            callback(true);
        } else {
            callback(false);
        }
    })
}

function getPendingMeetingsByDateV2(userId,dateMin,dateMax,findOutlookOrGoogle,callback){
    var projection = {"recurrenceId":1,invitationId:1,scheduleTimeSlots:1,senderEmailId:1,to:1,toList:1,actionItemSlot:1,actionItemSlotType:1}
    
    meetingClassObj.userMeetingsByDate(userId,new Date(dateMin),new Date(dateMax),projection,findOutlookOrGoogle,function(meetings){
        if(common.checkRequired(meetings) && meetings.length > 0){
            callback(meetings)
        }else callback([])
    })
}

function getTodayTasks2(userId,dateMin,dateMax,callback){

    taskManagementClassObj.getTasksForDateAllPopulate(userId,dateMin,dateMax,function(tasks){
        if(common.checkRequired(tasks) && tasks.length > 0){
            callback(tasks)
        }else callback([])
    })
}

function getTodayEvents(req,res,userId,dateMin,dateMax,data){
    eventDataAccessObj.getAllSubscribedEventsDate(userId,dateMin,dateMax,{eventName:1,locationName:1,startDateTime:1,endDateTime:1,createdBy:1},function(events){
        data.events = events;

        if(data.meetings.length > 0 || data.tasks.length > 0 || data.events.length > 0){
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":data
            });
        } else {
            res.send({
                "SuccessCode": 0,
                "Message": errorMessagesObj.getMessage("AGENDA_NOT_FOUND"),
                "ErrorCode": 0,
                "Data":data
            });
        }
    })
}

router.post('/today/status/count',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var date = moment();
    if(common.checkRequired(req.body.dateTime)){
        date = req.body.dateTime;
    }

    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1},function(error,user){
        if(common.checkRequired(user)){
            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';

            getUnPreparedMeetingsCount(userId,user,{},date,timezone,req,res,false);
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    })

});
// Changed to new(unconfirmed) meeting count
function getUnPreparedMeetingsCount(userId,user,data,date,timezone,req,res,callback){
    meetingClassObj.userNewMeetingsByDate(userId,moment(date).tz(timezone).format(),user.serviceLogin,function(meetings){

        if(common.checkRequired(meetings) && meetings.length > 0){
            data.unpreparedMeetingsCount = meetings.length;
            getLoosingTouchCount(userId,user,data,date,timezone,req,res,callback)
        }
        else{
            data.pendingMeetings = 0;
            getLoosingTouchCount(userId,user,data,date,timezone,req,res,callback)
        }
    });
}

function getLoosingTouchCount(userId,user,data,date,timezone,req,res,callback){

    contactSupportObj.getContactsLosingTouch(common.castToObjectId(userId),user,timezone,'count',0,20,true,function(response){
        data.losingTouchCount = response.Data.count
        if(callback){
            callback(response.Data.count)
        }else
            getPastSevenDaysInteractionsStatus(userId,user.emailId,date,timezone,data,req,res,false)
    });
}

function getPastSevenDaysInteractionsStatus(userId,emailId,date,timezone,data,req,res,callback){
    common.interactionDataFor(function(days){
        var end = moment(date).tz(timezone);

        end.hours(23);
        end.minutes(59);
        end.seconds(59);
        end.milliseconds(0);

        var start = end.clone();
        start.date(start.date() - days);
        start.hours(0);
        start.minutes(0);
        start.seconds(0);
        start.milliseconds(0);

        start = start.format();
        end = end.format();

        interactionObj.pastSevenDayInteractionStatus(common.castToObjectId(userId),emailId,{start:start,end:end},false,function(obj){
            if(common.checkRequired(obj) && (obj.youNotReplied.length > 0 || obj.othersNotReplied.length > 0)){
                data.emailResponsesPending = obj.youNotReplied.length;
                data.emailAwaitingResponses = obj.othersNotReplied.length;
            }
            else{
                data.emailResponsesPending = 0;
                data.emailAwaitingResponses = 0;
            }

            if(!callback){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":data
                });
            }else callback(data.emailAwaitingResponses || 0);
        });
    });
}

router.post('/today/traveling/location',common.isLoggedInUserToGoNext,function(req,res){
    
    var userId = common.getUserId(req.user);

    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,location:1,currentLocation:1,timezone:1},function(error,user){
        if(common.checkRequired(user)){

            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';

            var dateMin = moment().tz(timezone);
            var dateMax = moment().tz(timezone);

            if(req.body.dateTime){
                 dateMin = moment(req.body.dateTime).tz(timezone);
                 dateMax = moment(req.body.dateTime).tz(timezone);
            }

            /*if(dateMin.format("DD-MM-YYYY") != moment().tz(timezone).format("DD-MM-YYYY")){
                dateMin.hour(0)
                dateMin.minute(0)
                dateMin.second(0)
                dateMin.millisecond(0)
            }*/
            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)
            dateMax.hour(23)
            dateMax.minute(59)
            dateMax.second(59)
            dateMax.millisecond(59)

            dateMin = dateMin.format();
            dateMax = dateMax.format();

            var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
            var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
            var location = req.body.location;
            var designation = req.body.designation;

            if(common.checkRequired(location)){
                contactObj.getContactsMatchedLocation(common.castToObjectId(userId),user.emailId,location,designation,function(eror,contacts,userIdList,emailIdList){

                    if(emailIdList && emailIdList.length > 0){
                        var date = moment().tz(timezone).format()
                        interactionObj.interactionIndividualCountByLocation(common.castToObjectId(userId),user.emailId,emailIdList,location,designation,date,false,function(contacts){

                            var total = contacts.length;
                            var results = contacts.splice(skip,limit);
                            if(total == 0){
                                res.send({
                                    "SuccessCode": 0,
                                    "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION"),
                                    "ErrorCode": 1,
                                    "Data":{
                                        total:total,
                                        skipped:skip,
                                        limit:limit,
                                        returned:results.length,
                                        contacts:results,
                                        location:location,
                                        ignore:true
                                    }
                                });
                            }
                            else res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":{
                                    total:total,
                                    skipped:skip,
                                    limit:limit,
                                    returned:results.length,
                                    contacts:results,
                                    location:location,
                                    ignore:true
                                }
                            });
                        })
                    }
                    else{
                        res.send({
                            "SuccessCode": 0,
                            "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION"),
                            "ErrorCode": 1,
                            "Data":{
                                total:0,
                                skipped:skip,
                                limit:limit,
                                returned:0,
                                contacts:[],
                                location:location,
                                ignore:true
                            }
                        });
                    }
                })
            }
            else{

                //This function gets the location of the user on Today 3A from the meeting location. This is calculated on the fly and not stored anywhere.
                meetingClassObj.userCurrentDayTravelingTo(userId,dateMin,dateMax,common.getLocation(user.location,user.currentLocation),function(timeSlots){

                    if(common.checkRequired(timeSlots) && common.checkRequired(timeSlots.scheduleTimeSlots) && timeSlots.scheduleTimeSlots.length > 0){
                        var location = timeSlots.scheduleTimeSlots[0].location;

                        var loc = location.replace(/[^\w\s]/gi, '');

                        var participants = getParticipatEmailIds(timeSlots.senderEmailId,timeSlots.to,timeSlots.toList);
                        request('http://maps.googleapis.com/maps/api/geocode/json?address='+loc,function(e,r,b){
                            var new_location = null;

                            if(e){
                                logger.info("Error Google location Search searchByAddress() ",e, 'search content ',loc);
                            }
                            else{
                                try{
                                    b = JSON.parse(b);
                                }
                                catch(e){
                                    logger.info("Exception Google location Search searchByAddress() ",e, 'search content ',loc);
                                }

                                if(b && b.results && b.results.length > 0 && b.results[0] && b.results[0].address_components && b.results[0].address_components.length > 0){
                                    for(var i=0; i<b.results[0].address_components.length; i++){
                                        if(b.results[0].address_components[i].types.indexOf("locality") != -1){
                                            new_location = b.results[0].address_components[i].long_name;
                                            break;
                                        }
                                    }
                                  //  new_location = "Gurgaon"
                                    if(common.checkRequired(new_location)){
                                        contactObj.getContactsMatchedLocation(common.castToObjectId(userId),user.emailId,new_location,null,function(eror,contacts,userIdList,emailIdList) {
                                            var date = moment().tz(timezone).format()
                                            if (emailIdList && emailIdList.length > 0) {
                                                interactionObj.interactionIndividualCountByLocation(common.castToObjectId(userId),user.emailId,emailIdList,new_location,null,date,false,function(contacts){


                                                    var total = contacts.length;
                                                    var results = contacts.splice(skip,limit);
                                                    if(total == 0){
                                                        res.send({
                                                            "SuccessCode": 0,
                                                            "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION"),
                                                            "ErrorCode": 1,
                                                            "Data":{
                                                                total:total,
                                                                skipped:skip,
                                                                limit:limit,
                                                                returned:results.length,
                                                                contacts:results,
                                                                location:new_location,
                                                                participants:participants,
                                                                meeting:true
                                                            }
                                                        });
                                                    }
                                                    else{
                                                        res.send({
                                                            "SuccessCode": 1,
                                                            "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION"),
                                                            "ErrorCode": 0,
                                                            "Data":{
                                                                total:total,
                                                                skipped:skip,
                                                                limit:limit,
                                                                returned:results.length,
                                                                contacts:results,
                                                                location:new_location,
                                                                participants:participants,
                                                                meeting:true
                                                            }
                                                        });
                                                    }
                                                })
                                            }
                                            else{
                                                res.send({
                                                    "SuccessCode": 0,
                                                    "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION"),
                                                    "ErrorCode": 1,
                                                    "Data":{
                                                        total:0,
                                                        skipped:skip,
                                                        limit:limit,
                                                        returned:0,
                                                        contacts:[],
                                                        location:new_location,
                                                        participants:participants,
                                                        meeting:true
                                                    }
                                                });
                                            }
                                        });
                                    }
                                    else{
                                        contactsFromIpLocation(userId,user,timezone,skip,limit,req,res)
                                    }
                                }
                                else{
                                    contactsFromIpLocation(userId,user,timezone,skip,limit,req,res)
                                }
                            }
                        });
                    }
                    else{
                        contactsFromIpLocation(userId,user,timezone,skip,limit,req,res)
                    }
                })
            }
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });
});

function contactsFromIpLocation(userId,user,timezone,skip,limit,req,res){
    var ip = ipLocatorObj.getIP(req);
    ipLocatorObj.lookup(ip,null,function(locationObj){
        if(locationObj && locationObj.status != 'fail') {
            var location = common.getLocation(null, locationObj);
            getContactsByLocation(userId,user,timezone,location,skip,limit,req,res)
        }
        else{
            var locationNew = common.getLocation(user.location,user.currentLocation);
            if(common.checkRequired(locationNew)){
                getContactsByLocation(userId,user,timezone,locationNew,skip,limit,req,res)
            }
            else{

                res.send({
                    "SuccessCode": 0,
                    "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION"),
                    "ErrorCode": 1,
                    "Data":{
                        notValid:true,
                        Message:errorMessagesObj.getMessage("LOCATION_IS_NOT_VALID")
                    }
                });
            }
        }
    });
}

function getContactsByLocation(userId,user,timezone,location,skip,limit,req,res){
    contactObj.getContactsMatchedLocation(common.castToObjectId(userId),user.emailId,location,null,function(eror,contacts,userIdList,emailIdList) {
        if (emailIdList && emailIdList.length > 0) {
            var date = moment().tz(timezone).format()
            interactionObj.interactionIndividualCountByLocation(common.castToObjectId(userId),user.emailId,emailIdList,location,null,date,false,function(contacts){
                var total = contacts.length;
                var results = contacts.splice(skip,limit);
                if(total == 0){
                    res.send({
                        "SuccessCode": 0,
                        "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION"),
                        "ErrorCode": 1,
                        "Data":{
                            total:total,
                            skipped:skip,
                            limit:limit,
                            returned:results.length,
                            contacts:results,
                            location:location
                        }
                    });
                }
                else res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        total:total,
                        skipped:skip,
                        limit:limit,
                        returned:results.length,
                        contacts:results,
                        location:location
                    }
                });
            })
        }
        else{
            res.send({
                "SuccessCode": 0,
                "Message": errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_BY_LOCATION"),
                "ErrorCode": 1,
                "Data":{
                    total:0,
                    skipped:skip,
                    limit:limit,
                    returned:0,
                    contacts:[],
                    location:location
                }
            });
        }
    });
}

router.get('/today/social/updates/count',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,linkedin:1,facebook:1,twitter:1},function(error,user){
        if(common.checkRequired(user)){
            common.interactionDataFor(function(days){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var showSetup = true;
                if((common.checkRequired(user.linkedin) && common.checkRequired(user.linkedin.id)) || (common.checkRequired(user.twitter) && common.checkRequired(user.twitter.id)) || (common.checkRequired(user.facebook) && common.checkRequired(user.facebook.id))){
                    showSetup = false;
                }

                var dateMin = moment().tz(timezone);
                var dateMax = moment().tz(timezone);
                dateMin.date(dateMin.date() - days)
                dateMin.hour(0)
                dateMin.minute(0)
                dateMin.second(0)
                dateMin.millisecond(0)

                dateMin = dateMin.format()
                dateMax = dateMax.format()

                interactionObj.getInteractionsByDateSocialUpdatesCount(common.castToObjectId(userId),dateMin,function(response){
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":{showSetup:showSetup, count:response || 0}
                    });
                })
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });
});

function getLinkedinFeedContent(feed){
    var title = '';

    if(feed && common.checkRequired(feed.comment)){
        title = feed.comment;
    }

     if(!common.checkRequired(title) && feed && feed.content){
        if(common.checkRequired(feed.content.title)){
            title = feed.content.title
        }
        else if(feed.content.description){
            title = feed.content.description
        }
        else if(feed.content.eyebrowUrl){
            title = feed.content.eyebrowUrl
        }
        else if(feed.content.shortenedUrl){
            title = feed.content.shortenedUrl
        }
        else if(feed.content.submittedImageUrl){
            title = feed.content.submittedImageUrl
        }
        else if(feed.content.submittedUrl){
            title = feed.content.submittedUrl
        }
        else if(feed.content.thumbnailUrl){
            title = feed.content.thumbnailUrl
        }
    }

    return title;
}

router.get('/today/details/social/top/trending',common.isLoggedInUserToGoNext,function(req,res){
   if(common.checkRequired(req.query.id)){
       var query = {_id:req.query.id}
        userManagementObj.findUserProfileByIdWithCustomFields(req.query.id,{emailId:1,linkedin:1,twitter:1,facebook:1},function(error,user){
            if(user){
                var setup_social_accounts = false;
                if(common.checkRequired(user.linkedin) && common.checkRequired(user.linkedin))
                linkedinObj.getOtherUserProfileWithoutCommonConnections(user.linkedin,function(lerr,lFeed){
                    twitterObj.getTwitterFeed_Top_Trending_post_B(user.twitter,true,1,function(terr,tFeed){

                        //getFacebookFeeds() is not defined anywhere. Commented by Naveen on 15 Dec 2016
                        //facebookObj.getFacebookFeeds(user.facebook,true,1,function(ferr,fFeed){
                            var latestFeed = [];

                            if(!lerr && lFeed && lFeed.currentShare){
                                lFeed = lFeed.currentShare;
                                latestFeed.push({
                                    date:moment(lFeed.timestamp).format(),
                                    source:'linkedin',
                                    content:getLinkedinFeedContent(lFeed)
                                });
                            }

                            if(!terr && tFeed && tFeed.length > 0){
                                tFeed = tFeed[0];
                                latestFeed.push({
                                    date:moment(tFeed.created_at).format(),
                                    source:'twitter',
                                    content:tFeed.text
                                })
                            }

                            //if(ferr && fFeed && fFeed.length > 0){
                            //    fFeed = fFeed[0];
                            //    latestFeed.push({
                            //        date:moment(fFeed.created_time).format(),
                            //        source:'facebook',
                            //        content:fFeed.name || fFeed.description || fFeed.story || ''
                            //    })
                            //}

                            if(latestFeed.length > 0){
                                latestFeed.sort(function (o1, o2) {
                                    return new Date(o1.date) > new Date(o2.date) ? -1 : new Date(o1.date) < new Date(o2.date) ? 1 : 0;
                                });

                                res.send({
                                    "SuccessCode": 1,
                                    "Message": '',
                                    "ErrorCode": 0,
                                    "Data":latestFeed[0]
                                });
                            }
                            else res.send({
                                "SuccessCode": 0,
                                "Message": errorMessagesObj.getMessage("SOCIAL_FEED_NOT_AVAILABLE"),
                                "ErrorCode": 1,
                                "Data":{setup_social_accounts:false}
                            });
                        //})
                    })
                })
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("SOCIAL_FEED_NOT_AVAILABLE")));
        })
   }
   else res.send(errorObj.generateErrorResponse({
       status: statusCodes.INVALID_REQUEST_CODE,
       key: 'USER_ID_NOT_FOUND_IN_REQUEST'
   },errorMessagesObj.getMessage("SOCIAL_FEED_NOT_AVAILABLE")));
});

/* 3_0_Today+Sample */
router.get('/today/do/counts',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var profile = req.session.profile;
    var timezone;
    if(common.checkRequired(profile)){
        getTodayActionCounts(userId,profile,req,res)
    }
    else{
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{firstName:1,lastName:1,emailId:1,companyName:1,designation:1,location:1,currentLocation:1,timezone:1,profilePicUrl:1},function(error,user){
            if(common.checkRequired(user)){
                getTodayActionCounts(userId,user,req,res)
            }
            else{
                logger.info('Error in /today/top/section/info user: '+req.user ,error);
                res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
            }
        })
    }
});

router.get('/upcoming/meetings',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var redisKeyUpcomingMeeting = userId.toString()+"upcomingMeetings"

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(common.checkRequired(user)){
            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';
            var data = {userId:userId,timezone:timezone};
            getTwoDaysMeetings(userId,data,timezone,user,function (meetings) {
                next14DaysNonConfirmedMeetings(userId,data,timezone,user,function (meetings2) {

                    var data = {
                        nextTwoDaysMeetings:meetings,
                        next14DaysNonConfirmedMeetings:meetings2,
                        userId:userId
                    }
                    
                    res.send({
                        "SuccessCode": 1,
                        "Message": '',
                        "ErrorCode": 0,
                        "Data":data
                    });
                });
            });
        }
        else res.send(errorObj.generateErrorResponse({
            status: statusCodes.PROFILE_ERROR_CODE,
            key: 'USER_PROFILE_NOT_FOUND'
        }));
    });

    // redisClient.get(redisKeyUpcomingMeeting,function (error,cacheResult) {
    //     if(!error && cacheResult){
    //         res.send({
    //             "SuccessCode": 1,
    //             "Message": '',
    //             "ErrorCode": 0,
    //             "Data":JSON.parse(cacheResult).upcomingMeetings
    //         });
    //
    //     } else {
    //         common.getProfileOrStoreProfileInSession(userId,req,function(user){
    //             if(common.checkRequired(user)){
    //                 var timezone;
    //                 if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
    //                     timezone = user.timezone.name;
    //                 }else timezone = 'UTC';
    //                 var data = {userId:userId,timezone:timezone};
    //                 getTwoDaysMeetings(userId,data,timezone,user,function (meetings) {
    //                     next14DaysNonConfirmedMeetings(userId,data,timezone,user,function (meetings2) {
    //
    //                         var data = {
    //                             nextTwoDaysMeetings:meetings,
    //                             next14DaysNonConfirmedMeetings:meetings2,
    //                             userId:userId
    //                         }
    //
    //                         redisClient.setex(redisKeyUpcomingMeeting, 360, JSON.stringify({upcomingMeetings:data}));
    //                         res.send({
    //                             "SuccessCode": 1,
    //                             "Message": '',
    //                             "ErrorCode": 0,
    //                             "Data":data
    //                         });
    //                     });
    //                 });
    //             }
    //             else res.send(errorObj.generateErrorResponse({
    //                 status: statusCodes.PROFILE_ERROR_CODE,
    //                 key: 'USER_PROFILE_NOT_FOUND'
    //             }));
    //         });
    //     }
    // });

});

function getTwoDaysMeetings(userId,data,timezone,user,callback) {

    var dateMin = moment().add(1, "days");
    var dateMax = moment().add(2, "days");

    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(0)
    dateMax.millisecond(0)

    var projection = {recurrenceId:1,invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1}

    //this is for next two days
    meetingClassObj.userMeetingsByDate(userId,new Date(dateMin),new Date(dateMax),projection,user.serviceLogin,function(meetings){
        if(common.checkRequired(meetings) && meetings.length > 0){
            setCorrectLocationType(meetings);
            callback(meetings)
        }else callback([])
    });
}

function next14DaysNonConfirmedMeetings(userId,data,timezone,user,callback) {

    var dateMin = moment().add(3, "days");
    var dateMax = moment().add(14, "days");

    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    dateMax.hour(0)
    dateMax.minute(0)
    dateMax.second(0)
    dateMax.millisecond(0)

    var projection = {recurrenceId:1,invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1}

    //this for next 14 days - starting two days.
    
    meetingClassObj.allMeetingsByDate(userId,new Date(dateMin),new Date(dateMax),projection,user,function(meetings2){
        if(common.checkRequired(meetings2) && meetings2.length > 0){
            setCorrectLocationType(meetings2);
            callback(meetings2)
        }else callback([])
    });
}

function getTodayActionCounts(userId,user,req,res){
    var timezone;
    var data = {};
    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
        timezone = user.timezone.name;
    }else timezone = 'UTC';
    var dateMin = moment().tz(timezone);
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)
    var dateMax = moment().tz(timezone);

    dateMax.hour(23)
    dateMax.minute(59)
    dateMax.second(59)
    dateMax.millisecond(0)

    getTodayMeetingCount(userId,{},timezone,req,res,req.session.profile.serviceLogin,function(mCount){
        data.actionCount = mCount;
        taskManagementClassObj.getTasksForDateAll(userId,dateMin,dateMax,function(error,tasks){
            if(common.checkRequired(tasks) && tasks.length > 0){
                data.actionCount =  data.actionCount+tasks.length;
            }
            getLoosingTouchCount(userId,user,{},timezone,req,res,function(cCount){
                data.losingTouchCount = cCount;
                getPastSevenDaysInteractionsStatus(userId,user.emailId,timezone,{},req,res,function(non_respond_count){
                    data.nonRespondEmailCount = non_respond_count
                    notificationSupportClassObj.getNotificationsCount(userId, dateMin, timezone,function(nCount){
                        data.notificationCount = nCount;
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":data
                        });
                    });
                })
            })
        });
    })
}

function getParticipatEmailIds(senderEmailId,to,toList){
    var emailIdArr = [];
    if(common.checkRequired(senderEmailId)){
        emailIdArr.push(senderEmailId);
    }
    if(common.checkRequired(to) && common.checkRequired(to.receiverEmailId)){
        emailIdArr.push(to.receiverEmailId);
    }
    if(toList && toList.length > 0){
        for(var i=0; i<toList.length; i++){
            emailIdArr.push(toList[i].receiverEmailId);
        }
    }
    return emailIdArr;
}

function setCorrectLocationType(todayMeetings,loopedOnce) {

    if(loopedOnce){

        _.each(todayMeetings.scheduleTimeSlots, function (slot) {
            if(slot.location){

                if(common.setMeetingLocationType(slot.location)){
                    slot.locationType = common.setMeetingLocationType(slot.location);
                }
            }
        });
    } else {
        _.each(todayMeetings, function (meeting) {
            _.each(meeting.scheduleTimeSlots, function (slot) {

                if(slot.location){

                    if(common.setMeetingLocationType(slot.location)){
                        slot.locationType = common.setMeetingLocationType(slot.location);
                    }
                }
            });
        });
    }

}


module.exports = router;
