
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var DealsAtRiskManagement = require('../../dataAccess/dealsAtRiskManagement');
var oppManagement = require('../../dataAccess/opportunitiesManagement');
var UserKlass = require('../../databaseSchema/userManagementSchema').User;
var profileManagementClass = require('../../dataAccess/profileManagementClass');
var interactions = require('../../dataAccess/interactionManagement');
var accounts = require('../../dataAccess/accountManagement');
var MessageManagement = require('../../dataAccess/messageManagementV2');
var AccessManagement = require('../../accessControl/accountAccess');
var contactClass = require('../../dataAccess/contactsManagementClass');
var RevenueHierarchy = require('../../dataAccess/revenueHierarchy');
var SecondaryHierarchy = require('../../dataAccess/secondaryHierarchyManagement');
var AccessControlOpportunities = require('../../dataAccess/accessControlOpportunitiesManagement');
var MasterData = require('../../dataAccess/masterData')

var ProductHierarchy = require('../../dataAccess/productHierarchy');
var ContactManagement = require('../../dataAccess/contactManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var contactClassSupport = require('../../common/contactsSupport');
var OrgHierarchy = require('../../dataAccess/orgHierarchy');
var OppCommitManagement = require('../../dataAccess/oppCommitAccess');
var OppWeightsClass = require('../../dbCache/company.js')

var contactSupportObj = new contactClassSupport();
var accAccessObj = new AccessManagement();
var profileManagementClassObj = new profileManagementClass();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionObj = new interactions();
var accountsObj = new accounts();
var oppObj = new oppManagement();
var dealsAtRiskObj = new DealsAtRiskManagement();
var messageManagementObj = new MessageManagement();
var _ = require("lodash")
var async = require("async")
var statusCodes = errorMessagesObj.getStatusCodes();
var revenueHierarchyObj = new RevenueHierarchy();
var productHierarchyObj = new ProductHierarchy();
var secondaryHierarchyObj = new SecondaryHierarchy();
var accessControlOpportunitiesObj = new AccessControlOpportunities();
var masterDataObj = new MasterData()

var orgHierarchyObj = new OrgHierarchy();
var contactManagementObj = new ContactManagement();
var oppCommitObj = new OppCommitManagement();
var oppWeightsObj = new OppWeightsClass();

var redis = require('redis');
var redisClient = redis.createClient();

/* Company landing left section company list */
router.get('/company/all/user/interacted',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.session.companiesInteracted)){
        var list = JSON.parse(JSON.stringify(req.session.companiesInteracted));

        for (var i=0; i< list.length; i++){
            delete list[i]['emails'];
        }

        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data": list
        });
    }
    else{
        getCompaniesUserInteracted(userId,req,res,function(companiesInteracted){
            if(companiesInteracted.length > 0){
                req.session.companiesInteracted = JSON.parse(JSON.stringify(companiesInteracted));
                for (var i=0; i< companiesInteracted.length; i++){
                    delete companiesInteracted[i]['emails'];
                }
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": companiesInteracted
                });
            }
            else{
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": []
                });
            }
        })
    }
});

router.get('/company/past/days/interacted/info',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.session.companiesInteracted)){
        var list = JSON.parse(JSON.stringify(req.session.companiesInteracted));
        getPastSevenDaysCompanyInteractions(userId,list,req,res);
    }
    else{
        getCompaniesUserInteracted(userId,req,res,function(companiesInteracted){
            if(companiesInteracted.length > 0){
                req.session.companiesInteracted = JSON.parse(JSON.stringify(companiesInteracted));
                getPastSevenDaysCompanyInteractions(userId,companiesInteracted,req,res);
            }
            else{
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": []
                });
            }
        })
    }
});

function getCompaniesUserInteracted(userId,req,res,callback){
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(common.checkRequired(user)){
            interactionObj.getCompaniesUserInteracted(userId,user.emailId,function(companiesInteracted){
                callback(companiesInteracted)
            })
        }
        else res.send(errorObj.generateErrorResponse({
            status: statusCodes.PROFILE_ERROR_CODE,
            key: 'USER_PROFILE_NOT_FOUND'
        }));
    });
}

function getPastSevenDaysCompanyInteractions(userId,list,req,res){
    var emailIdList = [];
    var companiesCount = 0;
    if(common.checkRequired(req.session.interactionsCountByCompanyLocationType)){
        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data": req.session.interactionsCountByCompanyLocationType
        });
    }
    else{
        common.getProfileOrStoreProfileInSession(userId,req,function(user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';
                var date = moment().tz(timezone);
                var dateMax = date.clone()
                date.date(date.date()-7);

                for(var i=0; i<list.length; i++){

                    if(moment(list[i].lastInteractionDate).tz(timezone).isAfter(date)){
                        companiesCount++;
                        emailIdList = emailIdList.concat(list[i].emails)
                    }
                }

                if(emailIdList.length > 0){
                    interactionObj.getInteractionsCountByCompanyLocationTypeDate(userId,date.format(),dateMax.format(),emailIdList,[],function(resObj){
                        resObj.companiesCount = companiesCount;
                        req.session.interactionsCountByCompanyLocationType = resObj;
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": resObj
                        });
                    })
                }
                else res.send(errorObj.generateErrorResponse({
                    status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                    key: 'INTERACTIONS_NOT_FOUND'
                }));

            }
            else res.send(errorObj.generateErrorResponse({
                status: statusCodes.PROFILE_ERROR_CODE,
                key: 'USER_PROFILE_NOT_FOUND'
            }));
        });
    }
}

/* Company selected left section contacts list */
router.post('/company/selected/contacts/list',common.isLoggedInUserToGoNext,function(req,res){
    var companyName = req.body.companyName;
    var userId = common.getUserId(req.user);
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;
    if(common.checkRequired(companyName)){
        getEmailIdsMatchedCompany(userId,companyName,req,res,function(emails){
            if(emails.length > 0){
                contactObj.getContactsInEmailIdArrUserId(common.castToObjectId(userId),emails,function(err,contacts){
                    var total = contacts.length;
                    var results = contacts.splice(skip, limit);
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": {
                            total: total,
                            skipped: skip,
                            limit: limit,
                            returned: results.length,
                            contacts: results
                        }
                    });
                })
            }
            else res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": {
                    total: 0,
                    skipped: skip,
                    limit: limit,
                    returned: 0,
                    contacts: []
                }
            });
        })
    }
    else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'COMPANY_NAME_NOT_EXISTS'}));

});

/* Past 90 day InteractionsInitiations and RelationshipStrength with GE(company) */
router.post('/company/selected/past/days/interacted/info',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
    var companyName = req.body.companyName;
    if(common.checkRequired(req.session.companiesInteracted)){
        var list = JSON.parse(JSON.stringify(req.session.companiesInteracted));
        var emailIds = [];
        for(var i=0; i<list.length; i++){
            if(list[i].companyName == companyName){
                emailIds = list[i].emails;
                break;
            }
        }
        if(emailIds.length > 0){
            getInteractionInitiations(userId,emailIds,req,res)
        }
        else res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data": {}
        });
    }
    else{
        getCompaniesUserInteracted(userId,req,res,function(companiesInteracted){
            if(companiesInteracted.length > 0){
                req.session.companiesInteracted = JSON.parse(JSON.stringify(companiesInteracted));
                var emailIds = [];
                for(var i=0; i<companiesInteracted.length; i++){
                    if(companiesInteracted[i].companyName == companyName){
                        emailIds = companiesInteracted[i].emails;
                        break;
                    }
                }
                if(emailIds.length > 0){
                    getInteractionInitiations(userId,emailIds,req,res)
                }
                else res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": {}
                });

            }
            else{
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": {}
                });
            }
        })
    }
});

function getInteractionInitiations(userId,emailsArr,req,res){

    common.getProfileOrStoreProfileInSession(userId,req,function(user) {
        if (common.checkRequired(user)) {
            var timezone;
            if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                timezone = user.timezone.name;
            } else timezone = 'UTC';

            var dateMin = moment().tz(timezone);
            dateMin.date(dateMin.date() - 90);
            dateMin = dateMin.format();
            var dateMax = moment().tz(timezone).format();
            var data = {};

            interactionObj.getInteractionInitiationsCount(userId,user.emailId,emailsArr,true,dateMin,dateMax,user.serviceLogin,function(counts){
                if(common.checkRequired(counts) && counts.length > 0){
                    var interactionInitiations = [];
                    var you = false;
                    var others = false;
                    for(var i=0; i<counts.length; i++){
                        if(counts[i]._id == 'sender'){
                            interactionInitiations.push({_id:'others',count:counts[i].count});
                            others = true;
                        }
                        else {
                            you = true;
                            interactionInitiations.push({_id:'you',count:counts[i].count});
                        }
                    }
                    if(!you) interactionInitiations.push({_id:'you',count:0});

                    if(!others) interactionInitiations.push({_id:'others',count:0});

                    data.interactionInitiations = interactionInitiations;
                }
                else data.interactionInitiations = [];

                getInteractionsByTypeCount(userId,emailsArr,data,timezone,req,res);
            })

        }
        else res.send(errorObj.generateErrorResponse({
            status: statusCodes.PROFILE_ERROR_CODE,
            key: 'USER_PROFILE_NOT_FOUND'
        }));
    });
}

function getInteractionsByTypeCount(userId,emails,data,timezone,req,res){
    var dateMin = moment().tz(timezone);
    dateMin.date(dateMin.date() - 90);
    dateMin = dateMin.format();
    var dateMax = moment().tz(timezone)
    interactionObj.interactionWithContactByTypes(userId,emails,true,dateMin,dateMax.format(),function(typeCounts){
        if(common.checkRequired(typeCounts) && typeCounts.length > 0){
            data.interactionTypes = typeCounts.typeCounts
        }
        else data.interactionTypes = [];

        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data":data
        });
    })
}

/* Contacts Counts by relationship type the contacts in GE(company) */
router.post('/company/selected/company/contacts/relation/info',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var companyName = req.body.companyName;
    if(common.checkRequired(req.session.companiesInteracted)){
        var list = JSON.parse(JSON.stringify(req.session.companiesInteracted));
        getContactsRelationshipCountInCompany(companyName,userId,list,function(resObj){
            res.send(resObj);
        });
    }
    else{
        getCompaniesUserInteracted(userId,req,res,function(companiesInteracted){
            if(companiesInteracted.length > 0){
                req.session.companiesInteracted = JSON.parse(JSON.stringify(companiesInteracted));
                getContactsRelationshipCountInCompany(companyName,userId,companiesInteracted,function(resObj){
                    res.send(resObj);
                })
            }
            else{
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": []
                });
            }
        })
    }
});

function getContactsRelationshipCountInCompany(companyName,userId,companies,callback){
    var emailIds = [];
    var result = {};
    for(var i=0; i<companies.length; i++){
        if(companies[i].companyName == companyName){
            emailIds = companies[i].emails;
            break;
        }
    }

    if(common.checkRequired(emailIds) && emailIds.length > 0){
        contactObj.getContactsByTypeCount(common.castToObjectId(userId),emailIds,[],function(error,data){
            contactObj.getContactsFavoriteCount(common.castToObjectId(userId),emailIds,[],function(error,count){
                if(data.length > 0){
                    data.push({_id:'favorite',count:count});
                    callback({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": data
                    });
                }
                else{
                    callback({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": [{_id:'favorite',count:count}]
                    });
                }
            })
        })
    }
    else callback({
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data": []
    });
}

/* Connections from your company who have interacted with GE(company) */
router.post('/company/interacted/with/selected/company',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
    var companyName = req.body.companyName;
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

    if(common.checkRequired(req.session.companiesInteracted)){
        var list = JSON.parse(JSON.stringify(req.session.companiesInteracted));
        var emailIds = [];
        for(var i=0; i<list.length; i++){
            if(list[i].companyName == companyName){
                emailIds = list[i].emails;
                break;
            }
        }
        if(emailIds.length > 0){
            getUserContactsInteractedContactsInCompany(userId,emailIds,skip,limit,req,res)
        }
        else res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data": []
        });
    }
    else{
        getCompaniesUserInteracted(userId,req,res,function(companiesInteracted){
            if(companiesInteracted.length > 0){
                req.session.companiesInteracted = JSON.parse(JSON.stringify(companiesInteracted));
                var emailIds = [];
                for(var i=0; i<companiesInteracted.length; i++){
                    if(companiesInteracted[i].companyName == companyName){
                        emailIds = companiesInteracted[i].emails;
                        break;
                    }
                }
                if(emailIds.length > 0){
                    getUserContactsInteractedContactsInCompany(userId,emailIds,skip,limit,req,res)
                }
                else res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": []
                });

            }
            else{
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": []
                });
            }
        })
    }
});

function getUserContactsInteractedContactsInCompany(userId,emails,skip,limit,req,res){

        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,companyName:1},function(error,user){
            if(common.checkRequired(user)){
                if(common.checkRequired(user.companyName)){
                    contactSupportObj.getContactsWhoAllInCompanyInteractedWithArrayOfEmailsContact(userId,user.emailId,emails,user.companyName,skip,limit,function(error,results){
                        if(error){
                            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'}))
                        }
                        else{
                            res.send(results)
                        }
                    })
                }
                else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'COMPANY_NAME_NOT_EXISTS'}));
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });
}

function getEmailIdsMatchedCompany(userId,companyName,req,res,callback){
    if(common.checkRequired(req.session.companiesInteracted)){
        var list = JSON.parse(JSON.stringify(req.session.companiesInteracted));
        var emailIds = [];
        for(var i=0; i<list.length; i++){
            if(list[i].companyName == companyName){
                emailIds = list[i].emails;
                break;
            }
        }
        if(emailIds.length > 0){
            callback(emailIds)
        }
        else callback([])
    }
    else{
        getCompaniesUserInteracted(userId,req,res,function(companiesInteracted){
            if(companiesInteracted.length > 0){
                req.session.companiesInteracted = JSON.parse(JSON.stringify(companiesInteracted));
                var emailIds = [];
                for(var i=0; i<companiesInteracted.length; i++){
                    if(companiesInteracted[i].companyName == companyName){
                        emailIds = companiesInteracted[i].emails;
                        break;
                    }
                }
                if(emailIds.length > 0){
                    callback(emailIds)
                }
                else callback([])
            }
            else callback([])
        })
    }
}

router.post('/company/interactions/timeline/interactions',common.isLoggedInUserToGoNext,function(req,res){
    var companyName = req.body.companyName;
    var userId = common.getUserId(req.user);
    if(common.checkRequired(companyName)){

        common.getProfileOrStoreProfileInSession(userId,req,function(user){
            if (common.checkRequired(user)) {
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var dateMin = moment().tz(timezone);
                dateMin.date(dateMin.date() - 90);
                dateMin = dateMin.format();
                var dateMax = moment().tz(timezone);
                dateMax = dateMax.format();

                var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

                getEmailIdsMatchedCompany(userId,companyName,req,res,function(emails){
                    if(emails.length > 0){
                        interactionObj.getInteractionsByDateEmailArray(userId,emails,dateMin,dateMax,function(interactions){
                            if(interactions.length > 0){
                                var total = interactions.length;
                                var results = interactions.splice(skip, limit);

                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data": {
                                        total: total,
                                        skipped: skip,
                                        limit: limit,
                                        returned: results.length,
                                        interactions: results
                                    }
                                });
                            }
                            else{
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data": {
                                        total: 0,
                                        skipped: skip,
                                        limit: limit,
                                        returned: 0,
                                        interactions: []
                                    }
                                });
                            }
                        });
                    }
                    else{
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {
                                total: 0,
                                skipped: skip,
                                limit: limit,
                                returned: 0,
                                interactions: []
                            }
                        });
                    }
                })
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });
    }
    else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'COMPANY_NAME_NOT_EXISTS'}));
});

router.post('/company/interactions/files/interactions',common.isLoggedInUserToGoNext,function(req,res){
    var companyName = req.body.companyName;
    var userId = common.getUserId(req.user);
    if(common.checkRequired(companyName)){

        common.getProfileOrStoreProfileInSession(userId,req,function(user){
            if (common.checkRequired(user)) {
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var dateMin = moment().tz(timezone);
                dateMin.date(dateMin.date() - 90);
                dateMin = dateMin.format();
                var dateMax = moment().tz(timezone);
                dateMax = dateMax.format();

                var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
                var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

                getEmailIdsMatchedCompany(userId,companyName,req,res,function(emails){

                    if(emails.length > 0){
                        interactionObj.documentsSharedWithContactEmailArr(userId,emails,function(shareDocInteractions){
                            if(shareDocInteractions.length > 0){
                                var total = shareDocInteractions.length;
                                var results = shareDocInteractions.splice(skip, limit);

                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data": {
                                        total: total,
                                        skipped: skip,
                                        limit: limit,
                                        returned: results.length,
                                        contacts: results
                                    }
                                });
                            }
                            else{
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data": {
                                        total: 0,
                                        skipped: skip,
                                        limit: limit,
                                        returned: 0,
                                        contacts: []
                                    }
                                });
                            }
                        })
                    }
                    else{
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {
                                total: 0,
                                skipped: skip,
                                limit: limit,
                                returned: 0,
                                contacts: []
                            }
                        });
                    }
                })
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });
    }
    else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'COMPANY_NAME_NOT_EXISTS'}));
});

/* New company or customer landing API's */
router.get('/customer/landing',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    var userId = common.getUserId(req.user)
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render('customer/landing',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/accounts/all/v2',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    var userId = common.getUserId(req.user)
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(user.corporateUser){
            res.render('accounts/landing',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('accounts/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/accounts/all',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    var userId = common.getUserId(req.user)
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(user.corporateUser){
            var userProfile = common.getUserFromSession(req);
            contactManagementObj.updateAccounts(common.castToObjectId(userId));

            res.render('accounts/landing_v2',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('accounts/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/accounts/settings',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('accounts/modal',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/accounts/access/settings',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('accounts/accessSettings',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/accounts/interactions/sidebar',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('accounts/interactions_sidebar.html',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/accounts/settings/sidebar',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('accounts/sidebar',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/access/forbidden',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('accounts/access_forbidden',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/account/interactions/template',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('accounts/interactions',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/account/view/opportunity',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('accounts/oppEdit',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/account/add/note',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('accounts/addNote',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/account/interaction/initiations',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('accounts/interactionsInitiations',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/customer/select',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    var userId = common.getUserId(req.user)

   //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(user.corporateUser){
            res.render('customer/select',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/customer/middle/bar',function(req,res){
    res.render('accounts/leftContactsList',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/company/select/interactions/percentage',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user)
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
    var timezone;
    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
        timezone = user.timezone.name;
    }else timezone = 'UTC';
      var currentDate = moment().tz(timezone).toDate()
      startCurrentDuration = moment().tz(timezone).day(moment().tz(timezone).day() - 30).toDate()
      endPastDuration = moment().tz(timezone).day(moment().tz(timezone).day() - 31).toDate()
      startPastDuration = moment().tz(timezone).day(moment().tz(timezone).day() - 60).toDate()
      var currentDuration = {
        startDate: startCurrentDuration, 
        endDate: currentDate
      }
      var pastDuration = {
        startDate: startPastDuration,
        endDate: endPastDuration
      }
        userId = common.castToObjectId(userId)
        var companyName = req.query.company
        interactionObj.getInteractionPercentage([userId], userId, companyName,  pastDuration,currentDuration, function(data){
        var resObj = {
         "SuccessCode": 1,
         "Message": "",
         "ErrorCode": 0,
         "Data":data 
        }; 
        res.json(resObj)
        })
      })
})

router.get('/company/user/hierarchy', common.isLoggedInUserOrMobile, function(req, res){
    var userId = common.getUserIdFromMobileOrWeb(req)
    var userProfile = common.getUserFromSession(req);
    var timezone = "Asia/Kolkata"
    if(userProfile && userProfile.timezone && userProfile.timezone.name){
        timezone = userProfile.timezone.name;
    }

    userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, data){
        revenueHierarchyObj.getUserHierarchy(common.castToObjectId(userId), function(err_r, r_data){
            userManagementObj.getTeamMembers(common.castToObjectId(data[0].companyId),function (err,team) {

                var listOfMembers = [],
                    teamObj = {},
                    orgObj = {};

                _.each(team,function (el) {
                    teamObj[el.emailId] = el
                });

                _.each(data,function (el) {
                    orgObj[el.emailId] = el
                });

                _.each(team,function (el) {
                    _.each(team,function (hi) {
                        if(_.includes(hi.hierarchyPath,el._id)){
                            listOfMembers.push({
                                userId:el._id,
                                userEmailId:el.emailId,
                                teammate:hi
                            })
                        }
                    })
                });

                listOfMembers = _
                    .chain(listOfMembers)
                    .groupBy("userEmailId")
                    .map(function(values, key) {

                        var teamMatesEmailId = [],
                            teamMatesUserId = [];

                        _.each(values,function (va) {
                            teamMatesEmailId.push(va.teammate.emailId)
                            teamMatesUserId.push(va.teammate._id)
                        });

                        return {
                            userEmailId:key,
                            teamMatesEmailId:teamMatesEmailId,
                            teamMatesUserId:teamMatesUserId
                        }
                    })
                    .value();

                if(!err){

                    if(req.query.forCommits){
                        getTargetsAndAchievementsForCommit(data,common.castToObjectId(data[0].companyId),timezone,req.query.range,function (err,targetsAchievementCommits) {
                            var resObj = {
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                listOfMembers:listOfMembers,
                                "sessionHierarchy":req.session.hierarchy,
                                "Data": targetsAchievementCommits,
                                companyMembers:team
                            };

                            res.send(resObj)
                        });

                    } else {

                        var resObj = {
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            listOfMembers:listOfMembers,
                            "sessionHierarchy":req.session.hierarchy,
                            "Data": data,
                            companyMembers:team
                        };

                        res.send(resObj)
                    }
                } else {
                    res.send([])
                }
            })
        })
    })
});

router.get('/company/user/hierarchy/insights', common.isLoggedInUserOrMobile, function(req, res){
    var userId = common.getUserIdFromMobileOrWeb(req)
    var userProfile = common.getUserFromSession(req);
    var timezone = "Asia/Kolkata"
    if(userProfile && userProfile.timezone && userProfile.timezone.name){
        timezone = userProfile.timezone.name;
    }

    userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, data){
        secondaryHierarchyObj.getUserHierarchy(common.castToObjectId(userId), req.query.hierarchyType, function(err_r, r_data){
            userManagementObj.getTeamMembers(common.castToObjectId(data[0].companyId),function (err,team) {

                var listOfMembers = [],
                    teamObj = {},
                    orgObj = {};

                _.each(team,function (el) {
                    teamObj[el.emailId] = el
                });

                _.each(data,function (el) {
                    orgObj[el.emailId] = el
                });

                if(!err_r && r_data && r_data.length>0){
                    _.each(r_data,function (el) {
                        if(!orgObj[el.ownerEmailId]){
                            if(teamObj[el.ownerEmailId]){
                                data.push({
                                    _id: common.castToObjectId(el.userId),
                                    emailId:el.ownerEmailId,
                                    hierarchyParent:el.hierarchyParent,
                                    hierarchyPath:el.hierarchyPath,
                                    verticalOwner:teamObj[el.ownerEmailId].verticalOwner,
                                    productTypeOwner:teamObj[el.ownerEmailId].productTypeOwner,
                                    regionOwner:teamObj[el.ownerEmailId].regionOwner,
                                    firstName:teamObj[el.ownerEmailId].firstName,
                                    lastName:teamObj[el.ownerEmailId].lastName,
                                    designation:teamObj[el.ownerEmailId].designation,
                                    fromRevenueHierarchy:true
                                })
                            }
                        }
                    })
                }

                _.each(team,function (el) {
                    _.each(team,function (hi) {
                        if(_.includes(hi.hierarchyPath,el._id)){
                            listOfMembers.push({
                                userId:el._id,
                                userEmailId:el.emailId,
                                teammate:hi
                            })
                        }
                    })
                });

                listOfMembers = _
                    .chain(listOfMembers)
                    .groupBy("userEmailId")
                    .map(function(values, key) {

                        var teamMatesEmailId = [],
                            teamMatesUserId = [];

                        _.each(values,function (va) {
                            teamMatesEmailId.push(va.teammate.emailId)
                            teamMatesUserId.push(va.teammate._id)
                        });

                        return {
                            userEmailId:key,
                            teamMatesEmailId:teamMatesEmailId,
                            teamMatesUserId:teamMatesUserId
                        }
                    })
                    .value();

                if(!err){

                    if(req.query.forCommits){
                        getTargetsAndAchievements(data,common.castToObjectId(data[0].companyId),timezone,function (err,targetsAchievementCommits) {
                            var resObj = {
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                listOfMembers:listOfMembers,
                                "sessionHierarchy":req.session.hierarchy,
                                "Data": targetsAchievementCommits,
                                companyMembers:team
                            };

                            res.send(resObj)
                        });

                    } else {

                        var resObj = {
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            listOfMembers:listOfMembers,
                            "sessionHierarchy":req.session.hierarchy,
                            "Data": data,
                            companyMembers:team
                        };

                        res.send(resObj)
                    }
                } else {
                    res.send([])
                }
            })
        })
    })
});

function getTargetsAndAchievements(team,companyId,timezone,callback,commitFor) {

    var userIds = _.pluck(team,"_id");
    var userId = String(userIds[0]);

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters,timezone) {
        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if (oppStages) {
                _.each(oppStages, function (st) {
                    if (st.commitStage) {
                        commitStage = st.name;
                    }
                });
            }

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            var monthCommitCutOff = 10

            if(companyDetails && companyDetails.monthCommitCutOff){
                monthCommitCutOff = companyDetails.monthCommitCutOff
            }

            var startOfMonth = moment().startOf('month');
            var endOfMonth = moment(startOfMonth).add(1,'month'),
                monthYear = moment(startOfMonth).month()+""+moment(startOfMonth).year();

            var startOfQuarter = allQuarters[allQuarters.currentQuarter].start,
                endOfQuarter = allQuarters[allQuarters.currentQuarter].end;

            var start = startOfMonth;
            var end = endOfMonth;

            if(commitFor == "quarter"){
                start = startOfQuarter;
                end = endOfQuarter;
            } else if(commitFor == "week"){
                start = fyRange.start
                end = fyRange.end
            }

            common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
                async.parallel([
                    function (callback) {
                        getOppsStatusDataByDateRange(common.castListToObjectIds(userIds),start,end,commitStage,netGrossMarginReq,primaryCurrency,currenciesObj,fyRange.start,fyRange.end,callback)
                    },
                    function (callback) {
                        if(commitFor == "quarter"){
                            startOfQuarter = moment(startOfQuarter).tz(timezone).add(15,"day")
                            var quarterYear = common.getQuarterYear(allQuarters,startOfQuarter,fyMonth,timezone,fyRange.start);
                            oppCommitObj.getCommitsByQuarterRangeMultiUsers(userIds,quarterYear,callback)
                        } else if(commitFor == "week"){
                            var currentWeekYear = moment().week()+""+moment().year()
                            oppCommitObj.getCommitsByWeekRange(common.castToObjectId(userId),currentWeekYear,callback)
                        } else {
                            oppCommitObj.getCommitsByMonthRangeMultiUsers(common.castListToObjectIds(userIds),monthYear,callback)
                        }

                    }
                ], function (errors,data) {
                    if(!errors && data){
                        callback(null,attachTargetsAndAchievementsToTeam(team,data[0],data[1]))
                    } else {
                        callback(errors,null)
                    }

                });
            });
        });
    });
}

function getTargetsAndAchievementsForCommit(team,companyId,timezone,commitFor,callback) {

    var userIds = _.pluck(team,"_id");
    var userId = String(userIds[0]);

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters,timezone) {
        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var commitStage = "Proposal"; // default.
            if (oppStages) {
                _.each(oppStages, function (st) {
                    if (st.commitStage) {
                        commitStage = st.name;
                    }
                });
            }

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            var monthCommitCutOff = 10

            if(companyDetails && companyDetails.monthCommitCutOff){
                monthCommitCutOff = companyDetails.monthCommitCutOff
            }

            var startOfMonth = moment().startOf('month');
            var endOfMonth = moment(startOfMonth).add(1,'month'),
                monthYear = moment(startOfMonth).month()+""+moment(startOfMonth).year();

            var startOfQuarter = allQuarters[allQuarters.currentQuarter].start,
                endOfQuarter = allQuarters[allQuarters.currentQuarter].end;

            var start = startOfMonth;
            var end = endOfMonth;

            if(commitFor == "quarter"){
                start = startOfQuarter;
                end = endOfQuarter;
            } else if(commitFor == "week"){
                start = moment().startOf('isoweek')
                end = moment().endOf('isoweek')
            }

            common.checkNGMRequirement(common.castToObjectId(companyId),function (netGrossMarginReq) {
                async.parallel([
                    function (callback) {
                        getOppsStatusDataByDateRangeForCommit(common.castListToObjectIds(userIds),start,end,commitStage,netGrossMarginReq,primaryCurrency,currenciesObj,fyRange.start,fyRange.end,commitFor,callback)
                    },
                    function (callback) {
                        if(commitFor == "quarter"){
                            startOfQuarter = moment(startOfQuarter).tz(timezone).add(15,"day")
                            var quarterYear = common.getQuarterYear(allQuarters,startOfQuarter,fyMonth,timezone,fyRange.start);
                            oppCommitObj.getCommitsByQuarterRangeMultiUsers(userIds,quarterYear,callback)
                        } else if(commitFor == "week"){
                            var currentWeekYear = moment().week()+""+moment().year()
                            oppCommitObj.getCommitsByWeekRangeMultiUsers(common.castListToObjectIds(userIds),currentWeekYear,callback)
                        } else {
                            oppCommitObj.getCommitsByMonthRangeMultiUsers(common.castListToObjectIds(userIds),monthYear,callback)
                        }

                    }
                ], function (errors,data) {

                    if(!errors && data){
                        callback(null,attachTargetsAndAchievementsToTeam(team,data[0],data[1]))
                    } else {
                        callback(errors,null)
                    }

                });
            });
        });
    });
}

function attachTargetsAndAchievementsToTeam(team,targetsAndAchievements,commits) {

    var tAndAObj = {},
        commitsObj = {};
    if(targetsAndAchievements && targetsAndAchievements.length>0){
        _.each(targetsAndAchievements,function (el) {
            tAndAObj[el.userId] = el;
        })
    }
    if(commits && commits.rawData && commits.rawData.length>0){
        _.each(commits.rawData,function (el) {
            commitsObj[el.userId] = el;
        })
    }

    _.each(team,function (el) {

        if(tAndAObj[el._id]){
            el.targetAndAchievement = tAndAObj[el._id]
        }

        if(commitsObj[el._id]){
            el.commit = commitsObj[el._id]
        } else {
            el.commit = {
                month:{
                    userCommitAmount:0
                }
            }
        }
    });

    return team;
}

function getOppsStatusDataByDateRange(hierarchyList,dateMin,dateMax,commitStage,netGrossMarginReq,primaryCurrency,currenciesObj,fyStart,fyEnd,callback){

    oppObj.getTargetForUserByMonthRange(hierarchyList,moment(dateMin).startOf("month"),moment(dateMax).startOf("month"),null,null,null,fyStart,fyEnd,function (err,targets) {

        var targetsDictionary = {};
        _.each(targets,function (tr) {
            targetsDictionary[tr._id] = tr.target?tr.target:0
        });

        oppObj.opportunitiesForUsersByDate(hierarchyList,dateMin,dateMax,null,null,null,null,null,null,function (err,opps) {

            var allUsersValues = [],
                fillAndExtendData = [];

            if(!err && opps.length>0){
                _.each(opps,function (op) {
                    var pipeline = 0,won = 0,lost = 0,commitPipeline = 0,userId = null;
                    _.each(op.opportunities,function (el) {
                        userId = el.userId;
                        el.amountWithNgm = el.amount;

                        if(el.netGrossMargin){
                            el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                        }

                        el.convertedAmt = el.amount;
                        el.convertedAmtWithNgm = el.amountWithNgm

                        if(el.currency && el.currency !== primaryCurrency){

                            if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                                el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                            }

                            if(el.netGrossMargin){
                                el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                            } else {
                                el.convertedAmtWithNgm = el.convertedAmt;
                            }

                            el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))
                        }

                        if(el.stageName === commitStage){
                            commitPipeline = commitPipeline+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        }

                        if(_.includes(el.stageName,"Close Won") || _.includes(el.stageName,"Closed Won")){
                            won = won+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        } else if(_.includes(el.stageName,"Close Lost") || _.includes(el.stageName,"Closed Lost")){
                            lost = lost+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        } else {
                            pipeline = pipeline+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        }
                    });

                    var prob = 100;
                    if(lost>0){
                        prob = 0
                    }

                    allUsersValues.push({
                        userEmailId:op._id,
                        userId:userId,
                        pipeline:pipeline,
                        lost:lost,
                        won:won,
                        commitPipeline:commitPipeline,
                        probability:prob,
                        target:targetsDictionary[userId]
                    });
                });

                var allUsersValuesObj = {};
                _.each(allUsersValues,function (el) {
                    allUsersValuesObj[el.userId] = el;
                });

                _.each(hierarchyList,function (el) {
                    if(allUsersValuesObj[el]){
                        fillAndExtendData.push(allUsersValuesObj[el])
                    } else {

                        fillAndExtendData.push({
                            userEmailId:null,
                            userId:el,
                            pipeline:0,
                            lost:0,
                            won:0,
                            commitPipeline:0,
                            probability:0,
                            target:targetsDictionary[el]
                        })
                    }
                })

            } else {

                _.each(hierarchyList,function (hi) {
                    fillAndExtendData.push({
                        userEmailId:null,
                        userId:hi,
                        pipeline:0,
                        lost:0,
                        won:0,
                        commitPipeline:0,
                        probability:0,
                        target:targetsDictionary[hi]
                    })
                })
            }

            callback(err,fillAndExtendData)
        })
    })
}

function getOppsStatusDataByDateRangeForCommit(hierarchyList,dateMin,dateMax,commitStage,netGrossMarginReq,primaryCurrency,currenciesObj,fyStart,fyEnd,commitFor,callback){

    oppObj.getTargetForUserByMonthRange(hierarchyList,dateMin,dateMax,null,null,null,fyStart,fyEnd,function (err,targets) {

        var targetsDictionary = {};
        _.each(targets,function (tr) {
            targetsDictionary[tr._id] = tr.target?tr.target:0
        });

        oppObj.opportunitiesForUsersByDate(hierarchyList,dateMin,dateMax,null,null,null,null,null,null,function (err,opps) {

            var allUsersValues = [],
                fillAndExtendData = [];

            if(!err && opps.length>0){
                _.each(opps,function (op) {
                    var pipeline = 0,won = 0,lost = 0,commitPipeline = 0,userId = null;
                    _.each(op.opportunities,function (el) {
                        userId = el.userId;
                        el.amountWithNgm = el.amount;

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                        }

                        el.convertedAmt = el.amount;
                        el.convertedAmtWithNgm = el.amountWithNgm


                        if(el.currency && el.currency !== primaryCurrency){

                            if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                                el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                            }

                            if(el.netGrossMargin){
                                el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                            } else {
                                el.convertedAmtWithNgm = el.convertedAmt;
                            }

                            el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))
                        }

                        if(el.stageName === commitStage){
                            commitPipeline = commitPipeline+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        }

                        if(_.includes(el.stageName,"Close Won") || _.includes(el.stageName,"Closed Won")){
                            won = won+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        } else if(_.includes(el.stageName,"Close Lost") || _.includes(el.stageName,"Closed Lost")){
                            lost = lost+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        } else {
                            pipeline = pipeline+parseFloat(el.convertedAmtWithNgm.toFixed(2))
                        }
                    });

                    var prob = 100;
                    if(lost>0){
                        prob = 0
                    }

                    allUsersValues.push({
                        userEmailId:op._id,
                        userId:userId,
                        pipeline:pipeline,
                        lost:lost,
                        won:won,
                        commitPipeline:commitPipeline,
                        probability:prob,
                        target:commitFor == "week"?0:targetsDictionary[userId]
                    });
                });

                var allUsersValuesObj = {};
                _.each(allUsersValues,function (el) {
                    allUsersValuesObj[el.userId] = el;
                });

                _.each(hierarchyList,function (el) {
                    if(allUsersValuesObj[el]){
                        fillAndExtendData.push(allUsersValuesObj[el])
                    } else {

                        fillAndExtendData.push({
                            userEmailId:null,
                            userId:el,
                            pipeline:0,
                            lost:0,
                            won:0,
                            commitPipeline:0,
                            probability:0,
                            target:commitFor == "week"?0:targetsDictionary[el]
                        })
                    }
                })

            } else {

                _.each(hierarchyList,function (hi) {
                    fillAndExtendData.push({
                        userEmailId:null,
                        userId:hi,
                        pipeline:0,
                        lost:0,
                        won:0,
                        commitPipeline:0,
                        probability:0,
                        target:commitFor == "week"?0:targetsDictionary[hi]
                    })
                })
            }

            callback(err,fillAndExtendData)
        })
    })
}

router.get('/company/getTeamMembers', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user)
    var companyName = req.query.company
        common.getProfileOrStoreProfileInSession(userId,req,function(user){
            userManagementObj.getTeamMembers(user.companyId, function(err, data){
                if(!err){
                    var resObj = {
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "sessionHierarchy":req.session.hierarchy,
                        "Data": data
                    };
                    res.send(resObj)
                } else {
                    res.send([])
                }
            })
        })
})

router.get('/company/revenue/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user)
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        revenueHierarchyObj.getHierarchy(user.companyId,function (rErr,revenueHierarchyData) {
            if(!rErr){
                var findQ = {
                    _id:common.castToObjectId(user.companyId)
                }

                var updateQ = {
                    "isRevenueHierarchySetup":true
                }

                if(!revenueHierarchyData || (revenueHierarchyData && revenueHierarchyData.length == 0)){
                    updateQ = {
                        "isRevenueHierarchySetup":false
                    }
                }

                revenueHierarchyObj.customCompanyUpdate(findQ,{$set:updateQ},function (err,result) {

                });

                var key = "userProfile"+String(userId);
                redisClient.del(key);

                userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

                    var listOfMembers = [];

                    _.each(team,function (tm) {
                        _.each(revenueHierarchyData,function (el) {
                            if(el.ownerEmailId == tm.emailId){

                                el._id = tm._id;
                                el.emailId = tm.emailId;
                                el.firstName = tm.firstName;
                                el.lastName = tm.lastName;
                                el.designation = tm.designation;
                            }
                        })
                    })

                    _.each(revenueHierarchyData,function (el) {

                        _.each(revenueHierarchyData,function (hi) {
                            if(_.includes(hi.hierarchyPath,el._id)){
                                listOfMembers.push({
                                    userId:el._id,
                                    userEmailId:el.emailId,
                                    teammate:hi
                                })
                            }
                        })
                    });

                    var data = getChildren(listOfMembers)
                    var tmChildrenObj = data.obj;
                    _.each(revenueHierarchyData,function (el) {
                        el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                    })

                    var resObj = {
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": revenueHierarchyData,
                        "companyMembers":team,
                        "listOfMembers":data.array
                    };
                    res.send(resObj)
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers":[],
                    "listOfMembers":[]
                })
            }
        })
    });
});

router.get('/company/org/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user)
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

            if(!err && team){
                var orgHierarchyData = JSON.parse(JSON.stringify(team));

                orgHierarchyData = orgHierarchyData.filter(function (el) {
                    if(!el.orgHead && el.hierarchyPath && el.hierarchyParent){
                        return el;
                    } else if(el.orgHead){
                        return el;
                    }
                })

                var listOfMembers = [];

                _.each(orgHierarchyData,function (el) {
                    _.each(orgHierarchyData,function (hi) {
                        if(_.includes(hi.hierarchyPath,el._id)){
                            listOfMembers.push({
                                userId:el._id,
                                userEmailId:el.emailId,
                                teammate:hi
                            })
                        }
                    })
                });

                var data = getChildren(listOfMembers)
                var tmChildrenObj = data.obj;
                _.each(orgHierarchyData,function (el) {
                    el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                })

                var resObj = {
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": orgHierarchyData,
                    "companyMembers":team,
                    "listOfMembers":data.array
                };
                res.send(resObj)
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers":[],
                    "listOfMembers":[]
                })
            }
        })
    });
});

router.get('/company/user/revenue/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user)
    var userProfile = common.getUserFromSession(req);
    var timezone = "Asia/Kolkata"
    if(userProfile && userProfile.timezone && userProfile.timezone.name){
        timezone = userProfile.timezone.name;
    }

    if(req.query.userId){
        userId = req.query.userId;
    }

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        revenueHierarchyObj.getUserHierarchy(common.castToObjectId(userId),function (rErr,revenueHierarchyData) {

            if(!rErr){
                userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

                    var listOfMembers = [];
                    if(!revenueHierarchyData){
                        revenueHierarchyData = [user]
                    }

                    _.each(team,function (tm) {
                        _.each(revenueHierarchyData,function (el) {
                            if(el.ownerEmailId == tm.emailId){

                                el._id = tm._id;
                                el.emailId = tm.emailId;
                                el.firstName = tm.firstName;
                                el.lastName = tm.lastName;
                                el.designation = tm.designation;
                            }
                        })
                    })

                    _.each(revenueHierarchyData,function (el) {
                        _.each(revenueHierarchyData,function (hi) {
                            if(_.includes(hi.hierarchyPath,el._id)){
                                listOfMembers.push({
                                    userId:el._id,
                                    userEmailId:el.emailId,
                                    teammate:hi
                                })
                            }
                        })
                    });

                    var data = getChildren(listOfMembers)
                    var tmChildrenObj = data.obj;
                    _.each(revenueHierarchyData,function (el) {
                        el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                    })

                    if(req.query.forCommits){
                        getTargetsAndAchievements(revenueHierarchyData,common.castToObjectId(user.companyId),timezone,function (err,targetsAchievementCommits) {
                            var resObj = {
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                listOfMembers:data.array,
                                "sessionHierarchy":req.session.hierarchy,
                                "Data": targetsAchievementCommits,
                                companyMembers:team
                            };

                            res.send(resObj)
                        });

                    } else {

                        var resObj = {
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            listOfMembers:data.array,
                            "Data": revenueHierarchyData,
                            companyMembers:team
                        };

                        res.send(resObj)
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers":[],
                    "listOfMembers":[]
                })
            }
        })
    });
});

router.get('/company/user/all/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user)
    var userProfile = common.getUserFromSession(req);
    var timezone = "Asia/Kolkata"
    if(userProfile && userProfile.timezone && userProfile.timezone.name){
        timezone = userProfile.timezone.name;
    }

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        revenueHierarchyObj.getUserHierarchy(common.castToObjectId(userId),function (rErr,revenueHierarchyData) {
            userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, orgHierarchyData){

                if(!revenueHierarchyData){
                    revenueHierarchyData = [user]
                }

                if(!rErr){

                    var revenueHierarchyDataObj = {},
                        orgHierarchyDataObj = {},
                    allHierarchyDataObj = {};
                    if(revenueHierarchyData.length>0){
                        _.each(revenueHierarchyData,function (el) {
                            el.emailId = el.ownerEmailId
                            revenueHierarchyDataObj[el.ownerEmailId] = el;

                            if(el.hierarchyPath){
                                allHierarchyDataObj[el.ownerEmailId] = el.hierarchyPath;
                            }
                        })
                    }

                    if(orgHierarchyData.length>0){
                        _.each(orgHierarchyData,function (el) {
                            orgHierarchyDataObj[el.emailId] = el;
                            if(allHierarchyDataObj[el.emailId]){
                                allHierarchyDataObj[el.emailId] = allHierarchyDataObj[el.emailId]+el.hierarchyPath
                            } else {
                                allHierarchyDataObj[el.emailId] = el.hierarchyPath;
                            }
                        })
                    }

                    _.each(revenueHierarchyData,function (el) {
                        if(orgHierarchyDataObj[el.emailId] && orgHierarchyDataObj[el.emailId].hierarchyPath){
                            el.hierarchyPath = el.hierarchyPath+orgHierarchyDataObj[el.emailId].hierarchyPath
                        }
                    })

                    _.each(orgHierarchyData,function (el) {
                        if(revenueHierarchyDataObj[el.emailId] && revenueHierarchyDataObj[el.emailId].hierarchyPath){
                            el.hierarchyPath = el.hierarchyPath+revenueHierarchyDataObj[el.emailId].hierarchyPath
                        }
                    })

                    var allHierarchyData = [];

                    if(orgHierarchyData && orgHierarchyData.length>0 && revenueHierarchyData && revenueHierarchyData.length>0){
                        allHierarchyData = orgHierarchyData.concat(revenueHierarchyData)
                    } else if(orgHierarchyData && orgHierarchyData.length>0){
                        allHierarchyData = orgHierarchyData;
                    } else if(revenueHierarchyData && revenueHierarchyData.length>0){
                        allHierarchyData = revenueHierarchyData;
                    }

                    allHierarchyData = _.uniq(allHierarchyData,"emailId")

                    userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

                        var listOfMembers = [];

                        _.each(team,function (tm) {
                            _.each(allHierarchyData,function (el) {
                                if(el.emailId == tm.emailId){

                                    el._id = tm._id;
                                    el.emailId = tm.emailId;
                                    el.firstName = tm.firstName;
                                    el.lastName = tm.lastName;
                                    el.designation = tm.designation;
                                }
                            })
                        })

                        _.each(allHierarchyData,function (el) {
                            _.each(allHierarchyData,function (hi) {
                                if(_.includes(hi.hierarchyPath,el._id)){
                                    listOfMembers.push({
                                        userId:el._id,
                                        userEmailId:el.emailId,
                                        teammate:hi
                                    })
                                }
                            })
                        });

                        var data = getChildren(listOfMembers)
                        var tmChildrenObj = data.obj;
                        _.each(allHierarchyData,function (el) {
                            el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                        })

                        if(req.query.forCommits){
                            getTargetsAndAchievements(allHierarchyData,common.castToObjectId(user.companyId),timezone,function (err,targetsAchievementCommits) {
                                var resObj = {
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    listOfMembers:data.array,
                                    "sessionHierarchy":req.session.hierarchy,
                                    "Data": targetsAchievementCommits,
                                    companyMembers:team
                                };

                                res.send(resObj)
                            });

                        } else {

                            var resObj = {
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                listOfMembers:data.array,
                                "Data": allHierarchyData,
                                companyMembers:team
                            };

                            res.send(resObj)
                        }

                        // var resObj = {
                        //     "SuccessCode": 1,
                        //     "Message": "",
                        //     "ErrorCode": 0,
                        //     "Data": allHierarchyData,
                        //     "companyMembers":team,
                        //     "listOfMembers":data.array
                        // };
                        // res.send(resObj)
                    })
                } else {
                    res.send({
                        "SuccessCode": 0,
                        "Message": "",
                        "ErrorCode": 1,
                        "Data": [],
                        "companyMembers":[],
                        "listOfMembers":[]
                    })
                }
            });
        })
    });
});

router.get('/company/user/product/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user)
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        productHierarchyObj.getUserHierarchy(common.castToObjectId(userId),function (rErr,productHierarchyData) {
            if(!rErr){
                userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

                    var listOfMembers = [];

                    _.each(team,function (tm) {
                        _.each(productHierarchyData,function (el) {
                            if(el.ownerEmailId == tm.emailId){

                                el._id = tm._id;
                                el.emailId = tm.emailId;
                                el.firstName = tm.firstName;
                                el.lastName = tm.lastName;
                                el.designation = tm.designation;
                            }
                        })
                    })

                    _.each(productHierarchyData,function (el) {
                        _.each(productHierarchyData,function (hi) {
                            if(_.includes(hi.hierarchyPath,el._id)){
                                listOfMembers.push({
                                    userId:el._id,
                                    userEmailId:el.emailId,
                                    teammate:hi
                                })
                            }
                        })
                    });

                    var data = getChildren(listOfMembers)
                    var tmChildrenObj = data.obj;
                    _.each(productHierarchyData,function (el) {
                        el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                    })

                    var resObj = {
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": productHierarchyData,
                        "companyMembers":team,
                        "listOfMembers":data.array
                    };
                    res.send(resObj)
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers":[],
                    "listOfMembers":[]
                })
            }
        })
    });
});

function getChildren(listOfMembers,hashMapReq) {

    var data = _
        .chain(listOfMembers)
        .groupBy("userEmailId")
        .map(function(values, key) {

            var teamMatesEmailId = [],
                teamMatesUserId = [];

            _.each(values,function (va) {
                teamMatesEmailId.push(va.teammate.emailId)
                teamMatesUserId.push(va.teammate._id)
            })

            return {
                userEmailId:key,
                teamMatesEmailId:_.uniq(teamMatesEmailId),
                teamMatesUserId:_.uniq(teamMatesUserId)
            }
        })
        .value();

    var listOfMembersObj = {}
    _.each(data,function (el) {
        listOfMembersObj[el.userEmailId] = el
    });

    return {
        array:data,
        obj:listOfMembersObj
    }
}

function getSHChildrenIds(shData,userId,hashMapReq) {
    var shTeamMatesUserIds = [];
    _.each(shData,function (va) {

        // if( va._id == userId) {
        if( va._id.toString() == userId.toString()) {
            if(va.children != null){
                shTeamMatesUserIds = va.children.teamMatesUserId;
            }
        }

    })

    return shTeamMatesUserIds;
}

function getSHReportingManagers(shData,userId,hashMapReq) {
    var reportingManagers = [];
    var rms = [];
    _.each(shData,function (va) {
        if( va._id.toString() == userId.toString()) {
            // if( va._id.toString() == userId.toString()) {
            if(va.hierarchyPath != null){
                reportingManagers = va.hierarchyPath.split(",");
            }
        }
    })

    _.each(reportingManagers, function(rm){
        if (rm != ''){
            rms.push(rm);
        }
    })

    return rms;
}

router.get('/company/select/interactions/count/month',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user)
      , companyName = req.query.company
      , hierarchyList = []

      if(req.query.hierarchylist)
        hierarchyList = req.query.hierarchylist.split(",")
      else
        hierarchyList = [userId]
      hierarchyList = common.castListToObjectIds(hierarchyList)

    var sessionhierarchyList = req.session.hierarchy
    if(req.query.onSelect === "true"){
        hierarchyList = hierarchyList
    } else {
        hierarchyList = sessionhierarchyList
    }

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
      var timezone;
      if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
          timezone = user.timezone.name;
      }else timezone = 'UTC';
      var startDate = null

      if(!(req.query.days && req.query.days > 0)){
        req.query.days = 365
      }
      startDate = moment().tz(timezone)
      startDate.date(startDate.date() - Number(req.query.days));
      startDate = startDate.format();
      var endDate = moment().tz(timezone).format()
      UserKlass.find({corporateUser:true,hierarchyPath:{ $regex: userId.toString(), $options: 'i' },resource:{$ne:true}},{_id: 1})
      .exec(function(err, users){
          if(err)
          var allUsers = common.castListToObjectIds(_.pluck(users, "_id"))

          allUsers.push(common.castToObjectId(userId))
          interactionObj.fetchInteractionsCountByMonth(common.castToObjectId(userId), hierarchyList, allUsers, companyName, startDate, endDate, function(data){
            var months = [], interactionsCountTeam = [],interactionCountSelf =[], interactedBySelf = {}, interactedByTeam = {}
              , responseStartDate = data.length > 0 ? moment(data[0].sampleDate): moment()
              , responseEndDate = data.length > 0 ? moment(data[data.length -1].sampleDate) : moment()
              , hasDateEnded = !(data.length > 0)

            if(req.query.days){
              var graphStartDate = moment().subtract(Number(req.query.days), "days")
              if(graphStartDate < responseStartDate)
                responseStartDate = graphStartDate

                if(responseEndDate < moment()){
                    responseEndDate = moment()
                }
            }

              while(!hasDateEnded){

                if(responseEndDate.month() == responseStartDate.month() && responseEndDate.year() == responseStartDate.year()) {
                    hasDateEnded = true
                }
                months.push(responseStartDate.format("MMM\'YY"))
                responseStartDate = responseStartDate.add(1, "month")
            }

            for(var i=0, j=0; i < months.length; i++){

                if((data[j])&& (months[i] == moment().month(data[j]._id.month - 1).year(data[j]._id.year).format('MMM\'YY'))){
                interactionsCountTeam.push(data[j].interactionsByTeam)
                interactionCountSelf.push(data[j].interactionsBySelf)
                j++
              }
                else{
                interactionsCountTeam.push(0)
                interactionCountSelf.push(0)
              }
            }

            interactedBySelf.months = months
            interactedBySelf.interactions = interactionCountSelf
            interactedByTeam.months = months
            interactedByTeam.interactions = interactionsCountTeam
            var resObj = {
              "SuccessCode": 1,
              "Message": "",
              "ErrorCode": 0,
              "Data":{
                interactedBySelf: interactedBySelf,
                interactedByTeam: interactedByTeam
              }
            };

            res.send(resObj)
          })
      })
    })
})

router.get('/company/middle/bar/accounts',common.isLoggedInUserToGoNext,function(req,res){

    var userProfile = common.getUserFromSession(req);
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 25;

    var selfAccount = common.fetchCompanyFromEmail(userProfile.emailId);

    var query = {
        companyId:common.castToObjectId(userProfile.companyId),
        $and:[
            {"name":{$ne:"com"}},
            {"name": {'$regex' : '^((?!'+selfAccount+').)*$', '$options' : 'i'}}
        ]
    }
    
    if(req.query.searchContent){
        var searchContent = req.query.searchContent;

        query = {
            companyId:common.castToObjectId(userProfile.companyId),
            $and:[
                {"name":{$ne:"com"}},
                {"name":new RegExp(searchContent, "i")},
                {"name": {'$regex' : '^((?!'+selfAccount+').)*$', '$options' : 'i'}}
            ]
        }
    }
    
    var sort = {"name":1};

    if(req.query.options && !req.query.searchContent){

        if(req.query.options == "All"){
            sort = {"name":1}
        }

        if(req.query.options == "My"){
            query["ownerEmailId"] = userProfile.emailId;
        }

        if(req.query.options == "Important"){
            query["important"] = true;
            query["ownerEmailId"] = userProfile.emailId;
        }
    }

    if(!query.ownerEmailId){
        query["ownerEmailId"] = {$exists:true};
    }

    if(req.query.options == "Recent" && !req.query.searchContent){
        recentlyInteractedAccounts(userProfile,selfAccount,function (err,data) {
            respondToRequest(res,skip,limit,err,data,userProfile)
        });
    } else {
        accountsObj.getAllAccounts(query,sort,function (err,data) {
            respondToRequest(res,skip,limit,err,data,userProfile)
        });
    }
});

function setAccountAccess(userProfile,accounts,callback){
    accAccessObj.hierarchyAccess(common,userProfile,accounts,function (err,data) {
        callback(err,data)
    });
}

function respondToRequest(res,skip,limit,err,data,userProfile){

    var hierarchy = [
        {
            role: "Decision Maker",
            fullName: "Sudip Dutta",
            "_id" : "5af88a0ed75ae214e14cb390",
            "hierarchyParent" : null,
            "hierarchyPath" : null,
            "orgHead" : true,
            "designation" : "CEO",
            "emailId" : "sudip@relatas.com"
        },
        {
            role: "Influencer",
            fullName: "Naveen Paul",
            "_id" : "5af924f6d8159f0cc4adebd0",
            "emailId" : "naveenpaul@relatas.com",
            "hierarchyParent" : "ajitmoilyrelatas.com",
            "hierarchyPath" : ",ajitmoilyrelatas.com",
            "designation" : "VP Operations",
            "orgHead" : false
        },
        {
            role: "Influencer",
            fullName: "Ajit Moily",
            "_id" : "5af92663d8159f0cc4adebd1",
            "emailId" : "ajitmoily@relatas.com",
            "hierarchyParent" : "sudiprelatas.com",
            "hierarchyPath" : ",sudiprelatas.com",
            "orgHead" : false,
            "designation" : "COO, Operations"
        },
        {
            role: "Influencer",
            fullName: "Sumit R",
            "_id" : "5af9267dd8159f0cc4adebd2",
            "emailId" : "sumit@relatas.com",
            "hierarchyParent" : "naveenpaulrelatas.com",
            "hierarchyPath" : ",naveenpaulrelatas.com",
            "orgHead" : false,
            "designation" : "Operations Manager"
        },
        {
            role: "Decision Maker",
            fullName: "Sofia J",
            "_id" : "5af9267dd8159f0cc4adebd3",
            "emailId" : "sofia@relatas.com",
            "hierarchyParent" : "sudiprelatas.com",
            "hierarchyPath" : ",sudiprelatas.com",
            "orgHead" : false,
            "designation" : "CRO"
        },
        {
            role: "Influencer",
            fullName: "Joseph",
            "_id" : "5af9267dd8159f0cc4adebd9",
            "emailId" : "events@relatas.com",
            "hierarchyParent" : "sudiprelatas.com",
            "hierarchyPath" : ",sudiprelatas.com",
            "orgHead" : false,
            "designation" : "Director Marketing"
        },
        {
            role: "Influencer",
            fullName: "Anna",
            "_id" : "5af9267dd8159f0cc4adebd9",
            "emailId" : "admin@relatas.com",
            "hierarchyParent" : "sudiprelatas.com",
            "hierarchyPath" : ",sudiprelatas.com",
            "orgHead" : false,
            "designation" : "CMO"
        },
        {
            role: "Influencer",
            fullName: "Reena Gatti",
            "_id" : "5af9267dd8159f0cc4adebd9",
            "emailId" : "hello@relatas.com",
            "hierarchyParent" : "sofiarelatas.com",
            "hierarchyPath" : ",sofiarelatas.com",
            "orgHead" : false,
            "designation" : "VP Sales"
        }
    ]

    if(!err && data && data.length>0){

        // if(userProfile._id == "541c83d89ff44f767a23c546" || userProfile._id == "5adec19714cce40dc2ce2a7f"){
        if(userProfile._id == "541c83d89ff44f767a23c546"){
            data.unshift({
                "_id": "59b8dfe6b478aafd1e253978",
                "companyId": userProfile.companyId,
                "ownerId": "541c83d89ff44f767a23c546",
                "ownerEmailId": "sureshhoel@gmail.com",
                "name": "apple",
                "createdDate": new Date(),
                "lastInteractedDate": new Date(),
                "corporateUser": true,
                "target": [{
                    "_id": "59ba79701063b4eb2fa12c5e",
                    "year": "Apr 18 - Mar 19",
                    "amount": 10000,
                    "rawEndDate": new Date(),
                    "rawStartDate": new Date()
                }],
                "reportingHierarchyAccess": true,
                "important": true,
                "isTesting": true,
                "partner": true,
                "source": null,
                "accountAccessRequested": [],
                "contacts": [
                    "ceo@apple.com",
                    "naveenpaul@relatas.com",
                    "sudip@relatas.com",
                    "ajitmoily@relatas.com",
                    "events@relatas.com",
                    "sumit@relatas.com",
                    "hello@relatas.com",
                    "sofia@relatas.com",
                    "ceo@relatas.co.in",
                    "devmobile@relatas.com",
                    "rajiv.relatas",
                    "admin@relatas.com",
                    "dev@relatas.com"
                ],
                "access": [
                    {
                        "emailId": "sureshhoel@gmail.com"
                    },
                    {
                        "emailId": "relatas3@gmail.com"
                    },
                    {
                        "emailId": "relatas2@gmail.com"
                    },
                    {
                        "emailId": "ajitmoily@relatas.com"
                    },
                    {
                        "emailId": "sanjayjha3015@gmail.com"
                    },
                    {
                        "emailId": "naveenpaul.markunda@gmail.com"
                    },
                    {
                        "emailId": "naveenpaul@relatas.com"
                    }
                ],
                "costOfSales": 1000,
                "costOfDelivery": 99,
                "heiarchyWithAccess": [],
                "hierarchy": []
            })
        }

        var gleanedAccounts = [];

        _.each(data,function (el) {

            if(!common.isNumber(parseInt(el.name))){
                if(!_.includes(el.name,"/o=")){
                    gleanedAccounts.push(el)
                }
            } else {
                if(el.contacts && el.contacts.length>0){
                    _.each(el.contacts,function (co) {
                        if(common.validateEmail(co)){
                            if(!_.includes(el.name,"/o=")){
                                gleanedAccounts.push(el)
                            }
                            return false;
                        }
                    })
                }
            }
        });

        var total = gleanedAccounts.length;
        var results = gleanedAccounts.splice(skip,limit);

        setAccountAccess(userProfile,results,function (err,accounts) {
            var resObj = {
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{
                    total:total,
                    grandTotal:total,
                    skipped:skip,
                    limit:limit,
                    returned:accounts.length,
                    accounts:accounts
                },
                liuEmailId:userProfile.emailId
            };

            res.send(resObj)
        });

    } else {
        res.send({
            "SuccessCode": 0,
            "Message": "",
            "ErrorCode": 1,
            "Data":{
                total:0,
                grandTotal:0,
                skipped:0,
                limit:0,
                returned:0,
                accounts:[]
            },
            liuEmailId:userProfile.emailId
        })
    }
}

function recentlyInteractedAccounts(userProfile,selfAccount,callback){

    accountsObj.recentlyInteractedAccounts([common.castToObjectId(userProfile._id)],function (err,interactions) {

        var accounts = [],accountInteractions = {};

        if(!err && interactions && interactions.length>0){
            _.each(interactions,function (el) {

                var acc = common.fetchCompanyFromEmail(el._id)

                if(acc && acc != selfAccount){
                    accountInteractions[acc] = el
                    accounts.push(acc);
                }

            });
        }

        if(accounts.length>0){

            var query = {
                companyId:common.castToObjectId(userProfile.companyId),
                name:{$in:accounts},
                ownerEmailId:userProfile.emailId
            }

            var sort = {"name":1};

            query["ownerEmailId"] = {$exists:true}

            accountsObj.getAllAccounts(query,sort,function (err,accountsObj) {

                _.each(accountsObj,function (ac) {
                    ac.lastInteractedDate = new Date(accountInteractions[ac.name].lastInteractedDate)
                });

                accountsObj.sort(function (o1, o2) {
                    return new Date(o2.lastInteractedDate) > new Date(o1.lastInteractedDate) ? 1 : new Date(o2.lastInteractedDate) < new Date(o1.lastInteractedDate) ? -1 : 0;
                });

                callback(err,accountsObj)
            });
        } else {
            callback(err,[])
        }

    });
}

router.get('/company/middle/bar/accounts/new',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    
    var hierarchyList = []

    if(req.query.hierarchylist)
      hierarchyList = req.query.hierarchylist.split(",")
    if(hierarchyList.length == 0)
      hierarchyList.push(userId)
    hierarchyList = common.castListToObjectIds(hierarchyList)

    req.session.hierarchy = hierarchyList
    req.session.save();

    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 25;
    var arrTypes = [];
    var type = 'acc'
    if(common.contains(type,',')){
        arrTypes = type.split(',')
    }
    else arrTypes = [type]
     contactObj.searchUserContacts_accounts(req.query.searchContent,common.castToObjectId(userId), hierarchyList,skip,limit,arrTypes,function(error,data){

         var total = data.length;
         var results = data.splice(skip,limit);
         var resObj = {
             "SuccessCode": 1,
             "Message": "",
             "ErrorCode": 0,
             "Data":{
                 total:total,
                 grandTotal:total,
                 skipped:skip,
                 limit:limit,
                 returned:results.length,
                 contacts:results
             }
         };
         res.send(resObj)
     })
});

router.get('/company/middle/bar/accounts/:selected',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var selectedCompany = req.params.selected
    //userManagementObj.findUserProfileByIdWithCustomFields(userId,{})
    var hierarchyList = []

    if(req.query.hierarchylist)
      hierarchyList = req.query.hierarchylist.split(",")
    if(hierarchyList.length == 0)
      hierarchyList.push(userId)
    hierarchyList = common.castListToObjectIds(hierarchyList)
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 25;
    var arrTypes = [];
    var type = 'acc'
    if(common.contains(type,',')){
        arrTypes = type.split(',')
    }
    else arrTypes = [type];

        contactObj.searchContactWithinCompany(selectedCompany, req.query.searchContent,common.castToObjectId(userId), hierarchyList, function(error,data){

            interactionObj.getRecentlyInteractedContactsByEmailId(common.castToObjectId(userId),_.pluck(data,'_id'),function (interactedContacts) {
                var total = data.length;

                var mergedContacts = _.map(data, function(a) {
                    return _.extend(a, _.find(interactedContacts, function(b) {
                        return a._id === b._id;
                    }))
                });

                var results = mergedContacts.splice(skip,limit);
                var resObj = {
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        total:total,
                        grandTotal:total,
                        skipped:skip,
                        limit:limit,
                        returned:results.length,
                        contacts:results
                    }
                };
                res.send(resObj)

            })
        })
});

router.get('/company/past/days/interacted/contact/type',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user)
    , hierarchyList = []

    if(req.query.hierarchylist)
      hierarchyList = req.query.hierarchylist.split(",")
    if(hierarchyList.length == 0)
      hierarchyList.push(userId)
    hierarchyList = common.castListToObjectIds(hierarchyList)


    req.session.hierarchy = hierarchyList
    req.session.save();

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(common.checkRequired(user)){
            common.interactionDataFor(function(days){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var date = moment().tz(timezone);
                var start = date.clone();

                start.date(start.date() - days);

                var data = {};

              contactObj.getContactsByTypeCount_companyLanding(hierarchyList, function (error, dataTypes) {
                if (common.checkRequired(dataTypes) && dataTypes.length > 0) {
                  data.companies = dataTypes[0].totalCompanies
                  data.types = dataTypes;
                }
                res.send({
                  "SuccessCode": 1,
                  "Message": "",
                  "ErrorCode": 0,
                  "Data": data
                });
              });
            })
        }
        else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });
});

router.get("/company/interactions/initiative", common.isLoggedInUserToGoNext,function(req,res){
var userId = common.getUserId(req.user)
  , companyName = req.query.company
  , hierarchyList = []

  if(req.query.hierarchylist)
    hierarchyList = req.query.hierarchylist.split(",")
  if(hierarchyList.length == 0)
    hierarchyList = [userId]
  hierarchyList= common.castListToObjectIds(hierarchyList)

    var sessionhierarchyList = req.session.hierarchy

    if(req.query.onSelect === "true"){
        hierarchyList = hierarchyList
    } else {
        hierarchyList = common.castListToObjectIds(sessionhierarchyList)
    }

    //hierarchyList = common.castListToObjectIds(_.difference(hierarchyList, userId))
    common.getProfileOrStoreProfileInSession(userId,req,function(user) {
      if (common.checkRequired(user)) {
        var noOfDays = 180
        if(req.query.days && req.query.days > 0)
          noOfDays = req.query.days

        interactionObj.getInteractionInitiative(common.castToObjectId(userId), hierarchyList, companyName, noOfDays, function(err, data){

          res.send({
              "SuccessCode": 1,
              "Message": "",
              "ErrorCode": 0,
              "Data": data
          })
        })
      }
    })
})

/* Deprecated route */
router.get('/company/past/days/interacted/info/type',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    common.getProfileOrStoreProfileInSession(userId,req,function(user) {
        if (common.checkRequired(user)) {
            common.interactionDataFor(function(days){
                var timezone;
                if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                    timezone = user.timezone.name;
                } else timezone = 'UTC';

                var date = moment().tz(timezone);
                var start = date.clone();

                start.date(start.date() - days);
                profileManagementClassObj.getUsersReportingTo(common.castToObjectId(userId),user.emailId,function(uList,emailIdList){
                    interactionObj.getInteractionsCountByCompanyLocationTypeDate_companyLanding(uList,start.format(),date.format(),function(resultObj){
                        resultObj.interactionsByType = common.formatInteractionTypeGraphData(resultObj.interactionsByType)

                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":resultObj
                        });
                    });
                })
            })
        }
    })
});

router.get('/company/past/days/interacted/:interactionType/',common.isLoggedInUserToGoNext,function(req,res){

    var usersCompany = req.session.companyName

    var methodName = "getInteractionsCountBy" + req.params.interactionType[0].toUpperCase() + req.params.interactionType.slice(1)
  if(!(interactionObj[methodName] && typeof interactionObj[methodName] === "function"))
    res.send(errorObj.generateErrorResponse({
        status: statusCodes.SOMETHING_WENT_WRONG_CODE,
        key: 'ROUTE_DOES_NOT_EXIST'
  }))
  else{
    var userId = common.getUserId(req.user);
    var hierarchyList = []
    if(req.query.hierarchylist)
      hierarchyList = req.query.hierarchylist.split(",")
    hierarchyList = common.castListToObjectIds(hierarchyList)

      req.session.hierarchy = hierarchyList
      req.session.save();

      if(hierarchyList.length == 0)
      hierarchyList.push(common.castToObjectId(userId))
    common.getProfileOrStoreProfileInSession(userId,req,function(user) {
        if (common.checkRequired(user)) {
            common.interactionDataFor(function(days){
                var timezone;
                if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                    timezone = user.timezone.name;
                } else timezone = 'UTC';

                var date = moment().tz(timezone);
                var start = date.clone();
                var noOfDays = 180
                if(req.query.days != undefined)
                  noOfDays = req.query.days
                start.date(start.date() - noOfDays);

                userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1},function(error,user) {

                    usersCompany = common.fetchCompanyFromEmail(user.emailId);
                    
                    interactionObj[methodName](common.castToObjectId(userId), hierarchyList,start.format(),date.format(),usersCompany,user.serviceLogin,function(resultObj){

                        if(resultObj.interactionsByType)
                            resultObj.interactionsByType = common.formatInteractionTypeGraphData(resultObj.interactionsByType)

                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data":resultObj
                        });
                    });
                
                })
            })
        }
    })
  }
})

router.get('/company/interacted/typeforcompany/',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user)
      , hierarchyList = []
      , companyName = req.query.company

    if(req.query.hierarchylist)
      hierarchyList = req.query.hierarchylist.split(",")
    var noOfDays = 180
    if(req.query.days != undefined)
        noOfDays = req.query.days
    if(hierarchyList.length == 0)
      hierarchyList = [userId]
    hierarchyList = common.castListToObjectIds(hierarchyList)

    var sessionhierarchyList = req.session.hierarchy

    if(req.query.onSelect === "true"){
        hierarchyList = hierarchyList
    } else {
        hierarchyList = common.castListToObjectIds(sessionhierarchyList)
    }

    common.getProfileOrStoreProfileInSession(userId,req,function(user) {
        if (common.checkRequired(user)) {
            common.interactionDataFor(function(days){
                var timezone;
                if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                    timezone = user.timezone.name;
                } else timezone = 'UTC';

                var date = moment().tz(timezone);
                var start = date.clone();
                start.date(start.date() - noOfDays);
                  interactionObj.getInteractionsCountByTypeForCompany(common.castToObjectId(userId), hierarchyList,start.format(),date.format(),companyName,function(resultObj){

                    if(resultObj.interactionsByType)
                      resultObj.interactionsByType = common.formatInteractionTypeGraphData(resultObj.interactionsByType)

                      res.send({
                          "SuccessCode": 1,
                          "Message": "",
                          "ErrorCode": 0,
                          "Data":resultObj
                      });
                  });
            })
        }
    })
})

router.get("/company/heatmap",common.isLoggedInUserToGoNext, function(req, res){
  var userId = common.getUserId(req.user)
    , noOfDays = 30
    , hierarchyList = []
    //, userList = []

    var usersCompany = req.session.companyName
    var userObjId = common.castToObjectId(userId)

  if(req.query.hierarchylist)
    hierarchyList = req.query.hierarchylist.split(",");
    if(hierarchyList.length == 0)
    hierarchyList.push(userId)
  hierarchyList = common.castListToObjectIds(hierarchyList)

    req.session.hierarchy = hierarchyList
    req.session.save();

  if(req.query.days)
    noOfDays = req.query.days;

    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1},function(error,user){

        usersCompany = common.fetchCompanyFromEmail(user.emailId);

        interactionObj.getInteractionTimeline(hierarchyList, noOfDays, userId, userObjId,usersCompany,user.serviceLogin, function(data){
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": data
            });
        });

    });

});

router.get("/get/company/by/domain", function(req,res){

    var url = req.query.url;
    common.getCompanyLoginService(url,function (err,serviceLogin) {
        res.send(serviceLogin)
    });

});

router.get("/account/get/owners", function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;
    var skip = req.query.skip?req.query.skip:0;
    var searchContent = req.query.searchContent?req.query.searchContent:null;

    var query = {
        ownerId:common.castToObjectId(userId),
        companyId:common.castToObjectId(companyId)
    };

    if(common.checkRequired(req.query.isCorporateAdmin)){
        query = {
            companyId:common.castToObjectId(companyId)
        };
    }

    if(searchContent){
        var selfAccount = common.fetchCompanyFromEmail(userProfile.emailId);

        query = {
            ownerId:common.castToObjectId(userId),
            companyId:common.castToObjectId(companyId),
            $and:[
                {"name":{$ne:"com"}},
                {"name":new RegExp(searchContent, "i")},
                {"name": {'$regex' : '^((?!'+selfAccount+').)*$', '$options' : 'i'}}
            ]
        }

        if(common.checkRequired(req.query.isCorporateAdmin)){
            query = {
                companyId:common.castToObjectId(companyId),
                $and:[
                    {"name":{$ne:"com"}},
                    {"name":new RegExp(searchContent, "i")},
                    {"name": {'$regex' : '^((?!'+selfAccount+').)*$', '$options' : 'i'}}
                ]
            };
        }
    }

    if(req.query.getAllAccounts){
        query = {
            companyId:common.castToObjectId(companyId),
            ownerId:common.castToObjectId(req.query.getAllAccounts)
        };

        if(req.query.searchContent){
            query = {
                companyId: common.castToObjectId(companyId),
                ownerId: common.castToObjectId(req.query.getAllAccounts),
                $and: [
                    {"name": {$ne: "com"}},
                    {"name": new RegExp(req.query.searchContent, "i")},
                    {"name": {'$regex': '^((?!' + selfAccount + ').)*$', '$options': 'i'}}
                ]
            }
        }
    }

    accountsObj.getAllAccounts(query,{"name":1},function (err,accounts) {

        skip = parseInt(skip);
        
        var disablePagination = false;
        
        if(req.query.getAllAccounts){
            disablePagination = true;   
        }

        if(!err && accounts){
            var total = accounts.length;
            var results = req.query.getAllAccounts?accounts:accounts.splice(skip,25);
            var resObj = {
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{
                    disablePagination:disablePagination,
                    total:total,
                    grandTotal:total,
                    skipped:skip,
                    limit:25,
                    returned:results.length,
                    accounts:results
                }
            };

            res.send(resObj)

        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data:[]
            })
        }
    })

});

router.get("/company/network/count", common.isLoggedInUserToGoNext, function(req,res){
    var userId = common.getUserId(req.user)   
      , companyName = req.query.company
      , hierarchyList = []
    if(req.query.hierarchylist)
      hierarchyList = req.query.hierarchylist.split(",")
    hierarchyList = common.castListToObjectIds(_.difference(hierarchyList, userId))
    contactObj.getNetworkAndExtendedNetworkCount(common.castToObjectId(userId), hierarchyList, companyName, function(data){
      res.send({
          "SuccessCode": 1,
          "Message": "",
          "ErrorCode": 0,
          "Data": data
      })      
    })
})

router.get('/account/get/interactions',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var userIdList = common.getUserListForQuery(req);
    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?common.castToObjectId(userProfile.companyId):null;

    var account = req.query.account;
    var contacts = common.getContactsListForQuery(req);
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;

    accountsObj.getInteractionsCount(userId,account,userIdList,null,null,skip,contacts,userProfile.serviceLogin,companyId,function (err,interactionsCount) {
        accountsObj.getInteractionsPaginated(userIdList,null,null,skip,contacts,userProfile.serviceLogin,companyId,function (err,interactions) {
    
            if(!err){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":interactions,
                    interactionsCount:interactionsCount
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":"",
                    interactionsCount:0
                })
            }
        });
    });

});

router.get('/account/get/interactions/new',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var userIdList = common.getUserListForQuery(req);
    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?common.castToObjectId(userProfile.companyId):null;

    var account = req.query.account;
    var contacts = common.getContactsListForQuery(req);
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;

    accountsObj.getInteractionsCount_new(userId,account,userIdList,null,null,skip,contacts,userProfile.serviceLogin,companyId,function (err,interactionsCount) {
        accountsObj.getInteractionsPaginated_new(userIdList,null,null,skip,contacts,userProfile.serviceLogin,companyId,function (err,interactions) {

            if(!err){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":interactions,
                    interactionsCount:interactionsCount
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":"",
                    interactionsCount:0
                })
            }
        });
    });

});

router.get('/account/get/interactions/opps/notes',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var userIdList = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);
    var account = req.query.account;
    var userProfile = common.getUserFromSession(req);
    var serviceLogin = userProfile?userProfile.serviceLogin:null;
    var companyId = userProfile.companyId?common.castToObjectId(userProfile.companyId):null

    common.userFiscalYear(userId,function (err,fyMonth,startEndObj) {

        var from = startEndObj.start,
            to = startEndObj.end;

        async.parallel([
            function (callback) {
                getInteractionsByDateRange(userIdList,contacts,from,to,serviceLogin,companyId,callback)
            },
            function (callback) {
                getOpportunitiesByAccount(userIdList,contacts,from,to,callback)
            },
            // function (callback) {
            //     getNotesByAccount(userIdList,contacts,from,to,callback)
            // }
        ], function (errors,data) {

            if(!errors){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        interactions:data[0]?data[0]:[],
                        opps:data[1]?data[1]:[],
                        // notes:data[2]?data[2]:[]
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":""
                })
            }
        });
    })
});

router.get('/account/get/interactions/opps/notes/new',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var userIdList = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);
    var account = req.query.account;
    var userProfile = common.getUserFromSession(req);
    var serviceLogin = userProfile?userProfile.serviceLogin:null;
    var companyId = userProfile.companyId?common.castToObjectId(userProfile.companyId):null

    common.userFiscalYear(userId,function (err,fyMonth,startEndObj,allQuarters,timezone) {

        var from = allQuarters[allQuarters.currentQuarter].start,
            to = allQuarters[allQuarters.currentQuarter].end;

        async.parallel([
            function (callback) {
                getInteractionsByDateRange_new(userIdList,contacts,from,to,serviceLogin,companyId,callback)
            },
            function (callback) {
                getOpportunitiesByAccount(userIdList,contacts,null,null,callback)
            }
        ], function (errors,data) {

            if(!errors){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        interactions:data[0]?data[0]:[],
                        opps:data[1]?data[1]:[],
                        // notes:data[2]?data[2]:[]
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":""
                })
            }
        });
    })
});

router.get('/account/conversion/rate',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);
    
    var monthNames = common.monthNames();
    var account = req.query.account;

    oppObj.oppCreatedByMonthForAccount(hierarchyList,new Date(moment().subtract(6, "months")),contacts,function (err,opps) {

        if (!err && opps) {

            var filler = [];
            var monthsPrev = req.query.monthsPrev?req.query.monthsPrev:6;

            for(var i = 0;i<monthsPrev;i++){
                var prevDate = new Date(moment().subtract(i, "month").toDate())

                var obj = {
                    monthYear:monthNames[prevDate.getUTCMonth()].substring(0,3)+" "+prevDate.getUTCFullYear(),
                    sortDate:prevDate,
                    count:0
                };

                filler.push(obj)
            }

            var dataCreated = [],dataClosed = [];

            _.each(opps,function (op) {
                var obj = {};

                if(!op.createdDate){
                    op.createdDate = op._id.getTimestamp()
                }

                // if(Closed.test(op.stageName) || closed.test(op.stageName) || Close.test(op.stageName) || close.test(op.stageName) || op.isClosed){
                if(op.stageName && (op.stageName.toLowerCase() == "closed won" || op.stageName.toLowerCase() == "close won")){
                    dataClosed.push({
                        emailId:op.userEmailId,
                        monthYear:monthNames[moment(new Date(op.closeDate)).month()].substring(0,3) +" "+moment(new Date(op.closeDate)).year(),
                        sortDate:op.closeDate
                    })

                }

                obj["monthYear"] = monthNames[moment(new Date(op.createdDate)).month()].substring(0,3) +" "+moment(new Date(op.createdDate)).year();
                obj["emailId"] = op.userEmailId
                obj["sortDate"] = op.createdDate
                dataCreated.push(obj)
            });

            var created = _
                .chain(dataCreated)
                .groupBy('monthYear')
                .map(function(value, key) {

                    return {
                        monthYear: key,
                        sortDate:value[0].sortDate,
                        count: value.length
                    }
                })
                .value();

            var closed = _
                .chain(dataClosed)
                .groupBy('monthYear')
                .map(function(value, key) {

                    return {
                        monthYear: key,
                        sortDate:value[0].sortDate,
                        count: value.length
                    }
                })
                .value();

            //Find values that are in filler but not in arrayValues
            var fillerResults = filler.filter(function(obj) {
                return !created.some(function(obj2) {
                    return obj.monthYear == obj2.monthYear;
                });
            });

            var data = created.concat(fillerResults)

            //Find values that are in filler but not in arrayValues
            var fillerResultsClose = filler.filter(function(obj) {
                return !closed.some(function(obj2) {
                    return obj.monthYear == obj2.monthYear;
                });
            });

            var data2 = closed.concat(fillerResultsClose)

            var nonExistingMonthsClose = data.filter(function(obj) {
                return !data2.some(function(obj2) {
                    return obj.monthYear == obj2.monthYear;
                });
            });

            _.each(nonExistingMonthsClose,function (el) {
                data2.push({
                    monthYear:el.monthYear,
                    sortDate:el.sortDate,
                    count:0
                })
            });

            var nonExistingMonthsOpen = data2.filter(function(obj) {
                return !data.some(function(obj2) {
                    return obj.monthYear == obj2.monthYear;
                });
            });

            _.each(nonExistingMonthsOpen,function (el) {
                data.push({
                    monthYear:el.monthYear,
                    sortDate:el.sortDate,
                    count:0
                })
            });

            data.sort(function (o1, o2) {
                return new Date(o1.sortDate) > new Date(o2.sortDate) ? 1 : new Date(o1.sortDate) < new Date(o2.sortDate) ? -1 : 0;
            });

            data2.sort(function (o1, o2) {
                return new Date(o1.sortDate) > new Date(o2.sortDate) ? 1 : new Date(o1.sortDate) < new Date(o2.sortDate) ? -1 : 0;
            });

            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data:data.slice(Math.max(data.length - 7, 1)),
                dataClosed:data2.slice(Math.max(data2.length - 7, 1))
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data:[]
            })
        }
    })

});

router.get('/account/interactions/by/contacts',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = common.getUserListForQuery(req);
    var contacts = [];
    if(req.query.contacts)
        contacts = req.query.contacts.split(",")

    accountsObj.getInteractionsByContacts(hierarchyList,contacts,function (err,interactions) {

        if(!err){

            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data:interactions,
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data:[]
            })
        }
    })
});

router.get('/account/interactions/by/contacts/new',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = common.getUserListForQuery(req);
    var contacts = [];
    if(req.query.contacts)
        contacts = req.query.contacts.split(",")

    accountsObj.getInteractionsByContacts_new(hierarchyList,contacts,function (err,interactions) {

        if(!err){

            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data:interactions
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data:[]
            })
        }
    })
});

router.get('/account/interactions/verbose',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);
    var userProfile = common.getUserFromSession(req)
    var serviceLogin = userProfile?userProfile.serviceLogin:null;
    var companyId = userProfile.companyId?common.castToObjectId(userProfile.companyId):null

    common.userFiscalYear(userId,function (err,fyMonth,startEndObj) {

        var from = startEndObj.start,
            to = startEndObj.end;

        // companyId = null;

        async.parallel([
            function (callback) {
                accountsObj.getInteractionsByAccountMembers(hierarchyList,contacts,from,to,serviceLogin,companyId,callback)
            },
            function (callback) {
                accountsObj.getInteractionsByTeamMembers(hierarchyList,contacts,from,to,serviceLogin,companyId,callback)
            }
        ], function (errors,data) {

            var account = [];

            if(!data[0][0]){
                _.each(contacts,function (co) {
                    account.push({
                        _id:co,
                        count:0,
                        lastInteractedDate:null
                    })
                });
            } else {
                account = data[0]

                var contactsWithInteractions = _.difference(contacts,_.pluck(account,"_id"))

                _.each(contactsWithInteractions,function (co) {
                    account.push({
                        _id:co,
                        count:0,
                        lastInteractedDate:null
                    })
                });

            }

            if(!errors){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        account:account,
                        teamMembers:data[1]?data[1]:[]
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":""
                })
            }
        });

    });

});

router.get('/account/interactions/verbose/new',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);
    var userProfile = common.getUserFromSession(req)
    var serviceLogin = userProfile?userProfile.serviceLogin:null;
    var companyId = userProfile.companyId?common.castToObjectId(userProfile.companyId):null

    common.userFiscalYear(userId,function (err,fyMonth,startEndObj,allQuarters,timezone) {

        var from = allQuarters[allQuarters.currentQuarter].start,
            to = allQuarters[allQuarters.currentQuarter].end;

        async.parallel([
            function (callback) {
                accountsObj.getInteractionsByAccountMembers_new(hierarchyList,contacts,from,to,serviceLogin,companyId,callback)
            },
            function (callback) {
                accountsObj.getInteractionsByTeamMembers_new(hierarchyList,contacts,from,to,serviceLogin,companyId,callback)
            },
            function (callback) {
                getOpportunitiesByAccount(hierarchyList,contacts,null,null,callback)
            }
        ], function (errors,data) {

            var account = [];

            if(!data[0][0]){
                _.each(contacts,function (co) {
                    account.push({
                        _id:co,
                        count:0,
                        lastInteractedDate:null
                    })
                });
            } else {
                account = data[0]

                var contactsWithInteractions = _.difference(contacts,_.pluck(account,"_id"))

                _.each(contactsWithInteractions,function (co) {
                    account.push({
                        _id:co,
                        count:0,
                        lastInteractedDate:null
                    })
                });

            }

            if(!errors){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        account:account,
                        teamMembers:data[1]?data[1]:[],
                        opportunities:data[2]?data[2]:[]
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":""
                })
            }
        });

    });

});

router.get('/account/interactions/by/cxo',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = common.getUserListForQuery(req);
    var account = req.query.account;

    accountsObj.getInteractionsByCxo(hierarchyList,account,function (errors,data) {

        if(!errors){
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":data
            })
        } else {
            res.send({
                "SuccessCode": 0,
                "Message": "",
                "ErrorCode": 1,
                "Data":""
            })
        }
    });
});

router.get('/account/group/count',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

    var account = req.query.account;

    common.userFiscalYear(userId,function (err,fyMonth) {

        if (err && !fyMonth) {
            fyMonth = "April"; //Default
        }

        var currentYr = new Date().getFullYear()
        var currentMonth = new Date().getMonth()

        var toDate = null;
        var fromDate = new Date(moment().startOf('month'));
        fromDate.setMonth(monthNames.indexOf(fyMonth))

        if (currentMonth < monthNames.indexOf(fyMonth)) {
            fromDate.setFullYear(currentYr - 1)
        }

        toDate = moment(fromDate).add(11, 'month');
        toDate = moment(toDate).endOf('month');

        common.getUserAccessControl(hierarchyList,function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq,companyTargetAccess,companyDetails) {

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            oppObj.groupByUsersAndStageForAccount(hierarchyList,fromDate,toDate,contacts,netGrossMarginReq,primaryCurrency,currenciesObj,function (err,opps) {

                if(!err && opps){

                    var stages =[
                        'Prospecting',
                        'Evaluation',
                        'Proposal',
                        'Close Won',
                        'Close Lost'
                    ]

                    var existingStages = _.pluck(opps,"_id");

                    var uniqueResult = stages.filter(function(obj) {
                        return !existingStages.some(function(obj2) {
                            return obj == obj2;
                        });
                    });

                    var data = opps;

                    var nonExisting = [];
                    if(uniqueResult.length>0){
                        _.each(uniqueResult,function (el) {
                            nonExisting.push({
                                _id:el,
                                totalAmount:0,
                                count:0
                            })
                        })

                        data = opps.concat(nonExisting)
                    }

                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data: data,
                        fy:{fromDate:new Date(moment(fromDate).add(1,'day')),toDate:new Date(moment(toDate).subtract(1,'day'))}
                    })

                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data: [],
                        fy:{fromDate:new Date(moment(fromDate).add(1,'day')),toDate:new Date(moment(toDate).subtract(1,'day'))}
                    })
                }
            })
        });
    });

});

router.get('/account/details',common.isLoggedInUserOrMobile, function(req, res) {

    var userProfile = common.getUserFromSession(req)
    var account = req.query.account;
    var companyId = userProfile.companyId;

    accountsObj.findTemplate({companyId:common.castToObjectId(companyId),name:account},function (err,account) {
        if(!err){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data:account
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })

});

router.post('/account/monetary/settings',common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req)

    if(req.body){

        var companyId = userProfile.companyId;

        var findQuery = {
            companyId:common.castToObjectId(companyId),
            name:req.body.account
        }

        var updateObj = {
            $set:{
                target:req.body.target,
                costOfSales:parseFloat(req.body.costOfSales),
                costOfDelivery:parseFloat(req.body.costOfDelivery),
                reportingHierarchyAccess:req.body.reportingHierarchyAccess
            }
        };

        accountsObj.findTemplate(findQuery,function (err,account) {

            if(!err && account && account.target && account.target.length>0){
                if(req.body.target && req.body.target.length>0){
                    _.each(account.target,function (tr) {
                        _.each(req.body.target,function (tr1) {
                            if(tr.year == tr1.year){
                                tr.amount = tr1.amount
                                tr.costOfSales = tr1.costOfSales
                                tr.costOfDelivery = tr1.costOfDelivery
                            }
                        })
                    });

                    updateObj = {
                        $set:{
                            target:account.target,
                            costOfSales:parseFloat(req.body.costOfSales),
                            costOfDelivery:parseFloat(req.body.costOfDelivery),
                            reportingHierarchyAccess:req.body.reportingHierarchyAccess
                        }
                    };
                }
            }

            accountsObj.updateAccountDetails(findQuery,updateObj,function (err,result) {

                if(!err){
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0
                    })
                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1
                    })
                }
            })
        })
    } else {
        res.send({
            SuccessCode: 0,
            ErrorCode: 1
        })
    }
});

router.post('/account/update/relationship',common.isLoggedInUserOrMobile, function(req, res){
    var userProfile = common.getUserFromSession(req)

    if(req.body){

        var updateObj = {
            $set:{
                partner:req.body.partner,
                important:req.body.important
            }
        };

        var companyId = userProfile.companyId;

        var findQuery = {
            companyId:common.castToObjectId(companyId),
            name:req.body.account
        }

        accountsObj.updateAccountDetails(findQuery,updateObj,function (err,result) {

            if(!err){
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0
                })
            } else {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1
                })
            }
        })
    } else {
        res.send({
            SuccessCode: 0,
            ErrorCode: 1
        })
    }
});

router.get('/account/request/access',common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req);
    var account = req.query.account;

    accountsObj.requestAccess(common.castToObjectId(userProfile.companyId),account,userProfile.emailId,function (err,result) {

        if(!err){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })

});

router.get('/account/grant/access',common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req);
    var account = req.query.account;
    var emailId = req.query.emailId;

    accountsObj.grantAccess(common.castToObjectId(userProfile.companyId),account,emailId,userProfile.emailId,function (err,result) {

        if(!err){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })

});

router.post('/account/grant/access/bulk',common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req);
    var accounts = req.body.accounts;

    accountsObj.grantAccessBulk(common.castToObjectId(userProfile.companyId),accounts,userProfile.emailId,function (err,result) {

        if(!err){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })

});

router.post('/account/transfer/ownership/bulk',common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req);
    var accounts = req.body.accounts;
    var transferTo = req.body.transferTo;
    transferTo.userId = common.castToObjectId(transferTo.userId)

    accountsObj.transferOwnershipBulk(common.castToObjectId(userProfile.companyId),accounts,userProfile.emailId,transferTo,function (err,result) {

        if(!err){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })

});

router.get('/account/revoke/access',common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req);
    var account = req.query.account;
    var emailId = req.query.emailId;

    accountsObj.revokeAccess(common.castToObjectId(userProfile.companyId),account,emailId,userProfile.emailId,function (err,result) {

        if(!err){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })

});

router.get('/account/activity/log',common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req);
    var account = req.query.account;

    accountsObj.activityLog(common.castToObjectId(userProfile.companyId),account,function (err,result) {

        if(!err){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data:result
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data:[]
            })
        }
    })

});

router.get('/account/insights',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var account = req.query.account;
    var userIdList = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId?common.castToObjectId(userProfile.companyId):null

    common.userFiscalYear(userId,function (err,fyMonth,startEndMonths,quarters,timezone) {

        var from = startEndMonths.start,
            to = startEndMonths.end;

        async.parallel([
            function (callback) {
                opportunitiesPastCloseDateByAccount(userIdList,contacts,callback)
            },
            function (callback) {
                accountsObj.getLastInteraction(userIdList,contacts,from,to,companyId,callback)
            }
            // ,
            // function (callback) {
            //     accountsObj.getLastInteractionThread(userIdList,account,from,to,callback)
            // }
        ], function (errors,data) {

            if(!errors){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        opps:data[0]?data[0]:[],
                        interaction:data[1]?data[1]:[]
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":""
                })
            }
        });
    });

});

router.get('/account/insights/new',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var account = req.query.account;
    var userIdList = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId?common.castToObjectId(userProfile.companyId):null

    common.userFiscalYear(userId,function (err,fyMonth,startEndMonths,quarters,timezone) {

        var from = startEndMonths.start,
            to = startEndMonths.end;

        async.parallel([
            function (callback) {
                opportunitiesPastCloseDateByAccount(userIdList,contacts,callback)
            },
            function (callback) {
                accountsObj.getLastInteraction_new(userIdList,contacts,from,to,companyId,callback)
            }
            // ,
            // function (callback) {
            //     accountsObj.getLastInteractionThread(userIdList,account,from,to,callback)
            // }
        ], function (errors,data) {

            if(!errors){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        opps:data[0]?data[0]:[],
                        interaction:data[1]?data[1]:[]
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":""
                })
            }
        });
    });

});

router.get('/account/relationship/relevance',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var account = req.query.account;
    var userIdList = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);

    common.userFiscalYear(userId,function (err,fyMonth,startEndMonths,allQuarters,timezone) {

        var from = allQuarters[allQuarters.currentQuarter].start,
            to = allQuarters[allQuarters.currentQuarter].end;

        accountsObj.accountRelationshipRelevance(userIdList,from,to,timezone,contacts,function (err,data) {
            if(!err){
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data:data
                })
            } else {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                    Data:""
                })
            }
        })
    })
});

router.get('/account/deals/at/risk',common.isLoggedInUserOrMobile, function(req, res){

    var account = req.query.account;
    var from = req.query.from;
    var to = req.query.to;
    var userIdList = common.getUserListForQuery(req);
    var userId = common.getUserIdFromMobileOrWeb(req);
    var contacts = common.getContactsListForQuery(req);

    common.userFiscalYear(userId,function (err,fyMonth,startEndMonths) {

        dealsAtRiskObj.findDealsAtRiskInAccount(userIdList,startEndMonths.start,startEndMonths.end,contacts,function (err,dealsAtRisk) {

            res.send({
                dealsAtRisk:dealsAtRisk.length,
                data:dealsAtRisk
            })
        });
    })

});

router.get('/account/access/requested',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?userProfile.companyId:null;

    var query = {
        ownerId:common.castToObjectId(userId),
        'accountAccessRequested.1': {$exists: true}
    };

    if(companyId){
        query = {
            ownerId:common.castToObjectId(userId),
            companyId:common.castToObjectId(companyId),
            'accountAccessRequested.0': {$exists: true}
        };
    }

    accountsObj.getAllAccounts(query,{"name":1},function (err,accounts) {

        if(!err && accounts){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data:accounts
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data:[]
            })
        }
    })

});

router.get('/account/master/types/by/group',common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?userProfile.companyId:null;
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, data){

        accountsObj.getMasterAccTypesByGroup(common.castToObjectId(data[0].companyId),function (err,accounts) {

            if(!err && accounts){
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data:accounts
                })
            } else {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                    Data:[]
                })
            }
        })
    });
});

router.post('/account/master/types/search',common.isLoggedInUserOrMobile, function(req, res){

    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?userProfile.companyId:null;

    oppWeightsObj.getManualAccountsForGroup(companyId,req.body._id,function (cachedAccounts) {
        if(cachedAccounts){
            searchForAccountsAndRespond(req,res,cachedAccounts,req.body)
        } else {
            accountsObj.getMasterAccsForGroup(common.castToObjectId(companyId),req.body._id,function (err,accounts) {
                if(!err && accounts && accounts.length>0){
                    var key = String(companyId)+req.body._id;
                    oppWeightsObj.setCache(key,accounts);
                }
                searchForAccountsAndRespond(req,res,accounts,req.body)
            })
        }
    });
});

router.get("/accounts/hierarchy",common.isLoggedInUserOrMobile, function(req, res){
    res.render("accounts/hierarchy")
});

router.post("/account/admin/set/org/head",common.isLoggedInUser,function (req,res) {

    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?userProfile.companyId:null;
    req.body.liuEmailId = userProfile.emailId
    accountsObj.setOrgHead(req.body,common,common.castToObjectId(companyId),function (err,result) {
        getAccount(companyId,req.body.name,function(account){
            res.send(account)
        });
    })
})

router.post("/account/admin/contact/update",common.isLoggedInUser,function (req,res) {

    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?userProfile.companyId:null;
    req.body.liuEmailId = userProfile.emailId

    accountsObj.updateContactDetails(req.body,common.castToObjectId(companyId),function (err,result) {
        getAccount(companyId,req.body.name,function(account){
            res.send(account)
        });
    })
})

router.post('/account/admin/users/remove',common.isLoggedInUser,function(req,res){

    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?userProfile.companyId:null;
    req.body.liuEmailId = userProfile.emailId

    accountsObj.removeFromHierarchy([req.body],common.castToObjectId(companyId),function (err,result) {
        getAccount(companyId,req.body.name,function(account){
            res.send(account)
        });
    })
});

router.post('/account/admin/user/update',common.isLoggedInUser,function(req,res){

    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?userProfile.companyId:null;

    if(req.body.user){
        req.body.user.liuEmailId = userProfile.emailId
    }
    if(req.body.contact){
        req.body.contact.liuEmailId = userProfile.emailId
    }

    accountsObj.addUserToHierarchy(req.body.user,common,companyId,function (err,result) {
        accountsObj.updateContactDetails(req.body.contact,common.castToObjectId(companyId),function (err,result) {
            var accName = null;
            if(req.body.user && req.body.user.name) {
                accName = req.body.user.name
            }
            if(req.body.contact && req.body.contact.name) {
                accName = req.body.contact.name
            }
            if(accName){
                getAccount(companyId,accName,function(account){
                    res.send(account)
                });
            } else {
                res.send({})
            }
        });
    })
});

router.post('/account/admin/update/hierarchy/path',common.isLoggedInUser,function(req,res){

    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?userProfile.companyId:null;
    req.body.liuEmailId = userProfile.emailId

    accountsObj.updateParentAndPath(req.body,common.castToObjectId(companyId),function (err,result) {
        getAccount(companyId,req.body.name,function(account){
            res.send(account)
        });
    })
});

router.post('/account/admin/relationship/update',common.isLoggedInUser,function(req,res){
    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?userProfile.companyId:null;
    req.body.liuEmailId = userProfile.emailId

    accountsObj.updateRelationship(req.body,common,common.castToObjectId(companyId),function (err,results) {
        getAccount(companyId,req.body.name,function(account){
            res.send(account)
        });
    })
});

function getAccount(companyId,account,callback){
    accountsObj.findTemplate({companyId:common.castToObjectId(companyId),name:account},function (err,account) {
        callback(account)
    });
}

router.post('/account/admin/hierarchy/type/update',common.isLoggedInUser,function(req,res){
    var userProfile = common.getUserFromSession(req)
    var companyId = userProfile?userProfile.companyId:null;
    req.body.liuEmailId = userProfile.emailId

    accountsObj.updateHierarchyType(req.body,common,common.castToObjectId(companyId),function (err,results) {
        getAccount(companyId,req.body.name,function(account){
            res.send(account)
        });
    })
});

router.get('/account/insights/contact', common.isLoggedInUserToGoNext, function (req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var userIdList = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);
    var userProfile = common.getUserFromSession(req);
    var serviceLogin = userProfile?userProfile.serviceLogin:null;
    var companyId = userProfile.companyId?common.castToObjectId(userProfile.companyId):null

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {

        var from = fyRange.start,
            to = fyRange.end;

        async.parallel([
            function (callback) {
                accountsObj.getInteractionsCountForUsers(userIdList,null,null,contacts,serviceLogin,companyId,callback)
            },
            function (callback) {
                getOpportunitiesByAccount(userIdList,contacts,null,null,callback)
            }
        ], function (errors,data) {

            if(!errors){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        interactions:data[0]?data[0]:[],
                        opps:data[1]?data[1]:[]
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":""
                })
            }
        });
    })
});

router.get('/accounts/getAllSystemReferencedAccounts',common.isLoggedInUserOrMobile,function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);

    accountsObj.getAllSystemReferencedManualAccounts(objCompanyId, function (err,accounts) {

        if(!err && accounts){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data:accounts
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data:[]
            })
        }
    })
});

function searchForAccountsAndRespond(req,res,accounts,account) {

    var found = [];

    _.each(accounts,function (ac) {
        if(_.includes(ac.name.toLowerCase(),account.searchText.toLowerCase())){
            found.push(ac)
        }
    });

    res.send({
        SuccessCode: 1,
        ErrorCode: 0,
        Data:found
    })
}

function getInteractionsByDateRange(userIdList,contacts,from,to,serviceLogin,companyId,callback){
    accountsObj.getInteractionsByDateRange(common.castListToObjectIds(userIdList),from,to,contacts,serviceLogin,companyId,function (err,interactions) {
        callback(err,interactions)
    })
}

function getInteractionsByDateRange_new(userIdList,contacts,from,to,serviceLogin,companyId,callback){
    accountsObj.getInteractionsByDateRange_new(common.castListToObjectIds(userIdList),from,to,contacts,serviceLogin,companyId,function (err,interactions) {
        callback(err,interactions)
    })
}

function getNotesByAccount(userIdList,account,from,to,callback){
    messageManagementObj.getNotesForAccount(common.castListToObjectIds(userIdList),from,to,account,function (err,notes) {
        callback(err,notes)
    })
}

function getOpportunitiesByAccount(userIdList,contacts,from,to,callback){
    oppObj.getOpportunitiesByAccount(common.castListToObjectIds(userIdList),from,to,contacts,function (erp,opps) {
        callback(erp,opps)
    })
}

function opportunitiesPastCloseDateByAccount(userIdList,contacts,callback){
    oppObj.opportunitiesPastCloseDateByAccount(common.castListToObjectIds(userIdList),contacts,function (erp,opps) {
        callback(erp,opps)
    })
}

// Done and Tested - Do not touch
router.get('/company/get/secondary/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.castToObjectId(common.getUserId(req.user));
    var shType = req.query.shType;
    var accessControlUserIds = [];
    var oppList = [];
    var regionAccess = null, productAccess = null, verticalAccess = null;
    var resObj = {
        "SuccessCode": 0,
        "Message": "getAllSecondaryHierarchyOpportunityData failed",
        "ErrorCode": 1,
        "Data": [],
        "listOfMembers": [],
        "acccessControlOpps": []
    };

    accessControlUserIds.push(common.castToObjectId(userId))
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        secondaryHierarchyObj.getSecondaryHierarchy(user.companyId, shType, function (rErr,revenueHierarchyData) {
            if(!rErr){
                var findQ = {
                    _id:common.castToObjectId(user.companyId)
                }

                var updateQ = {
                    "isRevenueHierarchySetup":true
                }

                if(!revenueHierarchyData || (revenueHierarchyData && revenueHierarchyData.length == 0)){
                    updateQ = {
                        "isRevenueHierarchySetup":false
                    }
                }

                var key = "userProfile"+String(userId);
                redisClient.del(key);

                userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

                    var listOfMembers = [];

                    _.each(team,function (tm) {
                        _.each(revenueHierarchyData,function (el) {
                            if(el.ownerEmailId == tm.emailId){

                                el._id = tm._id;
                                el.emailId = tm.emailId;
                                el.firstName = tm.firstName;
                                el.lastName = tm.lastName;
                                el.designation = tm.designation;
                            }
                        })
                    })

                    _.each(revenueHierarchyData,function (el) {
                        _.each(revenueHierarchyData,function (hi) {
                            if(_.includes(hi.hierarchyPath,el._id)){
                                listOfMembers.push({
                                    userId:el._id,
                                    userEmailId:el.emailId,
                                    teammate:hi
                                })
                            }
                        })
                    });

                    var data = getChildren(listOfMembers)
                    var tmChildrenObj = data.obj;
                    _.each(revenueHierarchyData,function (el) {
                        el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                    })

                    var resObj = {
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": revenueHierarchyData,
                        "companyMembers":team,
                        "listOfMembers":data.array
                    };
                    res.send(resObj)
                })
            }
            else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers":[],
                    "listOfMembers":[]
                })
            }
        })
    });
});

router.get('/company/get/secondary/hierarchy/team/opportunities', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.castToObjectId(common.getUserId(req.user));
    var shType = req.query.shType;
    var accessControlUserIds = [];
    var oppList = [];
    var listOfMembers = [];

    accessControlUserIds.push(common.castToObjectId(userId));
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        secondaryHierarchyObj.getSecondaryHierarchy(user.companyId,
                                                                    shType,
                                                                    function (rErr,revenueHierarchyData) {
            if(!rErr){
                var findQ = {
                    _id:common.castToObjectId(user.companyId)
                }

                var updateQ = {
                    "isRevenueHierarchySetup":true
                }

                if((revenueHierarchyData && revenueHierarchyData.length > 0)){

                    userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {
                        _.each(team,function (tm) {
                            _.each(revenueHierarchyData,function (el) {
                                if(el.ownerEmailId == tm.emailId){

                                    el._id = tm._id;
                                    el.emailId = tm.emailId;
                                    el.firstName = tm.firstName;
                                    el.lastName = tm.lastName;
                                    el.designation = tm.designation;
                                }
                            })
                        })
                        _.each(revenueHierarchyData,function (el) {

                            _.each(revenueHierarchyData,function (hi) {
                                if(_.includes(hi.hierarchyPath,el._id)){
                                    listOfMembers.push({
                                        userId:el._id,
                                        userEmailId:el.emailId,
                                        teammate:hi
                                    })
                                }
                            })
                        });

                        var data = getChildren(listOfMembers)
                        var tmChildrenObj = data.obj;
                        _.each(revenueHierarchyData,function (el) {
                            el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                        })

                        shTeamUserIds = getSHChildrenIds(revenueHierarchyData, userId);
                        _.each(shTeamUserIds,function (el) {
                            accessControlUserIds.push(el)
                        });

                        oppObj.getOpportunityIdsByUserIds(accessControlUserIds,
                                                            common.castToObjectId(user.companyId),
                                                            function (err,opps) {

                                if (!err && opps.length > 0) {

                                    _.each(opps,function (el) {
                                        oppList.push(el._id)
                                    });

                                    var resObj = {
                                        "SuccessCode": 1,
                                        "Message": "Opportunities exists for Team Members",
                                        "ErrorCode": 0,
                                        "companyMembers":team,
                                        "listOfMembers":data.array,
                                        "listOfMembers1": accessControlUserIds,
                                        "acccessControlOpps": oppList
                                    };

                                    res.send(resObj);
                                } else {
                                    res.send({
                                        "SuccessCode": 0,
                                        "Message": "No Opportunities for Team Members",
                                        "ErrorCode": 1,
                                        "Data": [],
                                        "companyMembers": [],
                                        "listOfMembers": [],
                                        "acccessControlOpps": []

                                    })
                                }
                            })
                    })
                }else {
                    res.send({
                        "SuccessCode": 0,
                        "Message": "No Hierarchy Setup",
                        "ErrorCode": 1,
                        "listOfMembers": [],
                        "acccessControlOpps": []

                    })
                }
            }
        })
    });
});

function getAllSecondaryHierarchyOpportunityData( req, callback){
// router.get('/company/get/secondary/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.castToObjectId(common.getUserId(req.user));
    var shType = req.query.shType;
    var accessControlUserIds = [];
    var oppList = [];
    var regionAccess = null, productAccess = null, verticalAccess = null;
    var resObj = {
        "SuccessCode": 0,
        "Message": "getAllSecondaryHierarchyOpportunityData failed",
        "ErrorCode": 1,
        "Data": [],
        "listOfMembers": [],
        "acccessControlOpps": []
    };

    accessControlUserIds.push(common.castToObjectId(userId))
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        secondaryHierarchyObj.getSecondaryHierarchy(user.companyId, shType, function (rErr,revenueHierarchyData) {
            if(!rErr){
                var findQ = {
                    _id:common.castToObjectId(user.companyId)
                }

                var updateQ = {
                    "isRevenueHierarchySetup":true
                }

                if(!revenueHierarchyData || (revenueHierarchyData && revenueHierarchyData.length == 0)){
                    updateQ = {
                        "isRevenueHierarchySetup":false
                    }
                }

                var key = "userProfile"+String(userId);
                redisClient.del(key);

                userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

                    var listOfMembers = [];

                    _.each(team,function (tm) {
                        _.each(revenueHierarchyData,function (el) {
                            if(el.ownerEmailId == tm.emailId){

                                el._id = tm._id;
                                el.emailId = tm.emailId;
                                el.firstName = tm.firstName;
                                el.lastName = tm.lastName;
                                el.designation = tm.designation;
                            }
                        })
                    })

                    _.each(revenueHierarchyData,function (el) {

                        _.each(revenueHierarchyData,function (hi) {
                            if(_.includes(hi.hierarchyPath,el._id)){
                                listOfMembers.push({
                                    userId:el._id,
                                    userEmailId:el.emailId,
                                    teammate:hi
                                })
                            }
                        })
                    });

                    var data = getChildren(listOfMembers)
                    var tmChildrenObj = data.obj;
                    _.each(revenueHierarchyData,function (el) {
                        el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                    })

                    if ((revenueHierarchyData && revenueHierarchyData.length > 0)){
                        shTeamUserIds = getSHChildrenIds(revenueHierarchyData, userId);
                        _.each(shTeamUserIds,function (el) {
                            accessControlUserIds.push(el)
                        });

                    }

                    oppObj.getOpportunityIdsByUserIds(accessControlUserIds,common.castToObjectId(user.companyId),function (err,opps) {

                        if (!err && opps.length > 0) {

                            _.each(opps,function (el) {
                                oppList.push(el._id)
                            });

                            resObj = {
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data": revenueHierarchyData,
                                "listOfMembers": accessControlUserIds,
                                "acccessControlOpps": oppList
                            };

                            callback(resObj);
                        } else {
                            callback(resObj);
                        }
                    })
                })
            }
        })
    });
}

function getSecondaryHierarchyRMData( objCompanyId, objUserId, shType,  callback){
// router.get('/company/update/access/control/user/opportunities/secondary/hierarchy', common.isLoggedInUserToGoNext, function(req, res){

    var accessControlUserIds = [];
    var oppList = [];
    var isSHHierarchySetup = false;
    var secondaryHierarchyData = null;
    var reportingManagerHierarachyPath = null;
    var hierarachyPath = null;
    var allReportingManagers = [];
    var allTeamMembers = null;
    var allAcccessControlOpps = [];
    var resObj = {
        "SuccessCode": 0,
        "Message": "getSecondaryHierarchyRMData failed",
        "ErrorCode": 1,
        "Data": [],
        "HierarchyPath": [],
        "ReportingManagers":[],
    };

    var secondaryHierarchies = ['Revenue', 'Product','BusinessUnit', 'Regional', 'Vertical'];

    accessControlUserIds.push(objUserId);

    secondaryHierarchyObj.getAllSecondaryHierarchyData(objCompanyId, objUserId, shType, function (err, hierarchyPathData) {
        if (!err && hierarchyPathData.length > 0) {
            var reportingManagerHierarachyPath = hierarchyPathData[0].hierarchyPath;
            if (reportingManagerHierarachyPath != null) {
                var reportingManagers = reportingManagerHierarachyPath.split(",");
                _.each(reportingManagers, function (el) {
                    if (el != '' && el != null) {

                        allReportingManagers.push(el)

                    }
                });
            }
        }
    })

    // _.each(secondaryHierarchies,function (sh) {
    //     isSHHierarchySetup = false;
    //
    //     secondaryHierarchyObj.getAllSecondaryHierarchyData(objCompanyId, objUserId, sh, function (err, hierarchyPathData) {
    //         if (!err && hierarchyPathData.length > 0) {
    //             var reportingManagerHierarachyPath = hierarchyPathData[0].hierarchyPath;
    //             if (reportingManagerHierarachyPath != null) {
    //                 var reportingManagers = reportingManagerHierarachyPath.split(",");
    //                 _.each(reportingManagers, function (el) {
    //                     if (el != '' && el != null) {
    //
    //                         allReportingManagers.push(el)
    //
    //                     }
    //                 });
    //             }
    //         }
    //     })
    // });

    resObj = {
        "SuccessCode": 1,
        "Message": "",
        "ErrorCode": 0,
        "Data": secondaryHierarchyData,
        "HierarchyPath": reportingManagerHierarachyPath,
        "ReportingManagers": allReportingManagers,
    };

    callback(resObj);

}

//Not used to be removed
function updateOpportunitiesForSelfAndReportingManagers( req, callback) {
// router.get('/company/update/access/control/opportunities/org/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var accessControlUserIds = [];
    var objUserId = common.castToObjectId(common.getUserId(req.user));
    var objCompanyId = common.castToObjectId(req.user.companyId);

    var shType = req.query.shType;
    var rmHierarachyPath = null;
    var allReportingManagers = [];
    var allTeamMembers = [];
    var allAcccessControlOpps = [];

    accessControlUserIds.push(objUserId);

    var updateAccessControlOppsData = [{
        companyId: null,
        userId: objUserId,
        allAccessControlOpportunities: [],
        orgHierarchyAccessControlOpportunities: [],
        revenueAccessControlOpportunities: [],
        productAccessControlOpportunities: [],
        businessUnitAccessControlOpportunities: [],
        regionalAccessControlOpportunities: [],
        exceptionalAccessControlOpportunities: []
    }];


    getSecondaryHierarchyRMData(req, function (shHierarchyRMData) {
        if(shHierarchyRMData != null){
            rmHierarachyPath = shHierarchyRMData.HierarchyPath;
            allReportingManagers =  shHierarchyRMData.ReportingManagers;
        }

    })

    getAllSecondaryHierarchyOpportunityData(req, function (shHierarchyTeamData) {
        if(shHierarchyTeamData != null){
            allTeamMembers =  shHierarchyTeamData.listOfMembers;
            allAcccessControlOpps = shHierarchyTeamData.acccessControlOpps;
        }

    })

    common.getProfileOrStoreProfileInSession(userId, req, function (user) {
        userManagementObj.getTeamMembers(common.castToObjectId(user.companyId), function (err, team) {

            if (!err && team) {
                var orgHierarchyData = JSON.parse(JSON.stringify(team));

                orgHierarchyData = orgHierarchyData.filter(function (el) {
                    if (!el.orgHead && el.hierarchyPath && el.hierarchyParent) {
                        return el;
                    } else if (el.orgHead) {
                        return el;
                    }
                })

                var listOfMembers = [];

                _.each(orgHierarchyData, function (el) {
                    _.each(orgHierarchyData, function (hi) {
                        if (_.includes(hi.hierarchyPath, el._id)) {
                            listOfMembers.push({
                                userId: el._id,
                                userEmailId: el.emailId,
                                teammate: hi
                            })
                        }
                    })
                });

                var data = getChildren(listOfMembers)
                var tmChildrenObj = data.obj;
                _.each(orgHierarchyData, function (el) {
                    el.children = tmChildrenObj[el.emailId] ? tmChildrenObj[el.emailId] : null
                })

                if ((orgHierarchyData && orgHierarchyData.length > 0)) {
                    shTeamUserIds = getSHChildrenIds(orgHierarchyData, userId);
                    _.each(shTeamUserIds, function (el) {
                        accessControlUserIds.push(common.castToObjectId(el));
                    });

                }

                oppObj.getOpportunityIdsByUserIds(accessControlUserIds, common.castToObjectId(user.companyId), function (err, opps) {

                    if (!err && opps.length > 0) {

                        _.each(opps, function (el) {
                            oppList.push(el._id)
                        });


                        accessControlOpportunitiesObj.updateAccessControlOpportunitiesForUserAndRMs(common.castToObjectId(user.companyId), accessControlUserIds, oppList, function (err, updatedUsers) {

                            if (!err) {
                                var resObj = {
                                    "SuccessCode": 1,
                                    "Message": "Invoked updateAccessControlOpportunitiesForUserAndRMs",
                                    "ErrorCode": 0,
                                    "Data": orgHierarchyData,
                                    // "HierarchyPath": reportingManagerHierarachyPath,
                                    // "ReportingManagers": accessControlUserIds,
                                    "Update Users": accessControlUserIds

                                };

                                // accessControlOpportunitiesObj.updateAccessControlOpportunitiesOrgHierarchy(common.castToObjectId(user.companyId),
                                //     userId,
                                //     oppList,function (err,response){
                                //         if (!err){
                                //         }
                                //         res.send(resObj);
                                //
                                //  })

                                res.send(resObj);
                            } else {
                                res.send({
                                    "SuccessCode": 0,
                                    "Message": "",
                                    "ErrorCode": 1,
                                    "Data": [],
                                    "companyMembers": [],
                                    "listOfMembers": [],
                                    "acccessControlOpps": []
                                })
                            }

                        })
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers": [],
                    "listOfMembers": [],
                    "acccessControlOpps": []
                })
            }
        })
    });
}

//Done and Tested - Do not touch
router.get('/company/update/access/control/user/opportunities/secondary/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    var objUserId = common.castToObjectId(common.getUserId(req.user));
    var objCompanyId = common.castToObjectId(req.user.companyId);

    var shType = req.query.shType;
    var accessControlUserIds = [];
    var accessControlUserIdObjs = [];
    var allReportingManagers = [];
    var allReportingManagersObjs = [];
    var oppList = [];
    var regionAccess = null, productAccess = null, verticalAccess = null;
    var isSHHierarchySetup = true;
    var rmHierarachyPath = null;
    var allSH = ['Revenue', 'Product','BusinessUnit', 'Region', 'Vertical', 'Solution', 'Type'];
    var allTeamMembers = [];
    var allAcccessControlOpps = [];


    var updateAccessControlData = {companyId: null,
        userIds: [],
        allACOpportunities:[],
        orgHierarchyACOpportunities:[],
        revenueACOpportunities:[],
        productACOpportunities:[],
        businessUnitACOpportunities:[],
        regionACOpportunities:[],
        verticalACOpportunities:[],
        solutionACOpportunities:[],
        typeACOpportunities:[],

        allACReportingManagers:[],
        orgHierarchyACReportingManagers:[],
        revenueHierarchyACReportingManagers:[],
        productHierarchyACReportingManagers:[],
        businessUnitHierarchyACReportingManagers:[],
        regionHierarchyACReportingManagers:[],
        verticalHierarchyACReportingManagers:[],
        solutionHierarchyACReportingManagers:[],
        typeHierarchyACReportingManagers:[],

        allACTeamMembers:[],
        orgHierarchyACTeamMembers:[],
        revenueHierarchyACTeamMembers:[],
        productHierarchyACTeamMembers:[],
        businessUnitHierarchyACTeamMembers:[],
        regionHierarchyACTeamMembers:[],
        verticalHierarchyACTeamMembers:[],
        solutionHierarchyACTeamMembers:[],
        typeHierarchyACTeamMembers:[],
        trigger:"secondaryHierarchyUpdate"

    };

    accessControlUserIds.push(userId);
    accessControlUserIdObjs.push(common.castToObjectId(userId));
    allReportingManagers.push(userId);

    common.getProfileOrStoreProfileInSession(userId,req,function(user) {
        // secondaryHierarchyObj.getAllSecondaryHierarchyDataForUser(common.castToObjectId(user.companyId), common.castToObjectId(userId), shType, function (err, hierarchyPathData) {
        secondaryHierarchyObj.getAllSecondaryHierarchyData(common.castToObjectId(user.companyId),function (err, hierarchyPathData) {
            if (!err && hierarchyPathData.length > 0) {

                userManagementObj.getTeamMembers(common.castToObjectId(user.companyId), function (err, team) {

                    var listOfMembers = [];
                    if(!hierarchyPathData){
                        hierarchyPathData = [user]
                    }
                    _.each(team, function (tm) {
                        _.each(hierarchyPathData, function (el) {
                            if (el.ownerEmailId == tm.emailId) {

                                el._id = tm._id;
                                el.emailId = tm.emailId;
                                el.firstName = tm.firstName;
                                el.lastName = tm.lastName;
                                el.designation = tm.designation;
                            }
                        })
                    })

                    _.each(hierarchyPathData, function (el) {

                        _.each(hierarchyPathData, function (hi) {
                            if (_.includes(hi.hierarchyPath, el._id)) {
                                listOfMembers.push({
                                    userId: el._id,
                                    userEmailId: el.emailId,
                                    teammate: hi
                                })
                            }
                        })
                    });

                    var data = getChildren(listOfMembers)
                    var tmChildrenObj = data.obj;
                    _.each(hierarchyPathData, function (el) {
                        el.children = tmChildrenObj[el.emailId] ? tmChildrenObj[el.emailId] : null
                    })


                    if ((hierarchyPathData && hierarchyPathData.length > 0)) {
                        shTeamUserIds = getSHChildrenIds(hierarchyPathData, userId);
                        _.each(shTeamUserIds, function (el) {
                            accessControlUserIds.push(el.toString());
                            accessControlUserIdObjs.push(el)
                        });

                    }

                    if ((hierarchyPathData && hierarchyPathData.length > 0)) {
                        allReportingManagers = getSHReportingManagers(hierarchyPathData, userId);
                    }


                    oppObj.getOpportunityIdsByUserIds(accessControlUserIdObjs, common.castToObjectId(user.companyId), function (err, opps) {

                        if (!err && opps.length > 0) {

                            _.each(opps, function (el) {
                                oppList.push(el._id.toString())
                            });

                            updateAccessControlData.companyId = common.castToObjectId(user.companyId);
                            updateAccessControlData.userIds.push(userId);
                            updateAccessControlData.allACReportingManagers = allReportingManagers;

                            if(shType == "Revenue"){
                                updateAccessControlData.revenueACOpportunities = oppList;
                                updateAccessControlData.revenueHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.revenueHierarchyACTeamMembers = accessControlUserIds;

                            } else if (shType == "Product"){
                                updateAccessControlData.productACOpportunities = oppList;
                                updateAccessControlData.productHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.productHierarchyACTeamMembers = accessControlUserIds;

                            }else if (shType == 'BusinessUnit'){
                                updateAccessControlData.businessUnitACOpportunities = oppList;
                                updateAccessControlData.businessUnitHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.businessUnitHierarchyACTeamMembers = accessControlUserIds;

                            }else if (shType == 'Region'){
                                updateAccessControlData.regionACOpportunities = oppList;
                                updateAccessControlData.regionHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.regionHierarchyACTeamMembers = accessControlUserIds;

                            }else if (shType == 'Vertical'){
                                updateAccessControlData.verticalACOpportunities = oppList;
                                updateAccessControlData.verticalHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.verticalHierarchyACTeamMembers = accessControlUserIds;

                            }else if (shType == 'Solution'){
                                updateAccessControlData.solutionACOpportunities = oppList;
                                updateAccessControlData.solutionHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.solutionHierarchyACTeamMembers = accessControlUserIds;

                            }else if (shType == 'Type'){
                                updateAccessControlData.typeACOpportunities = oppList;
                                updateAccessControlData.typeHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.typeHierarchyACTeamMembers = accessControlUserIds;
                            }

                            allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.revenueHierarchyACTeamMembers));
                            allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.productHierarchyACTeamMembers));
                            allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.businessUnitHierarchyACTeamMembers));
                            allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.regionHierarchyACTeamMembers));
                            allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.verticalHierarchyACTeamMembers));
                            allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.solutionHierarchyACTeamMembers));
                            allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.typeHierarchyACTeamMembers));
                            allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.revenueHierarchyACTeamMembers));

                            _.each(allTeamMembers, function (el) {
                                if(el != null){
                                    // updateAccessControlData.allACTeamMembers.push(common.castToObjectId(el))
                                    updateAccessControlData.allACTeamMembers.push(el)

                                }
                            });

                            allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.revenueACOpportunities));
                            allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.productACOpportunities));
                            allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.businessUnitACOpportunities));
                            allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.regionACOpportunities));
                            allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.verticalACOpportunities));
                            allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.solutionACOpportunities));
                            allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.typeACOpportunities));


                            _.each(allAcccessControlOpps, function (el) {
                                if(el != null){
                                    // updateAccessControlData.allACOpportunities.push(common.castToObjectId(el))
                                    updateAccessControlData.allACOpportunities.push(el);

                                }
                            });

                            accessControlOpportunitiesObj.updateAccessControlOpportunitiesForUserAndRMs(updateAccessControlData, function (err, result) {

                                if (!result) {
                                    var resObj = {
                                        "SuccessCode": 1,
                                        "Message": "updated updateAccessControlOpportunitiesForUserAndRMs",
                                        "ErrorCode": 0,
                                        "Data": updateAccessControlData,
                                        "HierarchyPath": updateAccessControlData.revenueHierarchyACReportingManagers,
                                        "ReportingManagers": allReportingManagers,
                                        "Team Members": updateAccessControlData.allACTeamMembers

                                    };

                                    res.send(resObj);
                                } else {
                                    res.send({
                                        "SuccessCode": 0,
                                        "Message": "failed updateAccessControlOpportunitiesForUserAndRMs",
                                        "ErrorCode": 1,
                                        "Data": [],
                                        "companyMembers": [],
                                        "listOfMembers": [],
                                        "acccessControlOpps": []
                                    })
                                }

                            })
                        }
                        else {
                            res.send({
                                "SuccessCode": 0,
                                "Message": "No Opportunities",
                                "ErrorCode": 1,
                                "Data": [],
                                "companyMembers": [],
                                "listOfMembers": [],
                                "acccessControlOpps": []
                            })
                        }
                    })
                })
            }else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "No Secondary Hierarchy Set",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers": [],
                    "listOfMembers": [],
                    "acccessControlOpps": []
                })
            }

        })
    })
});

//function to Update the SH Data
function updateAllHierarchyOpportunitiesData( objCompanyId, shType, objUserId, callback){

    var accessControlUserIds = [];
    var accessControlUserIdObjs = [];
    var allReportingManagers = [];
    var allReportingManagersObjs = [];
    var oppList = [];
    var regionAccess = null, productAccess = null, verticalAccess = null;
    var isSHHierarchySetup = true;
    var rmHierarachyPath = null;
    var allSH = ['Revenue', 'Product','BusinessUnit', 'Region', 'Vertical', 'Solution', 'Type'];
    var allTeamMembers = [];
    var allAcccessControlOpps = [];

    var shTeamUserIds = [];
    var resObj = {
        "SuccessCode": 1,
        "Message": "Initial Value",
        "ErrorCode": 0,
        "Data": [],
        "ReportingManagers": [],
        "Team Members": []

    };

    var updateAccessControlData = {companyId: null,
        userIds: [],
        shType:'',
        allACOpportunities:[],
        orgHierarchyACOpportunities:[],
        revenueACOpportunities:[],
        productACOpportunities:[],
        businessUnitACOpportunities:[],
        regionACOpportunities:[],
        verticalACOpportunities:[],
        solutionACOpportunities:[],
        typeACOpportunities:[],

        allACReportingManagers:[],
        orgHierarchyACReportingManagers:[],
        revenueHierarchyACReportingManagers:[],
        productHierarchyACReportingManagers:[],
        businessUnitHierarchyACReportingManagers:[],
        regionHierarchyACReportingManagers:[],
        verticalHierarchyACReportingManagers:[],
        solutionHierarchyACReportingManagers:[],
        typeHierarchyACReportingManagers:[],

        allACTeamMembers:[],
        orgHierarchyACTeamMembers:[],
        revenueHierarchyACTeamMembers:[],
        productHierarchyACTeamMembers:[],
        businessUnitHierarchyACTeamMembers:[],
        regionHierarchyACTeamMembers:[],
        verticalHierarchyACTeamMembers:[],
        solutionHierarchyACTeamMembers:[],
        typeHierarchyACTeamMembers:[],
        trigger:"secondaryHierarchyUpdate"

    };

    // _.each(allSH, function(shType){

        accessControlUserIds = [];
        accessControlUserIdObjs = [];
        allReportingManagers = [];
        oppList = [];
        shTeamUserIds = [];

        accessControlUserIds.push(objUserId.toString());
        accessControlUserIdObjs.push(common.castToObjectId(objUserId));
        allReportingManagers.push(objUserId);

        updateAccessControlData.revenueACOpportunities = [];
        updateAccessControlData.revenueHierarchyACReportingManagers = [];
        updateAccessControlData.revenueHierarchyACTeamMembers = [];
        updateAccessControlData.productACOpportunities = [];
        updateAccessControlData.productHierarchyACReportingManagers = [];
        updateAccessControlData.productHierarchyACTeamMembers = [];
        updateAccessControlData.businessUnitACOpportunities = [];
        updateAccessControlData.businessUnitHierarchyACReportingManagers = [];
        updateAccessControlData.businessUnitHierarchyACTeamMembers = [];
        updateAccessControlData.regionACOpportunities = [];
        updateAccessControlData.regionHierarchyACReportingManagers = [];
        updateAccessControlData.regionHierarchyACTeamMembers = [];
        updateAccessControlData.verticalACOpportunities = [];
        updateAccessControlData.verticalHierarchyACReportingManagers = [];
        updateAccessControlData.verticalHierarchyACTeamMembers = [];
        updateAccessControlData.solutionACOpportunities = [];
        updateAccessControlData.solutionHierarchyACReportingManagers = [];
        updateAccessControlData.solutionHierarchyACTeamMembers = [];
        updateAccessControlData.typeACOpportunities = [];
        updateAccessControlData.typeHierarchyACReportingManagers = [];
        updateAccessControlData.typeHierarchyACTeamMembers = [];

        secondaryHierarchyObj.getAllSecondaryHierarchyDataForType(objCompanyId, shType, function (err, hierarchyPathData) {
            if (!err && hierarchyPathData.length > 0) {


                userManagementObj.getTeamMembers(objCompanyId, function (err, team) {

                    var listOfMembers = [];
                    if(!hierarchyPathData){
                        hierarchyPathData = [user]
                    }
                    _.each(team, function (tm) {
                        _.each(hierarchyPathData, function (el) {
                            if (el.ownerEmailId == tm.emailId) {

                                el._id = tm._id;
                                el.emailId = tm.emailId;
                                el.firstName = tm.firstName;
                                el.lastName = tm.lastName;
                                el.designation = tm.designation;
                            }
                        })
                    })

                    _.each(hierarchyPathData, function (el) {
                        _.each(hierarchyPathData, function (hi) {
                            if (_.includes(hi.hierarchyPath, el._id)) {
                                listOfMembers.push({
                                    userId: el._id,
                                    userEmailId: el.emailId,
                                    teammate: hi
                                })
                            }
                        })
                    });

                    var data = getChildren(listOfMembers)
                    var tmChildrenObj = data.obj;
                    _.each(hierarchyPathData, function (el) {
                        el.children = tmChildrenObj[el.emailId] ? tmChildrenObj[el.emailId] : null
                    })


                    if ((hierarchyPathData && hierarchyPathData.length > 0)) {
                        shTeamUserIds = getSHChildrenIds(hierarchyPathData, objUserId);
                        _.each(shTeamUserIds, function (el) {
                            accessControlUserIds.push(el.toString());
                            accessControlUserIdObjs.push(el)
                        });

                    }

                    if ((hierarchyPathData && hierarchyPathData.length > 0)) {
                        allReportingManagers = getSHReportingManagers(hierarchyPathData, objUserId);
                    }


                    oppObj.getOpportunityIdsByUserIds(accessControlUserIdObjs, objCompanyId, function (err, opps) {

                        if (!err && opps.length > 0) {

                            _.each(opps, function (el) {
                                oppList.push(el._id.toString())
                            });

                            updateAccessControlData.companyId = objCompanyId;
                            updateAccessControlData.userIds.push(objUserId);
                            updateAccessControlData.shType = shType;
                            updateAccessControlData.allACReportingManagers = allReportingManagers;

                            if(shType == 'Revenue'){
                                updateAccessControlData.revenueACOpportunities = oppList;
                                updateAccessControlData.revenueHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.revenueHierarchyACTeamMembers = accessControlUserIds;

                            } else if (shType == 'Product'){
                                updateAccessControlData.productACOpportunities = oppList;
                                updateAccessControlData.productHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.productHierarchyACTeamMembers = accessControlUserIds;

                            }else if (shType == 'BusinessUnit'){
                                updateAccessControlData.businessUnitACOpportunities = oppList;
                                updateAccessControlData.businessUnitHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.businessUnitHierarchyACTeamMembers = accessControlUserIds;

                            }else if (shType == 'Region'){
                                updateAccessControlData.regionACOpportunities = oppList;
                                updateAccessControlData.regionHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.regionHierarchyACTeamMembers = accessControlUserIds;

                            }else if (shType == 'Vertical'){
                                updateAccessControlData.verticalACOpportunities = oppList;
                                updateAccessControlData.verticalHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.verticalHierarchyACTeamMembers = accessControlUserIds;

                            }else if (shType == 'Solution'){
                                updateAccessControlData.solutionACOpportunities = oppList;
                                updateAccessControlData.solutionHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.solutionHierarchyACTeamMembers = accessControlUserIds;

                            }else if (shType == 'Type'){
                                updateAccessControlData.typeACOpportunities = oppList;
                                updateAccessControlData.typeHierarchyACReportingManagers = allReportingManagers;
                                updateAccessControlData.typeHierarchyACTeamMembers = accessControlUserIds;
                            }

                            // allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.revenueHierarchyACTeamMembers));
                            // allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.productHierarchyACTeamMembers));
                            // allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.businessUnitHierarchyACTeamMembers));
                            // allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.regionHierarchyACTeamMembers));
                            // allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.verticalHierarchyACTeamMembers));
                            // allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.solutionHierarchyACTeamMembers));
                            // allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.typeHierarchyACTeamMembers));
                            // allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.revenueHierarchyACTeamMembers));
                            //
                            // _.each(allTeamMembers, function (el) {
                            //     if(el != null){
                            //         // updateAccessControlData.allACTeamMembers.push(common.castToObjectId(el))
                            //         updateAccessControlData.allACTeamMembers.push(el)
                            //
                            //     }
                            // });
                            //
                            // allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.revenueACOpportunities));
                            // allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.productACOpportunities));
                            // allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.businessUnitACOpportunities));
                            // allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.regionACOpportunities));
                            // allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.verticalACOpportunities));
                            // allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.solutionACOpportunities));
                            // allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.typeACOpportunities));
                            //
                            //
                            // _.each(allAcccessControlOpps, function (el) {
                            //     if(el != null){
                            //         // updateAccessControlData.allACOpportunities.push(common.castToObjectId(el))
                            //         updateAccessControlData.allACOpportunities.push(el);
                            //
                            //     }
                            // });


                            accessControlOpportunitiesObj.updateAccessControlOpportunitiesForUserSHType(updateAccessControlData, shType, function (err, result) {

                                if (!result) {
                                    resObj = {
                                        "SuccessCode": 1,
                                        "Message": "updated updateAccessControlOpportunitiesForUserAndRMs",
                                        "ErrorCode": 0,
                                        "Data": updateAccessControlData,
                                        "ReportingManagers": allReportingManagers,
                                        "Team Members": updateAccessControlData.allACTeamMembers

                                    };

                                    // callback(resObj);
                                } else {
                                    resObj = {
                                        "SuccessCode": 0,
                                        "Message": "failed updateAccessControlOpportunitiesForUserAndRMs",
                                        "ErrorCode": 1,
                                        "Data": [],
                                        "companyMembers": [],
                                        "listOfMembers": [],
                                        "acccessControlOpps": []
                                    }
                                }

                            })
                        }
                        else {
                            resObj = {
                                "SuccessCode": 0,
                                "Message": "No Opportunities",
                                "ErrorCode": 1,
                                "Data": [],
                                "companyMembers": [],
                                "listOfMembers": [],
                                "acccessControlOpps": []
                            }
                        }
                    })
                })
            }else {
                resObj = {
                    "SuccessCode": 0,
                    "Message": "No Secondary Hierarchy Set",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers": [],
                    "listOfMembers": [],
                    "acccessControlOpps": []
                }
            }

        })

    // })

    accessControlOpportunitiesObj.getAllAccessControlData(objCompanyId, objUserId, function (err, result) {
        if(!err && result.length > 0){
            callback(result);

        }else{
            callback(updateAccessControlData);
        }
    })
}

//Testing in Progress
router.get('/company/update/all/access/control/user/opportunities/secondary/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    var objUserId = common.castToObjectId(common.getUserId(req.user));
    var objCompanyId = common.castToObjectId(req.user.companyId);

    var shType = req.query.shType;
    var accessControlUserIds = [];
    var accessControlUserIdObjs = [];
    var allReportingManagers = [];
    var allReportingManagersObjs = [];
    var oppList = [];
    var regionAccess = null, productAccess = null, verticalAccess = null;
    var isSHHierarchySetup = true;
    var rmHierarachyPath = null;
    var allSH = ['Revenue', 'Product','BusinessUnit', 'Region', 'Vertical', 'Solution', 'Type'];
    var allTeamMembers = [];
    var allAcccessControlOpps = [];
    var shType = req.query.shType;

    var updateAccessControlData = {companyId: null,
        userIds: [],
        allACOpportunities:[],
        orgHierarchyACOpportunities:[],
        revenueACOpportunities:[],
        productACOpportunities:[],
        businessUnitACOpportunities:[],
        regionACOpportunities:[],
        verticalACOpportunities:[],
        solutionACOpportunities:[],
        typeACOpportunities:[],

        allACReportingManagers:[],
        orgHierarchyACReportingManagers:[],
        revenueHierarchyACReportingManagers:[],
        productHierarchyACReportingManagers:[],
        businessUnitHierarchyACReportingManagers:[],
        regionHierarchyACReportingManagers:[],
        verticalHierarchyACReportingManagers:[],
        solutionHierarchyACReportingManagers:[],
        typeHierarchyACReportingManagers:[],

        allACTeamMembers:[],
        orgHierarchyACTeamMembers:[],
        revenueHierarchyACTeamMembers:[],
        productHierarchyACTeamMembers:[],
        businessUnitHierarchyACTeamMembers:[],
        regionHierarchyACTeamMembers:[],
        verticalHierarchyACTeamMembers:[],
        solutionHierarchyACTeamMembers:[],
        typeHierarchyACTeamMembers:[],
        trigger:"secondaryHierarchyUpdate"

    };

    accessControlUserIds.push(userId);
    accessControlUserIdObjs.push(common.castToObjectId(userId));
    allReportingManagers.push(userId);

    common.getProfileOrStoreProfileInSession(userId,req,function(user) {
        // _.each(allSH, function(shType) {
            updateAllHierarchyOpportunitiesData(common.castToObjectId(user.companyId), shType, common.castToObjectId(userId), function (response) {
                res.send(response);
            })
        // })
        // secondaryHierarchyObj.getAllSecondaryHierarchyDataForUser(common.castToObjectId(user.companyId), common.castToObjectId(userId), shType, function (err, hierarchyPathData) {
        // secondaryHierarchyObj.getAllSecondaryHierarchyData(common.castToObjectId(user.companyId),function (err, hierarchyPathData) {
        //     if (!err && hierarchyPathData.length > 0) {
        //
        //
        //         userManagementObj.getTeamMembers(common.castToObjectId(user.companyId), function (err, team) {
        //
        //             var listOfMembers = [];
        //             if(!hierarchyPathData){
        //                 hierarchyPathData = [user]
        //             }
        //             _.each(team, function (tm) {
        //                 _.each(hierarchyPathData, function (el) {
        //                     if (el.ownerEmailId == tm.emailId) {
        //
        //                         el._id = tm._id;
        //                         el.emailId = tm.emailId;
        //                         el.firstName = tm.firstName;
        //                         el.lastName = tm.lastName;
        //                         el.designation = tm.designation;
        //                     }
        //                 })
        //             })
        //
        //             _.each(hierarchyPathData, function (el) {
        //
        //                 _.each(hierarchyPathData, function (hi) {
        //                     if (_.includes(hi.hierarchyPath, el._id)) {
        //                         listOfMembers.push({
        //                             userId: el._id,
        //                             userEmailId: el.emailId,
        //                             teammate: hi
        //                         })
        //                     }
        //                 })
        //             });
        //
        //             var data = getChildren(listOfMembers)
        //             var tmChildrenObj = data.obj;
        //             _.each(hierarchyPathData, function (el) {
        //                 el.children = tmChildrenObj[el.emailId] ? tmChildrenObj[el.emailId] : null
        //             })
        //
        //
        //             if ((hierarchyPathData && hierarchyPathData.length > 0)) {
        //                 shTeamUserIds = getSHChildrenIds(hierarchyPathData, userId);
        //                 _.each(shTeamUserIds, function (el) {
        //                     accessControlUserIds.push(el.toString());
        //                     accessControlUserIdObjs.push(el)
        //                 });
        //
        //             }
        //
        //             if ((hierarchyPathData && hierarchyPathData.length > 0)) {
        //                 allReportingManagers = getSHReportingManagers(hierarchyPathData, userId);
        //             }
        //
        //
        //             oppObj.getOpportunityIdsByUserIds(accessControlUserIdObjs, common.castToObjectId(user.companyId), function (err, opps) {
        //
        //                 if (!err && opps.length > 0) {
        //
        //                     _.each(opps, function (el) {
        //                         oppList.push(el._id.toString())
        //                     });
        //
        //                     updateAccessControlData.companyId = common.castToObjectId(user.companyId);
        //                     updateAccessControlData.userIds.push(userId);
        //                     updateAccessControlData.allACReportingManagers = allReportingManagers;
        //
        //                     if(shType == "Revenue"){
        //                         updateAccessControlData.revenueACOpportunities = oppList;
        //                         updateAccessControlData.revenueHierarchyACReportingManagers = allReportingManagers;
        //                         updateAccessControlData.revenueHierarchyACTeamMembers = accessControlUserIds;
        //
        //                     } else if (shType == "Product"){
        //                         updateAccessControlData.productACOpportunities = oppList;
        //                         updateAccessControlData.productHierarchyACReportingManagers = allReportingManagers;
        //                         updateAccessControlData.productHierarchyACTeamMembers = accessControlUserIds;
        //
        //                     }else if (shType == 'BusinessUnit'){
        //                         updateAccessControlData.businessUnitACOpportunities = oppList;
        //                         updateAccessControlData.businessUnitHierarchyACReportingManagers = allReportingManagers;
        //                         updateAccessControlData.businessUnitHierarchyACTeamMembers = accessControlUserIds;
        //
        //                     }else if (shType == 'Region'){
        //                         updateAccessControlData.regionACOpportunities = oppList;
        //                         updateAccessControlData.regionHierarchyACReportingManagers = allReportingManagers;
        //                         updateAccessControlData.regionHierarchyACTeamMembers = accessControlUserIds;
        //
        //                     }else if (shType == 'Vertical'){
        //                         updateAccessControlData.verticalACOpportunities = oppList;
        //                         updateAccessControlData.verticalHierarchyACReportingManagers = allReportingManagers;
        //                         updateAccessControlData.verticalHierarchyACTeamMembers = accessControlUserIds;
        //
        //                     }else if (shType == 'Solution'){
        //                         updateAccessControlData.solutionACOpportunities = oppList;
        //                         updateAccessControlData.solutionHierarchyACReportingManagers = allReportingManagers;
        //                         updateAccessControlData.solutionHierarchyACTeamMembers = accessControlUserIds;
        //
        //                     }else if (shType == 'Type'){
        //                         updateAccessControlData.typeACOpportunities = oppList;
        //                         updateAccessControlData.typeHierarchyACReportingManagers = allReportingManagers;
        //                         updateAccessControlData.typeHierarchyACTeamMembers = accessControlUserIds;
        //                     }
        //
        //                     allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.revenueHierarchyACTeamMembers));
        //                     allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.productHierarchyACTeamMembers));
        //                     allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.businessUnitHierarchyACTeamMembers));
        //                     allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.regionHierarchyACTeamMembers));
        //                     allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.verticalHierarchyACTeamMembers));
        //                     allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.solutionHierarchyACTeamMembers));
        //                     allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.typeHierarchyACTeamMembers));
        //                     allTeamMembers =   _.uniq(allTeamMembers.concat(updateAccessControlData.revenueHierarchyACTeamMembers));
        //
        //                     _.each(allTeamMembers, function (el) {
        //                         if(el != null){
        //                             // updateAccessControlData.allACTeamMembers.push(common.castToObjectId(el))
        //                             updateAccessControlData.allACTeamMembers.push(el)
        //
        //                         }
        //                     });
        //
        //                     allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.revenueACOpportunities));
        //                     allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.productACOpportunities));
        //                     allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.businessUnitACOpportunities));
        //                     allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.regionACOpportunities));
        //                     allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.verticalACOpportunities));
        //                     allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.solutionACOpportunities));
        //                     allAcccessControlOpps =   _.uniq(allAcccessControlOpps.concat(updateAccessControlData.typeACOpportunities));
        //
        //
        //                     _.each(allAcccessControlOpps, function (el) {
        //                         if(el != null){
        //                             // updateAccessControlData.allACOpportunities.push(common.castToObjectId(el))
        //                             updateAccessControlData.allACOpportunities.push(el);
        //
        //                         }
        //                     });
        //
        //                     accessControlOpportunitiesObj.updateAccessControlOpportunitiesForUserAndRMs(updateAccessControlData, function (err, result) {
        //
        //                         if (!result) {
        //                             var resObj = {
        //                                 "SuccessCode": 1,
        //                                 "Message": "updated updateAccessControlOpportunitiesForUserAndRMs",
        //                                 "ErrorCode": 0,
        //                                 "Data": updateAccessControlData,
        //                                 "HierarchyPath": updateAccessControlData.revenueHierarchyACReportingManagers,
        //                                 "ReportingManagers": allReportingManagers,
        //                                 "Team Members": updateAccessControlData.allACTeamMembers
        //
        //                             };
        //
        //                             res.send(resObj);
        //                         } else {
        //                             res.send({
        //                                 "SuccessCode": 0,
        //                                 "Message": "failed updateAccessControlOpportunitiesForUserAndRMs",
        //                                 "ErrorCode": 1,
        //                                 "Data": [],
        //                                 "companyMembers": [],
        //                                 "listOfMembers": [],
        //                                 "acccessControlOpps": []
        //                             })
        //                         }
        //
        //                     })
        //                 }
        //                 else {
        //                     res.send({
        //                         "SuccessCode": 0,
        //                         "Message": "No Opportunities",
        //                         "ErrorCode": 1,
        //                         "Data": [],
        //                         "companyMembers": [],
        //                         "listOfMembers": [],
        //                         "acccessControlOpps": []
        //                     })
        //                 }
        //             })
        //         })
        //     }else {
        //         res.send({
        //             "SuccessCode": 0,
        //             "Message": "No Secondary Hierarchy Set",
        //             "ErrorCode": 1,
        //             "Data": [],
        //             "companyMembers": [],
        //             "listOfMembers": [],
        //             "acccessControlOpps": []
        //         })
        //     }
        //
        // })
    })
});

//Done and Tested - Do not touch
router.get('/company/update/access/control/opportunities/org/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    var accessControlUserIds = [];
    accessControlUserIds.push(common.castToObjectId(userId));
    var oppList = [];

    var updateAccessControlData = {companyId: null,
        userId: null,
        allACOpportunities:[],
        orgHierarchyACOpportunities:[],
        revenueACOpportunities:[],
        productACOpportunities:[],
        businessUnitACOpportunities:[],
        regionalACOpportunities:[],
        solutionACOpportunities:[],
        typeACOpportunities:[],
        trigger:"orgHierarchyUpdate"

    };

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

            if(!err && team){
                var orgHierarchyData = JSON.parse(JSON.stringify(team));

                orgHierarchyData = orgHierarchyData.filter(function (el) {
                    if(!el.orgHead && el.hierarchyPath && el.hierarchyParent){
                        return el;
                    } else if(el.orgHead){
                        return el;
                    }
                })

                var listOfMembers = [];

                _.each(orgHierarchyData,function (el) {
                    _.each(orgHierarchyData,function (hi) {
                        if(_.includes(hi.hierarchyPath,el._id)){
                            listOfMembers.push({
                                userId:el._id,
                                userEmailId:el.emailId,
                                teammate:hi
                            })
                        }
                    })
                });

                var data = getChildren(listOfMembers)
                var tmChildrenObj = data.obj;
                _.each(orgHierarchyData,function (el) {
                    el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                })

                if ((orgHierarchyData && orgHierarchyData.length > 0)){
                    shTeamUserIds = getSHChildrenIds(orgHierarchyData, userId);
                    _.each(shTeamUserIds,function (el) {
                        accessControlUserIds.push(el);
                    });

                }

                oppObj.getOpportunityIdsByUserIds(accessControlUserIds,common.castToObjectId(user.companyId),function (err,opps) {

                    if (!err && opps.length > 0) {

                        _.each(opps, function (el) {
                            oppList.push(el._id.toString())
                        });

                        updateAccessControlData.companyId = common.castToObjectId(user.companyId);
                        updateAccessControlData.userIds = accessControlUserIds;
                        updateAccessControlData.orgHierarchyACOpportunities = oppList;

                        accessControlOpportunitiesObj.updateAccessControlOpportunitiesForUserAndRMs(updateAccessControlData, function (err, updatedUsers) {

                            if(!err){
                                var resObj = {
                                    "SuccessCode": 1,
                                    "Message": "Invoked updateAccessControlOpportunitiesForUserAndRMs",
                                    "ErrorCode": 0,
                                    "Data": orgHierarchyData,
                                    // "HierarchyPath": reportingManagerHierarachyPath,
                                    // "ReportingManagers": accessControlUserIds,
                                    "Update Users": accessControlUserIds

                                };

                                // accessControlOpportunitiesObj.updateAccessControlOpportunitiesOrgHierarchy(common.castToObjectId(user.companyId),
                                //     userId,
                                //     oppList,function (err,response){
                                //         if (!err){
                                //         }
                                //         res.send(resObj);
                                //
                                //  })

                                res.send(resObj);
                            }else {
                                res.send({
                                    "SuccessCode": 0,
                                    "Message": "",
                                    "ErrorCode": 1,
                                    "Data": [],
                                    "companyMembers":[],
                                    "listOfMembers":[],
                                    "acccessControlOpps": []
                                })
                            }

                        })
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers":[],
                    "listOfMembers":[],
                    "acccessControlOpps": []
                })
            }
        })
    });
});

router.get('/company/update/access/control/opportunities/org/hierarchy', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    var accessControlUserIds = [];
    accessControlUserIds.push(common.castToObjectId(userId));
    var oppList = [];


    var updateAccessContrlOppsData = [{companyId: null,
        userId: null,
        allAccessControlOpportunities:[],
        orgHierarchyAccessControlOpportunities:[],
        revenueAccessControlOpportunities:[],
        productAccessControlOpportunities:[],
        businessUnitAccessControlOpportunities:[],
        regionalAccessControlOpportunities:[],
        exceptionalAccessControlOpportunities:[]
    }];

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

            if(!err && team){
                var orgHierarchyData = JSON.parse(JSON.stringify(team));

                orgHierarchyData = orgHierarchyData.filter(function (el) {
                    if(!el.orgHead && el.hierarchyPath && el.hierarchyParent){
                        return el;
                    } else if(el.orgHead){
                        return el;
                    }
                })

                var listOfMembers = [];

                _.each(orgHierarchyData,function (el) {
                    _.each(orgHierarchyData,function (hi) {
                        if(_.includes(hi.hierarchyPath,el._id)){
                            listOfMembers.push({
                                userId:el._id,
                                userEmailId:el.emailId,
                                teammate:hi
                            })
                        }
                    })
                });

                var data = getChildren(listOfMembers)
                var tmChildrenObj = data.obj;
                _.each(orgHierarchyData,function (el) {
                    el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                })

                if ((orgHierarchyData && orgHierarchyData.length > 0)){
                    shTeamUserIds = getSHChildrenIds(orgHierarchyData, userId);
                    _.each(shTeamUserIds,function (el) {
                        accessControlUserIds.push(common.castToObjectId(el));
                    });

                }

                oppObj.getOpportunityIdsByUserIds(accessControlUserIds,common.castToObjectId(user.companyId),function (err,opps) {

                    if (!err && opps.length > 0) {

                        _.each(opps, function (el) {
                            oppList.push(el._id)
                        });


                        accessControlOpportunitiesObj.updateAccessControlOpportunitiesForUserAndRMs(common.castToObjectId(user.companyId), accessControlUserIds, oppList, function (err, updatedUsers) {

                            if(!err){
                                var resObj = {
                                    "SuccessCode": 1,
                                    "Message": "Invoked updateAccessControlOpportunitiesForUserAndRMs",
                                    "ErrorCode": 0,
                                    "Data": orgHierarchyData,
                                    // "HierarchyPath": reportingManagerHierarachyPath,
                                    // "ReportingManagers": accessControlUserIds,
                                    "Update Users": accessControlUserIds

                                };

                                // accessControlOpportunitiesObj.updateAccessControlOpportunitiesOrgHierarchy(common.castToObjectId(user.companyId),
                                //     userId,
                                //     oppList,function (err,response){
                                //         if (!err){
                                //         }
                                //         res.send(resObj);
                                //
                                //  })

                                res.send(resObj);
                            }else {
                                res.send({
                                    "SuccessCode": 0,
                                    "Message": "",
                                    "ErrorCode": 1,
                                    "Data": [],
                                    "companyMembers":[],
                                    "listOfMembers":[],
                                    "acccessControlOpps": []
                                })
                            }

                        })
                    }
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers":[],
                    "listOfMembers":[],
                    "acccessControlOpps": []
                })
            }
        })
    });
});

router.get('/company/get/all/access/control/opportunities', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    var accessControlUserIds = [];
    accessControlUserIds.push(common.castToObjectId(userId));
    var oppList = [];


    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        userManagementObj.getTeamMembers(common.castToObjectId(user.companyId),function (err,team) {

            if(!err && team){
                var orgHierarchyData = JSON.parse(JSON.stringify(team));

                orgHierarchyData = orgHierarchyData.filter(function (el) {
                    if(!el.orgHead && el.hierarchyPath && el.hierarchyParent){
                        return el;
                    } else if(el.orgHead){
                        return el;
                    }
                })

                var listOfMembers = [];

                _.each(orgHierarchyData,function (el) {
                    _.each(orgHierarchyData,function (hi) {
                        if(_.includes(hi.hierarchyPath,el._id)){
                            listOfMembers.push({
                                userId:el._id,
                                userEmailId:el.emailId,
                                teammate:hi
                            })
                        }
                    })
                });

                var data = getChildren(listOfMembers)
                var tmChildrenObj = data.obj;
                _.each(orgHierarchyData,function (el) {
                    el.children = tmChildrenObj[el.emailId]?tmChildrenObj[el.emailId]:null
                })

                if ((orgHierarchyData && orgHierarchyData.length > 0)){
                    shTeamUserIds = getSHChildrenIds(orgHierarchyData, userId);
                    _.each(shTeamUserIds,function (el) {
                        accessControlUserIds.push(common.castToObjectId(el));
                    });

                }

                oppObj.getOpportunityIdsByUserIds(accessControlUserIds,common.castToObjectId(user.companyId),function (err,opps) {

                    if (!err && opps.length > 0) {

                        _.each(opps,function (el) {
                            oppList.push(el._id)
                        });

                        accessControlOpportunitiesObj.getAllAccessControlOpportunities(common.castToObjectId(user.companyId),
                            userId, function (err,allACOpps){
                                if (!err){
                                    var resObj = {
                                        "SuccessCode": 1,
                                        "Message": "",
                                        "ErrorCode": 0,
                                        "Data": orgHierarchyData,
                                        "companyMembers": team,
                                        "listOfMembers": data.array,
                                        "acccessControlOpps": oppList,
                                        "allACOpps":allACOpps
                                    };

                                    res.send(resObj);
                                }

                            })

                    } else {
                        res.send({
                            "SuccessCode": 0,
                            "Message": "",
                            "ErrorCode": 1,
                            "Data": [],
                            "companyMembers": [],
                            "listOfMembers": [],
                            "acccessControlOpps": []

                        })
                    }
                })

            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data": [],
                    "companyMembers":[],
                    "listOfMembers":[],
                    "acccessControlOpps": []
                })
            }
        })
    });
});

router.get('/company/users/for/hierarchy', common.isLoggedInUserToGoNext, function(req, res){

    var userId = common.getUserId(req.user);
    var hierarchyType = req.query.hierarchyType;

    userId = common.castToObjectId(userId);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        var companyId = common.castToObjectId(user.companyId);
        var timezone;
        if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
            timezone = user.timezone.name;
        }else timezone = 'UTC';

        //For All access
        //1. get all hierarchies+orgHier
        //2. merge all hierarchiesPath
        //3. then build respective children and add them.

        async.parallel([
            function (callback) {
                accessControlOpportunitiesObj.getAccessControlDataForUserIdsForSHType(
                    companyId
                    ,hierarchyType
                    ,userId
                    ,callback);
            },
            function (callback) {
                if (hierarchyType === 'All_Access') {
                    userManagementObj.getUserHierarchy(common.castToObjectId(userId), function (err_o,orgHier) {
                        callback(err_o,orgHier);
                    })
                } else {
                    callback(null,[])
                }
            },
            function (callback) {
                userManagementObj.getTeamMembers(companyId,callback);
            }
        ], function (errors,data) {

            if(!errors && data){
                appendCommitsTargetsChildren(data[0],data[2],data[1],req.query.forCommits,companyId,timezone,req.query.range,userId,hierarchyType,function (listOfMembers,team) {
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data:team,
                        listOfMembers:listOfMembers,
                        companyMembers:data[2]
                    })
                })
            } else {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                    Data:[]
                })
            }
        });
    });
});

function appendCommitsTargetsChildren(team,allCompany,myOrgHr,forCommits,companyId,timezone,range,userId,hierarchyType,callback){

    var listOfMembers = [],
        copyOfTeam = [],
        allCompanyObj = {};

    _.each(allCompany,function (el) {
        el._id = String(el._id);
        allCompanyObj[String(el._id)] = el;
    });

    if (hierarchyType === 'All_Access') {
        var teamObj = {};
        _.each(team,function (tm) {
            teamObj[String(tm.userId)] = tm;
        });

        if(myOrgHr && myOrgHr.length>0){
            _.each(myOrgHr,function (el) {
                copyOfTeam.push(el);
                _.each(myOrgHr,function (hi) {
                    if(_.includes(hi.hierarchyPath,el._id)){
                        listOfMembers.push({
                            userId:el._id,
                            userEmailId:el.emailId,
                            teammate:hi
                        })
                    }
                });

                if(teamObj[String(el._id)]){
                    if(teamObj[String(el._id)].teamMembers && teamObj[String(el._id)].teamMembers.length>0){
                        copyOfTeam.push(allCompanyObj[el._id]);
                        _.each(teamObj[String(el._id)].teamMembers,function (tm) {
                            if(tm){
                                tm = String(tm);
                                listOfMembers.push({
                                    userId:el._id,
                                    userEmailId:el.emailId,
                                    teammate:allCompanyObj[tm]
                                });
                            }
                        })
                    }
                }
            });
        };

        var userIds = [];
        _.each(copyOfTeam,function (tm) {
            userIds.push(common.castToObjectId(tm._id))
        })

        secondaryHierarchyObj.getAllChildren(userIds, common.castToObjectId(companyId), function(err_r, s_data){

            if(!err_r && s_data && s_data.length>0){
                listOfMembers = listOfMembers.concat(s_data);
            }

            listOfMembers = _
                .chain(listOfMembers)
                .groupBy(function (el) {
                    return String(el.userId);
                })
                .map(function(values, key) {

                    var teamMatesEmailId = [],
                        teamMatesUserId = [];

                    _.each(values,function (va) {
                        if(va.type == "secondary"){
                            teamMatesEmailId.push(va.teammate.ownerEmailId)
                            teamMatesUserId.push(va.teammate.userId)
                            copyOfTeam.push(allCompanyObj[String(va.teammate.userId)])
                        } else {
                            teamMatesEmailId.push(va.teammate.emailId)
                            teamMatesUserId.push(va.teammate._id)
                            copyOfTeam.push(allCompanyObj[String(va.teammate._id)])
                        }
                    });

                    return {
                        userEmailId:allCompanyObj[key].emailId,
                        teamMatesEmailId:_.uniq(teamMatesEmailId),
                        teamMatesUserId:_.uniq(teamMatesUserId)
                    }
                })
                .value();

            copyOfTeam = _.compact(_.uniq(copyOfTeam,"emailId"));

            if(forCommits){
                getTargetsAndAchievementsForCommit(copyOfTeam,companyId,timezone,range,function (err,targetsAchievementCommits) {
                    callback(listOfMembers,targetsAchievementCommits)
                });
            } else {
                callback(listOfMembers,copyOfTeam)
            }
        });

    } else {

        if(myOrgHr && myOrgHr.length>0){
            _.each(myOrgHr,function (el) {
                copyOfTeam.push(el);
                _.each(myOrgHr,function (hi) {
                    if(_.includes(hi.hierarchyPath,el._id)){
                        listOfMembers.push({
                            userId:el._id,
                            userEmailId:el.emailId,
                            teammate:hi
                        })
                    }
                })
            });
        };

        if(team && team.teamMembers && team.teamMembers.length>0){
            _.each(team.teamMembers,function (el) {
                el = String(el);

                if(allCompanyObj[el]){
                    copyOfTeam.push(allCompanyObj[el]);
                    listOfMembers.push({
                        userId:userId,
                        userEmailId:allCompanyObj[userId].emailId,
                        teammate:allCompanyObj[el]
                    });
                }
            });
        }
        copyOfTeam = _.uniq(copyOfTeam,"emailId");

        listOfMembers = _
            .chain(listOfMembers)
            .groupBy("userEmailId")
            .map(function(values, key) {

                var teamMatesEmailId = [],
                    teamMatesUserId = [];

                _.each(values,function (va) {
                    teamMatesEmailId.push(va.teammate.emailId)
                    teamMatesUserId.push(va.teammate._id)
                });

                return {
                    userEmailId:key,
                    teamMatesEmailId:teamMatesEmailId,
                    teamMatesUserId:teamMatesUserId
                }
            })
            .value();

        if(forCommits){
            getTargetsAndAchievementsForCommit(copyOfTeam,companyId,timezone,range,function (err,targetsAchievementCommits) {
                callback(listOfMembers,targetsAchievementCommits)
            });
        } else {
            callback(listOfMembers,copyOfTeam)
        }
    }

}

router.get('/company/user/all/hierarchy/types', common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    secondaryHierarchyObj.getAllHierarchyTypes(common.castToObjectId(userId),function (errors, hierarchies) {
        var allHierarchies = ["All Access","Org. Hierarchy"];
        if(!errors && hierarchies && hierarchies.length>0){
            allHierarchies = allHierarchies.concat(hierarchies)
        }

        res.send({
            SuccessCode: 1,
            ErrorCode: 0,
            Data:allHierarchies
        })
    });
});

router.get('/company/get/master/account', common.isLoggedInUserToGoNext, function(req, res){

    var userId = common.getUserId(req.user);

    common.getProfileOrStoreProfileInSession(common.castToObjectId(userId),req,function(user){
        masterDataObj.getMasterDataRow(common.castToObjectId(user.companyId),req.query.name,req.query.accName,function (err,data) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data:data
            })
        })
    });

});

module.exports = {
    router: router,
    getAllSecondaryHierarchyOpportunityData: getAllSecondaryHierarchyOpportunityData,
    getSecondaryHierarchyRMData: getSecondaryHierarchyRMData
}
