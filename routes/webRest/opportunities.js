
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var commonUtility = require('../../common/commonUtility');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var contactClass = require('../../dataAccess/contactsManagementClass');
var opportunityManagement = require('../../dataAccess/opportunitiesManagement');
var opportunityLogCollection = require('../../databaseSchema/oppLogSchema').opportunityLog
var MessageManagement = require('../../dataAccess/messageManagementV2');
var taskManagementClass = require('../../dataAccess/taskManagementClass');
var RevenueHierarchy = require('../../dataAccess/revenueHierarchy');
var OppWeightsClass = require('../../dbCache/company.js');
var async = require("async");
var _ = require("lodash");

var oppManagementObj = new opportunityManagement();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var company = new companyClass();
var oppWeightsObj = new OppWeightsClass();
var messageManagementObj = new MessageManagement();
var taskManagementClassObj = new taskManagementClass();
var revenueHierarchyObj = new RevenueHierarchy();
var redis = require('redis');
var redisClient = redis.createClient();

var winstonLog = require('../../common/winstonLog');
var logLib = new winstonLog();
var redisLogs = logLib.getRedisLog();

router.get('/opportunities/add/edit',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/oppAddEdit')
});

router.get('/opportunities/add/edit/v1',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/oppAddEditV1')
});

router.get('/opportunities/basic',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/basic')
});

router.get('/opportunities/customer',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/customer')
});

router.get('/opportunities/team',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/team')
});

router.get('/opportunities/notes',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/notes')
});

router.get('/opportunities/tasks',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/tasks')
});

router.get('/opportunities/close',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/close')
});

router.get('/opportunities/tasks/createNew',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/createNew')
});

router.get('/opportunities/activity',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/activity')
});

router.get('/opportunities/document/list',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/documents');
});

router.get('/opportunities/get/all/data', common.isLoggedInUserOrMobile, function (req, res) {
    var hierarchyList = [];
    var userId = common.getUserIdFromMobileOrWeb(req);
    var userProfile = common.getUserFromSession(req);

    var cEmailId = req.query.emailId?req.query.emailId:null;

    if(req.query.hierarchylist)
        hierarchyList = req.query.hierarchylist.split(",")
    else
        hierarchyList = [userId]

    var basedOnAccessControl = false;
    if(req.query.accessControl && req.query.accessControl == 'true'){
        basedOnAccessControl = true;
    }

    var liuEmailId = null;
    if(userProfile){
        liuEmailId = userProfile.emailId
    }

    hierarchyList = _.uniq(hierarchyList)
    hierarchyList = common.castListToObjectIds(hierarchyList);

    getUserAccessControl(hierarchyList,userId,function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq,companyTargetAccess,companyDetails,businessUnits) {
        userManagementObj.getTeamMembers(companyId,function (cErr,teamMembers) {

            var teamUserIds = []
                ,teamUserObj = {};

            _.each(teamMembers,function (el) {
                var id = String(el._id);
                teamUserObj[id] = el;
                teamUserIds.push(el._id);
            });

            if(!basedOnAccessControl){
                regionAccess = null,productAccess = null,verticalAccess = null,businessUnits = null;
            }

            common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {
                var fromDate = fyRange.start,toDate = fyRange.end;

                var fyList = [{
                    year:moment(fromDate).format("YYYY")+"-"+moment(toDate).format("YYYY"),
                    start:fromDate,
                    end:toDate
                }];

                var prevFrom = moment(fromDate).subtract(1,"year"),prevTo = moment(toDate).subtract(1,"year");
                fyList.push({
                    year:moment(prevFrom).format("YYYY")+"-"+moment(prevTo).format("YYYY"),
                    start:prevFrom,
                    end:prevTo
                });

                var nextFrom = moment(fromDate).add(1,"year"),nextTo = moment(toDate).add(1,"year");

                fyList.push({
                    year:moment(nextFrom).format("YYYY")+"-"+moment(nextTo).format("YYYY"),
                    start:nextFrom,
                    end:nextTo
                });

                if(req.query.fromDate){

                    if(req.query.fromDate == "future"){
                        fromDate = moment(fyRange.start).add(1,"year");
                        toDate = moment(fyRange.start).add(10,"year");
                    } else if(req.query.fromDate == "past"){
                        fromDate = new Date();
                        fromDate = fromDate.setFullYear(2000);
                        toDate = moment(fyRange.end).subtract(1,"year");
                    } else {
                        fromDate = fyRange.start;
                        toDate = fyRange.end;
                    }
                }

                var usersWithAccess = [liuEmailId];

                if(hierarchyList.length == 1 && hierarchyList[0] && hierarchyList[0] != userId){
                    usersWithAccess = [];
                    usersWithAccess.push(teamUserObj[hierarchyList[0]].emailId);
                    cEmailId = liuEmailId;
                }

                // Filtering opps for notifications. Don't touch
                if(req.query.notifyCategory && req.query.notifyDate) {
                    fromDate = moment(req.query.notifyDate, "DDMMYYYY").startOf('day').toDate();

                    if(req.query.notifyCategory == "oppClosingToday") {
                        toDate = moment(req.query.notifyDate, "DDMMYYYY").endOf('day').toDate();
                    }

                    else if(req.query.notifyCategory == "oppClosingThisWeek") {
                        toDate = moment(req.query.notifyDate, "DDMMYYYY").add(7, "days").endOf('day').toDate();
                    }
                }

                oppManagementObj.opportunitiesForUsers(hierarchyList,fromDate,toDate,cEmailId,regionAccess,productAccess,verticalAccess,businessUnits,teamUserIds,common.castToObjectId(companyId),usersWithAccess,req.query.portfolios,function (err,opp) {

                    if(opp && opp[0] && opp[0].opportunities){
                        
                        //Filtering opps for notifications. Don't touch
                        if(req.query.notifyCategory && req.query.notifyDate) { 
                            opp[0].opportunities = _.filter(opp[0].opportunities, function(opp) {
                                if(!_.includes(['Close Won','Close Lost','Closed Won','Closed Lost'], opp.stageName)) {
                                    return opp;
                                }
                            })
                        }

                        var usersAndContacts = _.flatten(_.pluck(opp,"usersAndContacts"))

                        contactObj.getContactsForUsers(usersAndContacts,function (error,matchedContacts) {

                            if(!error && matchedContacts){

                                res.send({
                                    SuccessCode: 1,
                                    err:err,
                                    fyList:fyList,
                                    allQuarters:allQuarters,
                                    data:opp,
                                    contacts:matchedContacts
                                });

                            } else {

                                res.send({
                                    SuccessCode: 0,
                                    ErrorCode: 1,
                                    fyList:fyList,
                                    allQuarters:allQuarters,
                                    data: []
                                });
                            }
                        })

                    } else {
                        res.send({
                            SuccessCode: 0,
                            ErrorCode: 1,
                            fyList:fyList,
                            allQuarters:allQuarters,
                            data: []
                        });
                    }
                });
            });

        });
    })

});

router.get('/opportunities/by/exceptional/access', common.isLoggedInUserOrMobile, function (req, res) {

    var hierarchyList = [];
    var userProfile = common.getUserFromSession(req);
    var userId = common.checkRequired(req.query.userId)?req.query.userId:common.getUserIdFromMobileOrWeb(req);

    var cEmailId = req.query.emailId?req.query.emailId:null;

    if(req.query.hierarchylist)
        hierarchyList = req.query.hierarchylist.split(",")
    else
        hierarchyList = [userId]

    var basedOnAccessControl = false;
    if(req.query.accessControl && req.query.accessControl == 'true'){
        basedOnAccessControl = true;
    }

    var liuEmailId = null;
    if(userProfile){
        liuEmailId = userProfile.emailId
    }

    getUserAccessControl(hierarchyList,userId,function (regionAccess,productAccess,verticalAccess,companyId) {

        revenueHierarchyObj.getUserHierarchy(common.castToObjectId(userId),function (rErr,revenueHierarchyData) {

            userManagementObj.getUserHierarchy(common.castToObjectId(userId),function (oErr,orgHierarchyData) {

                if (!rErr && revenueHierarchyData && revenueHierarchyData.length > 0 && basedOnAccessControl) {
                    _.each(revenueHierarchyData, function (el) {
                        hierarchyList.push(String(el.userId));
                    });
                }

                if (!oErr && orgHierarchyData && orgHierarchyData.length > 0 && basedOnAccessControl) {
                    _.each(orgHierarchyData, function (el) {
                        hierarchyList.push(String(el._id));
                    });
                }

                hierarchyList = _.uniq(hierarchyList)
                hierarchyList = common.castListToObjectIds(hierarchyList);

                userManagementObj.getTeamMembers(companyId,function (cErr,teamMembers) {

                    var teamUserIds = _.pluck(teamMembers,"_id");

                    if(!basedOnAccessControl){
                        regionAccess = null,productAccess = null,verticalAccess = null;
                    }

                    common.userFiscalYear(common.castToObjectId(userId),function (err,fyMonth,fyRange,allQuarters,timezone) {
                        var fromDate = fyRange.start,toDate = fyRange.end;

                        var fyList = [{
                            year:moment(fromDate).format("YYYY")+"-"+moment(toDate).format("YYYY"),
                            start:fromDate,
                            end:toDate
                        }];

                        var prevFrom = moment(fromDate).subtract(1,"year"),prevTo = moment(toDate).subtract(1,"year");
                        fyList.push({
                            year:moment(prevFrom).format("YYYY")+"-"+moment(prevTo).format("YYYY"),
                            start:prevFrom,
                            end:prevTo
                        });

                        var nextFrom = moment(fromDate).add(1,"year"),nextTo = moment(toDate).add(1,"year");

                        fyList.push({
                            year:moment(nextFrom).format("YYYY")+"-"+moment(nextTo).format("YYYY"),
                            start:nextFrom,
                            end:nextTo
                        });

                        if(req.query.fromDate && req.query.toDate){
                            fromDate = req.query.fromDate.split(" ")[0];
                            toDate = req.query.toDate.split(" ")[0]
                        }

                        oppManagementObj.opportunitiesForUsers(hierarchyList,fromDate,toDate,cEmailId,regionAccess,productAccess,verticalAccess,[],teamUserIds,common.castToObjectId(companyId),liuEmailId,req.query.portfolios,function (err,data) {

                            if(data && data[0] && data[0].opportunities){

                                var opps = [];

                                _.each(data,function (el) {
                                    opps = opps.concat(el.opportunities)
                                });

                                res.send({
                                    SuccessCode: 1,
                                    ErrorCode:0,
                                    Data:opps
                                });

                            } else {

                                res.send({
                                    SuccessCode: 0,
                                    ErrorCode: 1,
                                    fyList:fyList,
                                    allQuarters:allQuarters,
                                    Data: []
                                });
                            }
                        });
                    });
                });
            })
        })
    })

});

router.get('/opportunities/contact/impact/value', common.isLoggedInUserToGoNext, function (req, res){

    var userId = common.getUserId(req.user);
    var cEmailId = req.query.emailId;
    var companyId = req.query.companyId;
    var netGrossMargin = false;

    if(req.query.netGrossMargin && req.query.netGrossMargin == 'true'){
        netGrossMargin = true;
    }

    if(companyId && cEmailId){
        oppWeightsObj.getOppStages(common.castToObjectId(companyId),function (oppStages,companyDetails) {

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            userManagementObj.getTeamMembers(common.castToObjectId(companyId),function (err,team) {
                var teamUserIds = _.pluck(team,"_id");

                oppManagementObj.matchContactInOpportunity(common.castListToObjectIds(teamUserIds),cEmailId,netGrossMargin,primaryCurrency,currenciesObj,function (err,opp) {

                    if(!err && opp){

                        var oppOwnerCount = 0,partnersCount = 0,dmCount = 0,influencersCount = 0;
                        var oppOwnerAmount = 0,partnersAmount = 0,dmAmount = 0,influencersAmount = 0;

                        _.each(opp,function (o) {

                            if(cEmailId === o.contactEmailId){
                                oppOwnerCount++;
                                oppOwnerAmount = oppOwnerAmount+parseFloat(o.amount)
                            }
                            _.each(o.partners,function (val) {
                                if(val.emailId === cEmailId){
                                    partnersCount++;
                                    partnersAmount = partnersAmount+parseFloat(o.amount)
                                }
                            });

                            _.each(o.decisionMakers,function (val) {
                                if(val.emailId === cEmailId){
                                    dmCount++;
                                    dmAmount = dmAmount+parseFloat(o.amount)
                                }
                            });
                            _.each(o.influencers,function (val) {
                                if(val.emailId === cEmailId){
                                    influencersCount++;
                                    influencersAmount = influencersAmount+parseFloat(o.amount)
                                }
                            });
                        });

                        res.send({
                            SuccessCode: 1,
                            err:err,
                            Data:{
                                occurrence:{
                                    oppOwnerCount:oppOwnerCount,
                                    partnersCount:partnersCount,
                                    influencersCount:influencersCount,
                                    dmCount:dmCount
                                },
                                amount:{
                                    oppOwnerAmount:oppOwnerAmount,
                                    partnersAmount:partnersAmount,
                                    influencersAmount:influencersAmount,
                                    dmAmount:dmAmount
                                }
                            }
                        });
                    } else {
                        res.send({
                            SuccessCode: 0,
                            ErrorCode: 1,
                            Data: null
                        })
                    }

                });
            });
        })
    }else {
        res.send({
            SuccessCode: 0,
            ErrorCode: 1,
            Data: null
        })
    }

});

router.get('/opportunities/by/month/year', common.isLoggedInUserOrMobile, function (req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var userProfile = common.getUserFromSession(req);

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

    var hierarchyList;

    if(req.query.userId)
        hierarchyList = req.query.userId.split(",")
    else
        hierarchyList = [userId]
    hierarchyList = common.castListToObjectIds(hierarchyList);

    var basedOnAccessControl = false;
    if(req.query.accessControl && req.query.accessControl == 'true'){
        basedOnAccessControl = true;
    }

    var targetsFor = hierarchyList;
    if(req.query.companyId){
        targetsFor = [common.castToObjectId(req.query.companyId)];
    }

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {

        if(err || !fyMonth){
            fyMonth = "April"; //Default
        }

        var currentYr = new Date().getFullYear()
        var currentMonth = new Date().getMonth()

        var toDate = null;
        var fromDate = new Date(moment().startOf('month'));
        fromDate.setMonth(monthNames.indexOf(fyMonth))

        if(currentMonth<monthNames.indexOf(fyMonth)){
            fromDate.setFullYear(currentYr-1)
        }

        toDate = moment(fromDate).add(11, 'month');
        toDate = moment(toDate).endOf('month')

        if(req.query.forSixMonthsRange){
            fromDate = moment().subtract(2, "month")
            toDate = moment().add(3, "month")

            fromDate.hour(0);
            fromDate.minute(0);
            fromDate.second(0);
            fromDate.millisecond(0);

            toDate.hour(23);
            toDate.minute(59);
            toDate.second(59);
            toDate.millisecond(0);

            fromDate = new Date(fromDate).setDate(1);
            toDate = moment(toDate).endOf('month')
        }

        var strLength = 3;

        getUserAccessControl(hierarchyList,userId,function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq,companyTargetAccess,companyDetails) {

            var opportunityStages = [];
            if(companyDetails && companyDetails.opportunityStages){
                opportunityStages = _.map(companyDetails.opportunityStages,"name");
            }

            if(companyTargetAccess && basedOnAccessControl){
                targetsFor = [common.castToObjectId(companyId)]
            }

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            if(req.query.fromDate && req.query.toDate){
                fromDate = req.query.fromDate.split(" ")[0];
                toDate = req.query.toDate.split(" ")[0]
            }

            oppManagementObj.getTargetsForAllUsers(targetsFor,fromDate,toDate,fyRange.start,fyRange.end,function (errTargets,targets) {

                if(!errTargets && targets){

                    var targetsList = [];
                    var filler = [];
                    var monthsNext = req.query.monthsNext?req.query.monthsNext:9;
                    var monthsPrev = req.query.monthsPrev?req.query.monthsPrev:4;

                    for(var i = 0;i<monthsNext;i++){
                        var date = new Date(moment().add(i, "month").toDate())
                        var obj = {
                            monthYear:monthNames[date.getUTCMonth()].substring(0,strLength)+" "+date.getUTCFullYear(),
                            won:0,
                            lost:0,
                            total:0,
                            target:0,
                            openValue:0,
                            sortDate:date
                        };

                        filler.push(obj)
                    }

                    for(var j = 1;j<monthsPrev;j++){
                        var date2 = new Date(moment().subtract(j, "month").toDate())
                        var obj2 = {
                            monthYear:monthNames[date2.getUTCMonth()].substring(0,strLength)+" "+date2.getUTCFullYear(),
                            won:0,
                            lost:0,
                            total:0,
                            target:0,
                            openValue:0,
                            sortDate:date2
                        };

                        filler.push(obj2)
                    }

                    _.each(targets,function (t) {
                        targetsList.push({
                            monthYear:monthNames[t.month-1].substring(0,strLength) +" "+t.year,
                            target:t.target,
                            sortDate:t.date,
                            won:0,
                            lost:0,
                            openValue:0,
                            total:0
                        })
                    });

                    if(fromDate && toDate && !req.query.monthsNext && !req.query.monthsPrev){
                        var dtRange = getDateRange(moment(fromDate).add(1,'day'), moment(toDate).subtract(1,'day'));
                        filler = []
                        _.each(dtRange,function (dt) {
                            var obj = {
                                monthYear:monthNames[dt.getUTCMonth()].substring(0,strLength)+" "+dt.getUTCFullYear(),
                                won:0,
                                lost:0,
                                total:0,
                                target:0,
                                openValue:0,
                                sortDate:dt
                            };

                            filler.push(obj)
                        })
                    }

                    if(!basedOnAccessControl){
                        regionAccess = null,productAccess = null,verticalAccess = null;
                    }

                    oppManagementObj.opportunitiesByMonthYear(hierarchyList,fromDate,toDate,regionAccess,productAccess,verticalAccess,netGrossMarginReq,common.castToObjectId(companyId),primaryCurrency,currenciesObj,function (err,opp) {

                        if(!err && opp){
                            var opportunities = [];

                            _.each(opp,function (o) {
                                opportunities.push({
                                    monthYear:monthNames[o._id.month-1].substring(0,strLength) +" "+o._id.year,
                                    stage:o._id.stage,
                                    opportunitiesValue:o.opportunitiesValue,
                                    openValue:o.openValue,
                                    sortDate:o.sortDate
                                })
                            });

                            var oppByDateAndStage =
                                _.chain(opportunities)
                                    .groupBy('monthYear')
                                    .map(function(value, key) {
                                        var won = 0;
                                        var lost = 0;
                                        var total = 0;
                                        var openValue = 0;

                                        _.each(value,function (v) {
                                            if(_.includes(v.stage, 'Close Won') || _.includes(v.stage, 'Close Won')){
                                                won = won+v.opportunitiesValue
                                            } else if(_.includes(v.stage, 'Close Lost') || _.includes(v.stage, 'close lost')){
                                                lost = lost+v.opportunitiesValue
                                            } else {
                                                openValue = openValue+v.opportunitiesValue
                                            }

                                            total = total+v.opportunitiesValue
                                        });

                                        return {
                                            monthYear:key,
                                            won:won,
                                            lost:lost,
                                            total:total,
                                            openValue:openValue,
                                            target:0,
                                            sortDate:value[0].sortDate
                                        }
                                    })
                                    .value();

                            if(oppByDateAndStage.length>0){
                                _.each(oppByDateAndStage,function (opp) {
                                    _.each(targetsList,function (target) {
                                        if(target.monthYear === opp.monthYear){
                                            opp.target = opp.target+target.target
                                        }
                                    })
                                });
                            } else {
                                oppByDateAndStage = targetsList
                            }

                            //Find values that are in targetsList but not in oppByDateAndStage
                            var uniqueResultTwo = targetsList.filter(function(obj) {
                                return !oppByDateAndStage.some(function(obj2) {
                                    return obj.monthYear == obj2.monthYear;
                                });
                            });

                            var arrayValues = oppByDateAndStage.concat(uniqueResultTwo)

                            //Find values that are in filler but not in arrayValues
                            var uniqueResultThree = filler.filter(function(obj) {
                                return !arrayValues.some(function(obj2) {
                                    return obj.monthYear == obj2.monthYear;
                                });
                            });

                            var quarters = [
                                {
                                    quarter:1,
                                    startMonth:0,
                                    endMonth:2,
                                    target:0,
                                    won:0,
                                    pipeline:0,
                                    lost:0
                                },{
                                    quarter:2,
                                    startMonth:3,
                                    endMonth:5,
                                    target:0,
                                    won:0,
                                    pipeline:0,
                                    lost:0
                                },{
                                    quarter:3,
                                    startMonth:6,
                                    endMonth:8,
                                    target:0,
                                    won:0,
                                    pipeline:0,
                                    lost:0
                                },{
                                    quarter:4,
                                    startMonth:9,
                                    endMonth:11,
                                    target:0,
                                    won:0,
                                    pipeline:0,
                                    lost:0
                                }
                            ];

                            var currentQuarter = {}
                            _.each(quarters,function (q) {
                                if (q.startMonth <= new Date().getMonth() && q.endMonth >= new Date().getMonth()) {
                                    currentQuarter = q;
                                }
                            });
                            
                            var data = arrayValues.concat(uniqueResultThree)

                            _.each(data,function (value) {

                                if (currentQuarter.startMonth <= new Date(value.sortDate).getMonth() && currentQuarter.endMonth >= new Date(value.sortDate).getMonth()) {
                                    currentQuarter.target = currentQuarter.target + parseFloat(value.target)
                                    currentQuarter.won = currentQuarter.won + value.won
                                    currentQuarter.pipeline = currentQuarter.pipeline + value.openValue
                                    currentQuarter.lost = currentQuarter.lost + value.lost
                                }
                            });
                            
                            if(userProfile && userProfile.hierarchyParent && userProfile.companyId){
                                common.getCompanyHead(common.castToObjectId(userProfile.hierarchyParent),common.castToObjectId(userProfile.companyId),function (companyHead,reportingManager) {

                                    res.send({
                                        SuccessCode: 1,
                                        ErrorCode: 0,
                                        companyHead:companyHead,
                                        reportingManager:reportingManager,
                                        currentQuarter:currentQuarter,
                                        targets: opp,
                                        Data: data
                                    })
                                });
                            } else {

                                res.send({
                                    SuccessCode: 1,
                                    ErrorCode: 0,
                                    companyHead:null,
                                    reportingManager:null,
                                    currentQuarter:currentQuarter,
                                    targets: opp,
                                    Data: data
                                })
                            }

                        } else {
                            res.send({
                                SuccessCode: 0,
                                ErrorCode: 1,
                                Data: []
                            })
                        }
                    });

                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data: []
                    })
                }
            })
        });

    });

});

router.get('/account/by/month/year', common.isLoggedInUserOrMobile, function (req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile ? userProfile.companyId : null;

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

    var basedOnAccessControl = false;

    var targetsFor = common.getUserListForQuery(req);
    var contacts = common.getContactsListForQuery(req);
    var account = req.query.account;

    common.userFiscalYear(userId,function (err,fyMonth,fyRange) {

        if(err || !fyMonth){
            fyMonth = "April"; //Default
        }

        var currentYr = new Date().getFullYear()
        var currentMonth = new Date().getMonth()

        var toDate = null;
        var fromDate = new Date(moment().startOf('month'));
        fromDate.setMonth(monthNames.indexOf(fyMonth))

        if(currentMonth<monthNames.indexOf(fyMonth)){
            fromDate.setFullYear(currentYr-1)
        }

        toDate = moment(fromDate).add(11, 'month');
        toDate = moment(toDate).endOf('month')

        if(req.query.forSixMonthsRange){
            fromDate = moment().subtract(2, "month")
            toDate = moment().add(3, "month")

            fromDate.hour(0);
            fromDate.minute(0);
            fromDate.second(0);
            fromDate.millisecond(0);

            toDate.hour(23);
            toDate.minute(59);
            toDate.second(59);
            toDate.millisecond(0);

            fromDate = new Date(fromDate).setDate(1);
            toDate = moment(toDate).endOf('month')
        }

        var strLength = 3;

        getUserAccessControl(targetsFor,userId,function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq,companyTargetAccess,companyDetails) {

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if (el.isPrimary) {
                    primaryCurrency = el.symbol;
                }
            });

            if(companyTargetAccess && basedOnAccessControl){
                targetsFor = [common.castToObjectId(companyId)]
            }

            if(req.query.fromDate && req.query.toDate){
                fromDate = req.query.fromDate.split(" ")[0];
                toDate = req.query.toDate.split(" ")[0]
            }

            oppManagementObj.getTargetsForAllUsers(targetsFor,fromDate,toDate,fyRange.start,fyRange.end,function (errTargets,targets) {

                if(!errTargets && targets){

                    var targetsList = [];
                    var filler = [];
                    var monthsNext = req.query.monthsNext?req.query.monthsNext:9;
                    var monthsPrev = req.query.monthsPrev?req.query.monthsPrev:4;

                    for(var i = 0;i<monthsNext;i++){
                        var date = new Date(moment().add(i, "month").toDate())
                        var obj = {
                            monthYear:monthNames[date.getUTCMonth()].substring(0,strLength)+" "+date.getUTCFullYear(),
                            won:0,
                            lost:0,
                            total:0,
                            target:0,
                            openValue:0,
                            sortDate:date
                        };

                        filler.push(obj)
                    }

                    for(var j = 1;j<monthsPrev;j++){
                        var date2 = new Date(moment().subtract(j, "month").toDate())
                        var obj2 = {
                            monthYear:monthNames[date2.getUTCMonth()].substring(0,strLength)+" "+date2.getUTCFullYear(),
                            won:0,
                            lost:0,
                            total:0,
                            target:0,
                            openValue:0,
                            sortDate:date2
                        };

                        filler.push(obj2)
                    }

                    _.each(targets,function (t) {
                        targetsList.push({
                            monthYear:monthNames[t.month-1].substring(0,strLength) +" "+t.year,
                            target:t.target,
                            sortDate:t.date,
                            won:0,
                            lost:0,
                            openValue:0,
                            total:0
                        })
                    });

                    if(fromDate && toDate && !req.query.monthsNext && !req.query.monthsPrev){
                        var dtRange = getDateRange(moment(fromDate).add(1,'day'), moment(toDate).subtract(1,'day'));
                        filler = []
                        _.each(dtRange,function (dt) {
                            var obj = {
                                monthYear:monthNames[dt.getUTCMonth()].substring(0,strLength)+" "+dt.getUTCFullYear(),
                                won:0,
                                lost:0,
                                total:0,
                                target:0,
                                openValue:0,
                                sortDate:dt
                            };

                            filler.push(obj)
                        })
                    }

                    if(!basedOnAccessControl){
                        regionAccess = null,productAccess = null,verticalAccess = null;
                    }

                    oppManagementObj.opportunitiesByMonthYearAndAccount(targetsFor,fromDate,toDate,contacts,netGrossMarginReq,common.castToObjectId(companyId),primaryCurrency,currenciesObj,function (err,opp) {

                        if(!err && opp){
                            var opportunities = [];

                            _.each(opp,function (o) {
                                opportunities.push({
                                    monthYear:monthNames[o._id.month-1].substring(0,strLength) +" "+o._id.year,
                                    stage:o._id.stage,
                                    opportunitiesValue:o.opportunitiesValue,
                                    openValue:o.openValue,
                                    sortDate:o.sortDate
                                })
                            });

                            var oppByDateAndStage =
                                _.chain(opportunities)
                                    .groupBy('monthYear')
                                    .map(function(value, key) {
                                        var won = 0;
                                        var lost = 0;
                                        var total = 0;
                                        var openValue = 0;

                                        _.each(value,function (v) {
                                            if(_.includes(v.stage, 'Won')){
                                                won = won+v.opportunitiesValue
                                            }

                                            if(_.includes(v.stage, 'Lost')){
                                                lost = lost+v.opportunitiesValue
                                            }

                                            total = total+v.opportunitiesValue
                                            openValue = openValue+v.openValue
                                        });

                                        return {
                                            monthYear:key,
                                            won:won,
                                            lost:lost,
                                            total:total,
                                            openValue:openValue,
                                            target:0,
                                            sortDate:value[0].sortDate
                                        }
                                    })
                                    .value();

                            if(oppByDateAndStage.length>0){
                                _.each(oppByDateAndStage,function (opp) {
                                    _.each(targetsList,function (target) {
                                        if(target.monthYear === opp.monthYear){
                                            opp.target = opp.target+target.target
                                        }
                                    })
                                });
                            } else {
                                oppByDateAndStage = targetsList
                            }

                            //Find values that are in targetsList but not in oppByDateAndStage
                            var uniqueResultTwo = targetsList.filter(function(obj) {
                                return !oppByDateAndStage.some(function(obj2) {
                                    return obj.monthYear == obj2.monthYear;
                                });
                            });

                            var arrayValues = oppByDateAndStage.concat(uniqueResultTwo)

                            //Find values that are in filler but not in arrayValues
                            var uniqueResultThree = filler.filter(function(obj) {
                                return !arrayValues.some(function(obj2) {
                                    return obj.monthYear == obj2.monthYear;
                                });
                            });

                            res.send({
                                SuccessCode: 1,
                                ErrorCode: 0,
                                targets: opp,
                                Data: arrayValues.concat(uniqueResultThree)
                            })
                        } else {
                            res.send({
                                SuccessCode: 0,
                                ErrorCode: 1,
                                Data: []
                            })
                        }
                    });

                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data: []
                    })
                }
            })
        });

    });

});

router.get("/opportunities/get/targets", common.isLoggedInUserToGoNext, function (req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);

    var companyId = req.query.companyId;
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

    userManagementObj.getTeamMembers(common.castToObjectId(companyId),function (err,team) {

        common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {

            var teamUserIds = _.pluck(team, "_id");

            if (!fyMonth) {
                fyMonth = "April"; //Default
            }

            var currentYr = new Date().getFullYear()
            var currentMonth = new Date().getMonth()

            var toDate = null;
            var fromDate = new Date(moment().startOf('month'));
            fromDate.setMonth(monthNames.indexOf(fyMonth))

            if (currentMonth < monthNames.indexOf(fyMonth)) {
                fromDate.setFullYear(currentYr - 1)
            }

            toDate = moment(fromDate).add(11, 'month');
            toDate = moment(toDate).endOf('month')

            teamUserIds.push(common.castToObjectId(companyId))

            fyRange.start = new Date(moment(fyRange.start).subtract(1,"day"))
            fyRange.end = new Date(moment(fyRange.end).add(1,"day"))

            oppManagementObj.getTargetForUser(teamUserIds,fyRange.start,fyRange.end,function (err,result) {

                var dtRange = getDateRange12Months(moment(fromDate).add(1,'day'), moment(toDate).subtract(1,'day'))

                var companyTarget = null;

                _.each(result,function (re) {

                    if(common.checkRequired(re.isCompanyTarget)){
                        companyTarget = re
                    }
                })

                var data = _.map(team,function (tm) {
                    var obj = {
                        user:tm,
                        targets:[]
                    };

                    _.each(result,function (re) {
                        if((tm._id.toString() == re.userId)){

                            re.salesTarget.sort(function (o1, o2) {
                                return o1.date > o2.date ? 1 : o1.date < o2.date ? -1 : 0;
                            });

                            obj.targets = re
                        }
                    });

                    return obj;
                })

                if(companyTarget){
                    data.push({
                        user:{
                            firstName:"Company Target"
                        },
                        isCompanyTarget:true,
                        targets:companyTarget
                    });
                }

                res.send({err:err,team:data,fiscalYear:dtRange})
            })
        })
    });
})

router.post("/opportunities/set/target", common.isLoggedInUserToGoNext, function (req, res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {
        fyRange.start = moment(fyRange.start).add(1,"day");

        oppManagementObj.setTargetForUserBulk(req.body,common.castToObjectId,fyRange,function (err,result) {
            res.send(true)
        })

    })
})

router.post("/opportunities/transfer", common.isLoggedInUserOrMobile, function (req, res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    if(req.body.toTransfer && req.body.toTransfer.length>0){
        
        oppManagementObj.transferOpp(common.castToObjectId(userId),req.body.toTransfer,common,function (err,result) {

            if(!err && result){

                common.getAdminAndReportingManagers(common.castToObjectId(req.body.reportingManagerForUser1),
                    common.castToObjectId(req.body.reportingManagerForUser2),function (err,reportingManager) {

                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            reportingManager:reportingManager
                        })
                    })

            } else {

                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1
                })
            }
        });
    } else {
        res.send({
            SuccessCode: 0,
            ErrorCode: 1
        })
    }
})

router.post("/opportunities/set/company/target", common.isLoggedInUserToGoNext, function (req, res){

    oppManagementObj.setTargetForCompanyBulk(req.body,common.castToObjectId,function (err,result) {
        res.send(true)
    })
})

router.post("/opportunities/delete", common.isLoggedInUserToGoNext, function (req, res){
    var userId = common.getUserIdFromMobileOrWeb(req)

    if(req.body && req.body.oppId) {
        oppManagementObj.deleteOpp(common.castToObjectId(userId),common.castToObjectId(req.body.oppId),function (err,result) {
            if(!err && result){
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0
                })
            } else {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1
                })
            }
        })
    } else {
        res.send({
            SuccessCode: 0,
            ErrorCode: 1
        })
    }
})

router.post("/opportunities/update", common.isLoggedInUserToGoNext, function (req, res){
    var userId = common.castToObjectId(common.getUserId(req.user))

    var updateObj = {}

    updateObj[req.body.updateField] = req.body.updateField == "closeDate"?new Date(req.body.value):req.body.value;
    var opportunityId = req.body._id

    if(req.body.closeThisOpp){

        updateObj["isClosed"] = true;
        updateObj["isWon"] = false;
        updateObj["stageName"] = "Close Lost";
        updateObj["relatasStage"] = "Close Lost";
    }

    if(req.body.updateField == "stageName"){
        updateObj["lastStageUpdated"] = {
            fromStage:req.body.prevStage,
            date : new Date()
        }

        updateObj["relatasStage"] = req.body.value;
        updateObj["stageName"] = req.body.value;
    }

    oppManagementObj.updateOpportunityForContact(userId,opportunityId,updateObj,function (err,result) {
        res.send(true)
    })

})

router.get('/opportunities/get/log',common.isLoggedInUserOrMobile, function(req, res) {
    
    var userId = common.getUserIdFromMobileOrWeb(req);
    var opportunityId = req.query.opportunityId;

    opportunityLogCollection.collection.find({opportunityId:opportunityId}).toArray(function (err,log) {

        if(!err && log){

            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data:log
            })

        } else {

            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    })

});

router.get('/opportunities/group/count',common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var hierarchyList = []

    if (req.query.userIds)
        hierarchyList = req.query.userIds.split(",")
    else {
        hierarchyList = [userId]
    }

    hierarchyList = common.castListToObjectIds(hierarchyList);

    var basedOnAccessControl = false;
    if(req.query.accessControl && req.query.accessControl == 'true'){
        basedOnAccessControl = true;
    }

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

    common.userFiscalYear(userId,function (err,fyMonth) {

        if (err && !fyMonth) {
            fyMonth = "April"; //Default
        }

        var currentYr = new Date().getFullYear()
        var currentMonth = new Date().getMonth()

        var toDate = null;
        var fromDate = new Date(moment().startOf('month'));
        fromDate.setMonth(monthNames.indexOf(fyMonth))

        if (currentMonth < monthNames.indexOf(fyMonth)) {
            fromDate.setFullYear(currentYr - 1)
        }

        toDate = moment(fromDate).add(11, 'month');
        toDate = moment(toDate).endOf('month')

        getUserAccessControl(hierarchyList,userId,function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq,companyTargetAccess,companyDetails) {

            if(!basedOnAccessControl){
                regionAccess = null,productAccess = null,verticalAccess = null;
            }

            var primaryCurrency = "USD";
            var currenciesObj = {};

            companyDetails.currency.forEach(function (el) {
                currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    primaryCurrency = el.symbol;
                }
            });

            oppManagementObj.groupByUsersAndStage(hierarchyList,fromDate,toDate,regionAccess,productAccess,verticalAccess,netGrossMarginReq,common.castToObjectId(companyId),primaryCurrency,currenciesObj,function (err,opps) {

                if(!err && opps){

                    var stages =[
                        'Prospecting',
                        'Evaluation',
                        'Proposal',
                        'Close Won',
                        'Close Lost'
                    ]

                    var existingStages = _.pluck(opps,"_id");

                    var uniqueResult = stages.filter(function(obj) {
                        return !existingStages.some(function(obj2) {
                            return obj == obj2;
                        });
                    });

                    var data = opps;

                    var nonExisting = [];
                    if(uniqueResult.length>0){
                        _.each(uniqueResult,function (el) {
                            nonExisting.push({
                                _id:el,
                                totalAmount:0,
                                count:0
                            })
                        })

                        data = opps.concat(nonExisting)
                    }

                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data: data,
                        fy:{fromDate:new Date(moment(fromDate).add(1,'day')),toDate:new Date(moment(toDate).subtract(1,'day'))}
                    })

                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data: [],
                        fy:{fromDate:new Date(moment(fromDate).add(1,'day')),toDate:new Date(moment(toDate).subtract(1,'day'))}
                    })
                }
            })
        });
    });

});

router.get('/opportunity/related/data/by/id',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    var opportunityId = req.query.opportunityId;

    async.parallel([
        function (callback) {
            opportunityLogCollection.collection.find({opportunityId:opportunityId}).toArray(function (err,log) {
                callback(err,log)
            });
        },
        function (callback) {
            messageManagementObj.getConversationByConversationId(null,opportunityId,null,callback)
        },
        function (callback) {
            taskManagementClassObj.getTaskByRefId(opportunityId,callback)
        }
    ], function (errors,data) {
        if(!errors && data){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                logs:data[0],
                notes:data[1],
                tasks:data[2]
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data: null
            })
        }
    });
});

router.get('/opportunity/activity/interactions',common.isLoggedInUserOrMobile,function(req,res){
    var opportunityId = req.query.opportunityId;
    var userEmailIds = req.query.userEmailIds.split(",");
    var contacts = req.query.contacts.split(",");
    var from = req.query.from;
    var to = req.query.to;

    async.parallel([
        function (callback) {

            var query = {
                opportunityId:opportunityId
            }
            if(from){
                query = {
                    opportunityId:opportunityId,
                    date: {
                        $gte: new Date(from),
                        $lte: to?new Date(to):new Date()
                    }
                }
            }
            opportunityLogCollection.collection.find(query).toArray(function (err,log) {
                callback(err,log)
            });
        },
        function (callback) {
            oppManagementObj.getInteractionHistoryByDate(userEmailIds,contacts,from,to,callback)
        }
    ], function (errors,data) {
        if(!errors && data){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                logs:data[0],
                interactions:data[1]
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data: null
            })
        }
    });
});

router.get('/opportunities/get/opportunity/by/id',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    if(req.query.opportunityId){
        oppManagementObj.getOppByCustomQuery({opportunityId:req.query.opportunityId},null,function (err,opp) {
            res.send(opp && opp[0]?opp:null)
        })
    } else {
        res.send(null)
    }
});

router.get('/opportunities/search/by/name',common.isLoggedInUserOrMobile,function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var key = "oppsForHierarchy"+String(userId);
    var searchTerm = req.query.searchTerm;

    getCache(key,function (data) {
        if(data){
            searchAndSendOpps(searchTerm,data,res)
        } else {
            getUserAccessControl([userId],userId,function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq) {
                userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, data){
                    oppManagementObj.getAllOppsByUserId(_.pluck(data,"_id"),regionAccess,productAccess,verticalAccess,netGrossMarginReq,common.castToObjectId(companyId),function (err,opps) {
                        setCache(key,opps)
                        searchAndSendOpps(searchTerm,opps,res)
                    })
                });
            })
        }
    })
});

router.get('/documents/opportunities/get/by/user/id',common.isLoggedInUserOrMobile,function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var key = "oppsForHierarchy"+String(userId);
    var searchTerm = req.query.searchTerm;
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);

    // getCache(key,function (data) {
    //     if(data){
    //         searchAndSendOpps(searchTerm,data,res)
    //     } else {
            getUserAccessControl([userId],userId,function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq) {
                userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, data){
                    oppManagementObj.getAllSystemReferencedOppsByUserId(_.pluck(data,"_id"),
                                                                        regionAccess,
                                                                        productAccess,
                                                                        verticalAccess,
                                                                        netGrossMarginReq,
                                                                        objCompanyId,
                                                                        function (err,opps) {
                        // setCache(key,opps)
                        //searchAndSendOpps(searchTerm,opps,res);
                        searchAndSendOpps(opps,res);

                    })
                });
            })
    //     }
    // })
});

router.get('/opportunities/getMLOpportunityInsightsByUserId',common.isLoggedInUserOrMobile,function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var key = "oppsForHierarchy"+String(userId);
    var searchTerm = req.query.searchTerm;
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);

    // getCache(key,function (data) {
    //     if(data){
    //         searchAndSendOpps(searchTerm,data,res)
    //     } else {
    getUserAccessControl([userId],userId,function (regionAccess,productAccess,verticalAccess,companyId,netGrossMarginReq) {
        userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, data){
            oppManagementObj.getMLOpportunityInsightsByUserId(_.pluck(data,"_id"),
                regionAccess,
                productAccess,
                verticalAccess,
                netGrossMarginReq,
                objCompanyId,
                function (err,opps) {
                    // setCache(key,opps)
                    //searchAndSendOpps(searchTerm,opps,res);
                    searchAndSendOpps(opps,res);

                })
        });
    })
    //     }
    // })
});

function searchAndSendOpps(opps,res) {
    var searchedOpps = []
    var i  = 0;
    _.each(opps,function (op) {
        // if(i < 5 ){
        // if(op.opportunityName.match(new RegExp(searchTerm, 'i'))){
            searchedOpps.push(op);
            i = i +1;
        // }
    });

    res.send(searchedOpps)
}

function getUserAccessControl(userIds,liu,callback) {

    var key = "oppUserAccessControl"+String(liu);

    getCache(key,function (data) {

        if(data){
            getAccessDetails(data.oppOwnershipDetails,data.companyDetails,callback)
        } else {
            userManagementObj.getUserOppOwnerDetails(userIds,function (errO,oppOwnershipDetails) {

                if(oppOwnershipDetails[0].companyId){

                    company.findCompanyById(oppOwnershipDetails[0].companyId,function (companyDetails) {
                        setCache(key,{oppOwnershipDetails:oppOwnershipDetails,companyDetails:companyDetails})
                        getAccessDetails(oppOwnershipDetails,companyDetails,callback)
                    });
                } else {
                    callback(null,null,null,null,false,false)
                }
            });
        }

    })
}

function getAccessDetails(oppOwnershipDetails,companyDetails,callback) {

    var regionAccess = companyDetails.geoLocations && companyDetails.geoLocations.length>0?oppOwnershipDetails[0].regionOwner:null;
    var productAccess = companyDetails.productList && companyDetails.productList.length>0?oppOwnershipDetails[0].productTypeOwner:null;
    var verticalAccess = companyDetails.verticalList && companyDetails.verticalList.length>0?oppOwnershipDetails[0].verticalOwner:null;
    var businessUnits = companyDetails.businessUnits && companyDetails.businessUnits.length>0?oppOwnershipDetails[0].businessUnits:null;
    var netGrossMarginReq = companyDetails.netGrossMargin?companyDetails.netGrossMargin:false;
    var companyTargetAccess = oppOwnershipDetails[0].companyTargetAccess?oppOwnershipDetails[0].companyTargetAccess:false;

    callback(regionAccess,productAccess,verticalAccess,oppOwnershipDetails[0].companyId,netGrossMarginReq,companyTargetAccess,companyDetails,businessUnits)
}

function setCache(key,data){
    //Storing for 30 min
    redisClient.setex(key, 1800, JSON.stringify(data));
}

function getCache(key,callback){
    redisLogs.info("Opportunities ",key);
    if(!_.includes(key, 'null')){
        redisClient.get(key, function (error, result) {
            try {
                var data = JSON.parse(result);
                // callback(data)
                callback(null)
            } catch (err) {
                callback(null)
            }
        });
    } else {
        callback(null)
    }
}

function getDateRange(startDate, endDate) {

    var dates = [],
        end = moment(endDate),
        diff = endDate.diff(startDate, 'months');

    if(diff <= 0) {
        return;
    }

    dates.push(new Date(endDate))

    for(var i = 0; i < diff; i++) {
        dates.push(new Date(end.subtract(1,'month').format()));
    }

    return dates;
};

function getDateRange12Months(startDate, endDate) {

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

    var stDt = new Date(startDate);

    var dates = [{
        "_id": 0,
        "target": 0,
        "date": stDt,
        "monthYear": monthNames[stDt.getUTCMonth()].substring(0,3)+" "+stDt.getUTCFullYear()
    }];

    for(var i = 0; i < 11; i++) {
        var date = new Date(startDate.add(1,'month'));
        dates.push({
            "_id": 0,
            "target": 0,
            "date": date,
            "monthYear": monthNames[date.getUTCMonth()].substring(0,3)+" "+date.getUTCFullYear()
        });
        // dates.push(new Date(startDate.subtract(1,'month')));
    }

    return dates;
};

module.exports = router;