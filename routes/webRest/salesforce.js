var salesforce = require("node-salesforce");
var express = require('express');
var router = express.Router();
var request = require('request');
var _ = require("lodash");
var mongoose = require('mongoose');
var momentTZ = require('moment-timezone');
var moment = require('moment');
var companyCollection = require('../../databaseSchema/corporateCollectionSchema').companyModel;
var myUserCollection = require('../../databaseSchema/userManagementSchema').User;
var commonUtility = require('../../common/commonUtility');
var salesforceApi = require('../../common/salesforceAPI');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var contactClass = require('../../dataAccess/contactsManagementClass');
var opportunityManagement = require('../../dataAccess/opportunitiesManagement');
var SocialManagement = require("../../dataAccess/socialManagement")
var AccManagement = require("../../dataAccess/accountManagement")
var messageManagement = require('../../dataAccess/messageManagement');
var googleCalendarAPI = require('../../common/googleCalendar');
var Gmail = require('../../common/gmail');
var officeOutlookApi = require('../../common/officeOutlookAPI');

var messageObj = new messageManagement();
var accManagementObj = new AccManagement();
var salesforceApiObj = new salesforceApi();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var oppManagementObj = new opportunityManagement();
var socialManagement = new SocialManagement();
var customObjectId = require('mongodb').ObjectID;
var googleCalendar = new googleCalendarAPI();
var gmailObj = new Gmail();
var officeOutlook = new officeOutlookApi();

var redis = require('redis');
var redisClient = redis.createClient();

router.get('/salesforce/oauth2/auth',common.isLoggedInUserToGoNext, function(req, res) {

    if(req.query.companyId){

        companyCollection.findOne({_id:req.query.companyId},function(error,companyInfo){

            if(companyInfo){
                var oauth2 = new salesforce.OAuth2({
                    clientId : companyInfo.salesForceSettings.clientId,
                    clientSecret : companyInfo.salesForceSettings.clientSecret,
                });

                req.session.salesforceFinalPage = '/' + req.query.finalPage;
                req.session.save();
                var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';

                if(req.headers.host === "relatas.localhost.com:7000"){
                    oauth2.redirectUri = 'https://localhost:7000/salesforce/oauth2/callback'
                } else {
                    oauth2.redirectUri = 'https://' + req.headers.host + '/salesforce/oauth2/callback'
                }

                oauth2.redirectBackToApp = protocol+req.headers.host+'/' + req.query.finalPage

                redisClient.setex("salesForceSettings", 360, JSON.stringify(oauth2));

                res.redirect(oauth2.getAuthorizationUrl({ scope : 'refresh_token openid api full' }));
            }
        })
    }
});

router.get('/salesforce/oauth2/callback',common.isLoggedInUserToGoNext, function(req, res) {
    /*
        salesforce redirect the user to this API after user logged in to their salesforce account
        steps -
            1. store salesforce information in user profile
            2. update the opportunities, contacts from salesforce
            3. stores the session about status (login, data updated from salesforce)

        return -
            1. redirect the user back to last page (settings,...)
    */

    redisClient.get("salesForceSettings",function (error,cacheResult) {

        try{
            var oauth2 = JSON.parse(cacheResult)

            var conn = new salesforce.Connection({ oauth2 : oauth2 });
            var code = req.param('code');
            var userId = common.getUserId(req.user);

            conn.authorize(code, function(err, userInfo) {

                var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
                var redirectBackToApp = oauth2.redirectBackToApp

                if (err) {
                    req.session.salesforceLoginSuccess = false;
                    req.session.save();
                    res.redirect(redirectBackToApp);
                }
                else {
                    var connectionOptions = {
                        url:userInfo.url,
                        method:'GET',
                        headers: {
                            Authorization: 'Bearer '+conn.accessToken
                        }
                    };

                    request(connectionOptions, function ( error, r, status ) {
                        if(error){
                            req.session.salesforceLoginSuccess = false;
                            req.session.save();
                            res.redirect(redirectBackToApp);
                        } else {
                            var data = JSON.parse(status)
                            var obj = {
                                refreshToken:conn.refreshToken,
                                instanceUrl:conn.instanceUrl,
                                id:userInfo.id,
                                userInfoUrl:userInfo.url,
                                emailId:data.email
                            }
                            userManagementObj.updateUserSalesforceInfo(userId,obj,function(error, result){

                                if(error || !result.nModified){
                                    req.session.salesforceLoginSuccess = false;
                                    req.session.save();
                                    res.redirect(redirectBackToApp);
                                }
                                else{
                                    req.session.salesforceLoginSuccess = true;
                                    req.session.save();
                                    res.redirect(redirectBackToApp);

                                    getAndStoreOpportunities(userId, 'all', function(err, result){
                                        if(err){
                                            //check for api enable error
                                            req.session.salesforceUpdateSuccess = false;
                                            req.session.salesforceApiEnable = true;
                                            req.session.save();
                                        }
                                        else{
                                            req.session.salesforceUpdateSuccess = true;
                                            req.session.salesforceApiEnable = false;
                                            req.session.save();
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        } catch(err){
            res.redirect("/settings");
        }
    });

});

router.get('/salesforce/accountAdded',function(req, res){
    /*
        steps -
            1. check for salesforce login status to notify the user about the same
        returns -
            1. show (show notify)  value(success or fail)
     */
    if(req.session.salesforceLoginSuccess == true || req.session.salesforceLoginSuccess == false){
        res.send({data:{show:true, value:req.session.salesforceLoginSuccess}})
    }
    else{
        res.send({data:{show:false, value:false}})
    }
});

router.get('/salesforce/remove/account',function(req, res){
    var userId = common.getUserId(req.user);
    oppManagementObj.removeSalseforceAccount(common.castToObjectId(userId),function(result){
        if(result){
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            });
        }
        else{
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            });
        }
    })
});

router.get('/salesforce/sync/new/edited/opportunities',function(req, res){
    var userId = common.getUserId(req.user);

    var filter = 'newAndEdited';
    if(req.query.filter){
        filter = req.query.filter;
    }

    getAndStoreOpportunities(userId, filter, function(err, result){
        if(err){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            });
        }
        else{
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            });
        }
    });
});

router.get('/salesforce/get/opportunities/by/contact',function(req,res){
    var userId = common.getUserId(req.user);
    var contactEmailId = req.query.contactEmailId ? req.query.contactEmailId : null;
    var contactMobile = req.query.contactMobile ? req.query.contactMobile : null;
    oppManagementObj.getOpportunitiesByContact(common.castToObjectId(userId),contactEmailId,contactMobile,function(error,opportunities){
        if(error){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data: []
            });
        }
        else{
            opportunities.sort(function(a,b){
                var nameA = a.opportunityName.toUpperCase();
                var nameB = b.opportunityName.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            });

            var totalPipelineVal = 0;
            var tpvLost = 0;
            var tpvWon = 0;
            _.each(opportunities,function (opp) {
                if(!opp.isClosed || !opp.isWon){
                    totalPipelineVal += opp.amount;
                }

                if(opp.isWon){
                    tpvWon += opp.amount;
                }

                if(!opp.isWon && opp.isClosed){
                    tpvLost += opp.amount;
                }
            });

            savePipelineValue(totalPipelineVal,common.castToObjectId(userId),contactEmailId,'inProcess');
            savePipelineValue(tpvLost,common.castToObjectId(userId),contactEmailId,'lost');
            savePipelineValue(tpvWon,common.castToObjectId(userId),contactEmailId,'won');

            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: opportunities
            });
        }
    })
});

router.post('/salesforce/add/new/opportunity/for/contact',function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    var contactEmailId = req.body.contactEmailId;
    var contactMobile = req.body.contactMobile;

    var opp  = req.body.opportunity;

    var geoLocation  = {
        zone:req.body.zone?req.body.zone:null,
        town:req.body.town?req.body.town:null,
        lat:opp.geoLocation && opp.geoLocation.lat?opp.geoLocation.lat:null,
        lng:opp.geoLocation && opp.geoLocation.lng?opp.geoLocation.lng:null
    };

    var opportunitiesObj = [];

    // userManagementObj.selectUserProfileCustomQuery({_id:common.castToObjectId(userId)},{emailId:1,companyId:1}, function(error, user){
    common.getProfileOrStoreProfileInSession(common.castToObjectId(userId),req,function (user) {
        var error = 0;

        if(error){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
        else{

            var opportunity = {
                salesforceContactId : null,
                opportunityId : String(mongoose.Types.ObjectId()),
                opportunityName : opp.name,
                amount : parseFloat(opp.amount),
                isClosed : opp.isClosed,
                userEmailId: opp.userEmailId,
                createdByEmailId: user.emailId,
                isWon : opp.isWon,
                stageName : opp.stage,
                relatasStage: opp.stage,
                closeDate:new Date(opp.closeDate),
                createdDate: opp.createdDate? new Date(opp.createdDate):new Date(),
                influencers: opp.influencers?opp.influencers:[],
                partners: opp.partners?opp.partners:[],
                decisionMakers: opp.decisionMakers?opp.decisionMakers:[],
                productType: opp.productType,
                vertical:opp.vertical?opp.vertical:"",
                xr:opp.xr,
                currency:opp.currency,
                solution:opp.solution,
                businessUnit:opp.businessUnit,
                type:common.checkRequired(opp.type)?opp.type:"New",
                netGrossMargin:opp.netGrossMargin || opp.netGrossMargin === 0?parseFloat(opp.netGrossMargin):100,
                BANT:{
                    budget:opp.BANT?opp.BANT.budget:false,
                    authority:opp.BANT?opp.BANT.authority:false,
                    need:opp.BANT?opp.BANT.need:false,
                    time:opp.BANT?opp.BANT.time:false
                },
                competitor:opp.competitor?opp.competitor:"",
                closeReasons:opp.closeReasons,
                sourceType:opp.sourceType,
                closeReasonDescription:opp.closeReasonDescription,
                usersWithAccess: opp.usersWithAccess?opp.usersWithAccess:[],
                renewed: opp.renewed?opp.renewed:{
                    amount:0,
                    netGrossMargin:100,
                    closeDate:null,
                    createdDate:null
                },
                accounts: opp.accounts?opp.accounts:[],
                masterData: opp.masterData?opp.masterData:[],
                renewalStatusSet:opp.renewalStatusSet
            };

            var account = common.fetchCompanyFromEmail(contactEmailId);
            oppManagementObj.findOpportunityCustomQuery({contactEmailId:new RegExp('@'+account, "i")},function (err,opps) {

                if(user && user.companyId){
                    opportunity.companyId = common.castToObjectId(String(user.companyId));
                } else {
                    opportunity.companyId= null;
                }

                opportunitiesObj.push(buildOpportunitiesObj(common.castToObjectId(userId), user.emailId, contactEmailId, contactMobile, opportunity, 'relatas',geoLocation))

                if(_.includes(["Close Won","Close Lost"], opportunitiesObj[0].stageName)){
                    opportunitiesObj[0].closeDate = new Date()
                }

                if(opportunitiesObj && opportunitiesObj[0] && req.body.renewalCloseDate && opp.renewThisOpp && _.includes(["Close Won"], opportunitiesObj[0].stageName)) {
                    opportunitiesObj[0].renewed = {
                        amount: parseFloat(req.body.renewalAmount),
                        netGrossMargin: opportunitiesObj[0].netGrossMargin,
                        closeDate: new Date(req.body.renewalCloseDate),
                        createdDate: new Date(),
                    }
                }

                salesforceApiObj.insertOrUpdateOpportunities(opportunitiesObj, function (isUpdated) {

                    oppManagementObj.updateMetaData(common.castToObjectId(user.companyId),opportunitiesObj[0].userEmailId,opportunitiesObj[0],function () {

                        if(isUpdated && opportunitiesObj && opportunitiesObj[0] && req.body.renewalCloseDate && opp.renewThisOpp && _.includes(["Close Won"], opportunitiesObj[0].stageName)){

                            opportunitiesObj[0].renewed = {
                                amount:parseFloat(req.body.renewalAmount),
                                netGrossMargin:opportunitiesObj[0].netGrossMargin,
                                closeDate:new Date(req.body.renewalCloseDate),
                                createdDate:new Date(),
                            }

                            renewOpportunityCloseOnSameDay(userId
                                ,req.body.renewalAmount
                                ,req.body.renewalCloseDate
                                ,opportunitiesObj[0]
                                ,opportunitiesObj[0].opportunityId
                                ,function () {
                                    clearCache(String(user.companyId)+"oppsForReports");
                                });
                        } else {

                        }
                    })

                    oppManagementObj.createLogForCreation(common.castToObjectId(userId),user.emailId,opportunity.opportunityId,"created");

                    if(opportunitiesObj[0].stageName == "Close Won" || opportunitiesObj[0].stageName == "Close Lost"){
                        oppManagementObj.createLogForCreation(common.castToObjectId(userId),user.emailId,opportunity.opportunityId,"close");
                    }
                    oppManagementObj.createLogForOppStateChange(common.castToObjectId(userId),buildOppObjWithAuditableFields(opportunitiesObj[0].opportunityId),opportunitiesObj[0],function (newUsersAdded){

                    });

                    var contacts = [contactEmailId];

                    if(opportunity.partners.length>0){
                        contacts = contacts.concat(_.pluck(opportunity.partners,"emailId"))
                    }

                    if(opportunity.decisionMakers.length>0){
                        contacts = contacts.concat(_.pluck(opportunity.decisionMakers,"emailId"))
                    }

                    if(opportunity.influencers.length>0){
                        contacts = contacts.concat(_.pluck(opportunity.influencers,"emailId"))
                    }

                    // updateContactsLocation(common.castToObjectId(userId),contacts,geoLocation);

                    common.getCompanyHead(common.castToObjectId(req.body.hierarchyParent),common.castToObjectId(req.body.companyId),function (companyHead,reportingManager) {

                        if (isUpdated) {

                            // contactObj.updateRelationshipTypeByEmail(common.castToObjectId(userId),contactEmailId,"prospect_customer","prospect",function(err,isSuccess){
                            //     contactObj.updateFavoriteByEmailId(common.castToObjectId(userId),contactEmailId,true,function (err1,result) {
                            //
                            //     });
                            // });

                            clearCache(String(user.companyId)+"oppsForReports");

                            res.send({
                                SuccessCode: 1,
                                ErrorCode: 0,
                                Data:opportunitiesObj[0].opportunityId,
                                opportunity:opportunitiesObj[0],
                                companyHead:companyHead,
                                reportingManager:reportingManager
                            })
                        }
                        else{
                            res.send({
                                SuccessCode: 0,
                                ErrorCode: 1,
                                Data:null,
                                opportunity:null
                            })
                        }
                    })
                })

                if(!err && opps && account){
                    if(opps.length == 0){

                        var findQuery = {
                            companyId: common.castToObjectId(user.companyId),
                            name: new RegExp('@'+account, "i")
                        };

                        var updateObj = {
                            important:true
                        };

                        // accManagementObj.updateAccountDetails(findQuery,updateObj)
                    }
                }
            });
        }
    });

});

router.post('/salesforce/edit/opportunity/for/contact',function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var opp = req.body.opportunity;
    var opportunityId  = req.body.opportunity ? req.body.opportunity.opportunityId : null;
    var geoLocation  = {
        zone:req.body.zone?req.body.zone:null,
        town:req.body.town?req.body.town:null,
        lat:opp.geoLocation && opp.geoLocation.lat?opp.geoLocation.lat:null,
        lng:opp.geoLocation && opp.geoLocation.lng?opp.geoLocation.lng:null
    };

    common.getProfileOrStoreProfileInSession(common.castToObjectId(userId),req,function (userProfile) {
       if(userProfile){

           var renewed = opp.renewed?opp.renewed:{
               amount:0,
               netGrossMargin:100,
               closeDate:null,
               createdDate:null
           };

           if(opp.renewed && opp.renewed.amount && (opp.renewed.amount == "" || opp.renewed.amount == "null" || opp.renewed.amount == " ")){
               opp.renewed.amount = 0;
           }

           if(opp.renewed && opp.renewed.amount && opp.renewed.amount == "null"){
               opp.renewed.amount = 0;
           }

           var updateObj = {
               opportunityName : opp.name,
               amount : parseFloat(opp.amount),
               isClosed : opp.isClosed,
               isWon : opp.isWon,
               stageName : opp.stage,
               relatasStage: opp.stage,
               closeDate:new Date(opp.closeDate),
               notes:opp.notes,
               productType:opp.productType,
               contactEmailId:opp.contactEmailId,
               mobileNumber:opp.mobileNumber,
               vertical:opp.vertical?opp.vertical:"",
               netGrossMargin:opp.netGrossMargin || opp.netGrossMargin == 0?parseFloat(opp.netGrossMargin):100,
               BANT:{
                   budget:opp.BANT?opp.BANT.budget:false,
                   authority:opp.BANT?opp.BANT.authority:false,
                   need:opp.BANT?opp.BANT.need:false,
                   time:opp.BANT?opp.BANT.time:false
               },
               competitor:opp.competitor?opp.competitor:"",
               closeReasons:opp.closeReasons,
               closeReasonDescription:opp.closeReasonDescription,
               influencers: opp.influencers?opp.influencers:[],
               partners: opp.partners?opp.partners:[],
               sourceType:opp.sourceType,
               xr:opp.xr,
               currency:opp.currency,
               solution:opp.solution,
               businessUnit:opp.businessUnit,
               type:common.checkRequired(opp.type)?opp.type:"New",
               renewalStatusSet:opp.renewalStatusSet,
               userEmailId:opp.userEmailId,
               userId:common.castToObjectId(opp.userId),
               decisionMakers: opp.decisionMakers?opp.decisionMakers:[],
               usersWithAccess: opp.usersWithAccess?opp.usersWithAccess:[],
               accounts: opp.accounts?opp.accounts:[],
               masterData: opp.masterData?opp.masterData:[],
               renewed: renewed
           };

           if((opp.geoLocation && opp.geoLocation.town && opp.geoLocation.zone) || (opp.geoLocation && opp.geoLocation.zone) || (opp.geoLocation && opp.geoLocation.town)) {
               updateObj.geoLocation = opp.geoLocation
           } else {
               updateObj.geoLocation = geoLocation
           }

           var usersWithAccess = [updateObj.userEmailId];
           if(userProfile.emailId){
               usersWithAccess.push(userProfile.emailId)
           }

           if(opportunityId) {

               userManagementObj.selectUserProfileCustomQuery({_id:common.castToObjectId(userId)},{contacts:0}, function(error, user){

                   if(user && user.companyId){
                       updateObj.companyId = common.castToObjectId(String(user.companyId));
                   } else {
                       updateObj.companyId= null;
                   }

                   oppManagementObj.findOpportunityByIdForMultipleUsers([common.castToObjectId(userId),common.castToObjectId(opp.userId)], opportunityId, function (err, opportunity) {

                       var oppAccessible = false;

                       if(opportunity.userEmailId !== updateObj.userEmailId){
                           oppAccessible = true;
                       }

                       if(opportunity.stageName !== updateObj.stageName){
                           updateObj["lastStageUpdated"] = {
                               fromStage:opportunity.stageName,
                               date : new Date()
                           }
                       }

                       // if(opportunity.usersWithAccess && updateObj.usersWithAccess){
                       //     if(opportunity.usersWithAccess.length > updateObj.usersWithAccess.length){
                       //         var usersEmailIdsToRemove = _.difference(_.pluck(opportunity.usersWithAccess,"emailId"),_.pluck(updateObj.usersWithAccess,"emailId"));
                       //
                       //         var usersEmailIdsToRemoveObj = [];
                       //         _.each(usersEmailIdsToRemove,function (user) {
                       //             usersEmailIdsToRemoveObj.push({
                       //                 emailId:user
                       //             });
                       //         })
                       //
                       //         oppManagementObj.removeInternalTeamAccessForMultiUsers(usersEmailIdsToRemoveObj,userProfile.companyId,opportunityId)
                       //     }
                       // }

                       updateObj._id = opportunity._id
                       updateObj.createdDate = opportunity.createdDate

                       if(_.includes(["Close Won","Close Lost"], updateObj.stageName) && !_.includes(["Close Won","Close Lost"], opportunity.stageName)){
                           updateObj.closeDate = new Date()
                       }

                       if(userProfile && userProfile.corporateAdmin){
                           usersWithAccess.push(opportunity.userEmailId)
                       }

                       var contacts = [];
                       contacts.push(updateObj.contactEmailId)

                       if(updateObj.partners && updateObj.partners.length>0){
                           contacts = contacts.concat(_.pluck(updateObj.partners,"emailId"))
                       }

                       if(updateObj.decisionMakers && updateObj.decisionMakers.length>0){
                           contacts = contacts.concat(_.pluck(updateObj.decisionMakers,"emailId"))
                       }

                       if(updateObj.influencers && updateObj.influencers.length>0){
                           contacts = contacts.concat(_.pluck(updateObj.influencers,"emailId"))
                       }

                       if(updateObj.userEmailId != opportunity.userEmailId){

                           var contactsUpdateObj = [];
                           contactsUpdateObj.push({
                               contacts:contacts,
                               to:updateObj.userId,
                               emailId:updateObj.userEmailId
                           })

                           oppManagementObj.updateContacts(contactsUpdateObj,common,function () {

                           })
                       }

                       // updateContactsLocation(common.castToObjectId(userId),contacts,updateObj.geoLocation);

                       oppManagementObj.createLogForOppStateChange(common.castToObjectId(userId),opportunity,updateObj,function (newUsersAdded) {

                           if(!err && req.body.renewalCloseDate && opp.renewThisOpp && _.includes(["Close Won"], updateObj.stageName)){
                               updateObj.renewed = {
                                   amount:parseFloat(req.body.renewalAmount),
                                   netGrossMargin:updateObj.netGrossMargin,
                                   closeDate:new Date(req.body.renewalCloseDate),
                                   createdDate:new Date(),
                               }

                               renewOpportunity(userId
                                   ,req.body.renewalAmount
                                   ,req.body.renewalCloseDate
                                   ,updateObj
                                   ,opportunityId
                                   ,opportunity
                                   ,function () {
                                   clearCache(String(userProfile.companyId)+"oppsForReports");
                               });
                           }

                           if(req.body.companyId && req.body.hierarchyParent && _.includes(["Close Lost","Close Won"], updateObj.stageName)){

                               common.getCompanyHead(common.castToObjectId(req.body.hierarchyParent),common.castToObjectId(req.body.companyId),function (companyHead,reportingManager) {

                                   oppManagementObj.updateOpportunityForUsersWithAccess(common.castToObjectId(userId),usersWithAccess, opportunityId, updateObj,oppAccessible, function (err, result) {
                                       if (!err) {

                                           if(updateObj.stageName == "Close Lost" || updateObj.stageName == "Close Won"){
                                               // oppManagementObj.updateInteractionCountForClosingOpp([common.castToObjectId(userId)],[updateObj],function (err3,result) {
                                               //
                                               // });
                                               sendNotification(updateObj, opportunityId, req.body.hierarchyParent);
                                           }

                                           clearCache(String(userProfile.companyId)+"oppsForReports");

                                           res.send({
                                               SuccessCode: 1,
                                               ErrorCode: 0,
                                               Data:opp._id,
                                               opportunity:updateObj,
                                               companyHead:companyHead,
                                               reportingManager:reportingManager
                                           })
                                       }
                                       else {
                                           res.send({
                                               SuccessCode: 0,
                                               ErrorCode: 1
                                           })
                                       }
                                   });

                               });

                           } else {

                               oppManagementObj.updateOpportunityForUsersWithAccess(common.castToObjectId(userId),usersWithAccess, opportunityId, updateObj,oppAccessible, function (err, result) {

                                   if(updateObj.stageName == "Close Lost" || updateObj.stageName == "Close Won"){
                                       // oppManagementObj.updateInteractionCountForClosingOpp([common.castToObjectId(userId)],[updateObj],function (err4,result) {
                                       // });
                                   }

                                   clearCache(String(userProfile.companyId)+"oppsForReports");

                                   if (!err) {
                                       res.send({
                                           SuccessCode: 1,
                                           ErrorCode: 0,
                                           Data:opp._id
                                       })
                                   }
                                   else {
                                       res.send({
                                           SuccessCode: 0,
                                           ErrorCode: 1
                                       })
                                   }
                               });
                           }

                           if(newUsersAdded.length>0){

                               if(opportunity.userEmailId !== updateObj.userEmailId){
                                   newUsersAdded.push(updateObj.userEmailId)
                               }

                               newUsersAdded = _.uniq(newUsersAdded);

                               sendMailToNewUsersForAccess(userId,newUsersAdded,updateObj.opportunityName,updateObj.usersWithAccess,user)
                           }
                       });
                   })
               })
           }
           else{
               res.send({
                   SuccessCode: 0,
                   ErrorCode: 1
               })
           }
       }  else {
           res.send({
               SuccessCode: 0,
               ErrorCode: 1
           })
       }
    });
});

function clearCache(key){
    redisClient.del(key);
}

function sendNotification(opportunity, opportunityId, hierarchyParent) {
    userManagementObj.selectUserProfileCustomQuery({_id:common.castToObjectId(hierarchyParent)},{mobileFirebaseToken:1, webFirebaseSettings:1, companyId:1, _id:1, emailId:1}, function(error, user){
        if(!error && user && (user.mobileFirebaseToken || user.webFirebaseSettings.firebaseToken) ) {
            var notificationBody, notificationTitle;
            if(opportunity.stageName == "Close Won") {
                notificationBody = opportunity.userEmailId + " has won opportunity " + " worth " + opportunity.currency + " " + common.numberWithCommas(opportunity.amount, false);
                notificationTitle = "Opp Won: " + opportunity.opportunityName;
            
            } else {
                notificationBody = opportunity.userEmailId + " has lost opportunity " + " worth " + opportunity.currency + " " + common.numberWithCommas(opportunity.amount, false);
                notificationTitle = "Opp Lost: " + opportunity.opportunityName;

            }

            var payload = {
                notification: {
                    body: notificationBody,
                    title: notificationTitle,
                    click_action: "MY_ACTION"
                },
                data: {
                    category: "oppClose",
                    click_action: "/opportunities/all?opportunityId="+opportunityId
                }
            };
            common.sendNotification(payload, user.webFirebaseSettings.firebaseToken, user.mobileFirebaseToken, user.companyId, user._id, user.emailId, function() {
            })
        }

    })

}

function updateContactsLocation(userId,contacts,location,callback){

    if(location.lat || location.lng){

        var updateObj = {
            'contacts.$.location':location.town?location.town:null,
            'contacts.$.lat':location.lat?location.lat:null,
            'contacts.$.lng':location.lng?location.lng:null
        }

        userManagementObj.findContactsByEmailIdArray(userId,contacts,function (err,result) {

            var contactsToUpdate = [];

            if(result && result.length>0){
                _.each(result,function (el) {
                    if(!el.location || (el.location && (el.location == "" || el.location == " "))){
                        contactsToUpdate.push(el.personEmailId);
                    }
                })
            }

            if(contactsToUpdate.length>0){
                contactsToUpdate = _.uniq(contactsToUpdate);

                contactObj.updateContactDetailsMulti(userId,contactsToUpdate,updateObj,function(err,result){
                    contactObj.updateLocation(userId,contactsToUpdate,location.lat, location.lng,location.town);
                });
            }
        })
    }
}

function renewOpportunity(userId,renewalAmount,renewalCloseDate,opp,sourceOpportunityId,opportunityOld,callback){
    var opportunity = [];
    var renewalOppObj = _.cloneDeep(opp);

    if(!callback){
        callback = opportunityOld
    }

    renewalOppObj["_id"] = null;
    renewalOppObj["closeDate"] = new Date(renewalCloseDate);
    renewalOppObj["amount"] = parseFloat(renewalAmount);
    renewalOppObj["stageName"] = "Prospecting";
    renewalOppObj["relatasStage"] = "Prospecting";
    renewalOppObj["type"] = "Renewal";
    renewalOppObj["createdByEmailId"] = opp.userEmailId;
    renewalOppObj["opportunityId"] = String(mongoose.Types.ObjectId());
    renewalOppObj["sourceOpportunityId"] = String(sourceOpportunityId);

    renewalOppObj['renewed'] = {
        amount:0,
        netGrossMargin:100,
        closeDate:null,
        createdDate:null
    }

    renewalOppObj["renewalStatusSet"] = false;

    opportunity.push(renewalOppObj);

    opportunity.forEach(function (el) {

        el["createdDate"] = new Date();
        el["closeDate"] = new Date(el.closeDate);
        el["userId"] = common.castToObjectId(opp.userId);
        el["companyId"] = common.castToObjectId(opp.companyId);

        delete el["_id"];
    });

    var companyId = opp.companyId;

    clearCache(String(companyId)+"oppsForReports");

    getCompanyOppRenewalTypeAndFirstStage(companyId,function(renewalType,firstStage){
        renewalOppObj["type"] = renewalType;
        renewalOppObj["stageName"] = firstStage;
        renewalOppObj["relatasStage"] = firstStage;

        salesforceApiObj.insertOrUpdateOpportunities(opportunity, function (isUpdated) {
            oppManagementObj.updateMetaData(common.castToObjectId(companyId),opportunity[0].userEmailId,opportunity[0])

            oppManagementObj.createLogForCreation(common.castToObjectId(userId), opportunity[0].userEmailId, renewalOppObj.opportunityId, "Renewal")
            if(callback){
                callback()
            }
        });
    });
}

function renewOpportunityCloseOnSameDay(userId,renewalAmount,renewalCloseDate,opp,sourceOpportunityId,callback){
    var opportunity = [];
    var renewalOppObj = opp;

    renewalOppObj["closeDate"] = new Date(renewalCloseDate);
    renewalOppObj["amount"] = parseFloat(renewalAmount);
    renewalOppObj["stageName"] = "Prospecting";
    renewalOppObj["relatasStage"] = "Prospecting";
    renewalOppObj["type"] = "Renewal";
    renewalOppObj["createdByEmailId"] = opp.userEmailId;
    renewalOppObj["opportunityId"] = String(mongoose.Types.ObjectId());
    renewalOppObj["sourceOpportunityId"] = String(sourceOpportunityId);

    renewalOppObj['renewed'] = {
        amount:0,
        netGrossMargin:100,
        closeDate:null,
        createdDate:null
    }

    renewalOppObj["renewalStatusSet"] = false;

    opportunity.push(renewalOppObj);

    opportunity.forEach(function (el) {

        el["createdDate"] = new Date();
        el["closeDate"] = new Date(el.closeDate);
        el["userId"] = common.castToObjectId(el.userId);

        delete el["_id"];
    });

    var companyId = opportunity[0].companyId;

    getCompanyOppRenewalTypeAndFirstStage(companyId,function(renewalType,firstStage){
        renewalOppObj["type"] = renewalType;
        renewalOppObj["stageName"] = firstStage;
        renewalOppObj["relatasStage"] = firstStage;

        salesforceApiObj.insertOrUpdateOpportunities(opportunity, function (isUpdated) {
            clearCache(String(companyId)+"oppsForReports");
            setTimeout(function(){
                oppManagementObj.updateMetaData(common.castToObjectId(companyId),opportunity[0].userEmailId,opportunity[0]);
                oppManagementObj.createLogForCreation(common.castToObjectId(userId), opportunity[0].userEmailId, renewalOppObj.opportunityId, "Renewal")
                if(callback){
                    callback()
                }
            }, 2000);
        });
    });
}

function getCompanyOppRenewalTypeAndFirstStage(companyId,callback){

    var renewalType = "Renewal" //Fallback;
    var firstStage = "Prospecting" //Fallback;

    if(companyId){
        companyCollection.findOne({_id:common.castToObjectId(companyId)},{typeList:1,opportunityStages:1}).exec(function (err,company) {

            if(!err && company && company.typeList){

                _.each(company.typeList,function (el) {
                    if(el.isDefaultRenewalType){
                        renewalType = el.name
                    }
                });
            }
            if(!err && company && company.opportunityStages){
                _.each(company.opportunityStages,function (el) {
                    if(el.order === 1){
                        firstStage = el.name;
                    }
                })
            }

            callback(renewalType,firstStage)
        })
    } else {
        callback(renewalType,firstStage)
    }
}

router.get('/salesforce/get/pipeline/value/by/contact',function(req,res){
    var userId = common.getUserId(req.user);
    var contactEmailId = req.query.contactEmailId;
    oppManagementObj.getPipelineValueByContact(common.castToObjectId(userId),contactEmailId,function(error,result){
        if(error){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data: null
            })
        }
        else{
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: result
            })
        }
    })
});

router.get('/salesforce/get/upcoming/opportunities',function(req,res){
    var userId = common.getUserId(req.user);
    oppManagementObj.upcomingOpportunities(common.castToObjectId(userId),function(error,result){
        if(error){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data: null
            })
        }
        else{
            var timezone = 'UTC';
            if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                timezone = req.session.profile.timezone.name
            }

            var emailIds = _.pluck(result,'contactEmailId');

            contactObj.getContactsByEmailIdArray(common.castToObjectId(userId),emailIds,function (error,matchedContacts) {

                if(!error && matchedContacts && matchedContacts.length>0){
                    
                    result.forEach(function (oppurtunity) {
                        for (var i = 0; i < matchedContacts[0].contacts.length; i++) {

                            if (oppurtunity.contactEmailId === matchedContacts[0].contacts[i].personEmailId) {

                                oppurtunity._id = matchedContacts[0].contacts[i]._id;
                                oppurtunity.personId = matchedContacts[0].contacts[i].personId;
                                oppurtunity.personName = matchedContacts[0].contacts[i].personName;
                                oppurtunity.personEmailId = matchedContacts[0].contacts[i].personEmailId;
                                oppurtunity.companyName = matchedContacts[0].contacts[i].companyName;
                                oppurtunity.designation = matchedContacts[0].contacts[i].designation;
                                oppurtunity.mobileNumber = matchedContacts[0].contacts[i].mobileNumber;
                                oppurtunity.contactRelation = matchedContacts[0].contacts[i].contactRelation;
                                oppurtunity.favorite = matchedContacts[0].contacts[i].favorite;
                                oppurtunity.location = matchedContacts[0].contacts[i].location;
                                oppurtunity.pastMeetingLocation = matchedContacts[0].contacts[i].pastMeetingLocation;
                            }
                        }
                    });

                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data: result,
                        timezone:timezone
                    })

                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data: null
                    })
                }
            });
        }
    })
});

router.get('/salesforce/get/stale/opportunities',function(req,res){
    var userId = common.getUserId(req.user);
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile ? userProfile.companyId : null;

    oppManagementObj.staleOpportunities(common.castToObjectId(userId),null,null,null,common.castToObjectId(companyId),function(error,result){
        if(error){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data: null
            })
        }
        else{
            var timezone = 'UTC';
            if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                timezone = req.session.profile.timezone.name
            }

            var emailIds = _.pluck(result,'contactEmailId');

            contactObj.getContactsByEmailIdArray(common.castToObjectId(userId),emailIds,function (error,matchedContacts) {

                if(!error && matchedContacts && matchedContacts.length>0){

                    result.forEach(function (oppurtunity) {
                        for (var i = 0; i < matchedContacts[0].contacts.length; i++) {

                            if (oppurtunity.contactEmailId === matchedContacts[0].contacts[i].personEmailId) {

                                oppurtunity._id = matchedContacts[0].contacts[i]._id;
                                oppurtunity.personId = matchedContacts[0].contacts[i].personId;
                                oppurtunity.personName = matchedContacts[0].contacts[i].personName;
                                oppurtunity.personEmailId = matchedContacts[0].contacts[i].personEmailId;
                                oppurtunity.companyName = matchedContacts[0].contacts[i].companyName;
                                oppurtunity.designation = matchedContacts[0].contacts[i].designation;
                                oppurtunity.mobileNumber = matchedContacts[0].contacts[i].mobileNumber;
                                oppurtunity.contactRelation = matchedContacts[0].contacts[i].contactRelation;
                                oppurtunity.favorite = matchedContacts[0].contacts[i].favorite;
                                oppurtunity.location = matchedContacts[0].contacts[i].location;
                                oppurtunity.pastMeetingLocation = matchedContacts[0].contacts[i].pastMeetingLocation;
                            }
                        }
                    });
                    
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data: result,
                        timezone:timezone
                    })

                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data: null
                    })
                }
            });
        }
    })
});

router.get('/salesforce/get/today/opportunities',function(req,res){

    var dateMin = moment().add(0, "days");
    var dateMax = moment().add(0, "days");

    dateMin.date(dateMin.date());
    dateMin.hour(0);
    dateMin.minute(0);
    dateMin.second(0);
    dateMin.millisecond(0);

    dateMax.date(dateMax.date());
    dateMax.hour(23);
    dateMax.minute(59);
    dateMax.second(59);
    dateMax.millisecond(0);

    dateMin = dateMin.format()
    dateMax = dateMax.format()

    var userId = common.getUserId(req.user);
    oppManagementObj.todayOpportunities(common.castToObjectId(userId),dateMin,dateMax,function(error,result){
        if(error){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Data: null
            })
        }
        else{
            var timezone = 'UTC';
            if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                timezone = req.session.profile.timezone.name
            }

            var emailIds = _.pluck(result,'contactEmailId');

            contactObj.getContactsByEmailIdArray(common.castToObjectId(userId),emailIds,function (error,matchedContacts) {

                if(!error && matchedContacts && matchedContacts.length>0){

                    result.forEach(function (oppurtunity) {
                        for (var i = 0; i < matchedContacts[0].contacts.length; i++) {

                            if (oppurtunity.contactEmailId === matchedContacts[0].contacts[i].personEmailId) {

                                oppurtunity._id = matchedContacts[0].contacts[i]._id;
                                oppurtunity.personId = matchedContacts[0].contacts[i].personId;
                                oppurtunity.personName = matchedContacts[0].contacts[i].personName;
                                oppurtunity.personEmailId = matchedContacts[0].contacts[i].personEmailId;
                                oppurtunity.companyName = matchedContacts[0].contacts[i].companyName;
                                oppurtunity.designation = matchedContacts[0].contacts[i].designation;
                                oppurtunity.mobileNumber = matchedContacts[0].contacts[i].mobileNumber;
                                oppurtunity.contactRelation = matchedContacts[0].contacts[i].contactRelation;
                                oppurtunity.favorite = matchedContacts[0].contacts[i].favorite;
                                oppurtunity.location = matchedContacts[0].contacts[i].location;
                                oppurtunity.pastMeetingLocation = matchedContacts[0].contacts[i].pastMeetingLocation;
                            }
                        }
                    });

                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0,
                        Data: result,
                        timezone:timezone
                    })

                } else {
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data: null
                    })
                }
            });
        }
    })
});

router.post('/opportunities/add/people',common.isLoggedInUserOrMobile, function(req, res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    var type = req.body.type;
    var opportunityId = req.body.opportunityId;
    var contact = req.body.contact;

    var obj = {
        emailId:contact.emailId,
        fullName:contact.fullName,
        personId:contact.personId,
        contactId:contact._id
    };

    var updateObj = null;

    var updateObj2 = {
        'contacts.$.favorite':true
    }

    if(type === 'partners'){
        updateObj = { "partners": obj }
        updateObj2 = {
            'contacts.$.favorite':true,
            'contacts.$.contactRelation.partner':true,
        }
    } else if(type === 'decisionMakers'){
        updateObj = { "decisionMakers": obj }
        updateObj2 = {
            'contacts.$.favorite':true,
            'contacts.$.contactRelation.decision_maker':true
        }
    } else if(type === 'influencer' || type === 'influencers'){
        updateObj = { "influencers": obj }
        updateObj2 = {
            'contacts.$.favorite':true,
            'contacts.$.contactRelation.influencer':true
        }
    }

    oppManagementObj.findOpportunityByIdForMultipleUsers([],req.body.opportunityId2,function (err,oldOppState) {

        var oldValue = [],newValue = [];
        if(type === 'partners'){
            oldValue = oldOppState[type]
        } else if(type === 'decisionMakers'){
            oldValue = oldOppState[type]
        } else if(type === 'influencer' || type === 'influencers'){
            oldValue = oldOppState["influencers"]
        }

        myUserCollection.update({ _id: userId, contacts: { $elemMatch: { personEmailId: contact.emailId } } }, { $set: updateObj2 }, function(err, result) {
        })

        oppManagementObj.updateRelatedPeople(common.castToObjectId(userId),common.castToObjectId(opportunityId),updateObj,function (err,result) {
            oppManagementObj.findOpportunityByIdForMultipleUsers([],req.body.opportunityId2,function (err,newOppState) {
                if(type === 'partners'){
                    newValue = newOppState[type]
                } else if(type === 'decisionMakers'){
                    newValue = newOppState[type]
                } else if(type === 'influencer' || type === 'influencers'){
                    newValue = newOppState["influencers"]
                }

                oldValue = _.pluck(oldValue,"emailId")
                newValue = _.pluck(newValue,"emailId")

                oppManagementObj.createLogForPeopleAdded(common.castToObjectId(userId),req.body.opportunityId2,oldValue,newValue,type,type+"Added")
                if(err){
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data:err
                    });
                }
                else{
                    res.send({
                        SuccessCode: 1,
                        ErrorCode: 0
                    });
                }
            })
        });
    });

});

router.post('/opportunities/remove/it/from/access/control',common.isLoggedInUserToGoNext, function(req, res) {
    var userId = common.getUserId(req.user);
    var opportunityId = req.body.opportunityId;
    common.getProfileOrStoreProfileInSession(common.castToObjectId(userId),req,function (userProfile) {
        oppManagementObj.removeInternalTeamAccess(userProfile.companyId,opportunityId,function (response) {
            res.send(true);
        });
    });
});

router.post('/opportunities/remove/people',common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    var type = req.body.type;
    var opportunityId = req.body.opportunityId;
    var contact = req.body.contact;

    var obj = {
        emailId:contact.emailId
    };

    var updateObj = null;

    if(type === 'partner'){
        updateObj = { "partners": obj }
    } else if(type === 'decisionMaker'){
        updateObj = { "decisionMakers": obj }
    } else if(type === 'influencer' || type === 'influencers'){
        updateObj = { "influencers": obj }
    }

    oppManagementObj.findOpportunityByIdForMultipleUsers([],req.body.opportunityId2,function (err,oldOppState) {

        var oldValue = [], newValue = [];
        if (type === 'partners' || type === 'partner') {
            oldValue = oldOppState['partners']
        } else if (type === 'decisionMakers' || type === 'decisionMaker') {
            oldValue = oldOppState['decisionMakers']
        } else if (type === 'influencer' || type === 'influencers') {
            oldValue = oldOppState["influencers"]
        }

        oppManagementObj.removeRelatedPeople(common.castToObjectId(userId),common.castToObjectId(opportunityId),updateObj,function (err,result) {
            oppManagementObj.findOpportunityByIdForMultipleUsers([],req.body.opportunityId2,function (err,newOppState) {

                if (type === 'partners' || type === 'partner') {
                    newValue = newOppState['partners']
                } else if (type === 'decisionMakers' || type === 'decisionMaker') {
                    newValue = newOppState['decisionMakers']
                } else if (type === 'influencer' || type === 'influencers') {
                    newValue = newOppState["influencers"]
                }

                oldValue = _.pluck(oldValue,"emailId")
                newValue = _.pluck(newValue,"emailId")

                oppManagementObj.createLogForPeopleAdded(common.castToObjectId(userId),req.body.opportunityId2,oldValue,newValue,type,type+"Removed")
            });

            if(err){
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                    Data:err
                });
            }
            else{
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0
                });
            }
        });
    });


});

router.get('/opportunities/csv/to/json',common.isLoggedInUserOrMobile,function (req,res) {

    socialManagement.csvToJSON('./public/navneet.csv', function (err, result) {
        var opportunitiesObj = [];
        var notes = [];
        var emailIds = _.uniq(_.compact(_.pluck(result,"userEmailId")));

        var allLowerCase = [];
        _.each(emailIds,function (el) {
            allLowerCase.push(el.toLowerCase())
        });

        userManagementObj.getUserInfoByArrayOfEmailIdWithCustomFields(allLowerCase,{_id:1,emailId:1,firstName:1,lastName:1},function (err,userData) {

            _.each(result,function (r) {
                _.each(userData,function (user) {
                    if(user.emailId == r.userEmailId.toLowerCase()){
                        r["userId"] = user._id
                    }
                });
            });

            _.each(result,function (opp) {

                var geoLocation  = {
                    zone:opp.zone?opp.zone:null,
                    town:opp.city?opp.city:null
                };

                var partners = [],decisionMakers = [],influencers = [];
                if(opp.partner){
                    partners = {
                        emailId:opp.partner.toLowerCase()
                    }
                }

                if(opp.decisionMaker){
                    decisionMakers = {
                        emailId:opp.decisionMaker.toLowerCase()
                    }
                }

                if(opp.influencer){
                    influencers = {
                        emailId:opp.influencer.toLowerCase()
                    }
                }

                var isClosed = opp.stageName && (opp.stageName.toLowerCase() == 'won' || opp.stageName.toLowerCase() == 'lost')?true:false;
                var isWon = opp.stageName && (opp.stageName.toLowerCase() == 'won' || opp.stageName.toLowerCase() == 'lost')?true:false;

                var opportunity = {
                    salesforceContactId : null,
                    opportunityId : String(mongoose.Types.ObjectId()),
                    opportunityName : opp.opportunityName,
                    productType: capitalize(opp.product),
                    amount : parseFloat(opp.amount),
                    isClosed : opp.stageName && (opp.stageName.toLowerCase() == 'won' || opp.stageName.toLowerCase() == 'lost')?true:false,
                    isWon : opp.stageName && (opp.stageName.toLowerCase() == 'won' || opp.stageName.toLowerCase() == 'lost')?true:false,
                    stageName : isClosed?'Close '+capitalize(opp.stageName):capitalize(opp.stageName),
                    relatasStage: isClosed?'Close '+capitalize(opp.stageName):capitalize(opp.stageName),
                    closeDate:new Date(momentTZ(opp.closeDate).tz("Asia/Kolkata").format()),
                    createdDate: new Date(momentTZ(opp.createdDate).tz("Asia/Kolkata").format()),
                    partners:partners,
                    decisionMakers:decisionMakers,
                    influencers:influencers
                };

                var note = {
                    "conversationId" : new customObjectId(),
                    "messageReferenceType" : "contact",
                    "messageReferenceId" : common.castToObjectId(opp.userId),
                    "messageOwner" : {
                        "fullName" : "",
                        "emailId" : opp.userEmailId.toLowerCase(),
                        "userId" : common.castToObjectId(opp.userId)
                    },
                    "text" : opp.notes,
                    "emailId" : opp.contactEmailId.toLowerCase(),
                    "userId" : null,
                    "date" : opportunity.createdDate
                }
                
                notes.push(note)
                opportunitiesObj.push(buildOpportunitiesObj(common.castToObjectId(opp.userId), opp.userEmailId.toLowerCase(), opp.contactEmailId.toLowerCase(), null, opportunity, 'relatas',geoLocation))

            });

            res.send(opportunitiesObj)


        });
    });
})

router.get('/opportunities/all',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/index')
});

router.get('/opportunity/transferOpp',common.isLoggedInUserToGoNext, function(req, res){
    res.render('opportunity/transferOpp')
});

function capitalize(str) {

    if(str){

        return str.replace(/\b\w/g, function(l){
            return l.toUpperCase()
        })
    } else {
        return null
    }
}

function getAndStoreOpportunities(userId, filter, callback){
    /*
        is a module  - will be called when
            1. user login to relatas to updates the new or edited opportunities
            2. user add salesforce account to insert all opps

        steps -
            1. get all opportunities based on filter(all - first login to salesforce, newAndEdited - user login to relatas)
            2. add non existing contacts to relatas account
            3. upserts all opportunities
     */
    userManagementObj.selectUserProfileCustomQuery({_id:common.castToObjectId(userId)},{emailId:1,salesforce:1,companyId:1}, function(error, user){
        if(error){
            callback(error, null);
        }
        else{
            getOpportunities(user, filter, function(err, opprtunities, emailIds, contactDetails){

                if(err){
                    callback(err, null);
                }
                else
                {
                    checkAndAddContacts(common.castToObjectId(userId),user.emailId, emailIds, contactDetails, function (contactsAdded) {
                        salesforceApiObj.insertOrUpdateOpportunities(opprtunities, function (isUpdated) {
                            if (isUpdated) {
                                userManagementObj.updateUserSalesforceLastSync(userId, function (error, result) {
                                    callback(null, true)
                                });
                            }
                            else{
                                callback(true, null);
                            }
                        })
                    });
                }
            });
        }
    });
}

function getOpportunities(user, filter, callback){
    /*
        steps -
            1. get opportunities with ids from salesforce based on filter(all, newAndEdited)
            2. map all oppIds with associated contacts on salesforce and get contact ids
            3. by contact ids get contact info (email, name)

        return -
            1. error if any
            2. opportunities objects suitable to store in opportunities collection
            3. array of mailIds from salesforce to check for contact existence
            4. contact details
     */

    var userId = user._id;
    var userEmailId = user.emailId;
    var refreshToken = user.salesforce.refreshToken;
    var lastSync = user.salesforce.lastSync;

    salesforceApiObj.getAccessToken(userId,refreshToken, function(error, r){
        if(!r ||(r.access_token && error)){
            callback(error, null, null)
            //{"error":"invalid_grant","error_description":"expired access/refresh token"}
        }
        else {

            salesforceApiObj.getAndUpdateStageNames(r,function (stages) {

                if(user.companyId && stages && stages.length>0){
                    updateCompanyStages(common.castToObjectId(user.companyId.toString()),stages,function () {

                    });
                }

            });
            
            salesforceApiObj.getSalesforceOpportunitiesbyFilter(r,lastSync,filter, function(error, sfOpportunities){

                if(error){
                    callback(error, null, null);
                }
                else{

                    var opportunities = [],oppIds= [];

                    sfOpportunities.forEach(function(el){
                        el.opportunityId = el.Id;
                        delete el.Id;
                        delete el.attributes;

                        if(el.OwnerId == user.salesforce.id){
                            opportunities.push(el)
                            oppIds.push(el.opportunityId)
                        }
                    });

                    salesforceApiObj.getSalesforceOpportunityContactRoleByOppIds(r,oppIds, function(error, oppConRole){
                       if(error){
                           callback(error, null, null);
                       }
                        else{
                           var contactIds = []
                           oppConRole.forEach(function(a){
                               contactIds.push(a.ContactId)
                               delete a.attributes;
                           });

                           var mappedOpp = _.map(oppConRole, function (a) {
                               return _.extend(a, _.find(opportunities, function (b) {
                                   return b.opportunityId === a.OpportunityId;
                               }))
                           });

                           salesforceApiObj.getSalesforceContactsByIds(r,contactIds, function(error, contacts){
                              if(error){
                                  callback(error, null, null);
                              }
                              else{
                                  contacts.forEach(function(a){
                                      a.Email = a.Email ? a.Email.toLowerCase() : null;

                                      delete a.attributes;
                                  });
                                  var oppToAdd = _.map(mappedOpp, function (a) {
                                      return _.extend(a, _.find(contacts, function (b) {
                                          return b.Id === a.ContactId;
                                      }))
                                  });
                              }
                               var opportunitiesObj = [];
                               var contactDetails = [];
                               oppToAdd.forEach(function(a){

                                   var contactEmailId = a.Email ? a.Email.toLowerCase() : null;
                                   var opportunity ={
                                       salesforceContactId : a.ContactId,
                                       opportunityId : a.OpportunityId,
                                       opportunityName : a.Name,
                                       amount : a.Amount ? a.Amount : 0,
                                       isClosed : a.IsClosed,
                                       isWon : a.IsWon,
                                       stageName : a.StageName,
                                       closeDate: new Date(a.CloseDate),
                                       createdDate: new Date(a.CreatedDate),
                                       // relatasStage:mapStageToRelatasStage(a.StageName)
                                       relatasStage:a.StageName
                                   };

                                   if(contactEmailId) {
                                       opportunitiesObj.push(buildOpportunitiesObj(userId, userEmailId, contactEmailId, null, opportunity, 'salesforce',{town:null,zone:null}))
                                   }
                               })
                               var emailIds = _.uniq(_.compact(_.pluck(contacts,"Email")));
                               emailIds.forEach(function(a){
                                   contacts.forEach(function(b){
                                       if(a == b.Email){
                                           contactDetails.push({
                                                   emailId:b.Email,
                                                   firstName:b.FirstName,
                                                   lastName:b.LastName
                                               });
                                       }
                                   });
                               });

                               updateCompanyWithNewStages(listOfStages,user.companyId)

                               callback(null, opportunitiesObj, emailIds, contactDetails);
                           });
                       }
                    });
                }
            });
        }
    });
}

function updateCompanyWithNewStages(listOfStages,companyId) {

}

function checkAndAddContacts(userId,userEmailId,emailsFromSalesforce, contactDetails, callback){

    checkUserProfileForContactsAndStore(userId,userEmailId,contactDetails,emailsFromSalesforce,function(result){
        if(result){
            callback(true);
        }
        else{
            callback(false);
        }
    });
}

function updateCompanyStages(companyId,stages,callback){

    companyCollection.findOne({_id:companyId},{opportunityStages:1},function (err,existingStages) {

        var maxOrder = 0;

        if(existingStages && existingStages.opportunityStages && existingStages.opportunityStages.length>0){

            var eStages = _.pluck(existingStages.opportunityStages,"name");
            var stagesToAdd = _.difference(stages, eStages);

            var eMaxOrder = _.max(existingStages.opportunityStages,"order");

            if(eMaxOrder){
                maxOrder = eMaxOrder
            }

            addStages(companyId,stagesToAdd,maxOrder)

        } else {
            addStages(companyId,stages,maxOrder)
        }
    })
}

function addStages(companyId,stages,maxOrder,callback){

    if(stages && stages.length>0){
        var updateArray = [];

        _.each(stages,function (st,index) {
            updateArray.push({
                name: st,
                isStart: false,
                isEnd: false,
                isWon: false,
                isLost: false,
                weight:parseFloat(maxOrder)+10,
                order:parseInt(maxOrder+index+1)
            })
        })

        companyCollection.update({_id:companyId},{$addToSet:{opportunityStages:{ $each:updateArray}}},{upsert:true},function (err,result) {

            if(callback){
                callback(err,result)
            }
        })
    } else {
        if(callback){
            callback(null,null)
        }
    }
}

function buildOpportunitiesObj(userId,userEmailId,contactEmailId,contactMobile,opportunity,source,geoLocation){
    var obj = {
        userId: userId,
        userEmailId: opportunity.userEmailId?opportunity.userEmailId:userEmailId,
        contactEmailId: contactEmailId,
        salesforceContactId: opportunity.salesforceContactId,
        opportunityId:opportunity.opportunityId,
        createdByEmailId:opportunity.createdByEmailId,
        opportunityName:opportunity.opportunityName,
        amount:opportunity.amount,
        isClosed:opportunity.isClosed,
        isWon: opportunity.isWon,
        stageName:opportunity.stageName,
        mobileNumber:contactMobile ? contactMobile : null,
        source: source,
        relatasStage: opportunity.relatasStage,
        createdDate:opportunity.createdDate,
        closeDate:opportunity.closeDate,
        geoLocation:geoLocation,
        influencers: opportunity.influencers || [],
        partners: opportunity.partners || [],
        decisionMakers: opportunity.decisionMakers || [],
        productType: opportunity.productType || null,
        vertical:opportunity.vertical || null,
        netGrossMargin:opportunity.netGrossMargin || opportunity.netGrossMargin === 0?parseFloat(opportunity.netGrossMargin):100,
        xr:opportunity.xr,
        currency:opportunity.currency,
        solution:opportunity.solution,
        businessUnit:opportunity.businessUnit,
        type:opportunity.type,
        BANT:{
            budget:opportunity.BANT?opportunity.BANT.budget:null,
            authority:opportunity.BANT?opportunity.BANT.authority:null,
            need:opportunity.BANT?opportunity.BANT.need:null,
            time:opportunity.BANT?opportunity.BANT.time:null
        },
        competitor:opportunity.competitor || null,
        closeReasons:opportunity.closeReasons,
        sourceType:opportunity.sourceType,
        companyId:opportunity.companyId || null,
        closeReasonDescription:opportunity.closeReasonDescription,
        usersWithAccess:opportunity.usersWithAccess,
        accounts: opportunity.accounts,
        renewed: opportunity.renewed,
        masterData: opportunity.masterData || [],
        renewalStatusSet: opportunity.renewalStatusSet
    }

    return obj;
}

function mapStageToRelatasStage(stage){
    var newStage = null;
    var prospect = ['Qualification','Prospecting'];
    var eval  = ['Needs Analysis', 'Perception Analysis', 'Value Proposition', 'Id. Decision Makers'];
    var proposal = ['Negotiation/Review', 'Proposal/Price Quote'];
    var closedWon = ['Closed Won'];
    var closedLost  = ['Closed Lost'];

    var results = [];
    results.push({isTrue:_.includes(prospect, stage), value:'Prospecting'})
    results.push({isTrue:_.includes(eval, stage), value:'Evaluation'})
    results.push({isTrue:_.includes(proposal, stage), value:'Proposal'})
    results.push({isTrue:_.includes(closedWon, stage), value:'Close Won'})
    results.push({isTrue:_.includes(closedLost, stage), value:'Close Lost'})

    results.forEach(function(a){
        if(a.isTrue){
            newStage = a.value;
        }
    })

    return newStage;
}

function checkUserProfileForContactsAndStore(userId,userEmailId,contactsToAdd,emailArr,callback) {
    var query = {"emailId":{$in:emailArr}};
    var projection = {emailId:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,companyName:1,designation:1,mobileNumber:1,skypeId:1,location:1};

    userManagementObj.selectUserProfilesCustomQuery(query,projection,function(error,users){
        _.each(contactsToAdd,function (contactToAdd) {
            _.each(users,function (user) {
                if(contactToAdd.emailId == user.emailId){
                    contactToAdd.userId = user._id;
                    contactToAdd.firstName = user.firstName;
                    contactToAdd.lastName = user.lastName;
                    contactToAdd.publicProfileUrl = user.publicProfileUrl;
                    contactToAdd.profilePicUrl = user.profilePicUrl;
                    contactToAdd.companyName =  common.fetchCompanyFromEmail(user.emailId);
                    contactToAdd.designation = user.designation;
                    contactToAdd.location = user.location;
                    contactToAdd.mobileNumber = user.mobileNumber;
                    contactToAdd.emailId = user.emailId;
                }
            });
        });

        var contactObjs = [];
        contactsToAdd.forEach(function(a){
            contactObjs.push(buildContactObj(a));
        })
        if(contactObjs.length > 0) {
            contactObj.addContactNotExist(common.castToObjectId(userId.toString()), userEmailId, contactObjs, [], emailArr, 'salesforce', false);
        }
        callback(true)
    });
}

function buildContactObj(contact){
    var contactObj = {
        personId: contact.userId ? contact.userId.toString() : null,
        personName: contact.firstName + ' ' + contact.lastName,
        personEmailId: contact.emailId.toLowerCase(),
        birthday: null,
        companyName: contact.companyName ? contact.companyName : null,
        designation: null,
        addedDate: new Date(),
        verified: false,
        relatasUser: contact.userId ? true : false,
        relatasContact: true,
        mobileNumber: contact.mobileNumber ? contact.mobileNumber : null,
        location: contact.location ? contact.location : null,
        source: 'salesforce',
        account: {
            name: common.fetchCompanyFromEmail(contact.emailId),
            selected: false,
            updatedOn: new Date()
        },
        isSalesforceContact:true
    }
    return contactObj;
}

function savePipelineValue(totalPipelineVal,userId,contactEmailId,type) {
    var obj = {
        type:type,
        value:totalPipelineVal,
        email:contactEmailId
    };

    contactObj.updateContactValue(userId,obj,function (result) {
    })
};

function sendMailToNewUsersForAccess(userId,emailIds,opportunityName,usersWithAccess,userProfile){

    var usersWithAccessObj = {};
    _.each(usersWithAccess,function (op) {
        usersWithAccessObj[op.emailId] = op
    });

    var message = "You have been given access to the following opportunity on Relatas. Please login at www.relatas.com to view details\n"+opportunityName;
    var signature = "\n\n---\n"+userProfile.firstName+" "+userProfile.lastName+"\n"+userProfile.designation+", "+userProfile.companyName+"\n"+"www.relatas.com/"+userProfile.publicProfileUrl+'\n\n Powered by Relatas';

    for (var i = 0;i<emailIds.length;i++) {
        (function (i) {

            if(usersWithAccessObj[i] && usersWithAccessObj[i].fullName){
                message = "Hi "+usersWithAccessObj[i].fullName+",\n\n"+message
            }

            var body = {
                email_cc:null,
                receiverEmailId:i,
                receiverName:"",
                message:message+signature,
                subject:"Access to opportunity on Relatas | "+opportunityName,
                receiverId:null,
                docTrack:true,
                trackViewed:true,
                remind:null,
                respSentTime:new Date(),
                isLeadTrack:false,
                newMessage:true
            }
            messageObj.sendEmail(userId,body,googleCalendar,gmailObj,officeOutlook)

        })(emailIds[i]);
    }
}

function buildOppObjWithAuditableFields(opportunityId){

    return {
        opportunityId:opportunityId,
        usersWithAccess:[],
        decisionMakers:[],
        influencers:[],
        partners:[]
    }

}

module.exports = {router:router, getAndStoreOpportunities:getAndStoreOpportunities};
