//**************************************************************************************
// File Name            : document
// Functionality        : Router for documents(Quotation/Purchase Order/Invoice)
// History              : First Version
//
//
//*******************************************************************************

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var _ = require("lodash");
var fs = require('fs');

var commonUtility = require('../../common/commonUtility');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var contactClass = require('../../dataAccess/contactsManagementClass');
var rabbitmq = require('../../common/rabbitmq');
var documentTemplatesManagementClass = require('../../dataAccess/documentTemplateManagement');
var companyModelClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var documentTemplatesCollection = require('../../databaseSchema/documentTemplateSchema').documentTemplates;
var MasterData = require('../../dataAccess/masterData');

var companyModelClassObj = new companyModelClass();
var documentTemplatesManagementClassObj = new documentTemplatesManagementClass();

var documentManagementClass = require('../../dataAccess/documentManagement');
var documentCollection = require('../../databaseSchema/documentSchema').documentModel;
var documentsManagementClassObj = new documentManagementClass();
var masterAccObj = new MasterData();

var common = new commonUtility();
var rabbitmqObj = new rabbitmq();
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var statusCodes = errorMessagesObj.getStatusCodes();

// ui routes
router.get('/documents/show/all',common.isLoggedInUserToGoNext, function(req, res){
    res.render('documents/corporateAdmin/showAllDocs');
});

router.get('/document/index',common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user)

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render("documentTracking/index",{isLoggedIn : common.isLoggedInUserBoolean(req)});
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/document/create',common.isLoggedInUserToGoNext, function(req, res){
    res.render("documents/corporateUser/documentsIndex",{isLoggedIn : common.isLoggedInUserBoolean(req)});    
});

router.get('/document/upload',common.isLoggedInUserToGoNext, function(req, res){
    res.render("documentTracking/upload",{isLoggedIn : common.isLoggedInUserBoolean(req)});    
});

router.get('/document/analytics',common.isLoggedInUserToGoNext, function(req, res){
    res.render("documentTracking/analytics",{isLoggedIn : common.isLoggedInUserBoolean(req)});    
});
router.get('/document/list',common.isLoggedInUserToGoNext, function(req, res){
    res.render('documents/corporateUser/showAllDocuments');
});

router.get('/document/create/new',common.isLoggedInUserToGoNext, function(req, res){
    res.render('documents/corporateUser/createNewDocument');
});

router.get('/document/get/coporate/system/generated/number', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    // var objCompanyId = common.castToObjectId( userProfile.companyId);

    companyModelClassObj.getDocumentNumber(companyId, function(err, documentNumber){
        if(err) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
        else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: documentNumber
            })
        }
    });
});

router.post("/document/set/coporate/system/generated/number",  function (req, res) {
    var userProfile = common.getUserFromSession(req);
    // var objCompanyId = common.castToObjectId( userProfile.companyId);
    var companyId = userProfile.companyId;

    companyModelClassObj.setDocumentNumber(companyId, req.body.documentNumber,
                                                        function(error, result) {
        if(error) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        } else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
            })
        }
    })
});

router.get('/documents/get/all/data', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var objUserId = common.castToObjectId(common.getUserId(req.user))
    var companyId = userProfile.companyId;
    documentsManagementClassObj.getAllDocuments(companyId, objUserId, function(err, documents){
        if(err) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
        else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: documents

            })
        }
    });
});

router.get('/documents/get/linked', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    var refType = req.query.refType;
    var refId = req.query.refId;
    documentsManagementClassObj.getLinkedDocuments(companyId, refType, refId, function(err, documents){
        if(err) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
        else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: documents

            })
        }
    });
});

router.post("/documents/create/new",  function (req, res) {
    var objUserId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);
    var userEmailId = userProfile.emailId;

    documentsManagementClassObj.checkAndCreateDocument(objCompanyId,
                                                        objUserId,
                                                        userEmailId,
                                                        req.body,
                                                        function(error, savedDoc) {
        if(error) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        } else {

            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Document: savedDoc
            })
        }
    })
});

router.get('/corporate/admin/documents/get/all/data',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;

    if(domain){
        companyModelClassObj.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);

                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        documentsManagementClassObj.getAllDocuments(companyId, common.castToObjectId(req.query.userId), function(err, documents){
                            res.send(documents)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/admin/delete/documents',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        companyModelClassObj.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    var userProfile = common.getUserFromSession(req);
                    if(isAdmin && userProfile && userProfile.emailId){
                        documentsManagementClassObj.deleteMultipleDocuments(common.castToObjectId(companyProfile._id),req.body,function (err,documents) {
                            res.send(documents);
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});


router.post("/document/update/existing",  function (req, res){
    var objUserId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);
    var userEmailId = userProfile.emailId;

    documentsManagementClassObj.updateDocumentElements(objCompanyId,
                                                        objUserId,
                                                        userEmailId,
                                                        req.body,
                                                        function(err, result) {
            if(err) {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                });
            } else {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                })
            }
    });
});

router.post('/document/get/by/id', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;

    documentsManagementClassObj.getDocumentById(common.castToObjectId(companyId),
        common.castToObjectId(req.body.documentId),
        function(err, documents){
            if(err) {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1
                })
            }
            else {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data: documents

                })
            }
        });
});

router.post('/document/getDocumentByVersion', common.isLoggedInUserOrMobile, function (req, res) {
    var objUserId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);
    var userEmailId = userProfile.emailId;

    documentsManagementClassObj.getDocumentByVersion(objCompanyId,
                                                objUserId,
                                                req.body.documentTemplateType,
                                                req.body.documentTemplateName,
                                                req.body.documentName,
                                                req.body.documentVersion,
        function(err, documents){
            if(err) {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1
                })
            }
            else {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data: documents

                })
            }
        });
});

router.post('/document/clone', common.isLoggedInUserOrMobile, function(req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = common.castToObjectId( userProfile.companyId);
    var documentId = common.castToObjectId(req.body.documentId);
    var documentName = req.body.documentName;

    if(documentId) {
        documentsManagementClassObj.cloneDocument(companyId, documentId, documentName, function(error, document, message) {
            if(!error) {
                res.send({
                    SuccessCode: 1, 
                    ErrorCode: 0,
                })

            } else {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1, 
                    Message: message
                })
            }
            
        })
    } else {
        res.send({
            SuccessCode: 0,
            ErrorCode: 1, 
            Message: "Document Id is missing"
        })
    }
})


router.get('/documents/getCustomerProfiles', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);

    documentsManagementClassObj.getCustomerProfiles(
        objCompanyId,
        function(err,customerProfiles) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                CustomerProfiles: customerProfiles
            });
        });
});

router.post("/get/document/master/data/difference",  function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = common.castToObjectId(userProfile.companyId);
    var masterData = req.body.masterData;
    var dataTypeArr = [];
    var dataTypeColsArr = [];
    var updatedMasterData= [];

    dataTypeArr = _.map(masterData, 'name');

    _.each(masterData, function(mData) {
        dataTypeColsArr = _.union(dataTypeColsArr, _.map(mData.data, function(obj) {
                                    return common.castToObjectId(obj.id);
                                }))    
    });

    masterAccObj.getAccountDataDifference(dataTypeArr, dataTypeColsArr, companyId, function(err, result) {
        if(!err && result.length > 0) {
            var response = result;

            _.each(masterData, function(dataType) {
                var obj = {};

                var responseDataType = _.find(response, function(responseType) {
                                    return responseType.name == dataType.name;
                                });

                obj.name = dataType.name,
                obj.data = [];

                _.each(dataType.data, function(rowObj) {

                    var responseRowObj = _.find(responseDataType.data, function(resRowObj) {
                                            return rowObj.id == resRowObj._id;
                                        });

                    var dataTypeDataIndex = obj.data.length;
                    obj.data[dataTypeDataIndex] = {};
                    obj.data[dataTypeDataIndex].id = rowObj.id;
                    var colNames  = _.keys(rowObj);

                    _.each(colNames, function(name) {

                        if(name !== 'id') {
                            var updateObj = obj.data[dataTypeDataIndex];
                            updateObj[name] = {
                                'oldValue': rowObj[name],
                                'newValue': responseRowObj[name],
                                'valueChanged': rowObj[name] === responseRowObj[name] ? false : true
                            };
                            obj.data[dataTypeDataIndex] = updateObj;
                        }
                        
                    })
                })

                updatedMasterData.push(obj);
            })

            res.send(updatedMasterData);
        }
    })

});

function getCompanyObj(companyName,req){
    var company = {
        url:req.headers.host,
        companyName:companyName
    }
    return company;
}

module.exports = router;