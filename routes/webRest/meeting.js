
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var AWS = require('aws-sdk');

var commonUtility = require('../../common/commonUtility');
var meetingSupportClass = require('../../common/meetingSupportClass');
var appCredentials = require('../../config/relatasConfiguration');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var emailSender = require('../../public/javascripts/emailSender');
var errorMessages = require('../../errors/errorMessage');
var interactions = require('../../dataAccess/interactionManagement');
var meetingClass = require('../../dataAccess/meetingManagement');
var winstonLog = require('../../common/winstonLog');
var googleCalendarAPI = require('../../common/googleCalendar');
var contactClass = require('../../dataAccess/contactsManagementClass');
var relationshipStrength = require('../../dataAccess/relationshipStrength');
var officeOutlookApi = require('../../common/officeOutlookAPI');
var _ = require("lodash");
var async = require("async")

var errorObj = new errorClass();
var appCredential = new appCredentials();
var common = new commonUtility();
var interactionObj = new interactions();
var validation = new validations();
var userManagementObj = new userManagement();
var errorMessagesObj = new errorMessages();
var meetingsObj = new meetingClass();
var meetingSupportClassObj = new meetingSupportClass();
var googleCalendar = new googleCalendarAPI();
var logLib = new winstonLog();
var contactObj = new contactClass();
var relationshipStrengthObj = new relationshipStrength();
var officeOutlook = new officeOutlookApi();

var logger = logLib.getWinston();
var statusCodes = errorMessagesObj.getStatusCodes();
var awsData = appCredential.getAwsCredentials();

AWS.config.update(awsData);
AWS.config.region = awsData.region;


router.get('/meeting/:mId',common.checkUserDomain,common.checkUserDomain,function(req,res){
    var meetingId = req.params.mId;  /* Meeting/ Invitation id */
    req.session.meetingId = meetingId;
    req.session.finalPageUrl = '/meeting/'+meetingId;

    if(common.checkRequired(meetingId)){
        res.render('meetingDetails',common.getLoginStatus(req));
    }else res.redirect('/')
});

router.get('/meeting/:invitationId/prepare/:userId',common.isLoggedInUser,common.checkUserDomain,function(req, res){
    if(common.checkRequired(req.params.invitationId)){
        var userId = common.getUserId(req.user);
        meetingSupportClassObj.updateMeetingPrepared(userId,req.params.invitationId,function(isuccess){
            res.redirect('/today/details/'+req.params.invitationId);
        })
    }
    else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVITATION_ID_NOT_FOUND'}));
});

router.get('/meeting/get/:invitationId/meeting',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.params.invitationId)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{timezone:1},function(error,user){
            var timezone = req.session.profileTimezone || 'UTC';
            if(common.checkRequired(user)){
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }
            }
            meetingsObj.getMeetingWithTodoPopulate(req.params.invitationId,function(invitation){
                if(common.checkRequired(invitation)){
                    if(invitation.senderId == userId){
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "userId":common.getUserId(req.user),
                            "timezone":timezone,
                            "Data":invitation
                        });
                    }
                    else{
                        var exist = false;
                        var canceled = false;
                        for(var i=0; i<invitation.toList.length; i++){
                            if(invitation.toList[i].receiverId == userId){
                                if(invitation.toList[i].canceled){
                                    canceled = true;
                                }else
                                    exist = true;
                            }
                        }

                        if(invitation.to){
                            if(invitation.to.receiverId == userId){
                                if(invitation.to.canceled){
                                    canceled = true;
                                }else
                                    exist = true;
                            }
                        }
                        if(canceled){
                            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'MEETING_ALREADY_CANCELED'}))
                        }else
                        if(exist){
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "userId":common.getUserId(req.user),
                                "timezone":timezone,
                                "Data":invitation
                            });
                        }else{
                            userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1},function(profile){

                                if(profile){
                                    userManagementObj.findInvitationWithEmail(invitation.invitationId,profile.emailId,function(error,invi){
                                        if(invi){
                                            var obj = {
                                                receiverId:profile._id,
                                                receiverEmailId:profile.emailId,
                                                receiverFirstName:profile.firstName,
                                                receiverLastName:profile.lastName,
                                                isAccepted:false
                                            };

                                            updateInviToList(req,res,obj,invi,timezone);
                                        }else res.send(errorObj.generateErrorResponse({status:statusCodes.AUTHORIZATION_ERROR_CODE,key:'UN_AUTHORISED_ACCESS'}));
                                    })
                                }else res.send(errorObj.generateErrorResponse({status:statusCodes.AUTHORIZATION_ERROR_CODE,key:'UN_AUTHORISED_ACCESS'}));
                            })
                        }
                    }
                }else
                    res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'SOMETHING_WENT_WRONG'},errorMessagesObj.getMessage("MEETING_DELETED_CANCELED")))
            })
        });
    }
    else{
       res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVITATION_ID_NOT_FOUND'}));
    }
});

router.get('/meeting/participant/:participantEmailId/relationship',common.isLoggedInUserToGoNext,function(req,res){
    var participantEmailId = req.params.participantEmailId;
    var userId = common.getUserId(req.user);
    var mobileNumber = req.query.mobileNumber

    if(common.checkRequired(participantEmailId)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,twitter:1,facebook:1,linkedin:1,corporateUser:1,companyId:1,serviceLogin:1},function(error,user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var showSetup = true;
                if((common.checkRequired(user.linkedin) && common.checkRequired(user.linkedin.id)) || (common.checkRequired(user.twitter) && common.checkRequired(user.twitter.id)) || (common.checkRequired(user.facebook) && common.checkRequired(user.facebook.id))){
                    showSetup = false;
                }

                //Check for appConfig Corporate user and get CompanyId
                var isCorporateUser = false;
                if(user.corporateUser && user.companyId){
                    isCorporateUser = true;
                    var entityId = user.companyId;
                } else {
                    var entityId = userId;
                    //Load Features for retails users if any
                }

                var data = {showSocialSetup:showSetup};

                if(req.query.fetchProfile == 'yes'){
                    userManagementObj.findUserProfileByEmailIdWithCustomFields(participantEmailId,{publicProfileUrl:1,firstName:1,lastName:1},function(error,pUser){
                        data.profile = pUser;
                        getRelationship(userId,user.emailId,participantEmailId,data,timezone,entityId,isCorporateUser,req,res,mobileNumber,user.serviceLogin);
                    })
                }
                else getRelationship(userId,user.emailId,participantEmailId,data,timezone,entityId,isCorporateUser,req,res,mobileNumber,user.serviceLogin);
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });
    }
    else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'EMAIL_ID_NOT_FOUND_IN_REQUEST'}));
});

router.get('/account/relationship',common.isLoggedInUserToGoNext,function(req,res){
    var account = req.query.account;
    var userId = common.getUserId(req.user);
    var hierarchyList = common.getUserListForQuery(req);
    var userProfile = common.getUserFromSession(req);
    var serviceLogin = userProfile?userProfile.serviceLogin:null;
    var companyId = userProfile.companyId?common.castToObjectId(userProfile.companyId):null

    var contacts = common.getContactsListForQuery(req);
    
    common.userFiscalYear(userId,function (err,fyMonth,startEndObj) {

        var from = startEndObj.start,
            to = startEndObj.end;

        async.parallel([
            function (callback) {
                getAccountInteractionInitiations(hierarchyList,account,from,to,contacts,serviceLogin,companyId,callback)
            },
            function (callback) {
                interactionTypesForAccount(hierarchyList,account,from,to,contacts,serviceLogin,companyId,callback)
            }
        ], function (errors,data) {

            if(!errors){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":formatData(data)
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":""
                })
            }
        });
    });
});

router.get('/account/relationship/new',common.isLoggedInUserToGoNext,function(req,res){
    var account = req.query.account;
    var userId = common.getUserId(req.user);
    var hierarchyList = common.getUserListForQuery(req);
    var userProfile = common.getUserFromSession(req);
    var serviceLogin = userProfile?userProfile.serviceLogin:null;
    var companyId = userProfile.companyId?common.castToObjectId(userProfile.companyId):null

    var contacts = common.getContactsListForQuery(req);

    common.userFiscalYear(userId,function (err,fyMonth,startEndObj,allQuarters,timezone) {

        var from = allQuarters[allQuarters.currentQuarter].start,
            to = allQuarters[allQuarters.currentQuarter].end;

        async.parallel([
            function (callback) {
                getAccountInteractionInitiations_new(hierarchyList,account,from,to,contacts,serviceLogin,companyId,callback)
            },
            function (callback) {
                interactionTypesForAccount_new(hierarchyList,account,from,to,contacts,serviceLogin,companyId,callback)
            }
        ], function (errors,data) {

            if(!errors){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":formatData(data)
                })
            } else {
                res.send({
                    "SuccessCode": 0,
                    "Message": "",
                    "ErrorCode": 1,
                    "Data":""
                })
            }
        });
    });
});

function formatData(data){

    var formattedData = {};

    formattedData.interactionInitiations = [];
    formattedData.interactionTypes = [];

    if(data[0] && data[0][0]){

        _.each(data[0],function (el) {

            if(el._id == "sender"){
                formattedData.interactionInitiations.push({
                    "_id": "others",
                    "count": el.count
                })

            } else {
                formattedData.interactionInitiations.push({
                    "_id": "you",
                    "count": el.count
                })
            }
        })
    }
    
    if(data[1] && data[1][0]){
        formattedData.interactionTypes.push(data[1][0]);
    }

    return formattedData;

}

function sortData(interactions){
   return interactions.sort(function (o1, o2) {
        return new Date(o2.date) > new Date(o1.date) ? 1 : new Date(o2.date) < new Date(o1.date) ? -1 : 0;
    });
}

function getAccountInteractionInitiations(userIds,account,from,to,contacts,serviceLogin,companyId,callback){
    interactionObj.interactionsInitForAccount(userIds,account,from,to,contacts,serviceLogin,companyId,callback)
}

function interactionTypesForAccount(userIds,account,from,to,contacts,serviceLogin,companyId,callback){
    interactionObj.interactionTypesForAccount(userIds,account,from,to,contacts,serviceLogin,companyId,callback)
}

function getAccountInteractionInitiations_new(userIds,account,from,to,contacts,serviceLogin,companyId,callback){
    interactionObj.interactionsInitForAccount_new(userIds,account,from,to,contacts,serviceLogin,companyId,callback)
}

function interactionTypesForAccount_new(userIds,account,from,to,contacts,serviceLogin,companyId,callback){
    interactionObj.interactionTypesForAccount_new(userIds,account,from,to,contacts,serviceLogin,companyId,callback)
}

function getRelationship(userId,emailId,participantEmailId,data,timezone,entityId,isCorporateUser,req,res,mobileNumber,serviceLogin){

    var isEmail = true;

    if(!validateEmail(participantEmailId)) {
        isEmail = false
    }

    contactObj.getSingleContactRelation(userId,participantEmailId,isEmail,false,mobileNumber,function(contact){

        if(common.checkRequired(contact)){
            data.relation = contact;
            getInteractionInitiations(userId,emailId,participantEmailId,data,timezone,entityId,isCorporateUser,req,res,mobileNumber,serviceLogin)
        }
        else{
            data.relation = null;
            getInteractionInitiations(userId,emailId,participantEmailId,data,timezone,entityId,isCorporateUser,req,res,mobileNumber,serviceLogin)
        }
    })
}

function getInteractionInitiations(userId,emailId,participantEmailId,data,timezone,entityId,isCorporateUser,req,res,mobileNumber,serviceLogin){

    common.interactionDataFor(function(days){
        var dateMin = moment().tz(timezone);
        dateMin.date(dateMin.date() - days);
        dateMin = dateMin.format();
        var dateMax = moment().tz(timezone).format()
        data.dataFor = days;
        interactionObj.getInteractionInitiationsCount(common.castToObjectId(userId),emailId,participantEmailId,false,dateMin,dateMax,mobileNumber,serviceLogin,function(counts){

            if(common.checkRequired(counts) && counts.length > 0){
                var interactionInitiations = [];
                var connectedVia = [];
                var you = false;
                var others = false;
                for(var i=0; i<counts.length; i++){
                    connectedVia = connectedVia.concat(counts[i].connectedVia);
                    if(counts[i]._id == 'sender'){
                        interactionInitiations.push({_id:'others',count:counts[i].count});
                        others = true;
                    }
                    else {
                        you = true;
                        interactionInitiations.push({_id:'you',count:counts[i].count});
                    }
                }

                if(!you) interactionInitiations.push({_id:'you',count:0});

                if(!others) interactionInitiations.push({_id:'others',count:0});

                if(connectedVia.length > 0){
                    data.connectedVia = common.removeDuplicate_id(connectedVia,true);
                }
                else data.connectedVia = [];

                data.interactionInitiations = interactionInitiations;
            }
            else data.interactionInitiations = [];
            getInteractionsByTypeCount(userId,participantEmailId,data,timezone,entityId,isCorporateUser,req,res,days,mobileNumber,serviceLogin);
        })
    });
}

function getInteractionsByTypeCount(userId,participantEmailId,data,timezone,entityId,isCorporateUser,req,res, days,mobileNumber,serviceLogin){
    var dateMin = moment().tz(timezone);
    dateMin.date(dateMin.date() - days);
    dateMin = dateMin.format();
    var dateMax = moment().tz(timezone).format();
    interactionObj.interactionWithContactByTypes(common.castToObjectId(userId),participantEmailId,false,dateMin,dateMax,mobileNumber,serviceLogin,function(typeCounts){

        if(common.checkRequired(typeCounts) && typeCounts.length > 0){
            data.interactionTypes = common.formatInteractionTypeGraphData(typeCounts)
        }
        else data.interactionTypes = [];
        getRelationshipStrength(userId,participantEmailId,data,entityId,isCorporateUser,dateMin,dateMax,req,res,mobileNumber,timezone)
    })
}

function getRelationshipStrength(userId,participantEmailId,data,entityId,isCorporateUser,dateMin,dateMax,req,res,mobileNumber,timezone){

    interactionObj.calculateRelationshipStrength(common.castToObjectId(userId),participantEmailId,dateMin,dateMax,mobileNumber,function(typeCounts){

        if(common.checkRequired(typeCounts) && typeCounts.length > 0){
            data.relationshipStrength = typeCounts
        }
        else data.relationshipStrength = [];

        relationshipStrengthObj.getRelationshipStrengthDoc(function(error,doc){
            data.doc = doc;

            //Send App config for a LIU

            data.appConfigFeature = isCorporateUser;

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "timezone":timezone,
                "userId":userId,
                "Data":data
            });

            // common.getAppConfiguration(entityId,function(appConfigData){
            //     if(appConfigData) {
            //
            //         data.appConfigFeature = appConfigData;
            //
            //         res.send({
            //             "SuccessCode": 1,
            //             "Message": "",
            //             "ErrorCode": 0,
            //             "timezone":timezone,
            //             "userId":userId,
            //             "Data":data
            //         });
            //     } else {
            //         data.appConfigFeature = 'NO_FEATURES';
            //         res.send({
            //             "SuccessCode": 1,
            //             "Message": "",
            //             "ErrorCode": 0,
            //             "timezone":timezone,
            //             "userId":userId,
            //             "Data":data
            //         });
            //     }
            // });
        });
    })
}

function getMeetingById(invitationId,callback){
    if(common.checkRequired(invitationId)){
        userManagementObj.findInvitationById(invitationId,function(error,invitation,message){
            if(error || !invitation){
                callback(false)
            }
            else callback(invitation)
        });
    }else callback(false)
}

function updateInviToList(req,res,obj,invi,timezone){
    userManagementObj.updateInvitationToList(invi.invitationId,obj,function(error,isUpdated,message){
        logger.info(message);
        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "userId":common.getUserId(req.user),
            "timezone":timezone,
            "Data":invi
        });
    })
}

router.post('/meeting/before/after/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.body) && common.checkRequired(req.body.dateTime)){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user) {
            if (common.checkRequired(user)) {
                var timezone;
                if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                    timezone = user.timezone.name;
                } else timezone = 'UTC';
                var date = moment(req.body.dateTime);
                var dateMin = moment(date).tz(timezone);
                dateMin.hour(0);
                dateMin.minute(0);
                dateMin.second(0);
                dateMin.millisecond(0);

                var dateMax = moment(date).tz(timezone);
                dateMax.hour(23)
                dateMax.minute(59)
                dateMax.second(59)
                dateMax.millisecond(0)

                interactionObj.getMeetingsBeforeAndNext(user._id,dateMin,dateMax,req.body.invitationId,moment(date).tz(timezone).format(),function(meetings){
                    if(common.checkRequired(meetings) && meetings.length > 0){
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "timezone":timezone,
                            "Data":meetings
                        });
                    }
                    else{
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "timezone":timezone,
                            "Data":[]
                        });
                    }
                })
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'DATE_TIME_MISSED_IN_REQUEST_BODY'}));
});

router.post('/meetings/action/accept/web', common.isLoggedInUserToGoNext, function (req, res) {

    var userId = common.getUserId(req.user);
    
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            userManagementObj.findUserProfileByIdWithCustomFields(userId, {google:1,emailId:1,firstName:1,lastName:1,timezone:1,serviceLogin:1,outlook:1}, function(error, user){
                if (common.checkRequired(user)) {
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }
                    else if(common.checkRequired(req.body.timezone)){
                        timezone = req.body.timezone
                    }
                    else timezone = 'UTC';
                    
                    if(common.checkRequired(req.body.invitationId) && common.checkRequired(req.body.slotId)){
                        meetingSupportClassObj.validateUserAcceptance(req.body.invitationId,userId,req.body.slotId,req.body.message,function(isValid,error,obj){
                            if(isValid){
                                if(common.checkRequired(obj)){
                                    obj.timezone = timezone;
                                    getUserPrimaryGoogleEmailId(obj.senderId,function(emailId){

                                        if(common.checkRequired(emailId)){
                                            obj.senderPrimaryEmail = emailId;
                                        }
                                        
                                       // meetingSupportClassObj.confirmMeeting_new(obj,userId,user.serviceLogin,function(isSuccess,error){
                                        meetingSupportClassObj.confirmMeeting(obj,userId,req.body.message,function(isSuccess,error){
                                            
                                            if(isSuccess){
                                                meetingSupportClassObj.updateMeetingInvitation(obj,userId,function(result,err){

                                                    if(result){
                                                        uploadICSFileToAWS(result,obj,userId,req,res);
                                                    }else res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: err},errorMessagesObj.getMessage('ACCEPTING_MEETING_FAILED')))
                                                })
                                            }
                                            else{
                                                if(error == 'ERROR_FETCHING_PROFILE'){
                                                    res.send(errorObj.generateErrorResponse({status: statusCodes.PROFILE_ERROR_CODE, key: error},errorMessagesObj.getMessage('ACCEPTING_MEETING_FAILED')))
                                                }else res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: error},errorMessagesObj.getMessage('ACCEPTING_MEETING_FAILED')))
                                            }
                                        })
                                    });
                                }else res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'ACCEPT_MEETING_FAILED'},errorMessagesObj.getMessage('ACCEPTING_MEETING_FAILED')))
                            }
                            else{
                                if(error == 'UN_AUTHORISED_ACCESS'){
                                    res.send(errorObj.generateErrorResponse({status: statusCodes.AUTHORIZATION_ERROR_CODE, key: error},errorMessagesObj.getMessage('ACCEPTING_MEETING_FAILED_AUTHORIZATION')))
                                }else  res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: error},errorMessagesObj.getMessage('ACCEPTING_MEETING_FAILED')))
                            }
                        })
                    }
                    else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'SLOT_ID_OR_INVITATION_ID_MISSED'},errorMessagesObj.getMessage('ACCEPTING_MEETING_FAILED')))

                } else res.send(errorObj.generateErrorResponse({
                    status: statusCodes.PROFILE_ERROR_CODE,
                    key: 'ERROR_FETCHING_PROFILE'
                },errorMessagesObj.getMessage('ACCEPTING_MEETING_FAILED')));
            })
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage('ACCEPTING_MEETING_FAILED')));
});

function getUserPrimaryGoogleEmailId(userId,callback){
    if(common.checkRequired(userId)){

        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,officeCalendar:1,google:1,serviceLogin:1},function(error,user){
            if(common.checkRequired(user)){
                if(user.serviceLogin == 'office' && common.checkRequired(user.officeCalendar)){
                    callback(user.officeCalendar.emailId);
                }
                else if(googleCalendar.validateUserGoogleAccount(user)){
                    callback(user.google[0].emailId);
                }
                else callback(null);
            }
        })
    }
    else callback(null)
}

function uploadICSFileToAWS(icsFile,obj,userId,req,res){

    if(common.checkRequired(obj.icsFile) && common.checkRequired(obj.icsFile.awsKey)){

        obj.file = icsFile;
        meetingSupportClassObj.sendAcceptInvitationEmail(obj,userId);
        res.send({
            "SuccessCode": 1,
            "Message": errorMessagesObj.getMessage('ACCEPTING_MEETING_SUCCESS'),
            "ErrorCode": 0,
            "Data":{}
        });
    }
    else{

        obj.file = icsFile;
        var s3bucket = new AWS.S3({ params: {Bucket: awsData.icsBucket} });
        var url = 'https://' + awsData.icsBucket + '.s3.amazonaws.com/';
        var key = obj.invitationId+'.ics';
        var params = {Key: key, ContentType: 'text/calendar', Body: icsFile};

        s3bucket.putObject(params, function(err, data) {
            if (err) {

                logger.info('Uploading ICS file to AWS failed '+err)
                meetingSupportClassObj.sendAcceptInvitationEmail(obj,userId);
                res.send({
                    "SuccessCode": 1,
                    "Message": errorMessagesObj.getMessage('ACCEPTING_MEETING_SUCCESS'),
                    "ErrorCode": 0,
                    "Data":{}
                });
            } else {

                obj.icsAwsKey = key;
                obj.icsUrl = url+key;
                meetingSupportClassObj.updateMeetingWithIcs(obj,function(isSuccess,error){

                    if(error){
                        logger.info('Updating meeting with ICS file failed '+error);
                    }
                    meetingSupportClassObj.sendAcceptInvitationEmail(obj,userId);
                    res.send({
                        "SuccessCode": 1,
                        "Message": errorMessagesObj.getMessage('ACCEPTING_MEETING_SUCCESS'),
                        "ErrorCode": 0,
                        "Data":{}
                    });
                })
            }
        });
    }
}

/* invitationId & message */
router.post('/meetings/action/cancel/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user)

        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if(common.checkRequired(req.body.invitationId)){
                userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1,outlook:1,google:1},function(error,user){
                    if(common.checkRequired(user)){

                        var timezone;
                        if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                            timezone = user.timezone.name;
                        }else timezone = 'UTC';

                        meetingSupportClassObj.cancelMeeting(req.body.invitationId,userId,req.body.message,timezone,user,function(isSuccess,error){
                            if(isSuccess){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": errorMessagesObj.getMessage("CANCEL_MEETING_SUCCESS"),
                                    "ErrorCode": 0,
                                    "Data":{}
                                });
                            }
                            else{
                                logger.info('Meeting cancel failed /meetings/action/cancel/web',error,req.body,userId);
                                res.send({
                                    "SuccessCode": 0,
                                    "Message": errorMessagesObj.getMessage("CANCEL_MEETING_FAILED"),
                                    "ErrorCode": 1,
                                    "Data":{}
                                });
                            }
                        })

                    }
                    else{
                        logger.info('ERROR_FETCHING_PROFILE /meetings/action/cancel/web',error,userId);
                        res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("CANCEL_MEETING_FAILED")));
                    }
                })
            }
            else{
                logger.info('INVITATION_ID_NOT_FOUND /meetings/action/cancel/web',req.body,userId);
                res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVITATION_ID_NOT_FOUND'},errorMessagesObj.getMessage("CANCEL_MEETING_FAILED")));
            }
        }
        else{
            logger.info('NO_REQUEST_BODY CANCEL_MEETING_FAILED /meetings/action/cancel/web',req.body,userId);
            res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("CANCEL_MEETING_FAILED")));
        }

});

/* invitationId, message(comment), new times and location details */

function getAllParticipantsUserIdList(meeting){
    var list = [];
    if(common.checkRequired(meeting.senderId)){
        list.push(common.castToObjectId(meeting.senderId));
    }

    if(meeting.selfCalendar){
        for(var i=0; i<meeting.toList.length; i++){
            if(common.checkRequired(meeting.toList[i].receiverId)){
                list.push(common.castToObjectId(meeting.toList[i].receiverId))
            }
        }
    }
    else if(meeting.to && meeting.to.receiverId){
        list.push(common.castToObjectId(meeting.to.receiverId))
    }
    return list;
}

router.post('/meetings/action/edit/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

        if (common.checkRequired(req.body) && validation.isEmptyObject(req.body)) {
            var status = validation.validateMeetingNewTimeLocation(req.body);

            if(status.status == 5000){
                logger.info('Meeting Reschedule failed ',status, req.body);
                res.send(errorObj.generateErrorResponse(status,errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
            }
            else {
                var invitationDetails = req.body;

                userManagementObj.findInvitationById(invitationDetails.invitationId,function(error,invi,msg){
                    if(common.checkRequired(invi)){
                        var accepted = false;
                        var multipleSuggest = false;
                        var lastLocation;
                        var participantUserIdList = getAllParticipantsUserIdList(invi)
                        var lastUser = invi.lastActionUserId || userId;
                        var count = invi.updateCount || 0;
                        count = count > 0 ? count-1 : count;
                        var gEventId = invi.googleEventId;
                        gEventId = gEventId ? gEventId : 'relat'+getNumString(count)+invi.invitationId;
                        var maxSlot;
                        for(var i=0; i<invi.scheduleTimeSlots.length; i++){
                            if(invi.scheduleTimeSlots[i].isAccepted){
                                accepted = true;
                                maxSlot = invi.scheduleTimeSlots[i]
                            }
                        }

                        if(!common.checkRequired(maxSlot)){
                            maxSlot = invi.scheduleTimeSlots.reduce(function (a, b) {
                                return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                            });
                        }

                        var newInvitationDetails = {
                            comment:req.body.comment,
                            invitationId:invi.invitationId,
                            lastLocation:maxSlot.location,
                            scheduleTimeSlots:[
                                {
                                    start:{
                                        date:new Date(invitationDetails.newSlotDetails.startDateTime)
                                    },
                                    end:{
                                        date:new Date(invitationDetails.newSlotDetails.endDateTime)
                                    },
                                    title:maxSlot.title,
                                    location:maxSlot.location,
                                    suggestedLocation:invitationDetails.newSlotDetails.suggestedLocation,
                                    locationType:common.checkRequired(invitationDetails.newSlotDetails.locationType) ? invitationDetails.newSlotDetails.locationType : maxSlot.locationType,
                                    description:maxSlot.description
                                }
                            ]
                        };

                        if(invi.selfCalendar){
                            if(invi.toList.length > 1){
                                multipleSuggest = true;
                            }
                            /*updateReceiverContacts(invi.senderId,invi.toList[0].receiverId);
                             updateReceiverContacts(invi.toList[0].receiverId,invi.senderId);*/
                        }
                        else{
                            /*updateReceiverContacts(invi.senderId,invi.to.receiverId);
                             updateReceiverContacts(invi.to.receiverId,invi.senderId);*/
                        }

                        if(req.session.profile.serviceLogin == 'outlook'){
    
                            userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId),{emailId:1,outlook:1,timezone:1},function(error,userProfile) {
                                if (!error && userProfile) {
                                    _.each(userProfile.outlook,function (outlookObj) {
                                        if(userProfile.emailId == outlookObj.emailId){
                                            var updateObj = getOutlookMeetingObj(newInvitationDetails,userProfile.timezone);
                                            officeOutlook.getNewAccessTokenWithoutStore(outlookObj.refreshToken,function (newToken) {
                                                if(invi.partialMeeting){
                                                    //create new event if event is not exist on google or outlook calendar
                                                    var eventDetail = {
                                                        title:newInvitationDetails.scheduleTimeSlots[0].title,
                                                        location:newInvitationDetails.scheduleTimeSlots[0].location,
                                                        description:newInvitationDetails.scheduleTimeSlots[0].description,
                                                        start:newInvitationDetails.scheduleTimeSlots[0].start.date,
                                                        end:newInvitationDetails.scheduleTimeSlots[0].end.date,
                                                        locationType:newInvitationDetails.scheduleTimeSlots[0].locationType,
                                                        type:'reschedule'
                                                    };

                                                    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId),{google:1,timezone:1,outlook:1,emailId:1,firstName:1,lastName:1},function(error,user){
                                                        if(user) {
                                                            meetingSupportClassObj.createNewGoogleOrOutlookMeetingAndUpdateId(user, invi, eventDetail, 'outlook',outlookObj.refreshToken, function (result, msg,outlookMeetingId) {
                                                                if (result) {
                                                                    // updateInvitationToListFalse(invi);
                                                                    newInvitationDetails.invitationId = outlookMeetingId ? outlookMeetingId : newInvitationDetails.invitationId;
                                                                    updateInvitation(newInvitationDetails, userId, multipleSuggest, maxSlot, req, res, participantUserIdList);
                                                                    logger.info("Meeting successfully updated for user -", userProfile.emailId)
                                                                }
                                                                else {
                                                                    res.send(errorObj.generateErrorResponse({
                                                                        status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                                                                        key: 'UPDATE_MEETING_FAILED'
                                                                    }, errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
                                                                }
                                                            });
                                                        }
                                                        else{
                                                            res.send(errorObj.generateErrorResponse({
                                                                status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                                                                key: 'UPDATE_MEETING_FAILED'
                                                            }, errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
                                                        }
                                                    })
                                                }
                                                else {
                                                    officeOutlook.updateMeetingOnCalendarMSGraphAPI(newToken.access_token, updateObj, invi.invitationId, function (err, response) {
                                                        updateInvitationToListFalse(invi);
                                                        updateInvitation(newInvitationDetails, userId, multipleSuggest, maxSlot, req, res, participantUserIdList);
                                                        logger.info("Meeting successfully updated for user -", userProfile.emailId, response)
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            updateInvitationToListFalse(invi);
                            if (accepted) {
                                if (common.checkRequired(invi.icsFile) && common.checkRequired(invi.icsFile.awsKey)) {
                                    meetingSupportClassObj.deleteICSFile(invi.icsFile.awsKey)
                                }

                                if (invi.selfCalendar) {
                                    googleCalendar.updateExistingGoogleMeetingNew(common.castToObjectId(userId), gEventId, invi, newInvitationDetails, function (result) {
                                        if (result) {
                                            updateInvitation(newInvitationDetails, userId, multipleSuggest, maxSlot, req, res, participantUserIdList);
                                        } else {
                                            res.send(errorObj.generateErrorResponse({
                                                status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                                                key: 'UPDATE_MEETING_FAILED'
                                            }, errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
                                        }
                                    })
                                    // googleCalendar.removeEventFromGoogleCalendarForMeeting(gEventId,lastUser,function(flag){
                                    //     updateInvitation(newInvitationDetails,userId,multipleSuggest,maxSlot,req,res,participantUserIdList);
                                    // });
                                }
                                else {
                                    googleCalendar.updateExistingGoogleMeetingNew(common.castToObjectId(userId), gEventId, invi, newInvitationDetails, function (result) {
                                        if (result) {
                                            updateInvitation(newInvitationDetails, userId, multipleSuggest, maxSlot, req, res, participantUserIdList);
                                        }
                                        else {
                                            res.send(errorObj.generateErrorResponse({
                                                status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                                                key: 'UPDATE_MEETING_FAILED'
                                            }, errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
                                        }
                                    })
                                    // googleCalendar.removeEventFromGoogleCalendarForMeeting(gEventId,lastUser,function(flag){
                                    //     updateInvitation(newInvitationDetails,userId,multipleSuggest,maxSlot,req,res,participantUserIdList);
                                    // });
                                }
                            }
                            else {
                                if(invi.partialMeeting && !invi.googleEventId){
                                    //create new event if event is not exist on google or outlook calendar
                                    var eventDetail = {
                                        title:newInvitationDetails.scheduleTimeSlots[0].title,
                                        location:newInvitationDetails.scheduleTimeSlots[0].location,
                                        description:newInvitationDetails.scheduleTimeSlots[0].description,
                                        start:newInvitationDetails.scheduleTimeSlots[0].start.date,
                                        end:newInvitationDetails.scheduleTimeSlots[0].end.date,
                                        locationType:newInvitationDetails.scheduleTimeSlots[0].locationType,
                                        type:'reschedule'
                                    };

                                    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId),{google:1,timezone:1,outlook:1,emailId:1,firstName:1,lastName:1},function(error,user){
                                        if(user) {
                                            meetingSupportClassObj.createNewGoogleOrOutlookMeetingAndUpdateId(user, invi, eventDetail, 'google', null, function (result, msg) {
                                                if (result) {
                                                    updateInvitation(newInvitationDetails, userId, multipleSuggest, maxSlot, req, res, participantUserIdList);
                                                }
                                                else {
                                                    res.send(errorObj.generateErrorResponse({
                                                        status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                                                        key: 'UPDATE_MEETING_FAILED'
                                                    }, errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
                                                }
                                            });
                                        }
                                        else{
                                            res.send(errorObj.generateErrorResponse({
                                                status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                                                key: 'UPDATE_MEETING_FAILED'
                                            }, errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
                                        }
                                    })
                                }
                                else{
                                    googleCalendar.updateExistingGoogleMeetingNew(common.castToObjectId(userId), gEventId, invi, newInvitationDetails, function (result) {
                                        if (result) {
                                            updateInvitation(newInvitationDetails, userId, multipleSuggest, maxSlot, req, res, participantUserIdList);
                                        }
                                        else {
                                            res.send(errorObj.generateErrorResponse({
                                                status: statusCodes.SOMETHING_WENT_WRONG_CODE,
                                                key: 'UPDATE_MEETING_FAILED'
                                            }, errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
                                        }
                                    })
                                }
                                // updateInvitation(newInvitationDetails,userId,multipleSuggest,maxSlot,req,res,participantUserIdList);
                            }
                        }
                        
                    }
                    else{
                        logger.info("FAILED_TO_FETCH_MEETING_DETAILS ",req.body);
                        res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'FAILED_TO_FETCH_MEETING_DETAILS'},errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
                    }
                })
            }
        }
        else{
            logger.info("NO_REQUEST_BODY ",req.body);
            res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
        }

});

function getOutlookMeetingObj(meetingDetails,timezone) {

    var start,end,dpName;
    _.each(meetingDetails.scheduleTimeSlots,function (slot) {
       start = new Date(slot.start.date)
       end = new Date(slot.end.date)
        dpName = slot.suggestedLocation+" : "+slot.locationType
    });
    
    var meeting = {
        "Start": {
            "DateTime": start,
            "TimeZone": timezone.name?timezone.name:'UTC'
        },
        "End": {
            "DateTime": end,
            "TimeZone": timezone.name?timezone.name:'UTC'
        },
        "Location":{
            "DisplayName":dpName
        }
    };

    return meeting;
}

function getNumString(num){
    return num < 10 ? '0'+num : ''+num
}

function updateInvitationToListFalse(invitation){
    if(invitation.selfCalendar){
        var toList = invitation.toList;
        var userId = invitation.toList[0].receiverId;
        for(var i=0; i<toList.length; i++){
            toList[i].isAccepted = false;
            toList[i].canceled = false;
        }
        userManagementObj.updateInvitationToListNotAccepted(invitation.invitationId,toList,function(error,isUpdate,message){
            logger.info(message.message);
        })
    }
    else{
        meetingsObj.updateMeetingToNotAccepted(invitation.invitationId,function(success){

        })
    }
}

function updateInvitation(invitationDetails,userId,multipleSuggest,maxSlot,req,res,participantUserIdList){
    var date = new Date();

    var invitationObj = {
        readStatus:false,
        suggested:true,
        scheduleTimeSlots:invitationDetails.scheduleTimeSlots,
        lastModified:date,
        suggestedBy:{
            userId:userId,
            suggestedDate:date
        }
    }

    if(!common.checkRequired(invitationObj.scheduleTimeSlots[0].suggestedLocation)){
        invitationObj.scheduleTimeSlots[0].location = invitationDetails.lastLocation;
        invitationObj.scheduleTimeSlots[0].suggestedLocation = invitationDetails.lastLocation;

    }
    else{
        invitationObj.scheduleTimeSlots[0].location = invitationObj.scheduleTimeSlots[0].suggestedLocation;
    }

    var comment = {
        messageDate: new Date(),
        message: invitationDetails.comment,
        userId: userId
    };

    meetingsObj.updateInvitationSuggest(invitationDetails.invitationId,invitationObj,comment,function(error,isUpdated,message){
        if (isUpdated) {
            logger.info(message.message);
            removeInteractionsAndAddInteractionsSuggested(participantUserIdList,invitationDetails.invitationId,userId);

            var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
            var mUrl = protocol+req.headers.host+'/meeting/'+invitationDetails.invitationId;
            meetingSupportClassObj.sendSuggestMeetingEmail(invitationDetails.invitationId,maxSlot,invitationDetails.comment);
            //sendSuggestInvitationMailToSuggestedUser(invitationDetails.invitationId,userId,maxSlot,mUrl,invitationDetails.comment);
            res.send({
                "SuccessCode": 1,
                "Message": errorMessagesObj.getMessage("RESCHEDULE_MEETING_SUCCESS"),
                "ErrorCode": 0,
                "Data":{}
            });
        }
        else{
            logger.info("UPDATE_MEETING_FAILED ",req.body,error);
            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_MEETING_FAILED'},errorMessagesObj.getMessage("RESCHEDULE_MEETING_FAILED")));
        }
    });
}

function removeInteractionsAndAddInteractionsSuggested(participantUserIdList,invitationId,userId){
    interactionObj.removeInteractions_new(participantUserIdList,invitationId,function(){
        getMeetingById(invitationId,function(meeting){
            if(meeting && common.checkRequired(meeting.senderEmailId)){
                meetingSupportClassObj.storeCreateMeetingInteractions_(meeting,userId,true);
            }
        })
    });
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

module.exports = router;