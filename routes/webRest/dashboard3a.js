/**
 * Created by sunil on 05/14/16.
 */
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');
var jwt = require('jwt-simple');
var _ = require("lodash");

var allUsers = require('../../databaseSchema/userManagementSchema').User;
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactionManagement = require('../../dataAccess/interactionManagement');
var commonUtility = require('../../common/commonUtility');
var insightsManagement = require('../../dataAccess/insightsManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var contactClassSupport = require('../../common/contactsSupport');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var userOpenSlots = require('../../common/userOpenSlots');

var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var contactSupportObj = new contactClassSupport();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionManagementObj = new interactionManagement();
var userOpenSlotsObj = new userOpenSlots();

var statusCodes = errorMessagesObj.getStatusCodes();

router.get('/dashboard/unique/contacts/interacted', common.isLoggedInUserToGoNext, function(req, res) {
    var timezone;
    var uId = common.getUserId(req.user)
    var userId = common.castToObjectId(uId)
    todayDateMinMax(userId, function(err, date) {
        if (err || !date)
            res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
        else {
            if(req.query.days == 7){
                var minDate = moment(date.min).subtract(8, "days").toDate()
                var maxDate = moment(date.max).subtract(1, "days").toDate()
            }
            else {
                var minDate = moment(date.min).subtract(1, "days").toDate()
                var maxDate = moment(date.max).subtract(1, "days").toDate()
            }
            interactionManagementObj.getUniqueContactsInteractedByDate(userId, minDate, maxDate, function(err, result) {
                if (!err && result) {
                    if (result.length > 0) {
                        var contacts = _.pluck(result, '_id');
                        userManagementObj.searchUserConatctsByEmailAndMobile(userId, contacts, function(error, contact) {

                            if (!error && contact) {
                                var inf_dec_contacts = [];
                                contact.forEach(function(a) {
                                    if (a.contactRelation == 'influencer' || a.contactRelation == 'decision_maker')
                                        inf_dec_contacts.push(a)
                                })
                                res.send({
                                    SuccessCode: 1,
                                    ErrorCode: 0,
                                    Data: {
                                        uniqueContacts: contact.length > 0 ? contact : [],
                                        impactContacts: inf_dec_contacts.length
                                    }
                                })
                            } else {
                                res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                            }
                        })
                    } else {
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            Data: {
                                uniqueContacts: [],
                                impactContacts: 0
                            }
                        })
                    }

                } else {
                    res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                }
            });
        }
    });

});

router.get('/dashboard/extended/network/from/interacted/companies', common.isLoggedInUserToGoNext, function(req, res) {
    var timezone;
    var uId = common.getUserId(req.user)
    var userId = common.castToObjectId(uId)
    kickOffExtendedNetworkForRecentlyInteracted(userId,res,false);
});

router.get('/dashboard/extended/network/from/all/companies', common.isLoggedInUserToGoNext, function(req, res) {
    var uId = common.getUserId(req.user);
    var userId = common.castToObjectId(uId);
    kickOffExtendedNetwork(userId,res,false)
});

router.get('/redirect/extended/network',common.isLoggedInUserToGoNext, function(req, res) {
    var userId = common.getUserId(req.user)
    kickOffExtendedNetwork(common.castToObjectId(userId),res,function (extendedAccounts) {
        var companies = '';
        var url = '';

        if(extendedAccounts.length>0){
            for(var i=0;i<extendedAccounts.length;i++){
                companies += extendedAccounts[i].company + '+'
            }
            url = "/contact/connect?searchContent=" + companies.slice(0, -1) + "&yourNetwork=false&extendedNetwork=true&forCompanies=true&forInsights=false&forDashboard=true&forDashboardTopCompanies=true"
            res.redirect(url)
        } else{
            kickOffExtendedNetworkForRecentlyInteracted(common.castToObjectId(userId),res,function (accounts) {
                if(accounts)
                    for(var i=0;i<accounts.length;i++){
                        if(accounts[i].extendedNetwork>0){
                            companies += accounts[i].company + '+'
                        }
                    }
                url = "/contact/connect?searchContent=" + companies.slice(0, -1) + "&yourNetwork=false&extendedNetwork=true&forCompanies=true&forInsights=false&forDashboard=true&forDashboardTopCompanies=true"
                res.redirect(url)
            })
        }
    })
});

router.get('/dashboard/contacts/interacted/for/first/time', common.isLoggedInUserToGoNext, function(req, res) {
    var timezone;
    var uId = common.getUserId(req.user)
    var userId = common.castToObjectId(uId)
    todayDateMinMax(userId, function(err, date) {
        if (err || !date)
            res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
        else {
            var minDate = moment(date.min).subtract(1, "days").toDate()
            var maxDate = moment(date.max).subtract(1, "days").toDate()
            interactionManagementObj.getContactsInteractedForFirstTimeByDate(userId, minDate, maxDate, function(err, result) {
                if (err)
                    res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                else {
                    if (result.length > 0) {
                        var contacts = _.compact(_.pluck(result, '_id'));
                        userManagementObj.searchUserConatctsByEmailAndMobile(userId, contacts, function(error, contact) {
                            if (!error && contact) {
                                res.send({
                                    SuccessCode: 1,
                                    ErrorCode: 0,
                                    Data: {
                                        contactsInteractedForFirstTime: contact
                                    }
                                })
                            } else
                                res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                        });
                    } else {
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            Data: {
                                contactsInteractedForFirstTime: []
                            }
                        })
                    }
                }
            })
        }
    });
});

router.post('/dashboard/contacts/remind', common.isLoggedInUserToGoNext, function(req, res) {
    var uId = common.getUserId(req.user)
    var userId = common.castToObjectId(uId)
    var contactIds = common.castListToObjectIds(req.body.contactIds)
    var date = moment().add(7, "days").toDate();
    userManagementObj.updateReminderForContacts(userId, contactIds, date, function(err, result) {
        if (!err && result) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0
            })
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
    });
});

router.get('/remind/to/connect/all',common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    userManagementObj.getRemindToConnectContacts(common.castToObjectId(userId),function (contacts) {
        res.send(contacts)
    });
});

router.get('/check/onboarding/user',common.isLoggedInUserToGoNext, function(req, res){

    var userId = common.getUserId(req.user);
    userManagementObj.checkInsightsBuiltStatus(common.castToObjectId(userId),function (err,insightsBuiltStatus) {

        var fromOnBoarding = false;
        if(req.session.fromOnBoarding){
            fromOnBoarding = true;
        }

        if(!err && insightsBuiltStatus){
            res.send({
                fromOnBoarding:fromOnBoarding,
                insightsBuiltStatus:insightsBuiltStatus.insightsBuilt,
                publicProfileUrl:insightsBuiltStatus.publicProfileUrl
            })
        } else {
            res.send({
                fromOnBoarding:fromOnBoarding,
                insightsBuiltStatus:insightsBuiltStatus.insightsBuilt,
                publicProfileUrl:insightsBuiltStatus.publicProfileUrl
            })
        }
    });
});

router.get('/get/contacts/interactions/count',common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    var userIdArray = [];

    userIdArray.push(common.castToObjectId(userId));

    contactSupportObj.getContactsCount(userIdArray,function (numberOfContacts) {
        interactionManagementObj.totalInteractionCountByRange(common.castToObjectId(userId),function (interactionsCount) {
            if(!numberOfContacts || numberOfContacts.length == 0 || !interactionsCount[0]){
                res.send({contactsCount:0,interactionsCount:0})
            } else {
                res.send({contactsCount:numberOfContacts[0].contactsCount,interactionsCount:interactionsCount[0].count})
            }
        });
    });
});

router.get('/get/user/contacts/accounts',common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    contactSupportObj.getContactsAccountCount(common.castToObjectId(userId),function (numberOfAccounts) {
       res.send(numberOfAccounts)
    });
});

router.get('/dashboard/user/open/slots',common.isLoggedInUserToGoNext, function(req, res){
    var userId = common.getUserId(req.user);
    // userOpenSlotsObj.findEmailSendingPreferredTimeSlots(common.castToObjectId(userId),function(error,result){
    //     res.send({error:error,result:result})
    // })
    // userOpenSlotsObj.findUserMeetingOpenSlots(common.castToObjectId(userId),function(error, result){
    //     res.send({error:error,result:result})
    // })
    userOpenSlotsObj.findRelatasPreferredSlots(common.castToObjectId(userId),function(email,result){
        // res.send({email:email,result:result})
        res.send({result:result});
    })
});

router.get('/dashboard/user/open/slots/email',common.isLoggedInUserToGoNext, function(req, res){ //TODO sunil remove
    var userId = common.getUserId(req.user);
    userOpenSlotsObj.findEmailSendingPreferredTimeSlots(common.castToObjectId(userId),function(error,result){
        res.send({error:error,result:result})
    })
});

router.get('/dashboard/user/open/slots/meeting',common.isLoggedInUserToGoNext, function(req, res){ //TODO sunil remove
    var userId = common.getUserId(req.user);
    userOpenSlotsObj.findUserMeetingOpenSlots(common.castToObjectId(userId),function(error, result){
        res.send({error:error,result:result})
    })
});

router.get('/todaySeries/meetingView',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/meetingView');
});

router.get('/todaySeries/mailsToRespond',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/mailsToRespond');
});

router.get('/todaySeries/mailsToFollowUp',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/mailsToFollowUp');
});

router.get('/todaySeries/losingTouch',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/losingTouch');
});

router.get('/todaySeries/meetingsToday',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/meetingsToday');
});

router.get('/todaySeries/upcomingMeetings',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/upcomingMeetings');
});

router.get('/todaySeries/reminderToConnect',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/reminderToConnect');
});

router.get('/todaySeries/travellingToLocation',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/travellingToLocation');
});

router.get('/todaySeries/meetingFollowUp',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/meetingFollowUp');
});

router.get('/todaySeries/socialNews',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/socialNews');
});

router.get('/todaySeries/mailsToRespondActionBoard',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/mailsToRespondActionBoard');
});

router.get('/todaySeries/mailsToRespondActionBoard_v2',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/mailsToRespondActionBoard_v2');
});

router.get('/todaySeries/sendMailToConnectActionBoard',common.isLoggedInUserToGoNext, function(req, res){
    res.render('todaySeries/sendMailToConnectActionBoard');
});

router.get('/navigation/notificationsHeader',common.isLoggedInUserOrMobile, function(req, res){
    res.render('navigation/notificationsHeader');
});

function todayDateMinMax(userId, callback) {
    allUsers.find({ _id: userId }, { timezone: 1 }, function(err, user) {
        if (err || user.length == 0)
            callback(err, null);
        else {
            if (user[0].timezone && user[0].timezone.name)
                timezone = user[0].timezone.name;
            else
                timezone = 'UTC';
            var dateMin = moment().tz(timezone);
            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)
            var dateMax = moment().tz(timezone);

            dateMax.hour(23)
            dateMax.minute(59)
            dateMax.second(59)
            dateMax.millisecond(0)
            callback(null, { min: dateMin, max: dateMax })
        }
    });
}

function findExtendedNetwork(userId, companies, filter, callback){
    allUsers.find({_id:{$in:userId}},{contacts:1}).exec(function(error, userContacts){
        if(error)
            callback(err, null);
        else {
            var emailIdArr = [];
            emailIdArr = _.filter(userContacts[0].contacts, function(a) {
                if (a.personEmailId) {
                    if(userContacts[0].emailId) {
                        if (userContacts[0].emailId.toLowerCase() !== a.personEmailId.toLowerCase())
                            return a.personEmailId;
                    }
                    else
                        return a.personEmailId;
                }
            })
            emailIdArr = _.compact(_.pluck(emailIdArr, "personEmailId"));
            //emailIdArr.push(userContacts[0].emailId);
            var q = {
                "contacts.account.name": {$in: companies},
                "contacts.personEmailId":{$nin:emailIdArr},
                emailId:{$in:emailIdArr}
            };
            if(filter.indexOf("influencerAndDecisionMaker") != -1)
                q["contacts.contactRelation.decisionmaker_influencer"] = {$in: ['decision_maker', 'influencer']}

            allUsers.aggregate([
                {$match: {_id: {$ne: userId}}},
                {$unwind: "$contacts"},
                {
                    $match: q
                }, {
                    $group: {
                        _id: "$contacts.account.name",
                        contacts: {$addToSet: {email: "$contacts.personEmailId"}},
                    }
                }, {
                    $project: {
                        _id: 0,
                        company: "$_id",
                        extendedNetwork: {$size: "$contacts"},
                        //contacts:"$contacts"
                    }
                }
            ], function (err, extendedNet) {
                if (err)
                    callback(err, null)
                else {
                    callback(err, extendedNet)
                }
            });
        }
    })
}

function kickOffExtendedNetwork(userId,res,callback) {
    contactObj.getAccountsOfAllContacts(userId, function(error, accounts){
        if(error)
            res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
        else{
            if(accounts.length > 0){
                var companies = _.compact(_.pluck(accounts, "company"));
                var filter = [];
                filter.push("influencerAndDecisionMaker");
                findExtendedNetwork(userId, companies, filter, function(error, extendedContacts){
                    if(error)
                        res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                    else{

                        if(!callback){
                            res.send({
                                SuccessCode: 1,
                                ErrorCode: 0,
                                Data: extendedContacts
                            })
                        } else {
                            callback(extendedContacts)
                        }
                    }
                })
            }
            else{
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data: []
                })
            }
        }
    })
}

function kickOffExtendedNetworkForRecentlyInteracted(userId,res,callback) {
    todayDateMinMax(userId, function(err, date) {
        if (err || !date)
            res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
        else {
            var minDate = moment(date.min).subtract(8, "days").toDate()
            var maxDate = moment(date.max).subtract(1, "days").toDate()
            interactionManagementObj.topCompaniesInteractedByDate(userId, minDate, maxDate, function(err, result) {
                if (err)
                    res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                else {
                    if (result.length > 0) {
                        var companies = _.compact(_.pluck(result, 'company'));
                        var filter = [];
                        //filter.push("influencerAndDecisionMaker")
                        findExtendedNetwork(userId, companies, filter, function(error, extendedContacts){
                            if(error)
                                res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                            else{
                                var topCompanies = _.map(result, function(a) {
                                    return _.extend(a, _.find(extendedContacts, function(b) {
                                        return a.company === b.company;
                                    }))
                                });

                                topCompanies.forEach(function(a){
                                    if(!a.extendedNetwork)
                                        a.extendedNetwork = 0;
                                });

                                if(!callback){
                                    res.send({
                                        SuccessCode: 1,
                                        ErrorCode: 0,
                                        Data: topCompanies
                                    })
                                } else {
                                    callback(topCompanies)
                                }
                            }
                        })

                    } else {
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            Data:[]
                        })
                    }
                }
            })
        }
    });
}

module.exports = router;
