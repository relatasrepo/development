
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var crypto = require('crypto');
var fs = require('fs');
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var request=require('request');
var aggregateSupport = require('../../common/aggregateSupport');
var googleCalendar = require('../../common/googleCalendar');
var gmailApi = require('node-gmail-api');
var officeOutlookApi = require('../../common/officeOutlookAPI');
var _ = require("lodash");

var validations = require('../../public/javascripts/validation');
var emailSender = require('../../public/javascripts/emailSender');
var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var profileManagementClass = require('../../dataAccess/profileManagementClass');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var appCredentials = require('../../config/relatasConfiguration');
var contactClass = require('../../dataAccess/contactsManagementClass');
var contactManagementClass = require('../../dataAccess/contactManagement');
var winstonLog = require('../../common/winstonLog');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var adminClass = require('../../dataAccess/corporateDataAccess/adminModelClass');
var admin = new adminClass();
var company = new companyClass();
var contactObj = new contactClass();
var validation = new validations();
var common = new commonUtility();
var userManagementObj = new userManagement();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var profileManagementClassObj = new profileManagementClass();
var appCredential = new appCredentials();
var logLib = new winstonLog();
var officeOutlook = new officeOutlookApi();
var contactManagementObj = new contactManagementClass();

var logger = logLib.getWinston();
var statusCodes = errorMessagesObj.getStatusCodes();
var userCollection = require('../../databaseSchema/userManagementSchema').User;

var redis = require('redis');
var redisClient = redis.createClient();

router.get('/settings/profile',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(common.getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                res.render('profile/settings',{isLoggedIn : common.isLoggedInUserBoolean(req),companyId:companyProfile._id.toString(),company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
            }
            else res.render('profile/settings');
        });
    }
    else res.render('profile/settings')
});
router.get('/api/v2/workingHours',common.isLoggedInUserToGoNext,function(req,res){
    var userId=common.getUserId(req.user);

    userManagementObj.findUserProfileByIdWithCustomFields(userId,{workHoursWeek:1},function(err,user){
        if(!err){
            return res.send(user);
        }else{
            res.statusCode = 500;
            return res.send({error: 'Server error'+err});
        }
    })
});
router.get('/api/v2/calendar-lists',function(req,res){
    userCollection.find(function(err,user){
        if(!err){
            return res.send(user);
        }else{
            res.statusCode = 500;
            return res.send({error: 'Server error'+err});
        }
    });
});

var authConfig = appCredential.getAuthCredentials();

router.get('/profile/get/current/web',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    var key = "userProfile"+String(userId);

    getCache(key,function (data) {
        if(data){
            if(data.companyId){
                company.findCompanyById(common.castToObjectId(data.companyId),function (companyDetails) {
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "appConfig":data.corporateUser,
                        uniqueName:common.getValidUniqueName(data.publicProfileUrl),
                        "Data":data,
                        companyDetails:companyDetails
                    });
                })
            } else {

                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "appConfig":data.corporateUser,
                    uniqueName:common.getValidUniqueName(data.publicProfileUrl),
                    "Data":data,
                    companyDetails:{
                        "opportunityStages": [
                            {
                                "isEnd": false,
                                "isStart": true,
                                "isLost": false,
                                "isWon": false,
                                "order": 1,
                                "weight": 20,
                                "name": "Prospecting"
                            },
                            {
                                "isEnd": false,
                                "isStart": false,
                                "isLost": false,
                                "isWon": false,
                                "order": 2,
                                "weight": 10,
                                "name": "Evaluation"
                            },
                            {
                                "isEnd": false,
                                "isStart": false,
                                "isLost": false,
                                "isWon": false,
                                "order": 3,
                                "weight": 30,
                                "name": "Proposal"
                            },
                            {
                                "isEnd": true,
                                "isStart": false,
                                "isLost": false,
                                "isWon": false,
                                "order": 4,
                                "weight": 40,
                                "name": "Close Won"
                            },
                            {
                                "isEnd": true,
                                "isStart": false,
                                "isLost": false,
                                "isWon": false,
                                "order": 5,
                                "weight": 40,
                                "name": "Close Lost"
                            }
                        ]
                    }
                });
            }
        } else {
            userManagementObj.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,user){
                if(error){
                    logger.info('Error in getProfileOrStoreProfileInSession():CommonUtility 2 user: '+userId ,error);
                }

                if(common.checkRequired(user)){

                    setCache(key,user);

                    user.publicProfileUrl = common.getValidUniqueName(user.publicProfileUrl);
                    req.session.profile = user;
                    req.session.profileTimezone = common.getTimezone(user.timezone);

                    //Check for appConfig Corporate user and get CompanyId
                    var isCorporateUser = false;
                    if(user.corporateUser && user.companyId){
                        isCorporateUser = true;
                    }

                    if(user.companyId){
                        company.findCompanyById(user.companyId,function (companyDetails) {
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "appConfig":isCorporateUser,
                                uniqueName:common.getValidUniqueName(user.publicProfileUrl),
                                "Data":user,
                                companyDetails:companyDetails
                            });
                        })
                    } else {

                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "appConfig":isCorporateUser,
                            uniqueName:common.getValidUniqueName(user.publicProfileUrl),
                            "Data":user,
                            companyDetails:{
                                "opportunityStages": [
                                    {
                                        "isEnd": false,
                                        "isStart": true,
                                        "isLost": false,
                                        "isWon": false,
                                        "order": 1,
                                        "weight": 20,
                                        "name": "Prospecting"
                                    },
                                    {
                                        "isEnd": false,
                                        "isStart": false,
                                        "isLost": false,
                                        "isWon": false,
                                        "order": 2,
                                        "weight": 10,
                                        "name": "Evaluation"
                                    },
                                    {
                                        "isEnd": false,
                                        "isStart": false,
                                        "isLost": false,
                                        "isWon": false,
                                        "order": 3,
                                        "weight": 30,
                                        "name": "Proposal"
                                    },
                                    {
                                        "isEnd": true,
                                        "isStart": false,
                                        "isLost": false,
                                        "isWon": false,
                                        "order": 4,
                                        "weight": 40,
                                        "name": "Close Won"
                                    },
                                    {
                                        "isEnd": true,
                                        "isStart": false,
                                        "isLost": false,
                                        "isWon": false,
                                        "order": 5,
                                        "weight": 40,
                                        "name": "Close Lost"
                                    }
                                ]
                            }
                        });
                    }

                }
                else res.send(errorObj.generateErrorResponse({
                    status: statusCodes.PROFILE_ERROR_CODE,
                    key: 'USER_PROFILE_NOT_FOUND'
                }));
            });
        }
    })
});

router.get('/profile/get/edit',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var project = {notification:1,workHoursWeek:1,profilePrivatePassword:1,firstName:1,lastName:1,emailId:1,profilePicUrl:1,
        publicProfileUrl:1,companyName:1,designation:1,location:1,timezone:1,mobileNumber:1,mobileNumberWithoutCC:1,
        countryCode:1,skypeId:1,google:1,linkedin:1,twitter:1,facebook:1,contacts: { $size: "$contacts" },
        corporateUser:1,outlook:1,actionableMailSetting:1,dashboardSetting:1,salesforce:1,lastLoginDate:1,
        penultimateLoginDate:1,companyId:1,hierarchyParent:1,corporateAdmin:1}
    userManagementObj.selectUserProfileWithContactCount(common.castToObjectId(userId),project,function(error,user){
        if(error){
            logger.info('Error in /profile/get/edit:profile router 2 user: '+userId ,error);
        }

        if(common.checkRequired(user)){
            user.uniqueName = req.headers.host+'/'+common.getValidUniqueName(user.publicProfileUrl);

            user.prStatus = {
                profileCompletion:checkMandatoryFields(user),
                socialAccounts:checkSocialAccounts(user),
                calendarSettings:checkCalendarSettings(user),
                contactsImported:checkContactsUploaded(user.contactsUpload),
                inviteFriends:false
            };
            var totalProgress = 16.6666;
            totalProgress += (user.prStatus.profileCompletion*16.6666)/100;
            totalProgress += (user.prStatus.socialAccounts*16.6666)/100;
            totalProgress += (user.prStatus.calendarSettings*16.6666)/100;
            totalProgress += (user.prStatus.contactsImported*16.6666)/100;
            totalProgress += (user.prStatus.inviteFriends*16.6666)/100;
            user.prStatus.totalProgress = totalProgress;
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":user,
                "googleApiKey":authConfig.GOOGLE_API_KEY
            });
        }
        else res.send(errorObj.generateErrorResponse({
            status: statusCodes.PROFILE_ERROR_CODE,
            key: 'USER_PROFILE_NOT_FOUND'
        }));
    });
});

router.get('/profile/get/emailId/:emailId/web',function(req,res){

    if(common.checkRequired(req.params.emailId)){
        userManagementObj.findUserProfileByEmailIdWithCustomFields(req.params.emailId,{publicProfileUrl:1,profilePicUrl:1,firstName:1,lastName:1,emailId:1},function(error,user){
            if(error){
                logger.info('Error in /profile/get/emailId/:emailId/web: webRes/profile ' ,error);
            }

            if(common.checkRequired(user)){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":user
                });
            }
            else res.send(errorObj.generateErrorResponse({
                status: statusCodes.PROFILE_ERROR_CODE,
                key: 'USER_PROFILE_NOT_FOUND'
            }));
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'EMAIL_ID_NOT_FOUND_IN_REQUEST'}));
});

function checkContactsUploaded(contactsUploaded){
    if(common.checkRequired(contactsUploaded) && contactsUploaded.isUploaded){
        return 100;
    }else return 0
}

function checkCalendarSettings(user){
    var value = 0;
    if(common.checkRequired(user.workHoursWeek) && common.checkRequired(user.workHoursWeek.weekdayHours) && common.checkRequired(user.workHoursWeek.weekdayHours.start)){
        value += 100
    }

    return value;
}

function checkSocialAccounts(user){
    var value = 0;
    if(common.checkRequired(user.twitter) && common.checkRequired(user.twitter.id)){
        value += 33.3333
    }
    if(common.checkRequired(user.linkedin) && common.checkRequired(user.linkedin.id)){
        value += 33.3333
    }
    if(common.checkRequired(user.facebook) && common.checkRequired(user.facebook.id)){
        value += 33.3333
    }
    return value;
}

function checkMandatoryFields(userProfile) {
    var value = 0;
    if(common.checkRequired(userProfile.firstName)){
        value += 11.1111
    }
    if(common.checkRequired(userProfile.skypeId)){
        value += 11.1111
    }
    if(common.checkRequired(userProfile.lastName)){
        value += 11.1111
    }
    if(common.checkRequired(userProfile.location)){
        value += 11.1111
    }
    if(common.checkRequired(userProfile.designation)){
        value += 11.1111
    }
    if(common.checkRequired(userProfile.companyName)){
        value += 11.1111
    }
    if(common.checkRequired(userProfile.mobileNumber)){
        value += 11.1111
    }
    if(common.checkRequired(userProfile.timezone) && common.checkRequired(userProfile.timezone.name)){
        value += 11.1111
    }

    if(common.checkRequired(userProfile.profilePicUrl) && userProfile.profilePicUrl.indexOf('/default.png') == -1){
        value += 11.1111
    }
    return value;
}

router.get('/profile/update/primary/google',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var googleId = req.query.googleId;
    if(common.checkRequired(googleId)){
        userManagementObj.updateGoogleAccountToPrimary(userId,googleId,function(error,isUpdated){
            res.send({
                "SuccessCode": isUpdated ? 1 : 0,
                "Message": isUpdated ? "" : errorMessagesObj.getMessage("UPDATE_GOOGLE_PRIMARY_FAILED"),
                "ErrorCode": isUpdated ? 0 : 1,
                "Data": {}
            });
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'GOOGLE_ID_NOT_FOUND_IN_REQUEST'}));
});

router.get('/profile/update/primary/outlook',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var outlookId = req.query.outlookId;
    if(common.checkRequired(outlookId)){
        userManagementObj.updateOutlookAccountToPrimary(userId,outlookId,function(error,isUpdated){
            res.send({
                "SuccessCode": isUpdated ? 1 : 0,
                "Message": isUpdated ? "" : errorMessagesObj.getMessage("UPDATE_OUTLOOK_PRIMARY_FAILED"),
                "ErrorCode": isUpdated ? 0 : 1,
                "Data": {}
            });
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'GOOGLE_ID_NOT_FOUND_IN_REQUEST'}));
});

router.get('/profile/update/remove/google',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
    var googleId = req.query.googleId;
    if(common.checkRequired(googleId)){
        userManagementObj.removeGoogleAccountBy_Id(userId,googleId,function(error,isUpdated,message){
            logger.info(message.message);
            res.send({
                "SuccessCode": isUpdated ? 1 : 0,
                "Message": isUpdated ? "" : errorMessagesObj.getMessage("REMOVE_GOOGLE_ACCOUNT_FAILED"),
                "ErrorCode": isUpdated ? 0 : 1,
                "Data": {}
            });
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'GOOGLE_ID_NOT_FOUND_IN_REQUEST'}));
});

router.get('/profile/update/remove/outlook',common.isLoggedInUserToGoNext,function(req,res){

    var userId = common.getUserId(req.user);
    var outlookId = req.query.outlookId;
    if(common.checkRequired(outlookId)){
        userManagementObj.removeOutlookAccountBy_Id(userId,outlookId,function(error,isUpdated,message){
            logger.info(message.message);
            res.send({
                "SuccessCode": isUpdated ? 1 : 0,
                "Message": isUpdated ? "" : errorMessagesObj.getMessage("REMOVE_OUTLOOK_ACCOUNT_FAILED"),
                "ErrorCode": isUpdated ? 0 : 1,
                "Data": {}
            });
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'OUTLOOK_ID_NOT_FOUND_IN_REQUEST'}));
});

router.delete('/profile/update/remove/linkedin/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    userManagementObj.updateLinkedin(userId,'','','','',function(error,isUpdated,message){
        logger.info(message.message);
        res.send({
            "SuccessCode": isUpdated ? 1 : 0,
            "Message": isUpdated ? "" : errorMessagesObj.getMessage("REMOVE_LINKEDIN_ACCOUNT_FAILED"),
            "ErrorCode": isUpdated ? 0 : 1,
            "Data": {}
        });
    });
});

router.delete('/profile/update/remove/facebook/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    userManagementObj.updateFacebook(userId,'','','','',function(error,isUpdated,message){
        logger.info(message.message);
        res.send({
            "SuccessCode": isUpdated ? 1 : 0,
            "Message": isUpdated ? "" : errorMessagesObj.getMessage("REMOVE_FACEBOOK_ACCOUNT_FAILED"),
            "ErrorCode": isUpdated ? 0 : 1,
            "Data": {}
        });
    });
});

router.delete('/profile/update/remove/twitter/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    userManagementObj.updateTwitter(userId,'','','','','',function(error,isUpdated,message){
        logger.info(message.message);
        res.send({
            "SuccessCode": isUpdated ? 1 : 0,
            "Message": isUpdated ? "" : errorMessagesObj.getMessage("REMOVE_TWITTER_ACCOUNT_FAILED"),
            "ErrorCode": isUpdated ? 0 : 1,
            "Data": {}
        });
    });
});

router.get('/profile/update/add/linkedin/dashboard',common.isLoggedInUser,function(req,res){
    res.redirect('/linkedinEditProfile?finalPage=dashboard');
});

router.get('/profile/update/add/facebook/dashboard',common.isLoggedInUser,function(req,res){
    res.redirect('/facebookEditProfile?finalPage=dashboard');
});

router.get('/profile/update/add/twitter/dashboard',common.isLoggedInUser,function(req,res) {
    res.redirect('/twitterEditProfile?finalPage=dashboard');

});

router.get('/profile/update/add/linkedin/web',common.isLoggedInUser,function(req,res){
    res.redirect('/linkedinEditProfile?finalPage=settings');
});

router.get('/profile/update/add/facebook/web',common.isLoggedInUser,function(req,res){
    res.redirect('/facebookEditProfile?finalPage=settings');
});

router.get('/profile/update/add/twitter/web',common.isLoggedInUser,function(req,res){
    res.redirect('/twitterEditProfile?finalPage=settings');
});

router.post('/profile/update/settings',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);

    if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
        var profile = JSON.parse(JSON.stringify(req.body));
        
        var dataToSend = {
            emailId:profile.emailId,
            firstName:profile.firstName
        };

        if(common.checkRequired(profile.calPass) && profile.calPass != 'encrypted'){
            profile.profilePrivatePassword = encryptionValue(profile.calPass);
        }
        else
        if(!common.checkRequired(profile.calPass)){
            delete profile.calPass;
            profile.profilePrivatePassword = null;
        }
        else delete profile.calPass;

        delete profile.emailId;

        var date = moment().tz(profile.timezone);
        profile.timezone = {
            name: profile.timezone,
            zone: date.format("Z"),
            updated: true
        };

        profile.zoneNumber = getZoneNumber(profile.timezone.zone);
        var pathUpdate = {companyId:req.body.companyId,userId:userId,parentId:req.body.reportingManager};
        delete profile.companyId;
        delete profile.reportingManager;

        profileManagementClassObj.updateProfileCustomQuery(userId,profile,function(isUpdated){

            if(common.checkRequired(pathUpdate.parentId) && common.checkRequired(pathUpdate.companyId)){
                admin.updateAssigned(pathUpdate,function(isSuccess){

                })
            }

            common.updateContactMultiCollectionLevel(userId,false);
            var key = "userProfile"+String(userId);
            redisClient.del(key)

            res.send({
                "SuccessCode": isUpdated ? 1 : 0,
                "Message": isUpdated ? errorMessagesObj.getMessage("UPDATE_PROFILE_SUCCESS") : errorMessagesObj.getMessage("UPDATE_PROFILE_FAILED"),
                "ErrorCode": isUpdated ? 0 : 1,
                "Data": {}
            });
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("UPDATE_PROFILE_FAILED")));
});

function getZoneNumber(zone){
    if(zone){
        var zoneStr = zone.toString();
        var num = parseInt(zoneStr.substring(0, 3));
        if(zoneStr.charAt(0) == '+'){

            if(num >= 6 && num <= 11){
                return 1;
            }else if(num >= 0 && num <= 5){
                return 2;
            }else return 0;
        }else if(zoneStr.charAt(0) == '-'){
            if(num >= -6 && num <= 0){
                return 3;
            }else if(num >= -11 && num <= -6){
                return 4;
            }else return 0;
        }else return 0;
    }else return 0;
}

function encryptionValue(data){
    var shaAlgorithm = crypto.createHash('sha256');
    shaAlgorithm.update(data);

    var hashedData = shaAlgorithm.digest('hex');
    return hashedData;
}

router.get('/settings', common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render("settings/settings",{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.post('/update/account/visibility', common.isLoggedInUser,common.checkUserDomain,function(req,res){
    var userId = common.getUserId(req.user);
    var contacts = req.body;
    
    contactObj.updateAccountVisibility(common.castToObjectId(userId),contacts,common.castToObjectId,function (err, result) {
        if(result){
            res.send(true)
        }
    });
});

router.post('/profile/update/profilepic/web',function(req, res) {
    if(req.user){
        var userId = common.getUserId(req.user);
    } else {
        userId = req.session.partialFillAcc._id
    }

    if (common.checkRequired(userId)) {
        var fstream;
        if(common.checkRequired(req.busboy)){
            req.pipe(req.busboy);
            req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {

                if(common.contains(mimetype,'image')){
                    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,publicProfileUrl:1},function(error, userData){
                        if(error || userData == null){
                            res.send(errorObj.generateErrorResponse({
                                status: statusCodes.PROFILE_ERROR_CODE,
                                key: 'ERROR_FETCHING_PROFILE'
                            },errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_FAILED")));
                        }
                        else{
                            var extension = filename.split('.')[filename.split('.').length-1];
                            var uniqueName = common.getValidUniqueName(userData.publicProfileUrl);
                            var imagePath = '/profileImages/'+uniqueName+'.'+extension || 'jpg';
                            fstream = fs.createWriteStream('./public/profileImages/' + uniqueName+'.'+extension || 'jpg');
                            file.pipe(fstream);
                            fstream.on('close', function () {
                                userManagementObj.updateProfileImage(common.castToObjectId(userId.toString()),imagePath,function(error,isUpdated,message){
                                    if(error){
                                        res.send(errorObj.generateErrorResponse({status: statusCodes.PROFILE_ERROR_CODE, key: 'FAILED_TO_UPDATE_PROFILE_PIC'},errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_FAILED")));
                                    }
                                    else if(isUpdated){
                                        res.send({
                                            "SuccessCode": 1,
                                            "Message": errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_SUCCESS"),
                                            "ErrorCode": 0,
                                            "Data": {

                                            }
                                        });
                                    }
                                    else res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG'},errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_FAILED")));
                                });
                            });
                        }
                    });
                }
                else{
                    res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_IMAGE_FILE_TYPE'},errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_FAILED")));
                }
            });
        } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'NO_FILES_RECEIVED'},errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_FAILED")));
    } else res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'},errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_FAILED")));
});

router.get('/refresh/google/contacts',common.isLoggedInUserToGoNext,function(req, res){
    var userId = common.getUserId(req.user);
    if (common.checkRequired(userId)) {
        userManagementObj.getUserGoogleRefreshToken(userId,function(googleRefreshToken){
            
            if(googleRefreshToken && googleRefreshToken.google.length>0){

                getNewGoogleToken(googleRefreshToken.google[0].token,googleRefreshToken.google[0].refreshToken,function(token){
                    if(token){
                        getContactsFromGoogleApi(token,googleRefreshToken._id,function(err,result){
                            if(!err && (result || result == 0)){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": errorMessagesObj.getMessage("REFRESH_GOOGLE_CONTACTS_SUCCESS"),
                                    "ErrorCode": 0,
                                    "Data":{
                                        "contactsAdded":result
                                    }
                                });

                                logger.info("Google contacts synced for user " + googleRefreshToken.emailId + "Total Contacts -" +result)
                            } else {
                                res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG'},errorMessagesObj.getMessage("REFRESH_GOOGLE_CONTACTS_FAILED")))
                            }
                        });

                    } else {
                        res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG'},errorMessagesObj.getMessage("REFRESH_GOOGLE_CONTACTS_FAILED")))
                    }
                });
            } else {
                res.redirect('/refresh/outlook/contacts');
            }
        })
    }
});

router.get('/refresh/outlook/contacts',common.isLoggedInUserToGoNext,function(req, res){
    
    var userId = common.getUserId(req.user);
    var syncContacts = true;
    if (common.checkRequired(userId)) {
        userManagementObj.getUserProfileWithoutContacts(userId,function(err, userProfile){

            if(!err && userProfile){

                var outlookAccounts = userProfile.outlook;
                var counter = 0;
                var len = outlookAccounts.length;
                var numberOfContactsAdded = 0;

                _.each(outlookAccounts,function (outlookAccount) {
                    counter++;
                    len--;

                    setTimeout(function(){
                        kickOffSyncContacts(outlookAccount.refreshToken,userProfile,req,res)
                    }, counter * 1000);
                    
                    function kickOffSyncContacts(refreshToken,userProfile,req,res) {

                        officeOutlook.getNewAccessTokenWithoutStore(refreshToken,function (newToken) {
                            //TODO sync contacts secondary
                            officeOutlook.kickOffContactsProcess(newToken.access_token, userProfile,syncContacts,function (error,response) {
                                if(!error){
                                    numberOfContactsAdded = numberOfContactsAdded+response;

                                    if(len == 0){
                                        res.send({
                                            "SuccessCode": 1,
                                            "Message": errorMessagesObj.getMessage("REFRESH_OUTLOOK_CONTACTS_SUCCESS"),
                                            "ErrorCode": 0,
                                            "Data":{
                                                "contactsAdded":numberOfContactsAdded
                                            }
                                        });
                                    }
                                } else {

                                    if(len == 0){
                                        res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG'},errorMessagesObj.getMessage("REFRESH_OUTLOOK_CONTACTS_FAILED")))
                                    }
                                }
                            })
                        });
                    }
                });
            } else {
                res.send(errorObj.generateErrorResponse({status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG'},errorMessagesObj.getMessage("REFRESH_OUTLOOK_CONTACTS_FAILED")))
            }
        })
    }
});

router.get('/getEmail',function(req,res){
    var after = req.query.after;
    var before = req.query.before;
    var userId = req.query.userId;
        // after:2016/6/14 before:2016/6/18

    var filter = 'after:'+after +' '+'before:'+before;

    if (common.checkRequired(userId)) {
        userManagementObj.getUserGoogleRefreshToken(common.castToObjectId(userId), function (googleRefreshToken) {

            getNewGoogleToken(googleRefreshToken.google[0].token, googleRefreshToken.google[0].refreshToken, function (token) {

                common.getInvalidEmailListFromDb(function (invalidEmailList) {

                    if (token) {
                        var gmail = new gmailApi(token);
                        var allEmails = [];
                        var messagesByThread = [];

                        var s = gmail.messages(filter);

                        s.on('end',function(){

                            // var data = _.compact(checkPrevMailHasBeenReplied(messagesByThread,googleRefreshToken.emailId))
                            // updateInteractionActivity(common.castToObjectId(userId),data)
                            res.send(allEmails)

                        });

                        s.on('error',function(a){
                            res.send(allEmails)
                        });

                        s.on('data', function (d) {
                            if (common.checkRequired(d) && common.checkRequired(d.payload) && common.checkRequired(d.payload.headers)) {
                                if (d.payload.headers.length > 0) {
                                    allEmails.push(d)

                                    var headers = d.payload.headers;
                                    var email = parseEmailHeader(d.id, headers, d.threadId,invalidEmailList,d);

                                    email.snippet = d.snippet
                                    // messagesByThread.push(email)
                                }
                            }
                        });
                    }
                })
            })
        })
    }
});

function updateInteractionActivity(userId,data){
    contactManagementObj.updateInteractionActivity(userId,data)
}

function checkPrevMailHasBeenReplied (messages,userEmailId) {

    var data = _
        .chain(messages)
        .groupBy('emailThreadId')
        .map(function(value, key) {

            var sent = 0,received = 0;

            value.sort(function (o2, o1) {
                return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
            });

            var lastButOneSender = null;

            if(value && value.length>0){
                lastButOneSender = value[1]?value[1]:null
            }

            if(value && value.length == 2){
                lastButOneSender = value[0]
            }

            var emailIds = [];


            _.each(value,function (val) {
                if(val.from[0] == userEmailId){
                    sent++
                } else {
                    received++
                }

                if(val.To && val.To.length>0){
                    emailIds = emailIds.concat(val.To)
                }

                if(val.Cc && val.Cc.length>0){
                    emailIds = emailIds.concat(val.Cc)
                }

                if(val.Bcc && val.Bcc.length>0){
                    emailIds = emailIds.concat(val.Bcc)
                }
            })

            if(lastButOneSender){
                if(lastButOneSender.from[0].trim().toLowerCase() != userEmailId){
                    lastButOneSender = null;
                }
            }

            return {
                emailThreadId:key,
                lastButOneSender:lastButOneSender?true:false,
                cEmailIds:emailIds,
                received:received,
                sent:sent
            };

            // return {
            //     emailThreadId:key,
            //     lastButOneSender:lastButOneSender,
            //     data:value
            // }

        })
        .value();

    return data;
}

function getNewGoogleToken(token,refreshToken,callback){
    var tokenProvider = new GoogleTokenProvider({
        refresh_token: refreshToken,
        client_id:     authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        access_token: token
    });

    tokenProvider.getToken(function (err, token) { // start of token provider
        if (err) {
            callback(false)
        }
        else{
            callback(token)
        }
    })
}

function getContactsFromGoogleApi(access_token,userId,callback) {

    var qs = {
        'alt': 'json',
        'max-results': 10000,
        'orderby': 'lastmodified'
    }

    request.get({
        url: 'https://www.google.com/m8/feeds/contacts/default/full',

        qs: qs,
        headers: {
            'Authorization': 'Bearer ' + access_token,
            'GData-Version': '3.0'
        }
    }, function (err, res, body) {

        if(!res){
            logger.info("Error get google contacts ",body, err);
        }
        else if (res.statusCode === 401) {
            logger.info("Error get google contacts ",body, err);
        }
        else{

            var feed = JSON.parse(body);

            var emailIdArr = [];
            if(feed && feed.feed && feed.feed.entry){
                var users = feed.feed.entry.map(function (c) {

                    var r = {};
                    if (c['gd$name'] && c['gd$name']['gd$fullName']) {
                        r.name = c['gd$name']['gd$fullName']['$t'];
                    }
                    if(c['gContact$birthday'] && c['gContact$birthday'].when){
                        r.birthday = c['gContact$birthday'].when
                    }
                    if(c['gd$organization'] && c['gd$organization'].length >  0){
                        if(c['gd$organization'][0]['gd$orgName'])
                            r.companyName = c['gd$organization'][0]['gd$orgName']['$t'];
                        if(c['gd$organization'][0]['gd$orgTitle'])
                            r.designation = c['gd$organization'][0]['gd$orgTitle']['$t'];
                    }
                    if (c['gd$email'] && c['gd$email'].length > 0) {

                        r.email = c['gd$email'][0]['address'];
                        if(typeof r.email == 'string'){
                            emailIdArr.push(r.email.trim().toLowerCase());
                        }
                        r.nickname = c['gd$email'][0]['address'].split('@')[0];
                    }

                    if(c['gd$phoneNumber']){
                        if(c['gd$phoneNumber'].length > 0){
                            r.phone = c['gd$phoneNumber'][0]['$t'];
                        }
                    }

                    if(c['gd$where'] && c['gd$where'].valueString){
                        r.location = c['gd$where'].valueString
                    }

                    if (c['link']) {
                        var photoLink = c['link'].filter(function(link) {
                            return link.rel == 'http://schemas.google.com/contacts/2008/rel#photo' &&
                                'gd$etag' in link;
                        })[0];
                        if (photoLink) {
                            r.picture = photoLink.href;
                        }
                    }
                    return r;
                })
            }

            if(users && users.length > 0){

                userManagementObj.findUserProfileByIdWithCustomFields(userId, {
                    skypeId: 1,
                    firstName: 1,
                    lastName: 1,
                    emailId: 1,
                    publicProfileUrl: 1,
                    profilePicUrl: 1,
                    companyName: 1,
                    designation: 1,
                    location: 1,
                    mobileNumber: 1
                }, function (error, user) {
                    common.getInvalidEmailListFromDb(function (list) {
                        var newArray = [];
                        if (users.length > 0) {
                            for (var i = 0; i < users.length; i++) {

                                var con = addContacts(userId, users[i]);
                                if (con) {
                                    if (!con.account.name)
                                        con.account = {}
                                    if (users[i].email) {
                                        con.account.name = common.fetchCompanyFromEmail(users[i].email)
                                    }
                                    if (con != null) {
                                        newArray.push(con);
                                    }
                                }
                            }

                            if (newArray.length > 0) {
                                newArray = common.removeDuplicates_field(newArray, 'personEmailId').filter(function (elem) {
                                    var ok = common.isValidEmail(elem.personEmailId, list);
                                    return ok;
                                });
                            }
                        }

                        if (user && common.checkRequired(user._id)) {
                            mapUserContactsWithProfile(user, [], emailIdArr, newArray, callback)
                        }
                    });

                })
            }
        }
    });
}

function addContacts(userId,contact){

    var emailId = contact.email;
    var name = contact.name || contact.nickname || contact.email; // Last modified 14 March 16, Naveen. Added 'contact.email'
    var phone = contact.phone?contact.phone:null;
    var birthday = contact.birthday;
    var companyName = contact.companyName;
    var designation = contact.designation;
    var location = contact.location;

    if(common.checkRequired(userId) && common.checkRequired(name) && common.checkRequired(emailId)){
        if(emailId){
            emailId = emailId.trim().toLowerCase();
        } else {
            emailId = null
        }

        return {
            personId:null,
            personName:name,
            personEmailId:emailId,
            birthday:birthday,
            companyName:companyName,
            designation:designation,
            count:0,
            addedDate:new Date(),
            verified:false,
            relatasUser:false,
            relatasContact:true,
            mobileNumber:phone,
            location:location,
            source:'google-refresh',
            account:{
                name:null,
                selected:false,
                updatedOn:new Date()
            },
            contactImageLink:contact.picture || null
        };
    }
    else return null;
}

function mapUserContactsWithProfile(profile,mobileNumbers,emailIds,contacts,callback){
    var q = {$or:[]};
    var qExist = false;
    if(mobileNumbers.length > 0){
        qExist = true;
        q.$or.push({mobileNumber:{$in:mobileNumbers}});
    }
    if(emailIds.length > 0){
        qExist = true;
        q.$or.push({emailId:{$in:emailIds}})
    }
    if(qExist){
        userManagementObj.selectUserProfilesCustomQuery(q,{skypeId:1,firstName:1,lastName:1,emailId:1,publicProfileUrl:1,profilePicUrl:1,companyName:1,designation:1,location:1,mobileNumber:1},function(err,users){
            if(err){
                logger.info('Error in mapUserContactsWithProfile():aggregateSupport ',err);
            }
            if(users != null && users != undefined && users.length > 0){
                users.forEach(function(user){
                    for(var i=0; i<contacts.length; i++){
                        if(contacts[i].personEmailId == user.emailId){
                            contacts[i].personId = user._id
                            contacts[i].personName = user.firstName+' '+user.lastName
                            contacts[i].personEmailId = user.emailId
                            contacts[i].companyName = user.companyName
                            contacts[i].designation = user.designation
                            contacts[i].location = user.location
                            contacts[i].verified = true
                            contacts[i].relatasUser = true
                            contacts[i].relatasContact = true
                            contacts[i].mobileNumber = user.mobileNumber
                            contacts[i].skypeId = user.skypeId || ''
                        }
                    }
                })
            }

            contacts.forEach(function(a){
                if(a.mobileNumber && a.mobileNumber.match(new RegExp(/[^0-9]/g))) {
                    a.mobileNumber = a.mobileNumber.replace(new RegExp(/[^0-9]/g), ''); //remove special character from mobile number
                }
            })

            contactObj.addContactNotExistRefresh(common.castToObjectId(profile._id.toString()),profile.emailId,contacts,mobileNumbers,emailIds,'mobile',function(err,res){
                if(callback) callback(err,res)
            });
            
            contactManagementObj.insertContacts(common.castToObjectId(profile._id.toString()),profile.emailId,emailIds,null,profile.lastLoginDate,function (cErr,contactsList) {

            });
        })
    }
    else callback(true)
}

function storeProfileImg(url,access_token, imageName) {

    if (url != 'images/default.png' && url != '/images/default.png') {
        request.get({
            url: url,
            headers: {
                'Authorization': 'Bearer ' + access_token,
                'GData-Version': '3.0'
            },
            encoding: 'binary'
        }, function (err, response, body) {
            if(!err && body != null && body != undefined && !common.contains(body,'html') && !common.contains(body,'title')){
                fs.writeFile('./public/profileImages/' + imageName, body, 'binary', function (err) {
                    if (err){
                        logger.info('Error in getting image from third party server ',err);
                    }
                    else{
                        logger.info('Saving image in server success (image from thirdParty servers)');
                    }
                });
            }
            else{
                logger.info('Error in getting image from third party emailRegistration: router ', err);
            }
        });
    }
}

router.get('/check/location/ip',common.isLoggedInUserToGoNext,function(req, res){

    res.send({
        ip:common.getClientIp2(req)
    })
});

router.get('/current/reporting/manager',common.isLoggedInUserToGoNext, function(req, res){

    var userId = req.query.id;
    if(userId){
        userManagementObj.getUserBasicProfile(common.castToObjectId(userId),function (err,profile) {
            if(!err && profile){
                res.send(profile)
            } else {
                res.send(null)
            }
        });
    } else {
        res.send(null)
    }
});

function extractEmails (text) {
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

function parseEmailHeader(id,headers, threadId,invalidEmailList,data){
    var email = {emailContentId:id, emailThreadId:threadId};
    headers = data.payload.headers;
    for(var i=0; i<headers.length; i++){

        if(headers[i].name == 'From' || headers[i].name == 'from'|| headers[i].name == 'FROM'){
            email.from = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'To' || headers[i].name == 'to' || headers[i].name == 'TO'){
            email.To = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Subject' || headers[i].name == 'subject'){
            email.subject = headers[i].value;
        }
        if(headers[i].name == 'Cc' || headers[i].name == 'cc' || headers[i].name == 'CC'){
            email.Cc = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Bcc' || headers[i].name == 'bcc' || headers[i].name == 'BCC'){
            email.Bcc = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Date' || headers[i].name == 'date'){
            email.date = new Date(headers[i].value);
        }
        if(headers[i].name == 'Message-Id' || headers[i].name == 'Message-ID'){
            email.id = headers[i].value;
        }
    }

    return email
}

function setCache(key,data){
    //Storing for 30 min
    redisClient.setex(key, 1800, JSON.stringify(data));
}

function getCache(key,callback){
    redisClient.get(key, function (error, result) {
        try {
            var data = JSON.parse(result);
            // callback(data)
            callback(null)
        } catch (err) {
            callback(null)
        }
    });
}

module.exports = router;