var express = require('express');
var router = express.Router();

var EmailSender = require('../../public/javascripts/emailSender');
var EnterpriseRequests = require('../../dataAccess/enterpriseSignUpRequestClass');
var CommonUtility = require('../../common/commonUtility');
var UserManagement = require('../../dataAccess/userManagementDataAccess');

var enterpriseRequestsObj = new EnterpriseRequests();
var emailSenderObj = new EmailSender();
var common = new CommonUtility();
var userManagementObj = new UserManagement();

router.post("/leads/download/ebook",function (req,res) {

    try {

        var body = JSON.parse(JSON.stringify(req.body))
        body.createdDate = new Date();

        enterpriseRequestsObj.saveWebsiteLead([body],function (err,result) {
            emailSenderObj.ebookLinkNotifyAdmins(body);
            res.send({result:result})
        });

    } catch (e) {
        res.send("Fail")
    }
});

router.post("/leads/more/details",function (req,res) {

    try {

        var body = JSON.parse(JSON.stringify(req.body))
        body.createdDate = new Date();

        enterpriseRequestsObj.saveWebsiteLead([body],function (err,result) {
            emailSenderObj.sendEBookLink(body);
            res.send({result:result})
        });

    } catch (e) {
        res.send("Fail")
    }
});

router.get("/leads/download/ebook/test",function (req,res) {

    try {

        var body = {
            emailId: "naveenpaul.markunda@gmail.com"
        }

        body.createdDate = new Date();

        enterpriseRequestsObj.saveWebsiteLead([body],function (err,result) {
            emailSenderObj.sendEBookLink(body);
            emailSenderObj.ebookLinkNotifyAdmins(body);
            res.send({result:result})
        });

    } catch (e) {
        res.send("Fail")
    }
});

router.get("/admin/leads/dashboard",function (req,res) {
    common.isRelatasAdmin(req,res,function (isAdmin) {
        if(isAdmin){
            res.render("adminRelatas/leadDashboard")
        } else {
            res.send("error")
        }
    })
});

router.get("/admin/leads/get/all",function (req,res) {
    common.isRelatasAdmin(req,res,function (isAdmin) {
        if(isAdmin){
            enterpriseRequestsObj.getWebsiteLead(function (err,leads) {
                res.send(leads)
            });
        } else {
            res.send("error")
        }
    })
});

module.exports = router;
