
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var _ = require("lodash");

var commonUtility = require('../../common/commonUtility');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var contactClass = require('../../dataAccess/contactsManagementClass');
var rabbitmq = require('../../common/rabbitmq');
var recommendationManagementClass = require('../../dataAccess/recommendationManagementClass');
var recommendationCollection = require('../../databaseSchema/recommendationSchema').rrsProducts;

var recommendationManagementClassObj = new recommendationManagementClass();
var common = new commonUtility();
var rabbitmqObj = new rabbitmq();

router.get('/recommendations/getAccountOppsInsights', common.isLoggedInUserOrMobile, function (req, res) {

    var userProfile = common.getUserFromSession(req);
    var emailId = userProfile.emailId;

    recommendationManagementClassObj.getRecommendations(emailId,function(err, recommendations){
        if(err) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
            })
        }
        else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: recommendations
            })
        }
    });
});

router.get('/recommendations/getAccountInteractionInsights',common.isLoggedInUserOrMobile,function(req,res){
    console.log("calling recommendationManagementClassObj.getAllMLAccountInsights");
    recommendationManagementClassObj.getRecommendations(function(err, mlInsights){

        if(err) {
            console.log()
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
            })
        }
        else {
            console.log("calling recommendationManagementClassObj.getAllMLAccountInsights returned ", mlInsights);

            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: mlInsights
            })
        }
    });
});

router.get('/download/recommendations/account/opportunities', common.isLoggedInUserToGoNext, function (req, res) {

    var userProfile = common.getUserFromSession(req);

    if(userProfile && (userProfile.emailId == "sachin@relatas.com" || userProfile.emailId == "sudip@relatas.com" || userProfile.emailId == "sureshhoel@gmail.com")){

        var fileName = 'recommendations_'+moment().toString()+'.xlsx'

        recommendationManagementClassObj.getAllMLAccountInsights(function(err, recommendations){

            var recommendationsList = [];

            _.each(recommendations, function(el) {

                recommendationsList.push({
                    "Company Name":el.companyName,
                    "Account Domain Name":el.accountDomainName,
                    "User EmailId":el.rlUserEmailId,
                    "Account Interactions Count":el.interactionsCount,
                    "Opps Won Count":el.oppsWonCount,
                    "Opps Lost Count":el.oppsLostCount,
                    "Opps PipeLine Count":el.oppsPipeLineCount,
                    "Opps Total Count":el.oppsTotalCount,
                    "Opps Min Amount":el.minAmount,
                    "Opps Max Amount":el.maxAmount,
                    "Recommended Product for Account":el.recommendedProduct,
                    "Recommended Amount":el.recommendedAmount,
                    "Recommended User":el.recommendedUser
                });
            })
            res.xls(fileName, recommendationsList);
        });
    } else {
        res.send(false)
    }

});

router.get('/download/recommendations/account/interactions', common.isLoggedInUserToGoNext, function (req, res) {

    var userProfile = common.getUserFromSession(req);

    if(userProfile && (userProfile.emailId == "sachin@relatas.com" || userProfile.emailId == "sudip@relatas.com" || userProfile.emailId == "sureshhoel@gmail.com")){

        var fileName = 'recommendations_'+moment().toString()+'.xlsx'

        recommendationManagementClassObj.getRecommendations(function(err, recommendations){

            var recommendationsList = [];

            _.each(recommendations, function(el) {

                recommendationsList.push({
                    "Company Name":el.companyName,
                    "Account Domain Name":el.accountDomainName,
                    "User EmailId":el.rlUserEmailId,
                    "Account Interactions Count":el.interactionsCount,
                    "Opps Won Count":el.oppsWonCount,
                    "Opps Lost Count":el.oppsLostCount,
                    "Opps PipeLine Count":el.oppsPipeLineCount,
                    "Opps Total Count":el.oppsTotalCount,
                    "Opps Min Amount":el.minAmount,
                    "Opps Max Amount":el.maxAmount,
                    "Recommended Product for Account":el.recommendedProduct,
                    "Recommended Amount":el.recommendedAmount,
                    "Recommended User":el.recommendedUser
                });
            })
            res.xls(fileName, recommendationsList);
        });
    } else {
        res.send(false)
    }

});

router.get('/recommendations/all', common.isLoggedInUserOrMobile, function (req, res) {

    var userProfile = common.getUserFromSession(req);

    if(userProfile && (userProfile.emailId == "sachin@relatas.com" || userProfile.emailId == "sudip@relatas.com" || userProfile.emailId == "sureshhoel@gmail.com")){
        res.render('recommendations/index');
    } else {
        res.send(false)
    }
});

router.post('/recommendation/ignore', common.isLoggedInUserOrMobile, function (req, res){
    recommendationManagementClassObj.ignoreRecommendation(req.body.recommendedUser,req.body._id,function (err,result) {
        res.send(result);
    })
});

router.post('/recommendation/convert/to/opp', common.isLoggedInUserOrMobile, function (req, res){
    recommendationManagementClassObj.convertRecommendationToOpp(req.body.recommendedUser,req.body._id,function (err,result) {
        res.send(result);
    })
});

module.exports = router;