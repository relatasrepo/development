
var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var errorClass = require('../../errors/errorClass');
var validations = require('../../public/javascripts/validation');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var errorMessages = require('../../errors/errorMessage');
var interactions = require('../../dataAccess/interactionManagement');
var winstonLog = require('../../common/winstonLog');
var taskSupportClass = require('../../common/taskSupportClass');
var taskManagementClass = require('../../dataAccess/taskManagementClass');

var taskManagementClassObj = new taskManagementClass();
var taskSupportClassObj = new taskSupportClass();
var errorObj = new errorClass();
var common = new commonUtility();
var interactionObj = new interactions();
var validation = new validations();
var userManagementObj = new userManagement();
var errorMessagesObj = new errorMessages();
var logLib = new winstonLog();
var _ = require("lodash");
var logger = logLib.getWinston();
var statusCodes = errorMessagesObj.getStatusCodes();

router.get('/tasks/all',function(req,res){
    var userId = common.getUserId(req.user);

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render('tasks/index');
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/tasks/all/for/today',function(req,res){
    var userId = common.getUserId(req.user);

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render('tasks/index');
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/tasks/menu',function(req,res){
    res.render('tasks/menu');
});

router.get('/task/create/template',function(req,res){
    res.render('common/task');
});

router.get('/tasks/createNew',function(req,res){
    res.render('tasks/createNew');
});

router.get('/tasks/list',function(req,res){
    res.render('tasks/list');
});

router.get('/tasks/discussion',function(req,res){
    res.render('tasks/discussion');
});

router.get('/tasks/get/all', common.isLoggedInUserOrMobile,function (req,res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    var user = common.getUserFromSession(req);
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 25;
    var taskStatus = ["Not started","In progress","Completed"];

    var emailId = req.query.emailId?req.query.emailId:null;

    userManagementObj.getUserHierarchy(common.castToObjectId(userId), function(err, data){
        var userEmailIds = _.pluck(data,"emailId");

        if(emailId){
            userEmailIds = [emailId]
        }

        var filter = req.query.filter?req.query.filter:null
        var start = req.query.start?req.query.start:null
        var end = req.query.end?req.query.end:null

        taskManagementClassObj.getAllTasksForUserList(userEmailIds,emailId,filter,start,end,function (err,tasks) {
            var total = tasks.length;
            // var results = tasks.splice(skip, limit);

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": {
                    total: total,
                    skipped: skip,
                    limit: limit,
                    // returned: results.length,
                    // tasks: results,
                    tasks: tasks,
                    taskStatus:taskStatus
                }
            });
        })
    })

})

router.get('/tasks/all/:idType/:id/all',common.isLoggedInUserToGoNext,function(req,res){
    var id = req.params.id;
    var idType = req.params.idType;
    var userId = common.getUserId(req.user);
    if(common.checkRequired(id) && common.checkRequired(idType) && (idType == 'userId' || idType == 'emailId')){
        var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
        var limit = common.checkRequired(req.query.limit) ? parseInt(req.query.limit) : 20;

        taskManagementClassObj.getAllTasksBetweenUsers(userId,id,idType == 'emailId',function(tasks){
            var total = tasks.length;
            var results = tasks.splice(skip, limit);

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": {
                    total: total,
                    skipped: skip,
                    limit: limit,
                    returned: results.length,
                    contacts: results
                }
            });
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}));
});

router.post('/task/create/new/v2', common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagementObj.getUserBasicProfile(common.castToObjectId(userId),function (err,user) {
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){
            if (common.checkRequired(user)) {
                var task = req.body;
                task.createdBy = userId;
                task.createdByEmailId = user.emailId;
                taskSupportClassObj.createNewTask_v2(task,user,false,function(err,response){
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": response
                    });
                })
            } else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("TASK_CREATE_FAILED")));

        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("TASK_CREATE_FAILED")));
    })
});

router.post('/task/update/properties',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    var user = common.getUserFromSession(req);
        if(common.checkRequired(req.body) && validation.isEmptyObject(req.body)){

            if (common.checkRequired(userId)) {
                var task = req.body;
                taskManagementClassObj.updateTask(userId,task, function ( isSuccess) {
                    if(isSuccess){
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": {}
                        });
                    } else {
                        res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}))
                    }
                })
            } else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("TASK_CREATE_FAILED")));

        }else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'NO_REQUEST_BODY'},errorMessagesObj.getMessage("TASK_CREATE_FAILED")));
});

router.post('/task/update/status/web',common.isLoggedInUserOrMobile, function (req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);

        if (common.checkRequired(req.body) && validation.isEmptyObject(req.body)) {
            var data = req.body;
            if (common.checkRequired(data.taskId) && common.checkRequired(data.status)) {
                if (data.status == 'notStarted' || data.status == 'inProgress' || data.status == 'complete'){
                    taskManagementClassObj.updateTaskStatus(userId,data, function ( isSuccess) {
                        if (isSuccess) {
                            interactionObj.updateInteractionOpenedTaskBulk(data.taskId,true,data.lUseEmailId,data.cEmailId,data.status == 'complete',function(){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": data.status == 'complete' ? errorMessagesObj.getMessage("TASK_MARKED_COMPLETED") : errorMessagesObj.getMessage("TASK_MARKED_OPEN"),
                                    "ErrorCode": 0,
                                    "Data": {}
                                });
                            });
                        } else res.send(errorObj.generateErrorResponse({
                            status: statusCodes.UPDATE_FAILED,
                            key: 'INVALID_DATA_RECEIVED'
                        },errorMessagesObj.getMessage("TASK_STATUS_UPDATE_FAILED")));
                    })
                } else res.send(errorObj.generateErrorResponse({
                    status: statusCodes.INVALID_REQUEST_CODE,
                    key: 'INVALID_STATUS'
                },errorMessagesObj.getMessage("TASK_STATUS_UPDATE_FAILED")));
            } else res.send(errorObj.generateErrorResponse({
                status: statusCodes.INVALID_REQUEST_CODE,
                key: 'TASK_ID_OR_STATUS_NOT_FOUND'
            },errorMessagesObj.getMessage("TASK_STATUS_UPDATE_FAILED")))
        } else res.send(errorObj.generateErrorResponse({
            status: statusCodes.INVALID_REQUEST_CODE,
            key: 'NO_REQUEST_BODY'
        },errorMessagesObj.getMessage("TASK_STATUS_UPDATE_FAILED")));
});

router.get('/tasks/future/:idType/:id',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var id = req.params.id;
    var idType = req.params.idType;

    if(common.checkRequired(id) && common.checkRequired(idType) && (idType == 'userId' || idType == 'emailId')){
        userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user) {
            if (common.checkRequired(user)) {
                var timezone;
                if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                    timezone = user.timezone.name;
                } else timezone = 'UTC';
                var date = moment().tz(timezone)
                date.hour(0)
                date.minute(0)
                date.second(0)
                date.millisecond(0)
                date = date.format()

                taskManagementClassObj.getTasksForDateAllGreater(userId,id,idType == 'emailId',date,function(error,tasks){
                    if(error){
                        logger.info('Error in getTasksForDateAllGreater() tasks js web routes ',error)
                    }
                    else if(common.checkRequired(tasks) && tasks.length > 0){
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": tasks
                        });
                    }
                    else res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": []
                    });
                })
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}));
});

router.get('/tasks/with/:userId/latest/timeline',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var id = req.params.id;
    var idType = req.params.idType;
    if(common.checkRequired(id) && common.checkRequired(idType) && (idType == 'userId' || idType == 'emailId')){
        taskManagementClassObj.getAllTasksBetweenUsersLatestLimit(userId,id,idType == 'emailId',5,function(tasks){
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": tasks
            });
        })
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_URL_PARAMETER'}));
});

router.get('/tasks/by/refId',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    var refId = req.query.refId;

    taskManagementClassObj.getTaskByRefId(refId,function(err,tasks){
        res.send({
            "SuccessCode": 1,
            "Message": "",
            "ErrorCode": 0,
            "Data": tasks
        });
    })
});

module.exports = router;