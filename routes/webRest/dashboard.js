var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactions = require('../../dataAccess/interactionManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var meetingClass = require('../../dataAccess/meetingManagement');
var contactClassSupport = require('../../common/contactsSupport');
var validations = require('../../public/javascripts/validation');

var meetingsObj = new meetingClass();
var validation = new validations();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionObj = new interactions();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var contactSupportObj = new contactClassSupport();

var statusCodes = errorMessagesObj.getStatusCodes();

// Router to get dashboard page
router.get('/dashboard',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    res.render('secondSeries/dashboard',{isLoggedIn : common.isLoggedInUserBoolean(req)});
});

router.get('/latest/interacted/contact',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    var userId = common.getUserId(req.user);
    interactionObj.checkInteractedPastDay(common.castToObjectId(userId),function (result) {
        res.send(result)
    })
});

router.get('/check/pipeline/values',common.isLoggedInUser,common.checkUserDomain,function(req,res){
    var userId = common.getUserId(req.user);
    contactObj.checkPipelineValuesExists(common.castToObjectId(userId),function (result) {
        res.send(result)
    })
});

router.get('/dashboard/status/counts/top/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(common.checkRequired(user)){
            var timezone;
            if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                timezone = user.timezone.name;
            } else timezone = 'UTC';

            var dateMin = moment().tz(timezone);
            dateMin = dateMin.format();
            user._id = common.castToObjectId(userId);
            var data = {};
            getLoosingTouchCount(userId,user,timezone,function(losingTouchCount){
                data.losingTouchCount = losingTouchCount;
                meetingsObj.userNewMeetingsByDate(userId,dateMin,user.serviceLogin,function(meetings){
                    if(common.checkRequired(meetings) && meetings.length > 0) {
                        data.unpreparedMeetingsCount = meetings.length;
                    }
                    else{
                        data.unpreparedMeetingsCount = 0;
                    }

                    meetingsObj.upcomingMeetings(common.castToObjectId(userId),dateMin,function(upcomingMeetingsCount){
                        data.upcomingMeetings = upcomingMeetingsCount ? upcomingMeetingsCount.length : 0;
                       common.interactionDataFor(function(days){
                           var end = moment().tz(timezone);

                           end.hours(23);
                           end.minutes(59);
                           end.seconds(59);
                           end.milliseconds(0);

                           var start = end.clone();
                           start.date(start.date() - days);
                           start.hours(0);
                           start.minutes(0);
                           start.seconds(0);
                           start.milliseconds(0);

                           start = start.format();
                           end = end.format();

                           interactionObj.pastSevenDayInteractionStatus(common.castToObjectId(userId),user.emailId,{start:start,end:end},false,function(obj){
                               if(common.checkRequired(obj) && (obj.youNotReplied.length > 0 || obj.othersNotReplied.length > 0)){
                                   data.emailResponsesPending = obj.youNotReplied.length;
                                   data.emailAwaitingResponses = obj.othersNotReplied.length;
                               }
                               else{
                                   data.emailResponsesPending = 0;
                                   data.emailAwaitingResponses = 0;
                               }
                               res.send({
                                   "SuccessCode": 1,
                                   "Message": "",
                                   "ErrorCode": 0,
                                   "Data":data
                               });
                           })
                       });
                    })
                })
            });
        }
        else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });
});

function getLoosingTouchCount(userId,user,timezone,callback){
    contactSupportObj.getContactsLosingTouch(common.castToObjectId(userId),user,timezone,'count',0,20,true,function(response){
        callback(response.Data.count)
    })
}

router.get('/interactions/topfive/web',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(userId)){
        common.getProfileOrStoreProfileInSession(userId,req,function(user){
            if(common.checkRequired(user)){
                common.interactionDataFor(function(days){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';

                    var date = moment().tz(timezone);
                    var start = date.clone();
                    var end = date.clone();
                    start.date(start.date() - days);
                    interactionObj.interactionIndividualTopFive_new_dashboard(common.castToObjectId(userId),user.emailId,user.mobileNumber,start.format(),end.format(),function(interactions){
                        res.send({
                            "SuccessCode": interactions.length > 0 ? 1 : 0,
                            "Message": interactions.length > 0 ? "" : errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_TOP_FIVE"),
                            "ErrorCode": interactions.length > 0 ? 0 : 1,
                            "Data":interactions
                        });
                    })
                });
            }else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'},errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_TOP_FIVE")));
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'},errorMessagesObj.getMessage("NO_CONNECTIONS_FOUND_TOP_FIVE")));
});

router.get('/dashboard/status/past/web',common.isLoggedInUser,function(req,res){
    var userId = common.getUserId(req.user);
    userManagementObj.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(err,user){
        if(common.checkRequired(user)) {
            common.interactionDataFor(function(days){
                var timezone;
                if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                    timezone = user.timezone.name;
                } else timezone = 'UTC';

                var dateMin = moment().tz(timezone);
                dateMin.date(dateMin.date() - days)
                dateMin.hour(0)
                dateMin.minute(0)
                dateMin.second(0)
                dateMin.millisecond(0)

                dateMin = dateMin.format();

                interactionObj.interactionCountAndCompanyCount(userId,user.emailId,dateMin,days,function(dbResponse){
                    var data;
                    if(dbResponse.length > 0){
                        data = dbResponse[0]
                        getLoosingTouchCount(user,data,timezone,dateMin,req,res)
                    }
                    else{
                        data = { interactionsCount: 0, companiesCount: 0};
                        getLoosingTouchCount(user,data,timezone,dateMin,req,res)
                    }
                })
            })
        }
    });

    function getLoosingTouchCount(user,data,timezone,dateMin,req,res){

        contactObj.getAllFavoriteContactsEmailIds(user._id,user.emailId,function(emailIdArr,mobileNumberArr){
            if(emailIdArr.length > 0 || mobileNumberArr.length > 0){
                interactionObj.losingTouchByDateORDays(userId,emailIdArr,dateMin,mobileNumberArr,function(losingTouch){
                    if(common.checkRequired(losingTouch) && losingTouch.length > 0){
                        losingTouch = common.removeDuplicates_emailId(losingTouch);
                        data.losingTouchCount = losingTouch.length;
                        res.send(data)
                    }
                    else{
                        data.losingTouchCount = 0;
                        res.send(data)
                    }
                })
            }
            else{
                data.losingTouchCount = 0;
                res.send(data)
            }
        });
    }
});

router.get('/dashboard/past/days/interacted/info/type',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    common.getProfileOrStoreProfileInSession(userId,req,function(user) {
        if (common.checkRequired(user)) {
            common.interactionDataFor(function(days){
                var timezone;
                if (common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)) {
                    timezone = user.timezone.name;
                } else timezone = 'UTC';

                var date = moment().tz(timezone);
                var start = date.clone();

                start.date(start.date() - days);
                interactionObj.getInteractionsCountByCompanyLocationTypeDate(common.castToObjectId(userId),start.format(),date.format(),[],[],function(resultObj){
                    resultObj.interactionsByType = common.formatInteractionTypeGraphData(resultObj.interactionsByType)
                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data":resultObj
                    });
                });
            })
        }
    })
 });

router.get('/dashboard/past/days/interacted/info',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
        common.getProfileOrStoreProfileInSession(userId,req,function(user){
            if(common.checkRequired(user)){
                common.interactionDataFor(function(days){
                    var timezone;
                    if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                        timezone = user.timezone.name;
                    }else timezone = 'UTC';

                    var date = moment().tz(timezone);
                    var start = date.clone();

                    start.date(start.date() - days);

                    var data = {};
                    interactionObj.getCompaniesUserInteractedWithDateAndGetEmailIdsInteracted_dashboard(common.castToObjectId(userId),false,user.emailId,start.format(),user.mobileNumber,function(dataObj){

                        if(common.checkRequired(dataObj)){
                            data.companies = dataObj.interactionsWithCompany_Count;
                            if(dataObj.interactedWithEmails.length > 0 || dataObj.interactedWithMobileNumber.length > 0){
                                contactObj.getContactsByTypeCount(common.castToObjectId(userId),dataObj.interactedWithEmails,dataObj.interactedWithMobileNumber,function(error,dataTpes){
                                    if(common.checkRequired(dataTpes) && dataTpes.length > 0){
                                        data.types = dataTpes;
                                    }
                                    contactObj.getContactsFavoriteCount(common.castToObjectId(userId),dataObj.interactedWithEmails,dataObj.interactedWithMobileNumber,function(error,count){
                                        data.favorite = count || 0;
                                        res.send({
                                            "SuccessCode": 1,
                                            "Message": "",
                                            "ErrorCode": 0,
                                            "Data":data
                                        });
                                    })
                                })
                            }
                            else res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":data
                            });
                        }
                        else{
                            res.send({
                                "SuccessCode": 1,
                                "Message": "",
                                "ErrorCode": 0,
                                "Data":data
                            });
                        }
                    })
                })
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });
});

router.get('/dashboard/top/social/media/today',common.isLoggedInUserToGoNext,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(userId)){
        common.getProfileOrStoreProfileInSession(userId,req,function(user){
            if(common.checkRequired(user)){
                var timezone;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    timezone = user.timezone.name;
                }else timezone = 'UTC';

                var date = moment().tz(timezone);
                var dateMin = date.clone();
                var dateMax = date.clone();
              //  dateMin.date(dateMin.date() - 90);
                dateMin.hours(0);
                dateMin.minutes(0);
                dateMin.seconds(0);
                dateMax.hours(23);
                dateMax.minutes(59);
                dateMax.seconds(59);
                userId = common.castToObjectId(userId);
                contactObj.getAllFavoriteContactsEmailIds(userId,user.emailId,function(emailIdArr){

                    if(emailIdArr.length > 0){
                        interactionObj.topTenSocialMediaToday(userId,user.emailId,emailIdArr,dateMin,dateMax,function(interactions){
                            var exist = common.checkRequired(interactions) && interactions.values.length
                            res.send({
                                "SuccessCode": exist ? 1 : 0,
                                "Message": exist > 0 ? "" : errorMessagesObj.getMessage('NO_SOCIAL_MEDIA_ACTIVITY_TODAY'),
                                "ErrorCode": exist > 0 ? 0 : 1,
                                "noFavorites":false,
                                "Data":interactions
                            });
                        })
                    }
                    else{
                        res.send({
                            "SuccessCode": 0,
                            "Message": errorMessagesObj.getMessage('NO_FAVORITE_CONTACTS'),
                            "ErrorCode": 1,
                            "noFavorites":true,
                            "Data":[]
                        });
                    }
                });
            }
            else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
        });
    }
    else res.send(errorObj.generateErrorResponse({status:statusCodes.INVALID_REQUEST_CODE,key:'INVALID_TOKEN'}));
});

router.get('/dashboard/interactions/globalmap',common.isLoggedInUserToGoNext,function(req,res){
   var userId = common.getUserId(req.user);
    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        if(common.checkRequired(user)){
            var timezone;
            if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                timezone = user.timezone.name;
            }else timezone = 'UTC';

            var date = moment().tz(timezone);
            var dateMin = date.clone();
            var dateMax = date.clone();
            dateMin.date(dateMin.date() - 7);
            dateMin.hours(0);
            dateMin.minutes(0);
            dateMin.seconds(0);
            /*dateMax.hours(23);
            dateMax.minutes(59);
            dateMax.seconds(59);*/
            userId = common.castToObjectId(userId);
            interactionObj.totalInteractionsIndividual(userId,dateMin.format(),dateMax.format(),function(interactions){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":interactions
                });
            })
        }
        else res.send(errorObj.generateErrorResponse({status:statusCodes.PROFILE_ERROR_CODE,key:'ERROR_FETCHING_PROFILE'}));
    });

});

//router.post('/addContact/notExists',common.isLoggedInUserToGoNext,function(req,res){
//    var userId = common.getUserId(req.user);
//    console.log("Req");
//    console.log(req.body.emailId)
//    //console.log(JSON.stringify(req));
//
//    if(common.checkRequired(req.body.emailId)){
//        myUserCollection
//
//
//    }
//
//});

module.exports = router;