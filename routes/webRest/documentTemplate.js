//**************************************************************************************
// File Name            : documentTemplate
// Functionality        : Router for documentTemplates(Quotation/Purchase Order/Invoice)
// History              : First Version
//
//
//**************************************************************************************

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var _ = require("lodash");
var fs = require('fs');

// var pdfcrowd = require("pdfcrowd");

// create the API client instance
// var client = new pdfcrowd.HtmlToPdfClient("npmarkunda", "33c74eb5cadb0f02e394df9696fa0d19");

var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var commonUtility = require('../../common/commonUtility');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var contactClass = require('../../dataAccess/contactsManagementClass');
var rabbitmq = require('../../common/rabbitmq');
var documentTemplatesManagementClass = require('../../dataAccess/documentTemplateManagement');
var documentTemplatesCollection = require('../../databaseSchema/documentTemplateSchema').documentTemplates;

var documentTemplatesManagementClassObj = new documentTemplatesManagementClass();
var common = new commonUtility();
var rabbitmqObj = new rabbitmq();
var company = new companyClass();
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var statusCodes = errorMessagesObj.getStatusCodes();

router.get('/documentTemplates/index',common.isLoggedInUserToGoNext, function(req, res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('documents/corporateAdmin/documentTemplateIndex.html');                        
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/delete/documents',common.isLoggedInUserToGoNext, function(req, res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('documents/corporateAdmin/corporateDeleteDocument.html');                        
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/delete/document/templates',common.isLoggedInUserToGoNext, function(req, res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('documents/corporateAdmin/corporateDeleteDocumentTemplates.html');                        
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/documentTemplates/show/all',common.isLoggedInUserToGoNext, function(req, res){
    res.render('documents/corporateAdmin/showAllDocTemplates');
});

router.get('/documentTemplates/create/new',common.isLoggedInUserToGoNext, function(req, res){
    res.render('documents/corporateAdmin/createNewDocTemplate');
});

router.get('/documentTemplates/get/all/data', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    documentTemplatesManagementClassObj.getAllDocumentTemplates(companyId, function(err, documentTemplates){
        if(err) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
        else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: documentTemplates

            })
        }
    });
});

router.get('/download/accounts/upload/template', common.isLoggedInUserOrMobile, function (req, res) {

    var templateHeaders = [];

    templateHeaders.push({
        "name":'',
        "domain": '',
        "branch": '',
        "address": '',
        "phone": ''
    });

    res.xls("accounts_upload_template.xlsx", templateHeaders);
});


router.post('/documentTemplates/getById', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;

    documentTemplatesManagementClassObj.getAllDocumentTemplateById(common.castToObjectId(companyId),
                                                            common.castToObjectId(req.body.templateId),
        function(err, documentTemplates){
        if(err) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
        else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: documentTemplates

            })
        }
    });
});

router.post('/document/template/get/by/type/name', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;

    documentTemplatesManagementClassObj.getDocumentTemplateByTypeAndName(common.castToObjectId(companyId),
                            req.body.docTemplateType, req.body.docTemplateName,
                            function(err, document){
                                if(document) {
                                    res.send({
                                        SuccessCode: 1,
                                        ErrorCode: 0,
                                        data: document
                                    })
                                } else if(err) {
                                }
                            });
});

router.post('/document/template/increment/document/count', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;

    documentTemplatesManagementClassObj.incrementDocumentCreatedCount(common.castToObjectId(companyId),
                                                            common.castToObjectId(req.body.templateId),
                                                            req.body.documentCount,
        function(err, count){
        if(err) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
        else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: count
            })
        }
    });
});

/* router.get('/document/convert/to/pdf', function (req, res){

    try {
        client.setElementToConvert("#main-content");
    } catch(why) {
        console.error("Pdfcrowd Error: " + why);
        console.error("Pdfcrowd Error Code: " + why.getCode());
        console.error("Pdfcrowd Error Message: " + why.getMessage());
        process.exit(1);
    }

// run the conversion and write the result to a file
    client.convertUrlToFile(
        "/document/index",
        "html_part.pdf",
        function(err, fileName) {
            if (err) return console.error("Pdfcrowd Error: " + err);
            console.log("Success: the file was created " + fileName);
            res.send(err);
        });
}); */

router.get('/documentTemplates/getDataTypes', common.isLoggedInUserOrMobile, function (req, res) {
    var dataTypes = ['Boolean',
                        'Customized',
                        'Date',
                        'Formula',
                        'Numeric',
                        'String',
                        'System Generated',
                        'System Referenced',
                        ]
    res.send({
        SuccessCode: 1,
        ErrorCode: 0,
        DataTypes: dataTypes
    })
});

router.get('/documentTemplates/getDocumentTemplateTypes', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    documentTemplatesManagementClassObj.getAllDocumentTemplateTypes(common.castToObjectId(companyId),
        function(err,data) {
        res.send({
            SuccessCode: 1,
            ErrorCode: 0,
            DataTypes: data
        });
    });
});


router.post('/documentTemplates/getAllDocumentTemplateNamesByType', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    var templateType = req.body.documentTemplateType;
    documentTemplatesManagementClassObj.getAllDocumentTemplateNamesByType(common.castToObjectId(companyId),
                                                                        templateType,
        function(err,templateTypes) {
        res.send({
            SuccessCode: 1,
            ErrorCode: 0,
            TemplateNames: templateTypes
        });
    });
});

router.post("/documentTemplatesElements/add/new",  function (req, res){
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;

    console.log("Request body:", req.body.isTemplateVersionControlled);

    documentTemplatesManagementClassObj.updateDocumentTemplateElements(companyId,
        req.body.documentTemplateType,
        req.body.documentTemplateName,
        req.body.docTemplateElementList,
        req.body.documentTemplateUiId,
        req.body.isDefault,
        req.body.isTemplateVersionControlled,
        req.body.docTemplateAttrList,
        function(err, result) {
            if(err) {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                })
            } else {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                })
            }
        });
});

router.post("/document/template/create/new",  function (req, res) {
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    req.body.companyId = common.castToObjectId( userProfile.companyId);

    documentTemplatesManagementClassObj.checkAndCreateDocumentTemplate(req.body, function(error, savedTemplate) {
        if(error) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        } else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Template: savedTemplate
            })
        }
    })
});

router.post("/documentTemplates/updateIsDocTemplateDeactivated",  function (req, res){
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;

    documentTemplatesManagementClassObj.updateIsTemplateDeactivated(common.castToObjectId(companyId),
                                                                    req.body.documentTemplateType,
                                                                    common.castToObjectId(req.body.templateId),
                                                                    req.body.isDocTemplateDeactivated,
        function(err, result) {
            if(err) {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                })
            } else {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                })
            }
        });
});


router.post("/documentTemplates/setDocumentTemplateAsDefault",  function (req, res){
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    documentTemplatesManagementClassObj.setDocumentTemplateAsDefault(common.castToObjectId(companyId),
        req.body.documentTemplateType,
        common.castToObjectId(req.body.templateId),
        req.body.isDefault,
        function(err, result) {
            if(err) {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                })
            } else {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                })
            }
        });
});


router.post("/documentTemplates/setDocumentTemplateAsVersionControlled",  function (req, res){
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    var isVersionControlled = req.body.isTemplateVersionControlled;
    documentTemplatesManagementClassObj.setDocumentTemplateAsVersionControlled(common.castToObjectId(companyId),
        req.body.documentTemplateType,
        common.castToObjectId(req.body.templateId),
        isVersionControlled,
        function(err, result) {
            if(err) {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1,
                })
            } else {
                res.send({
                    SuccessCode: 1,
                    VersionControlled:isVersionControlled,
                    ErrorCode: 0,
                })
            }
        });
});

router.post('/profile/update/companyLogoForDocumentTemplate',function(req, res) {
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    var companyName = userProfile.companyName;

    if (common.checkRequired(userId)) {
        var fstream;
        //Check if the image is valid
        if(common.checkRequired(req.busboy)){
            
            
            req.pipe(req.busboy);
            req.busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {

                if(common.contains(mimetype,'image')){
                    documentTemplatesManagementClassObj.getDocumentTemplateByTypeAndName(companyId,"","",function(error, userData){
                        if(error || userData == null){
                            res.send(errorObj.generateErrorResponse({
                                status: statusCodes.PROFILE_ERROR_CODE,
                                key: 'ERROR_FETCHING_PROFILE'
                            },errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_FAILED")));
                        }
                        else{
                            fstream = fs.createWriteStream('./public/docTemplateLogos/' + filename);
                            file.pipe(fstream);
                            fstream.on('close', function () {

                                res.send({
                                    SuccessCode: 1,
                                    ErrorCode: 0
                                })
                            });
                        }
                    });
                }
                else{
                    res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_IMAGE_FILE_TYPE'},errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_FAILED")));
                }
            });
        } 
        else {
            res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'NO_FILES_RECEIVED'},errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_FAILED")));
        } 
    }
    else {
        res.send(errorObj.generateErrorResponse({status: statusCodes.INVALID_REQUEST_CODE, key: 'INVALID_TOKEN'},errorMessagesObj.getMessage("UPDATE_PROFILE_PIC_FAILED")));
    }
});

router.post('/corporate/admin/delete/document/templates',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    var userProfile = common.getUserFromSession(req);
                    if(isAdmin && userProfile && userProfile.emailId){
                        documentTemplatesManagementClassObj.deleteMultipleDocumentTemplates(common.castToObjectId(companyProfile._id),req.body,function (err,documentTemplates) {
                            res.send(documentTemplates);
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

function getCompanyObj(companyName,req){
    var company = {
        url:req.headers.host,
        companyName:companyName
    }
    return company;
}

router.post('/profile/get/companyLogo',function(req, res) {

});

module.exports = router;