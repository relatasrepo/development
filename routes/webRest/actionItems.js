/**
 * Created by naveen on 1/27/16.
 */

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');
var jwt = require('jwt-simple');
var _ = require("lodash");
var gcal = require('google-calendar');
var request = require('request');
var rabbitmq = require('../../common/rabbitmq');
var allUsers = require('../../databaseSchema/userManagementSchema').User;
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactionManagement = require('../../dataAccess/interactionManagement');
var commonUtility = require('../../common/commonUtility');
var insightsManagement = require('../../dataAccess/insightsManagement');
var ActionItemsManagement = require('../../dataAccess/actionItemsManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var twitterApi = require('../../common/twitter');
var appCredentials = require('../../config/relatasConfiguration');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var fullContact = require('../../common/fullContact');
var SocialManagement = require("../../dataAccess/socialManagement")

var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var meetingClassObj = new meetingManagement();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionManagementObj = new interactionManagement();
var actionItemsManagementObj = new ActionItemsManagement();
var twitterApiObj = new twitterApi();
var socialManagement = new SocialManagement();
var differenceBy = require('lodash.differenceby');

var winstonLog = require('../../common/winstonLog');
var logLib = new winstonLog();
var logger =logLib.getInteractionsErrorLogger();

var statusCodes = errorMessagesObj.getStatusCodes();

router.get('/fetch/remind/to/connect', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    var timezone = null;

    allUsers.find({ _id: common.castToObjectId(userId)}, { timezone: 1 }, function(err, user) {

        if (err || user.length == 0) {
            res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
        }
        else {
            if (user[0].timezone && user[0].timezone.name)
                timezone = user[0].timezone.name;
            else
                timezone = 'UTC';
            var dateMin = moment().tz(timezone);
            dateMin.hour(0);
            dateMin.minute(0);
            dateMin.second(0);
            dateMin.millisecond(0);
            var dateMax = moment().tz(timezone);

            dateMax.hour(23);
            dateMax.minute(59);
            dateMax.second(59);
            dateMax.millisecond(0);
            var date = {min:dateMin,max:dateMax};

            actionItemsManagementObj.findRemindToConnectByDate(common.castToObjectId(userId),date,function(actionResult){
                var contactIds = _.pluck(actionResult,"documentRecordId");

                allUsers.aggregate([
                    { $match: { _id: common.castToObjectId(userId) } },
                    { $unwind: "$contacts" },
                    // { $match: { "contacts.remindToConnect": { $lt: new Date(dateMax) } } }, {
                    { $match: { "contacts._id": {$in:contactIds} } }, {
                        $project: {
                            _id:0,
                            ownerId: "$_id",
                            remindToConnect: "$contacts.remindToConnect",
                            contactEmail: "$contacts.personEmailId",
                            company: "$contacts.companyName",
                            designation: "$contacts.designation",
                            value: "$contacts.account.value.inProcess",
                            userId: "$contacts.personId",
                            contactId: "$contacts._id",
                            rel: "$contacts.contactRelation",
                            mobileNumber: "$contacts.mobileNumber",
                            lastInteracted: "$contacts.lastInteracted",
                            personName: "$contacts.personName",
                            contactImageLink: "$contacts.contactImageLink"
                        }
                    }
                ], function(err, remind) {
                    if (err || remind.length == 0) {
                        res.send({
                            SuccessCode: 0,
                            ErrorCode: 1,
                            Data: []
                        })
                    } else {
                        var emailIds = [];
                        remind.forEach(function(ele) {
                            ele.suggestion = 'Your reminder to connect has been activated.\nConnect right now';
                            if (ele.rel && ele.rel.decisionmaker_influencer)
                                ele.relation = ele.rel.decisionmaker_influencer;
                            else
                                ele.relation = null;
                            if (!ele.value)
                                ele.value = 0;

                            if (ele.lastInteracted)
                                ele.lastInteracted = moment(ele.lastInteracted).format("DD MMM YYYY")
                            delete ele.rel;
                            if (ele.contactEmail)
                                emailIds.push(ele.contactEmail);
                            ele.reminder_notify = 'reminder';
                            actionResult.forEach(function(ac){
                                if(String(ac.documentRecordId) == String(ele.contactId)){
                                    ele.actionItemId = ac._id;
                                }
                            })
                        });

                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            Data: remind
                        })
                    }
                });
            });

            // insightsManagementObj.remindToConnect(common.castToObjectId(userId), dateMin, dateMax, timezone, function(err, result) {
            //
            //     if (!err && result) {
            //
            //         req.session.notifications.remindToConnect = result.length;
            //
            //         res.send({
            //             SuccessCode: 1,
            //             ErrorCode: 0,
            //             Data: result
            //         })
            //
            //     } else {
            //         res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
            //     }
            // });
        }
    });
});

router.get('/fetch/slots/and/update', function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var type = req.query.type;

    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,timezone:1,location:1,currentLocation:1,serviceLogin:1,google:1,outlook:1},function(error,user){
        actionItemsManagementObj.findYesterdaysMeetings(user,'store',function(result){
            res.send(result);
        })
    });

});

router.get('/fetch/meetings/by/date', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    var prevDays = req.query.prevDays?req.query.prevDays:1; // Default 24 hours range
    var tillDays = req.query.tillDays?req.query.tillDays:0;

    var dateMin = moment().subtract(prevDays, "days");
    var dateMax = moment().subtract(tillDays, "days");

    dateMin.date(dateMin.date())
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    dateMax.date(dateMax.date())
    dateMax.hour(0)
    dateMax.minute(0)
    dateMax.second(0)
    dateMax.millisecond(0)

    dateMin = dateMin.format()
    dateMax = dateMax.format()

    var projection = {
        recurrenceId:1,invitationId:1,
        scheduleTimeSlots:1,senderId:1,
        senderName:1,senderEmailId:1,
        senderPicUrl:1,isSenderPrepared:1,
        to:1,toList:1,
        suggested:1,suggestedBy:1,
        selfCalendar:1
    };

    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,timezone:1,location:1,currentLocation:1,serviceLogin:1,google:1,outlook:1},function(error,user){
        var findOutlookOrGoogle = user.serviceLogin;
        meetingClassObj.userMeetingsByDate(userId,new Date(dateMin),new Date(dateMax),projection,findOutlookOrGoogle,function(meetings){
            
            if(common.checkRequired(meetings) && meetings.length > 0){
                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data":{
                        meetings:meetings,
                        userId:userId.toString()
                    }
                })
            }else res.send(null)
        })
    });
});

router.get('/fetch/contacts/at/my/location', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);

    //Fetch contact at user's location by date.
    //For eg if the user is going to be in Mumbai on 7 of next week(07.MM.YY),
    //then fetch all the contacts currently at that location with complete address.

    //TODO 1. Fetch LIU traveling location for next 7 days. XXXX. Get the town. Right now matching for anything is difficult. Done.
    //TODO 2. If the contacts are Relatas users, check their profile for location. Pending?
    //TODO 3. For non Relatas user' contacts check in the contacts' list of the LIU. Reject those contacts with whom the LIU has the meetings. Done

    var userIdArr = [];
    userIdArr.push(common.castToObjectId(userId));
    var dateMin = moment().add(1, "days");

    dateMin.date(dateMin.date())
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    var meetingsLocations = [];
    var allLocations = [];
    var participants = [];

    meetingClassObj.userNextWeekTravelingTo(userIdArr, dateMin,function (meetings) {

        if(meetings.length>0){
            _.each(meetings,function (meeting) {
                _.each(meeting.scheduleTimeSlots,function (slot) {
                    meetingsLocations.push({
                        location: slot.location,
                        invitationId: meeting.invitationId,
                        date: slot.start.date
                    });
                    allLocations.push(slot.location);
                });

                _.each(meeting.participants,function (participant) {
                    participants.push(participant.emailId)
                })
            });

            allLocations = _.uniq(_.flatten(allLocations));

            var allLocationsTrimmed = [];
            var counter = 0;
            var len = allLocations.length;
            var verifiedMeetingLocations = [];

            _.each(allLocations,function (loc) {

                common.getLocationGoogleAPI(loc,function (googleLocations) {

                    counter++;

                    if(googleLocations && googleLocations.results[0]) {

                        _.each(googleLocations.results[0].address_components,function (component) {

                            if(component.types.indexOf("locality") > -1 && component.types.indexOf("political") > -1){
                                allLocationsTrimmed.push({loc:component.long_name.trim()})
                                verifiedMeetingLocations.push({
                                    location:component.long_name.trim()
                                })
                            }
                        });

                        if(counter == len){

                            if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                                var timezone = req.session.profile.timezone.name
                            }

                            contactObj.getContactsMatchingLocations(common.castToObjectId(userId),allLocationsTrimmed,participants,function (err,contacts) {

                                res.send({
                                    meetingsLocations:meetingsLocations,
                                    verifiedMeetingLocations:verifiedMeetingLocations,
                                    allLocations:allLocationsTrimmed,
                                    contacts:contacts,
                                    timezone:timezone
                                });

                            });
                        }
                    } else {
                        res.send({
                            meetingsLocations:[],
                            allLocations:[],
                            contacts:[],
                            timezone:null
                        });
                    }
                });
            });

        } else {
            res.send({
                meetingsLocations:[],
                allLocations:[],
                contacts:[],
                timezone:null
            });
        }

    });
});

router.get('/fetch/upcoming/meeting/locations', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);

    var userIdArr = [];
    var dateMin, dateMax;

    userIdArr.push(common.castToObjectId(userId));

    if(req.query.duration == "twoDays") {
        dateMin = moment().add(1, "days");
        dateMax = moment().add(3, "days");

    } else {
        dateMin = moment().add(1, "days");
        dateMax = moment().add(14, "days");
    }

    dateMin.date(dateMin.date())
    dateMin.hour(0)
    dateMin.minute(0)
    dateMin.second(0)
    dateMin.millisecond(0)

    dateMax.date(dateMax.date())
    dateMax.hour(0)
    dateMax.minute(0)
    dateMax.second(0)
    dateMax.millisecond(0)

    var meetingsLocations = [];
    var allLocations = [];

    userManagementObj.selectUserProfileCustomQuery(common.castToObjectId(userId),{google:1,outlook:1,emailId:1},function (err,userData) {

        var outlookOrGoogle = null;
        
        if(userData.google.length>0){
            outlookOrGoogle = 'google'
        } else if(userData.outlook.length>0) {
            outlookOrGoogle = 'outlook'
        }

        meetingClassObj.userNexTwoWeeksTravelingTo(userIdArr, dateMin,dateMax,outlookOrGoogle,function (meetings) {

            if (meetings.length > 0) {
                _.each(meetings, function (meeting) {
                    _.each(meeting.scheduleTimeSlots, function (slot) {

                        var participants = [];

                        _.each(meeting.participants, function (participant) {

                            if(userData.emailId !== participant.emailId){
                                participants.push(participant.emailId)
                            }
                        });

                        if(common.isMeetingLocationInPerson(slot.location)){

                            meetingsLocations.push({
                                location: slot.location,
                                invitationId: meeting.invitationId,
                                date: slot.start.date,
                                participants:participants
                            });

                        }
                    });
                });

                if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                    var timezone = req.session.profile.timezone.name
                }

                res.send({
                    "SuccessCode": 1,
                    "ErrorCode" : 0,
                    "Data":{
                        meetingsLocations:meetingsLocations
                    },
                    timezone:timezone
                })
            } else {
                sendErrorResponse(res)
            }
        });

    });
});

router.get('/fetch/contacts/at', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var participants = req.query.participants.split(',');
    var allLocationsTrimmed = [];
    var city = '';
    var meetingId = req.query.invitationId;

    common.getLocationGoogleAPI(req.query.location,function (googleLocations) {

        if(googleLocations && googleLocations.results[0]) {

            _.each(googleLocations.results[0].address_components,function (component) {

                if(component.types.indexOf("locality") > -1 && component.types.indexOf("political") > -1){
                    allLocationsTrimmed.push({loc:component.long_name.trim()})
                    city = component.long_name.trim();
                }
            });

            if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                var timezone = req.session.profile.timezone.name
            }

            contactObj.getContactsMatchingLocations(common.castToObjectId(userId),allLocationsTrimmed,participants,function (err,contacts) {
                var actionItemsToStore = [];
                var contactArray = contacts[0] ? contacts[0].contacts : [];

                var emailId = null;
                if(contacts.length>0){
                    emailId = contacts[0]._id
                } else if((req.session && req.session.profile && req.session.profile.emailId)) {
                    emailId = req.session.profile.emailId;
                }

                contactArray.forEach(function(a){
                    if(a.personEmailId != emailId){

                        var obj = {
                            meetingId:meetingId,
                            userId: common.castToObjectId(userId.toString()),
                            userEmailId: emailId,
                            itemCreatedDate:new Date(),
                            actionTakenSource:null,
                            actionTaken: false,
                            actionTakenDate: null,
                            documentRecordFrom: 'userCollection',
                            itemType: 'travellingToLocation',
                            documentRecordId:a._id?common.castToObjectId(a._id.toString()):null,
                            personEmailId:a.personEmailId
                        }
                        actionItemsToStore.push(obj);
                    }
                });

                actionItemsManagementObj.findAndInsertNonExistingItemsByMeetingId(common.castToObjectId(userId),meetingId,actionItemsToStore,'travellingToLocation',function(result){
                    actionItemsManagementObj.findActionItemsByMeetingId(common.castToObjectId(userId),meetingId,'travellingToLocation',function(existingItem){
                        var newResult = {_id:null, contacts:[]}
                        if(contacts[0] && contacts[0].contacts) {
                            contacts[0].contacts.forEach(function (a) {
                                existingItem.forEach(function (b) {
                                    if (String(a._id) == String(b.documentRecordId)) {
                                        a.actionItemId = b._id;
                                        newResult.contacts.push(a);
                                    }
                                })
                            })
                        }

                        res.send({
                            "SuccessCode": 1,
                            "ErrorCode" : 0,
                            "Data":{
                                contacts:[newResult],
                                timezone:timezone,
                                city:city
                            }
                        });
                    });
                })
            });
        } else {
            sendErrorResponse(res)
        }
    });
});

//This is the new API - '/fetch/contacts/at' has to be deprecated.
router.get('/fetch/contacts/travelling/to', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserIdFromMobileOrWeb(req);
    var allLocationsTrimmed = [],locationsCopy=[],locationsCopyForTwitter=[],locationsCopyForHashtag=[],participants = [];
    var city = null ,sublocality= null;
    var meetingId = req.query.invitationId;

    if(req.query.participants){
        participants = req.query.participants.split(',')
    }

    common.getLocationGoogleAPI(req.query.location,function (googleLocations) {

        if(googleLocations && googleLocations.results[0]) {

            _.each(googleLocations.results[0].address_components,function (component) {

                if(component.types.indexOf("locality") > -1 && component.types.indexOf("political") > -1){
                    allLocationsTrimmed.push({loc:component.long_name.trim()});
                    locationsCopy.push({loc:component.long_name.trim()});
                    locationsCopyForHashtag.push({loc:component.long_name.trim()});
                    locationsCopyForTwitter.push({loc:component.long_name.trim()});
                    city = component.long_name.trim();
                }

                if(component.types.indexOf("political") > -1 && component.types.indexOf("administrative_area_level_2") > -1){
                    locationsCopy.push({loc:component.long_name.trim()});
                    allLocationsTrimmed.push({loc:component.long_name.trim()});
                    locationsCopyForHashtag.push({loc:component.long_name.trim()});
                    locationsCopyForTwitter.push({loc:component.long_name.trim()});
                }

                if(component.types.indexOf("sublocality_level_1") > -1 && component.types.indexOf("political") > -1){
                    sublocality = component.long_name.trim();
                }
            });

            if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
                var timezone = req.session.profile.timezone.name
            }

            getTwitterLocations(common.castToObjectId(userId),locationsCopyForTwitter,participants,function (err,twitterContacts) {

                getHashtagLocation(common.castToObjectId(userId),locationsCopyForHashtag,participants,function (err,hashtagContacts) {

                    var twitterEmailIds = twitterContacts.length>0?_.pluck(twitterContacts, "emailId"):[];
                    var hashtagEmailIds = hashtagContacts.length>0?_.pluck(hashtagContacts[0].contacts, "personEmailId"):[];

                    contactObj.getContactsMatchingLocations(common.castToObjectId(userId),allLocationsTrimmed,participants,function (err,contacts) {

                        meetingClassObj.getParticipantsAtLocation(common.castToObjectId(userId), locationsCopy, participants, moment().subtract(180, "days"), moment().subtract(1, "days"), function (err, pastMeetings) {

                                var pastParticipants = [];

                                var liuEmailId = null;
                                if(contacts.length>0){
                                    liuEmailId = contacts[0]._id
                                } else if((req.session && req.session.profile && req.session.profile.emailId)) {
                                    liuEmailId = req.session.profile.emailId;
                                }

                                if(liuEmailId){
                                    _.each(pastMeetings,function (el) {
                                        _.each(el.participants,function (p) {
                                            if(p.emailId != liuEmailId){
                                                pastParticipants.push(p.emailId)
                                            }
                                        })
                                    });
                                }

                                var results3 = [];

                                _.each(pastMeetings,function (pm) {

                                    var result2 = pm.participants.map(function (val, index) {
                                        return {
                                            emailId:val.emailId,
                                            location:pm.scheduleTimeSlots[0].location,
                                            date:pm.scheduleTimeSlots[0].start.date
                                        }
                                    });

                                    results3.push(result2);

                                });

                                var contactsLocationEmailIds = contacts.length>0?_.pluck(contacts[0].contacts,"personEmailId"):[];
                                var uniqueEmailIds = _.union(twitterEmailIds,hashtagEmailIds,pastParticipants,contactsLocationEmailIds);

                                contactObj.getContactsByEmailIdArray(common.castToObjectId(userId),_.compact(uniqueEmailIds),function (error,matchedContacts) {

                                    if(results3.length>0 && matchedContacts.length>0){
                                        var result5 = _.chain(_.flatten(results3))
                                            .groupBy("emailId")
                                            .pairs()
                                            .map(function(currentItem) {
                                                var obj = _.object(_.zip(["emailId","meetingDetails"], currentItem));

                                                var orderedDates = obj.meetingDetails.reduce(function (a, b) {
                                                    return new Date(a.date) > new Date(b.date) ? a : b;
                                                });

                                                return orderedDates;
                                            })
                                            .value();

                                        if(result5.length>0) {

                                            result5.forEach(function (meeting) {
                                                for (var i = 0; i < matchedContacts[0].contacts.length; i++) {

                                                    if (meeting.emailId === matchedContacts[0].contacts[i].personEmailId) {
                                                        matchedContacts[0].contacts[i].pastMeetingLocation = meeting.location
                                                        matchedContacts[0].contacts[i].pastMeetingDate = meeting.date
                                                    }
                                                }
                                            });

                                            var actionItemsToStore = [];
                                            var contacts = matchedContacts[0].contacts;

                                            var emailId = liuEmailId;
                                            contacts.forEach(function(a){

                                                if(a.personEmailId != liuEmailId){

                                                    var obj = {
                                                        meetingId:meetingId,
                                                        userId: common.castToObjectId(userId.toString()),
                                                        userEmailId: emailId,
                                                        itemCreatedDate:new Date(),
                                                        actionTakenSource:null,
                                                        actionTaken: false,
                                                        actionTakenDate: null,
                                                        documentRecordFrom: 'userCollection',
                                                        itemType: 'travellingToLocation',
                                                        documentRecordId:a._id?common.castToObjectId(a._id.toString()):null,
                                                        personEmailId:a.personEmailId
                                                    }
                                                    actionItemsToStore.push(obj);
                                                }

                                            });

                                            actionItemsManagementObj.getTeamUserIds(common.castToObjectId(userId),null,null,function (err,teamData) {
                                                var teamMembers = _.pluck(teamData,"emailId");

                                                actionItemsManagementObj.findAndInsertNonExistingItemsByMeetingId(common.castToObjectId(userId),meetingId,actionItemsToStore,'travellingToLocation',function(result){
                                                    actionItemsManagementObj.findActionItemsByMeetingId(common.castToObjectId(userId),meetingId,'travellingToLocation',function(existingItem){
                                                        var newResult = {_id:null, contacts:[]}
                                                        matchedContacts[0].contacts.forEach(function(a){
                                                            existingItem.forEach(function(b){
                                                                if(String(a._id) == String(b.documentRecordId) && !_.contains(teamMembers, a.personEmailId) && !_.contains(teamMembers, b.personEmailId)){
                                                                    a.actionItemId = b._id;
                                                                    a.ownerEmailId = liuEmailId
                                                                    newResult.contacts.push(a);
                                                                }
                                                            })
                                                        });

                                                        res.send({
                                                            "SuccessCode": 1,
                                                            "ErrorCode" : 0,
                                                            "Data":{
                                                                contacts:[newResult],
                                                                timezone:timezone,
                                                                city:city,
                                                                sublocality:sublocality
                                                            }
                                                        });
                                                    });
                                                })
                                            })

                                        } else {
                                            res.send({
                                                "SuccessCode": 1,
                                                "ErrorCode" : 0,
                                                "Data":{
                                                    contacts:[],
                                                    timezone:timezone,
                                                    city:city,
                                                    sublocality:sublocality
                                                }
                                            });
                                        }
                                    } else if(matchedContacts.length>0) {
                                        actionItemsToStore = [];
                                        contacts = matchedContacts[0].contacts;

                                        emailId = liuEmailId;
                                        contacts.forEach(function(a){
                                            if(a.personEmailId != liuEmailId){
                                                var obj = {
                                                    meetingId:meetingId,
                                                    userId: common.castToObjectId(userId.toString()),
                                                    userEmailId: emailId,
                                                    itemCreatedDate:new Date(),
                                                    actionTakenSource:null,
                                                    actionTaken: false,
                                                    actionTakenDate: null,
                                                    documentRecordFrom: 'userCollection',
                                                    itemType: 'travellingToLocation',
                                                    documentRecordId:a._id?common.castToObjectId(a._id.toString()):null,
                                                    personEmailId:a.personEmailId
                                                }
                                                actionItemsToStore.push(obj);
                                            }
                                        });

                                        actionItemsManagementObj.getTeamUserIds(common.castToObjectId(userId),null,null,function (err,teamData) {
                                            var teamMembers = _.pluck(teamData, "emailId");
                                            actionItemsManagementObj.findAndInsertNonExistingItemsByMeetingId(common.castToObjectId(userId),meetingId,actionItemsToStore,'travellingToLocation',function(result){
                                                actionItemsManagementObj.findActionItemsByMeetingId(common.castToObjectId(userId),meetingId,'travellingToLocation',function(existingItem){
                                                    var newResult = {_id:null, contacts:[]}
                                                    matchedContacts[0].contacts.forEach(function(a){
                                                        existingItem.forEach(function(b){
                                                            if(String(a._id) == String(b.documentRecordId) && !_.contains(teamMembers, a.personEmailId) && !_.contains(teamMembers, b.personEmailId)){
                                                                a.actionItemId = b._id;
                                                                newResult.contacts.push(a);
                                                            }
                                                        })
                                                    });

                                                    res.send({
                                                        "SuccessCode": 1,
                                                        "ErrorCode" : 0,
                                                        "Data":{
                                                            contacts:[newResult],
                                                            timezone:timezone,
                                                            city:city,
                                                            sublocality:sublocality
                                                        }
                                                    });
                                                });
                                            })
                                        })


                                    } else {
                                        sendErrorResponse(res,{city:city,sublocality:sublocality})
                                    }
                                });
                        });
                    });
                });

            });
        } else {
            sendErrorResponse(res,{city:city,sublocality:sublocality})
        }
    });
});

router.get('/fetch/email/by/filter', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    var filter = req.query.filter;
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;
    var dataFor = req.query.dataFor;
    /*
    * Filter: all,responsePending,important,followUp,responsePendingAndImportant
    */
    var timezone = 'UTC';
    if(req.session && req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name){
        timezone = req.session.profile.timezone.name
    }

    actionItemsManagementObj.getTeamUserIds(common.castToObjectId(userId),null,null,function (err,teamData) {
        actionItemsManagementObj.fetchMailRecordIds(common.castToObjectId(userId),dataFor,filter,null,function(error,recordIds,detail){
            if(error){
                res.send({
                    "SuccessCode": 0,
                    "ErrorCode" : 1,
                    "Data":[]
                })
            }
            else {
                actionItemsManagementObj.getMailsByInteractionId(common.castToObjectId(userId), recordIds, function (err, mails) {
                    if (err) {
                        res.send({
                            "SuccessCode": 0,
                            "ErrorCode": 1,
                            "Data": []
                        })
                    }
                    else {
                        mails.forEach(function(a){
                            detail.forEach(function(b){
                                if(String(a.recordId) == String(b.documentRecordId) ){
                                    a.actionItemId = b._id;
                                }
                            })
                        })
                        if(dataFor == 'dashboard' && filter == "responsePendingAndImportant") {

                            var teamMembers = _.pluck(teamData,"emailId");

                            allUsers.findOne({_id: common.castToObjectId(userId)}, {actionableMailSetting: 1}, function (err, user) {
                                if(!user.actionableMailSetting || user.actionableMailSetting == 'all'){
                                    var impMailToMe = [];
                                    var impMailByImpContact = [];
                                    var impMails = [];
                                    var allMailByImpContact = [];
                                    var mailToMe = [];
                                    var followUp = [];
                                    var finalResult = [];

                                    mails.forEach(function (a) {

                                        if(!_.contains(teamMembers, a.emailId)){
                                            if(a.eAction && a.eAction == 'important' && a.toCcBcc == 'me'){
                                                impMailToMe.push(a);
                                            }
                                            if(a.eAction && a.eAction == 'important'){
                                                impMailByImpContact.push(a);
                                            }
                                            if(a.eAction && a.eAction == 'important'){
                                                impMails.push(a);
                                            }
                                            if(a.favorite){
                                                allMailByImpContact.push(a);
                                            }
                                            if(a.toCcBcc == 'me'){
                                                mailToMe.push(a)
                                            }
                                            if(a.trackInfo && a.trackInfo.trackResponse){
                                                followUp.push(a)
                                            }
                                        }
                                    });

                                    if(impMailToMe.length > 0){
                                        finalResult = impMailToMe;
                                    }
                                    else if(impMailByImpContact.length > 0){
                                        finalResult = impMailByImpContact;
                                    }
                                    else if(impMails.length > 0){
                                        finalResult = impMails;
                                    }
                                    else if(allMailByImpContact.length > 0){
                                        finalResult = allMailByImpContact;
                                    }
                                    else if(mailToMe.length > 0){
                                        finalResult = mailToMe;
                                    }
                                    else{
                                        finalResult = followUp;
                                    }

                                    res.send({
                                        "SuccessCode": 1,
                                        "ErrorCode": 0,
                                        "timezone": timezone,
                                        "Data": finalResult
                                    })
                                }
                                else {
                                    var impMailToMe = [];
                                    var impMailByImpContact = [];
                                    var impMails = [];
                                    var allMailByImpContact = [];
                                    var mailToMe = [];
                                    var filtered = [];

                                    mails.forEach(function (a) {
                                        if(a.eAction && a.eAction == 'important' && a.toCcBcc == 'me'){
                                            impMailToMe.push(a);
                                        }
                                        if(a.eAction && a.eAction == 'important'){
                                            impMailByImpContact.push(a);
                                        }
                                        if(a.eAction && a.eAction == 'important'){
                                            impMails.push(a);
                                        }
                                        if(a.favorite){
                                            allMailByImpContact.push(a);
                                        }
                                        if(a.toCcBcc == 'me'){
                                            mailToMe.push(a)
                                        }
                                    });

                                    if(user.actionableMailSetting == 'importantContact'){
                                        filtered = allMailByImpContact;
                                    }
                                    else if(user.actionableMailSetting == 'important'){
                                        if(impMailToMe.length > 0){
                                            filtered = impMailToMe;
                                        }
                                        else if(impMailByImpContact.length > 0){
                                            filtered = impMailByImpContact;
                                        }
                                        else{
                                            filtered = impMails;
                                        }
                                    }
                                    else if(user.actionableMailSetting == 'sentToMe'){
                                        filtered = mailToMe;
                                    }

                                    filtered.sort(function(a,b){
                                        return a.interactionDate < b.interactionDate;
                                    })
                                    res.send({
                                        "SuccessCode": 1,
                                        "ErrorCode": 0,
                                        "timezone": timezone,
                                        "Data": filtered
                                    })
                                }
                            })
                        }
                        else{
                            var filtered = [];
                            if(filter == 'important'){
                                mails.forEach(function (a) {
                                    if(a.eAction && a.eAction == 'important'){
                                        filtered.push(a);
                                    }
                                });
                            }
                            else{
                                filtered = mails;
                            }
                            res.send({
                                "SuccessCode": 1,
                                "ErrorCode": 0,
                                "timezone": timezone,
                                "Data": filtered
                            })
                        }
                    }
                })
            }
        });
    });
});

router.get('/fetch/contacts/at/my/meeting/location', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    var locations = [],participants = [],allLocationsTrimmed = [],locationsCopy=[],locationsCopyForHashtag=[],locationsCopyForTwitter=[],city='';

    participants.push(req.query.participants);
    locations.push(req.query.locations);
    var meetingId = req.query.meetingId;

    common.getLocationGoogleAPI(req.query.locations,function (googleLocations) {
        // common.getLocationGoogleAPI("NGV Koramangala, Bangalore",function (googleLocations) {

        if(googleLocations && googleLocations.results.length>0) {
            _.each(googleLocations.results[0].address_components,function (component) {


                if(component.types.indexOf("locality") > -1 && component.types.indexOf("political") > -1){
                    allLocationsTrimmed.push({loc:component.long_name.trim()});
                    locationsCopy.push({loc:component.long_name.trim()});
                    locationsCopyForHashtag.push({loc:component.long_name.trim()});
                    locationsCopyForTwitter.push({loc:component.long_name.trim()});
                    city = component.long_name.trim();
                }

                if(component.types.indexOf("political") > -1 && component.types.indexOf("administrative_area_level_2") > -1){
                    locationsCopy.push({loc:component.long_name.trim()});
                    allLocationsTrimmed.push({loc:component.long_name.trim()});
                    locationsCopyForHashtag.push({loc:component.long_name.trim()});
                    locationsCopyForTwitter.push({loc:component.long_name.trim()});
                }
            });

            getTwitterLocations(common.castToObjectId(userId),locationsCopyForTwitter,participants,function (err,twitterContacts) {

                getHashtagLocation(common.castToObjectId(userId), locationsCopyForHashtag, participants, function (err, hashtagContacts) {

                    var twitterEmailIds = twitterContacts.length>0?_.pluck(twitterContacts, "emailId"):[];
                    var hashtagEmailIds = hashtagContacts.length>0?_.pluck(hashtagContacts[0].contacts, "personEmailId"):[];

                    contactObj.getContactsMatchingLocations(common.castToObjectId(userId),allLocationsTrimmed,participants,function (err,contacts) {

                        var from = req.query.from;
                        var destination = req.query.destination;

                        meetingClassObj.getParticipantsAtLocation(common.castToObjectId(userId), locationsCopy, req.query.participants, moment().subtract(180, "days"), moment().subtract(1, "days"), function (err, pastMeetings) {

                            var pastParticipants = [];

                            var liuEmailId = null;
                            if(contacts.length>0){
                                liuEmailId = contacts[0]._id
                            } else if((req.session && req.session.profile && req.session.profile.emailId)) {
                                liuEmailId = req.session.profile.emailId;
                            }

                            if(liuEmailId){
                                _.each(pastMeetings,function (el) {
                                    _.each(el.participants,function (p) {
                                        if(p.emailId != req.query.participants && p.emailId != liuEmailId){
                                            pastParticipants.push(p.emailId)
                                        }
                                    })
                                });
                            }

                            var results3 = [];

                            _.each(pastMeetings,function (pm) {

                                var result2 = pm.participants.map(function (val, index) {
                                    return {
                                        emailId:val.emailId,
                                        location:pm.scheduleTimeSlots[0].location,
                                        date:pm.scheduleTimeSlots[0].start.date
                                    }
                                });

                                results3.push(result2);

                            });

                            var contactsLocationEmailIds = contacts.length>0?_.pluck(contacts[0].contacts,"personEmailId"):null;
                            var uniqueEmailIds = _.union(twitterEmailIds,hashtagEmailIds,pastParticipants,contactsLocationEmailIds);

                            contactObj.getContactsByEmailIdArray(common.castToObjectId(userId),_.compact(uniqueEmailIds),function (error,matchedContacts) {

                                if(results3.length>0 && matchedContacts.length>0){

                                    var result5 = _.chain(_.flatten(results3))
                                        .groupBy("emailId")
                                        .pairs()
                                        .map(function(currentItem) {
                                            var obj = _.object(_.zip(["emailId","meetingDetails"], currentItem));

                                            var orderedDates = obj.meetingDetails.reduce(function (a, b) {
                                                return new Date(a.date) > new Date(b.date) ? a : b;
                                            });

                                            return orderedDates;
                                        })
                                        .value();

                                    if(result5.length>0) {

                                        result5.forEach(function (meeting) {
                                            for (var i = 0; i < matchedContacts[0].contacts.length; i++) {

                                                if (meeting.emailId === matchedContacts[0].contacts[i].personEmailId) {
                                                    matchedContacts[0].contacts[i].pastMeetingLocation = meeting.location
                                                    matchedContacts[0].contacts[i].pastMeetingDate = meeting.date
                                                }
                                            }
                                        });
                                        var actionItemsToStore = [];
                                        var contacts = matchedContacts[0].contacts;
                                        var emailId = liuEmailId;
                                        contacts.forEach(function(a){

                                            if(a.personEmailId != liuEmailId){
                                                var obj = {
                                                    meetingId:meetingId,
                                                    userId: common.castToObjectId(userId.toString()),
                                                    userEmailId: emailId,
                                                    itemCreatedDate:new Date(),
                                                    actionTakenSource:null,
                                                    actionTaken: false,
                                                    actionTakenDate: null,
                                                    documentRecordFrom: 'userCollection',
                                                    itemType: 'peopleAtMeetingLocation',
                                                    documentRecordId:a._id?common.castToObjectId(a._id.toString()):null,
                                                    personEmailId:a.personEmailId
                                                }
                                                actionItemsToStore.push(obj);
                                            }

                                        });

                                        actionItemsToStore = _.uniq(actionItemsToStore,'personEmailId')

                                        actionItemsManagementObj.getTeamUserIds(common.castToObjectId(userId),null,null,function (err,teamData) {
                                            var teamMembers = _.pluck(teamData, "emailId");

                                            actionItemsManagementObj.findAndInsertNonExistingItemsByMeetingId(common.castToObjectId(userId),meetingId,actionItemsToStore,'peopleAtMeetingLocation',function(result){
                                                actionItemsManagementObj.findActionItemsByMeetingId(common.castToObjectId(userId),meetingId,'peopleAtMeetingLocation',function(existingItem){
                                                    var newResult = {_id:null, contacts:[],city:city}
                                                    matchedContacts[0].contacts.forEach(function(a){
                                                        existingItem.forEach(function(b){
                                                            if(String(a._id) == String(b.documentRecordId) && !_.contains(teamMembers, a.personEmailId) && !_.contains(teamMembers, b.personEmailId)){
                                                                a.actionItemId = b._id;
                                                                newResult.contacts.push(a);
                                                            }
                                                        })
                                                    })
                                                    res.send(newResult)
                                                });
                                            })
                                        });


                                    } else {
                                        sendErrorResponse(res);
                                    }
                                } else if(matchedContacts.length>0){
                                    actionItemsToStore = [];
                                    contacts = matchedContacts[0].contacts;
                                    emailId = liuEmailId;
                                    contacts.forEach(function(a){
                                        if(a.personEmailId != liuEmailId){
                                            var obj = {
                                                meetingId:meetingId,
                                                userId: common.castToObjectId(userId.toString()),
                                                userEmailId: emailId,
                                                itemCreatedDate:new Date(),
                                                actionTakenSource:null,
                                                actionTaken: false,
                                                actionTakenDate: null,
                                                documentRecordFrom: 'userCollection',
                                                itemType: 'peopleAtMeetingLocation',
                                                documentRecordId:a._id?common.castToObjectId(a._id.toString()):null,
                                                personEmailId:a.personEmailId
                                            }
                                            actionItemsToStore.push(obj);
                                        }
                                    });

                                    actionItemsToStore = _.uniq(actionItemsToStore,'personEmailId')

                                    actionItemsManagementObj.getTeamUserIds(common.castToObjectId(userId),null,null,function (err,teamData) {
                                        var teamMembers = _.pluck(teamData, "emailId");
                                        actionItemsManagementObj.findAndInsertNonExistingItemsByMeetingId(common.castToObjectId(userId),meetingId,actionItemsToStore,'peopleAtMeetingLocation',function(result){
                                            actionItemsManagementObj.findActionItemsByMeetingId(common.castToObjectId(userId),meetingId,'peopleAtMeetingLocation',function(existingItem){

                                                var newResult = {_id:null, contacts:[],city:city}
                                                matchedContacts[0].contacts.forEach(function(a){
                                                    existingItem.forEach(function(b){
                                                        if(String(a._id) == String(b.documentRecordId && !_.contains(teamMembers, a.personEmailId) && !_.contains(teamMembers, b.personEmailId))){
                                                            a.actionItemId = b._id;
                                                            newResult.contacts.push(a);
                                                        }
                                                    })
                                                })

                                                res.send(newResult)
                                            });
                                        })
                                    })

                                } else {
                                    sendErrorResponse(res);
                                }
                            });

                        });
                    });
                })
            });

        } else {
            sendErrorResponse(res);
        }
    });
});

router.get('/fetch/distance/matrix',common.isLoggedInUserOrMobile,function (req,res) {

    // var from = "Wilson Garden, Bangalore"
    // var destination = "HSR Layout Koramangala"+'|'+"Majestic, Bangalore"

    var from = req.query.from;
    var destination = req.query.destination;

    fetchDistanceMatrix(from,destination,function (err,response) {

        if(!err && response && response.destination_addresses){
            var result = response.destination_addresses.map(function(val, index){
                return {
                    source: from,
                    destination: val,
                    distance: response.rows[0].elements[index].distance.text,
                    duration: response.rows[0].elements[index].duration.text,
                    distanceInMeters: response.rows[0].elements[index].duration.value
                };
            });

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":result[0]
            });

        } else {
            sendErrorResponse(res)
        }
    })

});

router.get('/fetch/tweet/by/twitter/user/name', function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var twitterUserName = req.query.twitterUserName;
    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,twitter:1},function(error,user){
        if(error || !user){
            res.send({
                "SuccessCode": 0,
                "Message": "",
                "ErrorCode": 1,
                "Data":null
            })
        }
        else {

            socialManagement.fetchTweet(twitterUserName, function (err, data) {
                if(!err && data){
                    var tweetHTML = '<div>'+data.tweet+'</div>'

                    res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": {
                            tweet:tweetHTML,
                            id:data.tweetId,
                            imageUrl:data.imageUrl
                        }
                    })
                } else {
                    if(twitterApiObj.validateTwitterAccount(user.twitter)) {
                        twitterApiObj.getTopTweetByTwitterUserName(user.twitter, twitterUserName, function (error, topTweet) {

                            if(error){
                                res.send({
                                    "SuccessCode": 0,
                                    "Message": "",
                                    "ErrorCode": 1,
                                    "Data":null
                                })
                            }
                            else {

                                /*
                                 * Store recent tweets permanently
                                 * */
                                socialManagement.storeTweets(topTweet,twitterUserName);

                                var tweetHTML = '<div>'+topTweet.data+'</div>'

                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    // "Data": topTweet.statuses[0] ? topTweet.statuses[0].text : null
                                    "Data": {
                                        tweet:tweetHTML,
                                        id:topTweet.id,
                                        imageUrl:topTweet.imageUrl
                                    }
                                })
                            }
                        })
                    }
                    else {
                        // //TODO sunil if user is not twitter user use relatas account credentials
                        // twitter = {
                        //     refreshToken: 'relatas refresh token here',
                        //     token: 'relatas token here'
                        // }
                        res.send({
                            "SuccessCode": 1,
                            "Message": "",
                            "ErrorCode": 0,
                            "Data": ""
                        })
                    }
                }
            })
        }
    });
});

router.get('/fetch/stored/tweets',common.isLoggedInUserOrMobile,function (req,res) {
    
    if(req.query.twitterUserName){
        socialManagement.fetchTweet(req.query.twitterUserName,function (err,data) {

            if(!err && data){
                var tweetHTML = '<div>'+data.tweet+'</div>'

                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": {
                        tweet:tweetHTML,
                        id:data.tweetId,
                        imageUrl:data.imageUrl
                    }
                })
            } else {
                sendErrorResponse(res,'NO_TWEETS_FOUND')
            }
        });

    } else {
        sendErrorResponse(res,'NO_TWEETS_FOUND')
    }
});

router.get('/fetch/stored/tweets/by/email',common.isLoggedInUserOrMobile,function (req,res) {

    if(req.query.emailId){
        socialManagement.fetchTweetByEmailId(req.query.emailId,function (err,data) {

            if(!err && data){
                var tweetHTML = '<div>'+data.tweet+'</div>'

                res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": {
                        tweet:tweetHTML,
                        id:data.tweetId,
                        imageUrl:data.imageUrl
                    }
                })
            } else {
                sendErrorResponse(res,'NO_TWEETS_FOUND')
            }
        });

    } else {
        sendErrorResponse(res,'NO_TWEETS_FOUND')
    }
});

router.get('/fetch/favorite/contacts',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagementObj.findFavoriteTwitterContacts(common.castToObjectId(userId.toString()),function(error,data){

        if(!error && data.length>0){

            var contacts = _.uniq(data[0].contacts,'personEmailId');
            contacts.forEach(function(a){
                if(a.twitterUserName){
                    a.sortByName = a.personName;
                }
                else if(a.account && a.account.twitterUserName){
                    a.sortByName = a.account.name;
                }
            })
            contacts.sort(function(a,b){
                var nameA = a.sortByName.toUpperCase();
                var nameB = b.sortByName.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            })
            res.send(contacts);

        } else {
            res.send([])
        }
    });

});

router.get('/fetch/latest/tweets',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var counter = 1;

    // req.session.twitterCacheReady = false
    userManagementObj.findTwitterUsername(common.castToObjectId(userId.toString()),function(error,data){

        if(req.session && req.session.twitterCacheReady){

            if(!error && data && data[0]){
                tweetsStored(res,req,data[0].contacts);
            } else {
                res.send({
                    contacts:[],
                    noTwitterAccSetup:true
                });
            }

        } else {

            if(!error && data[0] && data[0].contacts.length>0 && twitterApiObj.validateTwitterAccount(data[0]._id)) {
                req.session?req.session.twitterCacheReady = true:false;

                var contacts = [];
                var tweets = [];
                var tempContacts = _.uniq(data[0].contacts,'personEmailId');
                _.each(tempContacts,function (contact) {

                    if(contact.account.twitterUserName){

                        twitterApiObj.getTopTweetByTwitterUserName(data[0]._id, contact.account.twitterUserName, function (error, topTweet) {

                            counter++;
                            if(!error && topTweet) {

                                var tweetHTML = '<div>'+topTweet.data+'</div>';
                                contact.tweet = {
                                    tweet:tweetHTML,
                                    tweetId:topTweet.id,
                                    imageUrl:topTweet.imageUrl,
                                    twitterUserName:contact.account.twitterUserName,
                                    tweetedDate: new Date(topTweet.created_at),
                                    emailId:null,
                                    company:contact.account.name,
                                    userLocation:topTweet.location
                                };

                                tweets.push(contact.tweet);

                                contacts.push(contact.tweet)

                                if(counter === data[0].contacts.length){
                                    /*
                                     * Store recent tweets permanently
                                     * */
                                    socialManagement.bulkStoreTweets(tweets);
                                    res.send({
                                        contacts:contacts,
                                        noTwitterAccSetup:false
                                    });
                                }
                            } else {
                                if(counter === data[0].contacts.length){
                                    socialManagement.bulkStoreTweets(tweets);
                                    res.send({
                                        contacts:contacts,
                                        noTwitterAccSetup:false
                                    });
                                }
                            }
                        });

                    }if(contact.twitterUserName) {

                        twitterApiObj.getTopTweetByTwitterUserName(data[0]._id, contact.twitterUserName, function (error, topTweet) {
                            counter++;

                            if(!error && topTweet && topTweet.data){
                                var tweetHTML = '<div>'+topTweet.data+'</div>';
                                contact.tweet = {
                                    tweet:tweetHTML,
                                    tweetId:topTweet.id,
                                    imageUrl:topTweet.imageUrl,
                                    twitterUserName:contact.twitterUserName,
                                    tweetedDate: new Date(topTweet.created_at),
                                    emailId:contact.personEmailId,
                                    company:contact.account.name,
                                    userLocation:topTweet.location
                                };

                                tweets.push(contact.tweet);

                                contacts.push(contact);

                                if(counter === data[0].contacts.length){
                                    socialManagement.bulkStoreTweets(tweets);
                                    res.send({
                                        contacts:contacts,
                                        noTwitterAccSetup:false
                                    });
                                }
                            } else {
                                if(counter === data[0].contacts.length){
                                    socialManagement.bulkStoreTweets(tweets);
                                    res.send({
                                        contacts:contacts,
                                        noTwitterAccSetup:false
                                    });
                                }
                            }
                        });
                    }
                });

            } else {
                fetchStoredTweetsForFavoriteContacts(res,common.castToObjectId(userId.toString()))
            }
        }
    });

});

router.get('/fetch/yesterdays/meetings', common.isLoggedInUserOrMobile, function(req, res) {
    var userId = common.getUserIdFromMobileOrWeb(req);
    var invitationIdToDelete = req.query.invitationId;

    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,timezone:1,location:1,currentLocation:1,serviceLogin:1,google:1,outlook:1},function(error,user){
        actionItemsManagementObj.findYesterdaysMeetings(user,'fetch',function(result){

            if(result && result.length>0){

                var queryFinal = { "$or": result.map(function(el) {
                        var obj = {};
                        obj["ownerId"] = common.castToObjectId(userId);
                        obj["userId"] = {$ne:common.castToObjectId(userId)}
                        obj["interactionDate"] = {'$gte': new Date(el.meetingDate)}
                        obj["emailId"] = el.personEmailId;
                        // obj["action"] = "receiver";
                        obj["interactionType"] = "email";
                        return obj;
                    })};
                interactionManagementObj.checkIfInteractedWithContacts(queryFinal,function (err,status) {

                    var diff = differenceBy(result,status,"personEmailId")
                    var invitationIds = _.uniq(_.pluck(diff,"invitationId"));

                    meetingClassObj.findMeetingsCustomQuery({invitationId:{$in:invitationIds}},{participants:1,invitationId:1},function (participants) {

                        _.each(diff,function (el) {
                            _.each(participants,function (p) {
                                if(el.invitationId === p.invitationId){

                                    var participants = [];
                                    _.each(p.participants,function (p) {
                                        if(p.emailId != user.emailId){
                                            participants.push(p)
                                        }
                                    })
                                    el["participants"] = participants
                                }
                            })
                        });

                        if(diff && diff[0]) {
                            // res.send(diff);
                            res.send({
                                SuccessCode: 1,
                                Errorcode: 0,
                                Data: diff
                            });
                        } else{
                            res.send({
                                SuccessCode: 1,
                                Errorcode: 0,
                                Data: []
                            });
                            
                        }
                    })
                });
            } else {
                res.send({
                    SuccessCode: 1,
                    Errorcode: 0,
                    Data: []
                });
            }
        })
    });
});

router.post('/update/relatas/events', common.isLoggedInUserOrMobile, function(req, res){

    var updateItem = req.body;
    var userId = common.getUserIdFromMobileOrWeb(req);

    var findQuery = {invitationId:updateItem.meeting.invitationId,"scheduleTimeSlots.title":updateItem.meeting.title};

    var updateObj = {
        $set: {
            "scheduleTimeSlots.$.start.date": updateItem.newSlotDetails.startDateTime,
            "scheduleTimeSlots.$.end.date": updateItem.newSlotDetails.endDateTime
        }
    };

    var attendees = [
        {
            email:"events@relatas.com"
        },
        {
            email: req.session.profile.emailId
        }
    ];

    var request_body = {
        id: updateItem.meeting.invitationId,
        summary: updateItem.meeting.title,
        attendees: attendees,
        end: {
            dateTime: new Date(updateItem.newSlotDetails.startDateTime)
        },
        start: {
            dateTime: new Date(updateItem.newSlotDetails.startDateTime)
        }
    }

    userManagementObj.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),{emailId:1,google:1},function(error,user){

        common.getNewGoogleToken(user.google[0].token,user.google[0].refreshToken,function (token) {
            if(token){

                var google_calendar = new gcal.GoogleCalendar(token);

                google_calendar.events.update('primary', updateItem.meeting.invitationId, request_body, function (err, googleResult) {

                    if (err) {

                        logger.info('Error in Google event update ' + JSON.stringify(err));
                        sendErrorResponse(res)
                    }
                    else {

                        meetingClassObj.updateRelatasEvent(findQuery,updateObj,function (error,result) {

                            if(!error && result){
                                res.send({
                                    "SuccessCode": 1,
                                    "Message": "",
                                    "ErrorCode": 0,
                                    "Data":""
                                });

                            } else {
                                sendErrorResponse(res)
                            }
                        });
                    }
                });

            } else {

                sendErrorResponse(res);
            }
        });
    });

});

router.get('/fetch/watchlist/contacts',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);

    var type = req.query.type;

    userManagementObj.findWatchListContacts(common.castToObjectId(userId.toString()),type,function(error,data){

        if(!error && data.length>0){

            var contacts = _.uniq(data[0].contacts,'personEmailId');
            var results = _.uniq(contacts,'account.name');

            res.send(results);

        } else {
            res.send([])
        }
    });
});

router.post('/update/watchlist/contact',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);

    contactObj.updateWatchlist(common.castToObjectId(userId),common.castToObjectId(req.body.contactId),req.body.watchThisContact,req.body.type,function(err,isSuccess){
        if(err){
            res.send(errorObj.generateErrorResponse({status:statusCodes.SOMETHING_WENT_WRONG_CODE,key:'UPDATE_DB_ERROR'}));
        }
        else if(isSuccess){
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data":{}
            });
        }else res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'INVALID_DATA_RECEIVED'}));
    })
});

router.post('/update/twitter/username/for/company/or/person',common.isLoggedInUserOrMobile, function(req, res){
    /*
     updateFor = "company"/"person"
     */
    
    var emailId = req.body.emailId;
    var twitterUserName = req.body.twitterUserName;
    var updateFor = req.body.updateFor;
    var companyName = req.body.companyName;
    userManagementObj.updateTwitterHandelForCompanyOrPerson(emailId,companyName,twitterUserName,updateFor,function(error,result){

        if(!error && result){

            req.session.twitterCacheReady = false; //Reset this so that new tweets are fetched.
            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": result
            })
        } else {
            res.send({
                "SuccessCode": 0,
                "Message": "",
                "ErrorCode": 1,
                "Data": result
            })
        }
    });
});

router.post('/search/user/contacts',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var contactName = req.body.contactName;
    var searchByEmailId = true;

    var fetchWatchlistType = req.body.fetchWatchlistType;
    
    contactObj.searchUserContacts(contactName,searchByEmailId,common.castToObjectId(userId),0,25,fetchWatchlistType,function(error,contacts){
        if(!error && contacts && contacts.length>0){

            var contactsFormatted = [];

            _.each(contacts,function (co) {

                if(!co.personId || (typeof co.personId == "String" || typeof co.personId == "string")){
                    co.personId = {}
                }

                contactsFormatted.push(co)

            });

            res.send({
                "SuccessCode": 1,
                "Message": "",
                "ErrorCode": 0,
                "Data": contactsFormatted
            });
        } else {
            res.send({
                "SuccessCode": 0,
                "Message": "",
                "ErrorCode": 1,
                "Data": []
            })
        }
    });
});

router.get('/v1/header/notifications',common.isLoggedInUserOrMobile, function(req, res){

    if(req.session && req.session.notifications){
        res.send(req.session.notifications)
    } else {
        sendErrorResponse(res)
    }

});

router.get('/v1/header/notifications/read/status',common.isLoggedInUserOrMobile, function(req, res){

    if(req.query.setNotificationsRead === 'true'){
        if(!req.session.notifications){
            req.session.notifications = {};
        }

        req.session.notifications.notficationsRead = true;
    }

    res.send(req.session)

});

function fetchDistanceMatrix(from,destination,callback) {

    var APIkey = authConfig.GOOGLE_API_KEY

    var url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='+from
    url = url + '&destinations='+destination+"&key="+ APIkey

    request(url,function(e,r,b) {
        
        try {
            var result = JSON.parse(b);
            callback(null,result)
        } catch (e) {
            callback(e,null)
        }
        // callback(e,b)
    });

}

router.post('/update/action/taken/info',common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var actionItemId = req.body.actionItemId;
    var actionTakenSource = req.body.actionTakenSource;
    var actionTakenType = req.body.actionTakenType;
    
    actionItemsManagementObj.updateActionTakenInfoByDocumentRecordId(common.castToObjectId(userId),common.castToObjectId(actionItemId),actionTakenSource,actionTakenType,function(result){
       if(result){
           res.send({
               "SuccessCode": 1,
               "Message": "",
               "ErrorCode": 0,
               "Data": []
           })
       } 
        else {
           res.send({
               "SuccessCode": 0,
               "Message": "",
               "ErrorCode": 1,
               "Data": []
           })
       }
    });

});

router.post('/update/lt/action/taken',common.isLoggedInUserOrMobile, function(req, res){

    var userEmailId = req.session.profile.emailId?req.session.profile.emailId:null
    
    var obj = {
        userId: common.getUserIdFromMobileOrWeb(req),
        actionTakenSource: "dashboard",
        actionTakenType: "sendEmail",
        itemCreatedDate: new Date(),
        documentRecordFrom:"losingTouchCollection",
        documentRecordId:null,
        personEmailId:req.body.emailId,
        actionTaken:true,
        actionTakenDate: new Date(),
        userEmailId:userEmailId,
        itemType:"losingTouch"
    }

    actionItemsManagementObj.updateLosingTouchActionTaken(obj,function(result){
       if(result){
           res.send({
               "SuccessCode": 1,
               "Message": "",
               "ErrorCode": 0,
               "Data": []
           })
       }
        else {
           res.send({
               "SuccessCode": 0,
               "Message": "",
               "ErrorCode": 1,
               "Data": []
           })
       }
    });

});

module.exports = router;

function todayDateMinMax(userId,date, callback) {
    allUsers.find({ _id: userId }, { timezone: 1 }, function(err, user) {
        if (err || user.length == 0) {
            callback(err, null);
        }
        else {
            if (user[0].timezone && user[0].timezone.name)
                timezone = user[0].timezone.name;
            else
                timezone = 'UTC';
            if(date){
                var dateMin = moment(date).tz(timezone);
                var dateMax = moment(date).tz(timezone);
            }
            else{
                var dateMin = moment().tz(timezone);
                var dateMax = moment().tz(timezone);
            }

            dateMin.hour(0)
            dateMin.minute(0)
            dateMin.second(0)
            dateMin.millisecond(0)

            dateMax.hour(23)
            dateMax.minute(59)
            dateMax.second(59)
            dateMax.millisecond(0)
            callback(null, { min: dateMin, max: dateMax, timezone:user[0].timezone })
        }
    });
}

function sendErrorResponse(res,data) {
    res.send({
        "SuccessCode": 0,
        "Message": "",
        "ErrorCode": 1,
        "Data":data?data:''
    });
}

function tweetsStored(res,req,contacts) {

    var twitterUserNames = [];
    var len = contacts.length;

    for(var i=0;i<len;i++){

        if(contacts[i].twitterUserName) {
            twitterUserNames.push(contacts[i].twitterUserName)
        }

        if(contacts[i].account.twitterUserName){
            twitterUserNames.push(contacts[i].account.twitterUserName)
        }
    }

    socialManagement.fetchTweets(_.uniq(twitterUserNames),function (err,data) {

        _.each(contacts, function (contact) {
            _.each(data, function (tweet) {
                if (contact.twitterUserName == tweet.twitterUserName) {
                    contact.tweet = tweet;
                }

                if(contact.account.twitterUserName == tweet.twitterUserName){
                    contacts.push({tweet:tweet})
                }
            });
        });

        res.send({
            contacts:contacts,
            noTwitterAccSetup:false
        })
    });
}

function fetchStoredTweetsForFavoriteContacts(res,userId) {

    userManagementObj.findFavoriteContacts(userId,function (err,data) {
       if(!err && data && data[0] && data[0].contacts.length>0){

           var contacts = data[0].contacts;
           var emailIds = _.pluck(data[0].contacts,'personEmailId');
           var companies = _.pluck(data[0].contacts,'account.name');
           var result = [];

           socialManagement.fetchTweetsForNonTwitterUser(emailIds,companies,function (err,tweets) {

               _.each(contacts, function (contact) {
                   _.each(tweets, function (tweet) {

                       if (contact.personEmailId == tweet.emailId) {

                           contact.tweet = tweet;
                           result.push(contact)
                       }

                       if(contact.account.name && contact.account.name == tweet.company){
                           result.push({tweet:tweet})
                       }
                   });
               });

               res.send({
                   contacts:_.uniq(result,'tweet.twitterUserName'),
                   noTwitterAccSetup:true
               })
           })

       } else {
           res.send([]);
       }
    });
}

// router.get('/generate/fake/data',common.isLoggedInUserOrMobile, function(req, res){
//
//     if (['DEV',
//             'LOCAL',
//             'LOCAL1'].indexOf(appCredential.getServerEnvironment()) != - 1) {
//
//         if(req.query.auth === 'jimmytestacc@gmail.com'){
//             var len = req.query.items;
//             var fakes = [];
//             for(var i = 0;i<len;i++){
//                 fakes.push(getFakeContact(i))
//             }
//
//             contactObj.insertContact(req.query.emailId,fakes,function (err,result) {
//                 res.send({
//                     err:err,
//                     result:result,
//                     fakes:fakes.length
//                 })
//             })
//         }
//
//     } else {
//         res.send(false)
//     }
//
// });
//
// router.get('/generate/messages',common.isLoggedInUserOrMobile, function(req, res) {
//
//         var before = moment().subtract(90, "days")
//         var after = moment().add(0, "days")
//
//         var obj = {
//             'userId': "54ed794100a1b2d70482d5da",
//             'afterDate': moment(after).format('YYYY-MM-DD'),
//             'beforeDate': moment(before).format('YYYY-MM-DD')
//         };
//
//         var len = 1000;
//         var counter = 0;
//
//         function seq(counter){
//
//             rabbitmqObj.producer(obj,function(result){
//
//                 if(result && counter<2000){
//                     logger.info("Add OBJ to processQueue Success ",counter);
//                     seq(counter+1)
//                 } else {
//                     logger.info("Failed to Add OBJ to processQueue ",counter);
//                     seq(counter+1)
//                 }
//             });
//         }
//         seq(counter)
// })
//
// router.get('/generate/fake/profile',common.isLoggedInUserOrMobile, function(req, res){
//
//     if (['DEV', 'LOCAL', 'LOCAL1'].indexOf(appCredential.getServerEnvironment()) != - 1) {
//
//         var len = req.query.items;
//         var name = req.query.name;
//         var fakeContacts = [];
//         var fakeProfiles = [];
//         for(var i = 0;i<10000;i++){
//             fakeContacts.push(getFakeContact(i))
//         }
//
//         for(var i = 0;i<len;i++){
//             fakeProfiles.push(getFakeProfile(i,name,fakeContacts))
//         }
//
//         contactObj.createFakeProfile(fakeProfiles,function (err,result) {
//
//             res.send({
//                 err:err,
//                 result:result,
//                 fakeProfiles:fakeProfiles.length,
//                 fakeContacts:fakeContacts.length
//             })
//         })
//     } else {
//         res.send(false)
//     }
//
// });
//
// function getFakeProfile(i,name,fakeContacts) {
//     return {
//         emailId: name+i+"@gmail.com",
//         publicProfileUrl: name+i,
//         contactsUpload: {
//             isUploaded: false
//         },
//         serviceLogin: "google",
//         currentLocation: {
//             longitude: 77.5833,
//             latitude: 12.9833,
//             region: "KA",
//             city: "Bengaluru",
//             country: "India"
//         },
//         timezone: {
//             updated: true,
//             zone: "+05:30",
//             name: "Asia/Kolkata"
//         },
//         location: "Bangalore, Karnataka, India",
//         mobileNumber: "919972607894",
//         companyName: "Infosys"+i,
//         designation: "F1 Racer"+i,
//         profilePicUrl: "/profileImages/naveenpaul.jpg",
//         corporateUser: false,
//         lastName: "Paul"+i,
//         firstName: "Naveen"+i,
//         contacts:fakeContacts,
//         registeredUser : true,
//         firstLogin : false,
//         createdDate : new Date(),
//         partialFill : false,
//         dashboardPopup : true
//     }
// }

// function getFakeContact(i){
//     return {
//         "addedDate" : new Date(),
//         "personEmailId" : "isaacbhaskar"+i+"@gmail.com",
//         "remindToConnect" : null,
//         "ignoreContact" : null,
//         "hashtag" : ["fake"],
//         "account" : {
//             "ignore" : null,
//             "value" : {
//                 "lost" : null,
//                 "won" : null,
//                 "inProcess" : null
//             },
//             "updatedOn" : null,
//             "showToReportingManager" : true,
//             "selected" : false,
//             "name" : null
//         },
//         "losingTouchRecommendation" : {
//             "date" : null
//         },
//         "notes" : [],
//         "relatasContact" : true,
//         "relatasUser" : true,
//         "verified" : true,
//         "favorite" : true,
//         "lastInteracted" : null,
//         "linkedinUserName" : null,
//         "facebookUserName" : null,
//         "twitterUserName" : null,
//         "count" : 0,
//         "relationshipStrength_updated" : 0,
//         "contactRelation" : {
//             "decisionmaker_influencer" : null,
//             "prospect_customer" : null
//         },
//         "source" : "google",
//         "mobileNumber" : "99726"+i,
//         "skypeId" : "",
//         "designation" : "F1 Racer",
//         "companyName" : "Ferrari Auto",
//         "location" : "Bengaluru, Karnataka 560001, India",
//         "personName" : "Jimmy Paul"+i,
//         "contactImageLink" : "https://www.google.com/m8/feeds/photos/media/jimmytestacc%40gmail.com/73d8e4328ee4f9a2",
//         "watchThisContact" : false
//     }
//
// }

router.get('/csv/to/jsontwitter',common.isLoggedInUserOrMobile,function (req,res) {

    socialManagement.csvToJSON('./public/oppIvalue.csv',function (err,result) {
        res.send({
            err:err,
            result:result
        })
    });

    // if(req.query.updateFor === 'company'){
    //     socialManagement.csvToJSON('./public/convert.csv',function (err,result) {
    //         result = _.uniq(result,'updateFor');
    //
    //         if(result.length>0){
    //             addTwitterHandle(0,result,req.query.updateFor,function (updatesDone) {
    //                 res.send({
    //                     err:err,
    //                     result:updatesDone
    //                 })
    //             });
    //         } else {
    //             res.send({
    //                 err:err,
    //                 result:result
    //             })
    //         }
    //     });
    // } else if(req.query.updateFor === 'person'){
    //     socialManagement.csvToJSON('./public/contacts.csv',function (err,result) {
    //         result = _.uniq(result,'email');
    //
    //         if(result.length>0){
    //             addTwitterHandle(0,result,req.query.updateFor,function (updatesDone) {
    //                 res.send({
    //                     err:err,
    //                     result:updatesDone
    //                 })
    //             });
    //         } else {
    //             res.send({
    //                 err:err,
    //                 result:result
    //             })
    //         }
    //     });
    // }
});

function addTwitterHandle(skip,data,updateFor,callback) {

    allUsers.collection.find({},{_id:1}).sort({_id: 1}).skip(skip).limit(1).toArray(function (err,user) {
        var userId = user[0]._id

        if(userId){
            contactObj.updateTwitterHandlesForCompanyOrPerson(userId,data,updateFor,function (result) {
                addTwitterHandle(skip+1,data,updateFor)
            });
        } else {
            if(callback){
                callback(true)
            }
        }
    });
}

function getHashtagLocation(userId,locations,participants,callback) {
    contactObj.getContactsMatchingLocationsHashtag(userId,locations,participants,function (err,contacts) {
        callback(err,contacts)
    })
}

function getTwitterLocations(userId,locations,participants,callback) {
    userManagementObj.findUserContactsTwitterUsername(common.castToObjectId(userId),participants,function (err,contactsEmailId) {

       var twitterUserNames = _.pluck(contactsEmailId,'twitterUserName');

        socialManagement.fetchTwitterUserLocation(twitterUserNames,locations,function (err,twitterLocations) {
            callback(err,twitterLocations)
        });
    })
}