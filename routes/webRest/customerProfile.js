//**************************************************************************************
// File Name            : customerProfile
// Functionality        : Router for customerProfiles(Quotation/Purchase Order/Invoice)
// History              : First Version
//
//
//*******************************************************************************

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var _ = require("lodash");
var fs = require('fs');

var commonUtility = require('../../common/commonUtility');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var contactClass = require('../../dataAccess/contactsManagementClass');
var rabbitmq = require('../../common/rabbitmq');
var customerProfileTemplatesManagementClass = require('../../dataAccess/customerProfileTemplateManagement');
var customerProfileTemplatesCollection = require('../../databaseSchema/customerProfileTemplateSchema').customerProfileTemplateModel;
var customerProfileTemplatesManagementClassObj = new customerProfileTemplatesManagementClass();

var customerProfileManagementClass = require('../../dataAccess/customerProfileManagement');
var customerProfileCollection = require('../../databaseSchema/customerProfileSchema').customerProfileModel;
var customerProfilesManagementClassObj = new customerProfileManagementClass();

var common = new commonUtility();
var rabbitmqObj = new rabbitmq();
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var statusCodes = errorMessagesObj.getStatusCodes();

// ui routes
router.get('/customerProfiles/show/all',common.isLoggedInUserToGoNext, function(req, res){
    res.render('customerProfiles/corporateAdmin/showAllCustomerProfiles');
});


router.get('/customerProfile/create/new',common.isLoggedInUserToGoNext, function(req, res){
    res.render('customerProfiles/corporateUser/createCustomerProfile');
});

router.get('/customerProfiles/get/all/data', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    customerProfilesManagementClassObj.getAllCustomerProfiles(companyId, function(err, customerProfiles){
        if(err) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        }
        else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Data: customerProfiles

            })
        }
    });
});

router.post("/customerProfiles/create/new",  function (req, res) {
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var companyId = common.castToObjectId( userProfile.companyId);

    console.log(" invoking checkAndCreateCustomerProfile", req.body);

    customerProfilesManagementClassObj.checkAndCreateCustomerProfile(companyId, userId, req.body, function(error, savedCustomerProfile) {
        if(error) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1
            })
        } else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                CustomerProfile: savedCustomerProfile
            })
        }
    })
});

router.post("/customerProfile/update/existing",  function (req, res){
    var userId = common.castToObjectId(common.getUserId(req.user))
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;
    var objCompanyId = common.castToObjectId( userProfile.companyId);

    customerProfilesManagementClassObj.updateCustomerProfileElements(objCompanyId, req.body, function(err, result) {
        if(err) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
            });
        } else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
            })
        }
    });
});

router.post('/customerProfile/get/by/id', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile.companyId;

    console.log("got the customerProfile Id as ", req.body.customerProfileId)
    customerProfilesManagementClassObj.getCustomerProfileById(common.castToObjectId(companyId),
        common.castToObjectId(req.body.customerProfileId),
        function(err, customerProfiles){
            if(err) {
                res.send({
                    SuccessCode: 0,
                    ErrorCode: 1
                })
            }
            else {
                res.send({
                    SuccessCode: 1,
                    ErrorCode: 0,
                    Data: customerProfiles

                })
            }
        });
});

router.post('/customerProfile/getCustomerProfilesByTemplateName', common.isLoggedInUserOrMobile, function (req, res) {
    var userProfile = common.getUserFromSession(req);
    var objCompanyId = common.castToObjectId( userProfile.companyId);
    var customerProfileTemplateType = req.body.referencedTemplateType;
    var customerProfileTemplateName = req.body.referencedTemplateName;

    console.log("calling getCustomerProfilesByTemplateName with ", objCompanyId,
        customerProfileTemplateType,
        customerProfileTemplateName)
    customerProfilesManagementClassObj.getCustomerProfilesByTemplateName(objCompanyId,
                                                                            customerProfileTemplateType,
                                                                            customerProfileTemplateName,

        function(err,customerProfiles) {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                CustomerProfiles: customerProfiles
            });
        });
});

module.exports = router;