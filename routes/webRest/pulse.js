/**
 * Created by naveen on 1/27/16.
 */

var express = require('express');
var router = express.Router();
var moment = require('moment-timezone');
var fs = require('fs');
var jwt = require('jwt-simple');
var _ = require("lodash");

var PipelineMeta = require('../../dataAccess/pipelineMetaManagement');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var interactionManagement = require('../../dataAccess/interactionManagement');
var commonUtility = require('../../common/commonUtility');
var insightsManagement = require('../../dataAccess/insightsManagement');
var ActionItemsManagement = require('../../dataAccess/actionItemsManagement');
var contactClass = require('../../dataAccess/contactsManagementClass');
var googleCalendarAPI = require('../../common/googleCalendar');
var meetingManagement = require('../../dataAccess/meetingManagement');
var errorClass = require('../../errors/errorClass');
var errorMessages = require('../../errors/errorMessage');
var validations = require('../../public/javascripts/validation');
var opportunityManagement = require('../../dataAccess/opportunitiesManagement');
var userOpenSlots = require('../../common/userOpenSlots');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var userRelationCollection = require('../../databaseSchema/userRelatonShipSchema').userRelation;
var OppWeightsClass = require('../../dbCache/company.js')
var OppCommitManagement = require('../../dataAccess/oppCommitAccess');

var validation = new validations();
var googleCalendar = new googleCalendarAPI();
var common = new commonUtility();
var userManagementObj = new userManagement();
var contactObj = new contactClass();
var meetingClassObj = new meetingManagement();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var interactionManagementObj = new interactionManagement();
var insightsManagementObj = new insightsManagement();
var actionItemsManagementObj = new ActionItemsManagement();
var oppManagementObj = new opportunityManagement();
var userOpenSlotsObj = new userOpenSlots();
var company = new companyClass();
var oppWeightsObj = new OppWeightsClass();
var pipelineMetaObj = new PipelineMeta();
var winstonLog = require('../../common/winstonLog');
var logLib = new winstonLog();
var oppCommitObj = new OppCommitManagement();

var statusCodes = errorMessagesObj.getStatusCodes();
var json2csv = require('json2csv');

var redis = require('redis');
var redisClient = redis.createClient();

router.get('/pulse/reports', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/pulse");
});

router.get('/pulse/team/reports', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserId(req.user)

    common.getProfileOrStoreProfileInSession(userId,req,function(user){
        //Built to check only for corporate users. Goal is to redirect based on AppConfig even for paid retail users.
        if(user.corporateUser){
            res.render("pulse/team");
        } else {
            res.render('customer/nonCustomer',{isLoggedIn : common.isLoggedInUserBoolean(req)});
        }
    })
});

router.get('/pulse/teamRank', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/teamRank");
});

router.get('/pulse/teamRankV2', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/teamRankV2");
});

router.get('/pulse/teamAnalysisSideBar', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/teamAnalysisSideBar");
});

router.get('/pulse/achievement', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/achievement");
});

router.get('/pulse/interactionsByAmount', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/interactionsByAmount");
});

router.get('/pulse/avgDealSize', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/avgDealSize");
});

router.get('/pulse/productivityMeasure', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/productivityMeasure");
});

router.get('/pulse/productivityMeasure', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/productivityMeasure");
});

router.get('/pulse/pipelineVelocity_sidebar', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/pipelineVelocity_sidebar");
});

router.get('/pulse/missingTarget', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/missingTarget");
});

router.get('/pulse/pipelineVelocity', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/pipelineVelocity");
});

router.get('/pulse/prediction', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/prediction");
});

router.get('/pulse/globalMap', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/globalMap");
});

router.get('/pulse/summary', common.isLoggedInUserOrMobile, function(req, res) {
    res.render("pulse/summary");
});

router.get('/pulse/interactions/per/deal', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserId(req.user)
    var users = [common.castToObjectId(userId)]

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {
        
        getInteractionsPerClosedDeal(users,allQuarters,fyRange,function (err,opps) {

            if(!err && opps){

                res.send({
                    Data:opps
                });
            } else {
                common.sendErrorResponse(res,[],"No opp open")
            }
        })
    });

});

// router.get('/account/relationship/relevance', common.isLoggedInUserOrMobile, function(req, res){
//     var userId = common.getUserId(req.user)
//     var userIds = [userId];
//     var account = req.query.account;
//
//     common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters,timezone) {
//
//         var fromDate = fyRange.start
//         var toDate = fyRange.end
//
//         interactionsByAccountRelevance(common.castListToObjectIds(userIds),fromDate,toDate,account,timezone,function (err,data) {
//             res.send(data);
//         })
//     })
// });

router.get('/pulse/overlay/module', common.isLoggedInUserOrMobile, function(req, res){
    res.render("pulse/weeklyReviewModule");
})

router.get('/pulse/user/:moduleType', common.isLoggedInUserOrMobile, function(req, res){

    if(req.params.moduleType == "achievement"){
        res.render("pulse/userAchievement");
    }
    if(req.params.moduleType == "pipelineReview"){
        res.render("pulse/userPipelineReview");
    }
    if(req.params.moduleType == "pipelineComparison"){
        res.render("pulse/userPipelineComparison");
    }
    if(req.params.moduleType == "discussionPoints"){
        res.render("pulse/userDiscussionPoints");
    }
    if(req.params.moduleType == "reports"){
        res.render("pulse/userReports");
    }
    if(req.params.moduleType == "oppsTable"){
        res.render("pulse/oppsTable");
    }
    if(req.params.moduleType == "commits"){
        res.render("pulse/teamCommit");
    }
})

router.get('/pulse/team/analysis', common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserId(req.user)
    var users = [userId]
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile ? userProfile.companyId : null;

    if(req.query.hierarchylist){
        users = req.query.hierarchylist.split(',')
    }

    var usersObjList = common.castListToObjectIds(users)

    var forQuarter = null,
        netGrossMargin = false;

    if(req.query.quarter){
        forQuarter = req.query.quarter;
    }

    if(req.query.netGrossMargin && req.query.netGrossMargin == 'true'){
        netGrossMargin = true;
    }

    var regionAccess = null,productAccess = null,verticalAccess = null;

    if(req.query.filterType == "exceptionalAccess"){

        if(req.query.regionAccess && req.query.regionAccess != 'null'){
            regionAccess = req.query.regionAccess.split(',');
        }

        if(req.query.productAccess && req.query.productAccess != 'null'){
            productAccess = req.query.productAccess.split(',');
        }

        if(req.query.verticalAccess && req.query.verticalAccess != 'null'){
            verticalAccess = req.query.verticalAccess.split(',');
        }
    }

    var domain = req.query.domain;

    var filter = {};

    filter = {
        location:req.query.location?req.query.location:null,
        product:req.query.product?req.query.product:null,
        source:req.query.source?req.query.source:null,
        vertical:req.query.vertical?req.query.vertical:null
    }

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters,timezone) {

        var fromDate = allQuarters[allQuarters.currentQuarter].start,toDate = allQuarters[allQuarters.currentQuarter].end;
        var prevFyStart = fyRange.start,
            prevFyEnd = fyRange.end
        if(forQuarter && forQuarter !== "PrevFY"){
            fromDate = allQuarters[forQuarter].start;
            toDate = allQuarters[forQuarter].end;
        } else if(forQuarter === "PrevFY"){
            fromDate = new Date(moment(fyRange.start).subtract(1,"year"));
            toDate = new Date(moment(fyRange.end).subtract(1,"year"));

            prevFyStart = fromDate;
            prevFyEnd = toDate
        }

        oppWeightsObj.getOppStages(common.castToObjectId(companyId), function (oppStages, companyDetails) {

            var currenciesObj = {};
            var primaryCurrency = "USD";

            if(companyDetails){
                _.each(companyDetails.currency,function (co) {
                    currenciesObj[co.symbol] = co;
                    if(co.isPrimary){
                        primaryCurrency = co.symbol;
                    }
                })
            }

            getTargets(usersObjList,fromDate,toDate,prevFyStart,prevFyEnd,function (err,targets) {
                getOpportunitiesForQuarter(usersObjList,fromDate,toDate,null,filter,regionAccess,productAccess,verticalAccess,common.castToObjectId(companyId),function (err,opps) {
                    getOppCreated(usersObjList,fromDate,toDate,netGrossMargin,filter,regionAccess,productAccess,verticalAccess,common.castToObjectId(companyId),function (err,oppsCreatedObj) {
                        var liuDomain = common.fetchCompanyFromEmail(domain);
                        getInteractionCountWithAllContacts(usersObjList,fromDate,toDate,liuDomain,function (err,interactions) {
                            getOneWayInteractions(usersObjList,fromDate,toDate,liuDomain,function (err,oneWayInteractions) {
                                var data = getOppData(opps,oppsCreatedObj,toDate,primaryCurrency,currenciesObj);

                                getAllContactsInOpp(usersObjList,function (err,contacts,userContactObj) {
                                    var contactsInOpp = [];
                                    _.each(contacts,function (c) {
                                        contactsInOpp.push(_.flattenDeep(c.contacts))
                                    });

                                    interactionCountForQuarterForOppRelevantContacts(usersObjList,_.uniq(_.flatten(contactsInOpp)),fromDate,toDate,liuDomain,function (intErr,intOppContacts) {
                                        oneWayInteractionsForOppContacts(usersObjList,_.uniq(_.flatten(contactsInOpp)),fromDate,toDate,liuDomain,function (intErr,oneWayIntOppContacts) {
                                            interactionsByContactRelevance(usersObjList,fromDate,toDate,timezone,function (err,contactRelevance) {
                                                var exists = _.map(data,function (el) {
                                                    if(el.userId){
                                                        return el.userId.toString()
                                                    }
                                                });

                                                var notExists = _.difference(users,exists)

                                                var completeData = fixNonExistingUsersData(data,notExists);

                                                var interactionsObj = {};
                                                _.each(interactions,function (el) {
                                                    interactionsObj[el._id.userId] = el
                                                });

                                                var oneWayInteractionsObj = {};
                                                _.each(oneWayInteractions,function (el) {
                                                    oneWayInteractionsObj[el._id.userId] = el
                                                });

                                                _.each(completeData,function (el) {
                                                    if(interactionsObj[el.userId]){
                                                        el.allInteractionsCount = interactionsObj[el.userId].count
                                                    } else {
                                                        el.allInteractionsCount = 0
                                                    }

                                                    if(oneWayInteractionsObj[el.userId]){
                                                        el.oneWayInteractionCount = oneWayInteractionsObj[el.userId].count
                                                    } else {
                                                        el.oneWayInteractionCount = 0
                                                    }
                                                })

                                                attachTargets(targets,completeData);
                                                attachInteractionsByContactRelation(userContactObj,intOppContacts,completeData)
                                                attachOneWayInteractionsByContactRelation(userContactObj,oneWayIntOppContacts,completeData)
                                                attachRelationRelevance(contactRelevance,completeData)

                                                if(req.query.filterType == "exceptionalAccess"){

                                                    combineData(users,completeData);

                                                    res.send({allQuarters:{start:fromDate,end:toDate},Data:completeData,opps:opps})
                                                } else {
                                                    res.send({allQuarters:{start:fromDate,end:toDate},Data:completeData,opps:opps})
                                                }
                                            });
                                        });

                                    });
                                })
                            })
                        });
                    })
                })
            });
        });
    });

});

router.get('/pulse/forecast', common.isLoggedInUserOrMobile, function(req, res){

    var userId = common.getUserId(req.user)
    var users = [userId]
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile ? userProfile.companyId : null;

    if(req.query.hierarchylist){
        users = req.query.hierarchylist.split(',')
    }

    var usersObjList = common.castListToObjectIds(users);
    oppWeightsObj.getOppStages(common.castToObjectId(companyId), function (oppStages, companyDetails) {
        common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {
            var startDate = allQuarters[allQuarters.currentQuarter].start;
            var endDate = allQuarters[allQuarters.currentQuarter].end;

            var fyStart = fyRange.start,
                fyEnd = fyRange.end;

            if(req.query.quarter == "next"){
                startDate = moment(startDate).add(3,"months");
                endDate = moment(endDate).add(3,"months");
            }

            if(req.query.quarter == "qc-1"){
                startDate = moment(startDate).subtract(3,"months");
                endDate = moment(endDate).subtract(3,"months");
            }

            if(req.query.quarter == "qc-2"){
                startDate = moment(startDate).subtract(6,"months");
                endDate = moment(endDate).subtract(6,"months");
            }

            if(new Date(startDate)< new Date(fyStart) && new Date(endDate)< new Date(fyEnd)){
                fyStart = moment(fyStart).subtract(1,"year")
                fyEnd = moment(fyEnd).subtract(1,"year")
            }

            if(new Date(startDate)>new Date(fyStart) && new Date(endDate)>new Date(fyEnd)){
                fyStart = moment(fyStart).add(1,"year")
                fyEnd = moment(fyEnd).add(1,"year")
            }

            var currenciesObj = {};
            var primaryCurrency = "USD"

            if(companyDetails){
                _.each(companyDetails.currency,function (co) {
                    currenciesObj[co.symbol] = co;
                    if(co.isPrimary){
                        primaryCurrency = co.symbol;
                    }
                })
            }

            getTargets(usersObjList,startDate,endDate,fyStart,fyEnd,function (err,targets) {

                if(targets && targets.length>3){

                    var flattenTargets = [];
                    _.each(targets,function (el) {
                        flattenTargets = flattenTargets.concat(el.targets);
                    });

                    var result = _(flattenTargets)
                        .groupBy('monthYear') // create groups of employees with the same name
                        .map(function(g,key) {
                            var obj = {
                                monthYear:key,
                                target: _.sum(g,"target"),
                                date:g[0].date
                            };

                            return obj;
                        })
                        .value();

                    result = [{targets:result}]

                } else {
                    result = targets;
                }

                if(req.query.quarter == "next"){

                    oppCommitObj.findLastUpdatedSnapShot(usersObjList,startDate,endDate,function (err,commits) {
                        oppManagementObj.getOppGroupDayMonthYear(usersObjList,startDate,endDate,common.castToObjectId(companyId),"open",function (err,opps) {

                            pipelineMetaObj.formatOpps(opps,startDate,endDate,result,commits,currenciesObj,primaryCurrency,true,function (nextQtrData) {
                                res.send({
                                    qtr:moment(startDate).format("MMM YYYY") +"-"+moment(endDate).format("MMM YYYY"),
                                    data:nextQtrData
                                });
                            })
                        })
                    })
                } else {

                    oppCommitObj.findLastUpdatedSnapShot(usersObjList,startDate,endDate,function (err,commits) {
                        oppManagementObj.getOppGroupDayMonthYear(usersObjList,startDate,endDate,common.castToObjectId(companyId),"close",function (err,opps) {

                            pipelineMetaObj.findPipelineDashboardColl(usersObjList,startDate,endDate,result,opps,commits,currenciesObj,primaryCurrency,false,function (err,pipelineFlow) {
                                pipelineMetaObj.findPipelineMetaByDateRange(usersObjList,startDate,endDate,result,opps,commits,currenciesObj,primaryCurrency,function (err,data) {

                                    res.send({
                                        opps:opps,
                                        pipelineFlow:pipelineFlow,
                                        commitsSum:_.sum(commits,"commitValue"),
                                        grpCommit:grpCommit(commits),
                                        commits:commits,
                                        qtr:moment(startDate).format("MMM YYYY") +"-"+moment(endDate).format("MMM YYYY"),
                                        // data:data
                                        data:pipelineFlow
                                    });
                                });
                            });
                        })
                    })
                }
            });
        });
    });

});

function grpCommit(commits){
    return _(commits)
        .groupBy('_id.userId')
        .map(function(g,key) {

            return {
                userId:key,
                val:_.sum(g,"commitValue")
            }
        })
        .value();
}

router.get('/pulse/forecast/for/quarter', common.isLoggedInUserOrMobile, function(req, res) {

    var userId = common.getUserId(req.user)
    var users = [userId]
    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile ? userProfile.companyId : null;

    if (req.query.hierarchylist) {
        users = req.query.hierarchylist.split(',')
    }

    var usersObjList = common.castListToObjectIds(users);

    common.userFiscalYear(userId, function (err, fyMonth, fyRange, allQuarters) {
        var startDate = allQuarters[allQuarters.currentQuarter].start;
        var endDate = allQuarters[allQuarters.currentQuarter].end;

        if (req.query.quarter == "next") {
            startDate = moment(startDate).add(3, "months");
            endDate = moment(endDate).add(3, "months");
        }

        if (req.query.quarter == "qc-1") {
            startDate = moment(startDate).subtract(3, "months");
            endDate = moment(endDate).subtract(3, "months");
        }

        if (req.query.quarter == "qc-2") {
            startDate = moment(startDate).subtract(6, "months");
            endDate = moment(endDate).subtract(6, "months");
        }

        if(req.query.quarter == "next"){

            oppManagementObj.getOppGroupDayMonthYear(usersObjList,startDate,endDate,common.castToObjectId(companyId),"open",function (err,opps) {
                var results = [];

                if(!err && opps && opps.length>0){
                    _.each(opps,function (el) {
                        results = results.concat(el.opps)
                    })
                }

                res.send(results);
            })

        } else {

            pipelineMetaObj.findPipelineDashboardColl(usersObjList,startDate,endDate,[],
                [],
                [],
                null,null,true,function (err,pipeline) {

                    oppManagementObj.getOppsByIds(common.castToObjectId(companyId),_.pluck(pipeline,"opportunityId"),startDate,endDate,function (err,opps) {
                        oppManagementObj.getWonOpp(usersObjList,startDate,endDate,common.castToObjectId(companyId),function (err,won_opps) {
                            var oppsObj = {}
                            if(opps && opps.length>0){
                                _.each(opps,function (op) {
                                    oppsObj[op.opportunityId] = op;
                                });
                            }

                            var result = opps.concat(won_opps);

                            result.forEach(function (op) {
                                op._id = String(op._id);
                            });

                            res.send(_.uniq(result,"_id"));
                        });
                    });
                })
        }

    })
})

function combineData(users,completeData){

    var cumulativeDataExceptSelectedUser = {
        "userId": "cumulativeData",
        "totalDeals": 0,
        "wonDeals": 0,
        "totalOppAmount": 0,
        "wonAmount": 0,
        "lostAmount": 0,
        "interactionsCountWon": 0,
        "interactionsCountAll": 0,
        "daysToLostCloseDeal": 0,
        "daysToWinCloseDeal": 0,
        "allInteractionsCount": 0,
        "oneWayInteractionCount": 0,
        "targetsCount": 0,
        "interactionsOppContactsForQtr": 0,
        "oneWayInteractionsOppContactsForQtr": 0,
        "contactRelevance": {
            "importantContacts": 0,
            "importantInteractions": 0,
            "partnersContacts": 0,
            "partnersInteractions": 0,
            "decisionMakersContacts": 0,
            "decisionMakersInteractions": 0,
            "influencersContacts": 0,
            "influencersInteractions": 0,
            "othersContacts": 0,
            "othersInteractions": 0
        }
    };
    
    _.each(completeData,function (el) {
        if(!_.includes(users,el.userId.toString())){


            for (var key in el) {
                if(!isNaN(el[key])){
                    cumulativeDataExceptSelectedUser[key] = cumulativeDataExceptSelectedUser[key]+el[key]
                }
            }
        }
    })

    completeData.push(cumulativeDataExceptSelectedUser);

}

function attachRelationRelevance(contactRelevance,completeData){

    var userObj = {}
    _.each(contactRelevance,function (data) {
        userObj[data.userId] = data;
    });

    _.each(completeData,function (el) {
        if(userObj[el.userId]){
            el.contactRelevance = userObj[el.userId]
        }
    })
}

function interactionsByContactRelevance(users,fromDate,toDate,timezone,callback){

    var monthYears = enumerateDaysBetweenDates(fromDate,toDate,timezone);

    userRelationCollection.aggregate([
        {
            $match:{
                userId:{$in:users},
                monthYear:{$in:monthYears}
            }
        },
        {
            $group:{
                _id:"$userId",
                data:{
                    $push:{
                        important:"$important",
                        partners:"$partners",
                        decisionMakers:"$decisionMakers",
                        influencers:"$influencers",
                        others:"$others"
                    }
                }
            }
        }
    ],function (err,result) {

        if(!err && result && result.length>0){
            var groupData = [];
            _.each(result,function (res) {
                var importantContacts = 0,partnersContacts = 0,decisionMakersContacts = 0,influencersContacts = 0,othersContacts = 0;
                var importantInteractions = 0,partnersInteractions = 0,decisionMakersInteractions = 0,influencersInteractions = 0,othersInteractions = 0;

                _.each(res.data,function (el) {

                    importantContacts = importantContacts+el.important.contacts
                    importantInteractions = importantInteractions+el.important.interactionCount

                    partnersContacts = partnersContacts+el.partners.contacts
                    partnersInteractions = partnersInteractions+el.partners.interactionCount

                    decisionMakersContacts = decisionMakersContacts+el.decisionMakers.contacts
                    decisionMakersInteractions = decisionMakersInteractions+el.decisionMakers.interactionCount

                    influencersContacts = influencersContacts+el.influencers.contacts
                    influencersInteractions = influencersInteractions+el.influencers.interactionCount

                    othersContacts = othersContacts+el.others.contacts
                    othersInteractions = othersInteractions+el.others.interactionCount
                })

                groupData.push({
                    userId:res._id,
                    importantContacts:importantContacts,
                    importantInteractions:importantInteractions,
                    partnersContacts:partnersContacts,
                    partnersInteractions:partnersInteractions,
                    decisionMakersContacts:decisionMakersContacts,
                    decisionMakersInteractions:decisionMakersInteractions,
                    influencersContacts:influencersContacts,
                    influencersInteractions:influencersInteractions,
                    othersContacts:othersContacts,
                    othersInteractions:othersInteractions
                })
            })

            callback(err,groupData)

        } else {
            callback(err,[])
        }
    });
}

function interactionsByAccountRelevance(users,fromDate,toDate,account,timezone,callback){

    var monthYears = enumerateDaysBetweenDates(fromDate,toDate,timezone);

    userRelationCollection.aggregate([
        {
            $match:{
                userId:{$in:users},
                monthYear:{$in:monthYears}
            }
        }
    ],function (err,result) {

        if(!err && result && result.length>0){
            var groupData = [];
            _.each(result,function (res) {
                var importantContacts = 0,partnersContacts = 0,decisionMakersContacts = 0,influencersContacts = 0,othersContacts = 0;
                var importantInteractions = 0,partnersInteractions = 0,decisionMakersInteractions = 0,influencersInteractions = 0,othersInteractions = 0;

                _.each(res.data,function (el) {

                    importantContacts = importantContacts+el.important.contacts
                    importantInteractions = importantInteractions+el.important.interactionCount

                    partnersContacts = partnersContacts+el.partners.contacts
                    partnersInteractions = partnersInteractions+el.partners.interactionCount

                    decisionMakersContacts = decisionMakersContacts+el.decisionMakers.contacts
                    decisionMakersInteractions = decisionMakersInteractions+el.decisionMakers.interactionCount

                    influencersContacts = influencersContacts+el.influencers.contacts
                    influencersInteractions = influencersInteractions+el.influencers.interactionCount

                    othersContacts = othersContacts+el.others.contacts
                    othersInteractions = othersInteractions+el.others.interactionCount
                })

                groupData.push({
                    userId:res._id,
                    importantContacts:importantContacts,
                    importantInteractions:importantInteractions,
                    partnersContacts:partnersContacts,
                    partnersInteractions:partnersInteractions,
                    decisionMakersContacts:decisionMakersContacts,
                    decisionMakersInteractions:decisionMakersInteractions,
                    influencersContacts:influencersContacts,
                    influencersInteractions:influencersInteractions,
                    othersContacts:othersContacts,
                    othersInteractions:othersInteractions
                })
            })

            callback(err,groupData)

        } else {
            callback(err,[])
        }
    });
}

function interactionCountForQuarterForOppRelevantContacts(users,contacts,fromDate,toDate,liuDomain,callback) {

    interactionManagementObj.interactionsByUsersAndContacts(users,contacts,fromDate,toDate,liuDomain,function (err,interactions) {
        callback(err,interactions)
    })
}

function oneWayInteractionsForOppContacts(users,contacts,fromDate,toDate,liuDomain,callback) {

    interactionManagementObj.oneWayInteractionsByUsersAndContacts(users,contacts,fromDate,toDate,liuDomain,function (err,interactions) {
        callback(err,interactions)
    })
}

function attachInteractionsByContactRelation(userContactObj,intOppContacts,data) {

    var intOppContactsObj = {};
    _.each(intOppContacts,function (el) {
        intOppContactsObj[el._id.userId+el._id.emailId] = el.count
    })

    _.each(data,function (el) {

        el.interactionsOppContactsForQtr = 0;
        if(userContactObj[el.userId] && userContactObj[el.userId].length>0){
            _.each(userContactObj[el.userId],function (c) {
                var index = el.userId+c;
                if(intOppContactsObj[index]){
                    el.interactionsOppContactsForQtr = el.interactionsOppContactsForQtr+intOppContactsObj[index]
                }
            })
        }

    })
}

function attachOneWayInteractionsByContactRelation(userContactObj,intOppContacts,data) {

    var intOppContactsObj = {};
    _.each(intOppContacts,function (el) {
        intOppContactsObj[el._id.userId+el._id.emailId] = el.count
    })

    _.each(data,function (el) {

        el.oneWayInteractionsOppContactsForQtr = 0;
        if(userContactObj[el.userId] && userContactObj[el.userId].length>0){
            _.each(userContactObj[el.userId],function (c) {
                var index = el.userId+c;
                if(intOppContactsObj[index]){
                    el.oneWayInteractionsOppContactsForQtr = el.oneWayInteractionsOppContactsForQtr+intOppContactsObj[index]
                }
            })
        }

    })
}

function attachTargets(targets,data) {

    var trObj = {}
    _.each(targets,function (tr) {
        trObj[tr._id] = tr.targets;
    });

    _.each(data,function (el) {
        var targetsCount = 0;
        var targets = [];
        _.each(trObj[el.userId],function (tr) {
            tr.amount = tr.target
            targets.push(tr);
            targetsCount = targetsCount+tr.target
        })

        el.targetsCount = targetsCount;
        el.targets = targets;
    })
}

function fixNonExistingUsersData(data,notExists) {

    if(data.length>0){
        _.each(notExists,function (el) {
            data.push({
                "userId": el,
                "targetsCount":0,
                "totalOppAmount":0,
                "wonAmount":0,
                "lostAmount":0,
                "interactionsCountWon":0,
                "interactionsCountAll":0,
                "influencersInteractionsCount":0,
                "dmsInteractionsCount":0,
                "partnersInteractionsCount":0,
                "ownersInteractionsCount":0

            })
        })
    } else {
        _.each(notExists,function (el) {
            data.push({
                "userId": el,
                "targetsCount":0,
                "totalOppAmount":0,
                "wonAmount":0,
                "lostAmount":0,
                "interactionsCountWon":0,
                "interactionsCountAll":0,
                "influencersInteractionsCount":0,
                "dmsInteractionsCount":0,
                "partnersInteractionsCount":0,
                "ownersInteractionsCount":0
            })
        })
    }

    return data
}

function getInteractionCountWithAllContacts(users,fromDate,toDate,liuDomain,callback) {

    interactionManagementObj.getInteractionsForUsersByDateRange(users,fromDate,toDate,liuDomain,function (err,interactions) {
        callback(err,interactions)
    })
}

function getOneWayInteractions(users,fromDate,toDate,liuDomain,callback) {
    interactionManagementObj.getOneWayInteractionsForUsersByDateRange(users,fromDate,toDate,liuDomain,function (err,interactions) {
        callback(err,interactions)
    })
}

function getOpportunitiesForQuarter(users,fromDate,toDate,netGrossMargin,filter,regionAccess,productAccess,verticalAccess,companyId,callback) {

    oppManagementObj.opportunitiesForUsersByDateWithLessData(users,fromDate,toDate,null,regionAccess,productAccess,verticalAccess,netGrossMargin,filter,companyId,function (err,opps) {
        callback(err,opps)
    });
}

function getOppCreated(users,fromDate,toDate,netGrossMargin,filter,regionAccess,productAccess,verticalAccess,companyId,callback) {

    oppManagementObj.oppCreatedByMonth(users,new Date(fromDate),regionAccess,productAccess,verticalAccess,companyId,function (err,opps) {
        if(!err && opps && opps.length>0){

            var obj = {};
            _.each(opps,function (op) {

                if (!op.createdDate) {
                    op.createdDate = op._id.getTimestamp()
                }

                if(!obj[String(op.userId)]){
                    obj[String(op.userId)] = []
                    obj[String(op.userId)].push(op)
                } else {
                    obj[String(op.userId)].push(op)
                }
            });

            callback(err,obj)
        } else {
            callback(err,null)
        }
    })
}

function getAllContactsInOpp(users,callback) {
    oppManagementObj.getAllContactsinOpp(users,function (err,contacts) {

        var userContactObj = {};

        _.each(contacts,function (co) {
            userContactObj[co._id.toString()] = _.unique(_.flattenDeep(co.contacts))
        })

        callback(err,contacts,userContactObj)
    })
}

function getOppData(opps,oppsCreatedObj,toDate,primaryCurrency,currenciesObj){
    var data = [];

    _.each(opps,function (opp) {
        var oppCreated = oppsCreatedObj?oppsCreatedObj[String(opp._id)]:null
        data.push(oppGroupByStage(opp.opportunities,oppCreated,toDate,primaryCurrency,currenciesObj))
    });

    return data;

}

function oppGroupByStage(opps,oppsCreatedObj,toDate,primaryCurrency,currenciesObj) {

    var interactionsCountAll=0,
        interactionsCountWon=0,
        interactionsCountLost=0,
        daysToWinCloseDeal=0,
        daysToLostCloseDeal=0,
        lostAmount=0,
        wonAmount=0,
        totalOppAmount=0,
        totalDeals=0,
        wonDeals=0,
        userId = "",
        allContacts = [],
        influencers = [],
        dms = [],
        partners = [],
        owners = [],
        companies = [],
        products = [],
        verticals = [],
        regions = [],
        monthlyCreated = [],
        monthlyClosed = [],
        monthlyOppWon = [],
        userEmailId = "";

    var won = ['Close Won','Closed Won']
    var lost = ['Close Lost','Closed Lost']

    if(oppsCreatedObj){

        _.each(oppsCreatedObj,function (op) {
            if(!op.createdDate){
                op.createdDate = op._id.getTimestamp();
            }

            if(new Date(op.createdDate) <= new Date(toDate)){
                monthlyCreated.push(op.createdDate)
            }

        });
    }

    _.each(opps,function (op) {

        if(!op.createdDate){
            op.createdDate = op._id.getTimestamp();
        }

        op.amountWithNgm = op.amount;

        if(op.netGrossMargin){
            op.amountWithNgm = (op.amount*op.netGrossMargin)/100
        }

        op.convertedAmt = op.amount;
        op.convertedAmtWithNgm = op.amountWithNgm

        if(op.currency && op.currency !== primaryCurrency){

            if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                op.convertedAmt = op.amount/currenciesObj[op.currency].xr
            }

            if(op.netGrossMargin){
                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
            }

            op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

        }

        allContacts.push(op.contactEmailId)
        owners.push(op.contactEmailId)

        totalDeals++;
        userId = op.userId;
        userEmailId = op.userEmailId;

        var a = moment(op.createdDate);
        var b = moment(op.closeDate);

        if(_.contains(won,op.relatasStage)){

            monthlyOppWon.push({
                date:op.closeDate,
                amount:op.convertedAmtWithNgm
            })

            monthlyClosed.push(op.closeDate)
            companies.push({
                name:common.fetchCompanyFromEmail(op.contactEmailId),
                amount:op.convertedAmtWithNgm
            });

            regions.push({
                name:op.geoLocation.zone,
                amount:op.convertedAmtWithNgm
            });

            products.push({
                name:op.productType,
                amount:op.convertedAmtWithNgm
            });

            verticals.push({
                name:op.vertical,
                amount:op.convertedAmtWithNgm
            });

            wonDeals++;
            wonAmount = wonAmount+op.convertedAmtWithNgm;
            if(op.interactionCount){
                interactionsCountWon = interactionsCountWon+op.interactionCount
            }

            if(b.diff(a, 'days') === 0){
                daysToWinCloseDeal = daysToWinCloseDeal+1 //Minimum one day taken to close opp
            } else {
                daysToWinCloseDeal = daysToWinCloseDeal+Math.abs(b.diff(a, 'days'));
            }

            if(op.influencers.length>0){
                interactionsCountWon = getInteractionCount(op.influencers,allContacts,influencers)+interactionsCountWon
            }
            if(op.partners.length>0){
                interactionsCountWon = getInteractionCount(op.partners,allContacts,partners)+interactionsCountWon
            }
            if(op.decisionMakers.length>0){
                interactionsCountWon = getInteractionCount(op.decisionMakers,allContacts,dms)+interactionsCountWon
            }

        } else if(_.contains(lost,op.relatasStage)){

            if(op.interactionCount){
                interactionsCountLost = interactionsCountLost+op.interactionCount
            }

            daysToLostCloseDeal = daysToLostCloseDeal+Math.abs(b.diff(a, 'days'));

            if(op.influencers.length>0){
                interactionsCountLost = getInteractionCount(op.influencers,allContacts,influencers)+interactionsCountLost
            }
            if(op.partners.length>0){
                interactionsCountLost = getInteractionCount(op.partners,allContacts,partners)+interactionsCountLost
            }
            if(op.decisionMakers.length>0){
                interactionsCountLost = getInteractionCount(op.decisionMakers,allContacts,dms)+interactionsCountLost
            }

            lostAmount = lostAmount+op.convertedAmtWithNgm;
        }

        interactionsCountAll = interactionsCountLost+interactionsCountWon;
        totalOppAmount = totalOppAmount+op.convertedAmtWithNgm;

    });

    return {
        userEmailId:userEmailId,
        userId:userId,
        totalDeals:totalDeals,
        wonDeals:wonDeals,
        totalOppAmount:totalOppAmount,
        wonAmount:wonAmount,
        lostAmount:lostAmount,
        interactionsCountWon:interactionsCountWon,
        interactionsCountAll:interactionsCountAll,
        daysToLostCloseDeal:daysToLostCloseDeal,
        daysToWinCloseDeal:daysToWinCloseDeal,
        allContacts:_.uniq(allContacts),
        companies: companies,
        products: products,
        verticals: verticals,
        regions: regions,
        monthlyCreated: monthlyCreated,
        monthlyClosed: monthlyClosed,
        monthlyOppWon: monthlyOppWon,
        contactsByRelation: {
            influencers:_.uniq(influencers),
            dms:_.uniq(dms),
            partners:_.uniq(partners),
            owners:_.uniq(owners)
        }
    }

}

function getTargets(userIds,fromDate,toDate,prevFyStart,prevFyEnd,callback) {

    oppManagementObj.getTargetsByUsers(userIds,fromDate,toDate,prevFyStart,prevFyEnd,function (err,users) {

        var datesInBetween = common.getMonthsBetweenDates(fromDate,toDate);

        if(!err && users){

            _.each(users,function (user) {
                var monthsSet = _.pluck(user.targets,"monthYear");
                var notSet = _.difference(datesInBetween,monthsSet)
                _.each(notSet,function (el) {

                    user.targets.push({
                        monthYear:el,
                        "target": 0,
                        "date": new Date(moment(new Date(el)).add(1,'d')) //compensate for timezones
                    });
                });
            });

            callback(err,users)

        } else {
            callback(err,null)
        }
    });
}

function getInteractionsPerClosedDeal(users,allQuarters,fyRange,callback) {

    oppManagementObj.getInteractionCountForOpp(users,fyRange.start,fyRange.end,function (err,opps) {

        if(!err && opps && opps.length>0){

            var result = [],group = {
                quarter1:{
                    totalInteractions:0,
                    deals:[],
                    range:""
                },
                quarter2:{
                    totalInteractions:0,
                    deals:[],
                    range:""
                },
                quarter3:{
                    totalInteractions:0,
                    deals:[],
                    range:""
                },
                quarter4:{
                    totalInteractions:0,
                    deals:[],
                    range:""
                }
            };
            _.each(opps,function (op) {
                var totalInteractions = 0;
                if(op.interactionCount){
                    totalInteractions = op.interactionCount+totalInteractions
                }

                if(op.influencers.length>0){
                    totalInteractions = getInteractionCount(op.influencers)+totalInteractions
                }
                if(op.partners.length>0){
                    totalInteractions = getInteractionCount(op.partners)+totalInteractions
                }
                if(op.decisionMakers.length>0){
                    totalInteractions = getInteractionCount(op.decisionMakers)+totalInteractions
                }

                for(var key in allQuarters){

                    if(new Date(op.closeDate) >= new Date(allQuarters[key].start) && new Date(op.closeDate) <= new Date(allQuarters[key].end)){
                        op.thisQuarter = {quarter:key,range:allQuarters[key]}
                    }
                    
                }

                var obj = {
                    totalInteractions: totalInteractions,
                    userEmailId: op.userEmailId,
                    closeDate: op.closeDate,
                    opportunityName: op.opportunityName
                }

                var quarter = op.thisQuarter.quarter

                if(group[quarter]){
                    group[quarter].deals.push(obj);
                    group[quarter].range = op.thisQuarter.range;
                    group[quarter].totalInteractions = group[quarter].totalInteractions+totalInteractions;
                }

            });

            // callback(err,group);
            callback(err,opps);

        } else {
            callback(err,[])
        }
    });
}

function getInteractionCount(data,allContacts,forRelations) {

    var counter = 0;

    _.each(data,function (el) {
        if(allContacts){
            allContacts.push(el.emailId)
        }

        if(forRelations){
            forRelations.push(el.emailId)
        }

        if(el.interactionCount){
            counter = el.interactionCount+counter
        }
    });

    return counter;
}

function getCsv(opps,interactions,intOppContacts,oneWayIntOppContacts,callback){

    var fields = ['userEmailId','contactEmailId','amount','createdDate','closeDate','relatasStage','interactionCount','partners','decisionMakers','influencers'];
    var fields2 = ["userEmailId","count"]
    var fields3 = ["userEmailId","contact","count"]

    var allOpp = [];
    _.each(opps,function (op) {
        _.each(op.opportunities,function (el) {

            if(!el.createdDate){
                el.createdDate = el._id.getTimestamp()
            }

            allOpp.push(el)
        });
    });

    var allInt = [];
    _.each(interactions,function (el) {
        allInt.push({
            userEmailId:el._id.userEmailId,
            count:el.count
        })
    })

    var allIntForRC = [];
    _.each(intOppContacts,function (el) {

        allIntForRC.push({
            userEmailId:el._id.userEmailId,
            contact:el._id.emailId,
            count:el.count
        })

    });

    var oneWay = [];
    _.each(oneWayIntOppContacts,function (el) {

        oneWay.push({
            userEmailId:el._id.userEmailId,
            contact:el._id.emailId,
            count:el.count
        })

    });

    var csv = json2csv({ data: allOpp, fields: fields });
    var csv2 = json2csv({ data: allInt, fields: fields2 });
    var csv3 = json2csv({ data: allIntForRC, fields: fields3 });
    var csv4 = json2csv({ data: oneWay, fields: fields3 });

    fs.writeFile('opp.csv', csv, function(err,file) {
        if (err) throw err;

        callback(csv)
    });

    fs.writeFile('allContactsInt.csv', csv2, function(err) {
        if (err) throw err;
    });

    fs.writeFile('oppContactsInt.csv', csv3, function(err) {
        if (err) throw err;
    });
    
    fs.writeFile('oppContactsInt.csv', csv4, function(err) {
        if (err) throw err;
    });
}

function enumerateDaysBetweenDates(startDate,endDate,timezone) {

    var start = moment(startDate).tz(timezone)
    var end = moment(endDate).tz(timezone)

    var now = start, dates = [];

    while (now <= end) {
        var date = now.add(2,'d');
        dates.push(date.format("M")+""+date.format("YYYY"));
        now.add(1,'months');
    }
    return dates;
};

module.exports = router;
