/**
 * Created by naveen on 28/11/16.
 */

var express = require('express');
var router = express.Router();
var _ = require("lodash");

var commonUtility = require('../../common/commonUtility');
var UserManagement = require('../../dataAccess/userManagementDataAccess');
var CompanyManagement = require('../../dataAccess/corporateDataAccess/companyModelClass');

var common = new commonUtility();
var userManagementObj = new UserManagement();
var companyManagementObj = new CompanyManagement();

router.get('/rweblead/fetch/formTemplate', function(req, res) {
    
    var projection = {emailDomains:0};
    
    var decodedC = decodeURIComponent(req.query);
    var companyId = decodedC.companyId
    
    companyManagementObj.findCompanyByIdWithProjection(common.castToObjectId(String(req.query.companyId)),projection,function (companyProfile) {
        
        var name = '<label>Name</label>'+'<input placeholder="Your name" name="leadName" id="leadName">'
        var emailId = '<label>Email ID</label>'+'<input placeholder="Your Email ID" name="leadEmailId" id="leadEmailId">'
        var phone = '<label>Phone</label>'+'<input placeholder="Your phone number" name="leadPhone" id="leadPhone">'
        var message = '<label>Message</label>'+'<textarea placeholder="Please type your comment" name="leadComment" id="leadComment"></textarea>'
        var submitBtn = '<div id="nextDetails" class="btn">SUBMIT</div>'

        var data = {
            header:getHeader(companyProfile && companyProfile.welcomeTitle?companyProfile.welcomeTitle:""),
            fullForm:'<div class="relatas-nav">'+getNavigation()+'</div>'+'<div class="relatas-main"><form class="relatas-form">'+name+emailId+phone+message+submitBtn+'</form></div>',
            onlyMessageAndEmailIdForm:'<div class="relatas-nav">'+getNavigation()+'</div>'+'<div class="relatas-main">'+getFormWithOnlyMessageAndEmailId()+'</div>',
            messages: {
                meetingTitle: companyProfile?companyProfile.meetingTitle:"",
                failureReply: companyProfile?companyProfile.failureReply:"",
                successReply: companyProfile?companyProfile.successReply:"",
                welcomeTitle: companyProfile?companyProfile.welcomeTitle:"",
                mailSubject: companyProfile?companyProfile.mailSubject:""
            },
            colorScheme:{
                majorColor:companyProfile?companyProfile.majorColor:"",
                textColor:companyProfile?companyProfile.textColor:""
            }
        };

        res.jsonp(data)

    })
});

router.get('/rweblead/search/team/mates',common.isLoggedInUserOrMobile, function(req, res) {
    
    var searchFor = req.query.searchFor
    var companyId = req.query.companyId

    userManagementObj.getTeam(common.castToObjectId(companyId),searchFor,function (err,team) {
        if(!err && team.length>0){
            res.send(team)
        } else {
            res.send([])
        }
    })

});

router.get('/rweblead/fetch/support/team', function(req, res) {

    var companyId = req.query.companyId?decodeURIComponent(req.query.companyId):sendError(req,res)
    companyManagementObj.getSupportTeam(common.castToObjectId(companyId),function (err,team) {

        userManagementObj.getUsersWithCustomFields(team,{publicProfileUrl:1,profilePicUrl:1,firstName:1,lastName:1,emailId:1,designation:1,location:1,mobileNumber:1,timezone:1},function (error,userProfiles) {

            if(!err && team.length>0){
                res.send(userProfiles)
            } else {
                res.send(null)
            }

        });
    })
});

function sendError(req,res) {
    res.send({
        ErrorCode:1
    })
}

function getHeader(welcomeMessage) {

    var header = '<div id="relatas-header"><p>'+welcomeMessage+'</p><div id="team-support"></div></div>';

    return header;
}

function getFormWithOnlyMessageAndEmailId() {

    var emailId = '<label>Email ID</label>'+'<input placeholder="Your Email ID" name="leadEmailId" id="leadEmailId">'
    var message = '<div class="relatas-message-wrapper required"><label id="commentLabel">Message</label>'+
        '<textarea placeholder="Please enter your query" name="leadComment" id="leadComment" class="relatas-short-form"></textarea></div>'
    var submitBtn = '<div id="nextDetails" class="btn">SEND</div>'

    return '<form class="relatas-form">'+message+submitBtn+'</form>'
}

function getNavigation() {
    var nav = '<ul>' +
        // '<li class="relatas-contact-card"><i class="fa fa-vcard-o"></i></li>' +
        '<li class="relatas-quick-send" title="Send a quick message"><i class="fa fa-envelope-o"></i></li>' +
        '<li class="relatas-schedule-meeting" title="Schedule a meeting"><i class="fa fa-calendar-plus-o"></i></li>' +
        '</ul>'
    return nav;
}

module.exports = router;