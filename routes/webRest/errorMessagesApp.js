var express = require('express');
var router = express.Router();

var commonUtility = require('../../common/commonUtility');
var Errors = require('../../errors/errorsForApp');
var common = new commonUtility();
var errObj = new Errors();

router.get('/error/messages/:id', common.isLoggedInUserOrMobile, function(req, res){
    var page = req.params.id;

    switch (page){
        case "opportunity": respondToReq(res,errObj.getErrorsForOpportunity());
        break;
        default:
            respondToReq(res,"No error messages defined.",true)
    }
})

function respondToReq(res,messages,noMessages) {
    res.send({
        SuccessCode: noMessages?0:1,
        ErrorCode: noMessages?1:0,
        Data:messages
    })
}


module.exports = router;