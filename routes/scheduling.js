/*

    Relatas New Scheduling Framework
    Rest API for scheduling

*/

var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var moment = require('moment-timezone');
var Twit = require('twit');

var userManagement = require('../dataAccess/userManagementDataAccess');
var emailSender = require('../public/javascripts/emailSender');
var validations = require('../public/javascripts/validation');
var appCredentials = require('../config/relatasConfiguration');
var commonUtility = require('../common/commonUtility');
var winstonLog = require('../common/winstonLog');
var googleCalendarAPI = require('../common/googleCalendar');

var common = new commonUtility();
var validation = new validations();
var emailSenders = new emailSender();
var appCredential = new appCredentials();
var userManagements = new userManagement();
var logLib = new winstonLog();
var googleCalendar = new googleCalendarAPI();

var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var logger =logLib.getWinston();
var linkedinErrorLog = logLib.getWinstonLinkedInError();
var domainName = domain.domainName;
var Linkedin = require('node-linkedin')(authConfig.LINKEDIN_CONSUMER_KEY, authConfig.LINKEDIN_CONSUMER_SECRET, authConfig.LINKEDIN_REDIRECT_URL);

router.get('/testing/schedule/getBusy',function(req,res){

    var userId;
    if(common.checkRequired(req.query) && common.checkRequired(req.query.id)){
        userId = req.query.id
    }
    else{
        userId = common.getUserId(req.user);
    }
    req.session[userId] = null;
    if(common.checkRequired(req.session[userId])){
        res.send(req.session[userId]);
    }else
    if(common.checkRequired(userId)){

        var key = req.query.key;
        userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0,linkedin:0,twitter:0,facebook:0,officeCalendar:0},function(error,user){
            if(common.checkRequired(user)){
                var givenDate = moment();
                var endDate = givenDate.clone();     //min date
                var timeMax = givenDate.clone();     //max date

                endDate.date(endDate.date()-1);
                timeMax.date(timeMax.date()+15);

                endDate.hour(0)
                endDate.minute(0)
                endDate.second(0)
                endDate.millisecond(0)

                timeMax.hour(23)
                timeMax.minute(59)
                timeMax.second(59)
                timeMax.millisecond(59)
                endDate = endDate.format()
                timeMax = timeMax.format()

                googleCalendar.getGoogleCalendarEventsByTimezone(user,req.query.timezone,endDate,timeMax,user.emailId,function(events,timeZoneCalendar){
                    var busySlots = [];

                    if(common.checkRequired(events) && events && events.length > 0){

                        for(var i=0;i<events.length;i++){
                            if(common.checkRequired(events[i])){
                                if(events[i].start.dateTime){
                                    var objG = {
                                        start:events[i].start.dateTime,
                                        end:events[i].end.dateTime
                                    };
                                    busySlots.push(objG);
                                }
                                else if(events[i].start.date){
                                    /*busySlots.push({
                                        start:events[i].start.date,
                                        end:events[i].end.date
                                    });*/
                                }
                            }
                        }
                    }
                    userManagements.newInvitationsByDateTimezone(user._id,endDate,timeMax,function(invitations){
                        if(common.checkRequired(invitations) && invitations.length > 0){
                            for(var m=0;m<invitations.length;m++){
                                for(var t=0;t<invitations[m].scheduleTimeSlots.length;t++){
                                    var obj = {
                                        start: invitations[m].scheduleTimeSlots[t].start.date,
                                        end:invitations[m].scheduleTimeSlots[t].end.date
                                    }
                                    busySlots.push(obj);
                                }
                            }
                        }
                        req.session[user._id] = JSON.stringify(busySlots);
                        res.send(busySlots)
                    })
                });
            }
            else res.send(false)
        })
    }
    else res.send(false)
});

// Router to get common connections
router.get('/linkedin/connections/common',isLoggedIn, function(req,res){

    var publicUserId = req.query.id;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    // This is to get authenticated user profile
    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,linkedin:1,firstName:1,lastName:1},function(error,authenticatedUserProfile){

        if(error){
            res.send({message:'No common connections found'})
        }
        else if(common.checkRequired(authenticatedUserProfile) && common.checkRequired(authenticatedUserProfile.linkedin) && common.checkRequired(authenticatedUserProfile.linkedin.id)){

            // This is to get Public profile
            userManagements.findUserProfileByIdWithCustomFields(publicUserId,{emailId:1,linkedin:1,firstName:1,lastName:1},function(error,publicUserProfile){
                if(error){
                    res.send({message:'No common connections found'})
                }
                else if(common.checkRequired(publicUserProfile) && common.checkRequired(publicUserProfile.linkedin) && common.checkRequired(publicUserProfile.linkedin.id)){
                    getCommonConnections(authenticatedUserProfile,publicUserProfile,req,res);
                }
                else{
                    var name = 'User';
                    if(common.checkRequired(publicUserProfile) && common.checkRequired(publicUserProfile.firstName)){
                        name = publicUserProfile.firstName;
                    }

                    logger.info(name+' has not added his LinkedIn profile on Relatas')
                    res.send({message:name+' has not added his LinkedIn profile on Relatas'})
                }
            });
        }
        else{
            logger.info(authenticatedUserProfile.emailId+' has not added his LinkedIn profile on Relatas');
            res.send({message:'You have not added your LinkedIn profile on Relatas'});
        }
    });
});

// Function to get common connections
function getCommonConnections(authProfile,PublicProfile,req,res){

    var linkedinAuthUser = Linkedin.init(authProfile.linkedin.token);
    linkedinAuthUser.people.id(PublicProfile.linkedin.id, ['id','public-profile-url','first-name', 'last-name','current-share','headline','positions','relation-to-viewer:(related-connections:(id,first-name,last-name,siteStandardProfileRequest,pictureUrl))'], function(err, data) {

        if(common.checkRequired(data)){
            if(data.errorCode == 0 || data.errorCode == '0'){
                linkedinErrorLog.info('error in getting linkedin common connections of '+authProfile.emailId+' error '+JSON.stringify(data));
                res.send({message:'No common connections found'});
            }
            else{
                res.send(data)
            }
        }
    });
}

router.get('/linkedin/profile/info',isLoggedIn,function(req,res){
    var userId = req.query.id;

    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,linkedin:1,firstName:1},function(error,authenticatedUserProfile){
        if (error) {
            linkedinErrorLog.info('error while reading token from DB '+error);
            res.send({message:'error while reading token from DB'})
        }
        else{
            if(common.checkRequired(authenticatedUserProfile)){
                if (common.checkRequired(authenticatedUserProfile.linkedin.token)) {
                    var linkedIn = Linkedin.init(authenticatedUserProfile.linkedin.token);
                    linkedIn.people.me(['first-name','last-name','headline','educations'], function(err, profile) {
                        if(err || !common.checkRequired(profile)){
                            linkedinErrorLog.info('error in getting linkedin profile of 1 '+authenticatedUserProfile.emailId+' error '+JSON.stringify(profile));
                            res.send({message:'Error in getting your linkedin connections'})
                        }
                        else{
                            if(!common.checkRequired(profile.educations)){
                                linkedinErrorLog.info('error in getting linkedin profile of 2 '+authenticatedUserProfile.emailId+' error '+JSON.stringify(profile));
                                res.send({message:'Error in getting your linkedin profile'})

                            }
                            else{
                                res.send(profile);
                            }
                        }
                    });
                }
                else res.send({message:'Linkedin credentials is invalid'})
            }
        }
    })
});

router.get('/twitter/posts/latest', isLoggedIn, function(req,res){

    var publicUserId = req.query.id;

    // This is to get Public profile
    userManagements.findUserProfileByIdWithCustomFields(publicUserId,{emailId:1,twitter:1,firstName:1,lastName:1},function(error,publicUserProfile){

        if (error) {
            res.send({message:'No latest post found on Twitter'})
        }
        else
        if(common.checkRequired(publicUserProfile)){
            if (common.checkRequired(publicUserProfile.twitter.id)) {
                getTwitterInfo(publicUserProfile,req,res);
            }
            else{
                logger.info(publicUserProfile.firstName+' has not added his Twitter profile on Relatas');
                res.send({message:publicUserProfile.firstName+' has not added his Twitter profile on Relatas'})
            }
        }
        else{
            logger.info(publicUserProfile.firstName+' has not added his Twitter profile on Relatas');
            res.send({message:publicUserProfile.firstName+' has not added his Twitter profile on Relatas'})
        }
    });
});

function getTwitterInfo(PublicProfile,req,res){

    var tweets = [];

    try{
        var T = new Twit({
            consumer_key:     authConfig.TWITTER_CONSUMER_KEY,
            consumer_secret:   authConfig.TWITTER_CONSUMER_SECRET,
            access_token:        PublicProfile.twitter.token,
            access_token_secret:  PublicProfile.twitter.refreshToken
        });

        T.get('statuses/user_timeline', function(err, data, response) {

            if (err) {
                res.send({message:'No latest tweet found'})
            }
            else{
                if (!common.checkRequired(data) && !common.checkRequired(data[0])) {
                    res.send({message:'No latest tweet found'});
                }
                else{
                    var status = {
                        id: PublicProfile._id,
                        userId: PublicProfile._id,
                        userEmailId: PublicProfile.emailId,
                        userFirstName: PublicProfile.firstName,
                        twitter:data.length <= 30 ? data : getThirtyTweets(data)
                    };

                    userManagements.storeTwitterStatus(status,function(error,isSuccess,message){
                        if(error){
                            res.send({message:'No latest tweet found'})
                        }
                    });

                    if (data.length <=5) {
                        res.send(data);
                    }
                    else{
                        for(var i=0; i<data.length; i++){
                            tweets.push(data[i]);
                            if (i == 5) {
                                tweets.push(data[i]);
                                res.send(tweets)
                            }
                        }
                    }
                }
            }
        })
    }
    catch(e){
        res.send({message:'No latest tweet found'})
    }
}

function getThirtyTweets(data){
    var tweets =[];
    for(var i=0; i<30; i++){
        if(data[i]){
            tweets.push(data[i])
        }

    }

    return tweets;
}

function isLoggedIn(req, res, next){
    // if user is authenticated in the session, carry on

    if (req.isAuthenticated()){

        return next();
    }else
    {
        res.send(false);
    }
}


module.exports = router;