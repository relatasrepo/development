/*
 * relatas-application framework
 *  
 * exports  [GET : Home - Dashboard page]
 *          [GET : user- profile info]    
 */

// initializing  the required module

var express = require('express');
var router = express.Router();
var passport = require('passport');
var request = require('request');
var gcal = require('google-calendar');
var jwt = require('jwt-simple');
var url = require('url');

var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var userManagement = require('../dataAccess/userManagementDataAccess');
var calendarPasswordManagement = require('../dataAccess/calendarPasswordManagement');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var googleCalendarAPI = require('../common/googleCalendar');
var companyClass = require('../dataAccess/corporateDataAccess/companyModelClass');

var appCredential = new appCredentials();
var common = new commonUtility();
var company = new companyClass();
var userManagements = new userManagement();
var googleCalendar = new googleCalendarAPI();

var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var awsCredentials = appCredential.getAwsCredentials();


var domainName = domain.domainName;
var domainNameForUniqueName = domain.domainNameForUniqueName;
var relatasSocialAccounts = appCredential.getRelatasSocialAccounts();
var relatasHeaders = appCredential.getRelatasHeaders();

var google = require('node-google-api')(authConfig.GOOGLE_CLIENT_ID);
var linkedinAPI = require('node-linkedin')(authConfig.LINKEDIN_CONSUMER_KEY, authConfig.LINKEDIN_CONSUMER_SECRET, authConfig.LINKEDIN_REDIRECT_URL);

var logLib = new winstonLog();
var logger =logLib.getWinston();

// Router to render profile page when user loggedin 
//router.get('/calendar',isLoggedIn,common.checkUserDomain, function(req, res) {
//    try {
//        res.render('profilePage');
//        logger.info('authorized user entering to the Home Page');
//    }
//    catch (ex) {
//        logger.error('authenticated is invalid to landing Home page', ex.stack);
//        throw ex;
//    }
//});

router.get('/getDomainName',function(req,res){
    res.send(domainName);
});

router.get('/getAwsCredentials',function(req,res){
    res.send(awsCredentials);
});

router.get('/getRelatasSocialAccounts',function(req,res){
    res.send(relatasSocialAccounts);
});

router.get('/getRelatasHeaders',function(req,res){
    res.send(relatasHeaders);
});


router.get('/u/:user/:calPass',function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.routerName = '/u/uniquename/calPass';

    req.session.timeSlots = [];
    req.session.meetingDetails = null;
    req.session.doc = null;
    req.session.showMeeting = false;
    req.session.isCalendarAuthorised = false;
    req.session.sendInviteLinkedinStatus = 'n'

    var calPssword = req.params.calPass;
    req.session.publicProfileUrl=req.params.user.toLowerCase();
    var url=domainNameForUniqueName+'/u/'+req.params.user.toLowerCase();

    userManagements.findUserPublicProfile(url,req.params.user.toLowerCase(),function(error,publicProfile){
        userLogInfo.functionName = 'findUserPublicProfile';
        if(error){
            userLogInfo.error = error;

            logger.info('Error occurred while searching user public profile in the DB',userLogInfo);
            res.render('error');
        }
        if (!publicProfile) {
            logger.info('user public profile not exist in the DB',userLogInfo);
            res.render('identityNotFound');
        }
        else{
            logger.info('sending user public profile information to requested client',userLogInfo);
            userManagements.authoriseCalendarWithEncryptedPassword(publicProfile._id,calPssword,function(error,isAuthorise,msg){
                userLogInfo.functionName = 'authoriseCalendarWithEncryptedPassword';

                if(error){
                    userLogInfo.error = error;
                    logger.info('Error occurred while authoriseCalendarWithEncryptedPassword in the DB',userLogInfo);
                    res.render('identityNotFound');
                }
                else if(isAuthorise){
                    req.session[req.params.user] = true;
                    res.redirect('/'+req.params.user);
                }
                else{
                    res.send('<html><head><script>alert("This url is not authorised to access this calendar.");window.location.replace("/")</script></head></html>');
                }
            })
        }
    });
});

router.get('/userPublicProfile',function(req,res){
    if (req.session.publicProfileUrl) {
        var userLogInfo = getUserLogInfo(req);
        userLogInfo.routerName = '/userPublicProfile';
        var url=domainNameForUniqueName+'/u/'+req.session.publicProfileUrl;

        userManagements.findUserPublicProfile(url,req.session.publicProfileUrl,function(error,publicProfile){
            userLogInfo.functionName = 'findUserPublicProfile';
            if(error){
                userLogInfo.error = error;

                logger.info('Error occurred while searching user public profile in the DB',userLogInfo);
                res.send(false);
            }
            if (!publicProfile) {

                logger.info('user public profile not exist in the DB',userLogInfo);
                res.send(false);
            }
            else{
                logger.info('sending user public profile information to requested client',userLogInfo);
                res.send(publicProfile);
            }
        });
    }else res.render('/');
});

// schedule page -7 & +45
router.get('/userPublicProfileCalendarEvents',function(req,res){
    req.session.loopCount=0;
    req.session.events=[];
    var startDate = new Date();
    //var endDate = new Date(startDate.getTime() + 3600000);
    var endDate = new Date();
    var timeMax = new Date();


        endDate.setDate(endDate.getDate() - 1)
        timeMax.setDate(timeMax.getDate() + 45)


    var endMonth = endDate.getMonth()+1;
    var maxMonth = timeMax.getMonth()+1;

    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'requested for calendar info with url';
    if (req.session.publicProfileUrl) {

        var url=domainNameForUniqueName+'/u/'+req.session.publicProfileUrl;

        userManagements.findUserPublicProfile(url,req.session.publicProfileUrl,function(error,publicProfile){
            userLogInfo.functionName = 'findUserPublicProfile';
            if(error){
                userLogInfo.error = error;
                userLogInfo.info = 'Error in mongo while searching for user profile with unique url';

                logger.info('Error in the DB',userLogInfo);
                res.send(false);
            }
            if (!publicProfile) {

                logger.info('User profile not found in searching with unique url',userLogInfo);
                res.send(false);
            }
            else{  // start of else usermanagement
                var userInfo = publicProfile;

                google.build(function(api) {  // start of google api
                    if (userInfo.google[0] != undefined) {
                        if (userInfo.google[0].id == '') {
                            logger.info('No Google account found in '+userInfo.firstName+'account',userLogInfo);
                            res.send(false)
                        }
                        else{
                            userInfo.google.forEach(function(info,index){ // start of forEach
                                var ind=index;
                                var tokenProvider = new GoogleTokenProvider({
                                    refresh_token: info.refreshToken,
                                    client_id:     authConfig.GOOGLE_CLIENT_ID,
                                    client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                                    access_token: info.token
                                });
                                tokenProvider.getToken(function (err, token) { // start of token provider
                                    if(err){
                                        userLogInfo.error = err;
                                        userLogInfo.info = 'Error while refreshing token';

                                        logger.info('Error in token provider module',userLogInfo);
                                    }

                                    api.calendar.events.list(  // calendar event list start
                                        {
                                            calendarId: 'primary',
                                            access_token:token, // You have to supply the OAuth token for the current user
                                            singleEvents:true,
                                            maxResults:2499,
                                            timeMin:endDate.getFullYear()+'-'+endMonth+'-'+endDate.getDate()+'T00:00:00.000Z',
                                            timeMax:timeMax.getFullYear()+'-'+maxMonth+'-'+timeMax.getDate()+'T00:00:00.000Z'
                                        },
                                        function(events) {

                                            userInfo.functionName += ', Google calendar events API';
                                            var account=[];
                                            if (events == undefined || events ==  null ) {

                                                userLogInfo.info = 'Google calendar api returns empty event list';


                                                logger.info('No events found in user google account',userLogInfo);
                                                res.send(false);
                                            }else{
                                                parseEvents(events, req, res, function(){
                                                    if (req.session.loopCount == userInfo.google.length) {
                                                        list=req.session.events;
                                                        req.session.events=null;

                                                        logger.info('List of events are received from google',userLogInfo);
                                                        res.send(list);
                                                    };
                                                });
                                            }
                                        }); // end of calendar event list
                                }); // end of token provider
                            }); // end of forEach
                        } // end of if (google profile check)
                    } else res.send(false);
                })
            } // close of else usermanagement
        });
    }
    else res.render('/');
});

router.get('/userProfile', isLoggedInProfile,function(req, res){
    var userLogInfo = getUserLogInfo(req);
    try {

        userLogInfo.action = 'request to get logged in user profile';
        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret);
        var userId = decoded.id;

        userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0,google:0,linkedin:0,facebook:0,twitter:0,officeCalendar:0},function(error,userProfile) {
            userLogInfo.functionName = 'selectUserProfile';

            if (error || !checkRequired(userProfile)) {
                userLogInfo.error = error;
                userLogInfo.info = 'Error in finding logged in user profile';
                logger.error('Error in mongo',userLogInfo);
                userProfile = null;
                res.send(userProfile);

            }
            else{
                userLogInfo.userName = userProfile.firstName;
                req.session.userLogInfo.userName = userProfile.firstName;
                userLogInfo.info = 'Logged in user profile found';
                logger.info('Transferring authenticated user profile information',userLogInfo);
                res.send(userProfile);
            }
        });
    }
    catch (ex) {
        logger.error('Exception in profile js file userProfile router ', ex.stack);
        throw ex;
    }
});

router.post('/authoriseCalendar',function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request authorise calendar';
    var userId = req.body.userId;
    var calPassword = req.body.calPassword;

    userManagements.authoriseCalendar(userId,calPassword,function(error,isValid,message){
        if(error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error got while authorising user calendar';
            logger.info('Error in mongo',userLogInfo);
        }
        if(isValid){
            userLogInfo.info = 'Calendar authorised with valid password'
            logger.info('User entered valid password for calendar ',userLogInfo);
        }

        req.session.isCalendarAuthorised = isValid;
        res.send(isValid);
    });
});

router.post('/calendarPasswordRequest',isLoggedIn,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    var reqObj = req.body;

    userLogInfo.action = 'request to send email request for calendar password';

    var calendarPasswordManagements = new calendarPasswordManagement();
    var emailSenders = new emailSender();


    if(checkRequired(reqObj)){
        if(checkRequired(reqObj.fromUserId) && checkRequired(reqObj.fromName) && checkRequired(reqObj.fromEmailId) && checkRequired(reqObj.toUserId) && checkRequired(reqObj.toName) && checkRequired(reqObj.toEmailId)){

            var obj = {
                requestFrom:{
                    userId:reqObj.fromUserId
                },
                requestTo:{
                    userId:reqObj.toUserId
                }
            };
            calendarPasswordManagements.saveRequest(obj,function(error,status,calendarPasswordReq){
                if(status == 'success'){
                    if(checkRequired(calendarPasswordReq)){
                        emailSenders.sendCalendarPasswordRequestMail(reqObj);

                        var interaction = {};
                        interaction.userId = calendarPasswordReq.requestTo.userId || '';
                        interaction.action = 'receiver'
                        interaction.type = 'calendar-password';
                        interaction.subType = 'calendar-password';
                        interaction.refId = calendarPasswordReq._id;
                        interaction.source = 'relatas';
                        interaction.title = 'Calendar password request';
                        interaction.description = '';
                        common.storeInteraction(interaction);

                        var interactionNew = {};
                        interactionNew.userId = calendarPasswordReq.requestFrom.userId;
                        interactionNew.action = 'sender';
                        interactionNew.type = 'calendar-password';
                        interactionNew.subType = 'calendar-password';
                        interactionNew.refId = calendarPasswordReq._id;
                        interaction.source = 'relatas';
                        interaction.title = 'Calendar password request';
                        interaction.description = '';
                        common.storeInteraction(interactionNew);

                        res.send(true)
                    }
                    else res.send(false)
                }
                else res.send(false)
            })
        }
        else res.send(false)
    }
    else res.send(false)
});


router.get('/getReceivedCalendarPasswordRequests',isLoggedIn,function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var calendarPasswordManagements = new calendarPasswordManagement();
    calendarPasswordManagements.getReceivedCalendarPasswordRequests(userId,function(error,calendarRequests){
        if(checkRequired(calendarRequests)){
            if(checkRequired(calendarRequests[0])){
                res.send(calendarRequests)
            }
            else res.send(false)
        }
        else res.send(false)
    })
})

router.get('/getUserBasicInfo/:userId',function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request to get basic user profile';
    var userId = req.params.userId;

    if(checkRequired(userId) && userId != 'null'){
        userManagements.getUserBasicProfile(userId,function(error,userProfile) {
            userLogInfo.functionName = 'selectUserProfile';

            if (error) {
                userLogInfo.error = error;
                userLogInfo.info = 'Error in finding basic user profile';
                logger.error('Error in mongo',userLogInfo);
                userProfile = null;
                res.send(userProfile);

            }
            else if(checkRequired(userProfile)){
                userLogInfo.userName = userProfile.firstName;
                req.session.userLogInfo.userName = userProfile.firstName;

                res.send(userProfile);
            }
            else{

                res.send(userProfile);
            }
        });
    }
})

router.post('/calendarPasswordResponse',isLoggedIn,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request to calendar password';
    var resObj = req.body;

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    var calendarPasswordManagements = new calendarPasswordManagement();
    var emailSenders = new emailSender();
    if(checkRequired(resObj.requestedByUserId) && checkRequired(resObj.requestedByName) && checkRequired(resObj.requestedByEmailId) && checkRequired(resObj.publicProfileUrl) && checkRequired(resObj.profilePrivatePassword) && checkRequired(resObj.emailId) && checkRequired(resObj.name) && checkRequired(resObj.requestId) && checkRequired(resObj.accepted)){
        var accepted = false;
        if(resObj.accepted == true || resObj.accepted == 'true'){
            accepted = true;
        }
        resObj.accepted = accepted;
        var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
        var host = req.headers.host;
        resObj.url = protocol+host+'/u/'+resObj.publicProfileUrl+'/'+resObj.profilePrivatePassword;
        if(accepted == false && !checkRequired(resObj.msg)){
            res.send('noMsg');
        }else{
            calendarPasswordManagements.updateCalendarRequest(resObj,function(error,isUpdated){
                userLogInfo.functionName = 'updateCalendarRequest';
                if(error){
                    userLogInfo.error = error;
                    userLogInfo.info = 'Error in updating calendar request';
                    logger.error('Error in mongo',userLogInfo);
                    res.send(isUpdated);
                }
                else if(isUpdated){
                    var interaction = {};
                    interaction.userId = userId;
                    interaction.action = 'receiver'
                    interaction.type = 'calendar-password';
                    interaction.subType = 'calendar-password';
                    interaction.refId = resObj.requestId;
                    interaction.source = 'relatas';
                    interaction.title = 'Calendar password response';
                    interaction.description = resObj.msg || '';
                    common.storeInteraction(interaction);

                    var interactionNew = {};
                    interactionNew.userId = resObj.requestedByUserId || '';
                    interactionNew.emailId = resObj.requestedByEmailId;
                    interactionNew.action = 'sender';
                    interactionNew.type = 'calendar-password';
                    interactionNew.subType = 'calendar-password';
                    interactionNew.refId = resObj.requestId;
                    interactionNew.source = 'relatas';
                    interactionNew.title = 'Calendar password response';
                    interactionNew.description = resObj.msg || '';
                    common.storeInteractionReceiver(interactionNew);

                    if(accepted == false && !checkRequired(resObj.msg)){
                        res.send('noMsg');
                    }
                    else{
                        emailSenders.sendCalendarPasswordMail(resObj);
                        emailSenders.sendCalendarPasswordSentMail(resObj);
                        res.send(true);
                    }
                }
                else{
                    res.send(isUpdated)
                }
            })
        }
    }
    else{
        res.send(false);
    }
})

// router to get linkedin user info
router.get('/linkedinProfile',function(req, res){

    var profile=req.session.linkedinUser;

    res.send(profile);
});


// router to get googleUser authentication details
router.get('/googleUser',function(req,res){
    var profile=req.session.googleUser;
    res.send(profile);
});

router.post('/addGoogleData',function(req,res){
    var googleData = req.body;
    req.session.googleUser = googleData;
    res.send(true)
});

// router to get facebookUser authentication details
router.get('/facebookUser',function(req,res){
    var profile=req.session.facebookUser;
    res.send(profile);
});

// router to get twitterUser authentication details
router.get('/twitterUser',function(req,res){
    var profile=req.session.twitterUser;
    res.send(profile);
});
// router to get linkedin user info
router.get('/removeLinkedinProfile',function(req, res){

    req.session.linkedinUser = '';

    res.send(true);
});


// router to get googleUser authentication details
router.get('/removeGoogleUser/:accNo',function(req,res){
    var googleAccountNo = req.params.accNo;
    req.session.googleUser[parseInt(googleAccountNo)] = null;
    res.send(true);
});

// router to get facebookUser authentication details
router.get('/removeFacebookUser',function(req,res){
    req.session.facebookUser = '';
    res.send(true);
});

// router to get twitterUser authentication details
router.get('/removeTwitterUser',function(req,res){
    req.session.twitterUser = '';
    res.send(true);
});

// router to get stored session 
router.get('/storeSession',function(req,res){
    var sessionData=req.session.storeSession;
    res.send(sessionData);
})

// schedule page -1 & +45
// dashboard current day
// calendar -30 & +45

router.get('/corporate/calendarEvents/dashboard/:userId',isLoggedIn,function(req,res){ // starting of calendar router

    var userId = req.params.userId;
    if(common.checkRequired(userId)){
        getCalendarEvents(userId,req,res,false,'schedule');
    }else res.send(false);
});

router.get('/calendarEvents',isLoggedIn,function(req,res){ // starting of calendar router
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    getCalendarEvents(userId,req,res,false,'calendar');

}); // end of calendar router

router.get('/calendarEvents/dashboard',isLoggedIn,function(req,res){ // starting of calendar router
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var flag = req.session.dashboardMsg;

    getCalendarEvents(userId,req,res,flag,'dashboard');

}); // end of calendar router

router.get('/calendarEvents/mapMyCalendar',isLoggedIn,function(req,res){ // starting of calendar router
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var flag = req.session.dashboardMsg;

    getCalendarEvents(userId,req,res,false,'schedule');

}); // end of calendar router

router.post('/mapMyCalendar',function(req,res){
    var userId = req.body.id;

    getCalendarEvents(userId,req,res,false,'schedule');
});

function getCalendarEvents(userId,req,res,flag,page){

    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for calendar events';
    req.session.loopCount=0;
    req.session.events=[];
    var endDate = new Date();
    var timeMax = new Date();


    try {  // starting of try block

        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,dashboardPopup:1},function(error,userProfile) {
            userLogInfo.functionName = 'selectUserProfile in calendarEvents router';
            if(error){
                userLogInfo.error = error;
                userLogInfo.info = 'Error in finding logged in user profile';
                logger.error('Error in mongo',userLogInfo);
                userProfile = null;
            }
            else if (userProfile == null) {
                logger.info('User profile not found in searching with user id',userLogInfo);
                res.send(false)
            }
            else{
                var userInfo=userProfile;

                if(userProfile.dashboardPopup != true && page != 'schedule' && page != 'calendar'){
                    endDate.setDate(endDate.getDate() - 90)
                    timeMax.setDate(timeMax.getDate() + 90)
                }else if(page == 'schedule'){
                    endDate.setDate(endDate.getDate() - 1)
                    timeMax.setDate(timeMax.getDate() + 45)
                }else if(page == 'calendar'){
                    endDate.setDate(endDate.getDate() - 30)
                    timeMax.setDate(timeMax.getDate() + 45)
                }else if(page == 'dashboard'){
                    endDate.setDate(endDate.getDate() - 3)
                    timeMax.setDate(timeMax.getDate() + 3)
                }

                var endMonth = endDate.getMonth()+1;
                var maxMonth = timeMax.getMonth()+1;
                var allEvents=[];

                google.build(function(api) {  // start of google api

                    if (!googleCalendar.validateUserGoogleAccount(userInfo)) {
                        logger.info('No Google account found in account',userLogInfo);
                        res.send(false)
                    }
                    else{
                        userInfo.google.forEach(function(info){ // start of forEach

                            var tokenProvider = new GoogleTokenProvider({
                                refresh_token: info.refreshToken,
                                client_id:     authConfig.GOOGLE_CLIENT_ID,
                                client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                                access_token: info.token
                            });

                            tokenProvider.getToken(function (err, token) { // start of token provider
                                if(err){
                                    userLogInfo.error = err;
                                    userLogInfo.info = 'Error while refreshing token';

                                    logger.info('Error in token provider module',userLogInfo);
                                }
                                api.calendar.events.list(  // calendar event list start
                                    {
                                        calendarId: 'primary',
                                        access_token:token, // You have to supply the OAuth token for the current user
                                        singleEvents:true,
                                        maxResults:2499,
                                        timeMin:endDate.getFullYear()+'-'+endMonth+'-'+endDate.getDate()+'T00:00:00.000Z',
                                        timeMax:timeMax.getFullYear()+'-'+maxMonth+'-'+timeMax.getDate()+'T00:00:00.000Z'
                                    },
                                    function(events) {

                                        userInfo.functionName += ', Google calendar events API';
                                        var account=[];
                                        if (events == undefined || events ==  null ) {
                                            userLogInfo.info = 'Google calendar api returns empty event list';


                                            logger.info('No events found in user google account',userLogInfo);
                                            res.send(false);
                                        }else{
                                            parseEvents(events, req, res, function(){

                                                if (req.session.loopCount == userInfo.google.length) {
                                                    var list=req.session.events;
                                                    list = list == null ? [] : list;
                                                    req.session.events=null;

                                                    logger.info('List of events are received from google',userLogInfo);

                                                    res.send(list);
                                                }
                                            });
                                        }
                                    }
                                ); // end of calendar event list
                            }); // end of token provider
                        }); // end of forEach
                    } // end of if (google profile check)
                    // } else res.send(false);
                });  // end of google api
            }
        }); // ending of user managements

    } // end of try block
    catch (ex) {
        logger.error('Exception in profile js calendarEvents router ', ex.stack);
        throw ex;
    }
}

router.post('/createRelatasPersonalEventOnGoogle',isLoggedInProfile,function(req,res){

    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for creating calendar event';
    try{
        var title=req.body.title;
        var start=req.body.startDate;
        var end=req.body.endDate;

        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user ,tokenSecret);
        var userId = decoded.id;

        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1},function(error,userProfile) {
            userLogInfo.functionName = 'selectUserProfile in createEvent router';
            if (error) {
                userLogInfo.error = error;
                userLogInfo.info = 'Error in finding logged in user profile';
                logger.error('Error in mongo',userLogInfo);
                userProfile = null;
                res.send(userProfile);
            }
            else{
                var userInfo=userProfile;
                if (userInfo.google[0].id == '') {
                    res.send(false);
                }
                else{
                    var tokenProvider = new GoogleTokenProvider({
                        refresh_token: userInfo.google[0].refreshToken,
                        client_id:     authConfig.GOOGLE_CLIENT_ID,
                        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                        access_token: userInfo.google[0].token
                    });
                    tokenProvider.getToken(function (err, token) {
                        if (err) {
                            logger.info('Error in token provider'+err);
                        }

                        var google_calendar = new gcal.GoogleCalendar(token);
                        var calendarId='primary';
                        var request_body = {
                            summary:title,
                            end:{
                                date: end
                            },
                            start:{
                                date:start
                            }
                        };

                        google_calendar.events.insert(calendarId,request_body,function(err,result){
                            if(err){
                                logger.error('creating event in google failed');
                                res.send('creating event in google failed');
                            }
                            else{
                                logger.info('creating event in google success');
                                res.send('creating event in google success');
                            }
                        });
                    }) // end of token provider
                }
            }
        });
    }
    catch (ex) {
        logger.error('Error while creating google event ', ex.stack);
        throw ex;
    }
});

router.post('/update/firebase/token',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagements.updateFirebaseToken(userId, req.body, function(error, result) {
        if(error) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Something went wrong in updating token"
            })
        } else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                Message: "Token update successfull"
            })
        }
    })
});

router.get('/get/firebase/token/mobile',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);
    userManagements.getFirebaseToken(userId, function(error, result) {
        if(error) {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                Message: "Something went wrong while fetching token"
            })
        } else {
            res.send({
                SuccessCode: 1,
                ErrorCode: 0,
                token: result.mobileFirebaseToken
            })
        }
    })
});

function goToSchedulePage(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.routerName = '/uniquename';
    req.session.publicProfileUrl=req.params.user.toLowerCase();
    var url=domainNameForUniqueName+'/u/'+req.params.user.toLowerCase();

    userManagements.isExistingProfile(url,req.params.user.toLowerCase(),function(error,isExisting,user){
        userLogInfo.functionName = 'isExistingProfile';
        if (error) {
            userLogInfo.error = error;
            logger.info('Error occurred while searching user public profile in the DB',userLogInfo);
            res.render('error');
        }
        if (isExisting) {
            var obj = getReturnObj(user);
            var companyId = common.getSubDomain(req);
            if(companyId){

                if(user.corporateUser){
                    company.findOrCreateCompanyByUrl(getCompanyObj(companyId,req),{url:1},function(companyProfile){

                        if(common.checkRequired(companyProfile) && user.companyId.toString() == companyProfile._id.toString()){
                                req.session.publicProfileUserId = user._id;
                                userLogInfo.info = 'User requested with a valid url 1';
                                logger.info('User requested with a valid public url 1',userLogInfo);
                                var status = common.getLoginStatus(req);
                                obj.loginSignUp = status.loginSignUp;
                                obj.logout = status.logout;
                                res.render('meetingProcess',obj);
                        }
                        else{
                            userLogInfo.urlIs = url;
                            userLogInfo.info = 'url is not valid or no corporate user found with that url 2';
                            logger.info('User requested with invalid public url of corporate user 2',userLogInfo);
                            res.render('identityNotFound');
                        }
                    })
                }
                else{
                    userLogInfo.urlIs = url;
                    userLogInfo.info = 'url is not valid or no corporate user found with that url 3';
                    logger.info('User requested with invalid public url of corporate user 3',userLogInfo);
                    res.render('identityNotFound');
                }
            }
            else{
                if(!user.corporateUser){
                    req.session.publicProfileUserId = user._id;
                    userLogInfo.info = 'User requested with a valid url 4';
                    logger.info('User requested with a valid public url 4',userLogInfo);
                    var status = common.getLoginStatus(req);
                    obj.loginSignUp = status.loginSignUp;
                    obj.logout = status.logout;
                    res.render('meetingProcess',obj);
                }
                else{
                    userLogInfo.urlIs = url;
                    userLogInfo.info = 'url is not valid or no user found with that url 4';
                    logger.info('User requested with invalid public url 4',userLogInfo);
                    res.render('identityNotFound');
                }
            }
        }
        else{
            userLogInfo.urlIs = url;
            userLogInfo.info = 'url is not valid or no user found with that url 5';
            logger.info('User requested with invalid public url 5',userLogInfo);
            res.render('identityNotFound');

        }
    });
}

function getReturnObj(userProfile){
    var obj = {};
    var userUrl = common.getValidUniqueUrl(userProfile.publicProfileUrl);
    obj.uniqueUrl = userUrl;
    obj.description =  "I've created my Unique Identity on Relatas ("+userUrl+"). Relatas is a new way to interact with your contacts with Unified Communication, Secure Cloud Scheduling and Intelligent Document Sharing";
    if(userProfile.profilePicUrl){

        if(userProfile.profilePicUrl.charAt(0) == '/' || userProfile.profilePicUrl.charAt(0) == 'h'){
            obj.image = domainName+''+userProfile.profilePicUrl;
        }else  obj.image = domainName+'/'+userProfile.profilePicUrl;
    }else obj.image = domainName+'/images/default.png';
    return obj;
}

function contains(str, subStr) {
    return str.indexOf(subStr) != -1;
}

function getCompanyObj(companyName,req){
    var company = {
        url:req.headers.host,
        companyName:companyName
    }
    return company;
}

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }else
    {
        // if they aren't redirect them to the home page
        return res.redirect('/');
    }
}

function isLoggedInProfile(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        return next();
    }else
    {
        // if they aren't redirect them to the home page
        res.send(false);
    }
}

function parseEvents(events, req, res, callback){
    var colorArray = ['#41b27a','#67a3bc','#876a9d','#FFFF00'];
    var count=req.session.loopCount;
    req.session.loopCount++;
    if (events.items == undefined || events.items == null || events.items == '') {
        callback();
    }
    else{
        for (var i = 0; i < events.items.length; i++) {

            events.items[i].color=colorArray[count];
            events.items[i].count = count;
            req.session.events.push(events.items[i]);
        }
    }
    callback();
}

function getUserLogInfo(req){
    var userIp = getClientIp(req);
    var userLogInfo;
    if(req.session.userLogInfo){
        userLogInfo = req.session.userLogInfo;
    }
    else{
        userLogInfo = {
            userName:'',
            userIp:userIp,
            loginType:'',
            info:'',
            cause:'',
            action : ''
        }
        req.session.userLogInfo = userLogInfo;
    }
    return userLogInfo;
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
};

function checkRequired(data){
    if(data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

module.exports = router;
