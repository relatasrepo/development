

var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var moment = require('moment-timezone');

var userManagement = require('../dataAccess/userManagementDataAccess');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var interactionManagement = require('../dataAccess/interactionManagement');


var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();

var userManagements = new userManagement();
var logLib = new winstonLog();
var common = new commonUtility();
var interactions = new interactionManagement();

var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();

//router.get('/interactions/pastSevenDays/status',isLoggedInUser,function(req,res){
//  res.render('interactionAction');
//});

router.get('/interactions/pastSevenDays/status/get',isLoggedInUser,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(userId)){
        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                var start,end;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    end = moment().tz(user.timezone.name);

                    end.hours(23);
                    end.minutes(59);
                    end.seconds(59);
                    end.milliseconds(0);

                    start = end.clone();
                    start.date(start.date() - 7);

                    start.hours(0);
                    start.minutes(0);
                    start.seconds(0);
                    start.milliseconds(0);

                    start = start.format();
                    end = end.format()
                }
                else{
                    start = new Date();
                    end = new Date();

                    start.setHours(0,0,0,0);
                    end.setHours(23,59,59,0);
                    start.setDate(start.getDate() - 7);
                    end.setDate(end.getDate() + 1);

                }

                interactions.pastSevenDayInteractionStatus(common.castToObjectId(userId),user.emailId,{start:start,end:end},true,function(interactions){
                    res.send(interactions);
                })
            }
            else{
                res.send(false);
            }
        })
    }
    else{
        res.send(false);
    }
});

router.get('/interactions/pastSevenDays/status/get/byId',isLoggedInUser,function(req,res){
    var userId = common.getUserId(req.user);
    if(common.checkRequired(req.query.userId)){
        userId = req.query.userId;
        userManagements.findUserProfileByIdWithCustomFields(common.castToObjectId(userId),{emailId:1,timezone:1},function(error,user){
            if(common.checkRequired(user)){
                var start,end;
                if(common.checkRequired(user.timezone) && common.checkRequired(user.timezone.name)){
                    end = moment().tz(user.timezone.name);

                    end.hours(23);
                    end.minutes(59);
                    end.seconds(59);
                    end.milliseconds(0);

                    start = end.clone();
                    start.date(start.date() - 7);

                    start.hours(0);
                    start.minutes(0);
                    start.seconds(0);
                    start.milliseconds(0);

                    start = start.format();
                    end = end.format()
                }
                else{
                    start = new Date();
                    end = new Date();

                    start.setHours(0,0,0,0);
                    end.setHours(23,59,59,0);
                    start.setDate(start.getDate() - 7);
                    end.setDate(end.getDate() + 1);

                }

                interactions.pastSevenDayInteractionStatus(common.castToObjectId(userId),user.emailId,{start:start,end:end},false,function(interactions){
                    res.send(interactions);
                })
            }
            else{
                res.send(false);
            }
        })
    }
    else{
        res.send(false);
    }
});

router.get('/interactions/get/byId',function(req,res){
    if(common.checkRequired(req.query.id)){
        interactions.findInteractionById(req.query.id,function(interaction){
            res.send(interaction);
        })
    }
    else res.send(false);
});

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }else
    {

        return res.redirect('/');
    }
}

module.exports = router;