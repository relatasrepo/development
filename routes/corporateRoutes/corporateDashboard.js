var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');

var appCredentials = require('../../config/relatasConfiguration');
var commonUtility = require('../../common/commonUtility');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var corporateClass = require('../../dataAccess/corporateDataAccess/corporateModelClass');
var CorporateLogo = require('../../databaseSchema/corporateCollectionSchema').corporatelogoModel;
var winstonLog = require('../../common/winstonLog');

var appCredential = new appCredentials();
var logLib = new winstonLog();
var company = new companyClass();
var common = new commonUtility();
var corporate = new corporateClass();

var corporateErrorLog = logLib.getCorporateErrorLogger();

router.get('/companylogos/:companyurl', common.isLoggedInUserToGoNext, function(req, res){
    corporate.saveCompanyLogo(req.params.companyurl, function(){
        res.status(200)
        res.send()
    })
})

router.get('/',common.checkUserDomain,function(req,res,next){
     var subDomain = common.getSubDomain(req);
    if(subDomain){
        company.findOrCreateCompanyByUrl(getCompanyObj(subDomain,req),{url:1,companyName:1,logo:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var logo = '/images/mailer/example.png';
                if(common.checkRequired(companyProfile.logo) && common.checkRequired(companyProfile.logo.url)){
                    logo = companyProfile.logo.url;
                }

                if(common.isLoggedInUserBoolean(req)){
                    res.redirect('/insights');
                }
                else res.render('onboarding/signup',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyLogo:logo});
            }
            else res.render('error');
        });
    }
    else return next();
});

router.get('/login',function(req,res){
    var subDomain = common.getSubDomain(req);
    if(subDomain){
        company.findOrCreateCompanyByUrl(getCompanyObj(subDomain,req),{url:1,companyName:1,logo:1,emailDomains:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var logo = '/images/mailer/example.png';
                if(common.checkRequired(companyProfile.logo) && common.checkRequired(companyProfile.logo.url)){
                    logo = companyProfile.logo.url;
                }

                if(isLoggedInUser(req,res)){

                    res.redirect('/today/dashboard');
                }else
                    res.render('corporateViews/corporateLogin',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyLogo:logo});
            }
            else res.render('error');
        });

    }else res.render('error');
});
/*
router.get('/dashboard',common.checkUserDomain,function(req,res,next){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1,logo:1},function(companyProfile) {
            if (companyProfile) {
                req.session.companyId = companyProfile._id;
                if (isLoggedInUser(req, res)) {
                    var tokenSecret = "alpha";
                    var decoded = jwt.decode(req.user ,tokenSecret);
                    var userId = decoded.id;
                    var html = '';
                    if(validateAdmin(companyProfile.admin,userId)){
                        html = 'style=display:block';
                    }
                    else{
                        html = 'style=display:none';
                    }
                    var logo = '/images/mailer/example.png';
                    if(common.checkRequired(companyProfile.logo) && common.checkRequired(companyProfile.logo.url)){
                        logo = companyProfile.logo.url;
                    }

                    common.getUserProfileAndStatus(common.castToObjectId(userId),req,function(result){

                        if(result){
                            result.company = JSON.stringify(companyProfile);
                            result.companyName = companyProfile.companyName;
                            result.adminContent = html;
                            result.companyLogo = logo;
                            res.render('corporateViews/corporateDashboard',result);
                        }else res.render('corporateViews/corporateDashboard');
                    });
                }
                else res.redirect('/');
            }
        });
    }else next();
});*/



function validateAdmin(adminList,userId){

    if(common.checkRequired(adminList) && adminList.length > 0){
        var isAdmin = false;
        for(var i=0; i<adminList.length; i++){

            if(adminList[i].adminId == userId){
                isAdmin = true;
            }
        }
        return isAdmin;
    }else return false;
}


router.get('/corporate/dashboard/getHierarchy',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    corporate.getUserHierarchy(userId,function(list){
        res.send(list);
    })
});

router.get('/external/login',function(req,res){

    if(common.checkRequired(req.query)){
        var token = req.query.token;
        var goTo = req.query.goTo;
        try{
            var decoded = jwt.decode(token ,"alpha");

            req.logIn(token,function(err){
                res.redirect(goTo);
            })
        }
        catch(e){
            corporateErrorLog.info('Invalid token in /external/login '+JSON.stringify(e));
            res.render('error');
        }
    }
    else{
        corporateErrorLog.info('No query parameters for /external/login ');
        res.render('error');
    }
});

function getCompanyObj(companyName,req){
    var company = {
        url:req.headers.host,
        companyName:companyName
    }
    return company;
}

function isLoggedInUser(req, res) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){
        return true;
    }else
    {
        // if they aren't redirect them to the home page

        return false;
    }
}

module.exports = router;