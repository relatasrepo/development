var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var _ = require("lodash");
var moment = require('moment-timezone');
var excel2Json = require('node-excel-to-json');
var redis = require('redis');
var redisClient = redis.createClient();
var adminLogsCollection = require('../../databaseSchema/adminLogs').adminLogs;

var commonUtility = require('../../common/commonUtility');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var corporateClass = require('../../dataAccess/corporateDataAccess/corporateModelClass');
var adminClass = require('../../dataAccess/corporateDataAccess/adminModelClass');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var OrgRoles = require('../../dataAccess/teamRoles');
var winstonLog = require('../../common/winstonLog');
var opportunityManagement = require('../../dataAccess/opportunitiesManagement');
var OppWeightsClass = require('../../dbCache/company.js')
var taskManagementClass = require('../../dataAccess/taskManagementClass');
var taskManagementClassObj = new taskManagementClass();
var emailSender = require('../../public/javascripts/emailSender');
var emailSenders = new emailSender();
var RevenueHierarchy = require('../../dataAccess/revenueHierarchy');
var SecondaryHierarchy = require('../../dataAccess/secondaryHierarchyManagement');

var OrgHierarchy = require('../../dataAccess/orgHierarchy');
var ProductHierarchy = require('../../dataAccess/productHierarchy');
var AccountManagement = require('../../dataAccess/accountManagement');
var MasterData = require('../../dataAccess/masterData');

var logLib = new winstonLog();
var company = new companyClass();
var corporate = new corporateClass();
var common = new commonUtility();
var admin = new adminClass();
var userManagements = new userManagement();
var oppManagementObj = new opportunityManagement();
var oppWeightsObj = new OppWeightsClass();
var revenueHierarchyObj = new RevenueHierarchy();
var secondaryHierarchyObj = new SecondaryHierarchy();

var orgHierarchyObj = new OrgHierarchy();
var productHierarchyObj = new ProductHierarchy();
var orgRolesObj = new OrgRoles();
var accountObj = new AccountManagement();
var masterAccObj = new MasterData();

var adminChangeLog = logLib.getAdminChangesLogger();

router.get('/corporate/admin',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/corporateAdmin',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/insights/summary/report',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);

                userManagements.selectUserProfilesCustomQuery({_id:common.castToObjectId(userId)},{emailId:1},function(error,user){

                    if(user[0] && user[0].emailId && (user[0].emailId =="sofia@relatas.com" || user[0].emailId =="sureshhoel@gmail.com" || user[0].emailId =="sudip@relatas.com" || user[0].emailId =="naveenpaul@relatas.com")){
                        res.render('corporateViews/adminInsights',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});

                    } else {
                        res.render('error')
                    };
                })
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/organisation/roles',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/orgRoles',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get("/corporate/admin/master/data/management",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/masterAccountManagement',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post("/corporate/admin/save/master/account/list",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body.data && req.body.data.length>0){
                            try{
                                var masterList = req.body.data;
                                masterAccObj.saveAcc(common.castToObjectId(companyProfile._id),masterList,req.body.accountListName,function (err,result) {
                                    res.send("ok")
                                });
                            } catch(err){
                                res.send("error")
                            }
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post("/corporate/admin/save/master/data/important/headers",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            try{
                                masterAccObj.updateImportantHeaders(common.castToObjectId(companyProfile._id),req.body,function (err,result) {
                                    res.send("ok")
                                });
                            } catch(err){
                                res.send("error")
                            }
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get("/corporate/admin/get/master/account/types",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    masterAccObj.getAccTypes(common.castToObjectId(companyProfile._id),function (err,result) {
                        if(!err && result && result[0] && result[0].types){
                            res.send(result[0].types)
                        } else {
                            res.send([])
                        }
                    })
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get("/corporate/admin/get/master/account/data",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        masterAccObj.getAccTypesAndData(common.castToObjectId(companyProfile._id),function (err,result) {
                            if(!err && result)
                                res.send(result)
                            else 
                                res.send([])
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post("/corporate/admin/add/to/existing/acctype",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body.data && req.body.data.length>0){
                            try{
                                var masterList = req.body.data;

                                company.addAccountTypes(common.castToObjectId(companyProfile._id),req.body.accountListName,function (err,addRes) {
                                    accountObj.updateManualAccounts(common.castToObjectId(companyProfile._id),masterList,req.body.accountListName,function (err,result) {
                                        res.send("ok")
                                    })
                                })
                            } catch(err){
                                res.send("error")
                            }
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post("/corporate/admin/edit/master/account/data",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);

                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            try{
                                masterAccObj.updateAccountData(common.castToObjectId(companyProfile._id),req.body,common,function (err,result) {
                                    updateOppsForMasterData(common.castToObjectId(companyProfile._id),req.body,function(){});
                                    res.send("ok")
                                })
                            } catch(err){
                                res.send("error")
                            }
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

function updateOppsForMasterData(companyId,data,callback){
    oppManagementObj.updateMasterData(companyId,data,callback)
}

router.post("/corporate/admin/remove/master/account/type",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            company.removeAccountTypes(common.castToObjectId(companyProfile._id),req.body.accountListName,function (err,addRes) {
                                masterAccObj.deleteAccount(common.castToObjectId(companyProfile._id),req.body.accountListName,function (err,result) {
                                    res.send("ok")
                                });
                            })
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post("/corporate/admin/edit/account/type",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            masterAccObj.deleteAccountData(common.castToObjectId(companyProfile._id),req.body,function (err,result) {
                                res.send("ok")
                            });
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post("/corporate/admin/master/opp/link",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            masterAccObj.updateAccOppLink(common.castToObjectId(companyProfile._id),req.body,function (err,result) {
                                res.send("ok")
                            });
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post("/corporate/admin/remove/master/account",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            masterAccObj.deleteAccountData(common.castToObjectId(companyProfile._id),req.body,function (err,result) {
                                res.send("ok")
                            });
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post("/corporate/admin/link/master/acc/to/relatas/acc",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            masterAccObj.linkToRelatasAcc(common.castToObjectId(companyProfile._id),
                                req.body.acc_identifier_rel,
                                req.body._id,
                                req.body.domain,function (err,result) {
                                res.send("ok")
                            });
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post("/corporate/admin/add/master/account",common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            try{
                                var masterList = [req.body.data]
                                accountObj.updateManualAccounts(common.castToObjectId(companyProfile._id),masterList,req.body.accountListName,function (err,result) {
                                    res.send("ok")
                                })
                            } catch(err){
                                res.send("error")
                            }
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get("/corporate/admin/get/master/account/list",common.isLoggedInUser,function(req,res){

    var userProfile = common.getUserFromSession(req);
    var companyId = userProfile?userProfile.companyId:null;
    var skip = common.checkRequired(req.query.skip) ? parseInt(req.query.skip) : 0;
    var search = req.query.search?req.query.search:null;
    var accountType = req.query.accountType?req.query.accountType:null;

    masterAccObj.getAccData(common.castToObjectId(companyId),accountType,search,skip,function (err,result) {

        if(!err && result && result.length>0){

            if(search && result[0].data && result[0].data.length>0){
                var matches = [];
                
                result[0].data.forEach(function (el) {
                    if(el._id){
                        el._id = String(el._id);
                    }
                    for(var key in el){
                        if(_.includes(String(el[key]).toLowerCase(),search.toLowerCase())){
                            matches.push(el)
                        }
                    }
                });

                result[0].data = _.uniq(matches,"_id");
                result[0].size = result[0].data.length;
                result[0].data = result[0].data.slice(skip, skip+50);

                res.send(result);
            } else {
                res.send(result)
            }
        } else {
            res.send([])
        }
    })
});

router.get('/corporate/admin/edit/opportunity/stages',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/oppStages',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/stage/directive',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/stageTemplate');
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/rev/getHierarchy',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                revenueHierarchyObj.getHierarchy(companyProfile._id,function(err,data){
                    if(data){
                        res.send({
                            "SuccessCode": 1,
                            Data: data
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/admin/update/stage',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){

                        var opportunityStages = [];

                        if(req.body && req.body.payload){

                            _.each(req.body.payload,function (op) {
                                opportunityStages.push({
                                    name:op.name,
                                    isStart:op.isStart,
                                    isEnd:op.isEnd,
                                    isWon:op.isWon,
                                    isLost:op.isLost,
                                    weight:parseFloat(op.weight),
                                    order:parseInt(op.order),
                                    closeType:op.closeType,
                                    commitStage:op.commitStage?true:false
                                })
                            })
                        }

                        var updateObj = {
                            opportunityStages:opportunityStages
                        }

                        company.updateObj(common.castToObjectId(companyProfile._id.toString()),updateObj,function (err,result) {

                            oppWeightsObj.clearOppCache(companyProfile._id.toString(),function () {
                                res.send(true)
                            })
                        })
                    }
                    else {
                        res.send(false)
                    };
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/companyDetails',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;

                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/corporateCompanyDetails',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyId:companyProfile._id});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/oppDetails',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;

                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/corporateOppDetails',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyId:companyProfile._id});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/matrix/access',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;

                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/matrixAccess',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyId:companyProfile._id});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/portfolios',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;

                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/portfolios',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyId:companyProfile._id});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/documentDetails',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;

                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/corporateDocumentDetails',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyId:companyProfile._id});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/web2Lead',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;

                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/web2Lead',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyId:companyProfile._id});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/admin/web2Lead/add/to/team',common.isLoggedInUser,function(req,res){

    var companyId = req.body.companyId;
    var emailId = req.body.emailId;
    
    userManagements.findUserProfileByEmailId(emailId,function (err,profile) {
        if(!err && profile){
            var userId = profile._id.toString();

            company.addToSupportTeam(common.castToObjectId(companyId),common.castToObjectId(userId),function (err,result) {
                if(!err && result){
                    res.send(true)
                } else {
                    res.send(false)
                }
            });
        } else {
            res.send(false)
        }
    })

});

router.post('/corporate/admin/web2Lead/remove/from/team',common.isLoggedInUser,function(req,res){

    var companyId = req.body.companyId;
    var emailId = req.body.emailId;

    userManagements.findUserProfileByEmailId(emailId,function (err,profile) {
        if(!err && profile){
            var userId = profile._id.toString();

            company.removeFromSupportTeam(common.castToObjectId(companyId),common.castToObjectId(userId),function (err,result) {
                if(!err && result){
                    res.send(true)
                } else {
                    res.send(false)
                }
            });
        } else {
            res.send(false)
        }
    })

});

router.get('/corporate/admin/setHierarchy',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/corporateAdminHierarchy',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyId:companyProfile._id.toString()});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/corporate/admin/NewsetHierarchy',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.sendfile('views/corporateViews/corporateAdminNewHierarchy.html');
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/corporate/admin/secondary/hierarchy',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.sendfile('views/corporateViews/revenueHierarchy.html');
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/corporate/admin/organisation/hierarchy',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.sendfile('views/corporateViews/orgHierarchy.html');
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/corporate/admin/product/hierarchy',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.sendfile('views/corporateViews/productHierarchy.html');
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/revenue/user/update',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        revenueHierarchyObj.addUserToHierarchy(req.body,companyProfile._id,common,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/secondary/hierarchy/user/update',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        secondaryHierarchyObj.addUserToHierarchy(req.body,companyProfile._id,common,function (err,result) {
                            res.send(result);
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/secondary/hierarchy/user/update/sh/list',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        secondaryHierarchyObj.updateShList(req.body.userId,companyProfile._id,common,req.body.shItemsList,req.body.shType,function (err,result) {
                            var updateObj = {
                                secondaryHierarchyType:req.body.shType,
                                userId:req.body.userId,
                                secondaryHierarchyItemsList:req.body.shItemsList
                            };

                            secondaryHierarchyObj.updateOppsForSh(companyProfile._id,common,updateObj,function () {
                            });
                            res.send(result);
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/corporate/admin/test',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        secondaryHierarchyObj.updateOppsForSh(companyProfile._id,common,{},function (hier) {
                            res.send(hier)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
})

router.get('/corporate/admin/secondary/hierarchy',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        secondaryHierarchyObj.updateOppsForSh(companyProfile._id,common,{secondaryHierarchyType:req.query.secondaryHierarchyType},function (result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/org/user/update',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        orgHierarchyObj.addUserToHierarchy(req.body,companyProfile._id,common,function (err,result) {
                            getUserHierarchyTeam(userId,function(hierData){
                                secondaryHierarchyObj.updateOppAccessOrgHierarchy(companyProfile._id,common,req.body,hierData,function () {

                                })
                            });
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

function getUserHierarchyTeam(userId,callback){

    userManagements.getUserHierarchy(common.castToObjectId(userId), function(err, data){

        var listOfMembers = [],
            userIds = [];

        _.each(data,function (el) {
            userIds.push(common.castToObjectId(String(el._id)));

            listOfMembers.push({
                userId:el._id,
                userEmailId:el.emailId,
                teammate:el
            })

            _.each(data,function (hi) {
                if(_.includes(hi.hierarchyPath,String(el._id))){
                    listOfMembers.push({
                        userId:el._id,
                        userEmailId:el.emailId,
                        teammate:hi
                    })
                }
            })
        });

        var listOfMembersData = _
            .chain(listOfMembers)
            .groupBy("userEmailId")
            .map(function(values, key) {

                var teamMatesEmailId = [],
                    teamMatesUserId = [];

                _.each(values,function (va) {
                    teamMatesEmailId.push(va.teammate.emailId)
                    teamMatesUserId.push(va.teammate._id)
                });

                return {
                    userEmailId:key,
                    userId:values[0].userId,
                    teamMatesEmailId:teamMatesEmailId,
                    teamMatesUserId:teamMatesUserId
                }
            })
            .value();

        callback({
            data:listOfMembersData,
            userIds:userIds,
            listOfMembers:listOfMembers
        });
    })
}

router.post('/corporate/admin/product/user/update',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        productHierarchyObj.addUserToHierarchy(req.body,companyProfile._id,common,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/revenue/users/remove',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body.userIds && req.body.userIds.length>0){
                        revenueHierarchyObj.removeFromHierarchy(common.castListToObjectIds(req.body.userIds),companyProfile._id,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/secondary/hierarchy/users/remove',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body.userIds && req.body.userIds.length>0){
                        secondaryHierarchyObj.removeFromHierarchy(common.castListToObjectIds(req.body.userIds),req.body.secondaryHierarchyType,companyProfile._id,common,function (err,result) {
                            var updateObj = {
                                secondaryHierarchyType:req.body.secondaryHierarchyType,
                            };

                            secondaryHierarchyObj.updateOppsForSh(companyProfile._id,common,updateObj,function () {
                                res.send(result)
                            });
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/org/users/remove',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body.userIds && req.body.userIds.length>0){
                        orgHierarchyObj.removeFromHierarchy(common.castListToObjectIds(req.body.userIds),companyProfile._id,function (err,result) {
                            secondaryHierarchyObj.removeAccessToOpps(companyProfile._id,req.body.userIds,'org',common,function () {
                            });
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/product/users/remove',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body.userIds && req.body.userIds.length>0){
                        productHierarchyObj.removeFromHierarchy(common.castListToObjectIds(req.body.userIds),companyProfile._id,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/revenue/set/org/head',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body){
                        var key = "getTeam"+String(companyProfile._id)
                        redisClient.del(key);

                        revenueHierarchyObj.setOrgHead(req.body,common,companyProfile._id,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/secondary/hierarchy/set/org/head',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body){
                        secondaryHierarchyObj.setOrgHead(req.body,common,companyProfile._id,function (err,result) {
                            res.send(result)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/secondary/hierarchy/user/items',common.isLoggedInUser,function(req,res){
    var userId = req.body.userId;
    var shType = req.body.shType;

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var loggedInUserId = common.getUserId(req.user);

                common.validateCorporateAdmin(companyProfile._id,loggedInUserId,function(isAdmin){
                    if(isAdmin && req.body){
                        secondaryHierarchyObj.getSecondaryHierarchyItems(companyProfile._id, userId,shType, function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/org/set/org/head',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body){

                        var key = "getTeam"+String(companyProfile._id)
                        redisClient.del(key);

                        orgHierarchyObj.setOrgHead(req.body,common,companyProfile._id,function (err,result) {
                            getUserHierarchyTeam(req.body.userId,function(hierData){
                                secondaryHierarchyObj.updateOppAccessOrgHierarchy(companyProfile._id,common,req.body,hierData,function () {
                                })
                            });
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/product/set/org/head',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body){
                        productHierarchyObj.setOrgHead(req.body,common,companyProfile._id,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/revenue/update/hierarchy/path',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body){
                        revenueHierarchyObj.updateHierarchyPaths(companyProfile._id,req.body,common,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/update/secondary/hierarchy/path',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body){
                        secondaryHierarchyObj.updateHierarchyPaths(companyProfile._id,req.body,common,function (err,result) {
                            res.send(result)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/org/update/hierarchy/path',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body){
                        orgHierarchyObj.updateHierarchyPaths(companyProfile._id,req.body,common,function (err,result) {
                            getUserHierarchyTeam(userId,function(hierData){
                                secondaryHierarchyObj.updateOppAccessOrgHierarchy(companyProfile._id,common,req.body,hierData,function () {
                                })
                            });
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/product/update/hierarchy/path',common.isLoggedInUser,function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin && req.body){
                        productHierarchyObj.updateHierarchyPaths(companyProfile._id,req.body,common,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/corporate/admin/monthlyTargets',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/memberTargets',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyId:companyProfile._id});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/corporate/admin/oppPropOwner',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/oppOwner',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,companyId:companyProfile._id});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/corporate/admin/getHierarchy',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        userManagements.getHierarchy(companyProfile._id,function(hierarchy){
                            // var userList = [];
                            // _.each(test22byTeam,function (el) {
                            //     userList.push({
                            //         _id: 0,
                            //         id: el._id,
                            //         nameF: el.firstName,
                            //         nameL: el.lastName,
                            //         designation: el.designation,
                            //         email: el.emailId,
                            //         parentName: "test",
                            //         parentId: el.hierarchyParent,
                            //         path: el.hierarchyPath,
                            //         imageUrl: "/profileImages/rahulj1",
                            //         companyName: "$companyName",
                            //         companyId: el.companyId,
                            //         orgHead:el.orgHead
                            //     })
                            // })

                            res.send(hierarchy)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/corporate/admin/send/tasks/by/week',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1,commitDay:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin,adminProfile){

                    if(isAdmin){
                        userManagements.getHierarchy(companyProfile._id,function(hierarchy){
                            var startOFweek = moment().startOf('isoweek');
                            var endOFweek = moment().endOf('isoweek');

                            if(companyProfile.commitDay){
                                startOFweek = moment().day(companyProfile.commitDay);
                                endOFweek = moment(startOFweek).add(7,'days');
                            }

                            taskManagementClassObj.getAllTasksForAllUsers(_.pluck(hierarchy,"email"),startOFweek,endOFweek,function (err,tasks) {

                                var emailAndUserIdObj = {};
                                var teamObj = {}
                                if(!err && hierarchy && hierarchy.length>0){
                                    _.each(hierarchy,function (el) {
                                        teamObj[el.email] = el;
                                        emailAndUserIdObj[el.email] = el.id;
                                    })
                                };

                                var listOfMembers = [];

                                _.each(hierarchy,function (el) {
                                    _.each(hierarchy,function (hi) {
                                        if(_.includes(hi.path,el.id)){
                                            listOfMembers.push({
                                                userId:el.id,
                                                userEmailId:el.email,
                                                teammate:hi
                                            })
                                        }
                                    })
                                });

                                var tasksObj = {};

                                _.each(tasks,function (ta) {

                                    ta.dueDateFormatted = moment(ta.dueDate).format("DD MMM YYYY");

                                    if(tasksObj[ta.assignedToEmailId] && tasksObj[ta.assignedToEmailId].length>0){
                                        tasksObj[ta.assignedToEmailId].push({
                                            taskName:ta.taskName,
                                            // oppValue:Math.random(),
                                            assignedToEmailId:ta.assignedToEmailId,
                                            createdByEmailId:ta.createdByEmailId,
                                            dueDateFormatted:moment(ta.dueDate).format("DD MMM YYYY")
                                        })
                                    } else {
                                        tasksObj[ta.assignedToEmailId] = [];
                                        tasksObj[ta.assignedToEmailId].push({
                                            taskName:ta.taskName,
                                            // oppValue:Math.random(),
                                            assignedToEmailId:ta.assignedToEmailId,
                                            createdByEmailId:ta.createdByEmailId,
                                            dueDateFormatted:moment(ta.dueDate).format("DD MMM YYYY")
                                        })
                                    }
                                })

                                var sumUp = _
                                    .chain(listOfMembers)
                                    .groupBy("userId")
                                    .map(function(values, key) {

                                        var tasksArray = [];
                                        var teamMates = _.pluck(values,"teammate.email");
                                        if(values[0] && values[0].userEmailId){
                                            teamMates.push(values[0].userEmailId)
                                        }

                                        _.each(teamMates,function (va) {
                                            tasksArray.push(tasksObj[va])
                                        });

                                        tasksArray = _.compact(_.flatten(tasksArray));

                                        var tasksGroup = _
                                            .chain(tasksArray)
                                            .groupBy("assignedToEmailId")
                                            .map(function(values_2, key_2) {

                                                var fullName = ""
                                                if(teamObj[key_2].nameF){
                                                    fullName = teamObj[key_2].nameF
                                                }
                                                if(teamObj[key_2].nameL){
                                                    fullName = fullName +" "+ teamObj[key_2].nameL
                                                }

                                                return {
                                                    assignedToEmailId:key_2,
                                                    fullName:fullName,
                                                    tasks:values_2
                                                }
                                            })
                                            .value();

                                        return {
                                            userId:key,
                                            userEmailId:values[0].userEmailId,
                                            tasksGroup:tasksGroup,
                                            allTasks:tasksArray,
                                            teamMates:teamMates
                                        }
                                    })
                                    .value();

                                _.each(sumUp,function (val,index) {
                                    if(val.tasksGroup && val.tasksGroup.length>0){
                                        setTimeout(function () {
                                            emailSenders.sendTaskReport(val.userEmailId,val.tasksGroup,startOFweek,endOFweek)
                                        },index*1)
                                    }
                                })

                                res.send({
                                    sumUp:sumUp,
                                    // tasksObj:tasksObj,
                                    tasks:tasks
                                })
                            })
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/updateHierarchyNew',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        userManagements.updateHierarchyNew(req.body.hierarchy, userId, function(status){
                            res.send(status)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/add/opp/owner',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        userManagements.addOppOwner(common.castToObjectId(req.body.userId),req.body,function(err,status){

                            oppWeightsObj.clearExceptionalAccessCache(req.body.userId,function () {
                            })
                            oppWeightsObj.clearCompanyHierarchyCache(companyProfile._id,function () {
                            })

                            res.send(status)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/set/org/head',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        var key = "getTeam"+String(companyProfile._id)
                        redisClient.del(key);

                        userManagements.setOrgHead(common.castToObjectId(req.body.userId),common.castToObjectId(req.body.companyId),function(err,status){
                            res.send(status)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.post('/corporate/admin/remove/opp/owner',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){

                        var userIds = [];
                        userIds.push(req.body.userId);

                        if(req.body.childrenReset && req.body.childrenReset.length>0){
                            userIds = req.body.childrenReset;
                            userIds.push(req.body.userId)
                        }

                        userManagements.removeOppOwner(common.castListToObjectIds(userIds),req.body,function(err,status){
                            res.send(status)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.redirect('/');
    }
});

router.get('/corporate/admin/create/meetingrooms',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('corporateViews/corporateMeetingRooms',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName,isLoggedIn : common.isLoggedInUserBoolean(req)});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/company/set/fym',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {

                        if(isAdmin){
                            redisClient.del(String(userId)+"fiscalYear");
                            company.setFyMonth(companyProfile._id,req.body.month,function (err,response) {
                                if(response){
                                    // company.updatePortfolioFy(companyProfile._id,fyRange)
                                }
                                res.send(response)
                            })
                        }
                        else res.render('error');
                    })
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/company/set/commitweek',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        
                        company.setCommitWeekday(companyProfile._id,req.body.commitWeekday,req.body.commitWeekHour,function (err,response) {
                            res.send(response)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/company/set/commit/month',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){

                        var updateObj = {
                            monthCommitCutOff:req.body.commitMonthly
                        }

                        company.updateObj(companyProfile._id,updateObj,function (err,response) {
                            res.send(response)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/company/set/commit/qtr',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){

                        var updateObj = {
                            qtrCommitCutOff:req.body.commitQtrly
                        }

                        company.updateObj(companyProfile._id,updateObj,function (err,response) {
                            res.send(response)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/meetingrooms/company/:companyId',common.isLoggedInUser,function(req,res){
    var companyId = req.params.companyId;
    if(common.checkRequired(companyId)){
        userManagements.selectUserProfilesCustomQuery({companyId:companyId,resource:true},{emailId:1,firstName:1,lastName:1,createdDate:1,publicProfileUrl:1},function(error,users){
            res.send(users)
        });
    }
    else res.send(false);
});

router.post('/corporate/admin/create/meetingrooms/new',common.isLoggedInUser,function(req,res){
     var meetingRoom = req.body;
     meetingRoom.uniqueName = common.getValidUniqueUrl(meetingRoom.publicProfileUrl);
    userManagements.addUser(meetingRoom,false,function(error,profile,message){
        if(error){
            res.send(false)
        }
        else{
            res.send(profile)
        }
    })
});

router.get('/corporate/admin/getMembers/company/:companyId',common.isLoggedInUser,function(req,res){
    var companyId = req.params.companyId;
    if(common.checkRequired(companyId)){
        userManagements.selectUserProfilesCustomQuery({companyId:companyId,corporateUser:true,resource:{$ne:true}},{emailId:1,firstName:1,lastName:1,hierarchyParent:1,hierarchyPath:1,userType:1},function(error,users){
            res.send(users)
        });
    }
    else res.send(false);
});

function validateAdmin(adminList,userId){

   if(common.checkRequired(adminList) && adminList.length > 0){
       var isAdmin = false;
       for(var i=0; i<adminList.length; i++){

           if(adminList[i].adminId == userId){
               isAdmin = true;
           }
       }
       return isAdmin;
   }else return false;
}

router.post('/corporate/admin/clearHierarchy',common.isLoggedInUser,function(req,res){
    var companyId = req.body.companyId;

    if(common.checkRequired(companyId)){
        admin.resetHierarchy(companyId,function(result){
            res.send(result);
        })
    }
    else res.send(false);
});

router.get('/corporate/admin/getCompanyList',function(req,res){
    company.getAllUserByCompany(function(companies){
        res.send(companies);
    });
});

router.get('/corporate/company/:companyId/contacts',function(req,res){
    var companyId = req.params.companyId;
    if(common.checkRequired(companyId)){
        company.getCompanyContacts(companyId,function(contacts){
            res.send(contacts);
        })
    }
    else res.send(false);
});

router.get('/corporate/company/:companyId',common.isLoggedInUser,function(req,res){
    var companyId = req.params.companyId;
    if(common.checkRequired(companyId)){
        company.findCompanyById(companyId,function(companyInfo){
            var userId = common.getUserId(req.user);
            fillTargets(companyInfo,common.castToObjectId(userId),function (data) {
                res.send(data);
            })

        })
    }else res.send(false);
});


function fillTargets(companyInfo,userId,callback){

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {

        var fyMonths = common.getMonthsBetweenDates(fyRange.start,fyRange.end);
        var values = [];

        fyMonths.forEach(function (el) {
            values.push({
                date:el,
                amount:0
            })
        });

        var targets = [{
            fy:{
                start:fyRange.start,
                end:fyRange.end
            },
            values:values
        }]

        if(companyInfo.businessUnits && companyInfo.businessUnits.length>0){
           _.each(companyInfo.businessUnits,function (el) {
               if(!el.targets || (el.targets && el.targets.length == 0)){
                   el.targets = targets;
               }
           })
        }

        if(companyInfo.verticalList && companyInfo.verticalList.length>0){
           _.each(companyInfo.verticalList,function (el) {
               if(!el.targets || (el.targets && el.targets.length == 0)){
                   el.targets = targets;
               }
           })
        }

        if(companyInfo.productList && companyInfo.productList.length>0){
           _.each(companyInfo.productList,function (el) {
               if(!el.targets || (el.targets && el.targets.length == 0)){
                   el.targets = targets;
               }
           })
        }

        if(companyInfo.geoLocations && companyInfo.geoLocations.length>0){
           _.each(companyInfo.geoLocations,function (el) {
               if(!el.targets || (el.targets && el.targets.length == 0)){
                   el.targets = targets;
               }
           })
        }

        callback(companyInfo)

    });
}

router.get('/corporate/admin/opportunity/stages/:companyId',common.isLoggedInUser,function(req,res){
    var companyId = req.params.companyId;
    if(common.checkRequired(companyId)){
        company.findCompanyByIdWithProjection(companyId,{opportunityStages:1},function(companyInfo){
            company.getOppsByCompanyId(companyId,common,function (err,oppsCount) {

                companyInfo = JSON.parse(JSON.stringify(companyInfo))

                if(!err && oppsCount && oppsCount.length>0){
                    _.each(companyInfo.opportunityStages,function (stage) {
                        _.each(oppsCount,function (op) {
                            if(op._id == stage.name){
                                stage.oppsCount = op.count?op.count:0
                            }
                        })
                    })
                }

                res.send({
                    companyInfo:companyInfo,
                    oppsCount:oppsCount
                });
            })
        })
    }else res.send(false);
});

router.get('/corporate/company/profile/byUrl',function(req,res){
    var domain = common.getSubDomain(req);

        company.findOrCreateCompanyByUrl(getCompanyObj(domain, req),{admin:0,contacts:0},function (companyProfile) {
            if (companyProfile) {
              res.send(companyProfile)
            }
            else res.send(false);
        });

});

router.post('/corporate/company/update',function(req,res){
    var data = req.body;
    if(common.checkRequired(data) && common.checkRequired(data.data)){
        var updateData = JSON.parse(data.data);

        company.updateCompanyDetails(updateData,function(result){
            
            userManagements.updateCompanyNameForAllUsers(common.castToObjectId(updateData.companyId),updateData.companyName,function () {
            });
            res.send(result);
        })
    }else res.send(false);
});

router.post('/corporate/geolocation/remove',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var regions = req.body.region;
        var companyId = req.body.companyId;
        
        var updateData = {
            region:regions
        }

        if(companyId && updateData){
            company.removeGeoLocation(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);

                removeFromOrgHead(companyId,updateData,function () {
                });
            })
        }
    }else res.send(false);
});

router.post('/corporate/source/remove',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var source = req.body.source;
        var companyId = req.body.companyId;

        var updateData = {
            name:source
        }

        if(companyId && updateData){
            company.removeSource(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/geolocation/add',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var regions = req.body.region;
        var companyId = req.body.companyId;

        var updateData = {
            region:regions
        }

        if(companyId && updateData){
            company.addGeoLocation(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
                addToOrgHead(companyId,updateData,function () {
                });
            })
        }
    }else res.send(false);
});

router.post('/corporate/close/reason/add',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var reason = req.body.reason;
        var companyId = req.body.companyId;

        var updateData = {
            name:reason
        }

        if(companyId && updateData){
            company.addReason(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/opp/email/notif',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var reason = req.body.reason;
        var companyId = req.body.companyId;

        var updateData = {
            notificationForReportingManager:req.body.notificationForReportingManager,
            notificationForOrgHead:req.body.notificationForOrgHead
        }

        if(companyId && updateData){
            company.updateObj(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/admin/renewalStatus',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var companyId = req.body.companyId;

        var updateData = {
            "opportunitySettings":{renewalStatusRequired:req.body.renewalStatus}
        }

        if(companyId && updateData){
            company.updateObj(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/admin/update/opps/stage',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var reason = req.body.reason;
        var companyId = req.body.companyId;

        var updateData = {
            relatasStage:req.body.stage,
            stageName:req.body.stage
        }

        if(companyId && updateData){
            company.updateOpps(common.castToObjectId(companyId.toString()),req.body.fromStage,updateData,function(err,result){
                res.send(result);
            })
        } else {
            res.send(false)
        }
    }else res.send(false);
});

router.post('/corporate/close/reason/remove',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var reason = req.body.name;
        var companyId = req.body.companyId;

        var updateData = {
            name:reason
        }

        if(companyId && updateData){
            company.removeCloseReason(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/solution/remove',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var companyId = req.body.companyId;

        var updateData = {
            name:req.body.name
        }

        if(companyId && updateData){
            company.removeSolution(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/bu/remove',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var companyId = req.body.companyId;

        var updateData = {
            name:req.body.name
        }

        if(companyId && updateData){
            company.removeBusinessUnit(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/type/remove',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var companyId = req.body.companyId;

        var updateData = {
            name:req.body.name
        }

        if(companyId && updateData){
            company.removeType(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/product/remove',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var product = req.body.product;
        var companyId = req.body.companyId;

        var updateData = {
            name:product
        }

        if(companyId && updateData){
            company.removeProductTotList(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);

                removeFromOrgHead(companyId,{product:product},function () {
                });
            })
        }
    }else res.send(false);
});

router.post('/corporate/vertical/remove',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var vertical = req.body.vertical;
        var companyId = req.body.companyId;

        var updateData = {
            name:vertical
        }

        if(companyId && updateData){
            company.removeFromVerticalList(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);

                removeFromOrgHead(companyId,{vertical:vertical},function () {
                });
            })
        }
    }else res.send(false);
});

router.post('/corporate/document/category/modify',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var companyId = req.body.companyId;

        var updateData = {
            name:req.body.name,
            active:req.body.active
        }

        if(companyId && updateData){
            company.modifyDocumentCategory(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/product/add',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var product = req.body.product;
        var companyId = req.body.companyId;

        var updateData = {
            name:product
        }

        if(companyId && updateData){
            company.addProductTotList(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);

                addToOrgHead(companyId,{product:product},function () {
                });
            })
        }
    }else res.send(false);
});

router.post('/corporate/source/add',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var source = req.body.source;
        var companyId = req.body.companyId;

        var updateData = {
            name:source
        }

        if(companyId && updateData){
            company.addSourceList(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/solution/add',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var source = req.body.name;
        var companyId = req.body.companyId;

        var updateData = {
            name:source
        }

        if(companyId && updateData){
            company.addSolutionList(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/type/add',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var companyId = req.body.companyId;

        var updateData = {
            name:req.body.name
        }

        if(companyId && updateData){
            company.addTypeList(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/bu/add',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var companyId = req.body.companyId;

        var updateData = {
            name:req.body.name
        }

        if(companyId && updateData){
            company.addBusinessUnit(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/vertical/add',common.isLoggedInUser,function(req,res){
    

    if(common.checkRequired(req.body)){
        var vertical = req.body.vertical;
        var companyId = req.body.companyId;

        var updateData = {
            name:vertical
        }

        if(companyId && updateData){
            company.addVerticalTotList(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
                addToOrgHead(companyId,{vertical:vertical},function () {
                });
            })
        }
    }else res.send(false);
});

router.post('/corporate/document/category/add',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var companyId = req.body.companyId;

        var updateData = {
            name:req.body.name,
            active:true
        }

        if(companyId && updateData){
            company.addDocumentCategory(common.castToObjectId(companyId),updateData,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/currency/update',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){

                        if(req.body){
                            var currency = {
                                name:req.body.name,
                                symbol:req.body.symbol,
                                isPrimary:req.body.isPrimary,
                                xr:parseFloat(req.body.xr)
                            };
                            company.updateCurrency(common.castToObjectId(companyProfile._id),currency,function(err,result){
                                res.send(result);
                            })
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/update/primary/currency',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            company.updatePrimaryCurrency(common.castToObjectId(companyProfile._id),req.body,common.castToObjectId,function(err,result){
                                res.send(result);
                            })
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/update/customer/setting',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            company.updateCustomerSettings(common.castToObjectId(companyProfile._id),req.body,function(err,result){
                                res.send(result);
                            })
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/update/default/type',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            company.updateDefaultType(common.castToObjectId(companyProfile._id),req.body,common.castToObjectId,function(err,result){
                                res.send(result);
                            })
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/update/renewal/type',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            company.setRenewalType(common.castToObjectId(companyProfile._id),req.body,common.castToObjectId,function(err,result){
                                res.send(result);
                            })
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/update/xr/currency',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            company.updateXrCurrency(common.castToObjectId(companyProfile._id),[req.body],common.castToObjectId,function(err,result){
                                res.send(result);
                            })
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/delete/currency',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){
                            company.deleteCurrency(common.castToObjectId(companyProfile._id),req.body.symbol,common.castToObjectId,function(err,result){
                                res.send(result);
                            })
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/netgrossmargin/required',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var netGrossMargin = req.body.required;
        var companyId = req.body.companyId;
        
        if(companyId && netGrossMargin){
            company.netGrossMarginUpdate(common.castToObjectId(companyId),netGrossMargin,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/admin/update/opp/settings/required',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        if(req.body){

                            var keys = Object.keys(req.body)
                            var updateObj = {
                            }

                            updateObj["opportunitySettings."+keys[0]] = req.body[keys[0]]

                            company.updateObj(common.castToObjectId(companyProfile._id),updateObj,function(err,result){
                                res.send(result);
                            })
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/company/add/salesforce',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var clientId = req.body.clientId;
        var clientSecret = req.body.clientSecret;
        var companyId = req.body.companyId;

        if(companyId && clientId && clientSecret){
            company.updateSalesforceSettings(common.castToObjectId(companyId),{clientId:clientId,clientSecret:clientSecret},function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/rweblead/messages',common.isLoggedInUser,function(req,res){

    if(common.checkRequired(req.body)){
        var companyId = req.body.companyId;
        var updateObj = req.body.updateObj;

        if(companyId){
            company.setTitlesForWidget(common.castToObjectId(companyId),updateObj,function(err,result){
                res.send(result);
            })
        }
    }else res.send(false);
});

router.post('/corporate/company/add/video',function(req,res){
    var data = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(common.checkRequired(data) && common.checkRequired(data.companyId) && common.checkRequired(data.url)){
        var videos = [{
            url:data.url,
            awsKey:data.awsKey,
            videoName:data.videoName,
            sourceType:data.sourceType,
            uploadedOn:new Date(),
            uploadedBy:userId
        }];

        company.updateCompanyVideo(data.companyId,videos,function(result){
            res.send(result);
        })
    }else res.send(false);
});

router.post('/corporate/company/add/logo',function(req,res){
    var data = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(common.checkRequired(data) && common.checkRequired(data.companyId) && common.checkRequired(data.imageName) && common.checkRequired(data.url)){
        var logo = {
            url:data.url,
            awsKey:data.awsKey,
            imageName:data.imageName,
            sourceType:data.sourceType,
            uploadedOn:new Date(),
            uploadedBy:userId
        };

        company.updateCompanyLogo(data.companyId,logo,function(result){
            res.send(result);
        })
    }else res.send(false);
});

router.post("/corporate/region/required",function (req,res) {

    var required = false;
    if(req.body.required === 'true' || req.body.required === true){
        required = true;
    }

    company.updateCompanyRegionRequired(common.castToObjectId(req.body.companyId),required,function(result){
        res.send(result);
    })
});

router.post("/corporate/product/required",function (req,res) {

    var required = false;
    if(req.body.required === 'true' || req.body.required === true){
        required = true;
    }

    company.updateCompanyProductRequired(common.castToObjectId(req.body.companyId),required,function(result){
        res.send(result);
    })
});

router.post("/corporate/bu/required",function (req,res) {

    var required = false;
    if(req.body.required === 'true' || req.body.required === true){
        required = true;
    }

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        company.updateCompanyBuRequired(common.castToObjectId(companyProfile._id),required,function (err,result) {
                            res.send(true)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post("/corporate/source/required",function (req,res) {

    var required = false;
    if(req.body.required === 'true' || req.body.required === true){
        required = true;
    }

    company.updateCompanySourceRequired(common.castToObjectId(req.body.companyId),required,function(result){
        res.send(result);
    })
});

router.post("/corporate/vertical/required",function (req,res) {

    var required = false;
    if(req.body.required === 'true' || req.body.required === true){
        required = true;
    }

    company.updateCompanyVerticalRequired(common.castToObjectId(req.body.companyId),required,function(result){
        res.send(result);
    })
});

router.post('/corporate/company/add/document',function(req,res){
    var data = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(common.checkRequired(data) && common.checkRequired(data.companyId) && common.checkRequired(data.url) && common.checkRequired(data.awsKey)){
        var doc = {
            url:data.url,
            awsKey:data.awsKey,
            documentName:data.documentName,
            uploadedOn:new Date(),
            uploadedBy:userId
        };

        company.addCompanyDocument(data.companyId,doc,function(result){
            res.send(result);
        })
    }else res.send(false);
});

router.post('/corporate/company/add/slide',function(req,res){
    var data = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(common.checkRequired(data) && common.checkRequired(data.companyId) && common.checkRequired(data.url)){
        var slide = {
            url:data.url,
            awsKey:data.awsKey,
            slideName:data.slideName,
            sourceType:data.sourceType,
            uploadedOn:new Date(),
            uploadedBy:userId
        };

        company.addCompanySlide(data.companyId,slide,function(result){
            res.send(result);
        })
    }else res.send(false);
});

router.post('/corporate/company/add/contact',function(req,res){
    var data = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(common.checkRequired(data) && common.checkRequired(data.companyId) && common.checkRequired(data.uniqueName_emailId) && common.checkRequired(data.contactFor)){
        var query = {
              $or:[{emailId:data.uniqueName_emailId},{publicProfileUrl:data.uniqueName_emailId}]
        };
        userManagements.selectUserProfileCustomQuery(query,{emailId:1,firstName:1,lastName:1,designation:1,publicProfileUrl:1},function(error,profile){
            if(!error && common.checkRequired(profile)){
                 var contact = {
                       userId:profile._id,
                       firstName:profile.firstName,
                       lastName:profile.lastName,
                       designation:profile.designation,
                       uniqueName:profile.publicProfileUrl,
                       emailId:profile.emailId,
                       contactFor:data.contactFor,
                       addedOn:new Date(),
                       addedBy:userId
                 };

                if(common.checkRequired(contact.userId) && common.checkRequired(contact.emailId)){
                    company.addCompanyContact(data.companyId,contact,function(result){
                        res.send(result);
                    })
                }
                else res.send(false);
            }
            else{
                res.send(false);
            }
        })
    }else res.send(false);
});

router.post('/corporate/admin/updateAdmin',function(req,res){
    var data = req.body;
    if(common.checkRequired(data.userId) && common.checkRequired(data.companyId)){
        var admin = [
            {
                adminId:data.userId,
                adminType:'root',
                addedOn:new Date()
            }
        ];
        company.updateCompanyAdmin(admin,data.companyId,function(response){

            if(response){
                logCompanyAdminUpdate(data)
            }
            res.send(response);
        })
    }else res.send(false);
});

function logCompanyAdminUpdate(obj){
   company.findCompanyById(obj.companyId,function(companyDetails){
       userManagements.findUserProfileByIdWithCustomFields(obj.userId,{firstName:1,lastName:1,emailId:1},function(error,profile){
           if(!error && common.checkRequired(companyDetails) && common.checkRequired(profile)){
               var logObj = {
                   companyName:companyDetails.companyName,
                   companyId:companyDetails._id.toString(),
                   adminName:profile.firstName+' '+profile.lastName,
                   adminEmail:profile.emailId,
                   dateCreated:new Date()
               }
               adminChangeLog.info(logObj);
           }
       })
   })
}

router.post('/corporate/admin/updateHierarchy',common.isLoggedInUser,function(req,res){

    var info = req.body;
    if(common.checkRequired(info.userId) && common.checkRequired(info.parentId) && common.checkRequired(info.companyId)){
        admin.updateAssigned(info,function(response){
            if(response){
                var tokenSecret = "alpha";
                var decoded = jwt.decode(req.user ,tokenSecret);
                var userId = decoded.id;

                logHierarchyUpdate(info.userId,info.parentId,userId,info.oldParent,info.action,info.companyId);
            }

            res.send(response);
        });
    }
});

function logHierarchyUpdate(userId,parentId,adminId,oldParent,action,companyId){

    var arr = [userId,parentId,adminId]
    if(common.checkRequired(oldParent) && oldParent != 'none'){
        arr.push(oldParent);
    }
    var companyLogger = logLib.getDynamicLogger(companyId+'.log');
   userManagements.getUserListByIdRequestedFields(arr,{emailId:1,firstName:1,lastName:1},function(userList){
       if(common.checkRequired(userList) && userList.length > 0){

           var logObj = {
               action:action
           };
           logObj.editDate = new Date();

           for(var user=0; user<userList.length; user++){
               if(userList[user]._id == userId){
                    logObj.corporateEmailId = userList[user].emailId;
                    logObj.corporateName = userList[user].firstName+' '+userList[user].lastName;
               }
               if(userList[user]._id == parentId){
                   logObj.changedTo = userList[user].emailId;
               }
               if(userList[user]._id == adminId){
                    logObj.changedBy = userList[user].emailId;
               }
               if(userList[user]._id == oldParent){
                    logObj.changedFrom = userList[user].emailId;
               }
           }
            if(oldParent == 'none'){
                logObj.changedFrom = 'none';
            }
          companyLogger.info(logObj)
       }
   })
}

router.get('/corporate/admin/log/:companyId',function(req,res){
    if(common.checkRequired(req.params.companyId)){
        res.render('corporateViews/corporateLogs');
    }
});

router.get('/corporate/admin/log/:companyId/getLog',function(req,res){
    if(common.checkRequired(req.params.companyId)){
        var corporateLogger = logLib.getDynamicLogger(req.params.companyId+'.log');
        if(common.checkRequired(corporateLogger)){

            corporateLogger.query({}, function (err, results) {
                if (err) {

                }
                res.send(results);
            });
        }
    }
});

router.get('/get/all/team',common.isLoggedInUser,function(req,res){

    var companyId = req.query.companyId;

    userManagements.getTeamMembers(common.castToObjectId(companyId),function (err,team) {
        res.send(team)
    })
});

router.get('/corporate/admin/all/team/members',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        userManagements.getTeamMembers(common.castToObjectId(companyProfile._id),function (err,team) {
                            res.send(team)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/fetch/opps/for/users',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        oppManagementObj.opportunitiesForUser(common.castToObjectId(req.query.userId),common.castToObjectId(companyProfile._id),function (err,opps) {
                            res.send(opps);
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/admin/delete/opps',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    var userProfile = common.getUserFromSession(req);
                    if(isAdmin && userProfile && userProfile.emailId){
                        // userId,companyId,oppIds,deletedByEmailId
                        oppManagementObj.deleteMultipleOpps(common.castToObjectId(companyProfile._id),req.body,userProfile.emailId,function (err,opps) {
                            var redisKey = String(companyProfile._id)+"oppsForReports";
                            var filterKey = String(userId)+"filtersApplied"

                            redisClient.del(filterKey);
                            redisClient.del(redisKey);

                            res.send(opps);
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/admin/transfer/opps',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    var userProfile = common.getUserFromSession(req);
                    if(isAdmin && userProfile && userProfile.emailId){
                        if(req.body.toTransfer && req.body.toTransfer.length>0) {

                            req.body.toTransfer.forEach(function (el) {
                                el.transferredBy = userProfile.emailId
                            })

                            oppManagementObj.transferOpp(common.castToObjectId(userId), req.body.toTransfer,common,common.castToObjectId(companyProfile._id),function (err, result) {
                                res.send("Ok")
                            })
                        }
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/admin/role/update',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        orgRolesObj.addOrUpdateRole(common.castToObjectId(companyProfile._id),req.body,common.castToObjectId,function (err,result) {
                            res.send(true);
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/admin/role/remove',common.isLoggedInUser,function(req,res){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        orgRolesObj.deleteRole(common.castToObjectId(companyProfile._id),req.body.roleName,function (err,result) {
                            res.send(true);
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.get('/corporate/admin/user/reports', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        res.render('adminRelatas/userReport/userReport',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
});

router.post('/corporate/admin/portfolio/save', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        company.updatePortfolio(common.castToObjectId(companyProfile._id),req.body,common.castToObjectId,function (err,result) {
                            res.send(result)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/portfolio/required', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        company.portfolioRequired(common.castToObjectId(companyProfile._id),req.body,function (err,result) {
                            res.send(result)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/portfolio/delete', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        company.deletePortfolio(common.castToObjectId(companyProfile._id),req.body,function (err,result) {
                            res.send(result)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.get('/corporate/admin/portfolio/logs', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        adminLogsCollection.find({companyId:common.castToObjectId(companyProfile._id),currentSelection:req.query.currentSelection}).exec(function (err,logs) {
                            res.send(logs);
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/portfolio/logs', isLoggedInUser, function(req, res) {
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){

                        if(req.body && req.body.length>0){
                            req.body.forEach(function (el) {
                                el.companyId = common.castToObjectId(companyProfile._id);
                                el.date = el.date?new Date(el.date): new Date();

                                if(el.type == "Sync Targets"){
                                    el.date = moment().add(3,"minutes")
                                }
                            });

                            adminLogsCollection.collection.insert(req.body)
                        }

                        res.send(true);
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }
})

router.post('/corporate/admin/portfolio/required', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        company.deletePortfolio(common.castToObjectId(companyProfile._id),req.body,function (err,result) {
                            res.send(result)
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/portfolio/create', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        generateTemplate(userId,req.body.data,common.castToObjectId(companyProfile._id),function(data){
                            company.createPortfolio(common.castToObjectId(companyProfile._id),data,function (err,result) {
                                res.send(true)
                            })
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

function generateTemplate(userId,data,companyId,callback){

    common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {
        var fyMonths = common.getMonthsBetweenDates(fyRange.start,fyRange.end);
        var values = [];

        fyMonths.forEach(function (el) {
            values.push({
                date:el,
                amount:0
            })
        })

        var newData = {
            companyId:companyId,
            name:data,
            list:[{
                name: "Others",
                head:null,
                headEmailId:null,
                targets: [{
                    fy:{
                        start:fyRange.start,
                        end:fyRange.end
                    },
                    fyEmailId:null,
                    values:values
                }]
            }]
        }
        callback(newData);
    });

}

router.get('/corporate/admin/portfolio/all', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        company.getAllPortfolios(common.castToObjectId(companyProfile._id),function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/portfolio/get/by/type', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        company.getAllPortfoliosByTypeAndName(common.castToObjectId(companyProfile._id),req.body.name,req.body.type,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/portfolio/sub/save', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        company.savePortfoliosForUsers(common.castToObjectId(companyProfile._id),req.body,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.get('/corporate/admin/get/user/targets', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {
                            oppManagementObj.getTargetsByUsers([common.castToObjectId(req.query.userId)],
                                fyRange.start,
                                fyRange.end,
                                fyRange.start,fyRange.end,function(err,targets){
                                res.send(targets)
                            });
                        });
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.post('/corporate/admin/portfolio/sub/delete', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        company.deletePortfoliosByUser(common.castToObjectId(companyProfile._id),req.body,function (err,result) {
                            res.send(result)
                        })
                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.get('/corporate/admin/portfolio/targets', isLoggedInUser, function(req, res) {

    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,admin:1,companyName:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                var userId = common.getUserId(req.user);
                common.validateCorporateAdmin(companyProfile._id,userId,function(isAdmin){
                    if(isAdmin){
                        common.userFiscalYear(userId,function (err,fyMonth,fyRange,allQuarters) {
                            company.getAllPortfoliosTargetsByType(common.castToObjectId(companyProfile._id),req.query.type,req.query.forHead,fyRange.start,fyRange.end,function (err,result) {
                                res.send({
                                    targets:result,
                                    fy:fyRange
                                })
                            })
                        });

                    }
                    else res.render('error');
                });
            }
            else res.render('error');
        });
    }
    else{
        res.render('error');
    }

});

router.get('/admin/getUserReport', isLoggedInUser, function(req, res) {
    var companyId = req.query.companyId;
    userReports(companyId,false,function (err,data) {
        res.send(data)
    });

});

router.get('/admin/getUserReport/csv', isLoggedInUser, function(req, res) { // to get all interactions
    var companyId = req.query.companyId;
    userReports(companyId,true,function (err,data) {
        res.xls('user_report.xlsx', data);
    });
});

function userReports(companyId,toCsv,callback) {

    userManagements.getTeamDataForUserReport(common.castToObjectId(companyId),function (err,teamMembers) {

        var userIds = _.pluck(teamMembers,"_id");
        var data = [];
        oppManagementObj.opportunitiesForUsers(userIds,null,null,null,null,null,null,null,null,common.castToObjectId(companyId),null,null,function (err,opps) {

            var obj = {};

            _.each(opps,function (op) {
                obj[op._id] = op.opportunities;
            });

            _.each(teamMembers,function (tm) {
                var totalOpps = 0,totalOppsValue = 0
                if(obj[tm.emailId]){
                    _.each(obj[tm.emailId],function (op) {
                        totalOpps++;
                        totalOppsValue = totalOppsValue+parseFloat(op.amount);
                    })
                }

                if(toCsv){

                    data.push({
                        "Email Id":tm.emailId,
                        "Full name":tm.firstName +" "+tm.lastName,
                        "Designation":tm.designation,
                        "Location":tm.location,
                        // "Partial registration":tm.createdDate,
                        "Registration Date":tm.registeredDate,
                        // "Last Login Date":tm.lastLoginDate,
                        "Total Opps created till date":totalOpps,
                        "Total Opps Value till date":totalOppsValue,
                        "Mobile login Date": tm.lastMobileSyncDate,
                        "Mobile App Version":tm.mobileAppVersion
                    })

                } else {

                    data.push({
                        emailId:tm.emailId,
                        firstName:tm.firstName,
                        lastName:tm.lastName,
                        designation:tm.designation,
                        location:tm.location,
                        createdDate:tm.createdDate,
                        registeredDate:tm.registeredDate,
                        lastLoginDate:tm.lastLoginDate,
                        orgHead:tm.orgHead?tm.orgHead:false,
                        totalOpps:totalOpps,
                        totalOppsValue:totalOppsValue,
                        lastMobileLoginDate:tm.lastMobileSyncDate,
                        mobileAppVersion:tm.mobileAppVersion
                    })
                }

            });

            callback(err,data)
        })
    });

}

function getCompanyObj(companyName,req){
    var company = {
        url:req.headers.host,
        companyName:companyName
    }
    return company;
}

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }else
    {

        // if they aren't redirect them to the home page

        return res.redirect('/');
    }
}

function addToOrgHead(companyId,updateData,callback) {

    //This function adds the access control to the
    // company head as they have access to everything by default..
    var obj = {
        type:"",
        name:""
    }

    if(updateData["region"]){
        obj.type = "regionOwner"
        obj.name = updateData.region
    }
    if(updateData["product"]){
        obj.type = "productTypeOwner"
        obj.name = updateData.product
    }
    if(updateData["vertical"]){
        obj.type = "verticalOwner"
        obj.name = updateData.vertical
    }

    // company.getOrgHead(common.castToObjectId(companyId),function (err1,orgHead) {
    //     userManagements.addOppOwner(orgHead[0]._id,obj,function(err,status){
    //
    //     });
    // });
}

function removeFromOrgHead(companyId,updateData,callback) {

    //This function removes the access control from the
    // user collection that is used by the queries in the program.
    // If no item exists at company level, then it will not exist at individual level
    var obj = {
        type:"",
        name:""
    }

    if(updateData["region"]){
        obj.type = "regionOwner"
        obj.name = updateData.region
    }
    if(updateData["product"]){
        obj.type = "productTypeOwner"
        obj.name = updateData.product
    }
    if(updateData["vertical"]){
        obj.type = "verticalOwner"
        obj.name = updateData.vertical
    }

    company.getOrgHead(common.castToObjectId(companyId),function (err1,orgHead) {
        userManagements.removeOppOwner(orgHead[0]._id,obj,function(err,status){

        });
    });
}

module.exports = router;
