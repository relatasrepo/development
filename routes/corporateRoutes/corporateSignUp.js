var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var passport = require('passport');
var crypto = require('crypto');
var fs = require('fs');
var request=require('request');
var geoip = require('geoip-lite');
var corporateSecrete = 'corporate-secrete-relatas';

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var corporateClass = require('../../dataAccess/corporateDataAccess/corporateModelClass');
var companyClass = require('../../dataAccess/corporateDataAccess/companyModelClass');
var emailSender = require('../../public/javascripts/emailSender');
var appCredentials = require('../../config/relatasConfiguration');
var winstonLog = require('../../common/winstonLog');
var validations = require('../../public/javascripts/validation');
var IPLocatorClass = require('../../common/ipLocator');

var logLib = new winstonLog();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var domainName = domain.domainName;

var IPLocator = new IPLocatorClass();
var loggerError = logLib.getCorporateErrorLogger();
var logger = logLib.getCorporateLogger();
var common = new commonUtility();
var corporate = new corporateClass();
var company = new companyClass();
var emailSenders = new emailSender();
var userManagements = new userManagement();
var validation = new validations();

router.post('/corporate/generateKey',function(req,res){
    var obj = req.body;

    if(common.checkRequired(obj.emailId) && common.checkRequired(obj.companyId)){
        var token = jwt.encode({ id: obj.emailId}, corporateSecrete);
        checkCorporateUser(obj,token,req,res);
    }
    else res.send(false);

});


function checkCorporateUser(details,token,req,res){
  var user = {
       emailId:details.emailId,
       token:token,
       companyId:details.companyId,
       firstName:details.firstName,
       lastName:details.lastName
  };

    corporate.checkCorporateUserExist(user,function(profile){
        if(profile == 'exist'){

            res.send('exist');
        }
        else if(profile != false){

            profile.url = req.headers.host;
            emailSenders.sendCorporateTokenEmail(profile);
            res.send(true);
        }
    })
}

function updateCorporatePartialAccount(userObj,callback){
   if(common.checkRequired(userObj)){
        var obj = {
            userId:userObj._id,
            emailId:userObj.emailId,
            firstName:userObj.firstName,
            lastName:userObj.lastName,
            active:true
        };
       corporate.updatePartialAccount(obj,callback);
   }
   else callback(false);
}

function findOrCreateCorporateUser(companyId,profile,updateUser,callback){

        if(common.checkRequired(profile)){
                        var obj = {
                             userId:profile._id,
                             emailId:profile.emailId,
                             firstName:profile.firstName,
                             lastName:profile.lastName,
                             companyId:companyId
                        };
            corporate.findOrCreateCorporateUser(obj,function(result){

                if(result){
                    if(updateUser){
                        userManagements.updateUserToCorporate(result.userId,result.companyId,function(error,flag,msg){
                            if(error){
                                loggerError.info('Error occurred while updating user to corporate. '+JSON.stringify(error));
                            }

                            callback(result);
                        });
                    }
                    else{
                        callback(result);
                    }

                    // login process
                }else callback(false);
            })
        }else{
            callback(false);
        }
}
/*
router.get('/login',function(req,res){
    var subDomain = common.getSubDomain(req);
    if(subDomain){
        company.findOrCreateCompanyByUrl(getCompanyObj(subDomain,req),function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                res.render('corporateViews/corporateLogin',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
            }
            else res.render('error');
        });

    }else res.render('error');
});*/

router.get('/signUp/linkedin',function(req,res){

    var domain = common.getSubDomain(req);
    var signUpType = req.params.signUpType;
    if(domain){
        if(!isLoggedInUser(req,res)){
            company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,companyName:1},function(companyProfile){
                req.session.companyId = companyProfile._id;
                res.render('corporateViews/corporateLinkedinSignUp',{signUpType:signUpType,company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
            });
        }else res.redirect('/');

    }else res.render('error');
});

router.get('/signUp/google',function(req,res){

    var domain = common.getSubDomain(req);
    var signUpType = req.params.signUpType;
    if(domain){
        if(!isLoggedInUser(req,res)){
            company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,companyName:1},function(companyProfile){
                req.session.companyId = companyProfile._id;
                res.render('corporateViews/corporateGoogleSignUp',{signUpType:signUpType,company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
            });
        }else res.redirect('/');

    }else res.render('error');
});

router.get('/signUp/office',function(req,res){

    var domain = common.getSubDomain(req);
    if(domain){
        if(!isLoggedInUser(req,res)){
            company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,companyName:1},function(companyProfile){
                req.session.companyId = companyProfile._id;
                res.render('corporateViews/corporateOfficeSignUp',{company:JSON.stringify(companyProfile),companyName:companyProfile.companyName});
            });
        }else res.redirect('/');

    }else res.render('error');
});

router.get('/corporate/auth/:emailId/:companyId/updatePartial',function(req,res){

    if(common.checkRequired(req.params.companyId)){
        authenticateUser(req.params.emailId,req.params.companyId,false,true,req,res);
    }
    else{
        var domain = common.getSubDomain(req);
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                authenticateUser(req.params.emailId,companyProfile._id,false,true,req,res);
            }
            else res.render('error');
        });
    }
});

router.get('/corporate/auth/:emailId',function(req,res){
    if(common.checkRequired(req.session.companyId)){
        authenticateUser(req.params.emailId,req.session.companyId,false,false,req,res);
    }
    else{
        var domain = common.getSubDomain(req);
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                authenticateUser(req.params.emailId,companyProfile._id,false,false,req,res);
            }
            else res.render('error');
        });
    }
});

router.get('/corporate/auth/:emailId/updateUser',function(req,res){
    if(common.checkRequired(req.session.companyId)){
        authenticateUser(req.params.emailId,req.session.companyId,true,false,req,res);
    }
    else{
        var domain = common.getSubDomain(req);
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1},function(companyProfile){
            if(companyProfile){
                req.session.companyId = companyProfile._id;
                authenticateUser(req.params.emailId,companyProfile._id,true,false,req,res);
            }
            else res.render('error');
        });
    }
});

function authenticateUser(emailId,companyId,updateUser,updatePartialAcc,req,res){
    userManagements.findUserProfileByEmailIdWithCustomFields(emailId,{contacts:0},function(error,userData){
        if(!error || common.checkRequired(userData)){
            if(updatePartialAcc){

                updateCorporatePartialAccount(userData,function(response){
                    if(response){
                        var tokenSecret ="alpha";
                        var token = jwt.encode({ id: userData._id}, tokenSecret);
                        req.logIn(token, function(err) {
                            var pageName = 'corporate-landing-page';
                            common.insertLog(userData,pageName,req);
                            // res.send(true);
                            res.redirect('/preOnBoarding');
                        })
                    }
                })
            }
            else{
                findOrCreateCorporateUser(companyId,userData,updateUser,function(corporate){
                    if(common.checkRequired(corporate)){
                        var tokenSecret ="alpha";
                        var token = jwt.encode({ id: userData._id}, tokenSecret);
                        req.logIn(token, function(err) {
                            var pageName = 'corporate-landing-page';
                            common.insertLog(userData,pageName,req);
                            // res.send(true);
                            res.redirect('/finalPage');
                        })
                    }else res.render('error');
                })
            }
        }else res.render('error');
    })
}

function loginCorporateUser(emailId,req,res,companyId){
    userManagements.findUserProfileByEmailIdWithCustomFields(emailId,{contacts:0},function(error,userData){
        if(!error || common.checkRequired(userData)){

                if(userData.corporateUser && userData.companyId == companyId){
                    var tokenSecret ="alpha";
                    var token = jwt.encode({ id: userData._id}, tokenSecret);
                    req.logIn(token, function(err) {
                        var pageName = 'corporate-landing-page';
                        common.insertLog(userData,pageName,req);
                        // res.send(true);
                        res.redirect('/today/dashboard');
                    })
                }
                else{
                    req.session.corporateLinkedInError = 'notCorporateUser';
                    res.redirect('/');
                }
        }
    })
}

/*
*
* Authentication
* */

router.get('/corporate/error/companyProfile',function(req,res){
  var companyId = req.session.corporateErrCompanyId;
  if(common.checkRequired(companyId)){
      company.findCompanyById(companyId,function(cProfile){
           res.send(cProfile);
      })
  }else res.send(false);
});

router.get('/corporate/linkedinError/get',function(req,res){
    var flag = req.session.corporateLinkedInError;
    delete req.session.corporateLinkedInError;
    res.send(flag);
});

router.get('/corporate/linkedinError/clear',function(req,res){
    delete req.session.corporateLinkedInError;
    res.send(true);
});

router.get('/corporate/linkedin/getSession',function(req,res){
    res.send(req.session.linkedinUser);
});

router.get('/corporate/google/getSession',function(req,res){
    res.send(req.session.googleUser);
});

router.get('/corporate/googleLogin/getSession',function(req,res){
    res.send(req.session.corpGoogleUserLogin);
});

router.post('/corporate/signUp/storeSession',function(req,res){
    req.session.corporateSignUpSession = req.body;
    res.send(true);
});

router.get('/corporate/signUp/getSession',function(req,res){
    res.send(req.session.corporateSignUpSession)
});

router.get('/corporate/signUp/clearSession',function(req,res){
     delete req.session.corporateSignUpSession;
    res.send(true);
});

router.get('/corporate/getCompanyName',function(req,res){
    var companyId = common.getSubDomain(req);
    company.findOrCreateCompanyByUrl(getCompanyObj(companyId,req),{emailDomains:1,companyName:1,url:1},function(companyProfile){
          res.send(companyProfile);
    });
});

router.post('/corporate/registration', function(req, res)
{
    try
    {
        var emailId;
        try{
            var decoded = jwt.decode(req.body.key,corporateSecrete);
             emailId = decoded.id;

            if(emailId != req.body.emailId){

                 return res.send('invalidKey');
            }
        }
        catch(exe){
            return res.send('invalidKey');
        }
        validation.RegistrationThirdParty(req.body,function(status){
            if(status == true){
                logger.info('validation completed');
                var userManagements = new userManagement();
                var userProfiles = req.body;

               /* var ipAddr = getClientIp(req);
                var geo = geoip.lookup(ipAddr.toString());
                var currentLocation = getCurrentLocation(geo);
                userProfiles.currentLocation = currentLocation;

                if(!common.checkRequired(userProfiles.location)){
                    userProfiles.location = common.checkRequired(currentLocation) ? currentLocation.city : '';
                }*/
                if(userProfiles.profilePicUrl.charAt(0) == 'h'){
                    storeProfileImg(userProfiles.profilePicUrl,userProfiles.screenName);
                    userProfiles.profilePicUrl = '/profileImages/'+userProfiles.screenName;
                }

                var dataGoogle=JSON.parse(userProfiles.google);
                var dataLinkedin =JSON.parse(userProfiles.linkedin);
                var dataFacebook = JSON.parse(userProfiles.facebook);
                var dataTwitter = JSON.parse(userProfiles.twitter);

                userProfiles.google=dataGoogle;
                userProfiles.linkedin=dataLinkedin;
                userProfiles.facebook=dataFacebook;
                userProfiles.twitter=dataTwitter;

                if(userProfiles.profilePrivatePassword != ''){
                    userProfiles.profilePrivatePassword = EncryptionValue(userProfiles.profilePrivatePassword);
                }

                if(userProfiles.serviceLogin == 'office'){
                    userProfiles.officeData = JSON.parse(userProfiles.officeData);
                }

                 userManagements.addUserThirdParty(userProfiles,function(error,userDataTobeSend,message)
                {
                    if (error) {
                        logger.info('Error occurred while writing user data in DB (googleLogin)');
                        res.redirect('/error');
                    }
                    if (message.message == 'userExist') {
                        res.send('<html><head><script>alert("User already exist");window.location.replace("/")</script></head></html>');
                    }
                    else{
                        updateCurrentLocation(getClientIp(req),userDataTobeSend._id);
                        var emailSenders = new emailSender();
                        req.session.userId = userDataTobeSend._id;
                        emailSenders.sendRegistrationConfirmationMail(userDataTobeSend);

                        res.send(true);

                    }
                });
            }
            else{
                logger.info("user credential is invalid");
                res.send(status);
            }
        });
    }
        //TODO: send the correct error message!:
    catch(err)
    {
        logger.info('Error occurred in user linkedin Registration '+err);
        throw  err;
    }
});


function EncryptionValue(data){
    var shaAlgorithm = crypto.createHash('sha256');
    shaAlgorithm.update(data);
    var hashedData = shaAlgorithm.digest('hex');
    return hashedData;
}

function storeProfileImg(url,imageName){
    if(url != 'images/default.png'){
        request.get({url: url, encoding: 'binary'}, function (err, response, body) {
            fs.writeFile('./public/profileImages/'+imageName, body, 'binary', function(err) {
                if(err)
                    logger.info('Error in getting image from third party server '+err);
                else logger.info('Saving image in server success (image from thirdParty servers)');


            });
        });
    }
}

function getCompanyObj(companyName,req){
    var company = {
        url:req.headers.host,
        companyName:companyName
    }
    return company;
}

function validateCorporateEmailId(emailId,companyName){

    var eArr = emailId.split('@');
    eArr = eArr[1].split('.');
    if(companyName == eArr[0]){
       return true;
    }else{
       return false;
    }
}

function validateEmailId(email) {
    var emailText = email;
    var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (pattern.test(emailText)) {

        return true;
    } else {

        return false;
    }
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
};


function getCurrentLocation(geo){
    if(common.checkRequired(geo)){
        if(common.checkRequired(geo.country) && common.checkRequired(geo.city) && common.checkRequired(geo.region) && common.checkRequired(geo.ll[0]) && common.checkRequired(geo.ll[1])){
            var location = {
                country:geo.country,
                city:geo.city,
                region:geo.region,
                latitude:geo.ll[0],
                longitude:geo.ll[1]
            }
            return location;
        }
        else  return null;
    }
    else return null;
}

function updateCurrentLocation(IP,userId) {
    IPLocator.lookup(IP, userId, function (location) {

    });
}

function isLoggedInUser(req, res) {
    if (req.isAuthenticated()){
        return true;
    }else
    {
        return false;
    }
}

module.exports = router;