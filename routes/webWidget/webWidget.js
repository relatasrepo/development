var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var moment = require('moment-timezone');

var commonUtility = require('../../common/commonUtility');
var userManagement = require('../../dataAccess/userManagementDataAccess');
var googleCalendarAPI = require('../../common/googleCalendar');

var common = new commonUtility();
var userManagements = new userManagement();
var googleCalendar = new googleCalendarAPI();

router.get('/webWidget/relatas/generateKey',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,widgetKey:1},function(error,user){
        if(common.checkRequired(user) && common.checkRequired(user.emailId)){
            if(common.checkRequired(user.widgetKey) && common.checkRequired(user.widgetKey.key)){
                res.send(user.widgetKey);
            }
            else{
                var secrete = new Date().toISOString();
                var token = jwt.encode({ id: user._id}, secrete);
                var obj = {key:token,secrete:secrete};
                userManagements.updateWidgetKey(user._id,obj,function(result){
                    if(result){
                        res.send(obj);
                    }else res.send(false);
                })
            }
        }
        else res.send(false);
    })
});

router.get('/webWidget/relatas/html',function(req,res){
    var css = '';
    res.render('widgetViews/widgetLanding');
});

router.get('/webWidget/relatas/contactMe',function(req,res){
    res.render('widgetViews/widgetContactMe');
});

router.get('/webWidget/relatas/getBusy',function(req,res){

    if(common.checkRequired(req.query) && common.checkRequired(req.query.key) && common.checkRequired(req.query.dateString) && common.checkRequired(req.query.timezone)){
        var key = req.query.key;
        userManagements.selectUserProfileWithWidgetKey(key,{contacts:0,linkedin:0,twitter:0,facebook:0,officeCalendar:0},function(error,user){
            if(common.checkRequired(user)){
                var givenDate = moment(req.query.dateString);
                var endDate = givenDate.clone();     //min date
                var timeMax = givenDate.clone();     //max date

                endDate.hour(0)
                endDate.minute(0)
                endDate.second(0)
                endDate.millisecond(0)

                timeMax.hour(23)
                timeMax.minute(59)
                timeMax.second(59)
                timeMax.millisecond(59)
                endDate = endDate.format()
                timeMax = timeMax.format()

                /*********** parameters are changed for this function **************/
                googleCalendar.getGoogleCalendarEventsByTimezone(user,req.query.timezone,endDate,timeMax,user.emailId,function(events,timeZoneCalendar){
                    var busySlots = [];

                    if(common.checkRequired(events) && events && events.length > 0){
                        for(var i=0;i<events.length;i++){
                            if(common.checkRequired(events[i])){

                               var objG = {
                                      start:new Date(events[i].start.dateTime || events[i].start.date),
                                      end:new Date(events[i].end.dateTime || events[i].end.date)
                               };
                               busySlots.push(objG);
                            }
                        }
                    }
                    userManagements.newInvitationsByDateTimezone(user._id,endDate,timeMax,function(invitations){

                        if(common.checkRequired(invitations) && invitations.length > 0){
                            for(var m=0;m<invitations.length;m++){
                                for(var t=0;t<invitations[m].scheduleTimeSlots.length;t++){
                                    var obj = {
                                        start:invitations[m].scheduleTimeSlots[t].start.date,
                                        end:invitations[m].scheduleTimeSlots[t].end.date
                                    }
                                    busySlots.push(obj);
                                }
                            }
                        }

                        res.send(busySlots)
                    })
                });
            }
            else res.send(false)
        })
    }
    else res.send(false)
});

router.get('/webWidget/relatas/profile/byToken/:key',function(req,res){
    if(common.checkRequired(req.params) && common.checkRequired(req.params.key)){
        var key = req.params.key || '';
        userManagements.selectUserProfileWithWidgetKey(key,{contacts:0,linkedin:0,twitter:0,facebook:0,officeCalendar:0},function(error,user){
            if(common.checkRequired(user)){
                if(googleCalendar.validateUserGoogleAccount(user)){
                    user.google = [{emailId:user.google[0].emailId}];
                }
                res.send(user);
            }
            else res.send(false)
        })
    }
    else res.send(false)
});

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    if (req.isAuthenticated()){
        return next();
    }
    else{
        return res.redirect('/');
    }
}

module.exports = router;