
var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var request = require('request');
var passport = require('passport');
var fs = require('fs');
var iCalEvent = require('icalevent');
var querystring = require("querystring");

var moment = require('moment');
        require('moment-range');

//var AzureAdOAuth2Strategy = require("passport-azure-ad-oauth2");

var userManagement = require('../dataAccess/userManagementDataAccess');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var googleCalendarAPI = require('../common/googleCalendar');
var companyClass = require('../dataAccess/corporateDataAccess/companyModelClass');
var contactClass = require('../dataAccess/contactsManagementClass');
var errorClass = require('../errors/errorClass');
var errorMessages = require('../errors/errorMessage');
var IPLocatorClass = require('../common/ipLocator');
var meetingSupportClass = require('../common/meetingSupportClass');
var officeOutlookApi = require('../common/officeOutlookAPI');
var interactionManagement = require('../dataAccess/interactionManagement');
var ActionItemsManagement = require('../dataAccess/actionItemsManagement');
var salesforceUpdate = require('./webRest/salesforce').getAndStoreOpportunities;
var rabbitmq = require('../common/rabbitmq');
var ContactManagement = require('../dataAccess/contactManagement');

var meetingSupportClassObj = new meetingSupportClass();
var interactionManagementObj = new interactionManagement();
var contactObj = new contactClass();
var officeOutlook = new officeOutlookApi();
var IPLocator = new IPLocatorClass();
var errorObj = new errorClass();
var errorMessagesObj = new errorMessages();
var common = new commonUtility();
var company = new companyClass();
var googleCalendar = new googleCalendarAPI();
var userManagements = new userManagement();
var appCredential = new appCredentials();
var actionItemsManagementObj = new ActionItemsManagement();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var domainName = domain.domainName;
var contactManagementObj = new ContactManagement();
var _ = require("lodash");

var rabbitmqObj = new rabbitmq();
var logLib = new winstonLog();
var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();
var statusCodes = errorMessagesObj.getStatusCodes();
var officeAuthStateSecrete = 'kGkwyDKgwZAp6c5QfG1umCS';//X

var google = require('node-google-api')(authConfig.GOOGLE_CLIENT_ID);

/*
passport.serializeUser(function (user, done) {
    done(null, user);
})

passport.deserializeUser(function (id, done) {
    done(null, id);
})*/

/*******************************************/

//router.get('/authenticate/office365Auth',passport.authenticate('azure_ad_oauth2'))
router.get('/authenticate/office365Auth/v2',function(req,res){
    req.session.officeAuthOnSuccess = req.query.onSuccess;
    req.session.officeAuthOnFailure = req.query.onFailure;
    req.session.officeAuthAction = req.query.action;

    var status = {
        onSuccess:req.query.onSuccess,
        onFailure:req.query.onFailure,
        action:req.query.action,
        timeStamp:new Date().toISOString()
    };
    //status = common.generateTokenWithObject(officeAuthStateSecrete,status);
    var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
    var href = protocol + req.headers.host+'/office365/auth/callback/v2';
    officeOutlook.authenticate_v2_appModel(req,res,href,'');
});

router.get('/office365/auth/callback/v2', function(req, res) {
    var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
    var href = protocol + req.headers.host+'/office365/auth/callback/v2';
    officeOutlook.authoriseCode_v2_appModel(req,res,href,function(profile){
        var status = common.decodeStatusToken(req.query.state,officeAuthStateSecrete);
    })
});

/*******************************************/

router.get('/outlook/v2.0/authenticate',function (req,res) {

    var redirectUri = authConfig.OUTLOOK_redirectURI;
    req.session.afterAuthenticationRedirect = req.query.onSuccess;

    req.session.redirectUri = redirectUri;

    officeOutlook.newOutlookAuthenticate(redirectUri,function (outlookUri) {
        res.redirect(outlookUri)
    });

});

router.get('/authorizeTest/',function (req,res) {

    var redirectUri = authConfig.OUTLOOK_redirectURI;
    req.session.redirectUri = redirectUri;

    var code = req.query.code;
    var fromWhere = req.session.fromWhere;
    var token2,userIp = common.getClientIp2(req);

    officeOutlook.getOutlookTokenFromCode(code,redirectUri,res,function (token) {
        token2 = token;
        var outlookAccessToken = token.access_token;
        var isCorporateDomain = null;
        officeOutlook.getUserProfileMSGraphAPI(outlookAccessToken,function (userProfile){

            //1. Create Main acc if partial profile exists.
            //2. Create a main account from on-boarding.
            //3. Add more outlook accs from settings

            if(userProfile && userProfile.userPrincipalName){
                if(!fromWhere || fromWhere !== 'settings'){
                    officeOutlook.checkExistingUser(userProfile.userPrincipalName.toLowerCase(), function (err, status) {

                        if (!err && status) {
                            if (status.userType == 'not-registered') {

                                //During Schedule Meeting partial profile is created.
                                // For this condition, if he/she comes to on-boarding then we need to authenticate
                                //his/her outlook acc. Update first name and last name.

                                officeOutlook.storePrimaryOutlookObject(status, token, false);
                                //TODO Update profile pic also.
                                officeOutlook.updateBasicProfile(userProfile,function (response) {
                                    req.session.partialFillAcc = status;
                                    isCorporateDomain = common.fetchCompanyFromEmail(userProfile.userPrincipalName.toLowerCase());

                                    if(isCorporateDomain){
                                        common.getCompanyObjAuth(req,status,function (companyProfile) {

                                            if(companyProfile){

                                                status.companyName = companyProfile.companyName;
                                                status.corporateUser = true
                                                status.companyId = companyProfile._id

                                                userManagements.updateUserCompany(common.castToObjectId(status._id),companyProfile,function(err,result){
                                                    if(!err){
                                                        res.redirect('/onboarding/enterprise');
                                                    } else {
                                                        res.redirect('/onboarding/new');
                                                    }
                                                });
                                            } else {
                                                res.redirect('/onboarding/new');
                                            }
                                        });
                                    } else {
                                        res.redirect('/onboarding/new');
                                    }
                                });

                                officeOutlook.kickOffContactsProcess(outlookAccessToken, status);

                                kickOffMailsProcess(outlookAccessToken, status ,req,function (mailsUpdated) {
                                    kickOffCalendarEventsProcess(outlookAccessToken, status, req,function (meetingsUpdated) {
                                        // actionItemsManagementObj.kickOffActionItemsMailProcess(status._id.toString(),status.lastLoginDate,req,function(result){
                                        // })
                                        updateCurrentLocation(userIp,common.castToObjectId(status._id.toString()));
                                    });
                                });
                            } else {

                                actionItemsManagementObj.setInsightsBuiltFlag(common.castToObjectId(status._id.toString()),false)
                                //This is for a registered user's login process
                                //TODO remove invalid emails.
                                officeOutlook.storePrimaryOutlookObject(status, token, false);

                                /* IMPORTANT
                                * FOR RETURNING USERS, NONE OF THE DATA IS FETCHED.
                                * IMPLEMENTED A REFRESH BUTTON ON 16 JAN 2018
                                * */

                                // officeOutlook.kickOffContactsProcess(outlookAccessToken, status);

                                // kickOffMailsProcess(outlookAccessToken, status ,req,function (mailsUpdated) {
                                //     kickOffCalendarEventsProcess(outlookAccessToken, status, req,function (meetingsUpdated) {
                                //         // actionItemsManagementObj.kickOffActionItemsMailProcess(status._id.toString(),status.lastLoginDate,req,function(result){
                                //         // })
                                //     });
                                // });


                                salesforceUpdate(status._id.toString(),'newAndEdited',function(err,result){
                                    // console.log(err,result) //TODO notify user if API access enable error
                                });
                                common.loginUserAndRedirectToApp(status._id, '/insights', status, '3A dashboard for outlook user', res, req);
                            }
                        } else {
                            //This is direct Registration.
                            //TODO Profile picture

                            getUserProfileAndOnboard(outlookAccessToken, token2, function (err, status) {

                                //TODO Get this check after Profile picture is done
                                // if (!err && status) {

                                officeOutlook.kickOffContactsProcess(outlookAccessToken, status);

                                kickOffMailsProcess(outlookAccessToken, status ,req,function (mailsUpdated) {
                                    kickOffCalendarEventsProcess(outlookAccessToken, status, req,function (meetingsUpdated) {
                                        updateCurrentLocation(userIp,common.castToObjectId(status._id.toString()));
                                        // actionItemsManagementObj.kickOffActionItemsMailProcess(status._id.toString(),status.lastLoginDate,req,function(result){
                                        // })
                                    });
                                });

                                if (status && status.userType == 'not-registered') {

                                    isCorporateDomain = common.fetchCompanyFromEmail(status.emailId);
                                    req.session.partialFillAcc = status;
                                    if(isCorporateDomain){
                                        common.getCompanyObjAuth(req,status,function (companyProfile) {

                                            if(companyProfile){
                                                status.companyName = companyProfile.companyName;
                                                status.corporateUser = true;
                                                status.companyId = companyProfile._id;

                                                userManagements.updateUserCompany(common.castToObjectId(status._id),companyProfile,function(err,result){
                                                    if(!err){
                                                        res.redirect('/onboarding/enterprise');
                                                    } else {
                                                        res.redirect('/onboarding/new');
                                                    }
                                                });
                                            } else {
                                                res.redirect('/onboarding/new');
                                            }
                                        });
                                    } else {
                                        res.redirect('/onboarding/new');
                                    }
                                }
                            });
                        }
                    });
                }else if(fromWhere == 'settings') {

                    //Map contacts,mails,calendar events with already registered user.
                    //UserProfile is required to only store the relevent email Id

                    officeOutlook.storeSecondaryOutlookObject(userProfile,token,sessionProfile,function (result) {
                        if(result){
                            userManagements.findUserProfileByEmailId(userProfile.emailId,function (err,sessionProfile) {

                                officeOutlook.kickOffContactsProcess(outlookAccessToken, sessionProfile);
                                kickOffMailsProcess(outlookAccessToken, sessionProfile ,req);
                                kickOffCalendarEventsProcess(outlookAccessToken, sessionProfile, req);
                                res.redirect('/'+fromWhere)
                            })
                        } else {
                            //TODO catch errors here and handle them gracefully
                            res.redirect('/'+fromWhere)
                        }
                    });
                }
            } else {
                res.send("Oops, there was an error at Microsoft end")
            }
        });
    });
});

router.get('/authenticate/more/outlook/accounts',function (req,res) {
    req.session.afterAuthenticationRedirect = '/'+req.query.finalPage;
    req.session.fromWhere = req.query.finalPage;
    var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
    var redirectUri = protocol+req.headers.host+'/authorizeTest/';
    officeOutlook.newOutlookAuthenticate(redirectUri,function (outlookUri) {
        res.redirect(outlookUri)
    });
});

function kickOffMailsProcess(outlookAccessToken,status,req,callback) {

    officeOutlook.getInboxEmailsMSGraphAPI(outlookAccessToken,status,function (inbox){

        officeOutlook.getSentEmailsMSGraphAPI(outlookAccessToken,status,function (sentMails){

            var mails = [];

            if(inbox && sentMails){
                mails = inbox.concat(sentMails);
            }

            if(inbox && !sentMails){
                mails = inbox;
            }

            if(!inbox && sentMails){
                mails = sentMails;
            }

            officeOutlook.getMailsAsInteractions(mails,status,function (interactions,emailIdArr,referenceIds,singleInteractionRecords) {

                if(interactions && interactions.length>0){
                    officeOutlook.mapInteractionsEmailIdToRelatasUsers(interactions,emailIdArr,function (mappedInteractions) {

                        if(mappedInteractions && mappedInteractions.length>0){

                            interactionManagementObj.updateNewInteractionsCollection(common.castToObjectId(status._id.toString()),status,singleInteractionRecords);
                            
                            interactionManagementObj.updateEmailInteractions(status,mappedInteractions,referenceIds,function(isSuccess){

                                if(req){
                                    req.session.interactionsWritten = true;
                                    req.session.save();
                                }

                                if(isSuccess){
                                    logger.info("Emails successfully saved for -",status.emailId);

                                    var fromDate;

                                    if(status && status.registeredUser){

                                        if(common.checkRequired(status.lastMobileSyncDate)){
                                            if(status.lastLoginDate>status.lastMobileSyncDate){
                                                fromDate = status.lastLoginDate
                                            } else {
                                                fromDate = status.lastMobileSyncDate
                                            }
                                        } else {
                                            fromDate = status.lastLoginDate
                                        }
                                    } else {
                                        fromDate = moment().subtract(90, "days").toDate();
                                    }

                                    fromDate = moment().subtract(30, "days").toDate();

                                    var obj = {
                                        'userId': status._id.toString(),
                                        'afterDate': moment(fromDate).format('YYYY-MM-DD'),
                                        'beforeDate': moment(moment().add(2, "days")).format('YYYY-MM-DD')
                                    };
                                    logger.info("Add interactions bulk EMAIL Success and send rabitmq message office 365");

                                    rabbitmqObj.producer(obj,function(result){
                                        if(result){
                                            logger.info("Add OBJ to processQueue Success ",obj);
                                        } else{
                                            logger.info("Failed to Add OBJ to processQueue ",obj);
                                        }
                                    });

                                    if(callback){
                                        callback(true);
                                    }
                                } else {
                                    logger.info("Emails unsuccessfully saved for -",status.emailId)
                                    if(callback){
                                        callback(false);
                                    }
                                }
                            });

                            //Update these interactions's 2nd party as contacts if they don't already exist.
                            addInteractedContacts(status,mappedInteractions);
                        }
                        else{
                            if(callback){
                                callback(false);
                            }
                        }
                    });
                }
                else{
                    if(callback){
                        callback(false);
                    }
                }
            });
        })
    })
}

function kickOffCalendarEventsProcess(outlookAccessToken,status,req,callback) {
    officeOutlook.getCalendarEventsMSGraphAPIV2(outlookAccessToken,status,function (calendarEvents) {
        officeOutlook.getMeetingObjectFromOutlookCalendar(status,calendarEvents,function (meetings,relatasUsers) {
            var newInteractions = buildObjForNewMeetingInteractions(status,meetings);
            interactionManagementObj.updateNewInteractionsCollection(common.castToObjectId(status._id.toString()),status,newInteractions);

            officeOutlook.insertMeetingsInBulk(status,meetings,false,relatasUsers,function (insertResults) {
                if(callback){
                    callback(true)
                }
            });
        });
    });
}

function addInteractedContacts(userProfile,mappedInteractions) {

    var interactedContacts = [];

    common.getInvalidEmailListFromDb(function (list) {

        _.each(mappedInteractions,function (interaction) {

            if(interaction.emailId) {
                var contactObj = {
                    personId: interaction.userId ? interaction.userId.toString() : null,
                    personName: interaction.emailId.toLowerCase(),
                    personEmailId: interaction.emailId.toLowerCase(),
                    birthday: null,
                    companyName: interaction.companyName ? interaction.companyName : null,
                    designation: null,
                    addedDate: new Date(),
                    verified: false,
                    relatasUser: interaction.userId ? true : false,
                    relatasContact: true,
                    mobileNumber: interaction.mobileNumber ? interaction.mobileNumber : null,
                    location: interaction.location ? interaction.location : null,
                    source: 'outlook-email-interactions',
                    account: {
                        name: common.fetchCompanyFromEmail(interaction.emailId),
                        selected: false,
                        updatedOn: new Date()
                    }
                }

                if(contactObj.personEmailId){
                    if(common.isValidEmail(contactObj.personEmailId, list)){
                        interactedContacts.push(contactObj)
                    }
                } else {
                    interactedContacts.push(contactObj)
                }

            }
        });

        var contacts = common.removeDuplicates_field(interactedContacts, 'personEmailId');
        var contactsEmailIdArr = _.pluck(interactedContacts,'personEmailId');
        var mobileNumbers = [];

        contactManagementObj.insertContacts(common.castToObjectId(userProfile._id.toString()),userProfile.emailId,contactsEmailIdArr,contacts,userProfile.lastLoginDate,function (cErr,contactsList) {

        });

        contactObj.addContactNotExist(common.castToObjectId(userProfile._id.toString()),userProfile.emailId,contacts,mobileNumbers,contactsEmailIdArr,'outlook',false);
    });

}

function getUserProfileAndOnboard(outlookAccessToken,token,callback) {
    officeOutlook.getUserProfileMSGraphAPI(outlookAccessToken,function (userProfile){
        userManagements.createPartialProfile(userProfile,token,'outlookUser',function (createProfileResult) {
            if(createProfileResult){
                officeOutlook.checkExistingUser(userProfile.userPrincipalName,function (err,status) {
                    callback(err,status)
                });
            }
        });

        //TODO Get profile pic
        // officeOutlook.getUserProfilePictureMSGraphAPI(outlookAccessToken,userProfile,function (profilePicture) {
        //     console.log('--profilePicture--')
        // console.log(profilePicture)
        // });
        // res.send({token:token2,userProfile:userProfile,profile:profile});
    });
}

router.get('/corporate/office365Auth',function(req,res){
   officeOutlook.authenticate(req,res,null,'no_state');
});

router.get('/get/unique/publicProfileUrl',function(req,res){
   userManagements.getUniquePublicProfileUrl(req.query.publicProfileUrl,function (uniquePublicProfileUrl) {
       res.send(uniquePublicProfileUrl);
   });
});

router.get('/authenticate/office365Auth/new',function(req,res){
    req.session.officeAuthOnSuccess = req.query.onSuccess;
    req.session.officeAuthOnFailure = req.query.onFailure;
    req.session.officeAuthAction = req.query.action;

    var status = {
        onSuccess:req.query.onSuccess,
        onFailure:req.query.onFailure,
        action:req.query.action,
        timeStamp:new Date().toISOString()
    };

    if(common.checkRequired(req.session.filled_meeting_details)){
        status.filled_meeting_details = req.session.filled_meeting_details;
    }
    if(common.checkRequired(req.session.publicUser)){
        status.publicUser = req.session.publicUser;
    }

    status = common.generateTokenWithObject(officeAuthStateSecrete,status);
    var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
    var href = protocol + req.headers.host+'/office365/auth/callback/new';
    officeOutlook.authenticate(req,res,href,status);
});

router.get('/office365/auth/callback/new', function(req, res) {
    var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
    var href = protocol + req.headers.host+'/office365/auth/callback/new';
    officeOutlook.authoriseCode(req,res,href,function(profile){
        var status = common.decodeStatusToken(req.query.state,officeAuthStateSecrete);
        if(common.checkRequired(status)){

            var emailId = null;
            if (checkRequired(profile.rawObject) && checkRequired(profile.rawObject.email)) {
                emailId = profile.rawObject.email;
            }
            else if (checkRequired(profile.username)) {
                emailId = profile.username.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
                if(common.checkRequired(emailId) && emailId.length > 0){
                    emailId = emailId[0];
                }
            }

            switch (status.action){
                case 'add':if(common.isLoggedInUserBoolean(req)){
                    //var userId = common.getUserId(req.user);
                    if(checkRequired(profile.tokenInfo)){

                        profile.tokenInfo.emailId = emailId;
                        userManagements.updateOfficeCalender(common.getUserId(req.user),profile.tokenInfo);
                    }
                    res.redirect('/'+status.onSuccess);
                }
                else res.redirect('/'+status.onFailure);
                    break;
                case 'login_outlook': if(profile != false && checkRequired(profile)) {

                    if(checkRequired(emailId)){
                        profile.emailId = emailId;

                        userManagements.findUserProfileByEmailIdWithCustomFields(emailId,{contacts:0},function(error,user){
                            if(error){
                                res.redirect('/'+status.onFailure);
                            }
                            else if(checkRequired(user)){
                                googleCalendar.getGoogleContactsEmailsEventsOffice365user(user);
                                // user login
                                if(checkRequired(profile.tokenInfo)){
                                    userManagements.updateOfficeCalender(user._id,profile.tokenInfo);
                                }
                                var tokenSecret ="alpha";
                                var token = jwt.encode({ id: user._id}, tokenSecret);
                                req.logIn(token, function(err){
                                    common.insertLog(user,'corporate-landing-page',req);
                                    res.redirect('/'+status.onSuccess);
                                })
                            }
                            else{
                                res.redirect('/'+status.onFailure);
                            }
                        })
                    }
                    else{
                        res.redirect('/'+status.onFailure);
                    }
                }
                else{
                    res.redirect('/'+status.onFailure);
                }
                    break;
                case 'cal_pass_request':
                    userManagements.findUserProfileByEmailIdWithCustomFields(emailId,{contacts:0},function(error,user) {
                        if (error) {
                            res.redirect('/' + status.onFailure);
                        }
                        else if(common.checkRequired(user)){

                            var tokenSecret ="alpha";
                            var token = jwt.encode({ id: user._id}, tokenSecret);
                            req.logIn(token, function(err){
                                common.insertLog(user,'cal_pass_request',req);
                                common.sendCalendarPasswordRequest(user,status.publicUser,function(result){
                                    if(result)
                                        res.redirect('/'+status.onSuccess+'?Success='+errorMessagesObj.getMessage("CALENDAR_PASSWORD_REQUEST_SENT"));
                                    else res.redirect('/'+status.onFailure+'?Error='+errorMessagesObj.getMessage("CALENDAR_PASSWORD_REQUEST_FAILED"));
                                });
                            })

                        }
                        else{
                            createPartialProfile(emailId,profile, req, res, status);
                        }
                    });
                break;
                default:res.redirect('/'+status.onFailure);
                    break;
            }
        }
        else{
            res.redirect('/'+status.onFailure);
        }
    })
});

router.get('/office365/auth/callback', function(req, res) {
    getCompanyObjAuth(req,function(companyObj){
        if(profile != false && checkRequired(profile)){
            var emailId = null;
            if(checkRequired(profile.rawObject) && checkRequired(profile.rawObject.email)){
                emailId = profile.rawObject.email;
            }
            else if(checkRequired(profile.username)){
                emailId = profile.username;
            }

            if(checkRequired(emailId)){
                profile.emailId = emailId;

                userManagements.findUserProfileByEmailIdWithCustomFields(emailId,{contacts:0},function(error,user){
                    if(error){
                        // error handle
                        res.redirect('error');
                    }
                    else if(checkRequired(user)){
                        if(user.corporateUser){

                            if( user.companyId.toString() == companyObj._id.toString()){
                                var lastLoginDate = common.checkRequired(user.lastLoginDate) ? user.lastLoginDate : null;

                                getOfficeEventsEmailsContacts(profile.access_token,lastLoginDate,user);
                                googleCalendar.getGoogleContactsEmailsEventsOffice365user(user);
                                // user login
                                if(checkRequired(profile.tokenInfo)){
                                    userManagements.updateOfficeCalender(user._id,profile.tokenInfo);
                                }
                                var tokenSecret ="alpha";
                                var token = jwt.encode({ id: user._id}, tokenSecret);
                                req.logIn(token, function(err){
                                    common.insertLog(user,'corporate-landing-page',req);
                                    res.redirect('/');
                                })
                            }
                            else{
                                req.session.corporateErrCompanyId = user.companyId;
                                req.session.corporateLinkedInError = 'otherCompanyUser';
                                return res.redirect('/login');
                            }
                        }
                        else{
                            // send user out with message
                            logger.info('non corporate user or user not in this company, sent out');
                            res.redirect('/login');
                        }
                    }
                    else if(validateCorporateEmailId(emailId,companyObj)){
                        // registration
                        req.session.office365Profile = profile;
                        res.redirect('/signUp/office');
                    }
                    else{
                        res.redirect('/login');
                    }
                })
            }
            else{
                // send user out with error
                res.redirect('/login');
            }
        }
        else{
            // send user out with error
            res.redirect('/login');
        }
    });
});

router.get('/office365/test/api', function(req, res) {

    var userId = common.getUserId(req.user);

    userManagements.findUserProfileByIdWithCustomFields(common.castToObjectId(userId), { emailId: 1, outlook: 1}, function (error, profile) {

        _.each(profile.outlook, function (outlookObj) {
            if (profile.emailId == outlookObj.emailId) {
                officeOutlook.getNewAccessTokenWithoutStore(outlookObj.refreshToken, function (newToken) {

                    officeOutlook.fetchContacts(newToken.access_token,function (contacts) {
                        res.send(contacts)
                    });
                });
            }
        });
    });
});

function createPartialProfile(emailId, outlookUser, req, res, status) {
    var currentLocation = null;

    var firstName = null;
    var lastName = null;
    var publicProfileUrl = null
    if(common.checkRequired(outlookUser.rawObject)){
        if(common.checkRequired(outlookUser.rawObject.given_name)){
            publicProfileUrl = outlookUser.rawObject.given_name
            firstName = outlookUser.rawObject.given_name;
        }
        else if(common.checkRequired(outlookUser.rawObject.displayName)){
            publicProfileUrl = outlookUser.rawObject.displayName;
            firstName = outlookUser.rawObject.displayName;
        }

        if(common.checkRequired(outlookUser.rawObject.family_name)){
            lastName = outlookUser.rawObject.family_name;
        }

        if(common.checkRequired(publicProfileUrl)){
            publicProfileUrl = common.getValidUniqueNameWithIdentity(publicProfileUrl);
        }
        else if(common.checkRequired(outlookUser.rawObject.family_name)){
            publicProfileUrl = common.getValidUniqueNameWithIdentity(outlookUser.rawObject.family_name);
        }
    }

    var officeUserProfile = {
        firstName: firstName,
        lastName: lastName,
        profilePicUrl: '/images/default.png',
        publicProfileUrl: publicProfileUrl,
        public: true,
        emailId: emailId.toLowerCase() || null,
        day: 0,
        month: 0,
        year: 0,
        registeredUser: false,
        partialFill: true,
        serviceLogin: 'office',
        currentLocation: currentLocation,
        userType:'not-registered',
        officeCalendar:outlookUser.tokenInfo
    }

    userManagements.addUserThirdParty(officeUserProfile, function (error, userProfile, message) {
        if (message.message == 'userExist') {
            var tokenSecret = "alpha";
            var token = jwt.encode({id: userProfile._id}, tokenSecret);
            googleCalendar.getGoogleContactsEmailsEventsOffice365user(userProfile);
            getOfficeEventsEmailsContacts(outlookUser.tokenInfo.access_token,userProfile.lastLoginDate || null,userProfile,false)
            var user = token;
            req.logIn(user, function (err) {
                common.insertLog(userProfile, 'login-schedule', req);
                req.session.dashboardMsg = true;
                req.session.userLoginFlag = true;

                if(common.checkRequired(status)){
                    if(status.action == 'send_meeting_request'){
                        scheduleMeetingNext(req,res,userProfile,status.publicUser,req.session.filled_meeting_details,false,status);
                    }
                    else if(status.action == 'cal_pass_request'){
                        common.sendCalendarPasswordRequest(userProfile,status.publicUser,function(result){
                            if(result)
                                res.redirect('/'+status.onSuccess+'?Success='+errorMessagesObj.getMessage("CALENDAR_PASSWORD_REQUEST_SENT"));
                            else res.redirect('/'+status.onFailure+'?Error='+errorMessagesObj.getMessage("CALENDAR_PASSWORD_REQUEST_FAILED"));
                        });
                    }
                    else res.redirect('/'+status.onFailure);
                }
                else res.redirect('/error');
            })
        }
        else if (error || userProfile == null) {
            //return to same page
            if(common.checkRequired(status)){
                res.redirect('/'+status.onFailure);
            }else
                res.redirect('/error');
        }
        else if (userProfile != null && userProfile != undefined) {
            req.session.uIdentity = common.getValidUniqueName(userProfile.publicProfileUrl);
            req.session.partialFillAcc = userProfile;
            updateCurrentLocation(common.getClientIp2(req),userProfile._id);
            googleCalendar.getGoogleContactsEmailsEventsOffice365user(userProfile);
            getOfficeEventsEmailsContacts(outlookUser.tokenInfo.access_token,userProfile.lastLoginDate || null,userProfile,true)
            if(common.checkRequired(status)){
                if(status.action == 'send_meeting_request'){
                    scheduleMeetingNext(req,res,userProfile,status.publicUser,req.session.filled_meeting_details,false,status);
                }
                else if(status.action == 'cal_pass_request'){
                    common.sendCalendarPasswordRequest(userProfile,status.publicUser,function(result){
                        if(result)
                            res.redirect('/'+status.onSuccess+'?Success='+errorMessagesObj.getMessage("CALENDAR_PASSWORD_REQUEST_SENT"));
                        else res.redirect('/'+status.onFailure+'?Error='+errorMessagesObj.getMessage("CALENDAR_PASSWORD_REQUEST_FAILED"));
                    });
                }
                else res.redirect('/'+status.onFailure);
            }
            else res.redirect('/join');
        }
        else{
            if(common.checkRequired(status)){
                res.redirect('/'+status.onFailure);
            }
            else res.redirect('/error');
        }
    });
}

function scheduleMeetingNext(req,res,sender,receiverObj,meetingDetails,isSelfCalendar,status){
    req.session.googleStatusToken = null;
    if(common.checkRequired(sender)){
        var senderObj = {
            senderId:sender._id,
            senderEmailId:sender.emailId,
            senderName:sender.firstName+' '+sender.lastName,
            senderPicUrl:sender.profilePicUrl
        };
        var type = isSelfCalendar ? 'selfCalendar' : 'normal';
        meetingSupportClassObj.scheduleNewMeeting(senderObj,receiverObj,type,meetingDetails,req,false,function(error,response){
            if(response == false){
                res.redirect('/'+status.onFailure+'&error=Error_Message');
            }
            else res.redirect('/'+status.onSuccess);
        })
    }
    else{
        res.redirect('/'+status.onFailure+'&error=Error_Message');
    }
}

function updateCurrentLocation(IP,userId) {
    IPLocator.lookup(IP, userId, function (location) {

    });
}

function validateCorporateEmailId(emailId,companyObject){

    var emailDomains = companyObject.emailDomains;
    var eArr = emailId.split('@');
    var isValid = false;

    for(var domain=0; domain < emailDomains.length; domain++){
        if(eArr[1] == emailDomains[domain]){
            isValid =true;
        }
    }
    return isValid;
}

function getOfficeEventsEmailsContacts(access_token,date,user,isNewUser){
    /*officeOutlook.getContacts(access_token,function(contacts){
        parseAndStoreOfficeContacts(contacts,user._id);
    });*/
    officeOutlook.getEmails(access_token,date,function(emails){

        //parseAndStoreOfficeEmails(emails,user._id);
    });
    /*officeOutlook.getCalendarEvents(access_token,isNewUser,user,function(events){
        //parseAndStoreOfficeEvents(events,userId);
        console.log(JSON.stringify(events))
    });*/
}

router.get('/corporate/office/getSession',function(req,res){
    res.send(req.session.office365Profile);
});

router.get('/test/ip',function(req,res){
    var userId = common.getUserId(req.user);
    IPLocator.lookup(req.query.ip, userId, function (location) {
        res.send(location)
    });
});

router.post('/addContacts/corporateUser/officeContacts',function(req,res){
    var details = req.body;

    if(common.checkRequired(details) && common.checkRequired(details.accessToken) && req.session.userId){
        getOfficeEventsEmailsContacts(details.accessToken,null,req.session.userId);
    }
    res.send(true)
});

function getPhoneNumber(contact){
    var phone = '';
    if(common.checkRequired(contact.MobilePhone1)){
        phone = contact.MobilePhone1;
    }
    else if(getIfBusinessNum(contact.BusinessPhones)){
        phone = contact.BusinessPhones[0];
    }
    else if(getIfBusinessNum(contact.HomePhones)){
        phone = contact.HomePhones[0];
    }
    return phone;
}

function getIfBusinessNum(businessNum){
    if(common.checkRequired(businessNum) && businessNum.length > 0){
        if(common.checkRequired(businessNum[0])){
            return true;
        }
        else return false;
    }
    else return false;
}

router.get('/postSignUp/interactions/googleMeeting',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    googleCalendar.getGoogleMeetingInteractions(userId);

});

router.post('/generateIcs/byTimezone',function(req,res){
  var details = req.body();
   if(checkRequired(details.meetingId)){
       generateIcs(details.meetingId,details.timezone,req,res);
   }else res.send(false);
});

function processContacts(userProfile,contactsNestedArray) {

    var contacts = [],contactsEmailIdArr = [],mobileNumbers = [];

    common.getInvalidEmailListFromDb(function (list) {

        _.each(contactsNestedArray,function (eachContactsArray) {
            _.each(eachContactsArray,function (contact) {
                var c = common.buildOutlookContactObject(contact);

                contacts.push(c);
                contactsEmailIdArr.push(c.personEmailId);
                mobileNumbers.push(c.mobileNumber);

            });
        });

        contactsEmailIdArr = _.uniq(contactsEmailIdArr);

        //Map each receiver and sender to Relatas User profile.
        var projection = {firstName:1,lastName:1,emailId:1,profilePicUrl:1,publicProfileUrl:1,designation:1,location:1,mobileNumber:1,_id:1};
        userManagements.selectUserProfilesCustomQuery({emailId:{$in:contactsEmailIdArr}},projection,function(error,relatasUsers){

            if(!error && relatasUsers && relatasUsers.length > 0){
                _.each(relatasUsers,function (rUser) {
                    _.each(contacts,function (contact) {

                        if(contact.mobileNumber && contact.mobileNumber.match(new RegExp(/[^0-9]/g))) {
                            contact.mobileNumber = contact.mobileNumber.replace(new RegExp(/[^0-9]/g), ''); //remove special character from mobile number
                        }

                        if(rUser.emailId == contact.personEmailId){
                            contact.personId = rUser._id.toString()
                            contact.personName = rUser.firstName+' '+rUser.lastName
                            contact.personEmailId = rUser.emailId
                            contact.companyName = rUser.companyName
                            contact.designation = rUser.designation
                            contact.location = rUser.location
                            contact.verified = true
                            contact.relatasrUser = true
                            contact.relatasContact = true
                            contact.mobileNumber = rUser.mobileNumber
                            contact.skypeId = rUser.skypeId || ''
                        }
                    });
                });
            }

            contacts = _.uniq(contacts,'personEmailId');

            contactObj.addContactNotExist(common.castToObjectId(userProfile._id.toString()),userProfile.emailId,contacts,mobileNumbers,contactsEmailIdArr,'outlook',false);
            //Set off another process to update contact name and image link
            contactObj.updateContactNameAndProfileImage(common.castToObjectId(userProfile._id.toString()),contacts,common.castToObjectId,false);
        });
    });
}

function generateIcs(meetingId,timezone,req,res){

    userManagements.findInvitationById(meetingId,function(error,meeting){
        if(error || !checkRequired(meeting)){
            res.send(false);
        }else{
            var event = new iCalEvent();

            event.set('uid', meeting.invitationId);
            event.set('offset', new Date().getTimezoneOffset());
            event.set('method', 'request');
            event.set('status', 'confirmed');
            event.set('attendees', [
                {
                    name: meeting.senderName,
                    email: meeting.senderEmailId
                },
                {
                    name: meeting.to.receiverName,
                    email: meeting.to.receiverEmailId
                }
            ]);

            for(var i=0; i<meeting.scheduleTimeSlots.length; i++){
                if(meeting.scheduleTimeSlots[i].isAccepted){
                    event.set('start', meeting.scheduleTimeSlots[i].start.date);
                    event.set('end', meeting.scheduleTimeSlots[i].end.date);
                    event.set('summary',  meeting.scheduleTimeSlots[i].title);
                    event.set('description', meeting.scheduleTimeSlots[i].description);
                    event.set('location', meeting.scheduleTimeSlots[i].locationType+': '+meeting.scheduleTimeSlots[i].location);
                }
            }

            event.set('timezone', timezone || '');
            event.set('organizer', { name:  meeting.senderName, email: meeting.senderEmailId });
            event.set('url', domainName+'/meeting/'+meeting.invitationId);
            var icsFile = event.toFile();

            res.send(icsFile);
        }
    })
}

function getCompanyObjAuth(req,callback){
    var domain = common.getSubDomain(req);
    if(domain){
        company.findOrCreateCompanyByUrl(getCompanyObj(domain,req),{url:1,emailDomains:1},function(companyProfile){
            req.session.companyId = companyProfile._id;
            callback(companyProfile)
        });

    }else callback(null)
}

function getCompanyObj(companyName,req){
    var company = {
        url:req.headers.host,
        companyName:companyName
    };
    return company;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}


var emailSender = require('../public/javascripts/emailSender');
var emailSenderObj = new emailSender();

router.get('/send/outlook/mail',common.isLoggedInUserOrMobile,function (req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var mailBody = req.body;

    var mail = {
        // "Message": {
        //     "Subject": mailBody.subject,
        //     "Body": {
        //         "ContentType": "Text",
        //         "Content": mailBody.content
        //     },
        //     "ToRecipients": [
        //         {
        //             "EmailAddress": {
        //                 "Address": mailBody.rEmailId
        //             }
        //         }
        //     ]
        // },
        // "SaveToSentItems": "true"
    };

    var recipients = [];
    mailBody.email_cc = ["naveenpaul@relatas.com"];

    _.each(mailBody.email_cc,function (emailId) {
        recipients.push({
            "EmailAddress": {
                "Address": emailId
            }
        })
    });

    mail = {
        "comment": "Does this save?"
    }
    
    var id = "AQMkADAwATNiZmYAZC1mYzY0LTZlMTItMDACLTAwCgBGAAADiGBHMIShWE2xaAN2bK0mwgcAvSLKfpxJlk62z0hrovEs7gAAAgEMAAAAvSLKfpxJlk62z0hrovEs7gABJ0mSlgAAAA=="

    userManagements.getUserProfileWithoutContacts(common.castToObjectId(userId),function(err, userProfile){

        emailSenderObj.sendPreSignUpRequestMail(userProfile);

        var refreshToken = userProfile.outlook[0].refreshToken;
        officeOutlook.getNewAccessTokenWithoutStore(refreshToken,function (newToken) {

            officeOutlook.createReplyEmailMSGraphAPI(newToken.access_token,mail,id,function (err,response) {

                var draftMessage = JSON.parse(response);

                var content = "<html><body><div>This is my<br> response to your<br> message.</div></body></html>" +draftMessage.body.content
                // var content = "This is my response to your message."

                draftMessage.ccRecipients = recipients;
                var updateDraft = {
                    "body": {
                        "contentType": "html",
                        "content": content
                    },
                    "ccRecipients":recipients
                }

                officeOutlook.updateDraftMSGraphAPI(newToken.access_token,draftMessage.id,updateDraft,function (err,updateRes) {
                    officeOutlook.sendDraftMSGraphAPI(newToken.access_token,draftMessage.id,function (err,sent) {
                        res.send({
                            sent:sent,
                            updateRes:JSON.parse(updateRes),
                            err:err
                        })
                    })
                })
            });
        });
    });
});

router.get('/sync/outlook/data',common.isLoggedInUserOrMobile,function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var isOnBoarding = req.query.isOnBoarding;

    common.getProfileOrStoreProfileInSession(userId,req,function (userProfile) {

        if(userProfile){

            officeOutlook.getNewAccessTokenForMobileSync(userProfile.outlook[0].refreshToken,userProfile,function (newToken) {

                officeOutlook.kickOffContactsProcess(newToken, userProfile,null,function (response) {

                    kickOffMailsProcess(newToken, userProfile ,req,function (mailsUpdated) {

                        kickOffCalendarEventsProcess(newToken, userProfile, req,function (meetingsUpdated) {

                            var lastSyncDate = new Date();

                            if(isOnBoarding && isOnBoarding != "false"){
                                setUpOnboardingUsers(userProfile,lastSyncDate,res,isOnBoarding);
                            } else {
                                
                                var obj = {
                                    'userId': String(userId),
                                    'afterDate': moment(lastSyncDate).format('YYYY-MM-DD'),
                                    'beforeDate': moment().format('YYYY-MM-DD')
                                };
                                logger.info("Add interactions bulk EMAIL Success and send rabitmq message office 365");

                                rabbitmqObj.producer(obj,function(result){
                                    if(result)
                                        logger.info("Add OBJ to processQueue Success ",obj);
                                    else
                                        logger.info("Failed to Add OBJ to processQueue ",obj);
                                });


                                if(req.session.mobileSession){

                                    userManagements.updateMobileSyncDate(common.castToObjectId(userId.toString()), lastSyncDate, function (err, result) {
                                        if (err) {
                                            res.send(errorObj.generateErrorResponse({status: statusCodes.UPDATE_FAILED, key: 'UPDATE_FAILED'}))
                                        } else {

                                            res.send({
                                                "SuccessCode": 1,
                                                "Message": "ALL_INTERACTIONS_WERE_SYNCED",
                                                "ErrorCode": 0,
                                                "Data": {
                                                    "isOnBoarding":isOnBoarding,
                                                    "lastSyncDate":lastSyncDate
                                                }
                                            });
                                        }
                                    })
                                } else {
                                    res.send({
                                        "SuccessCode": 1,
                                        "Message": "ALL_INTERACTIONS_WERE_SYNCED",
                                        "ErrorCode": 0,
                                        "Data": {
                                            "isOnBoarding":isOnBoarding,
                                            "lastSyncDate":lastSyncDate
                                        }
                                    });
                                }
                            }
                        });
                    });
                });
            });

        } else {
            res.send({
                "SuccessCode": 1,
                "Message": "NOT_ALL_INTERACTIONS_WERE_SYNCED",
                "ErrorCode": 0,
                "Data": {
                    "isOnBoarding":isOnBoarding,
                    "lastSyncDateAttempt":''
                }
            })
        }
    });
});

function setUpOnboardingUsers(user,lastSyncDate,res,isOnBoarding) {
    // updateMobileOnBoard Date  only for non relatas users onBoarding the first time..along with partial Profile Date ( created Date)
    if(user.createdDate)
        if(user.mobileOnBoardingDate == null){
            userManagements.updateMobileOnBoardDate(user._id,lastSyncDate,function(err,res){
                if(err){
                    res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'UPDATE_FAILED'}));
                    // updateLastSync = false
                }else {
                    if(user.createdDate==null || user.createdDate=="") {
                        userManagements.updatePartialProfileDate(user._id, lastSyncDate, function (err, result) {
                            if(err){
                                res.send(errorObj.generateErrorResponse({status:statusCodes.UPDATE_FAILED,key:'UPDATE_FAILED'}));
                                // updateLastSync = false
                            }else{
                                if(user.registeredDate == null || user.registeredDate == ""){
                                    userManagements.updateRegisteredDateUser(user._id,lastSyncDate,function(err,result){
                                        if(err) {
                                            res.send(errorObj.generateErrorResponse({
                                                status: statusCodes.UPDATE_FAILED,
                                                key: 'UPDATE_FAILED'
                                            }));
                                            // updateLastSync = false
                                        }else{
                                            res.send({
                                                "SuccessCode": 1,
                                                "Message": "ALL_INTERACTIONS_WERE_SYNCED",
                                                "ErrorCode": 0,
                                                "Data": {
                                                    "isOnBoarding":isOnBoarding,
                                                    "lastSyncDate":lastSyncDate
                                                }
                                            });
                                        }
                                    })
                                } else {
                                    res.send({
                                        "SuccessCode": 1,
                                        "Message": "ALL_INTERACTIONS_WERE_SYNCED",
                                        "ErrorCode": 0,
                                        "Data": {
                                            "isOnBoarding":isOnBoarding,
                                            "lastSyncDate":lastSyncDate
                                        }
                                    });
                                }
                            }
                        })
                    } else {
                        res.send({
                            "SuccessCode": 1,
                            "Message": "ALL_INTERACTIONS_WERE_SYNCED",
                            "ErrorCode": 0,
                            "Data": {
                                "isOnBoarding":isOnBoarding,
                                "lastSyncDate":lastSyncDate
                            }
                        });
                    }
                }
            })
        }
}

router.get('/test/outlook/data',common.isLoggedInUserOrMobile,function(req,res){

    var userId = common.getUserIdFromMobileOrWeb(req);
    var project = {contacts:0};
    userId = "584bdab1c67f2dff137f9f07"

    userManagements.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),project,function (err,userProfile) {
        officeOutlook.getNewAccessTokenForMobileSync(userProfile.outlook[0].refreshToken,userProfile,function (newToken) {

            // officeOutlook.getInboxEmailsMSGraphAPI(newToken,userProfile,function (inbox){
            //
            //     officeOutlook.getSentEmailsMSGraphAPI(newToken,userProfile,function (sentMails){
            //         var mails = [];
            //
            //         if(inbox && sentMails){
            //             mails = inbox.concat(sentMails);
            //         }
            //
            //         if(inbox && !sentMails){
            //             mails = inbox;
            //         }
            //
            //         if(!inbox && sentMails){
            //             mails = sentMails;
            //         }
            //
            //         officeOutlook.getMailsAsInteractions(mails,userProfile,function (interactions,emailIdArr,referenceIds,singleInteractionRecords) {
            //             res.send(singleInteractionRecords)
            //         })
            //     })
            //
            // })

            officeOutlook.getCalendarEventsMSGraphAPIV2(newToken,userProfile,function (calendarEvents) {
                // res.send(calendarEvents)
                officeOutlook.getMeetingObjectFromOutlookCalendar(userProfile,calendarEvents,function (meetings,relatasUsers) {
                    var newInteractions = buildObjForNewMeetingInteractions(userProfile,meetings);
                    // officeOutlook.insertMeetingsInBulk(userProfile,meetings,false,relatasUsers,function (insertResults) {
                        res.send({
                            newInteractions:newInteractions,
                            meetings:meetings
                        });
                    // });
                });
            });
        });
    });
});

function buildObjForNewMeetingInteractions(userProfile,meetings){

    var interactions = [];

    _.each(meetings,function (el) {
        var data = getCommonMeetingInteractionObj(userProfile,el);

        _.each(data.recordsFor,function (emailId) {
            var intObj = _.cloneDeep(data.obj)
            intObj.emailId = emailId;
            intObj.ownerId = userProfile._id;
            intObj.companyId = userProfile.companyId?userProfile.companyId:null;
            interactions.push(intObj)
        })
    });

    return interactions;

}

function getCommonMeetingInteractionObj(userProfile,meeting){
    var action = "receiver",
        recordsFor = [];

    if(meeting.senderEmailId == userProfile.emailId){
        action = "sender"
    }

    var obj = {
        ownerEmailId:userProfile.emailId,
        ownerId:userProfile._id,
        user_twitter_id:null,
        user_twitter_name:null,
        firstName:null,
        lastName:null,
        publicProfileUrl:null,
        profilePicUrl:null,
        emailId:null,
        companyName:null,
        designation:null,
        location:null,
        mobileNumber:null,
        action:action,
        interactionType:"meeting",
        subType:meeting.scheduleTimeSlots[0].locationType,
        refId:meeting.invitationId,
        source:"outlook",
        title:meeting.scheduleTimeSlots[0].title,
        description:meeting.scheduleTimeSlots[0].description,
        interactionDate:new Date(meeting.scheduleTimeSlots[0].start.date),
        endDate:null,
        createdDate:new Date(),
        trackId:null,
        emailContentId:null,
        googleAccountEmailId:userProfile.emailId,
        toCcBcc:null,
        emailThreadId:null,
        to:[],
        cc:[],
        hasUnsubscribe:false,
        companyId:userProfile.companyId?userProfile.companyId:null
    };

    _.each(meeting.participants,function (el) {
        if(userProfile.emailId != el.emailId){
            recordsFor.push(el.emailId)
        }
    })

    return {
        obj:obj,
        recordsFor:_.uniq(recordsFor)
    }
}

router.get("/admin/updateAllDataForOutlookUsers",common.isLoggedInUserOrMobile,function (req,res) {

    var project = {
        emailId:1,serviceLogin:1,google:1,outlook:1,lastMobileSyncDate:1,mobileOnBoardingDate:1,registeredDate:1,createdDate:1,_id:1,lastLoginDate:1,companyId:1
    }

    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagements.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),project,function (err,userProfile) {

        if (!err && userProfile) {

            res.redirect("/")
            // officeOutlook.getNewAccessTokenForMobileSync(userProfile.outlook[0].refreshToken, userProfile, function (newToken) {
            //
            //     officeOutlook.kickOffContactsProcess(newToken, userProfile, null, function (response) {
            //
            //         kickOffMailsProcess(newToken, userProfile, req, function (mailsUpdated) {
            //
            //             kickOffCalendarEventsProcess(newToken, userProfile, req, function (meetingsUpdated) {
            //                 res.redirect("/")
            //                 // actionItemsManagementObj.kickOffActionItemsMailProcess(userProfile._id.toString(),userProfile.lastLoginDate,req,function(result){
            //                 // })
            //             })
            //         })
            //     })
            // })
        }
    })
});

router.get("/test/mail/refresh",common.isLoggedInUserOrMobile,function (req,res) {

    var project = {
        emailId:1,serviceLogin:1,google:1,outlook:1,lastMobileSyncDate:1,mobileOnBoardingDate:1,registeredDate:1,createdDate:1,_id:1,companyId:1
    }

    var userId = req.query.id;

    userManagements.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),project,function (err,userProfile) {

        if (!err && userProfile) {
            officeOutlook.getNewAccessTokenForMobileSync(userProfile.outlook[0].refreshToken, userProfile, function (newToken) {
                kickOffMailsProcess(newToken, userProfile, req, function (mailsUpdated) {

                });
            });
        }
    })
})

router.get("/get/outlook/mail/collection",function (req,res) {

    var project = {
        emailId:1,serviceLogin:1,google:1,outlook:1,lastMobileSyncDate:1,mobileOnBoardingDate:1,registeredDate:1,createdDate:1,_id:1,penultimateLoginDate:1,companyId:1
    }

    var userId = req.query.userId?req.query.userId:common.getUserIdFromMobileOrWeb(req);
    var fromDate = req.query.after

    userManagements.findUserProfileByIdWithCustomFields(common.castToObjectId(userId.toString()),project,function (err,userProfile) {

        if (!err && userProfile) {

            officeOutlook.getNewAccessTokenForMobileSync(userProfile.outlook[0].refreshToken, userProfile, function (newToken) {

                officeOutlook.getMessageCollection(newToken, new Date(fromDate), function (messages) {

                    var forAnalysis = [];

                    _.each(messages,function (message) {
                        _.each(message,function (ob) {

                            var obj = {
                                headers:[
                                    {
                                        name:"Message-ID",
                                        value:ob.id
                                    },
                                    {
                                        name:"Subject",
                                        value:ob.subject
                                    },
                                    {
                                        name:"From",
                                        value:ob.from.emailAddress.name+' <'+ob.from.emailAddress.address+'>'
                                    }
                                ],
                                body:{
                                    data:ob.body.content
                                }
                            }

                            forAnalysis.push({payload:obj});
                        });
                    });

                    res.send(forAnalysis);

                });
            })
        }
    });
})

module.exports = router;
