
var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');

var userManagement = require('../dataAccess/userManagementDataAccess');
var validations = require('../public/javascripts/validation');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');

var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();

var domain = appCredential.getDomain();

var domainName = domain.domainName;
var userManagements = new userManagement();

var logLib = new winstonLog();

var logger =logLib.getWinston()


router.get('/forgotPassword',function(req,res){
        logger.info('User requested for password reset');
    res.render('forgotPassword');
});

router.post('/sendPasswordResetToken',function(req,res){
        var emailId = req.body.emailId;

    var emailSenders = new emailSender();
    if(checkRequredField(emailId)) {
        userManagements.findUserProfileByEmailIdWithCustomFields(emailId, {emailId:1,password:1,firstName:1,lastName:1},function (error, profile, message) {
            if (error || profile == null) {
                logger.info('Error in db while searching user profile using email id in forgot password router');
                res.send(false);
            }
            else {
                if(checkRequredField(profile.password)){
                    var tokenSecret = "alpha";
                    var token = jwt.encode({ id: profile._id}, tokenSecret);
                    var url = domainName + '/resetPassword/t/' + token
                    var dataToSend = {
                        token:token,
                        url: url,
                        profile: profile
                    }
                    emailSenders.sendForgetPasswordMail(dataToSend);
                    res.send(true);
                }
                else{
                    res.send(false);
                }
            }
        });
    }
    else{
        res.send(false)
    }
});

router.get('/resetPassword/t/:token',function(req,res){
        var token = req.params.token;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(token ,tokenSecret);
    var userId = decoded.id;

    if(checkRequredField(userId)){
        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1},function(error,profile){
            if(error || profile == null){

            }
            else{
                req.session.passwordResetUserId = userId;
                res.render('resetPassword');
            }
        })
    }
});

router.post('/resetPasswordPost',function(req,res){
    var password = req.body.password;
    var userId = req.session.passwordResetUserId || '';

    if(checkRequredField(userId) && checkRequredField(password)){
            userManagements.updateProfilePassword(userId,password,function(error,isUpdated,message){
                if(error){
                    logger.info('Profile password reset failed in db updation '+error);
                    res.send(false)
                }
                else{
                    res.send(isUpdated);
                }

            })
    }
    else res.send(false)
})

    function checkRequredField(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    };

module.exports = router;