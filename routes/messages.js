var express = require('express');
var router = express.Router();

var userManagement = require('../dataAccess/userManagementDataAccess');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var messageDataAccess = require('../dataAccess/messageManagement');
var commonUtility = require('../common/commonUtility');

var common = new commonUtility();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var userManagements = new userManagement();
var messageAccess = new messageDataAccess();

var logLib = new winstonLog();
var logger =logLib.getWinston();

router.get('/messages/all/:UserId',function(req,res){
    res.render('messages');
});


router.get('/messages/all/summary/:userId',function(req,res){
    var otherUserId = req.params.userId;
    var userId = common.getUserId(req.user);
    messageAccess.getMessageInteractions(userId,otherUserId,function(messages){
        res.send(messages);
    })
});

router.get('/messages/getMessage/:messageId',function(req,res){
    var messageId = req.params.messageId;
    messageAccess.getMessageById(messageId,function(message){
        res.send(message)
    })
});

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }else
    {

        return res.redirect('/');
    }
}

module.exports = router;