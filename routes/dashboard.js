
var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var ip = require('ip');
var gcal = require('google-calendar');
var request = require('request');
var fs = require('fs');
var mongoose = require('mongoose')
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;

var userManagement = require('../dataAccess/userManagementDataAccess');
var interactions = require('../dataAccess/interactionManagement');
var validations = require('../public/javascripts/validation');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var contactClass = require('../dataAccess/contactsManagementClass');

var contactObj = new contactClass();
var appCredential = new appCredentials();
var interaction = new interactions();
var logLib = new winstonLog();
var common = new commonUtility();
var userManagements = new userManagement();
var _ = require("lodash")

var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();
var domainName = domain.domainName;


//router.get('/interactions/dashboard/today',function(req,res){
//    var tokenSecret = "alpha";
//    var decoded = jwt.decode(req.user ,tokenSecret);
//    var userId = decoded.id;
//     interaction.interactionsByDateDashboard(userId,function(interactions){
//          res.send(interactions)
//     });
//});

function getUserId(req){
    var decoded = jwt.decode(req.user ,"alpha");
    return decoded.id;
}

router.get('/dashboard/upcomingMeetings/sevenDays',isLoggedInUser,function(req,res){
     var userId = getUserId(req);
    interaction.upcomingMeetingsNextSevenDays(userId,true,function(info){
        res.send(info);

    });
});


router.get('/dashboard/pastMeetings/sevenDays',isLoggedInUser,function(req,res){
    var userId = getUserId(req);
    interaction.upcomingMeetingsNextSevenDays(userId,false,function(info){
        res.send(info);

    });
});

router.get('/dashboard/upcomingMeetings/sevenDays/:userId',isLoggedInUser,function(req,res){
    var userId = req.params.userId;
    interaction.upcomingMeetingsNextSevenDays(userId,true,function(info){
        res.send(info);
    });
});


router.get('/dashboard/pastMeetings/sevenDays/:userId',isLoggedInUser,function(req,res){
    var userId = req.params.userId;
    interaction.upcomingMeetingsNextSevenDays(userId,false,function(info){
        res.send(info);
    });
});

router.get('/dashboard/login/getFlag',function(req,res){
    var flag = req.session.userLoginFlag;
    delete req.session.userLoginFlag;
    res.send(flag);
});

router.get('/dashboard/login/clearFlag',function(req,res){
    delete req.session.userLoginFlag;
    res.send(true);
});

router.get('/dashboard/message/getFlag',function(req,res){
    var flag = req.session.dashboardMsg;
    delete req.session.dashboardMsg;
     res.send(flag);
});

router.get('/dashboard/message/clearFlag',function(req,res){
    delete req.session.dashboardMsg;
    res.send(true);
});

router.get('/dashboard/message/updateFlag',function(req,res){

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    userManagements.updateFirstLoginFlag(userId,function(error,isSuccess,msg){

        res.send(true);
    })
});

function getUserLogInfo(req){
    var userIp = getClientIp(req);
    var userLogInfo;
    if(req.session.userLogInfo){
        userLogInfo = req.session.userLogInfo;
    }
    else{
        userLogInfo = {
            userName:'',
            userIp:userIp,
            loginType:'',
            info:'',
            cause:'',
            action : ''
        }
        req.session.userLogInfo = userLogInfo;
    }
    return userLogInfo;
}

router.get('/dashboard/userProfileWithOutContacts',isLoggedInUser,function(req,res){

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    userManagements.selectUserProfileWitOutContatcs(userId,function(error,profile){
        res.send(profile);
    })
});

router.get('/dashboard/userProfileWithContactsCount',isLoggedInUser,function(req,res){

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    userManagements.selectUserProfileWithContactCount(new mongoose.Types.ObjectId(userId),
        {
            contactsLength: { $size: "$contacts" },
            publicProfileUrl:1,
            firstName:1,
            lastName:1,
            profilePicUrl:1,
            linkedin:1,
            facebook:1,
            twitter:1
        },
        function(error,profile){
        // Send only CONTACTS count not all Contacts array todo
        res.send(profile);
    })
});

router.post('/dashboard/getUsersFromContacts',function(req,res){
    var data = req.body;
    userManagements.findUserProfileByIdWithCustomFields(data.userId,{emailId:1},function(error,userInfo){
        if(!error && common.checkRequired(userInfo)){
            userManagements.getIdInContacts(userInfo._id,userInfo.emailId,function(error,data){
                res.send(data);
            })
        }else res.send([]);
    })
});

router.post('/getUserProfile/byEmailIdArr',function(req,res){
    var arrStr = req.body;
    if(checkRequredField(arrStr) && checkRequredField(arrStr.emailIdArr)){
        var emailIdArr = JSON.parse(arrStr.emailIdArr);
        if(emailIdArr.length > 0){
            userManagements.getUserInfoByArrayOfEmailIdWithCustomFields(emailIdArr,{firstName:1,lastName:1,emailId:1,profilePicUrl:1,publicProfileUrl:1},function(error,list){
                if(!error){
                    res.send(list);
                }else{
                    res.send([])
                }
            })
        }
    }
});

router.post('/userBasicInfoByArrayOfIds',function(req,res){

    var userArr = JSON.parse(req.body.data);
    if(checkRequredField(userArr)){
      userManagements.getBasicUserInfoThreeFieldsByArrayOfId(userArr,function(error,users){
          res.send(users);
      })
    }else res.send(false);
});

function createInteractions(arrInitial,req,res){
    var i=0;
    var limit = 1;
    getInteractionTest(arrInitial);
    function getInteractionTest(arr){
        if(arr.length > 0){
            if(arr.length < 10){
                updateInside(arr);
                sendResponse();
            }
            else{
                var nArr = spliceArray(arr);
                updateInside(nArr[0]);
                setTimeout(function(){
                    if(nArr[1] == arr.length){
                        sendResponse();
                    }else
                        getInteractionTest(arr);
                },0);
            }

        }else sendResponse();
    }

    function spliceArray(arr){

        var length = arr.length;
        if(limit > length) limit = length;

        var newArr = arr.slice(i,limit);
        if(limit == length){
            return [newArr,limit];
        }else{
            i =limit;
            limit = limit+1;
            return [newArr,limit];
        }
    }

    function updateInside(arr){

        arr.forEach(function(obj,index){
            if(checkRequredField(obj.userId)){
                common.storeInteraction(obj)
            }
            else{
                common.storeInteractionReceiver(obj)
            }
        });
    }

    function sendResponse(){

        res.send({
            interacted:0,
            nonInteracted:0,
            total:0,
            totalInteractions:0,
            interactedArrStr:JSON.stringify([])
        })
    }
}

router.post('/dashboard/message/getNotInteractedByInteractions',function(req,res){
    var arrString = req.body;
    var arr = JSON.parse(arrString.data);
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    if(arr.length == 0){
        getInteractedEmailsAndCount(userId,req,res)
    }else{
        createInteractions(arr,req,res);
    }
});

router.get('/dashboard/message/getNewInteractions',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    getInteractedEmailsAndCount(userId,req,res);
});

function getInteractedEmailsAndCount(userId,req,res){
    interaction.interactionEmailsByDate(userId,function(interactionDetails){
       calculateNonInteractedByEmails(userId,interactionDetails,req,res);
    });
}

function calculateNonInteractedByEmails(userId,interactionDetails,req,res){

    var count = interactionDetails.totalInteractions;

    userManagements.totalContacts(userId,function(error,contacts){
        updateDashboardPopup(userId)
        if(contacts.length > 0){

            var contactsCount = contacts[0].contacts.length;
            var emailCount = interactionDetails.interactedEmails.length;
            var nonInteracted = contactsCount - emailCount;

            var obj = {
                interacted:emailCount > contactsCount ? contactsCount : emailCount,
                nonInteracted:nonInteracted > 0 ? nonInteracted : 0,
                total:contacts.length,
                totalInteractions:count,
                interactedArrStr:JSON.stringify(interactionDetails.interactedEmails)
            };

            res.send(obj);
        }else{
            res.send({
                interacted:interactionDetails.interactedEmails.length,
                nonInteracted:0,
                total:0,
                totalInteractions:count,
                interactedArrStr:JSON.stringify(interactionDetails.interactedEmails)
            })
        }
    })
}

function updateDashboardPopup(userId){

    userManagements.updateDashboardPopup(userId,function(error,flag,msg){
        logger.info(msg.message);
    })
}


// Router to get loggedin user total meetings info
router.get('/meetingCount',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for meeting count';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    getTotalAcceptedMeetingsPastFutureCount(userId,function(obj){
        res.send(obj)
    });
});

// Router to get loggedin user total meetings info
router.get('/meetingCount/:userId',isLoggedInUser,function(req,res){

    var userId = req.params.userId;
    getTotalAcceptedMeetingsPastFutureCount(userId,function(obj){
        res.send(obj)
    });
});

function getTotalAcceptedMeetingsPastFutureCount(userId,callback){
    userManagements.getTotalAcceptedMeetings(userId,function(error, invitationData, message){

        if(error){

            callback(false)
        }
        else{

            var now = new Date();
            var pastMeetingsCount = 0;
            var upcomingMeetings = 0;
            invitationData.forEach(function(invitation){
                invitation.scheduleTimeSlots.forEach(function(slot){
                    if(slot.isAccepted == true){

                        if(now < new Date(slot.start.date)){
                            upcomingMeetings++;
                        }
                        else{
                            pastMeetingsCount++;
                        }
                    }
                })
            });
           callback({pastMeetingsCount:pastMeetingsCount,upcomingMeetings:upcomingMeetings});
        }
    });
}

router.get('/totalMeetings',isLoggedInUser,function(req,res){

    var userLogInfo = getUserLogInfo(req);
    userLogInfo.action = 'request for total meetings';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    var meetings = [];
    userManagements.getTotalAcceptedMeetings(userId,function(error, invitationData, message){
        userLogInfo.functionName = 'getTotalMeetings in totalMeetings router'
        if(error){
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching meetings';
            logger.info('Error in mongo',userLogInfo);
            res.send(invitationData);
        }
        invitationData.forEach(function(invitation){
            if(invitation.meetingLocation == undefined || invitation.meetingLocation ==null){

            }else meetings.push(invitation);
        });
        res.send(invitationData);
    });
});


// Router to get today's meetings
router.get('/todayMeetings',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for today meetings';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.getTotalMeetingsUser(userId,function(error, invitationData, message){
        userLogInfo.functionName = 'getTotalMeetings in todayMeetings router'
        if(error){
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching meetings';
            logger.info('Error in mongo',userLogInfo);
            res.send(false)
        }
        else{
            res.send(invitationData);
        }
    });
});

router.get('/updateShareId',function(req,res){
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.updateIdShare(userId,function(error,result){
        res.send(true);
    })
})

// Router to get new invitations count
router.get('/newInvitationsCount',isLoggedInUser,function(req,res){
    var tokenSecret = "alpha";
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for new invitations count and new invitations';
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    getNewInvitations(userId,function(obj){
        res.send(obj)
    });
});


// Router to get new invitations count
router.get('/newInvitationsCount/:userId',isLoggedInUser,function(req,res){

    var userId = req.params.userId;
    getNewInvitations(userId,function(obj){
        res.send(obj)
    });
});

function getNewInvitations(userId,callback){
    var invitationsCount = 0;

    userManagements.newInvitations(userId,function(error,invitations,message){

        if(error){
            callback({invitationsCount:0,invitations:invitations});
        }
        else{
            invitations.forEach(function(invitation){
                invitationsCount++;
            });
            callback({invitationsCount:invitationsCount,invitations:invitations});
        }

    });
}

// Router to get loggedin user total document info(documents shared with others)
router.get('/myDocuments',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for documents of logged in user';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.getDocumentsSharedWithOthers(userId,function(error,documents,message){
        userLogInfo.functionName = 'getDocumentsSharedWithOthers in myDocuments router'
        if(error){
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching documents';
            logger.info('Error in mongo',userLogInfo);
            res.send({docsCount:0,documents:documents});
        }
        else{
            var docsCount = 0;
            documents.forEach(function(document){
                docsCount++;
            });

            res.send({docsCount:docsCount,documents:documents});
        }

    });

});

router.get('/get/uploaded/documents',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagements.getDocumentsSharedWithOthers(userId,function(error,documents,message){
        if(error){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                docsCount:0,
            });
        }
        else{
            var docsCount = 0;
            documents.forEach(function(document){
                _.each(document.sharedWith, function(sharedWith) {
                    var pagesTotalReadTime  = {};
                    var totalViewTime = 0;
                    var pageReadTimeArr = [];

                    _.each(sharedWith.accessInfo.pageReadTimings, function(page) {
                        var pageNum = page.pageNumber;
                        totalViewTime += parseFloat(page.Time);
                        pagesTotalReadTime[pageNum] =  pagesTotalReadTime[pageNum] ? pagesTotalReadTime[pageNum] + parseFloat(page.Time) : parseFloat(page.Time)
                    });

                    _.each(_.keys(pagesTotalReadTime), function(page) {
                        pageReadTimeArr.push({
                            "page": parseInt(page),
                            "readTime": pagesTotalReadTime[page]
                        })
                    });

                    sharedWith.totalViewTime = totalViewTime;
                    sharedWith.groupPagesReadTime =  pageReadTimeArr;
                })
                docsCount++;
            });

            res.send({docsCount:docsCount,documents:documents});
        }

    });

});

router.get('/get/uploaded/documents',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagements.getDocumentsSharedWithOthers(userId,function(error,documents,message){
        if(error){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                docsCount:0,
            });
        }
        else{
            var docsCount = 0;
            documents.forEach(function(document){
                _.each(document.sharedWith, function(sharedWith) {
                    var pagesTotalReadTime  = {};
                    var totalViewTime = 0;
                    var pageReadTimeArr = [];

                    _.each(sharedWith.accessInfo.pageReadTimings, function(page) {
                        var pageNum = page.pageNumber;
                        totalViewTime += parseFloat(page.Time);
                        pagesTotalReadTime[pageNum] =  pagesTotalReadTime[pageNum] ? pagesTotalReadTime[pageNum] + parseFloat(page.Time) : parseFloat(page.Time)
                    });

                    _.each(_.keys(pagesTotalReadTime), function(page) {
                        pageReadTimeArr.push({
                            "page": parseInt(page),
                            "readTime": pagesTotalReadTime[page]
                        })
                    });

                    sharedWith.totalViewTime = totalViewTime;
                    sharedWith.groupPagesReadTime =  pageReadTimeArr;
                })
                docsCount++;
            });

            res.send({docsCount:docsCount,documents:documents});
        }

    });

});

router.get('/get/document/not/accessed',common.isLoggedInUserOrMobile,function(req,res){
    var userId = common.getUserIdFromMobileOrWeb(req);

    userManagements.getNotOpenedDocuments(userId, req.query, "Asia/Kolkata", function(error,documents,message){
        if(error){
            res.send({
                SuccessCode: 0,
                ErrorCode: 1,
                docsCount:0,
            });
        }
        else{
            var docsCount = 0;

            _.remove(documents, function(doc) {
                if(!doc.sharedWith || (doc.sharedWith.length == 0)) {
                    return true;
                }
            })

            documents.forEach(function(document){
                _.each(document.sharedWith, function(sharedWith) {
                    var pagesTotalReadTime  = {};
                    var totalViewTime = 0;
                    var pageReadTimeArr = [];

                    _.each(sharedWith.accessInfo.pageReadTimings, function(page) {
                        var pageNum = page.pageNumber;
                        totalViewTime += parseFloat(page.Time);
                        pagesTotalReadTime[pageNum] =  pagesTotalReadTime[pageNum] ? pagesTotalReadTime[pageNum] + parseFloat(page.Time) : parseFloat(page.Time)
                    });

                    _.each(_.keys(pagesTotalReadTime), function(page) {
                        pageReadTimeArr.push({
                            "page": parseInt(page),
                            "readTime": pagesTotalReadTime[page]
                        })
                    });

                    sharedWith.totalViewTime = totalViewTime;
                    sharedWith.groupPagesReadTime =  pageReadTimeArr;
                })
                docsCount++;
            });

            res.send({docsCount:docsCount,documents:documents});
        }

    });

});

// Router to get loggedin user total document info(documents shared with me)
router.get('/documentsSharedWithMe',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for documents of logged in user';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.documentsSharedWithMe(userId,function(error,documents,message){
        userLogInfo.functionName = 'documentsSharedWithMe in documentsSharedWithMe router'
        if(error){
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching documents';
            logger.info('Error in mongo',userLogInfo);
            res.send({docsCount:0,documentsSharedWithMe:documents});
        }
        else{
            var docsCount = 0;
            documents.forEach(function(document){
                docsCount++;
            });

            res.send({docsCount:docsCount,documentsSharedWithMe:documents});
        }

    });

});

// Router to get total documents count for one user
router.get('/totalDocsCount',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for total documents count';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;


    userManagements.getDocumentsRecCount(userId,function(count){
        res.send({docsCount:count});
    });
});

// Router to get total documents count for one user
router.get('/totalDocsCount/:userId',isLoggedInUser,function(req,res){

    var userId = req.params.userId;
    userManagements.getDocumentsRecCount(userId,function(count){
        res.send({docsCount:count});
    });
});

// Router to get total contacts of loggedin user
router.get('/totalContacts',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for total contacts count';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    getTotalContacts(userId,function(obj){
        res.send(obj)
    });
});

router.get('/totalContacts/:userId',isLoggedInUser,function(req,res){

    var userId = req.params.userId;
    getTotalContacts(userId,function(obj){
        res.send(obj)
    });
});

function getTotalContacts(userId,callback){
    var contactCount = 0;
    userManagements.selectUserProfileWithContactCount(new mongoose.Types.ObjectId(userId),
        {
            contactsLength: { $size: "$contacts" }
        },
        function(error,count){
        if(error){
            callback({contactCount:contactCount});
        }
        else{
            callback({contactCount:count.contactsLength});
        }
    });
}

router.post('/getProfileUsingId', isLoggedInUser,function(req, res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for get user profile using id';
    try {

        var userId = req.body.id;

        if(userId != ''){
            userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1,profilePicUrl:1,publicProfileUrl:1},function(error,userProfile) {
                userLogInfo.functionName = 'selectUserProfile in getProfileUsingId router'
                if (error) {
                    userLogInfo.error= error;
                    userLogInfo.info = 'Error in mongo db while searching user profile using id';
                    logger.info('Error in mongo',userLogInfo);
                    userProfile = null;
                    res.send(userProfile);

                }
                else{
                    userLogInfo.info = 'User profile found in db';
                    logger.info('Transferring user profile found by using id',userLogInfo);
                    res.send(userProfile);
                }
            });
        }
        else res.send(false)
    }
    catch (ex) {
        logger.error('Exception in dashboard js in getProfileUsingId ', ex.stack);
        throw ex;
    }
});

router.get('/getParticipantProfile/:emailId',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for get participant profile using email';
    var emailId = req.params.emailId;

    userManagements.findUserProfileByEmailIdWithCustomFields(emailId,{emailId:1,firstName:1,lastName:1,publicProfileUrl:1},function(error,userProfile){
        userLogInfo.functionName = 'findUserProfileByEmailId in getParticipantProfile/:emailId router';
        if(error){
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching user profile using email';
            logger.info('Error in mongo',userLogInfo);
            res.send('');
        }else if(userProfile != null){

            userLogInfo.info = 'User profile found in db';
            logger.info('Transferring user profile found by using email',userLogInfo);

            res.send(userProfile);
        }
        else{
            res.send('');
        }

    })
});

router.post('/uploadDocument',function(req,res){

    var doc = req.body;

    storeDocumentInfo(doc,req,res);

});

// Router to add invitees to existing meeting
router.get('/addInvitees/:invitationId/:email',isLoggedInUser,function(req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for add invitees to existing event ';
    var validate = new validations();
    var emailsText = req.params.email;
    emailsText +=',abc';
    var emails = emailsText.split(',');

    var invitees = {
        invitationId:req.params.invitationId,
        invitees:[]
    };
    for (var i=0;i<emails.length-1;i++){
        validate.validateEmail(emails[i],function(isValid){
            if(isValid){
                invitees.invitees.push({
                    emailId:emails[i]
                });
            }
        });

    }


    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.updateMeetingWithNewInvities(invitees.invitationId,invitees.invitees,function(error,isUpdated,message){
        userLogInfo.functionName = 'updateMeetingWithNewInvities  in addInvitees/:invitationId/:email router';
        if(error){
            userLogInfo.error= error ;
            userLogInfo.info = 'Updating invitees in meeting failed';
            logger.info('Error in mongo',userLogInfo);
        }
        else if(isUpdated){
            userLogInfo.info = 'updating meeting with new invitees success'
            logger.info('Updating meeting success',userLogInfo);
            updateInvitiesInGoogle(invitees.invitationId,invitees.invitees,userId,req,res);
        }
        else{
            userLogInfo.info = 'updating meeting with new invitees failed'
            logger.info('Updating meeting failed',userLogInfo);
            res.send(false);
        }
    });
});

router.post('/addDocToMeeting',isLoggedInUser,function(req,res){

    var validation = new validations();
    var emailSenders = new emailSender();
    var userLogInfo = getUserLogInfo(req);
    var docInfo = req.body;
    var docId = docInfo.documentId;

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    var participantsArray = docInfo.participants.split(',');
    userLogInfo.action = 'request for updating meeting with document';
    userManagements.findDocumentById(docId,function(error,document,message) {
        userLogInfo.functionName = 'findDocumentById in addDocToMeeting router';
        if (error){
            userLogInfo.error = error;
            userLogInfo.info = 'Error in finding document with doc id';
            logger.error('Error in mongo', userLogInfo);
            res.send(false);
        }
        else{
            if(checkRequredField(document)){
                var shareDetails = {
                    docId:docId,
                    emails:[]
                };
                for(var i=0;i<participantsArray.length;i++){
                    validateEmail(participantsArray[i],function(result){
                        if(result != null){
                            shareDetails.emails.push(result)
                        }

                    })
                }
                setTimeout(function(){
                    if(checkRequredField(shareDetails.emails[0])){
                        userManagements.shareDocument(shareDetails,function(error,isSuccess,message){
                            userLogInfo.functionName = 'shareDocument in addDocToMeeting router';
                            if(error || isSuccess == false){
                                userLogInfo.error = error;
                                userLogInfo.info = 'Error in sharing document';
                                logger.error('Error in mongo',userLogInfo);

                                res.send(false);

                            }
                            else if(isSuccess){
                                if(checkRequredField(shareDetails.emails)){
                                    if(checkRequredField(shareDetails.emails[0])){

                                        var interaction = {};
                                        interaction.userId = userId;
                                        interaction.action = 'sender';
                                        interaction.type = 'document-share';
                                        interaction.subType = 'document-share';
                                        interaction.refId =  docId;
                                        interaction.source = 'relatas';
                                        interaction.title = document.documentName;
                                        interaction.description = '';
                                        common.storeInteraction(interaction);

                                        for(var i=0; i<shareDetails.emails.length; i++){
                                            if(checkRequredField(userId) && shareDetails.emails[i].userId){
                                                updateReceiverContacts(userId,shareDetails.emails[i].userId);
                                                updateReceiverContacts(shareDetails.emails[i].userId,userId);
                                            }
                                            var interactionReceiver = {};
                                            interactionReceiver.userId = shareDetails.emails[i].userId || '';
                                            interactionReceiver.emailId = shareDetails.emails[i].emailId;
                                            interactionReceiver.action = 'receiver';
                                            interactionReceiver.type = 'document-share';
                                            interactionReceiver.subType = 'document-share';
                                            interactionReceiver.refId = docId;
                                            interactionReceiver.source = 'relatas';
                                            interactionReceiver.title = document.documentName;
                                            interactionReceiver.description = '';
                                            common.storeInteractionReceiver(interactionReceiver);
                                        }
                                    }
                                }
                                var docInfoForMeeting = {
                                    documentName:document.documentName,
                                    documentUrl:document.documentUrl,
                                    documentId:document._id
                                }
                                userManagements.updateInvitationWithDocument(docInfo.invitationId,docInfoForMeeting,function(error,isUpdated,message){
                                    if(error || !isUpdated){
                                        userLogInfo.error= error;
                                        userLogInfo.info = 'Error in mongo db while updating invitation with document';
                                        logger.info('Error in mongo, updateInvitationWithDocument function',userLogInfo);
                                        res.send(false);
                                    }
                                    else{
                                        for(var p=0; p<shareDetails.emails.length; p++){
                                            var mailDataToSend = {
                                                uploadedBy:document.sharedBy,
                                                receivedBy:shareDetails.emails[p],
                                                docInfo   :docInfoForMeeting
                                            }
                                            emailSenders.sendDocUploadConfirmationMailToReceiver(mailDataToSend);

                                        }
                                        res.send(true);
                                    }
                                });
                            }
                            else res.send(isSuccess)
                        })
                    }
                    else res.send(false);
                },100)
            }
            else res.send(false);
        }
    })
});

router.get('/privacy',function(req,res){
    res.render('privacy',common.getLoginStatus(req));
});

router.get('/contact',function(req,res){
    res.redirect('/company');
});

router.get('/about',function(req,res){
    res.redirect('/company');
});

router.get('/terms',function(req,res){
    res.render('termsOfService',common.getLoginStatus(req));
});

router.get('/sub-processor',function(req,res){
    res.render('subProcessor',common.getLoginStatus(req));
});
router.get('/data-processing-addendum',function(req,res){
    res.render('dataProcessingAddendum',common.getLoginStatus(req));
});

router.get('/relationship-intelligence-company',function(req,res){
    res.render('company',common.getLoginStatus(req));
});

router.get('/FAQ',function(req,res){
    res.render('FAQ',common.getLoginStatus(req));
});

router.get('/relationship-cartoon',function(req, res){
    res.render('surveys/beLikeMe',common.getLoginStatus(req));
});

router.get('/relationship-intelligence-product',function(req,res){
    res.render('Relationship-Intelligence',common.getLoginStatus(req));
});

function updateInvitiesInGoogle(invitationId,invitees,userId,req,res){

    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for updating invitees in google';
    userManagements.findInvitationById(invitationId,function(iError,invitation){
        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1,lastName:1},function(error,userProfile){
            userLogInfo.functionName = 'findInvitationById and selectUserProfile in updateInvitiesInGoogle';
            if(iError || error){
                userLogInfo.error= error || iError;
                userLogInfo.info = 'Updating invitees in google failed, Error in mongo db while searching user profile using id';
                logger.info('Error in mongo',userLogInfo);
                res.send(false)
            }
            else{

                var userInfo = userProfile;
                if(userInfo.google[0].token == ''){
                    logger.info('No Google account found in '+userInfo.firstName+'account',userLogInfo);
                }
                else{
                    var tokenProvider = new GoogleTokenProvider({
                        refresh_token: userInfo.google[0].refreshToken,
                        client_id:     authConfig.GOOGLE_CLIENT_ID,
                        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                        access_token: userInfo.google[0].token
                    });
                    tokenProvider.getToken(function (err, token) {
                        if (err) {
                            userLogInfo.error = err;
                            userLogInfo.info = 'Error while refreshing token';

                            logger.info('Error in token provider module',userLogInfo);
                        }

                        var google_calendar = new gcal.GoogleCalendar(token);
                        var calendarId='primary';
                        var eventId = invitation.googleEventId;
                        var request_body = {
                            attendees:[],
                            end:{
                                dateTime:null
                            },
                            start: {
                                dateTime: null
                            }
                        };
                        invitees.forEach(function(person){
                            request_body.attendees.push({
                                email:person.emailId
                            })
                        });
                        invitation.scheduleTimeSlots.forEach(function(slot){
                            if(slot.isAccepted == true){
                                request_body.end.dateTime = slot.end.date;
                                request_body.start.dateTime = slot.start.date;
                            }
                        });

                        google_calendar.events.update(calendarId,eventId,request_body,function(err,result){

                            if(err){
                                userLogInfo.error = err;
                                userLogInfo.info = 'Error while updating google event with invities';

                                logger.info('Error in Google event update',userLogInfo);

                                res.send(false);
                            }
                            else{
                                userLogInfo.info = 'updating event in google with invitees success'
                                logger.info('updating event in google success',userLogInfo);

                                res.send(true);
                            }
                        });
                    }) // end of token provider
                }

            }
        })
    });
}

// Function to store document info
function storeDocumentInfo(docInfo,req,res){
    var userLogInfo = getUserLogInfo(req);

    userLogInfo.action = 'request for store doc info';
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,firstName:1,lastName:1, profilePicUrl:1, companyName:1, companyId: 1},function(error,profile){
        userLogInfo.functionName = 'selectUserProfile in storeDocumentInfo function';
        if(docInfo.participants)
            var participantsArray = docInfo.participants.split(',');

        if(error){
            userLogInfo.error= error;
            userLogInfo.info = 'Error in mongo db while searching user profile using id';
            logger.info('Error in mongo',userLogInfo);
        }

        if(checkRequredField(docInfo.documentName) && checkRequredField(docInfo.documentCategory)){

            var docDetails = {
                sharedBy:{
                    userId:profile._id,
                    emailId:profile.emailId,
                    firstName:profile.firstName,
                    lastName:profile.lastName,
                    companyName:profile.companyName,
                    companyId:profile.companyId,
                    profilePicUrl:profile.profilePicUrl
                },
                sharedWith:[],
                awsKey:docInfo.awsKey,
                documentName:docInfo.documentName,
                documentCategory:docInfo.documentCategory,
                documentUrl:docInfo.documentUrl,
                access:docInfo.access == 'public' ? 'public': 'private',
                sharedDate:docInfo.sharedDate
            };

            if(docInfo.participants) {
                for(var i=0;i<participantsArray.length;i++){
                    validateEmail(participantsArray[i],function(result){
                        if(result != null){
                            docDetails.sharedWith.push(result)
                        }
    
                    })
                }
            }
            if(docInfo.invitationId == 'no'){

                userManagements.storeDocument(docDetails,function(error,document,message){
                    if(error || !checkRequredField(document)){
                        userLogInfo.error= error;
                        userLogInfo.info = 'Error in mongo db while storing document info';
                        logger.info('Error in mongo',userLogInfo);
                        res.send(false);
                    }
                    else{
                        if(checkRequredField(document.sharedWith)){
                            if(checkRequredField(document.sharedWith[0])){
                                var interaction = {};
                                interaction.userId = userId;
                                interaction.action = 'sender';
                                interaction.type = 'document-share';
                                interaction.subType = 'document-share';
                                interaction.refId =  document._id;
                                interaction.source = 'relatas';
                                interaction.title = document.documentName;
                                interaction.description = '';
                                common.storeInteraction(interaction);
                                for(var i=0; i<document.sharedWith.length; i++){
                                    if(checkRequredField(userId) && document.sharedWith[i].userId){
                                        updateReceiverContacts(userId,document.sharedWith[i].userId);
                                        updateReceiverContacts(document.sharedWith[i].userId,userId);
                                    }
                                    var interactionReceiver = {};
                                    interactionReceiver.userId = document.sharedWith[i].userId || '';
                                    interactionReceiver.emailId = document.sharedWith[i].emailId;
                                    interactionReceiver.action = 'receiver';
                                    interactionReceiver.type = 'document-share';
                                    interactionReceiver.subType = 'document-share';
                                    interactionReceiver.refId = document._id;
                                    interactionReceiver.source = 'relatas';
                                    interactionReceiver.title = document.documentName;
                                    interactionReceiver.description = '';
                                    common.storeInteractionReceiver(interactionReceiver);
                                }
                            }
                        }
                        res.send(document);
                    }
                });
            }
            else{
                setTimeout(function(){
                    var docInfoForMeeting = {
                        documentName:docDetails.documentName,
                        documentUrl:docDetails.documentUrl
                    }

                    userManagements.storeDocument(docDetails,function(error,document,message){
                        if(error || document == null){
                            userLogInfo.error= error;
                            userLogInfo.info = 'Error in mongo db while storing document';
                            logger.info('Error in mongo, storeDocument function',userLogInfo);
                            res.redirect('/');
                        }
                        else{
                            if(checkRequredField(document.sharedWith)){
                                if(checkRequredField(document.sharedWith[0])){

                                        var interaction = {};
                                        interaction.userId = userId;
                                        interaction.action = 'sender';
                                        interaction.type = 'document-share';
                                        interaction.subType = 'document-share';
                                        interaction.refId =  document._id;
                                        interaction.source = 'relatas';
                                        interaction.title = document.documentName;
                                        interaction.description = '';
                                        common.storeInteraction(interaction);

                                    for(var i=0; i<document.sharedWith.length; i++){
                                        if(checkRequredField(userId) && document.sharedWith[i].userId){
                                            updateReceiverContacts(userId,document.sharedWith[i].userId);
                                            updateReceiverContacts(document.sharedWith[i].userId,userId);
                                        }



                                        var interactionReceiver = {};
                                        interactionReceiver.userId = document.sharedWith[i].userId || '';
                                        interactionReceiver.emailId = document.sharedWith[i].emailId;
                                        interactionReceiver.action = 'receiver';
                                        interactionReceiver.type = 'document-share';
                                        interactionReceiver.subType = 'document-share';
                                        interactionReceiver.refId = document._id;
                                        interactionReceiver.source = 'relatas';
                                        interactionReceiver.title = document.documentName;
                                        interactionReceiver.description = '';
                                        common.storeInteractionReceiver(interactionReceiver);
                                    }
                                }
                            }
                            docInfoForMeeting.documentId = document._id;
                            userManagements.updateInvitationWithDocument(docInfo.invitationId,docInfoForMeeting,function(error,isUpdated,message){
                                if(error || !isUpdated){
                                    userLogInfo.error= error;
                                    userLogInfo.info = 'Error in mongo db while updating invitation with document';
                                    logger.info('Error in mongo, updateInvitationWithDocument function',userLogInfo);
                                    res.redirect('/');
                                }
                                else{
                                    // sendDocUploadConfirmationMails(docDetails,docInfoForMeeting,docInfo);
                                    res.redirect('/');
                                }
                            });
                        }
                    });
                },1500);
            }
        } else {
            res.send({
                SuccessCode: 0,
                ErrorCode: 1, 
                message: "Document name or Document category is missing"
            })
        }
    });
}

function validateEmail(participant,callback){
    var validation = new validations()
    validation.validateEmail(participant,function(isValid){
        if(isValid){
            getProfileObj(participant,function(obj){
                callback(obj)
            })
        }else callback(null)
    })
}

function getProfileObj(participant,callback){

    userManagements.findUserProfileByEmailIdWithCustomFields(participant,{emailId:1,firstName:1,lastName:1},function(error,profile){
        if(profile != null){
            callback({
                userId:profile._id,
                emailId:profile.emailId,
                firstName:profile.firstName+' '+profile.lastName,
                accessStatus:true,
                sharedOn:new Date()
            });
        }else{
            callback({
                userId:'',
                emailId:participant,
                firstName:'',
                accessStatus:true,
                sharedOn:new Date()
            });
        }
    })
}

function sendDocUploadConfirmationMails(docInfo,docInfoForMeeting,docFromClient){

    var emailSenders = new emailSender();
    for (var i in docInfo.sharedWith){
        if(docInfo.sharedWith[i].userId != docInfo.sharedBy.userId){


            var mailDataToSend = {
                uploadedBy:docInfo.sharedBy,
                receivedBy:docInfo.sharedWith[i],
                docInfo   :docInfoForMeeting
            }

            if(checkRequredField(mailDataToSend.receivedBy)){

                emailSenders.sendDocUploadConfirmationMailToReceiver(mailDataToSend);
            }

        }
    }
}

//Function to add Contact
function updateContact(userId,contact){

    contactObj.addSingleContact(userId,contact)
}

function updateReceiverContacts(senderId,receiverId){

    userManagements.findUserProfileByIdWithCustomFields(senderId,{firstName:1,lastName:1,emailId:1,mobileNumber:1},function(error,profile) {

        if(error){

        }else
        if(profile != null || profile != undefined){

            var contact1 = {
                personId:profile._id,
                personName:profile.firstName+' '+profile.lastName,
                personEmailId:profile.emailId,
                count:1,
                lastInteracted:new Date(),
                addedDate: new Date(),
                verified:true,
                relatasContact:true,
                relatasUser:true,
                mobileNumber:profile.mobileNumber
            }
            updateContact(receiverId,contact1);
        }
    });
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;

    }
    return ipAddress;
}

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }else
    {

        // if they aren't redirect them to the home page

        return res.redirect('/');
    }
}


function checkRequredField(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
};

module.exports = router;