var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var async = require("async")
var request = require('request');
var passport = require('passport');
var csv = require('ya-csv');
var fs = require('fs');
var json2csv = require('nice-json2csv');
var _ = require("lodash");
var momentTZ = require('moment-timezone');
var moment = require("moment")
// var app = express();
router.use(json2csv.expressDecorator);
var json2xls = require('json2xls');

var userLogCollection = require('../databaseSchema/userManagementSchema').userlog;
var adminCollection = require('../databaseSchema/userManagementSchema').adminCollection;
var userManagement = require('../dataAccess/userManagementDataAccess');
var createDocumentManagement = require('../dataAccess/documentManagement');
var insightsManagement = require('../dataAccess/insightsManagement');
var videosManagement = require('../dataAccess/marketingMaterial/videosManagement');
var enterPriceRequest = require('../dataAccess/enterpriseSignUpRequestClass');
var eventDataAccess = require('../dataAccess/eventDataAccess');
var messageDataAccess = require('../dataAccess/messageManagement');
var emailSender = require('../public/javascripts/emailSender');
var winstonLog = require('../common/winstonLog');
var commonUtility = require('../common/commonUtility');
var linkedin = require('../common/linkedin');
var interactionManagement = require('../dataAccess/interactionManagement');
var documentManagement = require('../dataAccess/documentManagement');
var contactsManagement = require('../dataAccess/contactsManagementClass');
var meetingManagement = require('../dataAccess/meetingManagement');
var massMailCountManagement = require('../dataAccess/massMailCountManagement');
var calendarPasswordManagement = require('../dataAccess/calendarPasswordManagement');
var migrateInteractionsToNewStructure = require('../dataAccess/migrateInteractionsToNewStructure');
var userLogManagement = require('../dataAccess/userLogManagement');
var relationshipStrength = require('../dataAccess/relationshipStrength');
var socialBatchAPI = require('../batchAPI/socialBatchAPI');
var migrateMeetingToCalendar = require('../dataAccess/migrationMeetingToCalendar');
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var opportunityManagement = require('../dataAccess/opportunitiesManagement');
var googleCalendarAPI = require('../common/googleCalendar');

var logLib = new winstonLog();
var logger = logLib.getWinston();
var common = new commonUtility();
var socialBatchAPIObj = new socialBatchAPI();
var userManagements = new userManagement();
var createDocumentManagements = new createDocumentManagement();
var migrateInteractionsToNewStructureObj = new migrateInteractionsToNewStructure();
var interactions = new interactionManagement();
var emailSenders = new emailSender();
var enterPriceRequestObj = new enterPriceRequest();
var messageAccess = new messageDataAccess();
var eventAccess = new eventDataAccess();
var documentAccess = new documentManagement();
var meetingClassObj = new meetingManagement();
var massMailCountManagementObj = new massMailCountManagement();
var calendarPasswordManagementObj = new calendarPasswordManagement();
var userLogManagementObj = new userLogManagement();
var relationshipStrengthObj = new relationshipStrength();
var migrateMeeting = new migrateMeetingToCalendar();
var insights = new insightsManagement();
var contactsManagementObj = new contactsManagement();
var oppManagementObj = new opportunityManagement();
var videosManagementObj = new videosManagement();
var googleCalendar = new googleCalendarAPI();

var corporateAdminChangeLog = logLib.getAdminChangesLogger();

function getUserProfile(userId, back) {
    userManagements.findUserProfileByIdWithCustomFields(userId, { emailId: 1, admin: 1 }, back);
}

function authenticateAdmin(req, res, callbask) {
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user, tokenSecret);
    var userId = decoded.id;
    getUserProfile(userId, function(error, user) {
        if (common.checkRequired(user)) {
            if (user.admin == true) {
                callbask(true);
            } else callbask(false);
        } else callbask(false);
    })
}

router.get('/admin/run/remove/invalid/interaction/content', common.isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            interactions.removeInvalidInteractions({}, common.getInvalidEmailList());
            res.send('OK')
        } else res.render('error');
    })
});

router.get('/admin/run/twitter/batch', common.isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            socialBatchAPIObj.runTwitterBatch();
            res.send('OK')
        } else res.render('error');
    })
});

router.get('/admin/get/all/companies', common.isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag || !flag) {
            companyCollection.find({},{companyName:1}).exec(function(error, companies){
                if(!error){
                    res.send(companies)
                }
            });
        } else res.render('error');
    })
});

router.get('/admin/run/linkedin/batch', common.isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            socialBatchAPIObj.runLinkedinBatch();
            res.send('OK')
        } else res.render('error');
    })
});

router.get('/admin/run/location/migration/batch', common.isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            socialBatchAPIObj.runLocationMigration();
            res.send('OK')
        } else res.render('error');
    })
});

router.get('/admin/run/migration/interactions', common.isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            migrateInteractionsToNewStructureObj.runInteractionMigration();
            res.send('OK')
        } else res.render('error');
    })
});

router.get('/admin/run/migration/list', common.isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            migrateMeeting.runMeetingMigration();
            res.send('OK');
        } else {
            res.render('error');
        }
    })
});

router.get('/admin/run/migration/meeting', common.isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            migrateMeeting.migrateCalendar();
            res.send('OK');
        } else {
            res.render('error');
        }
    })
});

router.get('/admin/relationship/strength/doc', common.isLoggedInUserToGoNext, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            relationshipStrengthObj.getRelationshipStrengthDoc(function(error, doc) {
                if (error) {
                    res.send({
                        "SuccessCode": 0,
                        "Message": "",
                        "ErrorCode": 1,
                        "Data": {}
                    });
                } else res.send({
                    "SuccessCode": 1,
                    "Message": "",
                    "ErrorCode": 0,
                    "Data": doc
                });
            })
        } else res.send({
            "SuccessCode": 0,
            "Message": "UN_AUTHORISED_ACCESS",
            "ErrorCode": 5004,
            "Data": {}
        });
    })
});

router.post('/admin/relationship/strength/update/doc', common.isLoggedInUserToGoNext, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            var update = {};
            var isUpdate = false;
            if (common.checkRequired(req.body.minRatio)) {
                update.minRatio = req.body.minRatio;
                isUpdate = true;
            }
            if (common.checkRequired(req.body.maxRatio)) {
                update.maxRatio = req.body.maxRatio;
                isUpdate = true;
            }

            if (isUpdate) {
                relationshipStrengthObj.updateRelationshipStrengthDoc(req.body.docId, { minRatio: req.body }, function(error, doc) {
                    if (error) {
                        res.send({
                            "SuccessCode": 0,
                            "Message": "DB_ERROR",
                            "ErrorCode": 1,
                            "Data": {}
                        });
                    } else res.send({
                        "SuccessCode": 1,
                        "Message": "",
                        "ErrorCode": 0,
                        "Data": doc
                    });
                })
            } else res.send({
                "SuccessCode": 0,
                "Message": "INVALID_DATA_RECEIVED",
                "ErrorCode": 5000,
                "Data": {}
            });
        } else res.send({
            "SuccessCode": 0,
            "Message": "UN_AUTHORISED_ACCESS",
            "ErrorCode": 5004,
            "Data": {}
        });
    })
});

/******* user interactions*********/
router.get('/admin/getAllUserInteractions', isLoggedInUser, function(req, res) { // to get all interactions
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            interactions.getAllUserInteractions(req.query.from, req.query.to, req.query.category, function(userInteraction) {
                res.send(userInteraction);
            });
        } else res.render('error');
    });
});

router.get('/admin/getListOfCompanies', isLoggedInUser, function(req, res) { // to get all interactions
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            interactions.getListOfCompanies(function(listOfCompanies) {
                res.send(listOfCompanies);
            });
        } else res.render('error');
    });
});

router.get('/admin/csv/getAllUserInteractions/:from/:to/:category', isLoggedInUser, function(req, res) { // to export all interactions
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            interactions.getAllUserInteractions(req.params.from, req.params.to, req.params.category, function(userInteractions) {
                var csvFormat = [];
                var userInteraction = userInteractions.array;
                for (var i = 0; i < userInteraction.length; i++)
                    csvFormat.push({
                        Email: userInteraction[i].email,
                        'C/R': userInteraction[i].userType,
                        Google: null,
                        G_email: userInteraction[i].google.email,
                        G_meeting: userInteraction[i].google.meeting,
                        G_total: userInteraction[i].google.total,
                        Linkedin: null,
                        L_share: userInteraction[i].linkedin.share,
                        L_like: userInteraction[i].linkedin.like,
                        L_comment: userInteraction[i].linkedin.comment,
                        L_total: userInteraction[i].linkedin.total,
                        Facebook: null,
                        F_facebook: userInteraction[i].facebook,
                        F_total: userInteraction[i].facebook,
                        Twitter: null,
                        T_twitter: userInteraction[i].twitter,
                        T_total: userInteraction[i].twitter,
                        Relatas: null,
                        R_calanderPassword: userInteraction[i].relatas.calander,
                        R_documentShare: userInteraction[i].relatas.documentShare,
                        R_email: userInteraction[i].relatas.email,
                        R_meetigComment: userInteraction[i].relatas.meeting,
                        R_message: userInteraction[i].relatas.message,
                        R_meeting: null,
                        R_meeting_conferenceCall: userInteraction[i].relatas.call,
                        R_meeting_inPerson: userInteraction[i].relatas.inPerson,
                        R_meeting_phone: userInteraction[i].relatas.phone,
                        R_meeting_skype: userInteraction[i].relatas.skype,
                        R_total: userInteraction[i].relatas.total,
                        Mobile:null,
                        M_call: userInteraction[i].mobile.call,
                        M_sms: userInteraction[i].mobile.sms,
                        M_total: userInteraction[i].mobile.total,
                        GrandTotal: userInteraction[i].grandTotal
                    });
                csvFormat.sort(function(a, b) {
                    return parseFloat(b.GrandTotal) - parseFloat(a.GrandTotal);
                });
                res.csv(csvFormat, 'interactionReport.csv');
            });
        } else res.render('error');
    });
});

router.get('/admin/user/interactions', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/adminUserInteraction.html');
        } else res.render('error');
    });
});

router.get('/admin/user/notifications', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/adminUserNotification.html');
        } else res.render('error');
    });
});

router.get('/admin/user/war', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/adminUserWar.html');
        } else res.render('error');
    });
});

router.get('/admin/user/insights/action/log', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/adminInsightsActionLog.html');
        } else res.render('error');
    });
});
/********************/
router.get('/admin/user/insights/action/log/info', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            insights.insightsActionLogForAdmin(req.query.from, req.query.to, req.query.infoFor, function(err, result) {
                if (!err && result) {
                    if (req.query.infoFor == 'csv') {
                        res.csv(result, 'insightsActionLogReport.csv');
                    } else
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            Data: result
                        })
                } else {
                    // res.send(errorObj.generateErrorResponse({ status: statusCodes.SOMETHING_WENT_WRONG_CODE, key: 'SOMETHING_WENT_WRONG' }))
                    res.send({
                            SuccessCode: 0,
                            ErrorCode: 1,
                            Data: result
                        })
                }
            })
        } else res.render('error');
    });
});

router.get('/admin/growth/report', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/adminGrowthReport.html');
        } else res.render('error');
    });
});

router.get('/admin/growth/report/info', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            var timezone = req.session.profile && req.session.profile.timezone && req.session.profile.timezone.name ? req.session.profile.timezone.name : 'UTC';
            var newDate =  momentTZ(req.query.endDate).tz(timezone)
            var dateMax = new Date(moment(req.query.endDate).endOf('month'));
            var dateMin = new Date(moment(req.query.endDate))
            var date = {min:dateMin, max:dateMax};
            companyCollection.find({createdDate:{$lt:new Date(date.max)}},{_id:1,companyName:1}).exec(function(error, companies){
                if(error){
                    logger.error('Error in geting company details for growth report', error);
                    res.send({
                        SuccessCode: 0,
                        ErrorCode: 1,
                        Data: {}
                    })
                }
                else{
                    var accounts = 0;
                    var companyIds = [];
                    var userQuery = {createdDate:{$lt:new Date(date.max)}, resource: { $ne: true }}

                    if(req.query.filter == 'business') {
                        accounts = companies.length ? companies.length : 0;
                        companyIds = _.compact(_.pluck(companies, '_id'));
                        userQuery["corporateUser"] = true;
                        userQuery["companyId"] = {$in:companyIds}
                    }
                    else if(req.query.filter == 'individuals'){
                        userQuery["corporateUser"] = {$ne:true};
                    }
                    if((companyIds.length > 0 && req.query.filter == 'business') || req.query.filter == 'individuals') {
                        userManagements.selectUserProfilesCustomQuery(userQuery, {_id: 1, emailId:1}, function (error, userArray) {
                            if(error){
                                logger.error('Error in admin - geting user count for growth report', error);
                                res.send({
                                    SuccessCode: 0,
                                    ErrorCode: 1,
                                    Data: {}
                                })
                            }
                            else if(userArray){
                                var userEmails = [];
                                var users = userArray ? userArray.length : 0;
                                userEmails = _.compact(_.pluck(userArray, 'emailId'));

                                userManagements.getUserContactsCountByDateAndCompany(companyIds, date, req.query.filter, function (company) {
                                    var contacts = 0;
                                    company.forEach(function(a){
                                        contacts += a.contacts;
                                    })

                                    interactions.totalUsersInteractionsByEmail(userEmails, date, function(interactions){
                                        var interaction = interactions;
                                        res.send({
                                            SuccessCode: 1,
                                            ErrorCode: 0,
                                            Data: {accounts: accounts, users: users, contacts: contacts, interactions: interaction, filter:req.query.filter}
                                            //Data:company
                                        })
                                    })
                                })
                            }
                            else{
                                res.send({
                                    SuccessCode: 1,
                                    ErrorCode: 0,
                                    Data: {accounts:0, users:0, contacts:0, interactions:0, filter:req.query.filter}
                                })
                            }
                        })
                    }
                    else{
                        res.send({
                            SuccessCode: 1,
                            ErrorCode: 0,
                            Data: {accounts:0, users:0, contacts:0, interactions:0}
                        })
                    }
                }
            })
        } else res.render('error');
    });
});

/*************redundant contacts *****/
router.get('/admin/user/redundantContacts', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/adminRedundantContact.html');
        } else res.render('error');
    });
});

router.get('/admin/marketing-materials', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/marketingMaterial.html');
        } else res.render('error');
    });
});

router.get('/admin/getRedundantContacts', isLoggedInUser, function(req, res) { // to get all interactions
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            userManagements.getRedundantContacts(function(redundantContacts) {
                res.send(redundantContacts);
            })
        } else res.render('error');
    });
});

router.post('/admin/user/deleteRedundantContacts', isLoggedInUser, function(req, res) { // to get all interactions
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            var contactsToBeDeleted = req.body;

            userManagements.deleteRedundantContact(contactsToBeDeleted, function(status) {
                res.send(status);
            })
        } else res.render('error');
    });
});
/**********/
router.get('/admin', isLoggedInUser, function(req, res) {

    try {
        authenticateAdmin(req, res, function(flag) {
            if (flag) {
                res.render('admin');

                logger.info('User entered in to admin page');
            } else res.render('error');
        })

    } catch (ex) {
        logger.error('Error in rendering admin page', ex.stack);
        throw ex;
    }
});

router.get('/admin/user/list', isLoggedInUser, function(req, res) {

    try {
        authenticateAdmin(req, res, function(flag) {
            if (flag) {
                res.render('adminUserList');

                logger.info('User entered in to adminUserList page');
            } else res.render('error');
        })


    } catch (ex) {
        logger.error('Error in rendering adminUserList page', ex.stack);
        throw ex;
    }
});

router.get('/admin/user/list2', isLoggedInUser, function(req, res) {

    try {
        authenticateAdmin(req, res, function(flag) {
            if (flag) {
                res.render('adminUserList2');
                logger.info('User entered in to adminUserList2 page');
            } else res.render('error');
        })

    } catch (ex) {
        logger.error('Error in rendering adminUserList2 page', ex.stack);
        throw ex;
    }
});

router.get('/admin/user/delete', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.render('adminDeleteUser');
        } else res.render('error');
    })
});

router.get('/admin/reports/', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.render('adminCheckUser');
        } else res.render('error');
    })
});

router.get('/admin/corporate/changeAdmin', isLoggedInUser, function(req, res) {

    try {
        authenticateAdmin(req, res, function(flag) {
            if (flag) {
                res.render('relatasCorporateAdmin');
                logger.info('User entered in to relatasCorporateAdmin page');
            } else res.render('error');
        })

    } catch (ex) {
        logger.error('Error in rendering relatasCorporateAdmin page', ex.stack);
        throw ex;
    }
});

router.get('/admin/corporate/changeAdmin/log', isLoggedInUser, function(req, res) {

    try {
        authenticateAdmin(req, res, function(flag) {
            if (flag) {
                res.render('relatasCorporateLog');
            } else res.render('error');
        })

    } catch (ex) {
        logger.error('Error in rendering relatasCorporateLog page', ex.stack);
        throw ex;
    }
});

//draw company hierarchy for relatas admin

var relatasAdmin_CompanyId;
var relatasAdmin_CompanyName;

router.get('/admin/corporate/hierarchy/:companyId/:companyName', isLoggedInUser, function(req, res) {
    try {
        relatasAdmin_CompanyId = null;
        relatasAdmin_CompanyName = null;
        authenticateAdmin(req, res, function(flag) {
            if (flag) {
                relatasAdmin_CompanyId = req.params.companyId;
                relatasAdmin_CompanyName = req.params.companyName;
                res.sendfile('views/relatasAdmin_CompanyHirarchy.html');
            } else res.render('error');
        })

    } catch (ex) {
        logger.error('Error in rendering hierarchy page', ex.stack);
        throw ex;
    }
});

router.get('/admin/corporate/hierarchyDraw', isLoggedInUser, function(req, res) {
    try {
        authenticateAdmin(req, res, function(flag) {
            if (flag) {
                userManagements.getHierarchy(relatasAdmin_CompanyId, function(hierarchy) {
                    res.send({ array: hierarchy, companyName: relatasAdmin_CompanyName });
                });
            } else res.render('error');
        })

    } catch (ex) {
        logger.error('Error in rendering hierarchy page', ex.stack);
        throw ex;
    }
});

router.get('/admin/corporate/changeAdmin/getLogs', isLoggedInUser, function(req, res) {

    try {
        authenticateAdmin(req, null, function(flag) {
            if (flag) {
                corporateAdminChangeLog.query(function(err, results) {

                    res.send(results);
                });
            }
        })

    } catch (ex) {
        logger.error('Error in rendering relatasCorporateAdmin page', ex.stack);
        throw ex;
    }
});

router.get('/admin/user/:emailId/get/admin', function(req, res) {

    var emailId = req.params.emailId;
    if (common.checkRequired(emailId)) {

        userManagements.findUserProfileByEmailIdWithCustomFields(emailId, { firstName: 1, lastName: 1, emailId: 1, publicProfileUrl: 1, createdDate: 1, corporateUser: 1 }, function(error, user) {
            if (common.checkRequired(user)) {
                var userId = user._id.toString();

                userManagements.getSentInvitationsCount(userId, function(SentInvitationsCount) {
                    user.meetingsCount = SentInvitationsCount;
                    userManagements.getDocumentsUploadedCount(userId, function(documentsUploadedCount) {
                        user.documentsCount = documentsUploadedCount;
                        eventAccess.getEventsCountPostedByUser(userId, function(eventsCountPostedByUser) {
                            user.eventsCount = eventsCountPostedByUser;
                            var obj = { user: user, meetingsCount: SentInvitationsCount, documentsCount: documentsUploadedCount, eventsCount: eventsCountPostedByUser };
                            res.send(obj);
                        })
                    })
                })
            } else res.send('no-user');
        })
    } else res.send(false);
});

router.post('/admin/user/delete/permanently', function(req, res) {
    var reqData = req.body;
    if (common.checkRequired(reqData.firstName) && common.checkRequired(reqData.userId) && common.checkRequired(reqData.emailId)) {
        authenticateAdmin(req, res, function(flag) {
            if (flag) {
                deleteUserRelatedInfo(reqData.userId, reqData.emailId, function(status, statusArr) {
                    if (status == true) {
                        var dataToSend = {
                            toName: reqData.firstName,
                            emailId: reqData.emailId,
                            subject: 'Your Relatas account deletion',
                            mailContent: 'The Relatas team would like to thank you for giving us a chance. As requested by you, we have deleted the account associated with email id:<b>"' + reqData.emailId + '"</b> and Unique Relatas id:<b>"' + reqData.uniqueName + '"</b>. <br><br> On the other hand, we are sad to see you go. We would appreciate if you could help us understand why you decided to move away from Relatas. It will help us correct what we are not doing right. We would appreciate if you could take 5 minutes off your calendar and help us with constructive feedback. Your feedback is very important for us and we would love to hear from you. Please help us with your feedback here:<a href="https://www.surveymonkey.com/s/RelatasDeleted">https://www.surveymonkey.com/s/RelatasDeleted</a><br><br> If there is anything else we can help you with please do let us know. Lastly, we would love to get you back and use Relatas again when you feel is the right time for you.'
                        };

                        emailSenders.sendMeetingCancelOrDeletionMail(dataToSend, 'Account Deletion', true);
                    }
                    logger.info("Account deletion ", status);
                    res.send({ status: status, statusInfo: statusArr });
                })
            } else res.send({ status: 'access-denied', statusInfo: [] });
        })
    } else res.send({ status: 'no-id', statusInfo: [] });
});

router.get('/getAllUsers/basicProfile', isLoggedInUser, function(req, res) {

    userManagements.getAllUserInfoBasic(function(userList) {
        //emailSenders.sendUncaughtException('profile','list',userList,'admin');
        res.send(userList);
    })
});

router.get('/getAllProfiles/admin/list', isLoggedInUser, function(req, res) {

    userManagements.getAllUsers(function(userList) {
        if (common.checkRequired(userList)) {
            if (userList.length > 0) {
                res.send(userList)
            } else res.send(false);
        } else res.send(false);
    })
});

/*using in admin new user list*/
router.get('/getListOfAllUser/admin/list', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            userManagements.getListOfAllUser(req.query.from, req.query.to, req.query.category, function(userList) {
                if (userList.length > 0) {
                    res.send(userList)
                } else res.send(false);
            })
        } else res.render('error');
    });
});

router.get('/csv/getListOfAllUser/admin/list', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            userManagements.getListOfAllUser(req.query.from, req.query.to, req.query.category, function(userList) {
                var csvFormate = [];
                userList.forEach(function(ele) {
                    if (ele.createdDate !== undefined && ele.createdDate !== null) {
                        var date = ele.createdDate.getDate() <= 9 ? '0' + ele.createdDate.getDate() : ele.createdDate.getDate();
                        var month = ele.createdDate.getMonth() + 1 <= 9 ? '0' + (ele.createdDate.getMonth() + 1) : (ele.createdDate.getMonth() + 1);
                        var year = ele.createdDate.getFullYear();
                        ele.createdDate = year + '-' + month + '-' + date;
                    }
                    if (ele.lastLoginDate !== undefined && ele.lastLoginDate !== null) {
                        var date = ele.lastLoginDate.getDate() <= 9 ? '0' + ele.lastLoginDate.getDate() : ele.lastLoginDate.getDate();
                        var month = ele.lastLoginDate.getMonth() + 1 <= 9 ? '0' + (ele.lastLoginDate.getMonth() + 1) : (ele.lastLoginDate.getMonth() + 1);
                        var year = ele.lastLoginDate.getFullYear();
                        ele.lastLoginDate = year + '-' + month + '-' + date;
                    }
                    if (ele.registeredDate !== undefined && ele.registeredDate !== null) {
                        var date = ele.registeredDate.getDate() <= 9 ? '0' + ele.registeredDate.getDate() : ele.registeredDate.getDate();
                        var month = ele.registeredDate.getMonth() + 1 <= 9 ? '0' + (ele.registeredDate.getMonth() + 1) : (ele.registeredDate.getMonth() + 1);
                        var year = ele.registeredDate.getFullYear();
                        ele.registeredDate = year + '-' + month + '-' + date;
                    }
                    if (ele.lastMobileSyncDate !== undefined && ele.lastMobileSyncDate !== null) {
                        var date = ele.lastMobileSyncDate.getDate() <= 9 ? '0' + ele.lastMobileSyncDate.getDate() : ele.lastMobileSyncDate.getDate();
                        var month = ele.lastMobileSyncDate.getMonth() + 1 <= 9 ? '0' + (ele.lastMobileSyncDate.getMonth() + 1) : (ele.lastMobileSyncDate.getMonth() + 1);
                        var year = ele.lastMobileSyncDate.getFullYear();
                        ele.lastMobileSyncDate = year + '-' + month + '-' + date;
                    }
                    if (ele.mobileOnBoardingDate !== undefined && ele.mobileOnBoardingDate !== null) {
                        var date = ele.mobileOnBoardingDate.getDate() <= 9 ? '0' + ele.mobileOnBoardingDate.getDate() : ele.mobileOnBoardingDate.getDate();
                        var month = ele.mobileOnBoardingDate.getMonth() + 1 <= 9 ? '0' + (ele.mobileOnBoardingDate.getMonth() + 1) : (ele.mobileOnBoardingDate.getMonth() + 1);
                        var year = ele.mobileOnBoardingDate.getFullYear();
                        ele.mobileOnBoardingDate = year + '-' + month + '-' + date;
                    }

                    if (ele.registeredUser === true && (ele.registeredDate !== undefined || ele.registeredDate !== null)) {
                        ele.registeredDate = ele.createdDate;
                    }

                    csvFormate.push({
                        PartialRegDate: ele.createdDate,
                        WebRegDate: ele.registeredDate,
                        MobRegDate: ele.mobileOnBoardingDate,
                        LastLoginDate: ele.lastLoginDate,
                        LastSyncDate: ele.lastMobileSyncDate,
                        FirstName: ele.firstName,
                        LastName: ele.lastName,
                        'C/R': ele.corporateUser ? 'C' : 'R',
                        Signup: ele.serviceLogin,
                        Location: ele.location,
                        Company: ele.companyName,
                        Designation: ele.designation,
                        Email: ele.emailId,
                        Profile: ele.publicProfileUrl,
                        Contacts: ele.contacts
                    });
                });

                res.csv(csvFormate, 'userListReport.csv');
            })
        } else res.render('error');
    });
});

router.get('/consolidate/uploaded/document/report', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {

            userManagements.getConsolidateDocumentReport(function(error, response) {
                if(!error && response)
                    res.send(response);
                else 
                    res.send(false);
            });

        } else res.render('error');
    });
});

router.get('/consolidate/created/document/report', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            createDocumentManagements.getDocumentConsolidatedReport(function(error, response) {
                if(!error && response)
                    res.send(response);
                else 
                    res.send(false);
            });
        } else res.render('error');
    });
});

router.get('/detail/uploaded/document/report', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            
            userManagements.getDetailDocumentReport(function(errors,response) {
                if(!errors && response)
                    res.send(response);
                else 
                    res.send(false);
            });

        } else res.render('error');
    });
});

router.get('/detail/created/document/report', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {

            createDocumentManagements.getDocumentDetailReport(function (errors,response) {
                if(!errors && response)
                    res.send(response);
                else 
                    res.send(false);
            });
        } else res.render('error');
    });
});

router.get('/admin/user/newList', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        console.log("the flag:", flag);
        if (flag) {
            res.sendfile('views/adminNewUserList.html');
        } else res.render('error');
    });
});

router.get('/getInteractions/all/summary/:withUserId', isLoggedInUser, function(req, res) {
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user, tokenSecret);
    var userId = decoded.id;
    var withUserId = req.params.withUserId;
    interactions.totalInteractionsIndividualSummary(userId, withUserId, false, false, null, function(interactions) {
        res.send(interactions)
    })
});

router.get('/getInteractions/all/count/:withUserId', isLoggedInUser, function(req, res) {
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user, tokenSecret);
    var userId = decoded.id;
    var withUserId = req.params.withUserId;
    interactions.totalInteractionsWithOneUserCount(userId, withUserId, function(interactions) {
        res.send(interactions)
    })
});

router.get('/getInteractions/all/individual', function(req, res) {

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user, tokenSecret);
    var userId = decoded.id;
    userManagements.findUserProfileByIdWithCustomFields(userId, { emailId: 1 }, function(error, userData) {
        if (!error && common.checkRequired(userData)) {
            interactions.interactionIndividualByDate(userId, userData.emailId, function(interactions) {
                res.send(interactions);
            })
        } else res.send([]);
    });
});

router.get('/getInteractions/all/individual/byMonth', function(req, res) {
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user, tokenSecret);
    var userId = decoded.id;
    interactions.interactionIndividualByMonth(userId, function(interactions) {
        res.send(interactions)
    })
});

router.get('/getInteractions/all/bySubType', function(req, res) {
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user, tokenSecret);
    var userId = decoded.id;
    interactions.totalInteractionsBySubType(userId, function(interactions) {
        res.send(interactions)
    })
});

router.get('/invitationsSentCount/:userId', function(req, res) {
    var userId = req.params.userId;

    if (common.checkRequired(userId)) {
        userManagements.getSentInvitationsCount(userId, function(SentInvitationsCount) {
            res.send(SentInvitationsCount);
        })
    } else res.send(false);
});

router.get('/invitationsRecCount/:userId', function(req, res) {
    var userId = req.params.userId;

    if (common.checkRequired(userId)) {
        userManagements.getTotalMeetingsRecCount(userId, function(TotalMeetingsRecCount) {
            res.send(TotalMeetingsRecCount);
        })
    } else res.send(false);
});

router.get('/documentsUploadedCount/:userId', function(req, res) {
    var userId = req.params.userId;

    if (common.checkRequired(userId)) {
        userManagements.getDocumentsUploadedCount(userId, function(documentsUploadedCount) {
            res.send(documentsUploadedCount);
        })
    } else res.send(false);
});

router.get('/documentsSharedWithMeCount/:userId', function(req, res) {
    var userId = req.params.userId;

    if (common.checkRequired(userId)) {
        userManagements.getDocumentsSharedWithMeCount(userId, function(documentsSharedWithMeCount) {
            res.send(documentsSharedWithMeCount);
        })
    } else res.send(false);
});

router.get('/documentsRecCount/:userId', function(req, res) {
    var userId = req.params.userId;

    if (common.checkRequired(userId)) {
        userManagements.getDocumentsRecCount(userId, function(documentsRecCount) {

            res.send(documentsRecCount);
        })
    } else res.send(false);
});

router.get('/eventsPostedCount/:userId', function(req, res) {
    var userId = req.params.userId;

    if (common.checkRequired(userId)) {
        eventAccess.getEventsCountPostedByUser(userId, function(eventsCountPostedByUser) {
            res.send(eventsCountPostedByUser);
        })
    } else res.send(false);
});

router.get('/eventsSubscribedCount/:userId', function(req, res) {
    var userId = req.params.userId;

    if (common.checkRequired(userId)) {
        eventAccess.getUserSubscribeEventsCount(userId, function(userSubscribeEventsCount) {
            res.send(userSubscribeEventsCount);
        })
    } else res.send(false);
});

router.get('/admin/update/pastMeetingInteractions', isLoggedInUser, function(req, res) {

    userManagements.getAllUsersAcceptedMeetings(function(meetings) {

        updateMeetingInteractions(meetings, req, res);
        res.send(true);
    });
});

function updateMeetingInteractions(meetings, req, res) {
    if (common.checkRequired(meetings) && meetings.length > 0) {

        for (var i = 0; i < meetings.length; i++) {
            var interactionDate, endDate;
            var title, description;
            var locationType;
            for (var j = 0; j < meetings[i].scheduleTimeSlots.length; j++) {
                if (meetings[i].scheduleTimeSlots[j].isAccepted) {
                    interactionDate = new Date(meetings[i].scheduleTimeSlots[j].start.date);
                    endDate = new Date(meetings[i].scheduleTimeSlots[j].end.date);
                    title = meetings[i].scheduleTimeSlots[j].title;
                    description = meetings[i].scheduleTimeSlots[j].description;
                    locationType = meetings[i].scheduleTimeSlots[j].locationType;
                }
            }

            createStoreInteraction(meetings[i].senderId, meetings[i].senderEmailId, 'sender', interactionDate, endDate, 'meeting', meetings[i].invitationId, title, description, locationType);

            if (meetings[i].selfCalendar) {
                for (var k = 0; k < meetings[i].toList.length; k++) {
                    if (meetings[i].toList[k].isAccepted) {
                        createStoreInteraction(meetings[i].toList[k].receiverId, meetings[i].toList[k].receiverEmailId, 'receiver', interactionDate, endDate, 'meeting', meetings[i].invitationId, title, description, locationType);
                    }
                }
            } else {
                createStoreInteraction(meetings[i].to.receiverId, meetings[i].to.receiverEmailId, 'receiver', interactionDate, endDate, 'meeting', meetings[i].invitationId, title, description, locationType);
            }
        }
        res.send(true);
    } else res.send('no meetings')
}

router.get('/admin/update/pastDocumentInteraction', isLoggedInUser, function(req, res) {

    userManagements.getAllUsersDocuments(function(documents) {
        updateDocumentInteractions(documents, req, res);
        res.send(true);
    });
});

function updateDocumentInteractions(documents, req, res) {
    if (common.checkRequired(documents) && documents.length > 0) {
        for (var i = 0; i < documents.length; i++) {
            var interactionDate;
            if (common.checkRequired(documents[i].sharedWith) && documents[i].sharedWith.length > 0) {
                for (var j = 0; j < documents[i].sharedWith.length; j++) {
                    interactionDate = common.checkRequired(documents[i].sharedWith[j].sharedOn) ? new Date(documents[i].sharedWith[j].sharedOn) : new Date();
                    var message = common.checkRequired(documents[i].sharedWith[j]) ? documents[i].sharedWith[j].message : documents[i].documentName;
                    message = message || documents[i].documentName;
                    createStoreInteraction(documents[i].sharedWith[j].userId, documents[i].sharedWith[j].emailId, 'receiver', interactionDate, null, 'document-share', documents[i]._id, documents[i].documentName, message, "document-share");
                }
                createStoreInteraction(documents[i].sharedBy.userId, documents[i].sharedBy.emailId, 'sender', interactionDate || documents[i].sharedDate, null, 'document-share', documents[i]._id, documents[i].documentName, documents[i].documentName, "document-share");
            }
        }
        res.send(true);
    } else res.send('no docs');
}

router.get('/admin/update/pastMessageInteraction', isLoggedInUser, function(req, res) {

    messageAccess.getAllUsersMessages(function(messages) {
        updateMessageInteractions(messages, req, res);
        res.send(true);
    });
});

function updateMessageInteractions(messages, req, res) {
    if (common.checkRequired(messages) && messages.length > 0) {
        for (var i = 0; i < messages.length; i++) {
            var interaction = {};
            interaction.userId = messages[i].senderId;
            interaction.action = 'sender';
            interaction.type = 'message';
            interaction.subType = 'message';
            interaction.refId = messages[i]._id;
            interaction.source = 'relatas';
            interaction.title = messages[i].subject;
            interaction.description = messages[i].message;
            interaction.interactionDate = messages[i].sentOn;
            common.storeInteraction(interaction);

            var interactionReceiver = {};
            interactionReceiver.userId = messages[i].receiverId || '';
            interactionReceiver.emailId = messages[i].receiverEmailId;
            interactionReceiver.action = 'receiver';
            interactionReceiver.type = 'message';
            interactionReceiver.subType = 'message';
            interactionReceiver.refId = messages[i]._id;
            interactionReceiver.source = 'relatas';
            interactionReceiver.title = messages[i].subject;
            interactionReceiver.description = messages[i].message;
            interactionReceiver.interactionDate = messages[i].sentOn;
            common.storeInteractionReceiver(interactionReceiver);
        }
        res.send(true);
    } else res.send('no messages')
}

function createStoreInteraction(userId, emailId, cation, interactionDate, endDate, type, refId, title, description, subType) {
    var interaction = {};
    interaction.userId = userId;
    interaction.action = cation;
    interaction.interactionDate = interactionDate;
    interaction.type = type;
    interaction.emailId = emailId;
    interaction.refId = refId;
    interaction.subType = subType;
    interaction.source = 'relatas';
    interaction.title = title;
    interaction.description = description;
    if (common.checkRequired(endDate)) {
        interaction.endDate = endDate;
    }
    common.storeInteraction(interaction);
}

router.post('/admin/request/enterpriseDemo', function(req, res) {
    var userData = req.body;
    if (common.checkRequired(userData.firstName) && common.checkRequired(userData.lastName) && common.checkRequired(userData.emailId) && common.checkRequired(userData.companyName) && common.checkRequired(userData.phoneNumber) && common.checkRequired(userData.skypeId)) {
        enterPriceRequestObj.saveRequestedUser(userData, function(response) {
            if (response == true) {
                emailSenders.sendPreSignUpRequestMail({ firstName: userData.firstName, emailId: userData.emailId });
                emailSenders.sendPreSignUpRequestMailAdminCopy(userData);
            }
            res.send(response);
        })
    } else res.send(false);
});

router.post('/admin/request/mobileAppLaunch', function(req, res) {
    var userData = req.body;
    if (common.checkRequired(userData.emailId)) {
        if (userData.iosApp || userData.androidApp || userData.windowsApp) {
            enterPriceRequestObj.saveMobileAppSubscriber(userData, function(response) {
                res.send(response);
            });
        } else res.send(false);
    } else res.send(false);
});

router.post('/admin/survey/relationshipCartoon', function(req, res) {
    var userData = req.body;

    if (common.checkRequired(userData.email)) {
        enterPriceRequestObj.relationshipCartoonSave(userData, function(response) {
            res.send(response);
        });
    } else res.send(false);
});

function deleteUserRelatedInfo(userId, emailId, callback) {
    var resultsObj = [];

    function addResult(status, process, error, result) {
        resultsObj.push({
            status: status,
            process: process,
            error: error,
            result: result
        });
    }
    interactions.removeInteractions_user(common.castToObjectId(userId), emailId, function(status, process, error, result) {
        addResult(status, process, error, result)
        documentAccess.removeUserDocuments(userId, function(status, process, error, result) {
            addResult(status, process, error, result)
            meetingClassObj.removeUserMeetings(userId, function(status, process, error, result) {
                addResult(status, process, error, result)
                eventAccess.removeUserEvents(userId, function(status, process, error, result) {
                    addResult(status, process, error, result)
                    eventAccess.removeUserSubscribeDetails(userId, function(status, process, error, result) {
                        addResult(status, process, error, result)
                        messageAccess.removeUserMessages(userId, function(status, process, error, result) {
                            addResult(status, process, error, result)
                            massMailCountManagementObj.removeUserMailCount(userId, function(status, process, error, result) {
                                addResult(status, process, error, result)
                                calendarPasswordManagementObj.removeUserRequests(userId, function(status, process, error, result) {
                                    addResult(status, process, error, result)
                                    userLogManagementObj.removeUserLogs(userId, function(status, process, error, result) {
                                        addResult(status, process, error, result)
                                        userManagements.removeUserProfile(userId, function(status, process, error, result) {
                                            addResult(status, process, error, result)
                                            callback(status, resultsObj);
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
}

// route middleware to make sure a user is logged in
function isLoggedInUser(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        return next();
    } else {
        // if they aren't redirect them to the home page
        return res.redirect('/');
    }
}

router.get('/admin/get/linkedin/feeds', common.isLoggedInUserToGoNext, function(req, res) {
    var lObj = new linkedin()
    userManagements.findUserProfileByIdWithCustomFields("5499578055815399169681fe", { linkedin: 1 }, function(error, user) {
        lObj.getUserFeedsByDate(user.linkedin, null, function() {

        })
    })
});

router.get('/help/new', common.isLoggedInUserToGoNext, function(req, res) {
    res.render('help/help', { isLoggedIn: common.isLoggedInUserBoolean(req) });
});

router.get('/set/corporate/users', common.isLoggedInUserToGoNext, function(req, res) {
    res.render('adminRelatas/corporateSettings', { isLoggedIn: common.isLoggedInUserBoolean(req) });
});

router.get('/get/companies', common.isLoggedInUserToGoNext, function(req, res) {
    companyCollection.find({}).exec(function(error, companies){
        if(!error){
            res.send(companies)
        }
    });
});

// router.get('admin/invalid/email/lists',common.isLoggedInUserToGoNext, function(req, res){
router.get('/admin/get/invalid/email/lists', function(req, res){
    common.getInvalidEmailListFromDb(function (list) {
        res.send(list)
    });
});

// router.get('/admin/add/invalid/email/lists', function(req, res){
//     var list = req.query.list.split(",");
//     var type = req.query.validOrInvalid;
//
//     common.insertValidOrInvalidEmail(list,type,function (result) {
//         res.send(result)
//     });
// });

router.get('/admin/invalid/contacts', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('views/adminAddInvalidEmail.html');
        } else res.render('error');
    });
});

router.post('/admin/invalid/contacts/add', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            var temp = req.body.list;
            var list = [];
            if(temp.length > 0) {
                temp.forEach(function (a) {
                    if(a != "") {
                        list.push(new RegExp(a, 'i'));
                    }
                })
                common.insertValidOrInvalidEmail(list, req.body.type, function (result) {
                    res.send(result)
                });
            }
            else{
                res.send(false)
            }
        } else {
            res.render('error');
        }
    });
});

router.post('/admin/invalid/contacts/delete', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            var temp = req.body.list;
            var list = [];
            if(temp.length > 0) {
                temp.forEach(function (a) {
                    if(a != "") {
                        list.push(new RegExp(a, 'i'));
                    }
                })
                common.deleteValidOrInvalidEmail(list, req.body.type, function (result) {
                    res.send(result)
                });
            }
            else{
                res.send(false)
            }
        } else {
            res.render('error');
        }
    });
});

router.get('/admin/invalid/contacts/list', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            var skip = req.query.skip,limit=req.query.limit;

            var isCorporate = true;

            if(req.query.corporate === 'true'){
            } else if(req.query.corporate === 'false'){
                isCorporate = false
            }

            common.getInvalidEmailListFromDb(function (list) {
                if(list && list.invalid) {
                    var regList = [];
                    if(req.query.type == 'invalid') {
                        regList = list.invalid;
                    }
                    else if(req.query.type == 'exception') {
                        regList = list.exception;
                    }

                    userManagements.getAllUserIds(isCorporate,function (err,userIds) {
                        if(!err && userIds){

                            var userIdsArray = _.pluck(userIds,'_id');

                            var users = userIdsArray.splice(skip, limit);
                            // console.log(users)
                            // contactsManagementObj.searchInvalidContacts(list,req.query.type, function(error,contacts){
                                // console.log(JSON.stringify(contacts,null,2))
                                contactsManagementObj.searchInvalidContactsPagination(list,req.query.type,users,function(error,contacts){
                                var groupByPattern = [];

                                regList.forEach(function(a){
                                    var count = 0;
                                    var usersList = [];
                                    contacts.forEach(function(email){
                                        if (a.test(email.contactEmailId)) {
                                            // console.log(email)
                                            usersList.push(email)
                                            count ++;
                                            // count += email.occurrence
                                        }
                                    });
                                    groupByPattern.push({pattern:a.source, occurence:count,usersList:usersList})
                                });

                                groupByPattern.sort(function(a,b){
                                    var nameA = a.pattern.toUpperCase();
                                    var nameB = b.pattern.toUpperCase();
                                    if (nameA < nameB) {
                                        return -1;
                                    }
                                    if (nameA > nameB) {
                                        return 1;
                                    }

                                    return 0;
                                });

                                res.send({
                                    grandTotal:userIds.length,
                                    users:users.length,
                                    contacts:groupByPattern
                                })
                            });
                        } else {
                            res.send([])
                        }
                        
                    });
                }
                else {
                    res.send([])
                }
            });
        } else {
            res.render('error');
        }
    });
});

router.get('/admin/invalid/contacts/list/by/pattern', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            common.getInvalidEmailListFromDb(function (list) {
                if(list && list.invalid) {
                    if(req.query.type == 'invalid') {
                        list.invalid = [new RegExp(req.query.pattern, 'i')]
                    }
                    else if(req.query.type == 'exception') {
                        list.exception = [new RegExp(req.query.pattern, 'i')]
                    }
                    contactsManagementObj.searchInvalidContacts(list,req.query.type, function(error,contacts){
                        if(error){
                            res.send([])
                        }
                        else{
                            contacts.sort(function(a,b){
                                var nameA = a.contactEmailId.toUpperCase();
                                var nameB = b.contactEmailId.toUpperCase();
                                if (nameA < nameB) {
                                    return -1;
                                }
                                if (nameA > nameB) {
                                    return 1;
                                }

                                return 0;
                            })
                            res.send(contacts)
                        }
                    });
                }
                else {
                    res.send([])
                }
            });
        } else {
            res.render('error');
        }
    });
});

router.post('/admin/invalid/contacts/delete/or/edit', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {

            var contacts = req.body.contacts

            var query = { "$or": contacts.map(function(el) {
                var q = {};
                // q["ownerId"] = common.castToObjectId(el.userId.toString())
                q["ownerEmailId"] = el.userEmailId
                q["emailId"] = el.contactEmailId;
                return q;
            })};

            if(contacts.length > 0) {
                contactsManagementObj.removeInvalidContactsFromDB(contacts,req.query.action,common.castToObjectId, function (error, result) {

                    interactions.removeInvalidInteractionsByUser(query,function(err,result){
                        res.send(result);
                    })
                })
            }
            else{
                res.send(null);
            }
        } else {
            res.render('error');
        }
    });
});

router.get('/admin/csv/invalid/contacts', isLoggedInUser, function(req, res) { 
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            common.getInvalidEmailListFromDb(function (list) {
                if(list && list.invalid) {
                    var formatted = [];
                    contactsManagementObj.searchInvalidContacts(list, req.query.type,function(error,contacts){
                        contacts.forEach(function(a){
                            var obj = {
                                userEmailId:a.userEmailId,
                                contactEmailId:a.contactEmailId
                            }
                            formatted.push(obj)
                        })
                        formatted.sort(function(a,b){
                            var nameA = a.userEmailId.toUpperCase();
                            var nameB = b.userEmailId.toUpperCase();
                            if (nameA < nameB) {
                                return -1;
                            }
                            if (nameA > nameB) {
                                return 1;
                            }

                            return 0;
                        })
                        // res.send(formatted)
                        res.csv(formatted, 'invalidContacts.csv');
                    })
                }
                else {
                    res.render('error');
                }
            });
        } else res.render('error');
    });
});

router.get('/admin/email/classification', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('views/adminEmailClassification.html');
        } else res.render('error');
    });
});

router.get('/admin/email/classification/list', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            interactions.getEmailClassificationByEmail(req.query.from,req.query.to,req.query.emailId,function(error, result){
                if(error){
                    res.send({
                        "SuccessCode": 0,
                        "ErrorCode": 1,
                        "Data": []
                    });
                }
                else{
                    res.send({
                        "SuccessCode": 1,
                        "ErrorCode": 0,
                        "Data": result
                    });
                }
            });
        } else res.render('error');
    });
});

router.get('/admin/company/opportunities/report', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/companyReport.html');
        } else res.render('error');
    });
});

router.get('/admin/company/usage/report', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/usageReport.html');
        } else res.render('error');
    });
});

router.get('/admin/company/contacts/and/opportunities/report', isLoggedInUser, function(req, res) {

    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/contactsOpportunitiesReport.html');
        } else res.render('error');
    });
});

router.get('/admin/company/document/consolidated/report', isLoggedInUser, function(req, res) {

    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/adminRelatas/documents/consolidatedDocumentReport.html');
        } else res.render('error');
    });
});

router.get('/admin/company/document/detail/report', isLoggedInUser, function(req, res) {

    authenticateAdmin(req, res, function(flag) {
        if (flag) {
            res.sendfile('./views/adminRelatas/documents/detailDocumentReport.html');
        } else res.render('error');
    });
});

router.get('/admin/getCompanyReports', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(isAdmin) {
        if (isAdmin) {

            var companyId = req.query.category;
            var fromDate = req.query.from;
            var toDate = req.query.to;

            userManagements.getTeamMembers(companyId,function (err,teamMembers) {
                var userIds = _.pluck(teamMembers,"_id");

                getOpportunities(userIds,fromDate,toDate,function (err,opportunities) {
                    var dataForQuery = [];
                    _.each(opportunities,function (opportunity) {
                        _.each(opportunity.opportunities,function (op) {
                            _.each(teamMembers,function (user) {
                                if(op.userEmailId == user.emailId){
                                    op["createdDate"] = user.createdDate;
                                    op["userId"] = user._id;
                                    dataForQuery.push(op);
                                }
                            });
                        });
                    });

                    getInteractionsPerDayBeforeJoiningRelatas(teamMembers,dataForQuery,function (err,interactionsBefore) {
                        getInteractionsPerDayAfterJoiningRelatas(teamMembers,dataForQuery,function (err,interactionsAfter) {
                            actionsOnRelatasForMultiUsers(teamMembers,dataForQuery,function (err,interactionsOnRelatas) {
                                getLosingTouchCount(teamMembers,function (err,losingTouch) {

                                    var data = [];
                                    _.each(dataForQuery,function (tm) {
                                        var obj = tm;
                                        // _.each(opportunities,function (op) {
                                        //     if(tm.emailId == op._id){
                                        //         obj["user"] = tm;
                                        //         obj["opportunities"] = op.opportunities;
                                        //     }
                                        // });

                                        obj["partners"] = tm.partners?tm.partners.length:0;
                                        obj["decisionMakers"] = tm.decisionMakers?tm.decisionMakers.length:0;
                                        obj["influencers"] = tm.influencers?tm.influencers.length:0;

                                        _.each(interactionsBefore,function (ib) {
                                            if(tm.userEmailId == ib._id.ownerEmailId){
                                                obj["interactionsBeforeJoiningRelatas"] = Math.ceil(ib.beforeCount/90)
                                            }
                                        })

                                        var registeredDate = moment(tm.createdDate);
                                        var now = moment(new Date());
                                        var duration = now.diff(registeredDate, 'days')

                                        _.each(interactionsAfter,function (ib) {
                                            if(tm.userEmailId == ib._id.ownerEmailId){
                                                obj["interactionsAfterJoiningRelatas"] = Math.ceil(ib.afterCount/duration)
                                            }
                                        })
                                        _.each(interactionsOnRelatas,function (ib) {
                                            if(tm.userEmailId == ib._id.ownerEmailId){
                                                obj["interactionsOnRelatas"] = Math.ceil(ib.sourceRelatasCount/duration)
                                            }
                                        })
                                        _.each(losingTouch,function (ib) {
                                            if(tm.userEmailId == ib._id){
                                                obj["losingTouch"] = ib.ltCount
                                            }
                                        })

                                        obj["relatasUserForDays"] = duration;
                                        obj["company"] = common.fetchCompanyFromEmail(tm.contactEmailId);

                                        data.push(obj)
                                    })

                                    res.send({
                                        // teamMembers:teamMembers,
                                        // opportunities:opportunities,
                                        beforeJoining:interactionsBefore,
                                        afterJoining:interactionsAfter,
                                        // interactionsOnRelatas:interactionsOnRelatas,
                                        data:data
                                    });
                                });
                            });
                        });
                    });
                });
            });
        } else res.render('error');
    });
});

router.get('/admin/getCompanyUsageReports', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(isAdmin) {
        if (isAdmin) {

            var companyId = req.query.category;
            var fromDate = req.query.from;
            var toDate = req.query.to;

            userManagements.getTeamMembersWithContacts(common.castToObjectId(companyId),function (err,teamMembers) {
                // req.session.teamContacts = teamMembers;

                // var size = 0;
                // _.each(teamMembers,function (tm) {
                //     size = size+common.sizeOfObject(tm)
                // })

                // console.log("size  ---",common.bytesToSize(size))
                res.send(teamMembers);
            });
        } else res.render('error');
    });
});

router.get('/admin/company/users', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function (flag) {
        var findQuery = {companyName:req.query.companyName};
        var userProfile = common.getUserFromSession(req);
        var companyId = userProfile?userProfile.companyId:null;

        if(!req.query.companyName){
            findQuery = {_id:common.castToObjectId(companyId)}
        }

        companyCollection.findOne(findQuery,{_id:1}).lean().exec(function (err,company) {
            userManagements.getTeamMembers(company._id,function (err,team) {
                res.send({
                    "SuccessCode": 1,
                    Data: team,
                    companyId:company._id
                })
            });
        })
    });
});

router.get('/admin/user/interactions/v2', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {
        var fromDate = req.query.from;
        var toDate = req.query.to;
        var joiningDate = req.query.joiningDate;
        var userEmailId = req.query.userEmailId;
        var userId = req.query.userId;
        
        var registeredDate = moment(joiningDate);
        var now = moment(new Date());
        var duration = now.diff(registeredDate, 'days');

        interactions.interactionsBeforeJoining(userEmailId,joiningDate,function (err,beforeInteractions) {
            interactions.interactionsAfterJoining(userEmailId,joiningDate,function (err,afterInteractions) {
                interactions.interactionsOnRelatas(userEmailId,joiningDate,function (err,interactionsOnRelatas) {
                    interactions.interactionsPast30Days(common.castToObjectId(userId),function (err,past30Days) {
                        oppManagementObj.opportunitiesCount(userEmailId,function (err,oppCount) {
                            getUserLoginReport(userEmailId,function (err,userReports) {
                                interactions.interactionsOnRelatasPast30Days(userEmailId,function (err,interactionsOnRelatasPast30Days) {
                                    getUserLoginReportPast30Days(userEmailId,function (err,userReportsPast30Days) {

                                        var obj = {
                                            beforeInteractions:beforeInteractions[0],
                                            afterInteractions:afterInteractions[0],
                                            interactionsOnRelatas:interactionsOnRelatas[0],
                                            interactionsOnRelatasPast30Days:interactionsOnRelatasPast30Days[0],
                                            past30Days:past30Days[0],
                                            oppCount:oppCount[0],
                                            userReports:userReports,
                                            userReportsPast30Days:userReportsPast30Days[0],
                                            userFromDays:duration
                                        }

                                        res.send(JSON.parse(JSON.stringify(obj)));
                                    });

                                })
                            });
                        });
                    });
                });
            });
        });
    });
});

router.get('/admin/generate/password', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function(flag) {

        var tokenSecret = "alpha";
        var decoded = jwt.decode(req.user, tokenSecret);
        var userId = decoded.id;

        var OTP = userId+req.query.otp;

        var token = jwt.encode({
            id: OTP
        }, tokenSecret);

        var obj = {
            userId:userId,
            token:token,
            expire:moment().add(60, "seconds").toDate()
        }

        adminCollection.collection.insert(obj, function(error, result){
            res.send(obj)
        })
    });
});

router.get('/admin/test/login', function(req, res) {

    var token = req.query.token;

    adminCollection.collection.findOne({token:token}, function(error, result){

        if(result && token === result.token){

            userManagements.findUserProfileByEmailIdWithCustomFields(req.query.emailId, {
                _id: 1,
                publicProfileUrl: 1,
                profilePicUrl: 1,
                google:1,
                outlook:1,
                companyId:1,
            }, function (er, userProfile) {
                if (common.checkRequired(userProfile)) {
                    var tokenSecret = 'alpha';
                    var token = jwt.encode({
                        id: userProfile._id
                    }, tokenSecret);
                    req.logIn(token, function (err) {
                        common.checkImageExtension(userProfile)
                        var timezone;
                        if (common.checkRequired(userProfile.timezone) && common.checkRequired(userProfile.timezone.name)) {
                            timezone = userProfile.timezone.name;
                        } else timezone = 'UTC';


                        if(userProfile.google.length>0){
                            // googleCalendar.updateAllDataForGoogleUsers(userProfile,req)
                            setTimeout(res.redirect('/'),3000)
                        } else {
                            res.redirect("/admin/updateAllDataForOutlookUsers")
                        }

                    })
                } else res.send("profile not found")
            })

        } else {
            res.send({token:result,message:"token not found"})
        }

    });

});

router.get('/admin/user/contacts/opportunities', isLoggedInUser, function(req, res) {
    authenticateAdmin(req, res, function (flag) {

        var leads = req.query.leads?req.query.leads.split(','):[];
        var prospects = req.query.prospects?req.query.prospects.split(','):[];
        var customers = req.query.customers?req.query.customers.split(','):[];

        var userEmailId = req.query.userEmailId;
        var userId = common.castToObjectId(req.query.userId);

        getInteractionsAndOpportunitiesForContacts(userEmailId,leads,null,null,function (err,leadsData) {
            getInteractionsAndOpportunitiesForContacts(userEmailId,prospects,null,null,function (err,prospectsData) {
                getInteractionsAndOpportunitiesForContacts(userEmailId,customers,null,null,function (err,customersData) {
                    getOpportunitiesForContacts(userEmailId,leads,function (err,leadsOpportunities) {
                        getOpportunitiesForContacts(userEmailId,prospects,function (err,prospectsOpportunities) {
                            getOpportunitiesForContacts(userEmailId,customers,function (err,customersOpportunities) {

                                interactions.interactionsPast30Days(userId,function (err,past30Days) {
                                    oppManagementObj.opportunitiesCountAndValues(userEmailId,function (err,oppCount) {

                                        res.send({
                                            leadsData:{
                                                interactions:leadsData[0],
                                                opportunities:leadsOpportunities
                                            },
                                            prospectsData:{
                                                interactions:prospectsData[0],
                                                opportunities:prospectsOpportunities
                                            },
                                            customersData:{
                                                interactions:customersData[0],
                                                opportunities:customersOpportunities
                                            },
                                            all:{
                                                past30DaysInteractions:past30Days[0],
                                                opportunities:oppCount
                                            }
                                        });

                                    })
                                })
                            });
                        });
                    });
                });
            });
        });
    });
});

router.post('/admin/sales-ai/add/urls',isLoggedInUser, function (req,res) {

    videosManagementObj.updateVideos(req.body,function (err,result) {
        if(!err && result){
            res.send(true)
        } else {
            res.send(false)
        }
    })
    
});

router.post('/admin/sales-ai/set/default',isLoggedInUser, function (req,res) {

    videosManagementObj.updateDefaultVideos(req.body.link,function (err,result) {
        if(!err && result){
            res.send(true)
        } else {
            res.send(false)
        }
    })

});

router.post('/admin/sales-ai/set/setOrder',isLoggedInUser, function (req,res) {

    videosManagementObj.setOrder(req.body.link,req.body.order,function (err,result) {
        if(!err && result){
            res.send(true)
        } else {
            res.send(false)
        }
    })

});

router.post('/admin/sales-ai/delete/video',isLoggedInUser, function (req,res) {

    videosManagementObj.deleteVideos(req.body.link,function (err,result) {
        if(!err && result){
            res.send(true)
        } else {
            res.send(false)
        }
    })

});

router.get('/admin/sales-ai/get/urls', function (req,res) {
    videosManagementObj.getVideos('videos',function (err,urls) {
        res.send({
            err:err,
            data:urls[0]
        })
    })
});

function getInteractionsAndOpportunitiesForContacts(userEmailId,contacts,from,to,callback) {

    if(contacts.length>0){
        interactions.interactionsByDate(userEmailId,contacts,from,to,function (err,interactions) {
            callback(err,interactions)
        })
    } else {
        callback(null,[])
    }
}

function getLosingTouchCount(teamMembers,callback) {
    insights.getLosingTouchForTeam(teamMembers,callback)
}

function getInteractionsPerDayBeforeJoiningRelatas(teamMembers,dataForQuery,callback) {
    interactions.interactionsBeforeJoiningForMultiUsers(teamMembers,dataForQuery,callback)
}

function getInteractionsPerDayAfterJoiningRelatas(teamMembers,dataForQuery,callback) {
    interactions.interactionsAfterAfterForMultiUsers(teamMembers,dataForQuery,callback)
}

function actionsOnRelatasForMultiUsers(teamMembers,dataForQuery,callback) {
    interactions.actionsOnRelatasForMultiUsers(teamMembers,dataForQuery,callback)
}

function getOpportunities(userIds,fromDate,toDate,callback) {
    oppManagementObj.opportunitiesForUsers(userIds,fromDate,toDate,null,{},null,callback)
}

function getOpportunitiesForContacts(userEmailId,contacts,callback) {

    if(contacts.length>0){
        oppManagementObj.opportunitiesForContacts(userEmailId,contacts,callback)
    } else {
        callback(null,[])
    }
}

function getUserLoginReport(userEmailId,callback) {
    userLogCollection.aggregate([
        {
            $match:{
                emailId:userEmailId
            }
        },
        {
            $group: {_id: { $month:'$loginDate'}, count: {$sum:1}}
        }
    ]).exec(function (err,userReports) {
        callback(err,userReports)
    })
}

function getUserLoginReportPast30Days(userEmailId,callback) {
    userLogCollection.aggregate([
        {
            $match:{
                emailId:userEmailId,
                loginDate:{$gte: moment().subtract(30, "days").toDate()}
            }
        },
        {
            $group: {_id: null, count: {$sum:1}}
        }
    ]).exec(function (err,userReports) {
        callback(err,userReports)
    })
}

module.exports = router;
