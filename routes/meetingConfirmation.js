/*
 * relatas-application framework
 *
 *
 *
 */

var express = require('express');
var router = express.Router();
var jwt = require('jwt-simple');
var passport = require('passport');
var gcal = require('google-calendar');
var simple_unique_id = require('simple-unique-id');
var moment = require('moment-timezone');

var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var userManagement = require('../dataAccess/userManagementDataAccess');
var meetingManagement = require('../dataAccess/meetingManagement');
var interactions = require('../dataAccess/interactionManagement');
var eventDataAccess = require('../dataAccess/eventDataAccess');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var messageDataAccess = require('../dataAccess/messageManagement');
var Ics = require('../common/icsFormat');
var commonUtility = require('../common/commonUtility');
var meetingSupportClass = require('../common/meetingSupportClass');
var googleCalendarAPI = require('../common/googleCalendar');
var contactClass = require('../dataAccess/contactsManagementClass');

var contactObj = new contactClass();
var meetingSupportClassObj = new meetingSupportClass();
var common = new commonUtility();
var interaction = new interactions();
var googleCalendar = new googleCalendarAPI();
var emailSenders = new emailSender();
var userManagements = new userManagement();
var meetingClassObj = new meetingManagement();
var eventAccess = new  eventDataAccess();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var domain = appCredential.getDomain();
var domainName = domain.domainName;

var winstonLog = require('../common/winstonLog');

var logLib = new winstonLog();
var logger =logLib.getWinston()

router.get('/meeting/getAcceptStatus',function(req,res){
    var status =req.session.acceptMeeting
    delete req.session.acceptMeeting;
    res.send(status);
})

router.get('/meeting/clearAcceptMeeting',function(req,res){
    delete req.session.acceptMeeting;

    res.send(true);
})
/*
router.get('/meeting/:mId',common.checkUserDomain,function(req,res){
    var meetingId = req.params.mId;  // Meeting/ Invitation id
    req.session.meetingId = meetingId;
    req.session.finalPageUrl = '/meeting/'+meetingId;

    if(checkRequred(meetingId)){
        res.render('meetingDetails',common.getLoginStatus(req));
    }else res.redirect('/')
});*/

router.get('/meeting/accept/:mId',function(req,res){
    req.session.acceptMeeting = true;
        res.redirect('/meeting/'+req.params.mId);
});

router.get('/meeting/:mId/:tab',isLoggedIn,function(req,res){
    var meetingId = req.params.mId;  /* Meeting/ Invitation id */
    req.session.meetingId = meetingId;
    req.session.finalPageUrl = '/meeting/'+meetingId;
    req.session.acceptMeeting = false;
    if(checkRequred(meetingId)){
        res.render('meetingDetails',common.getLoginStatus(req));
    }else res.redirect('/')
});


function getMeetingById(invitationId,callback){

    if(checkRequred(invitationId)){
        userManagements.findInvitationById(invitationId,function(error,invitation,message){
            if(error || !invitation){
                callback(false)
            }
            else callback(invitation)
        });
    }else callback(false)
}

router.get('/getRequestedMeeting',isLoggedInUserToSendInvitation,function(req,res){
    var meetingId = req.session.meetingId;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    meetingClassObj.getMeetingWithTodoPopulate(meetingId,function(invitation){
        if(invitation){

            if(invitation.senderId == userId){
                res.send(invitation);
            }else{

                var exist = false;
                var canceled = false;
                for(var i=0; i<invitation.toList.length; i++){
                    if(invitation.toList[i].receiverId == userId){
                        if(invitation.toList[i].canceled){
                            canceled = true;
                        }else
                        exist = true;
                    }
                }

                if(invitation.to){
                    if(invitation.to.receiverId == userId){
                        if(invitation.to.canceled){
                            canceled = true;
                        }else
                        exist = true;
                    }
                }
                if(canceled){
                    res.send('canceled');
                }else
                if(exist){
                    res.send(invitation);
                }else{
                    getUserProfile(userId,function(profile){

                        if(profile){
                            userManagements.findInvitationWithEmail(invitation.invitationId,profile.emailId,function(error,invi){
                                if(invi){
                                    var obj = {
                                        receiverId:profile._id,
                                        receiverEmailId:profile.emailId,
                                        receiverFirstName:profile.firstName,
                                        receiverLastName:profile.lastName,
                                        isAccepted:false
                                    }

                                    updateInviToList(req,res,obj,invi);
                                }else res.send('not authorised');
                            })
                        }else res.send('not authorised');
                    })
                }
            }
        }else
            res.send(false);
    })
});

function updateInviToList(req,res,obj,invi){
    userManagements.updateInvitationToList(invi.invitationId,obj,function(error,isUpdated,message){
        logger.info(message);
       res.send(invi);
    })
}

router.post('/meeting/getDatesBetween/currentInputDay',function(req,res){
    var dates = JSON.parse(req.body.dates);

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    var invitationId = req.body.invitationId;
    var dateStartList = [];
    var dateEndList = [];
    var notList = [];

    for(var i=0; i<dates.length; i++){
       var start = moment(dates[i]).tz('UTC');
        start.date(start.date() - 1);
       start.hour(0);
        start.minute(0);
        start.second(0);
        start.millisecond(0);

        notList.push(new Date(dates[i]));
        dateStartList.push(start.toDate());
        var end = moment(dates[i]).tz('UTC');
        end.date(end.date() + 1);
        end.hour(23);
        end.minute(59);
        end.second(0);
        end.millisecond(0);

        dateEndList.push(end.toDate());
    }

    if(dateStartList.length > 0 && dateEndList.length > 0){

        userManagements.getMeetingsByDatesBetween(userId,invitationId,dateStartList,dateEndList,notList,function(meetings){
            eventAccess.getAllConfirmedEvents(userId,dateStartList,dateEndList,function(eventsArr){
               interaction.getInteractionsByDatesArr(userId,dateStartList,dateEndList,function(googleMeetings){
                   var obj = {
                       meetings:meetings,
                       events:eventsArr,
                       googleEvents:googleMeetings
                   };
                   res.send(obj);
               });
            });
        });
    }else res.send(false);
});

function getUserProfile(userId,back){

    userManagements.getUserBasicProfile(userId,function(error,profile){
        back(profile);
    });
}

router.get('/invitationById/:invitationId',isLoggedIn,function(req,res){
    var invitationId = req.params.invitationId;
    getMeetingById(invitationId,function(invitation){
        res.send(invitation);
    })
});

router.post('/getSenderProfile',function(req,res){
    var userId = req.body.id;

    userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0},function(error,userProfile){
        if (error || userProfile == null || userProfile == undefined) {
            res.send(false);
        }
        else{
            res.send(userProfile);
        }
    })
});

router.post('/createEventGoogle',isLoggedIn, function(req,res){
    var eventDetails = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

        getMeetingById(eventDetails.invitationId,function(invitation){
            var count = invitation.updateCount || 0;
            userManagements.selectUserProfileWitOutContatcs(userId,function(error,profile){
                if(googleCalendar.validateUserGoogleAccount(profile)){

                    createOrUpdateGoogleEvent(userId,eventDetails,req,res,true,false,invitation.googleEventId,invitation,count,false);
                }else{
                    if(invitation.senderId != userId){

                        createOrUpdateGoogleEvent(invitation.senderId,eventDetails,req,res,true,false,invitation.googleEventId,invitation,count,true);
                    }else{
                        if(invitation.selfCalendar && invitation.toList.length == 1){

                            createOrUpdateGoogleEvent(invitation.toList[0].receiverId,eventDetails,req,res,true,false,invitation.googleEventId,invitation,count,true);
                        }else{

                            createOrUpdateGoogleEvent(invitation.to.receiverId,eventDetails,req,res,true,false,invitation.googleEventId,invitation,count,true);
                        }
                    }
                }
            })
        })
});

function getNumString(num){
    return num < 10 ? '0'+num : ''+num
}

function createOrUpdateGoogleEvent(userId,eventDetails,req,res,create,update,googleEventId,invitation,count,oneP){

    if(invitation.isGoogleMeeting == true){
        if(invitation.scheduleTimeSlots.length > 0){
            googleCalendar.acceptGoogleMeeting(userId,googleEventId,invitation.scheduleTimeSlots[0],"accepted",invitation,function(result){
                if(result){
                    updateReceiverContacts(userId,eventDetails.senderId);
                    updateReceiverContacts(eventDetails.senderId,userId);
                    var interaction = {};
                    interaction.userId = eventDetails.receiverId;
                    interaction.action = 'receiver';
                    interaction.interactionDate = eventDetails.start;
                    interaction.endDate = eventDetails.end;
                    interaction.type = 'meeting';
                    interaction.subType = eventDetails.locationType;
                    interaction.refId = eventDetails.invitationId;
                    interaction.source = 'relatas';
                    interaction.title = eventDetails.title;
                    interaction.description = eventDetails.description;
                    interaction.trackInfo = {lastOpenedOn: new Date()};
                    common.storeInteraction(interaction);

                    var interactionNew = {};
                    interactionNew.userId = eventDetails.senderId;
                    interactionNew.action = 'sender';
                    interactionNew.interactionDate = eventDetails.start;
                    interactionNew.endDate = eventDetails.end;
                    interactionNew.type = 'meeting';
                    interactionNew.subType = eventDetails.locationType;
                    interactionNew.refId = eventDetails.invitationId;
                    interactionNew.source = 'relatas';
                    interactionNew.title = eventDetails.title;
                    interactionNew.description = eventDetails.description;
                    interactionNew.trackInfo = {lastOpenedOn: new Date()};
                    common.storeInteraction(interactionNew);

                    common.storeInteraction_new(eventDetails.receiverId,[interaction,interactionNew]);
                    common.storeInteraction_new(eventDetails.senderId,[interaction,interactionNew])

                }
                res.send(true);
            })
        }else res.send(false);
    }
    else{
        userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1},function(error,userProfile) {
            if (error) {
                logger.error('Error occurred while searching authenticated user profile in the DB');
                userProfile = null;
                res.send(userProfile);
            }
            else{
                var userInfo=userProfile;
                if(checkRequred(userInfo.google)){
                    if(checkRequred(userInfo.google[0])){
                        if(checkRequred(userInfo.google[0].id)){
                            var tokenProvider = new GoogleTokenProvider({
                                refresh_token: userInfo.google[0].refreshToken,
                                client_id:     authConfig.GOOGLE_CLIENT_ID,
                                client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                                access_token: userInfo.google[0].token
                            });
                            tokenProvider.getToken(function (err, token) {
                                if (err) {
                                    logger.info('Error in token provider'+err);
                                }
                                var location = eventDetails.locationType+': '+eventDetails.location;

                                var id = eventDetails.invitationId || simple_unique_id.generate('googleRelatas');
                                var google_calendar = new gcal.GoogleCalendar(token);
                                var calendarId='primary';
                                var gId = 'relat'+getNumString(count)+id;
                                var updateCountFlag = true;
                                if(invitation.selfCalendar){
                                    if(invitation.toList.length > 1){
                                        gId = googleEventId ? googleEventId : gId;
                                        updateCountFlag = googleEventId ? false : true;
                                    }
                                }

                                var request_body = {
                                    id:gId,
                                    summary:eventDetails.title,
                                    end:{
                                        dateTime:new Date(eventDetails.end)
                                    },
                                    start:{
                                        dateTime:new Date(eventDetails.start)
                                    },
                                    description:eventDetails.description,
                                    location:location,
                                    locationType:eventDetails.locationType,
                                    locationDetails:eventDetails.location,
                                    relatasEvent:true
                                };


                                if(oneP){
                                    request_body.attendees = [
                                        {
                                            email:userInfo.google[0].emailId,
                                            displayName:userInfo.firstName
                                        }
                                    ]
                                }else{
                                    request_body.attendees = [
                                        {
                                            email:userInfo.google[0].emailId,
                                            displayName:userInfo.firstName
                                        },
                                        {
                                            email:eventDetails.senderPrimaryEmail || eventDetails.senderEmailId,
                                            displayName:eventDetails.senderName
                                        }
                                    ]
                                }

                                if(create){

                                    google_calendar.events.insert(calendarId,request_body,function(err,result){

                                        if(err){
                                            logger.error('creating event in google failed ',err);
                                            res.send(false);
                                        }
                                        else{
                                            count++;
                                            if(updateCountFlag)
                                                updateMeetingWithGoogleEventId(eventDetails.invitationId,result.id,result.iCalUID,count,userId);

                                            logger.info('creating event in google success');

                                            updateReceiverContacts(userId,eventDetails.senderId);
                                            updateReceiverContacts(eventDetails.senderId,userId);
                                            var interaction = {};
                                            interaction.userId = eventDetails.receiverId;
                                            interaction.action = 'receiver';
                                            interaction.interactionDate = eventDetails.start;
                                            interaction.endDate = eventDetails.end;
                                            interaction.type = 'meeting';
                                            interaction.subType = eventDetails.locationType;
                                            interaction.refId = eventDetails.invitationId;
                                            interaction.source = 'relatas';
                                            interaction.title = eventDetails.title;
                                            interaction.description = eventDetails.description;
                                            interaction.trackInfo = {lastOpenedOn: new Date()};
                                            common.storeInteraction(interaction);

                                            var interactionNew = {};
                                            interactionNew.userId = eventDetails.senderId;
                                            interactionNew.action = 'sender';
                                            interactionNew.interactionDate = eventDetails.start;
                                            interactionNew.endDate = eventDetails.end;
                                            interactionNew.type = 'meeting';
                                            interactionNew.subType = eventDetails.locationType;
                                            interactionNew.refId = eventDetails.invitationId;
                                            interactionNew.source = 'relatas';
                                            interactionNew.title = eventDetails.title;
                                            interactionNew.description = eventDetails.description;
                                            interactionNew.trackInfo = {lastOpenedOn: new Date()};
                                            common.storeInteraction(interactionNew);

                                            common.storeInteraction_new(eventDetails.receiverId,[interaction,interactionNew])
                                            common.storeInteraction_new(eventDetails.senderId,[interaction,interactionNew])
                                            res.send(true);
                                        }
                                    });
                                }
                                else if(update){
                                    var eventId = googleEventId
                                    request_body.sequence=count;
                                    google_calendar.events.update(calendarId,eventId,request_body,function(err,result){

                                        if(err){
                                            logger.info('Error in Google event update '+JSON.stringify(err));
                                            res.send(false);
                                        }
                                        else{
                                            logger.info('updating event in google success');
                                            var googleEventId = result.id;
                                            count = count+1;
                                            updateMeetingWithGoogleEventId(eventDetails.invitationId,result.id,result.iCalUID,count,userId);
                                            res.send(true);
                                        }
                                    });
                                }
                            }) // end of token provider
                        }else  res.send(true);
                    }else  res.send(true);
                }else  res.send(true);
            }
        });
    }
}

function removeEventFromGoogleCalendar(googleEventId,userId,req,res,response,callback){

    userManagements.findUserProfileByIdWithCustomFields(userId,{emailId:1,google:1,firstName:1},function(error,userProfile){
        if(error){
            if(response == false){
                callback(false)
            }
            else{
                res.send(false)
            }
        }
        else{
            var userInfo = userProfile;
            if(userInfo.google[0].token == ''){
                logger.info('No Google account found in '+userInfo.firstName+'account to remove event from google calendar');
            }
            else{
                var tokenProvider = new GoogleTokenProvider({
                    refresh_token: userInfo.google[0].refreshToken,
                    client_id:     authConfig.GOOGLE_CLIENT_ID,
                    client_secret: authConfig.GOOGLE_CLIENT_SECRET,
                    access_token: userInfo.google[0].token
                });
                tokenProvider.getToken(function (err, token) {
                    if (err) {
                        logger.info('Error in token provider module');
                    }

                    var google_calendar = new gcal.GoogleCalendar(token);
                    var calendarId='primary';

                    google_calendar.events.delete(calendarId,googleEventId,function(err,result){

                        if(err){
                            logger.info('Error in Google event delete'+JSON.stringify(err));

                            if(response == false){
                                callback(false)
                            }else
                                res.send(false);
                        }
                        else{

                            logger.info('Deleting event in google success');
                            if(response == false){
                                callback(true)
                            }
                            else{
                                res.send(true);
                            }

                        }
                    });
                }) // end of token provider
            }
        }
    })
}

router.post('/sendSuggestionInvitation',isLoggedInUserToSendInvitation,function(req,res){

    var invitationDetails = constructInvitationDetails(req.body);
    invitationDetails.comment = req.body.comment;
    invitationDetails.lastLocation = req.body.lastLocation;
    var info = {
        invitationId:invitationDetails.invitationId
    }

    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.findInvitationById(invitationDetails.invitationId,function(error,invi,msg){
        if(checkRequred(invi) && invi.isGoogleMeeting != true){
            var accepted = false;
            var multipleSuggest = false;
            var lastUser = invi.lastActionUserId || userId;
            var count = invi.updateCount || 0;
                count = count > 0 ? count-1 : count;
            var gEventId = invi.googleEventId;
            gEventId = gEventId ? gEventId : 'relat'+getNumString(count)+invi.invitationId;

            var maxSlot;
            for(var s=0; s<invi.scheduleTimeSlots.length; s++){
                if(invi.scheduleTimeSlots[s].isAccepted){
                    maxSlot = invi.scheduleTimeSlots[s];
                    accepted = true;
                }
            }

            if(!common.checkRequired(maxSlot)){
                maxSlot = invi.scheduleTimeSlots.reduce(function (a, b) {
                    return new Date(a.start.date) < new Date(b.start.date) ? a : b;
                });
            }

            if(invi.selfCalendar){
                if(invi.toList.length > 1){
                    multipleSuggest = true;
                }
                updateReceiverContacts(invi.senderId,invi.toList[0].receiverId);
                updateReceiverContacts(invi.toList[0].receiverId,invi.senderId);
            }else{
                updateReceiverContacts(invi.senderId,invi.to.receiverId);
                updateReceiverContacts(invi.to.receiverId,invi.senderId);
            }

            updateInvitationToListFalse(invi);
            if(accepted){
                if(invi.selfCalendar){
                    removeEventFromGoogleCalendar(gEventId,lastUser,req,res,false,function(flag){
                        updateInvitation(invitationDetails,userId,multipleSuggest,req,res,maxSlot);
                    });
                }
                else{
                    removeEventFromGoogleCalendar(gEventId,lastUser,req,res,false,function(flag){
                        updateInvitation(invitationDetails,userId,multipleSuggest,req,res,maxSlot);
                    });
                }
            }
            else{
                updateInvitation(invitationDetails,userId,multipleSuggest,req,res,maxSlot);
            }
        }
        else{
            res.send(false);
            //updateInvitation(invitationDetails,userId,multipleSuggest,req,res,maxSlot);
        }
    })
});

function updateInvitationToListFalse(invitation){
    if(invitation.selfCalendar){
        var toList = invitation.toList;
        var userId = invitation.toList[0].receiverId;
        for(var i=0; i<toList.length; i++){
            toList[i].isAccepted = false;
            toList[i].canceled = false;
        }
        userManagements.updateInvitationToListNotAccepted(invitation.invitationId,toList,function(error,isUpdate,message){
            logger.info(message.message);
        })
    }
    else{
        meetingClassObj.updateMeetingToNotAccepted(invitation.invitationId,function(success){

        })
    }
}

function updateInvitation(invitationDetails,userId,multipleSuggest,req,res,maxSlot){
    var date = new Date();

    var invitationObj = {
        readStatus:false,
        suggested:true,
        scheduleTimeSlots:invitationDetails.scheduleTimeSlots,
        lastModified:date,
        suggestedBy:{
            userId:userId,
            suggestedDate:date
        }
    }

    if(!checkRequred(invitationObj.scheduleTimeSlots[0].suggestedLocation)){
        invitationObj.scheduleTimeSlots[0].location = invitationDetails.lastLocation;
        invitationObj.scheduleTimeSlots[0].suggestedLocation = invitationDetails.lastLocation;

    }else{
        invitationObj.scheduleTimeSlots[0].location = invitationObj.scheduleTimeSlots[0].suggestedLocation;
    }
    
    var comment = {
            messageDate: new Date(),
            message: invitationDetails.comment,
            userId: userId
         };

    userManagements.updateInvitation(invitationDetails.invitationId,invitationObj,comment,function(error,isUpdated,message){
        if (isUpdated) {
            logger.info(message.message);
            interaction.removeInteractions(invitationDetails.invitationId);
            meetingSupportClassObj.sendSuggestMeetingEmail(invitationDetails.invitationId,maxSlot,invitationDetails.comment);
            sendSuggestInvitationMail(invitationDetails.invitationId,multipleSuggest,req,res);
        }
        else{
            logger.info(message.message);
            res.send(false);
        }
    });
}

function sendSuggestInvitationMail(invitationId,multipleSuggest,req,res){
    getMeetingById(invitationId,function(invitation){
        if(invitation){
            if(!multipleSuggest){
                var userId = invitation.suggestedBy.userId;

                if(invitation.selfCalendar){
                    var obj1;
                    for(var i=0; i<invitation.toList.length; i++){
                        if(invitation.toList[i].receiverId == userId){
                            var rFName2 = invitation.toList[i].receiverFirstName || '';
                            var rLName2 = invitation.toList[i].receiverLastName || '';

                            obj1 = {
                                invitationId:invitationId,
                                senderName:rFName2+' '+rLName2,
                                senderEmailId:invitation.toList[i].receiverEmailId,
                                senderId:invitation.toList[i].receiverId,
                                to:{
                                    receiverName:invitation.senderName,
                                    receiverEmailId:invitation.senderEmailId,
                                    receiverId:invitation.senderId
                                }
                            }
                        }
                    }
                    if(!checkRequred(obj1)){
                        var rFName3 = invitation.toList[0].receiverFirstName || '';
                        var rLName3 = invitation.toList[0].receiverLastName || '';
                        obj1 = {
                            invitationId:invitationId,
                            senderName:invitation.senderName,
                            senderEmailId:invitation.senderEmailId,
                            senderId:invitation.senderId,
                            to:{
                                receiverName:rFName3+' '+rLName3,
                                receiverEmailId:invitation.toList[0].receiverEmailId,
                                receiverId:invitation.toList[0].receiverId
                            }
                        }
                    }

                    if(obj1){
                        if(!multipleSuggest){
                            sendSuggestInvitationMailToReceiver(userId,req.body.timezone,req.body.comment,obj1,invitation);
                        }
                       // var maxSlot =  invitation.scheduleTimeSlots.reduce(function (a, b) { return new Date(a.start.date) < new Date(b.start.date) ? a : b; });
                        //sendNewTemplateToSender(obj1,maxSlot,null,false,req.body.comment);
                        //emailSenders.sendSuggestionInvitationMailToSender(obj1);
                        //emailSenders.sendSuggestionInvitationMailToReceiver(obj1);
                    }
                }else{
                    var obj;
                    if(invitation.to.receiverId == userId){
                        obj = {
                            invitationId:invitationId,
                            senderName:invitation.to.receiverName,
                            senderEmailId:invitation.to.receiverEmailId,
                            senderId:invitation.to.receiverId,
                            to:{
                                receiverName:invitation.senderName,
                                receiverEmailId:invitation.senderEmailId,
                                receiverId:invitation.senderId
                            }
                        }

                    }else{
                        obj = {
                            invitationId:invitationId,
                            senderName:invitation.senderName,
                            senderEmailId:invitation.senderEmailId,
                            senderId:invitation.senderId,
                            to:{
                                receiverName:invitation.to.receiverName,
                                receiverEmailId:invitation.to.receiverEmailId,
                                receiverId:invitation.to.receiverId
                            }
                        }
                    }

                    if(obj){
                        sendSuggestInvitationMailToReceiver(userId,req.body.timezone,req.body.comment,obj,invitation);
                        //var maxSlot2 =  invitation.scheduleTimeSlots.reduce(function (a, b) { return new Date(a.start.date) < new Date(b.start.date) ? a : b; });
                        //sendNewTemplateToSender(obj,maxSlot2,null,false,req.body.comment);
                        //emailSenders.sendSuggestionInvitationMailToSender(obj);
                        //emailSenders.sendSuggestionInvitationMailToReceiver(obj);
                    }
                }
                res.send('success');
            }else{
               var obj2 = {
                    invitationId:invitationId,
                    senderName:invitation.senderName,
                    senderId:invitation.senderId,
                    senderEmailId:invitation.senderEmailId,
                    to:{
                        receiverName:''
                    }
                }

                for(var list=0; list<invitation.toList.length; list++){
                    obj2.to.receiverName += '<br>'+invitation.toList[list].receiverEmailId;
                    obj2.to.receiverId = invitation.toList[list].receiverId;
                }

                sendSuggestInvitationMailToReceiver(invitation.senderId,req.body.timezone,req.body.comment,obj2,invitation,multipleSuggest);
                //emailSenders.sendSuggestionInvitationMailToSender(obj2,multipleSuggest);
                res.send('success');
            }
        }else res.send('success');
    });
}

function sendSuggestInvitationMailToReceiver(userId, timezone, comment, obj, invitationData, multipleSuggest) {
    userManagements.findUserProfileByIdWithCustomFields(userId,{contacts:0,google:0,linkdin:0,facebook:0,twitter:0}, function (error,profile) {
        if (checkRequred(profile)) {

            var maxSlot = invitationData.scheduleTimeSlots.reduce(function (a, b) {
                return new Date(a.start.date) < new Date(b.start.date) ? a : b;
            });

            /*var start = moment(maxSlot.start.date).tz(timezone || 'UTC');
            var date = start.format("DD-MMM-YYYY");
            var time = start.format("h:mm a z");
            var mLocation = maxSlot.locationType + ':' + maxSlot.suggestedLocation || maxSlot.location;
            var mAgenda = maxSlot.description;*/
            var mTitle = maxSlot.title;
            /*var companyName = profile.companyName || '';
            var designation = profile.designation || '';*/

            if (multipleSuggest) {
                for (var i = 0; i < invitationData.toList.length; i++) {

                  /*  var receiverNameF = invitationData.toList[i].receiverFirstName || '';
                    var receiverNameL = invitationData.toList[i].receiverLastName || '';
                    var rfn = receiverNameF + ' ' + receiverNameL;
                    var senderName = invitationData.senderName;
                    var dataNew = {
                        invitationId: invitationData.invitationId,
                        receiverEmailId: invitationData.toList[i].receiverEmailId,
                        senderId:invitationData.senderId,
                        receiverId:invitationData.toList[i].receiverId,
                        senderLocation:profile.location || '',
                        timezone:timezone,
                        receiverName: rfn,
                        senderName: senderName,
                        mDate: date,
                        mTime: time,
                        mLocation: mLocation,
                        mAgenda: mAgenda,
                        mTitle: mTitle,
                        senderCompany: designation + ', ' + companyName,
                        senderPicUrl: domainName + '/getImage/' + invitationData.senderId,
                        comment: comment
                    };*/

                    if(checkRequred(comment)){
                        storeCommentInMessageCollection(profile._id,invitationData.toList[i].receiverId,invitationData.toList[i].receiverEmailId,mTitle,comment);
                    }
                    /*if(common.checkRequired(invitationData.toList[i].receiverId)){
                        sendMailMultipleSingleMail(maxSlot,invitationData.toList[i],timezone,invitationData,profile,comment);
                    }else common.sendMeetingInvitationMailToReceiver(dataNew,true);*/
                }
            }
            else if(common.checkRequired(obj.to.receiverId)){

              userManagements.findUserProfileByIdWithCustomFields(obj.to.receiverId,{firstName:1,lastName:1,emailId:1,timezone:1},function(error,receiver){
                if(common.checkRequired(receiver)){
                    if (receiver.timezone) {
                        if (receiver.timezone.name) {
                            timezone = receiver.timezone.name || 'UTC'
                        }
                    }
                    /*var start = moment(maxSlot.start.date).tz(timezone || 'UTC');
                    var date = start.format("DD-MMM-YYYY");
                    var time = start.format("h:mm a z");
                    var mLocation = maxSlot.locationType + ':' + maxSlot.suggestedLocation || maxSlot.location;
                    var mAgenda = maxSlot.description;*/
                    var mTitle = maxSlot.title;
                    //var companyName = profile.companyName || '';
                    //var designation = profile.designation || '';
                    /*var data = {
                        invitationId: invitationData.invitationId,
                        receiverEmailId: receiver.emailId,
                        receiverName: receiver.firstName+' '+receiver.lastName,
                        senderName: obj.senderName,
                        senderId:obj.senderId,
                        receiverId:receiver._id,
                        senderLocation:profile.location || '',
                        timezone:timezone,
                        mDate: date,
                        mTime: time,
                        mLocation: mLocation,
                        mAgenda: mAgenda,
                        mTitle: mTitle,
                        senderCompany: designation + ', ' + companyName,
                        senderPicUrl: domainName + '/getImage/' + profile._id,
                        comment: comment
                    };*/
                    if(checkRequred(comment)){
                        storeCommentInMessageCollection(profile._id,obj.to.receiverId,obj.to.receiverEmailId,mTitle,comment);
                    }
                    //common.sendMeetingInvitationMailToReceiver(data,true);
                }
              });
            }
            else{
                /*var data = {
                    invitationId: invitationData.invitationId,
                    receiverEmailId: obj.to.receiverEmailId,
                    receiverName: obj.to.receiverName,
                    senderName: obj.senderName,
                    senderId:obj.senderId,
                    receiverId:obj.to.receiverId,
                    senderLocation:profile.location || '',
                    timezone:timezone,
                    mDate: date,
                    mTime: time,
                    mLocation: mLocation,
                    mAgenda: mAgenda,
                    mTitle: mTitle,
                    senderCompany: designation + ', ' + companyName,
                    senderPicUrl: domainName + '/getImage/' + profile._id,
                    comment: comment
                }*/
                if(checkRequred(comment)){
                    storeCommentInMessageCollection(profile._id,obj.to.receiverId,obj.to.receiverEmailId,mTitle,comment);
                }
                //common.sendMeetingInvitationMailToReceiver(data,true);
            }
        }
    })
}

function sendMailMultipleSingleMail(maxSlot,to,timezone,invitationData,profile,comment){
    userManagements.findUserProfileByIdWithCustomFields(to.receiverId,{firstName:1,lastName:1,emailId:1,timezone:1},function(error,publicProfile){
       if(common.checkRequired(publicProfile)){
           if (publicProfile.timezone) {
               if (publicProfile.timezone.name) {
                   timezone = publicProfile.timezone.name || 'UTC'
               }
           }

           var start = moment(maxSlot.start.date).tz(timezone || 'UTC');
           var date = start.format("DD-MMM-YYYY");
           var time = start.format("h:mm a z");
           var mLocation = maxSlot.locationType + ':' + maxSlot.suggestedLocation || maxSlot.location;
           var mAgenda = maxSlot.description;
           var mTitle = maxSlot.title;
           var companyName = profile.companyName || '';
           var designation = profile.designation || '';

           var receiverNameF = publicProfile.firstName || '';
           var receiverNameL = publicProfile.lastName || '';
           var rfn = receiverNameF + ' ' + receiverNameL;
           var senderName = invitationData.senderName;
           var dataNew = {
               invitationId: invitationData.invitationId,
               receiverEmailId: publicProfile.emailId,
               senderId:invitationData.senderId,
               receiverId:publicProfile._id,
               senderLocation:profile.location || '',
               timezone:timezone,
               receiverName: rfn,
               senderName: senderName,
               mDate: date,
               mTime: time,
               mLocation: mLocation,
               mAgenda: mAgenda,
               mTitle: mTitle,
               senderCompany: designation + ', ' + companyName,
               senderPicUrl: domainName + '/getImage/' + invitationData.senderId,
               comment: comment
           };
           common.sendMeetingInvitationMailToReceiver(dataNew,true);
       }
    });
}

//*  Sending Meeting request emails  *//
function sendNewTemplateToSender(invitationData,maxSlot,senderTimezone,defaultEmail,comment){
    var exclude = {contacts:0,google:0,linkedin:0,twitter:0,officeCalendar:0,currentLocation:0,calendarAccess:0};
    userManagements.findUserProfileByIdWithCustomFields(invitationData.to.receiverId,exclude,function(error,receiver){
        userManagements.findUserProfileByIdWithCustomFields(invitationData.senderId,exclude,function(er,sender){
            if(common.checkRequired(receiver) && common.checkRequired(sender)){

                if (sender.timezone) {
                    if (sender.timezone.name) {
                        senderTimezone = sender.timezone.name
                    }
                }

                var timezone2 = senderTimezone || 'UTC';
                var start = moment(maxSlot.start.date).tz(timezone2);
                var date = start.format("DD-MMM-YYYY");
                var time = start.format("h:mm a z");
                var mLocation = maxSlot.locationType+':'+maxSlot.location;
                var mAgenda = maxSlot.description;
                var mTitle = maxSlot.title;
                var companyName = receiver.companyName || '';
                var designation = receiver.designation || '';

                var data = {
                    invitationId:invitationData.invitationId,
                    receiverEmailId:invitationData.senderEmailId,
                    receiverName:invitationData.senderName,
                    senderName:invitationData.to.receiverName,
                    senderId:invitationData.to.receiverId,
                    receiverId:invitationData.senderId,
                    mDate:date,
                    mTime:time,
                    mLocation:mLocation,
                    mAgenda:mAgenda,
                    mTitle:mTitle,
                    senderLocation:receiver.location || '',
                    timezone:timezone2,
                    senderCompany:designation+', '+companyName,
                    senderPicUrl:domainName +'/getImage/'+receiver._id,
                    comment:comment
                };
                common.sendMeetingInvitationMailToSender(data,true);
            }
            else{
                if(defaultEmail)
                    emailSenders.sendInvitationMailToSender(invitationData);
            }
        });
    });
}


function storeCommentInMessageCollection(senderId,receiverId,receiverEmailId,mTitle,message){

        var messageAccess = new messageDataAccess();
        var message = {
            senderId:senderId,
            receiverId:receiverId || '',
            receiverEmailId:receiverEmailId,
            type:'meeting-comment',
            subject:mTitle,
            message:message.replace(/<br>/g,'\n')
        }
        messageAccess.createMessage(message,function(message){
            if(message){
                var interaction = {};
                interaction.userId = message.senderId;
                interaction.action = 'sender';
                interaction.type = 'meeting-comment';
                interaction.subType = 'meeting-comment';
                interaction.refId = message._id;
                interaction.source = 'relatas';
                interaction.title = message.subject;
                interaction.description = message.message;
                common.storeInteraction(interaction);

                var interactionReceiver = {};
                interactionReceiver.userId = message.receiverId || '';
                interactionReceiver.emailId = message.receiverEmailId;
                interactionReceiver.action = 'receiver';
                interactionReceiver.type = 'meeting-comment';
                interactionReceiver.subType = 'meeting-comment';
                interactionReceiver.refId = message._id;
                interactionReceiver.source = 'relatas';
                interactionReceiver.title = message.subject;
                interactionReceiver.description = message.message;
                common.storeInteractionReceiver(interactionReceiver);
            }
        });

}

router.post('/updateEventStatus',function(req,res){
    var invitation = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;

    userManagements.updateInvitationReadStatus(invitation,userId,function(error,isUpdated,message){
        if (isUpdated) {

            logger.info(message.message);
            if(invitation.selfCalendar){
                invitation.loggeinUserId = userId;

                acceptMeetingSelfCalendar(invitation,userId,function(result){
                    res.send(result)
                })
            }else{

                generateIcsFile(invitation,userId,function(result){
                    res.send(result)
                })
            }

        }else{
            logger.info(message.message);
            res.send(false);
        }
    });
});

function acceptMeetingSelfCalendar(invitation,userId,callback){

    userManagements.updateInvitationToListAccepted(invitation.invitationId,invitation,function(error,isSuccess,message){

        if(error || !isSuccess){
            callback(false);
        }else{
            generateIcsFile(invitation,userId,function(result){
                callback(result);
            })
        }
    })
}

function generateIcsFile(invitation,userId,callback){
    var icsFormat = new Ics();

    userManagements.findInvitationById(invitation.invitationId,function(error,meeting){
        if(checkRequred(meeting)){
            icsFormat.createMeetingIcs(meeting,invitation.timezone,invitation.receiverName,function(icsFile){

                callback(icsFile);
            })
        }else{
            sendAcceptInvitationEmail(invitation);

            callback('ics failed');
        }
    })
}

router.post('/updateMeeting/icsfile',function(req,res){
    var details = req.body;
    if(checkRequred(details.invitationId) && checkRequred(details.icsAwsKey) && checkRequred(details.icsUrl)){
        userManagements.updateInvitationWithIcs(details,function(error,isUpdated){
            if(isUpdated){

                res.send(true);
            }else res.send(false)
        })
    }else res.send(false);
});

router.post('/sendEmailWithAttachment',function(req,res){
    var invitationDetails = req.body;
    var tokenSecret = "alpha";
    var decoded = jwt.decode(req.user ,tokenSecret);
    var userId = decoded.id;
    if(invitationDetails){
        sendAcceptInvitationEmail(invitationDetails,userId);
        res.send(true);
    }else{
        res.send(false);
    }
});

function sendAcceptInvitationEmail(invitation,userId){

    meetingSupportClassObj.sendAcceptInvitationEmail(invitation,userId);
   /* var date = new Date();
    if(checkRequred(invitation.message)){
        var messageAccess = new messageDataAccess();
        var message = {
            senderId:userId,
            receiverId:invitation.senderId || '',
            receiverEmailId:invitation.senderEmailId,
            type:'meeting message',
            subject:invitation.description,
            message:invitation.message.replace(/<br>/g,'\n')
        }
        messageAccess.createMessage(message,function(message){
            if(message){
                var interaction = {};
                interaction.userId = message.senderId;
                interaction.action = 'sender';
                interaction.type = 'message';
                interaction.subType = 'message';
                interaction.refId = message._id;
                interaction.source = 'relatas';
                interaction.title = message.subject;
                interaction.description = message.message;
                common.storeInteraction(interaction);

                var interactionReceiver = {};
                interactionReceiver.userId = message.receiverId || '';
                interactionReceiver.emailId = message.receiverEmailId;
                interactionReceiver.action = 'receiver';
                interactionReceiver.type = 'message';
                interactionReceiver.subType = 'message';
                interactionReceiver.refId = message._id;
                interactionReceiver.source = 'relatas';
                interactionReceiver.title = message.subject;
                interactionReceiver.description = message.message;
                common.storeInteractionReceiver(interactionReceiver);
            }
        });
    }
    emailSenders.sendInvitationAcceptMailToSender(invitation);
    emailSenders.sendInvitationAcceptMailToReceiver(invitation);*/
}

router.post('/declineMeeting',function(req,res){
    var invitation = req.body;
    userManagements.removeInvitation(invitation.invitationId,function(error,isDeleted,message){
        if (isDeleted) {
            logger.info(message.message);
            res.send(true);
        }
        else{
            logger.info(message.message);
            res.send(false);
        }
    });
});

function updateReceiverContacts(senderId,receiverId){

    if(checkRequred(senderId) && checkRequred(receiverId)){
        userManagements.findUserProfileByIdWithCustomFields(senderId,{emailId:1,firstName:1,lastName:1,mobileNumber:1},function(error,profile) {

            if(error){

            }else
            if(profile != null || profile != undefined){

                var contact1 = {
                    personId:profile._id,
                    personName:profile.firstName+' '+profile.lastName,
                    personEmailId:profile.emailId,
                    count:1,
                    lastInteracted:new Date(),
                    addedDate: new Date(),
                    verified:true,
                    relatasContact:true,
                    relatasUser:true,
                    mobileNumber:profile.mobileNumber
                };
                updateContact(receiverId,contact1);
            }
        });
    }
}

function checkRequred(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();//next();
    }else
    {

        return res.redirect('/');
    }
}

function isLoggedInUserToSendInvitation(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()){

        return next();
    }else
    {
        res.send('loginRequired');
    }
}


function constructInvitationDetails(data){
    var invitationDetails = {
        suggested:true,
        invitationId:data.invitationId,
        senderId:'',
        senderName:'',
        senderEmailId:'',
        participants:[
            {
                emailId:data.receiverEmailId
            }
        ],
        to:{
            receiverId:data.receiverId,
            receiverName:data.receiverName,
            receiverEmailId:data.receiverEmailId
        },
        scheduleTimeSlots:[],
        docs:[]
    }

    for (var i in data.docs){
        if(checkRequred(data.docs[i].documentName) && checkRequred(data.docs[i].documentUrl)){
            invitationDetails.docs.push({
                documentName:data.documentName,
                documentUrl:data.documentUrl,
                documentId:data.documentId
            })
        }
    }



    if (checkRequred(data.startDate1) && checkRequred(data.endDate1)) {
        var slotOne = {
            start:{
                date      : data.startDate1
            },
            end:{
                date:data.endDate1
            },
            title:data.title1,
            location:data.location1,
            suggestedLocation:data.suggestedLocation1,
            locationType:data.locationType1,
            description:data.description1
        }
        invitationDetails.scheduleTimeSlots.push(slotOne);
    }

    if (checkRequred(data.startDate2) && checkRequred(data.endDate2)) {
        var slotTwo = {
            start:{
                date  : data.startDate2
            },
            end:{
                date  : data.endDate2
            },
            title:data.title2,
            location:data.location2,
            suggestedLocation:data.suggestedLocation2,
            locationType:data.locationType2,
            description:data.description2
        }
        invitationDetails.scheduleTimeSlots.push(slotTwo);
    }

    if (checkRequred(data.startDate3) && checkRequred(data.endDate3)) {
        var slotThree = {
            start:{
                date    : data.startDate3
            },
            end:{
                date    : data.endDate3
            },
            title:data.title3,
            location:data.location3,
            suggestedLocation:data.suggestedLocation3,
            locationType:data.locationType3,
            description:data.description3
        }
        invitationDetails.scheduleTimeSlots.push(slotThree);
    }

    return invitationDetails;
}

//Function to add Contact
function updateContact(userId,contact){
    contactObj.addSingleContact(userId,contact)
}

function updateMeetingWithGoogleEventId(invitationId,googleEId,iCalUID,count,userId){

    userManagements.updateMeetingWithGoogleEventId(invitationId,googleEId,iCalUID,count,userId,function(error,isUpdated,message){
        logger.info(message.message);
    })
}

module.exports = router;