var myUserCollection = require('../databaseSchema/userManagementSchema').User;
var accountsCollection = require('../databaseSchema/accountSchema').accounts;
var accountsLogCollection = require('../databaseSchema/accountLog').accountsLog;

var async = require("async");

var mongoose = require('mongoose');

var winstonLog = require('../common/winstonLog');
var Mailer = require('../common/mailer');

var logLib = new winstonLog();
var mailObj = new Mailer();

var logger =logLib.getInteractionsErrorLogger();
var _ = require("lodash")
    , moment = require("moment")

var momentTZ = require('moment-timezone');

function AccessManagement(){}

AccessManagement.prototype.hierarchyAccess = function (common,liu,accounts,callback) {

    var ownerEmailIds = [], accountsWithAccessProperties=[];
    _.each(accounts,function (ac) {
        ownerEmailIds.push(ac.ownerEmailId);
    });

    findAccountOwnersHierarchy(ownerEmailIds,function (err,access) {

        var accessToOwnerAccounts = {};
        if(!err && access && access.length>0){
            _.each(access,function (el) {
                var userIds = [];
                if(el.hierarchyPath && el.hierarchyPath[0]){
                    userIds.push(_.compact(el.hierarchyPath[0].split(",")))
                }

                accessToOwnerAccounts[el._id] = _.flatten(userIds)
            });
        }

        _.each(accounts,function (ac) {
            var obj = JSON.parse(JSON.stringify(ac))
            
            if(obj.reportingHierarchyAccess){
                obj.heiarchyWithAccess = accessToOwnerAccounts[ac.ownerEmailId]
            } else {
                obj.heiarchyWithAccess = []
            }

            accountsWithAccessProperties.push(obj)
        })

        callback(null,accountsWithAccessProperties)
    });
};

function getLiuHierarchyAccess(companyId,userId,callback){

    var find = {
        companyId:companyId,
        hierarchyPath: { $regex: userId.toString(), $options: 'i' }
    }

    var project = {
        emailId:1
    }

    findAndProjectUsersTemplate(find,project,function (err,accessToUsers) {
        callback(err,accessToUsers)
    })

};

function getUsersEmailId(userIds,callback){
    findAndProjectUsersTemplate({_id:{$in:userIds}},{emailId:1},function (err,users) {
        callback(err,users)
    })
}

function findAndProjectUsersTemplate(findQuery,project,callback){
    myUserCollection.find(findQuery,project,function (err,data) {
        callback(err,data)
    });
}

function findAndProjectUserTemplate(findQuery,project,callback){
    myUserCollection.findOne(findQuery,project,function (err,data) {
        callback(err,data)
    });
}

function findAccountOwnersHierarchy(ownerEmailIds,callback){
    myUserCollection.aggregate([
        {
            $match:{
                emailId:{$in:ownerEmailIds}
            }

        },
        {
            $group:{
                _id:"$emailId",
                hierarchyPath:{$push:"$hierarchyPath"}
            }
        }
    ]).exec(function (err,data) {
        callback(err,data)
    })
}

module.exports = AccessManagement;