
//**************************************************************************************
// File Name            : documentView
// History              : renamed from documents -> documentView
// Functionality        :
//
//
//**************************************************************************************

var express = require('express');
var mongoose = require('mongoose');
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var engines = require('consolidate');
var busboy = require('connect-busboy');
var redis   = require("redis");
var client  = redis.createClient();
var redisStore = require('connect-redis')(session);
var emailSender = require('./public/javascripts/emailSender');
var debug = require('debug')('Relatas-Framework');
var json2xls = require('json2xls');
const registerModels = require('./register-models');

var app = express();

var compress = require('compression');
app.use(compress());

var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
};

app.use(function (req, res, next) {
 if(req.headers['x-user-ip'])
  req.headers.userRemoteIP = req.headers['x-user-ip'].split(",")[0]
 next();
});
var appCredentials = require('./config/relatasConfiguration');
var winstonLog = require('./common/winstonLog');
var logLib = new winstonLog();
var appCredential = new appCredentials();
var serverPort = appCredential.getServerPort();

// var socketServer = http.createServer(app);
// var io = require('socket.io').listen(socketServer);

// var io = require('./routes/socketServer');
//
if (appCredential.getServerEnvironment() == 'STAGING' || appCredential.getServerEnvironment() == 'DEV' || appCredential.getServerEnvironment() == 'LIVE') {

    var dbPasswords = appCredential.getDBPasswords();
    var emailSenders = new emailSender();
    mongoose.connect(dbPasswords.relatasDB.dbConnectionUrl, {user: dbPasswords.relatasDB.user, pass: dbPasswords.relatasDB.password, auth: { authdb: 'Relatas_mask' }},function(error){
        if(error)
            emailSenders.sendUncaughtException(error, JSON.stringify(error), "error", appCredential.getServerEnvironment() + ": uncaughtException");
    });
} else {
    // mongoose.connect("mongodb://localhost/newDb?poolSize=100",function(error){
    mongoose.connect("mongodb://localhost/Relatas?poolSize=100",function(error){
    })
}

/*
*
* ####### This is multi db connections ######
* // require your models directory
var models = require('./models');

// Create models using mongoose connection for use in controllers
app.use(function db(req, res, next) {
    req.db = {
        User: global.App.activdb.model('User', models.agency_user, 'users')
        //Post: global.App.activdb.model('Post', models.Post, 'posts')
    };
    return next();
});
*
*
* * ####### This is multi db connections ######
* */

// var loggerInfo = logLib.getWinston();
var loggerError = logLib.getWinstonError();

var meetx = require('./routes/meetx');
var profiles = require('./routes/profile');
var editProfile = require('./routes/editProfile');
var meetingProcess = require('./routes/meetingProcess');
var emailRegistration = require('./routes/emailRegistration');
var meetingConfirmation = require('./routes/meetingConfirmation');
var dashboard = require('./routes/dashboard');
var myDocuments = require('./routes/myDocuments');
var forgotPassword = require('./routes/forgotPassword');
var events = require('./routes/events');
var contacts = require('./routes/contacts');
var meetings = require('./routes/meetings');
var interactions = require('./routes/interactions');
var admin = require('./routes/admin');
var office365 = require('./routes/office365');
var notifications = require('./routes/notifications');
var messages = require('./routes/messages');
var tasks = require('./routes/tasks');
var corporateDashboard = require('./routes/corporateRoutes/corporateDashboard');
var corporateSignUp = require('./routes/corporateRoutes/corporateSignUp');
var corporateAdmin = require('./routes/corporateRoutes/corporateAdmin');
var webWidget = require('./routes/webWidget/webWidget.js');

// MOBILE API
var mobileAuth = require('./routes/mobileRest/authenticate');
var aggregate = require('./routes/mobileRest/aggregate');
var mobileInteractions = require('./routes/mobileRest/interactions');
var mobileContacts = require('./routes/mobileRest/contacts');
var mobileNotifications = require('./routes/mobileRest/notifications');
var mobileProfile = require('./routes/mobileRest/profile');
var mobileTasks = require('./routes/mobileRest/tasks');
var mobileMeetings = require('./routes/mobileRest/meetings');
var mobileDashboard = require('./routes/mobileRest/dashboard');
var mobileCombinedAPI = require('./routes/mobileRest/combinedAPI');

// NEW SCHEDULING API
var scheduling = require('./routes/scheduling');

// New Web
var webDashboard = require('./routes/webRest/dashboard');
var webProfile = require('./routes/webRest/profile');
var webScheduling = require('./routes/webRest/schedule');
var webToday = require('./routes/webRest/today');
var webMeetings = require('./routes/webRest/meeting');
var webContacts = require('./routes/webRest/contacts');
var webOpportunities = require('./routes/webRest/opportunities');
var webRecommendations = require('./routes/webRest/recommendation');
var contactsUpload = require('./routes/webRest/contactsUpload');
var webInteractions = require('./routes/webRest/interactions');
var webTasks = require('./routes/webRest/tasks');
var webMessages = require('./routes/webRest/message');
var webCompanies = require('./routes/webRest/company').router;
var webSearch = require('./routes/webRest/search');
var webCalendar = require('./routes/webRest/calendar');
var webCalendarV2 = require('./routes/webRest/calendarV2');
var webNotifications = require('./routes/webRest/notifications');
var webSignup = require('./routes/webRest/signUp');
var webHashtag = require('./routes/webRest/hashtag');
//var webRelationshipCartoon = require('./routes/webRest/surveys');
var webInsights = require('./routes/webRest/insights');
var webPulse = require('./routes/webRest/pulse');
var webMeetingReview = require('./routes/webRest/meetingReview');
var webActionItems = require('./routes/webRest/actionItems');
var messagesv2 = require('./routes/webRest/messagesV2');
var webDashboard3a = require('./routes/webRest/dashboard3a');
var webRWebLead = require('./routes/webRest/rWebLead');
var webOrgRoles = require('./routes/webRest/orgRoles');
var webErrorMessages = require('./routes/webRest/errorMessagesApp');
var webReports = require('./routes/webRest/reports');
var webMeetingScheduler = require('./routes/webRest/meetingScheduler');
var salesforce = require('./routes/webRest/salesforce').router;
var webDocuments = require('./routes/webRest/documents');
var webDocumentView = require('./routes/documentView');
var webDocumentShare = require('./routes/webRest/documentShare');
var webDocumentTemplate = require('./routes/webRest/documentTemplate');
var webCustomerProfiles = require('./routes/webRest/customerProfile');
var webCustomerProfileTemplate = require('./routes/webRest/customerProfileTemplate');
var websiteLeads = require('./routes/webRest/websiteLeads');
var webMultiDbs = require('./routes/webRest/multiDbApis');


app.set('views', path.join(__dirname + '/views'));
app.engine('html', engines.swig);
app.set('view engine', 'html');

app.use(favicon());
app.use(logger('dev'));

app.use(bodyParser.urlencoded({limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));
// app.use(bodyParser.text({ type: 'json' }));

app.use(cookieParser());
app.use(busboy());
app.use(require('less-middleware')(path.join(__dirname + '/views')));
app.use(allowCrossDomain);
app.use(express.static(path.join(__dirname, 'public')));
//TODO: need to use it back: secret key!
var EXPIRY_TIME_LIMIT = 60 * 60 * 24;
app.use(session({
    store: new redisStore({ host: 'localhost', port: 6379, client: client, ttl:7200}),
    secret: "123zxcvbnm890",
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

app.all ('/*', function (req, res, next){
    var protocol = 'http' + (req.connection.encrypted ? 's' : '') + '://';
    var host = req.headers.host;
    var href;

    // no www. present, nothing to do here
    if (!/^www\./i.test(host)) {
        next();
        return;
    }

    // remove www.
    host = host.replace(/^www\./i, '');
    href = protocol + host + req.url;

    res.redirect(href);

});

function redirectToCorporateUrl(req, res, next){
    if (req.isAuthenticated()){
        var UserCollection = require('./databaseSchema/userManagementSchema').User;
        var commonUtility = require('./common/commonUtility');
        var common = new commonUtility();
        var companyCollection = require('./databaseSchema/corporateCollectionSchema').companyModel
        var userId = common.getUserId(req.user);
        UserCollection.findOne({_id: userId},{emailId: 1}, function(err, user){
            var companyDomain = user.emailId.substr(user.emailId.indexOf("@") + 1)
            companyCollection.findOne({emailDomains: companyDomain}, function(err, company){

                if(company)
                    if(req.headers.host != company.url)
                        res.redirect("http://" + company.url)
                    else
                        next()
                else
                if(req.headers.host != appCredential.getDomain().mainDomain)
                    res.redirect(appCredential.getDomain().domainName)
                else
                    next()
            })
        })
    }else
        next()
}


app.use(json2xls.middleware);

//app.get('/', redirectToCorporateUrl)

app.use('/api',webCalendarV2);
app.use('/', corporateDashboard);
app.use('/', interactions);
app.use('/', corporateSignUp);
app.use('/', corporateAdmin);
app.use('/', dashboard);
app.use('/', meetx);
app.use('/', emailRegistration);
app.use('/', meetingProcess);
app.use('/', editProfile);
app.use('/', meetingConfirmation);
app.use('/', myDocuments);
app.use('/', forgotPassword);
app.use('/', events);
app.use('/', contacts);
app.use('/', notifications);
app.use('/', meetings);
app.use('/', admin);
app.use('/', office365);
app.use('/', profiles);
app.use('/', webWidget);
app.use('/', messages);
app.use('/', tasks);
app.use('/', webNotifications);

// MOBILE API
app.use('/', mobileAuth);
app.use('/', aggregate);
app.use('/', mobileInteractions);
app.use('/', mobileContacts);
app.use('/', mobileNotifications);
app.use('/', mobileProfile);
app.use('/', mobileTasks);
app.use('/', mobileMeetings);
app.use('/', mobileDashboard);
app.use('/', mobileCombinedAPI);

// NEW SCHEDULING
app.use('/',scheduling);

// NEW WEB
app.use('/',webDashboard);
app.use('/',webInteractions);
app.use('/',webProfile);
app.use('/',webScheduling);
app.use('/',webToday);
app.use('/',webMeetings);
app.use('/',webContacts);
app.use('/',contactsUpload);
app.use('/',webTasks);
app.use('/',webMessages);
app.use('/',webCompanies);
app.use('/',webSearch);
app.use('/',webCalendar);
app.use('/',webSignup);
app.use('/',webHashtag);
app.use('/',webInsights);
app.use('/',webPulse);
app.use('/',webMeetingReview);
app.use('/',webActionItems);
app.use('/',messagesv2);
app.use('/',webDashboard3a);
app.use('/',salesforce);
app.use('/',webOrgRoles);
app.use('/',webErrorMessages);
app.use('/',webRWebLead);
app.use('/',webReports);
app.use('/',webOpportunities);
app.use('/',webRecommendations);
app.use('/',websiteLeads);
app.use('/',webMultiDbs);
//app.use('/',webRelationshipCartoon);
app.use('/',webDocuments);
app.use('/',webDocumentView);
app.use('/',webDocumentShare);
app.use('/',webDocumentTemplate);
app.use('/',webCustomerProfiles);
app.use('/',webCustomerProfileTemplate);
app.use('/',webMeetingScheduler);

/// catch 404 and forwarding to error handler
/*app.use(function (req, res, next) {
 var err = new Error('Not Found');
 err.status = 404;
 next(err);
 });*/
/*
 // Make our db accessible to our router
 app.use(function (req, res, next) {
 req.db = db;
 next();
 });*/

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        loggerError.info('Error app.js '+err)

        res.status(err.status || 500);
        var emailSenders = new emailSender();
        //emailSenders.sendUncaughtException(err,err.message,err.stack,appCredential.getServerEnvironment()+" Local Dev URL Error Report");
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

//if(appCredential.getServerEnvironment() != 'LOCAL'){
process.on('uncaughtException', function (err) {

    loggerError.info('uncaughtException: message: ' + err.message + ' stack: ' + JSON.stringify(err));
    var emailSenders = new emailSender();
    emailSenders.sendUncaughtException(err,err.message,err.stack,appCredential.getServerEnvironment()+": uncaughtException");
});
//}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    var emailSenders = new emailSender();
    //emailSenders.sendUncaughtException(err, err.message, err.stack, appCredential.getServerEnvironment()+": Production URL Error Report");
    res.render('error', {
        message: err.message,
        error: {}
    });
});

var PORT = serverPort.PORT;

// here we are working on few features where we need to include the 
// correct path to notify the user about how to 
// develop once all the development is finished to query for the serve

// here is the code to see all the features to make sure that we are on the same track as of on the web standards
// so we are not on the same track we need time to understand each other properly to make sure that 
// this ide 

app.set('port', process.env.PORT || PORT);
var server = app.listen(app.get('port'), function () {
    debug('Express server listening on port ' + server.address().port);
});

// io.attach(server);

// socketServer.listen(app.get('port'), function () {
//   console.log("Socket Listener")
// });

module.exports = app;

