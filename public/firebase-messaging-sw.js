importScripts('https://www.gstatic.com/firebasejs/6.0.1/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/6.0.1/firebase-messaging.js')

const firebaseConfig = {
    apiKey: "AIzaSyAYmfsxJkugz87BuzC0JDaDQvFHEB5szIw",
    authDomain: "relatas-showcase.firebaseapp.com",
    databaseURL: "https://relatas-showcase.firebaseio.com",
    projectId: "relatas-showcase",
    storageBucket: "relatas-showcase.appspot.com",
    messagingSenderId: "871167737144",
    appId: "1:871167737144:web:7f229ff8d7278dca"
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

// Handling notification message when web app in background
messaging.setBackgroundMessageHandler(function(payload) {
    const title = payload.notification.title;
    const options = {
        body: payload.notification.body,
    }

    return self.registration.showNotification(title, options);
})