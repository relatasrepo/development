
$(window).load(function(){
    $("a").click(function(event) {
        if($(this).attr('href') != undefined){
            var cb = generate_callback($(this).attr('href'));
            event.preventDefault();
            mixpanel.track($(this).attr('m_event') || 'Link_clicked',{link:$(this).attr('href'),page:window.location.href});
            mixpanel.people.increment($(this).attr('m_event') || 'Link_clicked',cb);

            setTimeout(cb, 500);
        }
    });

    function generate_callback(link) {
        console.log('clicked link')
        return function() {
            window.location.replace(link);
        }
    }
});

function trackMixpanel(m_event,url,cb){
    mixpanel.track(m_event || 'Link_clicked',{link:url,page:window.location.href},cb);
    // mixpanel.people.increment($(this).attr('m_event') || 'Link_clicked',cb);
    setTimeout(cb, 500);
}

function identifyMixPanelUser(profile){

    mixpanel.identify(profile.emailId);
    mixpanel.people.set({
        "$email": profile.emailId,    // only special properties need the $
        "$first_name": profile.firstName,
        "$last_name": profile.lastName,
        "$created": new Date(),
        "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
        "page":window.location.href
    });

}