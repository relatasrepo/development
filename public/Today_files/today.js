var reletasApp = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);
var timezone ;
reletasApp.controller("logedinUser", function ($scope, $http,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {

            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
            }
            else{

            }
        }).error(function (data) {

        })
});

/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

reletasApp.controller("bottom_search", function ($scope, $http) {
    $scope.mySearchFunction = function(searchContent){
        if(typeof searchContent == 'string' && checkRequired(searchContent.trim())){
            window.location = '/contacts/search?searchContent='+searchContent;
        }
    }
    $scope.goToContactsSearch = function(searchContent){
        if(typeof searchContent == 'string' && checkRequired(searchContent.trim())){
            window.location = '/contacts/search?searchContent='+searchContent;
        }
    }
});

var once = true;
reletasApp.controller("left_bar_today", function ($scope, $http) {
    if(once){
        once = false
        $scope.todayMeetings = [];
        $scope.newMeetings = [];
        $scope.upcomingMeetings = {};
        $scope.tasks = {};
        $http.get('/today/left/section')
            .success(function (response) {
                if(response.SuccessCode){
                    if(checkRequired(response.Data.timezone)){
                        timezone = response.Data.timezone;
                    }
                    if(typeof response.Data.confirmedMeetings != 'string' && checkRequired(response.Data.confirmedMeetings != undefined && response.Data.confirmedMeetings.length > 0)){
                        for(var i=0; i<response.Data.confirmedMeetings.length; i++){
                            if($scope.todayMeetings.length < 3){
                                var m = $scope.formatMeetingDetails(response.Data.confirmedMeetings[i],response.Data.userId)
                                $scope.todayMeetings.push(m)
                                $scope.todayMeetings.sort(function (o1, o2) {
                                    return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                });
                            }
                        }
                    }
                    if(typeof response.Data.pendingMeetings != 'string' && checkRequired(response.Data.pendingMeetings != undefined && response.Data.pendingMeetings.length > 0)){
                        var pendingMeetingsMeetingsSourceArr = [];
                        for(var j=0; j<response.Data.pendingMeetings.length; j++){
                            if($scope.newMeetings.length < 3){
                                $scope.newMeetings.push($scope.formatMeetingDetails(response.Data.pendingMeetings[j],response.Data.userId));
                                $scope.newMeetings.sort(function (o1, o2) {
                                    return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                });
                            }
                        }
                    }
                    if(typeof response.Data.upcomingMeetings != 'string' && checkRequired(response.Data.upcomingMeetings != undefined && response.Data.upcomingMeetings.length > 0)){
                        for(var k=0; k<response.Data.upcomingMeetings.length; k++){
                            //if($scope.upcomingMeetings.length < 5)
                            if(k < 4){
                                var mObj = $scope.formatMeetingDetails(response.Data.upcomingMeetings[k],response.Data.userId);
                                if($scope.upcomingMeetings[mObj.dateUpcoming]){
                                    $scope.upcomingMeetings[mObj.dateUpcoming].push(mObj);
                                    $scope.upcomingMeetings[mObj.dateUpcoming].sort(function (o1, o2) {
                                        return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                    });
                                }else $scope.upcomingMeetings[mObj.dateUpcoming] = [mObj]
                            }else break;
                        }
                    }
                    if(typeof response.Data.pendingTasks != 'string' && checkRequired(response.Data.pendingTasks != undefined && response.Data.pendingTasks.length > 0)){
                        for(var l=0; l<response.Data.pendingTasks.length; l++){
                            var obj = {
                                url:'/task/'+response.Data.pendingTasks[l]._id,
                                sortDate:new Date(response.Data.pendingTasks[l].dueDate)
                            };
                            obj.taskName = response.Data.pendingTasks[l].taskName || 'No Name'
                            if(response.Data.pendingTasks[l].assignedTo == response.Data.userId){
                                obj.id = response.Data.pendingTasks[l].createdBy;
                                obj.picUrl = '/getImage/'+response.Data.pendingTasks[l].createdBy || '/images/default.png';
                            }
                            else{
                                obj.id = response.Data.pendingTasks[l].assignedTo;
                                obj.picUrl = '/getImage/'+response.Data.pendingTasks[l].assignedTo || '/images/default.png';
                            }
                            if($scope.tasks[obj.id]){
                                $scope.tasks[obj.id].push(obj);
                                $scope.tasks[obj.id].sort(function (o1, o2) {
                                    return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                });
                            }else $scope.tasks[obj.id] = [obj];

                        }
                    }
                }
                if(typeof response.Data.pendingMeetings == 'string'){
                    $scope.noNewInvitationsMessage = response.Data.pendingMeetings;
                }
                if(typeof response.Data.pendingTasks == 'string'){
                    $scope.noTasksMessage = response.Data.pendingTasks;
                }
                if(typeof response.Data.confirmedMeetings == 'string'){
                    $scope.noConfirmedMeetingsMessage = response.Data.confirmedMeetings;
                }
                if(typeof response.Data.upcomingMeetings == 'string'){
                    $scope.noUpcomingMeetingsMessage = response.Data.upcomingMeetings;
                }
            }).error(function (data) {

            });
    }

    $scope.goToCalendar = function(){
        window.location = '/calendar'
    };

    $scope.goToTasks = function(){
        window.location = '/tasks'
    };

    $scope.goToUrl = function(url){
        if(url && url.length > 0 && url[0] && url[0][0] && url[0][0] != 'null' && url[0][0] != null ) {
            window.location = url[0][0];
        }
    };

    $scope.formatMeetingDetails = function(meeting,userId){
        var date = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].start.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].start.date);
        var obj = {
            sortDate:new Date(date.format()),
            start:date.format("hh:mm A"),
            end:checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].end.date).tz(timezone).format("hh:mm A") : moment(meeting.scheduleTimeSlots[0].end.date),
            title:meeting.scheduleTimeSlots[0].title,
            date:date.format("DD MMM YYYY"),
            dateUpcoming:date.format("MMM DD YYYY"),
            invitationId:meeting.invitationId,
            url:'/today/details/'+meeting.invitationId
        };

        if(meeting.suggested){
            if(meeting.suggestedBy.userId == userId){
                if(meeting.senderId == userId){
                    var data1 = $scope.getDetailsIfSender(meeting);
                    obj.picUrl = data1.picUrl;
                    obj.name = data1.name;
                }
                else{
                    var data3 = $scope.getDetailsIfReceiver(meeting);
                    obj.picUrl = data3.picUrl;
                    obj.name = data3.name;
                }
            }
            else{
                if(meeting.senderId == meeting.suggestedBy.userId){
                   var data5 = $scope.getDetailsIfReceiver(meeting)
                    obj.picUrl = data5.picUrl;
                    obj.name = data5.name;
                }
                else{
                    if(meeting.selfCalendar){
                        for(var j=0; j<meeting.toList.length; j++){
                            if(meeting.suggestedBy.userId == meeting.toList[j].receiverId){
                                obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                                var fName = meeting.toList[j].receiverFirstName || '';
                                var lName = meeting.toList[j].receiverLastName || '';
                                obj.name = fName+' '+lName;
                            }
                        }
                    }
                    else{
                        obj.picUrl = '/getImage/'+meeting.to.receiverId;
                        obj.name = meeting.to.receiverName;
                    }
                }
            }
        }
        else{
            if(meeting.senderId == userId){
                var data2 = $scope.getDetailsIfSender(meeting);
                obj.picUrl = data2.picUrl;
                obj.name = data2.name;
            }
            else{
                var data4 = $scope.getDetailsIfReceiver(meeting);
                obj.picUrl = data4.picUrl;
                obj.name = data4.name;
            }
        }

        return obj;
    };

    $scope.getDetailsIfSender = function(meeting){

        var obj = {};
        if(meeting.selfCalendar){

            if(meeting.toList.length > 0){

                var isDetailsExists = false;
                for(var i=0; i<meeting.toList.length; i++){
                    if(checkRequired(meeting.toList[i].receiverId)){

                        isDetailsExists = true;
                        obj.picUrl = '/getImage/'+meeting.toList[i].receiverId;
                        var fName = meeting.toList[i].receiverFirstName || '';
                        var lName = meeting.toList[i].receiverLastName || '';
                        obj.name = fName+' '+lName;
                    }
                }
                if(!isDetailsExists){

                    obj.picUrl = '/images/default.png';
                    obj.name = meeting.toList[0].receiverEmailId;
                }
            }
        }
        else{
            if(meeting.to){
                if(meeting.to.receiverId){
                    obj.picUrl = '/getImage/'+meeting.to.receiverId;
                    obj.name = meeting.to.receiverName;
                }
                else{
                    obj.picUrl = '/images/default.png';
                    obj.name = meeting.to.receiverEmailId;
                }
            }
        }

        return obj;
    };

    $scope.getDetailsIfReceiver = function(meeting){
        var obj = {};
        if(meeting.senderId){
            obj.picUrl = '/getImage/'+meeting.senderId;
            obj.name = meeting.senderName;
        }
        else{
            obj.picUrl = '/images/default.png';
            obj.name = meeting.senderEmailId;
        }
        return obj;
    }
});

reletasApp.controller("weather_and_today_coun", function ($scope, $http) {

    $http.get('/today/weather/forecast/web')
        .success(function (response) {
            if(response.SuccessCode){
                if(checkRequired(response.Data.city)){
                    $scope.city_country_top = response.Data.city.name+', '+response.Data.city.country;
                }
                if(response.Data.list != undefined && response.Data.list.length > 0){
                    function getDate(dt){
                        if(dt){
                            $scope.date_today_top = checkRequired(timezone) ? moment.unix(dt).tz(timezone).format('DD MMM YYYY') : moment.unix(dt).format('DD MMM YYYY')
                        }
                        else $scope.date_today_top = checkRequired(timezone) ? moment.unix().tz(timezone).format('DD MMM YYYY') : moment.unix().format('DD MMM YYYY')
                    }
                    getDate(response.Data.list[0].dt)

                    $scope.today_temp_min = response.Data.list[0].temp.min+'C';
                    $scope.today_temp_max = response.Data.list[0].temp.max+'C';
                }
            }
        }).error(function (data) {

        });

    $http.get('/today/top/section/info')
        .success(function (response) {
            if(response.SuccessCode){
               $scope.today_meeting_count_top = response.Data.todayMeetings || 0;
               $scope.today_tasks_count_top = response.Data.todayTasks || 0;
               $scope.image_top = '/images/app_images/drawable-xxxhdpi/'+response.Data.image;
            }
            else{

            }
        }).error(function (data) {

        })
});

reletasApp.controller("today_landing_top_counts", function ($scope, $http) {
    $scope.socialUpdatesSetupClass = 'hide';
    $http.get('/today/status/count')
        .success(function (response) {
            if(response.SuccessCode){
                $scope.losingTouchCount = response.Data.losingTouchCount || 0;
                $scope.unpreparedMeetingsCount = response.Data.unpreparedMeetingsCount || 0;
                $scope.emailResponsesPending = response.Data.emailResponsesPending || 0;
                $scope.emailAwaitingResponses = response.Data.emailAwaitingResponses || 0;
            }
            else{

            }
        }).error(function (data) {

        });

    $http.get('/today/social/updates/count')
        .success(function (response) {
            if(response.SuccessCode){
                $scope.socialUpdatesSetupClass = 'hide';
                $scope.socialUpdatesOpacity = '';
                if(typeof response.Data == 'number'){
                    $scope.socialUpdates = response.Data || 0;
                }
                else{
                    $scope.socialUpdatesOpacity = 'opacity: .3';
                    $scope.socialUpdatesSetupClass = '';
                }
            }
            else{
                $scope.socialUpdatesOpacity = 'opacity: .3';
                $scope.socialUpdatesSetupClass = '';
            }
        }).error(function (data) {

        })
});

reletasApp.controller("today_landing_agenda", function ($scope, $http) {
    $scope.agendaItems =[];
    $scope.arrowDirection = 'right';
    var dateAgenda = checkRequired(timezone) ? moment().tz(timezone).format() : moment().format();
    $scope.noAgendaFoundFlag = false;
    $scope.fetchAgenda = function(dateAgenda){
        $scope.agendaItems =[];
        $http.post('/today/agenda/section/web',{dateTime:dateAgenda})
            .success(function (response) {

                if(response.SuccessCode){
                    if(checkRequired(response.Data.meetings != undefined && response.Data.meetings.length > 0)){

                        for(var i=0; i<response.Data.meetings.length; i++){
                            $scope.agendaItems.push($scope.formatMeetingDetails(response.Data.meetings[i],response.Data.userId));
                        }

                        for(var j=0; j<response.Data.events.length; j++){
                            var dateStart = checkRequired(timezone) ? moment(response.Data.events[j].startDateTime).tz(timezone) : moment(response.Data.events[j].startDateTime);
                            var dateEnd = checkRequired(timezone) ? moment(response.Data.events[j].endDateTime).tz(timezone) : moment(response.Data.events[j].endDateTime);
                            var obj = {
                                sortDate:new Date(dateStart.format()),
                                time:dateStart.format("hh:mm A")+' - '+dateEnd.format("hh:mm A"),
                                title:response.Data.events[j].eventName || '',
                                location:response.Data.events[j].locationName || '',
                                eventId:response.Data.events[j]._id,
                                isAccepted:response.Data.events[j].accepted || false,
                                picUrl:'/getImage/'+response.Data.events[j].createdBy.userId,
                                url:'/event/'+response.Data.events[j]._id,
                                name:response.Data.events[j].createdBy.userName,
                                locationTypePic:"/images/icon_location_small.png",
                                preparedMeetingClass:'hide'
                            };

                            $scope.agendaItems.push(obj);
                        }

                        for(var l=0; l<response.Data.tasks.length; l++){
                            var tDate = checkRequired(timezone) ? moment(response.Data.tasks[l].dueDate).tz(timezone): moment(response.Data.tasks[l].dueDate);
                            var objT = {
                                sortDate:new Date(tDate.format()),
                                title:response.Data.tasks[l].taskName || 'No Name',
                                time:tDate.format("hh:mm A"),
                                location:'Task',
                                url:'/task/'+response.Data.tasks[l]._id,
                                locationTypePic:"/images/icon_location_small.png",
                                preparedMeetingClass:'hide',
                                taskId:response.Data.tasks[l]._id
                            };

                            if(response.Data.tasks[l].assignedTo._id == response.Data.userId){
                                objT.picUrl = '/getImage/'+response.Data.tasks[l].createdBy._id || '/images/default.png';
                                objT.name = response.Data.tasks[l].createdBy.firstName+' '+response.Data.tasks[l].createdBy.lastName;
                            }
                            else{
                                objT.picUrl = '/getImage/'+response.Data.tasks[l].assignedTo._id || '/images/default.png';
                                objT.name = response.Data.tasks[l].assignedTo.firstName+' '+response.Data.tasks[l].assignedTo.lastName;
                            }
                            $scope.agendaItems.push(objT);
                        }
                    }
                    if($scope.agendaItems.length > 0){
                        $scope.agendaItems.sort(function (o1, o2) {
                            return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                        });
                    }
                }
                else{
                    $scope.noAgendaFoundFlag = true;
                    $scope.noAgendaFound = response.Message;
                }
                var dateMoment = checkRequired(timezone) ? moment(dateAgenda).tz(timezone) : moment(dateAgenda);
                var now = checkRequired(timezone) ? moment().tz(timezone) : moment();
                if(dateMoment.format("DD MMM YYYY") == now.format("DD MMM YYYY")){
                    $scope.agendaTitle = "Today's Agenda ("+dateMoment.format("DD MMM YYYY")+")";
                }
                else $scope.agendaTitle = "Agenda For ("+dateMoment.format("DD MMM YYYY")+")";
            }).error(function (data) {

            });

    };

    $scope.fetchAgenda(dateAgenda);
    $scope.agendaNext = function(){
        $scope.noAgendaFoundFlag = false;
        if($scope.arrowDirection == 'right'){
            $scope.arrowDirection = 'left';
            var dateAgenda = checkRequired(timezone) ? moment().tz(timezone) : moment();
            dateAgenda.date(dateAgenda.date() + 1);
            $scope.fetchAgenda(dateAgenda.format());
        }
        else{
            $scope.arrowDirection = 'right';
            $scope.fetchAgenda(checkRequired(timezone) ? moment().tz(timezone).format() : moment().format());
        }
    };

    $scope.formatMeetingDetails = function(meeting,userId){
        var date = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].start.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].start.date);
        var end = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].end.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].end.date);
        var obj = {
            sortDate:new Date(date.format()),
            time:date.format("hh:mm A")+' - '+end.format("hh:mm A"),
            title:meeting.scheduleTimeSlots[0].title || '',
            location:meeting.scheduleTimeSlots[0].location || '',
            locationType:meeting.scheduleTimeSlots[0].locationType || '',
            date:date.format("DD MMM YYYY"),
            dateUpcoming:date.format("MMM DD YYYY"),
            invitationId:meeting.invitationId,
            url:'/today/details/'+meeting.invitationId,
            isPreparedMeeting:$scope.isPreparedMeeting(meeting,userId),
            isSender:false,
            isAccepted:meeting.scheduleTimeSlots[0].isAccepted || false,
            locationTypePic:$scope.getLocationTypePic(meeting.scheduleTimeSlots[0].locationType)
        };
        obj.preparedMeetingClass = obj.isPreparedMeeting ? 'hide' :'';
        obj.preparedMeetingUrl = obj.isPreparedMeeting ? null : '/meeting/'+meeting.invitationId+'/prepare/'+userId;

        if(meeting.suggested){
            obj.location = checkRequired(meeting.scheduleTimeSlots[0].suggestedLocation) ? meeting.scheduleTimeSlots[0].suggestedLocation : meeting.scheduleTimeSlots[0].location;
        }

        if(meeting.suggested){
            if(meeting.suggestedBy.userId == userId){
                obj.isSender = true;
                if(meeting.senderId == userId){
                    var data1 = $scope.getDetailsIfSender(meeting);
                    obj.picUrl = data1.picUrl;
                    obj.name = data1.name;
                }
                else{
                    var data3 = $scope.getDetailsIfReceiver(meeting);
                    obj.picUrl = data3.picUrl;
                    obj.name = data3.name;
                }
            }
            else{
                if(meeting.senderId == meeting.suggestedBy.userId){
                    obj.isSender = true;
                    var data5 = $scope.getDetailsIfReceiver(meeting)
                    obj.picUrl = data5.picUrl;
                    obj.name = data5.name;
                }
                else{
                    if(meeting.selfCalendar){
                        for(var j=0; j<meeting.toList.length; j++){
                            if(meeting.suggestedBy.userId == meeting.toList[j].receiverId){
                                obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                                var fName = meeting.toList[j].receiverFirstName || '';
                                var lName = meeting.toList[j].receiverLastName || '';
                                obj.name = fName+' '+lName;
                            }
                        }
                    }
                    else{
                        obj.picUrl = '/getImage/'+meeting.to.receiverId;
                        obj.name = meeting.to.receiverName;
                    }
                }
            }
        }
        else{
            if(meeting.senderId == userId){
                obj.isSender = true;
                var data2 = $scope.getDetailsIfSender(meeting);
                obj.picUrl = data2.picUrl;
                obj.name = data2.name;
            }
            else{
                var data4 = $scope.getDetailsIfReceiver(meeting);
                obj.picUrl = data4.picUrl;
                obj.name = data4.name;
            }
        }

        return obj;
    };

    $scope.getDetailsIfSender = function(meeting){

        var obj = {};
        if(meeting.selfCalendar == true){
            if(meeting.toList.length > 0){
                var isDetailsExists = false;
                for(var i=0; i<meeting.toList.length; i++){
                    if(checkRequired(meeting.toList[i].receiverId)){
                        isDetailsExists = true;
                        obj.picUrl = '/getImage/'+meeting.toList[i].receiverId;
                        var fName = meeting.toList[i].receiverFirstName || '';
                        var lName = meeting.toList[i].receiverLastName || '';
                        obj.name = fName+' '+lName;
                    }
                }
                if(!isDetailsExists){
                    obj.picUrl = '/images/default.png';
                    obj.name = meeting.toList[0].receiverEmailId;
                }
            }
        }
        else{
            if(meeting.to){
                if(meeting.to.receiverId){
                    obj.picUrl = '/getImage/'+meeting.to.receiverId;
                    obj.name = meeting.to.receiverName;
                }
                else{
                    obj.picUrl = '/images/default.png';
                    obj.name = meeting.to.receiverEmailId;
                }
            }
        }
        return obj;
    };

    $scope.getDetailsIfReceiver = function(meeting){
        var obj = {};
        if(meeting.senderId){
            obj.picUrl = '/getImage/'+meeting.senderId;
            obj.name = meeting.senderName;
        }
        else{
            obj.picUrl = '/images/default.png';
            obj.name = meeting.senderEmailId;
        }
        return obj;
    };

    $scope.isPreparedMeeting = function(meeting,userId){

        if(meeting.senderId == userId){
           return meeting.isSenderPrepared;
        }
        else if(meeting.selfCalendar){
            var isExists = false;
            var prepared = true;
            for(var j=0; j<meeting.toList.length; j++){
                if(meeting.toList[j].receiverId == userId){
                    isExists = true;
                    prepared = meeting.toList[j].isPrepared || false;
                }
            }
           return prepared;
        }
        else{
            if(meeting.to.receiverId == userId){
                return meeting.to.isPrepared || false;
            }else return true;
        }
    };

    $scope.getLocationTypePic = function(meetingLocationType){
        switch (meetingLocationType) {
            case 'In-Person':
                return "/images/icon_location_small.png";
                break;
            case 'Phone':
               return "/images/icon_phone.png";
                break;
            case 'Skype':
               return "/images/icon_skype.png";
                break;
            case 'Conference Call':
               return "/images/conference_call.png";
                break;
            default:return "/images/icon_location_small.png"
                break;
        }
    };

    $scope.goToUrl = function(url){
        if(url && url.length > 0 && url[0] && url[0][0] && url[0][0] != 'null' && url[0][0] != null ) {
            window.location = url[0][0];
        }
    };

    $scope.prepareMeeting = function(url){
       if(checkRequired(url)){
           window.location = url;
       }
    }
});

var start = 1;

var itemsScroll = 1;
var itemsToShow = 6;
var end = itemsToShow;

if(window.innerWidth < 480){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 1
}else if(window.innerWidth < 900){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 3
}

//var html = '';
reletasApp.controller("traveling_location", function ($scope, $http) {
    $scope.cConnections = [];
    $scope.cConnectionsIds = [];
    $scope.currentLength = 0;
    $scope.isFirstTime = true;
    $scope.isConnectionOpen = false;
    $scope.start = start;
    $scope.end = end;
    $scope.isAllConnectionsLoaded = false;

    $scope.isConnectionOpen = true;
    var url = '/today/traveling/location?skip=0&limit=12';
    getConnections($scope, $http, url, true);

    $scope.NextConn = function(){
        var statusNext =  $scope.statusNext;
        var statusPrev =  $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.end < total){
            var itemsToStayPrev = $scope.start+1;
            var itemsToStayNext = $scope.end+1;
            for(var i=0; i<$scope.cConnectionsIds.length; i++){
                /// if(i+1 > statusNext && i+1 <= (statusNext+itemsScroll)){
                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    addHide($scope.cConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end+1;
            $scope.start = $scope.start+1;

            $scope.statusNext = statusNext+itemsScroll;
            $scope.statusPrev = statusNext;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
        }
        $scope.showHideArrows();
        if($scope.fetchCommonConnectionsNext() && $scope.isAllConnectionsLoaded == false && $scope.isConnectionOpen == false){
            $scope.isConnectionOpen = true;
            if(checkRequired($scope.dataEntered)){
                var url1 = '/today/traveling/location?skip='+$scope.currentLength+'&limit=12&location='+$scope.dataEntered.location+'&designation='+$scope.dataEntered.designation;
                getConnections($scope, $http, url1, false);
            }
            else{
                var url2 = '/today/traveling/location?skip='+$scope.currentLength+'&limit=12';
                getConnections($scope, $http, url2, false);
            }
        }
    };

    $scope.searchTraveling = function(){
        $scope.dataEntered = getLocationAndTitle();
        if(checkRequired($scope.dataEntered.location)){
            var url = '/today/traveling/location?skip=0&limit=12&location='+$scope.dataEntered.location+'&designation='+$scope.dataEntered.designation;
            getConnections($scope, $http, url, true);
        }
    };

    $scope.fetchCommonConnectionsNext = function(){
        if($scope.end >= $scope.cConnectionsIds.length - 12){
            return true;
        }else return false;
    };

    $scope.PrevConn = function(){

        var statusPrev = $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.start >1){
            var itemsToStayPrev = $scope.start-1;
            var itemsToStayNext = $scope.end-1;

            for(var i=0; i<$scope.cConnectionsIds.length; i++){
                //if(i+1 <= statusPrev && i+1 > (statusPrev-itemsScroll)){
                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    addHide($scope.cConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end-1;
            $scope.start = $scope.start-1;

            $scope.statusNext = statusPrev;
            $scope.statusPrev = statusPrev-itemsScroll+1;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
            $scope.showHideArrows()
        }
    };

    $scope.showHideArrows = function(){
        if($scope.grandTotal >0){
            if($scope.start <=1 ){
                addHide("als-prev",true)
            }
            else addHide("als-prev",false)

            if($scope.end >= $scope.grandTotal){
                addHide("als-next",true)
            }
            else addHide("als-next",false)
        }
        else{
            addHide("als-prev",true)
            addHide("als-next",true)
        }
    };

    $scope.connClick = function(url){
        if(url && url.length > 0 && url[0] && url[0][0] && url[0][0] != 'null' && url[0][0] != null ) {
           // trackMixpanel("clicked on common connection",url[0][0],function(){
                window.location.replace(url[0][0]);
           // })
        }
    }
});

function getLocationAndTitle(){
    return {location:$("#locationEntered").val(),designation:$("#designationEntered").val()}
}

function addHide(elementId,addHide){
    addHide ? $("#"+elementId).addClass('hide') : $("#"+elementId).removeClass('hide')
    //addHide ? $("#"+elementId).css({display:'none'}) : $("#"+elementId).css({display:'block'})
}

function getConnections($scope, $http, url, isHides){
    if(isHides){
        $scope.cConnections = [];
        $scope.cConnectionsIds = [];
    }
    $http.get(url)
        .success(function (response) {
            if(response.SuccessCode == 0){
                $scope.location_traveling = response.Data.location || ''
                $scope.noConnectionsFoundFlag = true;
                $scope.noConnectionsFoundForSearch = response.Message;
            }else $scope.noConnectionsFoundFlag = false;
            $scope.isConnectionOpen = false;
            if(response.SuccessCode){
                $scope.grandTotal = response.Data.total;
                $scope.location_traveling = response.Data.location || ''
                response.Data = response.Data.contacts;
                if(response.Data && response.Data.length > 0){
                    var html = '';
                    hidePrevNext(true);
                    if(isHides){
                        $scope.currentLength = response.Data.length;
                        $scope.cConnections = [];
                        $scope.cConnectionsIds = [];
                    }
                    else $scope.currentLength += response.Data.length;

                    for(var i=0; i<response.Data.length; i++){
                        var li = '';
                        var obj = {name:''};
                        if(checkRequired(response.Data[i].firstName) && response.Data[i].firstName.length > 0){
                            response.Data[i].firstName.forEach(function(name){
                                if(checkRequired(name)){
                                   obj.name = name
                                }
                            })
                        }
                        if(checkRequired(response.Data[i].lastName) && response.Data[i].lastName.length > 0){
                            response.Data[i].lastName.forEach(function(name){
                                if(checkRequired(name)){
                                    obj.name += ' '+name
                                }
                            })
                        }
                        if(!checkRequired(obj.name) && response.Data[i]._id.emailId){
                            obj.name = getTextLength(response.Data[i]._id.emailId,13);
                        }
                        else if(!checkRequired(obj.name) && response.Data[i]._id.mobileNumber){
                            obj.name = getTextLength(response.Data[i]._id.mobileNumber,13)
                        }

                        if(checkRequired(obj.name)){
                            obj.fullName = obj.name
                            obj.name = getTextLength(obj.name,13);
                        }
                        if(checkRequired(response.Data[i].userId) && response.Data[i].userId.length > 0){
                            response.Data[i].userId.forEach(function(id){
                                if(checkRequired(id)){
                                    obj.image = '/getImage/'+id;
                                }
                            })
                        }
                        if(!checkRequired(obj.image)){
                            obj.cursor = 'cursor:default';
                            obj.image = '/images/default.png';
                        }else obj.cursor = 'cursor:pointer';

                        if(checkRequired(response.Data[i].publicProfileUrl) && response.Data[i].publicProfileUrl.length > 0){
                            response.Data[i].publicProfileUrl.forEach(function(id){
                                if(checkRequired(id)){
                                    if(id.charAt(0) != 'h'){
                                        obj.url = '/'+id;
                                    }
                                }
                            })
                        }
                        if(!checkRequired(obj.url)){
                            obj.url = null;
                        }
                        obj.interactionsCount = response.Data[i].count || 0;
                        obj.idName = 'com_con_item_'+response.Data[i].lastInteracted
                        obj.className = 'hide';

                        if(isHides && i<itemsToShow){
                            obj.className = ''
                        }
                        $scope.cConnections.push(obj);
                        $scope.cConnectionsIds.push('com_con_item_'+response.Data[i].lastInteracted);

                    }
                    if(isHides){
                        $scope.statusPrev = 1;
                        $scope.statusNext = itemsScroll;
                    }
                    $scope.showHideArrows()
                }
                else if(isHides){
                    hidePrevNext(true)
                   // commonConnectionsMessage(false)
                    $scope.commonConnectionsNotExist = response.Message;
                }else $scope.isAllConnectionsLoaded = true;
            }
            else if(isHides){
                hidePrevNext(true)
                //commonConnectionsMessage(false)
                $scope.commonConnectionsNotExist = response.Message;
            }else $scope.isAllConnectionsLoaded = true;
        })
}

function hidePrevNext(hide){
    if(hide){
        $(".als-prev").addClass('hide')
        $(".als-next").addClass('hide')
    }
    else{
        $(".als-prev").removeClass('hide')
        $(".als-next").removeClass('hide')
    }
}

/*
reletasApprouter.controller("loginPopup_controller", function ($scope, $http) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");

    $scope.login_with_google = '/authenticate/google/web?onSuccess='+uName+'&onFailure='+uName+'&action=login_google';
    $scope.login_with_outlook = '/authenticate/office365Auth/new?onSuccess='+uName+'&onFailure='+uName+'&action=login_outlook';
});

reletasApprouter.controller("toastr_msg_controller", function ($scope, $http) {
    $http.get('/schedule/get/message')
        .success(function (response) {
            removeSession();

            if (response.Data && response.Data.message && !response.Data.red) {

                if (response.Data.type == 'error') {
                    toastr.error(response.Data.message)
                }
                else toastr.success(response.Data.message);
            }
        });
    function removeSession(){
        $http.delete('/schedule/remove/message')
            .success(function (response) {

            });
    }
});*/

function getTextLength(text,maxLength){

    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

function changeTextById(id,text){
    $("#"+id).html(text);
}
