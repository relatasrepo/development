(function() {

    var jQuery;
    var personId = [];
    var messagesGlobal = { meetingTitle: null,failureReply: null,successReply: null,welcomeTitle: null,mailSubject: null};
    var colorSchemeGlobal = { majorColor:null, textColor:null};
    var mailOutgoingBody = "Thanks for reaching out to us. We will contact back shortly."
    var cssColors = {}
    var cssNone = {
        "background-color": "transparent",
        "outline": "none",
        "border-bottom":"1px solid rgba(204, 204, 204, 0.66)",
        "width": "100%"
    }

    // var relatasBaseUrl = "https://relatas.com";
    // var relatasBaseUrl = "https://showcase.relatas.com";
    var relatasBaseUrl = "http://localhost:7000";

    /******** Load jQuery if not present *********/
    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '3.1.1') {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type","text/javascript");
        script_tag.setAttribute("src",
            "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js");
        if (script_tag.readyState) {
            script_tag.onreadystatechange = function () { // For old versions of IE
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    scriptLoadHandler();
                }
            };
        } else {
            script_tag.onload = scriptLoadHandler;
        }
        // Try to find the head, otherwise default to the documentElement
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;

        window.jQuery.getScript(relatasBaseUrl+'/javascripts/datePicker/jquery-ui.min.js', function() {
            window.jQuery.getScript(relatasBaseUrl+'/javascripts/datePicker/moment.js', function() {
                window.jQuery.getScript(relatasBaseUrl+'/javascripts/datePicker/moment-timezone.js', function() {
                    main();
                });
            });
            // main();
        });
        // main();
    }

    /******** Called once jQuery has loaded ******/
    function scriptLoadHandler() {
        // Restore $ and window.jQuery to their previous values and store the
        // new jQuery in our local jQuery variable
        // Call our main function
        // main();

        window.jQuery.getScript(relatasBaseUrl+'/javascripts/datePicker/jquery-ui.min.js', function() {
            window.jQuery.getScript(relatasBaseUrl+'/javascripts/datePicker/moment.js', function() {
                window.jQuery.getScript(relatasBaseUrl+'/javascripts/datePicker/moment-timezone.js', function() {
                    jQuery = window.jQuery.noConflict(true);
                    main();
                });
            });
        });
    }

    function main() {

        jQuery(document).ready(function($) {

            var css_link = $("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: relatasBaseUrl+"/rWebLead/style.css"
            });

            css_link.appendTo('head');

            var jdt_link = $("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: relatasBaseUrl+"/javascripts/datePicker/jquery-ui.theme.min.css"
            });

            jdt_link.appendTo('head');

            var jdt2_link = $("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: relatasBaseUrl+"/javascripts/datePicker/jquery-ui.min.css"
            });

            jdt2_link.appendTo('head');

            var fa_link = $("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: "//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
            });
            fa_link.appendTo('head');

            /******* Load HTML *******/

            var this_js_script = $('script[src*=load]');
            var companyId = this_js_script.attr('data-relatas_company_id');

            function loadHTML(type) {
                var jsonp_url = relatasBaseUrl+"/rweblead/fetch/formTemplate?companyId="+companyId+"&callback=?";

                jsonp_url = encodeURI(jsonp_url)

                console.log(jsonp_url)

                $.getJSON(jsonp_url, function(data) {

                    colorSchemeGlobal = { majorColor:data.colorScheme.majorColor, textColor:data.colorScheme.textColor};
                    cssColors = { "background":colorSchemeGlobal.majorColor, "color":colorSchemeGlobal.textColor?colorSchemeGlobal.textColor:"#fff"}
                    messagesGlobal = { meetingTitle: data.messages.meetingTitle,failureReply: data.messages.failureReply,successReply: data.messages.successReply,welcomeTitle: data.messages.welcomeTitle,mailSubject: data.messages.mailSubject}
                    mailOutgoingBody = data.mailOutgoingBody?data.mailOutgoingBody:mailOutgoingBody

                    renderForm(data,type,$,function (result) {

                        $("#relatas-header").css(cssColors);
                        $(".relatas-schedule-meeting").css({
                            "color":colorSchemeGlobal.majorColor
                        });
                        $(".btn").css(cssColors);
                        $(".relatas-open-btn").css(cssColors);
                        $(".relatas-close-btn").css(cssColors);
                        $(".relatas-quick-send").css({
                            "color":colorSchemeGlobal.majorColor
                        });

                        getSupportTeam(companyId,$);
                        beautifyHeader($);
                    })
                });
            }

            loadHTML();

            $("body").on('click', '.relatas-contact-card', function (e) {
                loadHTML('fullForm')
            });

            $("body").on('click', '.relatas-quick-send', function (e) {
                var template = getFormWithOnlyMessageAndEmailId();
                $('.relatas-main').hide().html(template).fadeIn('slow');
                $(".btn").css(cssColors);
            });

            $("body").on('click', '.relatas-open-btn', function (e) {
                $("#relatas-body").css('visibility','visible').animate(1200);
                $(".relatas-open-btn").toggle();
                // $(".relatas-close-btn").fadeToggle(700);
                $(".relatas-close-btn").toggle();
                $(".btn").css(cssColors);
            });

            $("body").on('click', '.relatas-close-btn', function (e) {
                $("#relatas-body").css('visibility','hidden').animate(1200);
                $(".relatas-open-btn").toggle();
                $(".relatas-close-btn").toggle();

                resetFormFields($)
            });

            $("body").on('click', '#nextDetails', function (e) {

                var name = '<div class="relatas-no-name required"><label>Name</label>'+'<input placeholder="Please enter your full name" name="leadName" id="leadName"></div>'
                var emailId = '<p class="relatas-no-emailId required"><label>Email ID</label>'+'<input placeholder="Please enter your Email ID" name="leadEmailId" id="leadEmailId"></p>'
                var prevMsg = '<p class="relatas-prev-msg">'+$("#leadComment").val()+'</p>'
                var message = '<p class="relatas-msg-error">Please enter a valid email ID to send this query.</p>'
                // var goback = '<div id="prevDetails" class="btn"><i class="fa fa-arrow-left"></i> Go Back</div>'
                var goback = '<div id="sendEmail" class="btn">SEND</div>'
                var template = prevMsg+'<div class="relatas-result-replace">'+message+name+emailId+'</div>'+goback;

                var comment = $("#leadComment").val()

                if(comment && (comment !== "" || comment !== " ")){
                    $('.relatas-form').hide().html(template).fadeIn('slow');
                } else {
                    $(".relatas-message-wrapper").addClass('relatas-error-empty-form')
                }

                $(".btn").css(cssColors);

            });

            $("body").on('focus', '#relatas-body .relatas-form input', function (e) {
                onFocus('#relatas-body .relatas-form input',$)
            })

            $("body").on('focus', '#relatas-body .relatas-form textarea', function (e) {
                onFocus('#relatas-body .relatas-form textarea',$)
            })

            $("body").on('click', '#nextMeetingDetails', function (e) {

                var emailId = '<div class="relatas-no-emailId required replace"><label>Email ID</label>'+'<input placeholder="Please enter your email ID" name="leadEmailIdMeeting" id="leadEmailIdMeeting"></div>'
                var name = '<div class="relatas-no-name required"><label>Name</label>'+'<input placeholder="Please enter your full name" name="leadNameMeeting" id="leadNameMeeting"></div>'
                var prevMsg = '<p class="relatas-prev-msg">'+$("#leadComment").val()+'</p>'
                var message = '<p class="relatas-msg-error" style="text-align: center;"><strong style="color:#ff4b00">Invite not sent.</strong><br>Please enter a valid email ID to send this invite.</p>'
                // var goback = '<div id="prevDetails" class="btn"><i class="fa fa-arrow-left"></i> Go Back</div>'
                var schedule = '<div id="sendMeeting" class="btn">SCHEDULE</div>'
                var meetingTimings = null;
                var meetingAgenda = null;
                var locationDetails = null;

                var timeSelected = $("#selectedStartTime option:selected").val();

                if(timeSelected){

                    var startObj = new Date($("#startDate").val())
                    var time = timeSelected.split(":")
                    var hrs = time[0];
                    var min = time[1].split(" ")
                    var amPm = min;
                    if(amPm[1] == 'PM'){
                        hrs = Number(hrs)
                        hrs = hrs+12
                    }

                    min = min[0];

                    startObj.setHours(Number(hrs))
                    startObj.setMinutes(Number(min))
                    var startObjCopy = new Date(startObj.valueOf())
                    var endObj = startObjCopy.setMinutes(startObjCopy.getMinutes() + 30);
                    endObj = new Date(endObj);

                    $("#endDate").html(formatDate2(endObj));
                    $("#startTime").hide();

                    meetingTimings = "<p>Start: "+formatDate2(startObj)+"<span id='relatas-startTime'>"+startObj+"</span>"+
                        "<br>End:"+formatDate2(endObj)+"<span id='relatas-endTime'>"+endObj+"</span>"+
                        "</p>";
                }

                if($("#meetingAgenda").val()){
                    meetingAgenda = "<p>Agenda: "+"<span id='relatas-agenda'>"+$("#meetingAgenda").val()+"</span>"+"</p>";
                }
                
                if($("#locationType option:selected").val() && $("#locationDetails").val()){
                    var locationType = $("#locationType option:selected").val();
                    var locDetails = {}
                    if(locationType == "Skype"){
                        locDetails = {
                            icon:"fa fa-skype",
                            data:$("#locationDetails").val()
                        }
                    } else {
                        locDetails = {
                            icon:"fa fa-phone",
                            data:$("#locationDetails").val()
                        }
                    }
                    locationDetails = "<p><span id='relatas-locationType'>"+$("#locationType option:selected").val()+"</span>"+": "+"<span id='relatas-locationDetails'>"+$("#locationDetails").val()+"</span>"+"</p>";
                }
                var timeObj = formatDate3(startObj).time +"-"+formatDate3(endObj).time;

                var timezoneSelected = '<span class="timezones">'+$("#timezones option:selected").val()+'</span>'

                var calendarTemplate = getCalendarIcon(formatDate3(startObj).date,timeObj,locDetails,colorSchemeGlobal,$("#timezones option:selected").val())

                var hidden = '<div style="display: none;">'+meetingAgenda+meetingTimings+locationDetails+timezoneSelected+'</div>'
                var template = calendarTemplate+message+name+emailId+schedule+hidden;

                if(!meetingAgenda){
                    $('.relatas-error-highlight-agenda').addClass('relatas-error-empty-form')
                } else if(!meetingTimings){
                    $('.relatas-error-highlight-times').addClass('relatas-error-empty-form')
                } else if(!locationDetails){
                    $('.relatas-error-highlight-meetingtype').addClass('relatas-error-empty-form')
                }

                if(meetingAgenda && meetingTimings && locationDetails ){
                    $('.relatas-form').hide().html(template).fadeIn('slow');
                    $(".btn").css(cssColors);
                }

            });

            $("body").on('click', '#prevDetails', function (e) {
                var template = getFormWithOnlyMessageAndEmailId();
                $('.relatas-main').hide().html(template).fadeIn('slow')
                $(".btn").css(cssColors);
            });

            $("body").on('click', '#sendEmail', function (e) {

                var message = mailOutgoingBody+
                    '\n\n---------------------------------------------------------------------------\n\n'+
                    $(".relatas-prev-msg").text();
                var rEmailId = $("#leadEmailId").val();
                var rName = $("#leadName").val()
                var subject = messagesGlobal.mailSubject

                message = 'Hi '+rName+',\n'+message;

                if(rName && validateEmail(rEmailId)){
                    sendEmail(message,rEmailId,subject,personId[0]._id,rName,$)
                } else if(rName){
                    $(".relatas-no-emailId").addClass('relatas-error-empty-form')
                } else {
                    $(".relatas-no-name").addClass('relatas-error-empty-form')
                }
            });

            $("body").on('click', '#sendMeeting', function (e) {
                var meetingDetails = {}
                var senderName = $("#leadNameMeeting").val();
                var senderEmailId = $("#leadEmailIdMeeting").val();

                var sender = {
                    "receiverId": null,
                    "receiverFirstName": senderName,
                    "receiverLastName": '',
                    "receiverEmailId": senderEmailId
                }

                if(senderName && senderEmailId && validateEmail(senderEmailId)){
                    meetingDetails.userId = personId[0]._id
                    meetingDetails["meetingDetails"] = {
                        "selfCalendar":true,
                        "publicProfileUrl":personId[0].publicProfileUrl,
                        "start":$("#relatas-startTime").text(),
                        "end":$("#relatas-endTime").text(),
                        "title":messagesGlobal.meetingTitle,
                        "locationType":$("#relatas-locationType").text(),
                        "location":$("#relatas-locationDetails").text(),
                        "description":$("#relatas-agenda").text(),
                        "toList":[sender],
                        "senderEmailId":senderEmailId,
                        "senderFirstName":senderName,
                        "senderLastName":"",
                        "senderSelectedTimezone":$(".timezones").text()
                    };
                    
                    scheduleMeeting(meetingDetails,$)
                }else {

                    if(!senderName){

                    } else {
                        $(".relatas-no-emailId").addClass('relatas-error-empty-form')
                        if(!validateEmail(senderEmailId)){
                            $(".relatas-msg-error").html("Please enter a valid Email ID")
                        }
                    }
                }
            });

            $("body").on('click', '.relatas-schedule-meeting', function (e) {
                var template = getCalendarForm();
                $('.relatas-main').hide().html(template).fadeIn('slow',function () {
                    
                    $(".btn").css(cssColors);
                    getDatePicker($)
                    // setTimeout(function () {
                    // },500)
                });
            });

            $("body").on('change', '#locationType', function (e) {
                var placeholder = "Enter Skype handle"
                if(this.value == "Phone"){
                    placeholder = "Country code - phone"
                }
                $("#locationDetails").attr("placeholder", placeholder);
            });

            $("body").on('change', '#timezones', function (e) {
            var user = personId[0];
            var selectedDay = $("#startDate").val()

            var selectedTz = $("#timezones").val()
            selectedDay = formatDate(selectedDay)
            $("#timeWrapper").html('<select id="selectedStartTime"><option>Fetching slots</option></select>')

            getOpenSlots(user._id,selectedTz,30,14,selectedDay,$,function (response) {
                var selectOptions = '<select id="selectedStartTime">'+response.html+'</select>'
                if(response.SuccessCode){
                    $("#timeWrapper").html(selectOptions);
                    $("#dd-arrow").hide();
                    $("#allowedTimes").hide();
                }
            });
        })

            $("body").on('keyup change', 'textarea', "#meetingAgenda", function (e) {
                $(".relatas-error-highlight-agenda").removeClass("relatas-error-empty-form");
                $(this).css(cssNone);
            })

            $("body").on('keyup change', 'input', "#locationDetails", function (e) {
                $(".relatas-error-highlight-meetingtype").removeClass("relatas-error-empty-form");
                $(this).css(cssNone);
            })

            $("body").on('keyup change', 'input', "#leadNameMeeting", function (e) {
                $(".relatas-no-name").removeClass("relatas-error-empty-form");
                $(this).css(cssNone);
            })

        });

        function getDatePicker($){
            $('#startDate').datepicker({
                minDate: 0,
                onSelect: function(dateText) {

                    var user = personId[0];
                    var selectedDay = dateText

                    $("#timeWrapper").html('<select id="selectedStartTime"><option>Fetching slots</option></select>')

                    var defaultTz = $("#timezones").val()
                    selectedDay = formatDate(selectedDay)

                    getOpenSlots(user._id,defaultTz,30,14,selectedDay,$,function (response) {
                        var selectOptions = '<select id="selectedStartTime">'+response.html+'</select>'
                        if(response.SuccessCode){
                            $("#timeWrapper").html(selectOptions);
                            $("#dd-arrow").hide();
                            $("#allowedTimes").hide();
                        }
                    });

                    $(".relatas-date-picker-row").removeClass("relatas-error-empty-form");
                    $(this).css(cssNone);
                }
            });
        }

        function getOpenSlots(personId,timezone,duration,daysDuration,selectedDay,$,callback){
            var url = relatasBaseUrl+'/rweblead/schedule/slots/available?id='+personId+'&slotDuration='+duration+'&daysDuration='+daysDuration+'&timezone='+timezone+'&withSlotDates=yes&selectedDay='+selectedDay;

            $.get(url,function (response) {
                // var start = response.start
                // var end = response.end
                // var givenDate = moment(selectedDay).tz(timezone);
                // var calendarStartTime = givenDate.clone();
                // var calendarEndTime = givenDate.clone();
                //
                // calendarStartTime.hours(start)
                // calendarStartTime.minutes(0)
                // calendarStartTime.seconds(0)
                //
                // calendarEndTime.hours(end)
                // calendarEndTime.minutes()
                // calendarEndTime.seconds(0)
                //
                // var option = '';

                // for(var i = 0;i<response.slots;i++){
                //     if(!slot.isBlocked && new Date(slot.start)>new Date(calendarStartTime) && new Date(slot.start)<new Date(calendarEndTime)){
                //         option = option+'<option>'+formatToAmPm(new Date(slot.start))+'</option>'
                //         openSlots.push(formatToAmPm(new Date(slot.start)))
                //     } else {
                //         option = option+'<option disabled>'+formatToAmPm(new Date(slot.start))+'</option>'
                //     }
                // }

                callback(response)
            });
        }

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [month, day, year].join('-');
        }

        function getSupportTeam(companyId,$) {
            $.get( relatasBaseUrl+"/rweblead/fetch/support/team?companyId="+companyId, function( data ) {
                var imgs = '';
                for(var i=0;i<data.length;i++){
                    var obj = {
                        image:relatasBaseUrl+"/getImage/"+data[i]._id,
                        // name: data[i].firstName+' '+data[i].lastName,
                        name: data[i].firstName,
                        designation:data[i].designation,
                        mobileNumber:data[i].mobileNumber
                    }

                    personId = data

                    var li = '<li>'+
                        '<img src=" '+obj.image+'">'+
                        '<p>'+obj.name+'<br>'+'<span>'+obj.designation+'</span>'+'</p>'+
                        '</li>';
                    imgs = imgs+li
                }

                var template = '<ul>'+imgs+'</ul>'

                $('#team-support').hide().html(template).fadeIn('slow');
            });
        }

        function renderForm(data,type,$,callback) {
            var template = getBody(data,type)+
                getOpenButton();

            $('#relatas-weblead-container').hide().html(template).fadeIn('slow');
            callback(true)
        }

        function getBody(data,type) {
            var form = data.onlyMessageAndEmailIdForm
            if(type && type === 'fullForm'){
                form = data.fullForm
            }

            return '<div id="relatas-body">'+data.header+form+getFooter()+'</div>'
        }

        function beautifyHeader($) {

            var numberOfLinks = $('#relatas-weblead-container ul li').length;
            var colors = ["1abc9c", "2ecc71", "3498db", "34495e"];

            $('#relatas-weblead-container li').each(function(i) {
                // $(this).css('color', '#'+colors[i % colors.length]);
                $(this).css('width',100/numberOfLinks+'%')
            });
        }

        function getOpenButton() {
            return '<i class="fa fa-comments-o relatas-open-btn"></i><span class="relatas-close-btn">X</span>'
        }

        function getFooter() {
            return '<div class="footer">' +
                '<a href="https://relatas.com/" target="_blank">Powered by <img src="https://showcase.relatas.com/landing/img/rel_logo_GSv2.jpg"></a></div>'
        }

        function getFormWithOnlyMessageAndEmailId() {
            var emailId = '<label>Email ID</l>'+'<input placeholder="Please enter your Email ID" name="leadEmailId" id="leadEmailId">'
            var message = '<div class="relatas-message-wrapper required"><label id="commentLabel">Message</label>'+'<textarea placeholder="Please enter your query" name="leadComment" id="leadComment" class="relatas-short-form"></textarea></div>'
            var submitBtn = '<div id="nextDetails" class="btn">SEND</div>'

            return '<div class="relatas-form">'+message+submitBtn+'</div>'
        }

        function getCalendarForm() {
            var name = '<label>Meeting Title</label>'+'<input placeholder="Your name" name="leadName" id="leadName">'
            var meetingType = '<label>Meeting Type</label>'+
                '<select id="locationType"> ' +
                '<option value="Skype">Skype</option>' +
                '<option value="Phone">Phone</option>' +
                // '<option value="In-person">In-Person</option>' +
                '</select>'
            var meetingLocation = '<span class="required" style="margin-left: 25px;"><input placeholder="Skype handle" name="locationDetails" id="locationDetails"></span>'
            var message = '<span class="required"><label>Agenda</label><textarea placeholder="Meeting description" name="meetingAgenda" id="meetingAgenda"></textarea></span>'
            var startTime = '<div class="relatas-date-picker-row relatas-error-highlight-times"><span class="required"><label>Start</label>'+'<input placeholder="Meeting Date" id="startDate" type="text"></span>' +
                '<span id="timeWrapper"><span id="dd-arrow"><input placeholder="Meeting Time" id="startTime" type="text"></span></span><span id="allowedTimes"></span></div>'
            var endTime = '<div class="relatas-date-picker-row-end"><label>End</label><p id="endDate"></p></div>'
            var meetingLocationWrap = '<div class="relatas-error-highlight-meetingtype">'+meetingType+meetingLocation+'</div>'
            var submitBtn = '<div id="nextMeetingDetails" class="btn">SEND</div>';
            var timezones = '<select id=timezones>'+getTimezone()+'</select>';

            return '<div class="relatas-form"><form>'+'<div class="relatas-error-highlight-agenda">'+message+'</div>'+startTime+timezones+meetingLocationWrap+submitBtn+'</form></div>'
        }

        function formatDate2(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
        }

        function formatDate3(date) {

            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            var monthIndex = date.getMonth();
            return {
                date:'<span class="day">'+date.getDate()+'</span>'+'<span class="month">'+monthNames[monthIndex]+'</span>',
                time:strTime
            };
        }

        function getCalendarIcon(date,time,locDetails,colors,timezone) {

            var style = 'background:'+colorSchemeGlobal.majorColor+';color:'+colorSchemeGlobal.textColor;

            return '<div id="relatas-calendar"><div id="square" style="'+style+'"><p class="date">'+date+'</p><div class="time">'+time+'<p style="text-align: center;">'+timezone+'</p><p class="loc-details">'+'<i class="'+locDetails.icon+'"></i>'+getTextLength(locDetails.data,10)+'</p></div><span class="hook-left"></span><span class="hook-right"></span>' +
                '<div id="triangle-topleft"></div><div id="triangle-bottomleft"></div>' +
                '</div></div>'
        }

        var errorColors = {
            'background':'#eee',
            'color':'#f86951'
        }

        function sendEmail(message,rEmailId,subject,userId,rName,$) {
            var obj = {
                "userId":userId,
                "receiverEmailId": rEmailId,
                "receiverName": rName,
                "message": message,
                "subject": subject,
                "receiverId": null,
                "docTrack": true,
                "trackViewed": true,
                "remind": true,
                "isLeadTrack": true,
                "newMessage": true,
                "email_cc":""
            }

            $.ajax({
                url:relatasBaseUrl+'/rweblead/messages/send/email/single/web',
                type:'POST',
                datatype:'JSON',
                data:obj,
                success:function(result){
                    if(result.SuccessCode){
                        $(".relatas-no-emailId").removeClass("relatas-error-empty-form");
                        $(".relatas-result-replace").html(messagesGlobal.successReply);
                        $("#sendEmail").hide();
                        $(".relatas-result-replace").css({
                            "background": "#eee",
                            "padding":"5px",
                            "color": "#489448"
                        })
                    } else {
                        $(".relatas-result-replace").html(messagesGlobal.failureReply);
                        $("#sendEmail").hide();
                        $(".relatas-result-replace").css({
                            "background": "#eee",
                            "padding":"5px",
                            "color": "rgb(248, 105, 81)"
                        })
                    }
                }
            });
        }

        function scheduleMeeting(meetingDetails,$) {
            $.ajax({
                url:relatasBaseUrl+'/rweblead/schedule/new/meeting',
                type:'POST',
                datatype:'JSON',
                data:meetingDetails,
                success:function(result){
                    $(".relatas-no-emailId").removeClass("required");
                    $(".relatas-no-name").hide();

                    if(result.SuccessCode){
                        $(".relatas-msg-error").hide()
                        $(".relatas-no-name").hide()

                        $("#sendMeeting").hide()
                        $(".replace").html(messagesGlobal.successReply)
                        $(".replace").css({
                            "background": "#eee",
                            "padding":"5px",
                            "color": "#489448"
                        })
                    } else {
                        $("#sendMeeting").hide()
                        $(".relatas-msg-error").hide()
                        $(".replace").html(messagesGlobal.failureReply)
                        $(".replace").css({
                            "background": "#eee",
                            "padding":"5px",
                            "color": "rgb(248, 105, 81)"
                        })
                    }
                }
            });
        }

        function validateEmail(email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }

        function onFocus(selector,$) {

            var css = {
                "background-color": "transparent",
                "outline": "none",
                "border-bottom":"2px solid"+ colorSchemeGlobal.majorColor,
                "width": "100%",
                "-webkit-transition": "width .2s ease",
                "transition": "width .2s ease"
            }

            $(selector).focus( function() {
                $(this).css(css);
            });

            $(selector).blur( function() {
                $(this).css(cssNone);
            });
        }

        function resetFormFields($) {
            $("#leadComment").val("").css({"border-bottom": "1px solid rgba(204, 204, 204, 0.66)"}).attr("placeholder","Type your query")
            $("#meetingAgenda").val("").css({"border-bottom": "1px solid rgba(204, 204, 204, 0.66)"}).attr("placeholder","Type your meeting agenda/description")
            $("#startDate").val("").css({"border-bottom": "1px solid rgba(204, 204, 204, 0.66)"}).attr("placeholder","Meeting Date")
            $("#startTime").val("").css({"border-bottom": "1px solid rgba(204, 204, 204, 0.66)"}).attr("placeholder","Meeting Time")
            $("#locationDetails").val("").css({"border-bottom": "1px solid rgba(204, 204, 204, 0.66)"}).attr("placeholder","Enter Skype handle")
        }

        function getTextLength(text,maxLength){
            if(!text)
                return "";

            var textLength = text.length;
            if(textLength >maxLength){
                var formattedText = text.slice(0,maxLength)
                return formattedText+'..';
            }
            else return text;
        }

        function getTimezone() {

            var clientTimezone = null;
            clientTimezone = Intl.DateTimeFormat().resolvedOptions().timeZone

            var timezonesList = [
                'Pacific/Samoa',
                'Pacific/Honolulu',
                'US/Alaska',
                'America/Los_Angeles',
                'US/Arizona',
                'America/Managua',
                'America/Bogota',
                'US/Eastern',
                'America/Lima',
                'Canada/Atlantic',
                'America/Caracas',
                'America/La_Paz',
                'America/Santiago',
                'Canada/Newfoundland',
                'America/Sao_Paulo',
                'America/Argentina/Buenos_Aires',
                'America/Godthab',
                'America/Noronha',
                'Atlantic/Azores',
                'Atlantic/Cape_Verde',
                'Africa/Casablanca',
                'Europe/London',
                'Africa/Monrovia',
                'UTC',
                'Europe/Amsterdam',
                'Africa/Lagos',
                'Europe/Athens',
                'Africa/Harare',
                'Asia/Jerusalem',
                'Africa/Johannesburg',
                'Asia/Baghdad',
                'Europe/Minsk',
                'Africa/Nairobi',
                'Asia/Riyadh',
                'Asia/Tehran',
                'Asia/Muscat',
                'Asia/Baku',
                'Asia/Tbilisi',
                'Asia/Yerevan',
                'Asia/Kabul',
                'Asia/Karachi',
                'Asia/Tashkent',
                'Asia/Kolkata',
                'Asia/Katmandu',
                'Asia/Almaty',
                'Asia/Dhaka',
                'Asia/Yekaterinburg',
                'Asia/Rangoon',
                'Asia/Bangkok',
                'Asia/Jakarta',
                'Asia/Novosibirsk',
                'Asia/Hong_Kong',
                'Asia/Chongqing',
                'Asia/Krasnoyarsk',
                'Asia/Kuala_Lumpur',
                'Australia/Perth',
                'Asia/Singapore',
                'Asia/Ulan_Bator',
                'Asia/Urumqi',
                'Asia/Irkutsk',
                'Asia/Tokyo',
                'Asia/Seoul',
                'Australia/Adelaide',
                'Australia/Darwin',
                'Australia/Brisbane',
                'Australia/Canberra',
                'Pacific/Guam',
                'Australia/Hobart',
                'Australia/Melbourne',
                'Pacific/Port_Moresby',
                'Australia/Sydney',
                'Asia/Yakutsk',
                'Asia/Vladivostok',
                'Pacific/Auckland',
                'Pacific/Kwajalein',
                'Asia/Kamchatka',
                'Asia/Magadan',
                'Pacific/Fiji',
                'Asia/Magadan',
                'Pacific/Auckland',
                'Pacific/Tongatapu'
            ];

            var option = '';
            if(clientTimezone){
                option = '<option>'+clientTimezone+'</option>'
            }

            for(var i = 0;i<timezonesList.length;i++){
                option = option+'<option>'+timezonesList[i]+'</option>'
            }

            return option
        }

    }
})();