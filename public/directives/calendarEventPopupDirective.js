/**
 * Created by elavarasan on 29/10/15.
 */
var reletasApprouter = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

reletasApprouter.directive('calendarEventPopup',function(){
    return{
        template:'<p>[[eventTitle]]</p>',
        restrict: 'E',
        link: function (scope) {
            scope.test=function(){
                console.log('test');
            };
            scope.eventTitle='test';
        }
    }
});