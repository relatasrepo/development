

var calendarRouter = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider', function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]')

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

calendarRouter.controller("header_controller", function($scope){
    $scope.getMiddleBarTemplate = function(){
        return ""
    }
});

calendarRouter.controller("logedinUser", function ($scope, $http) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var url = '/'+uName+'/profile/web';

    $http.get(url)
        .success(function (response) {
            setTimeout(function(){
                $(".seeMoreIcon").addClass("disabled-see-more")
            },1000)

            if(response.Data.self == true){
                $scope.l_usr = response.Data.logged_in_user;
            }
            else{
                if(response.Data.logged_in_user.Error){

                    $(".login_header").hide();
                }
                else{
                    $(".login_header").show();
                    identifyMixPanelUser(response.Data.logged_in_user);
                    $scope.l_usr = response.Data.logged_in_user;
                }
            }
        }).error(function (data) {

        })
});

calendarRouter.controller("send_meeting_request", function ($scope, $http) {

    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var url = '/' + uName + '/profile/web';
    var doc = document.querySelector('#page-content-wrapper');
    var slotDate = doc.getAttribute('slotDate');
    var duration = doc.getAttribute('slotDuration');
    var timezone = doc.getAttribute('timezone');

    $scope.send_request_google = '/authenticate/google/web?onSuccess='+uName+'?s=success&onFailure='+uName+'?s=login&action=send_meeting_request';

    $scope.sendMeetingRequest_not_logged_in = function(emailId, firstName, lastName){

        if(!validateEmail(emailId)){
            toastr.error('Please enter a valid email ID')
        }
        else if(!checkRequired(firstName)){
            toastr.error('Please enter your first name')
        }
        else if(!checkRequired(lastName)){
            toastr.error('Please enter your last name')
        }
        else{
            
            identifyMixPanelUser({firstName:firstName,lastName:lastName,emailId:emailId});
            $http.post('/schedule/new/meeting/withoutlogin',{senderEmailId:emailId,senderFirstName:firstName,senderLastName:lastName,timezone:timezone}).
                success(function(response){
                    trackMixpanel("meeting invite without login",function(){});
                    if(response.SuccessCode == 1){
                        window.location.replace(buildUrl('/'+uName,{s:"success",slotDuration:duration,slotDate:slotDate,timezone:timezone,meetingId:response.Data.invitationId}))

                        //Google event tracker
                        ga('send', {
                            hitType: 'event',
                            eventCategory: 'Non LIU',
                            eventAction: 'Schedule Meeting',
                            eventLabel: 'Success'
                        });

                    }
                    else{
                        if(response.Error == 'LOGIN_REQUIRED'){
                            window.location.replace(buildUrl('/'+uName,{s:"login",slotDuration:duration,slotDate:slotDate,timezone:timezone}))
                        }
                    }
                })
        }
    }

    $scope.mixpanel = function (emailId, firstName, lastName) {
        mixpanel.track('Schedule meeting - User not Signed-in', {
            'User First Name' : firstName,
            'User Last Name':  lastName,
            'User Email' : emailId
        });
    }

    $scope.mScheduleMeetingNoLogin = function (){
        mixpanel.track('Schedule meeting - Google login');
    }

    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }

});

calendarRouter.controller("toastr_msg_controller", function ($scope, $http) {
    $http.get('/schedule/get/message')
        .success(function (response) {
            removeSession();

            if (response.Data && response.Data.message && !response.Data.red) {

                if (response.Data.type == 'error') {
                    toastr.error(response.Data.message)
                }
                //else toastr.success(response.Data.message);
            }
        });
    function removeSession(){
        $http.delete('/schedule/remove/message')
            .success(function (response) {

            });
    }
});

function buildUrl(url, parameters){
    var qs = "";
    for(var key in parameters) {
        var value = parameters[key];
        qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
    }
    if (qs.length > 0){
        qs = qs.substring(0, qs.length-1); //chop off last "&"
        url = url + "?" + qs;
    }
    return url;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}