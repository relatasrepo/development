var reletasApp = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]')

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope){
    $scope.getMiddleBarTemplate = function(){
        return ""
    }

    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        //var str = searchContent.replace(/[^\w\s]/gi, '');
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            if(searchContent.length >= 3) {
                yourNetwork = yourNetwork ? true : false;
                extendedNetwork = extendedNetwork ? true : false;
                forCompanies = forCompanies ? true : false;
                window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
            } }
        else toastr.error("Please enter search content")
    };
});

reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

reletasApp.controller("logedinUser", function ($scope, $http,$rootScope) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var url = '/'+uName+'/profile/web';

    $http.get(url)
        .success(function (response) {
            setTimeout(function(){
                $(".seeMoreIcon").addClass("disabled-see-more")
            },1000)

            $rootScope.other_user = response.Data.other_user
            $rootScope.other_user["name"] = $rootScope.other_user.firstName+$rootScope.other_user.lastName

            if(response.Data.self == true){
                $scope.l_usr = response.Data.logged_in_user;
                trackMixpanel("meeting confirmation",function(){});
            }
            else{
                if(response.Data.logged_in_user.Error){
                }
                else{
                    identifyMixPanelUser(response.Data.logged_in_user);
                    $scope.l_usr = response.Data.logged_in_user;
                    trackMixpanel("meeting confirmation",function(){});
                }
            }
        }).error(function (data) {

        })
});

reletasApp.controller("loginPopup_controller", function ($scope, $http) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");

    function jsonToQueryString(json) {
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }

    var reqObj = {
        onSuccess:'/'+uName,
        onFailure:'/'+uName,
        action:'scheduleFlow',
        registrationService:'scheduleFlow',
        onBoarding:'/onboarding/new'
    };
    $scope.login_with_google = '/authenticate/google/signup/web'+jsonToQueryString(reqObj);

    //$scope.login_with_google = '/authenticate/google/web?onSuccess='+uName+'&onFailure='+uName+'&action=login_google';
    //$scope.login_with_outlook = '/authenticate/office365Auth/new?onSuccess='+uName+'&onFailure='+uName+'&action=login_outlook';
});

reletasApp.controller("toastr_msg_controller", function ($scope, $http) {
    $http.get('/schedule/get/message')
        .success(function (response) {

            removeSession();

            if (response.Data && response.Data.message && !response.Data.red) {
                if (response.Data.type == 'error') {
                    toastr.error(response.Data.message)
                }
                else{
                    toastr.success(response.Data.message);
                }

                var messageNewProfile = "";
                if(checkRequired(response.Data.messageNewProfile)){
                    messageNewProfile= '<br>'+response.Data.messageNewProfile;
                    toastr.info(messageNewProfile)
                }
            }
        });
    function removeSession(){
        $http.get('/schedule/go/back')
            .success(function (response) {

            });
    }
});

function getTextLength(text,maxLength){

    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}