
var calendarRouter = angular.module('reletasApp', ['ngRoute','ngCookies','angular-loading-bar']).config(['$interpolateProvider','$httpProvider', function ($interpolateProvider, $httpProvider) { $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]')

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

calendarRouter.controller("header_controller", function($scope){
    $scope.getMiddleBarTemplate = function(){
        return ""
    }

    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            if(str.length >= 3) {
                yourNetwork = yourNetwork ? true : false;
                extendedNetwork = extendedNetwork ? true : false;
                forCompanies = forCompanies ? true : false;
                window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
            } }
        else toastr.error("Please enter search content")
    };
});

calendarRouter.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

calendarRouter.service('shareService', function () {
    return {
        otherParticipants:[],
        emailIdsNotToTake:[]
    };
});

calendarRouter.controller("logedinUser", function ($scope, $http, shareService,$rootScope) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var url = '/'+uName+'/profile/web';

    $http.get(url)
        .success(function (response) {

            $scope.firstName = response.Data.logged_in_user.firstName
            $scope.profilePicUrl = '/getImage/'+response.Data.logged_in_user._id+'/'+new Date().toISOString()

            setTimeout(function(){
                $(".seeMoreIcon").addClass("disabled-see-more")
            },1000)

            $rootScope.other_user = response.Data.other_user
            $rootScope.other_user["name"] = $rootScope.other_user.firstName+$rootScope.other_user.lastName

            if(response.Data.self == true){
                $scope.l_usr = response.Data.logged_in_user;
            }
            else{
                if(!response.Data.other_user.Error){
                    shareService.o_user = response.Data.other_user;
                    shareService.emailIdsNotToTake.push(shareService.o_user.emailId);
                }

                if(response.Data.logged_in_user.Error){
                    shareService.isLoggedInUser = false;
                    $(".login_header").hide();
                }
                else{
                    $(".login_header").show();
                    shareService.isLoggedInUser = true;
                    identifyMixPanelUser(response.Data.logged_in_user);
                    $scope.l_usr = response.Data.logged_in_user;
                    shareService.emailIdsNotToTake.push($scope.l_usr.emailId);
                    shareService.l_user = response.Data.logged_in_user;
                }
            }
        }).error(function (data) {

        })
    append();

    $http.get('/schedule/session/getSavedMeetingDetails')
        .success(function(response){
            if(response.Data){
                shareService.appendMeetingDetails_(response.Data.meetingTitle,response.Data.meetingLocation,response.Data.meetingDescription,response.Data.meetingLocationType)
            }
        })
});

var slotDateText1 = '';
var slotDateText2 = '';

function append(){
    var doc = document.querySelector('#page-content-wrapper');

    var userId = doc.getAttribute('userId');
    var duration = doc.getAttribute('slotDuration');
    var timezone = doc.getAttribute('timezone');
    var slotDate = doc.getAttribute('slotDate');
    var uName = window.location.pathname;
    uName = uName.replace("/", "");

    var date = moment(slotDate).tz(timezone);

    var elemDate = document.querySelector("#selected_slot_date");
    var elemTime = document.querySelector("#selected_slot_time");
    slotDateText1 = date.format("dddd DD MMMM YYYY");
    slotDateText2 = date.format("h:mm a z ")+" (GMT "+date.format("Z")+")";
    if (typeof elemDate.textContent !== "undefined") {
        elemDate.textContent = date.format("dddd DD MMMM YYYY");
        elemTime.textContent = date.format("h:mm a z ")+" (GMT "+date.format("Z")+")";
    }
    else {
        elemDate.innerText = date.format("dddd DD MMMM YYYY");
        elemTime.innerText = date.format("h:mm a z ")+" (GMT "+date.format("Z")+")";
    }
}

calendarRouter.controller("meeting_details_controller", function ($scope, $http,$cookies,shareService) {
    $scope.errorMessageShow = false;
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var url = '/' + uName + '/profile/web';
    var doc = document.querySelector('#page-content-wrapper');
    var slotDate = doc.getAttribute('slotDate');
    var duration = doc.getAttribute('slotDuration');
    var timezone = doc.getAttribute('timezone');

    $("#meetingLocationType").on("change",function(){
        if($(this).val() == 'corporate'){
            var companyObj = $("#companyDetails").text();
            companyObj = JSON.parse(companyObj);
            $scope.getAvailableMeetingRooms(companyObj._id)
        }
        $("#meeting_location").val("");

        if($(this).val()=='Meeting Type *'){
            $scope.meetingType = "Please select meeting type";
        }
        else if($(this).val()=='Skype') {
            $scope.meetingType = "Please enter a valid Skype ID";
        }
        else if($(this).val()=='Conference Call') {
            $scope.meetingType = "Please enter valid phone numbers";
        }
        else if($(this).val()=='Phone') {
            $scope.meetingType = "Please enter a valid phone number";
        }
        else {
            $scope.meetingType = "Please enter a valid meeting address";
        };

        if($(this).val() == 'Phone' || $(this).val() == 'Conference Call'){
            $("#meeting_location").addClass("phone_validate");
            $(".phone_validate").bind("keypress", function (event) {
                if (event.charCode != 0) {
                    var regex = new RegExp("^[0-9+-]*$");

                    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

                    if (!regex.test(key) && key != ' ') {
                        event.preventDefault();
                        return false;
                    }
                }
            });
        }
        else{
            $("#meeting_location").removeClass("phone_validate");
            $("#meeting_location").unbind( "keypress" );
        }
    });

    $scope.getAvailableMeetingRooms = function(companyId){
        $scope.companyMeetingRooms = [];
        var date = moment(slotDate).tz(timezone);
        var start = date.clone().format();
        var end = date.minute(date.minute()+parseInt(duration)).format()
        var reqObj = {
            start:start,
            end:end,
            companyId:companyId
        };
        $http.post('/schedule/freeslot/meetingroom',reqObj)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.companyMeetingRooms = response.Data;
                }
            })
    };

    shareService.appendMeetingDetails_ = function(meetingTitle,meetingLocation,meetingDescription,meetingLocationType){
            $scope.meetingTitle = meetingTitle;
            $scope.meetingLocation = meetingLocation;
            $scope.meetingDescription = meetingDescription;
            $("#meeting_location").val(meetingLocationType);
        $scope.m_location_type = meetingLocationType;
    };

    shareService.getMeetingDetails_ = function(){
        return {
            meetingTitle:$scope.meetingTitle,
            meetingLocation:$scope.meetingLocation,
            meetingDescription:$scope.meetingDescription,
            meetingLocationType:document.querySelector("#meetingLocationType").value
        }
    };

    $scope.sendMeetingRequest = function(meetingTitle, meetingLocation, meetingDescription, meetingRoom){
        $scope.toListParticipants = [];
        $scope.toListParticipants.push({
            receiverId: shareService.o_user._id,
            receiverFirstName: shareService.o_user.firstName,
            receiverLastName: shareService.o_user.lastName,
            receiverEmailId: shareService.o_user.emailId,
            isResource:false,
            isPrimary:true
        });
        var meetingLocationType = document.querySelector("#meetingLocationType").value;

        var isValid = true;
        if(meetingLocationType == 'corporate'){
            if(meetingRoom == 'notSelected'){
                isValid = false;
                toastr.error('Please select meeting room');
            }
            else{
                var companyObj = $("#companyDetails").text();
                companyObj = JSON.parse(companyObj);
                meetingLocationType = companyObj.companyName
                meetingRoom = JSON.parse(meetingRoom);
                meetingLocation = meetingRoom.firstName;
                $scope.toListParticipants.push({
                    receiverId: meetingRoom._id,
                    receiverFirstName: meetingRoom.firstName,
                    receiverLastName: meetingRoom.lastName,
                    receiverEmailId: meetingRoom.emailId,
                    isResource:true
                })
            }
        }

        if(!isValid){

        }
        else if(!checkRequired(meetingTitle)){
            toastr.error('Please enter meeting title');
        }
        else if(!checkRequired(meetingLocationType) || meetingLocationType == 'Meeting Location Type *'){
            toastr.error('Please select location type');
        }
        else if(!checkRequired(meetingLocation)){
            toastr.error('Please enter location details')
        }
        else if(!checkRequired(meetingDescription)){
            toastr.error('Please enter meeting description/ agenda')
        }
        else{

           $scope.errorMessageShow = false;
           var date = moment(slotDate).tz(timezone);
           var meetingDetails = {
                "selfCalendar":true,
                "publicProfileUrl":uName,
                "start":slotDate,
                "end":date.minute(date.minute()+parseInt(duration)).format(),
                "title":meetingTitle,
                "locationType":meetingLocationType,
                "location":meetingLocation,
                "description":meetingDescription,
                slotDateText1:slotDateText1,
                slotDateText2:slotDateText2,
               "toList":$scope.toListParticipants
            };

            if(shareService.otherParticipants.length > 0){
                for(var i=0; i<shareService.otherParticipants.length; i++){
                    meetingDetails.toList.push(
                        {
                            receiverId: shareService.otherParticipants[i].assignedTo,
                            receiverFirstName: shareService.otherParticipants[i].pName,
                            receiverLastName: null,
                            receiverEmailId: shareService.otherParticipants[i].assignedToEmailId,
                            isResource:false
                        }
                    )
                }
            }

            var setTimeoutCounter = 0;
            var msgShownExpire = moment().add(2, "hours").toDate()

            var meetingsUpdate = function() {

                //For Meetings update
                $http.get('/schedule/meeting/location/error')
                    .success(function(response){

                        setTimeoutCounter++;
                        var msgShownCookie = $cookies.get('msgShown');

                        if(response && response == "ZERO_RESULTS"){

                            toastr.error("Please enter a valid address.");
                            $cookies.put('msgShown', true,{expires:msgShownExpire});
                        } else {
                            if(setTimeoutCounter<15 && msgShownCookie || msgShownCookie != 'true'){
                                setTimeout(meetingsUpdate,2000);
                            }
                        }
                    });
            }

            meetingsUpdate();

            $http.post('/schedule/new/meeting',meetingDetails)
                .success(function(response){
                    if(response.SuccessCode == 1){
                        trackMixpanel("meeting invite with login",function(){});
                        window.location.replace(buildUrl('/'+uName,{s:"success",slotDuration:duration,slotDate:slotDate,timezone:timezone}))
                    }
                    else{
                        if(response.Error == 'LOGIN_REQUIRED'){
                            window.location.replace(buildUrl('/'+uName,{s:"login",slotDuration:duration,slotDate:slotDate,timezone:timezone}))

                            //Google event tracker
                            ga('send', {
                                hitType: 'event',
                                eventCategory: 'Non LIU',
                                eventAction: ' Meeting Details',
                                eventLabel: 'Success'
                            });
                        }
                        else toastr.error('Sending invitation Failed. Please try again');
                    }
                })
        }
    };

    function buildUrl(url, parameters){
        var qs = "";
        for(var key in parameters) {
            var value = parameters[key];
            qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
        }
        if (qs.length > 0){
            qs = qs.substring(0, qs.length-1); //chop off last "&"
            url = url + "?" + qs;
        }
        return url;
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }
});

calendarRouter.controller("clear_schedule",function ($scope, $http){

    $scope.goBack = function (){
        var uName = window.location.pathname;
        $http.get('/schedule/go/back')
            .success(function (response) {
                window.location = uName;
            });
    };
});

calendarRouter.controller("loginPopup_controller", function ($scope, $http, shareService) {
    var doc = document.querySelector('#page-content-wrapper');
    var timezone_doc = $("#my_zone").find('span').attr('value')
    var userId = doc.getAttribute('userId');
    var duration = doc.getAttribute('slotDuration');
    var timezone = doc.getAttribute('timezone');
    var slotDate = doc.getAttribute('slotDate');
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    $("#login_popup_message").text("Login/Signup")

    function jsonToQueryString(json) {
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }

    var reqObj = {
        onSuccess:'/'+uName+'?s=fillMeetingInfo&slotDuration='+duration+'&slotDate='+slotDate+'&timezone='+timezone,
        onFailure:'/'+uName+'?s=fillMeetingInfo&slotDuration='+duration+'&slotDate='+slotDate+'&timezone='+timezone,
        action:'scheduleFlow',
        registrationService:'scheduleFlow',
        onBoarding:'/onboarding/new'
    };
    $scope.login_with_google = '/authenticate/google/signup/web'+jsonToQueryString(reqObj);

    //$scope.login_with_google = '/authenticate/google/web?onSuccess='+uName+'&onFailure='+uName+'&action=login_google&s=fillMeetingInfo&slotDuration='+duration+'&slotDate='+slotDate+'&timezone='+timezone;
    //$scope.login_with_outlook = '/authenticate/office365Auth/new?onSuccess='+uName+'&onFailure='+uName+'&action=login_outlook&s=fillMeetingInfo&slotDuration='+duration+'&slotDate='+slotDate+'&timezone='+timezone;

    shareService.updateLoginPopupMessage = function(message){
        $scope.login_popup_message = message;
        $("#login_popup_message").text(message)
    }
});

calendarRouter.controller("toastr_msg_controller", function ($scope, $http) {
    $http.get('/schedule/get/message')
        .success(function (response) {
            removeSession();

            if (response.Data && response.Data.message && !response.Data.red) {

                if (response.Data.type == 'error') {
                    toastr.error(response.Data.message)
                }
                else toastr.success(response.Data.message);
            }
        });
    function removeSession(){
        $http.delete('/schedule/remove/message')
            .success(function (response) {

            });
    }
});

calendarRouter.controller("add_more_contacts_controller", function($scope, $http, shareService){

    $scope.fetchContactsOnChange = function(searchContent){
        if(typeof searchContent == 'string' && searchContent.length > 2){
            $scope.fetchContacts(searchContent);
        }
        else $(".search-results").hide()
    };

    $scope.fetchContacts = function(searchContent){
        if(checkRequired(searchContent) && checkRequired(searchContent.trim())){
            $(".search-results").show();
            if(!$scope.isOnProgress){
                $scope.getContacts('/contacts/filter/custom/web',0,25,'search',searchContent);
            }
        }
        else{
            $(".search-results").hide();
            toastr.error("Please enter your contact name");
        }
    };

    $scope.selectContactTask = function(userId,emailId,name,image){
        $scope.cEmailId = emailId;
        $scope.cuserId = userId;
        $scope.cName = name;
        $scope.cImage = image;
        $scope.searchContactAssignTask = name;
        $("#searchContactAssignTask").val(name);
        $(".search-results").hide()
    };

    $scope.getContacts = function(url,skip,limit,filterBy,searchContent,isBack){
        $scope.isOnProgress = true;
        $http.get(url+'?skip='+skip+'&limit='+limit+'&filterBy='+filterBy+'&searchContent='+searchContent+'&searchByEmailId=yes')
            .success(function(response){
                $scope.isOnProgress = false;
                $scope.contactsList = [];
                $scope.isConnectionOpen = false;
                if(response.SuccessCode){
                    $scope.grandTotal = response.Data.total;
                    if(response.Data.contacts.length > 0){
                        if(!isBack || $scope.currentLength <= 0){
                            $scope.currentLength += response.Data.returned;
                        }

                        for(var i=0; i<response.Data.contacts.length; i++){
                            var li = '';
                            var obj;
                            if(shareService.emailIdsNotToTake.indexOf(response.Data.contacts[i].personEmailId) == -1){
                                if(checkRequired(response.Data.contacts[i].personId) && checkRequired(response.Data.contacts[i].personId.firstName)){
                                    var name = getTextLength(response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName,25);
                                    var image = '/getImage/'+response.Data.contacts[i].personId._id;
                                    var url = response.Data.contacts[i].personId.publicProfileUrl;
                                    var companyName = checkRequired(response.Data.contacts[i].personId.companyName) ? response.Data.contacts[i].personId.companyName : "";
                                    var designation = checkRequired(response.Data.contacts[i].personId.designation) ? response.Data.contacts[i].personId.designation : "";

                                    if(url.charAt(0) != 'h'){
                                        url = '/'+url;
                                    }
                                    obj = {
                                        userId:response.Data.contacts[i].personId._id,
                                        fullName:response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName,
                                        companyName:getTextLength(companyName,25),
                                        designation:getTextLength(designation,25),
                                        fullCompanyName:companyName,
                                        fullDesignation:designation,
                                        emailId:response.Data.contacts[i].personEmailId,
                                        name:name,
                                        image:image,
                                        url:url,
                                        cursor:'cursor:pointer',
                                        idName:'com_con_item_'+response.Data.contacts[i]._id
                                    };
                                    $scope.contactsList.push(obj)
                                }
                                else{
                                    var companyName2 = checkRequired(response.Data.contacts[i].companyName) ? response.Data.contacts[i].companyName : ""
                                    var designation2 = checkRequired(response.Data.contacts[i].designation) ? response.Data.contacts[i].designation : ""

                                    obj = {
                                        userId:null,
                                        fullName:response.Data.contacts[i].personName,
                                        companyName:getTextLength(companyName2,25),
                                        designation:getTextLength(designation2,25),
                                        fullCompanyName:companyName2,
                                        fullDesignation:designation2,
                                        name:getTextLength(response.Data.contacts[i].personName,25),
                                        emailId:response.Data.contacts[i].personEmailId,
                                        image:"/images/default.png",
                                        cursor:'cursor:default',
                                        url:null,
                                        idName:'com_con_item_'+response.Data.contacts[i]._id
                                    };
                                    $scope.contactsList.push(obj);

                                }
                            }
                        }
                    }
                    else {
                        $scope.isAllConnectionsLoaded = true;
                        //$scope.commonConnectionsNotExist = response.Message;
                    }
                }
                else{
                    $scope.isAllConnectionsLoaded = true;
                    //$scope.commonConnectionsNotExist = response.Message;
                }
            })
    };

    $scope.pList = {"zxc":[]};

    $scope.updateParticipants = function() {
        var dp=true;
        if(shareService.emailIdsNotToTake.indexOf($scope.cEmailId) == -1 && shareService.emailIdsNotToTake.indexOf($scope.searchContactAssignTask) == -1){

            $scope.participantsList = {
                "assignedTo":$scope.cuserId,
                "assignedToEmailId":$scope.cEmailId,
                "pName":$scope.cName,
                "pImg":$scope.cImage,
                "unregisteredUser":$scope.searchContactAssignTask
            };

            if(!checkRequired($scope.participantsList.assignedToEmailId)){
                $scope.participantsList.assignedToEmailId = $scope.participantsList.unregisteredUser
            }
            if(checkRequired($scope.cEmailId) && checkRequired($scope.cEmailId.trim())){
                shareService.emailIdsNotToTake.push($scope.cEmailId);
            }

            var ttt = angular.copy($scope.participantsList);

            if(!checkRequired(ttt.pName) || !checkRequired(ttt.pName.trim())) {

                if(validateEmail(ttt.unregisteredUser)){
                    dp=true;
                    for (var i=0;i<$scope.pList.zxc.length;i++){
                        if($scope.pList.zxc[i].unregisteredUser==ttt.unregisteredUser){
                            dp=false;
                        }
                    }

                    if(dp) {

                        ttt.assignedToEmailId = ttt.unregisteredUser;
                        $http.get('/profile/get/emailId/'+ttt.assignedToEmailId+'/web')
                            .success(function(response){
                                if(response.SuccessCode){
                                    $scope.pList.zxc.push({
                                        "assignedTo":response.Data._id,
                                        "assignedToEmailId":response.Data.emailId,
                                        "pName":response.Data.firstName+" "+response.Data.lastName,
                                        "pImg":'/getImage/'+response.Data._id,
                                        "unregisteredUser":response.Data.emailId
                                    })
                                }
                                else $scope.pList.zxc.push(ttt);
                            })

                    } else {
                        $(".show-error").slideDown();
                        setTimeout(function(){
                            $(".show-error").slideUp();}, 5000);
                    }
                }
                else {
                    $(".show-email-error").slideDown();
                    setTimeout(function(){
                        $(".show-email-error").slideUp();}, 5000);
                }
            }
            else {
                dp=true;
                for (var i=0;i<$scope.pList.zxc.length;i++){
                    if($scope.pList.zxc[i].assignedToEmailId==ttt.assignedToEmailId){
                        dp=false;
                    }
                }

                if(dp) {
                    $scope.pList.zxc.push(ttt);

                } else {
                    $(".show-error").slideDown();
                    setTimeout(function(){
                        $(".show-error").slideUp();}, 5000);
                }
            }
        }

        function validateEmail(email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }

        $scope.cuserId = " ";
        $scope.cEmailId = " ";
        $scope.cName = " ";
        $scope.cImage = " ";
        $scope.searchContactAssignTask = " ";

        $scope.remove = function(p) {
            //delete $scope.pList.zxc[p];
            var emailId = $scope.pList.zxc[p].assignedToEmailId
            if(shareService.emailIdsNotToTake.indexOf(emailId) != -1){
                shareService.emailIdsNotToTake.splice(shareService.emailIdsNotToTake.indexOf(emailId),1)
            }
            $scope.pList.zxc.splice(p,1);
            shareService.otherParticipants = $scope.pList.zxc;
        };

        shareService.otherParticipants = $scope.pList.zxc;
    };

    $("body").on("click","#checkbox231",function(){
        if(shareService.isLoggedInUser){
            $(".show-more-participants").slideToggle(5);
        }
        else{
            $(this).prop("checked",false);
            $("#login_model_button").trigger("click");
            shareService.updateLoginPopupMessage("Login to Relatas to Add more participants")
            var details = shareService.getMeetingDetails_();
            $http.post('/schedule/session/saveMeetingDetails/new',details)
                .success(function(response){

                })
        }
    });
});

calendarRouter.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){
                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else{
                    obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    obj.noPPic = obj.name.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    scope.noPPic = obj.name.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                //call the function that was passed
                scope.$apply(attrs.imageonload);
            });
        }
    };
});
