var reletasApp = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]')

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope){
    $scope.getMiddleBarTemplate = function(){
        return ""
    }

    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            if(str.length >= 3) {
                yourNetwork = yourNetwork ? true : false;
                extendedNetwork = extendedNetwork ? true : false;
                forCompanies = forCompanies ? true : false;
                window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
            }  }
        else toastr.error("Please enter search content")
    };
});

reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

reletasApp.controller("slot_duration_fill", function ($scope, $http,$rootScope) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var doc = document.querySelector('#page-content-wrapper');
    var userId = doc.getAttribute('userId');
    var duration = 30;
    var timezone = doc.getAttribute('timezone');

    var url = '/'+uName+'/profile/web';
    var isLoggedInUser = '';

    $http.get(url)
        .success(function (response) {
            
            $rootScope.other_user = response.Data.other_user
            $rootScope.other_user["name"] = $rootScope.other_user.firstName+$rootScope.other_user.lastName

            if(response.Data.logged_in_user.Error){
                isLoggedInUser = false;
            } else {
                isLoggedInUser = true;
            }
        });

    $http.get('/schedule/slots/available?id='+userId+'&slotDuration='+duration+'&daysDuration=14&timezone='+timezone+'&gotoGoogle=true')
        .success(function(arrayOfResults) {

            $scope.first_slot_duration = '/'+uName+'?s=calendar&slotDuration=15'
            $scope.second_slot_duration = '/'+uName+'?s=calendar&slotDuration=30'
            $scope.third_slot_duration = '/'+uName+'?s=calendar&slotDuration=60'

            //Google event tracker
            if(!isLoggedInUser){
                ga('send', {
                    hitType: 'event',
                    eventCategory: 'Non LIU',
                    eventAction: ' Selected Slot',
                    eventLabel: 'Success'
                });
            }

        });
});

reletasApp.controller("logedinUser", function ($scope, $http) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var url = '/'+uName+'/profile/web';

    $http.get(url)
        .success(function (response) {
            //response.Data.isCalendarLocked = false;

            $scope.firstName = response.Data.logged_in_user.firstName
            $scope.profilePicUrl = '/getImage/'+response.Data.logged_in_user._id+'/'+new Date().toISOString()

            setTimeout(function(){
                $(".seeMoreIcon").addClass("disabled-see-more")
            },1000)

            calendar_locked(response.Data.isCalendarLocked);

            if(response.Data.self == true){
                hideElements(false,false,true);
                $scope.l_usr = response.Data.logged_in_user;
            }
            else{
                if(response.Data.logged_in_user.Error){
                    hideElements('login',false,false)
                    $(".login_header").hide();
                }
                else{
                    hideElements('login',true,false);
                    $(".login_header").show();
                    identifyMixPanelUser(response.Data.logged_in_user);
                    $scope.l_usr = response.Data.logged_in_user;
                }
            }
        }).error(function (data) {

        })
});

var http = null;

reletasApp.controller("calendar_loc_controller", function ($scope, $http) {
    http = $http;
});

showMessages();
function showMessages(){
    var params = getParams(window.location.href);
    if(params.Error){
        toastr.error(decodeURIComponent(params.Error))
    }
    else if(params.Success){
        toastr.success(decodeURIComponent(params.Success))
    }
}

function sendCallPassRequest(){
    var url = '/schedule/calendar/lock/request';
    if(http != null)
        http.get(url)
            .success(function (response) {
                if(response.SuccessCode){
                    document.getElementById('call_pass_message').innerHTML = 'The calendar is locked. <br/> Request has been sent to unlock the calendar';
                    document.getElementById('call_pass_button').style.visibility = "hidden";
                    toastr.success(response.Message)
                }
                else{
                    toastr.error(response.Message)
                }
            })
}

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

function calendar_locked(isLocked){
    if(isLocked){
        $("#cal_loc_hide").removeClass("hide");
        $("#schedule_content_hide").addClass("hide");
    }
    else{
        $("#cal_loc_hide").addClass("hide");
        $("#schedule_content_hide").removeClass("hide");
    }
}

var start = 1;

var itemsScroll = 6;
var itemsToShow = 6;

if(window.innerWidth < 480){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 1
    itemsScroll = 1
}else if(window.innerWidth < 900){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 3
    itemsScroll = 3
}
var end = itemsToShow;

//var html = '';
reletasApp.controller("commonConnections", function ($scope, $http) {
    $scope.cConnections = [];
    $scope.cConnectionsIds = [];
    $scope.currentLength = 0;
    $scope.isFirstTime = true;
    $scope.isConnectionOpen = false;
    $scope.start = start;
    $scope.end = end;

    $scope.isAllConnectionsLoaded = false;
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    $scope.isConnectionOpen = true;
    var url = '/schedule/social/contacts/common/web?uniqueName='+uName+'&skip=0&limit=12';
    getCommonConnections($scope, $http, url, true);

    $scope.NextConn = function(){
        var statusNext =  $scope.statusNext;
        var statusPrev =  $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.end < total){
            var itemsToStayPrev = $scope.start+itemsScroll;
            var itemsToStayNext = $scope.end+itemsScroll;
            for(var i=0; i<$scope.cConnectionsIds.length; i++){
               /// if(i+1 > statusNext && i+1 <= (statusNext+itemsScroll)){
                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    addHide($scope.cConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end+itemsScroll;
            $scope.start = $scope.start+itemsScroll;

            $scope.statusNext = statusNext+itemsScroll;
            $scope.statusPrev = statusNext;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
        }
        $scope.showHideArrows();
        if($scope.fetchCommonConnectionsNext() && $scope.isAllConnectionsLoaded == false && $scope.isConnectionOpen == false){
             $scope.isConnectionOpen = true;
             var url = '/schedule/social/contacts/common/web?uniqueName='+uName+'&skip='+$scope.currentLength+'&limit=12';
             getCommonConnections($scope, $http, url, false);
        }
    };

    $scope.fetchCommonConnectionsNext = function(){
        if($scope.end >= $scope.cConnectionsIds.length - 12){
            return true;
        }else return false;
    };

    $scope.PrevConn = function(){

        var statusPrev = $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.start >1){
            var itemsToStayPrev = $scope.start-itemsScroll;
            var itemsToStayNext = $scope.end-itemsScroll;

            for(var i=0; i<$scope.cConnectionsIds.length; i++){
                //if(i+1 <= statusPrev && i+1 > (statusPrev-itemsScroll)){
                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    //$scope.cConnections[i].calssName = ''
                    addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    //$scope.cConnections[i].calssName = 'hide'
                    addHide($scope.cConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end-itemsScroll;
            $scope.start = $scope.start-itemsScroll;

            $scope.statusNext = statusPrev;
            $scope.statusPrev = statusPrev-itemsScroll+itemsScroll;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
            $scope.showHideArrows()
        }
    };

    $scope.showHideArrows = function(){
        if($scope.grandTotal >0){
            if($scope.start <=1 ){
                addHide("als-prev",true)
            }
            else addHide("als-prev",false)

            if($scope.end >= $scope.grandTotal){
                addHide("als-next",true)
            }
            else addHide("als-next",false)
        }
        else{
            addHide("als-prev",true)
            addHide("als-next",true)
        }
    };

    $scope.connectionClick = function(url){
        if(url && url.length > 0 && url[0] && url[0][0] && url[0][0] != 'null' && url[0][0] != null ) {
            trackMixpanel("clicked on common connection",url[0][0],function(){
                window.location.replace(url[0][0]);
            })
        }
    }

    $scope.interactionsContext = function(emailId,mobileNumber){
        if(mobileNumber && mobileNumber !='undefined'){
            window.location.replace('/contact/selected?context='+emailId+'&mobileNumber='+mobileNumber.replace(/[^a-zA-Z0-9]/g,''))
        } else {
            window.location.replace('/contact/selected?context='+emailId)
        }
    };
});

function addHide(elementId,addHide){
    addHide ? $("#"+elementId).addClass('hide') : $("#"+elementId).removeClass('hide')
    //addHide ? $("#"+elementId).css({display:'none'}) : $("#"+elementId).css({display:'block'})
}

function getCommonConnections($scope, $http, url, isHides){

    $http.get(url)
        .success(function (response) {
            $scope.isConnectionOpen = false;
            if(response.SuccessCode){
                $scope.grandTotal = response.Total;
                if(response.Data && response.Data.length > 0){
                    var html = '';
                    hidePrevNext(true)
                    $scope.currentLength += response.Data.length;
                    for(var i=0; i<response.Data.length; i++){
                        var li = '';
                        var obj;
                        if(checkRequired(response.Data[i].personId) && checkRequired(response.Data[i].personName)){

                            var name = getTextLength(response.Data[i].personName,13);
                            var image = '/getImage/'+response.Data[i].personId;

                            obj = {
                                fullName:response.Data[i].personName,
                                name:name,
                                image:image,
                                cursor:'cursor:pointer',
                                idName:'com_con_item_'+response.Data[i]._id,
                                className:'hide',
                                emailId:response.Data[i].personEmailId,
                                mobileNumber : response.Data[i].mobileNumber?response.Data[i].mobileNumber.replace(/[^a-zA-Z0-9]/g,''):null
                            };
                            if(isHides && i<itemsToShow){
                                obj.className = ''
                            }
                            $scope.cConnections.push(obj);

                            $scope.cConnectionsIds.push('com_con_item_'+response.Data[i]._id);
                        }
                        else{
                            var contactImageLink = response.Data[i].contactImageLink?encodeURIComponent(response.Data[i].contactImageLink):null

                            obj = {
                                fullName:response.Data[i].personName,
                                name:getTextLength(response.Data[i].personName,13),
                                image:'/getContactImage/'+response.Data[i].personEmailId+'/'+contactImageLink,
                                cursor:'cursor:pointer',
                                url:null,
                                // noPicFlag:true,
                                idName:'com_con_item_'+response.Data[i]._id,
                                className:'hide',
                                emailId:response.Data[i].personEmailId,
                                mobileNumber : response.Data[i].mobileNumber?response.Data[i].mobileNumber.replace(/[^a-zA-Z0-9]/g,''):null
                            };
                            obj.nameNoImg = (obj.name.substr(0,2)).toUpperCase();
                            if(isHides && i<itemsToShow){
                                obj.className = ''
                            }
                            obj.emailId = response.Data[i].personEmailId;

                            $scope.cConnections.push(obj);
                            $scope.cConnectionsIds.push('com_con_item_'+response.Data[i]._id);
                        }
                    }
                    if(isHides){
                        $scope.statusPrev = 1;
                        $scope.statusNext = itemsScroll;
                    }
                    $scope.showHideArrows()
                }
                else if(isHides){
                    hidePrevNext(true)
                    commonConnectionsMessage(false)
                    $scope.commonConnectionsNotExist = response.Message;
                }else $scope.isAllConnectionsLoaded = true;
            }
            else if(isHides){
                hidePrevNext(true)
                commonConnectionsMessage(false)
                $scope.commonConnectionsNotExist = response.Message;
            }else $scope.isAllConnectionsLoaded = true;
        })
}

function showOrHideArrows(){
    $("#als-prev")
    $("#als-next")
}

reletasApp.controller("linkedinInfo", function ($scope, $http) {

    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var url = '/schedule/social/linkedin/web?uniqueName=' + uName;
    $http.get(url)
        .success(function (response) {

            if (response.SuccessCode) {
                var description = '';
                var dateTime = '';
                var linkedinAuther = '';
                var positions= [];
                var post = '';

                if(response.Data.currentShare){
                    if(response.Data.currentShare.content  && response.Data.currentShare.content.description){
                        description = response.Data.currentShare.content.description;
                    }
                    else if(response.Data.currentShare.content  && response.Data.currentShare.content.title){
                        description = response.Data.currentShare.content.title;
                    }
                    dateTime = new Date(response.Data.currentShare.timestamp)
                    dateTime = moment(dateTime).format('hh:mm a - DD MMM YYYY');

                    if(response.Data.currentShare.author  && response.Data.currentShare.author.firstName){
                        linkedinAuther = response.Data.currentShare.author.firstName;
                    }
                    hideLinkedin_post(false);
                }
                else{
                    hideLinkedin_post(true);
                }

                if(response.Data.currentShare.comment){
                    post = response.Data.currentShare.comment;

                    var wordCount = post.match(/(\w+)/g).length;

                    if(wordCount>140){
                        var readMore = 'read-more';

                    } else {
                        var readMore = 'short-post';
                    }

                }

                if(response.Data.positions && response.Data.positions.values && response.Data.positions.values.length > 0){

                   // positions = response.Data.positions.values;
                    for(var i=0; i<response.Data.positions.values.length; i++){
                        if(response.Data.positions.values[i].isCurrent != true){
                            positions.push(response.Data.positions.values[i])
                        }
                    }
                    if(positions.length > 0){
                        hideLinkedin_positions(false)
                    }
                    else hideLinkedin_positions(true)
                }
                else{
                    hideLinkedin_positions(true)
                }

                $scope.description = description;
                $scope.dateTime = dateTime;
                $scope.linkedinAuther = linkedinAuther;
                $scope.positions = positions;
                $scope.post = post;
                $scope.readMore = readMore;
            }
            else{
                hideLinkedin_positions(true)
                hideLinkedin_post(true)
            }
        })
});

reletasApp.controller("twitterTweetsController", function ($scope, $http, $q) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var url = '/schedule/social/twitter/web?uniqueName=' + uName;
    $http.get(url)
        .success(function (response) {

            if (response.SuccessCode) {
                var tDescription = '';
                var tDateTime = '';
                var tAuther = '';
                if(response.Data && response.Data[0]){
                    hideTwitter(false)
                    if(response.Data[0].text){
                        tDescription =   response.Data[0].text;
                    }
                    tDateTime = new Date(response.Data[0].created_at);
                    tDateTime = moment(tDateTime).format('hh:mm a - DD MMM YYYY');

                    if(response.Data[0].user && response.Data[0].user.screen_name){
                        tAuther = '@'+response.Data[0].user.screen_name;
                    }
                    $scope.tDescription = tDescription;
                    $scope.tDateTime = tDateTime;
                    $scope.tAuther = tAuther;
                }
                else{
                    hideTwitter(true)
                }
            }
            else hideTwitter(true)
        });
});

reletasApp.controller("loginPopup_controller", function ($scope, $http) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");

    function jsonToQueryString(json) {
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }

    var reqObj = {
        onSuccess:'/'+uName,
        onFailure:'/'+uName,
        action:'scheduleFlow',
        registrationService:'scheduleFlow',
        onBoarding:'/onboarding/new'
    };
    $scope.login_with_google = '/authenticate/google/signup/web'+jsonToQueryString(reqObj);


    //$scope.login_with_google = '/authenticate/google/web?onSuccess='+uName+'&onFailure='+uName+'&action=login_google';
    //$scope.login_with_outlook = '/authenticate/office365Auth/new?onSuccess='+uName+'&onFailure='+uName+'&action=login_outlook';
});

reletasApp.controller("toastr_msg_controller", function ($scope, $http) {
    $http.get('/schedule/get/message')
        .success(function (response) {
            removeSession();

            if (response.Data && response.Data.message && !response.Data.red) {

                if (response.Data.type == 'error') {
                    toastr.error(response.Data.message)
                }
                else toastr.success(response.Data.message);
            }
        });
    function removeSession(){
        $http.delete('/schedule/remove/message')
            .success(function (response) {

            });
    }
});

var isLoaded = false;

function loadFile(callback){
    if(!isLoaded){
        $.getScript( "/javascripts/schedule/slide/jquery.als-1.6.min.js" )
            .done(function( script, textStatus ) {
                isLoaded = true;
                callback()
            })
    }else callback();

}

function hideElements(statusL,isLoggedIn,isSelf){
    if(isSelf){
        $('#social_intelligence_btn').hide()
        $('#social_intelligence_container').hide()
    }
    else if(statusL == 'login'){
        if(!isLoggedIn){
            $('#social_intelligence_btn').show()
            $('#social_intelligence_container').hide()
        }
        else{
            $('#social_intelligence_btn').hide()
            $('#social_intelligence_container').show()
        }
    }
    else{
        if(isLoggedIn){

        }
    }
}

function hidePrevNext(hide){
    if(hide){
        $(".als-prev").addClass('hide')
        $(".als-next").addClass('hide')
    }
    else{
        $(".als-prev").removeClass('hide')
        $(".als-next").removeClass('hide')
    }
}

function hideLinkedin_post(hide){
    if(hide){
        $("#linkedinInfoContainer").addClass('hide')
    }
    else{
        $("#linkedinInfoContainer").removeClass('hide')
    }
}

function hideLinkedin_positions(hide){
    if(hide){
        $("#linkedinPositions").addClass('hide')
    }
    else{
        $("#linkedinPositions").removeClass('hide')
    }
}

function hideTwitter(hide){
    if(hide){
        $("#twitterTweetsContainer").addClass('hide')
    }
    else{
        $("#twitterTweetsContainer").removeClass('hide')
    }
}

function commonConnectionsMessage(hide){
    if(hide){
        $("#commonConnectionsName_msg").addClass('hide')
    }
    else{
        $("#commonConnectionsName_msg").removeClass('hide')
    }
}

function getTextLength(text,maxLength){
    if(!checkRequired(text)){
        return "";
    }
    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}
