
var calendarRouter = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider', function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]')

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

calendarRouter.controller("header_controller", function($scope){
    $scope.getMiddleBarTemplate = function(){
        return ""
    }

    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        //var str = searchContent.replace(/[^\w\s]/gi, '');
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            if(str.length >= 3) {
                yourNetwork = yourNetwork ? true : false;
                extendedNetwork = extendedNetwork ? true : false;
                forCompanies = forCompanies ? true : false;
                window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
            }}
        else toastr.error("Please enter search content")
    };
});


calendarRouter.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

calendarRouter.controller("logedinUser", function ($scope, $http,$rootScope) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var url = '/'+uName+'/profile/web';

    $http.get(url)
        .success(function (response) {

            $scope.firstName = response.Data.logged_in_user.firstName
            $scope.profilePicUrl = '/getImage/'+response.Data.logged_in_user._id+'/'+new Date().toISOString()

            setTimeout(function(){
                $(".seeMoreIcon").addClass("disabled-see-more")
            },1000)

            $rootScope.other_user = response.Data.other_user
            $rootScope.other_user["name"] = $rootScope.other_user.firstName+$rootScope.other_user.lastName

            if(response.Data.self == true){
                $scope.l_usr = response.Data.logged_in_user;
            }
            else{
                if(response.Data.logged_in_user.Error){

                    $(".login_header").hide();
                }
                else{
                    $(".login_header").show();
                    identifyMixPanelUser(response.Data.logged_in_user);
                    $scope.l_usr = response.Data.logged_in_user;
                }
            }
        }).error(function (data) {

        })
});

var $scope_calendar_days_slots,
    $http_calendar_days_slots,
    timezone_changed,
    oldTimezone;
var mapMyCal = true;
var isLoggedIn = false;
var itemsScroll = 7;
if(window.innerWidth < 900){
    itemsScroll = 3;
}

function updateCalendar(){

    var doc = document.querySelector('#page-content-wrapper');
    var timezone_doc = $("#my_zone").find('span').attr('value')
    var userId = doc.getAttribute('userId');
    var duration = doc.getAttribute('slotDuration');
    var timezone = doc.getAttribute('timezone');
    var slotDate = doc.getAttribute('slotDate');
    var meridiem = doc.getAttribute('meridiem');

    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    timezone = timezone_doc;
    timezone_changed = timezone;
    currentTime2();
    trackMixpanel("timezone changed",function(){});

    var date = moment(slotDate).tz(timezone_changed);
    var oldTimeDiff = moment().tz(oldTimezone).format("Z").split(":")[0];
    var newTimeDiff = moment().tz(timezone).format("Z").split(":")[0];
    var finalDiff = oldTimeDiff - newTimeDiff;


    if(Math.abs(finalDiff) >= 12){

        if(meridiem == 'pm'){
            meridiem = 'am';
        }
        else{
            meridiem = 'pm'
        }
        //meridiem = meridiem == 'pm' ? 'am' : 'pm'
        if(finalDiff >= 0){
            //date.date(date.date() - 1);
            //decrease day & revers AP/PM
        }
        else{
            //date.date(date.date() + 1);
            // increase day & reves
        }
    }

    var meridiem_slot_title = document.querySelector("#meridiem_slot_title");

    //var elem = document.querySelector("#timezone_value");
    var elemDate = document.querySelector("#slot_day_date");
    if (typeof elemDate.textContent !== "undefined") {
       // elem.textContent = zoneName+" time (GMT "+date.format("Z")+")";
        elemDate.textContent = date.format("dddd DD MMMM YYYY");
        meridiem_slot_title.textContent = 'Available '+meridiem.toUpperCase()+' start times for '+duration+' Minutes on'
    }
    else {
       // elem.innerText = zoneName+" time (GMT "+date.format("Z")+")";
        elemDate.innerText = date.format("dddd DD MMMM YYYY");
        meridiem_slot_title.innerText = 'Available '+meridiem.toUpperCase+' start times for '+duration+' Minutes on'
    }

    bindCalendarData($http_calendar_days_slots, $scope_calendar_days_slots, userId, timezone, duration, uName, slotDate, mapMyCal, meridiem);
}

var currentTimeStarted = false;
calendarRouter.controller("calendar_days_slots", function ($scope, $http, $route) {
    $scope_calendar_days_slots = $scope;
    $http_calendar_days_slots = $http;
    $scope.checked_map_my_cal = true;
    var doc = document.querySelector('#page-content-wrapper');
    var userId = doc.getAttribute('userId');
    var duration = doc.getAttribute('slotDuration');
    var timezone = doc.getAttribute('timezone');
    var slotDate = doc.getAttribute('slotDate');
    var isLogged = doc.getAttribute('isLoggedIn');
    var meridiem = doc.getAttribute('meridiem');
    oldTimezone = timezone;
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    if(isLogged == 'true' || isLogged == true)
        isLoggedIn = true;

    $scope.checked_map_my_cal = isLoggedIn;
    $http.get('/schedule/get/timezone/session')
        .success(function(response) {

            if (response != null && response != undefined) {
                timezone = response;

                var date = moment(slotDate).tz(timezone);
                timezone_changed = timezone;
                if(!currentTimeStarted){
                    currentTimeStarted = true;
                    currentTime();
                }
                var zoneName = '';
                if(timezone.indexOf('/') != -1){
                    zoneName = timezone.split('/')[1];
                }else zoneName = timezone;

                var elem = document.querySelector("#timezone_value");
                var elemDate = document.querySelector("#slot_day_date");
                if (typeof elem.textContent !== "undefined") {
                    elem.textContent = zoneName+" time (GMT "+date.format("Z")+")";
                    elemDate.textContent = date.format("dddd DD MMMM YYYY");
                }
                else {
                    elem.innerText = zoneName+" time (GMT "+date.format("Z")+")";
                    elemDate.innerText = date.format("dddd DD MMMM YYYY");
                }
            }
        })

    $scope.mapMyCalChange = function(isChecked){
        mapMyCal = isChecked;
        var text = isChecked ? 'map my calendar checked' : 'map my calendar unchecked';
        trackMixpanel(text,function(){});
        if(isChecked && !isLoggedIn){
            $scope.checked_map_my_cal = false;
            clickLoginPopup();
            //alert('Please login to your account to use map my calendar');
        }
        else bindCalendarData($http, $scope, userId, timezone, duration, uName, slotDate, isChecked, meridiem);
    };

    $scope.slotClick = function(url){
        if(url && url.length > 0 && url[0] && url[0][0] && url[0][0] != 'null' && url[0][0] != null ) {
            trackMixpanel("calendar slot",url[0][0],function(){
                window.location.replace(url[0][0]);
            })
        }

        if(!isLoggedIn){
            //Google event tracker
               ga('send', {
                    hitType: 'event',
                    eventCategory: 'Non LIU',
                    eventAction: ' Selected Start Times',
                    eventLabel: 'Success'
                });
            }
    };

    bindCalendarData($http, $scope, userId, timezone, duration, uName, slotDate, true, meridiem);

    $scope.goBack = function (){
        var uName = window.location.pathname;
        $http.get('/schedule/go/back')
            .success(function (response) {
                window.location = uName;
            });
    };


});

function bindCalendarData($http, $scope, userId, timezone, duration, uName, slotDate, mapMyCal, meridiem){

    var url = '/schedule/slots/available?id='+userId+'&slotDuration='+duration+'&daysDuration=14&timezone='+timezone+'&slotDate='+slotDate
    if(mapMyCal && isLoggedIn){
        url = '/schedule/slots/available/mapcalendar?id='+userId+'&slotDuration='+duration+'&daysDuration=14&timezone='+timezone+'&slotDate='+slotDate
    }

    $http.get(url)
        .success(function(oFreeSlots){
              if(oFreeSlots.SuccessCode){

                  var allSlots = [];
                  var slots = oFreeSlots.Data[moment(slotDate).tz(timezone).format("MM-DD-YYYY")];
                  if(meridiem == 'am' && slots.am.slots){
                      allSlots = allSlots.concat(slots.am.slots);
                  }
                  if(meridiem == 'pm' && slots.pm.slots){
                      allSlots = allSlots.concat(slots.pm.slots);
                  }
                  var freeSlots = [];
                  allSlots.forEach(function(slot,index){
                       freeSlots.push({
                           startDate:slot.start,
                           endDate:slot.end,
                           class:slot.isBlocked ? 'stripe-1':'',
                           start:slot.isTitleExists ? moment(slot.start).tz(timezone).format("hh:mm a")+' '+slot.title :moment(slot.start).tz(timezone).format("hh:mm a"),
                           url:buildUrl('/'+uName,{s:"fillMeetingInfo",slotDuration:duration,slotDate:slot.start,timezone:timezone},slot.isBlocked)
                       })
                  });

                  $scope.freeSlots = freeSlots;
              }
        })
}

function buildUrl(url, parameters, isBlocked){
    var qs = "";
    for(var key in parameters) {
        var value = parameters[key];
        qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
    }
    if (qs.length > 0){
        qs = qs.substring(0, qs.length-1); //chop off last "&"
        url = url + "?" + qs;
    }
    return isBlocked ? null : url;
}

function currentTime(){
    currentTime2();
    setTimeout(function(){
        currentTime()
    },1000)
}

function currentTime2(){
    $("#current_time").text(moment().tz(timezone_changed).format('h:mm a'));
}

calendarRouter.controller("loginPopup_controller", function ($scope, $http) {
    var doc = document.querySelector('#page-content-wrapper');
    var timezone_doc = $("#my_zone").find('span').attr('value')
    var userId = doc.getAttribute('userId');
    var duration = doc.getAttribute('slotDuration');
    var timezone = doc.getAttribute('timezone');
    var slotDate = doc.getAttribute('slotDate');
    var uName = window.location.pathname;
    uName = uName.replace("/", "");

    function jsonToQueryString(json) {
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }

    var reqObj = {
        onSuccess:'/'+uName+'?s=selectedSlot&slotDuration='+duration+'&slotDate='+slotDate+'&timezone='+timezone,
        onFailure:'/'+uName+'?s=selectedSlot&slotDuration='+duration+'&slotDate='+slotDate+'&timezone='+timezone,
        action:'scheduleFlow',
        registrationService:'scheduleFlow',
        onBoarding:'/onboarding/new'
    };
    $scope.login_with_google = '/authenticate/google/signup/web'+jsonToQueryString(reqObj);

    //$scope.login_with_google = '/authenticate/google/web?onSuccess='+uName+'&onFailure='+uName+'&action=login_google&s=selectedSlot&slotDuration='+duration+'&slotDate='+slotDate+'&timezone='+timezone;
    //$scope.login_with_outlook = '/authenticate/office365Auth/new?onSuccess='+uName+'&onFailure='+uName+'&action=login_outlook&s=selectedSlot&slotDuration='+duration+'&slotDate='+slotDate+'&timezone='+timezone;
});

calendarRouter.controller("timezones_controller", function ($scope, $http) {
    var timezonesList = [
        'Pacific/Samoa',
        'Pacific/Honolulu',
        'US/Alaska',
        'America/Los_Angeles',
        'US/Arizona',
        'America/Managua',
        'America/Bogota',
        'US/Eastern',
        'America/Lima',
        'Canada/Atlantic',
        'America/Caracas',
        'America/La_Paz',
        'America/Santiago',
        'Canada/Newfoundland',
        'America/Sao_Paulo',
        'America/Argentina/Buenos_Aires',
        'America/Godthab',
        'America/Noronha',
        'Atlantic/Azores',
        'Atlantic/Cape_Verde',
        'Africa/Casablanca',
        'Europe/London',
        'Africa/Monrovia',
        'UTC',
        'Europe/Amsterdam',
        'Africa/Lagos',
        'Europe/Athens',
        'Africa/Harare',
        'Asia/Jerusalem',
        'Africa/Johannesburg',
        'Asia/Baghdad',
        'Europe/Minsk',
        'Africa/Nairobi',
        'Asia/Riyadh',
        'Asia/Tehran',
        'Asia/Muscat',
        'Asia/Baku',
        'Asia/Tbilisi',
        'Asia/Yerevan',
        'Asia/Kabul',
        'Asia/Karachi',
        'Asia/Tashkent',
        'Asia/Kolkata',
        'Asia/Katmandu',
        'Asia/Almaty',
        'Asia/Dhaka',
        'Asia/Yekaterinburg',
        'Asia/Rangoon',
        'Asia/Bangkok',
        'Asia/Jakarta',
        'Asia/Novosibirsk',
        'Asia/Hong_Kong',
        'Asia/Chongqing',
        'Asia/Krasnoyarsk',
        'Asia/Kuala_Lumpur',
        'Australia/Perth',
        'Asia/Singapore',
        'Asia/Ulan_Bator',
        'Asia/Urumqi',
        'Asia/Irkutsk',
        'Asia/Tokyo',
        'Asia/Seoul',
        'Australia/Adelaide',
        'Australia/Darwin',
        'Australia/Brisbane',
        'Australia/Canberra',
        'Pacific/Guam',
        'Australia/Hobart',
        'Australia/Melbourne',
        'Pacific/Port_Moresby',
        'Australia/Sydney',
        'Asia/Yakutsk',
        'Asia/Vladivostok',
        'Pacific/Auckland',
        'Pacific/Kwajalein',
        'Asia/Kamchatka',
        'Asia/Magadan',
        'Pacific/Fiji',
        'Asia/Magadan',
        'Pacific/Auckland',
        'Pacific/Tongatapu'
    ];

    var timezones = [];
    for(var i=0; i<timezonesList.length; i++){
        var date = moment().tz(timezonesList[i]);
        var zoneName = '';
        if(timezonesList[i].indexOf('/') != -1){
            zoneName = timezonesList[i].split('/')[1];
        }else zoneName = timezonesList[i];

        timezones.push({
            //date:date.format("z")+" time (GMT "+date.format("Z")+")",
            date:zoneName+" time (GMT "+date.format("Z")+")",
            zone:timezonesList[i]
        })
    }
    $scope.listItems = timezones;
});

function clickLoginPopup(){
    $("#map_my_cal_popup_msg").removeClass('hide')
    $("#login_model_button").trigger("click");
}

calendarRouter.controller("toastr_msg_controller", function ($scope, $http) {
    $http.get('/schedule/get/message')
        .success(function (response) {
            removeSession();

            if (response.Data && response.Data.message && !response.Data.red) {

                if (response.Data.type == 'error') {
                    toastr.error(response.Data.message)
                }
               // else toastr.success(response.Data.message);
            }
        });
    function removeSession(){
        $http.delete('/schedule/remove/message')
            .success(function (response) {

            });
    }
});

calendarRouter.controller("clear_schedule",function ($scope, $http){

    $scope.goBack = function (){
        var uName = window.location.pathname;
        $http.get('/schedule/go/back')
            .success(function (response) {
                window.location = uName;
            });
    };
});

calendarRouter.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){
                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else{
                    obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    obj.noPPic = obj.name.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    scope.noPPic = obj.name.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                //call the function that was passed
                scope.$apply(attrs.imageonload);
            });
        }
    };
});
