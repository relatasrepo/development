
$(window).load(function(){
    window.noOfDays = {}
    $("a").click(function(event) {
        if($(this).attr('href') != undefined){
            var dropType = $($(this)[0].outerHTML).attr("drop-type")
            var cb
            if(dropType)
              cb = invokeRepaint(dropType, $(this).html())
            else 
              cb = generate_callback($(this).attr('href'));

            event.preventDefault();
            mixpanel.track($(this).attr('m_event') || 'Link_clicked',{link:$(this).attr('href'),page:window.location.href});
            mixpanel.people.increment($(this).attr('m_event') || 'Link_clicked',cb);

            setTimeout(cb, 500);
        }
    });

    function invokeRepaint(type, days){
        //console.log(type, days);
        window.noOfDays[type] = days
    }

    function generate_callback(link) {
        return function() {
            window.location.replace(link);
        }
    }

});

function trackMixpanel(m_event,url,cb){
    mixpanel.track(m_event || 'Link_clicked',{link:url,page:window.location.href},cb);
    // mixpanel.people.increment($(this).attr('m_event') || 'Link_clicked',cb);
    setTimeout(cb, 500);
}

function identifyMixPanelUser(profile){

    mixpanel.identify(profile.emailId);
    mixpanel.people.set({
        "$email": profile.emailId,    // only special properties need the $
        "$first_name": profile.firstName,
        "$last_name": profile.lastName,
        "$created": new Date(),
        "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
        "page":window.location.href
    });

}
