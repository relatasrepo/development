(function() {

    var jQuery;

    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.4.2') {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type","text/javascript");
        script_tag.setAttribute("src",rww.getDomain()+"/javascripts/jquery.min.js");
        if (script_tag.readyState) {
            script_tag.onreadystatechange = function () {
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    scriptLoadHandler();
                }
            };
        }
        else {
            script_tag.onload = scriptLoadHandler;
        }

        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    }
    else {

        jQuery = window.jQuery;
        main();
    }

    function scriptLoadHandler() {
        jQuery = window.jQuery.noConflict(true);

        var scripts = ['moment.js','moment-timezone.js','jstz-1.0.4.min.js','pace.js'],
            index   = 0;

        load_script();

        function load_script() {

            if (index < scripts.length) {

                jQuery.getScript(rww.getDomain()+"/javascripts/"+scripts[index], function () {

                    index++;
                    load_script();
                });
            }
            else{
                main();
            }
        }
    }
    var timezone,widgetUserProfile,widgetUserTimezone,busySlots = [],timeSlots = [], JQ, analysisFlag = false,loggedInUserTimezone;

    function main() {
        jQuery(document).ready(function($) {
            JQ = $;
            getWidgetUserProfile();
            $("#widget-container").on("click","#web-widget-relatas-contact-me",function(){
                getHtml(rww.getDomain()+'/webWidget/relatas/contactMe',true);
            });

            var jsonp_url = rww.getDomain()+"/webWidget/relatas/html";
            getHtml(jsonp_url);
            function getHtml(url,busyTimes){
                $.ajax({
                    url:url,
                    type:'GET',
                    success:function(data){
                        $('#widget-container').html(data);
                        if(busyTimes){
                            getBusySlots(new Date().toISOString(),widgetUserTimezone);
                        }
                    }
                });
            }

            function getBusySlots(dateString,timezone){
                Pace.track(function() {
                    $.ajax({
                        url: rww.getDomain() + '/webWidget/relatas/getBusy?dateString=' + dateString + '&timezone=' + timezone + '&key=' + rww.getKey(),
                        type: 'GET',
                        datatype: 'JSON',
                        success: function (data) {
                            if (checkRequiredField(data) && data.length > 0) {
                                for (var j = 0; j < data.length; j++) {
                                    data[j] = {
                                        start: moment(data[j].start).tz(widgetUserTimezone).toDate(),
                                        end: moment(data[j].end).tz(widgetUserTimezone).toDate()
                                    }
                                }
                                busySlots = busySlots.concat(data);
                            }
                            loadUserBusyTimes(widgetUserProfile, timezone || getTimezone(), widgetUserTimezone, new Date());
                        }
                    });
                })
            }

            function getWidgetUserProfile(){

                $.ajax({
                    url:rww.getDomain()+'/webWidget/relatas/profile/byToken/'+rww.getKey(),
                    type:'GET',
                    datatype:'JSON',
                    success:function(data){
                        if(data != false && checkRequiredField(data)){
                            getTimezone();
                            widgetUserProfile = data;
                            if(checkRequiredField(widgetUserProfile.timezone) && checkRequiredField(widgetUserProfile.timezone.name)){
                                widgetUserTimezone = widgetUserProfile.timezone.name;
                            }
                            else{
                                widgetUserTimezone = timezone;
                            }
                        }
                    }
                });
            }

            function constructInvitationDetails(){

                var invitationDetails = {
                    senderId:'',
                    senderName:'',
                    senderEmailId:'',
                    receiverId:widgetUserProfile._id,
                    receiverName:widgetUserProfile.firstName+" "+widgetUserProfile.lastName,
                    receiverEmailId:widgetUserProfile.emailId,
                    documentName : '',
                    documentUrl  : '',
                    awsKey:'',
                    scheduledDate:new Date()
                };

                if (timeSlots.length < 2) {
                    if (timeSlots.length == 0) {
                       alert("Please select time slots on the calendar before sending the invitation.")
                    }
                    else if (!analysisFlag) {
                        alert("Our analysis shows that Relatas users prefer 3 selected time options. This has higher probability of getting a meeting accepted faster. Select additional time slots else click on Send invite again.")
                        analysisFlag = true;
                    }
                    else {
                        constructTimeSlotsJson(invitationDetails, timeSlots)
                    }
                }
                else {
                    constructTimeSlotsJson(invitationDetails, timeSlots)
                }
            }

            function contains(str,subStr) {
                return str.indexOf(subStr) != -1;
            }

            function constructTimeSlotsJson(invitationDetails,timeSlots){
                if (timeSlots[0]) {
                    invitationDetails.start1 = timeSlots[0].start;
                    invitationDetails.end1 = timeSlots[0].end;
                    invitationDetails.title1 = timeSlots[0].title;
                    invitationDetails.location1 = timeSlots[0].location;
                    invitationDetails.locationType1 = timeSlots[0].locationType;
                    invitationDetails.description1 = timeSlots[0].description
                }
                if (timeSlots[1]) {
                    invitationDetails.start2 = timeSlots[1].start;
                    invitationDetails.end2 = timeSlots[1].end;
                    invitationDetails.title2 = timeSlots[1].title;
                    invitationDetails.location2 = timeSlots[1].location;
                    invitationDetails.locationType2 = timeSlots[1].locationType;
                    invitationDetails.description2 = timeSlots[1].description
                }
                if (timeSlots[2]) {
                    invitationDetails.start3 = timeSlots[2].start
                    invitationDetails.end3 = timeSlots[2].end
                    invitationDetails.title3 = timeSlots[2].title
                    invitationDetails.location3 = timeSlots[2].location
                    invitationDetails.locationType3 = timeSlots[2].locationType
                    invitationDetails.description3 = timeSlots[2].description
                }
                sendInvitation(invitationDetails)
            }
        });

        function validateUserGoogleAccount(userInfo){
            if(checkRequredField(userInfo.google)){
                if(checkRequredField(userInfo.google[0])){
                    if(checkRequredField(userInfo.google[0].emailId)){
                        return true;
                    }else  return false;
                }else  return false;
            }else  return false;
        }

        function sendInvitation(invitationDetails){
            if(checkRequiredField(invitationDetails.start1)){

                if(validateUserGoogleAccount(widgetUserProfile)){
                    invitationDetails.receiverPrimaryEmailId = widgetUserProfile.google[0].emailId;
                }

                invitationDetails.senderTimezone = loggedInUserTimezone || timezone || getTimezone();
                $.ajax({
                    url:rww.getDomain()+'/sendInvitation',
                    type:'POST',
                    datatype:'JSON',
                    data:invitationDetails,
                    traditional:true,
                    success:function(result){

                        if (result == 'loginRequired') {
                            alert('Login required');
                        }
                        if (result == 'success') {
                            alert("Congratulations! Your meeting invitation has been sent. Confirmation mail has been sent to your email id.")
                        }
                        if (result == 'error') {
                            alert("An error occurred while sending your invitation. Please try again")
                        }
                    },
                    error: function (event, request, settings) {
                        var status = event.status;
                        switch(status){
                            case 4010:
                                console.error("please specify all fields");
                                alert("Error occurred while sending your invitation. Please try again")
                                break;
                        }
                    },
                    timeout: 20000
                })
            }
            else{
                alert("Please select time slots on the calendar before sending the invitation.")
            }
        }
    }

    function checkTime(time){
        if(time == '00:00' || time == '' || time == null || time == undefined){
            return false;
        }else{
            return true;
        }
    }

    function getTimezone(){
        var tz = jstz.determine();
        timezone = tz.name();
        return timezone;
    }


    function loadUserBusyTimes(publicProfile,timezone,timezone2,dateGiven){

        var start1Arr,end1Arr,start2Arr,end2Arr;
        var startTime1 = publicProfile.calendarAccess.timeOne.start;
        var endTime1 = publicProfile.calendarAccess.timeOne.end;
        var startTime2 = publicProfile.calendarAccess.timeTwo.start;
        var endTime2 = publicProfile.calendarAccess.timeTwo.end;

        if(checkRequiredField(startTime1) && checkRequiredField(endTime1)){
            start1Arr = startTime1.split(':');
            end1Arr = endTime1.split(':');
        }
        if(checkRequiredField(startTime2) && checkRequiredField(endTime2)){
            start2Arr = startTime2.split(':');
            end2Arr = endTime2.split(':');
        }

        if(publicProfile.calendarAccess.calendarType == 'selectPublic'){
            var dateBusy = dateGiven;

            for(var i=0;i<=0;i++){
                if(i == 0){

                }else dateBusy.setDate(dateBusy.getDate()+1);

                var dn = dateBusy.getDate();
                var mn = dateBusy.getMonth();
                var yn = dateBusy.getFullYear();

                if(checkTime(publicProfile.calendarAccess.timeOne.end) && checkTime(publicProfile.calendarAccess.timeTwo.start)){
                    var t1,t2,t3,t4,t5,t6;

                    if(checkRequiredField(timezone2)){

                        var date = moment(dateBusy).tz(timezone2)

                        var d = date.date();
                        var m = date.month();
                        var y = date.year();

                        var datet1 = date.clone();
                        var datet2 = date.clone();
                        var datet3 = date.clone();
                        var datet4 = date.clone();
                        var datet5 = date.clone();
                        var datet6 = date.clone();

                        datet1.hours(0)
                        datet1.minutes(0)
                        datet1.seconds(0)
                        datet1.milliseconds(0)
                        datet1.year(y)
                        datet1.month(m)
                        datet1.date(d)
                        t1 = datet1.tz(timezone || getTimezone()).toDate();

                        datet2.hours(parseInt(start1Arr[0]))
                        datet2.minutes(0)
                        datet2.seconds(0)
                        datet2.milliseconds(0)
                        datet2.year(y)
                        datet2.month(m)
                        datet2.date(d)
                        t2 = datet2.tz(timezone || getTimezone()).toDate();

                        datet3.hours(parseInt(end1Arr[0]))
                        datet3.minutes(0)
                        datet3.seconds(0)
                        datet3.milliseconds(0)
                        datet3.year(y)
                        datet3.month(m)
                        datet3.date(d)
                        t3 = datet3.tz(timezone || getTimezone()).toDate();


                        datet4.hours(parseInt(start2Arr[0]))
                        datet4.minutes(0)
                        datet4.seconds(0)
                        datet4.milliseconds(0)
                        datet4.year(y)
                        datet4.month(m)
                        datet4.date(d)
                        t4 = datet4.tz(timezone || getTimezone()).toDate();


                        datet5.hours(parseInt(end2Arr[0]))
                        datet5.minutes(0)
                        datet5.seconds(0)
                        datet5.milliseconds(0)
                        datet5.year(y)
                        datet5.month(m)
                        datet5.date(d)
                        t5 = datet5.tz(timezone || getTimezone()).toDate();


                        datet6.hours(23)
                        datet6.minutes(59)
                        datet6.seconds(0)
                        datet6.milliseconds(0)
                        datet6.year(y)
                        datet6.month(m)
                        datet6.date(d)
                        t6 = datet6.tz(timezone || getTimezone()).toDate();


                    }else{
                        t1 = moment(new Date(yn,mn,dn,0,0,0,0,0)).tz(timezone || getTimezone()).toDate();
                        t2 = moment(new Date(yn,mn,dn,parseInt(start1Arr[0]),0,0,0,0)).tz(timezone || getTimezone()).toDate()
                        t3 = moment(new Date(yn,mn,dn,parseInt(end1Arr[0]),0,0,0,0)).tz(timezone || getTimezone()).toDate()
                        t4 = moment(new Date(yn,mn,dn,parseInt(start2Arr[0]),0,0,0,0)).tz(timezone || getTimezone()).toDate()
                        t5 = moment(new Date(yn,mn,dn,parseInt(end2Arr[0]),0,0,0,0)).tz(timezone || getTimezone()).toDate()
                        t6 = moment(new Date(yn,mn,dn,24,0,0,0,0)).tz(timezone || getTimezone()).toDate()
                    }

                    if(!moment(t1).isSame(t2)){
                        if(checkTime(publicProfile.calendarAccess.timeOne.start) || checkTime(publicProfile.calendarAccess.timeOne.start)){
                            busySlots.push({
                                start:t1,
                                end:t2
                            });
                        }
                    }
                    if(!moment(t3).isSame(t4)) {
                        if(checkTime(publicProfile.calendarAccess.timeOne.end) || checkTime(publicProfile.calendarAccess.timeTwo.start)){
                            busySlots.push({
                                start:t3,
                                end:t4
                            });
                        }
                    }
                    if(!moment(t5).isSame(t6)) {
                        if(checkTime(publicProfile.calendarAccess.timeTwo.end) || checkTime(publicProfile.calendarAccess.timeTwo.start)){
                            busySlots.push({
                                start:t5,
                                end:t6
                            });
                        }
                    }

                } else if(checkTime(publicProfile.calendarAccess.timeOne.start) || checkTime(publicProfile.calendarAccess.timeOne.end)){

                    var t7,t8,t9,t10;
                    if(checkRequiredField(timezone2)){

                        var date2 = moment(dateBusy).tz(timezone2)

                        var d2 = date2.date();
                        var m2 = date2.month();
                        var y2 = date2.year();

                        var datet7 = date2.clone();
                        var datet8 = date2.clone();
                        var datet9 = date2.clone();
                        var datet10 = date2.clone();

                        datet7.hours(0)
                        datet7.minutes(0)
                        datet7.seconds(0)
                        datet7.milliseconds(0)
                        datet7.year(y2)
                        datet7.month(m2)
                        datet7.date(d2)
                        t7 = datet7.tz(timezone || getTimezone()).toDate();


                        datet8.hours(parseInt(start1Arr[0]))
                        datet8.minutes(0)
                        datet8.seconds(0)
                        datet8.milliseconds(0)
                        datet8.year(y2)
                        datet8.month(m2)
                        datet8.date(d2)
                        t8 = datet8.tz(timezone || getTimezone()).toDate();

                        datet9.hours(parseInt(end1Arr[0]))
                        datet9.minutes(0)
                        datet9.seconds(0)
                        datet9.milliseconds(0)
                        datet9.year(y2)
                        datet9.month(m2)
                        datet9.date(d2)
                        t9 = datet9.tz(timezone || getTimezone()).toDate();

                        datet10.hours(23)
                        datet10.minutes(59)
                        datet10.seconds(0)
                        datet10.milliseconds(0)
                        datet10.year(y2)
                        datet10.month(m2)
                        datet10.date(d2)
                        t10 = datet10.tz(timezone || getTimezone()).toDate();



                    }else{
                        t7 = moment(new Date(yn,mn,dn,0,0,0,0,0)).tz(timezone || getTimezone()).toDate()
                        t8 = moment(new Date(yn,mn,dn,parseInt(start1Arr[0]),0,0,0,0)).tz(timezone || getTimezone()).toDate()
                        t9 = moment(new Date(yn,mn,dn,parseInt(end1Arr[0]),0,0,0,0)).tz(timezone || getTimezone()).toDate()
                        t10 = moment(new Date(yn,mn,dn,24,0,0,0,0)).tz(timezone || getTimezone()).toDate()

                    }

                    if(!moment(t7).isSame(t8)) {
                        busySlots.push({
                            start:t7,
                            end:t8
                        })
                    }
                    if(!moment(t9).isSame(t10)) {
                        busySlots.push({
                            start:t9,
                            end:t10
                        })
                    }
                }
            }
        }
        loadWorkHours(publicProfile,timezone,timezone2,new Date());

    }

    function loadWorkHours(publicProfile,timezone,timezone2,dateGiven){
        if(checkRequiredField(publicProfile.workHours)){
            if(publicProfile.workHours.start == '00:00' && publicProfile.workHours.end == '00:00'){

            }
            else{
                var eventSource = [];
                var dateBusy = dateGiven;

                var workHoursStartArr = publicProfile.workHours.start.split(':');
                var workHoursEndArr = publicProfile.workHours.end.split(':');
                var date = new Date();
                var start = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(workHoursStartArr[0]),parseInt(workHoursStartArr[1]),0,0);
                var end = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(workHoursEndArr[0]),parseInt(workHoursEndArr[1]),0,0);
                if(start < end){

                    for(var i=0;i<=0;i++) {
                        if (i == 0) {

                        } else dateBusy.setDate(dateBusy.getDate() + 1);

                        var d = dateBusy.getDate();
                        var m = dateBusy.getMonth();
                        var y = dateBusy.getFullYear();

                        var t1,t2,t3,t4;

                        if(checkRequiredField(timezone2)){

                            var date4 = moment(dateBusy).tz(timezone2)

                            var d3 = date4.date();
                            var m3 = date4.month();
                            var y3 = date4.year();

                            var datet1 = date4.clone();
                            var datet2 = date4.clone();
                            var datet3 = date4.clone();
                            var datet4 = date4.clone();

                            datet1.hours(0)
                            datet1.minutes(0)
                            datet1.seconds(0)
                            datet1.milliseconds(0)
                            datet1.year(y3)
                            datet1.month(m3)
                            datet1.date(d3)
                            t1 = datet1.tz(timezone || getTimezone()).toDate();


                            datet2.hours(parseInt(workHoursStartArr[0]))
                            datet2.minutes(0)
                            datet2.seconds(0)
                            datet2.milliseconds(0)
                            datet2.year(y3)
                            datet2.month(m3)
                            datet2.date(d3)
                            t2 = datet2.tz(timezone || getTimezone()).toDate();


                            datet3.hours(parseInt(workHoursEndArr[0]))
                            datet3.minutes(parseInt(workHoursEndArr[1]))
                            datet3.seconds(0)
                            datet3.milliseconds(0)
                            datet3.year(y3)
                            datet3.month(m3)
                            datet3.date(d3)
                            t3 = datet3.tz(timezone || getTimezone()).toDate();


                            datet4.hours(23)
                            datet4.minutes(59)
                            datet4.seconds(0)
                            datet4.milliseconds(0)
                            datet4.year(y3)
                            datet4.month(m3)
                            datet4.date(d3)
                            t4 = datet4.tz(timezone || getTimezone()).toDate();


                        }else{
                            t1 = moment(new Date(y,m,d,0,0,0,0,0)).tz(timezone || getTimezone()).toDate();
                            t2 = moment(new Date(y,m,d,parseInt(workHoursStartArr[0]),0,0,0,0)).tz(timezone || getTimezone()).toDate()
                            t3 = moment(new Date(y,m,d,parseInt(workHoursEndArr[0]),0,0,0,0)).tz(timezone || getTimezone()).toDate()
                            t4 = moment(new Date(y,m,d,24,0,0,0,0)).tz(timezone || getTimezone()).toDate()
                        }

                        if(publicProfile.workHours.start != '00:00' || publicProfile.workHours.end != '23:59'){
                            if(!moment(t1).isSame(t2)) {

                                busySlots.push({
                                    start:t1,
                                    end:t2
                                });

                            }
                            if(!moment(t3).isSame(t4)) {

                                busySlots.push({
                                    start:t3,
                                    end:t4
                                });

                            }
                        }
                    }
                }
            }
        }
        displayEmptySlots(busySlots);
    }

    function displayEmptySlots(busyList){
       var today = moment(new Date()).tz(widgetUserTimezone);

        today.hour(0)
        today.minute(0)
        today.second(0)
        today.millisecond(0)

        var loopDateStart = today.clone();
        var loopDateEnd = today.clone();
        loopDateEnd.minute(loopDateEnd.minute()+30);

        var daySlots = [
            {
                start:loopDateStart.clone().toDate(),
                end:loopDateEnd.clone().toDate()
            }
        ];
        validateBusyTime(daySlots[0],busyList)
        for(var i=0; i<47; i++){
            validateBusyTime({
                start:loopDateStart.minute(loopDateStart.minute()+30).toDate(),
                end: loopDateEnd.minute(loopDateEnd.minute()+30).toDate()
            },busyList);

        }
    }

    function validateBusyTime(obj,objList) {

        var t = true;
        for(var i=0; i<objList.length;i++){
            if (obj.start <= objList[i].start) {
                if (obj.end <= objList[i].start) {

                } else t = false;
            }
            else {
                if (obj.start >= objList[i].end) {

                } else t = false;
            }
        }
        if(t){
            JQ("#date-time").append('<li>'+obj.start.getHours()+':'+obj.start.getMinutes()+'----'+obj.end.getHours()+':'+obj.end.getMinutes()+'</li>')
        }
    }

    function checkRequiredField(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }

})(); // We call our anonymous function immediately
