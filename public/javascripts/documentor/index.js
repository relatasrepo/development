function linedraw(ax,ay,bx,by) {
    if(ay>by)
    {
        bx=ax+bx;
        ax=bx-ax;
        bx=bx-ax;
        by=ay+by;
        ay=by-ay;
        by=by-ay;
    }
    var calc=Math.atan((ay-by)/(bx-ax));
    calc=calc*180/Math.PI;
    var length=Math.sqrt((ax-bx)*(ax-bx)+(ay-by)*(ay-by));


    var ultaCalc = Math.atan((bx-ax)/(ay-by))
    ultaCalc=ultaCalc*180/Math.PI;

    var counter = 0;
    var zIndexCss = "z-index:"+99999+";";
    var lengthCss = "width:" + length+"px;";
    var posCss = "height:1px;background-color:orange;position:absolute;"
    var topCss = "top:"+ay+"px;";
    var leftCss = "left:"+ax+"px;";
    var transFormCss = "transform:rotate(" + calc + "deg);" +
        "-ms-transform:rotate(" + calc + "deg);" +
        "transform-origin:0% 0%;" +
        "-moz-transform:rotate(" + calc + "deg);" +
        "-moz-transform-origin:0% 0%;" +
        "-webkit-transform:rotate(" + calc  + "deg);" +
        "-webkit-transform-origin:0% 0%;" +
        "-o-transform:rotate(" + calc + "deg);" +
        "-o-transform-origin:0% 0%;";

    var transformHorizontal = "transform:rotate(" + 90 + "deg);" +
        "-ms-transform:rotate(" + 90 + "deg);" +
        "transform-origin:0% 0%;" +
        "-moz-transform:rotate(" + 90 + "deg);" +
        "-moz-transform-origin:0% 0%;" +
        "-webkit-transform:rotate(" + 90  + "deg);" +
        "-webkit-transform-origin:0% 0%;" +
        "-o-transform:rotate(" + 90 + "deg);" +
        "-o-transform-origin:0% 0%;";

    var style = 'style='+zIndexCss+lengthCss+posCss+topCss+leftCss;

    console.log(ax,ay,bx,by);
    console.log(style)

    function checkDOMExists(){

        counter++;
        if(document.body){
            document.body.innerHTML += "<div id='line'" + style + "></div>"
        } else {
            setTimeout(function(){
                checkDOMExists();
            },1000)
        }
    }

    checkDOMExists();

}

function getImage(className,isId) {

    $(function() {
        var identifier = "."+className;

        if(isId){
            identifier = "#"+className
        }
        console.log(identifier);

        html2canvas($(identifier), {
            onrendered: function(canvas) {
                console.log(canvas)
                theCanvas = canvas;
                document.body.appendChild(canvas);

                // Convert and download as image
                Canvas2Image.saveAsPNG(canvas);
                $("#documentor").append(canvas);
                // Clean up
                //document.body.removeChild(canvas);
            }
        });
    });
}

var arrayWithElements = [];

var tempCounter = 0;
var ax,ay,bx,by =0;

function clickListener(e) {
    var clickedElement=(window.event)
            ? window.event.srcElement
            : e.target,
        tags=document.getElementsByTagName(clickedElement.tagName);

    for(var i=0;i<tags.length;++i)
    {
        if(tags[i]==clickedElement)
        {
            arrayWithElements.push({tag:clickedElement.tagName,index:i});
            // console.log(arrayWithElements);
        }
    }

    toImage();

    tempCounter++;

    if(tempCounter == 1){
        var posOne = printMousePos(e);
        ax = posOne.x;
        ay = posOne.y;
    }

    if(tempCounter == 2){

        var posTwo = printMousePos(e);
        bx = posTwo.x;
        by = posTwo.y;

        //Now draw
        // linedraw(ax,ay,bx,by);
    }



}

function toImage(id){
    html2canvas($('.dropdown-toggle'), {
        onrendered: function(canvas) {
            var img = canvas.toDataURL()

            var theDiv = document.getElementById("some-id");
            theDiv.appendChild(canvas);
        }
    });
}

function addCustomClass(e){
    e.preventDefault(); // prevent default action
    this.class = "divBtn"; // add id if it doesn't have one
}

function printMousePos(event) {

    return {
        x:event.clientX,
        y:event.clientY
    }
}

document.onclick = clickListener;