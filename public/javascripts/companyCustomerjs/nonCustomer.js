var defaultNoOfDays = 180
reletasApp.controller("dropWatchController", function($rootScope, $window){
      var numberOfDays = {}
      setInterval(function(){
        if($window.noOfDays)
          Object.keys($window.noOfDays).forEach(function(key){
            if(numberOfDays[key] !== $window.noOfDays[key]){
              if(isNaN($window.noOfDays[key]))
                $window.noOfDays[key] = 0
              $rootScope.$broadcast(key + "DaysChanged", $window.noOfDays[key])
              numberOfDays[key] = $window.noOfDays[key] 
            }
          })
      }, 1000)

})

reletasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/customer/middle/bar"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        //searchContent = searchContent.replace(/[^\w\s]/gi, '');

        var str = encodeURIComponent(searchContent);

            if(typeof str == 'string' && str.length > 0){
                if(searchContent.length >= 3) {
                    yourNetwork = yourNetwork ? true : false;
                    extendedNetwork = extendedNetwork ? true : false;
                    forCompanies = forCompanies ? true : false;
                    window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;

                }
            }
            else toastr.error("Please enter search content")

    };
});
var timezone;
reletasApp.controller("logedinUser", function ($scope, $http, $rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {

            if(response.SuccessCode){
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.l_usr = response.Data;
                $rootScope.currentUser = response.Data;
                $rootScope.$broadcast("fetchedUserProfile", response.Data.corporateUser)
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }

                if(response.Data.contactsUpload && response.Data.contactsUpload.isUploaded){

                }else $("#uploadContactsAlert").removeClass("hide");
            }
            else{

            }
        }).error(function (data) {

    })
});

reletasApp.controller("checkCorporateUser", function ($scope, $http, $rootScope) {
    $http.get('/profile/get/current/web')
        .success(function (response) {
            if (response.SuccessCode) {
                $scope.isCorporateUser = response.Data.corporateUser
            }
        });
});

reletasApp.controller("left_contacts_bar", function ($scope, $http, $rootScope, share) {
    $scope.contactsList = [];
    $scope.currentLength = 0;
    $scope.lastFetched = 0;
    $scope.filterBy = 'all';
    $scope.show_contacts_back = false;
    $scope.show_filter = false;
    $scope.usersInHierarchy = []
    var isFilter = false;

    $scope.searchContacts_filter = function(searchContent,checkedBoxes){

        var checked = [];
        for(var type in checkedBoxes){
            if(checkedBoxes[type]){
                checked.push(type);
            }
        }
        if(checked.length > 0){
            $scope.filterBy = checked.join(',')
            isFilter = true;
            $scope.contactsList = [];
            $scope.currentLength = 0;
            var url = fetchUrlWithParameter("/company/middle/bar/accounts", "hierarchylist", $scope.usersInHierarchy)
            $scope.getContacts(url,0,25,$scope.filterBy,searchContent);
        }
        else{
            isFilter = false;
            $scope.refreshContactsList()
        }
    };

    $scope.searchContacts = function(searchContent){
        if(checkRequired(searchContent)){
            $scope.filterBy = isFilter ? $scope.filterBy : 'search';
            $scope.contactsList = [];
            $scope.currentLength = 0;
            var url = fetchUrlWithParameter("/company/middle/bar/accounts", "hierarchylist", $scope.usersInHierarchy)
            $scope.getContacts(url,0,25,$scope.filterBy,searchContent);
        }
    };

    $scope.refreshContactsList = function(){
        $scope.filterBy = isFilter ? $scope.filterBy : 'all';
        $scope.searchContent = "";
        $(".searchContentClass").val("");
        $scope.contactsList = [];
        $scope.currentLength = 0;
        var url = fetchUrlWithParameter("/company/middle/bar/accounts", "hierarchylist", $scope.usersInHierarchy)
        $scope.getContacts(url,0,25,$scope.filterBy,'');
    };

    $scope.getContacts = function(url,skip,limit,filterBy,searchContent,isBack){
        if(url.indexOf("?") == -1)
          url = url + "?"
        else
          url = url + "&"
        if(checkRequired(searchContent)){
            url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy+'&searchContent='+searchContent
        }
        else url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy;

        $http.get(url)
            .success(function(response){
                $scope.contactsList = [];
                $scope.isConnectionOpen = false;
                if(response.SuccessCode){

                    $scope.grandTotal = response.Data.total;
                    if(skip == 0 && !checkRequired(searchContent))
                      $rootScope.$broadcast("totalCompanies", $scope.grandTotal )
                    if(response.Data.contacts.length > 0){

                        if(!isBack || $scope.currentLength <= 0){
                            $scope.currentLength += response.Data.returned;
                            $scope.returned = response.Data.returned
                        }

                        if($scope.currentLength > 25){
                            $scope.show_contacts_back = true;
                        }else $scope.show_contacts_back = false;
                        for(var i=0; i<response.Data.contacts.length; i++){

                            var li = '';
                            var obj;

                            obj = {
                                userId:response.Data.contacts[i]._id,
                                fullName:response.Data.contacts[i]._id,
                                companyName:$scope.validNum(response.Data.contacts[i].contactsCount)+" Contacts",
                                designation:"",
                                fullCompanyName:$scope.validNum(response.Data.contacts[i].contactsCount)+" Contacts",
                                fullDesignation:"",
                                name:getTextLength(response.Data.contacts[i]._id,25),
                                emailId:response.Data.contacts[i]._id,
                                noPicFlag:true,
                                image:"https://logo.clearbit.com/"+ getTextLength(response.Data.contacts[i]._id,25) +".com",
                                noPPic:(response.Data.contacts[i]._id.substr(0,2)).capitalizeFirstLetter(),
                                cursor:'cursor:default',
                                url:null,
                                idName:'com_con_item_'+response.Data.contacts[i]._id
                            };
                            $scope.contactsList.push(obj);
                            $http.get("/companylogos/" + getTextLength(response.Data.contacts[i]._id,25) +".com")
                        }
                    }
                    else {
                        $scope.isAllConnectionsLoaded = true;
                        //$scope.commonConnectionsNotExist = response.Message;
                    }
                }
                else{
                    $scope.isAllConnectionsLoaded = true;
                    //$scope.commonConnectionsNotExist = response.Message;
                }
            })
    };

    $scope.validNum = function(num){
        return num < 10 ? '0'+num : num;
    }

    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    $scope.getContacts('/company/middle/bar/accounts',0,25,$scope.filterBy);

    $scope.loadMoreContacts = function(skip,limit,searchContent,isBack){
        if(isBack){
            $scope.currentLength = $scope.currentLength-25;
            skip = skip-25
        }
        if(skip < 25){
            skip = 0
            $scope.currentLength = 0;
        }
        $scope.getContacts('/company/middle/bar/accounts',skip,limit,$scope.filterBy,searchContent,isBack);
    };

    $scope.calculateFromTo = function(current){
        return current < 0 ? 0 : current
    };

    $scope.showInteractions = function(emailId,companyName,designation,name,url,userId,disableContext){

        isGetContactInfo(function(){
            share.getContactInfo(emailId,companyName,designation,name,url,userId);
        });
    };

    $scope.accountSelected = function(account){
        window.location = '/customer/select?account='+account.userId
    };

    function isGetContactInfo(callback){
        if(typeof share.getContactInfo == 'function'){
            callback();
        }
        else{
            setTimeout(function(){
                return isGetContactInfo(callback);
            },100)
        }
    }

    $rootScope.$on("hierarchyModified", function(event, data){
      $scope.usersInHierarchy = data
      $scope.refreshContactsList()
    })
});

reletasApp.controller("contacts_data_controller",function($scope, $http, share){
    $scope.selectFile = function(){
        $("#selectedFile").trigger("click");
    };
    $scope.upload = function(file){
        var formData = new FormData();
        formData.append('contacts', file, file.name);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/contacts/upload/file', true);
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            var response = JSON.parse(xhr.response)
            if(response.SuccessCode){
                $("#uploadContactsAlert").addClass("hide");
                toastr.success(response.Message)
            }
            else{
                toastr.error(response.Message)
            }
        };
        // Send the Data.
        xhr.send(formData);
    };
    $("#selectedFile").on("change",function(){
        if($(this)[0] && $(this)[0].files && $(this)[0].files.length > 0){
            $scope.upload($(this)[0].files[0])
        }
        else toastr.error("Please select file to upload")
    });

    $scope.mySearchFunction = function(searchContent){
        if(typeof searchContent == 'string' && checkRequired(searchContent.trim())){
            $scope.searchFromHeader(searchContent,false,true,false)
            //window.location = '/contact/connect?searchContent='+searchContent;
        }
    }
    $scope.goToContactsSearch = function(searchContent){
        if(typeof searchContent == 'string' && checkRequired(searchContent.trim())){
            $scope.searchFromHeader(searchContent,false,true,false)
            //window.location = '/contact/connect?searchContent='+searchContent;
        }
    }

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        searchContent = searchContent.replace(/[^\w\s]/gi, '');

        if(typeof searchContent == 'string' && searchContent.length > 0){
            if(searchContent.length >= 3) {
                yourNetwork = yourNetwork ? true : false;
                extendedNetwork = extendedNetwork ? true : false;
                forCompanies = forCompanies ? true : false;
                window.location = '/contact/connect?searchContent=' + searchContent + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
            }
        }
        else toastr.error("Please enter search content")
    };
});

reletasApp.controller("dashboard_past_7_day_interactions_by_contact_type", function ($scope, $http, $rootScope, share) {
    $rootScope.$on("hierarchyModified", function(event, data){
      paintInteractionStatus(data)
    })
    $rootScope.$on("totalCompanies", function(event, data){
      $scope.companies = data
    })
    function paintInteractionStatus(usersInHierarchy){
      var url = fetchUrlWithParameter("/company/past/days/interacted/contact/type", "hierarchylist", usersInHierarchy)
      $http.get(url)
        .success(function (response) {
          if(response.SuccessCode){
            //$scope.companies = $scope.getValidStringNumber(response.Data.companies || 0);

            if(response.Data.types && response.Data.types.length > 0){
              $scope.customer = $scope.getValidStringNumber(response.Data.types[0].customer || 0);
              $scope.prospect = $scope.getValidStringNumber(response.Data.types[0].prospect || 0);
              $scope.influencer = $scope.getValidStringNumber(response.Data.types[0].influencer || 0);
              $scope.decision_maker = $scope.getValidStringNumber(response.Data.types[0].decision_maker || 0);
              $scope.favorite = $scope.getValidStringNumber(response.Data.types[0].favorite || 0);
            }
            else {
              $scope.customer = $scope.getValidStringNumber(0);
              $scope.prospect = $scope.getValidStringNumber(0);
              $scope.influencer = $scope.getValidStringNumber(0);
              $scope.decision_maker = $scope.getValidStringNumber(0);
              $scope.favorite = $scope.getValidStringNumber(0);
            }
          }
          else{
            //$scope.companies = $scope.getValidStringNumber(0);
            $scope.favorite = $scope.getValidStringNumber(0);
            $scope.customer = $scope.getValidStringNumber(0);
            $scope.prospect = $scope.getValidStringNumber(0);
            $scope.influencer = $scope.getValidStringNumber(0);
            $scope.decision_maker = $scope.getValidStringNumber(0);
          }
        }).error(function (data) {

        });
    }


    paintInteractionStatus()

    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };
});

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue){
  if(parameterValue instanceof Array)
    if(parameterValue.length > 0)
      parameterValue = parameterValue.join(",")
    else
      parameterValue = null

  if(parameterValue != undefined && parameterValue != null){
    if(baseUrl.indexOf("?") == -1)
     baseUrl += "?" 
    else
     baseUrl += "&" 
    baseUrl+= parameterName + "=" + parameterValue
  }

  return baseUrl 
}

reletasApp.controller("top_companies_locations_interaction_by_type", function ($scope, $http, share, $rootScope, $window) {
    $scope.interactionsByCompany = [];
    $scope.interactionsByLocation = [];
    $scope.interactionsByType = [];
    $scope.totalInteractionsCount = 0;
    var dropType = "interactions"
    
    $rootScope.$on("hierarchyModified", function(event, data){
      paintCompaniesInteracted(data, $window.noOfDays[dropType] || defaultNoOfDays)
      paintLocationsInteracted(data, $window.noOfDays[dropType] || defaultNoOfDays)
      paintTypeInteracted(data, $window.noOfDays[dropType] || defaultNoOfDays)
    })
    $rootScope.$on("interactionsDaysChanged", function(event, data){
      var localHierarchy = $rootScope.usersInHierarchy || null
      paintCompaniesInteracted(localHierarchy, data)
      paintLocationsInteracted(localHierarchy, data)
      paintTypeInteracted(localHierarchy, data)
    })

    paintCompaniesInteracted(null,defaultNoOfDays)
    paintLocationsInteracted(null,defaultNoOfDays)
    paintTypeInteracted(null, defaultNoOfDays)
    function paintCompaniesInteracted(usersInHierarchy, days){
      //var url = fetchUrlWithParameter("/company/past/days/interacted/company/", "hierarchylist", usersInHierarchy)
      var url = fetchUrlWithParameter("/company/heatmap/", "hierarchylist",usersInHierarchy)
      url = fetchUrlWithParameter(url, "days", days)
      $http.get(url).success(function( response){

          if(response.SuccessCode)

          var sortedArrTopFive = sortObject(response.Data)

          if(response.Data.length>5){
              sortedArrTopFive = response.Data.slice(0,5)
          }

          if(sortedArrTopFive && sortedArrTopFive.length > 0){
              var maxCount = sortedArrTopFive[0].mailCount;
              $scope.interactionsByCompany = []
              sortedArrTopFive.forEach(function(item){
                  var obj = {
                      companyName:item.company,
                      count:item.mailCount,
                      percentage:'width:'+$scope.calculatePercentage(item.mailCount,maxCount,100)+'%'
                  };
                  $scope.interactionsByCompany.push(obj)
                  //$scope.calculatePercentage = function(count,total,percentageFor){
                  //    return Math.round((count*percentageFor)/total);
                  //};
              });
          }else{
            $scope.interactionsByCompany = []
          }
      })    
    }
    function paintLocationsInteracted(usersInHierarchy, days){
      var url = fetchUrlWithParameter("/company/past/days/interacted/location/", "hierarchylist", usersInHierarchy)
      //var url = fetchUrlWithParameter("/company/heatmap/", "hierarchylist",usersInHierarchy)
      url = fetchUrlWithParameter(url, "days", days)
      $http.get(url).success(function( response){
        if(response.SuccessCode)
          if(response.Data && response.Data.interactionsByLocation && response.Data.interactionsByLocation.length > 0 && response.Data.interactionsByLocation[0] && response.Data.interactionsByLocation[0].typeCounts && response.Data.interactionsByLocation[0].typeCounts.length > 0){
              $scope.interactionsByLocation = {
                  "name": "",
                  "children": []
              };
              response.Data.interactionsByLocation[0].typeCounts.forEach(function(item){
                  var obj = {
                      name:item._id,
                      count:item.count,
                      mcap:$scope.calculatePercentage(item.count,response.Data.interactionsByLocation[0].totalCount,100)
                  };
                  $scope.interactionsByLocation.children.push(obj);
              });
              fillLocations($scope.interactionsByLocation)
          }else{
            $scope.interactionsByLocation = {
              "name": "",
              "children": []
            };
            //fillLocations($scope.interactionsByLocation)
            $( ".treemap" ).empty()
          }

      })
    }    

    function paintTypeInteracted(usersInHierarchy, days){
      var url = fetchUrlWithParameter("/company/past/days/interacted/type/", "hierarchylist", usersInHierarchy)
      url = fetchUrlWithParameter(url, "days", days)
      $http.get(url).success(function( response){

          if(response.SuccessCode)

          if(response.Data && response.Data.interactionsByType && response.Data.interactionsByType.length > 0 && response.Data.interactionsByType[0] && response.Data.interactionsByType[0].typeCounts && response.Data.interactionsByType[0].typeCounts.length > 0){
              $scope.totalInteractionsCount = response.Data.interactionsByType[0].totalCount || 0;
              var existingTypes = [];
              var allTypes = ['meeting','call','sms','email','facebook','twitter','linkedin'];
              $scope.interactionsByType = []
              for(var t=0; t<response.Data.interactionsByType[0].typeCounts.length; t++){
                  var interactionTypeObj = $scope.getInteractionTypeObj(response.Data.interactionsByType[0].typeCounts[t],response.Data.interactionsByType[0].maxCount,100);
                  if(interactionTypeObj != null){
                      existingTypes.push(interactionTypeObj.type);
                      $scope.interactionsByType.push(interactionTypeObj);
                  }
              }
              var mobileCountStatus = 0;
              var mobileCountStatusLists = ['call','sms','email'];
              for(var u=0; u<allTypes.length; u++){
                  if(existingTypes.indexOf(allTypes[u]) == -1){
                      var newObj = $scope.getInteractionTypeObj({_id:allTypes[u],count:0},response.Data.interactionsByType[0].maxCount,100)
                      if(newObj != null){
                          if(mobileCountStatusLists.indexOf(allTypes[u]) != -1){
                              mobileCountStatus ++;
                          }
                          $scope.interactionsByType.push(newObj);
                      }
                  }
              }

              if(mobileCountStatus > 0){
                  $scope.showDownloadMobileApp = true;
              }else $scope.showDownloadMobileApp = false;
              $scope.showSocialSetup = response.Data.showSocialSetup;

              $scope.interactionsByType.sort(function (o1, o2) {
                  return o1.priority < o2.priority ? -1 : o1.priority > o2.priority ? 1 : 0;
              });

              $scope.interactionsByType = {
                  a:$scope.interactionsByType[0],
                  b:$scope.interactionsByType[1],
                  c:$scope.interactionsByType[2],
                  d:$scope.interactionsByType[3],
                  e:$scope.interactionsByType[4],
                  f:$scope.interactionsByType[5],
                  g:$scope.interactionsByType[6]
              };
          }else{
            $scope.interactionsByType = []
            $scope.totalInteractionsCount = 0
          }


      })
    }    


    $scope.getInteractionTypeObj = function(obj,total,percentageFor){
        switch (obj._id){
            case 'google-meeting':
            case 'meeting':return {
                priority:0,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'call':return {
                priority:1,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'sms':return {
                priority:2,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'email':return {
                priority:3,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'facebook':return {
                priority:4,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'twitter':return {
                priority:5,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'linkedin':return {
                priority:6,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            default : return null;
        }
    };

    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };
    $scope.calculatePercentage = function(count,total,percentageFor){
        return Math.round((count*percentageFor)/total);
    };
});

reletasApp.controller("heatMapController", function($scope, $http, $rootScope, $window){
  function paintHeatMap(usersInHierarchy, days){
    var url = fetchUrlWithParameter("/company/heatmap/", "hierarchylist",usersInHierarchy)
    url = fetchUrlWithParameter(url, "days", days)
    $http.get(url).success(function(data){
      var jsonfile = data.Data;
        jsonfile = jsonfile.sort(function(a,b){return a.mailCount - b.mailCount});
          var average = d3.mean(jsonfile.map(function(i) {
              return (i.lastInteractedInDays+ i.mailCount)/2;
          }));

          var chart_scatterplot = c3.generate({
              tooltip: {
                  contents: function(d, defaultTitleFormat, defaultValueFormat, color) {
                      var company = jsonfile[d[0].index].company;
                      var mailCount = jsonfile[d[0].index].mailCount;
                      var lastInteractedInDays = jsonfile[d[0].index].lastInteractedInDays;
                      var lastInteractedDate = jsonfile[d[0].index].lastInteractedDate;

                      var companyData = "<table class='data-c3-table'><tr><th colspan='2'>"+company+"</th></tr><tr><td>Interactions<br>" + mailCount + "</td><td>Last Interacted Day<br>" + lastInteractedDate + "</td></tr></table>"
                      return companyData;
                  }
              },
              point: {
                  r: 7
              },
              data: {
                  json: jsonfile,
                  keys: {
                      x: 'mailCount',
                      value: ['lastInteractedInDays', 'company']
                  },
                  color: function(color, d) {
                      if (d.value > average) {
                          return "#F86A52"
                      } else {
                          return "#49B5A6"
                      };
                  },
                  type: 'scatter',
                  onclick: function (d) {
                      var company = jsonfile[d.index].company.toLowerCase();
                      window.location = '/customer/select?account='+company
//                          updateCompany(jsonfile[d.index]);
                  }
              },
              axis: {
                  x: {
                      label: 'Interactions',
                      tick: {
                          fit: false
                      }
                  },
                  y: {
                      label: 'Days'
                  }
              },
              legend: {
                  show: false
              }
          });
    
    })
    //$.getJSON("/company/heatmap", function(data){
      //var jsonfile = data.Data;
        //jsonfile = jsonfile.sort(function(a,b){return a.mailCount - b.mailCount});
          //var average = d3.mean(jsonfile.map(function(i) {
              //return (i.lastInteractedInDays+ i.mailCount)/2;
          //}));

          //var chart_scatterplot = c3.generate({
              //tooltip: {
                  //contents: function(d, defaultTitleFormat, defaultValueFormat, color) {
                      //var company = jsonfile[d[0].index].company;
                      //var mailCount = jsonfile[d[0].index].mailCount;
                      //var lastInteractedInDays = jsonfile[d[0].index].lastInteractedInDays;
                      //var lastInteractedDate = jsonfile[d[0].index].lastInteractedDate;

                      //var companyData = "<table class='data-c3-table'><tr><th colspan='2'>"+company+"</th></tr><tr><td>Interactions<br>" + mailCount + "</td><td>Last Interacted Day<br>" + lastInteractedDate + "</td></tr></table>"
                      //return companyData;
                  //}
              //},
              //point: {
                  //r: 7
              //},
              //data: {
                  //json: jsonfile,
                  //keys: {
                      //x: 'mailCount',
                      //value: ['lastInteractedInDays', 'company']
                  //},
                  //color: function(color, d) {
                      //if (d.value > average) {
                          //return "#F86A52"
                      //} else {
                          //return "#49B5A6"
                      //};
                  //},
                  //type: 'scatter',
                  //onclick: function (d) {
                      //window.location = '/customer/select?account='+jsonfile[d.index].company
                  //}
              //},
              //axis: {
                  //x: {
                      //label: 'Interactions',
                      //tick: {
                          //fit: false
                      //}
                  //},
                  //y: {
                      //label: 'Days'
                  //}
              //},
              //legend: {
                  //show: false
              //}
          //});
    //});
  }
  paintHeatMap(null, defaultNoOfDays)
  //var dropType = "heatMap"
  var dropType = "interactions"
  $rootScope.$on("hierarchyModified", function(event, data){
    paintHeatMap(data,$window.noOfDays[dropType] || defaultNoOfDays)
  })
  $rootScope.$on(dropType + "DaysChanged", function(event, data){
    var localHierarchy = $rootScope.usersInHierarchy || null
    paintHeatMap(localHierarchy, data)
  })

})

reletasApp.controller("hierarchyController", function($scope, $http, $location, $rootScope){
    $scope.isCorporateUser = false

    $rootScope.$on("fetchedUserProfile", function(event, isCorporateUser){
      $scope.isCorporateUser = isCorporateUser
    })
    $(document).ready(function(){
      function convertArrayToTree(data){
        //var tree;
        //var obj = {};
        //data.forEach(function (user) {
        //
        //    var _id = user._id,
        //        hierarchyParent = user.hierarchyParent,
        //        a = obj[_id] || { _id: _id , emailId: user.emailId, designation: user.designation, firstName: user.firstName, lastName: user.lastName};
        //    if (hierarchyParent) {
        //        obj[hierarchyParent] = obj[hierarchyParent] || { _id: hierarchyParent };
        //        obj[hierarchyParent].children = obj[hierarchyParent].children || [];
        //        obj[hierarchyParent].children.push(a);
        //    } else {
        //        tree = obj[_id];
        //        tree.emailId = user.emailId
        //        tree["designation"] = user.designation
        //        tree.firstName = user.firstName
        //        tree.lastName = user.lastName
        //    }
        //    obj[_id] = obj[_id] || a;
        //})
        //return tree
        var root = data.find(function(item) {
          return item.hierarchyParent === null;
        });

        var tree = {
          _id: root._id,
          firstName: root.firstName,
          lastName: root.lastName,
          designation: root.designation,
          emailId: root.emailId
        };

        var parents = [tree];
        while (parents.length > 0) {
          var newParents = [];
          parents.forEach(function(parent) {
            var childrenList = data.filter(function(item) {
              return item.hierarchyParent == parent._id
            }).forEach(function(child) {
              var c = { _id: child._id, firstName: child.firstName, lastName: child.lastName, designation: child.designation, emailId: child.emailId};
              parent.children = parent.children || [];
              parent.children.push(c);
              newParents.push(c);
            });
          });
          parents = newParents;
        }

        return tree
      }

      $.getJSON("/company/user/hierarchy", function(data){
          var data2 = {};
          var data2 = convertArrayToTree(data.Data);

          var data3 = {};
          data3 = {children:[data2]};
          function addItem(parentUL, branch) {

              for (var key in branch.children) {
                  var item = branch.children[key];
                  $item = $('<li>', {
                      id: "item" + item._id
                  });

                  $item.append($('<input>', {
                      type: "checkbox",
                      id: "item" + item._id,
                      name: "item" + item._id
                  }));
                  $item.append($('<label>', {
                      for: "item" + item._id,
                      text: item.firstName +' '+item.lastName
                  }));
                  $item.append($('<p>', {
                      text: item.designation
                  }));
                  parentUL.append($item);
                  if (item.children) {
                      var $ul = $('<ul>', {
//                          style: 'display: none' This shows hierarchy open by default
                      }).appendTo($item);
                      $item.append();
                      addItem($ul, item);
                  }
              }
          }

        $("#updateHierarchy").click(function(e){
            var selected = $("#root input:checked").map(function(i,el){return el.name.replace("item","");}).get();
          $rootScope.usersInHierarchy = selected
          $rootScope.$broadcast("hierarchyModified", selected)
        });
          

          $(function () {
              addItem($('#root'), data3);
              //$(':checkbox').click(function () {
              //    $(this).find(':checkbox').trigger('click');
              //    var matchingId = $(this).attr('id');
              //
              //    console.log(matchingId);
              //    if ($(this).attr('checked'))
              //    {
              //        console.log('input[id*=' + matchingId +']');
              //
              //        $('input[id*=' + matchingId +']').each(function() {
              //            $(this).removeAttr('checked');
              //            $(this).prop('checked', $(this).attr('checked'));
              //        });
              //    }
              //    else {
              //        console.log('input[id*=' + matchingId +']');
              //
              //        $('input[id*=' + matchingId +']').each(function() {
              //            $(this).attr('checked', 'checked');
              //            $(this).prop('checked', $(this).attr('checked'));
              //
              //        });
              //    }
              //
              //});

              //Select all children if parent selected

              //$("input[type='checkbox']").change(function () {
              //    $(this).siblings('ul')
              //        .find("input[type='checkbox']")
              //        .prop('checked', this.checked);
              //});

              //Select All

              //$("#checkAllHierarchy").toggle(function(){
              //    $('input:checkbox').attr('checked','checked');
              //});

              $('#checkAllHierarchy').click( function () {
                  var checkBoxes = $("input");
                  checkBoxes.prop("checked", !checkBoxes.prop("checked"));
              })

              //Check only the parent
              $('#root>li>input:first-child').prop('checked', true);

              //function applyhierarchy(){
              //    var selected = $("#root input:checked").map(function(i,el){return el.name.replace("item","");}).get();
              //    $rootScope.usersInHierarchy = selected
              //    $rootScope.$broadcast("hierarchyModified", selected)
              //}
              //
              //applyhierarchy();

              //Slide toggle children of hierarchy
              $('label').click(function(){
                  $(this).closest('li').children('ul').slideToggle();

              });
          });



      })


    //    var data = {
    //    children: [{
    //        id: 1,
    //        title: "Alok Kohli",
    //        position: "Assistant Manager",
    //        children: [{
    //            id: 11,
    //            title: "Rahul Singha",
    //            position: "Project Manager",
    //            children: [{
    //                id: 111,
    //                title: "Ela",
    //                position: "SDE"
    //            }, {
    //                id: 112,
    //                title: "Sachin",
    //                position: "Opening Batsman",
    //                children: [{
    //                    id:1, title: "Zaheer", position: "Bowler"
    //                },
    //                    {id:1, title: "MS Dhoni", position: "Wicket Keeper",children: [{id:1, title: "Zaheer", position: "Bowler"},{id:1, title: "MS Dhoni", position: "Wicket Keeper"}]
    //                    }]
    //            }]
    //        }, {
    //            id: 12,
    //            title: "Rocky Balboa",
    //            position: "Champ Boxer",
    //            children: [{id:21, title:"Sammy", position: "useless"}]
    //        }]
    //    }]
    //};

//
//    function addItem(parentUL, branch) {
//
//        console.log(branch);
//        for (var key in branch.children) {
//            var item = branch.children[key];
//            $item = $('<li>', {
//                id: "item" + item.id
//            });
//            console.log($item);
//
//            $item.append($('<input>', {
//                type: "checkbox",
//                id: "item" + item.id,
//                name: "item" + item.id
//            }));
//            $item.append($('<label>', {
//                for: "item" + item.id,
//                text: item.title
//            }));
//            $item.append($('<p>', {
//                text: item.position
//            }));
//            parentUL.append($item);
//            if (item.children) {
//                var $ul = $('<ul>', {
//                    style: 'display: none'
//                }).appendTo($item);
//                $item.append();
//                addItem($ul, item);
//            }
//        }
//    }
//
//    $(function () {
//        addItem($('#root'), data);
//        $(':checkbox').click(function () {
//            $(this).find(':checkbox').trigger('click');
//            var matchingId = $(this).attr('id');
//            if ($(this).attr('checked'))
//            {
//                $('input[id*=' + matchingId +']').each(function() {
//                    $(this).removeAttr('checked');
//                    $(this).prop('checked', $(this).attr('checked'));
//                });
//            }
//            else {
//                $('input[id*=' + matchingId +']').each(function() {
//                    $(this).attr('checked', 'checked');
//                    $(this).prop('checked', $(this).attr('checked'));
//
//                });
//            }
//
//        });
//        $('label').click(function(){
//            $(this).closest('li').children('ul').slideToggle();
//
//        });
//    });
    });

})

reletasApp.controller("contactRelatas", function($scope, $http){

    $scope.submit = function (firstName,lastName,emailId,companyName,phoneNumber,skypeId) {

        if(validateEmail(emailId)){
            var demoUser = {
                firstName:firstName,
                lastName:lastName,
                emailId:emailId,
                companyName:companyName,
                phoneNumber:phoneNumber,
                skypeId:skypeId
            };

            $http.post('/admin/request/enterpriseDemo',demoUser)
                .success(function (response) {
                    if(response){
                        toastr.success("Thank you for your interest in Relatas. We will get in touch soon.")
                    } else {
                        toastr.error("Oops! Something went wrong. Please try again.")
                    }
                })
        } else {
            toastr.error("Please enter a valid Email ID")
        }
    }
    
});

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function sortObject(arr) {
    arr.sort(function(a, b) {
        return b.mailCount - a.mailCount ;
    });
    return arr; // returns array
}