
const firebaseConfig = {
    apiKey: "AIzaSyAYmfsxJkugz87BuzC0JDaDQvFHEB5szIw",
    authDomain: "relatas-showcase.firebaseapp.com",
    databaseURL: "https://relatas-showcase.firebaseio.com",
    projectId: "relatas-showcase",
    storageBucket: "relatas-showcase.appspot.com",
    messagingSenderId: "871167737144",
    appId: "1:871167737144:web:7f229ff8d7278dca"
};

firebase.initializeApp(firebaseConfig);
var messaging = firebase.messaging();

// Requesting permission
messaging.requestPermission()
.then(function() {

}).catch(function(err) {
    console.log(err);
});

// Get token
messaging.getToken().then(function(currentToken) {
    if(currentToken) {
        updateFirebaseToken({
            token: currentToken,
            from: "web",
            notificationEnabled: true
        })
    }

}).catch(function(err) {
    console.log("Error while getting certificate:", err);

    updateFirebaseToken({
        token: null,
        from: "web",
        notificationEnabled: false
    })
});

// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(function() {

    messaging.getToken().then(function(refreshedToken) {
        if(refreshedToken) {
            updateFirebaseToken({
                token: refreshedToken,
                from: "web",
                notificationEnabled: true
            })
        }

    }).catch(function(err) {
        console.log('Unable to retrieve refreshed token ', err);

        updateFirebaseToken({
            token: null,
            from: "web",
            notificationEnabled: false
        })
    });
});

// when message received when the web page is current tab
messaging.onMessage(function(payload) {
    const title = payload.notification.title;
    const options = {
        body: payload.notification.body,
        icon: payload.notification.icon
    }

    var notification = new Notification(title, options);
    notification.onclick = function(event) {
        event.preventDefault(); // prevent the browser from focusing the Notification's tab
        window.open(payload.data.click_action , '_blank');
        notification.close();
    }
});

function updateFirebaseToken(tokenData) {

    $.ajax({
        url: '/update/firebase/token',
        type: 'POST',
        datatype: 'JSON',
        data: tokenData,
        traditional: true,
        success: function (result) {
        }
    })
}