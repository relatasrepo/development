/**
 * Created by rahul on 1/23/16.
 */

reletasApp.directive('errortoggle', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('load', function(){

        if(scope.contact)
          scope.contact.noPicFlag = false
        scope.displayLogo = false;
        scope.$apply()
      })
    }
  }
});