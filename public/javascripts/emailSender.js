var crypto = require('crypto');
var appCredentials = require('../../config/relatasConfiguration');
var winstonLog = require('../../common/winstonLog');
var moment = require('moment-timezone');

var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill('unlvL85QRNwryRBDi8kIdQ');
var mandrill_client_massMails = new mandrill.Mandrill('unlvL85QRNwryRBDi8kIdQ');

var appCredential = new appCredentials();
var logLib = new winstonLog();

var logger =logLib.getWinston();
var loggerError = logLib.getWinstonError();
var domain = appCredential.getDomain();
var headers = appCredential.getRelatasHeaders();

var domainName = domain.domainName;

var relatasHeaderJpg = headers.relatasHeader;
var relatasTwitterJpg = headers.relatasTwitter;
var relatasFacebookJpg = headers.relatasFacebook;
var relatasLinkedinJpg = headers.relatasLinkedin;
var relatasGrayLogoJpg = headers.relatasGrayLogo;

var note = "NOTE: This is an automated email, please don't REPLY to this email as this mailbox is not tracked frequently. ";



function emailSender() {
       
}

emailSender.prototype.sendForgetPasswordMail = function(dataToSend) {

    try {
        var emailId = dataToSend.profile.emailId;
        var url = dataToSend.url;
        var firstName = dataToSend.profile.firstName;
        var params = {
            "template_name": "NewPasswordRequest",
            "template_content": [
                {
                    "name": "NewPasswordRequest",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": "Relatas: Password Reset",
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": emailId,

                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": emailId,
                        "vars": [
                            {
                                "name": "firstName",
                                "content": firstName
                            },
                            {
                                "name":"token",
                                "content":dataToSend.token
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });


    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendEditProfileConfirmationMail = function(dataToSend) {

    try {
        var emailId = dataToSend.emailId;

        var firstName = dataToSend.firstName;
        var params = {
            "template_name": "EditProfile",
            "template_content": [
                {
                    "name": "EditProfile",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": "Relatas: Profile saved successfully",
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": emailId,
                        "vars": [
                            {
                                "name": "name",
                                "content": firstName
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendTaskReport = function(toEmailId,dataToSend,startOFweek,endOFweek) {


    try {
        var emailId = toEmailId;
        // var emailId = "naveenpaul@relatas.com";
        var firstName = toEmailId;

        var params = {
            "template_name": "tasks_report",
            "template_content": [
                {
                    "name": "tasks_report",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": "Tasks report for week",
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge_language": "handlebars",
                "merge": true,
                // "global_merge_vars": [
                //     {
                //         "name": "var1",
                //         "content": "Global Value 1"
                //     }
                // ],
                "merge_vars": [
                    {
                        "rcpt": emailId,
                        "vars": [
                            {
                                "name": "users",
                                "content": dataToSend
                            },
                            {
                                "name": "fName",
                                "content":firstName
                            },
                            {
                                "name": "startOFweek",
                                "content":moment(startOFweek).format("DD MMM YYYY")
                            },
                            {
                                "name": "endOFweek",
                                "content":moment(endOFweek).format("DD MMM YYYY")
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendCalendarPasswordRequestMail = function(dataToSend) {

    try {

        var emailId = dataToSend.toEmailId;

        var firstName = dataToSend.toName;
        var params = {
            "template_name": "CalendarPasswordRequest",
            "template_content": [
                {
                    "name": "CalendarPasswordRequest",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": "Relatas : Calendar Password Request",
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": emailId,
                        "vars": [
                            {
                                "name": "name",
                                "content": firstName
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });


    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendCalendarPasswordMail = function(dataToSend) {

    try {

        var emailId = dataToSend.requestedByEmailId;
        var url = dataToSend.url;
        var requestedByName = dataToSend.requestedByName;
        var calUserName = dataToSend.name;
        var mailContent = '';
         if(checkRequired(dataToSend.msg) && dataToSend.accepted == true){
            mailContent = calUserName+' has shared his calendar password with you. <a href='+url+' target="_blank" title="Relatas.com">Click here</a> to view '+calUserName+' Relatas Calendar.<br>';
            mailContent += dataToSend.msg;
         }
         else if(!checkRequired(dataToSend.msg) && dataToSend.accepted == true){
             mailContent = '<span style="color:rgb(89, 89, 89); font-family:helvetica neue,arial,helvetica,verdana,sans-serif; font-size:12px; line-height:22.5px">'+calUserName+' has shared his calendar password with you. <a href='+url+' target="_blank" title="Relatas.com">Click here</a> to view '+calUserName+' Relatas Calendar.</span>';
         }
        else if(checkRequired(dataToSend.msg) && dataToSend.accepted == false){
             mailContent = dataToSend.msg;
         }

        var params = {
            "template_name": "CalendarPasswordReceived",
            "template_content": [
                {
                    "name": "CalendarPasswordReceived",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": "Relatas : Calendar Password Received",
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": emailId,
                        "vars": [
                            {
                                "name": "requesterName",
                                "content": requestedByName
                            },
                            {
                                "name": "mailContent",
                                "content": mailContent
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });


    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendCalendarPasswordSentMail = function(dataToSend) {

    try {

        var emailId = dataToSend.emailId;

        var requestedByName = dataToSend.requestedByName;
        var calUserName = dataToSend.name;
        var params = {
            "template_name": "CalendarPasswordSent",
            "template_content": [
                {
                    "name": "CalendarPasswordSent",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": "Relatas : Your calendar Password Sent",
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": emailId,
                        "vars": [
                            {
                                "name": "requesterName",
                                "content": requestedByName
                            },
                            {
                                "name": "calUserName",
                                "content": calUserName
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });


    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

// email sender for meetx login
emailSender.prototype.sendRegistrationConfirmationMail = function(userInfo){

    try{
        var smtpTransport = setUpSmtp();
        var firstName = userInfo.firstName;
        var emailId = userInfo.emailId;
        var serviceLogin = userInfo.serviceLogin;
        var publicProfileUrl= userInfo.publicProfileUrl;
        var settingsUrl = domainName+'/settings/';
        var company = checkRequired(userInfo.companyName) ? userInfo.companyName : "Update <a href=" + settingsUrl +">Company</a>";
        var designation = checkRequired(userInfo.designation) ? userInfo.designation : "Update <a href=" + settingsUrl +">Designation</a>";
        var location = userInfo.currentLocation.city;
        var timezoneAbbr = checkRequired(userInfo.timezone.name) ? moment().tz(userInfo.timezone.name).zoneName() : "Update <a href=" + settingsUrl +">Timezone</a>";


        var params = {
             "template_name": "R_AccountCreation_Jul2018",
             "template_content": [
                 {
                     "name": "R_AccountCreation_Jul2018",
                     "content": "example content"
                 }
             ],
             "message": {
                 "subject": "Relatas : You have successfully registered",
                 "from_email": "no-reply@relatas.co.in",
                 "from_name": "Relatas",
                 "to": [
                     {
                         "email": userInfo.emailId,

                         "type": "to"
                     }
                 ],
                 "important": false,
                 "track_opens": null,
                 "track_clicks": null,
                 "auto_text": null,
                 "auto_html": null,
                 "inline_css": null,
                 "url_strip_qs": null,
                 "preserve_recipients": null,
                 "view_content_link": null,

                 "tracking_domain": null,
                 "signing_domain": null,
                 "return_path_domain": null,
                 "merge": true,
                 "global_merge_vars": [
                     {
                         "name": "var1",
                         "content": "Global Value 1"
                     }
                 ],
                 "merge_vars": [
                     {
                         "rcpt": userInfo.emailId,
                         "vars": [
                             {
                                 "name": "FirstName",
                                 "content": firstName
                             },
                             {
                                 "name": "FNAME",
                                 "content": firstName
                             },
                             {
                                 "name": "identity1",
                                 "content": publicProfileUrl
                             },
                             {
                                 "name": "identity2",
                                 "content": publicProfileUrl
                             },
                             {
                                 "name": "share",
                                 "content": "<img src='http://showcase.relatas.com/images/mailer/relatas_mail_facebook.png'/>"
                             },
                             {
                                 "name": "designation",
                                 "content": designation
                             },
                             {
                                 "name": "company",
                                 "content": company
                             },
                             {
                                 "name": "location",
                                 "content": location + ', ' + timezoneAbbr
                             }
                         ]
                     }
                 ]

             }
         }

        mandrill_client.messages.sendTemplate(params, function(res) {
           logger.info(res);
        },function(err) {
           // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }

};

emailSender.prototype.sendEBookLink = function(obj){

    try{
        var smtpTransport = setUpSmtp();
        var accessToBook = "Welcome to Relatas";
        var ebookLink = "https://s3.amazonaws.com/relatas-live-website-resources/RelatasSalesAI.pdf"

        var template_name = "requestForDemo";
        var fullName = ""
        var downloadType = "Demo request"

        if(obj){
            fullName = obj.fullName?obj.fullName:obj.emailId;

            if(obj.type === "pricing"){
                template_name = "requestPricing"
                downloadType = "Pricing request"
            }
            if(obj.type === "whitePaper"){
                downloadType = "White Paper"
                template_name = "requestResource"
                ebookLink =  "https://s3.amazonaws.com/relatas-live-website-resources/RelatasWhitePaper.pdf"
            }
            if(obj.type === "useCase"){
                downloadType = "Use Case"
                template_name = "requestResource"
                ebookLink =  "https://s3.amazonaws.com/relatas-live-website-resources/RelatasUseCases.pdf"
            }
            if(obj.type === "aiBook"){
                downloadType = "Sales AI Playbook"
                template_name = "requestResource"
            }
            if(obj.type === "caseStudy"){
                ebookLink =  "https://s3.amazonaws.com/relatas-live-website-resources/Relatas_CustomerSuccess.pdf"
                downloadType = "Case Study"
                template_name = "requestResource"
            }
        }

        var params = {
             "template_name": template_name,
             "template_content": [
                 {
                     "name": template_name,
                     "content": "example content"
                 }
             ],
             "message": {
                 "subject": "Relatas : "+downloadType,
                 "from_email": "no-reply@relatas.co.in",
                 "from_name": "Relatas",
                 "to": [
                     {
                         "email": obj.emailId,

                         "type": "to"
                     }
                 ],
                 "important": false,
                 "track_opens": null,
                 "track_clicks": null,
                 "auto_text": null,
                 "auto_html": null,
                 "inline_css": null,
                 "url_strip_qs": null,
                 "preserve_recipients": null,
                 "view_content_link": null,

                 "tracking_domain": null,
                 "signing_domain": null,
                 "return_path_domain": null,
                 "merge": true,
                 "global_merge_vars": [
                     {
                         "name": "var1",
                         "content": "Global Value 1"
                     }
                 ],
                 "merge_vars": [
                     {
                         "rcpt": obj.emailId,
                         "vars": [
                             {
                                 "name": "FullName",
                                 "content": fullName
                             },
                             {
                                 "name": "ebookLink",
                                 "content": ebookLink
                             },
                             {
                                 "name": "downloadType",
                                 "content": downloadType
                             }
                         ]
                     }
                 ]

             }
         }

        mandrill_client.messages.sendTemplate(params, function(res) {
           logger.info(res);
        },function(err) {
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }

};

emailSender.prototype.ebookLinkNotifyAdmins = function(obj){

    try{
        var smtpTransport = setUpSmtp();
        var accessToBook = "Welcome to Relatas";
        var ebookLink = "https://s3.amazonaws.com/relatas-live-website-resources/RelatasSalesAI.pdf"

        var template_name = "ebookNotifyAdmin";
        var fullName = ""
        var downloadType = "Demo request"

        if(obj){
            fullName = obj.fullName?obj.fullName:obj.emailId;

            if(obj.type === "pricing"){
                downloadType = "Pricing request"
            }
            if(obj.type === "whitePaper"){
                downloadType = "White Paper"
                ebookLink =  "https://s3.amazonaws.com/relatas-live-website-resources/RelatasWhitePaper.pdf"
            }
            if(obj.type === "useCase"){
                downloadType = "Use Case"
                ebookLink =  "https://s3.amazonaws.com/relatas-live-website-resources/RelatasUseCases.pdf"
            }
            if(obj.type === "aiBook"){
                downloadType = "Sales AI Playbook"
            }
            if(obj.type === "caseStudy"){
                ebookLink =  "https://s3.amazonaws.com/relatas-live-website-resources/Relatas_CustomerSuccess.pdf"
                downloadType = "Case Study"
            }
        }

        var params = {
             "template_name": template_name,
             "template_content": [
                 {
                     "name": template_name,
                     "content": "example content"
                 }
             ],
             "message": {
                 "subject": "Relatas : "+downloadType,
                 "from_email": "no-reply@relatas.co.in",
                 "from_name": "Relatas",
                 "to": [
                     {
                         // "email": "naveenpaul@relatas.com",
                         "email": "sudip@relatas.com",

                         "type": "to"
                     }
                 ],
                 "important": false,
                 "track_opens": null,
                 "track_clicks": null,
                 "auto_text": null,
                 "auto_html": null,
                 "inline_css": null,
                 "url_strip_qs": null,
                 "preserve_recipients": null,
                 "view_content_link": null,

                 "tracking_domain": null,
                 "signing_domain": null,
                 "return_path_domain": null,
                 "merge": true,
                 "global_merge_vars": [
                     {
                         "name": "FullName",
                         "content": fullName
                     },
                     {
                         "name": "ebookLink",
                         "content": ebookLink
                     },
                     {
                         "name": "downloadType",
                         "content": downloadType
                     },
                     {
                         "name": "emailId",
                         "content": obj.emailId
                     }
                 ],
                 "merge_vars": [
                     {
                         "rcpt": obj.emailId,
                         "vars": [
                             {
                                 "name": "FullName",
                                 "content": fullName
                             },
                             {
                                 "name": "ebookLink",
                                 "content": ebookLink
                             },
                             {
                                 "name": "downloadType",
                                 "content": downloadType
                             },
                             {
                                 "name": "emailId",
                                 "content": obj.emailId
                             }
                         ]
                     }
                 ]

             }
         }

        mandrill_client.messages.sendTemplate(params, function(res) {
           logger.info(res);
        },function(err) {
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }

};

// email sender for Partial Account Creation
emailSender.prototype.sendPartialRegistrationMail = function(userInfo){

    var onboardingUrl = domainName+'/schedule/partialProfile/next/onboarding?userId='+userInfo._id

    try{
        var smtpTransport = setUpSmtp();

        var firstName = userInfo.firstName;
        var emailId = userInfo.emailId;
        var publicProfileUrl= userInfo.publicProfileUrl;

        var params = {
            "template_name": "R_partialAccountSchedule",
            "template_content": [
                {
                    "name": "R_partialAccountSchedule",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": "Relatas : Your partial account successfully created",
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": emailId,

                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": userInfo.emailId,
                        "vars": [
                            {
                                "name": "FirstName",
                                "content": firstName
                            },
                            {
                                "name": "FNAME",
                                "content": firstName
                            },
                            {
                                "name": "identity1",
                                "content": publicProfileUrl
                            },
                            {
                                "name": "identity2",
                                "content": publicProfileUrl
                            },
                            {
                                "name": "share",
                                "content": "<img src='"+domainName+"'/images/mailer/relatas_mail_facebook.png'/>"
                            },
                            {
                                "name": "onboardingUrl",
                                "content": onboardingUrl
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }

};

// email sender for meetx login
emailSender.prototype.sendPreSignUpRequestMail = function(userInfo){

    try{

        var params = {
            "template_name": "Enterprise_SignupRequest",
            "template_content": [
                {
                    "name": "Enterprise_SignupRequest",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": "Relatas : Enterprise pre-sign up",
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": userInfo.emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": userInfo.emailId,
                        "vars": [
                            {
                                "name": "FNAME",
                                "content": userInfo.firstName
                            }
                        ]
                    }
                ]
            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }

};

// email sender for meetx login
emailSender.prototype.sendPreSignUpRequestMailAdminCopy = function(userInfo){

    try{

        var params = {
            "template_name": "Enterprise_signup_admin_copy",
            "template_content": [
                {
                    "name": "Enterprise_signup_admin_copy",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": "Relatas : Enterprise pre-sign up",
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": "admin@relatas.com",
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": "admin@relatas.com",
                        "vars": [
                            {
                                "name": "name",
                                "content": userInfo.firstName +' '+ userInfo.lastName
                            },{
                                "name": "emailId",
                                "content": userInfo.emailId
                            },{
                                "name": "phoneNumber",
                                "content": userInfo.phoneNumber
                            },{
                                "name": "skypeId",
                                "content": userInfo.skypeId
                            },{
                                "name": "companyName",
                                "content": userInfo.companyName
                            }
                        ]
                    }
                ]
            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }

};

// email sender for invitation
emailSender.prototype.sendInvitationMailToSender = function(invitationDetails){
    try{

        var receiverName1 = invitationDetails.to.receiverName;
        var receiverName2 = invitationDetails.to.receiverName;
        var senderName = invitationDetails.senderName;
        var senderEmailId = invitationDetails.senderEmailId;
        var subject = "Relatas : Your invitation to "+receiverName1+" has been sent"
         if(invitationDetails.selfCalendar){
             subject = "Relatas : Your invitation has been successfully sent"
             receiverName2 = "they"
         }

         var params = {
             "template_name": "NewMeetingInvitation_ToSender",
             "template_content": [
                 {
                     "name": "NewMeetingInvitation_ToSender",
                     "content": "example content"
                 }
             ],
             "message": {
                 "subject": subject,
                 "from_email": "no-reply@relatas.co.in",
                 "from_name": "Relatas",
                 "to": [
                     {
                         "email": senderEmailId,

                         "type": "to"
                     }
                 ],
                 "important": false,
                 "track_opens": null,
                 "track_clicks": null,
                 "auto_text": null,
                 "auto_html": null,
                 "inline_css": null,
                 "url_strip_qs": null,
                 "preserve_recipients": null,
                 "view_content_link": null,

                 "tracking_domain": null,
                 "signing_domain": null,
                 "return_path_domain": null,
                 "merge": true,
                 "global_merge_vars": [
                     {
                         "name": "var1",
                         "content": "Global Value 1"
                     }
                 ],
                 "merge_vars": [
                     {
                         "rcpt": senderEmailId,
                         "vars": [
                             {
                                 "name": "senderName",
                                 "content": senderName
                             },
                             {
                                 "name": "receiverName1",
                                 "content": receiverName1
                             },
                             {
                                 "name": "receiverName2",
                                 "content": receiverName2
                             },
                             {
                                 "name":"meetingId",
                                 "content":invitationDetails.invitationId
                             }
                         ]
                     }
                 ]

             }
         }

         mandrill_client.messages.sendTemplate(params, function(res) {
             logger.info(res);
         },function(err) {
             // log(err);
             logger.info('A mandrill error occurred: ' +err);
         });


     }
     catch (err) {
        throw err;
      }
}

emailSender.prototype.sendInvitationMailToReceiver = function(invitationDetails){
    try{
        var smtpTransport = setUpSmtp();
        var receiverName = invitationDetails.to.receiverName;
        var receiverEmailId = invitationDetails.to.receiverEmailId;
        var senderName = invitationDetails.senderName;
       // var senderEmailId = invitationDetails.senderEmailId;
        //var description = invitationDetails.meeting.description;
        var documentUrl = invitationDetails.docs.documentUrl;

         if (documentUrl == undefined || documentUrl == null || documentUrl == '') {
              documentUrl = '';
         }
         else{
            documentUrl = senderName+" Shared a document with you: "+documentUrl;
         }

         var params = {
             "template_name": "NewMeetingInvitation_ToReceiver",
             "template_content": [
                 {
                     "name": "NewMeetingInvitation_ToReceiver",
                     "content": "example content"
                 }
             ],
             "message": {
                 "subject": "Relatas : You got an invitation from "+senderName+" ",
                 "from_email": "no-reply@relatas.co.in",
                 "from_name": "Relatas",
                 "to": [
                     {
                         "email": receiverEmailId,

                         "type": "to"
                     }
                 ],
                 "important": false,
                 "track_opens": null,
                 "track_clicks": null,
                 "auto_text": null,
                 "auto_html": null,
                 "inline_css": null,
                 "url_strip_qs": null,
                 "preserve_recipients": null,
                 "view_content_link": null,

                 "tracking_domain": null,
                 "signing_domain": null,
                 "return_path_domain": null,
                 "merge": true,
                 "global_merge_vars": [
                     {
                         "name": "var1",
                         "content": "Global Value 1"
                     }
                 ],
                 "merge_vars": [
                     {
                         "rcpt": receiverEmailId,
                         "vars": [
                             {
                                 "name": "receiverName",
                                 "content": receiverName
                             },
                             {
                                 "name": "senderName",
                                 "content": senderName
                             },
                             {
                                 "name":"meetingId1",
                                 "content":invitationDetails.invitationId
                             },
                             {
                                 "name":"meetingId2",
                                 "content":invitationDetails.invitationId
                             },
                             {
                                 "name":"meetingId3",
                                 "content":invitationDetails.invitationId
                             },
                             {
                                 "name":"meetingId4",
                                 "content":invitationDetails.invitationId
                             }
                         ]
                     }
                 ]

             }
         }

         mandrill_client.messages.sendTemplate(params, function(res) {
             logger.info(res);
         },function(err) {
             // log(err);
             logger.info('A mandrill error occurred: ' +err);
         });

     } catch (err) {
        throw err;
      }
}

function getTextLength(text,maxLength){
    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'.. ';
    }
    else return text;
}

emailSender.prototype.sendInvitationMailToReceiver_selfCalendar = function(invitationDetails,suggested){

    try{
        var smtpTransport = setUpSmtp();
        var receiverName = invitationDetails.receiverName;
        var receiverEmailId = invitationDetails.receiverEmailId;
        var senderName = invitationDetails.senderName;
        var sPic = invitationDetails.senderPicUrl;

        var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;

        if(invitationDetails.mAgenda.length > 100){
            var agenda = getTextLength(invitationDetails.mAgenda,100);
            agenda +=' <a href='+meetingUrl+'>more</a>';
            invitationDetails.mAgenda = agenda;
        }

        var message = '';
        var subject = '';
        if(suggested){
            message = '<span>'+senderName+'&nbsp;suggested a new time</span>';
            if(checkRequired(invitationDetails.comment)){
                message += '<br><span> Message from '+senderName+'</span><br><span>'+invitationDetails.comment+'</span>';
              // message += ''+invitationDetails.comment+'</span>';
            }
            subject = "Relatas : "+senderName+" has suggested a new meeting time ";
        }else{
            message = "You received a new meeting invitation.";
            subject = "Relatas : You got an invitation from "+senderName+" "
        }

        var params = {
            "template_name": "NewMeetingInvitation_ToReceiver_SelfCalendar",
            "template_content": [
                {
                    "name": "NewMeetingInvitation_ToReceiver_SelfCalendar",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": receiverEmailId,

                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": receiverEmailId,
                        "vars": [
                            {
                                "name": "receiverName",
                                "content": receiverName || ''
                            },
                            {
                                "name": "message",
                                "content": message || ''
                            },
                            {
                                "name": "senderName",
                                "content": senderName
                            },
                            {
                                "name": "senderPic",
                                "content": sPic
                            },
                            {
                                "name": "senderName",
                                "content": invitationDetails.senderName
                            },
                            {
                                "name": "senderInfo",
                                "content": invitationDetails.senderCompany
                            },
                            {
                                "name": "meetingUrl",
                                "content": meetingUrl
                            },
                            {
                                "name":"mDate",
                                "content":invitationDetails.mDate
                            },
                            {
                                "name":"mTime",
                                "content":invitationDetails.mTime
                            },
                            {
                                "name":"mLocation",
                                "content":invitationDetails.mLocation
                            },
                            {
                                "name":"mAgenda",
                                "content":invitationDetails.mAgenda
                            },
                            {
                                "name":"mTitle",
                                "content":invitationDetails.mTitle
                            },
                            {
                                "name":"meetingId2",
                                "content":invitationDetails.invitationId
                            },
                            {
                                "name":"meetingId3",
                                "content":invitationDetails.invitationId
                            },
                            {
                                "name":"meetingId4",
                                "content":invitationDetails.invitationId
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
};

emailSender.prototype.sendInvitationMailToReceiver_NewTemplate = function(invitationDetails,suggested){

    try{

        var receiverEmailId = invitationDetails.receiverEmailId;
        var receiverName = invitationDetails.receiverName || '';
        var senderNameSub = invitationDetails.senderName || '';
        var senderLocation = checkRequired(invitationDetails.senderLocation) ? invitationDetails.senderLocation.replace(/\s/g, '&nbsp;') : '';
        var senderCompany = checkRequired(invitationDetails.senderCompany) ? invitationDetails.senderCompany.replace(/\s/g, '&nbsp;') : '';

        receiverName = checkRequired(receiverName) ? receiverName.replace(/\s/g, '&nbsp;') : '';
        var senderName = checkRequired(senderNameSub) ? senderNameSub.replace(/\s/g, '&nbsp;') : '';

        var sPic = invitationDetails.senderPicUrl;
        var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;

        if(invitationDetails.mAgenda.length > 100){
            var agenda = getTextLength(invitationDetails.mAgenda,100);
            agenda = agenda.replace(/\s/g, '&nbsp;');
            agenda +=' <a href='+meetingUrl+'>more</a>';
            invitationDetails.mAgenda = agenda;
        }

        var message = '';
        var subject = '';
        if(suggested){
            message = '<span style="color:#000000; font-family:helvetica neue,arial,helvetica,verdana,sans-serif; font-size:12px; line-height:22.5px">'+senderName+'&nbsp;suggested a new time';
            if(checkRequired(invitationDetails.comment)){
                var comment = invitationDetails.comment.replace(/\s/g, '&nbsp;');
                message += '<br> Message from '+senderName+'<br>'+comment;
            }
            message +='</span>';
            subject = "Relatas : "+senderNameSub+" has suggested a new meeting time ";
        }
        else{
            message = "You received a new meeting invitation.";
            subject = "Relatas : You got an invitation from "+senderNameSub+" "
        }

        var params = {
            "template_name": "MeetingInvitationToReceiver",
            "template_content": [
                {
                    "name": "MeetingInvitationToReceiver",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": receiverEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": receiverEmailId,
                        "vars": [
                            {
                                "name": "receiverName",
                                "content": receiverName || ''
                            },
                            {
                                "name": "message",
                                "content": message || ''
                            },
                            {
                                "name": "senderName",
                                "content": senderName
                            },
                            {
                                "name":"interactionsUrl",
                                "content":invitationDetails.interactionUrl
                            },
                            {
                                "name": "senderPic",
                                "content": sPic
                            },
                            {
                                "name": "senderLocation",
                                "content": senderLocation
                            },
                            {
                                "name": "senderName",
                                "content": senderName
                            },
                            {
                                "name": "senderInfo",
                                "content": senderCompany
                            },
                            {
                                "name": "meetingUrl",
                                "content": meetingUrl
                            },
                            {
                                "name":"mDate",
                                "content":invitationDetails.mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mTime",
                                "content":invitationDetails.mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mLocation",
                                "content":invitationDetails.mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mAgenda",
                                "content":invitationDetails.mAgenda
                            },
                            {
                                "name":"mTitle",
                                "content":invitationDetails.mTitle.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"meetingId2",
                                "content":invitationDetails.invitationId
                            },
                            {
                                "name":"meetingId3",
                                "content":invitationDetails.invitationId
                            },
                            {
                                "name":"meetingId4",
                                "content":invitationDetails.invitationId
                            },
                            {
                                "name":"lastInteractedOn",
                                "content":invitationDetails.lastInteractedOn.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"totalInteractions",
                                "content":invitationDetails.totalInteractions
                            },
                            {
                                "name":"mCommonConnections",
                                "content":invitationDetails.commonConnections == 'Not Found' ? invitationDetails.commonConnections.replace(/\s/g, '&nbsp;') : invitationDetails.commonConnections
                            }
                        ]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
}

emailSender.prototype.sendInvitationMailToSender_NewTemplate = function(invitationDetails,suggested){
    try{

        var receiverEmailId = invitationDetails.receiverEmailId;
        var receiverName = invitationDetails.receiverName || '';
        var senderNameSub = invitationDetails.senderName || '';
        var senderLocation = checkRequired(invitationDetails.senderLocation) ? invitationDetails.senderLocation.replace(/\s/g, '&nbsp;') : '';
        var senderCompany = checkRequired(invitationDetails.senderCompany) ? invitationDetails.senderCompany.replace(/\s/g, '&nbsp;') : '';

        receiverName = checkRequired(receiverName) ? receiverName.replace(/\s/g, '&nbsp;') : '';
        var senderName = checkRequired(senderNameSub) ? senderNameSub.replace(/\s/g, '&nbsp;') : '';

        var sPic = invitationDetails.senderPicUrl;
        var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;

        if(invitationDetails.mAgenda.length > 100){
            var agenda = getTextLength(invitationDetails.mAgenda,100);
            agenda = agenda.replace(/\s/g, '&nbsp;');
            agenda +=' <a href='+meetingUrl+'>more</a>';
            invitationDetails.mAgenda = agenda;
        }

        var message = '';
        var subject = '';
        if(suggested){
            message = '<span style="color:#000000; font-family:helvetica neue,arial,helvetica,verdana,sans-serif; font-size:12px; line-height:22.5px">&nbsp;You suggested a new time as below';
            if(checkRequired(invitationDetails.comment)){
                var comment = invitationDetails.comment.replace(/\s/g, '&nbsp;');
                message += '<br> Your message <br>'+comment;
            }
            message +='</span>';
            subject = "Relatas :  Your revised suggested times to "+senderNameSub+" has been sent";
        }
        else{
            message = "You sent the meeting request with following details to: ";
            subject = "Relatas : Your invitation has been successfully sent"
        }

        var params = {
            "template_name": "MeetingInvitationToSender",
            "template_content": [
                {
                    "name": "MeetingInvitationToSender",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": receiverEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": receiverEmailId,
                        "vars": [
                            {
                                "name": "senderName",
                                "content": receiverName || ''
                            },
                            {
                                "name": "message",
                                "content": message || ''
                            },
                            {
                                "name": "receiverName",
                                "content": receiverName
                            },
                            {
                                "name":"interactionsUrl",
                                "content":invitationDetails.interactionUrl
                            },
                            {
                                "name": "receiverPic",
                                "content": sPic
                            },
                            {
                                "name": "receiverLocation",
                                "content": senderLocation
                            },
                            {
                                "name": "receiverName",
                                "content": senderName
                            },
                            {
                                "name": "receiverInfo",
                                "content": senderCompany
                            },
                            {
                                "name": "meetingUrl",
                                "content": meetingUrl
                            },
                            {
                                "name":"mDate",
                                "content":invitationDetails.mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mTime",
                                "content":invitationDetails.mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mLocation",
                                "content":invitationDetails.mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mAgenda",
                                "content":invitationDetails.mAgenda
                            },
                            {
                                "name":"mTitle",
                                "content":invitationDetails.mTitle.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"meetingId2",
                                "content":invitationDetails.invitationId
                            },
                            {
                                "name":"meetingId3",
                                "content":invitationDetails.invitationId
                            },
                            {
                                "name":"meetingId4",
                                "content":invitationDetails.invitationId
                            },
                            {
                                "name":"lastInteractedOn",
                                "content":invitationDetails.lastInteractedOn.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"totalInteractions",
                                "content":invitationDetails.totalInteractions
                            },
                            {
                                "name":"mCommonConnections",
                                "content":invitationDetails.commonConnections == 'Not Found' ? invitationDetails.commonConnections.replace(/\s/g, '&nbsp;') : invitationDetails.commonConnections
                            }
                        ]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
}

emailSender.prototype.sendWarUser = function(userStats, numberWithCommas, primaryCurrency, callback){
    try{
        var subject = "Weekly Abstract Report",
            receiverEmailId = userStats.userInfo.emailId;

        var params = {
            "template_name": "WAR_Individual",
            "template_content": [
                {
                    "name": "WAR_Individual",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": receiverEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "I_Target",
                        "content": numberWithCommas(userStats.I_Target, primaryCurrency == "INR")
                    },
                    {
                        "name": "I_Won",
                        "content": numberWithCommas(userStats.I_Won, primaryCurrency == "INR")
                    },
                    {
                        "name": "I_Commit",
                        "content": numberWithCommas(userStats.I_Commit, primaryCurrency == "INR")
                    },
                    {
                        "name": "I_Won_percent",
                        "content": userStats.I_Won_percent !== "-" ?  parseFloat(userStats.I_Won_percent.toFixed(2)) : userStats.I_Won_percent
                    },
                    {
                        "name": "I_Commit_percent",
                        "content": userStats.I_Commit_percent !== "-" ? parseFloat(userStats.I_Commit_percent.toFixed(2)) : userStats.I_Commit_percent
                    },
                    {
                        "name": "I_CommitPipeline_percent",
                        "content": userStats.I_CommitPipeline_percent !== "-" ? parseFloat(userStats.I_CommitPipeline_percent.toFixed(2)) : userStats.I_CommitPipeline_percent
                    },
                    {
                        "name": "I_RiskDealsCount",
                        "content": userStats.I_RiskDealsCount
                    },
                    {
                        "name": "I_RiskDealsValue",
                        "content": numberWithCommas(userStats.I_RiskDealsValue, primaryCurrency == "INR")
                    },
                    {
                        "name": "I_30DaysCount",
                        "content": userStats.I_30DaysCount
                    },
                    {
                        "name": "I_30DaysValue",
                        "content": numberWithCommas(userStats.I_30DaysValue, primaryCurrency == "INR")
                    },
                    {
                        "name": "I_StaleOppsCount",
                        "content": userStats.I_StaleOppsCount
                    },
                    {
                        "name": "I_StaleOppsValue",
                        "content": numberWithCommas(userStats.I_StaleOppsValue, primaryCurrency == "INR")
                    },
                    {
                        "name": "I_Tasks",
                        "content": userStats.I_Tasks
                    },
                    {
                        "name": "I_Meetings",
                        "content": userStats.I_Meetings
                    },
                    {
                        "name": "LosingTouchCount",
                        "content": userStats.LosingTouchCount
                    },
                    {
                        "name": "LosingTouchValue",
                        "content": userStats.LosingTouchValue
                    },
                    {
                        "name": "I_CommitPipeline",
                        "content": numberWithCommas(userStats.I_CommitPipeline, primaryCurrency == "INR")
                    },
                    {
                        "name": "startYourWeek",
                        "content": userStats.startYourWeek
                    },
                    {
                        "name": "I_DealWonPastWeek",
                        "content": numberWithCommas(userStats.I_DealWonPastWeek, primaryCurrency == "INR")
                    },
                    {
                        "name": "I_CommitOppCount",
                        "content": userStats.I_CommitOppCount
                    },
                    {
                        "name": "I_CommitStage",
                        "content": userStats.I_CommitStage
                    },
                    {
                        "name": "I_CommitOppValue",
                        "content": numberWithCommas(userStats.I_CommitOppValue, primaryCurrency == "INR")
                    },
                    {
                        "name": "I_LastWeekOppCount",
                        "content": userStats.I_LastWeekOppCount
                    },
                    {
                        "name": "I_OppClosingThisMonth",
                        "content": userStats.I_OppClosingThisMonth
                    },
                    {
                        "name": "I_CommitOppValuePerc",
                        "content": userStats.I_CommitOppValuePerc !== "-" ? parseFloat(userStats.I_CommitOppValuePerc.toFixed(2)) : userStats.I_CommitOppValuePerc
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": receiverEmailId,
                        "vars": [{
                            "name": "FirstName",
                            "content": userStats.userInfo.firstName
                        }]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            // logger.info(res);
            callback();
        },function(err) {
            // logger.info('A mandrill error occurred: ' +err);
            callback()
        });

    } catch (err) {
        throw err;
    }
}

emailSender.prototype.sendWarManager = function(userStats, numberWithCommas, primaryCurrency, callback){
    try{
        var subject = "Weekly Abstract Report",
            receiverEmailId = userStats.userInfo.emailId;

        var params = {
            "template_name": "WAR_Manager",
            "template_content": [
                {
                    "name": "WAR_Manager",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": receiverEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "T_Target",
                        "content": numberWithCommas(userStats.T_Target, primaryCurrency == "INR")
                    },
                    {
                        "name": "T_Won",
                        "content": numberWithCommas(userStats.T_Won, primaryCurrency == "INR")
                    },
                    {
                        "name": "T_Commit",
                        "content": numberWithCommas(userStats.T_Commit, primaryCurrency == "INR")
                    },
                    {
                        "name": "T_Won_percent",
                        "content": userStats.T_Won_percent !== "-" ? parseFloat(userStats.T_Won_percent.toFixed(2)) : userStats.T_Won_percent
                    },
                    {
                        "name": "T_Commit_percent",
                        "content": userStats.T_Commit_percent !== "-" ? parseFloat(userStats.T_Commit_percent.toFixed(2)) : userStats.T_Commit_percent
                    },
                    {
                        "name": "T_CommitPipeline_percent",
                        "content": userStats.T_CommitPipeline_percent !== "-" ? parseFloat(userStats.T_CommitPipeline_percent.toFixed(2)) : userStats.T_CommitPipeline_percent
                    },
                    {
                        "name": "T_RiskDealsCount",
                        "content": userStats.T_RiskDealsCount
                    },
                    {
                        "name": "T_RiskDealsValue",
                        "content": numberWithCommas(userStats.T_RiskDealsValue, primaryCurrency == "INR")
                    },
                    {
                        "name": "T_30DaysCount",
                        "content": userStats.T_30DaysCount
                    },
                    {
                        "name": "T_30DaysValue",
                        "content": numberWithCommas(userStats.T_30DaysValue, primaryCurrency == "INR")
                    },
                    {
                        "name": "T_StaleOppsCount",
                        "content": userStats.T_StaleOppsCount
                    },
                    {
                        "name": "T_StaleOppsValue",
                        "content": numberWithCommas(userStats.T_StaleOppsValue, primaryCurrency == "INR")
                    },
                    {
                        "name": "I_Tasks",
                        "content": userStats.I_Tasks
                    },
                    {
                        "name": "I_Meetings",
                        "content": userStats.I_Meetings
                    },
                    {
                        "name": "LosingTouchCount",
                        "content": userStats.LosingTouchCount
                    },
                    {
                        "name": "LosingTouchValue",
                        "content": userStats.LosingTouchValue
                    },
                    {
                        "name": "T_CommitPipeline",
                        "content": numberWithCommas(userStats.T_CommitPipeline, primaryCurrency == "INR")
                    },
                    {
                        "name": "startYourWeek",
                        "content": userStats.startYourWeek
                    },
                    {
                        "name": "T_DealWonPastWeek",
                        "content": numberWithCommas(userStats.T_DealWonPastWeek, primaryCurrency == "INR")
                    },
                    {
                        "name": "T_CommitOppCount",
                        "content": userStats.T_CommitOppCount
                    },
                    {
                        "name": "T_CommitStage",
                        "content": userStats.T_CommitStage
                    },
                    {
                        "name": "T_CommitOppValue",
                        "content": numberWithCommas(userStats.T_CommitOppValue, primaryCurrency == "INR")
                    },
                    {
                        "name": "T_LastWeekOppCount",
                        "content": userStats.T_LastWeekOppCount
                    },
                    {
                        "name": "T_OppClosingThisMonth",
                        "content": userStats.T_OppClosingThisMonth
                    },
                    {
                        "name": "T_CommitOppValuePerc",
                        "content": userStats.T_CommitOppValuePerc !== "-" ? parseFloat(userStats.T_CommitOppValuePerc.toFixed(2)) : userStats.T_CommitOppValuePerc
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": receiverEmailId,
                        "vars": [{
                            "name": "FirstName",
                            "content": userStats.userInfo.firstName
                        }]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            // logger.info(res);
            callback();
        },function(err) {
            // logger.info('A mandrill error occurred: ' +err);
            callback()
        });

    } catch (err) {
        throw err;
    }
}

emailSender.prototype.sendInvitationAcceptMailToSender = function(invitationDetails){
       try{
        var smtpTransport = setUpSmtp();
        var receiverName = invitationDetails.receiverName;
        //var receiverEmailId = invitationDetails.receiverEmailId;
         var message = "";
           if(invitationDetails.message != ''){
               message = "Message from "+receiverName+": "+invitationDetails.message+"";
           }
        var senderName = invitationDetails.senderName;
        var senderEmailId = invitationDetails.senderEmailId;
        //var description = invitationDetails.meeting.description;

           var params = {
               "template_name": "MeetingConfirmation",
               "template_content": [
                   {
                       "name": "MeetingConfirmation",
                       "content": "example content"
                   }
               ],
               "message": {
                   "subject": "Relatas : Your meeting with "+receiverName+" is confirmed  ",
                   "from_email": "no-reply@relatas.co.in",
                   "from_name": "Relatas",
                   "to": [
                       {
                           "email": senderEmailId,

                           "type": "to"
                       }
                   ],
                   "important": false,
                   "track_opens": null,
                   "track_clicks": null,
                   "auto_text": null,
                   "auto_html": null,
                   "inline_css": null,
                   "url_strip_qs": null,
                   "preserve_recipients": null,
                   "view_content_link": null,

                   "tracking_domain": null,
                   "signing_domain": null,
                   "return_path_domain": null,
                   "merge": true,
                   "global_merge_vars": [
                       {
                           "name": "var1",
                           "content": "Global Value 1"
                       }
                   ],
                   "merge_vars": [
                       {
                           "rcpt": senderEmailId,
                           "vars": [
                               {
                                   "name": "name1",
                                   "content": senderName
                               },
                               {
                                   "name": "name2",
                                   "content": receiverName
                               },
                               {
                                   "name":"meetingId1",
                                   "content":invitationDetails.invitationId
                               },
                               {
                                   "name":"meetingId2",
                                   "content":invitationDetails.invitationId
                               },
                               {
                                   "name":"meetingId3",
                                   "content":invitationDetails.invitationId
                               },
                               {
                                   "name":"userMsg",
                                   "content":message
                               }
                           ]
                       }
                   ]

               }
           }

           if(checkRequired(invitationDetails.file)){

               var file = new Buffer(invitationDetails.file).toString('base64');
               params.message.attachments = [
                   {
                       "type": "text/calendar",
                       "name": "calendar.ics",
                       "content": file
                   }
               ]
           }

           mandrill_client.messages.sendTemplate(params, function(res) {
               logger.info(res);
           },function(err) {
               // log(err);
               logger.info('A mandrill error occurred: ' +err);
           });

     } catch (err) {
        throw err;
      }
}

emailSender.prototype.sendInvitationAcceptMailToReceiver = function(invitationDetails){
       try{
        var smtpTransport = setUpSmtp();
        var receiverName = invitationDetails.receiverName;
        var receiverEmailId = invitationDetails.receiverEmailId;
        var senderName = invitationDetails.senderName;

           var params = {
               "template_name": "MeetingConfirmation",
               "template_content": [
                   {
                       "name": "MeetingConfirmation",
                       "content": "example content"
                   }
               ],
               "message": {
                   "subject": "Relatas : Your meeting with "+senderName+"  is confirmed ",
                   "from_email": "no-reply@relatas.co.in",
                   "from_name": "Relatas",
                   "to": [
                       {
                           "email": receiverEmailId,

                           "type": "to"
                       }
                   ],
                   "important": false,
                   "track_opens": null,
                   "track_clicks": null,
                   "auto_text": null,
                   "auto_html": null,
                   "inline_css": null,
                   "url_strip_qs": null,
                   "preserve_recipients": null,
                   "view_content_link": null,

                   "tracking_domain": null,
                   "signing_domain": null,
                   "return_path_domain": null,
                   "merge": true,
                   "global_merge_vars": [
                       {
                           "name": "var1",
                           "content": "Global Value 1"
                       }
                   ],
                   "merge_vars": [
                       {
                           "rcpt": receiverEmailId,
                           "vars": [
                               {
                                   "name": "name1",
                                   "content": receiverName
                               },
                               {
                                   "name": "name2",
                                   "content": senderName
                               },
                               {
                                   "name":"meetingId1",
                                   "content":invitationDetails.invitationId
                               },
                               {
                                   "name":"meetingId2",
                                   "content":invitationDetails.invitationId
                               },
                               {
                                   "name":"meetingId3",
                                   "content":invitationDetails.invitationId
                               },
                               {
                                   "name":"userMsg",
                                   "content":""
                               }
                           ]
                       }
                   ]

               }
           }

           if(checkRequired(invitationDetails.file)){
               var file = new Buffer(invitationDetails.file).toString('base64')
               params.message.attachments = [
                   {
                       "type": "text/calendar",
                       "name": "calendar.ics",
                       "content": file
                   }
               ]
           }

           mandrill_client.messages.sendTemplate(params, function(res) {
               logger.info(res);
           },function(err) {
               // log(err);
               logger.info('A mandrill error occurred: ' +err);
           });


     } catch (err) {
        throw err;
      }
}

emailSender.prototype.sendSuggestionInvitationMailToSender = function(invitationDetails,multipleSuggest){
        try{

        var receiverName1 = invitationDetails.to.receiverName;
        var receiverName2 = invitationDetails.to.receiverName;
        var senderName = invitationDetails.senderName;
        var senderEmailId = invitationDetails.senderEmailId;
        //var description = invitationDetails.meeting.description;

            var subject = "Relatas : Your revised suggested times to "+receiverName1+" has been sent";
            if(multipleSuggest){
                subject = "Relatas :  Your revised suggested times has been successfully sent"
                receiverName2 = "they"
            }

            var params = {
                "template_name": "RevisedTimeSuggestion_toSender",
                "template_content": [
                    {
                        "name": "RevisedTimeSuggestion_toSender",
                        "content": "example content"
                    }
                ],
                "message": {
                    "subject":subject ,
                    "from_email": "no-reply@relatas.co.in",
                    "from_name": "Relatas",
                    "to": [
                        {
                            "email": senderEmailId,

                            "type": "to"
                        }
                    ],
                    "important": false,
                    "track_opens": null,
                    "track_clicks": null,
                    "auto_text": null,
                    "auto_html": null,
                    "inline_css": null,
                    "url_strip_qs": null,
                    "preserve_recipients": null,
                    "view_content_link": null,

                    "tracking_domain": null,
                    "signing_domain": null,
                    "return_path_domain": null,
                    "merge": true,
                    "global_merge_vars": [
                        {
                            "name": "var1",
                            "content": "Global Value 1"
                        }
                    ],
                    "merge_vars": [
                        {
                            "rcpt": senderEmailId,
                            "vars": [
                                {
                                    "name": "senderName",
                                    "content": senderName
                                },
                                {
                                    "name": "receiverName1",
                                    "content": receiverName1
                                },
                                {
                                    "name": "receiverName2",
                                    "content": receiverName2
                                },
                                {
                                    "name":"meetingId",
                                    "content":invitationDetails.invitationId
                                }
                            ]
                        }
                    ]

                }
            }

            mandrill_client.messages.sendTemplate(params, function(res) {
                logger.info(res);
            },function(err) {
                // log(err);
                logger.info('A mandrill error occurred: ' +err);
            });

       } catch (err) {
        throw err;
       }
}

emailSender.prototype.sendCorporateTokenEmail = function(dataToSend){
    try{

        var params = {
            "template_name": "Corporate_Registration_Token",
            "template_content": [
                {
                    "name": "Corporate_Registration_Token",
                    "content": "example content"
                }
            ],
            "message": {
                "subject":"Relatas Key" ,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": dataToSend.emailId,

                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": dataToSend.emailId,
                        "vars": [
                            {
                                "name": "key",
                                "content": dataToSend.key
                            },
                            {
                                "name": "name",
                                "content": dataToSend.firstName
                            },
                            {
                                "name": "url",
                                "content": dataToSend.url
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
}

emailSender.prototype.sendDocUploadConfirmationMailToReceiver = function(mailData) {

    try {
        var documentUrl = domainName+'/readDocument/'+mailData.docInfo.documentId;
        var emailId = mailData.receivedBy.emailId;

        var image = '';
        if(mailData.headerImage == 'true' || mailData.headerImage == true){
            image += '<img style="width:100%; height:100px !important;" src="'+mailData.imageUrl+'">';
        }

        var params = {
            "template_name": "NewDocumentShare",
            "template_content": [
                {
                    "name": "NewDocumentShare",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": mailData.subject || "Relatas: Shared Document",
                "from_email": "no-reply@relatas.co.in",
                "from_name": mailData.uploadedBy.firstName || "Relatas",
                "to": [
                    {
                        "email": emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": emailId,
                        "vars": [
                            {
                                "name": "header",
                                "content":  image
                            },
                            {
                                "name": "sharedWithName",
                                "content": mailData.receivedBy.firstName || ''
                            },
                            {
                                "name": "sharedByName1",
                                "content": mailData.uploadedBy.firstName
                            },
                            {
                                "name": "sharedByName2",
                                "content":  mailData.uploadedBy.firstName
                            },
                            {
                                "name": "sharedByName3",
                                "content":  mailData.uploadedBy.firstName
                            },
                            {
                                "name": "url1",
                                "content":'readDocument/'+mailData.docInfo.documentId
                            },
                            {
                                "name": "url2",
                                "content": 'readDocument/'+mailData.docInfo.documentId
                            },
                            {
                                "name": "userMsg",
                                "content": "New document has been shared with you."
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client_massMails.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred in mass Mails: ' +err);
        });



    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendShareDocMailToNonRelatasUser = function(dataToSend) {
    console.log("Send mail to non relatas user:", dataToSend);
    try {
        var emailId = dataToSend.emailId;
        var url = domainName+'/'+dataToSend.url;

        var image = '';
        if(dataToSend.headerImage == 'true' || dataToSend.headerImage == true){
            image += '<img style="width:100%; height:100px !important;" src="'+dataToSend.imageUrl+'">';
        }

        var userMsg = dataToSend.userMsg;
        var sharedByEmail = dataToSend.sharedByEmail;
        var sharedByFirstName = dataToSend.sharedByFirstName;
        var sharedWithName = dataToSend.sharedWithName;
        var params = {
            "template_name": "NewDocumentShare",
            "template_content": [
                {
                    "name": "NewDocumentShare",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": dataToSend.subject || "Relatas: Shared Document",
                "from_email": "no-reply@relatas.co.in",
                "from_name": sharedByFirstName || "Relatas",
                "to": [
                    {
                        "email": emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": emailId,
                        "vars": [
                            {
                                "name": "header",
                                "content":  image
                            },
                            {
                                "name": "sharedWithName",
                                "content": sharedWithName
                            },
                            {
                                "name": "sharedByName1",
                                "content": sharedByFirstName
                            },
                            {
                                "name": "sharedByName2",
                                "content": sharedByFirstName
                            },
                            {
                                "name": "sharedByName3",
                                "content": sharedByFirstName
                            },
                            {
                                "name": "url1",
                                "content": dataToSend.url
                            },
                            {
                                "name": "url2",
                                "content": dataToSend.url
                            },
                            {
                                "name": "userMsg",
                                "content": userMsg
                            }
                        ]
                    }
                ]

            }
        }

        console.log("Params:", params);

        mandrill_client_massMails.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred in mass mails: ' +err);
        });


    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendShareDocMailToRelatasUser = function(dataToSend) {
    console.log("Send mail torelatas user:", dataToSend);

    try {
        var emailId = dataToSend.emailId;
        var url = domainName+''+dataToSend.url;
        var userMsg = dataToSend.userMsg;

        var image = '';
        if(dataToSend.headerImage == 'true' || dataToSend.headerImage == true){
            image += '<img style="width:100%; height:100px !important;" src="'+dataToSend.imageUrl+'">';
        }

        var sharedByEmail = dataToSend.sharedByEmail;
        var sharedByFirstName = dataToSend.sharedByFirstName;
        var receivedByFirstName = dataToSend.receivedByFirstName;
        var params = {
            "template_name": "NewDocumentShare",
            "template_content": [
                {
                    "name": "NewDocumentShare",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": dataToSend.subject ||  "Relatas: Shared Document",
                "from_email": "no-reply@relatas.co.in",
                "from_name": sharedByFirstName || "Relatas",
                "to": [
                    {
                        "email": emailId,

                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": emailId,
                        "vars": [
                            {
                                "name": "header",
                                "content":  image
                            },
                            {
                                "name": "sharedWithName",
                                "content": receivedByFirstName
                            },
                            {
                                "name": "sharedByName1",
                                "content": sharedByFirstName
                            },
                            {
                                "name": "sharedByName2",
                                "content": sharedByFirstName
                            },
                            {
                                "name": "sharedByName3",
                                "content": sharedByFirstName
                            },
                            {
                                "name": "url1",
                                "content": dataToSend.url
                            },
                            {
                                "name": "url2",
                                "content": dataToSend.url
                            },
                            {
                                "name": "userMsg",
                                "content": userMsg
                            }

                        ]
                    }
                ]

            }
        }

        console.log("Params:", params);

        mandrill_client_massMails.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred in mass mails: ' +err);
        });

    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendMeetingCancelOrDeletionMail = function(dataToSend,templateTitle,toAdmin) {

    try {

        var params = {
            "template_name": "Meeting_Cancel_deletion_Template",
            "template_content": [
                {
                    "name": "Meeting_Cancel_deletion_Template",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": dataToSend.subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": dataToSend.emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": dataToSend.emailId,
                        "vars": [
                            {
                                "name": "templateTitle",
                                "content":templateTitle
                            },
                            {
                                "name": "requesterName",
                                "content": dataToSend.toName
                            },
                            {
                                "name": "mailContent",
                                "content": dataToSend.mailContent
                            }
                        ]
                    }
                ]

            }
        }
        if(toAdmin == true){
            params.message.to.push(
                {
                    "email": "support@relatas.com",
                    "type": "bcc"
                }
            )
            params.message.merge_vars.push(
                {
                    "rcpt": "support@relatas.com",
                    "vars": [
                        {
                            "name": "templateTitle",
                            "content":templateTitle
                        },
                        {
                            "name": "requesterName",
                            "content": dataToSend.toName
                        },
                        {
                            "name": "mailContent",
                            "content": dataToSend.mailContent
                        }
                    ]
                }
            )
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });


    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendEmailMessage = function(params) {

    try {
      mandrill_client_massMails.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred in mass mails: ' +JSON.stringify(err));
        });

    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendDailyAgenda = function(params) {

    try {
        mandrill_client_massMails.messages.sendTemplate(params, function(res) {
           // loggerError.info(res);
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred in mass mails: ' +JSON.stringify(err));
            loggerError.info('A mandrill error occurred in mass mails: ' +JSON.stringify(err));
        });

    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        loggerError.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendUncaughtException = function(err,message,stack,type,toEmailId){
    try{
        var smtpTransport = setUpSmtp();
        var html = "Error: "+message+" <br>"+stack || ''
        if(toEmailId){
            html = message+" <br>"+stack || ''
        }
        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: "Relatas Error Report", // sender address
            to: toEmailId?toEmailId:"dev@relatas.com", // list of receivers
            subject: type || "Relatas Error Report", // Subject line
            text: type, // plaintext body
            html: html
        };

        // send mail with defined transport object
        smtpTransport.sendMail(mailOptions, function(error, response) {
            if(error) {
                logger.error(error);
            }else{
                logger.info("Relatas Error Report Message sent: " + response.message);
                logger.info(err.stack)
            }
        });
    } catch (err) {
        throw err;
    }
};

emailSender.prototype.sendAPIAccessAlerts = function(err,message,stack,type){
    try{
        var smtpTransport = setUpSmtp();
        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: "Relatas Alerts", // sender address
            to: "sudip@relatas.com", // list of receivers
            subject: "ALERT /admin/reports/ accessed", // Subject line
            text: "", // plaintext body
            html: ''
        };

        // send mail with defined transport object
        smtpTransport.sendMail(mailOptions, function(error, response) {
            if(error) {
                logger.error(error);
            }else{
                logger.info("Relatas alert Report Message sent: " + response.message);
            }
        });
    } catch (err) {
        throw err;
    }
};

/* NEW TEMPLATES */

emailSender.prototype.sendRegistrationConfirmationMail_new = function(userInfo){

    try{

        var firstName = userInfo.firstName;
        var emailId = userInfo.emailId;
        var designation = userInfo.designation || '';
        var companyName = userInfo.companyName || '';
        var info = designation+' '+companyName;
        var location= userInfo.location;
        var identity2 = userInfo.publicProfileUrl

        var params = {
            "template_name": "R_AccountCreation_Jul2018",
            "template_content": [
                {
                    "name": "R_AccountCreation_Jul2018",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": "Relatas : You have successfully registered",
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": emailId,

                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,

                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": userInfo.emailId,
                        "vars": [
                            {
                                "name": "FirstName",
                                "content": firstName
                            },
                            {
                                "name": "Info",
                                "content": info
                            },
                            {
                                "name": "Location",
                                "content": location
                            },
                            {
                                "name": "identity2",
                                "content": identity2
                            }
                        ]
                    }
                ]
            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
};

emailSender.prototype.sendMeetingLateMessage = function(dataToSend) {

    try {

        var params = {
            "template_name": "R_MeetingLateMsg",
            "template_content": [
                {
                    "name": "R_MeetingLateMsg",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": dataToSend.subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": dataToSend.emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": dataToSend.emailId,
                        "vars": [
                            {
                                "name": "senderPic",
                                "content": dataToSend.senderPic
                            },
                            {
                                "name": "senderName",
                                "content": dataToSend.senderName
                            },
                            {
                                "name": "senderInfo",
                                "content": dataToSend.senderInfo
                            },
                            {
                                "name": "senderLocation",
                                "content": dataToSend.senderLocation
                            },
                            {
                                "name": "receiverName",
                                "content": dataToSend.receiverName
                            },
                            {
                                "name": "LateMessage",
                                "content": dataToSend.lateMessage
                            },
                            {
                                "name": "Meeting",
                                "content": dataToSend.meetingTitle
                            },
                            {
                                "name": "mDate",
                                "content": dataToSend.mDate
                            },
                            {
                                "name": "mTime",
                                "content": dataToSend.mTime
                            },
                            {
                                "name": "mLocation",
                                "content": dataToSend.mLocation
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendMeetingCancelOrDeletionMail_new = function(dataToSend,MeetingStatus,toAdmin) {

    try {

        var params = {
            "template_name": "R_Meeting_Cancel_Delete",
            "template_content": [
                {
                    "name": "R_Meeting_Cancel_Delete",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": dataToSend.subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": dataToSend.emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": dataToSend.emailId,
                        "vars": [
                            {
                                "name": "MeetingStatus",
                                "content":MeetingStatus
                            },
                            {
                                "name": "senderName",
                                "content": dataToSend.senderName
                            },
                            {
                                "name": "senderInfo",
                                "content": dataToSend.senderInfo
                            },
                            {
                                "name": "senderLocation",
                                "content": dataToSend.senderLocation
                            },
                            {
                                "name": "receiverName",
                                "content": dataToSend.receiverName
                            },
                            {
                                "name": "meetingStatus",
                                "content": MeetingStatus
                            },
                            {
                                "name": "MeetingTitle",
                                "content": dataToSend.meetingTitle
                            },
                            {
                                "name": "MeetingAgenda",
                                "content": dataToSend.meetingAgenda
                            },
                            {
                                "name": "mDate",
                                "content": dataToSend.mDate
                            },
                            {
                                "name": "mTime",
                                "content": dataToSend.mTime
                            },
                            {
                                "name": "mLocation",
                                "content": dataToSend.mLocation
                            },
                            {
                                "name": "mMessage",
                                "content": dataToSend.mMessage
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });


    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendEmailMessage_new = function(params) {

    try {
        mandrill_client_massMails.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred in mass mails: ' +JSON.stringify(err));
        });

    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendMeetingConfirmEmail_new = function(dataToSend) {

    try {

        var params = {
            "template_name": "R_Meeting_Confim",
            "template_content": [
                {
                    "name": "R_Meeting_Confim",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": dataToSend.subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": dataToSend.emailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": dataToSend.emailId,
                        "vars": [
                            {
                                "name": "senderName",
                                "content": dataToSend.senderName
                            },
                            {
                                "name": "senderName2",
                                "content": dataToSend.senderName2
                            },
                            {
                                "name": "senderInfo",
                                "content": dataToSend.senderInfo
                            },
                            {
                                "name": "senderPic",
                                "content": dataToSend.senderPic
                            },
                            {
                                "name": "senderLocation",
                                "content": dataToSend.senderLocation
                            },
                            {
                                "name": "receiverName",
                                "content": dataToSend.receiverName
                            },
                            {
                                "name": "mDate",
                                "content": dataToSend.mDate
                            },
                            {
                                "name": "mTime",
                                "content": dataToSend.mTime
                            },
                            {
                                "name": "mLocation",
                                "content": dataToSend.mLocation
                            },
                            {
                                "name": "mMessage",
                                "content": dataToSend.message
                            },
                            {
                                "name": "meetingUrl",
                                "content": dataToSend.mUrl
                            }
                        ]
                    }
                ]

            }
        }

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        logger.error("Error in Email Sending!",err.stack);
        throw err;
    }
};

emailSender.prototype.sendInvitationMailToReceiver_NewTemplate_R_Meeting_Receiver = function(invitationDetails){

    try{

        var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;
        var meetingUrlAccept = domainName+'/today/details/'+invitationDetails.invitationId;

        if(invitationDetails.mAgenda.length > 100){
            var agenda = getTextLength(invitationDetails.mAgenda,100);
            agenda = agenda.replace(/\s/g, '&nbsp;');
            agenda +=' <a href='+meetingUrl+'>more</a>';
            invitationDetails.mAgenda = agenda;
        }

        var subject = "Relatas : You got an invitation from "+invitationDetails.senderName+" ";


        var params = {
            "template_name": "R_Meeting_Receiver",
            "template_content": [
                {
                    "name": "R_Meeting_Receiver",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": invitationDetails.receiverEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": invitationDetails.receiverEmailId,
                        "vars": [
                            {
                                "name": "receiverName",
                                "content": invitationDetails.receiverName || ''
                            },
                            {
                                "name": "senderPic",
                                "content": invitationDetails.senderPicUrl
                            },
                            {
                                "name": "senderName",
                                "content": invitationDetails.senderName
                            },
                            {
                                "name": "senderInfo",
                                "content": invitationDetails.senderInfo || ""
                            },
                            {
                                "name": "senderLocation",
                                "content": invitationDetails.senderLocation || ""
                            },
                            {
                                "name":"interactionsUrl",
                                "content":invitationDetails.interactionUrl
                            },
                            {
                                "name":"mDate",
                                "content":invitationDetails.mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mTime",
                                "content":invitationDetails.mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mLocation",
                                "content":invitationDetails.mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mAgenda",
                                "content":invitationDetails.mAgenda
                            },
                            {
                                "name":"lastInteractedOn",
                                "content":invitationDetails.lastInteractedOn.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"totalInteractions",
                                "content":invitationDetails.totalInteractions
                            },
                            {
                                "name":"commonConnections",
                                "content":invitationDetails.commonConnections == 'Not Found' ? invitationDetails.commonConnections.replace(/\s/g, '&nbsp;') : invitationDetails.totalCommonConnections + " \nCommon Connections found. Click here to see details"
                            },
                            {
                                "name": "meetingUrl",
                                "content": meetingUrlAccept
                            }
                        ]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
};

emailSender.prototype.sendInvitationMailToReceiver_NewTemplate_R_Meeting_Sender = function(invitationDetails){

    try{

        if(!invitationDetails.isRegisteredUser){
            var meetingUrl = domainName+'/schedule/partialProfile/next/onboarding?userId='+invitationDetails.n_senderId;
        } else {
            var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;
        }

        if(invitationDetails.mAgenda.length > 100){
            var agenda = getTextLength(invitationDetails.mAgenda,100);
            agenda = agenda.replace(/\s/g, '&nbsp;');
            agenda +=' <a href='+meetingUrl+'>more</a>';
            invitationDetails.mAgenda = agenda;
        }

        var subject = "Relatas : Your invitation has been successfully sent";

        var params = {
            "template_name": "R_Meeting_Sender",
            "template_content": [
                {
                    "name": "R_Meeting_Sender",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": invitationDetails.senderEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": invitationDetails.senderEmailId,
                        "vars": [
                            {
                                "name": "SenderName",
                                "content": invitationDetails.senderName || ''
                            },
                            {
                                "name": "receiverPic",
                                "content": invitationDetails.receiverPic
                            },
                            {
                                "name": "receiverName",
                                "content": invitationDetails.receiverName
                            },
                            {
                                "name": "receiverInfo",
                                "content": invitationDetails.receiverInfo || ""
                            },
                            {
                                "name": "receiverLocation",
                                "content": invitationDetails.receiverLocation || ""
                            },
                            {
                                "name":"interactionsUrl",
                                "content":meetingUrl
                            },
                            {
                                "name":"mDate",
                                "content":invitationDetails.mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mTime",
                                "content":invitationDetails.mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mLocation",
                                "content":invitationDetails.mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mAgenda",
                                "content":invitationDetails.mAgenda
                            },
                            {
                                "name":"lastInteractedOn",
                                "content":invitationDetails.lastInteractedOn.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"totalInteractions",
                                "content":invitationDetails.totalInteractions
                            },
                            {
                                "name":"commonConnections",
                                "content":invitationDetails.commonConnections == 'Not Found' ? invitationDetails.commonConnections.replace(/\s/g, '&nbsp;') : invitationDetails.totalCommonConnections + " \n Common Connections found. Click here to see details"
                            },
                            {
                                "name": "meetingUrl",
                                "content": meetingUrl
                            }
                        ]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
};

emailSender.prototype.web2Lead_Receiver = function(invitationDetails){

    try{

        var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;
        var meetingUrlAccept = domainName+'/today/details/'+invitationDetails.invitationId;

        if(invitationDetails.mAgenda.length > 100){
            var agenda = getTextLength(invitationDetails.mAgenda,100);
            agenda = agenda.replace(/\s/g, '&nbsp;');
            agenda +=' <a href='+meetingUrl+'>more</a>';
            invitationDetails.mAgenda = agenda;
        }

        var subject = "Relatas : You got an invitation from "+invitationDetails.senderName+" ";


        var params = {
            "template_name": "web2Lead_Receiver",
            "template_content": [
                {
                    "name": "web2Lead_Receiver",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": invitationDetails.receiverEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": invitationDetails.receiverEmailId,
                        "vars": [
                            {
                                "name": "receiverName",
                                "content": invitationDetails.receiverName || ''
                            },
                            {
                                "name": "senderPic",
                                "content": invitationDetails.senderPicUrl
                            },
                            {
                                "name": "senderName",
                                "content": invitationDetails.senderName
                            },
                            {
                                "name": "senderInfo",
                                "content": invitationDetails.senderInfo || ""
                            },
                            {
                                "name": "senderLocation",
                                "content": invitationDetails.senderLocation || ""
                            },
                            {
                                "name":"interactionsUrl",
                                "content":invitationDetails.interactionUrl
                            },
                            {
                                "name":"mDate",
                                "content":invitationDetails.mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mTime",
                                "content":invitationDetails.mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mLocation",
                                "content":invitationDetails.mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mAgenda",
                                "content":invitationDetails.mAgenda
                            },
                            {
                                "name":"lastInteractedOn",
                                "content":invitationDetails.lastInteractedOn.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"totalInteractions",
                                "content":invitationDetails.totalInteractions
                            },
                            {
                                "name":"commonConnections",
                                "content":invitationDetails.commonConnections == 'Not Found' ? invitationDetails.commonConnections.replace(/\s/g, '&nbsp;') : invitationDetails.totalCommonConnections + " \nCommon Connections found. Click here to see details"
                            },
                            {
                                "name": "meetingUrl",
                                "content": meetingUrlAccept
                            }
                        ]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
};

emailSender.prototype.web2Lead_Sender = function(invitationDetails){

    try{

        if(!invitationDetails.isRegisteredUser){
            var meetingUrl = domainName+'/schedule/partialProfile/next/onboarding?userId='+invitationDetails.n_senderId;
        } else {
            var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;
        }

        if(invitationDetails.mAgenda.length > 100){
            var agenda = getTextLength(invitationDetails.mAgenda,100);
            agenda = agenda.replace(/\s/g, '&nbsp;');
            agenda +=' <a href='+meetingUrl+'>more</a>';
            invitationDetails.mAgenda = agenda;
        }

        var subject = "Relatas : Your invitation has been successfully sent";

        var params = {
            "template_name": "Web2Lead_sender",
            "template_content": [
                {
                    "name": "Web2Lead_sender",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": invitationDetails.senderEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": invitationDetails.senderEmailId,
                        "vars": [
                            {
                                "name": "SenderName",
                                "content": invitationDetails.senderName || ''
                            },
                            {
                                "name": "receiverPic",
                                "content": invitationDetails.receiverPic
                            },
                            {
                                "name": "receiverName",
                                "content": invitationDetails.receiverName
                            },
                            {
                                "name": "receiverInfo",
                                "content": invitationDetails.receiverInfo || ""
                            },
                            {
                                "name": "receiverLocation",
                                "content": invitationDetails.receiverLocation || ""
                            },
                            {
                                "name":"interactionsUrl",
                                "content":meetingUrl
                            },
                            {
                                "name":"mDate",
                                "content":invitationDetails.mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mTime",
                                "content":invitationDetails.mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mLocation",
                                "content":invitationDetails.mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mAgenda",
                                "content":invitationDetails.mAgenda
                            },
                            {
                                "name":"lastInteractedOn",
                                "content":invitationDetails.lastInteractedOn.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"totalInteractions",
                                "content":invitationDetails.totalInteractions
                            },
                            {
                                "name":"commonConnections",
                                "content":invitationDetails.commonConnections == 'Not Found' ? invitationDetails.commonConnections.replace(/\s/g, '&nbsp;') : invitationDetails.totalCommonConnections + " \n Common Connections found. Click here to see details"
                            },
                            {
                                "name": "meetingUrl",
                                "content": meetingUrl
                            }
                        ]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
};

emailSender.prototype.sendInvitationMailToReceiver_NewTemplate_R_Meeting_Sender_Multi = function(invitationDetails){

    try{

        var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;

        if(invitationDetails.mAgenda.length > 100){
            var agenda = getTextLength(invitationDetails.mAgenda,100);
            agenda = agenda.replace(/\s/g, '&nbsp;');
            agenda +=' <a href='+meetingUrl+'>more</a>';
            invitationDetails.mAgenda = agenda;
        }

        var subject = "Relatas : Your invitation has been successfully sent";

        var params = {
            "template_name": "R_Meeting_Sender_Multi",
            "template_content": [
                {
                    "name": "R_Meeting_Sender_Multi",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": invitationDetails.senderEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": invitationDetails.senderEmailId,
                        "vars": [
                            {
                                "name": "senderName",
                                "content": invitationDetails.senderName || ''
                            },
                            {
                                "name": "senderPic",
                                "content": invitationDetails.senderPic
                            },
                            {
                                "name":"mDate",
                                "content":invitationDetails.mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mTime",
                                "content":invitationDetails.mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mLocation",
                                "content":invitationDetails.mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mAgenda",
                                "content":invitationDetails.mAgenda.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"meetingAttendees",
                                "content":invitationDetails.meetingAttendees
                            },
                            {
                                "name": "meetingUrl",
                                "content": meetingUrl
                            }
                        ]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
}

emailSender.prototype.sendInvitationMailToReceiver_NewTemplate_R_SuggestedTime_Receiver = function(invitationDetails){

    try{

        if(invitationDetails.partialProfile){
            var meetingUrlAccept = domainName+'/schedule/partialProfile/next/onboarding?userId='+invitationDetails.receiverId;
        } else {
            var meetingUrlAccept = domainName+'/today/details/'+invitationDetails.invitationId;
        }

        var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;

        var subject = "Relatas : "+invitationDetails.senderName+" has suggested a new meeting time";


        var params = {
            "template_name": "R_SuggestedTime_Receiver",
            "template_content": [
                {
                    "name": "R_SuggestedTime_Receiver",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": invitationDetails.receiverEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": invitationDetails.receiverEmailId,
                        "vars": [
                            {
                                "name": "receiverName",
                                "content": invitationDetails.receiverName || ''
                            },
                            {
                                "name": "senderPic",
                                "content": invitationDetails.senderPicUrl
                            },
                            {
                                "name": "senderName",
                                "content": invitationDetails.senderName
                            },
                            {
                                "name": "senderInfo",
                                "content": invitationDetails.senderInfo
                            },
                            {
                                "name": "senderLocation",
                                "content": invitationDetails.senderLocation
                            },
                            {
                                "name":"o_mDate",
                                "content":invitationDetails.o_mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"o_mTime",
                                "content":invitationDetails.o_mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"o_mLocation",
                                "content":invitationDetails.o_mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"n_mDate",
                                "content":invitationDetails.n_mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"n_mTime",
                                "content":invitationDetails.n_mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"n_mLocation",
                                "content":invitationDetails.n_mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name": "message",
                                "content": invitationDetails.message
                            },

                            {
                                "name": "meetingUrl",
                                "content": meetingUrlAccept
                            }
                        ]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
};

//Send new meeting details email to the person who rescheduled the meeting

emailSender.prototype.sendInvitationMailToReceiver_NewTemplate_R_SuggestedTime_Sender = function(invitationDetails){

    try{

        var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;
        var subject = "Relatas : Your revised suggested times to "+invitationDetails.receiverName+" has been sent";

        var params = {
            "template_name": "R_SuggestedTime_Sender",
            "template_content": [
                {
                    "name": "R_SuggestedTime_Sender",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": invitationDetails.senderEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": invitationDetails.senderEmailId,
                        "vars": [
                            {
                                "name": "senderName",
                                "content": invitationDetails.senderName || ''
                            },
                            {
                                "name": "senderPic",
                                "content": invitationDetails.senderPic
                            },
                            {
                                "name":"o_mDate",
                                "content":invitationDetails.o_mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"o_mTime",
                                "content":invitationDetails.o_mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"o_mLocation",
                                "content":invitationDetails.o_mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"n_mDate",
                                "content":invitationDetails.n_mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"n_mTime",
                                "content":invitationDetails.n_mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"n_mLocation",
                                "content":invitationDetails.n_mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name": "message",
                                "content": invitationDetails.message
                            },
                            {
                                "name": "meetingUrl",
                                "content": meetingUrl
                            }
                        ]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
}

emailSender.prototype.sendInvitationMailToReceiver_NewTemplate_R_SuggestedTime_Sender_Multi = function(invitationDetails){

    try{

        var meetingUrl = domainName+'/today/details/'+invitationDetails.invitationId;
        var subject = "Relatas : Your revised suggested times to "+invitationDetails.receiverName+" has been sent";

        if(invitationDetails.mAgenda.length > 100){
            var agenda = getTextLength(invitationDetails.mAgenda,100);
            agenda = agenda.replace(/\s/g, '&nbsp;');
            agenda +=' <a href='+meetingUrl+'>more</a>';
            invitationDetails.mAgenda = agenda;
        }

        var params = {
            "template_name": "R_SuggestedTime_Sender_Multi",
            "template_content": [
                {
                    "name": "R_SuggestedTime_Sender_Multi",
                    "content": "example content"
                }
            ],
            "message": {
                "subject": subject,
                "from_email": "no-reply@relatas.co.in",
                "from_name": "Relatas",
                "to": [
                    {
                        "email": invitationDetails.senderEmailId,
                        "type": "to"
                    }
                ],
                "important": false,
                "track_opens": null,
                "track_clicks": null,
                "auto_text": null,
                "auto_html": null,
                "inline_css": null,
                "url_strip_qs": null,
                "preserve_recipients": null,
                "view_content_link": null,
                "tracking_domain": null,
                "signing_domain": null,
                "return_path_domain": null,
                "merge": true,
                "global_merge_vars": [
                    {
                        "name": "var1",
                        "content": "Global Value 1"
                    }
                ],
                "merge_vars": [
                    {
                        "rcpt": invitationDetails.senderEmailId,
                        "vars": [
                            {
                                "name": "senderName",
                                "content": invitationDetails.senderName || ''
                            },
                            {
                                "name": "senderPic",
                                "content": invitationDetails.senderPic
                            },
                            {
                                "name":"mDate",
                                "content":invitationDetails.mDate.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mTime",
                                "content":invitationDetails.mTime.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mLocation",
                                "content":invitationDetails.mLocation.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"mAgenda",
                                "content":invitationDetails.mAgenda.replace(/\s/g, '&nbsp;')
                            },
                            {
                                "name":"meetingAttendees",
                                "content":invitationDetails.meetingAttendees
                            },
                            {
                                "name": "meetingUrl",
                                "content": meetingUrl
                            }
                        ]
                    }
                ]

            }
        };

        mandrill_client.messages.sendTemplate(params, function(res) {
            logger.info(res);
        },function(err) {
            // log(err);
            logger.info('A mandrill error occurred: ' +err);
        });

    } catch (err) {
        throw err;
    }
}

function checkRequired(data){
    if(data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

function setUpSmtp()
{
    var nodeMailer = require("nodemailer");
    var smtpTransport = nodeMailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: "dev@relatas.com",
            pass: "rel1devoct30"
        }
    });


    return smtpTransport;
}

//TODo: This code is duplicate at present we need to see how we will sort this out with out call back hell!
function Generate64BitRandomNumber (length) {

    return crypto.randomBytes(Math.ceil(length * 3 / 4))
        .toString('base64')   // convert to base64 format
        .slice(0, length)        // return required number of characters
        .replace(/\+/g, '0')  // replace '+' with '0'
        .replace(/\//g, '0'); // replace '/' with '0'
}




module.exports = emailSender;