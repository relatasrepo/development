var relatasApp = angular.module('relatasApp', ['angular-loading-bar', 'ngSanitize']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

var notificationCategory = getParams(window.location.href).notifyCategory;
var notificationDate = getParams(window.location.href).notifyDate;

relatasApp.service('share', function() {
    return {
        setTargetForFy:function(value){
            this.targetForFy = value;
        }
    }
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function(ControllerChecker,$scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }

        else toastr.error("Please enter search content")
    };
});

relatasApp.controller("logedinUser", function(ControllerChecker,$scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {

        $rootScope.liuEmailId = response.Data.emailId;
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        $rootScope.opportunityStagesFilter = [{name:"All"}]
        $rootScope.opportunityStagesFilter = $rootScope.opportunityStagesFilter.concat(share.opportunityStages)
        share.companyDetails.currency.forEach(function (el) {
            share.currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                share.primaryCurrency = el.symbol;
            }
        });

        if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
            share.timezone = response.Data.timezone.name;
        }

        $rootScope.primaryCurrency = share.primaryCurrency;

        setTimeOutCallback(100,function () {
            share.currentFy = getCurrentFiscalYear("UTC",response.companyDetails.fyMonth);
            share.initialiseAccFilters(share.currentFy);
        })
    });
});

relatasApp.controller("liu_hierarchy", function(ControllerChecker,$scope, $http, share,$rootScope) {

    $rootScope.viewForSelected = "dashboard" //Default;
    $scope.openMenu = function(){
        $(".reports .left-panel").toggle();
    }

    $scope.dataBy = "org";
    $rootScope.achvBy = "org";

    function loadPortfolios(){

        $scope.portfolios = [];

        if(share.portfolioDetails){
            $scope.groupedPortfolios = share.portfolioDetails.groupedPortfolios;
            $scope.portfolios = _.map($scope.groupedPortfolios,"type");
            $scope.widthStyle = share.portfolioDetails.widthStyle;
            $scope.colClass = share.portfolioDetails.colClass;
            // share.getAchievementByPortFolios($rootScope.achvBy);
        } else {
            setTimeOutCallback(1500,function () {
                loadPortfolios();
            })
        }
    };

    loadPortfolios();

    $scope.achievementsBy = function(achvBy){
        mixpanelTracker("Insights>Achievements "+achvBy);
        $rootScope.achvBy = achvBy;
        share.getAchievementByPortFolios(achvBy);
    }

    $scope.closePa = function(){
        $scope.showByPa = false;
        if(!share.selectedPortfolio){
            $scope.dataBy = "org";
        }
    }

    closeAllDropDownsAndModals($scope,".portfolio-ls",null,share);

    $scope.getOrgDataBy = function(){
        $scope.dataBy = "org";
        share.selectedPortfolio = null;
        $scope.showByPa = false;
        $rootScope.portfolioName = null;

        _.each($scope.groupedPortfolios,function (po) {
            _.each(po.data,function (el) {
                el.selected = false;
            });
        });

        $scope.team = share.team;

        $scope.selection = {
            fullName:"Show all team members",
            emailId:"Show all team members",
            nameNoImg:"All",
            noPicFlag:true
        };

        share.selection = $scope.selection;
        share.drawOppsTable($scope.selection.emailId);
        share.resetPrevfilters();
    };

    $scope.getDataBySomething = function(data) {

        $rootScope.portfolioName = data.name;
        data.selected = data.name;
        share.selectedPortfolio = data;
        $scope.team = [];
        $scope.selection = null;
        $scope.canViewSummary = false;

        if(data && data.users && data.users.length>0){
            _.each(data.users,function (user) {

                var obj = share.usersDictionary[user.ownerEmailId];
                if(user.isHead){
                    obj.underlineStyle = "text-decoration: underline;";
                    $scope.selection = obj;
                }

                $scope.team.push(obj)
            });
        };

        if(!$scope.selection){
            $scope.selection = $scope.team[0];
        }

        share.selection = $scope.selection;
        share.drawOppsTable($scope.selection.emailId);
        share.resetPrevfilters();
    }

    $scope.openPortfolios = function(){
        $scope.displayPortfolios = true;
        $scope.showByPa = true;
        mixpanelTracker("Insights Portfolios "+$scope.dataBy);
    };

    $scope.dataAsOf = "Data as of "+moment().subtract(1,"day").format(standardDateFormat());

    $scope.matrixAccess = function(){
        share.matrixAccessView = !share.matrixAccessView;
        $scope.matrixAccessView = !$scope.matrixAccessView;
        $scope.matrixAccessStyle = "";

        if(share.matrixAccessView){
            $scope.matrixAccessStyle = "matrixAccessStyle"
        }

        share.drawOppsTable(share.liuData.emailId,null,share.matrixAccessView)
    }

    $scope.isTop100 = true;
    share.toggleFySelection = function(by){
        $scope.isTop100 = by === 'opp'

        if($scope.isTop100){
            share.initialiseAccFilters(share.currentFy);
        }
    }

    function loadTypes(){

        $http.get('/company/user/all/hierarchy/types')
          .success(function (response) {
              if(response && response.SuccessCode){
                  $scope.hierarchyList = [];

                  _.each(response.Data,function (el) {
                      $scope.hierarchyList.push({
                          name:el
                      })
                  });

                  $scope.selectedHierarchy = $scope.hierarchyList[0];
              }
          })
    };

    setTimeOutCallback(1500,function () {
        loadTypes();
    })

    share.initialiseAccFilters = function(data){
        var startOfQuarter = data.quarter.obj[data.quarter.currentQuarter].start,
          endOfQuarter = data.quarter.obj[data.quarter.currentQuarter].end;

        $scope.start = {
            month: moment(startOfQuarter).format("MMM"),
            year: moment(startOfQuarter).format("YYYY")
        }
        $scope.end = {
            month: moment(endOfQuarter).format("MMM"),
            year: moment(endOfQuarter).format("YYYY")
        }

        $scope.oppRange = "Greater than";
        $scope.oppValue = 0;
        $scope.intRange = "Greater than";
        $scope.intValue = 0;
    }

    $scope.toggleQtrList = function(){
        $scope.ifOpenList = !$scope.ifOpenList;
        mixpanelTracker("Dashboard Quarter Range Selection");
    }

    $scope.dataForQuarter = function(qtr,viewFor){
        share.qtrFilterSelected = qtr
        $scope.qtrFilterSelected = qtr.display
        $scope.ifOpenList = !$scope.ifOpenList

        var emailIds = $scope.selection && $scope.selection.emailId?$scope.selection.emailId:null;
        if($scope.selection.emailId == "Show all team members"){
            emailIds = "all"
        }

        if(viewFor == "achievements"){
            share.getAchievementByPortFolios($rootScope.achvBy)
        } else {
            share.getDashBoardInsights(emailIds,qtr)
        }
    }

    $scope.months = monthsAndYear().months;
    $scope.years = monthsAndYear().years;

    share.resetAccountFilters = function(){
        $scope.oppRange = null;
        $scope.oppValue = null;
        $scope.intRange = null;
        $scope.intValue = null;

        $scope.start = {};
        $scope.end = {};

        $rootScope.opportunityStagesFilter = [{name:"All"}]
        $rootScope.opportunityStagesFilter = $rootScope.opportunityStagesFilter.concat(share.opportunityStages)
        $scope.selectedStage = $rootScope.opportunityStagesFilter[0];
    }

    $scope.filterAccs = function(){

        mixpanelTracker("Insights>Accs>Apply Filter");
        var oppRange = {
            range: $scope.oppRange,
            value: $scope.oppValue
        }

        var intRange = {
            range: $scope.intRange,
            value: $scope.intValue
        }

        if($scope.start && $scope.start.month && $scope.start.year){
            var start = new Date(moment().year(parseInt($scope.start.year)).month(parseInt(moment().month($scope.start.month).format("M")-1)).startOf('month'))
        }

        if($scope.end && $scope.end.year && $scope.end.month){
            var end = new Date(moment().year(parseInt($scope.end.year)).month(parseInt(moment().month($scope.end.month).format("M")-1)));
        }

        share.accAndOppFilterApplied = $scope.selectedStage.name;

        share.filterAccounts($scope.selectedStage,oppRange,intRange,start,end)
    }

    getFilterDates(share,$scope);

    share.getDataForLiu = function() {
        var liuUser = _.filter($scope.team, function(user) {
            return (user.emailId === $rootScope.liuEmailId);
        })
        $scope.getDataFor(liuUser[0] ? liuUser[0] : "all");
    }

    share.getDataFor = function(member) {
        $scope.getDataFor(member);
    }

    $scope.getDataFor = function (member) {

        $scope.selection = member && member != "all"?member:{fullName:"Show all team members",emailId:"Show all team members",nameNoImg:"All",noPicFlag:true};
        share.selection = $scope.selection;

        if($rootScope.viewForSelected == "region"){

            $rootScope.oppUrl = '/reports/opportunities';
            $rootScope.popUrl = '/reports/opportunities/edit/popup';
            share.regionChart($scope.selection.fullName == "Show all team members");
        }

        if($rootScope.viewForSelected == "dashboard" || !$rootScope.viewForSelected){
            if($scope.selection.fullName == "Show all team members"){
                share.getDashBoardInsights("all",share.qtrFilterSelected);
            } else {
                share.getDashBoardInsights(member.emailId?member.emailId:null,share.qtrFilterSelected);
            }
        }

        share.rangeType = null // Reset range type
        $scope.selectFromList = !$scope.selectFromList
        share.setAllTeamMembers($scope.selection.fullName == "Show all team members");
        share.setLoaders2();

        if($rootScope.viewForSelected == "opportunity"){

            $rootScope.oppUrl = '/reports/opportunities';
            $rootScope.popUrl = '/reports/opportunities/edit/popup';

            function checkCtrlLoad() {

                if($("#opportunity-insights") && $("#opportunity-insights").length>0){

                    share.setLoaders();
                    share.resetPrevfilters();
                    if($scope.selection.fullName !== "Show all team members"){
                        share.drawOppsTable($scope.selection.emailId)
                    } else {
                        share.drawOppsTable()
                    }

                } else {
                    setTimeOutCallback(50,function () {
                        checkCtrlLoad()
                    });
                }
            }

            checkCtrlLoad();
        };

        if($rootScope.viewForSelected == "forecast"){
            share.loadGraphs()
        }

        if($rootScope.viewForSelected == "accounts"){

            $rootScope.accUrl = '/reports/account';

            function checkCtrlLoadA() {

                if($(".accounts-ints") && $(".accounts-ints").length>0){
                    $scope.selectedStage = $rootScope.opportunityStagesFilter[0];
                    if($scope.selection.fullName == "Show all team members"){
                        setTimeOutCallback(500,function () {
                            share.getAccountInsights("all",share.qtrFilterSelected);
                        })
                    } else {
                        setTimeOutCallback(500,function () {
                            share.getAccountInsights(member.emailId?member.emailId:null,share.qtrFilterSelected);
                        })
                    }
                } else {
                    setTimeOutCallback(250,function () {
                        checkCtrlLoadA()
                    })
                }
            }

            checkCtrlLoadA();

            $(document).ready(function() {
                function checkLoaded(){
                    if($('#spiderChart3').width()){
                        $("#spiderChart3 svg").attr("width",$('#spiderChart3').width());
                    } else {
                        setTimeOutCallback(1000,function () {
                            checkLoaded();
                        })
                    }
                }

                checkLoaded();
            });
        };
    }

    closeAllDropDownsAndModals2($scope,".list");
    closeAllDropDownsAndModals2($scope,".forClosing");

    $scope.getLiuHierarchy = function (hierarchy) {

        mixpanelTracker("Insights Hierarchy selection "+hierarchy);

        var url = '/company/user/hierarchy/insights';

        if(hierarchy && hierarchy !== "Org. Hierarchy"){
            url = '/company/users/for/hierarchy';
            url = fetchUrlWithParameter(url,"hierarchyType",hierarchy.replace(/[^A-Z0-9]+/ig, "_"))
        }

        share.selectedHierarchy = hierarchy;

        $http.get(url)
          .success(function (response) {

              $scope.team = [];
              if(response && response.SuccessCode && response.Data && response.Data.length>0){
                  $scope.team = buildTeamProfiles(response.Data)

                  if($scope.team.length>1){
                      $scope.selection = {fullName:"Show all team members",emailId:"Show all team members",nameNoImg:"All",noPicFlag:true};
                  } else {
                      $scope.selection = $scope.team[0];
                  }

                  share.selection = $scope.selection

                  share.team = $scope.team;
                  var usersDictionary = {};
                  share.usersIdDictionary = {};

                  share.teamChildren = {};
                  _.each(response.listOfMembers,function (el) {
                      share.teamChildren[el.userEmailId] = el.teamMatesEmailId
                  });

                  if(response.companyMembers.length>0){
                      var companyMembers = buildAllTeamProfiles(response.companyMembers)

                      _.each(companyMembers,function (member) {
                          usersDictionary[member.emailId] = member
                          share.usersIdDictionary[member.userId] = member
                      });

                      share.teamMembers = companyMembers;
                  }

                  checkDashboardTemplateLoaded();

                  function checkDashboardTemplateLoaded(){
                      $rootScope.oppUrl = '/reports/opportunities';
                      $rootScope.popUrl = '/reports/opportunities/edit/popup';

                      if(share.drawOppsTable){

                          $rootScope.viewForSelected = "opportunity";

                          if($rootScope.viewForSelected == "dashboard"){
                              var panel = $scope.team.length>1?1:0;
                              var all = $scope.team.length>1?'all':null;

                              share.loadRightPanel(panel)
                              // share.loadRightPanel(6) //Loads the right page
                              panel = (notificationCategory == "dealsAtRiskForUser") ? 0 : panel;

                              if(!panel){
                                  share.openViewFor({
                                      name:"Opportunity",
                                      selected:"selected"
                                  })
                                  // share.getDashBoardInsights(all);
                              } else {
                                  share.getDashBoardInsights(all);
                              }

                          } else if($rootScope.viewForSelected == "opportunity") {
                              share.openViewFor({
                                  name:"Opportunity",
                                  selected:"selected"
                              });
                              share.loadRightPanel(2);
                              $rootScope.oppUrl = '/reports/opportunities';
                              $rootScope.popUrl = '/reports/opportunities/edit/popup';
                              setTimeOutCallback(500,function () {
                                  share.drawOppsTable();
                              })
                          }else if($rootScope.viewForSelected == "accounts") {
                              $rootScope.accUrl = '/reports/account';
                              setTimeOutCallback(500,function () {
                                  share.getAccountInsights("all",share.qtrFilterSelected);
                              });
                          }

                          // share.loadRightPanel(5)
                          // share.getDashBoardInsights();
                      } else {
                          setTimeOutCallback(500,function () {
                              checkDashboardTemplateLoaded();
                          });
                      }
                  }

                  share.usersDictionary = usersDictionary;
              }
          });
    }

    $scope.getLiuHierarchy("All Access");

    share.resetUserSelection = function(noReset){
        $scope.selection = $scope.team[0];
        share.teamData = $scope.team;
        $scope.getDataFor(noReset?"all":$scope.selection)
        $scope.selectFromList = false;
    }

});

relatasApp.controller("wrapper_controller", function(ControllerChecker,$scope, $http, share,$rootScope) {
    share.loadRightPanel = function(index){
        $scope.menu = menuItems(true,index);
        $scope.selectedTab = $scope.menu[index];
        $scope.viewFor = $scope.menu[index].name.toLowerCase();
        $rootScope.viewForSelected = $scope.viewFor;

        $scope.showLiu = $rootScope.viewForSelected == "today" ? false : true;
    }

    check_right_data_panel_loaded();

    function check_right_data_panel_loaded(){
        if(share.viewFor){
            share.viewFor($scope.viewFor);
        } else {
            setTimeOutCallback(100,function () {
                share.viewFor($scope.viewFor);
            })
        }
    }

    $scope.openViewFor = function (viewFor,redirectFrom) {
        handleSideBarSelections(ControllerChecker,$scope,$rootScope,share,viewFor,redirectFrom);
    };

    share.openViewFor = function(viewFor, redirectFrom){
        $scope.openViewFor(viewFor,redirectFrom)
    }

    share.viewFor = function (viewFor) {
        $scope.viewFor = viewFor;
    }

})

relatasApp.controller("exceptionalAccess", function(ControllerChecker,$scope,$http,share,$rootScope){

});

relatasApp.controller("opportunities", function(ControllerChecker,$scope,$http,share,$rootScope,searchService){

    $scope.loadingMetaData = true;
    closeAllDropDownsAndModals2($scope,".port-ls");

    $scope.selectAllVal = function(type){
        if(type && type.selected){
            _.each(type.values,function (va) {
                va.selected = true;
            });
        } else {
            _.each(type.values,function (va) {
                va.selected = false;
            });
        }
    }

    $scope.getOppsBasedOnPortfolios = function(){

        $scope.filtersApplied = [];
        var portfolios = [];

        $scope.filtersApplied.push({
            name:moment(moment(share.quarterRange.qStart).add(1,"day")).format("MMM YYYY")+"-"+moment(moment(share.quarterRange.qEnd).subtract(1,"day")).format("MMM YYYY"),
            type:"closeDate",
            start:share.quarterRange.qStart,
            end:share.quarterRange.qEnd
        });

        _.each($scope.groupedFilters,function (el) {

            el.values.forEach(function (po) {
                if(po.selected){
                    portfolios.push(po.name);
                    $scope.filtersApplied.push({
                        displayName: po.name,
                        name: po.name,
                        type: po.type
                    });
                }
            })
        });

        _.each($scope.stageMetaInfo,function (el) {
            el.rangeType = "This Quarter"
        });

        if(portfolios.length>0){
            $scope.portfolioSelected = "portfolioSelected";
        } else {
            $scope.portfolioSelected = "";
        }

        share.drawOppsTable(share.selection.emailId,$scope.filtersApplied,share.matrixAccessView);
    };

    share.setLoaders = function(){
        $scope.loadingMetaData = true;
    }

    $scope.getDetails = function (colType) {

        if(colType.colType == "Deals At Risk"){

            if(share.selection && share.selection.emailId == "Show all team members"){
                alert("Deals at risk insights not available for team. Please select individual team members to view deals at risk");
            } else if(share.selectedPortfolio){
                alert("Deals at risk insights not available for portfolios. Please select individual team members from Org hierarchy to view deals at risk");
            } else {
                var userId = share.selection?share.selection.userId:share.liuData.userId;
                share.forDealsAtRisk(userId);
            }
        }
    }

    $scope.selectAll = function (colType) {
        _.each(colType.values,function (el) {
            el.selected = colType.selectingAll;
            $scope.selectFilter(colType.type,el)
        })
    }

    share.filterOpps = function (item,fromDashboard) {
        $scope.filterOpps(item,fromDashboard)
    }

    share.setCurrentQuarter = function (data) {

        var startOfQuarter = data.quarter.obj[data.quarter.currentQuarter].start,
          endOfQuarter = data.quarter.obj[data.quarter.currentQuarter].end;

        share.startOfQuarter = startOfQuarter;
        share.endOfQuarter = endOfQuarter;

        $scope.dateRange = {
            text:moment(startOfQuarter).format("MMM YYYY")+"-"+moment(endOfQuarter).format("MMM YYYY"),
            show:true
        };
    }

    $scope.goToOpp = function (op) {
        mixpanelTracker("Insights>opps>view opp ");
        $rootScope.oppTabView = true;
        share.getInteractionHistory(op);
        // getInteractionHistory($scope,$rootScope,searchService,$http,share,op,null);
    }

    share.drawIntGrowth = function (data) {
        drawIntGrowth($scope,share,data)
    }

    $scope.openView = function(viewFor){
        setTabView($scope,viewFor);
        $scope.viewModeFor = viewFor;
    }

    $scope.closeOppInsightsModal = function(){
        $scope.showOppInsights = false;
        $scope.rolesList = [];
    };

    $scope.filterOpps = function (item,fromDashboard) {

        if(!$scope.filtersApplied || $scope.filtersApplied.length>0){
            $scope.filtersApplied = [];
        }

        if(item.colType == "Won"){
            $scope.filtersApplied.push({
                name:"Close Won",
                type:"stageName"
            });
        }

        if(item.colType == "Lost"){
            $scope.filtersApplied.push({
                name:"Close Lost",
                type:"stageName"
            });
        }

        if(item.colType == "Deals At Risk"){
            $scope.filtersApplied.push({
                name:"Close Won",
                type:"source"
            });
        }

        if(item.colType == "Renewal"){
            $scope.filtersApplied.push({
                name:"renewal",
                type:"source"
            });
        }

        var start = share.startOfQuarter
        var end = share.endOfQuarter

        if(item.colType == "Closing"){

            _.each(share.companyDetails.opportunityStages,function (el) {
                if(el.name !== "Close Won" && el.name !== "Close Lost"){
                    $scope.filtersApplied.push({
                        name:el.name,
                        type:"stageName"
                    });
                }
            });

            $scope.start = {
                month:moment(start).month,
                year:moment(start).year,
            }

            $scope.end = {
                month:moment(end).month,
                year:moment(end).year,
            }

            $scope.filtersApplied.push({
                name:moment(moment(start).add(1,"day")).format("MMM YYYY")+"-"+moment(moment(end).subtract(1,"day")).format("MMM YYYY"),
                type:"closeDate",
                start:start,
                end:end
            });

        } else {

            $scope.filtersApplied.push({
                name:moment(moment(start).add(1,"day")).format("MMM YYYY")+"-"+moment(moment(end).subtract(1,"day")).format("MMM YYYY"),
                type:"closeDate",
                start:start,
                end:end
            })
        }

        $scope.applyFilters($scope.filtersApplied[0],true);
        share.rangeType = "This Quarter"
    }

    $scope.dropDownSelection = null;
    $scope.openFilterDropDown = function (type) {
        type.open = true;
        resetOtherDropDowns($scope,type)
    }

    $scope.sortType = "closeDate";
    $scope.sortReverse = false;
    $scope.sortTable = function (item) {
        $scope.sortReverse = !$scope.sortReverse;
        $scope.sortType = item.type
    }

    $scope.sortTableByNumbers = function (item) {
        if(item.type == 'amount' || item.type == 'netGrossMargin' || item.type == 'convertedAmt' || item.type == 'convertedAmtWithNgm'){
            $scope.sortReverse = !$scope.sortReverse;
            $scope.sortType = item.type
        }
    }

    closeAllDropDownsAndModals2($scope,".drop-down");

    share.resetPrevfilters = function () {
        $scope.start = {};
        $scope.end = {};

        $scope.filtersApplied = [];
        _.each($scope.headers,function (he) {
            if(he.values && he.values.length>0){
                _.each(he.values,function (va) {
                    va.selected = false;
                });
            }
        })
    }

    $scope.months = monthsAndYear().months
    $scope.years = monthsAndYear().years;

    $scope.start = {};
    $scope.end = {};

    $scope.selectFilter = function (type,filter,colType) {

        if(!$scope.filtersApplied){
            $scope.filtersApplied = [];
        }

        if(!filter.selected){
            $scope.filtersApplied = $scope.filtersApplied.filter(function (fl) {
                return fl.name !== filter.name
            })
        }

        if(filter.selected){

            var found = false;

            _.each($scope.filtersApplied,function (fl) {
                if(fl.name == filter.name && fl.type == type){
                    found = true;
                    return false;
                }
            });

            if(!found){

                if(type == 'userEmailId'){

                    share.selection = share.usersDictionary[filter.name]?share.usersDictionary[filter.name]:{fullName:filter.name,emailId:filter.name}

                    $scope.filtersApplied.push({
                        name:filter.name,
                        type:type,
                        displayName:share.usersDictionary[filter.name].fullName
                    });
                } else {
                    $scope.filtersApplied.push({
                        name:filter.name,
                        type:type,
                        displayName:filter.name
                    });
                }
            }
        }

        if(colType && colType.values){

            var selectingAll = true;
            _.each(colType.values,function (el) {
                if(!el.selected){
                    selectingAll = false;
                    return false;
                }
            })

            colType.selectingAll = selectingAll;
        }

        $scope.filtersApplied = _.uniqBy($scope.filtersApplied,"name");
    };

    share.applyFilters = function(colType) {
        $scope.applyFilters(colType);
    }

    $scope.applyFilters = function (colType,dontUpdateMetaData,index) {

        if(!$scope.filtersApplied){
            $scope.filtersApplied = [];
        }

        var closeDateExists = false;

        if(colType.type === "closeDate"){

            $scope.filtersApplied = $scope.filtersApplied.filter(function (fl) {
                return fl.type != "closeDate"
            });

            if($scope.start && typeof $scope.start.month == "string"){

                if($scope.start.date) {
                    var start = moment().year(parseInt($scope.start.year)).month(parseInt($scope.start.month)-1).date(parseInt($scope.start.date));
                } else {
                    var start = moment().year(parseInt($scope.start.year)).month(parseInt($scope.start.month)-1);
                }

                if($scope.end.date) {
                    var end = moment().year(parseInt($scope.end.year)).month(parseInt($scope.end.month)-1).date(parseInt($scope.end.date));
                } else {
                    var end = moment().year(parseInt($scope.end.year)).month(parseInt($scope.end.month)-1);
                }


                start = new Date(moment(start).startOf("month"));
                end = new Date(moment(end).endOf("month"));

                if(colType.includeDateRange) {
                    $scope.filtersApplied.push({
                        name:moment(moment(start).add(1,"d")).format("MMM DD YYYY")+"-"+moment(moment(end).subtract(1,"d")).format("MMM DD YYYY"),
                        type:colType.type,
                        start:new Date(moment(start).add(1,"d")),
                        end:new Date(moment(end).subtract(1,"d")),
                        includeDateRange: colType.includeDateRange
                    });

                } else {

                    $scope.filtersApplied.push({
                        name:moment(moment(start).add(1,"d")).format("MMM YYYY")+"-"+moment(moment(end).subtract(1,"d")).format("MMM YYYY"),
                        type:colType.type,
                        start:start,
                        end:end,
                        includeDateRange: colType.includeDateRange
                    });
                }

                share.rangeType = moment(moment(start).add(1,"d")).format("MMM YYYY")+"-"+moment(moment(end).subtract(1,"d")).format("MMM YYYY")
            }

        }

        $scope.dateRange.show = !$scope.dateRange.show;

        colType.open = !colType.open;

        _.each($scope.headers,function (he) {
            he.open = false;
        })

        var userEmailIdExists = false;
        _.each($scope.filtersApplied,function (el) {
            if(el.type == "userEmailId"){
                userEmailIdExists = true;
            }

            if(el.type == "closeDate"){
                closeDateExists = true;
            }
        });

        if(share.selection && share.selection.fullName !== "Show all team members"){
            $scope.filtersApplied.push({
                name:share.selection.emailId,
                displayName:share.selection.fullName,
                type:"userEmailId"
            })
        } else if(!userEmailIdExists && !share.selection){
            $scope.filtersApplied.push({
                name:share.liuData.emailId,
                displayName:share.liuData.fullName,
                type:"userEmailId"
            })
        }

        if(!closeDateExists){
            // share.rangeType = "All FYs"

            if(!share.quarterRange){
                share.quarterRange = {
                    qStart:share.portfolioDetails.qStart,
                    qEnd:share.portfolioDetails.qEnd,
                }
            }

            $scope.filtersApplied.push({
                name:moment(moment(share.quarterRange.qStart).add(1,"day")).format("MMM YYYY")+"-"+moment(moment(share.quarterRange.qEnd).subtract(1,"day")).format("MMM YYYY"),
                type:"closeDate",
                start:share.quarterRange.qStart,
                end:share.quarterRange.qEnd
            });
        }

        _.each($scope.stageMetaInfo,function (el) {
            el.rangeType = "This Quarter"
        })

        $scope.filtersApplied = _.uniqBy($scope.filtersApplied,"name");

        share.drawOppsTable(null,$scope.filtersApplied);
    }

    share.populateFilters = function(companyDetails){

        $scope.companyDetails = companyDetails;

        $scope.filterLists = [],$scope.filterListObj = {};
        for(var key in companyDetails){

            if(_.includes(["opportunityStages","accountTypes","businessUnits","geoLocations","productList","solutionList","sourceList","typeList","verticalList"], key)){

                var values = companyDetails[key];
                if(key === "geoLocations"){
                    values = [];
                    _.each(companyDetails[key],function (el) {
                        values.push({
                            name:el.region
                        })
                    });
                }

                values.forEach(function (el) {
                    if(key == "typeList"){
                        el.displayName = el.name; //this is needed for sorting.
                    }
                    el.selected = false;
                });

                var typeFormat = getTypeFormat(key);

                $scope.filterListObj[typeFormat] = {
                    type:key,
                    typeFormatted:typeFormat,
                    values:values
                }

                $scope.filterLists.push({
                    type:key,
                    typeFormatted:typeFormat,
                    values:values
                })
            }
        }
        setOppTableHeader($scope,share,$scope.filterListObj);
    }

    $scope.downloadFilteredOpps = function(){
        var url = "/reports/download/opportunities/v2";

        mixpanelTracker("Insights>opps>download");

        $http.post(url,share.filterForOppDownload)
          .success(function (response) {
              window.open(response.link);
          })
    }

    share.drawOppsTable = function (emailId,filters) {

        var filterObj = {};
        var url = "/reports/get/opportunities/v2"

        if(filters && filters.length>0){
            filterObj.filters = filters
        } else if(emailId && share.usersDictionary[emailId] && share.usersDictionary[emailId].userId) {
            filterObj.forUserEmailId = emailId
            filterObj.forUserId = share.usersDictionary[emailId].userId;
        } else if(!emailId && share.selection && share.selection.fullName !== "Show all team members") {
            filterObj.forUserEmailId = share.liuData.emailId
            filterObj.forUserId = share.liuData._id;
        }

        if(share.selection && share.selection.fullName === "Show all team members"){
            filterObj.allUserEmailId = [];
            filterObj.allUserId = [];

            _.each(share.team,function (tm) {
                filterObj.allUserEmailId.push(tm.emailId);
                filterObj.allUserId.push(tm.userId);
            });
        }

        filterObj["selectedHierarchy"] = share.selectedHierarchy;

        if(share.matrixAccessView){
            filterObj.matrixAccess = share.liuData.matrixAccess;
        }

        if(share.selectedPortfolio){
            if(share.selectedPortfolio){
                _.each(share.selectedPortfolio.users,function(pu){
                    if(emailId){
                        if(pu.ownerEmailId == emailId){
                            filterObj.selectedPortfolio = pu
                        }
                    } else {
                        if(pu.ownerEmailId == share.selection.emailId){
                            filterObj.selectedPortfolio = pu
                        }
                    }
                });
            }
        }

        share.filterForOppDownload = filterObj;

        $scope.selection = share.selection;
        $scope.selection.isHead = false;
        if(filterObj.selectedPortfolio && filterObj.selectedPortfolio.isHead){
            $scope.selection.isHead = true;
        }

        $http.post(url,filterObj)
          .success(function (response) {

              var importantHeaders = [],
                importantHeadersObj = {};

              if(response && response.masterData && response.masterData.length>0){
                  _.each(response.masterData,function (ma) {
                      if(ma.importantHeaders){
                          _.each(ma.importantHeaders,function (ih) {
                              if(ih.isImportant){
                                  importantHeaders.push(ih.name);
                              }
                          });
                      }
                  })
              };

              importantHeaders = _.sortBy(importantHeaders,function (el) {
                  return el.toLowerCase()
              });

              if(!response.dealsAtRisk){
                  response.dealsAtRisk = {
                      "count": 0,
                      "amount": 0,
                      dealsRiskAsOfDate : new Date()
                  }
              }

              _.each(importantHeaders,function (el) {
                  importantHeadersObj[el] = true;
              });

              setOppTableHeader($scope,share,$scope.filterListObj,importantHeaders);

              $scope.rangeType = share.rangeType?share.rangeType:"This Quarter"

              var thisQuarterOpps = [],
                thisQuarterOppsObj = {},
                monthStartDate = moment().startOf("month");

              var date30End = moment().add(30,"days");

              var contacts = [],
                owners = [],
                productsAll = [],
                accountsAll = [],
                wonAmt = 0,
                wonCount = 0,
                lostCount = 0,
                pipelineCount = 0,
                renewalCount = 0,
                staleCount = 0,
                staleAmt = 0,
                closing30DaysCount = 0,
                lostAmt = 0,
                closing30DaysAmt = 0,
                pipelineAmt = 0,
                renewalAmt = 0,
                targetAmt = 0,
                wonReasons = [],
                accounts = [],
                products = [],
                filterLists = [],
                locations = [],
                lostReasons = [],
                sourceTypes = [],
                createdThisMonthOpps = [],
                oppTypes = [];

              if(response.targets && response.targets.length>0){
                  targetAmt = _.sumBy(response.targets,"target")
              }

              var renewalTypes = [];

              _.each(share.companyDetails.typeList,function (tl) {
                  if(tl.isTypeRenewal){
                      renewalTypes.push(tl.name)
                  }
              })

              if(!$scope.filtersApplied || $scope.filtersApplied.length == 0){
                  $scope.filtersApplied = [];
              }

              $scope.oppsExists = false;
              if(response && response.opps){

                  _.each(response.opps,function (op) {

                      if(op.masterData && op.masterData.length>0){
                          _.each(op.masterData,function (ma) {
                              _.each(ma.data,function (da) {

                                  for(var key in da){
                                      if(importantHeadersObj[key]){

                                          if(!op.masterDataFormat){
                                              op.masterDataFormat = [];
                                          }

                                          if(da[key]){
                                              op.masterDataFormat.push({
                                                  key:key,
                                                  value:da[key]
                                              })
                                              op[key] = da[key];
                                          } else {
                                              op[key] = "";
                                              op.masterDataFormat.push({
                                                  key:key,
                                                  value:""
                                              })
                                          }
                                      }
                                  }
                              });
                          })
                      }

                      if(op.masterDataFormat && op.masterDataFormat.length>0){

                      } else if((!op.masterDataFormat || op.masterDataFormat.length === 0) && importantHeaders.length>0){
                          op.masterDataFormat = [];
                          _.each(importantHeaders,function (ih) {
                              op.masterDataFormat.push({
                                  key:ih,
                                  value:""
                              });
                          });
                      };

                      op.isNotOwner = false;
                      if(!op.stageName){
                          op.stageName = ""
                      }

                      op.isOppClosed = _.includes(op.stageName.toLowerCase(),"close");

                      if(op.isOppClosed){
                          if(share.liuData.corporateAdmin){
                              op.isNotOwner = false;
                          } else {
                              op.isNotOwner = true;
                          }
                      }

                      var nonExistingImpHeaders = _.xor(importantHeaders,_.map(op.masterDataFormat,'key'));

                      if(nonExistingImpHeaders && nonExistingImpHeaders.length>0){
                          _.each(nonExistingImpHeaders,function (ih) {
                              op.masterDataFormat.push({
                                  key:ih,
                                  value:""
                              })
                          });

                      };

                      op.masterDataFormat = _.sortBy(op.masterDataFormat,function (el) {
                          return el.key.toLowerCase()
                      });

                      if(op.partners && op.partners.length>0){
                          op.partnersList = "";
                          _.each(op.partners,function (pr) {
                              op.partnersList = pr.emailId+','+op.partnersList
                          });
                      }

                      op.amount = parseFloat(op.amount);
                      op.amountWithNgm = op.amount;

                      if(op.netGrossMargin || op.netGrossMargin == 0){
                          op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                      }

                      op.convertedAmt = op.amount;
                      op.convertedAmtWithNgm = op.amountWithNgm

                      if(op.currency && op.currency !== share.primaryCurrency){

                          if(share.currenciesObj[op.currency] && share.currenciesObj[op.currency].xr){
                              op.convertedAmt = op.amount/share.currenciesObj[op.currency].xr
                          }

                          if(op.netGrossMargin || op.netGrossMargin == 0){
                              op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                          }

                          op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

                      }

                      var acc = fetchCompanyFromEmail(op.contactEmailId);
                      op.account = acc?acc:"Others";

                      op.stageColor = "";

                      if(op.stageName == "Close Won") {
                          op.stageColor = "won"
                      }

                      if(op.stageName == "Close Lost") {
                          op.stageColor = "lost"
                      }

                      op.amount = parseFloat(op.amount);
                      op.amountWithNgm = op.amount;

                      if(op.netGrossMargin || op.netGrossMargin == 0){
                          op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                      }

                      op.convertedAmt = op.amount;
                      op.convertedAmtWithNgm = op.amountWithNgm

                      if(op.currency && op.currency !== share.primaryCurrency){

                          if(share.currenciesObj[op.currency] && share.currenciesObj[op.currency].xr){
                              op.convertedAmt = op.amount/share.currenciesObj[op.currency].xr
                          }

                          if(op.netGrossMargin || op.netGrossMargin == 0){
                              op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                          }

                          op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))
                      }

                      if(op.closeReasons && op.closeReasons.length>0){
                          op.closeReasonsString = "";
                          _.each(op.closeReasons,function (cr,index) {
                              if(index == 0){
                                  op.closeReasonsString = cr;
                              } else {
                                  op.closeReasonsString = op.closeReasonsString+","+cr;
                              }
                          })
                      }

                      var monthYear =  moment(op.closeDate).format("MMM YYYY");
                      op.monthYear = monthYear;

                      if(_.includes(["Close Won"], op.stageName)){
                          wonAmt = wonAmt+op.convertedAmtWithNgm
                          wonCount++;
                          if(op.closeReasons && op.closeReasons.length>0){
                              _.each(op.closeReasons,function (cr) {
                                  var wonObj = {
                                      name:cr?cr:"Others",
                                      amount:op.convertedAmtWithNgm
                                  }
                                  wonReasons.push(wonObj)
                              })
                          }

                          if(op.geoLocation && op.geoLocation.town){
                              locations.push(op.geoLocation.town)
                          }

                          var obj = {}
                          obj[op.productType?op.productType:"Others"] = op.convertedAmtWithNgm;
                          var accObj = {}
                          accObj[acc] = op.convertedAmtWithNgm
                          accounts.push(accObj)
                          products.push(obj);

                          oppTypes.push({
                              name:op.type,
                              amount:op.convertedAmtWithNgm
                          })

                          sourceTypes.push({
                              name:op.sourceType,
                              amount:op.convertedAmtWithNgm
                          })

                      } else if(_.includes(["Close Lost"], op.stageName)){
                          lostAmt = lostAmt+op.convertedAmtWithNgm
                          lostCount++

                          if(op.closeReasons && op.closeReasons.length>0){
                              _.each(op.closeReasons,function (cr) {
                                  var lostObj = {
                                      name:cr?cr:"Others",
                                      amount:op.convertedAmtWithNgm
                                  }
                                  lostReasons.push(lostObj)
                              })
                          }
                      } else {

                          if(new Date(op.closeDate)>= new Date() && new Date(op.closeDate)<= new Date(date30End)){
                              closing30DaysCount++;
                              closing30DaysAmt = closing30DaysAmt+op.convertedAmtWithNgm;
                          }

                          pipelineAmt = pipelineAmt+op.convertedAmtWithNgm
                          pipelineCount++
                      }

                      if(op.relatasStage !== "Close Lost" && op.relatasStage !== "Close Won"){
                          if(new Date(op.closeDate)< new Date(moment().startOf("day"))){
                              staleCount++;
                              staleAmt = staleAmt+op.convertedAmtWithNgm
                          }
                      }

                      if(!op.currency){
                          op.currency = share.primaryCurrency
                      }

                      if(_.includes(renewalTypes,op.type)){
                          renewalAmt = renewalAmt+op.amountWithNgm
                          renewalCount++;
                      }

                      op.amountFormatted = op.amount.r_formatNumber(2)
                      op.amountWithNgm = parseFloat(op.amountWithNgm.r_formatNumber(2))
                      op.convertedAmtWithNgm = parseFloat(op.convertedAmtWithNgm.r_formatNumber(2))

                      op.account = acc?acc:"Others";

                      op.closeDateFormatted = moment(op.closeDate).format(standardDateFormat());
                      contacts.push({
                          name:op.contactEmailId,
                          displayName:op.contactEmailId
                      });

                      owners.push({
                          name:op.userEmailId,
                          displayName:op.userEmailId
                      })

                      if(acc && acc != "Others"){
                          accountsAll.push({
                              name:acc?acc:"Others",
                              displayName:acc?acc:"Others"
                          })
                      }

                      op.owner = share.usersDictionary[op.userEmailId]?share.usersDictionary[op.userEmailId]:{fullName:op.userEmailId};

                      productsAll.push({
                          name:op.productType,
                          displayName:op.productType
                      });

                      if(new Date(op.createdDate)>= new Date(monthStartDate) && new Date(op.createdDate)<= new Date()){
                          createdThisMonthOpps.push(op)
                      }
                  });
                  if(share.redirectFrom == "today_overdue") {
                      $scope.opps = _.filter(response.opps, function(opp) {
                          return !(opp.stageName == 'Close Won' || opp.stageName == 'Close Lost')
                      });

                  } else {
                      $scope.opps = response.opps;
                  }

                  drawPipeline($scope,wonAmt,lostAmt,wonCount,
                    lostCount,pipelineAmt,pipelineCount,
                    closing30DaysAmt,closing30DaysCount,response.dealsAtRisk.count,
                    response.dealsAtRisk.amount,response.dealsAtRisk.dealsRiskAsOfDate,
                    renewalAmt,renewalCount,staleAmt,staleCount,share,targetAmt);
              }
              $scope.oppsExists = true;

              checkHeadersLoaded()
              function checkHeadersLoaded(){
                  if($scope.headers){

                      _.each($scope.headers,function (he) {

                          if(he.name == "Contact"){

                              if(!he.values || !he.values[0]){
                                  he.values = _.uniqBy(contacts,"name");
                              }
                          }

                          if(he.name == "Owner"){
                              he.values = _.uniqBy(owners,"name");
                              he.values.forEach(function (el) {
                                  el.displayName = share.usersDictionary[el.name]?share.usersDictionary[el.name].fullName:el.name
                              })
                          }

                          if(he.name == "Product" && productsAll && productsAll.length>0){
                              if(!he.values || !he.values[0]){
                                  he.values = _.uniqBy(productsAll,"name");
                              }
                          }

                          if(he.name == "Account"){
                              he.values = _.uniqBy(accountsAll,"name");
                          }
                      });

                      // Prepare Excel data:
                      $scope.fileName = 'Opps-'+moment().format("DDMMMMYY");
                      $scope.exportData = [];
                      // Headers:

                      $scope.exportData.push(getOppXLSHeaders(importantHeaders));
                      // Data:

                      angular.forEach($scope.opps, function(el, key) {
                          if(el.closeReasons && el.closeReasons.length>0){
                              el.closeReasonsString = "";
                              _.each(el.closeReasons,function (cr,index) {
                                  if(index == 0){
                                      el.closeReasonsString = cr;
                                  } else {
                                      el.closeReasonsString = el.closeReasonsString+","+cr;
                                  }
                              })
                          }

                          var arr = [
                              el.opportunityName,
                              el.userEmailId,
                              el.contactEmailId,
                              fetchCompanyFromEmail(el.contactEmailId),
                              el.currency,
                              parseFloat(el.amount),
                              parseFloat(el.netGrossMargin),
                              parseFloat(el.amountWithNgm.toFixed(2)),
                              share.currenciesObj[el.currency] && share.currenciesObj[el.currency].xr?share.currenciesObj[el.currency].xr:1,
                              parseFloat(el.convertedAmt),
                              parseFloat(el.convertedAmtWithNgm.toFixed(2)),
                              el.stageName,
                              new Date(el.closeDate),
                              el.productType,
                              el.businessUnit,
                              el.solution,
                              el.type,
                              el.geoLocation?el.geoLocation.zone:"",
                              el.geoLocation?el.geoLocation.town:"",
                              el.sourceType,
                              el.vertical,
                              _.map(el.partners,"emailId").join(","),
                              new Date(el.createdDate),
                              el.createdByEmailId,
                              el.opportunityId,
                              el.closeReasonsString,
                              el.closeReasonDescription
                          ];

                          if(el.masterDataFormat){
                              _.each(el.masterDataFormat,function (ma) {
                                  arr.push(ma.value);
                              });
                          }

                          $scope.exportData.push(arr);
                      });

                      $scope.loadingMetaData = false;
                  } else {
                      setTimeOutCallback(500,function () {
                          checkHeadersLoaded();
                      })
                  }
              }
          });
    }

    $scope.removeFilter = function (filter) {

        if(filter.type == "closeDate"){
            $scope.start.month = null;
            $scope.start.year = null;
            $scope.end.month = null;
            $scope.end.year = null;
        }

        _.each($scope.headers,function(he){
            if(he.type == filter.type && he.values && he.values.length>0){
                _.each(he.values,function(va){
                    if(va.name == filter.name){
                        va.selected = false;
                    }
                });
            }

        });

        $scope.filtersApplied = $scope.filtersApplied.filter(function (fl) {
            return fl.name !== filter.name
        });

        if($scope.filtersApplied.length == 0){
            share.rangeType = "This Quarter";
        } else {
            share.rangeType = "All FYs";
            _.each($scope.filtersApplied,function (fl) {
                if(fl.type == "closeDate"){
                    share.rangeType = fl.name;
                }
            })
        }

        var emailId = null;

        if(share.selectedPortfolio){
            emailId = share.selection.emailId;
        }

        share.drawOppsTable(emailId,$scope.filtersApplied)
    }

});

relatasApp.controller("currentInsights", function(ControllerChecker,$scope,$http,share,$rootScope){

    share.getCurrentInsights = function(response){
        currentInsights($scope,$http,share,response)
    }

    share.currentInsightsData = function(pipelinePercentage,achievementPercentage){
        $scope.pipelinePercentage = pipelinePercentage;
        $scope.achievementPercentage = achievementPercentage;
    }

    $scope.loadingMetaData = true;
});

relatasApp.controller("prediction", function ($scope,$http,$rootScope,share){

    $scope.sortType = 'amount';
    $scope.sortReverse = false;

    $scope.sortTable = function (item) {

        var sorType = "amount";

        if(item == "Opportunity Name"){
            sorType = "opportunityName"
        }

        if(item == "Company"){
            sorType = "account"
        }
        if(item == "Stage"){
            sorType = "stageName"
        }
        if(item == "Opp Owner"){
            sorType = "userEmailId"
        }

        if(item == "Selling To"){
            sorType = "contactEmailId"
        }

        if(item == "Contact Name"){
            sorType = "personEmailId"
        }

        if(item == "Type"){
            sorType = "prospect_customer"
        }

        if(item == "Contact Owner"){
            sorType = "ownerEmailId"
        }

        if(item == "closeDate"){
            sorType = "closeDate"
        }

        $scope.sortReverse = !$scope.sortReverse;
        $scope.sortType = sorType;
    }

    $scope.getDataForQtr = function (qtr) {
        var userIds = [];
        if(share.selection && share.selection.fullName === "Show all team members"){
            _.each(share.team,function (tm) {
                userIds.push(tm.userId);
            });
        } else {
            if(share.selection){
                userIds.push(share.selection.userId)
            } else {
                userIds.push(share.liuData.userId)
            }
        }

        var url = "/pulse/forecast/for/quarter?quarter="+qtr;

        if(userIds){
            url = fetchUrlWithParameter(url, "hierarchylist", userIds);
        }

        $scope.noDataFound = false;

        $http.get(url)
          .success(function (response) {
              $scope.opps = response;

              var won = 0;
              $scope.opps.forEach(function (op) {
                  op.closeDateFormatted = moment(op.closeDate).format(standardDateFormat());
                  op.account = fetchCompanyFromEmail(op.contactEmailId);

                  op.regions = null;

                  op.highlightContact = "";

                  if(op.geoLocation && op.geoLocation.town){
                      op.regions = op.geoLocation.town
                  }

                  if(op.stageName == "Close Lost"){
                      op.highlightContact = "lost";
                  }

                  if(op.stageName == "Close Won"){
                      won = won+op.amount;
                      op.highlightContact = "won";
                  }

                  if(op.geoLocation && op.geoLocation.zone){
                      if(op.regions){
                          op.regions = op.regions+", "+op.geoLocation.zone
                      } else {
                          op.regions = op.geoLocation.zone
                      }
                  }

                  op.amount = parseFloat(op.amount.toFixed(2));
              });

              $scope.opps = _.uniq($scope.opps,"_id");

              $scope.noDataFound = $scope.opps.length===0;
          })
    }

    share.loadGraphs = function () {

        var userIds = [];
        $scope.opps = [];

        if(share.selection && share.selection.fullName === "Show all team members"){
            _.each(share.team,function (tm) {
                userIds.push(tm.userId);
            });
        } else {
            if(share.selection){
                userIds.push(share.selection.userId)
            } else {
                userIds.push(share.liuData.userId)
            }
        }

        var url = "/pulse/forecast";

        if(userIds){
            url = fetchUrlWithParameter(url, "hierarchylist", userIds);
        }

        $scope.loadingDealsQtr1 = true;
        $scope.loadingDealsQtr2 = true;
        $scope.loadingDealsQtrC = true;
        $scope.loadingDealsQtrNext = true;

        $(".ct-chart-q1").empty();
        $(".ct-chart-q2").empty();
        $(".ct-chart-qcurrent").empty();
        $(".ct-chart-qnext").empty();

        setTimeOutCallback(100,function () {
            $http.get(fetchUrlWithParameter(url,"quarter","qc-2"))
              .success(function (response) {
                  $scope.qtrQ1 = response.qtr;
                  $scope.loadingDealsQtr1 = false;
                  drawChart(response,".ct-chart-q1",$scope,share);
              });
        });

        setTimeOutCallback(300,function () {
            $http.get(fetchUrlWithParameter(url,"quarter","qc-1"))
              .success(function (response) {
                  $scope.qtrQ2 = response.qtr;
                  $scope.loadingDealsQtr2 = false;
                  drawChart(response,".ct-chart-q2",$scope,share);
              });
        });

        setTimeOutCallback(500,function () {
            $http.get(fetchUrlWithParameter(url,"quarter","current"))
              .success(function (response) {
                  $scope.qtrCurrent = response.qtr;
                  $scope.loadingDealsQtrC = false;
                  drawChart(response,".ct-chart-qcurrent",$scope,share);
              });
        });

        setTimeOutCallback(800,function () {
            $http.get(fetchUrlWithParameter(url,"quarter","next"))
              .success(function (response) {
                  $scope.qtrNext = response.qtr;
                  $scope.loadingDealsQtrNext = false;
                  drawChart(response,".ct-chart-qnext",$scope,share);
              });
        });
    }

});

function drawChart(result,id,$scope,share) {

    if(result && result.data && result.data.length>0){
        result = result.data;
    }

    var label = [],
      won = [],
      target = [],
      commits = [],
      pipeline = [];

    if(result && result.length>0){
        result.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(result,function (el) {
            label.push(moment(el.date).format("MMM YY"));
            won.push(el.won)
            pipeline.push(el.pipeline)
            target.push(el.target)
            commits.push(el.commits)
        })
    }

    var pipelineFilled = [];

    if(id == ".ct-chart-qnext"){
        pipelineFilled = getCumulativeOfArray(pipeline)
    } else {
        pipelineFilled = arrayFiller(pipeline)
    }

    var commitsFilledCumulative = getCumulativeOfArray(commits);
    var wonCumulative = getCumulativeOfArray(won);

    var t = target[target.length-1],
      w = wonCumulative[wonCumulative.length-1],
      c = commitsFilledCumulative[commitsFilledCumulative.length-1],
      p = pipelineFilled[pipelineFilled.length-1];

    var wonPerc = "0%";
    var commitPerc = "0%";
    if(w>t && t == 0){
        wonPerc = "100%";
    } else if(w>t){
        wonPerc = calculatePercentage(w,t)+"%"
    } else {
        wonPerc = calculatePercentage(w,t);
        if(!isNaN(wonPerc)){
            wonPerc = wonPerc+"%"
        } else {
            wonPerc = "0%"
        }
    }

    if(c>t && t == 0){
        commitPerc = "100%"
    } else if(c>t){
        commitPerc = calculatePercentage(c,t)+"%";
    } else {
        commitPerc = calculatePercentage(c,t);
        if(!isNaN(commitPerc)){
            commitPerc = commitPerc+"%"
        } else {
            commitPerc = "0%"
        }
    }

    c = getAmountInThousands(c,2,share.primaryCurrency == "INR");
    t = getAmountInThousands(t,2,share.primaryCurrency == "INR");
    w = getAmountInThousands(w,2,share.primaryCurrency == "INR");
    p = getAmountInThousands(p,2,share.primaryCurrency == "INR");

    var showLabel = false;
    if(id == ".ct-chart-q1"){
        showLabel = true;
        $scope.t1 = t;
        $scope.w1 = w;
        $scope.wp1 = wonPerc;
        $scope.p1 =p;
        $scope.c1 = c;
        $scope.cp1 = commitPerc;
    }

    if(id == ".ct-chart-q2"){
        $scope.t2 = t;
        $scope.w2 = w;
        $scope.wp2 = wonPerc;
        $scope.p2 =p;
        $scope.c2 = c;
        $scope.cp2 = commitPerc;
    }

    if(id == ".ct-chart-qnext"){
        $scope.tnext = t;
        $scope.wnext = w;
        $scope.wpnext = wonPerc;
        $scope.pnext =p;
        $scope.cnext = c;
        $scope.cpnext = commitPerc;
    }

    if(id == ".ct-chart-qcurrent"){
        $scope.tcurrent = t;
        $scope.wcurrent = w;
        $scope.wpcurrent = wonPerc;
        $scope.pcurrent =p;
        $scope.ccurrent = c;
        $scope.cpcurrent = commitPerc;
    }

    new Chartist.Line(id, {
        labels: label,
        series: [wonCumulative,getCumulativeOfArray(target,true),pipelineFilled,commitsFilledCumulative]
    }, {
        low: 0,
        showArea: true,
        axisY:{
            // showLabel: showLabel,
            // showLabel: false,
            labelInterpolationFnc: function(value){
                return getAmountInThousands(value,2,share.primaryCurrency == "INR");
            },
            // type: Chartist.FixedScaleAxis,
            // ticks: [10000, 30000, 40000, 80000, 100000]
        },
        axisX:{
            showLabel:false
        },
        plugins: [
            Chartist.plugins.tooltip()
        ],
        width: '250px',
        height: '150px'
    });
}

function arrayFiller(arr) {
    for (var i = 1; i < arr.length; i++) {
        if(arr[i] === 0){
            arr[i] = arr[i-1];
        }
    }

    return arr;
}

function getCumulativeOfArray(arr,testing){
    var new_array = arr.concat(); //Copy initial array incase dont want to pollute original array.
    for (var i = 1; i < arr.length; i++) {
        new_array[i] = new_array[i-1] + arr[i];

    }

    return new_array;
}

function spiderChartInit(share,TranslateX,account,$scope,callback) {

    var RadarChart = {
        draw: function(id, d, options){
            var cfg = {
                radius: 2, //dot radii
                w: 300,
                h: 300,
                factor: 1,
                factorLegend: .85,
                levels: 5,
                maxValue: 100,
                radians: 2 * Math.PI,
                opacityArea: 0.5,
                ToRight: 5,
                TranslateX: TranslateX?TranslateX:80,
                TranslateY: 30,
                ExtraWidthX: 100,
                ExtraWidthY: 100,
                color: d3.scale.category10()
            };

            if('undefined' !== typeof options){
                for(var i in options){
                    if('undefined' !== typeof options[i]){
                        cfg[i] = options[i];
                    }
                }
            }
            cfg.maxValue = Math.max(cfg.maxValue, d3.max(d, function(i){return d3.max(i.map(function(o){return o.value;}))}));

            var tooltipClass = ".tooltip"
            var valueId = "#value"
            var cursorPosX = 100;
            var cursorPosY = 100;

            if(id == "#spiderChart1"){
                tooltipClass = ".tooltip2"
                valueId = "#value2"
            }

            if(id == "#spiderChart3"){
                tooltipClass = ".tooltip3"
                valueId = "#value3"
                cursorPosX = 100;
                cursorPosY = 200;
            }

            var allAxis = (d[0].map(function(i, j){return i}));
            var total = allAxis.length;
            var radius = cfg.factor*Math.min(cfg.w/2, cfg.h/2);
            var Format = d3.format('%');
            d3.select(id).select("svg").remove();

            var g = d3.select(id)
              .append("svg")
              .attr("width", cfg.w+cfg.ExtraWidthX)
              .attr("height", cfg.h+cfg.ExtraWidthY)
              .append("g")
              .attr("transform", "translate(" + cfg.TranslateX + "," + cfg.TranslateY + ")")
              .on('mousemove', function() {
                  if(d3.mouse(this) && d3.mouse(this)[0] && d3.mouse(this)[1]){
                      cursorPosX = d3.mouse(this)[0]
                      cursorPosY = d3.mouse(this)[1]
                  }
              });

            var tooltip;

            //Circular segments
            for(var j=0; j<cfg.levels-1; j++){
                var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
                g.selectAll(".levels")
                  .data(allAxis)
                  .enter()
                  .append("svg:line")
                  .attr("x1", function(d, i){return levelFactor*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
                  .attr("y1", function(d, i){return levelFactor*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
                  .attr("x2", function(d, i){return levelFactor*(1-cfg.factor*Math.sin((i+1)*cfg.radians/total));})
                  .attr("y2", function(d, i){return levelFactor*(1-cfg.factor*Math.cos((i+1)*cfg.radians/total));})
                  .attr("class", "line")
                  .style("stroke", "#ccc")
                  .style("stroke-opacity", "0.75")
                  .style("stroke-width", "0.3px")
                  .attr("transform", "translate(" + (cfg.w/2-levelFactor) + ", " + (cfg.h/2-levelFactor) + ")");
            }

            series = 0;

            var axis = g.selectAll(".axis")
              .data(allAxis)
              .enter()
              .append("g")
              .attr("class", "axis");

            axis.append("line")
              .attr("x1", cfg.w/2)
              .attr("y1", cfg.h/2)
              .attr("x2", function(d, i){return cfg.w/2*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
              .attr("y2", function(d, i){return cfg.h/2*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
              .attr("class", "line")
              .style("stroke", "grey")
              .style("stroke-width", "0.65px")
              .on('mouseover', function (d) {

                  var text = ""
                  var oppVal = 0;
                  var intsVal = 0;

                  if(account){

                      function checkAccObjLoaded(){
                          if(share.accObjForTooltip){
                              oppVal = parseFloat(share.accObjForTooltip[d.axis].oppsAmount.toFixed(2))
                              intsVal = parseFloat(share.accObjForTooltip[d.axis].interactionsCount.toFixed(2))
                          } else {
                              setTimeOutCallback(500,function () {
                                  checkAccObjLoaded()
                              })
                          }
                      }

                      checkAccObjLoaded();

                  } else {

                      function checkOppObjLoaded(){
                          if(share.oppObjForTooltip){
                              if(share.oppObjForTooltip[d.opportunityId]){
                                  oppVal = parseFloat(share.oppObjForTooltip[d.opportunityId].oppsAmount.toFixed(2))
                                  intsVal = parseFloat(share.oppObjForTooltip[d.opportunityId].interactionsCount.toFixed(2))
                              }
                          } else {
                              setTimeOutCallback(500,function () {
                                  checkOppObjLoaded()
                              })
                          }
                      }

                      checkOppObjLoaded();
                  }

                  oppVal = getAmountInThousands(oppVal,2,share.primaryCurrency == "INR")

                  var name = d.axis?d.axis.toUpperCase():""
                  if(account){
                      text = name+" | Value: "+oppVal+ " | Int: "+ intsVal
                  } else {
                      text = name+" | Value: "+oppVal+ " | Int: "+ intsVal + " | "+ share.oppObjForTooltip[d.opportunityId].contactEmailId
                  }

                  d3.select(tooltipClass)
                    .style("left", cursorPosX + "px")
                    .style("top", cursorPosY+35 + "px")
                    .select(valueId)
                    .text(text);

                  d3.select(tooltipClass).classed("hidden", false);

              })
              .on('mouseout', function (d) {
                  d3.select(this).style("stroke-width", ".65px");
                  d3.select(tooltipClass).classed("hidden", true);
              })
              .on("click", function(d) {

                  if(account){
                      window.location = "/accounts/all?accountName="+d.axis
                  } else {
                      window.location = "/opportunities/all?opportunityId="+d.opportunityId
                  }
              });

            axis.append("text")
              .attr("class", "legend")
              .on('mouseover', function (d){
                  var fullText = ""

                  $(this)
                    .attr("class", "no-pointer")
                    .text(fullText)
                    .css({'margin-top':'100px'})
                    .css({'font-size':'11px'})
                    .css({'pointer-events':'none'})
              })
              .on('mouseout', function(d){
                  $(this)
                    .attr("class", "legend")
                    .text(".")
                    .attr("fill", function (d) {
                    })
                    .css("font-size", "70px")
                    .css({'pointer-events':'auto'})

              })
              .attr("dy", "0.25em")
              .attr("transform", function(d, i){return "translate(0, -10)"})
              .attr("x", function(d, i){return cfg.w/2*(0.9-cfg.factorLegend*Math.sin(i*cfg.radians/total))-60*Math.sin(i*cfg.radians/total);})
              .attr("y", function(d, i){return cfg.h/2*(1-Math.cos(i*cfg.radians/total))-20*Math.cos(i*cfg.radians/total);})
              .text(function(d) {
                  if(!TranslateX){
                      return ".";
                  }
              })
              .attr("fill", function (d) {
              })
              .style("font-family", "Lato")
              .style("font-size", "70px")
              .attr("text-anchor", "middle")


            d.forEach(function(y, x){
                dataValues = [];
                var key3 = {
                    value:0
                };
                if(y && y[x] && y[x].value){
                    key3 = y[x]
                }

                g.selectAll(".nodes")
                  .data(y, function(j, i){
                      dataValues.push([
                          cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)),
                          cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total))
                      ]);
                  });
                dataValues.push(dataValues[0]);
                g.selectAll(".area")
                  .data([dataValues])
                  .enter()
                  .append("polygon")
                  .attr("class", "radar-chart-serie"+series)
                  .style("stroke-width", "2px")
                  .style("stroke", cfg.color(series))
                  .attr("data-id", function(j){return j.axis})
                  .attr("points",function(d) {
                      var str="";
                      for(var pti=0;pti<d.length;pti++){
                          str=str+d[pti][0]+","+d[pti][1]+" ";
                      }
                      return str;
                  })
                  .attr("cx", function(j, i){
                      return cfg.w/2*(1-(Math.max(key3.value, 0)/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total));
                  })
                  .attr("cy", function(j, i){
                      return cfg.h/2*(1-(Math.max(key3.value, 0)/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total));
                  })
                  .style("fill", function(j, i){return cfg.color(series)})
                  .style("fill-opacity", cfg.opacityArea)
                  .on('mouseover', function (d){

                      z = "polygon."+d3.select(this).attr("class");

                      d3.select(this).style("stroke-width", "3px");

                      g.selectAll("polygon")
                        .transition(200)
                        .style("fill-opacity", 0.1);
                      g.selectAll(z)
                        .transition(200)
                        .style("fill-opacity", .7);

                      var fullText = "";
                      var oppVal = 0;
                      var intsVal = 0;

                      var key = {
                          axis:null,
                          opportunityId:null
                      };

                      if(y && y[x] && y[x].axis){
                          key = y[x]
                      }

                      if(account){
                          oppVal = parseFloat(share.accObjForTooltip[key.axis].oppsAmount.toFixed(2))
                          intsVal = parseFloat(share.accObjForTooltip[key.axis].interactionsCount.toFixed(2))

                      } else {
                          oppVal = parseFloat(share.oppObjForTooltip[key.opportunityId].oppsAmount.toFixed(2))
                          intsVal = parseFloat(share.oppObjForTooltip[key.opportunityId].interactionsCount.toFixed(2))
                      }

                      oppVal = getAmountInThousands(oppVal,2,share.primaryCurrency == "INR")

                      var name = key.axis?key.axis.toUpperCase():"";

                      if(account){
                          fullText = name+" | Value: "+oppVal+ " | Int: "+ intsVal;
                      } else {
                          fullText = name+" | Value: "+oppVal+ " | Int: "+ intsVal + " | "+ share.oppObjForTooltip[key.opportunityId].contactEmailId
                      }

                      d3.select(tooltipClass)
                        .style("left", cursorPosX + "px")
                        .style("top", cursorPosY+35 + "px")
                        .select(valueId)
                        .text(fullText);

                      d3.select(tooltipClass).classed("hidden", false);

                      z = "polygon."+d3.select(this).attr("class");
                      g.selectAll("polygon")
                        .transition(200)
                        .style("fill-opacity", 0.1);
                      g.selectAll(z)
                        .transition(200)
                        .style("fill-opacity", .7);
                  })
                  .on('mouseout', function(){
                      g.selectAll("polygon")
                        .transition(200)
                        .style("fill-opacity", cfg.opacityArea);

                      d3.select(this).style("stroke-width", "2px");
                      d3.select(tooltipClass).classed("hidden", true);

                  })
                  .on("click", function(d,val) {

                      if(y && y[x] && y[x].axis){
                          if(account){
                              window.location = "/accounts/all?accountName="+y[x].axis
                          } else {
                              window.location = "/opportunities/all?opportunityId="+y[x].opportunityId
                          }
                      }
                  });
                series++;
            });
            series=0;

            d.forEach(function(y, x){
                g.selectAll(".nodes")
                  .data(y).enter()
                  .append("svg:circle")
                  .attr("class", "radar-chart-serie"+series)
                  .attr('r', cfg.radius)
                  .attr("alt", function(j){return Math.max(j.value, 0)})
                  .attr("cx", function(j, i){

                      dataValues.push([
                          cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)),
                          cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total))
                      ]);
                      return cfg.w/2*(1-(Math.max(j.value, 0)/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total));
                  })
                  .attr("cy", function(j, i){
                      return cfg.h/2*(1-(Math.max(j.value, 0)/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total));
                  })
                  .attr("data-id", function(j){return j.axis})
                  .style("fill", cfg.color(series)).style("fill-opacity", .9)
                  .on('mouseover', function (d){

                      d3.select(this).style("stroke-width", "3px");

                      var fullText = "";
                      var oppVal = 0;
                      var intsVal = 0;

                      if(account){
                          oppVal = parseFloat(share.accObjForTooltip[d.axis].oppsAmount.toFixed(2))
                          intsVal = parseFloat(share.accObjForTooltip[d.axis].interactionsCount.toFixed(2))

                      } else {
                          oppVal = parseFloat(share.oppObjForTooltip[d.opportunityId].oppsAmount.toFixed(2))
                          intsVal = parseFloat(share.oppObjForTooltip[d.opportunityId].interactionsCount.toFixed(2))
                      }

                      oppVal = getAmountInThousands(oppVal,2,share.primaryCurrency == "INR")

                      var name = d.axis?d.axis.toUpperCase():"";

                      if(account){
                          fullText = name+" | Value: "+oppVal+ " | Int: "+ intsVal;
                      } else {
                          fullText = name+" | Value: "+oppVal+ " | Int: "+ intsVal + " | "+ share.oppObjForTooltip[d.opportunityId].contactEmailId
                      }

                      d3.select(tooltipClass)
                        .style("left", cursorPosX + "px")
                        .style("top", cursorPosY+35 + "px")
                        .select(valueId)
                        .text(fullText);

                      d3.select(tooltipClass).classed("hidden", false);

                      z = "polygon."+d3.select(this).attr("class");
                      g.selectAll("polygon")
                        .transition(200)
                        .style("fill-opacity", 0.1);
                      g.selectAll(z)
                        .transition(200)
                        .style("fill-opacity", .7);
                  })
                  .on('mouseout', function(){

                      d3.select(tooltipClass).classed("hidden", true);
                      d3.select(this).style("stroke-width", "2px");
                  })
                  .append("svg:title")
                  .text(function(j){
                      // return Math.max(j.value, 0)
                  })
                  .on("click", function(d) {

                      if(y && y[x] && y[x].axis){
                          if(account){
                              window.location = "/accounts/all?accountName="+y[x].axis
                          } else {
                              window.location = "/opportunities/all?opportunityId="+y[x].opportunityId
                          }
                      }
                  });

                series++;
            });
            // Tooltip
            tooltip = g.append('text')
              .style('opacity', 0)
              .style('font-family', 'Lato')
              .style('font-size', '11px');

            var angleSlice = Math.PI * 2 / total;

            var radarLine = d3.svg.line.radial()
              .interpolate("basis")
              .radius(function(d) { return rScale(d.value); })
              .angle(function(d,i) {	return i*angleSlice; });

            var rScale = d3.scale.linear()
              .range([0, radius])
              .domain([0, cfg.maxValue]);

        }
    };

    callback(RadarChart)
}

function spiderDataInit(RadarChart,data,share,w,h,id,opps_original,interactions_original,forAccs,$scope,reverseSort,$http,verbose){
    w = w?w:200;
    h = h?h:200;
    id = id?id:"#spiderChart"

    var interactions = [],
      opps = [];

    _.each(opps_original,function (op) {
        opps.push(op)
    })

    _.each(interactions_original,function (op) {
        interactions.push(op)
    });

    //Legend titles
    var LegendOptions = ['Accounts','Opportunities'];

    var nonExistingInOpps = [],
      nonExistingInInts = [];

    if(interactions.length>opps.length){
        nonExistingInOpps = _.differenceBy(interactions,opps,"axis")
    } else {
        nonExistingInOpps = _.differenceBy(interactions,opps,"axis")
    }

    if(opps.length>interactions.length){
        nonExistingInInts = _.differenceBy(opps,interactions,"axis")
    } else {
        nonExistingInInts = _.differenceBy(opps,interactions,"axis")
    }

    if(nonExistingInOpps.length>0){
        _.each(nonExistingInOpps,function (el) {
            opps.push({
                axis:el.axis,
                original_val:0,
                value: 0,
                opportunityId:null
            })
        })
    }

    if(nonExistingInInts.length>0){
        _.each(nonExistingInInts,function (el) {
            interactions.push({
                axis:el.axis,
                original_val:0,
                value: 0,
                opportunityId:null
            })
        })
    }

    var accsIntsInsights = {
        avgInts: [],
        belowAvgInts: [],
        lastIntsBelow30Days: []
    }
    var oppsExistForAcc = {};

    var minInt = _.minBy(interactions,"value");
    var maxInt = _.maxBy(interactions,"value");
    var avgInt = _.meanBy(interactions,"value");
    var minOpp = _.minBy(opps,"value");
    var maxOpp = _.maxBy(opps,"value");
    var minOppAllowed = minOpp.value>0?1:0;
    var minIntAllowed = minInt.value>0?1:0;
    var dataOpps = [],
      dataInts = [];
    share.intsObj = {}

    var daysAgo30 = new Date(moment().subtract(30,'days'))

    if(opps.length>0) {
        _.each(opps,function (el) {

            if(verbose || forAccs){
                if(el.interactionsCount < avgInt) {
                    accsIntsInsights.belowAvgInts.push(el.axis)
                }
            }

            oppsExistForAcc[el.axis] = el.oppsAmount?el.oppsAmount:el.amountWithNgm;
            dataOpps.push({
                axis:el.axis,
                original_val:el.amountWithNgm,
                value: scaleBetween(el.value,minOpp.value,maxOpp.value,minOppAllowed),
                opportunityId:el.opportunityId,
                opps:verbose?el.opps:[]
            })
        })
    };

    _.each(interactions,function (el) {
        if(verbose){
            share.intsObj[el.axis] = {
                count:el.interactionsCount,
                lastInteractedDate:el.lastInteractedDate
            };
        }

        if(verbose || forAccs){
            if(el.value>avgInt){
                if(!oppsExistForAcc[el.axis]){
                    accsIntsInsights.avgInts.push(el.axis)
                }
            }

            if(new Date(el.lastInteractedDate)<daysAgo30){
                accsIntsInsights.lastIntsBelow30Days.push(el.axis)
            }
        }

        dataInts.push({
            axis:el.axis,
            original_val:el.value,
            value:scaleBetween(el.value,minInt.value,maxInt.value,minIntAllowed),
            opportunityId:el.opportunityId
        });
    })

    if(verbose || forAccs){
        $scope.accsIntsInsights = [];


        if(accsIntsInsights.avgInts.length>0){
            $scope.accsIntsInsights.push(accsIntsInsights.avgInts.length+" accounts have interactions above average. Opp creation recommended with those accounts")
        }

        if(accsIntsInsights.belowAvgInts.length>0){
            $scope.accsIntsInsights.push(accsIntsInsights.belowAvgInts.length+" accounts with opps have below average interactions")
        }

        if(accsIntsInsights.lastIntsBelow30Days.length>0){
            $scope.accsIntsInsights.push(accsIntsInsights.lastIntsBelow30Days.length+" accounts with opps not interacted in the past 30 days")
        }
    }

    if(verbose){
        getOppForAccounts($scope,$http,dataOpps,share.intsObj,share.primaryCurrency,share.accAndOppFilterApplied)
    }

    if(forAccs){
        share.accObjForTooltip = {};
        _.each(opps,function (op) {
            share.accObjForTooltip[op.axis] = {
                oppsAmount: op.oppsAmount?op.oppsAmount:0
            }
        });

        _.each(interactions,function (ints) {
            if(share.accObjForTooltip[ints.axis]){
                share.accObjForTooltip[ints.axis].interactionsCount = ints.interactionsCount?ints.interactionsCount:0
            } else {
                share.accObjForTooltip[ints.axis] = {
                    interactionsCount: ints.interactionsCount?ints.interactionsCount:0
                }
            }
        });

    } else {
        share.oppObjForTooltip = {};
        _.each(opps,function (op) {

            share.oppObjForTooltip[op.opportunityId] = {
                oppsAmount: op.value?op.value:0,
                contactEmailId:op.contactEmailId
            }
        });

        _.each(interactions,function (ints) {

            ints.oppsAmount = ints.amountWithNgm;

            if(share.oppObjForTooltip[ints.opportunityId]){
                share.oppObjForTooltip[ints.opportunityId].interactionsCount = ints.interactionsCount?ints.interactionsCount:0;

            } else {
                share.oppObjForTooltip[ints.opportunityId] = ints;
            }
        })
    }

    if(reverseSort){

        dataOpps.sort(function(a, b) {
            return a.value - b.value;
        });

        var accIntIndexes = dataOpps.reduce(function(lookup, key, index) {
            lookup[key.axis] = index;
            return lookup;
        }, {});

        dataInts.sort(function(k1, k2) {
            return accIntIndexes[k1.axis] - accIntIndexes[k2.axis];
        });
    } else {

        dataInts.sort(function(a, b) {
            return a.value - b.value;
        });

        var accIntIndexes = dataInts.reduce(function(lookup, key, index) {
            lookup[key.axis] = index;
            return lookup;
        }, {});

        dataOpps.sort(function(k1, k2) {
            return accIntIndexes[k1.axis] - accIntIndexes[k2.axis];
        });
    }

//Data
    var d = [dataInts,dataOpps];

//Options for the Radar chart, other than default
    var mycfg = {
        w: w,
        h: h,
        maxValue: 100,
        levels: 5,
        ExtraWidthX: 300
    }

//Call function to draw the Radar chart
//Will expect that data is in %'s
    RadarChart.draw(id, d, mycfg);

    var svg = d3.select('#body')
      .selectAll('svg')
      .append('svg')
      .attr("width", w+300)
      .attr("height", h)
}

function getOppXLSHeaders(importantHeaders){
    var data = ["Opp Name" ,
        "Opp Owner" ,
        "Contact (selling to)" ,
        "Account",
        "Currency",
        "Amount",
        "Margin",
        "Bottomline",
        "Exchange Rate",
        "Top Line (Primary Currency)",
        "Bottom Line (Primary Currency)",
        "Stage",
        "Close Date",
        "Product",
        "BU ",
        "Solution",
        "Type",
        "Region",
        "City",
        "Source",
        "Vertical",
        "Partners",
        "Created Date",
        "Created By" ,
        "Opportunity Id",
        "Close reasons",
        "Close reasons description" ];
    if(importantHeaders && importantHeaders.length>0){
        data = data.concat(importantHeaders);
    }

    return data;
}

relatasApp.controller("achievements", function(ControllerChecker,$scope,$http,share,$rootScope){

    if(window.location.pathname == "/insights/achievements"){

        function achievementsBy (type) {
            share.selectedPortoflioType = type;
            $scope.selectedPortoflioType = type;
            $scope.portfoliosObj = {};
            var portfolio = null;
            var portfolios = share.portfolioDetails.groupedPortfolios;

            if(portfolios && portfolios.length>0){
                _.each(portfolios,function(po){
                    $scope.portfoliosObj[po.type] = po.data
                    if(po.type == share.selectedPortoflioType){
                        portfolio = po.data;
                    }
                });
            }

            dataByPortfolio($scope,$http,share,$rootScope,portfolio);
        }

        $scope.loadingAchivements = true;

        share.getAchievementByPortFolios = function (selectedPortoflioType) {
            $scope.loadingAchivements = true;
            share.selectedPortoflioType = selectedPortoflioType?selectedPortoflioType:"Regions";
            achievementsBy(share.selectedPortoflioType);
        }

        $http.get("/opp/last/cached")
          .success(function (response) {
              $scope.achievementsLastUpdated = response;
          });

        share.initSummaryGraph = function (companies,products,verticals,regions,bus) {

            var companyData = groupAndChainForTeamSummary(companies,share)
            var productData = groupAndChainForTeamSummary(products,share)
            var verticalData = groupAndChainForTeamSummary(verticals,share)
            var regionData = groupAndChainForTeamSummary(regions,share)
            var buData = groupAndChainForTeamSummary(bus,share)
            $scope.companyAmount = 0;
            $scope.regionAmount = 0;
            $scope.productAmount = 0;
            $scope.verticalAmount = 0;
            $scope.buAmount = 0;

            if(companyData.length>0){
                $scope.companyAmount = getAmountInThousands(_.sumBy(companyData,"amount"),2,share.primaryCurrency == "INR");
            }

            if(regionData.length>0){
                $scope.regionAmount = getAmountInThousands(_.sumBy(regionData,"amount"),2,share.primaryCurrency == "INR");
            }

            if(productData.length>0){
                $scope.productAmount = getAmountInThousands(_.sumBy(productData,"amount"),2,share.primaryCurrency == "INR");
            }

            if(verticalData.length>0){
                $scope.verticalAmount = getAmountInThousands(_.sumBy(verticalData,"amount"),2,share.primaryCurrency == "INR");
            }

            if(buData.length>0){
                $scope.buAmount = getAmountInThousands(_.sumBy(buData,"amount"),2,share.primaryCurrency == "INR");
            }

            summaryChart(companyData,".company-chart",shadeGenerator(48,101,154,companyData.length,40));
            summaryChart(regionData,".region-chart",shadeGenerator(71,89,129, regionData.length,20));
            summaryChart(productData,".product-chart",shadeGenerator(0,115,76,productData.length,15));
            summaryChart(verticalData,".vertical-chart",shadeGenerator(250,158,70, verticalData.length,20));
            summaryChart(buData,".bu-chart",shadeGenerator(205,108,70, buData.length,20));

            $scope.companies = companyData.length>3?companyData.slice(0,3):companyData;
            $scope.regions = regionData.length>3?regionData.slice(0,3):regionData;
            $scope.products = productData.length>3?productData.slice(0,3):productData;
            $scope.verticals = verticalData.length>3?verticalData.slice(0,3):verticalData;
            $scope.bus = buData.length>3?buData.slice(0,3):buData;
        }

        loadKPIs(ControllerChecker,$scope,$http,share,$rootScope);

        $scope.displayToolTip = function (member) {
            member.showTooltip = true;

            if(member && member.targetsArray){

                member.targetsArray.sort(function (o1, o2) {
                    return o1.sort > o2.sort ? 1 : o1.sort < o2.sort ? -1 : 0;
                });

                _.each(member.targetsArray,function (el) {
                    el.valueWithCommas = numberWithCommas(el.value.r_formatNumber(2));
                })
            }

            if(member && member.monthlyOppWon){

                member.monthlyOppWon.sort(function (o1, o2) {
                    return o1.sort > o2.sort ? 1 : o1.sort < o2.sort ? -1 : 0;
                });

                _.each(member.monthlyOppWon,function (el) {
                    el.valueWithCommas = numberWithCommas(el.value.r_formatNumber(2));
                })
            }
        }

        $scope.displayToolTip2 = function (member) {
            member.showTooltip2 = true;

            if(member && member.monthlyCreated){

                member.monthlyCreated.sort(function (o1, o2) {
                    return o1.sort > o2.sort ? 1 : o1.sort < o2.sort ? -1 : 0;
                });

                _.each(member.monthlyCreated,function (el) {
                    el.valueWithCommas = numberWithCommas(el.value.r_formatNumber(2));
                })
            }

            if(member && member.monthlyClosed){

                member.monthlyClosed.sort(function (o1, o2) {
                    return o1.sort > o2.sort ? 1 : o1.sort < o2.sort ? -1 : 0;
                });

                _.each(member.monthlyClosed,function (el) {
                    el.valueWithCommas = numberWithCommas(el.value.r_formatNumber(2));
                })
            }
        }

        $scope.viewAccessLevels = function(user){

            $scope.groupedPortfolios = [];

            if($scope.portfoliosList && $scope.portfoliosList.length>0){
                _.each($scope.portfoliosList,function(po){
                    _.each(po.users,function(u){
                        u.headName = share.usersDictionary[u.ownerEmailId].fullName;
                        u.headEmailId = u.ownerEmailId;
                        if(user.emailId == u.ownerEmailId){
                            $scope.groupedPortfolios.push(u)
                        }
                    });
                });
            }

            user.openPortfoliosTb = !user.openPortfoliosTb;
        }

        $scope.seeAllAccess = function(user){

            $scope.allPortfolios = [
                {
                    name:"Business Units",
                    list:share.companyDetails.businessUnits
                },
                {
                    name:"Regions",
                    list:share.companyDetails.geoLocations
                },
                {
                    name:"Products",
                    list:share.companyDetails.productList
                },
                {
                    name:"Verticals",
                    list:share.companyDetails.verticalList
                }
            ];

            $scope.allPortfolios.forEach(function (al) {
                if(al.name == "Regions"){
                    al.list.forEach(function(re){
                        if(!re.name){
                            re.name = re.region
                        }
                    });
                }
                al.list.sort(function (a,b) {
                    if(a.name.toLowerCase() < b.name.toLowerCase()) return -1;
                    if(a.name.toLowerCase() > b.name.toLowerCase()) return 1;
                    return 0;
                })
            });

            var accessLevel = {}
            _.each(user.accessLevel,function(ac){
                accessLevel[ac.type] = ac.values;
            })

            _.each($scope.allPortfolios,function(al){
                var allSelected = true;
                _.each(al.list,function(li){
                    li.selected = accessLevel[al.name]?accessLevel[al.name].indexOf(li.name)>-1:false;
                    if(!li.selected){
                        allSelected = false;
                    }
                });

                al.allSelected = allSelected;
            });

            user.showAccess = true;
        }

        $scope.getAmountInthousandas = function(value){
            if(!value){
                value = 0;
            }
            return getAmountInThousands(value,2,share.primaryCurrency == "INR")
        }

        $scope.addKPIRow = function(){
            $scope.kpi.push({
                kpiFor:"",
                trainingTarget:"",
                trainingAchieved:"",
                demoTarget:"",
                demoAchieved:"",
                pocTarget:"",
                pocAchieved:"",
                logoTarget:"",
                logoAchieved:"",
                accTarget:"",
                accAchieved:"",
                marginTarget:"",
                marginAchieved:""
            })
        }
    }
});

function loadLeaderboard($scope,$http,share,$rootScope,portfolios){

    var userEmailId = share.selection.emailId;
    var emailIds = share.selection.emailId;
    var userIds = share.selection.userId;
    url = fetchUrlWithParameter("/reports/big/query")
    var qtrFilterSelected = share.qtrFilterSelected;

    var obj = {
        start: qtrFilterSelected?moment(qtrFilterSelected.range.qStart).toISOString():null,
        end: qtrFilterSelected?moment(qtrFilterSelected.range.qEnd).toISOString():null,
        userIds:_.map(share.team, "userId"),
        userEmailIds:_.map(share.team, "emailId")
    }

    share.portfolioUsers = [];
    share.portfolioUsersObj = {};
    var selectedPortfolios = [];

    if(portfolios){
        _.each(portfolios,function(portfolio){
            _.each(portfolio.users,function(pu){
                pu.userId = share.usersDictionary[pu.ownerEmailId].userId;
                share.portfolioUsers.push(pu.ownerEmailId);
                share.portfolioUsersObj[pu.ownerEmailId] = true;

                if(pu.isHead){
                    selectedPortfolios.push(pu);
                } else if(pu.ownerEmailId == share.liuData.emailId){
                    selectedPortfolios.push(pu);
                }
            });
        });
    }

    obj.selectedPortfolio = selectedPortfolios.length>0?selectedPortfolios:null;
    obj.usersDictionary = share.usersIdDictionary;
    $http.post(url,obj)
      .success(function (response) {

          $scope.allTeamData = [];
          $scope.quarterRange = response.allQuarters?response.allQuarters:null;
          var teamTarget = 0;
          var teamWon = 0;
          var wonDataByPortfolio = {};

          var companies = [],products = [],verticals = [],regions = [],bus = [];

          var prevWin = 0;
          if(response.data.prevQtr){
              _.each(response.data.prevQtr,function(el){
                  prevWin = prevWin+el.wonAmount;
              });
          }

          share.ytdPortfolioTarget = {};

          if(portfolios){
              _.each(portfolios,function(portfolio){
                  var totalYtdTarget = 0;
                  _.each(portfolio.users,function(user){
                      _.each(user.targets,function(tr){
                          _.each(tr.values,function(va){
                              if(new Date(va.date) >= new Date(response.fyRange.start) && new Date(va.date) <= new Date(response.allQuarters.end)){
                                  totalYtdTarget = totalYtdTarget+va.amount;
                              }
                          });
                      });
                  });
                  share.ytdPortfolioTarget[portfolio.name] = totalYtdTarget
              });
          }

          if(share.selectedPortoflioType == "org"){
              _.each(share.team,function (tm,i) {
                  var obj = {
                      designation:tm.designation,
                      emailId:tm.emailId,
                      fullName:tm.fullName,
                      image:tm.image,
                      userId:tm.userId,
                      children:tm.children,
                      noPicFlag:true,
                      nameNoImg:tm.fullName.substr(0,2).toUpperCase()
                  };

                  processLeaderBoardData(response.data.thisQtr,obj,i,companies,products,verticals,regions,bus,$scope.quarterRange,share.primaryCurrency);

                  if(obj.target){
                      teamTarget = teamTarget+obj.target;
                  }

                  if(obj.won){
                      teamWon = teamWon+obj.won;
                  }

                  $scope.allTeamData.push(obj)
              });

              share.teamList= $scope.allTeamData;

              var labelWithDates = enumerateDaysBetweenDates($scope.quarterRange.start,$scope.quarterRange.end);
              var label = _.map(labelWithDates,"month");
              var labelmonthYear = _.map(labelWithDates,"monthYear");
              var order = orderOfQuarterMonths(label);
              var qoqChange = 0;

              if(teamWon && prevWin){
                  qoqChange=(teamWon-prevWin)/prevWin*100
              }

              if(isNaN(qoqChange)){
                  qoqChange = 0;
              }

              $scope.qoqChange = qoqChange.toFixed(2)+"%";

              targetVsWonChart(null,teamTarget,teamWon,share.primaryCurrency);

              if(share.selectedPortoflioType == "org"){
                  share.initSummaryGraph(companies,products,verticals,regions,bus);
              }

              _.each($scope.allTeamData,function (tm,i) {

                  tm.pieChartId = "pie-"+i;
                  tm.pieChartId2 = "pie_2-"+i;
                  tm.pieChartId3 = "pie_3-"+i;
                  tm.lineChart = "line-chart-"+i;
                  tm.lineChart2 = "line-chart-achievement-"+i;
                  tm.lineChart2ToolTip = "team-custom-tooltip-"+i;
                  tm.lineChart3ToolTip = "conver-custom-tooltip-"+i;

                  setTimeOutCallback(100,function () {
                      drawLineChartLeaderBoardConversion($scope,share,tm.monthlyCreated,tm.monthlyClosed,"."+tm.lineChart,labelWithDates,label,order);
                  });

                  setTimeOutCallback(100,function () {
                      drawLineChartLeaderBoard($scope,share,tm.targetsArray,tm.monthlyOppWon,tm.commits,"."+tm.lineChart2,labelWithDates,label,labelmonthYear,order,'achievement');
                  });

              })

              $scope.teamRank = getRanking($scope.allTeamData);

          } else {

              loadLeaderboardTable($scope,$http,share,$rootScope,obj,portfolios);
          }

          achievementByPortfolios($scope
            ,share
            ,portfolios
            ,$scope.quarterRange
            ,response.data.thisQtr
            ,response.data.thisFy
            ,response.data.prevQtr,function(companies,products,verticals,regions,bus){
                share.initSummaryGraph(companies,products,verticals,regions,bus);
            });

          $scope.loadingAchivements = false;

      });
}

function loadLeaderboardTable($scope,$http,share,$rootScope,obj,portfolios){

    $scope.portfoliosList = portfolios;
    $scope.getLeaderBoardByPortfolio = function(selectedPortfolio){
        mixpanelTracker("Insights>Achievements>portfolio dropdown");
        $scope.allTeamData = [];
        share.teamList = [];
        $scope.teamRank = [];

        obj.allPortfolios = [selectedPortfolio];
        $http.post("/reports/big/query",obj)
          .success(function (response) {

              var companies = []
                ,products = []
                ,verticals = []
                ,regions = []
                ,bus= [];

              _.each(share.team,function (tm,i) {

                  if(response.data && response.data.leaderboard){
                      if(response.data.leaderboard[tm.emailId]){
                          var obj2 = {
                              designation:tm.designation,
                              emailId:tm.emailId,
                              fullName:tm.fullName,
                              image:tm.image,
                              userId:tm.userId,
                              children:tm.children,
                              noPicFlag:true,
                              nameNoImg:tm.fullName.substr(0,2).toUpperCase()
                          };

                          processLeaderBoardDataForPortfolios(response.data.leaderboard[tm.emailId].processed,obj2,i,companies,products,verticals,regions,bus,$scope.quarterRange,share.primaryCurrency);

                          $scope.allTeamData.push(obj2);
                      }
                  }
              });

              share.teamList = $scope.allTeamData;

              var labelWithDates = enumerateDaysBetweenDates($scope.quarterRange.start,$scope.quarterRange.end);
              var label = _.map(labelWithDates,"month");
              var labelmonthYear = _.map(labelWithDates,"monthYear");
              var order = orderOfQuarterMonths(label);

              _.each($scope.allTeamData,function (tm,i) {

                  tm.pieChartId = "pie-"+i;
                  tm.pieChartId2 = "pie_2-"+i;
                  tm.pieChartId3 = "pie_3-"+i;
                  tm.lineChart = "line-chart-"+i;
                  tm.lineChart2 = "line-chart-achievement-"+i;
                  tm.lineChart2ToolTip = "team-custom-tooltip-"+i;
                  tm.lineChart3ToolTip = "conver-custom-tooltip-"+i;

                  setTimeOutCallback(100,function () {
                      drawLineChartLeaderBoardConversion($scope,share,tm.monthlyCreated,tm.monthlyClosed,"."+tm.lineChart,labelWithDates,label,order);
                  });

                  setTimeOutCallback(100,function () {
                      drawLineChartLeaderBoard($scope,share,tm.targetsArray,tm.monthlyOppWon,tm.commits,"."+tm.lineChart2,labelWithDates,label,labelmonthYear,order,'achievement');
                  });
              })

              $scope.teamRank = getRanking($scope.allTeamData);
          });
    }

    $scope.selectedPortfolio = $scope.portfoliosList[0];
    $scope.getLeaderBoardByPortfolio($scope.selectedPortfolio);
}

function achievementByPortfolios($scope
  ,share
  ,portfolios
  ,quarterRange
  ,thisQtr
  ,thisFy
  ,prevQtr
  ,callback){

    var products_thisQtr = [],verticals_thisQtr = [],regions_thisQtr = [],bus_thisQtr = [],companies_thisQtr=[];
    var products_fy = [],verticals_fy = [],regions_fy = [],bus_fy = [],companies_fy=[];
    var products_prevQtr = [],verticals_prevQtr = [],regions_prevQtr = [],bus_prevQtr = [],companies_prevQtr=[];
    var products_yoy = [],verticals_yoy = [],regions_yoy = [],bus_yoy = [],companies_yoy=[];

    _.each(share.teamMembers,function (tm,i) {
        var obj = {
            designation:tm.designation,
            emailId:tm.emailId,
            fullName:tm.fullName,
            image:tm.image,
            userId:tm.userId,
            children:tm.children,
            noPicFlag:true,
            nameNoImg:tm.fullName.substr(0,2).toUpperCase()
        };

        processLeaderBoardData(thisFy,obj,i,companies_fy,products_fy,verticals_fy,regions_fy,bus_fy,quarterRange,share.primaryCurrency);
        processLeaderBoardData(prevQtr,obj,i,companies_prevQtr,products_prevQtr,verticals_prevQtr,regions_prevQtr,bus_prevQtr,quarterRange,share.primaryCurrency);
        processLeaderBoardData(thisQtr,obj,i
          ,companies_thisQtr,products_thisQtr,verticals_thisQtr,regions_thisQtr,bus_thisQtr
          ,quarterRange,share.primaryCurrency,companies_yoy,products_yoy,verticals_yoy,regions_yoy,bus_yoy);
    });

    share.achievementsFyAndPrevQtr = {
        thisFy: {
            products: products_fy,
            verticals: verticals_fy,
            regions: regions_fy,
            bus:bus_fy
        },
        prevQtr: {
            products: products_prevQtr,
            verticals: verticals_prevQtr,
            regions: regions_prevQtr,
            bus:bus_prevQtr
        },
        yoy: {
            products: products_yoy,
            verticals: verticals_yoy,
            regions: regions_yoy,
            bus:bus_yoy
        }
    }

    drawAchievementGraph($scope,share
      ,portfolios,products_thisQtr,verticals_thisQtr,regions_thisQtr,bus_thisQtr
      ,quarterRange
      ,thisFy
      ,prevQtr);
    callback(companies_thisQtr,products_thisQtr,verticals_thisQtr,regions_thisQtr,bus_thisQtr);
}

function getRanking(team){
    var rankingProperty = "achievementNumber";
    team.sort(function (o1, o2) {
        return o2[rankingProperty] > o1[rankingProperty] ? 1 : o2[rankingProperty] < o1[rankingProperty] ? -1 : 0;
    });

    var appendAtLast = [];
    var filteredData = team.filter(function (tm) {
        if(tm.target != 0 || tm.won != 0){
            return tm
        } else {
            appendAtLast.push(tm)
        }
    });

    filteredData.push.apply(filteredData, appendAtLast)

    _.each(filteredData,function (tm,index) {
        tm.index = index+1
    });

    return filteredData;
}

function orderOfQuarterMonths(months){

    var order = [];
    _.each(months,function (mon,i) {
        order.push({
            month:mon,
            sort:i+1
        })
    })

    return order;

}

function processLeaderBoardDataForPortfolios(data,obj,i,companies,products,verticals,regions,bus,quarterRange,primaryCurrency){

    _.each(data,function (el) {

        obj.avgSalesCycle = "-";
        obj.interactionsPerDeal = "-";
        obj.averageDealSize = 0;
        obj.averageDealSize_sort = 0;
        obj.avgSalesCycle_sort = 0;

        if(quarterRange){

            obj.monthlyCreated = [];
            obj.monthlyClosed = [];

            if(el.monthlyCreated){
                obj.monthlyCreated = groupAndChainForCR(el.monthlyCreated,quarterRange)
            }

            if(el.monthlyClosed){
                obj.monthlyClosed = groupAndChainForCR(el.monthlyClosed,quarterRange)
            }

            obj.conversionRate = "0 %";
            obj.achievementPercentage = "0 %";

            if(obj.monthlyCreated.length>0 && obj.monthlyClosed.length>0){
                var crNumber = ((_.sumBy(obj.monthlyClosed,'value')/_.sumBy(obj.monthlyCreated,'value'))*100).r_formatNumber(2)
                obj.conversionRate = crNumber+" %"
            }

            if(!obj.monthlyCreated.length && !obj.monthlyClosed.length){
                obj.conversionRate = "0 %"
            }

            if(!obj.monthlyCreated.length && obj.monthlyClosed.length){
                obj.conversionRate = "100 %"
                obj.commitClass = "up";
            }
        }

        obj.monthlyOppWon = groupAndChainForAchievement (el.monthlyOppWon)
        obj.targetsArray = groupAndChainForAchievement (el.targets)

        if(el.companies){
            companies.push(el.companies);
        }

        if(el.products){
            products.push(el.products);
        }

        if(el.products){
            verticals.push(el.verticals);
        }

        if(el.regions){
            regions.push(el.regions);
        }

        if(el.bus){
            bus.push(el.bus);
        }

        obj.target = el.targetsCount;
        obj.won = el.wonAmount;
        obj.wonDeals = el.wonDeals;
        obj.lostAmount = el.lostAmount;
        obj.gap = obj.target - (obj.won + obj.lostAmount);
        obj.interactions = el.interactionsCountAll;
        obj.interactionsCountWon = el.interactionsCountWon;
        obj.wonAchievementPer = "";
        obj.commits = el.commits;
        obj.commitsFormatted = el.commits?getAmountInThousands(el.commits,2,primaryCurrency == "INR"):"No commits found";
        obj.commitClass = "grey";
        if(el.commits && el.wonAmount){
            var wonCommitNumber = ((el.wonAmount/el.commits)*100).toFixed(2);
            obj.wonAchievementPer = wonCommitNumber+"%";
            obj.commitClass = "down";
            if(parseFloat(wonCommitNumber)>=100){
                obj.commitClass = "up";
            }
        }

        obj.pipeline = el.totalOppAmount;

        obj.oneWayInteractionCount = el.oneWayInteractionCount

        obj.interactionsWithOppContacts = el.interactionsOppContactsForQtr;
        obj.oneWayInteractionsWithOppContacts = el.oneWayInteractionsOppContactsForQtr;

        obj.gapNum = obj.gap;
        obj.openValue = el.totalOppAmount-el.wonAmount;

        if(el.wonAmount && el.wonDeals){
            var num = (el.wonAmount/el.wonDeals).r_formatNumber(2)
            obj.averageDealSize = el.wonAmount/el.wonDeals>0?numberWithCommas(num):0;
            obj.averageDealSize_sort = parseFloat(num);
        } else {
            obj.averageDealSize = "0";
            obj.averageDealSize_sort = 0
        }

        if(el.daysToWinCloseDeal){
            obj.avgSalesCycle = (el.daysToWinCloseDeal/el.wonDeals).r_formatNumber(2);
            obj.avgSalesCycle_sort = el.daysToWinCloseDeal/el.wonDeals;
        } else {
            obj.avgSalesCycle_sort = 0;
            obj.avgSalesCycle = "-";
        }

        obj.daysToWinCloseDeal = el.daysToWinCloseDeal

        obj.interactionsOverall = el.allInteractionsCount;

        if(el.contactRelevance){
            obj.contactRelevance = el.contactRelevance;
        }

        if(el.interactionsCountWon && el.wonAmount){
            // el.wonAmount = el.wonAmount/1000
            obj.interactionsPerDeal = (el.interactionsCountWon/(el.wonAmount/1000)).r_formatNumber(2);
        } else {
            obj.interactionsPerDeal = 0;
        }

        if(obj.interactionsPerDeal === "NaN"){
            obj.interactionsPerDeal = 0;
        }

        if(obj.won && obj.target){
            obj.achievementNumber = parseFloat(((obj.won/obj.target)*100).r_formatNumber(2));
            obj.achievementPercentage = obj.achievementNumber+" %";
        } else if(obj.won && (!obj.target || obj.target == 0)){
            obj.achievementNumber = 100;
            obj.achievementPercentage = "100 %";
        } else if(!obj.won && obj.target){
            obj.achievementNumber = 0;
            obj.achievementPercentage = "0 %";
        } else {
            obj.achievementNumber = 0;
            obj.achievementPercentage = "0 %";
        }

        var interactionsNumbers = [],dealNumbers = [],dealNumbersWithoutTarget = [],interactionsAndDeals = [];
        interactionsNumbers.push(obj.interactions)
        interactionsNumbers.push(obj.interactionsOverall)
        interactionsNumbers.push(obj.interactionsCountWon)

        dealNumbers.push(obj.openValue)
        dealNumbers.push(obj.pipeline)
        dealNumbers.push(obj.target)
        dealNumbers.push(obj.won)

        dealNumbersWithoutTarget.push(obj.openValue)
        dealNumbersWithoutTarget.push(obj.pipeline)
        dealNumbersWithoutTarget.push(obj.won)

        var minDeals = Math.min.apply( null, dealNumbers );
        var maxDeals = Math.max.apply( null, dealNumbers );

        var minDeals_v = Math.min.apply( null, dealNumbersWithoutTarget );
        var maxDeals_v = Math.max.apply( null, dealNumbersWithoutTarget );

        obj.wonPercentage = "0 %";
        if(obj.won && obj.pipeline){
            obj.wonPercentage = ((obj.won/obj.pipeline)*100).r_formatNumber(2)+" %"
        }

        if(!obj.target && !obj.won){
            obj.targetStyle = {'width':0+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
            obj.wonStyle = {'width':0+'%',background: '#8ECECB',height:'inherit',"max-width":"100%"}
        } else {
            obj.targetStyle = {'width':Math.abs(scaleBetween(obj.target,1,obj.won))+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
            obj.wonStyle = {'width':Math.abs(scaleBetween(obj.won,0,obj.target))+'%',background: '#8ECECB',height:'inherit',"max-width":"100%"}
        }

        if(el.userId == "cumulativeData"){
            //Cumulative Data doesn't include Liu's data.
            obj.cumulativeData = el;
            obj.ifExceptionalAccess = true;
            var userWon = obj.won;
            var eaWon = obj.cumulativeData.wonAmount?obj.cumulativeData.wonAmount:0;
            var total = userWon+eaWon;
            var target = obj.target;

            obj.eaWon = eaWon;
            obj.eaWithCommas = numberWithCommas(eaWon.r_formatNumber(2))

            obj.targetStyle = {'width':Math.abs(scaleBetween(obj.target,1,total))+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
            obj.userWonStyle = {'width':Math.abs(scaleBetween(userWon,1,target))+'%',height:'inherit',"max-width":"100%"}
            obj.eaWonStyle = {'width':Math.abs(scaleBetween(eaWon,1,target))+'%',height:'inherit',"max-width":"100%"}

            if(total && target){
                obj.achievementNumber = parseFloat(((obj.won/obj.target)*100).r_formatNumber(2));
                obj.achievementPercentage = obj.achievementNumber+" %";
            } else if(total && !obj.target){
                obj.achievementNumber = 100;
                obj.achievementPercentage = ((total/1)*100).r_formatNumber(2)+" %";

            } else if(!total && target){
                obj.achievementNumber = 0;
                obj.achievementPercentage = "0 %";
            } else {
                obj.achievementNumber = 0;
                obj.achievementPercentage = "0 %";
            }

            if(!total && !target){
                obj.targetStyle = {'width':0+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
                obj.userWonStyle = {'width':'0%',height:'inherit',"max-width":"100%"}
                obj.eaWonStyle = {'width':'0%',height:'inherit',"max-width":"100%"}

            }

        } else {
            obj.ifExceptionalAccess = false;
        }

        obj.wonClass = "grey";
        if(obj.achievementNumber && obj.achievementNumber>100){
            obj.wonClass = "up";
        } else if(obj.achievementNumber && obj.achievementNumber<100){
            obj.wonClass = "down";
        }

        if(obj.interactionsPerDeal || obj.interactionsOverall){
            obj.interactionsPerDealStyle = {'width':scaleBetween(parseFloat(obj.interactionsPerDeal),1,obj.interactionsOverall)+'%',background: '#F4D03F',height:'inherit'}
            obj.interactionsOverallStyle = {'width':100+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(!obj.interactionsOverall){
            obj.interactionsOverallStyle = {'width':0+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(obj.interactionsWithOppContacts && obj.oneWayInteractionCount){
            obj.oneWayInteractionStyle = {'width':100+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(!obj.interactionsWithOppContacts && obj.oneWayInteractionCount){
            obj.oneWayInteractionStyle = {'width':100+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(obj.interactionsWithOppContacts && obj.interactionsOverall){
            obj.interactionsWithOppContactsStyle = {'width':scaleBetween(parseFloat(obj.interactionsWithOppContacts),1,obj.interactionsOverall)+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
        } else {
            obj.interactionsWithOppContactsStyle = {'width':0+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
        }

        if(obj.interactionsPerDeal && obj.interactionsOverall){
            obj.interactionsDealOverallRatio = (parseFloat(obj.interactionsPerDeal)/obj.interactionsOverall).r_formatNumber(2)+" %"
        } else {
            obj.interactionsDealOverallRatio = "0 %"
        }

        if(obj.interactionsWithOppContacts && obj.interactionsOverall){
            obj.interactionsProductivityRatio = ((parseFloat(obj.interactionsWithOppContacts)/obj.interactionsOverall)*100).r_formatNumber(2)+" %";
        } else {
            obj.interactionsProductivityRatio = "0 %"
        }

        if(obj.oneWayInteractionsWithOppContacts && obj.oneWayInteractionCount){
            obj.oneWayInteractionsProductivityRatio = ((parseFloat(obj.oneWayInteractionsWithOppContacts)/obj.oneWayInteractionCount)*100).r_formatNumber(2)+" %"
            obj.oneWayInteractionsWithOppContactsStyle = {'width':scaleBetween(parseFloat(obj.oneWayInteractionsWithOppContacts),1,obj.oneWayInteractionCount)+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
            obj.productivityMeasure_sort = parseFloat((parseFloat(obj.oneWayInteractionsWithOppContacts)/obj.oneWayInteractionCount))*100
        } else {
            obj.oneWayInteractionsProductivityRatio = "0 %"
            obj.oneWayInteractionsWithOppContactsStyle = {'width':0+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
            obj.productivityMeasure_sort = 0;
        }

        obj.targetStyle_v = {'height':scaleBetween(obj.target,minDeals,maxDeals)+'%',background: '#FE9E83',width:'inherit'}
        obj.wonStyle_v = {'height':scaleBetween(obj.won,minDeals_v,maxDeals_v)+'%',background: '#8ECECB',width:'inherit'}
        obj.pipelineStyle_v = {'height':scaleBetween(obj.pipeline,minDeals_v,maxDeals_v)+'%',background: '#767777',width:'inherit'}

        var randomNumber = Math.floor(Math.random() * 22) + 80;

        if(randomNumber<75){
            obj.bestCaseClass = "low"
        } else {
            obj.bestCaseClass = "high"
        }

        obj.targetAnalysisNumber = randomNumber+"%";
        if(randomNumber<=50 || i == 2){
            obj.targetLikelihood = "Low"
            obj.targetAnalysisStyle = {'width':scaleBetween(randomNumber,1,100)+'%',background: '#e74c3c',height:'inherit'}
        }

        if(randomNumber>50 && randomNumber<=80){
            obj.targetLikelihood = "Average"
            obj.targetAnalysisStyle = {'width':scaleBetween(randomNumber,1,100)+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(randomNumber>80){
            obj.targetLikelihood = "High"
            obj.targetAnalysisStyle = {'width':scaleBetween(randomNumber,1,100)+'%',background: '#2ecc71',height:'inherit'}
        }

        if(i == 6){
            obj.targetAnalysisNumber = 61+"%";
            obj.bestCaseClass = "low"
            obj.targetLikelihood = "Average"
            obj.targetAnalysisStyle = {'width':scaleBetween(60,1,100)+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(i == 3){
            obj.targetAnalysisNumber = 10+"%";
            obj.bestCaseClass = "low"
            obj.targetLikelihood = "Low"
            obj.targetAnalysisStyle = {'width':scaleBetween(10,1,100)+'%',background: '#e74c3c',height:'inherit'}
        }

        obj.nQtarget = Math.floor(Math.random() * 50) + 100;
        obj.nQPipeline = Math.floor(Math.random() * 25) + 100;

        obj.nextQtrGap = obj.nQtarget - obj.nQPipeline;
        obj.nextQtr = obj.nextQtrGap

        if(obj.gap>0){
            obj.gapClass = "high"
            obj.gap = "-"+obj.gap
        } else {
            obj.gapClass = "low"
            obj.gap = "+"+-(obj.gap)
        }

        if(obj.nextQtrGap>0){
            obj.nextQtrGapClass = "high"
            obj.nextQtrGap = "-"+obj.nextQtrGap
        } else {
            obj.nextQtrGapClass = "low"
            obj.nextQtrGap = "+"+-(obj.nextQtrGap)
        }

        if(!obj.wonAchievementPer){
            obj.wonAchievementPer = "";
        }

        if(!obj.commitsFormatted){
            obj.commitsFormatted = 0;
        }

        if(!obj.commitClass){
            obj.commitClass = "grey";
        }

        obj.wonWithCommas = numberWithCommas(parseFloat(obj.won).r_formatNumber(2))
        obj.targetWithCommas = numberWithCommas(parseFloat(obj.target).r_formatNumber(2))
        obj.openValueWithCommas = numberWithCommas(parseFloat(obj.openValue).r_formatNumber(2))
        obj.lostAmountWithCommas = numberWithCommas(parseFloat(obj.lostAmount).r_formatNumber(2))

    })
}

function processLeaderBoardData(data,obj,i
  ,companies,products,verticals,regions,bus
  ,quarterRange,primaryCurrency
  ,companies_yoy,products_yoy,verticals_yoy,regions_yoy,bus_yoy){

    _.each(data,function (el) {

        if(obj.userId == el.userId){
            obj.avgSalesCycle = "-";
            obj.interactionsPerDeal = 0;
            obj.averageDealSize = 0;
            obj.averageDealSize_sort = 0;
            obj.avgSalesCycle_sort = 0;

            if(quarterRange){

                obj.monthlyCreated = [];
                obj.monthlyClosed = [];

                if(el.monthlyCreated){
                    obj.monthlyCreated = groupAndChainForCR(el.monthlyCreated,quarterRange)
                }

                if(el.monthlyClosed){
                    obj.monthlyClosed = groupAndChainForCR(el.monthlyClosed,quarterRange)
                }

                obj.conversionRate = "0 %";
                obj.achievementPercentage = "0 %";

                if(obj.monthlyCreated.length>0 && obj.monthlyClosed.length>0){
                    var crNumber = ((_.sumBy(obj.monthlyClosed,'value')/_.sumBy(obj.monthlyCreated,'value'))*100).r_formatNumber(2)
                    obj.conversionRate = crNumber+" %"
                }

                if(!obj.monthlyCreated.length && !obj.monthlyClosed.length){
                    obj.conversionRate = "0 %"
                }

                if(!obj.monthlyCreated.length && obj.monthlyClosed.length){
                    obj.conversionRate = "100 %"
                    obj.commitClass = "up";
                }
            }

            obj.monthlyOppWon = groupAndChainForAchievement (el.monthlyOppWon)
            obj.targetsArray = groupAndChainForAchievement (el.targets);

            if(el.companies){
                companies.push(el.companies);
            }

            if(el.products){
                products.push(el.products);
            }

            if(el.products){
                verticals.push(el.verticals);
            }

            if(el.regions){
                regions.push(el.regions);
            }

            if(el.bus){
                bus.push(el.bus);
            }

            if(el.products_yoy && products_yoy){
                products_yoy.push(el.products_yoy);
            }

            if(el.products_yoy && products_yoy){
                verticals_yoy.push(el.verticals_yoy);
            }

            if(el.regions_yoy && regions_yoy){
                regions_yoy.push(el.regions_yoy);
            }

            if(el.bus_yoy && bus_yoy){
                bus_yoy.push(el.bus_yoy);
            }

            obj.target = el.targetsCount;
            obj.won = el.wonAmount;
            obj.wonDeals = el.wonDeals;
            obj.lostAmount = el.lostAmount;
            obj.gap = obj.target - (obj.won + obj.lostAmount);
            obj.interactions = el.interactionsCountAll;
            obj.interactionsCountWon = el.interactionsCountWon;
            obj.wonAchievementPer = "";
            obj.commits = el.commits;
            obj.commitsFormatted = el.commits?getAmountInThousands(el.commits,2,primaryCurrency == "INR"):"No commits found";
            obj.commitClass = "grey";
            if(el.commits && el.wonAmount){
                var wonCommitNumber = ((el.wonAmount/el.commits)*100).toFixed(2);
                obj.wonAchievementPer = wonCommitNumber+"%";
                obj.commitClass = "down";
                if(parseFloat(wonCommitNumber)>=100){
                    obj.commitClass = "up";
                }
            }

            obj.pipeline = el.totalOppAmount;
            obj.oneWayInteractionCount = el.oneWayInteractionCount

            obj.interactionsWithOppContacts = el.interactionsOppContactsForQtr;
            obj.oneWayInteractionsWithOppContacts = el.oneWayInteractionsOppContactsForQtr;

            obj.gapNum = obj.gap;
            obj.openValue = el.totalOppAmount-el.wonAmount;

            if(el.wonAmount && el.wonDeals){
                var num = (el.wonAmount/el.wonDeals).r_formatNumber(2)
                obj.averageDealSize = el.wonAmount/el.wonDeals>0?numberWithCommas(num):0;
                obj.averageDealSize_sort = parseFloat(num);
            } else {
                obj.averageDealSize = "0";
                obj.averageDealSize_sort = 0
            }

            if(el.daysToWinCloseDeal){
                obj.avgSalesCycle = (el.daysToWinCloseDeal/el.wonDeals).r_formatNumber(2);
                obj.avgSalesCycle_sort = el.daysToWinCloseDeal/el.wonDeals;
            } else {
                obj.avgSalesCycle_sort = 0;
                obj.avgSalesCycle = "-";
            }

            obj.daysToWinCloseDeal = el.daysToWinCloseDeal

            obj.interactionsOverall = el.allInteractionsCount;

            if(el.contactRelevance){
                obj.contactRelevance = el.contactRelevance;
            }

            if(el.interactionsCountWon && el.wonAmount){
                // el.wonAmount = el.wonAmount/1000
                obj.interactionsPerDeal = (el.interactionsCountWon/(el.wonAmount/1000)).r_formatNumber(2);
            } else {
                obj.interactionsPerDeal = 0;
            }

            if(obj.interactionsPerDeal === "NaN"){
                obj.interactionsPerDeal = 0;
            }
        }

        if(obj.won && obj.target){
            obj.achievementNumber = parseFloat(((obj.won/obj.target)*100).r_formatNumber(2));
            obj.achievementPercentage = obj.achievementNumber+" %";
        } else if(obj.won && (!obj.target || obj.target == 0)){
            obj.achievementNumber = 100;
            obj.achievementPercentage = "100 %";
        } else if(!obj.won && obj.target){
            obj.achievementNumber = 0;
            obj.achievementPercentage = "0 %";
        } else {
            obj.achievementNumber = 0;
            obj.achievementPercentage = "0 %";
        }

        var interactionsNumbers = [],dealNumbers = [],dealNumbersWithoutTarget = [],interactionsAndDeals = [];
        interactionsNumbers.push(obj.interactions)
        interactionsNumbers.push(obj.interactionsOverall)
        interactionsNumbers.push(obj.interactionsCountWon)

        dealNumbers.push(obj.openValue)
        dealNumbers.push(obj.pipeline)
        dealNumbers.push(obj.target)
        dealNumbers.push(obj.won)

        dealNumbersWithoutTarget.push(obj.openValue)
        dealNumbersWithoutTarget.push(obj.pipeline)
        dealNumbersWithoutTarget.push(obj.won)

        var minDeals = Math.min.apply( null, dealNumbers );
        var maxDeals = Math.max.apply( null, dealNumbers );

        var minDeals_v = Math.min.apply( null, dealNumbersWithoutTarget );
        var maxDeals_v = Math.max.apply( null, dealNumbersWithoutTarget );

        obj.wonPercentage = "0 %";
        obj.wonPercentageNumber = ((obj.won/obj.pipeline)*100);
        if(obj.won && obj.pipeline){
            obj.wonPercentage = ((obj.won/obj.pipeline)*100).r_formatNumber(2)+" %"
        }

        if(!obj.target && !obj.won){
            obj.targetStyle = {'width':0+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
            obj.wonStyle = {'width':0+'%',background: '#8ECECB',height:'inherit',"max-width":"100%"}
        } else {
            obj.targetStyle = {'width':Math.abs(scaleBetween(obj.target,1,obj.won))+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
            obj.wonStyle = {'width':Math.abs(scaleBetween(obj.won,0,obj.target))+'%',background: '#8ECECB',height:'inherit',"max-width":"100%"}
        }

        if(el.userId == "cumulativeData"){
            //Cumulative Data doesn't include Liu's data.
            obj.cumulativeData = el;
            obj.ifExceptionalAccess = true;
            var userWon = obj.won;
            var eaWon = obj.cumulativeData.wonAmount?obj.cumulativeData.wonAmount:0;
            var total = userWon+eaWon;
            var target = obj.target;

            obj.eaWon = eaWon;
            obj.eaWithCommas = numberWithCommas(eaWon.r_formatNumber(2))

            obj.targetStyle = {'width':Math.abs(scaleBetween(obj.target,1,total))+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
            obj.userWonStyle = {'width':Math.abs(scaleBetween(userWon,1,target))+'%',height:'inherit',"max-width":"100%"}
            obj.eaWonStyle = {'width':Math.abs(scaleBetween(eaWon,1,target))+'%',height:'inherit',"max-width":"100%"}

            if(total && target){
                obj.achievementNumber = parseFloat(((obj.won/obj.target)*100).r_formatNumber(2));
                obj.achievementPercentage = obj.achievementNumber+" %";
            } else if(total && !obj.target){
                obj.achievementNumber = 100;
                obj.achievementPercentage = ((total/1)*100).r_formatNumber(2)+" %";

            } else if(!total && target){
                obj.achievementNumber = 0;
                obj.achievementPercentage = "0 %";
            } else {
                obj.achievementNumber = 0;
                obj.achievementPercentage = "0 %";
            }

            if(!total && !target){
                obj.targetStyle = {'width':0+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
                obj.userWonStyle = {'width':'0%',height:'inherit',"max-width":"100%"}
                obj.eaWonStyle = {'width':'0%',height:'inherit',"max-width":"100%"}

            }

        } else {
            obj.ifExceptionalAccess = false;
        }

        obj.wonClass = "grey";
        if(obj.achievementNumber && obj.achievementNumber>100){
            obj.wonClass = "up";
        } else if(obj.achievementNumber && obj.achievementNumber<100){
            obj.wonClass = "down";
        }

        if(obj.interactionsPerDeal || obj.interactionsOverall){
            obj.interactionsPerDealStyle = {'width':scaleBetween(parseFloat(obj.interactionsPerDeal),1,obj.interactionsOverall)+'%',background: '#F4D03F',height:'inherit'}
            obj.interactionsOverallStyle = {'width':100+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(!obj.interactionsOverall){
            obj.interactionsOverallStyle = {'width':0+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(obj.interactionsWithOppContacts && obj.oneWayInteractionCount){
            obj.oneWayInteractionStyle = {'width':100+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(!obj.interactionsWithOppContacts && obj.oneWayInteractionCount){
            obj.oneWayInteractionStyle = {'width':100+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(obj.interactionsWithOppContacts && obj.interactionsOverall){
            obj.interactionsWithOppContactsStyle = {'width':scaleBetween(parseFloat(obj.interactionsWithOppContacts),1,obj.interactionsOverall)+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
        } else {
            obj.interactionsWithOppContactsStyle = {'width':0+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
        }

        if(obj.interactionsPerDeal && obj.interactionsOverall){
            obj.interactionsDealOverallRatio = (parseFloat(obj.interactionsPerDeal)/obj.interactionsOverall).r_formatNumber(2)+" %"
        } else {
            obj.interactionsDealOverallRatio = "0 %"
        }

        if(obj.interactionsWithOppContacts && obj.interactionsOverall){
            obj.interactionsProductivityRatio = ((parseFloat(obj.interactionsWithOppContacts)/obj.interactionsOverall)*100).r_formatNumber(2)+" %";
        } else {
            obj.interactionsProductivityRatio = "0 %"
        }

        if(obj.oneWayInteractionsWithOppContacts && obj.oneWayInteractionCount){
            obj.oneWayInteractionsProductivityRatio = ((parseFloat(obj.oneWayInteractionsWithOppContacts)/obj.oneWayInteractionCount)*100).r_formatNumber(2)+" %"
            obj.oneWayInteractionsWithOppContactsStyle = {'width':scaleBetween(parseFloat(obj.oneWayInteractionsWithOppContacts),1,obj.oneWayInteractionCount)+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
            obj.productivityMeasure_sort = parseFloat((parseFloat(obj.oneWayInteractionsWithOppContacts)/obj.oneWayInteractionCount))*100
        } else {
            obj.oneWayInteractionsProductivityRatio = "0 %"
            obj.oneWayInteractionsWithOppContactsStyle = {'width':0+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
            obj.productivityMeasure_sort = 0;
        }

        obj.targetStyle_v = {'height':scaleBetween(obj.target,minDeals,maxDeals)+'%',background: '#FE9E83',width:'inherit'}
        obj.wonStyle_v = {'height':scaleBetween(obj.won,minDeals_v,maxDeals_v)+'%',background: '#8ECECB',width:'inherit'}
        obj.pipelineStyle_v = {'height':scaleBetween(obj.pipeline,minDeals_v,maxDeals_v)+'%',background: '#767777',width:'inherit'}

        var randomNumber = Math.floor(Math.random() * 22) + 80;

        if(randomNumber<75){
            obj.bestCaseClass = "low"
        } else {
            obj.bestCaseClass = "high"
        }

        obj.targetAnalysisNumber = randomNumber+"%";
        if(randomNumber<=50 || i == 2){
            obj.targetLikelihood = "Low"
            obj.targetAnalysisStyle = {'width':scaleBetween(randomNumber,1,100)+'%',background: '#e74c3c',height:'inherit'}
        }

        if(randomNumber>50 && randomNumber<=80){
            obj.targetLikelihood = "Average"
            obj.targetAnalysisStyle = {'width':scaleBetween(randomNumber,1,100)+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(randomNumber>80){
            obj.targetLikelihood = "High"
            obj.targetAnalysisStyle = {'width':scaleBetween(randomNumber,1,100)+'%',background: '#2ecc71',height:'inherit'}
        }

        if(i == 6){
            obj.targetAnalysisNumber = 61+"%";
            obj.bestCaseClass = "low"
            obj.targetLikelihood = "Average"
            obj.targetAnalysisStyle = {'width':scaleBetween(60,1,100)+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(i == 3){
            obj.targetAnalysisNumber = 10+"%";
            obj.bestCaseClass = "low"
            obj.targetLikelihood = "Low"
            obj.targetAnalysisStyle = {'width':scaleBetween(10,1,100)+'%',background: '#e74c3c',height:'inherit'}
        }

        obj.nQtarget = Math.floor(Math.random() * 50) + 100;
        obj.nQPipeline = Math.floor(Math.random() * 25) + 100;

        obj.nextQtrGap = obj.nQtarget - obj.nQPipeline;
        obj.nextQtr = obj.nextQtrGap

        if(obj.gap>0){
            obj.gapClass = "high"
            obj.gap = "-"+obj.gap
        } else {
            obj.gapClass = "low"
            obj.gap = "+"+-(obj.gap)
        }

        if(obj.nextQtrGap>0){
            obj.nextQtrGapClass = "high"
            obj.nextQtrGap = "-"+obj.nextQtrGap
        } else {
            obj.nextQtrGapClass = "low"
            obj.nextQtrGap = "+"+-(obj.nextQtrGap)
        }

        if(!obj.wonAchievementPer){
            obj.wonAchievementPer = "";
        }

        if(!obj.commitsFormatted){
            obj.commitsFormatted = 0;
        }

        if(!obj.commitClass){
            obj.commitClass = "grey";
        }

        obj.wonWithCommas = numberWithCommas(parseFloat(obj.won).r_formatNumber(2))
        obj.targetWithCommas = numberWithCommas(parseFloat(obj.target).r_formatNumber(2))
        obj.openValueWithCommas = numberWithCommas(parseFloat(obj.openValue).r_formatNumber(2))
        obj.lostAmountWithCommas = numberWithCommas(parseFloat(obj.lostAmount).r_formatNumber(2))

    })
}

function groupAndChainForCR(data,quarterRange) {

    var cleanData = [];

    if(data.length>0){
        _.each(data,function (el) {
            if(new Date(el)>= new Date(quarterRange.start) && new Date(el) <= new Date(quarterRange.end)){
                cleanData.push(el)
            }
        })
    }

    var group = _
      .chain(cleanData)
      .groupBy(function (el) {
          return moment(el).format("MMM");
      })
      .map(function(values, key) {
          return {
              meta:key,
              value:values.length
          }
      })
      .value();

    return group;

}

function groupAndChainForAchievement(data) {

    var group = _
      .chain(data)
      .groupBy(function (el) {
          return moment(el.date).format("MMM");
      })
      .map(function(values, key) {

          var amount = _.sumBy(values, 'amount');
          return {
              meta:key,
              value:amount,
              sortDate:values && values[0]?moment(values[0].date).clone().startOf('month'):null
          }
      })
      .value();

    return group;

}

function loadKPIs(ControllerChecker,$scope,$http,share,$rootScope){
    $scope.kpi = [{
        kpiFor:"North",
        trainingTarget:"2",
        trainingAchieved:"2",
        demoTarget:"15",
        demoAchieved:"17",
        pocTarget:"15",
        pocAchieved:"12",
        logoTarget:"5",
        logoAchieved:"2",
        accTarget:"45",
        accAchieved:"60",
        marginTarget:"9%",
        marginAchieved:"7.5%",
    },{
        kpiFor:"South",
        trainingTarget:"3",
        trainingAchieved:"2",
        demoTarget:"17",
        demoAchieved:"15",
        pocTarget:"18",
        pocAchieved:"11",
        logoTarget:"3",
        logoAchieved:"1",
        accTarget:"25",
        accAchieved:"40",
        marginTarget:"7%",
        marginAchieved:"4.5%",
    }]
}

function dataByPortfolio($scope,$http,share,$rootScope,portfolio) {
    function checkTeamLoaded(){
        if(share.team){
            loadLeaderboard($scope,$http,share,$rootScope,portfolio);
        } else {
            setTimeOutCallback(1000,function () {
                checkTeamLoaded();
            });
        }
    }

    checkTeamLoaded();
}

function drawAchievementGraph($scope,share,portfolio,products,verticals,regions,bus,quarterRange){

    $scope.setHeight = "height:200px";
    $scope.graphData = [];
    var graphData = [];

    var wonData = {};
    var wonDataYoy = {};
    var wonDataPrevQtr = {};
    var wonDataYtd = {};
    if(share.selectedPortoflioType == "Regions"){
        _.each(regions,function(el){
            _.each(el,function(item){
                if(wonData[item.name]){
                    wonData[item.name] = item.amount+wonData[item.name]
                } else {
                    wonData[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.prevQtr.regions,function(el){
            _.each(el,function(item){
                if(wonDataPrevQtr[item.name]){
                    wonDataPrevQtr[item.name] = item.amount+wonDataPrevQtr[item.name]
                } else {
                    wonDataPrevQtr[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.thisFy.regions,function(el){
            _.each(el,function(item){
                if(wonDataYtd[item.name]){
                    wonDataYtd[item.name] = item.amount+wonDataYtd[item.name]
                } else {
                    wonDataYtd[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.yoy.regions,function(el){
            _.each(el,function(item){
                if(wonDataYoy[item.name]){
                    wonDataYoy[item.name] = item.amount+wonDataYoy[item.name]
                } else {
                    wonDataYoy[item.name] = item.amount
                }
            })
        });
    }

    if(share.selectedPortoflioType == "Products"){

        _.each(products,function(el){
            _.each(el,function(item){
                if(wonData[item.name]){
                    wonData[item.name] = item.amount+wonData[item.name]
                } else {
                    wonData[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.prevQtr.products,function(el){
            _.each(el,function(item){
                if(wonDataPrevQtr[item.name]){
                    wonDataPrevQtr[item.name] = item.amount+wonDataPrevQtr[item.name]
                } else {
                    wonDataPrevQtr[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.thisFy.products,function(el){
            _.each(el,function(item){
                if(wonDataYtd[item.name]){
                    wonDataYtd[item.name] = item.amount+wonDataYtd[item.name]
                } else {
                    wonDataYtd[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.yoy.products,function(el){
            _.each(el,function(item){
                if(wonDataYoy[item.name]){
                    wonDataYoy[item.name] = item.amount+wonDataYoy[item.name]
                } else {
                    wonDataYoy[item.name] = item.amount
                }
            })
        });
    }

    if(share.selectedPortoflioType == "Verticals"){
        _.each(verticals,function(el){
            _.each(el,function(item){
                if(wonData[item.name]){
                    wonData[item.name] = item.amount+wonData[item.name]
                } else {
                    wonData[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.prevQtr.verticals,function(el){
            _.each(el,function(item){
                if(wonDataPrevQtr[item.name]){
                    wonDataPrevQtr[item.name] = item.amount+wonDataPrevQtr[item.name]
                } else {
                    wonDataPrevQtr[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.thisFy.verticals,function(el){
            _.each(el,function(item){
                if(wonDataYtd[item.name]){
                    wonDataYtd[item.name] = item.amount+wonDataYtd[item.name]
                } else {
                    wonDataYtd[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.yoy.verticals,function(el){
            _.each(el,function(item){
                if(wonDataYoy[item.name]){
                    wonDataYoy[item.name] = item.amount+wonDataYoy[item.name]
                } else {
                    wonDataYoy[item.name] = item.amount
                }
            })
        });
    }

    if(share.selectedPortoflioType == "Business Units"){
        _.each(bus,function(el){
            _.each(el,function(item){
                if(wonData[item.name]){
                    wonData[item.name] = item.amount+wonData[item.name]
                } else {
                    wonData[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.prevQtr.bus,function(el){
            _.each(el,function(item){
                if(wonDataPrevQtr[item.name]){
                    wonDataPrevQtr[item.name] = item.amount+wonDataPrevQtr[item.name]
                } else {
                    wonDataPrevQtr[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.thisFy.bus,function(el){
            _.each(el,function(item){
                if(wonDataYtd[item.name]){
                    wonDataYtd[item.name] = item.amount+wonDataYtd[item.name]
                } else {
                    wonDataYtd[item.name] = item.amount
                }
            })
        });

        _.each(share.achievementsFyAndPrevQtr.yoy.bus,function(el){
            _.each(el,function(item){
                if(wonDataYoy[item.name]){
                    wonDataYoy[item.name] = item.amount+wonDataYoy[item.name]
                } else {
                    wonDataYoy[item.name] = item.amount
                }
            })
        });
    }

    _.each(portfolio,function(el){
        var targetTotal = 0;
        var yrTarget = 0;
        var qoqChange = 0;
        var yoyChange = 0;
        var wonPerQtr = 0;
        var wonPerYr = 0;

        if(share.ytdPortfolioTarget[el.name]){
            yrTarget = share.ytdPortfolioTarget[el.name]
        }

        _.each(el.users,function(user){
            _.each(user.targets[0].values,function(tr){
                if(new Date(tr.date) >= new Date(quarterRange.start) && new Date(tr.date) <= new Date(quarterRange.end)){
                    targetTotal = targetTotal+tr.amount;
                }
            })
        });

        if(!wonData[el.name]){
            wonData[el.name] = 0
        }

        if(!wonDataYtd[el.name]) {
            wonDataYtd[el.name] = 0
        }

        if(!wonDataPrevQtr[el.name]){
            wonDataPrevQtr[el.name] = 0
        }

        wonPerQtr = (wonData[el.name]/targetTotal)*100;
        wonPerYr = (wonDataYtd[el.name]/yrTarget)*100;

        if(wonPerQtr == Infinity){
            wonPerQtr = 100;
        }

        if(wonPerYr == Infinity){
            wonPerYr = 100;
        }

        if(isNaN(wonPerQtr)){
            wonPerQtr = 0;
        }

        if(isNaN(wonPerYr)){
            wonPerYr = 0;
        }

        if(wonData[el.name] && wonDataPrevQtr[el.name]){
            qoqChange=(wonData[el.name]-wonDataPrevQtr[el.name])/wonDataPrevQtr[el.name]*100;
        }

        if(wonData[el.name] && wonDataYoy[el.name]){
            yoyChange=(wonData[el.name]-wonDataYoy[el.name])/wonDataYoy[el.name]*100;
        }

        if(wonData[el.name] && !wonDataPrevQtr[el.name]){
            qoqChange=100;
        }

        if(wonData[el.name] && !wonDataYoy[el.name]){
            yoyChange=100;
        }

        if(!wonData[el.name] && wonDataYoy[el.name]){
            yoyChange=0;
        }

        if(!wonData[el.name] && !wonDataPrevQtr[el.name]){
            qoqChange=0;
        }

        if(isNaN(qoqChange)){
            qoqChange = 0;
        }

        if(isNaN(yoyChange)){
            yoyChange = 0;
        }

        var qoqChangeClass = "no-change";
        var yoyChangeClass = "no-change";

        if(qoqChange !=0){
            qoqChangeClass = qoqChange>0?"up":"down"
        }

        if(yoyChange !=0){
            yoyChangeClass = yoyChange>0?"up":"down"
        }

        if(isNaN(wonPerYr)){
            wonPerYr = 0;
        }

        graphData.push({
            "name": el.name,
            "target": targetTotal,
            "yrTarget": yrTarget,
            "wonPerQtr": wonPerQtr.toFixed(2)+"%",
            "qoqChange": qoqChange.toFixed(2)+"%",
            "yoyChange": yoyChange.toFixed(2)+"%",
            "wonPerYr": wonPerYr.toFixed(2)+"%",
            "won": wonData[el.name].toFixed(2) || 0,
            "wonYr": wonDataYtd[el.name].toFixed(2) || 0,
            "qoqChangeClass":qoqChangeClass,
            "yoyChangeClass":yoyChangeClass,
        });

        $scope.graphData.push({
            "name": el.name,
            "target": getAmountInThousands(targetTotal,2,share.primaryCurrency == "INR"),
            "yrTarget": getAmountInThousands(yrTarget,2,share.primaryCurrency == "INR"),
            "wonPerQtr": wonPerQtr.toFixed(2)+"%",
            "qoqChange": qoqChange.toFixed(2)+"%",
            "yoyChange": yoyChange.toFixed(2)+"%",
            "wonPerYr": wonPerYr.toFixed(2)+"%",
            "won": getAmountInThousands(wonData[el.name],2,share.primaryCurrency == "INR") || 0,
            "wonYr": getAmountInThousands(wonDataYtd[el.name],2,share.primaryCurrency == "INR") || 0,
            "qoqChangeClass":qoqChangeClass,
            "yoyChangeClass":yoyChangeClass,
        });
    });

    if(graphData.length>5){
        var height = 100+((25*graphData.length)*0.9);
        setHeight = "height:"+height+"px";
    }

    var chart = AmCharts.makeChart("chartdiv", {
        "theme": "light",
        "type": "serial",
        "dataProvider": graphData,
        "valueAxes": [{
            "unit": "",
            "position": "left",
            "title": "",
            minimum: 0,
            "labelFunction": function(valueText) {
                if(valueText){
                    return getAmountInThousands(valueText,2,share.primaryCurrency == "INR")
                } else {
                    return valueText;
                }
            }
        }],
        "rotate": true,
        "startDuration": 0,
        "graphs": [{
            "balloonText": "Target: <b>[[value]]</b>",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "title": "2004",
            "type": "column",
            "valueField": "target",
            "fillColors":"#83c1e7" //targets
        }, {
            "balloonText": "Won: <b>[[value]]</b>",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "title": "2005",
            "type": "column",
            "clustered": false,
            "columnWidth": 0.5,
            "valueField": "won",
            "fillColors":"#4dafa2" //won
        }],
        "plotAreaFillAlphas": 0.1,
        "categoryField": "name",
        "categoryAxis": {
            "gridPosition": "start"
        }
    });
}

function targetVsWonChart(selectedRange,target,won,primaryCurrency) {

    if(!selectedRange){
        selectedRange = ""
    }

    if(target){
        target = parseFloat(target.toFixed(2));
    }

    if(won){
        won = parseFloat(won.toFixed(2));
    }

    var chart = AmCharts.makeChart("targetVsWon", {
        "type": "serial",
        "theme": "none",
        "categoryField": "year",
        "rotate": true,
        "startDuration": 0,
        "categoryAxis": {
            "gridPosition": "start",
            "position": "left"
        },
        "trendLines": [],
        "graphs": [
            {
                "balloonText": "Target:[[value]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "title": "Target",
                "type": "column",
                "valueField": "target",
                "fillColors":"#83c1e7" //targets
            },
            {
                "balloonText": "Won:[[value]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-2",
                "lineAlpha": 0.2,
                "title": "Won",
                "type": "column",
                "valueField": "won",
                "fillColors":"#4dafa2" //won
            }
        ],
        "guides": [],
        "valueAxes": [
            {
                "id": "ValueAxis-1",
                "position": "bottom",
                "axisAlpha": 0,
                minimum: 0,
                "labelFunction": function(valueText, date, categoryAxis) {
                    if(valueText){
                        return getAmountInThousands(valueText,2,primaryCurrency == "INR")
                    } else {
                        return valueText;
                    }
                }
            }
        ],
        "allLabels": [],
        "balloon": {},
        "titles": [],
        "dataProvider": [
            {
                "year": selectedRange,
                "target": target,
                "won": won
            }
        ]
    });
}

function summaryChart(data,className,pattern,height,width){

    var columns = _.map(data,function (el) {
        return [el.name,el.amount]
    });

    if(!height && !width){
        height = 79;
        width = 79;
    }

    var thickness = 0.12*height;

    var chart = c3.generate({
        bindto: className,
        data: {
            columns: columns,
            type : 'donut'
        },
        // pie: {
        donut: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            },
            width:thickness
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: height,
            width:width
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                return {top: top, left: parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))}
            }
        }
    });

}

relatasApp.controller("account", function(ControllerChecker,$scope,$http,share,$rootScope){

    window.localStorage.clear();

    $scope.allAcc = {
        name:"opp"
    };

    $scope.sortType = 'openCountSort';
    $scope.sortReverse = false;

    share.getAccountInsights = function(emailId,qtrFilterSelected){
        $scope.loadingMetaData = true;
        $scope.accounts = [];
        accountsInsightVerbose($http,$scope,share,qtrFilterSelected,null,emailId)
    }

    share.filterAccounts = function(stage,oppRange,intRange,start,end){
        if(stage && stage.name){
            $scope.getAccsByFilters($scope.allAcc.name,stage.name,oppRange,intRange,start,end)
        }
    }

    $scope.getAccsByFilters = function(by,stage,oppRange,intRange,start,end){
        accountsInsightVerbose($http,$scope,share,share.accountqtrFilterSelected,by,null,stage,oppRange,intRange,start,end)
    }

    $scope.getAccsBy = function(by){
        mixpanelTracker("Insights>Accounts "+by);
        share.toggleFySelection(by);
        share.resetAccountFilters()
        accountsInsightVerbose($http,$scope,share,share.accountqtrFilterSelected,by)
    }

    $scope.goToAcc = function (acc) {
        window.location = "/accounts/all?accountName="+acc
    }

});

function accountsInsightVerbose($http,$scope,share,qtrFilterSelected,filter,userEmailId,stage,oppRange,intRange,start,end){

    $("#spiderChart3").empty();
    $scope.loadingMetaData = true;

    try {
        if(window.localStorage['relatasLocalDb']){
            share.responseAccApi = JSON.parse(window.localStorage['relatasLocalDb'])
        }
    } catch (e) {
        console.log("Local Storage Error")
        console.log(e)
    }

    $scope.accounts = [];

    if(filter && share.responseAccApi){

        if(filter === 'all') {
            draw(filterAccounts(share.responseAccApi,stage,oppRange,intRange,start,end,share.accAndOppFilterApplied))
        } else if(filter === 'ints'){

            share.responseAccApi.accountsInteractions.interactions.sort(function(b, a) {
                return a.interactionsCount - b.interactionsCount;
            });

            var topIntsAccs = share.responseAccApi.accountsInteractions.interactions.slice(0, 100);
            var corrOpps = [];
            var accName = _.map(topIntsAccs,"axis");

            corrOpps = share.responseAccApi.accountsInteractions.opportunities.filter(function (o2) {
                return _.includes(accName,o2.axis);
            });

            draw({
                accountsInteractions:{
                    opportunities:corrOpps,
                    interactions:topIntsAccs
                }
            })
        } else if(filter === 'opp'){
            if(share.responseAccApi.topAccByOppThisQtr){
                share.responseAccApi.topAccByOppThisQtr.opportunities.sort(function(b, a) {
                    return a.oppsCount - b.oppsCount;
                });

                var topOppsAccs = share.responseAccApi.topAccByOppThisQtr.opportunities.slice(0, 100);
                var accName = _.map(topOppsAccs,"axis");

                var corrInts = share.responseAccApi.accountsInteractions.interactions.filter(function (o2) {
                    return _.includes(accName,o2.axis);
                });

                draw(filterAccounts({
                    accountsInteractions:{
                        opportunities:topOppsAccs,
                        interactions:corrInts
                    }
                }, stage,oppRange,intRange,start,end,share.accAndOppFilterApplied),filter)

            } else {

                draw({
                    accountsInteractions:null
                })
            }
        }
    } else {

        var url = '/reports/dashboard/account/insights';

        var emailId = _.map(share.team,"emailId");
        if(userEmailId && userEmailId !== "all"){
            emailId = [userEmailId]
            if(share.teamChildren[userEmailId]){
                emailId = emailId.concat(share.teamChildren[userEmailId])
            }
        }

        url = fetchUrlWithParameter(url+"?emailId="+emailId)

        if(qtrFilterSelected){
            url = fetchUrlWithParameter(url+"&qStart="+moment(qtrFilterSelected.range.qStart).toISOString())
            url = fetchUrlWithParameter(url+"&qEnd="+moment(qtrFilterSelected.range.qEnd).toISOString())
        }

        $http.get(url)
          .success(function (response) {

              window.localStorage['relatasLocalDb'] = JSON.stringify(response);
              share.responseAccApi = response;
              filter = "opp"; //Default

              if(response && response.accountsInteractions){
                  if(response.accountsInteractions.interactions.length === 0 && response.accountsInteractions.opportunities.length === 0){
                      $scope.disableRadioButtons = true;
                  }
              }

              if(filter){
                  accountsInsightVerbose($http,$scope,share,qtrFilterSelected,filter);
              } else {
                  draw(response)
              }
          })
    }

    function draw(response,reverseSort) {

        $scope.loadingMetaData = false;
        $scope.noAccAndInts = false;

        if(response && response.accountsInteractions) {

            if(response && response.accountsInteractions){
                if(response.accountsInteractions.interactions.length === 0 && response.accountsInteractions.opportunities.length === 0){
                    $scope.noAccAndInts = true;
                }
            }

            spiderChartInit(share,235,true,$scope,function (RadarChart) {

                if(response.accountsInteractions.interactions.length>0 || response.accountsInteractions.opportunities.length>0){
                    spiderDataInit(RadarChart,[],share,300,300,"#spiderChart3"
                      ,response.accountsInteractions.opportunities
                      ,response.accountsInteractions.interactions,true,$scope,reverseSort,$http,true);
                }
            });
        } else {
            $scope.noAccAndInts = true;
        }
    }
}

function filterAccounts(response,stage,oppRange,intRange,start,end,accAndOppFilterApplied){
    var intsObj = {}
      ,modifiedResponse = {};

    modifiedResponse["accountsInteractions.interactions"] = [];
    modifiedResponse["accountsInteractions.opportunities"] = [];

    _.each(response.accountsInteractions.interactions,function (el) {
        intsObj[el.axis] = el.interactionsCount;
    });

    if(response && response.accountsInteractions && response.accountsInteractions.opportunities){

        response.accountsInteractions.opportunities
          .forEach(function (el) {
              if(intsObj && intsObj[el.axis]){
                  el.interactionsCount = intsObj[el.axis]
              } else {
                  el.interactionsCount = 0;
              }

              var oppAmt = 0,
                oppsCount = 0;

              if(el && el.opps){

                  if(stage && stage !== "All"){
                      el.opps = el.opps.filter(function (op) {
                          if(stage === op.stageName){
                              return op;
                          }
                      });
                  }

                  if(start && end && isValidDate(start) && isValidDate(end)){
                      el.opps = el.opps.filter(function (op) {
                          if(new Date(op.closeDate) >= new Date(start) && new Date(op.closeDate) <= new Date(end)){
                              return op;
                          }
                      });

                  } else {

                      if(start && isValidDate(start)){
                          el.opps = el.opps.filter(function (op) {
                              if(new Date(op.closeDate) >= new Date(start)){
                                  return op;
                              }
                          });
                      }

                      if(end && isValidDate(end)){
                          el.opps = el.opps.filter(function (op) {
                              if(new Date(op.closeDate) <= new Date(end)){
                                  return op;
                              }
                          });
                      }
                  }

                  el.opps.forEach(function (op) {
                      oppAmt = oppAmt +op.amount;
                      oppsCount++;
                  });

                  el.value = oppAmt;
                  el.oppsAmount = oppAmt;
                  el.oppsCount = oppsCount;
              }
          });
    }

    if(oppRange && oppRange.range && oppRange.value){
        oppRange.value = parseFloat(oppRange.value);
        response.accountsInteractions.opportunities = response.accountsInteractions.opportunities.filter(function (op) {
            if(oppRange.range == "Less than"){
                if(op.oppsAmount<oppRange.value) {
                    return op;
                }
            } else if(oppRange.range == "Greater than"){
                if(op.oppsAmount>oppRange.value) {
                    return op;
                }
            }
        });
    }

    if(intRange && intRange.range && intRange.value){
        intRange.value = parseFloat(intRange.value);
        response.accountsInteractions.opportunities = response.accountsInteractions.opportunities.filter(function (op) {

            if(intRange.range == "Less than"){
                if(op.interactionsCount<intRange.value) {
                    return op;
                }
            } else if(intRange.range == "Greater than"){
                if(op.interactionsCount>intRange.value) {
                    return op;
                }
            }
        });

        response.accountsInteractions.interactions = response.accountsInteractions.interactions.filter(function (op) {

            if(intRange.range == "Less than"){
                if(op.interactionsCount<intRange.value) {
                    return op;
                }
            } else if(intRange.range == "Greater than"){
                if(op.interactionsCount>intRange.value) {
                    return op;
                }
            }
        });
    };

    if(accAndOppFilterApplied && accAndOppFilterApplied != "All"){

        var accsToBeShown = [];

        if(response.accountsInteractions.opportunities && response.accountsInteractions.opportunities.length>0){
            _.each(response.accountsInteractions.opportunities,function (op) {
                if(op.value>0){
                    accsToBeShown.push(op.axis);
                }
            })
        }

        if(response.accountsInteractions.opportunities && response.accountsInteractions.opportunities.length>0){
            response.accountsInteractions.opportunities = response.accountsInteractions.opportunities.filter(function (op) {
                return _.includes(accsToBeShown, op.axis);
            })
        }

        if(response.accountsInteractions.interactions && response.accountsInteractions.interactions.length>0){
            response.accountsInteractions.interactions = response.accountsInteractions.interactions.filter(function (op) {
                return _.includes(accsToBeShown, op.axis);
            })
        }
    }

    return response;
}

function getOppForAccounts($scope,$http,accounts,intsObj,primaryCurrency,accAndOppFilterApplied){

    $scope.accounts = [];
    if(accounts && accounts.length>0){
        _.each(accounts,function (ac) {
            var openCount = 0,
              wonCount = 0,
              lostCount = 0;

            if(ac.opps && ac.opps.length>0){
                _.each(ac.opps,function (op) {
                    if(op.stageName == "Close Won"){
                        wonCount = wonCount+op.amount
                    } else if(op.stageName == "Close Lost"){
                        lostCount = lostCount+op.amount
                    } else {
                        openCount = openCount+op.amount
                    }
                })
            }

            ac.openCount = getAmountInThousands(openCount,2,primaryCurrency == "INR");
            ac.wonCount = getAmountInThousands(wonCount,2,primaryCurrency == "INR");
            ac.lostCount = getAmountInThousands(lostCount,2,primaryCurrency == "INR");

            ac.openCountSort = openCount;
            ac.wonCountSort = wonCount;
            ac.lostCountSort = lostCount;

            if(intsObj && intsObj[ac.axis] && intsObj[ac.axis].count){
                ac.interactionsCount = intsObj[ac.axis].count
                ac.lastInteractedDateFormatted = moment(intsObj[ac.axis].lastInteractedDate).format(standardDateFormat());
                ac.lastInteractedDate = intsObj[ac.axis].lastInteractedDate
            } else {
                ac.interactionsCount = 0;
                ac.lastInteractedDateFormatted = "-"
            }
        })
    }

    $scope.accounts = accounts;
}

relatasApp.controller("regions", function(ControllerChecker,$scope,$http,share,$rootScope) {

    closeAllDropDownsAndModals2($scope,".wrapper-loc");

    share.regionChart = function (all,location) {

        $scope.loadingMap = true;

        var emailIds = [share.selection.emailId]
        if(share.selection.fullName == "Show all team members"){
            emailIds = _.map(share.team, "emailId");
        }

        if($scope.viewFor == 'opps'){

            getRegionChart($http, $scope, share, emailIds)
        } else {

            var url = '/reports/contacts';
            url = fetchUrlWithParameter(url+"?forUserEmailId="+emailIds);
            if(location){
                url = fetchUrlWithParameter(url+"&location="+location);
            }

            $http.get(url)
              .success(function (response) {
                  drawRegionContactsChart(response,[0, 0],$scope);
              })
        }
    }

    $scope.goToOpp = function (op) {
        mixpanelTracker("Insights>regions>view opp ");
        $rootScope.regionTabView = true;
        share.getInteractionHistory(op);
    }

    $scope.loadNoLocData = function(){
        $scope.viewingLocation = "";
        $scope.tbRows = $scope.oppsWithNoLoc;
        $scope.tbRows.forEach(function (el) {
            if(el.personEmailId){
                el.account = fetchCompanyFromEmail(el.personEmailId);
            } else {
                el.account = fetchCompanyFromEmail(el.contactEmailId);
            }
        })
    }

    $scope.sortType = 'amount';
    $scope.sortReverse = false;
    $scope.viewFor = 'opps';

    $scope.getDataFor = function (item,contactType) {

        mixpanelTracker("Insights>regions>Data by "+item);

        $scope.regEndDate = moment().format(standardDateFormat());
        $scope.regStartDate = moment().subtract(90,"days").format(standardDateFormat());

        $scope.tbRows = [];

        var emailIds = [share.selection.emailId]
        if(share.selection.fullName == "Show all team members"){
            emailIds = _.map(share.team, "emailId");
        }

        $scope.loadingMap = true;

        if(item == 'opps'){
            getRegionChart($http, $scope, share, emailIds)
        } else {

            var url = '/reports/contacts';
            url = fetchUrlWithParameter(url+"?forUserEmailId="+emailIds);
            if(contactType){
                url = fetchUrlWithParameter(url+"&contactType="+contactType)
            }

            $http.get(url)
              .success(function (response) {
                  drawRegionContactsChart(response,[0, 0],$scope);
              })
        }
    }

    $scope.sortTable = function (item) {

        var sorType = "amount";

        if(item == "Opportunity Name"){
            sorType = "opportunityName"
        }

        if(item == "Company"){
            sorType = "account"
        }
        if(item == "Stage"){
            sorType = "relatasStage"
        }
        if(item == "Sales Person"){
            sorType = "userEmailId"
        }

        if(item == "Selling To"){
            sorType = "contactEmailId"
        }

        if(item == "Contact Name"){
            sorType = "personEmailId"
        }

        if(item == "Type"){
            sorType = "prospect_customer"
        }

        if(item == "Contact Owner"){
            sorType = "ownerEmailId"
        }

        if(item == "Last Interacted"){
            sorType = "lastInteractedDate"
        }

        $scope.sortReverse = !$scope.sortReverse;
        $scope.sortType = sorType;
    }

    $scope.regEndDate = moment().format(standardDateFormat());
    $scope.regStartDate = moment().subtract(90,"days").format(standardDateFormat());

    $scope.applyFilter = function(){
        mixpanelTracker("Insights>regions>apply filter");

        $scope.tbRows = [];
        if(share.selection && share.selection.emailId == "Show all team members"){
            getRegionChart($http, $scope, share, _.map(share.team, "emailId"),$scope.opp.stage,$scope.regStartDate,$scope.regEndDate)
        } else {
            getRegionChart($http, $scope, share, [share.selection.emailId],$scope.opp.stage,$scope.regStartDate,$scope.regEndDate)
        }
    }

    $scope.registerDatePickerId = function(id){
        $('#'+id).datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    $scope[id] = moment(dp).format(standardDateFormat());
                });
            }
        });
    }

    getOppStages();

    function getOppStages(){

        if(share.opportunityStages){
            $scope.opp = {};
            $scope.stagesSelection = ["Show All Opportunities"];
            $scope.stagesSelection = $scope.stagesSelection.concat(_.map(share.opportunityStages,"name"));
            $scope.opp.stage = $scope.stagesSelection[0];
        } else {
            setTimeOutCallback(500,function () {
                getOppStages()
            })
        }
    }

    $scope.contactTypes = ["All contacts","Contact","Lead","Prospect","Customer","Influencer","Decision Maker", "CXO"];

    $scope.contact = {
        type:"All contacts"
    }

    $scope.getContactsForType = function (type) {
        $scope.location = "";
        $scope.getDataFor("contacts",type == 'All contacts'?null:type)
    }

    $scope.goToContact = function (contact) {
        // window.location = '/contacts/all?contact='+contact.personEmailId+'&acc=true';

        window.open( '/contacts/all?contact='+contact.personEmailId+'&acc=true', "_newtab" );
    }

    $scope.displayContacts = function (loc) {

        $scope.loadingOpps = true;
        if($scope.contactsObj[loc]){
            // $scope.viewingLocation = $scope.contactsObj[loc][0].location.replace(/ /g, ',');
            $scope.viewingLocation = $scope.contactsObj[loc][0].location;
        }

        if($scope.contactsObj[loc]){
            var url = "/reports/contact/details";
            url = fetchUrlWithParameter(url+"?emailIds="+_.map($scope.contactsObj[loc],"personEmailId"));
            var all = share.selection.fullName == "Show all team members";
            if(all){
                url = fetchUrlWithParameter(url+"&forUserEmailId="+_.map(share.team, "emailId"))
            }

            // $scope.tbHeaders = ["Contact Name", "Designation", "Company", "Type", "Amount", "Contact Owner", "Last Interacted"];
            $scope.tbHeaders = ["Contact Name", "Designation", "Company", "Type", "Amount", "Contact Owner"];

            $http.get(url)
              .success(function (response) {
                  $scope.tbRows = $scope.contactsObj[loc];

                  if(response){
                      $scope.tbRows.forEach(function (el) {
                          var d = response[el.ownerEmailId+el.personEmailId];

                          if(!el.contactRelation || (el.contactRelation && !el.contactRelation.prospect_customer)){
                              el.contactRelation = {
                                  prospect_customer: "Contact"
                              }
                          }

                          if(!el.amount){
                              el.amount = 0;
                          }

                          el.stageColor = "";

                          if(el.stageName == "Close Won") {
                              el.stageColor = "won"
                          }

                          if(el.stageName == "Close Lost") {
                              el.stageColor = "lost"
                          }

                          el.account = fetchCompanyFromEmail(el.personEmailId);
                          if(d){
                              el.amount = d.amount?d.amount:0;
                              el.lastInteractedDateFormatted = d.lastInteractedDate?moment(d.lastInteractedDate).format(standardDateFormat()):"-";
                              if(d.lastInteractedDate){
                                  el.lastInteractedDate = new Date(d.lastInteractedDate);
                              }

                              if(d.lastInteractedDate && (new Date(d.lastInteractedDate)< new Date(moment().subtract(60,"days")))){
                                  el.highlightContact = "lost";
                              }
                          } else {
                              el.highlightContact = "lost";
                              el.lastInteractedDateFormatted = "-"
                          }

                          if(!el.lastInteractedDateFormatted){
                              el.highlightContact = "lost";
                          }

                          if(el.lastInteractedDateFormatted == "-"){
                              el.highlightContact = "lost";
                              el.lastInteractedDate = new Date(moment().subtract(99,"years"));
                          }

                      });
                  }

                  $scope.loadingOpps = false;
              })
        }
    }

    $scope.displayOpps = function (loc) {
        $scope.tbRows = [];
        if($scope.regionOpps[loc]){

            $scope.tbRows = $scope.regionOpps[loc].opps;
            if($scope.tbRows[0] && $scope.tbRows[0].geoLocation && $scope.tbRows[0].geoLocation.town){
                $scope.viewingLocation = $scope.tbRows[0].geoLocation.town;
                // $scope.viewingLocation = $scope.viewingLocation.replace(/ /g, ',');
            }
        }

        $scope.tbRows.forEach(function (op) {
            op.isNotOwner = false;
            op.isOppClosed = _.includes(op.relatasStage.toLowerCase(),"close");

            if(op.isOppClosed){
                if(share.liuData.corporateAdmin){
                    op.isNotOwner = false;
                } else {
                    op.isNotOwner = true;
                }
            }

            op.account = fetchCompanyFromEmail(op.contactEmailId);
        });

        $scope.loadingOpps = false;
    }
});

relatasApp.controller("dashboard", function(ControllerChecker,$scope,$http,share,$rootScope){

    $scope.togglePvHelp = function(){
        $scope.openPvHelp = !$scope.openPvHelp
    }

    $scope.toggleDRHelp = function(){
        $scope.openDRHelp = !$scope.openDRHelp
    }

    $scope.toggleAIHelp = function(){
        $scope.openAIHelp = !$scope.openAIHelp
    }

    $scope.toggleOIHelp = function(){
        $scope.openOIHelp = !$scope.openOIHelp
    }

    $scope.goToAccount = function(){
        share.openViewFor({
            name:"Accounts",
            selected:""
        })
    }

    share.setLoaders2 = function(){
        $scope.loadingMetaData = true;
    }

    share.getDashBoardInsights = function(emailId,qtrFilterSelected){
        $scope.loadingMetaData = true;
        checkLiuDataLoaded(emailId,qtrFilterSelected)
    }

    $scope.getAccsBy = function(by){
        mixpanelTracker("Insights Dashboard Acc "+by);

        accountsInsights($http,$scope,share,share.accountqtrFilterSelected,by)
    }

    $scope.getOppsBy = function(by){
        oppsInsights($http,$scope,share,share.accountqtrFilterSelected,by)
    }

    function checkLiuDataLoaded(emailId,qtrFilterSelected){

        $scope.allAcc = {
            name:"opp"
        };
        $scope.oppInt = {
            name: "toOpp"
        };

        if(share.liuData){

            var url = '/reports/dashboard/insights';

            if(emailId){
                url = fetchUrlWithParameter(url+"?emailId="+emailId)
            }

            if(qtrFilterSelected && qtrFilterSelected.range){
                url = fetchUrlWithParameter(url+"&qStart="+moment(qtrFilterSelected.range.qStart).toISOString())
                url = fetchUrlWithParameter(url+"&qEnd="+moment(qtrFilterSelected.range.qEnd).toISOString())
            }

            share.accountEmailId = emailId;
            share.accountqtrFilterSelected = qtrFilterSelected;

            metaInfo_pipelineFlow($scope,$rootScope,$http,share,url,qtrFilterSelected)

            setTimeOutCallback(500,function () {
                pipelineVelocity_accs_oppsInts($scope,$http,share,fetchUrlWithParameter(url+"&section=second"),qtrFilterSelected)
            })

            setTimeOutCallback(1000,function () {
                pipelineFunnel_cr_newCompanies_wonTypes_ci($scope,$http,share,url = fetchUrlWithParameter(url+"&section=third"),qtrFilterSelected)
            })

            setTimeOutCallback(1250,function () {
                accountsInsights($http,$scope,share,qtrFilterSelected,null,emailId);
            });

        } else {
            setTimeOutCallback(200,function () {
                checkLiuDataLoaded(emailId,qtrFilterSelected)
            })
        }
    }

    $scope.loadingMetaData = true;

    share.newOppsCreated = function (data) {

        function checkQuarterRangeLoaded(){
            if(share.quarterRange){

                $scope.newOppsAdded = _.sumBy(data,function (el) {
                    if(new Date(el.sortDate) >= new Date(share.quarterRange.qStart) && new Date(el.sortDate) <= new Date(share.quarterRange.qEnd)){
                        return el.count
                    }
                })
            } else {
                setTimeOutCallback(1000,function () {
                    checkQuarterRangeLoaded()
                })
            }
        }

        checkQuarterRangeLoaded()
    }

    $scope.filterOpps = function (item) {
        share.filterOpps(item,true)
        share.openViewFor({
            name:"Opportunity",
            selected:""
        },true)
    }

    $rootScope.allTeamMembers = true;
    share.setAllTeamMembers = function (settings) {
        $rootScope.allTeamMembers = settings
    }

    $scope.showTable = function (table) {

        if(share.selection && share.selection.emailId == "Show all team members"){
            alert("Pipeline velocity insights not available for team. Please select individual team members to view pipeline velocity");
        } else {

            if(table == "pipelineVelocity"){
                $scope.pipelineVelocityCss = "insight-selection"
                var userId = share.selection?share.selection.userId:share.liuData.userId;
                share.showPipelineVelocity(true,userId);
            }
        }
    }

    $scope.getDetails = function (colType) {

        if(colType.colType == "Deals At Risk"){

            if(share.selection && share.selection.emailId == "Show all team members"){
                alert("Deals at risk insights not available for team. Please select individual team members to view deals at risk");
            } else {
                var userId = share.selection?share.selection.userId:share.liuData.userId;
                share.forDealsAtRisk(userId);
            }
        }
    }

});

function accountsInsights($http,$scope,share,qtrFilterSelected,filter,userEmailId){

    $("#spiderChart2").empty();
    if(filter && share.responseAccApi){

        if(filter === 'all') {
            draw(share.responseAccApi)
        } else if(filter === 'ints'){

            share.responseAccApi.accountsInteractions.interactions.sort(function(b, a) {
                return a.interactionsCount - b.interactionsCount;
            });

            var topIntsAccs = share.responseAccApi.accountsInteractions.interactions.slice(0, 100);

            var corrOpps = share.responseAccApi.accountsInteractions.opportunities;
            var accName = _.map(topIntsAccs,"axis");

            if(topIntsAccs.length>0){
                corrOpps = share.responseAccApi.accountsInteractions.opportunities.filter(function (o2) {
                    return _.includes(accName,o2.axis);
                });
            }

            draw({
                accountsInteractions:{
                    opportunities:corrOpps,
                    interactions:topIntsAccs
                }
            })
        } else if(filter === 'opp'){
            if(share.responseAccApi.topAccByOppThisQtr){
                share.responseAccApi.topAccByOppThisQtr.opportunities.sort(function(b, a) {
                    return a.oppsCount - b.oppsCount;
                });

                var topOppsAccs = share.responseAccApi.topAccByOppThisQtr.opportunities.slice(0, 100);

                var corrInts = share.responseAccApi.accountsInteractions.interactions;
                if(topOppsAccs.length>0){
                    var accName = _.map(topOppsAccs,"axis");
                    corrInts = share.responseAccApi.accountsInteractions.interactions.filter(function (o2) {
                        return _.includes(accName,o2.axis);
                    });
                }

                draw({
                    accountsInteractions:{
                        opportunities:topOppsAccs,
                        interactions:corrInts
                    }
                },filter)
            } else {

                draw({
                    accountsInteractions:null
                })
            }
        }
    } else {

        var url = '/reports/dashboard/account/insights';

        var emailId = _.map(share.team,"emailId");
        if(userEmailId && userEmailId !== "all"){
            emailId = [userEmailId]
            if(share.teamChildren[userEmailId]){
                emailId = emailId.concat(share.teamChildren[userEmailId])
            }
        }

        url = fetchUrlWithParameter(url+"?emailId="+emailId)

        if(qtrFilterSelected){
            url = fetchUrlWithParameter(url+"&qStart="+moment(qtrFilterSelected.range.qStart).toISOString())
            url = fetchUrlWithParameter(url+"&qEnd="+moment(qtrFilterSelected.range.qEnd).toISOString())
        }

        $http.get(url)
          .success(function (response) {

              share.responseAccApi = response;
              filter = "opp"; //Default

              if(response && response.accountsInteractions){
                  if(response.accountsInteractions.interactions.length === 0 && response.accountsInteractions.opportunities.length === 0){
                      $scope.disableRadioButtons = true;
                  }
              }

              if(filter){
                  accountsInsights($http,$scope,share,qtrFilterSelected,filter);
              } else {
                  draw(response)
              }
          })
    }

    function draw(response,reverseSort) {

        $scope.noAccAndInts = false;

        if(response && response.accountsInteractions) {

            if(response && response.accountsInteractions){
                if(response.accountsInteractions.interactions.length === 0 && response.accountsInteractions.opportunities.length === 0){
                    $scope.noAccAndInts = true;
                }
            }

            if(reverseSort == "opp" && response.accountsInteractions.opportunities.length === 0){
                $scope.noAccAndInts = true;
            }

            spiderChartInit(share,35,true,$scope,function (RadarChart) {

                if(response.accountsInteractions.interactions.length>0 || response.accountsInteractions.opportunities.length>0){
                    spiderDataInit(RadarChart,[],share,200,200,"#spiderChart2"
                      ,response.accountsInteractions.opportunities
                      ,response.accountsInteractions.interactions,true,$scope,reverseSort);
                }
            });
        } else {
            $scope.noAccAndInts = true;
        }

    }
}

function oppsInsights($http,$scope,share,qtrFilterSelected,filter){

    $("#spiderChart1").empty();

    if(filter){

        if(filter === 'all') {
            draw(share.apiResponse)
        } else if(filter === 'opp' || filter === 'toOpp'){

            if(share.apiResponse){
                share.apiResponse.topOppByOppThisQtr.opportunities.sort(function(b, a) {
                    return a.value - b.value;
                });

                var topOppsAccs = share.apiResponse.topOppByOppThisQtr.opportunities.slice(0, 100);
                var accName = _.map(topOppsAccs,"axis");

                var corrInts = share.apiResponse.topOppByOppThisQtr.interactions.filter(function (o2) {
                    return _.includes(accName,o2.axis);
                });
            }

            draw({
                oppsInteractions:{
                    opportunities:topOppsAccs,
                    interactions:corrInts
                }
            },filter)
        }
    } else {
        draw(share.apiResponse)
    }

    function draw(response,filter) {

        if(response && response.oppsInteractions) {
            spiderChartInit(share,135,false,$scope,function (RadarChart) {

                if(!response.oppsInteractions.interactions){
                    response.oppsInteractions.interactions = []
                }
                if(!response.oppsInteractions.opportunities){
                    response.oppsInteractions.opportunities = []
                }

                if(response.oppsInteractions.interactions.length>0 || response.oppsInteractions.opportunities.length>0){
                    spiderDataInit(RadarChart,[],share,200,200,"#spiderChart1"
                      ,response.oppsInteractions.opportunities
                      ,response.oppsInteractions.interactions,false,$scope,filter);
                } else {
                    $scope.noOppAndInts = true;
                }
            });
        }
    }
}

relatasApp.controller("pipeline_velocity",function ($scope,ControllerChecker,$http,share,$rootScope) {

    $scope.closePipelineVelocity = function () {
        $scope.openPipelineVelocity = false;
    }

    $scope.takeAction = function (opp) {

        if(!$rootScope.noAccess){
            $scope.opp = opp;
            $scope.showModal = true;
        }
    }

    $scope.closeModal = function () {
        $scope.showModal = false;
    }

    $scope.sortType = 'closeDate';
    $scope.sortReverse = false;

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.registerDatePickerId = function(){

        $('#opportunityCloseDateSelector4').datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    updateOpportunity($scope,$http,$scope.opp,"closeDate",dp,false,function (result) {
                        if(result){
                            share.refreshDealsAtRisk();
                            $scope.opp.formattedCloseDate = moment(dp).format("DD MMM YYYY");
                        } else {

                        }
                    })
                });
            }
        });
    }

    share.showPipelineVelocity = function (value,userId) {
        $scope.openPipelineVelocity = value;
        getPipelineVelocity(userId)
    }

    $scope.goTo = function () {
        window.location = "/opportunities/all"
    }

    share.forPipelineVelocity = function (userId,accessControl) {
        getPipelineVelocity(userId,accessControl)
    }

    function getPipelineVelocity (userId,accessControl){

        var url = '/insights/pipeline/velocity'
        if(userId){
            url = url+"?userId="+userId;
        }

        if(accessControl){

            if(share.liuData && share.liuData.orgHead){
                url = fetchUrlWithParameter(url+"&accessControl="+true)
                url = fetchUrlWithParameter(url+"&companyId="+share.liuData.companyId)
            } else {
                url = fetchUrlWithParameter(url+"&accessControl="+true)
            }
        }

        $http.get(url)
          .success(function (response) {
              if(response && response.SuccessCode){

                  var opportunityStages = {};

                  if(share.opportunityStages){
                      _.each(share.opportunityStages,function (op) {
                          opportunityStages[op.name] = op.order;
                      })
                  }

                  $scope.expectedPipeline = response.Data.expectedPipeline
                  $scope.deals = response.Data.oppNextQ && response.Data.oppNextQ[0] && response.Data.oppNextQ[0].opportunities?response.Data.oppNextQ[0].opportunities:[];
                  $scope.currentQuarter = response.Data.currentQuarter;

                  var allValues = [];
                  var target = response.Data.currentTargets[0]? response.Data.currentTargets[0].target:0

                  var pipeline = 0,won=0;
                  _.each(response.Data.currentOopPipeline,function (op) {

                      if(op._id == "Close Won"){
                          won = won+op.sumOfAmount
                      }

                      if(op._id != "Close Won" && op._id != "Close Lost"){
                          pipeline = pipeline+op.sumOfAmount
                      }
                  });

                  $scope.staleOppsExist = false;
                  $scope.nextQuarterOppsExist = false;

                  var gap = target - won;

                  allValues.push(target)
                  allValues.push(pipeline)
                  allValues.push(won)
                  allValues.push(gap)

                  $scope.targetCount = numberWithCommas(target.r_formatNumber(2),share.primaryCurrency == "INR");
                  $scope.pipelineCount = numberWithCommas(pipeline.r_formatNumber(2),share.primaryCurrency == "INR");
                  $scope.wonCount = numberWithCommas(won.r_formatNumber(2),share.primaryCurrency == "INR");
                  $scope.gapCount = numberWithCommas(gap.r_formatNumber(2),share.primaryCurrency == "INR");

                  var max = _.max(allValues);
                  var min = _.min(allValues);

                  $scope.target = {'width':scaleBetween(target,min,max)+'%',background: '#FE9E83'}
                  $scope.pipeline = {'width':scaleBetween(pipeline,min,max)+'%',background: '#767777'}
                  $scope.won = {'width':scaleBetween(won,min,max)+'%',background: '#8ECECB'}
                  $scope.gap = {'width':scaleBetween(gap,min,max)+'%',background: '#e74c3c'}

                  if(won>target){
                      $scope.expectationsExceed = true;
                  }

                  if($scope.deals.length>0){

                      $scope.nextQuarterOppsExist = true;

                      _.each($scope.deals,function (deal) {
                          deal.amountWithCommas = numberWithCommas(parseFloat(deal.amount.r_formatNumber(2)),share.primaryCurrency == "INR")
                          deal.formattedCloseDate = moment(deal.closeDate).format("DD MMM YYYY")
                          deal.company = fetchCompanyFromEmail(deal.contactEmailId)
                          deal.isStale = false
                          deal.suggestion = "Suggest moving this opportunity closing next quarter to current quarter."
                      })
                  }

                  if(response.Data.staleOpps && response.Data.staleOpps.length>0){

                      $scope.staleOppsExist = true;

                      _.each(response.Data.staleOpps,function (deal) {
                          deal.amountWithCommas = numberWithCommas(parseFloat(deal.amount).r_formatNumber(2),share.primaryCurrency == "INR")
                          deal.formattedCloseDate = moment(deal.closeDate).format("DD MMM YYYY")
                          deal.company = fetchCompanyFromEmail(deal.contactEmailId)
                          deal.isStale = true
                          deal.suggestion = "This is a stale opportunity. \n Move this deal to current quarter to meet your target or close the opportunity."

                          $scope.deals.push(deal)
                      })
                  }

                  if(response.Data.currentTargets && response.Data.currentTargets[0] && response.Data.currentTargets[0].target || accessControl){

                      $scope.actionRequired = true;
                      if(response.Data.expectedPipeline>response.Data.currentTargets[0].target || accessControl){
                          $scope.actionRequired = true;
                      }
                  }

                  if(!target && !pipeline){
                      $scope.targetPipelineNone = true;
                  }

                  if(pipeline>target){
                      $scope.targetPipelineNone = false;
                  }

                  if(won>target){
                      $scope.actionRequired = false;
                  }

                  if(target) {

                      if(!won){
                          $scope.actionRequired = true;
                          if(pipeline>=target){
                              $scope.actionRequired = false;
                          }

                      } else {
                          gap = target-won;
                          var wonPercentage = (won/target)*100;
                          var targetPipelineGap = pipeline-won;

                          //targetPipelineGap is the remaining pipeline after achievement, which still can be won
                          //Gap is the minimum won amount required to meet quarter target.

                          if(targetPipelineGap>gap && wonPercentage>=100){
                              $scope.actionRequired = false;
                              $scope.expectationsExceed = true;
                          }

                          if(target>pipeline && gap>0){
                              $scope.actionRequired = true;
                          }
                      }
                  }

                  _.each($scope.deals,function (deal) {
                      deal.stageStyle2 = oppStageStyle(deal.stageName,opportunityStages[deal.stageName]-1,true);
                  })

              } else {
                  $scope.deals = []
                  $scope.target = {}
                  $scope.pipeline = {}
                  $scope.won = {}
                  $scope.gap = {}

                  $scope.targetCount = 0;
                  $scope.pipelineCount = 0;
                  $scope.wonCount = 0;
                  $scope.gapCount = 0;
              }
          });
    }

});

relatasApp.controller("deals_at_risk",function ($scope,ControllerChecker,$http,share,searchService,$rootScope) {

    function getDealsAtRisk(userId,accessControl) {

        var url = "/insights/deals/at/risk"
        if(userId){
            url = url+"?userIds="+userId;
        }

        if(accessControl && !$rootScope.orgHead){
            url = fetchUrlWithParameter(url+"&accessControl="+true)
        }

        $scope.showDealsAtRisk = true;
        $scope.loadingDealsAtRisk = true;

        $http.get(url)
          .success(function (response) {
              if(response && response.deals){
                  dealsAtRiskGraph($scope,share,response.deals,response.averageRisk)
              }
          });
    }

    share.forDealsAtRisk = function (userId,accessControl) {
        getDealsAtRisk(userId,accessControl)
    }

    share.setTeamMembers = function (usersDictionary,usersArray) {

        var ids = _.map(usersArray,"_id");
        var url = "/insights/deals/at/risk/team/meta";
        url = fetchUrlWithParameter(url,'userIds',ids)

        $http.get(url)
          .success(function (response) {
              var team_atRisk = 0;
              $scope.teamDealsAtRisk = response;
              if(response && response.length>0) {
                  _.each(response,function (el) {
                      team_atRisk = team_atRisk+el.count;
                  })
              }
              share.teamRiskData(team_atRisk)
          });
    };

    share.refreshDealsAtRisk = function () {
        getDealsAtRisk();
    }

    $scope.closeDealsAtRisk = function () {
        $scope.showDealsAtRisk = false;
    }

    $scope.goToContact = function(emailId){
        // window.location = '/contacts/all?contact='+emailId+'&acc=true';
        window.open( '/contacts/all?contact='+emailId+'&acc=true', "_newtab" );
    };

    $scope.sortType = 'closeDate';
    $scope.sortReverse = false;

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.oppUpdateStage = function (stage) {

        updateOpportunity($scope,$http,$scope.opp,"stageName",stage,false,function (result) {
            toastr.success("Stage updated successfully");
            $scope.isStagnant = "fa-check-circle-o";
            $scope.stagnantStatus = true;
            $scope.stagnantDaysAgo = "Stage was last updated today";
            share.refreshDealsAtRisk();
            share.refreshPipelineSnaphot();
        });
    }

    $scope.getSuggestions = function (deal) {

        if(!$rootScope.noAccess){

            $scope.openDMs(); // Default open DMs

            $scope.dmActiveClass = "active"
            $scope.iActiveClass = "inactive"
            $scope.stagActiveClass = "inactive"
            $scope.staleActiveClass = "inactive"
            $scope.ciActiveClass = "inactive"
            $scope.mActiveClass = "inactive"
            $scope.compIActiveClass = "inactive"
            $scope.ltActiveClass = "inactive"

            $scope.totalValue = $scope.totalPipeLineValue?numberWithCommas($scope.totalPipeLineValue.r_formatNumber(2),share.primaryCurrency == "INR"):"";

            var percentageAtRisk = calculatePercentage(deal.amount,$scope.totalPipeLineValue);

            if(percentageAtRisk == 0){
                $scope.percentageOfTotal = "< 1% of "
            } else {
                $scope.percentageOfTotal = percentageAtRisk+"% of "
            }

            if(!$scope.totalPipeLineValue || !deal.amount){
                $scope.percentageOfTotal = false;
            }

            $scope.dmExist = "fa-exclamation-circle";
            $scope.InfExist = "fa-exclamation-circle";
            $scope.isStaleOpp = "fa-exclamation-circle";
            $scope.isStagnant = "fa-exclamation-circle";
            $scope.metDmInfl = "fa-exclamation-circle";
            $scope.IntScr = "fa-exclamation-circle";
            $scope.ltWithOwner = "fa-exclamation-circle";
            $scope.contatIntr = "fa-exclamation-circle";

            if(!deal.ltWithOwner){
                $scope.ltWithOwner = "fa-check-circle-o"
                $scope.ltTrue = false;
            } else {
                $scope.ltTrue = true;
            }

            if(!deal.skewedTwoWayInteractions){
                $scope.contatIntr = "fa-check-circle-o"
            }

            if(deal.daysSinceStageUpdated<45){
                $scope.isStagnant = "fa-check-circle-o"
                $scope.stagnantStatus = true;
            } else {
                $scope.stagnantStatus = false;
            }

            $scope.stagnantDaysAgo = "Stage was last updated "+deal.daysSinceStageUpdated + " days back";

            if(deal.daysSinceStageUpdated == 0){
                $scope.stagnantDaysAgo = "Stage was last updated today";
            }

            $scope.metDecisionMaker_infuencer = false;
            if(deal.metDecisionMaker_infuencer){
                $scope.metDecisionMaker_infuencer = true;
                $scope.metDmInfl = "fa-check-circle-o"
            }

            $scope.companyIntr = false;

            if(deal.averageInteractionsPerDeal){
                $scope.companyIntr = true;
                $scope.IntScr = "fa-check-circle-o"
            }

            if(share.usersDictionary[deal.userEmailId]){
                deal.owner = share.usersDictionary[deal.userEmailId]
            }

            $scope.opportunityName = deal.opportunityName;
            $scope.contactEmailId = deal.contactEmailId;

            $scope.closeDate = moment(deal.closeDate).format("DD MMM YYYY");
            $scope.oppCreatedDate = deal.createdDate?moment(deal.createdDate).format("DD MMM YYYY"):'';

            $scope.opp = deal;

            $scope.showModal = true;
            $scope.noDMs = !deal.decisionMakersExist;
            $scope.noInfl = !deal.influencersExist;

            if(deal.decisionMakersExist){
                $scope.dmExist = "fa-check-circle-o"
            }

            if(deal.influencersExist){
                $scope.InfExist = "fa-check-circle-o"
            }

            if(new Date(deal.closeDate)< new Date()){
                $scope.staleOpp = true;
            } else {
                $scope.staleOpp = false
                $scope.isStaleOpp = "fa-check-circle-o"
            }

            $scope.stageUpdated = deal.stageName;

            getLiu();
        }

    }

    $scope.openDMs = function () {
        $scope.showStagOpp = false;
        $scope.showDms = true;
        $scope.showContInt = false;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "active"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"

    }

    $scope.openInfl = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showInfl = true;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "active"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $scope.openStaleOpp = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showStaleOpp = true;

        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "active"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $scope.openDmOrInfMet = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showMetDmInf = true;

        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "active"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"

        if($scope.opp.dmsInfls[0]){
            getLiu($scope.opp.dmsInfls[0]);
        }
    }

    $scope.openIntScore = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showIntScr = true;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "active"
        $scope.ltActiveClass = "inactive"

    }

    $scope.openLt = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showIntScr = false;
        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.closeThis = false;
        $scope.showLt = true;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "active"

    }

    $scope.openStagOpp = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = true;
        $scope.showStaleOpp = false;
        $scope.closeThis = false;
        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "active"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    };

    $scope.openContactIntr = function () {
        $scope.showContInt = true;
        $scope.showStagOpp = false;
        $scope.showStaleOpp = false;
        $scope.closeThis = false;
        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "active"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $(document).on('keyup',function(evt) {
        if (evt.keyCode == 27) {
            $scope.$apply(function (){
                $scope.showModal = false;
                // resetSuggestions($scope)
            })
        }
    });

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.removeRecipient = function(contact,type){
        removeRecipient($scope,$http,contact,type)
    }

    $scope.closeModal = function () {
        $scope.showModal = false;
    }

    $scope.oppCloseLost = function () {
        updateOpportunity($scope,$http,$scope.opp,"closeDate",new Date(),true,function (result) {
            if(result){
                share.refreshClosingSoonDeals()
                share.refreshDealsAtRisk()
                share.refreshPipelineSnaphot()
                share.refreshTarget()
                $scope.closeModal();

                toastr.success("Opportunity closed.")
            } else {
                toastr.error("An error occurred while closing this opportunity. Please try later")
            }
        })
    }

    $scope.registerDatePickerId = function(){

        $('#opportunityCloseDateSelector2').datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    updateOpportunity($scope,$http,$scope.opp,"closeDate",dp,false,function (result) {
                        if(result){
                            share.refreshDealsAtRisk();
                            $scope.closeDate = moment(dp).format("DD MMM YYYY");
                            $scope.isStaleOpp = "fa-check-circle-o";
                            $scope.staleOpp = false;
                        } else {

                        }
                    })
                });
            }
        });
    }

    function getLiu(emailId) {

        if(share.liuData){

            var signature = "\n\n"+getSignature(share.liuData.firstName + ' '+ share.liuData.lastName,
              share.liuData.designation,
              share.liuData.companyName,
              share.liuData.publicProfileUrl)

            var interactionsFor = emailId?emailId:$scope.contactEmailId;

            getLastInteractedDetailsMessage($scope,$http,interactionsFor,share.liuData.firstName,share.liuData.publicProfileUrl,function (message) {

                $scope.subject = message.subject;
                $scope.body = message.body+signature;

                $scope.subjectLt = message.subject;
                $scope.subjectCi = message.subject;
                $scope.bodyLt = "\n\n"+signature;

            });

        } else {
            setTimeOutCallback(100,function () {
                getLiu()
            });
        }
    }

    $scope.sendEmail = function (subject,body,reason,contactEmailId) {

        var contactDetails = {
            contactEmailId: contactEmailId?contactEmailId:$scope.opp.contactEmailId,
            personId:null,
            personName:null
        }

        sendEmail($scope,$http,subject,body,contactDetails,reason)
    }

});

relatasApp.controller("acc_growth",function ($scope,ControllerChecker,$http,share) {
    $scope.loadingMetaData = true;
    share.forAccsCreated = function (newCompaniesInteracted) {
        $scope.loadingMetaData = true;
        getAccGrowth(newCompaniesInteracted)
    }

    function getAccGrowth(response){
        var label = [], series = [];

        if(response) {
            response.sort(function (o1, o2) {
                return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
            });

            _.each(response, function (el) {
                var month = monthNames[moment(new Date(el.sortDate)).month()];
                var monthYear = monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year();
                label.push(month.substring(0, 3));
                series.push({meta:monthYear,value:el.accountDetails.length});
            });

            $scope.noAcc = _.sumBy(series,"value") === 0;
            drawLineChart($scope, share, series, label, ".acc-growth")
        } else {
            $scope.noAcc = true;
        }

        $scope.loadingMetaData = false;
    }
});

relatasApp.controller("opp_growth",function ($scope,ControllerChecker,$http,share) {

    $scope.loadingMetaData = true;
    share.forOppGrowth = function (response) {

        $scope.loadingMetaData = true;
        getOppGrowth(response)
    }

    function getOppGrowth(response) {
        if (response) {

            $scope.noOpp = true;
            var label = [], seriesOpen = [], seriesClose = [];

            function comparer(otherArray){
                return function(current){
                    return otherArray.filter(function(other){
                        return other.monthYear == current.monthYear
                    }).length == 0;
                }
            }

            var onlyInA = response.created.filter(comparer(response.closed));
            var onlyInB = response.closed.filter(comparer(response.created));

            if(onlyInA.length>0){
                var onlyInAObj = [];
                _.each(onlyInA,function (el) {
                    onlyInAObj.push(el.monthYear);
                })
            }

            if(onlyInB.length>0){
                var onlyInBObj = [];
                _.each(onlyInB,function (el) {
                    onlyInBObj.push(el.monthYear);
                })
            }

            if(onlyInAObj && onlyInAObj.length>0){
                response.created = response.created.filter(function (el) {
                    return !_.includes(onlyInAObj,el.monthYear)
                })
            }

            if(onlyInBObj && onlyInBObj.length>0){
                response.closed = response.closed.filter(function (el) {
                    return !_.includes(onlyInBObj,el.monthYear)
                })
            }

            response.created.sort(function (o1, o2) {
                return new Date(o1.sortDate) > new Date(o2.sortDate) ? 1 : new Date(o1.sortDate) < new Date(o2.sortDate) ? -1 : 0;
            });

            response.closed.sort(function (o1, o2) {
                return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
            });

            _.each(response.created, function (el) {
                if(el.count>0){
                    $scope.noOpp = false;
                }
                label.push(monthNames[moment(new Date(el.sortDate)).month()].substring(0,3))
                seriesOpen.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
            });

            _.each(response.closed, function (el) {
                seriesClose.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
            });

            $scope.label2 = "Created";
            $scope.label1 = "Won";
            // $scope.loadingMetaData = false;
            drawLineChart($scope,share,seriesOpen,label,".opp-growth",seriesClose);
            share.newOppsCreated(response.Data)

        } else {
            $scope.noOpp = true;
        }

        $scope.loadingMetaData = false;
    }

});

relatasApp.controller("opportunitiesByStage",function ($scope,ControllerChecker,$http,$rootScope,share) {

    share.forSnapshot = function (response,fy) {
        $scope.loadingMetaData = true;
        getPipelineSnapshot(response,fy)
    }

    $scope.loadingMetaData = true;

    function getPipelineSnapshot(response,fy) {

        var oppExist = false;
        if(response && response.length>0){
            if(fy){
                $scope.fiscalYear = moment(fy.fromDate).format("MMM YY")+" - "+moment(fy.toDate).format("MMM YY")
            } else {
                $scope.fiscalYear = "Apr 19 - Mar 20"
            }

            oppExist = true;

            var maxCount = _.max(_.map(response,"count"))
            var minCount = _.min(_.map(response,"count"))
            var maxAmount = _.max(_.map(response,"totalAmount"))
            var minAmount = _.min(_.map(response,"totalAmount"))

            $scope.prospectColorLeft = "white";
            $scope.EvaluationColorLeft = "white";
            $scope.proposalColorLeft = "white";
            $scope.wonColorLeft = "white";
            $scope.lostColorLeft = "white";

            $scope.prospectColor = "white";
            $scope.EvaluationColor = "white";
            $scope.proposalColor = "white";
            $scope.wonColor = "white";
            $scope.lostColor = "white";

            $scope.funnels = [];
            var stagesWithData = [];

            function getPipelineFunnel(){

                if($rootScope.stages && $rootScope.stages.length>0){
                    _.each(response,function (el) {

                        _.each($rootScope.stages,function (st) {

                            if(el._id == st){
                                stagesWithData.push(st);
                                $scope.funnels.push({
                                    name:st,
                                    countLength:{'width':scaleBetween(el.count, minCount, maxCount)+'%'},
                                    amountLength:{'width':scaleBetween(el.totalAmount, minAmount, maxAmount)+'%'},
                                    amount:el.totalAmount.r_formatNumber(2),
                                    oppsCount:el.count
                                })
                            }
                        })
                    });

                    var noDataStages = _.difference($rootScope.stages,stagesWithData)

                    _.each(noDataStages,function (st) {
                        $scope.funnels.push({
                            name:st,
                            countLength:{'width':0+'%'},
                            amountLength:{'width':0+'%'},
                            amount:0,
                            oppsCount:0
                        })
                    })

                    _.each($scope.funnels,function (fl) {
                        _.each(share.opportunityStages,function (st) {
                            if(fl.name == st.name){
                                fl.order = st.order
                            }
                        })
                    });

                    $scope.funnels = _.sortBy($scope.funnels,function (o) {
                        return o.order
                    })


                } else {
                    setTimeOutCallback(1000,function () {
                        getPipelineFunnel()
                    });
                }
            }

            getPipelineFunnel()
        }

        $scope.loadingMetaData = false;
        $scope.noPipeline = oppExist;
    }

});

relatasApp.controller("opp_prop_viz",function ($scope,ControllerChecker,$http,$rootScope,share) {

    $scope.loadingMetaData = true;

    share.opp_props = function(oppTypes,sourceTypes,products) {
        $scope.loadingMetaData = true;

        $scope.noOppType = false;
        $scope.noSourceType = false;
        $scope.noProds = false;

        if(oppTypes.length == 0){
            $scope.noOppType = true;
        }

        if(sourceTypes.length == 0){
            $scope.noSourceType = true;
        }

        if(products.length == 0){
            $scope.noProds = true;
        }

        $(".typeGraph").empty()
        $(".sourceTypeGraph").empty()

        var oppType = groupAndChainForTeamSummary(oppTypes)
        donutChart(oppType,".typeGraph",shadeGenerator(63,81,181,oppType.length,15),185,185,185*.10);

        var sourceType = groupAndChainForTeamSummary(sourceTypes)
        donutChart(sourceType,".sourceTypeGraph",shadeGenerator(205,220,57,sourceType.length,15),185,185,185*.10);
        $scope.loadingMetaData = false;
        pieChart(_.flattenDeep(products),$scope);
    }
});

relatasApp.directive('excelExport', function () {
    return {
        restrict: 'A',
        scope: {
            fileName: "@",
            data: "&exportData"
        },
        replace: true,
        template: '<button class="btn btn-white margin0" ng-click="download()">Download <i class="fa fa-download"></i></button>',
        link: function (scope, element) {

            scope.download = function() {

                function datenum(v, date1904) {
                    if(date1904) v+=1462;
                    var epoch = Date.parse(v);
                    return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                };

                function getSheet(data, opts) {
                    var ws = {};
                    var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
                    for(var R = 0; R != data.length; ++R) {
                        for(var C = 0; C != data[R].length; ++C) {
                            if(range.s.r > R) range.s.r = R;
                            if(range.s.c > C) range.s.c = C;
                            if(range.e.r < R) range.e.r = R;
                            if(range.e.c < C) range.e.c = C;
                            var cell = {v: data[R][C] };
                            if(cell.v == null) continue;
                            var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

                            if(typeof cell.v === 'number') cell.t = 'n';
                            else if(typeof cell.v === 'boolean') cell.t = 'b';
                            else if(cell.v instanceof Date) {
                                cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                                cell.v = datenum(cell.v);
                            }
                            else cell.t = 's';

                            ws[cell_ref] = cell;
                        }
                    }
                    if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                    return ws;
                };

                function Workbook() {
                    if(!(this instanceof Workbook)) return new Workbook();
                    this.SheetNames = [];
                    this.Sheets = {};
                }

                var wb = new Workbook(), ws = getSheet(scope.data());
                /* add worksheet to workbook */
                wb.SheetNames.push(scope.fileName);
                wb.Sheets[scope.fileName] = ws;
                var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

                function s2ab(s) {
                    var buf = new ArrayBuffer(s.length);
                    var view = new Uint8Array(buf);
                    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                    return buf;
                }

                saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), scope.fileName+'.xlsx');

            };

        }
    };
});

function getTypeFormat(type){

    var typeFormat = "Account"
    if(type == "geoLocations"){
        typeFormat = "Region"
    }

    if(type == "businessUnits"){
        typeFormat = "BU"
    }
    if(type == "productList"){
        typeFormat = "Product"
    }

    if(type == "solutionList"){
        typeFormat = "Solution"
    }
    if(type == "sourceList"){
        typeFormat = "Source"
    }

    if(type == "typeList"){
        typeFormat = "Type"
    }

    if(type == "verticalList"){
        typeFormat = "Vertical"
    }
    if(type == "opportunityStages"){
        typeFormat = "Stage"
    }

    return typeFormat;
}

function currentInsights($scope,$http,share,response) {

    $scope.currentInsights = response.Data;

    if(response && response.Data){

        $scope.insightsExist = true;
        $scope.staleCount = response.Data.staleOpps

        $scope.todayDate = moment().format(standardDateFormat());

        for(var key in $scope.currentInsights){
            if(typeof $scope.currentInsights[key] == "number"){
                $scope.currentInsights[key+"Commas"] = getAmountInThousands($scope.currentInsights[key],2,share.primaryCurrency=="INR");
            }
        }

        $scope.currentInsights.avgDaysToCloseOppGraph = {width:'0%'};
        $scope.currentInsights.avgDealSizeGraph = {width:'0%'};
        $scope.currentInsights.avgInteractionsPerAmountWonGraph = {width:'0%'};

        if($scope.currentInsights.minDaysToCloseOpp && $scope.currentInsights.minDaysToCloseOpp == $scope.currentInsights.maxDaysToCloseOpp){
            $scope.currentInsights.avgDaysToCloseOppGraph = {width:100+'%'};
        } else if($scope.currentInsights.minDaysToCloseOpp && $scope.currentInsights.maxDaysToCloseOpp){
            $scope.currentInsights.avgDaysToCloseOppGraph = {width:scaleBetween($scope.currentInsights.avgDaysToCloseOpp,$scope.currentInsights.minDaysToCloseOpp,$scope.currentInsights.maxDaysToCloseOpp)+'%'};
        }

        if($scope.currentInsights.minDealSize && $scope.currentInsights.minDealSize == $scope.currentInsights.maxDealSize){
            $scope.currentInsights.avgDealSizeGraph = {width:scaleBetween($scope.currentInsights.avgDealSize,1,$scope.currentInsights.maxDealSize)+'%'};
        } else if($scope.currentInsights.minDealSize && $scope.currentInsights.maxDealSize){
            $scope.currentInsights.avgDealSizeGraph = {width:scaleBetween($scope.currentInsights.avgDealSize,$scope.currentInsights.minDealSize,$scope.currentInsights.maxDealSize)+'%'};
        }

        if($scope.currentInsights.maxDaysToCloseOpp && $scope.currentInsights.maxDaysToCloseOpp){
            $scope.currentInsights.avgInteractionsPerAmountWonGraph = {width:scaleBetween($scope.currentInsights.avgInteractionsPerAmountWon,$scope.currentInsights.minIntPerThousandAmountWon,$scope.currentInsights.maxIntPerThousandAmountWon)+'%'};
        }
    } else {
        $scope.currentInsights = {};
        $scope.insightsExist = false;
    }
    $scope.loadingMetaData = false;
}

function pipelineVelocity($scope,share,thisQuarterOpps,targets,thisQuarterOppsObj){
    $scope.targetGraph = targets
    var currentMonthYear = moment().format("MMM YYYY")

    _.each($scope.targetGraph,function (tr) {
        tr.monthYear = moment(tr.date).format("MMM YYYY")
        var thisMonthOpps = thisQuarterOppsObj[tr.monthYear];

        var won = 0,
          lost = 0,
          pipeline = 0,
          min = 0,
          max = 0;

        _.each(thisMonthOpps,function (op) {
            if(op.relatasStage == "Close Won"){
                won = won+op.convertedAmtWithNgm
            } else if(op.relatasStage == "Close Lost"){
                lost = lost+op.convertedAmtWithNgm
            } else {
                pipeline = pipeline+op.convertedAmtWithNgm
            }
        })

        max = won+lost+pipeline+tr.target;

        tr.won = won;
        tr.lost = lost;
        tr.pipeline = pipeline;

        if(tr.monthYear == currentMonthYear){
            tr.highLightCurrentMonth = true
        }

        tr.heightWon = {'height':scaleBetween(tr.won,min,max)+'%'}
        tr.heightLost = {'height':scaleBetween(tr.lost,min,max)+'%'}
        tr.heightTotal = {'height':scaleBetween(tr.pipeline,min,max)+'%'}
        tr.heightTarget = {'height':scaleBetween(tr.target,min,max)+'%'}

        tr.won = numberWithCommas(tr.won.r_formatNumber(2),share.primaryCurrency == "INR");
        tr.lost = numberWithCommas(tr.lost.r_formatNumber(2),share.primaryCurrency == "INR")
        tr.openValue = numberWithCommas(tr.pipeline.r_formatNumber(2),share.primaryCurrency == "INR")
        tr.target = numberWithCommas(tr.target?tr.target.r_formatNumber(2):0,share.primaryCurrency == "INR")

        tr.month = moment(tr.date).format("MMM")

    })

    $scope.targetGraph.sort(function (o1,o2) {
        return o1.date > o2.date ? 1 : o1.date < o2.date ? -1 : 0;
    })
}

function metaInfo_pipelineFlow($scope,$rootScope,$http,share,url,qtrFilterSelected) {

    $http.get(url)
      .success(function (response) {
          $scope.loadingMetaData = false;

          $(".accChart").empty();
          $(".donutGraphWon").empty();
          $(".donutGraphLost").empty();

          if(response && response.pipelineFlow && response.pipelineFlow.oppStages){
              $rootScope.stages = response.pipelineFlow.oppStages
          }

          drawSankeyGraph(response && response.pipelineFlow?response.pipelineFlow:null,share,$scope);

          var reasons = {
              won:groupAndChainForTeamSummary(response.reasonsWon),
              lost:groupAndChainForTeamSummary(response.reasonsLost)
          };

          $scope.noWon = reasons.won.length === 0;
          $scope.noLost = reasons.lost.length === 0;

          donutChart(reasons.won,".donutGraphWon",shadeGenerator(0,150,136,reasons.won.length,15),60,60)
          donutChart(reasons.lost,".donutGraphLost",shadeGenerator(244,67,54,reasons.lost.length,15),60,60);

          if(!response.dealsAtRisk){
              response.dealsAtRisk = {
                  "count": 0,
                  "amount": 0,
                  dealsRiskAsOfDate : new Date()
              }
          }

          if(response.dealsAtRisk){
              share.dealsAtRiskCount = response.dealsAtRisk.count
              share.totalDealValueAtRisk = response.dealsAtRisk.amount
              share.dealsRiskAsOfDate = response.dealsAtRisk.dealsRiskAsOfDate;
          }


          var target = 0;

          $scope.noOppAndInts = false;
          $scope.loadingMetaData = false;

          $scope.allTeamMembers = response.forTeam;
          $scope.targetGraph = [];

          if(response && response.pipelineVelocity && response.pipelineVelocity.length>0){
              $scope.targetGraph = response.pipelineVelocity
          }

          if($scope.targetGraph && $scope.targetGraph.length>0){
              $scope.targetGraph.forEach(function (tr) {
                  target = target+parseFloat(tr.target.replace (/,/g, ""))
              });
          }

          if(response){

              drawPipeline($scope,response.oppWon.amount,response.oppLost.amount,response.oppWon.count,response.oppLost.count
                ,0,0,0,0,response.dealsAtRisk.count,
                response.dealsAtRisk.amount,response.dealsAtRisk.dealsRiskAsOfDate,response.renewalOpen.amount,response.renewalOpen.count,response.stale.amount,response.stale.count,share,target)

          } else {
              drawPipeline($scope,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,share,target)
          }

      });

}

function pipelineVelocity_accs_oppsInts($scope,$http,share,url,qtrFilterSelected) {
    $http.get(url)
      .success(function (response) {

          share.apiResponse = response;

          if(response){
              treemapChart(response.accountsWon,$scope,share);
          } else {
              treemapChart([],$scope,share);
          }

          if(response && response.oppsInteractions){
              if(response.oppsInteractions.interactions.length === 0 && response.oppsInteractions.opportunities.length === 0){
                  $scope.noOppAndInts = true;
              }
          } else {
              $scope.noOppAndInts = true;
          }

          oppsInsights($http,$scope,share,qtrFilterSelected,$scope.oppInt.name);

      });

}

function pipelineFunnel_cr_newCompanies_wonTypes_ci($scope,$http,share,url,qtrFilterSelected) {

    $http.get(url)
      .success(function (response) {

          share.forAccsCreated(response?response.newCompaniesInteracted:null)
          share.forOppGrowth(response && response.conversionRate && response.conversionRate[0]?response.conversionRate[0]:null);

          share.forSnapshot(response && response.pipelineFunnel?response.pipelineFunnel:null);

          if(response){
              share.opp_props(response.typesWon,response.sourcesWon,response.productsWon)
          } else {
              share.opp_props([],[],[])
          }

          if(response.currentInsights && response.currentInsights[0]){
              share.getCurrentInsights({Data:response.currentInsights[0]})
          } else {
              share.getCurrentInsights({Data:null})
          }
      });

}

function drawPipeline($scope,wonAmt,lostAmt,wonCount,lostCount,pipelineAmt,pipelineCount,closing30DaysAmt,closing30DaysCount,dealsAtRiskCount,totalDealValueAtRisk,dealsRiskAsOfDate,renewalAmt,renewalCount,staleAmt,staleCount,share,targetAmt) {

    $scope.rangeType = share.rangeType?share.rangeType:"This Quarter"

    var achievementPercentage = 0,
      pipelinePercentage = 0;
    $scope.stageMetaInfo = [];

    if(targetAmt){
        achievementPercentage = parseFloat(((wonAmt/targetAmt)*100).r_formatNumber(2))
        achievementPercentage = achievementPercentage>100?100:achievementPercentage

        pipelinePercentage = parseFloat(((pipelineAmt/targetAmt)*100).r_formatNumber(2))
        pipelinePercentage = pipelinePercentage>100?100:pipelinePercentage
    } else {
        $scope.noTargetsSet = true;
    }

    $scope.pipelinePercentage = pipelinePercentage;
    $scope.achievementPercentage = achievementPercentage;

    share.currentInsightsData(pipelinePercentage,achievementPercentage)

    $scope.stageMetaInfo = [
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Won",
            colIcon:"fas fa-trophy",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(wonAmt,2,share.primaryCurrency =="INR"),
            oppCount:wonCount,
            textColor:"green",
            currency:share.primaryCurrency,
            asOfDate:true,
            percentage:achievementPercentage,
            style:{width:achievementPercentage+"%"}
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Lost",
            colIcon:"fas fa-trophy",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(lostAmt,2,share.primaryCurrency =="INR"),
            oppCount:lostCount,
            textColor:"red",
            currency:share.primaryCurrency,
            asOfDate:true
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Closing",
            colIcon:"fas fa-chart-bar",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(pipelineAmt,2,share.primaryCurrency =="INR"),
            oppCount:pipelineCount,
            textColor:"blue",
            currency:share.primaryCurrency,
            asOfDate:true,
            percentage:pipelinePercentage,
            style:{width:pipelinePercentage+"%"}
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Deals At Risk",
            colIcon:"fas fa-dollar-sign",
            rangeType:dealsRiskAsOfDate?"As of "+moment(dealsRiskAsOfDate).format(standardDateFormat()):null,
            amountValue:getAmountInThousands(totalDealValueAtRisk,2,share.primaryCurrency =="INR"),
            oppCount:dealsAtRiskCount,
            textColor:"red",
            currency:share.primaryCurrency,
            asOfDate:dealsRiskAsOfDate?moment(dealsRiskAsOfDate).format(standardDateFormat()):null,
            cursor:"cursor",
            allTeamMembers:share.selection && share.selection.fullName == "Show all team members"
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Renewal",
            colIcon:"fas fa-dollar-sign",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(renewalAmt,2,share.primaryCurrency =="INR"),
            oppCount:renewalCount,
            textColor:"yellow",
            currency:share.primaryCurrency,
            asOfDate:true
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Stale",
            colIcon:"fas fa-dollar-sign",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(staleAmt,2,share.primaryCurrency =="INR"),
            oppCount:staleCount,
            textColor:"red",
            currency:share.primaryCurrency,
            asOfDate:true
        }
    ];

}

function drawSankeyGraph(response,share,$scope){

    $("#oppFlow").empty();

    if(!response){
        $scope.noOppFlowData = true;
        $scope.startAmount = 0;
        $scope.endAmount = 0;
    } else {

        var formattedData = formatDataForSankeyGraph(response,share);

        share.quarterRange = {
            qStart:response.qStart,
            qEnd:response.qEnd
        }

        // $scope.oppFlowHeader = moment(response.qStart).format("MMM") +"- "+moment(moment(response.qStart).add(1,"month")).format("MMM") +"- "+moment(moment(response.qEnd).subtract(1,"d")).format("MMM YYYY");
        $scope.oppFlowHeader = "This Quarter";

        share.totalCommitAmt = 0;
        share.totalPipelineAmt = 0;
        share.totalNewAmt = 0;

        $scope.startAmount = getAmountInThousands(share.startAmount,2,share.primaryCurrency == "INR");
        $scope.endAmount = getAmountInThousands(share.endAmount,2,share.primaryCurrency == "INR");

        var newExists = false,
          commitStartExists = false,
          pipelineStartExists = false,
          commitExists = false,
          pipelineExists = false,
          closeWonExists = false,
          closeLostExists = false;

        _.each(formattedData,function (el) {

            if(el.source == "New"){
                newExists = true;
            }

            if(el.source == "Pipeline"){
                pipelineStartExists = true;
            }

            if(el.source == "Commit"){
                commitStartExists = true;
            }

            if(el.target == "Pipeline "){
                pipelineExists = true;
            }

            if(el.target == "Commit "){
                commitExists = true;
            }

            if(el.target == "Close Won"){
                closeWonExists = true;
            }

            if(el.target == "Close Lost"){
                closeLostExists = true;
            }

            if(el.source == "New"){
                share.totalNewAmt = share.totalNewAmt+el.sourceAmt
            } else if(el.source == "Commit"){
                share.totalCommitAmt = share.totalCommitAmt+el.sourceAmt
            } else {
                share.totalPipelineAmt = share.totalPipelineAmt+el.sourceAmt
            }
        });

        var commitStage = "Commit" //response.commitStage;
        var commitStageEnd = "Commit " //commitStage+" ";

        var colorScheme = {
            "Pipeline":"#cbcfd2",
            "Pipeline ":"#cbcfd2",
            "Close Won": "#8ECECB", //"#47b758",
            "Close Lost":"#e74c3c",
            "New":"#2499c9"
        }

        colorScheme[commitStage] = "#f1c40f";
        colorScheme[commitStageEnd] = "#f1c40f";

        var allNodes = [];

        if(closeLostExists){
            allNodes.push({"name": "Close Lost"})
        }

        if(closeWonExists){
            allNodes.push({"name": "Close Won"})
        }

        if(newExists){
            allNodes.push({"name": "New"})
        }

        if(pipelineStartExists){
            allNodes.push({"name": "Pipeline"})
        }

        if(commitStartExists){
            allNodes.push({"name": commitStage})
        }

        if(pipelineExists){
            allNodes.push({"name": "Pipeline "})
        }

        if(commitExists){
            allNodes.push({"name": commitStageEnd})
        }

        share.sankeyData = {
            "nodes":allNodes,
            "links": formattedData
        };

        var data = {
            "nodes":allNodes,
            "links": formattedData
        }

        $scope.noOppFlowData = false;
        if(formattedData && formattedData.length>0){
            sankeyGraphSettings($scope,share,data,colorScheme)
        } else {
            $scope.noOppFlowData = true;
        }

        $scope.loadingMetaData = false;
    }

}

relatasApp.directive('resize', function ($window) {
    return function (scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function () {
            return {
                'h': w.height(),
                'w': w.width()
            };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.windowHeight = newValue.h;
            scope.windowWidth = newValue.w;
        }, true);

        w.bind('resize', function () {
            scope.$apply();
        });
    }
})

function sankeyGraphSettings($scope,share,data,colorScheme) {

    var viewBox = "0 0 515 350";

    if($(window).width() && $(window).width()>1380){
        viewBox = "0 0 675 350";
    }

    if($(window).width() && $(window).width()>1900){
        viewBox = "0 0 855 350";
    }

    var nodeHash = {};
    data.nodes.forEach(function(d){
        nodeHash[d.name] = d;
    });
    // loop links and swap out string for object
    data.links.forEach(function(d){
        d.source = nodeHash[d.source];
        d.target = nodeHash[d.target];
    });

    var margin = {top: 1, right: 1, bottom: 6, left: 1},
      width = 515 - margin.left - margin.right,
      height = 295 - margin.top - margin.bottom;

    var formatNumber = d3.format(",.0f"),
      format = function(d) { return share.primaryCurrency +" "+formatNumber(d); },
      color = d3.scale.category20();

    var svg = d3.select("#oppFlow")
      .append("div")
      .classed("svg-container", true)
      .append("svg")
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", viewBox)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .classed("svg-content-responsive", true);

    var sankey = d3.sankey()
      .nodeWidth(5)
      .nodePadding(10)
      .size([width, height]);

    var path = sankey.link();

    sankey
      .nodes(data.nodes)
      .links(data.links)
      .layout(32);

    var tooltip = d3.select("body").append("div")
      .attr("class", "tooltip")
      .style("opacity", 0);

    var css = '.stop-left { stop-color: #3f51b5; } .stop-right {stop-color: #009688;} .filled {fill: url(#mainGradient);}',
      head = document.head || document.getElementsByTagName('head')[0],
      style = document.createElement('style');

    style.type = 'text/css';
    style.appendChild(document.createTextNode(css));
    head.appendChild(style);

    var link = svg.append("g").selectAll(".link")
      .data(data.links)
      .enter().append("path")
      .attr("class", "link")
      .attr("d", path)
      .style("stroke-width", function(d) { return Math.max(1, d.dy); })
      .style("stroke", function(d) {
      })
      .style("stroke-opacity", function(d) {
          if(d.target.name === "Close Won" || d.target.name === "Close Lost"){
              // return 0.8;
          }
      })
      .sort(function(a, b) { return b.dy - a.dy; })
      .on("mouseover", function(d) {
          var html = getToolTip(d,share)
          tooltip.transition()
            .duration(200)
            .style("opacity", .99);
          tooltip	.html(html)
            .style("left", (d3.event.pageX - 50) + "px")
            .style("top", (d3.event.pageY - 55) + "px");
      })
      .on("mouseout", function(d) {
          tooltip.transition()
            .duration(500)
            .style("opacity", 0);
      });

    var node = svg.append("g").selectAll(".node")
      .data(data.nodes)
      .enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
      .call(d3.behavior.drag()
        .origin(function(d) { return d; })
        .on("dragstart", function() { this.parentNode.appendChild(this); })
        .on("drag", dragmove));

    node.append("rect")
      .attr("height", function(d) { return d.dy; })
      .attr("width", sankey.nodeWidth())
      .style("fill", function(d) {
          return d.color = colorScheme[d.name];
      })
      .style("stroke", function(d) {
          return colorScheme[d.name];
      })
      .append("title")
      .text(function(d) {return d.name + "\n" + format(d.value); })
    // .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

    node.append("text")
      .attr("x", -6)
      .attr("y", function(d) { return d.dy / 2; })
      .attr("dy", ".35em")
      .attr("text-anchor", "end")
      .attr("transform", null)
      .text(function(d) { return d.name; }) //Text in the middle
      .filter(function(d) { return d.x < width / 2; })
      .attr("x", 6 + sankey.nodeWidth())
      .attr("text-anchor", "start");

    function dragmove(d) {
        d3.select(this).attr("transform", "translate(" + d.x + "," + (d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))) + ")");
        sankey.relayout();
        link.attr("d", path);
    }
}

function getToolTip(data,share) {

    var movementCount = data.numberOfOpps + " opps";
    var amountDiff = 0;
    var amountDiffPercentage = 0;

    if(data.sourceAmt && data.targetAmt){
        if(data.source.name == "New"){
            amountDiff = data.targetAmt;
            amountDiffPercentage = (amountDiff/share.totalNewAmt)*100;
            amountDiffPercentage = parseFloat(amountDiffPercentage.toFixed(2))+"%";
        } else if(data.source.name == "Pipeline"){
            amountDiff = data.targetAmt;
            amountDiffPercentage = (amountDiff/share.totalPipelineAmt)*100;
            amountDiffPercentage = parseFloat(amountDiffPercentage.toFixed(2))+"%";
        } else {
            amountDiff = data.targetAmt;
            amountDiffPercentage = (amountDiff/share.totalCommitAmt)*100;
            amountDiffPercentage = parseFloat(amountDiffPercentage.toFixed(2))+"%";
        }
    }

    var directionElement = "<i class='grey fas fa-arrow-right'></i>";

    if(data.target.name == "Close Lost"){
        directionElement = "<i class='red fas fa-arrow-right'></i>"
    }

    if(data.target.name == "Close Won"){
        directionElement = "<i class='green fas fa-arrow-right'></i>"
    }

    var wrapperStart = "<div class='tooltip-wrapper'>";
    var wrapperEnd = "</div>";
    var source = "<div class='source'>"+data.source.name+"</div>"
    var target = "<div class='target'>"+data.target.name+"</div>"
    var direction = "<div class='direction'>"+directionElement+"</div>"
    var moreInfoStart = "<div class='more-info'>"
    var moreInfoEnd = "</div>"
    var spanNumber = "<span>"+amountDiffPercentage+" | "+share.primaryCurrency+" "+getAmountInThousands(amountDiff,2,share.primaryCurrency == "INR")+" | "+movementCount+"</span>"+"<span>"+"</span>"
    var moreInfo = moreInfoStart+spanNumber+moreInfoEnd;

    return wrapperStart+source+direction+target+moreInfo+wrapperEnd;
}

function formatDataForSankeyGraph(data,share){

    data.metaData.forEach(function (el,index) {
        if(!el){
            data.metaData[index] = {}
        }
    });

    data.metaData.sort(function (o1,o2) {
        return o1.month - o2.month;
    });

    var startOfMonth = {},
      endOfMonth = {};

    _.each(data.oppStages,function (el) {
        startOfMonth[el] = []
        endOfMonth[el] = []
    });

    var startMonth = [],
      oppsStart = {},
      oppsEnd = {},
      endMonth = [];

    startMonth = data.metaData[0];
    endMonth = data.metaData[1].oppMetaDataFormat;

    share.startAmount = 0;
    share.endAmount = 0;

    var monthStartDate = data.qStart;
    var monthEndDate = data.qEnd;
    var oppCreatedAndClosingThisQuarter= {}

    if(endMonth){
        _.each(endMonth.data,function (st) {
            _.each(st.oppIds,function (el) {
                el.amount = parseFloat(el.amount);
                el.stageName = st.stageName;

                oppsEnd[el.opportunityId] = el;

                if(st.stageName !== "Close Won" && st.stageName !== "Close Lost"){
                    oppCreatedAndClosingThisQuarter[el.opportunityId] = el.closeDate;
                } else if(st.stageName === "Close Won" || st.stageName === "Close Lost"){
                    if(new Date(el.closeDate)>= new Date(monthStartDate) && new Date(el.closeDate)<= new Date(monthEndDate)){
                        oppCreatedAndClosingThisQuarter[el.opportunityId] = el.closeDate;
                    }
                }
            })
        });
    }

    //Opps created in the selection will be added from data.metaData[1].createdThisSelection source. Not from the opp meta data snapshot.
    // This was to reduce computations when large number of users are involved. Until we move to the new meta data on top of the existing
    // oppMetaData, this will be the implementation...

    _.each(startMonth.data,function (st) {
        _.each(st.oppIds,function (el) {

            // if(oppCreatedAndClosingThisQuarter[el.opportunityId]){ // Considering opps closing this month.
            if(oppCreatedAndClosingThisQuarter[el.opportunityId]){ // Considering opps closing this selection and also removing opps created in the month.
                el.amount = parseFloat(el.amount);
                el.stageName = st.stageName;
                oppsStart[el.opportunityId] = el
                share.startAmount = share.startAmount+parseFloat(el.amount)
            }
        })
    });

    if(data.metaData[1].createdThisSelection && data.metaData[1].createdThisSelection.length>0){
        _.each(data.metaData[1].createdThisSelection,function (el) {
            oppsStart[el.opportunityId] = el;
            share.startAmount = share.startAmount+parseFloat(el.amount)
        })
    }

    var ending = [],
      totalCommitAmt = 0,
      totalPipelineAmt = 0,
      srcCommitAmt = 0,
      targetCommitAmt = 0,
      srcPipelineAmt = 0,
      targetPipelineAmt = 0;

    for(var key1 in oppsStart){

        var source = oppsStart[key1];
        var target = oppsEnd[key1];

        if(target && target.stageName){

            var src = source.stageName, // Start
              trgt = target.stageName;

            if(!source.fromSnapShot){
                src = "New"
                srcCommitAmt = srcCommitAmt+source.amount;
                totalCommitAmt = totalCommitAmt+source.amount;
            } else if(src == data.commitStage){
                src = "Commit"
                srcCommitAmt = srcCommitAmt+source.amount;
                totalCommitAmt = totalCommitAmt+source.amount;
            } else {
                src = "Pipeline"
                srcPipelineAmt = srcPipelineAmt+source.amount;
                totalPipelineAmt = totalPipelineAmt+source.amount;
            }

            if(trgt == data.commitStage){
                trgt = "Commit "
                targetCommitAmt = targetCommitAmt+target.amount;
                totalCommitAmt = totalCommitAmt+target.amount;
            } else if(trgt !== "Close Won" && trgt !== "Close Lost") {
                targetPipelineAmt = targetPipelineAmt+target.amount;
                totalPipelineAmt = totalPipelineAmt+target.amount;
                trgt = "Pipeline "
            }

            // share.endAmount = share.endAmount+parseFloat(source.amount)
            share.endAmount = share.endAmount+parseFloat(target.amount)

            ending.push({
                source:src,
                target:trgt,
                value:1,
                sourceAmt:source.amount,
                targetAmt:target.amount
            });
        }
    }

    share.targetPipelineAmt = targetPipelineAmt;
    share.srcCommitAmt = srcCommitAmt;
    share.targetCommitAmt = targetCommitAmt;
    share.srcPipelineAmt = srcPipelineAmt;

    var group = _
      .chain(ending)
      .groupBy('source')
      .map(function(value, key) {
          var obj = {};
          var endings = groupByTargets(key,value,share);
          obj[key] = key;
          obj.value = endings
          return obj;
      })
      .value();

    return _.flatten(_.map(group,"value"));
}

function groupByTargets(pKey,data,share){

    var group = _
      .chain(data)
      .groupBy('target')
      .map(function(value, key) {
          var obj = {};

          var targetAmt = 0,
            sourceAmt = 0;

          _.each(value,function (va) {
              targetAmt = targetAmt+va.targetAmt;
              sourceAmt = sourceAmt+va.sourceAmt;
          });

          obj.source = pKey;
          obj.target = key;
          obj.value = sourceAmt;
          obj.numberOfOpps = value.length;
          obj.sourceAmt = sourceAmt;
          obj.targetAmt = targetAmt;
          obj.diffAmt = sourceAmt-targetAmt;

          return obj;
      })
      .value();

    return group;

}

function menuItems(forRelatas,tabIndex){

    var list = [{
        name:"Today",
        selected:tabIndex == 0?"selected":""
    },{
        name:"Dashboard",
        selected:tabIndex == 1?"selected":""
    },
        //     {
        //     name:"Commit",
        //     selected:""
        // },{
        //     name:"Team",
        //     selected:""
        // },
        {
            name:"Opportunity",
            selected:tabIndex == 2?"selected":""
        },{
            name:"Accounts",
            selected:""
        },{
            name:"Downloads",
            selected:""
        },{
            name:"Region",
            selected:""
        }];

    if(isTestingEnvironment()){
        list.push({
            name:"Forecast",
            selected:""
        })

        // list.push({
        //     name:"Playground",
        //     selected:""
        // })
    }

    if(window.location.pathname == "/insights/achievements"){
        list.push({
            name:"Achievements",
            selected:""
        });
    }

    return list;

}

function menuToggleSelection(select,menu) {

    _.each(menu,function (item) {
        if(item.name == select){
            item.selected = "selected"
        } else {
            item.selected = ""
        }
    })
}

function donutChart(data,className,pattern,height,width,thickness){

    var columns = _.map(data,function (el) {
        return [el.name,el.amount]
    });

    if(!height && !width){
        height = 155;
        width = 155;
    }

    if(!thickness) {
        thickness = 0.12*height
    }

    var chart = c3.generate({
        bindto: className,
        data: {
            columns: columns,
            type : 'donut'
        },
        donut: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            },
            width:thickness
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: height,
            width:width
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                return {top: top, left: parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))}
            }
        }
    });

}

function treemapChart(accounts,$scope,share) {

    var accountsData = {};
    var dataAcc = [];

    $scope.noAccounts = accounts.length === 0;

    if(accounts.length>0) {
        accounts.forEach(function (x) {
            for (var key in x) {
                accountsData[key] = (accountsData[key] || 0) + x[key];
            }
        });

        for (var key in accountsData) {
            dataAcc.push({
                name: key,
                total: parseFloat(accountsData[key].toFixed(2))
            })
        }

        accountsWon(dataAcc);

        var data = {
            "name": "cluster",
            "children": dataAcc
        };

        var range = shadeGenerator(96,125,139,dataAcc.length,10);
        var color = d3.scale.ordinal()
          .domain([0, dataAcc.length])
          .range(range);

        if(dataAcc.length>15){
            color = d3.scale.category20c();
        }
        // var color = d3.scale.category20c();

        var treemap =
          d3.layout.treemap()
            .size([100, 100])
            .sticky(true)
            .value(function(d) { return d.total; });

        var div = d3.select(".accChart");

        function position() {
            this
              .style("left", function(d) { return d.x + "%"; })
              .style("top", function(d) { return d.y + "%"; })
              .style("width", function(d) { return d.dx + "%"; })
              .style("height", function(d) { return d.dy + "%"; });
        }

        function getLabel(d) {
            return d.name;
        }

        var tooltip = d3.select("body").append("div")
          .attr("class", "tooltip")
          .style("opacity", 0);

        var node =
          div.datum(data).selectAll(".node")
            .data(treemap.nodes)
            .enter().append("div")
            .attr("class", "node")
            .call(position)
            .style("background", function(d) {
                return color(getLabel(d));
            })
            .text(getLabel)
            .on("mouseover", function(d) {
                tooltip.transition()
                  .duration(200)
                  .style("opacity", .99);
                tooltip	.html(treeMapTooltip(d,share))
                  .style("left", (d3.event.pageX - 0) + "px")
                  .style("top", (d3.event.pageY - 0) + "px");
            })
            .on("mouseout", function(d) {
                tooltip.transition()
                  .duration(500)
                  .style("opacity", 0);
            })
    }

}

function accountsWon(data){
    var chart = AmCharts.makeChart( "accChart", {
        "type": "pie",
        "theme": "light",
        "dataProvider": data,
        "labelsEnabled": false,
        "valueField": "total",
        "titleField": "name",
        "fillAlphas"  : 100,
        "balloon":{
            "fixedPosition":true
        },
        // "export": {
        //     "enabled": true
        // }
    } );

}

function pieChart(data,$scope){

    var productsData = {};
    var dataProd = [];
    var columns = [];

    $scope.noProds = data.length === 0;

    data.forEach(function(x) {
        for(var key in x){
            productsData[key] = (productsData[key] || 0)+x[key];
        }
    });

    for(var key in productsData){
        columns.push([key,productsData[key]])
        dataProd.push({
            name:key,
            total:productsData[key]
        })
    }

    // var pattern = ['#fadad0','#d1c0d1','#60a7b8','#c9deeb']
    var pattern = shadeGenerator(3,169,244,data.length,15)

    var chart = c3.generate({
        bindto: ".productChart",
        data: {
            columns: columns,
            type : 'pie'
        },
        pie: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            }
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: 200,
            width:200
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                var left = parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))
                return {top: top+50, left: 0}
            }
        }
    });

}

function buildTeamProfiles(data) {
    var team = [];

    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase()
        })
    });

    return team;
}

function buildAllTeamProfiles(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase()
        })
    });

    return team;
}

function closeAllDropDownsAndModals2($scope,id,isModal) {

    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
          && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                $scope.selectFromList = false;
                $scope.ifOpenList = false;
                $scope.displayLocations = false;
                $scope.displayPortfolios = false;

                // resetOtherDropDowns($scope,{name:"closeThis"})
            })
        }
    });
}

function setOppTableHeader($scope,share,filterListObj,importantHeaders){

    $scope.headers = [
        {
            filterReq:false,
            cursor:"",
            name:"Opp Name",
            type:"opportunityName"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Bottom Line("+share.primaryCurrency+")",
            type:"convertedAmtWithNgm",
            align:"text-right"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Stage",
            type:"stageName"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Close Date",
            type:"closeDate"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Account",
            type:"account"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Contact",
            type:"contactEmailId"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Product",
            type:"productType"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Type",
            type:"type"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Region",
            type:"geoLocation"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Currency",
            type:"currency"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Amount",
            type:"amount",
            align:"text-right"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Margin(%)",
            type:"netGrossMargin",
            align:"text-right"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Bottom Line",
            type:"convertedAmt",
            align:"text-right"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Owner",
            type:"userEmailId"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"BU",
            type:"businessUnit"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Vertical",
            type:"vertical"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Solution",
            type:"solution"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Partners",
            type:"partner"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Close Reasons",
            type:"closeReasons"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Close Reasons Description",
            type:"closeReasonsDescription"
        }
    ];

    if(importantHeaders && importantHeaders.length>0){
        _.each(importantHeaders,function (ih) {
            $scope.headers.push({
                filterReq:false,
                cursor:"",
                name:ih,
                type:ih
            })
        })
    }

    $scope.cellWidth = getWidthOfCell();

    _.each($scope.headers,function (he) {
        var type = getWidthOfCell()[he.type]
        he.styleWidth = type?type:""

        if(filterListObj && filterListObj[he.name]){
            he.values = filterListObj[he.name].values;

            if(he.type != "stageName"){
                he.values = he.values.sort(function (a,b) {
                    if(a.name < b.name) return -1;
                    if(a.name > b.name) return 1;
                    return 0;
                })
            }
        }
    })
}

function getWidthOfCell(){
    return {
        currency:'min-width:'+65+'px;max-width:'+65+'px',
        account:'min-width:'+80+'px;max-width:'+80+'px',
        amount:'min-width:'+100+'px;max-width:'+100+'px',
        convertedAmt:'min-width:'+100+'px;max-width:'+100+'px',
        blc:'min-width:'+120+'px;max-width:'+120+'px',
        netGrossMargin:'min-width:'+75+'px;max-width:'+75+'px',
        margin:'min-width:'+75+'px;max-width:'+75+'px',
        closeDate:'min-width:'+100+'px;max-width:'+100+'px',
        type:'min-width:'+75+'px;max-width:'+75+'px'
    }
}

function monthsAndYear(){

    return {
        months:[{
            name:"Jan",
            val:1
        },{
            name:"Feb",
            val:2
        },{
            name:"Mar",
            val:3
        },{
            name:"Apr",
            val:4
        },{
            name:"May",
            val:5
        },{
            name:"Jun",
            val:6
        },{
            name:"Jul",
            val:7
        },{
            name:"Aug",
            val:8
        },{
            name:"Sep",
            val:9
        },{
            name:"Oct",
            val:10
        },{
            name:"Nov",
            val:11
        },{
            name:"Dec",
            val:12
        }],
        years:[{
            name:"2015",
            val:2015
        },{
            name:"2016",
            val:2016
        },{
            name:"2017",
            val:2017
        },{
            name:"2018",
            val:2018
        },{
            name:"2019",
            val:2019
        },{
            name:"2020",
            val:2020
        }]
    }
}

function resetOtherDropDowns($scope,type){

    _.each($scope.headers,function (he) {
        if(he.name !== type.name){
            he.open = false;
        }
    })
}

function treeMapTooltip(d,share){
    var wrapperStart = "<div class='tooltip-wrapper'>"
    var wrapperEnd = "</div>"

    return wrapperStart+"<div class='left'>" +d.name+
      "</div>"+
      "<div class='right'>" +share.primaryCurrency+" "+d.total+
      "</div>" +
      "</div>"+wrapperEnd
}

function dealsAtRiskGraph($scope,share,deals,averageRisk,accessControl,totalDeals) {

    var atRisk = 0,safe = 0;
    $scope.total = deals.length;
    $scope.deals = [];
    $scope.totalDealValue = 0;
    var allRisks = _.map(deals,'riskMeter');
    $scope.totalPipeLineValue = 0;

    var dealsWithRiskValue = {};
    var amountAtRisk = 0;

    var maxRisk = _.max(allRisks);
    var minRisk = _.min(allRisks);

    var ngmReq = share.companyDetails && share.companyDetails.netGrossMargin;

    processData()

    function processData(){

        var opportunityStages = {};

        if(share.opportunityStages){
            _.each(share.opportunityStages,function (op) {
                opportunityStages[op.name] = op.order;
            })

            _.each(deals,function (de) {

                var counter = 0;

                _.forIn(de, function(value, key) {
                    if(value === true){
                        counter++
                    }
                });

                de.ngmReq = ngmReq;
                de.amountWithNgm = de.amount;

                if(ngmReq){
                    de.amountWithNgm = (de.amount*de.netGrossMargin)/100
                }

                de.stageStyle2 = oppStageStyle(de.stageName,opportunityStages[de.stageName]-1,true);

                $scope.totalPipeLineValue = $scope.totalPipeLineValue+parseFloat(de.amount);

                $scope.totalDealValue = isNumber(de.amount)?$scope.totalDealValue+parseFloat(de.amount):$scope.totalDealValue+0;

                if((de.riskMeter >= averageRisk && counter<3) || de.ltWithOwner){
                    $scope.deals.push(de)
                    atRisk++
                    amountAtRisk = amountAtRisk+parseFloat(de.amountWithNgm)
                } else {
                    safe++
                }

                de.amountWithNgm =parseFloat(de.amountWithNgm);
                de.amountWithNgm = de.amountWithNgm.r_formatNumber(2)

                var riskPerc = scaleBetween(de.riskMeter,minRisk,maxRisk);

                if(minRisk == maxRisk){
                    riskPerc = 100
                }

                var suggestion = "Opportunity at highest risk";

                if(riskPerc > 70 && riskPerc < 90){
                    suggestion = "Opportunity at high risk";
                }

                if(riskPerc > 50 && riskPerc < 70){
                    suggestion = "Opportunity at medium risk";
                }

                if(riskPerc < 50){
                    suggestion = "Opportunity at low risk";
                }

                de.amountWithCommas = numberWithCommas(parseFloat(de.amount).r_formatNumber(2),share.primaryCurrency == "INR")
                de.riskSuggestion = suggestion;
                de.riskPercentage = riskPerc+'%';
                de.riskPercentageStyle = {
                    'width':riskPerc+'%'
                }

                if(de.riskMeter >= averageRisk || de.ltWithOwner){
                    de.riskClass = 'risk-high'
                } else {
                    de.riskClass = 'risk-low'
                }

                dealsWithRiskValue[de.opportunityId] = {
                    riskMeter:de.riskMeter,
                    riskPercentage:de.riskPercentage,
                    riskPercentageStyle:de.riskPercentageStyle,
                    riskSuggestion:de.riskSuggestion,
                    riskClass:de.riskClass,
                    averageRisk:averageRisk,
                    averageInteractionsPerDeal: de.averageInteractionsPerDeal,
                    metDecisionMaker_infuencer: de.metDecisionMaker_infuencer,
                    ltWithOwner: de.ltWithOwner,
                    skewedTwoWayInteractions: de.skewedTwoWayInteractions
                }

            });

            $scope.loadingDealsAtRisk = false;

        } else {
            setTimeOutCallback(1000,function () {
                processData()
            })
        }
    }
}

function convertToCumulativeData(data){

    _.sortBy(data, [function(o) { return o.sort; }]);

    var cumulative = 0;
    _.each(data,function (el) {
        cumulative = cumulative+el.value;
        el.value = cumulative;
    });

    _.each(data,function (el) {

        if(el.value){
            el.value = parseFloat(el.value.r_formatNumber(2))
        }
    });
}

function drawLineChartLeaderBoardConversion($scope,share,series,series2,className,labelWithDates,label,order,achievement) {

    if(!series){
        series = []
    }

    if(!series2){
        series2 = []
    }

    var showArea = false;
    var grids = {
        x: {
            show: true
        },
        y: {
            show: true
        }
    }

    //This is for filling non existing months with zero values.
    var existingMonths_created = _.map(series,"meta");
    var nonExistingMonths_created = _.difference(label, existingMonths_created)

    var existingMonths_closed = _.map(series2,"meta");
    var nonExistingMonths_closed = _.differenceBy(label, existingMonths_closed);


    _.each(nonExistingMonths_created,function (el) {
        series.push({meta:el,value:0})
    });

    _.each(nonExistingMonths_closed,function (el) {
        series2.push({meta:el,value:0})
    });

    var seriesOneSorted = [];
    var seriesTwoSorted = [];

    _.each(order,function (or) {

        _.each(series,function (se) {
            if(se.meta == or.month){
                se.sort = or.sort;
                seriesOneSorted.push(se)
            }
        });

        _.each(series2,function (se2) {
            if(se2.meta == or.month){
                se2.sort = or.sort;
                seriesTwoSorted.push(se2)
            }
        });

    });

    if(achievement){
        showArea = true;
        convertToCumulativeData(seriesOneSorted);
        convertToCumulativeData(seriesTwoSorted);
    }

    var seriesArray = [seriesOneSorted];

    if(seriesOneSorted && seriesTwoSorted){
        seriesArray = [ seriesOneSorted, seriesTwoSorted]
    }

    var chart = new Chartist.Line(className, {
        labels: label,
        series: seriesArray
    }, {
        low: 0,
        showArea: showArea,
        plugins: [
            // tooltip
        ],
        width: seriesOneSorted.length>3?'175px':'150px',
        height: seriesOneSorted.length>3?'125px':'85px'
    });

    chart.on('draw', function(data) {

        if(data.type === "label" && seriesOneSorted.length>3){
            data.element.remove();

        }

        if(!achievement && data.type === 'grid' && data.index !== 0 && seriesOneSorted.length>3) {
            data.element.remove();
        }

        if(achievement){
            if(data.type === 'grid' && data.index !== 0) {
                data.element.remove();
            }
        }
    });
}

function drawLineChartLeaderBoard($scope,share,target,won,commits,className,labelWithDates,label,labelmonthYear,order,achievement) {

    if(!target){
        target = []
    }

    if(!won){
        won = []
    }

    if(!commits){
        commits = []
    }

    var existingMonths_created = _.map(target,"meta");
    var nonExistingMonths_created = _.difference(label, existingMonths_created)

    var existingMonths_closed = _.map(won,"meta");
    var nonExistingMonths_closed = _.differenceBy(label, existingMonths_closed);


    _.each(nonExistingMonths_created,function (el) {
        target.push({meta:el,value:0})
    });

    _.each(nonExistingMonths_closed,function (el) {
        won.push({meta:el,value:0})
    });

    _.each(commits,function(co){
        co.meta = moment(co.date).format("MMM")
    })

    var colTarget = ["Target"],
      colWon = ["Won"],
      colWonGrowth = ["Cumulative Won"],
      cumulativeWon = [],
      colCommit = ["Commit"];

    _.each(order,function (or) {

        _.each(target,function (se) {
            if(se.meta == or.month){
                se.sort = or.sort;
                colTarget.push(se.value)
            }
        });

        _.each(won,function (se2) {
            if(se2.meta == or.month){
                se2.sort = or.sort;
                colWon.push(se2.value)
                cumulativeWon.push(se2.value)
            }
        });

        _.each(commits,function (se2) {
            if(se2.meta == or.month){
                se2.sort = or.sort;
                colCommit.push(se2.amount)
            }
        });

    });

    colWonGrowth = colWonGrowth.concat(getCumulativeOfArray(cumulativeWon));

    var axes = {
    }

    var colors = {
        "Target": "#FE9E83",
        "Commit": '#638ca6',
        'Won': '#8ECECB',
        'Cumulative Won': '#0f825b'
    }

    var chart = c3.generate({
        bindto: className,
        data: {
            columns: [colTarget,colWon,colCommit,colWonGrowth],
            // axes: axes,
            type: 'bar',
            types: {
                'Cumulative Won': 'spline'
            },
            colors:colors,
            bar: {
                width: {
                    ratio: 0.25
                }
            },
            onclick: function(e) {
            }
        },
        axis: {
            y: {
                label: {
                    text:'Amount',
                    position: 'outer-bottom'
                },
                show: false,
                tick: {
                    fit: true,
                    format: function (x) {
                        return getAmountInThousands(x,2,share.primaryCurrency == "INR");
                    }
                }
            },
            x : {
                tick: {
                    fit: true,
                    format: function (x) {
                        return label[x];
                    }
                }
            }
        },
        tooltip: {
            format: {
                title: function (x) {
                    return labelmonthYear[x];
                }
            }
        },
        legend: {
            show: false
        }
    });
}

function drawLineChart($scope,share,series,label,className,series2) {

    var seriesArray = [series];

    if(series && series2){
        seriesArray = [ series, series2]
    }

    new Chartist.Line(className, {
        labels: label,
        series: seriesArray
    }, {
        low: 0,
        showArea: true,
        plugins: [
            tooltip
        ]
    });
}

function groupAndChainForTeamSummary(data,share) {

    var totalAmount = 0;
    var group = _
      .chain(_.flatten(data))
      .groupBy(function (el) {
          if(el && el.name && el.name != "null"){
              return el.name;
          } else if((el && (el.name == null || el.name == "null")) || (el && !el.name && el.amount)) {
              return "Others"
          }
      })
      .map(function(values, key) {

          if(checkRequired(key)){
              var amount = _.sumBy(values, 'amount');
              totalAmount = amount;

              return {
                  // nameTruncated:key.substring(0,12),
                  nameTruncated:key,
                  name:key,
                  amount:amount,
                  amountWithCommas:share?getAmountInThousands(amount,2,share.primaryCurrency == "INR"):getAmountInThousands(amount,2)
              }
          }
      })
      .value();

    var sortProperty = "amount";

    group.sort(function (o1, o2) {
        return o2[sortProperty] > o1[sortProperty] ? 1 : o2[sortProperty] < o1[sortProperty] ? -1 : 0;
    });

    return _.compact(group);

}

function getFilterDates(share,$scope){

    var format = "MMM YYYY";
    var startDtObj = {'year': 2018, 'month': 3}
    var endDtObj = {'year': 2018, 'month': 5}
    $scope.quartersForFilter = [{
        range: {
            qStart:new Date(moment(startDtObj).startOf("month")),
            qEnd:new Date(moment(endDtObj).endOf("month"))
        },
        display:  moment(new Date(moment(startDtObj))).format(format)+"-"+moment(new Date(moment(endDtObj))).format(format)
    },{
        range: {
            qStart:new Date(moment({'year': 2018, 'month': 6}).startOf("month")),
            qEnd:new Date(moment({'year': 2018, 'month': 8}).endOf("month"))
        },
        display:  moment(new Date(moment({'year': 2018, 'month': 6}))).format(format)+"-"+moment(new Date(moment({'year': 2018, 'month': 8}))).format(format)
    },{
        range: {
            qStart:new Date(moment({'year': 2018, 'month': 9}).startOf("month")),
            qEnd:new Date(moment({'year': 2018, 'month': 11}).endOf("month"))
        },
        display:  moment(new Date(moment({'year': 2018, 'month': 9}))).format(format)+"-"+moment(new Date(moment({'year': 2018, 'month': 11}))).format(format)
    },{
        range: {
            qStart:new Date(moment({'year': 2019, 'month': 0}).startOf("month")),
            qEnd:new Date(moment({'year': 2019, 'month': 2}).endOf("month"))
        },
        display:  moment(new Date(moment({'year': 2019, 'month': 0}))).format(format)+"-"+moment(new Date(moment({'year': 2019, 'month': 2}))).format(format)
    },{
        range: {
            qStart:new Date(moment({'year': 2019, 'month': 3}).startOf("month")),
            qEnd:new Date(moment({'year': 2019, 'month': 5}).endOf("month"))
        },
        display:  moment(new Date(moment({'year': 2019, 'month': 3}))).format(format)+"-"+moment(new Date(moment({'year': 2019, 'month': 5}))).format(format)
    },{
        range: {
            qStart:new Date(moment({'year': 2019, 'month': 6}).startOf("month")),
            qEnd:new Date(moment({'year': 2019, 'month': 8}).endOf("month"))
        },
        display:  moment(new Date(moment({'year': 2019, 'month': 6}))).format(format)+"-"+moment(new Date(moment({'year': 2019, 'month': 8}))).format(format)
    },{
        range: {
            qStart:new Date(moment({'year': 2019, 'month': 9}).startOf("month")),
            qEnd:new Date(moment({'year': 2019, 'month': 11}).endOf("month"))
        },
        display:  moment(new Date(moment({'year': 2019, 'month': 9}))).format(format)+"-"+moment(new Date(moment({'year': 2019, 'month': 11}))).format(format)
        }];

    $scope.qtrFilterSelected = $scope.quartersForFilter[$scope.quartersForFilter.length-1].display;
}

    function composeCloseDateFilters(share, type) {
        var scope = angular.element($("#opportunity-insights")).scope();

        var colType = {
            cursor: "cursor",
            filterReq: true,
            name: "Close Date",
            open: true,
            type: "closeDate",
            includeDateRange: true
        }

        if(type == "today_deals_closing") {
            var startDate = moment()
            var endDate = moment().add(30,"days");

            scope.start.month = String(startDate.format('M'));
            scope.start.year = String(startDate.format('YYYY'));

            scope.end.month = String(endDate.format('M'));
            scope.end.year = String(endDate.format('YYYY'));

        } else if(type == "today_overdue") {
            var startDate = moment(new Date("01 Jan 2015"))
            var endDate = moment().subtract(1,"days");

            scope.start.month = String(startDate.format('M'));
            scope.start.year = String(startDate.format('YYYY'));
            scope.start.date = String(startDate.format('DD'));

            scope.end.month = String(endDate.format('M'));
            scope.end.year = String(endDate.format('YYYY'));
            scope.end.date = String(endDate.format('DD'));

        }


        return colType
    }

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    function handleSideBarSelections(ControllerChecker,$scope,$rootScope,share,viewFor,redirectFrom) {
        mixpanelTracker("Insights "+viewFor.name);

        $rootScope.viewForSelected = viewFor.name.toLowerCase()
        $scope.viewFor = viewFor.name.toLowerCase();
        share.viewFor($scope.viewFor);
        share.redirectFrom = redirectFrom;

        menuToggleSelection(viewFor.name,$scope.menu);

        if(viewFor && viewFor.name.toLowerCase() == "opportunity"){
            if(share.redirectFrom == "today_deals_closing") {
                var colType = composeCloseDateFilters(share, redirectFrom);
                share.applyFilters(colType);

            } else if(share.redirectFrom == "today_overdue") {
                var colType = composeCloseDateFilters(share, redirectFrom);
                share.applyFilters(colType);
            } else if($rootScope.viewForSelected == "opportunity" && $rootScope.oppUrl == '/reports/opportunities'){
                $rootScope.oppUrl = '/reports/opportunities';
                $rootScope.popUrl = '/reports/opportunities/edit/popup';

                function checkCtrlLoad() {

                    if($("#opportunity-insights") && $("#opportunity-insights").length>0 && share.currentFy){
                        share.resetPrevfilters();
                        share.populateFilters(share.companyDetails);
                        share.setCurrentQuarter(share.currentFy);
                        share.drawOppsTable();

                    } else {
                        setTimeOutCallback(50,function () {
                            checkCtrlLoad()
                        });
                    }
                }

                checkCtrlLoad();
            };
        } else if(viewFor && viewFor.name.toLowerCase() == "accounts") {
            $rootScope.accUrl = '/reports/account';

            function checkCtrlLoadA() {

                if($(".accounts-ints") && $(".accounts-ints").length>0){
                    share.resetUserSelection(true)
                } else {
                    setTimeOutCallback(150,function () {
                        checkCtrlLoadA()
                    })
                }
            }

            checkCtrlLoadA();
        } else if(viewFor && viewFor.name.toLowerCase() == "downloads") {
            $rootScope.dwnUrl = '/reports/exceptional/access';
        } else if(viewFor && viewFor.name.toLowerCase() == "dashboard") {
            share.resetUserSelection(true)
        } else if(viewFor && viewFor.name.toLowerCase() == "region") {
            $rootScope.regionUrl = '/reports/region/template';

            function checkCtrlLoadR() {

                if($("#mapWrapper") && $("#mapWrapper").length>0){
                    share.regionChart(true);
                } else {
                    setTimeOutCallback(150,function () {
                        checkCtrlLoadR()
                    })
                }
            }

            checkCtrlLoadR();
        } else if(viewFor && viewFor.name.toLowerCase() == "achievements"){
            share.getAchievementByPortFolios($rootScope.achvBy);
        } else {
            share.resetUserSelection(true)
        }

        if(viewFor && viewFor.name.toLowerCase() == "today") {

            $rootScope.todayUrl = '/reports/today';
            $scope.showLiu = false;
            function checkCtrlLoadT() {
                if($(".today-wrapper") && $(".today-wrapper").length>0){
                    share.loadToday();
                    share.loadTargets();

                } else {
                    setTimeOutCallback(50,function () {
                        checkCtrlLoadT()
                    })
                }
            }

            checkCtrlLoadT();
        } else {
            $scope.showLiu = true;
        }

        if(viewFor && viewFor.name.toLowerCase() == "allcrossfilters"){

        }

        if(viewFor && viewFor.name.toLowerCase() == "forecast"){
            share.loadGraphs()
        }
    }

    function getRegionChart($http,$scope,share,emailIds,stage,startDate,endDate) {

        var url = "/reports/region";
        if(emailIds && emailIds.length>0){
            url = fetchUrlWithParameter(url+"?forUserEmailId="+emailIds)
        }

        if(stage && stage != "Show All Opportunities"){
            url = fetchUrlWithParameter(url+"&stage="+stage)
        }

        if(startDate){
            url = fetchUrlWithParameter(url+"&startDate="+startDate)
        }

        if(endDate){
            url = fetchUrlWithParameter(url+"&endDate="+endDate)
        }

        $scope.tbRows = [];

        $scope.tbHeaders = ["Opportunity Name", "Company", "Stage","Currency", "Amount", "Sales Person", "Selling To"];

        $http.get(url)
          .success(function (response) {

              // var center = [20, 70];//india
              var center = [0, 0];
              $scope.regionOpps = {}

              var oppsWithLoc = [];$scope.oppsWithNoLoc = [];
              $scope.oppsWithLoc = [];

              if(response && response.length>0){
                  _.each(response,function (el) {
                      if(!el.lat && !el.lon){
                          el.lat = center[0];
                          el.lon = center[1];
                      }


                      el.opps.forEach(function (op) {

                          op.stageColor = "";

                          if(op.stageName == "Close Won") {
                              op.stageColor = "won"
                          }

                          if(op.stageName == "Close Lost") {
                              op.stageColor = "lost"
                          }
                      })

                      if(el.loc !== "0/0"){
                          oppsWithLoc.push(el)
                      } else {
                          $scope.oppsWithNoLoc = $scope.oppsWithNoLoc.concat(el.opps);
                      }

                      $scope.regionOpps[el.loc] = el;
                  });

              };
              $scope.oppsWithLoc = oppsWithLoc;
              drawRegionChart(oppsWithLoc,center,$scope);
          });
    }

    function drawRegionChart(data,center,$scope){

        document.getElementById('mapWrapper').innerHTML = "<div id='regionMap' style='width: 99%; height: 400px;'></div>";
        $scope.loadingMap = false;
        var max = _.maxBy(data,"total")
        var min = _.minBy(data,"total")

        var mymap = L.map('regionMap', {scrollWheelZoom: false}).setView(center, 2)

        var tiles = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}');
        tiles.addTo(mymap);
        var charts = {};

        function onClick(e) {
            $scope.loadingOpps = true;
            if(e && e.target && e.target.options && e.target.options.orginal_city){
                $scope.displayOpps(e.target.options.orginal_city);
            }
        }

        for (var i = 0; i < data.length; i++) {
            var d = data[i];

            var scores = [
                d.Won,
                d.Lost,
                d.Pipeline
            ];

            charts[d.city] = L.minichart([d.lat, d.lon], {data: scores,
                maxValues: max.total,
                type:"pie",
                orginal_city:d.city,
                width:scaleBetween(d.total, min.total, max.total,10,95)
            });

            charts[d.city].on("click", function (e) {
                $scope.$apply(function () {
                    onClick(e);
                });
            });

            mymap.addLayer(charts[d.city] ) ;
        }
    }

    function drawRegionContactsChart(data,center,$scope,filtered){
        $scope.oppsWithLoc = [];
        $scope.loadingMap = false;
        document.getElementById('mapWrapper').innerHTML = "<div id='regionMap' style='width: 99%; height: 400px;'></div>";

        var max = _.maxBy(data,"count")
        var min = _.minBy(data,"count")

        var mymap = L.map('regionMap', {scrollWheelZoom: false}).setView(center, 2)

        var tiles = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}');
        tiles.addTo(mymap);
        var charts = {};

        function onClickContacts(e) {
            $scope.loadingOpps = true;
            if(e && e.target && e.target.options && e.target.options.orginal_city){
                $scope.displayContacts(e.target.options.orginal_city);
            }
        }

        if(!filtered){
            $scope.locations = [];
            $scope.storeLocations = data;
        }

        $scope.searchForLocations = function(){
            $scope.displayLocations = true;
        }

        $scope.loadThisLocation = function(loc){
            $scope.displayLocations = false;
            $scope.location = loc.name;
            drawRegionContactsChart(_.filter($scope.storeLocations,function (el) {
                return el.loc == loc.latLong;
            }),center,$scope,true)
        }

        $scope.tbRows = [];

        var oppsWithLoc = [];$scope.oppsWithNoLoc = [];

        _.each(data,function (el) {

            if(el.loc !== "0/0"){
                if(el.contacts && !filtered){
                    $scope.locations.push({
                        latLong:el.loc,
                        name:el.contacts[0].location
                    });
                }
                oppsWithLoc.push(el)
            } else {
                $scope.oppsWithNoLoc = $scope.oppsWithNoLoc.concat(el.opps)
            }
        })

        $scope.contactsObj = {};
        $scope.oppsWithLoc = oppsWithLoc;

        for (var i = 0; i < oppsWithLoc.length; i++) {
            var d = data[i];

            var scores = [d.count];

            $scope.contactsObj[d.city] = d.contacts;

            charts[d.city] = L.minichart([d.lat, d.lon], {data: scores,
                maxValues: max.count,
                type:"pie",
                orginal_city:d.city,
                width:scaleBetween(d.count, min.count, max.count,25,75)
            });

            charts[d.city].on("click", function (e) {
                $scope.$apply(function () {
                    onClickContacts(e);
                });
            });

            mymap.addLayer(charts[d.city] ) ;
        }
    }

    function cursorPos(canvas,evt) {
        return getMousePos(canvas, evt);
    }

    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
    }

    var tooltip = Chartist.plugins.tooltip();

    relatasApp.service('ControllerChecker', function($controller) {
        return {
            controllerExists: function(controllerName) {
                if(typeof window[controllerName] == 'function') {
                    return true;
                }
                try {
                    $controller(controllerName);
                    return true;
                } catch (error) {
                    return !(error instanceof TypeError);
                }
            }
        };
    });

    function getParams(url){
        var regex = /[?&]([^=#]+)=([^&#]*)/g,
          params = {},
          match;
        while(match = regex.exec(url)) {
            params[match[1]] = match[2];
        }
        return params;
    }

    function somedata() {
        return {
            "data": [
                {
                    "userEmailId": "naveenpaul.markunda@gmail.com",
                    "userId": "54ed77eec55b18101e82f205",
                    "totalDeals": 30,
                    "wonDeals": 1,
                    "totalOppAmount": 24391.57,
                    "wonAmount": 11,
                    "lostAmount": 1404.95,
                    "interactionsCountWon": 81,
                    "interactionsCountAll": 93,
                    "daysToLostCloseDeal": 17547,
                    "daysToWinCloseDeal": 18021,
                    "allContacts": [
                        "jimmytestacc@gmail.com",
                        "relatas2@gmail.com",
                        "naveenpaul@relatas.com",
                        "iamnaveenpaul@gmail.com",
                        "movies@inoxmovies.com",
                        "sureshhoel@gmail.com",
                        "naveen@anomkin.com",
                        "isaacbhaskar@outlook.com",
                        "india@udacity.com",
                        "naveen@saleshq.in",
                        "suresh@saleshq.in",
                        "appexchange-notif@salesforce.com",
                        "hdfclife@epromo.hdfclife.asia",
                        "justein346@gmail.com",
                        "abhimanyu09@gmail.com",
                        "nikita@saleshq.in",
                        "imnaveenpaul@gmail.com",
                        "naveen.paul.jimmy@facebook.com"
                    ],
                    "companies": [
                        {
                            "name": null,
                            "nameTruncated": null,
                            "amount": 11
                        }
                    ],
                    "products": [
                        {
                            "name": "ARTracker",
                            "nameTruncated": "ARTracker",
                            "amount": 11
                        }
                    ],
                    "verticals": [
                        {
                            "name": "",
                            "nameTruncated": "",
                            "amount": 11
                        }
                    ],
                    "regions": [
                        {
                            "name": "East",
                            "nameTruncated": "East",
                            "amount": 11
                        }
                    ],
                    "bus": [
                        {
                            "name": "null",
                            "nameTruncated": "null",
                            "amount": 11
                        }
                    ],
                    "monthlyCreated": [],
                    "monthlyClosed": [
                        "2019-05-05T02:14:35.799Z"
                    ],
                    "monthlyOppWon": [
                        {
                            "date": "2019-05-05T02:14:35.799Z",
                            "amount": 11
                        }
                    ],
                    "contactsByRelation": {
                        "influencers": [],
                        "dms": [],
                        "partners": [],
                        "owners": [
                            "jimmytestacc@gmail.com",
                            "relatas2@gmail.com",
                            "naveenpaul@relatas.com",
                            "iamnaveenpaul@gmail.com",
                            "movies@inoxmovies.com",
                            "sureshhoel@gmail.com",
                            "naveen@anomkin.com",
                            "isaacbhaskar@outlook.com",
                            "india@udacity.com",
                            "naveen@saleshq.in",
                            "suresh@saleshq.in",
                            "appexchange-notif@salesforce.com",
                            "hdfclife@epromo.hdfclife.asia",
                            "justein346@gmail.com",
                            "abhimanyu09@gmail.com",
                            "nikita@saleshq.in",
                            "imnaveenpaul@gmail.com",
                            "naveen.paul.jimmy@facebook.com"
                        ]
                    },
                    "commits": 500,
                    "targetsCount": 66,
                    "targets": [
                        {
                            "date": "2019-04-04T18:30:00.000Z",
                            "target": 11,
                            "monthYear": "Apr 2019",
                            "amount": 11
                        },
                        {
                            "date": "2019-05-04T18:30:00.000Z",
                            "target": 22,
                            "monthYear": "May 2019",
                            "amount": 22
                        },
                        {
                            "date": "2019-06-04T18:30:00.000Z",
                            "target": 33,
                            "monthYear": "Jun 2019",
                            "amount": 33
                        }
                    ]
                },
                {
                    "userEmailId": "relatas2@gmail.com",
                    "userId": "57c12ffe1f8f4c6356283ef8",
                    "totalDeals": 35,
                    "wonDeals": 0,
                    "totalOppAmount": 313650,
                    "wonAmount": 0,
                    "lostAmount": 1850,
                    "interactionsCountWon": 0,
                    "interactionsCountAll": 0,
                    "daysToLostCloseDeal": 0,
                    "daysToWinCloseDeal": 0,
                    "allContacts": [
                        "sureshhoel@outlook.com",
                        "naveenpaul@relatas.com",
                        "robby@amazon.com",
                        "dara@uber.com",
                        "relatasadmintest@aporv.com",
                        "sudip@relatas.com",
                        "asst@pinterest.com"
                    ],
                    "companies": [],
                    "products": [],
                    "verticals": [],
                    "regions": [],
                    "bus": [],
                    "monthlyCreated": [],
                    "monthlyClosed": [],
                    "monthlyOppWon": [],
                    "contactsByRelation": {
                        "influencers": [],
                        "dms": [],
                        "partners": [],
                        "owners": [
                            "sureshhoel@outlook.com",
                            "naveenpaul@relatas.com",
                            "robby@amazon.com",
                            "dara@uber.com",
                            "relatasadmintest@aporv.com",
                            "sudip@relatas.com",
                            "asst@pinterest.com"
                        ]
                    },
                    "targetsCount": 0,
                    "targets": [
                        {
                            "date": "2019-04-04T18:30:00.000Z",
                            "target": 0,
                            "monthYear": "Apr 2019",
                            "amount": 0
                        },
                        {
                            "date": "2019-05-04T18:30:00.000Z",
                            "target": 0,
                            "monthYear": "May 2019",
                            "amount": 0
                        },
                        {
                            "date": "2019-06-04T18:30:00.000Z",
                            "target": 0,
                            "monthYear": "Jun 2019",
                            "amount": 0
                        }
                    ]
                },
                {
                    "userEmailId": "sanjayjha3015@gmail.com",
                    "userId": "56a471cf2fffe81850ff2166",
                    "totalDeals": 1,
                    "wonDeals": 0,
                    "totalOppAmount": 1000,
                    "wonAmount": 0,
                    "lostAmount": 0,
                    "interactionsCountWon": 0,
                    "interactionsCountAll": 0,
                    "daysToLostCloseDeal": 0,
                    "daysToWinCloseDeal": 0,
                    "allContacts": [
                        "dara@uber.com"
                    ],
                    "companies": [],
                    "products": [],
                    "verticals": [],
                    "regions": [],
                    "bus": [],
                    "monthlyCreated": [
                        "2019-05-29T18:21:56.719Z"
                    ],
                    "monthlyClosed": [],
                    "monthlyOppWon": [],
                    "contactsByRelation": {
                        "influencers": [],
                        "dms": [],
                        "partners": [],
                        "owners": [
                            "dara@uber.com"
                        ]
                    },
                    "targetsCount": 66,
                    "targets": [
                        {
                            "date": "2019-04-04T18:30:00.000Z",
                            "target": 11,
                            "monthYear": "Apr 2019",
                            "amount": 11
                        },
                        {
                            "date": "2019-05-04T18:30:00.000Z",
                            "target": 22,
                            "monthYear": "May 2019",
                            "amount": 22
                        },
                        {
                            "date": "2019-06-04T18:30:00.000Z",
                            "target": 33,
                            "monthYear": "Jun 2019",
                            "amount": 33
                        }
                    ]
                },
                {
                    "userEmailId": "sureshhoel@gmail.com",
                    "userId": "541c83d89ff44f767a23c546",
                    "totalDeals": 135,
                    "wonDeals": 9,
                    "totalOppAmount": 366082.19,
                    "wonAmount": 8265,
                    "lostAmount": 7200,
                    "interactionsCountWon": 22,
                    "interactionsCountAll": 418,
                    "daysToLostCloseDeal": 35680,
                    "daysToWinCloseDeal": 1653,
                    "allContacts": [
                        "relatas3@gmail.com",
                        "sanjayjha3015@gmail.com",
                        "sofia@relatas.com",
                        "naveenpaul.markunda@gmail.com",
                        "neil@neilpatel.com",
                        "naveenpaul@relatas.com",
                        "relatas2@gmail.com",
                        "wired@eml.condenast.com",
                        "sudip@relatas.com",
                        "ajit.singh@webengage.com",
                        "avantika.pandey@webengage.com",
                        "aananth@salesforce.com",
                        "sumitrampal@gmail.com",
                        "kemparajutest@outlook.com",
                        "sam@anomkin.com",
                        "pallav@fusioncharts.com",
                        "gabriela.giacoman@evercontact.com",
                        "paramveer.singh@startv.com",
                        "abhijit_shriyan@infosys.com",
                        "bsherfy@rackspace.com",
                        "robby@amazon.com",
                        "ramanan.sambukumar@wipro.com",
                        "fio@hotjar.com",
                        "info@zemeorganics.com",
                        "iman.roy@wipro.com",
                        "avlesh@webengage.com",
                        "ankur@filtr.io",
                        "raj_sajja@bmc.com",
                        "pjain@cisco.com",
                        "shdas@cisco.com",
                        "shiladitya@apple.com",
                        "chris.reimer@rackspace.com",
                        "dmills@rackspace.com",
                        "sumit@relatas.com",
                        "aarron@invisionapp.com",
                        "bharadwaj.nagendra@ivalue.co.in",
                        "aditya_jha@infosys.com",
                        "debabratabagchi@google.com",
                        "becky.doyle@rackspace.com",
                        "mkbagrecha@digitalcontrolls.com",
                        "aditya.pathak@cognizant.com",
                        "sandeep.mina@swiggy.in",
                        "kemparaju.1112@gmail.com",
                        "shyampjoy@pes.edu",
                        "ruradhak@cisco.com",
                        "ajitmoily@relatas.com"
                    ],
                    "companies": [
                        {
                            "name": "webengage",
                            "nameTruncated": "webengage",
                            "amount": 1000
                        },
                        {
                            "name": "webengage",
                            "nameTruncated": "webengage",
                            "amount": 1000
                        },
                        {
                            "name": "rackspace",
                            "nameTruncated": "rackspace",
                            "amount": 500
                        },
                        {
                            "name": "wipro",
                            "nameTruncated": "wipro",
                            "amount": 1500
                        },
                        {
                            "name": "infosys",
                            "nameTruncated": "infosys",
                            "amount": 500
                        },
                        {
                            "name": "cognizant",
                            "nameTruncated": "cognizant",
                            "amount": 500
                        },
                        {
                            "name": "rackspace",
                            "nameTruncated": "rackspace",
                            "amount": 1265
                        },
                        {
                            "name": "wipro",
                            "nameTruncated": "wipro",
                            "amount": 1000
                        },
                        {
                            "name": "wipro",
                            "nameTruncated": "wipro",
                            "amount": 1000
                        }
                    ],
                    "products": [
                        {
                            "name": "CRM",
                            "nameTruncated": "CRM",
                            "amount": 1000
                        },
                        {
                            "name": "CRM",
                            "nameTruncated": "CRM",
                            "amount": 1000
                        },
                        {
                            "name": "A10",
                            "nameTruncated": "A10",
                            "amount": 500
                        },
                        {
                            "name": "CRM",
                            "nameTruncated": "CRM",
                            "amount": 1500
                        },
                        {
                            "name": "CRM",
                            "nameTruncated": "CRM",
                            "amount": 500
                        },
                        {
                            "name": "CRM",
                            "nameTruncated": "CRM",
                            "amount": 500
                        },
                        {
                            "name": "CRM",
                            "nameTruncated": "CRM",
                            "amount": 1265
                        },
                        {
                            "name": "CRM",
                            "nameTruncated": "CRM",
                            "amount": 1000
                        },
                        {
                            "name": "A10",
                            "nameTruncated": "A10",
                            "amount": 1000
                        }
                    ],
                    "verticals": [
                        {
                            "name": "BFSI",
                            "nameTruncated": "BFSI",
                            "amount": 1000
                        },
                        {
                            "name": "Enterprise",
                            "nameTruncated": "Enterprise",
                            "amount": 1000
                        },
                        {
                            "name": "Govt",
                            "nameTruncated": "Govt",
                            "amount": 500
                        },
                        {
                            "name": "Govt",
                            "nameTruncated": "Govt",
                            "amount": 1500
                        },
                        {
                            "name": "Govt",
                            "nameTruncated": "Govt",
                            "amount": 500
                        },
                        {
                            "name": "Govt",
                            "nameTruncated": "Govt",
                            "amount": 500
                        },
                        {
                            "name": "Govt",
                            "nameTruncated": "Govt",
                            "amount": 1265
                        },
                        {
                            "name": "Enterprise",
                            "nameTruncated": "Enterprise",
                            "amount": 1000
                        },
                        {
                            "name": "Govt",
                            "nameTruncated": "Govt",
                            "amount": 1000
                        }
                    ],
                    "regions": [
                        {
                            "name": "North",
                            "nameTruncated": "North",
                            "amount": 1000
                        },
                        {
                            "name": "South",
                            "nameTruncated": "South",
                            "amount": 1000
                        },
                        {
                            "name": "East",
                            "nameTruncated": "East",
                            "amount": 500
                        },
                        {
                            "name": "East",
                            "nameTruncated": "East",
                            "amount": 1500
                        },
                        {
                            "name": "East",
                            "nameTruncated": "East",
                            "amount": 500
                        },
                        {
                            "name": "East",
                            "nameTruncated": "East",
                            "amount": 500
                        },
                        {
                            "name": "North",
                            "nameTruncated": "North",
                            "amount": 1265
                        },
                        {
                            "name": "South",
                            "nameTruncated": "South",
                            "amount": 1000
                        },
                        {
                            "name": "East",
                            "nameTruncated": "East",
                            "amount": 1000
                        }
                    ],
                    "bus": [
                        {
                            "name": "Security",
                            "nameTruncated": "Security",
                            "amount": 1000
                        },
                        {
                            "name": "Security",
                            "nameTruncated": "Security",
                            "amount": 1000
                        },
                        {
                            "name": "Security",
                            "nameTruncated": "Security",
                            "amount": 500
                        },
                        {
                            "name": "Networking",
                            "nameTruncated": "Networking",
                            "amount": 1500
                        },
                        {
                            "name": "BPO",
                            "nameTruncated": "BPO",
                            "amount": 500
                        },
                        {
                            "name": "BPO",
                            "nameTruncated": "BPO",
                            "amount": 500
                        },
                        {
                            "name": "Networking",
                            "nameTruncated": "Networking",
                            "amount": 1265
                        },
                        {
                            "name": "null",
                            "nameTruncated": "null",
                            "amount": 1000
                        },
                        {
                            "name": "BPO",
                            "nameTruncated": "BPO",
                            "amount": 1000
                        }
                    ],
                    "monthlyCreated": [
                        "2019-04-23T07:10:03.896Z",
                        "2019-04-01T06:04:33.171Z",
                        "2019-05-30T09:17:03.447Z",
                        "2019-04-18T11:03:31.370Z",
                        "2019-05-10T06:17:37.697Z",
                        "2019-05-21T15:13:26.652Z",
                        "2019-04-09T11:09:56.893Z",
                        "2019-05-03T05:54:33.966Z",
                        "2019-04-30T12:20:36.609Z",
                        "2019-05-11T20:04:17.687Z",
                        "2019-05-30T09:04:09.497Z",
                        "2019-04-23T11:49:37.846Z",
                        "2019-05-21T14:38:36.493Z"
                    ],
                    "monthlyClosed": [
                        "2019-04-01T05:52:05.914Z",
                        "2019-05-29T18:33:28.391Z",
                        "2019-05-30T09:18:04.685Z",
                        "2019-06-01T12:37:55.013Z",
                        "2019-05-21T15:13:39.187Z",
                        "2019-05-30T09:04:32.594Z",
                        "2019-05-16T20:08:35.822Z",
                        "2019-04-09T11:09:56.887Z",
                        "2019-05-21T14:38:36.605Z"
                    ],
                    "monthlyOppWon": [
                        {
                            "date": "2019-04-01T05:52:05.914Z",
                            "amount": 1000
                        },
                        {
                            "date": "2019-05-29T18:33:28.391Z",
                            "amount": 1000
                        },
                        {
                            "date": "2019-05-30T09:18:04.685Z",
                            "amount": 500
                        },
                        {
                            "date": "2019-06-01T12:37:55.013Z",
                            "amount": 1500
                        },
                        {
                            "date": "2019-05-21T15:13:39.187Z",
                            "amount": 500
                        },
                        {
                            "date": "2019-05-30T09:04:32.594Z",
                            "amount": 500
                        },
                        {
                            "date": "2019-05-16T20:08:35.822Z",
                            "amount": 1265
                        },
                        {
                            "date": "2019-04-09T11:09:56.887Z",
                            "amount": 1000
                        },
                        {
                            "date": "2019-05-21T14:38:36.605Z",
                            "amount": 1000
                        }
                    ],
                    "contactsByRelation": {
                        "influencers": [],
                        "dms": [],
                        "partners": [],
                        "owners": [
                            "relatas3@gmail.com",
                            "sanjayjha3015@gmail.com",
                            "sofia@relatas.com",
                            "naveenpaul.markunda@gmail.com",
                            "neil@neilpatel.com",
                            "naveenpaul@relatas.com",
                            "relatas2@gmail.com",
                            "wired@eml.condenast.com",
                            "sudip@relatas.com",
                            "ajit.singh@webengage.com",
                            "avantika.pandey@webengage.com",
                            "aananth@salesforce.com",
                            "sumitrampal@gmail.com",
                            "kemparajutest@outlook.com",
                            "sam@anomkin.com",
                            "pallav@fusioncharts.com",
                            "gabriela.giacoman@evercontact.com",
                            "paramveer.singh@startv.com",
                            "abhijit_shriyan@infosys.com",
                            "bsherfy@rackspace.com",
                            "robby@amazon.com",
                            "ramanan.sambukumar@wipro.com",
                            "fio@hotjar.com",
                            "info@zemeorganics.com",
                            "iman.roy@wipro.com",
                            "avlesh@webengage.com",
                            "ankur@filtr.io",
                            "raj_sajja@bmc.com",
                            "pjain@cisco.com",
                            "shdas@cisco.com",
                            "shiladitya@apple.com",
                            "chris.reimer@rackspace.com",
                            "dmills@rackspace.com",
                            "sumit@relatas.com",
                            "aarron@invisionapp.com",
                            "bharadwaj.nagendra@ivalue.co.in",
                            "aditya_jha@infosys.com",
                            "debabratabagchi@google.com",
                            "becky.doyle@rackspace.com",
                            "mkbagrecha@digitalcontrolls.com",
                            "aditya.pathak@cognizant.com",
                            "sandeep.mina@swiggy.in",
                            "kemparaju.1112@gmail.com",
                            "shyampjoy@pes.edu",
                            "ruradhak@cisco.com",
                            "ajitmoily@relatas.com"
                        ]
                    },
                    "commits": 2000,
                    "targetsCount": 3000,
                    "targets": [
                        {
                            "date": "2019-04-04T18:30:00.000Z",
                            "target": 1000,
                            "monthYear": "Apr 2019",
                            "amount": 1000
                        },
                        {
                            "date": "2019-05-04T18:30:00.000Z",
                            "target": 1000,
                            "monthYear": "May 2019",
                            "amount": 1000
                        },
                        {
                            "date": "2019-06-04T18:30:00.000Z",
                            "target": 1000,
                            "monthYear": "Jun 2019",
                            "amount": 1000
                        }
                    ]
                },
                {
                    "userEmailId": "sureshhoel@gmail.com",
                    "userId": "5c21f72717e5b63ff5190b72",
                    "totalDeals": 1,
                    "wonDeals": 0,
                    "totalOppAmount": 100,
                    "wonAmount": 0,
                    "lostAmount": 0,
                    "interactionsCountWon": 0,
                    "interactionsCountAll": 0,
                    "daysToLostCloseDeal": 0,
                    "daysToWinCloseDeal": 0,
                    "allContacts": [
                        "sachin@relatas.com"
                    ],
                    "companies": [],
                    "products": [],
                    "verticals": [],
                    "regions": [],
                    "bus": [],
                    "monthlyCreated": [],
                    "monthlyClosed": [],
                    "monthlyOppWon": [],
                    "contactsByRelation": {
                        "influencers": [],
                        "dms": [],
                        "partners": [],
                        "owners": [
                            "sachin@relatas.com"
                        ]
                    },
                    "commits": 2000,
                    "targetsCount": 0,
                    "targets": []
                }
            ],
            "allQuarters": {
                "start": "2019-04-01T00:00:00+05:30",
                "end": "2019-06-30T23:59:59+05:30"
            },
            "err": null
        }
    }
