relatasApp.controller("allCrossFilters", function($scope, $http, share,$rootScope) {
    share.steps = [{ "name": "user", "label": "User" },
        { "name": "accounts", "label": "Accounts" },
        { "name": "products", "label": "Products" },
        { "name": "location", "label": "Locations" }];

    // getCrossFilterData2(function(data) {
    //     initMap(share,$scope,data);
    // });

    parallelCoordinates();

    $scope.filterList = [{
        name:"User",
        selected:true
    },{
        name:"Accounts",
        selected:true
    },{
        name:"Products",
        selected:true
    },{
        name:"Locations",
        selected:true
    },{
        name:"Verticals",
        selected:false
    },{
        name:"Quarter",
        selected:true
    },{
        name:"Type",
        selected:true
    },{
        name:"Source",
        selected:true
    }];

    $scope.addToFilter = function () {
        $scope.showDropDown = !$scope.showDropDown;
    }
});

function initMap(share,$scope,data) {

    var margin = {top: 1, right: 1, bottom: 6, left: 1},
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

    var colorScheme = {
        "user":"#2499c9",
        "product":"#5fb6a6",
        "account": "#e74c3c", //"#47b758",
        "location":"#8ECECB",
    }

    var formatNumber = d3.format(",.0f"),
        format = function(d) { return formatNumber(d) + "  Opps"; },
        color = d3.scale.category20();

    var svg = d3.select("#flow").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var sankey = d3.sankey()
        .nodeWidth(15)
        .nodePadding(10)
        .size([width, height]);

    var path = sankey.link();
    var tooltip = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    renderLabels(svg,margin,width,height,share);

    sankey
        .nodes(data.nodes)
        .links(data.links)
        .layout(32);

    var link = svg.append("g").selectAll(".link")
        .data(data.links)
        .enter().append("path")
        .attr("class", "link")
        .attr("d", path)
        .style("stroke-width", function(d) { return Math.max(1, d.dy); })
        .sort(function(a, b) { return b.dy - a.dy; })
        // .on("mouseover", function(d) {
        //     var wrapperStart = "<div class='tooltip-wrapper'>";
        //     var wrapperEnd = "</div>";
        //
        //     var source = d.source.name;
        //     var target = d.target.name;
        //
        //     var html = wrapperStart+source+target+wrapperEnd;
        //     tooltip.transition()
        //         .duration(200)
        //         .style("opacity", .99);
        //     tooltip	.html(html)
        //         .style("left", (d3.event.pageX - 50) + "px")
        //         .style("top", (d3.event.pageY - 55) + "px");
        // })
        // .on("mouseout", function(d) {
        //     tooltip.transition()
        //         .duration(500)
        //         .style("opacity", 0);
        // });

    link.append("title")
        .text(function(d) { return d.source.name + " → " + d.target.name + "\n" + format(d.value); });

    var node = svg.append("g").selectAll(".node")
        .data(data.nodes)
        .enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
        .call(d3.behavior.drag()
            .origin(function(d) { return d; })
            .on("dragstart", function() { this.parentNode.appendChild(this); })
            .on("drag", dragmove));

    node.append("rect")
        .attr("height", function(d) { return d.dy; })
        .attr("width", 3)
        .attr("x", function(d) { return d.dx; })
        .style("fill", function(d) {
            return d.color = colorScheme[d.type];
        })
        .style("stroke", function(d) {
            return colorScheme[d.type];
        })
        .append("title")
        .text(function(d) { return d.name + "\n" + format(d.value); });

    node.append("text")
        .attr("x", -6)
        .attr("y", function(d) { return d.dy / 2; })
        .attr("dy", ".35em")
        .attr("text-anchor", "end")
        .attr("transform", null)
        .text(function(d) { return d.name; })
        .filter(function(d) { return d.x < width / 2; })
        .attr("x", 6 + sankey.nodeWidth())
        .attr("text-anchor", "start");

    function dragmove(d) {
        d3.select(this).attr("transform", "translate(" + d.x + "," + (d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))) + ")");
        sankey.relayout();
        link.attr("d", path);
    }
}

function getCrossFilterData2(callback) {

    var a = {
        "nodes": [
        {
            "name": "John Doe",
            type: "user"
        },{
            "name": "Samantha Jones",
                type: "user"
        },
        {
            "name": "Infosys",
            type: "account"
        },
        {
            "name": "Wipro",
            type: "account"
        },
        {
            "name": "ICICI",
            type: "account"
        },
        {
            "name": "CRM",
            type: "product"
        },
        {
            "name": "ArTracker",
            type: "product"
        },
        {
            "name": "Bangalore",
            type: "location"
        },
        {
            "name": "Chennai",
            type: "location"
        },
        {
            "name": "Hyderabad",
            type: "location"
        },
        {
            "name": "Delhi",
            type: "location"
        }
    ],
        "links": [
            {
                "source": 0,
                "target": 2,
                "value": 30
            },{
                "source": 1,
                "target": 3,
                "value": 50
            },{
                "source": 1,
                "target": 4,
                "value": 20
            },{
                "source": 0,
                "target": 4,
                "value": 20
            },{
                "source": 2,
                "target": 5,
                "value": 10
            },{
                "source": 2,
                "target": 6,
                "value": 20
            },{
                "source": 3,
                "target": 6,
                "value": 22
            },{
                "source": 3,
                "target": 5,
                "value": 28
            },{
                "source": 4,
                "target": 6,
                "value": 17
            },{
                "source": 4,
                "target": 5,
                "value": 23
            },{
                "source": 5,
                "target": 7,
                "value": 41
            },{
                "source": 5,
                "target": 8,
                "value": 20
            },{
                "source": 6,
                "target": 8,
                "value": 20
            },{
                "source": 6,
                "target": 9,
                "value": 20
            },{
                "source": 6,
                "target": 10,
                "value": 19
            }]
    };

    callback(a)
}

function getCrossFilterData1(callback) {

    var a = {
        "nodes": [
        {
            "name": "John Doe"
        },{
            "name": "Samantha Jones"
        },
        {
            "name": "CRM"
        },
        {
            "name": "ArTracker"
        },
        {
            "name": "Bangalore"
        },
        {
            "name": "Chennai"
        },
        {
            "name": "Hyderabad"
        },
        {
            "name": "No Region"
        }
    ],
        "links": [
            {
                "name": "John Doe",
                "source": 0,
                "target": 2,
                "value": 30
            },{
                "name": "Samantha Jones",
                "source": 1,
                "target": 3,
                "value": 50
            },{
                "name": "artracker",
                "source": 3,
                "target": 6,
                "value": 20
            },{
                "name": "artracker",
                "source": 3,
                "target": 7,
                "value": 20
            },{
                "name": "artracker",
                "source": 2,
                "target": 7,
                "value": 5
            },{
                "name": "artracker",
                "source": 2,
                "target": 6,
                "value": 10
            },{
                "name": "crm",
                "source": 2,
                "target": 4,
                "value": 15
            },{
                "name": "artracker",
                "source": 3,
                "target": 6,
                "value": 10
            }]
    };

    callback(a)
}

function renderLabels(svg,margins,width,height,share) {

    let steps = share.steps;

    let barsGroup = svg.append("g");
    let bars = barsGroup.selectAll('.label')
        .data(steps);

    bars
        .enter()
        .append('g')
        .attr('class', 'label');

    bars
        .append('rect')
        .attr('class', 'bar')
        .attr('height', function(d) {
            return height;
        });

    bars
        .append("text")
        .attr("dy", ".75em")
        .attr("transform", null);

    // Enter + Update
    bars
        .select('.bar')
        .style('fill', 'transparent')
        .transition()
        .duration(150)
        .attr('width', function(d, i) {
            return width / steps.length;
        })
        .attr('x', function(d, i) {
            return width / steps.length * i;
        });

    bars
        .select('text')
        .transition()
        .duration(50)
        .attr('y', -margins.top)
        .attr('x', function(d, i) {
            if(i === 0){
                return 25;
            } else {
                return 175 * i;
            }
        })
        .attr("text-anchor", function(d, i) {
            if (steps.length < 4) {
                return "middle";
            }
            else {
                return "end";
            }
        })
        .style('font-weight', 'bold')
        .style('font-size', '12px')
        .text(function(d) {
            return d.label;
        });

    // Exit
    bars.exit().remove();

    return bars;
}

function parallelCoordinates(){
    var margin = {top: 50, right: 50, bottom: 50, left: 50},
        width = 960 - margin.left - margin.right,
        height = 500 - margin.top - margin.bottom;

    var tooltip = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    var dimensions = [
        {
            name: "Team Members",
            scale: d3.scale.ordinal().rangePoints([0, height]),
            type: "string"
        },
        {
            name: "Quarter",
            scale: d3.scale.ordinal().rangePoints([0, height]),
            type: "string"
        },
        {
            name: "Accounts",
            scale: d3.scale.ordinal().rangePoints([0, height]),
            type: "string"
        },
        {
            name: "Products",
            scale: d3.scale.ordinal().rangePoints([0, height]),
            type: "string"
        },
        {
            name: "Locations",
            scale: d3.scale.ordinal().rangePoints([0, height]),
            type: "string"
        },
        {
            name: "Type",
            scale: d3.scale.ordinal().rangePoints([0, height]),
            type: "string"
        },
        {
            name: "Source",
            scale: d3.scale.ordinal().rangePoints([0, height]),
            type: "string"
        }
    ];

    var x = d3.scale.ordinal().domain(dimensions.map(function(d) { return d.name; })).rangePoints([0, width]),
        y = {},
        dragging = {};

    var line = d3.svg.line(),
        axis = d3.svg.axis().orient("left"),
        background,
        foreground;

    var svg = d3.select("#flow").append("svg")
        // .attr("width", width + margin.left + margin.right+100)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    getPCOData(function(error, data) {

        //Create the dimensions depending on attribute "type" (number|string)
        //The x-scale calculates the position by attribute dimensions[x].name
        dimensions.forEach(function(dimension) {
            dimension.scale.domain(dimension.type === "number"
                ? d3.extent(data, function(d) { return +d[dimension.name]; })
                : data.map(function(d) { return d[dimension.name]; }).sort());
        });

        // Add grey background lines for context.
        background = svg.append("g")
            .attr("class", "background")
            .selectAll("path")
            .data(data)
            .enter().append("path")
            .attr("d", path);

        // Add blue foreground lines for focus.
        foreground = svg.append("g")
            .attr("class", "foreground")
            .selectAll("path")
            .data(data)
            .enter().append("path")
            .attr("d", path)
            .on("mouseover", function(d) {
                var html = getToolTip2(d)

                d3.select(this).transition().duration(100)
                    .style({'stroke' : '#e65b32'});

                tooltip.transition()
                    .duration(200)
                    .style("opacity", .99);
                tooltip	.html(html)
                    .style("left", (d3.event.pageX - 50) + "px")
                    .style("top", (d3.event.pageY - 55) + "px");
            })
            .on("mouseout", function(d) {
                d3.select(this).transition().duration(100)
                    .style({'stroke': '#2499c9' })

                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            })
            .on("mousemove", function(){return tooltip.style("top", (event.pageY-10)+"px").style("left",(event.pageX+10)+"px");});

        // Add a group element for each dimension.
        var g = svg.selectAll(".dimension")
            .data(dimensions)
            .enter().append("g")
            .attr("class", "dimension")
            .attr("transform", function(d) { return "translate(" + x(d.name) + ")"; })
            .call(d3.behavior.drag()
                .origin(function(d) { return {x: x(d.name)}; })
                .on("dragstart", function(d) {
                    dragging[d.name] = x(d.name);
                    background.attr("visibility", "hidden");
                })
                .on("drag", function(d) {
                    dragging[d.name] = Math.min(width, Math.max(0, d3.event.x));
                    foreground.attr("d", path);
                    dimensions.sort(function(a, b) { return position(a) - position(b); });
                    x.domain(dimensions.map(function(d) { return d.name; }));
                    g.attr("transform", function(d) { return "translate(" + position(d) + ")"; })
                })
                .on("dragend", function(d) {
                    delete dragging[d.name];
                    transition(d3.select(this)).attr("transform", "translate(" + x(d.name) + ")");
                    transition(foreground).attr("d", path);
                    background
                        .attr("d", path)
                        .transition()
                        .delay(500)
                        .duration(0)
                        .attr("visibility", null);
                })
            );

        // Add an axis and title.
        g.append("g")
            .attr("class", "axis")
            .each(function(d) { d3.select(this).call(axis.scale(d.scale)); })
            .append("text")
            .style("text-anchor", "middle")
            .attr("class", "axis-label")
            .attr("y", -20)
            .text(function(d) {return d.name;});

        // Add and store a brush for each axis.
        g.append("g")
            .attr("class", "brush")
            .each(function(d) {
                d3.select(this).call(d.scale.brush = d3.svg.brush().y(d.scale).on("brushstart", brushstart).on("brush", brush));
            })
            .selectAll("rect")
            .attr("x", -8)
            .attr("width", 16);
    });

    function position(d) {
        var v = dragging[d.name];
        return v == null ? x(d.name) : v;
    }

    function transition(g) {
        return g.transition().duration(500);
    }

    // Returns the path for a given data point.
    function path(d) {
        //return line(dimensions.map(function(p) { return [position(p), y[p](d[p])]; }));
        return line(dimensions.map(function(dimension) {
            var v = dragging[dimension.name];
            var tx = v == null ? x(dimension.name) : v;
            return [tx, dimension.scale(d[dimension.name])];
        }));
    }

    function brushstart() {
        d3.event.sourceEvent.stopPropagation();
    }

    // Handles a brush event, toggling the display of foreground lines.
    function brush() {
        var actives = dimensions.filter(function(p) { return !p.scale.brush.empty(); }),
            extents = actives.map(function(p) { return p.scale.brush.extent(); });

        foreground.style("display", function(d) {
            return actives.every(function(p, i) {
                if(p.type==="number"){
                    return extents[i][0] <= parseFloat(d[p.name]) && parseFloat(d[p.name]) <= extents[i][1];
                }else{
                    return extents[i][0] <= p.scale(d[p.name]) && p.scale(d[p.name]) <= extents[i][1];
                }
            }) ? null : "none";
        });
    }
}

function getPCOData(callback){
    var data = [
        {
            "Team Members": "John Doe",
            "Accounts": "Wipro",
            "Products": "CRM",
            "Locations":"Bangalore",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },{
            "Team Members": "John Doe",
            "Accounts": "Wipro",
            "Products": "CRM",
            "Locations":"Bangalore",
            "Quarter":"Q1 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "John Doe",
            "Accounts": "Infosys",
            "Products": "CRM",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Kabir Khan",
            "Accounts": "Infosys",
            "Products": "CRM",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Accenture",
            "Products": "CRM",
            "Locations":"Bangalore",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Jonny Walker",
            "Accounts": "Accenture",
            "Products": "CRM",
            "Locations":"Bangalore",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Wipro",
            "Products": "A10",
            "Locations":"Bangalore",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Samantha Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Kabir Khan",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Delhi",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Kabir Khan",
            "Accounts": "Infosys",
            "Products": "CRM",
            "Locations":"Bangalore",
            "Quarter":"Q2 2018",
            "Type": "New",
            "Source": "Customer Referral"
        },
        {
            "Team Members": "Kabir Khan",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q1 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Jonny Walker",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q1 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Neil Jones",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q1 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Bill Doily",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q1 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Naveen Paul",
            "Accounts": "Accenture",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q1 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Rahul J",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q1 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Sumit Rampal",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q1 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Bill Doily",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q1 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Bill Doily",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q2 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Naveen Paul",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q2 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Rahul J",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q2 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Sumit Rampal",
            "Accounts": "Wipro",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q2 2018",
            "Type": "Renewal",
            "Source": "Social Media"
        },
        {
            "Team Members": "Bill Doily",
            "Accounts": "Infosys",
            "Products": "ArTracker",
            "Locations":"Hyderabad",
            "Quarter": "Q2 2018",
            "Type": "Renewal",
            "Source": "Events"
        },
        {
            "Team Members": "Venu C",
            "Accounts": "Infosys",
            "Products": "A10",
            "Locations":"Hyderabad",
            "Quarter": "Q2 2018",
            "Type": "Renewal",
            "Source": "Events"
        },
        {
            "Team Members": "Rajiv Rajan",
            "Accounts": "Infosys",
            "Products": "CRM",
            "Locations":"Delhi",
            "Quarter": "Q2 2018",
            "Type": "Renewal",
            "Source": "Events"
        }
    ]

    callback(null,data);
}

function getToolTip2(data) {

    var wrapperStart = "<div class='tooltip-wrapper'>";
    var wrapperEnd = "</div>";
    var body = "";
    var table = "";

    for(var key in data){
        var val = data[key];
        var str = key.replace(/s\s*$/, "");
        body = body+"<p>"+str+" - "+val+"</p>"
        table = "<table><tr>" +
            "<td>"+val+"</td>" +
            "<td>"+str+"</td>" +
            "</tr></table>"
    }

    return wrapperStart+body+wrapperEnd;
}