
$(document).ready(function() {
   var companyTable;
    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    applyDataTableCompany();
    getCompanyList();
    function getCompanyList(){
        $.ajax({
            url:'/corporate/admin/getCompanyList',
            type:'GET',
            datatype:'JSON',
            success:function(companyList){
                if(checkRequired(companyTable)){
                    companyTable.clear().draw();
                }
                displayCompanyList(companyList);
            }
        })
    }

    function displayCompanyList(companies){
        console.log(companies)
        if(companies && checkRequired(companies) && companies.length > 0){
            for(var company=0; company<companies.length; company++){
                var adminId =  companies[company]._id.admin.length > 0 ? companies[company]._id.admin[0].adminId : null;
                var logFile = '/admin/corporate/changeAdmin/log';
                addRowTableCompany([
                    companies[company]._id.companyName,
                    companies[company]._id._id,
                    getAdminOptionList(companies[company].users,companies[company]._id._id,adminId),
                    companies[company]._id.admin.length > 0 ? companies[company]._id.admin[0].adminId : '',
                    getEventDateFormat(companies[company]._id.admin.length > 0 ? companies[company]._id.admin[0].addedOn:''),
                    '<a target="_blank" href="/admin/corporate/changeAdmin/log">log</a>',
                    '<a target="_blank" href="/admin/corporate/hierarchy/'+companies[company]._id._id+'/'+companies[company]._id.companyName+'">view</a>'
                ])
            }
        }
    }
    function getEventDateFormat(date){

        if(checkRequired(date) && date != ''){
            date = new Date(date)
            var day = date.getDate();
            var format = getVakidNumber(day)+'-'+monthNameSmall[date.getMonth()]+'-'+date.getFullYear();
            return format;
        }else return ''

    }
    function getVakidNumber(num){
        if(num < 10){
            return '0'+num;
        }else return num;
    }
    function getAdminOptionList(userList,companyId){

        var optionsHtml = '';
        var admin = null;
        if(userList && userList.length > 0){
            for(var user=0; user<userList.length; user++){

                var names = userList[user].firstName+' '+userList[user].lastName;
                var val = userList[user]._id+'_'+companyId; // userId_companyId

                if(userList[user].corporateAdmin){
                    admin = '<option value='+val+'>'+names+'</option>';
                }
                else{
                    optionsHtml += '<option value='+val+'>'+names+'</option>';
                }
            }
        }
        if(admin != null){
            optionsHtml = admin+''+optionsHtml;
        }

        if(admin == null){
            optionsHtml = '<select class="assign-admin"><option value="none_none">none</option>'+optionsHtml+'</select>';
        }
        else{
            optionsHtml = '<select class="assign-admin">'+optionsHtml+'</select>';
        }
        return optionsHtml;
    }

    $("body").on("change",".assign-admin",function(){
        var user_company = $(this).val();
        user_company = user_company.split('_');
        var obj = {
            userId:user_company[0],
            companyId:user_company[1]
        }
        updateCompanyAdmin(obj);
    });

    function updateCompanyAdmin(obj){
        $.ajax({
            url:'/corporate/admin/updateAdmin',
            type:'POST',
            datatype:'JSON',
            data:obj,
            success:function(response){

                if(response){
                    alert('Company admin successfully updated');
                }else{
                    alert('Failed. Please try again.');
                }
                getCompanyList();
            }
        })
    }


    function addRowTableCompany(rowArr){
        companyTable.row.add( rowArr ).draw();
    }


    function applyDataTableCompany(){
        companyTable = $('#company-table').DataTable({
            "dom": '<"top"iflp<"clear">>',
            "order": [[ 1, "asc" ]],

            "oLanguage": {
                "sEmptyTable": "No Members Found"
            }
        });
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }
});