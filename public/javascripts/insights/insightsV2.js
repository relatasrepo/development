
// var relatasApp = angular.module('relatasApp', ['ngRoute', 'ngSanitize', 'angular-loading-bar','ngLodash']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
var relatasApp = angular.module('relatasApp', ['ngRoute', 'angular-loading-bar','ngLodash']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.controller("header_controller", function($scope) {
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent, yourNetwork, extendedNetwork, forCompanies) {

        var str = searchContent;

        if (typeof str == 'string' && str.length > 0) {
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
        } else toastr.error("Please enter search content")
    };
});

var timezone;

function getParams(url) {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while (match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var contextEmailId = getParams(window.location.href).context;

var showContext = true;

if (!checkRequired(contextEmailId)) {
    showContext = false;
}

relatasApp.service('share', function() {
    return {
        setCompanyId:function(companyId){
            this.companyId = companyId;
        },
        setTown:function(geoLocationTown){
            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        },
        setLiu:function(liu){
            this.liuData = liu;
        }
    }
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("logedinUser", function($scope, $http, share,$rootScope) {

    getLiuProfile($scope, $http, share,$rootScope,function (response) {

        $(".seeMoreIcon").addClass("disabled-see-more")
        if (response.SuccessCode) {

            $scope.firstName = response.Data.firstName;
            $scope.profilePicUrl = '/getImage/'+response.Data._id+'/'+new Date().toISOString()

            $rootScope.orgHead = response.Data.orgHead

            var regionOwner = response.Data.regionOwner
            var verticalOwner = response.Data.verticalOwner
            var productTypeOwner = response.Data.productTypeOwner

            $rootScope.hasExcpAccess = false;

            if(response.Data.regionOwner.length>0 || response.Data.productTypeOwner.length>0 || response.Data.verticalOwner.length>0){
                $rootScope.hasExcpAccess = true;
            }

            var regions = response.Data.regionOwner.length>0?"Regions: "+regionOwner.join(',')+" \n": ''
            var products = response.Data.productTypeOwner.length>0?"Products: "+productTypeOwner.join(',')+" \n": ''
            var verticals = response.Data.verticalOwner.length>0?"Verticals: "+verticalOwner.join(',')+" \n": ''

            $rootScope.accessHelpTxt = "You have access to view  \n"+regions+products+verticals;
            $rootScope.accessControl = false;

            share.companyDetails_fn(response.companyDetails)
            share.liu(response.Data)
            share.setLiu(response.Data)

            $rootScope.currency = 'USD';
            share.primaryCurrency = "USD";
            share.currenciesObj = {};

            if(response.companyDetails && response.companyDetails.currency){
                _.each(response.companyDetails.currency,function (el) {
                    if(el.isPrimary){
                        $rootScope.currency = el.symbol
                        share.primaryCurrency = el.symbol
                    }

                    share.currenciesObj[el.symbol] = el;
                })
            }

            share.setCompanyId(response.Data.companyId)
        } else {

        }
    })

    $scope.getTeam = function () {

        $http.get('/company/getTeamMembers')
            .success(function (response) {
                $scope.team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.team = buildTeamProfiles(response.Data)
                    $scope.selection = $scope.team[0];

                    var usersDictionary = {};

                    if($scope.team.length>0){
                        _.each($scope.team,function (member) {
                            usersDictionary[member.emailId] = member;
                        });
                    }

                    function checkLiuData(){
                        setTimeOutCallback(1000,function () {
                            if(share.liuData){
                                share.usersDictionary = _.cloneDeep(usersDictionary);
                                share.liuData.fullName = share.liuData.firstName +" "+share.liuData.lastName
                                share.liuData.image = "/getImage/"+share.liuData.userId;
                                share.usersDictionary[share.liuData.emailId] = share.liuData

                            } else {
                                checkLiuData()

                            }
                        })
                    }

                    checkLiuData()

                    share.getDealsClosingTeam(response.Data);
                    share.setTeamMembers(usersDictionary,response.Data);
                }
            });
    }

    $scope.getTeam();
});

relatasApp.controller("team_hierarchy",function ($scope,$http,$rootScope,share) {

    $scope.getDataFor = function (member) {

        if(share.liuData){
            if(share.liuData._id == member.userId){
                $rootScope.noAccess = false;
            } else {
                $rootScope.noAccess = true;
            }
        } else {
            $rootScope.noAccess = true;
        }

        $scope.selection = member
        share.forUser(member.userId);
        share.forLT(member.userId);
        share.forDealsAtRisk(member.userId);
        share.forMonthlyTargets(member.userId);
        share.forOppGrowth(member.userId);
        share.forAccsCreated(member.userId);
        share.forSnapshot(member.userId);
        share.forPipelineVelocity(member.userId);

        share.refreshNumberAllGrowth()
    }

    $scope.getLiuHierarchy = function () {

        $http.get('/company/user/hierarchy')
            .success(function (response) {
                $scope.team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.team = buildTeamProfiles(response.Data)
                    $scope.selection = $scope.team[0];

                    var usersDictionary = {};

                    if(response.companyMembers.length>0){
                        var companyMembers = buildTeamProfiles(response.companyMembers)

                        _.each(companyMembers,function (member) {
                            usersDictionary[member.emailId] = member
                        })
                    }
                }
            });
    }

    $scope.getLiuHierarchy();

    $scope.getOpportunityWithAccess = function (liu) {

        if(!liu){
            $rootScope.viewingExceptionalAccess = true;

            share.forUser($scope.selection.userId,'accessControl');
            share.forLT($scope.selection.userId,'accessControl');
            share.forDealsAtRisk($scope.selection.userId,'accessControl');
            share.forMonthlyTargets($scope.selection.userId,'accessControl');
            share.forOppGrowth($scope.selection.userId,'accessControl');
            share.forAccsCreated($scope.selection.userId,'accessControl');
            share.forSnapshot($scope.selection.userId,'accessControl');
            share.forPipelineVelocity($scope.selection.userId,'accessControl');

        } else {
            $rootScope.viewingExceptionalAccess = false;
            $scope.getDataFor($scope.selection)
        }
    }

    closeAllDropDownsAndModals($scope,".list");
});

relatasApp.controller("insights_stats",function ($scope,$http,lodash) {

    $http.get("/insights/current/month/stats")
        .success(function (response) {
            drawMonthlyStats($scope,response.values)
        });
});

relatasApp.controller("opportunitiesByStage",function ($scope,$http,$rootScope,share) {

    share.refreshPipelineSnaphot = function () {
        getPipelineSnapshot();
    }

    share.forSnapshot = function (userId,accessControl) {
        getPipelineSnapshot(userId,accessControl)
    }

    function getPipelineSnapshot(userId,accessControl) {
        var url = "/opportunities/group/count"
        if(userId){
            url = url+"?userIds="+userId;
        }

        if(accessControl){
            url = fetchUrlWithParameter(url+"&accessControl="+true)
        }

        $http.get(url)
            .success(function (response) {

                var oppExist = false;
                if(response && response.SuccessCode && response.Data.length>0){
                    if(response.fy){
                        $scope.fiscalYear = moment(response.fy.fromDate).format("MMM YY")+" - "+moment(response.fy.toDate).format("MMM YY")
                    }

                    oppExist = true;

                    var maxCount = _.max(_.map(response.Data,"count"))
                    var minCount = _.min(_.map(response.Data,"count"))
                    var maxAmount = _.max(_.map(response.Data,"totalAmount"))
                    var minAmount = _.min(_.map(response.Data,"totalAmount"))

                    $scope.prospectColorLeft = "white";
                    $scope.EvaluationColorLeft = "white";
                    $scope.proposalColorLeft = "white";
                    $scope.wonColorLeft = "white";
                    $scope.lostColorLeft = "white";

                    $scope.prospectColor = "white";
                    $scope.EvaluationColor = "white";
                    $scope.proposalColor = "white";
                    $scope.wonColor = "white";
                    $scope.lostColor = "white";

                    $scope.funnels = [];
                    var stagesWithData = [];

                    function getPipelineFunnel(){
                        if($rootScope.stages){

                            _.each(response.Data,function (el) {

                                _.each($rootScope.stages,function (st) {

                                    if(el._id == st){
                                        stagesWithData.push(st);
                                        $scope.funnels.push({
                                            name:st,
                                            countLength:{'width':scaleBetween(el.count, minCount, maxCount)+'%'},
                                            amountLength:{'width':scaleBetween(el.totalAmount, minAmount, maxAmount)+'%'},
                                            amount:el.totalAmount.r_formatNumber(2),
                                            oppsCount:el.count
                                        })
                                    }
                                })
                            });

                            var noDataStages = _.difference($rootScope.stages,stagesWithData)

                            _.each(noDataStages,function (st) {
                                $scope.funnels.push({
                                    name:st,
                                    countLength:{'width':0+'%'},
                                    amountLength:{'width':0+'%'},
                                    amount:0,
                                    oppsCount:0
                                })
                            })

                            _.each($scope.funnels,function (fl) {
                                _.each(share.opportunityStages,function (st) {
                                    if(fl.name == st.name){
                                        fl.order = st.order
                                    }
                                })
                            });

                            $scope.funnels = _.sortBy($scope.funnels,function (o) {
                                return o.order
                            })

                        } else {
                            setTimeOutCallback(1000,function () {
                               getPipelineFunnel()
                            });
                        }
                    }

                    getPipelineFunnel()
                }

                $scope.noPipeline = oppExist;
                share.oppExist(oppExist)
            });
    }

    getPipelineSnapshot()

});

relatasApp.controller("deals_closing_soon",function ($scope,$http,share,$rootScope,searchService) {

    share.teamClosing30Days = function (response) {
        $scope.cache_teamDealsClosing30Days = response
    }

    $scope.getContactProfile = function (item) {
        item.emailId = item.contactEmailId;
        getContactProfileInfoFromDb($http,item)
    }

    $scope.oppUpdateStage = function (stage) {

        updateOpportunity($scope,$http,$scope.opp,"stageName",stage,false,function (result) {
            toastr.success("Stage updated successfully");
            $scope.isStagnant = "fa-check-circle-o";
            $scope.stagnantStatus = true;
            $scope.stagnantDaysAgo = "Stage was last updated today";
            share.refreshDealsAtRisk();
            share.refreshPipelineSnaphot();
        });
    }

    function getDealsClosingSoon(userId,accessControl){
        setTimeOutCallback(1000,function (){

            var url = "/insights/deals/closing/soon";
            if(userId){
                url = url+"?userIds="+userId;
            }

            if(accessControl){

                $scope.oppClosing30Days = $scope.cache_teamDealsClosing30Days.Data.length>0;

                donutDraw($scope,share,[{meta:"Other open opportunities",value:$scope.cache_teamDealsClosing30Days.oppsAll},{meta:"Closing in 30 days",value:$scope.cache_teamDealsClosing30Days.Data.length}],['Other open opportunities', 'This month'],'.open-deals');
                drawDealsClosingSoon($scope,share,$scope.cache_teamDealsClosing30Days.Data)
                share.closingSoonNumbers($scope.cache_teamDealsClosing30Days.oppsAll,$scope.cache_teamDealsClosing30Days.Data.length)

            } else {

                $http.get(url)
                    .success(function (response) {
                        if(response && response.SuccessCode){

                            $scope.oppClosing30Days = response.Data.length>0;

                            donutDraw($scope,share,[{meta:"Other open opportunities",value:response.oppsAll},{meta:"Closing in 30 days",value:response.Data.length}],['Other open opportunities', 'This month'],'.open-deals');
                            drawDealsClosingSoon($scope,share,response.Data)
                            share.closingSoonNumbers(response.oppsAll,response.Data.length)
                        }
                    });
            }
        })
    }

    share.getDealsClosing30Days = function () {
        getDealsClosingSoon();
    }

    share.forUser = function (userId,accessControl) {
        getDealsClosingSoon(userId,accessControl)
    }

    share.refreshClosingSoonDeals = function () {
        getDealsClosingSoon();
    }

    share.showClosingSoon = function(value){
        $scope.showClosingSoon = value;
    }

    $scope.sortType = 'closeDate';
    $scope.sortReverse = false;

    $scope.openOppBox = function () {
        share.openOppBox(true);
    }
    
    $scope.showOpportunity = function (deal) {

        if(!$rootScope.noAccess){

            $scope.openDMs(); // Default open DMs

            $scope.dmActiveClass = "active"
            $scope.iActiveClass = "inactive"
            $scope.stagActiveClass = "inactive"
            $scope.staleActiveClass = "inactive"
            $scope.ciActiveClass = "inactive"
            $scope.mActiveClass = "inactive"
            $scope.compIActiveClass = "inactive"
            $scope.ltActiveClass = "inactive"

            $scope.totalValue = $scope.totalPipeLineValue?numberWithCommas($scope.totalPipeLineValue.r_formatNumber(2),share.primaryCurrency == "INR"):"";

            var percentageAtRisk = calculatePercentage(deal.amount,$scope.totalPipeLineValue);

            if(percentageAtRisk == 0){
                $scope.percentageOfTotal = "< 1% of "
            } else {
                $scope.percentageOfTotal = percentageAtRisk+"% of "
            }

            if(!$scope.totalPipeLineValue || !deal.amount){
                $scope.percentageOfTotal = false;
            }

            $scope.dmExist = "fa-exclamation-circle";
            $scope.InfExist = "fa-exclamation-circle";
            $scope.isStaleOpp = "fa-exclamation-circle";
            $scope.isStagnant = "fa-exclamation-circle";
            $scope.metDmInfl = "fa-exclamation-circle";
            $scope.IntScr = "fa-exclamation-circle";
            $scope.ltWithOwner = "fa-exclamation-circle";
            $scope.contatIntr = "fa-exclamation-circle";
            $scope.oppCreatedDate = deal.createdDate?moment(deal.createdDate).format("DD MMM YYYY"):'';

            if(share.usersDictionary[deal.userEmailId]){
                deal.owner = share.usersDictionary[deal.userEmailId]
            }

            if(!deal.ltWithOwner){
                $scope.ltWithOwner = "fa-check-circle-o"
                $scope.ltTrue = false;
            } else {
                $scope.ltTrue = true;
            }

            if(!deal.skewedTwoWayInteractions){
                $scope.contatIntr = "fa-check-circle-o"
            }

            if(deal.daysSinceStageUpdated<45){
                $scope.isStagnant = "fa-check-circle-o"
                $scope.stagnantStatus = true;
            } else {
                $scope.stagnantStatus = false;
            }

            $scope.stagnantDaysAgo = "Stage was last updated "+deal.daysSinceStageUpdated + " days back";

            if(deal.daysSinceStageUpdated == 0){
                $scope.stagnantDaysAgo = "Stage was last updated today";
            }

            $scope.metDecisionMaker_infuencer = false;
            if(deal.metDecisionMaker_infuencer){
                $scope.metDecisionMaker_infuencer = true;
                $scope.metDmInfl = "fa-check-circle-o"
            }

            $scope.companyIntr = false;

            if(deal.averageInteractionsPerDeal){
                $scope.companyIntr = true;
                $scope.IntScr = "fa-check-circle-o"
            }

            $scope.opportunityName = deal.opportunityName;
            $scope.contactEmailId = deal.contactEmailId;

            $scope.closeDate = moment(deal.closeDate).format("DD MMM YYYY");

            if(deal.decisionMakersExist || deal.decisionMakers.length>0){
                $scope.dmExist = "fa-check-circle-o"
                $scope.noDMs = false;
                deal.decisionMakersExist = true
            }

            if(deal.influencersExist || deal.influencers.length>0){
                $scope.InfExist = "fa-check-circle-o"
                $scope.noInfl = false;
                deal.influencersExist = true
            }

            $scope.opp = deal;

            $scope.showModal = true;
            $scope.noDMs = !deal.decisionMakersExist;
            $scope.noInfl = !deal.influencersExist;

            if(new Date(deal.closeDate)< new Date()){
                $scope.staleOpp = true;
            } else {
                $scope.staleOpp = false
                $scope.isStaleOpp = "fa-check-circle-o"
            }

            $scope.stageUpdated = deal.stageName;

            getLiu();

            getTweet($scope,$http,deal.contactEmailId);
        }
    }

    $scope.reactToTweet = function (item,status,action) {
        reactToTweet($http,item,status,action)
    };

    $scope.openDMs = function () {
        $scope.showStagOpp = false;
        $scope.showDms = true;
        $scope.showContInt = false;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "active"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"

    }

    $scope.openInfl = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showInfl = true;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "active"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $scope.openStaleOpp = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showStaleOpp = true;

        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "active"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $scope.openDmOrInfMet = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showMetDmInf = true;

        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "active"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"

        if($scope.opp.dmsInfls[0]){
            getLiu($scope.opp.dmsInfls[0]);
        }
    }

    $scope.openIntScore = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showIntScr = true;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "active"
        $scope.ltActiveClass = "inactive"

    }

    $scope.openLt = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showIntScr = false;
        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.closeThis = false;
        $scope.showLt = true;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "active"

    }

    $scope.openStagOpp = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = true;
        $scope.showStaleOpp = false;
        $scope.closeThis = false;
        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "active"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    };

    $scope.openContactIntr = function () {
        $scope.showContInt = true;
        $scope.showStagOpp = false;
        $scope.showStaleOpp = false;
        $scope.closeThis = false;
        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "active"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $(document).on('keyup',function(evt) {
        if (evt.keyCode == 27) {
            $scope.$apply(function (){
                $scope.showModal = false;
                resetSuggestions($scope)
            })
        }
    });

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.removeRecipient = function(contact,type){
        removeRecipient($scope,$http,contact,type)
    }

    $scope.closeModal = function () {
        $scope.showModal = false;
    }

    $scope.oppCloseLost = function () {
        updateOpportunity($scope,$http,$scope.opp,"closeDate",new Date(),true,function (result) {
            if(result){
                share.refreshClosingSoonDeals()
                share.refreshDealsAtRisk()
                share.refreshPipelineSnaphot()
                share.refreshTarget()
                $scope.closeModal();

                toastr.success("Opportunity closed.")
            } else {
                toastr.error("An error occurred while closing this opportunity. Please try later")
            }
        })
    }

    $scope.registerDatePickerId = function(){

        $('#opportunityCloseDateSelector3').datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    updateOpportunity($scope,$http,$scope.opp,"closeDate",dp,false,function (result) {
                        if(result){
                            share.refreshDealsAtRisk();
                            $scope.closeDate = moment(dp).format("DD MMM YYYY");
                            $scope.isStaleOpp = "fa-check-circle-o";
                            $scope.staleOpp = false;
                        } else {

                        }
                    })
                });
            }
        });
    }

    function getLiu(emailId) {

        if(share.liuData){

            var signature = "\n\n"+getSignature(share.liuData.firstName + ' '+ share.liuData.lastName,
                    share.liuData.designation,
                    share.liuData.companyName,
                    share.liuData.publicProfileUrl)

            var interactionsFor = emailId?emailId:$scope.contactEmailId;

            getLastInteractedDetailsMessage($scope,$http,interactionsFor,share.liuData.firstName,share.liuData.publicProfileUrl,function (message) {

                $scope.subject = message.subject;
                $scope.body = message.body+signature;

                $scope.subjectLt = message.subject;
                $scope.subjectCi = message.subject;
                $scope.bodyLt = "\n\n"+signature;

            });

        } else {
            setTimeOutCallback(100,function () {
                getLiu()
            });
        }
    }

    $scope.sendEmail = function (subject,body,reason,contactEmailId) {

        var contactDetails = {
            contactEmailId: contactEmailId?contactEmailId:$scope.opp.contactEmailId,
            personId:null,
            personName:null
        }

        sendEmail($scope,$http,subject,body,contactDetails,reason)
    }

})

relatasApp.controller("add_opp",function ($scope,$http,share,searchService,$rootScope) {

    autoInitGoogleLocationAPI(share);

    share.openOppBox = function () {
        $scope.isOppModalOpen = true;
    }

    share.companyDetails_fn = function (companyDetails) {
        $scope.companyDetails = companyDetails;
    }

    share.liu = function (liu) {
        $scope.liu = liu;
    }
    
    $scope.closeModal = function () {
        $scope.isOppModalOpen = false;
    }

    getOppStages();

    function getOppStages(){

        if(share.opportunityStages){
            $rootScope.stages = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(1000,function () {
                getOppStages()
            })
        }
    }

    $scope.registerDatePickerId = function(){

        if(!$scope.opp){
            $scope.opp = {}
        }

        $('#opportunityCloseDateSelector').datetimepicker({
            value:$scope.opp.closeDateFormatted,
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    $scope.opp.closeDate = moment(dp).tz(timezone).format();
                    $scope.opp.closeDateFormatted = moment(dp).tz(timezone).format("DD MMM YYYY");
                });
            }
        });
    }

    $(document).on('keyup',function(evt) {
        if (evt.keyCode == 27) {
            $scope.$apply(function (){
                $scope.isOppModalOpen = false;
            })
        }
    });

    $scope.addToReasonList = function (reason) {

        if(!$scope.opp.closeReasons || !$scope.opp.closeReasons[0]){
            $scope.opp.closeReasons = [];
        }
        var list = $scope.opp.closeReasons;
        list.push(reason)
        $scope.opp.closeReasons = _.uniq(list)
    }

    $scope.ifOppClose = function (stage) {
        if(_.includes(["Close Lost","Close Won"], stage) && (!$scope.opp.closeReasons || !$scope.opp.closeReasons.length>0)){
            $scope.reasonsRequired = true;
        }
    }

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.saveOpportunity =function () {

        if($scope.companyDetails.geoLocations && $scope.companyDetails.geoLocations.length>0){
            $scope.town = share.getTown();
        } else {
            $scope.town = ""
        }

        $scope.renewalAmount = $("#renewalAmount").val()

        $scope.opp && $scope.opp._id?saveExistingOpportunity($scope,$http,share,$scope.liu._id,$scope.cEmailId,$scope.cPhone,$scope.liu,$scope.contactDetails,$rootScope):addNewOpportunity($scope,$http,share,$scope.liu._id,$scope.cEmailId,$scope.cPhone,$scope.liu,$scope.contactDetails,$rootScope)
    }

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.removeRecipient = function(contact,type){
        removeRecipient($scope,$http,contact,type)
    }

    $scope.selectContact = function(contact){
        $scope.searchContent = contact.fullName + " ("+contact.emailId+")";
        $scope.newOppContact = contact;
        $scope.showResultscontact = false;
    }
})

relatasApp.controller("deals_at_risk",function ($scope,$http,share,searchService,$rootScope) {

    function getDealsAtRisk(userId,accessControl) {

        var url = "/insights/deals/at/risk"
        if(userId){
            url = url+"?userIds="+userId;
        }

        if(accessControl && !$rootScope.orgHead){
            url = fetchUrlWithParameter(url+"&accessControl="+true)
        }

        var deals = []
        var totalDeals = 0

        if(accessControl && $rootScope.orgHead) {

            _.each($scope.teamDealsAtRisk,function (el) {
                if(el.deals.length>0){
                    deals.push(el.deals)
                }
                totalDeals = totalDeals+el.totalDeals
            });

            deals = _.flatten(deals)
            dealsAtRiskGraph($scope,share,deals,null,accessControl,totalDeals)
        } else if(accessControl) {

            _.each($scope.teamDealsAtRisk,function (el) {
                if(el.deals.length>0){
                    var allRisks = _.map(el.deals,'riskMeter');

                    var maxRisk = _.max(allRisks);
                    var minRisk = _.min(allRisks);

                    _.each(el.deals,function (deal) {
                        var riskPerc = scaleBetween(deal.riskMeter,minRisk,maxRisk);

                        if(minRisk == maxRisk){
                            riskPerc = 100
                        }

                        var suggestion = "Opportunity at highest risk";

                        if(riskPerc > 70 && riskPerc < 90){
                            suggestion = "Opportunity at high risk";
                        }

                        if(riskPerc > 50 && riskPerc < 70){
                            suggestion = "Opportunity at medium risk";
                        }

                        if(riskPerc < 50){
                            suggestion = "Opportunity at low risk";
                        }

                        deal.riskSuggestion = suggestion;
                        deal.riskPercentage = riskPerc+'%';
                        deal.riskPercentageStyle = {
                            'width':riskPerc+'%'
                        }

                        if(deal.riskMeter >= parseFloat(el.averageRisk)){
                            deal.riskClass = 'risk-high'
                        } else {
                            deal.riskClass = 'risk-low'
                        }

                        if(riskPerc == 0){
                            deal.riskClass = "risk-low"
                        }

                        share.dealsWithRiskValue[deal.opportunityId] = deal;
                        if(_.includes(share.liuData.regionOwner,deal.geoLocation?deal.geoLocation.zone:null) &&
                            _.includes(share.liuData.productTypeOwner,deal.productType) &&
                            _.includes(share.liuData.verticalOwner,deal.vertical)){
                            deals.push(deal)
                        } else {

                        }
                    })
                }
                
                totalDeals = totalDeals+el.totalDeals
            });

            dealsAtRiskGraph($scope,share,deals,null,accessControl,totalDeals)

        }else {
            callApi(url)
        }

        function callApi(url) {

            $http.get(url)
                .success(function (response) {
                    share.getDealsClosing30Days();
                    if(response && response.deals){
                        dealsAtRiskGraph($scope,share,response.deals,response.averageRisk)
                    }
                });
        }
    }

    share.forDealsAtRisk = function (userId,accessControl) {
        getDealsAtRisk(userId,accessControl)
    }

    share.setTeamMembers = function (usersDictionary,usersArray) {

        var ids = _.map(usersArray,"_id");
        var url = "/insights/deals/at/risk/team/meta";
        url = fetchUrlWithParameter(url,'userIds',ids)

        $http.get(url)
            .success(function (response) {
                var team_atRisk = 0;
                $scope.teamDealsAtRisk = response;
                if(response && response.length>0) {
                    _.each(response,function (el) {
                        team_atRisk = team_atRisk+el.count;
                    })
                }
                share.teamRiskData(team_atRisk)
            });
    };

    getDealsAtRisk();

    share.refreshDealsAtRisk = function () {
        getDealsAtRisk();
    }

    share.showDealsAtRisk = function(value) {
        $scope.showDealsAtRisk = value;
    }

    $scope.goToContact = function(emailId){
        window.location = '/contacts/all?contact='+emailId+'&acc=true'
    };

    $scope.sortType = 'closeDate';
    $scope.sortReverse = false;

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    share.oppExist = function (value) {
        $scope.noOppsCreated = !value;
    }

    $scope.openOppBox = function () {
        share.openOppBox(true);
    }

    $scope.getContactProfile = function (item) {
        item.name = item.fullName;
        item.userEmailId = $scope.opp.userEmailId;
        getContactProfileInfoFromDb($http,item)
    }
    
    $scope.oppUpdateStage = function (stage) {

        updateOpportunity($scope,$http,$scope.opp,"stageName",stage,false,function (result) {
            toastr.success("Stage updated successfully");
            $scope.isStagnant = "fa-check-circle-o";
            $scope.stagnantStatus = true;
            $scope.stagnantDaysAgo = "Stage was last updated today";
            share.refreshDealsAtRisk();
            share.refreshPipelineSnaphot();
        });
    }

    $scope.getSuggestions = function (deal) {

        if(!$rootScope.noAccess){

            $scope.openDMs(); // Default open DMs

            $scope.dmActiveClass = "active"
            $scope.iActiveClass = "inactive"
            $scope.stagActiveClass = "inactive"
            $scope.staleActiveClass = "inactive"
            $scope.ciActiveClass = "inactive"
            $scope.mActiveClass = "inactive"
            $scope.compIActiveClass = "inactive"
            $scope.ltActiveClass = "inactive"

            $scope.totalValue = $scope.totalPipeLineValue?numberWithCommas($scope.totalPipeLineValue.r_formatNumber(2),share.primaryCurrency == "INR"):"";

            var percentageAtRisk = calculatePercentage(deal.amount,$scope.totalPipeLineValue);

            if(percentageAtRisk == 0){
                $scope.percentageOfTotal = "< 1% of "
            } else {
                $scope.percentageOfTotal = percentageAtRisk+"% of "
            }

            if(!$scope.totalPipeLineValue || !deal.amount){
                $scope.percentageOfTotal = false;
            }

            $scope.dmExist = "fa-exclamation-circle";
            $scope.InfExist = "fa-exclamation-circle";
            $scope.isStaleOpp = "fa-exclamation-circle";
            $scope.isStagnant = "fa-exclamation-circle";
            $scope.metDmInfl = "fa-exclamation-circle";
            $scope.IntScr = "fa-exclamation-circle";
            $scope.ltWithOwner = "fa-exclamation-circle";
            $scope.contatIntr = "fa-exclamation-circle";

            if(!deal.ltWithOwner){
                $scope.ltWithOwner = "fa-check-circle-o"
                $scope.ltTrue = false;
            } else {
                $scope.ltTrue = true;
            }

            if(!deal.skewedTwoWayInteractions){
                $scope.contatIntr = "fa-check-circle-o"
            }

            if(deal.daysSinceStageUpdated<45){
                $scope.isStagnant = "fa-check-circle-o"
                $scope.stagnantStatus = true;
            } else {
                $scope.stagnantStatus = false;
            }

            $scope.stagnantDaysAgo = "Stage was last updated "+deal.daysSinceStageUpdated + " days back";

            if(deal.daysSinceStageUpdated == 0){
                $scope.stagnantDaysAgo = "Stage was last updated today";
            }

            $scope.metDecisionMaker_infuencer = false;
            if(deal.metDecisionMaker_infuencer){
                $scope.metDecisionMaker_infuencer = true;
                $scope.metDmInfl = "fa-check-circle-o"
            }

            $scope.companyIntr = false;

            if(deal.averageInteractionsPerDeal){
                $scope.companyIntr = true;
                $scope.IntScr = "fa-check-circle-o"
            }

            if(share.usersDictionary[deal.userEmailId]){
                deal.owner = share.usersDictionary[deal.userEmailId]
            }

            $scope.opportunityName = deal.opportunityName;
            $scope.contactEmailId = deal.contactEmailId;

            $scope.closeDate = moment(deal.closeDate).format("DD MMM YYYY");
            $scope.oppCreatedDate = deal.createdDate?moment(deal.createdDate).format("DD MMM YYYY"):'';

            $scope.opp = deal;

            $scope.showModal = true;
            $scope.noDMs = !deal.decisionMakersExist;
            $scope.noInfl = !deal.influencersExist;

            if(deal.decisionMakersExist){
                $scope.dmExist = "fa-check-circle-o"
            }

            if(deal.influencersExist){
                $scope.InfExist = "fa-check-circle-o"
            }

            if(new Date(deal.closeDate)< new Date()){
                $scope.staleOpp = true;
            } else {
                $scope.staleOpp = false
                $scope.isStaleOpp = "fa-check-circle-o"
            }

            $scope.stageUpdated = deal.stageName;

            getLiu();

            getTweet($scope,$http,deal.contactEmailId);
        }

    }

    $scope.reactToTweet = function (item,status,action) {
        reactToTweet($http,item,status,action)
    };

    $scope.openDMs = function () {
        $scope.showStagOpp = false;
        $scope.showDms = true;
        $scope.showContInt = false;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "active"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"

    }

    $scope.openInfl = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showInfl = true;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "active"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $scope.openStaleOpp = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showStaleOpp = true;

        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "active"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $scope.openDmOrInfMet = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showMetDmInf = true;

        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "active"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"

        if($scope.opp.dmsInfls[0]){
            getLiu($scope.opp.dmsInfls[0]);
        }
    }

    $scope.openIntScore = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showIntScr = true;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "active"
        $scope.ltActiveClass = "inactive"

    }

    $scope.openLt = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showIntScr = false;
        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.closeThis = false;
        $scope.showLt = true;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "active"

    }

    $scope.openStagOpp = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = true;
        $scope.showStaleOpp = false;
        $scope.closeThis = false;
        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "active"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    };

    $scope.openContactIntr = function () {
        $scope.showContInt = true;
        $scope.showStagOpp = false;
        $scope.showStaleOpp = false;
        $scope.closeThis = false;
        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "active"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $(document).on('keyup',function(evt) {
        if (evt.keyCode == 27) {
            $scope.$apply(function (){
                $scope.showModal = false;
                resetSuggestions($scope)
            })
        }
    });

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.removeRecipient = function(contact,type){
        removeRecipient($scope,$http,contact,type)
    }

    $scope.closeModal = function () {
        $scope.showModal = false;
    }

    $scope.oppCloseLost = function () {
        updateOpportunity($scope,$http,$scope.opp,"closeDate",new Date(),true,function (result) {
            if(result){
                share.refreshClosingSoonDeals()
                share.refreshDealsAtRisk()
                share.refreshPipelineSnaphot()
                share.refreshTarget()
                $scope.closeModal();

                toastr.success("Opportunity closed.")
            } else {
                toastr.error("An error occurred while closing this opportunity. Please try later")
            }
        })
    }

    $scope.registerDatePickerId = function(){

        $('#opportunityCloseDateSelector2').datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    updateOpportunity($scope,$http,$scope.opp,"closeDate",dp,false,function (result) {
                        if(result){
                            share.refreshDealsAtRisk();
                            $scope.closeDate = moment(dp).format("DD MMM YYYY");
                            $scope.isStaleOpp = "fa-check-circle-o";
                            $scope.staleOpp = false;
                        } else {

                        }
                    })
                });
            }
        });
    }

    function getLiu(emailId) {

        if(share.liuData){

            var signature = "\n\n"+getSignature(share.liuData.firstName + ' '+ share.liuData.lastName,
                share.liuData.designation,
                share.liuData.companyName,
                share.liuData.publicProfileUrl)

            var interactionsFor = emailId?emailId:$scope.contactEmailId;

            getLastInteractedDetailsMessage($scope,$http,interactionsFor,share.liuData.firstName,share.liuData.publicProfileUrl,function (message) {

                $scope.subject = message.subject;
                $scope.body = message.body+signature;

                $scope.subjectLt = message.subject;
                $scope.subjectCi = message.subject;
                $scope.bodyLt = "\n\n"+signature;

            });

        } else {
            setTimeOutCallback(100,function () {
                getLiu()
            });
        }
    }

    $scope.sendEmail = function (subject,body,reason,contactEmailId) {

        var contactDetails = {
            contactEmailId: contactEmailId?contactEmailId:$scope.opp.contactEmailId,
            personId:null,
            personName:null
        }

        sendEmail($scope,$http,subject,body,contactDetails,reason)
    }

});

function getTweet($scope,$http,emailId){

    $http.get('/fetch/stored/tweets/by/email?emailId='+emailId)
        .success(function (response) {

            if(response.SuccessCode){
                $scope.tweet = response.Data
                $scope.ifTweetExists = true;

            } else {
                $scope.ifTweetExists = false;
                $scope.tweet = {}
            }
        });
}

function updateOpportunity($scope,$http,opp,updateField,value,closeThisOpp,callback) {

    var obj = {
        updateField:updateField,
        value:value,
        _id:opp.opportunityId,
        closeThisOpp:closeThisOpp,
        prevStage:$scope.opp.stageName
    }
    
    $http.post("/opportunities/update",obj)
        .success(function (response) {
          callback(response)
        });
}

function resetSuggestions($scope) {

    $scope.noDMs = false;
    $scope.noInfl = false;
    $scope.decisionMaker = ""
    $scope.influencer = ""

}

relatasApp.controller("all_graphs",function ($scope,$http,share,$rootScope) {

    $scope.loadingTargets = true;
    $scope.loadingDealsClosing = true;
    $scope.loadingLt = true;
    var ngmReq = share.companyDetails && share.companyDetails.netGrossMargin;

    share.removeLoadingIcon = function (className) {

        if(className == ".losing-touch-graph"){
            $scope.loadingLt = false;
        }

        if(className == ".open-deals"){
            $scope.loadingDealsClosing = false;
        }
    }

    share.totalDealValueAtRisk = function (total,atRisk) {

        $scope.total_open_deals = total;
        $scope.valueAtRisk = getAmountInThousands(atRisk,2);
        $scope.risk_totalValPerc = {
            'width':calculatePercentage(atRisk,total)+'%'
        }
    }

    share.self30DaysValAtRisk = function (valueAtRisk) {

        $scope.valueAtRiskSelf30Days = getAmountInThousands(valueAtRisk,2)

        function getTotalValue(){
            if($scope.total_open_deals || $scope.total_open_deals === 0){
                $scope.closing30days_totalValPerc = {
                    'width':calculatePercentage(valueAtRisk,$scope.total_open_deals+valueAtRisk)+'%'
                }

            } else {

                $scope.closing30days_totalValPerc = {
                    'width':calculatePercentage(valueAtRisk,0+valueAtRisk)+'%'
                }

                setTimeOutCallback(10,function () {
                    getTotalValue();
                });
            }
        }

        getTotalValue();
    }

    share.refreshNumberAllGrowth = function () {
        // getDealsAtRiskPercentages();
        // geLTPercentages();
    }

    share.getDealsClosingTeam = function (users) {

        var userIds = _.map(users,"_id");
        var deals_url = "/insights/deals/closing/soon"
        if(userIds){
            deals_url = deals_url+"?userIds="+userIds;
        }

        $http.get(deals_url)
            .success(function (response) {
                if(response && response.SuccessCode){
                    $scope.team_closing_30days = response.Data.length;

                    share.teamClosing30Days(response)
                }
            });
    }

    function getTargets(userId,accessControl) {
        var url = "/opportunities/by/month/year?monthsNext=4&monthsPrev=3&forSixMonthsRange=true";

        if(userId){
            url = url+"&userId="+userId;
        }

        if(accessControl){

            if(share.liuData && share.liuData.orgHead){
                url = fetchUrlWithParameter(url+"&accessControl="+true)
                url = fetchUrlWithParameter(url+"&companyId="+share.liuData.companyId)
            } else {
                url = fetchUrlWithParameter(url+"&accessControl="+true)
            }
        }

        setTimeOutCallback(3000,function () {
            drawTargets($scope,share,$http,url)
        });
    }

    getTargets();

    share.forMonthlyTargets = function (userId,accessControl) {
        getTargets(userId,accessControl)
    }

    share.refreshTarget = function () {
        getTargets();
    }

    $scope.showPipelineVelocity = function () {
        share.showPipelineVelocity(true);
    };

    share.dealsAtRiskNumbers = function (max,min) {
        $scope.atRisk = min
        getDealsAtRiskPercentages();
    }

    function getDealsAtRiskPercentages() {
        if(($scope.atRisk || $scope.atRisk === 0) && ($scope.team_atRisk || $scope.team_atRisk === 0)){

            var total = $scope.team_atRisk+$scope.atRisk;

            $scope.risk_selfPerc = {
                'width':calculatePercentage($scope.atRisk,total)+'%'
            }
            $scope.risk_teamPerc = {
                'width':calculatePercentage($scope.team_atRisk,total)+'%'
            }

            if($rootScope.viewingExceptionalAccess){
                $scope.risk_selfPerc = {
                    'width':100+'%'
                }
                $scope.risk_teamPerc = {
                    'width':100+'%'
                }
            }

        } else {
            setTimeOutCallback(1000,function () {
                getDealsAtRiskPercentages()
            });
        }
    }

    getDealsAtRiskPercentages();

    function getDealsClosingPercentages() {

        if(($scope.closingSoon || $scope.closingSoon === 0) && ($scope.team_closing_30days || $scope.team_closing_30days === 0)){

            var total = $scope.closingSoon+$scope.team_closing_30days;
            $scope.team_dealsPerc = {
                'width':calculatePercentage($scope.team_closing_30days,total)+'%'
            }
            $scope.self_dealsPerc = {
                'width':calculatePercentage($scope.closingSoon,total)+'%'
            }

            if($scope.closingSoon == $scope.team_closing_30days){
                $scope.team_dealsPerc = {
                    'width':100+'%'
                }
                $scope.self_dealsPerc = {
                    'width':100+'%'
                }
            }

        } else {
            setTimeOutCallback(1000,function () {
                getDealsClosingPercentages()
            });
        }
    }

    getDealsClosingPercentages();

    function geLTPercentages() {
        if(($scope.losingTouch || $scope.losingTouch === 0) && ($scope.losingTouchCompany || $scope.losingTouchCompany === 0)){

            var total = $scope.losingTouch+$scope.losingTouchCompany;
            $scope.team_LTPerc = {
                'width':calculatePercentage($scope.losingTouchCompany,total)+'%'
            }
            $scope.self_LTPerc = {
                'width':calculatePercentage($scope.losingTouch,total)+'%'
            }
        } else {
            setTimeOutCallback(1000,function () {
                geLTPercentages()
            });
        }
    }

    geLTPercentages();

    share.ltNumbers = function (max,min,companyLosingTouch) {
        $scope.losingTouch = min
        $scope.losingTouchCompany = $scope.losingTouchCompany?$scope.losingTouchCompany:companyLosingTouch
        geLTPercentages();
    }

    share.closingSoonNumbers = function (max,min) {
        $scope.closingSoon = min
        getDealsClosingPercentages();
    }

    $scope.showTableInit = function () {

        $scope.dealsAtRiskCss = "insight-selection"
        setTimeOutCallback(1,function () {
            share.showDealsAtRisk(true);
            share.showClosingSoon(false);
            share.showLosingTouch(false);
            share.showPipelineVelocity(false);
        })
    }

    $scope.showTable = function (table) {

        if(table == "dealsAtRisk"){

            $scope.losingTouchCss = ""
            $scope.pipelineVelocityCss = ""
            $scope.closingSoonCss = ""
            $scope.dealsAtRiskCss = "insight-selection"

            share.showDealsAtRisk(true);
            share.showClosingSoon(false);
            share.showLosingTouch(false);
            share.showPipelineVelocity(false);
        } else if(table == "closingSoon"){

            $scope.pipelineVelocityCss = ""
            $scope.losingTouchCss = ""
            $scope.closingSoonCss = "insight-selection"
            $scope.dealsAtRiskCss = ""

            share.showDealsAtRisk(false);
            share.showClosingSoon(true);
            share.showLosingTouch(false);
            share.showPipelineVelocity(false);
        } else if(table == "losingTouch"){

            $scope.losingTouchCss = "insight-selection"
            $scope.closingSoonCss = ""
            $scope.dealsAtRiskCss = ""
            $scope.pipelineVelocityCss = ""

            share.showDealsAtRisk(false);
            share.showClosingSoon(false);
            share.showLosingTouch(true);
            share.showPipelineVelocity(false);

        } else if(table == "pipelineVelocity"){

            $scope.losingTouchCss = ""
            $scope.closingSoonCss = ""
            $scope.dealsAtRiskCss = ""
            $scope.pipelineVelocityCss = "insight-selection"

            share.showDealsAtRisk(false);
            share.showClosingSoon(false);
            share.showLosingTouch(false);
            share.showPipelineVelocity(true);
        }
    }

    share.teamRiskData = function (riskDeals) {
        $scope.team_atRisk = riskDeals;
    }
});

relatasApp.controller("acc_growth",function ($scope,$http,share) {

    share.forAccsCreated = function (userId,accessControl) {
        getAccGrowth(userId,accessControl)
    }

    function getAccGrowth(userId,accessControl){
        var url = "/insights/accounts/interaction/growth"
        if(userId){
            url = url+"?userIds="+userId;
        }

        if(accessControl){
            url = fetchUrlWithParameter(url+"&accessControl="+true) // Access control unnecessary here.
        }

        setTimeOutCallback(1500, function () {

            $http.get(url)
                .success(function (response) {
                    if (response && response.SuccessCode && response.Data.length>0) {

                        var label = [], series = [];

                        response.Data.sort(function (o1, o2) {
                            return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
                        });

                        _.each(response.Data, function (el) {
                            var month = monthNames[moment(new Date(el.sortDate)).month()];
                            var monthYear = monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year();
                            label.push(month.substring(0, 3));
                            series.push({meta:monthYear,value:el.accountDetails.length});
                        });

                        drawLineChart($scope, share, series, label, ".acc-growth")
                    } else {
                        $scope.noAcc = true;
                    }
                })
        });
    }

    getAccGrowth();
});

relatasApp.controller("opp_growth",function ($scope,$http,share) {

    share.forOppGrowth = function (userId,accessControl) {
        getOppGrowth(userId,accessControl)
    }

    function getOppGrowth(userId,accessControl) {
        setTimeOutCallback(500, function () {
            var url = "/insights/opp/growth"
            if(userId){
                url = url+"?userIds="+userId;
            }

            if(accessControl){
                url = fetchUrlWithParameter(url+"&accessControl="+true); // Access control unnecessary here.
            }

            $http.get(url)
                .success(function (response) {
                    if (response && response.SuccessCode) {

                        $scope.noOpp = true;
                        var label = [], seriesOpen = [], seriesClose = [];

                        _.each(response.Data, function (el) {
                            if(el.count>0){
                                $scope.noOpp = false;
                            }
                            label.push(monthNames[moment(new Date(el.sortDate)).month()].substring(0,3))
                            seriesOpen.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
                        });

                        _.each(response.dataClosed, function (el) {
                            seriesClose.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
                        });

                        $scope.label2 = "Created",$scope.label1 = "Won";

                        drawLineChart($scope,share,seriesOpen,label,".opp-growth",seriesClose);

                    } else {
                        $scope.noOpp = true;
                    }
                })
        });
    }

    getOppGrowth();

    share.refreshOppGrowth = function () {
        getOppGrowth();
    }

});

relatasApp.controller("pipeline_velocity",function ($scope,$http,share,$rootScope) {

    $scope.takeAction = function (opp) {

        if(!$rootScope.noAccess){
            $scope.opp = opp;
            $scope.showModal = true;
        }
    }

    $scope.closeModal = function () {
        $scope.showModal = false;
    }

    $scope.sortType = 'closeDate';
    $scope.sortReverse = false;

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.registerDatePickerId = function(){

        $('#opportunityCloseDateSelector4').datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    updateOpportunity($scope,$http,$scope.opp,"closeDate",dp,false,function (result) {
                        if(result){
                            share.refreshDealsAtRisk();
                            $scope.opp.formattedCloseDate = moment(dp).format("DD MMM YYYY");
                        } else {

                        }
                    })
                });
            }
        });
    }

    share.showPipelineVelocity = function (value) {
        $scope.openPipelineVelocity = value;
    }

    $scope.goTo = function () {
        window.location = "/opportunities/all"
    }

    setTimeOutCallback(4000,function () {
        getPipelineVelocity()
    });

    share.forPipelineVelocity = function (userId,accessControl) {
        getPipelineVelocity(userId,accessControl)
    }

    function getPipelineVelocity (userId,accessControl){

        var url = '/insights/pipeline/velocity'
        if(userId){
            url = url+"?userId="+userId;
        }

        if(accessControl){

            if(share.liuData && share.liuData.orgHead){
                url = fetchUrlWithParameter(url+"&accessControl="+true)
                url = fetchUrlWithParameter(url+"&companyId="+share.liuData.companyId)
            } else {
                url = fetchUrlWithParameter(url+"&accessControl="+true)
            }
        }

        $http.get(url)
            .success(function (response) {
                if(response && response.SuccessCode){

                    var opportunityStages = {};

                    if(share.opportunityStages){
                        _.each(share.opportunityStages,function (op) {
                            opportunityStages[op.name] = op.order;
                        })
                    }

                    $scope.expectedPipeline = response.Data.expectedPipeline
                    $scope.deals = response.Data.oppNextQ && response.Data.oppNextQ[0] && response.Data.oppNextQ[0].opportunities?response.Data.oppNextQ[0].opportunities:[];
                    $scope.currentQuarter = response.Data.currentQuarter;

                    var allValues = [];
                    var target = response.Data.currentTargets[0]? response.Data.currentTargets[0].target:0

                    var pipeline = 0,won=0;
                    _.each(response.Data.currentOopPipeline,function (op) {

                        if(op._id == "Close Won"){
                            won = won+op.sumOfAmount
                        }

                        if(op._id != "Close Won" && op._id != "Close Lost"){
                            pipeline = pipeline+op.sumOfAmount
                        }
                    });

                    $scope.staleOppsExist = false;
                    $scope.nextQuarterOppsExist = false;

                    var gap = target - won;

                    allValues.push(target)
                    allValues.push(pipeline)
                    allValues.push(won)
                    allValues.push(gap)

                    $scope.targetCount = numberWithCommas(target.r_formatNumber(2),share.primaryCurrency == "INR");
                    $scope.pipelineCount = numberWithCommas(pipeline.r_formatNumber(2),share.primaryCurrency == "INR");
                    $scope.wonCount = numberWithCommas(won.r_formatNumber(2),share.primaryCurrency == "INR");
                    $scope.gapCount = numberWithCommas(gap.r_formatNumber(2),share.primaryCurrency == "INR");

                    var max = _.max(allValues);
                    var min = _.min(allValues);

                    $scope.target = {'width':scaleBetween(target,min,max)+'%',background: '#FE9E83'}
                    $scope.pipeline = {'width':scaleBetween(pipeline,min,max)+'%',background: '#767777'}
                    $scope.won = {'width':scaleBetween(won,min,max)+'%',background: '#8ECECB'}
                    $scope.gap = {'width':scaleBetween(gap,min,max)+'%',background: '#e74c3c'}

                    if(won>target){
                        $scope.expectationsExceed = true;
                    }

                    if($scope.deals.length>0){

                        $scope.nextQuarterOppsExist = true;

                        _.each($scope.deals,function (deal) {
                            deal.amountWithCommas = numberWithCommas(parseFloat(deal.amount.r_formatNumber(2)),share.primaryCurrency == "INR")
                            deal.formattedCloseDate = moment(deal.closeDate).format("DD MMM YYYY")
                            deal.company = fetchCompanyFromEmail(deal.contactEmailId)
                            deal.isStale = false
                            deal.suggestion = "Suggest moving this opportunity closing next quarter to current quarter."
                        })
                    }

                    if(response.Data.staleOpps && response.Data.staleOpps.length>0){

                        $scope.staleOppsExist = true;

                        _.each(response.Data.staleOpps,function (deal) {
                            deal.amountWithCommas = numberWithCommas(parseFloat(deal.amount).r_formatNumber(2),share.primaryCurrency == "INR")
                            deal.formattedCloseDate = moment(deal.closeDate).format("DD MMM YYYY")
                            deal.company = fetchCompanyFromEmail(deal.contactEmailId)
                            deal.isStale = true
                            deal.suggestion = "This is a stale opportunity. \n Move this deal to current quarter to meet your target or close the opportunity."

                            $scope.deals.push(deal)
                        })
                    }

                    if(response.Data.currentTargets && response.Data.currentTargets[0] && response.Data.currentTargets[0].target || accessControl){

                        $scope.actionRequired = true;
                        if(response.Data.expectedPipeline>response.Data.currentTargets[0].target || accessControl){
                            $scope.actionRequired = true;
                        }
                    }

                    if(!target && !pipeline){
                        $scope.targetPipelineNone = true;
                    }

                    if(pipeline>target){
                        $scope.targetPipelineNone = false;
                    }

                    if(won>target){
                        $scope.actionRequired = false;
                    }

                    if(target) {

                        if(!won){
                            $scope.actionRequired = true;
                            if(pipeline>=target){
                                $scope.actionRequired = false;
                            }

                        } else {
                            gap = target-won;
                            var wonPercentage = (won/target)*100;
                            var targetPipelineGap = pipeline-won;

                            //targetPipelineGap is the remaining pipeline after achievement, which still can be won
                            //Gap is the minimum won amount required to meet quarter target.

                            if(targetPipelineGap>gap && wonPercentage>=100){
                                $scope.actionRequired = false;
                                $scope.expectationsExceed = true;
                            }

                            if(target>pipeline && gap>0){
                                $scope.actionRequired = true;
                            }
                        }
                    }

                    _.each($scope.deals,function (deal) {
                        deal.stageStyle2 = oppStageStyle(deal.stageName,opportunityStages[deal.stageName]-1,true);
                    })


                } else {
                    $scope.deals = []
                    $scope.target = {}
                    $scope.pipeline = {}
                    $scope.won = {}
                    $scope.gap = {}

                    $scope.targetCount = 0;
                    $scope.pipelineCount = 0;
                    $scope.wonCount = 0;
                    $scope.gapCount = 0;
                }
            });
    }

});

relatasApp.controller("losing_touch",function ($scope,$http,share,$rootScope) {

    $scope.sortType = 'lastInteractionDate';
    $scope.sortReverse = false;

    $scope.openEmailBox = function (emailId) {

        if(!$rootScope.noAccess){

            $scope.sendLtEmail = true;
            $scope.contactEmailId = emailId;

            getLiu(emailId);

            function getLiu(emailId) {

                if(share.liuData){

                    var signature = "\n\n"+getSignature(share.liuData.firstName + ' '+ share.liuData.lastName,
                            share.liuData.designation,
                            share.liuData.companyName,
                            share.liuData.publicProfileUrl)

                    var interactionsFor = emailId?emailId:$scope.contactEmailId;

                    getLastInteractedDetailsMessage($scope,$http,interactionsFor,share.liuData.firstName,share.liuData.publicProfileUrl,function (message) {
                        $scope.subjectLt = message.subject;
                        $scope.bodyLt = message.body+"\n"+signature;

                    });

                } else {
                    setTimeOutCallback(100,function () {
                        getLiu(emailId)
                    });
                }
            }
        }
    }

    $scope.sendEmail = function (subject,body,reason,contactEmailId) {

        var contactDetails = {
            contactEmailId: contactEmailId?contactEmailId:$scope.contactEmailId,
            personId:null,
            personName:null
        }

        sendEmail($scope,$http,subject,body,contactDetails,reason);
        $scope.sendLtEmail = false;
    }

    share.forLT = function (userId) {
        getLosingTouch(userId)
    }

    function getLosingTouch(userId) {
        setTimeOutCallback(1000,function (){

            var url = "/insights/losing/touch/info/by/relation";

            if(userId){
                url = url+"?userIds="+userId;
            }

            $http.get(url)
                .success(function (response) {

                    timezone = timezone?timezone:"UTC";

                    if(share.liuData && share.liuData.emailId){
                        var liuCompanyName = fetchCompanyFromEmail(share.liuData.emailId);
                    } else {
                        liuCompanyName = null;
                    }

                    if(liuCompanyName == "Others") {
                        liuCompanyName = null;
                    }

                    if(response && response.SuccessCode){
                        var len = response.Data.length;
                        var obj = [];
                        var losingTouchCompany = response.aggregatedNumbers.company.companyLosingTouch
                        for (var i = 0; i < len; i++) {

                            var cImg, profilePic, ownerProfilePic, ownerImg, relation, relationTitle
                            var contactImageLink = response.Data[i].contactImageLink?encodeURIComponent(response.Data[i].contactImageLink):null;

                            if (response.Data[i].personId) {
                                cImg = '/getImage/' + response.Data[i].personId;
                                profilePic = false;
                                //Boundary condition. Sometimes the profile images are not found. Showing first 2 char of emailId.
                                // if(!imageExists(cImg)){
                                //     profilePic = true;
                                //     cImg = response.Data[i].contactEmailId.substr(0, 2).toUpperCase()
                                // }

                            }else if(contactImageLink){
                                profilePic = false;
                                cImg = '/getContactImage/'+response.Data[i].contactEmailId+'/'+contactImageLink
                            } else {
                                profilePic = true;
                                cImg = response.Data[i].contactEmailId?response.Data[i].contactEmailId.substr(0, 2).toUpperCase():response.Data[i].contactName.substr(0, 2).toUpperCase()
                            }

                            if (response.Data[i].ownerId) {
                                ownerImg = '/getImage/' + response.Data[i].ownerId;
                                ownerProfilePic = false;
                            } else {
                                ownerProfilePic = true;
                                ownerImg = response.Data[i].ownerFullName.substr(0, 2).toUpperCase()
                            }

                            var influencerCss = 'fa fa-share-alt grey-color';
                            var decisionMakerCss = 'fa fa-user grey-color';

                            if (response.Data[i].contactRelation && response.Data[i].contactRelation.decisionmaker_influencer == 'influencer') {
                                influencerCss = 'fa fa-share-alt orange-color';
                                relation = 'Influencer';
                            } else if (response.Data[i].contactRelation && response.Data[i].contactRelation.decisionmaker_influencer == 'decision_maker') {
                                decisionMakerCss = 'fa fa-user orange-color';
                                relation = "Decision Maker";
                            } else{
                                relation = ' - ';
                            }

                            $scope.ifTweets = false;

                            if (response.Data[i].twitterRefId && response.Data[i].twitterTitle && response.Data[i].lastInteractionType === 'twitter') {
                                $scope.ifTweets = true;
                            }

                            if(response.Data[i].trackInfo && response.Data[i].trackInfo){
                                var emailOpens = response.Data[i].trackInfo.emailOpens ? response.Data[i].trackInfo.emailOpens : 0;
                                var readOn = response.Data[i].trackInfo.isRed?"First read on "+moment(response.Data[i].trackInfo.lastOpenedOn).tz(timezone).format("DD MMM YYYY, h:mm a")+" ("+emailOpens+")":"Unread";
                                if(response.Data[i].action == 'sender' && response.Data[i].trackInfo.trackOpen){
                                    var trackOpen = true;
                                }
                            }

                            if(!response.Data[i].trackInfo && !response.Data[i].trackInfo && response.Data[i].action == 'receiver'){
                                readOn = "Send mails through Relatas to see when mail was read."
                            }

                            var showTrackInfo = false;
                            if(response.Data[i].lastInteractionType == 'email'){
                                showTrackInfo = true;
                            }

                            var interactionIconType = getInteractionIconType(response.Data[i].lastInteractionType)

                            var contactCompany = fetchCompanyFromEmail(response.Data[i].contactEmailId);

                            // if(response.Data[i].contactEmailId && liuCompanyName != contactCompany && response.Data[i].lastInteractionDays>29){
                            if(response.Data[i].contactEmailId && response.Data[i].lastInteractionDays>15){
                                obj.push({
                                    name: response.Data[i].contactName,
                                    fullName: response.Data[i].contactName,
                                    contactEmailId: response.Data[i].contactEmailId,
                                    designation: response.Data[i].designation,
                                    company: response.Data[i].company?response.Data[i].company:"Others",
                                    lastInteracted: response.Data[i].lastInteracted ? moment(response.Data[i].lastInteracted).format("DD MMM YYYY") : null,
                                    contactValue: response.Data[i].contactValue,
                                    suggestion: response.Data[i].suggestion,
                                    publicProfileUrl: typeof response.Data[i].publicProfileUrl != "undefined" ? '/' + response.Data[i].publicProfileUrl : '#',
                                    contactId: response.Data[i].contactId,
                                    image: cImg,
                                    relation: relation,
                                    relationTitle: relationTitle,
                                    no_pic: profilePic,
                                    contactOwner: response.Data[i].ownerFullName,
                                    ownerImage: ownerImg,
                                    owner_no_pic: ownerProfilePic,
                                    twitterRefId: response.Data[i].twitterRefId || "No Tweets to reply to",
                                    twitterTitle: response.Data[i].twitterTitle || "No Tweets to reply to",
                                    ifTweets: $scope.ifTweets,
                                    decisionMakerCss: decisionMakerCss,
                                    influencerCss: influencerCss,
                                    radioLabelA:'radioLabelA'+i,
                                    radioLabel:'radioLabel'+i,
                                    index: i + 1,
                                    ownerEmailId: response.Data[i].ownerEmailId,
                                    ownerId:response.Data[i].ownerId,
                                    ownerFullName: response.Data[i].ownerFullName,
                                    refId: response.Data[i].refId,
                                    title: response.Data[i].title,
                                    description: response.Data[i].description,
                                    emailContentId: response.Data[i].emailContentId,
                                    lastInteractionType: response.Data[i].lastInteractionType,
                                    emailFetch: response.Data[i].description ? false : true,
                                    // setFor: message,
                                    fetchingProblem:false,
                                    readOn:readOn,
                                    interactionIconType:interactionIconType,
                                    showTrackInfo:showTrackInfo,
                                    mobileNumber : response.Data[i].contactMobileNumber?response.Data[i].contactMobileNumber.replace(/[^a-zA-Z0-9]/g,''):null,
                                    emailOpens:emailOpens,
                                    trackOpen:trackOpen ? trackOpen : null,
                                    trackId:response.Data[i].trackId,
                                    userId:response.Data[i].personId,
                                    lastInteractionDate:response.Data[i].lastInteractionDate
                                });
                            }
                        }

                        $scope.losingTouch = _.uniqBy(obj,"contactEmailId");

                        var all = 0,imp = $scope.losingTouch.length;
                        if(response){
                            all = response.totalFavTemp-imp
                        }

                        share.ltNumbers (all,imp,losingTouchCompany)

                        donutDraw($scope,share,[{meta:"Other important contacts",value:all},{meta:"Losing touch important contacts",value:imp}],['All', 'Important'],'.losing-touch-graph');
                    } else {
                        $scope.losingTouch = [];
                        share.ltNumbers (0,0,0)
                        donutDraw($scope,share,[{meta:"Other important contacts",value:0},{meta:"Losing touch important contacts",value:0}],['All', 'Important'],'.losing-touch-graph');
                    }
                });
        })
    }

    getLosingTouch();

    share.showLosingTouch = function(value){
        $scope.showLosingTouch = value;
    }
});

function drawDealsClosingSoon($scope,share,dataValues) {

    var opportunityStages = {};

    var ngmReq = share.companyDetails && share.companyDetails.netGrossMargin;

    if(share.opportunityStages){
        _.each(share.opportunityStages,function (op) {
            opportunityStages[op.name] = op.order;
        })
    }

    var valueAtRisk = 0;
    _.each(dataValues,function (el) {
        if(!isNumber(el.amount)){
            el.amount = 0;
        }

        el.ngmReq = ngmReq;
        el.amountWithNgm = el.amount;

        if(ngmReq){
            el.amountWithNgm = (el.amount*el.netGrossMargin)/100
        }

        el.stageStyle2 = oppStageStyle(el.stageName,opportunityStages[el.stageName]-1,true);

        var company = fetchCompanyFromEmail(el.contactEmailId);
        el.closeDate = moment(el.closeDate).format("DD MMM YYYY");
        el.closeDateForSort = moment(el.closeDate).format("DD MMM YYYY");
        el.company = company?company:"Others"
        valueAtRisk = valueAtRisk+parseFloat(el.amount);
        // el.amount = numberWithCommas(el.amount)
        el.amountWithCommas = numberWithCommas(parseFloat(el.amount).r_formatNumber(2),share.primaryCurrency == "INR")

        el.amountWithNgm = el.amountWithNgm.r_formatNumber(2)

        if(share.dealsWithRiskValue && share.dealsWithRiskValue[el.opportunityId]){
            el.averageInteractionsPerDeal = share.dealsWithRiskValue[el.opportunityId].averageInteractionsPerDeal
            el.metDecisionMaker_infuencer = share.dealsWithRiskValue[el.opportunityId].metDecisionMaker_infuencer
            el.ltWithOwner = share.dealsWithRiskValue[el.opportunityId].ltWithOwner
            el.skewedTwoWayInteractions = share.dealsWithRiskValue[el.opportunityId].skewedTwoWayInteractions
            el.riskData = share.dealsWithRiskValue[el.opportunityId]
        } else {
            el.riskData = {
                riskClass : "risk-low"
            } // Opp not existing in share.dealsWithRiskValue as it was not @ risk. Hence not saved.
        }

    });

    share.self30DaysValAtRisk(valueAtRisk)

    $scope.dealsClosingSoon = dataValues;

}

function drawMonthlyStats($scope,dataValues) {

    if(dataValues && dataValues[0]){

        $scope.values = dataValues[0];
        var d = new Date();

        $scope.values["monthName"] = monthNames[d.getMonth()]+" "+d.getFullYear();

    }
}

function dealsAtRiskGraph($scope,share,deals,averageRisk,accessControl,totalDeals) {

    var atRisk = 0,safe = 0;
    $scope.total = deals.length;
    $scope.deals = [];
    $scope.totalDealValue = 0;
    var allRisks = _.map(deals,'riskMeter');
    $scope.totalPipeLineValue = 0;

    var dealsWithRiskValue = {};
    var amountAtRisk = 0;

    var maxRisk = _.max(allRisks);
    var minRisk = _.min(allRisks);

    var ngmReq = share.companyDetails && share.companyDetails.netGrossMargin;

    processData()

    function processData(){

        var opportunityStages = {};

        if(share.opportunityStages){
            _.each(share.opportunityStages,function (op) {
                opportunityStages[op.name] = op.order;
            })

            _.each(deals,function (de) {

                var counter = 0;

                _.forIn(de, function(value, key) {
                    if(value === true){
                        counter++
                    }
                });

                de.ngmReq = ngmReq;
                de.amountWithNgm = de.amount;

                if(ngmReq){
                    de.amountWithNgm = (de.amount*de.netGrossMargin)/100
                }


                de.stageStyle2 = oppStageStyle(de.stageName,opportunityStages[de.stageName]-1,true);

                $scope.totalPipeLineValue = $scope.totalPipeLineValue+parseFloat(de.amount);

                $scope.totalDealValue = isNumber(de.amount)?$scope.totalDealValue+parseFloat(de.amount):$scope.totalDealValue+0;

                if(de.riskMeter >= averageRisk && counter<3){
                    $scope.deals.push(de)
                    atRisk++
                    amountAtRisk = amountAtRisk+parseFloat(de.amountWithNgm)
                } else {
                    safe++
                }

                de.amountWithNgm = de.amountWithNgm.r_formatNumber(2)

                var riskPerc = scaleBetween(de.riskMeter,minRisk,maxRisk);

                if(minRisk == maxRisk){
                    riskPerc = 100
                }

                var suggestion = "Opportunity at highest risk";

                if(riskPerc > 70 && riskPerc < 90){
                    suggestion = "Opportunity at high risk";
                }

                if(riskPerc > 50 && riskPerc < 70){
                    suggestion = "Opportunity at medium risk";
                }

                if(riskPerc < 50){
                    suggestion = "Opportunity at low risk";
                }

                de.amountWithCommas = numberWithCommas(parseFloat(de.amount).r_formatNumber(2),share.primaryCurrency == "INR")
                de.riskSuggestion = suggestion;
                de.riskPercentage = riskPerc+'%';
                de.riskPercentageStyle = {
                    'width':riskPerc+'%'
                }

                if(de.riskMeter >= averageRisk){
                    de.riskClass = 'risk-high'
                } else {
                    de.riskClass = 'risk-low'
                }

                dealsWithRiskValue[de.opportunityId] = {
                    riskMeter:de.riskMeter,
                    riskPercentage:de.riskPercentage,
                    riskPercentageStyle:de.riskPercentageStyle,
                    riskSuggestion:de.riskSuggestion,
                    riskClass:de.riskClass,
                    averageRisk:averageRisk,
                    averageInteractionsPerDeal: de.averageInteractionsPerDeal,
                    metDecisionMaker_infuencer: de.metDecisionMaker_infuencer,
                    ltWithOwner: de.ltWithOwner,
                    skewedTwoWayInteractions: de.skewedTwoWayInteractions
                }

            });

            if(accessControl){
                safe = totalDeals - atRisk
            }

            share.dealsAtRiskNumbers(safe,atRisk);

            if(!_.size(share.dealsWithRiskValue)){
                share.dealsWithRiskValue = dealsWithRiskValue;
            }

            share.totalDealValueAtRisk($scope.totalDealValue,amountAtRisk);

            donutDraw($scope,share,[{meta:"Low Risk",value:safe},{meta:"High Risk",value:atRisk}],['Low Risk','High Risk'],'.deals-at-risk');
        } else {
            setTimeOutCallback(1000,function () {
                processData()
            })
        }
    }

}

function donutDraw($scope,share,series,label,className) {

    var chart = new Chartist.Pie(className, {
        series: series,
        labels: label
    }, {
        donut: true,
        donutWidth: 6,
        showLabel:false,
        plugins: [
            tooltip
        ]
    });

    chart.on('draw', function(data) {
        if(data.type === 'slice') {

            var pathLength = data.element._node.getTotalLength();

            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 1000,
                    from: -pathLength + 'px',
                    to:  '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    fill: 'freeze'
                }
            };

            if(data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            data.element.animate(animationDefinition, false);

            share.removeLoadingIcon(className)
        }
    });
}

function drawLineChart($scope,share,series,label,className,series2) {

    var seriesArray = [series];

    if(series && series2){
        seriesArray = [ series, series2]
    }

    new Chartist.Line(className, {
        labels: label,
        series: seriesArray
    }, {
        low: 0,
        showArea: true,
        plugins: [
            tooltip
        ]
    });
}

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){

                // if(obj.)
                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else if(obj.name){
                    obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    obj.noPPic = obj.name.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    scope.noPPic = obj.name.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                } else if(obj.fullName){

                    obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                //call the function that was passed
                scope.$apply(attrs.imageonload);
            });
        }
    };
});

relatasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

var tooltip = Chartist.plugins.tooltip();

function getContactProfileInfoFromDb($http,item){

    item["fullName"] = item.contactEmailId
    item["name"] = item.contactEmailId
    item["noPicFlag"] = true
    item["nameNoImg"] = item["name"].substr(0,2).toUpperCase()

    // $http.get('/contact/get/details?emailId='+item.emailId+"&userEmailId="+item.userEmailId).success(function (response) {
    //     if(response && response.Data.contacts[0]){
    //
    //         if(checkRequired(response.Data.contacts[0].personId) && checkRequired(response.Data.contacts[0].personName)){
    //             var name = getTextLength(response.Data.contacts[0].personName,20);
    //             var image = '/getImage/'+response.Data.contacts[0].personId._id;
    //
    //             item["fullName"] = response.Data.contacts[0].personName
    //             item["name"] = checkRequired(response.Data.contacts[0].personName)?response.Data.contacts[0].personName:response.Data.contacts[0].personEmailId
    //             item["image"] = image
    //         }
    //         else {
    //
    //             var contactImageLink = response.Data.contacts[0].contactImageLink ? encodeURIComponent(response.Data.contacts[0].contactImageLink) : null
    //             item["fullName"] = response.Data.contacts[0].personName
    //             item["name"] = checkRequired(response.Data.contacts[0].personName)?response.Data.contacts[0].personName:response.Data.contacts[0].personEmailId
    //             item["image"] = '/getContactImage/' + response.Data.contacts[0].personEmailId + '/' + contactImageLink
    //
    //             item["name"] = getTextLength(item["name"], 13)
    //         }
    //
    //         item["nameNoImg"] = item["name"].substr(0,2).toUpperCase()
    //
    //     }
    // })
}

function drawTargets($scope,share,$http,url){
    $http.get(url)
        .success(function (response) {

            console.log(response)
            var total = _.map(response.Data,"total")
            var won = _.map(response.Data,"won")
            var lost = _.map(response.Data,"lost")
            var target = _.map(response.Data,"target")

            var values = _.flatten(_.concat(total,won,lost,target))

            var min = Math.min.apply( null, values );
            var max = Math.max.apply( null, values );
            var currentMonth = monthNames[new Date().getMonth()]

            _.each(response.Data,function (value) {
                var thisMonth = monthNames[new Date(value.sortDate).getMonth()];

                if(thisMonth == currentMonth){
                    value.highLightCurrentMonth = true
                    // value.highLightCurrentMonth = "bold"
                }

                value.heightWon = {'height':scaleBetween(value.won,min,max)+'%'}
                value.heightLost = {'height':scaleBetween(value.lost,min,max)+'%'}
                value.heightTotal = {'height':scaleBetween(value.openValue,min,max)+'%'}
                value.heightTarget = {'height':scaleBetween(value.target,min,max)+'%'}

                if(!value.total){
                    value.total = 0;
                }

                if(!value.won){
                    value.won = 0;
                }

                if(!value.openValue){
                    value.openValue = 0;
                }

                if(!value.target){
                    value.target = 0;
                }

                if(!value.lost){
                    value.lost = 0;
                }

                value.won = numberWithCommas(value.won.r_formatNumber(2),share.primaryCurrency == "INR");
                value.lost = numberWithCommas(value.lost.r_formatNumber(2),share.primaryCurrency == "INR")
                value.openValue = numberWithCommas(value.openValue.r_formatNumber(2),share.primaryCurrency == "INR")
                value.target = numberWithCommas(value.target.r_formatNumber(2),share.primaryCurrency == "INR")

                var monthYearArray = value.monthYear.split(" ");

                value.month = monthYearArray[0]
                value.year = monthYearArray[1]

            });

            $scope.targetGraph = response.Data;

            $scope.targetGraph.sort(function (o1, o2) {
                return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
            });

            $scope.loadingTargets = false;
        })
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

var calculatePercentage = function(count,total){
    return Math.round((count*100)/total);
};

relatasApp.directive('searchResultsP', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultspartner">' +
        '<div ng-repeat="item in partners track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchResultsD', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultsdecisionMaker">' +
        '<div ng-repeat="item in decisionMakers track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchResultsI', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultsinfluencer">' +
        '<div ng-repeat="item in influencers track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchResultsContacts', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultscontact">' +
        '<div ng-repeat="item in contacts track by $index"> ' +
        '<div class="clearfix cursor" ng-click="selectContact(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

function getLastInteractedDetailsMessage($scope,$http,emailId,liuFirstName,publicProfileUrl,callback){

    var url = '/interactions/get/last/interacted/details?emailId='+emailId;
    var calendarLink = "www.relatas.com/"+publicProfileUrl

    var message = {
        subject:"Reconnecting",
        body:"It's been sometime since we last interacted."
        // body:"It's been sometime since we last interacted. My calendar is readily available here: "
    };

    $http.get(url)
        .success(function (response) {
            if(response.SuccessCode && response.Data){

                var interactionType = response.Data[0]?response.Data[0].lastInteractedType:"";
                var lastInteractedDate = moment(response.Data[0].lastInteractedDate).tz(timezone).format("DD MMM YYYY");

                var contactName = response.Data[0].personName?response.Data[0].personName:response.Data[0].personEmailId;

                $scope.contactName = contactName;
                $scope.contactLastInteracted = lastInteractedDate;

                message.subject = "Reconnecting "+contactName+ " & "+liuFirstName;
                message.body = "Hi "+contactName+",\n"+ "It's been sometime since we last interacted over "+interactionType+" on "+lastInteractedDate+" " +
                    "and there's been silence from my end. How about catching up sometime soon?";
                    // "and there's been silence from my end. How about catching up sometime soon? My calendar is readily available here: "+calendarLink;

                if(interactionType == "meeting"){
                    message.body = "Hi "+contactName+",\n"+ "It's been sometime since we last met on "+lastInteractedDate+" " +
                        "and there's been silence from my end. How about catching up sometime soon?";
                        // "and there's been silence from my end. How about catching up sometime soon? My calendar is readily available here: "+calendarLink;
                }

                callback(message);

            } else {
                callback(message)
            }
        });
}

function searchResults($scope,$http,keywords,share,searchService,type) {

    var selector = "showResults"+type;
    var typeSelector = type+"s"

    if(keywords && keywords.length > 2){

        searchService.search(keywords).success(function(response){

            if(response.SuccessCode){
                $scope[selector] = true;
                processSearchResults($scope,$http,response.Data,type);
            } else {
                $scope[selector] = false;
                $scope[typeSelector] = [];

                var obj = {
                    fullName: '',
                    name: '',
                    image: '/getContactImage/' + null + '/' + null,
                    emailId:keywords
                }

                $scope[typeSelector].push(obj)

            }
        }).error(function(){
            console.log('error');
        });
    } else {
        $scope[selector] = false;
        $scope[typeSelector] = [];
    }
}

function removeRecipient($scope,$http,contact,type){

    var opportunityId = $scope.opp._id
    var selector = "showResults"+type;
    var typeSelector = type+"s";

    var hashtag = type;
    var relation = "decision_maker"

    if(type === 'partner'){
        hashtag = "partner"
        relation = "partner"
    } else if(type === 'decisionMaker'){
        hashtag = "decisionMaker"
        relation = "decision_maker"
    } else if(type === 'influencer'){
        hashtag = "influencers"
        relation = "influencer"
    }

    $http.post('/opportunities/remove/people',{type:type,contact:contact,opportunityId:opportunityId})
        .success(function (response) {
            if(response.SuccessCode){
                updateRelationship($http,contact,relation,"decisionmaker_influencer",relation,false)
                deleteHashtag($http,contact.contactId,hashtag,contact.emailId)
                var rmIndex = $scope.opp[typeSelector].indexOf(contact);
                $scope.opp[typeSelector].splice(rmIndex, 1);
                $scope.opp[typeSelector+'Exist'] = $scope.opp[typeSelector].length>0;

                if(type == 'decisionMaker' && !$scope.opp[typeSelector].length>0){
                    $scope.dmExist = "fa-exclamation-circle"
                } else if(!$scope.opp[typeSelector].length>0){
                    $scope.InfExist = "fa-exclamation-circle"
                }
            }
        });
}

function addRecipient($scope,$http,contact,type){

    var opportunityId = $scope.opp?$scope.opp._id:null;
    var selector = "showResults"+type;
    var typeSelector = type+"s";

    $scope.partner = '';
    var hashtag = type;
    var relation = "decision_maker"
    if(type === 'partner'){
        hashtag = "partner"
        relation = "partner"
    } else if(type === 'decisionMaker'){
        hashtag = "decisionMaker";
        relation = "decision_maker"
    } else if(type === 'influencer'){
        hashtag = "influencers"
        relation = "influencer"
    }

    var message = "Successfully added "+contact.emailId+" as influencer for this opportunity"

    if(type == "decisionMaker"){
        message = "Successfully added "+contact.emailId+" as decision maker for this opportunity"
    }

    addHashtag ($http,contact._id,hashtag)
    updateRelationship($http,contact,relation,"decisionmaker_influencer",null,true);

    if(validateEmail(contact.emailId) && opportunityId){
        $http.post('/opportunities/add/people',{type:typeSelector,contact:contact,opportunityId:opportunityId})
            .success(function (response) {
                if(response.SuccessCode){
                    $scope.opp[typeSelector].push(contact)
                    $scope[selector] = false;

                    toastr.success(message);

                    $scope.decisionMaker = ''
                    $scope.influencer = ''

                    if(typeSelector == "influencers"){
                        $scope.noInfl = false;
                        $scope.InfExist = "fa-check-circle-o"
                        $scope.opp.influencersExist = true;

                    } else {
                        $scope.opp.decisionMakersExist = true;
                        $scope.noDMs = false;
                        $scope.dmExist = "fa-check-circle-o"
                    }
                } else {

                    $scope[selector] = false;
                }
            });
    } else {

        $scope[typeSelector] = [];

        if(!$scope.opp){
            $scope.opp = {}
        }

        $scope.opp[typeSelector] = [];
        $scope.opp[typeSelector].push(contact)

        $scope[selector] = false;
    }
}

function addHashtag ($http,p_id,hashtag){
    var str = hashtag.replace(/[^\w\s]/gi, '');
    var obj = { contactId:p_id, hashtag: str};
    $http.post("/api/hashtag/new",obj)
        .success(function (response) {

        });
}

function deleteHashtag($http,p_id,hashtag,contactEmailId) {

    if(p_id){
        $http.get("/api/opportunities/hashtag/delete?contactId=" + p_id + "&hashtag=" + hashtag+"&contactEmailId="+contactEmailId+"&type="+hashtag)
            .success(function (response) {
                if (response.SuccessCode) {
                    // toastr.success("Hashtag Deleted");
                }
                else {
                    // toastr.error("Hashtag Delete Failed. Please try again later");
                }
            });
    }
}

function processSearchResults($scope,$http,response,type) {

    var typeSelector = type+"s";
    var contactsArray = response;
    $scope[typeSelector] = [];

    if(contactsArray.length>0){
        for(var i=0;i<contactsArray.length;i++){

            var obj = {};

            if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                var name = getTextLength(contactsArray[i].personName,20);
                var image = '/getImage/'+contactsArray[i].personId._id;

                obj = {
                    fullName:contactsArray[i].personName,
                    name:name,
                    image:image
                };

                obj.emailId = contactsArray[i].personEmailId;
                obj.twitterUserName = contactsArray[i].twitterUserName;

            }
            else {
                var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                obj = {
                    fullName: contactsArray[i].personName,
                    name: getTextLength(contactsArray[i].personName, 20),
                    image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                    emailId:contactsArray[i].personEmailId,
                    twitterUserName: contactsArray[i].twitterUserName
                    // noPicFlag:true
                };
            }

            if(obj.twitterUserName){
                obj.tweetAccExists = true;
            }

            obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
            obj._id = contactsArray[i]._id;
            obj.personId = contactsArray[i].personId && contactsArray[i].personId._id?contactsArray[i].personId._id:null;
            obj.type = type

            if(!userExists(obj.name) && validateEmail(obj.emailId)){
                $scope[typeSelector].push(obj)
            }

            function userExists(username) {
                return $scope[typeSelector].some(function(el) {
                    return el.name === username;
                });
            }
        }
    }
}

function addNewOpportunity($scope,$http,share,userId,emailId,mobileNumber,liu,contactDetails,$rootScope){

    $scope.opp.name = $scope.opp.opportunityName;
    var checkStrNGM = String($scope.opp.netGrossMargin)

    if(!$scope.opp.opportunityName){
        toastr.error("Please enter the opportunity name")
    } else if(!$scope.opp.closeDateFormatted){
        toastr.error("Please select the closing date")
    } else if(!$scope.opp.amount){
        toastr.error("Please enter the opportunity value")
    } else if(!$scope.opp.stage){
        toastr.error("Please select the current stage")
    } else if($scope.opp.amount.match(/[a-z]/i)){
        toastr.error("Please enter only numbers for opportunity amount")
    } else if($scope.opp.netGrossMargin && !isNumber($scope.opp.netGrossMargin) && checkStrNGM.match(/[a-z]/i)){
        toastr.error("Please enter only numbers for Net Gross Margin")
    } else if($scope.opp.netGrossMargin && ($scope.opp.netGrossMargin>100 || $scope.opp.netGrossMargin<0)){
        toastr.error("Net Gross Margin must be between 1 - 100")
    } else if($rootScope.netGrossMargin && !$scope.opp.netGrossMargin){
        toastr.error("Net Gross Margin setting is mandatory")
    } else if($scope.opp.renewThisOpp && !$scope.renewalAmount){
        toastr.error("Renewal amount is required")
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    } else if($scope.opp.renewThisOpp && !$scope.renewalCloseDate){
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
        toastr.error("Renewal close date is required")
    } else if($scope.opp.renewThisOpp && $scope.renewalAmount && String($scope.renewalAmount).match(/[a-z]/i)){
        toastr.error("Please enter only numbers for Renewal amount")
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    } else {

        $http.post("/salesforce/add/new/opportunity/for/contact", {
                opportunity: $scope.opp,
                contactEmailId: $scope.newOppContact.emailId,
                contactMobile: $scope.newOppContact.mobileNumber?$scope.newOppContact.mobileNumber:null,
                town:$scope.town,
                zone:$scope.opp.geoLocation?$scope.opp.geoLocation.zone:null,
                renewalCloseDate:$scope.renewalCloseDate,
                renewalAmount:$scope.renewalAmount
            })
            .success(function (response) {
                if(response.SuccessCode){
                    toastr.success("Opportunity added successfully");
                    $scope.isOppModalOpen = !$scope.isOppModalOpen;

                    share.refreshDealsAtRisk()
                    share.refreshOppGrowth()
                    share.refreshTarget()
                    share.refreshPipelineSnaphot()
                    share.refreshClosingSoonDeals()

                } else {
                    toastr.error("Opportunity not created. Please try again later")
                }
            })
    }
}

function saveExistingOpportunity($scope,$http,share,userId,emailId,mobileNumber,liu,contactDetails,$rootScope){

    $scope.opp.name = $scope.opp.opportunityName;
    var checkStrNGM = String($scope.opp.netGrossMargin)

    if(!$scope.opp.opportunityName){
        toastr.error("Please the opportunity name")
    } else if(!$scope.opp.closeDateFormatted){
        toastr.error("Please select the closing date")
    } else if(!$scope.opp.amount){
        toastr.error("Please enter the value of this opportunity")
    } else if(!$scope.opp.stage){
        toastr.error("Please select the current stage")
    } else if($scope.opp.amount.match(/[a-z]/i)){
        toastr.error("Please enter only numbers for opportunity amount")
    } else if($scope.opp.netGrossMargin && !isNumber($scope.opp.netGrossMargin) && checkStrNGM.match(/[a-z]/i)){
        toastr.error("Please enter only numbers for Net Gross Margin")
    } else if($rootScope.netGrossMargin && !$scope.opp.netGrossMargin){
        toastr.error("Net Gross Margin setting is mandatory")
    } else if($scope.opp.netGrossMargin && ($scope.opp.netGrossMargin>100 || $scope.opp.netGrossMargin<0)){
        toastr.error("Net Gross Margin must be between 1 - 100")
    } else if($scope.opp.renewThisOpp && !$scope.renewalAmount){
        toastr.error("Renewal amount is required")
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    } else if($scope.opp.renewThisOpp && !$scope.renewalCloseDate){
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
        toastr.error("Renewal close date is required")
    } else if($scope.opp.renewThisOpp && $scope.renewalAmount && String($scope.renewalAmount).match(/[a-z]/i)){
        toastr.error("Please enter only numbers for Renewal amount")
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    } else {

        $http.post("/salesforce/edit/opportunity/for/contact", {
                opportunity: $scope.opp,
                contactEmailId: $scope.cEmailId,
                contactMobile: $scope.cPhone,
                town:$scope.town,
                zone:$scope.opp.geoLocation?$scope.opp.geoLocation.zone:null,
                renewalCloseDate:$scope.renewalCloseDate,
                renewalAmount:$scope.renewalAmount
            })
            .success(function (response) {
                if(response.SuccessCode){

                    oppTargetMetNotifications($http,$scope);
                    toastr.success("Opportunity saved successfully");
                    share.timelineTimeline(userId,emailId,mobileNumber,liu,contactDetails);
                    setTimeout($scope.closeModal(),function () {
                    },3000)
                } else {
                    toastr.error("Opportunity save failed. Please try again later")
                }
            });
    }
}

function updateRelationship($http,contact,relation,relationKey,relationType,value){

    if(contact.contactId){
        var id = contact.contactId
    } else {
        id = contact._id
    }

    var reqObj = {contactId:id,type:relation,relationKey:relationKey,relation:relationType,value:value};

    $http.post('/opportunities/contacts/update/reltionship/type',reqObj)
        .success(function(response){

        });
}

function autoInitGoogleLocationAPI(share){

    function initAutocomplete() {

        var autocomplete2 = new google.maps.places.Autocomplete(
            (document.getElementById('autocompleteCity')),
            {types:['(cities)']});

        autocomplete2.addListener('place_changed', function(){
            share.setTown($("#autocompleteCity").val())
            return false;
        });
    }

    window.initAutocomplete = initAutocomplete;
}

function sendEmail($scope,$http,subject,body,contactDetails,reason){

    var respSentTimeISO = moment().format();

    if(!checkRequired(subject)){
        toastr.error("Please enter the subject.")
    }
    else if(!checkRequired(body)){
        toastr.error("Please enter the message.")
    }
    else{

        var obj = {
            receiverEmailId:contactDetails.contactEmailId,
            receiverName:contactDetails.contactEmailId,
            message:body,
            subject:subject,
            receiverId:contactDetails.personId,
            docTrack:true,
            trackViewed:true,
            remind:null,
            respSentTime:respSentTimeISO,
            isLeadTrack:false,
            newMessage:true
        };

        $http.post("/messages/send/email/single/web",obj)
            .success(function(response){
                if(response.SuccessCode){
                    toastr.success("Email sent successfully")

                    if(reason == "ci"){
                        $scope.contatIntr = "fa-check-circle-o";
                    } else if(reason == "lt"){
                        $scope.ltWithOwner = "fa-check-circle-o";
                    } else if(reason == "meeting") {
                        $scope.metDmInfl = "fa-check-circle-o";
                    } else {
                        $scope.IntScr = "fa-check-circle-o";
                    }
                }
                else{
                    toastr.error("Email not sent. Please try again later");
                }
            })
    }
}

function reactToTweet($http,item,status,action) {

    var url = "/social/feed/twitter/tweet/update/web?postId="+item.id+"&action=reply&status="+status;

    var message = "Retweeted successfully"
    if(action == "favorite"){
        message = "Successfully tweet tagged as favorite"
        url = "/social/feed/twitter/tweet/update/web?postId="+item.id+"&action=favorite";
    } else if(action == "re-tweet"){
        url = "/social/feed/twitter/tweet/update/web?postId="+item.id+"&action=re-tweet";
    }

    $http.get(url)
        .success(function (response) {
            if(response.SuccessCode){
                toastr.success(message)
            } else {
                toastr.error("Action on tweet already taken!")
            }
        });

}

function buildTeamProfiles(data) {
    var team = [];
    var liu = {}
    _.each(data,function (el) {
        if(!el.hierarchyParent){
            liu = {
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                noPicFlag:true,
                nameNoImg:el.firstName.substr(0,2).toUpperCase()
            };
        } else {
            team.push({
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                noPicFlag:true,
                nameNoImg:el.firstName.substr(0,2).toUpperCase()
            })
        }
    });

    team.unshift(liu);
    return team;
}

function closeAllDropDownsAndModals($scope,id,isModal) {
    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                $scope.showProductList = false;
                $scope.selectFromList = false;
                $scope.showLocationList = false;
                $scope.showDateRangePicker = false;
                $scope.showCompanyList = false;
                $scope.showContactList = false;
                if(isModal){
                    $scope.closeModal()
                }
            })
        }
    });
}