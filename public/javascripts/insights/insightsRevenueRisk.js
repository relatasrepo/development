
var reletasApp = angular.module('reletasApp', ['ngRoute','ngSanitize','angular-loading-bar','ngLodash','datatables','ui.bootstrap']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = searchContent;

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

reletasApp.factory('teamMembers', function($http, $q) {
    var data = [],
        lastRequestFailed = true,
        promise;
    return {
        getTeamMembers: function() {
            if (!promise || lastRequestFailed) {
                // $http returns a promise, so we don't need to create one with $q
                promise = $http.get('/company/members')
                    .then(function(res) {
                        lastRequestFailed = false;
                        data = res.data;
                        return data;
                    }, function(res) {
                        return $q.reject(res);
                    });
            }
            return promise;
        }
    }
});

reletasApp.controller("logedinUser", function ($scope, $http,share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {
            $(".seeMoreIcon").addClass("disabled-see-more")
            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $scope.firstName = response.Data.firstName
                $scope.profilePicUrl = '/getImage/'+response.Data._id+'/'+new Date().toISOString()

                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
                share.lUseEmailId = response.Data.emailId
                share.setCompanyId(response.Data.companyId)
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
            }
            else{

            }
        }).error(function (data) {

    })
});
reletasApp.service('share', function () {
    return {
        setCompanyId:function(companyId){
        this.companyId = companyId;
        }
    }
});

reletasApp.factory('sharedServiceSelection', function($rootScope) {
    var sharedService = {};

    sharedService.message = '';

    sharedService.prepForBroadcast = function(msg) {
        this.message = msg;
        this.broadcastItem();
    };

    sharedService.broadcastItem = function() {
        $rootScope.$broadcast('handleBroadcast');
    };
    
    sharedService.paginationBroadcast = function (getFor,getPage) {
        this.getFor = getFor;
        this.getPage = getPage;
        this.broadcastPaginationItem();
    };

    sharedService.broadcastPaginationItem = function () {
        $rootScope.$broadcast('handlePaginationBroadcast')
    }

    return sharedService;
});

reletasApp.controller('selfOrCompany',function($scope, sharedServiceSelection){
    $scope.selectedSelf = 'col-md-6 insights-hierarchy btn-green-xlg';
    $scope.selectedHierarchy = 'col-md-6 insights-hierarchy btn-green-hover-xlg';

    $scope.selectHierachy = function(selection){

        if(selection == 'team')
        { $scope.selectedHierarchy = 'col-md-6 btn-green-xlg insights-hierarchy';
            $scope.selectedSelf = 'col-md-6 btn-green-hover-xlg insights-hierarchy';

        } else if (selection == 'self') {
            $scope.selectedHierarchy = 'col-md-6 btn-green-hover-xlg insights-hierarchy';
            $scope.selectedSelf = 'col-md-6 btn-green-xlg insights-hierarchy';
        }

        sharedServiceSelection.prepForBroadcast(selection);
    }
});

var timezone ;

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var contextEmailId = getParams(window.location.href).context;
var showContext = true;
if(!checkRequired(contextEmailId)){
    showContext = false;
}

var limitGlobal,currentPage,numberOfPages;
reletasApp.controller('losingTouchDetails',function($scope,$http,$rootScope, $sce,sharedServiceSelection,teamMembers,DTOptionsBuilder,DTColumnDefBuilder,lodash,share){

    $scope.$on('handleBroadcast', function() {
        $scope.message = sharedServiceSelection.message;
        paintRevenueRiskTable(sharedServiceSelection.message,false);
    });


    $rootScope.currency = '';

    share.setCompanyId = function (companyId) {
        if(companyId){
            $http.get('/corporate/company/' + companyId)
                .success(function (companyProfile) {
                    if(companyProfile.currency){
                        $rootScope.currency = companyProfile.currency
                    }
                });
        }
    }

    $scope.textload = "Loading data. Please wait..";
    $scope.textProcess = function(){return $scope.textload}
    var vm = this;
    vm.persons = [];
    vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10).withLanguage({
            "oPaginate": {
                "sNext": "»",
                "sPrevious": "«"
            }
        })
        .withOption('columns', [{ "orderDataType": "dom-value" },null,null,{ "orderDataType": "dom-value" },null])
        .withOption('order', [[ 0, "desc" ]])
        .withOption("bDestroy",true)
        // .withOption('bInfo', false)
        .withOption('paging', false)
        .withOption('oLanguage', {
            "sEmptyTable": $scope.textProcess,
            "sLengthMenu": "Show _MENU_ for page",
            //"sProcessing": "DataTables is currently busy",
            //"bProcessing":false,
            "processing":true
        });

    var columnOptions = function(message){

        if(message == 'self') {
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0),
                DTColumnDefBuilder.newColumnDef(1),
                DTColumnDefBuilder.newColumnDef(2),
                DTColumnDefBuilder.newColumnDef(3),
                DTColumnDefBuilder.newColumnDef(4),
                DTColumnDefBuilder.newColumnDef(5),
                DTColumnDefBuilder.newColumnDef(6).notSortable(),
                DTColumnDefBuilder.newColumnDef(7).notSortable(),
                DTColumnDefBuilder.newColumnDef(8).notSortable(),
                //DTColumnDefBuilder.newColumnDef(9).notSortable()
            ];

            vm.dtInstance = {};

            vm.dtInstanceCallback = dtInstanceCallback;

            function dtInstanceCallback(dtInstance) {
                vm.dtInstance = dtInstance;
            }

            $scope.show_loadMore = false;

            $scope.textload = "Loading Data. Please wait..";
            if(vm.dtInstance && vm.dtInstance.DataTable){
                vm.dtInstance.DataTable.clear().draw();
            }
        } else if (message == 'team') {
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0),
                DTColumnDefBuilder.newColumnDef(1),
                DTColumnDefBuilder.newColumnDef(2),
                DTColumnDefBuilder.newColumnDef(3),
                DTColumnDefBuilder.newColumnDef(4),
                DTColumnDefBuilder.newColumnDef(5),
                DTColumnDefBuilder.newColumnDef(6).notSortable(),
                DTColumnDefBuilder.newColumnDef(7).notSortable(),
                DTColumnDefBuilder.newColumnDef(8).notSortable()
            ];

            vm.dtInstance = {};

            vm.dtInstanceCallback = dtInstanceCallback;

            function dtInstanceCallback(dtInstance) {
                vm.dtInstance = dtInstance;
            }

            $scope.show_loadMore = false;

            $scope.textload = "Loading Data. Please wait..";
            if(vm.dtInstance && vm.dtInstance.DataTable){
                vm.dtInstance.DataTable.clear().draw();
            }
        }
    };
    
    $scope.$on('handlePaginationBroadcast', function() {
        paintRevenueRiskTable(sharedServiceSelection.getFor,limitGlobal,sharedServiceSelection.getPage);
    });

     //Default Self
    paintRevenueRiskTable('self',true);

    function paintRevenueRiskTable (message,limit,getPage){

        limitGlobal = limit;

        if(limitGlobal){
            $scope.showAllBtn = true;
        } else {
            $scope.showAllBtn = false;
        }

        columnOptions(message);

        teamMembers.getTeamMembers()
            .then(function(companyData) {

            var url = '/insights/revenue/risk/all/data'
            if (message == 'self') {

                $scope.forTeam = false;

                var hierarchyList = [];
                hierarchyList.push(companyData.Data.userId);
                url = fetchUrlWithParameter(url, 'hierarchyList', hierarchyList);
                url = fetchUrlWithParameter(url, 'getFor', 'self');
                url = fetchUrlWithParameter(url, 'limit', limit);

            } else if (message == 'team') {

                if(companyData.Data.teamMembers.length<=0){
                    toastr.error("No-one reporting to you.")
                }

                $scope.forTeam = true;
                url = fetchUrlWithParameter(url, 'hierarchyList', companyData.Data.teamMembers);
                url = fetchUrlWithParameter(url, 'getFor', 'team');
            }

                if(!getPage) {
                    url = fetchUrlWithParameter(url,'getPage',1)
                } else {
                    $scope.showAllBtn = false;
                    url = fetchUrlWithParameter(url,'getPage',getPage)
                }

            $http.get(url)
                .success(function(response){
                    if(response.SuccessCode && response.Data.length>0){

                        $scope.totalItems = response.totalList;

                        var len = response.Data.length;
                        var objAll = [];

                        for (var i = 0; i < len; i++) {

                            var cImg,profilePic,ownerProfilePic,ownerImg,relation,relationTitle;
                            var contactImageLink = response.Data[i].contactImageLink?encodeURIComponent(response.Data[i].contactImageLink):null

                            if(response.Data[i].personId) {
                                cImg = '/getImage/'+response.Data[i].personId;
                                profilePic = false;
                                if(!imageExists(cImg)){
                                    profilePic = true;
                                    cImg = response.Data.contactName != "undefined" ? response.Data[i].contactName.substr(0,2).toUpperCase() : ''
                                }
                            } else if(contactImageLink){
                                profilePic = false;
                                cImg = '/getContactImage/'+response.Data[i].contactEmailId+'/'+contactImageLink
                            }
                            else {
                                profilePic = true;
                                cImg =  typeof response.Data[i].contactName != "undefined" ? response.Data[i].contactName.substr(0,2).toUpperCase() : ''
                            }

                            if(response.Data[i].ownerId) {
                                ownerImg = '/getImage/'+response.Data[i].ownerId;
                                ownerProfilePic = false;
                            } else {
                                ownerProfilePic = true;
                                ownerImg = typeof response.Data[i].ownerFullName != "undefined" ? response.Data[i].ownerFullName.substr(0,2).toUpperCase() : ''
                            }

                            var influencerCss = 'fa fa-share-alt grey-color';
                            var decisionMakerCss = 'fa fa-user grey-color';

                            if(response.Data[i].contactRelation && response.Data[i].contactRelation.decisionmaker_influencer == 'influencer') {
                                influencerCss = 'fa fa-share-alt orange-color';
                                relation = 'influencer';
                            } else if(response.Data[i].contactRelation && response.Data[i].contactRelation.decisionmaker_influencer == 'decision_maker') {
                                decisionMakerCss = 'fa fa-user orange-color';
                                relation = "decision_maker";
                            }else{
                                relation = null;
                            }

                            $scope.ifTweets = false;

                            if (response.Data[i].twitterRefId && response.Data[i].twitterTitle && response.Data[i].lastInteractionType === 'twitter') {
                                $scope.ifTweets = true;
                            }

                            if(response.Data[i].trackInfo && response.Data[i].trackInfo.trackDocument){
                                var readOn = response.Data[i].trackInfo.isRed?"Read on "+moment(response.Data[i].trackInfo.lastOpenedOn).tz(timezone).format("DD MMM YYYY, h:mm a"):"Unread";
                                if(response.Data[i].action == 'sender' && response.Data[i].trackInfo.trackOpen){
                                    var trackOpen = true;
                                }
                            }

                            if(!response.Data[i].trackInfo && !response.Data[i].trackInfo && response.Data[i].action == 'receiver'){
                                readOn = "Send mails through Relatas to see when mail was read."
                            }

                            var showTrackInfo = false;
                            if(response.Data[i].lastInteractionType == 'email'){
                                showTrackInfo = true;
                            }
                            var interactionIconType = getInteractionIconType(response.Data[i].lastInteractionType)

                            objAll.push(
                                {
                                    contactName: response.Data[i].contactName,
                                    contactEmailId:response.Data[i].contactEmailId,
                                    designation:response.Data[i].designation,
                                    company:response.Data[i].company,
                                    interactionGrowth:response.Data[i].interactionGrowth ? response.Data[i].interactionGrowth:0,
                                    lastInteracted:response.Data[i].lastInteracted ? moment(response.Data[i].lastInteracted).format("DD MMM YYYY") : ' ',
                                    contactValue:response.Data[i].contactValue,
                                    suggestion:response.Data[i].suggestion,
                                    publicProfileUrl: typeof response.Data[i].publicProfileUrl != "undefined" ? '/'+response.Data[i].publicProfileUrl  : '#',
                                    contactId: response.Data[i].contactId,
                                    contactImage: cImg,
                                    no_pic:profilePic,
                                    contactOwner: response.Data[i].ownerFullname,
                                    risk: response.Data[i].risk,
                                    ownerImage:ownerImg,
                                    owner_no_pic:ownerProfilePic,
                                    twitterRefId:response.Data[i].twitterRefId || "No Tweets to reply to",
                                    twitterTitle:response.Data[i].twitterTitle  || "No Tweets to reply to",
                                    ifTweets: $scope.ifTweets,
                                    decisionMakerCss:decisionMakerCss,
                                    influencerCss:influencerCss,
                                    radioLabelA:'radioLabelA'+i,
                                    radioLabel:'radioLabel'+i,
                                    index: i + 1,
                                    relation: relation,
                                    ownerEmailId: response.Data[i].ownerEmailId,
                                    ownerId:response.Data[i].ownerId,
                                    ownerFullName: response.Data[i].ownerFullName,
                                    refId: response.Data[i].refId,
                                    title: response.Data[i].title,
                                    description: response.Data[i].description,
                                    emailContentId: response.Data[i].emailContentId,
                                    lastInteractionType: response.Data[i].lastInteractionType,
                                    emailFetch: response.Data[i].description ? false : true,
                                    setFor: message,
                                    fetchingProblem:false,
                                    readOn:readOn,
                                    interactionIconType:interactionIconType,
                                    showTrackInfo:showTrackInfo,
                                    mobileNumber : response.Data[i].contactMobileNumber?response.Data[i].contactMobileNumber.replace(/[^a-zA-Z0-9]/g,''):null,
                                    trackOpen:trackOpen ? trackOpen : null,
                                    trackId:response.Data[i].trackId,
                                    userId:response.Data[i].personId
                                }
                            );
                        }

                        vm.persons = objAll
                    } else {

                        $scope.textload = "There is no Data";
                        $scope.interactions = [];
                        if(vm.dtInstance && vm.dtInstance.DataTable){
                            vm.dtInstance.DataTable.clear().draw();
                        }
                    }
                });
            });
    }

    $scope.viewPastEmail = function(item){
        if((item.description == null || item.fetchingProblem) && item.emailShow && item.lastInteractionType == 'email') {
            if(item.fetchingProblem) {
                item.description = null;
                item.emailFetch = true;
            }
            $http.get('/message/get/email/single/web?emailContentId=' + item.emailContentId + '&googleAccountEmailId=' + item.ownerEmailId)
                .success(function (response) {
                    if (response.SuccessCode) {
                        item.description = response.Data.data.replace(/\n/g, "<br />");

                        var temp = response.Data.data.replace(/\n/g, "<br />"); //to remove internal css
                        var start, end;
                        start = temp.indexOf("<style");
                        end = temp.indexOf("</style>");
                        end = end != -1 ? end + 8 : end;
                        var replace = temp.slice(start , end);
                        //item.description = item.description.replace(replace, "")

                        temp = item.description;
                        var index = temp.indexOf('/track/email/open/')
                        if(index != -1){
                            item.description = removeRelatasTrackImg(item.description, index)
                        }
                        item.description = $sce.trustAsHtml(item.description);
                        item.emailFetch = false;
                        item.fetchingProblem = false;
                    } else {
                        item.description = response.Message || '';
                        item.fetchingProblem = true;
                        item.emailFetch = false;
                    }
                })
        }
    }

    $scope.updateEmailOpen = function(emailId,trackId,userId){
        if(checkRequired(emailId) && checkRequired(trackId) && checkRequired(userId)){
            $http.get('/track/email/open/'+emailId+'/track/id/'+trackId+'/user/'+userId)
                .success(function(response){
                });
        }
    };

    $scope.goToContactSelected = function(emailId,mobileNumber) {
        if(mobileNumber && mobileNumber !='undefined'){
            window.location.replace('/contact/selected?context='+emailId+'&mobileNumber='+mobileNumber)
        } else {
            window.location.replace('/contact/selected?context='+emailId)
        }
    }

    $scope.ignoreLosingTouch = function(contactId,contactEmailId,ignoreFor){
        var days;
        if(ignoreFor === 30) {
            days = moment().add(ignoreFor, "days").toDate()
        } else if(ignoreFor === 'never') {
            days = moment().add(99, "years").toDate();
        }

        $http.post('/insights/ignore/contact',{contactId:contactId,contactEmailId:contactEmailId,ignoreFor:days})
            .success(function(response){
                if(response){
                    paintRevenueRiskTable('self',limitGlobal);
                }
            });
    }

    $scope.showMoreInsights = function(){
        paintRevenueRiskTable('self',false);
    }

    $scope.showTwitterForm = function(item, full) {
        //$scope.resetFields();
        for (var i = 0; i < full.length; i++)
            if (item !== full[i])
                full[i].showTwitter = false;
        item.showTwitter ? item.showTwitter = false : item.showTwitter = true;
    }

    $scope.replyTwitterPost = function(refId,tweet) {
        var action = 'reply';
        $http.get('/social/feed/twitter/tweet/update/web?action='+action+'&postId='+refId+'&status='+tweet)
            .success(function(response) {
                if (response.SuccessCode) {
                    // console.log(response)
                }
            });
    }

    $scope.showEmailDiv = function(item, full) {
        if(item.lastInteractionType == 'email')
            $scope.resetFields('reply', item);
        else
            $scope.resetFields('fresh', item);
        for (var i = 0; i < full.length; i++)
            if (item !== full[i])
                full[i].emailShow = false;
        item.emailShow ? item.emailShow = false : item.emailShow = true;
        if(item.emailShow  && item.trackOpen){
            $scope.updateEmailOpen(item.ownerEmailId,item.trackId,item.userId);
        }
    }

    $scope.resetFields = function(flag, item) {
        if(flag == 'reply' && item.setFor == 'self'){
            $scope.compose_email_subject = 'Re: ' + item.title;
        }
        else{
            $scope.compose_email_subject = "";
        }

        $scope.compose_email_body = "";
        $scope.compose_email_doc_tracking = true;
        $scope.compose_email_track_viewed = true;
        $scope.compose_email_remaind = true;
        $scope.url = "";
        $scope.companyName = "";
        $scope.designation = "";
    };

    $scope.sendEmail = function(subject, body, docTrack, trackViewed, remind, rName, rEmailId, receiverId, index, suggestion, self_hierarchy, item) {

        if (!checkRequired(subject)) {
            toastr.error("Please enter an email subject.")
        } else if (!checkRequired(body)) {
            toastr.error("Please enter an email body.")
        } else {
            if(self_hierarchy == 'self')
                $scope.actionTaken(null, rName, rEmailId, null, 'mail', 'revenueAtRisk', index, suggestion, self_hierarchy);
            else
                $scope.actionTaken(null, rName, rEmailId, null, 'followUp', 'revenueAtRisk', index, null, self_hierarchy);

            var obj = {
                receiverEmailId: rEmailId,
                receiverName: rName,
                message: body,
                subject: subject,
                receiverId: receiverId,
                docTrack: docTrack,
                trackViewed: trackViewed,
                remind: remind
            };

            if(item){
                if(item.refId){
                    obj.updateReplied = true;
                    obj.refId = item.refId;
                    obj.id = item.refId //Used for Outlook
                    //obj.subject = item.title;
                }
            }

            $("#send-email-but").addClass("disabled");
            $(".send-email-but").addClass("disabled");
            $http.post("/messages/send/email/single/web", obj)
                .success(function(response) {
                    if (response.SuccessCode) {
                        $("#send-email-but").removeClass("disabled");
                        $(".send-email-but").removeClass("disabled");
                        $scope.resetFields();
                        $(".compose-email-inline").slideToggle(200);
                        toastr.success(response.Message);
                    } else {
                        $("#send-email-but").addClass("disabled")
                        toastr.error(response.Message);
                    }
                });
        }
    };

    $scope.actionTaken = function(userId, rName, rEmailId, rmobileNumber, type, source, index, suggestion, self_hierarchy) {

        var actionTaken = {
            contact: {
                userId: null,
                name: rName ? rName : null,
                personEmailId: rEmailId ? rEmailId : null,
                mobileNumber: rmobileNumber ? rmobileNumber : null,
            },
            actionType: type,
            suggestion: suggestion ? suggestion : null,
            insightSource: source,
            suggestionIndexValue: index,
            self_hierarchy: self_hierarchy
        }
        $http.post("/insights/action/taken", { actionTaken: actionTaken })
            .success(function(response) {
                if (response.SuccessCode) {
                    // console.log(response);
                }
            });
    }

    $scope.updateInProcessValue = function(inProcess,email){

        if(isNumber(inProcess)){
            var obj = {
                type:'inProcess',
                value:inProcess,
                email:email
            }

            $http.post('/contacts/update/value/type',obj)
                .success(function(response){
                    if(response){
                    } else {
                        toastr.error("Error! Please try again later")
                    }
                });
        } else {
            //toastr.error("Please enter a valid number")
        }
    }

    // $scope.updateRelation = function(email,value) {

    //     var relationKey = 'decisionmaker_influencer';
    //     var reqObj = {email: email, type: value, relationKey: relationKey};

    //     $http.post('/contacts/update/relationship/type/by/email', reqObj)
    //         .success(function (response) {
    //             if(response.SuccessCode){
    //                 paintRevenueRiskTable('self',limitGlobal);
    //             }
    //         })
    // }

    $scope.updateRelation = function(contact, relation) {
        var value = null;
        var dm = 0;
        var inf = 0;
        var influencerCss = 'fa fa-share-alt grey-color';
        var decisionMakerCss = 'fa fa-user grey-color';

        if (relation == "influencer") {
            if (contact.relation != "influencer"){
                influencerCss = 'fa fa-share-alt orange-color';
                value = "influencer";
            }
        } else if (relation == "decision_maker") {
            if (contact.relation != "decision_maker"){
                decisionMakerCss = 'fa fa-user orange-color';
                value = "decision_maker";
            }
        }
        //update
        toastr.remove();
        $http.post('/insights/contact/relation', { contactId: contact.contactId, value: value })
            .success(function(data) {
                if (data.SuccessCode) {
                    // toastr.success("successfully updated")
                    contact.relation = value;
                    contact.decisionMakerCss = decisionMakerCss;
                    contact.influencerCss = influencerCss;
                } else {
                    // console.log(data);
                    toastr.error("try again")
                }
            })


    }

    $scope.viewEmail = function(item, flag){
        if(flag == 'yes')
            item.showEmailBody = item.showEmailBody ? false: true;
        if((item.description == null || item.fetchingProblem) && item.showEmailBody) {
            if(item.fetchingProblem) {
                item.description = null;
                item.emailFetch = true;
            }
            $http.get('/message/get/email/single/web?emailContentId=' + item.emailContentId + '&googleAccountEmailId=' + item.ownerEmailId)
                .success(function (response) {
                    if (response.SuccessCode) {
                        item.description = response.Data.data.replace(/\n/g, "<br />");
                        var temp = response.Data.data.replace(/\n/g, "<br />"); //to remove internal css
                        var start, end;
                        start = temp.indexOf("<style");
                        end = temp.indexOf("</style>");
                        end = end != -1 ? end + 8 : end;
                        var replace = temp.slice(start , end);
                        //item.description = item.description.replace(replace, "")

                        temp = item.description;
                        var index = temp.indexOf('/track/email/open/')
                        if(index != -1){
                            item.description = removeRelatasTrackImg(item.description, index)
                        }
                        item.description = $sce.trustAsHtml(item.description);
                        item.emailFetch = false;
                        item.fetchingProblem = false;
                    }
                    else {
                        item.description = response.Message || '';
                        item.emailFetch = false;
                        item.fetchingProblem = true;
                    }
                })
        }
        if(item.showEmailBody  && item.trackOpen){
            $scope.updateEmailOpen(item.ownerEmailId,item.trackId,item.userId);
        }
    }

    $scope.maxSize = 5;
});

reletasApp.controller('selfPagination',function ($scope,sharedServiceSelection) {
    $scope.pageChanged = function(getFor) {
        sharedServiceSelection.paginationBroadcast(getFor,$scope.currentPage)
    };
})

reletasApp.controller('teamPagination',function ($scope,sharedServiceSelection) {
    $scope.pageChanged = function(getFor) {
        sharedServiceSelection.paginationBroadcast(getFor,$scope.currentPage)
    };
})

angular.element(document).ready(function(){
//var pageloadCount = 0;
//function loadModal($http) {

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        'positionClass': 'toast-top-full-width',
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "3000",
        "hideDuration": "1000",
        "timeOut": "8000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
});

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue){

    if(parameterValue instanceof Array)
        if(parameterValue.length > 0)
            parameterValue = parameterValue.join(",")
        else
            parameterValue = null

    if(parameterValue != undefined && parameterValue != null){
        if(baseUrl.indexOf("?") == -1)
            baseUrl += "?"
        else
            baseUrl += "&"
        baseUrl+= parameterName + "=" + parameterValue
    }

    return baseUrl
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function imageExists(url){
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status != 404;
}

function getInteractionIconType(interactionType) {
    switch (interactionType){
        case 'meeting': return 'fa fa-calendar-check-o margin0';break;
        case 'email': return 'fa fa-envelope margin0';break;
        case 'call': return 'fa fa-phone margin0';break;
        case 'sms': return 'fa fa-reorder margin0';break;
        case 'twitter': return 'fa fa-twitter-square margin0';break;
    }
}

function removeRelatasTrackImg(data, index){
    var temp = data;
    var f = temp.lastIndexOf('<img style=', index);
    var e = index + temp.substring(index).indexOf('\">');
    e = e != -1 ? e + 2 : e;
    var slice = temp.slice(f, e)

    return temp = temp.replace(slice, "")
    //return temp = temp;
}