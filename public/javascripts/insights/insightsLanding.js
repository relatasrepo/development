var reletasApp = angular.module('reletasApp', ['ngRoute', 'ngSanitize', 'angular-loading-bar','ngLodash','datatables']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope) {
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent, yourNetwork, extendedNetwork, forCompanies) {

        var str = searchContent;

        if (typeof str == 'string' && str.length > 0) {
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
        } else toastr.error("Please enter search content")
    };
});

var timezone;

function getParams(url) {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while (match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var contextEmailId = getParams(window.location.href).context;
var showContext = true;
if (!checkRequired(contextEmailId)) {
    showContext = false;
}

reletasApp.factory('teamMembers', function($http, $q) {
    var data = [],
        lastRequestFailed = true,
        promise;
    return {
        getTeamMembers: function() {
            if (!promise || lastRequestFailed) {
                // $http returns a promise, so we don't need to create one with $q
                promise = $http.get('/company/members')
                    .then(function(res) {
                        lastRequestFailed = false;
                        data = res.data;
                        return data;
                    }, function(res) {
                        return $q.reject(res);
                    });
            }
            return promise;
        }
    }
});

reletasApp.service('share', function() {
    return {
        setCompanyId:function(companyId){
            this.companyId = companyId;
        }
    }
});

reletasApp.factory('sharedServiceSelection', function($rootScope,$http,teamMembers) {
    var sharedService = {};

    sharedService.message = '';

    sharedService.prepForBroadcast = function(msg) {

        this.message = msg;
        this.broadcastItem();

        //Save hierarchy selection in the session

        teamMembers.getTeamMembers()
            .then(function(companyData) {

                var obj = companyData.Data.companyMembers;
                var companyArr = Object.keys(obj).map(function(k) {
                    return obj[k]
                });

                var url = '/session/hierarchy/selection';
                var hierarchyList = [];
                hierarchyList.push(companyData.Data.userId);
                url = fetchUrlWithParameter(url, "companyMembers", companyArr);

                if (msg == 'self') {
                    url = fetchUrlWithParameter(url, 'hierarchyList', hierarchyList);
                    url = fetchUrlWithParameter(url, 'getFor', 'self');
                } else if (msg == 'team') {

                    if(companyData.Data.teamMembers.length<=0){
                        //$scope.disableHierarchyButton = true
                        toastr.error("No-one reporting to you.")
                    }
                    var index = companyData.Data.teamMembers.indexOf(companyData.Data.userId);

                    if (index > -1) {
                        companyData.Data.teamMembers.splice(index, 1);
                    }
                    url = fetchUrlWithParameter(url, 'teamMembers', companyData.Data.teamMembers);
                    url = fetchUrlWithParameter(url, 'getFor', 'team');
                }
                $http.post(url)
            })
    };

    sharedService.broadcastItem = function() {
        $rootScope.$broadcast('handleBroadcast');
    };

    return sharedService;
});

reletasApp.controller("logedinUser", function($scope, $http, share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function(response) {
            $(".seeMoreIcon").addClass("disabled-see-more")
            if (response.SuccessCode) {
                $scope.l_usr = response.Data;
                $scope.firstName = response.Data.firstName
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.profilePicUrl = '/getImage/'+response.Data._id+'/'+new Date().toISOString()

                identifyMixPanelUser(response.Data);
                if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                    timezone = response.Data.timezone.name;
                }
                share.lUseEmailId = response.Data.emailId
                share.setCompanyId(response.Data.companyId)
            } else {

            }
        }).error(function(data) {

        })
});

reletasApp.controller('selfOrCompany', function($scope,$http,$rootScope,sharedServiceSelection,share) {

    $scope.selectHierachy = function(selection) {
        if (selection == 'team') {
            $scope.selectedHierarchy = 'col-md-6 btn-green-xlg insights-hierarchy';
            $scope.selectedSelf = 'col-md-6 btn-green-hover-xlg insights-hierarchy';

        } else if (selection == 'self') {
            $scope.selectedHierarchy = 'col-md-6 btn-green-hover-xlg insights-hierarchy';
            $scope.selectedSelf = 'col-md-6 btn-green-xlg insights-hierarchy';
        }

        sharedServiceSelection.prepForBroadcast(selection);
    }

    $rootScope.currency = '';

    share.setCompanyId = function (companyId) {
        if(companyId){
            $http.get('/corporate/company/' + companyId)
                .success(function (companyProfile) {
                    if(companyProfile.currency){
                        $rootScope.currency = companyProfile.currency
                    }
                });
        }
    }

});

reletasApp.controller('revenue_at_risk', function($scope,$http,$rootScope,sharedServiceSelection,teamMembers,DTOptionsBuilder,DTColumnDefBuilder,lodash,$controller,share) {
    $scope.$on('handleBroadcast', function() {
        $scope.message = sharedServiceSelection.message;
        paintRevenueAtRisk($scope.message);
    });

    $scope.selectedSelf = 'col-md-6 insights-hierarchy btn-green-xlg';
    $scope.selectedHierarchy = 'col-md-6 insights-hierarchy btn-green-hover-xlg';

    var navigation = getParams(window.location.href);
    $scope.hideUpdatePopUp = false;
    if(navigation.navigatedFrom == "todayLanding")
        $scope.hideUpdatePopUp = true;

    paintRevenueAtRisk('self');

    function paintRevenueAtRisk(message) {

        teamMembers.getTeamMembers()
            .then(function(companyData) {

                var obj = companyData.Data.companyMembers;
                var companyArr = Object.keys(obj).map(function(k) {
                    return obj[k]
                });

                var url = '/insights/revenue/risk/graph';
                var hierarchyList = [];
                hierarchyList.push(companyData.Data.userId);
                url = fetchUrlWithParameter(url, "companyMembers", companyArr);

                $scope.selection = 'Self';

                $scope.hide = false;

                if (message == 'self') {
                    url = fetchUrlWithParameter(url, 'hierarchyList', hierarchyList);
                    url = fetchUrlWithParameter(url, 'getFor', 'self');
                } else if (message == 'team') {

                    if(companyData.Data.teamMembers.length<=0){
                        //$scope.disableHierarchyButton = true
                        toastr.error("No-one reporting to you.")
                    }

                    $scope.hide = true;
                    $scope.selection = 'My Hierarchy'
                    var index = companyData.Data.teamMembers.indexOf(companyData.Data.userId);

                    if (index > -1) {
                        companyData.Data.teamMembers.splice(index, 1);
                    }
                    url = fetchUrlWithParameter(url, 'hierarchyList', companyData.Data.teamMembers);
                    url = fetchUrlWithParameter(url, 'getFor', 'team');
                }

                $http.get(url)
                    .success(function(res) {
                        if (res.SuccessCode) {

                            $scope.revenueAtRiskSuggestion = 10;

                            if(res.Data.length<10){
                                $scope.revenueAtRiskSuggestion = res.Data.length;
                            }

                            $scope.allContactsValue = typeof numberWithCommas(res.allContactsValue) != "undefined" ? numberWithCommas(res.allContactsValue) : 0;
                            $scope.valueAtRisk = typeof numberWithCommas(res.valueAtRisk) != "undefined" ? numberWithCommas(res.valueAtRisk) : 0;
                            $scope.revenueAtRiskPercentage = Math.round((res.valueAtRisk / res.allContactsValue)*100) +'%';

                            $scope.revenueAtRiskNumber = Math.round((res.valueAtRisk / res.allContactsValue)*100)

                            //var companyContactValue = res.companyRevenueRisk.companyContactValue;
                            //var companyValueAtRisk = res.companyRevenueRisk.companyValueAtRisk;
                            $scope.companyRevenueAtRisk = Math.round((res.companyRevenueRisk.companyValueAtRisk / res.companyRevenueRisk.companyContactValue)*100)

                            if(isNaN(Math.round((res.valueAtRisk / res.allContactsValue)*100))){
                                $scope.revenueAtRiskPercentage = 0 +'%'
                                $scope.revenueAtRiskNumber = 0
                            }

                            if(isNaN($scope.companyRevenueAtRisk)){
                                $scope.companyRevenueAtRisk = 0 + '%'
                            } else {
                                $scope.companyRevenueAtRisk = $scope.companyRevenueAtRisk + '%'
                            }

                            var companies = lodash.map(res.Data, 'company');

                            //Inefficient way to select only those companies which are shown on the Revenue
                            //at risk graph on the Insights landing page. Session is set for the companies in the graph.

                            if(message == 'self') {
                                //$scope.saveCompaniesToSession(companies);
                                $scope.getCompanies(companies);
                            }

                            var interGrowth = lodash.map(res.Data,'interactionGrowth');
                            var values = lodash.map(res.Data,'contactValue');

                            var chart = c3.generate({
                                bindto: '#revenueRiskChart',
                                data: {
                                    json: {
                                        'Value': values,
                                        'Interaction Growth': interGrowth
                                    },
                                    axes: {
                                        'Value': 'y',
                                        'Interaction Growth': 'y2'
                                    },
                                    type: 'bar',
                                    colors: {
                                        'Value': '#0C948A',
                                        'Interaction Growth': '#D53D28'
                                    },
                                    onclick: function() {
                                        window.location = '/insights/revenue/risk/details';
                                    }
                                },
                                axis: {
                                    x: {
                                        type: 'category',
                                        categories: companies
                                    },
                                    y2: {
                                        show: true,
                                        label: {
                                            text: 'Interaction Growth',
                                            position: 'outer-middle'
                                        }
                                    },
                                    y: {
                                        show: true,
                                        label: {
                                            text: 'Value( '+ $rootScope.currency +' )',
                                            position: 'outer-middle'
                                        }
                                    }
                                }
                            });

                            var svg = d3.select(document.getElementById('revenueRiskChart'));

                            //svg.select(".c3-axis-x .tick").selectAll("text").remove();

                            svg.selectAll(".c3-axis-x .tick").each( function(d) {
                                var p = d3.select(this);

                                p.append("svg:image")
                                    .attr("x", -9)
                                    .attr("y", 3)
                                    .attr("dy", ".35em")
                                    .attr("width", 20)
                                    .attr("height", 20)
                                    .attr("xlink:href",
                                        function (d) {
                                            return "https://logo.clearbit.com/"+ companies[d] +".com" ;
                                        }
                                    );
                            });

                            var chart = c3.generate({
                                bindto:"#gaugeChartRevenue",
                                data: {
                                    columns: [
                                        ['Company Wide', $scope.companyRevenueAtRisk],
                                        ['Self', Math.round((res.valueAtRisk / res.allContactsValue)*100)],
                                    ],
                                    colors: {
                                        'Company Wide':'#AAAAAA',
                                        Self:'#D53D28'
                                    },
                                    color: function(d,v){
                                        return d;
                                        if(v.id === "Company Wide"){
                                            //d = '#77777';
                                        }
                                    },
                                    type: 'gauge',
                                },
                                gauge: {
                                    width: 15,
                                    label: {
                                        format: function(d,value,selfOrTeam) {
                                            if(selfOrTeam == 'Self'){
                                                //return d.toFixed(1) + '%';
                                                return '';
                                            }
                                        },
                                        show: true
                                    },
                                    max: 100, // 100 is default
                                },
                                size: {
                                    height: 120
                                }
                            });
                        }
                    });
            });
    };

    $scope.goToDetails = function(){
        mixpanel.track('Insights Suggestion - Go to Revenue at Risk Details');
        window.location = '/insights/revenue/risk/details'
    }

    //Inefficient way to select only those companies which are shown on the Revenue
    //at risk graph on the Insights landing page. Session is set for the companies in the graph.
    $scope.saveCompaniesToSession = function(list){

        var url = fetchUrlWithParameter('/get/companies/list','list',list)
        $http.post(url)
    }

    /*Modal*/

    $scope.textload = "Loading data. Please wait..";
    $scope.textProcess = function(){return $scope.textload}
    var vm = this;
    vm.persons = [];
    vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10).withLanguage({
            "oPaginate": {
                "sNext": "»",
                "sPrevious": "«"
            }
        })
        .withOption('columns', [{ "orderDataType": "dom-value" },null,null,{ "orderDataType": "dom-value" },null])
        .withOption('order', [[ 0, "desc" ]])
        .withOption("bDestroy",true)
        .withOption('oLanguage', {
            "sEmptyTable": $scope.textProcess,
            "sLengthMenu": "Show _MENU_ for page",
            //"sProcessing": "DataTables is currently busy",
            //"bProcessing":false,
            "processing":true
        });

    var columnOptions = function(){

        vm.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4),
            DTColumnDefBuilder.newColumnDef(5),
            DTColumnDefBuilder.newColumnDef(6),
        ];

        vm.dtInstance = {};

        vm.dtInstanceCallback = dtInstanceCallback;

        function dtInstanceCallback(dtInstance) {
            vm.dtInstance = dtInstance;
        }

        $scope.show_loadMore = false;

        $scope.textload = "Loading Data. Please wait..";
        if (vm.dtInstance && vm.dtInstance.DataTable) {
            vm.dtInstance.DataTable.clear().draw();
        }
    }

    paintInitDataTable();

    function paintInitDataTable () {
        columnOptions();

        $scope.getCompanies = function(list){
            var url = fetchUrlWithParameter('/insights/update/init','list',list)
            $http.get(url)
                .success(function (response) {
                if (response.SuccessCode) {

                    var len = response.Data.length;
                    var obj = [];
                    for (var i = 0; i < len; i++) {

                        var preSelectedValue = 'black-color';
                        if(response.Data[i].inProcess){
                        } else {
                            preSelectedValue = 'orange-color'
                        }

                        var influencerCss = 'fa fa-share-alt grey-color';
                        var decisionMakerCss = 'fa fa-user grey-color';
                        if (response.Data[i].contactRelation && response.Data[i].contactRelation.decisionmaker_influencer == 'influencer') {
                            influencerCss = 'fa fa-share-alt orange-color';
                        } else if (response.Data[i].contactRelation && response.Data[i].contactRelation.decisionmaker_influencer == 'decisionmaker') {
                            decisionMakerCss = 'fa fa-user orange-color';
                        }

                        obj.push({
                            name: response.Data[i].name || response.Data[i].email,
                            company: response.Data[i].company,
                            designation: response.Data[i].designation,
                            contactId: response.Data[i].contactId,
                            email: response.Data[i].email,
                            inProcessValue: response.Data[i].inProcess || 1000,
                            decisionMakerCss: decisionMakerCss,
                            influencerCss: influencerCss,
                            preSelectedValue: preSelectedValue,
                            interactionsCount: response.Data[i].interactionCount,
                            lastInteracted: response.Data[i].lastInteracted?moment(response.Data[i].lastInteracted).format("DD MMM YYYY") : '',
                            inProcessValEmpty: response.Data[i].inProcess ? true : false
                        })
                    }

                    obj = lodash.uniqBy(obj, function (item) {
                        return item.company;
                    });

                    if (obj.length > 0) {
                        vm.persons = obj
                    }
                    else {
                        $scope.textload = "There is no Data";
                        $scope.interactions = [];
                        if (vm.dtInstance && vm.dtInstance.DataTable) {
                            vm.dtInstance.DataTable.clear().draw();
                        }
                    }
                } else {

                    $('#myModal').modal('hide');
                    $scope.textload = "There is no Data";
                    $scope.interactions = [];
                    if (vm.dtInstance && vm.dtInstance.DataTable) {
                        vm.dtInstance.DataTable.clear().draw();
                    }
                }
            })
        }
    }

    $scope.updateInProcessValue = function(inProcess,email){
        if(isNumber(inProcess)){
            var obj = {
                type:'inProcess',
                value:inProcess,
                email:email
            }

            $http.post('/contacts/update/value/type',obj)
                .success(function(response){
                    if(response){
                    } else {
                        toastr.error("Error! Please try again later")
                    }
                });
        } else {
            //toastr.error("Please enter a valid number")
        }
    }

    $scope.updateRelation = function(email,value) {

        var relationKey = 'decisionmaker_influencer';
        var reqObj = {email: email, type: value, relationKey: relationKey};

        $http.post('/contacts/update/relationship/type/by/email', reqObj)
            .success(function (response) {
                if(response.SuccessCode){
                    paintRevenueAtRisk('self');
                }
            })
    }

    $scope.saveUpdateDataInit = function(remind30days,remindNever,inProcessValue){

        var reminder = null;

        if(remind30days) {
            reminder = moment().add(30, "days").toDate()
        } else if(remindNever) {
            reminder = 'never';
        }

        var values = lodash.map(inProcessValue,function(d){
            var k = {
                inProcess:d.inProcessValue,
                email:d.email
            }
            return k;
        });

        var counter = values.length;
        for(var i=0;i<values.length;i++){
            $scope.updateInProcessValue(values[i].inProcess,values[i].email)
            counter --;
        }

        var obj = {
            insightsRemindIn: reminder
        }

        $http.post('/insights/init/save',obj)
            .success(function(response){
                if(response && counter<=0){
                    sharedServiceSelection.prepForBroadcast('self');
                    mixpanel.track('Insights Pop Up - Saved Values');
                    $('#myModal').modal('hide');
                }
            });
    }
});

reletasApp.controller('losing_touch&company_risk', function($scope, $http, teamMembers, sharedServiceSelection) {

    $scope.$on('handleBroadcast', function() {
        paintGraphLosingTouch(sharedServiceSelection.message);
        paintGraphCompanyRelationship(sharedServiceSelection.message);
    });

    paintGraphLosingTouch('self'); //Default paint for self
    paintGraphCompanyRelationship('self');
    $scope.suggestionCRR = true;

    function paintGraphLosingTouch(message) {

        teamMembers.getTeamMembers()
            .then(function(companyData) {
                var obj = companyData.Data.companyMembers;
                var companyArr = Object.keys(obj).map(function(k) {
                    return obj[k]
                });
                var url = fetchUrlWithParameter('/insights/losing/touch/', "companyMembers", companyArr);

                $scope.hide = false;

                $scope.selection = 'Self';
                if (message == 'self') {
                    var hierarchyList = [];
                    hierarchyList.push(companyData.Data.userId);
                    url = fetchUrlWithParameter(url, 'hierarchyList', hierarchyList);
                } else if (message == 'team') {
                    $scope.hide = true;
                    $scope.selection = 'My Hierarchy';
                    url = fetchUrlWithParameter(url, 'hierarchyList', companyData.Data.teamMembers);
                }

                $http.get(url)
                    .success(function(res) {
                        if (res.SuccessCode) {

                            var losingTouchSelfAverage = (res.Data.self.selfLosingTouch / res.Data.self.selfTotalCount) * 100;
                            var losingTouchCompanyAverage = (res.Data.company.companyLosingTouch / res.Data.company.companyTotalContacts) * 100;

                            $scope.losingTouchCompanyAverage = Math.round(losingTouchCompanyAverage) + '%';
                            $scope.losingTouchSelfAverage = Math.round(losingTouchSelfAverage) + '%';
                            if(isNaN(losingTouchCompanyAverage)){
                                $scope.losingTouchCompanyAverage = 0 + '%'
                            }
                            if(isNaN(losingTouchSelfAverage)){
                                $scope.losingTouchSelfAverage = 0 + '%'
                            }

                            if (losingTouchSelfAverage > losingTouchCompanyAverage) {
                                $scope.takeAction = "You are above the company average of ";
                                $scope.warningCss = 'orange-color fs-20';
                            } else {
                                $scope.takeAction = "You are faring better than the company average of";
                                $scope.improveAction = "Improve this number by getting in touch with 5 people today.";
                                $scope.warningCss = 'green-color fs-20';
                            }

                            $scope.inTouch = res.Data.self.selfTotalCount - res.Data.self.selfLosingTouch;
                            $scope.losingTouch = res.Data.self.selfLosingTouch;
                            $scope.losingTouch = 20;
                            var donutCenter = $scope.losingTouch;

                            if (!$scope.inTouch) {
                                $scope.losingTouch = 0;
                            }

                            $scope.donutCenter = 5;
                            if(donutCenter<5){
                                $scope.donutCenter = donutCenter;
                            }

                            $scope.contactsCount = res.Data.contactsCount;

                            if($scope.inTouch === 0 && $scope.losingTouch === 0 ){
                                $scope.losingTouch = 0
                            }

                            var noLT = 0;
                            if($scope.losingTouch == 0){
                                noLT = 1;
                            }

                            var chart = c3.generate({
                                bindto: '#chartLosingTouch',
                                data: {
                                    columns: [
                                        ['In Touch', $scope.inTouch],
                                        ['Losing Touch', $scope.losingTouch],
                                        ['No LT', noLT]
                                    ],
                                    type: 'donut',
                                    onclick: function() {
                                        window.location = '/insights/losing/touch/details'
                                    },
                                    colors: {
                                        'In Touch': '#0C948A',
                                        'Losing Touch': '#D53D28',
                                        'No LT':'#e0e0e0'
                                    }
                                },
                                tooltip: {
                                    format: {
                                        value: function (value, ratio, id) {
                                            if(noLT)
                                                return;
                                            return value;
                                        }
                                    }
                                },
                                donut: {
                                    title: donutCenter,
                                    width: 35,
                                    label: {
                                        format: function(value, ratio, id) {
                                            if(noLT)
                                                return;
                                            return value;
                                        },
                                        show:true
                                    }
                                },
                            });
                            $('.c3-legend-item-No-LT').hide()
                            //Gauge Chart
                            var chart = c3.generate({
                                bindto:"#gaugeChart",
                                data: {
                                    columns: [
                                        ['Company Wide', Math.round(losingTouchCompanyAverage)],
                                        ['Self', Math.round(losingTouchSelfAverage)],
                                    ],
                                    colors: {
                                        'Company Wide':'#AAAAAA',
                                        Self:'#D53D28'
                                    },
                                    color: function(d,v){
                                        return d;
                                        if(v.id === "Company Wide"){
                                            //d = '#77777';
                                        }
                                    },
                                    type: 'gauge',
                                    legend: {
                                        position: 'right'
                                    }
                                },
                                gauge: {
                                    width: 15,
                                    label: {
                                        format: function(d,value,selfOrTeam) {
                                            if(selfOrTeam == 'Self'){
                                                return  ' ';
                                            }
                                        },
                                        show: true
                                    },
                                max: 100, // 100 is default
                                },
                                size: {
                                    height: 120
                                }
                            });
                        }
                    })
            });
    };

    function paintGraphCompanyRelationship(message) {
        var jsonfile = [];
        var url;
        var users = [];
        teamMembers.getTeamMembers()
            .then(function(data) {
                if (message == 'self') {
                    users = []
                    users = data.Data.userId;
                    var detailsFor = 'self';
                    $scope.suggestionCRR = true;
                    $scope.msgFor = 'Your';
                } else if (message == 'team') {
                    users = []
                    users = data.Data.teamMembers;
                    var detailsFor = 'hierarchy';
                    $scope.suggestionCRR = false;
                    $scope.msgFor = 'Your Team';
                }

                $http.get('/insights/company/relationship/risk', { params: { userIds: users, detailsFor:detailsFor } }).then(function(response) {
                    if (response.data.SuccessCode) {
                        var account = response.data.Data;
                        $scope.companiesForExtendedNetwork = [];
                        var cont = []
                        var companyLimit = 20;
                        var flag = 0
                        account.accounts.forEach(function(ele) {
                            flag++;
                            if(flag <= companyLimit){
                                jsonfile.push({
                                    "company": ele.company,
                                    "value": ele.contactValues,
                                    "interactionGrowth": ele.interaction,
                                    "numberOfContacts": ele.contacts,
                                    "influencer": ele.influencer,
                                    "decisionMaker": ele.decisionMaker
                                });
                                cont.push(ele.contacts);
                            }
                        });

                        $scope.noOfCompanies = account.accounts.length <= 10 ? account.accounts.length : 10;

                        cont.sort(function(a,b){
                            return b - a;
                        });

                        var selfRateOfInteraction = 0;
                        if (account.intraction != 0)
                            selfRateOfInteraction = (account.intraction / account.value);

                        var obj = data.Data.companyMembers;
                        var companyArr = Object.keys(obj).map(function(k) {
                            return obj[k]
                        });

                        if (message == 'self'){
                            $scope.extendedNetwork = 0;
                            $http.get('/dashboard/extended/network/from/all/companies').then(function(respo) {
                                if (respo.data.SuccessCode) {
                                    for(var i=0;i<respo.data.Data.length;i++){
                                       $scope.extendedNetwork += respo.data.Data[i].extendedNetwork;
                                        if(respo.data.Data[i].extendedNetwork)
                                            $scope.companiesForExtendedNetwork.push(respo.data.Data[i].company);
                                    }

                                    var companies = '';
                                    for(var j=0;j<$scope.companiesForExtendedNetwork.length;j++) {
                                        if(j < $scope.companiesForExtendedNetwork.length - 1)
                                            companies += $scope.companiesForExtendedNetwork[j] + '+';
                                        else
                                            companies += $scope.companiesForExtendedNetwork[j];
                                    }
                                    if($scope.extendedNetwork > 0)
                                        $scope.urlForExtendedNetwork = "/contact/connect?searchContent=" + companies + "&yourNetwork=false&extendedNetwork=true&forCompanies=true&forInsights=false&forDashboard=true";
                                    else{
                                        $http.get('/dashboard/extended/network/from/interacted/companies')
                                            .success(function (response) {
                                                var extendedContactsNumber = 0;
                                                if (response.SuccessCode) {
                                                    var contacts = response.Data;
                                                    var extendedNetcompanies = [];
                                                    for(var i=0; i<contacts.length;i++){
                                                        if(i == 10 && extendedContactsNumber != 0)
                                                            break;
                                                        extendedContactsNumber += contacts[i].extendedNetwork;
                                                        if(contacts[i].extendedNetwork > 0)
                                                            extendedNetcompanies.push(contacts[i].company);

                                                    }
                                                }
                                                if(extendedContactsNumber != 0){
                                                    $scope.extendedNetwork = extendedContactsNumber > 10 ? 10 : extendedContactsNumber;
                                                    var companies = '';
                                                    for(var j=0;j<extendedNetcompanies.length;j++) {
                                                        if(j < extendedNetcompanies.length - 1)
                                                            companies += extendedNetcompanies[j] + '+';
                                                        else
                                                            companies += extendedNetcompanies[j];
                                                    }
                                                    $scope.urlForExtendedNetwork = "/contact/connect?searchContent=" + companies + "&yourNetwork=false&extendedNetwork=true&forCompanies=true&forInsights=false&forDashboard=true&forDashboardTopCompanies=true";
                                                }
                                                else{
                                                    $scope.extendedNetwork = 0;
                                                    $scope.urlForExtendedNetwork = '#';
                                                }
                                            });

                                    }
                                }
                            });
                        }

                        $http.get('/insights/company/relationship/risk/company', { params: { userIds: companyArr } }).then(function(resp) {
                            $scope.companyRelationshipAverage = 0;
                            if (resp.data.SuccessCode) {
                                if (resp.data.Data.interaction !== 0)
                                    var companyRelationshipAverage = ((resp.data.Data.interaction / resp.data.Data.value));
                                $scope.interactionPerDollar = 0;

                                if(selfRateOfInteraction != 0)
                                  $scope.interactionPerDollar = (((selfRateOfInteraction - companyRelationshipAverage) / selfRateOfInteraction ) * 100).toFixed(0);

                                var sign = Math.sign($scope.interactionPerDollar);
                                if(sign == 1){
                                    $scope.avg = 'greater';
                                    $scope.warningCssForCR = 'green-color fs-20';
                                }
                                if(sign == -1 || sign == 0){
                                    $scope.interactionPerDollar = Math.abs($scope.interactionPerDollar);
                                    $scope.avg = 'less';
                                    $scope.warningCssForCR = 'orange-color fs-20';
                                }
                                if(isNumber($scope.interactionPerDollar)){
                                    $scope.interactionPerDollar += '%';
                                } else {
                                    $scope.interactionPerDollar = 0 + '%';
                                }
                            }
                        });

                        jsonfile = jsonfile.sort(function(a, b) {
                            return a.value - b.value
                        });

                        var maxContacts = Math.max.apply(Math, jsonfile.map(function(o) {
                            return o.numberOfContacts;
                        }));
                        var minContacts= Math.min.apply(Math, jsonfile.map(function(o) {
                            return o.numberOfContacts;
                        }));

                        var numberOfContacts = jsonfile.map(function(i) {
                            return scaleBetween(i.numberOfContacts, minContacts, maxContacts)
                        });

                        var chart = c3.generate({
                            bindto: '#chartCompanyRelationshipRisk',
                            tooltip: {
                                contents: function(d, defaultTitleFormat, defaultValueFormat, color) {
                                    var company = jsonfile[d[0].index].company;
                                    var contactCount = jsonfile[d[0].index].numberOfContacts;
                                    var contactValue = jsonfile[d[0].index].value;
                                    var interaction = jsonfile[d[0].index].interactionGrowth;
                                    var influencer = jsonfile[d[0].index].influencer;
                                    var decisionMaker = jsonfile[d[0].index].decisionMaker;

                                    var companyData = "<table class='data-c3-table'><tr style='text-align:left;'><th colspan='2'>" + company + "</th></tr><td>Contacts: <b>" + contactCount + "</b>  <i class='fa fa-user' style='color: #777777!important'></i>&nbsp<b>" + decisionMaker + "</b> <i class='fa fa-share-alt' style='color: #777777!important'></i>&nbsp<b>" + influencer + "</b>    </td></tr><tr style='text-align:left;'> <td>Contact value: <b>" + contactValue + "</b></td></tr><tr style='text-align:left;'><td>Interactions: <b>" + interaction + "</b></td></tr></table>"
                                    return companyData;
                                }
                            },
                            point: {
                                // r:15,
                                r: function(d) {
                                    return numberOfContacts[d.index];
                                },
                                focus: {
                                    expand: {
                                        enabled: false
                                    }
                                }
                            },
                            data: {
                                json: jsonfile,
                                x: 'value',
                                keys: {
                                    value: ['value', 'interactionGrowth'],
                                },
                                color: function (color, d) {
                                    color = '#D53D28';
                                    return color;
                                },
                                type: 'scatter',
                                onclick: function() {
                                    window.location = '/insights/company/relationship/risk/details'
                                }
                            },
                            axis: {
                                x: {
                                    label: {
                                        text: 'Values( INR )',
                                        position: 'outer-center'
                                    },
                                    tick: {
                                        fit: false
                                    }
                                },
                                y: {
                                    label: {
                                        text: 'Interactions',
                                        position: 'outer-middle'
                                    },
                                    tick: {
                                        fit: false
                                    }
                                }
                            },
                            legend: {
                                show: false
                            }
                        });
                    }
                })
            });
    }

    $scope.goToDetails = function(type){
        if(type == 'crr'){
            mixpanel.track('Insights Suggestion - Go to CRR Details');
            window.location = '/insights/company/relationship/risk/details'
        } else if(type == 'lt') {
            mixpanel.track('Insights Suggestion - Go to Losing Touch Details');
            window.location = '/insights/losing/touch/details'
        } else if(type == 'rr'){
            window.location = '/insights/revenue/risk/details'
        }
    }
})

reletasApp.controller('responsePending', function($scope, $http, teamMembers, sharedServiceSelection) {

    $scope.$on('handleBroadcast', function() {
        if(sharedServiceSelection.message == 'team') {
            $scope.hide = true;
            //paintGraphResponsePending('today');
            //paintGraphResponsePending('last15Days')
        }
    });

    $scope.hide = false;
    $scope.todayExternalMails = 0;
    paintGraphResponsePending('today');
    paintGraphResponsePending('last15Days')

    function paintGraphResponsePending(chartFor) {
        var days = 14;
        var chartId = '#chartResponsePending15Days';

        if(chartFor == 'today'){
            var days = 0;
            var chartId = '#chartResponsePendingToday';
        }

        $http.get('/insights/your/responses/pending', { params: { days:days, infoFor:"graph" } }).then(function(resp) {
            if (resp.data.SuccessCode) {
                var responded  = resp.data.Data.responded ? resp.data.Data.responded: 0;
                var internal = resp.data.Data.internal.length ? resp.data.Data.internal.length: 0;
                var external = resp.data.Data.external.length ? resp.data.Data.external.length: 0;
                var totalMails = internal + external + responded;
                var noMails = 0;
                if(internal == 0 && external == 0 && responded == 0)
                    noMails = 1;

                if(chartFor == 'today')
                    $scope.todayExternalMails = external;

                var chart = c3.generate({
                    bindto: chartId,
                    data: {
                        columns: [
                            ['Responded', responded],
                            ['Not Responded(Internal)', internal],
                            ['Not Responded(External)', external],
                            ['No Mails', noMails]
                        ],

                        type: 'donut',
                        onclick: function(a) {
                            var days = 14;
                            if(chartFor == 'today')
                                days = 0;
                            var filter = 'external';
                            if(a.id == 'Not Responded(Internal)')
                                filter = 'internal';
                            if(a.id == 'Not Responded(Internal)' || a.id == 'Not Responded(External)')
                                window.location = '/insights/your/responses/pending/details?filter='+filter+'&days='+days;
                        },
                        colors: {
                            'Responded':'#0C948A',
                            'Not Responded(Internal)': '#F88B4F',
                            'Not Responded(External)': '#D53D28',
                            'No Mails':'#e0e0e0'
                        }
                    },
                    tooltip: {
                        format: {
                            value: function (value, ratio, id) {
                                if(noMails)
                                    return;
                                return value;
                            }
                        }
                    },
                    donut: {
                        title: totalMails,
                        width: 35,
                        label: {
                            format: function(value, ratio, id) {
                                if(noMails)
                                    return;
                                return value;
                            },
                            show:true
                        }
                    },
                });
                $('.c3-legend-item-No-Mails').hide() //remove 'No Mail' legend from list
            }
        });
    };

})

angular.element(document).ready(function ($http) {
//var pageloadCount = 0;
//function loadModal($http) {

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        'positionClass':'toast-top-full-width',
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "3000",
        "hideDuration": "1000",
        "timeOut": "8000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    //pageloadCount++
    //if(pageloadCount === 0 || pageloadCount === 1){

    $http.get('/insights/get/insights/reminder')
        .success(function(response){

            var remindIn = new Date(response[0].insightsRemindIn)
            //$('#myModal').modal({backdrop: 'static', keyboard: false})

            var todaysDate = new Date();
            if(remindIn.getTime() > todaysDate.getTime())
            {
            } else if(remindIn.getTime() < todaysDate.getTime()){
                $('#myModal').modal({backdrop: 'static', keyboard: false})
            } else if(remindIn.getTime() == todaysDate.getTime()){
                $('#myModal').modal({backdrop: 'static', keyboard: false})
            }
        });
});

function getPercentage(val1, val2) {
    return (val1 / val2) * 100
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function convertRange( value, r1, r2 ) {
    return ( value - r1[ 0 ] ) * ( r2[ 1 ] - r2[ 0 ] ) / ( r1[ 1 ] - r1[ 0 ] ) + r2[ 0 ];
}

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue){

    if(parameterValue instanceof Array)
        if(parameterValue.length > 0)
            parameterValue = parameterValue.join(",")
        else
            parameterValue = null

    if(parameterValue != undefined && parameterValue != null){
        if(baseUrl.indexOf("?") == -1)
            baseUrl += "?"
        else
            baseUrl += "&"
        baseUrl+= parameterName + "=" + parameterValue
    }

    return baseUrl
}

function scaleBetween(unscaledNum, min, max) {
    var minAllowed = 5;
    var maxAllowed = 20;
    return (maxAllowed - minAllowed) * (unscaledNum - min) / (max - min) + minAllowed;
}