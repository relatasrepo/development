var reletasApp = angular.module('reletasApp', ['ngRoute', 'ngSanitize', 'angular-loading-bar','ngLodash','datatables','ui.bootstrap']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope) {
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent, yourNetwork, extendedNetwork, forCompanies) {

        var str = searchContent;

        if (typeof str == 'string' && str.length > 0) {
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
        } else toastr.error("Please enter search content")
    };
});
var timezone;
reletasApp.controller("logedinUser", function($scope, $http, share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function(response) {
            $(".seeMoreIcon").addClass("disabled-see-more")
            if (response.SuccessCode) {
                $scope.l_usr = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                identifyMixPanelUser(response.Data);
                if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                    timezone = response.Data.timezone.name;
                }
                share.lUseEmailId = response.Data.emailId
                $scope.uniqueName = response.uniqueName;

            } else {

            }
        }).error(function(data) {

    })
});

reletasApp.service('share', function() {
    return {}
});

reletasApp.factory('sharedServiceSelection', function($rootScope) {
    var sharedService = {};

    sharedService.message = '';

    sharedService.prepForBroadcast = function(msg) {
        this.message = msg;
        this.broadcastItem();
    };

    sharedService.broadcastItem = function() {
        $rootScope.$broadcast('handleBroadcast');
    };

    sharedService.paginationBroadcast = function (getPage) {
        //this.getFor = getFor;
        this.getPage = getPage;
        this.broadcastPaginationItem();
    };

    sharedService.broadcastPaginationItem = function () {
        $rootScope.$broadcast('handlePaginationBroadcast')
    }

    return sharedService;
});

function getParams(url) {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while (match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

reletasApp.controller('pagination',function ($scope,sharedServiceSelection) {
    $scope.pageChanged = function() {
        sharedServiceSelection.paginationBroadcast($scope.currentPage)
    };
})

var limitGlobal,currentPage,numberOfPages;
reletasApp.controller('responsePending', function($scope,$sce, $http, DTOptionsBuilder, DTColumnDefBuilder,lodash, sharedServiceSelection) {
    var inputFilter = getParams(window.location.href);

    $scope.textload = "Loading data. Please wait..";
    $scope.textProcess = function() {
        return $scope.textload
    }
    var vm = this;
    vm.persons = [];
    vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10).withLanguage({
            "oPaginate": {
                "sNext": "»",
                "sPrevious": "«"
            }
        })
        .withOption('columns', [{ "orderDataType": "dom-value" }, null, null, { "orderDataType": "dom-value" }, null])
        .withOption('order', [
            [1, "asce"]
        ])
        .withOption("bDestroy", true)
        .withOption('paging', false)
        .withOption('oLanguage', {
            "sEmptyTable": $scope.textProcess,
            "sLengthMenu": "Show _MENU_ for page",
            "processing": true
        });

    var columnOptions = function() {
        vm.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3).notSortable(),
            DTColumnDefBuilder.newColumnDef(4).notSortable(),
            DTColumnDefBuilder.newColumnDef(5).notSortable(),
            DTColumnDefBuilder.newColumnDef(6).notSortable(),
            //DTColumnDefBuilder.newColumnDef(7).notSortable()
        ];

        vm.dtInstance = {};

        vm.dtInstanceCallback = dtInstanceCallback;

        function dtInstanceCallback(dtInstance) {
            vm.dtInstance = dtInstance;
        }

        $scope.show_loadMore = false;

        var skip = 0;
        var limit = 100;

        $scope.textload = "Loading Data. Please wait..";
        if (vm.dtInstance && vm.dtInstance.DataTable) {
            vm.dtInstance.DataTable.clear().draw();
        }
    };

    $scope.noDataToShow = false;
    $scope.pageName = 'Last 15 days';
    if(inputFilter.days == 0)
        $scope.pageName = 'Today';
    $scope.contactTypeFilter = inputFilter.filter;
    $scope.companyFilter = 'all';
    paintResponsePendingTable(inputFilter.filter, inputFilter.days, inputFilter.company, sharedServiceSelection.getPage);

    $scope.$on('handlePaginationBroadcast', function() {
        paintResponsePendingTable($scope.contactTypeFilter, inputFilter.days, $scope.companyFilter,sharedServiceSelection.getPage);
    });

    function paintResponsePendingTable(filter, days, company, getPage) {
        columnOptions();
        $scope.showCompanyList = true;
        if(filter == 'internal') {
            $scope.companyFilter = 'all';
            $scope.showCompanyList = false;
        }

        if(!getPage)
            getPage = 1;
        $http.get('/insights/your/responses/pending', {params : {infoFor:'detailsPage', filter:filter, days:days, company:company, getPage:getPage}}).then(function(respo) {
            if (respo.data.SuccessCode) {
                $scope.totalItems = respo.data.totalList;
                var data = respo.data.Data.explain;
                $scope.companies = [];
                respo.data.Data.companies.forEach(function(a){
                    $scope.companies.push({name: a.company, value: a.total});
                })

                $scope.showCompanyList = true;
                if(filter == 'internal'){
                    $scope.showCompanyList = false;
                }

                var len = data.length;
                var obj = [];
                vm.persons = [];
                if(len>0){
                    for (var i = 0; i < len; i++) {

                        var cImg, profilePic;
                        var influencerCss = 'fa fa-share-alt grey-color';
                        var decisionMakerCss = 'fa fa-user grey-color';

                        if (data[i].personId) {
                            cImg = '/getImage/' + data[i].personId;
                            profilePic = false;
                        } else {
                            profilePic = true;
                            cImg = data[i].personName ? data[i].personName.substr(0, 2).toUpperCase() : data[i].personEmailId.substr(0, 2).toUpperCase();
                        }

                        if (data[i].relation && data[i].relation == 'influencer') {
                            influencerCss = 'fa fa-share-alt orange-color';
                        } else if (data[i].relation && data[i].relation == 'decision_maker') {
                            decisionMakerCss = 'fa fa-user orange-color';
                        }
                        var readOn = null;
                        if(data[i].trackInfo){

                            //readOn = data[i].trackInfo.isRed?"Read on "+moment(data[i].trackInfo.lastOpenedOn).tz(timezone).format("DD MMM YYYY, h:mm a"):"Unread";
                        }
                        if(data[i].action == 'sender' && data[i].trackInfo && data[i].trackInfo.trackOpen){
                            var trackOpen = true;
                        }

                        obj.push({
                            company: data[i].company,
                            value: data[i].contactValue,
                            email:data[i].personEmailId,
                            interactionDate: data[i].interactionDate,
                            designation: data[i].designation,
                            contactName: data[i].personName ? data[i].personName : data[i].personEmailId,
                            relation: data[i].relation,
                            //suggestion: data[i].suggestion,
                            no_pic: profilePic,
                            contactImage: cImg,
                            decisionMakerCss:decisionMakerCss,
                            influencerCss:influencerCss,
                            contactId:data[i].contactId,
                            publicProfileUrl: typeof data[i].publicProfileUrl != "undefined" ? '/' + data[i].publicProfileUrl : '/',
                            index:i + 1,
                            mobileNumber:data[i].mobileNumber,
                            userId:data[i].userId,
                            title:data[i].title ? data[i].title : null,
                            refId:data[i].refId ? data[i].refId : null,
                            //reminder_notify:data[i].reminder_notify,
                            interactionId:data[i].interactionId,
                            emailContentId:data[i].emailContentId,
                            ownerEmailId:data[i].ownerEmailId,
                            description: data[i].description || null,
                            showEmailBody:false,
                            emailFetch:data[i].description ? false : true,
                            //remindToconnect:data[i].remindToconnect ? moment(data[i].remindToconnect).tz(timezone).format("DD MMM YYYY") : null,
                            fetchingProblem: false,
                            readOn:readOn,
                            mailType:data[i].mailType,
                            emailThreadId:data[i].emailThreadId,
                            trackId:data[i].trackId,
                            trackOpen:trackOpen ? trackOpen : null,
                            ownerId:data[i].ownerId
                        });
                    }
                    if (obj.length > 0) {
                        vm.persons = obj
                    } else {
                        $scope.textload = "You have no mail to respond";
                        $scope.interactions = [];
                        if (vm.dtInstance && vm.dtInstance.DataTable) {
                            vm.dtInstance.DataTable.clear().draw();
                        }
                        $scope.noDataToShow = true;
                    }

                } else {
                    $scope.textload = "You have no mail to respond";
                    $scope.interactions = [];
                    if (vm.dtInstance && vm.dtInstance.DataTable) {
                        vm.dtInstance.DataTable.clear().draw();
                    }
                    $scope.noDataToShow = true;
                }
            }
        });
    }

    $scope.filter = function(){
        paintResponsePendingTable($scope.contactTypeFilter, inputFilter.days, $scope.companyFilter,1);
    }

    $scope.updateEmailOpen = function(emailId,trackId,userId){
        if(checkRequired(emailId) && checkRequired(trackId) && checkRequired(userId)){
            $http.get('/track/email/open/'+emailId+'/track/id/'+trackId+'/user/'+userId)
                .success(function(response){
                });
        }
    };

    $scope.viewEmail = function(item, flag){
        if(flag == 'no')
            item.showEmailBody = item.showEmailBody ? false: true;
        if((item.description == null || item.fetchingProblem) && (flag == 'yes' || item.showEmailBody)) {
            if(item.fetchingProblem) {
                item.description = null;
                item.emailFetch = true;
            }
            $http.get('/message/get/email/single/web?emailContentId=' + item.emailContentId + '&googleAccountEmailId=' + item.ownerEmailId)
                .success(function (response) {
                    if (response.SuccessCode) {
                        item.description = response.Data.data.replace(/\n/g, "<br />");

                        var temp = response.Data.data.replace(/\n/g, "<br />"); //to remove internal css
                        var start, end;
                        start = temp.indexOf("<style");
                        end = temp.indexOf("</style>");
                        end = end != -1 ? end + 8 : end;
                        var replace = temp.slice(start , end);
                        //item.description = item.description.replace(replace, "")
                        temp = item.description;

                        var index = temp.indexOf('/track/email/open/')
                        if(index != -1){
                            item.description = removeRelatasTrackImg(item.description, index)
                        }
                        item.description = $sce.trustAsHtml(item.description);
                        item.emailFetch = false;
                        item.fetchingProblem = false;
                    }
                    else {
                        item.description = response.Message || '';
                        item.emailFetch = false;
                        item.fetchingProblem = true;
                    }
                })
        }
        if(item.showEmailBody && item.trackOpen){
            $scope.updateEmailOpen(item.ownerEmailId,item.trackId,item.userId);
        }
    }

    $scope.actionTaken = function(userId, rName, rEmailId, rmobileNumber, type, source, index, suggestion, self_hierarchy) {
        var actionTaken = {
            contact: {
                userId: userId ? userId : null,
                name: rName ? rName : null,
                personEmailId: rEmailId ? rEmailId : null,
                mobileNumber: rmobileNumber ? rmobileNumber : null,
            },
            actionType: type,
            suggestion: suggestion ? suggestion : null,
            insightSource: source,
            suggestionIndexValue: index,
            self_hierarchy: self_hierarchy
        }
        $http.post("/insights/action/taken", { actionTaken: actionTaken })
            .success(function(response) {
                if (response.SuccessCode) {
                     //console.log(response);
                }
            });
    }

    $scope.removeItem = function(item, table){
        for(var i=0;i<table.length;i++){
            if(item == table[i])
                table.splice(i,1);
        }

    };

    $scope.updateRelation = function(contact, relation) {
        var value = null;
        var dm = 0;
        var inf = 0;
        var influencerCss = 'fa fa-share-alt grey-color';
        var decisionMakerCss = 'fa fa-user grey-color';

        if (relation == "influencer") {
            if (contact.relation != "influencer"){
                influencerCss = 'fa fa-share-alt orange-color';
                value = "influencer";
            }
        } else if (relation == "decision_maker") {
            if (contact.relation != "decision_maker"){
                decisionMakerCss = 'fa fa-user orange-color';
                value = "decision_maker";
            }
        }
        //update
        toastr.remove();
        $http.post('/insights/contact/relation', { contactId: contact.contactId, value: value })
            .success(function(data) {
                if (data.SuccessCode) {
                    // toastr.success("successfully updated")
                    contact.relation = value;
                    contact.decisionMakerCss = decisionMakerCss;
                    contact.influencerCss = influencerCss;
                } else {
                    toastr.error("try again")
                }
            })
    }

    $scope.updateInProcessValue = function(inProcess, contactId) {

        if (isNumber(inProcess)) {
            var obj = {
                value: inProcess,
                contactId: contactId
            }

            $http.post('/insights/contact/inProcess', obj)
                .success(function(response) {
                    if (response) {} else {
                        toastr.error("Error! Please try again later")
                    }
                });
        } else {
            //toastr.error("Please enter a valid number")
        }
    }

    $scope.ignore = function(item){
        $http.post('/insights/your/response/pending/ignore', {emailType:item.mailType, emailThreadId:item.emailThreadId, interactionId:item.interactionId})
                .success(function(response) {
                    if (response) {
                        //console.log(response)
                    } else {
                        toastr.error("Error! Please try again later")
                    }
                });
    }

    $scope.showEmailDiv = function(item, full) {
        $scope.resetFields();
        for (var i = 0; i < full.length; i++)
            if (item !== full[i])
                full[i].emailShow = false;
        if(item.title)
            $scope.compose_email_subject = 'Re: ' + item.title;
        item.emailShow ? item.emailShow = false : item.emailShow = true;
        if(item.emailShow && item.trackOpen){
            $scope.updateEmailOpen(item.ownerEmailId,item.trackId,item.userId);
        }
    }

    $scope.resetFields = function() {
        $scope.compose_email_subject = "";
        $scope.compose_email_body = "";
        $scope.compose_email_doc_tracking = true;
        $scope.compose_email_track_viewed = true;
        $scope.compose_email_remaind = true;
        $scope.url = "";
        $scope.companyName = "";
        $scope.designation = "";
    };

    $scope.sendEmail = function(subject, body, docTrack, trackViewed, remind, rName, rEmailId, receiverId, index, suggestion, self_hierarchy, refId, mobileNumber, item) {
        if (!checkRequired(subject)) {
            toastr.error("Please enter an email subject.")
        } else if (!checkRequired(body)) {
            toastr.error("Please enter an email body.")
        } else {
            $scope.actionTaken(receiverId, rName, rEmailId, mobileNumber, 'mail', 'responsePending', index, suggestion, self_hierarchy);
            var obj = {
                receiverEmailId: rEmailId,
                receiverName: rName,
                message: body,
                subject: subject,
                receiverId: receiverId,
                docTrack: docTrack,
                trackViewed: trackViewed,
                remind: remind
            };
            if(refId){
                obj.updateReplied = true;
                obj.refId = refId;
                obj.id = item.refId //Used for Outlook
                //obj.subject = item.title;
            }
            else{
                $scope.ignore(item)
            }

            $("#send-email-but").addClass("disabled");
            $(".send-email-but").addClass("disabled");
            $http.post("/messages/send/email/single/web", obj)
                .success(function(response) {
                    if (response.SuccessCode) {
                        $("#send-email-but").removeClass("disabled");
                        $(".send-email-but").removeClass("disabled");
                        $scope.resetFields();
                        $(".compose-email-inline").slideToggle(200);
                        toastr.success(response.Message);
                    } else {
                        $("#send-email-but").addClass("disabled")
                        toastr.error(response.Message);
                    }
                });
        }
    };

    $scope.openCalander = function(index, contactId, item, table){
        $('.remindToConnectSelector'+index).datetimepicker({
            value:new Date(),
            timepicker:false,
            // format: 'd.m.Y H:i',
            minDate: new Date(),
            format: 'd.m.Y',
            onClose: function (dp, $input){
                var obj = {
                    remind:dp,
                    contactId:contactId
                }
                $http.post('/contacts/remindtoconnect',obj)
                    .success(function(response){
                        if(response){
                            toastr.success("Reminder to connect set successfully")
                            $scope.removeItem(item, table)
                        } else {
                            toastr.error("Error! Please try again later")
                        }
                    });
            }
        });
    }
    $scope.maxSize = 5;
});

function isNumber(o) {
    return !isNaN(o - 0) && o !== null && o !== "" && o !== false;
}

function removeRelatasTrackImg(data, index){
    var temp = data;
    var f = temp.lastIndexOf('<img style=', index);
    var e = index + temp.substring(index).indexOf('\">');
    e = e != -1 ? e + 2 : e;
    var slice = temp.slice(f, e)

    return temp = temp.replace(slice, "")
    //return temp = temp;
}