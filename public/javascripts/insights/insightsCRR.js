var reletasApp = angular.module('reletasApp', ['ngRoute', 'ngSanitize', 'angular-loading-bar', 'datatables']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope) {
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent, yourNetwork, extendedNetwork, forCompanies) {

        var str = searchContent;

        if (typeof str == 'string' && str.length > 0) {
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
        } else toastr.error("Please enter search content")
    };
});

reletasApp.factory('teamMembers', function($http, $q) {
    var data = [],
        lastRequestFailed = true,
        promise;
    return {
        getTeamMembers: function() {
            if (!promise || lastRequestFailed) {
                // $http returns a promise, so we don't need to create one with $q
                promise = $http.get('/company/members')
                    .then(function(res) {
                        lastRequestFailed = false;
                        data = res.data;
                        return data;
                    }, function(res) {
                        return $q.reject(res);
                    });
            }
            return promise;
        }
    }
});

reletasApp.controller("logedinUser", function($scope, $http, share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function(response) {
            $(".seeMoreIcon").addClass("disabled-see-more")
            if (response.SuccessCode) {
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.l_usr = response.Data;
                $scope.profilePicUrl = '/getImage/'+response.Data._id+'/'+new Date().toISOString()
                $scope.firstName = response.Data.firstName
                identifyMixPanelUser(response.Data);
                if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                    timezone = response.Data.timezone.name;
                }
                share.lUseEmailId = response.Data.emailId
                $scope.uniqueName = response.uniqueName;

            } else {

            }
        }).error(function(data) {

        })
});
reletasApp.service('share', function() {
    return {}
});

reletasApp.factory('sharedServiceSelection', function($rootScope) {
    var sharedService = {};

    sharedService.message = '';

    sharedService.prepForBroadcast = function(msg) {
        this.message = msg;
        this.broadcastItem();
    };

    sharedService.broadcastItem = function() {
        $rootScope.$broadcast('handleBroadcast');
    };

    return sharedService;
});

reletasApp.controller('selfOrCompany', function($scope, sharedServiceSelection) {
    $scope.selectedSelf = 'col-md-6 insights-hierarchy btn-green-xlg';
    $scope.selectedHierarchy = 'col-md-6 insights-hierarchy btn-green-hover-xlg';

    $scope.selectHierachy = function(selection) {
        if (selection == 'team') {
            $scope.selectedHierarchy = 'col-md-6 btn-green-xlg insights-hierarchy';
            $scope.selectedSelf = 'col-md-6 btn-green-hover-xlg insights-hierarchy';

        } else if (selection == 'self') {
            $scope.selectedHierarchy = 'col-md-6 btn-green-hover-xlg insights-hierarchy';
            $scope.selectedSelf = 'col-md-6 btn-green-xlg insights-hierarchy';
        }

        sharedServiceSelection.prepForBroadcast(selection);
    }
});

var timezone;

function getParams(url) {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while (match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var contextEmailId = getParams(window.location.href).context;
var showContext = true;
if (!checkRequired(contextEmailId)) {
    showContext = false;
}

reletasApp.controller('CRRDetails', function($scope, $http, sharedServiceSelection, teamMembers, DTOptionsBuilder, DTColumnDefBuilder) {

    $scope.$on('handleBroadcast', function() {
        $scope.message = sharedServiceSelection.message;
        if ($scope.message == 'self')
            paintCRRTable(sharedServiceSelection.message, 10);
        else
            paintCRRTable(sharedServiceSelection.message, 100);
    });

    $http.get('/profile/get/current/web')
        .success(function(response) {
            $(".seeMoreIcon").addClass("disabled-see-more")
            if (response.SuccessCode) {

                $scope.uniqueName = response.uniqueName;

            } else {

            }
        }).error(function(data) {

        })

    $scope.textload = "Loading data. Please wait..";
    $scope.textProcess = function() {
        return $scope.textload
    }
    var vm = this;
    vm.persons = [];
    vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10).withLanguage({
            "oPaginate": {
                "sNext": "»",
                "sPrevious": "«"
            }
        })
        .withOption('columns', [{ "orderDataType": "dom-value" }, null, null, { "orderDataType": "dom-value" }, null])
        .withOption('order', [
            [1, "asce"]
        ])
        .withOption("bDestroy", true)
        .withOption('oLanguage', {
            "sEmptyTable": $scope.textProcess,
            "sLengthMenu": "Show _MENU_ for page",
            //"sProcessing": "DataTables is currently busy",
            //"bProcessing":false,
            "processing": true
        });

    var columnOptions = function(message) {

        if (message == 'self') {
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0),
                DTColumnDefBuilder.newColumnDef(1),
                DTColumnDefBuilder.newColumnDef(2),
                DTColumnDefBuilder.newColumnDef(3),
                DTColumnDefBuilder.newColumnDef(4),
                DTColumnDefBuilder.newColumnDef(5),
                DTColumnDefBuilder.newColumnDef(6)
            ];

            vm.dtInstance = {};

            vm.dtInstanceCallback = dtInstanceCallback;

            function dtInstanceCallback(dtInstance) {
                vm.dtInstance = dtInstance;
            }

            $scope.show_loadMore = false;

            var skip = 0;
            var limit = 100;

            $scope.textload = "Loading Data. Please wait..";
            if (vm.dtInstance && vm.dtInstance.DataTable) {
                vm.dtInstance.DataTable.clear().draw();
            }
        } else if (message == 'team') {
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0),
                DTColumnDefBuilder.newColumnDef(1),
                DTColumnDefBuilder.newColumnDef(2),
                DTColumnDefBuilder.newColumnDef(3),
                DTColumnDefBuilder.newColumnDef(4),
                DTColumnDefBuilder.newColumnDef(5)
            ];

            vm.dtInstance = {};

            vm.dtInstanceCallback = dtInstanceCallback;

            function dtInstanceCallback(dtInstance) {
                vm.dtInstance = dtInstance;
            }

            $scope.show_loadMore = false;

            var skip = 0;
            var limit = 100;

            $scope.textload = "Loading Data. Please wait..";
            if (vm.dtInstance && vm.dtInstance.DataTable) {
                vm.dtInstance.DataTable.clear().draw();
            }
        }
    };

    //Default Self
    paintCRRTable('self', 10);
    var userId = null;

    function paintCRRTable(message, li) {
        columnOptions(message);

        teamMembers.getTeamMembers()
            .then(function(companyData) {

                var url;
                if (message == 'self') {

                    $scope.forTeam = false;
                    userId = companyData.Data.userId;
                    var url = '/insights/company/relationship/risk/detail/self';

                    $http.get(url, { params: { userIds: userId, limit: li } }).then(function(response) {
                        if (response.data.SuccessCode) {

                            var data = response.data.Data;
                            var len = data.length;
                            var obj = [];

                            for (var i = 0; i < len; i++) {
                                obj.push({
                                    company: data[i].company,
                                    value: data[i].contactValue,
                                    interaction: data[i].interaction,
                                    noOfContact: data[i].noOfContact,
                                    decisionmaker: data[i].noOfDecisionmaker,
                                    influencer: data[i].noOfInfluencer,
                                    extendedNetwork: data[i].extendedNetwork,
                                    contactsShow: false,
                                    contacts: data[i].contacts
                                });
                            }


                            if (obj.length > 0) {
                                vm.persons = obj
                            } else {
                                $scope.textload = "There is no Data";
                                $scope.interactions = [];
                                if (vm.dtInstance && vm.dtInstance.DataTable) {
                                    vm.dtInstance.DataTable.clear().draw();
                                }
                            }

                        }
                    });

                } else if (message == 'team') {

                    $scope.forTeam = true;
                    var url = '/insights/company/relationship/risk/detail/hierarchy';
                    var team = [];
                    for (var i = 0; i < companyData.Data.teamMembers.length; i++) {
                        if (companyData.Data.teamMembers[i] !== companyData.Data.userId)
                            team.push(companyData.Data.teamMembers[i])
                    }
                    $http.get(url, { params: { userIds: team, limit: li } }).then(function(response) {
                        if (response.data.SuccessCode) {

                            var data = response.data.Data;
                            var len = data.length;
                            var obj = [];
                            ind = 1;

                            for (var i = 0; i < len; i++) {
                                var cImg, profilePic;

                                if (data[i]._id) {
                                    cImg = '/getImage/' + data[i]._id;
                                    profilePic = false;
                                } else {
                                    profilePic = true;
                                    cImg = data[i].userName.substr(0, 2).toUpperCase()
                                }

                                obj.push({
                                    _id: data[i]._id,
                                    nameH: data[i].userName,
                                    emailH: data[i].userEmail,
                                    companyH: data[i].company,
                                    url: data[i].url,
                                    intractionsH: data[i].interaction,
                                    valuesH: data[i].value,
                                    contactsH: data[i].noOfContact,
                                    profileUrl: typeof data[i].profileUrl != "undefined" ? data[i].profileUrl : '#',
                                    no_pic: profilePic,
                                    contactImage: cImg,
                                    emailShow: false,
                                    index: i + 1,
                                    mobileNumber: data[i].mobileNumber
                                });
                            }

                            if (obj.length > 0) {
                                vm.persons = obj
                            } else {
                                vm.persons = [];
                                $scope.textload = "There is no Data";
                                $scope.interactions = [];
                                if (vm.dtInstance && vm.dtInstance.DataTable) {
                                    vm.dtInstance.DataTable.clear().draw();
                                }
                            }
                        } else {
                            vm.persons = [];
                            $scope.textload = "There is no Data";
                        }
                    });
                }
            });
    }

    $scope.updateContactValue = function(update, item, allContact, contactEmail) {
        toastr.remove();
        if (isNumber(update)) {
            $http.post('/insights/contact/inProcess', { user: userId, contact: contactEmail, value: update })
                .success(function(data) {
                    if (data.SuccessCode) {
                        toastr.success("successfully updated")
                        allContact.value = 0;
                        for (var i = 0; i < allContact.contacts.length; i++)
                            allContact.value += Number(allContact.contacts[i].value);
                    } else {
                        // console.log(data);
                        toastr.error("try again")
                    }
                })
        } else {
            toastr.error("enter valid number")
            item.value = 0;
            allContact.value = 0;
            for (var i = 0; i < allContact.contacts.length; i++)
                allContact.value += Number(allContact.contacts[i].value);
        }
    }

    $scope.contactShow = function(item, full) {
        for (var i = 0; i < full.length; i++)
            if (item !== full[i])
                full[i].contactsShow = false;
        item.contactsShow ? item.contactsShow = false : item.contactsShow = true;
    }

    $scope.showEmailDiv = function(item, full) {
        $scope.resetFields();
        for (var i = 0; i < full.length; i++)
            if (item !== full[i])
                full[i].emailShow = false;
        item.emailShow ? item.emailShow = false : item.emailShow = true;
        var location = window.location.hostname;
        $scope.compose_email_body = "Hi " + item.nameH.split(' ')[0] + ",\nI would like to have a detailed discussion on Company Relationship Risk report. Please set meeting on my calendar\nat " + location + "/" + $scope.uniqueName;
    }
    $scope.resetFields = function() {
        $scope.compose_email_subject = "Company Relationship Risk - discussion";
        // $scope.compose_email_body = "";
        $scope.compose_email_doc_tracking = true;
        $scope.compose_email_track_viewed = true;
        $scope.compose_email_remaind = true;
        $scope.url = "";
        $scope.companyName = "";
        $scope.designation = "";
    };

    $scope.sendEmail = function(subject, body, docTrack, trackViewed, remind, rName, rEmailId, receiverId, index, rmobileNumber, self_hierarchy, item) {
        if (!checkRequired(subject)) {
            toastr.error("Please enter an email subject.")
        } else if (!checkRequired(body)) {
            toastr.error("Please enter an email body.")
        } else {
            var actionTaken = {
                contact: {
                    userId: receiverId ? receiverId : null,
                    name: rName ? rName : null,
                    personEmailId: rEmailId ? rEmailId : null,
                    mobileNumber: rmobileNumber ? rmobileNumber : null,
                },
                actionType: 'followUp',
                suggestion: null,
                insightSource: 'companyRelationshipRisk',
                suggestionIndexValue: index,
                self_hierarchy: self_hierarchy
            }
            $http.post("/insights/action/taken", { actionTaken: actionTaken })
                .success(function(response) {
                    if (response.SuccessCode) {
                        // console.log(response);
                    }
                });
            var obj = {
                receiverEmailId: rEmailId,
                receiverName: rName,
                message: body,
                subject: subject,
                receiverId: receiverId,
                docTrack: docTrack,
                trackViewed: trackViewed,
                remind: remind
            };

            if(item && item.refId){
                obj.id = item.refId //Used for Outlook
            }

            $("#send-email-but").addClass("disabled")
            $(".send-email-but").addClass("disabled")
            $http.post("/messages/send/email/single/web", obj)
                .success(function(response) {
                    if (response.SuccessCode) {
                        $("#send-email-but").removeClass("disabled");
                        $(".send-email-but").removeClass("disabled");
                        $scope.resetFields();
                        $(".compose-email-inline").slideToggle(200);
                        toastr.success(response.Message);
                        for (var i = 0; i < item.length; i++) {
                            if (item[i].index == index)
                                item.splice(i, 1);
                        }

                    } else {
                        $("#send-email-but").removeClass("disabled")
                        toastr.error(response.Message);
                    }
                })
        }
    };

    $scope.updateRelation = function(contact, relation, item) {
        var value = null;
        var dm = 0;
        var inf = 0;
        if (relation == "influencer") {
            if (contact.relation != "influencer")
                value = "influencer";
        } else if (relation == "decision_maker") {
            if (contact.relation != "decision_maker")
                value = "decision_maker";
        }
        //update
        toastr.remove();
        $http.post('/insights/contact/relation', { contact: contact.email, value: value })
            .success(function(data) {
                if (data.SuccessCode) {
                    toastr.success("successfully updated")
                    contact.relation = value;
                    for (var i = 0; i < item.contacts.length; i++) {
                        if (item.contacts[i].relation == "decision_maker")
                            dm++;
                        else if (item.contacts[i].relation == "influencer")
                            inf++;
                    }
                    item.decisionmaker = dm;
                    item.influencer = inf;
                } else {
                    // console.log(data);
                    toastr.error("try again")
                }
            })


    }

    $scope.company = function(company) {
        window.location = '/customer/select?account=' + company;
    }

    $scope.showMoreSelf = function() {
        paintCRRTable('self', 0);
    }

    $scope.showMoreHierarchy = function() {
        paintCRRTable('team', 0);
    }
})

function isNumber(o) {
    return !isNaN(o - 0) && o !== null && o !== "" && o !== false;
}
