var reletasApp = angular.module('reletasApp', ['ngRoute', 'ngSanitize', 'angular-loading-bar', 'datatables','ui.bootstrap']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope) {
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent, yourNetwork, extendedNetwork, forCompanies) {

        var str = searchContent;

        if (typeof str == 'string' && str.length > 0) {
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
        } else toastr.error("Please enter search content")
    };
});

reletasApp.factory('teamMembers', function($http, $q) {
    var data = [],
        lastRequestFailed = true,
        promise;
    return {
        getTeamMembers: function() {
            if (!promise || lastRequestFailed) {
                // $http returns a promise, so we don't need to create one with $q
                promise = $http.get('/company/members')
                    .then(function(res) {
                        lastRequestFailed = false;
                        data = res.data;
                        return data;
                    }, function(res) {
                        return $q.reject(res);
                    });
            }
            return promise;
        }
    }
});

reletasApp.controller("logedinUser", function($scope, $http, share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function(response) {
            $(".seeMoreIcon").addClass("disabled-see-more")
            if (response.SuccessCode) {
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.l_usr = response.Data;
                $scope.firstName = response.Data.firstName
                $scope.profilePicUrl = '/getImage/'+response.Data._id+'/'+new Date().toISOString()
                identifyMixPanelUser(response.Data);
                if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                    timezone = response.Data.timezone.name;
                }
                share.lUseEmailId = response.Data.emailId
                share.setCompanyId(response.Data.companyId)
            } else {

            }
        }).error(function(data) {

        })
});
reletasApp.service('share', function() {
    return {
        setCompanyId:function(companyId){
            this.companyId = companyId;
        }
    }
});

reletasApp.factory('sharedServiceSelection', function($rootScope) {
    var sharedService = {};

    sharedService.message = '';

    sharedService.prepForBroadcast = function(msg) {
        this.message = msg;
        this.broadcastItem();
    };

    sharedService.broadcastItem = function() {
        $rootScope.$broadcast('handleBroadcast');
    };

    sharedService.paginationBroadcast = function (getFor,getPage) {
        this.getFor = getFor;
        this.getPage = getPage;
        this.broadcastPaginationItem();
    };

    sharedService.broadcastPaginationItem = function () {
        $rootScope.$broadcast('handlePaginationBroadcast')
    }

    return sharedService;
});

reletasApp.controller('selfOrCompany', function($scope,$http,sharedServiceSelection,$rootScope,share) {
    $scope.selectedSelf = 'col-md-6 insights-hierarchy btn-green-xlg';
    $scope.selectedHierarchy = 'col-md-6 insights-hierarchy btn-green-hover-xlg';

    $scope.selectHierachy = function(selection) {
        if (selection == 'team') {
            $scope.selectedHierarchy = 'col-md-6 btn-green-xlg insights-hierarchy';
            $scope.selectedSelf = 'col-md-6 btn-green-hover-xlg insights-hierarchy';

        } else if (selection == 'self') {
            $scope.selectedHierarchy = 'col-md-6 btn-green-hover-xlg insights-hierarchy';
            $scope.selectedSelf = 'col-md-6 btn-green-xlg insights-hierarchy';
        }

        sharedServiceSelection.prepForBroadcast(selection);
    }
});

var timezone;

function getParams(url) {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while (match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var contextEmailId = getParams(window.location.href).context;
var showContext = true;
if (!checkRequired(contextEmailId)) {
    showContext = false;
}

var limitGlobal;

reletasApp.controller('losingTouchDetails', function($scope, $http,$rootScope, $sce, sharedServiceSelection, teamMembers, DTOptionsBuilder, DTColumnDefBuilder,share) {

    $scope.$on('handleBroadcast', function() {
        $scope.message = sharedServiceSelection.message;
        paintLosingTouchTable(sharedServiceSelection.message, false);
    });

    $rootScope.currency = '';

    share.setCompanyId = function (companyId) {
        if(companyId){
            $http.get('/corporate/company/' + companyId)
                .success(function (companyProfile) {
                    if(companyProfile.currency){
                        $rootScope.currency = companyProfile.currency
                    }
                });
        }
    }

    $scope.textload = "Loading data. Please wait..";
    $scope.textProcess = function() {
        return $scope.textload }
    var vm = this;
    vm.persons = [];
    vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10).withLanguage({
            "oPaginate": {
                "sNext": "»",
                "sPrevious": "«"
            }
        })
        .withOption('columns', [{ "orderDataType": "dom-value" }, null, null, { "orderDataType": "dom-value" }, null])
        .withOption('order', [
            [0, "desc"]
        ])
        .withOption("bDestroy", true)
        .withOption('paging', false)
        .withOption('oLanguage', {
            "sEmptyTable": $scope.textProcess,
            "sLengthMenu": "Show _MENU_ for page",
            //"sProcessing": "DataTables is currently busy",
            //"bProcessing":false,
            "processing": true
        });

    var columnOptions = function(message) {

        if (message == 'self') {
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0),
                DTColumnDefBuilder.newColumnDef(1),
                DTColumnDefBuilder.newColumnDef(2),
                DTColumnDefBuilder.newColumnDef(3),
                DTColumnDefBuilder.newColumnDef(4),
                DTColumnDefBuilder.newColumnDef(5).notSortable(),
                DTColumnDefBuilder.newColumnDef(6).notSortable(),
                DTColumnDefBuilder.newColumnDef(7).notSortable()
            ];

            vm.dtInstance = {};

            vm.dtInstanceCallback = dtInstanceCallback;

            function dtInstanceCallback(dtInstance) {
                vm.dtInstance = dtInstance;
            }

            $scope.show_loadMore = false;

            $scope.textload = "Loading Data. Please wait..";
            if (vm.dtInstance && vm.dtInstance.DataTable) {
                vm.dtInstance.DataTable.clear().draw();
            }
        } else if (message == 'team') {
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0),
                DTColumnDefBuilder.newColumnDef(1),
                DTColumnDefBuilder.newColumnDef(2),
                DTColumnDefBuilder.newColumnDef(3),
                DTColumnDefBuilder.newColumnDef(4),
                DTColumnDefBuilder.newColumnDef(5).notSortable(),
                DTColumnDefBuilder.newColumnDef(6).notSortable(),
                DTColumnDefBuilder.newColumnDef(7).notSortable()
            ];

            vm.dtInstance = {};

            vm.dtInstanceCallback = dtInstanceCallback;

            function dtInstanceCallback(dtInstance) {
                vm.dtInstance = dtInstance;
            }

            $scope.show_loadMore = false;

            $scope.textload = "Loading Data. Please wait..";
            if (vm.dtInstance && vm.dtInstance.DataTable) {
                vm.dtInstance.DataTable.clear().draw();
            }
        }
    };

    //Default Self
    paintLosingTouchTable('self', true);

    $scope.$on('handlePaginationBroadcast', function() {
        paintLosingTouchTable(sharedServiceSelection.getFor,limitGlobal,sharedServiceSelection.getPage);
    });

    function paintLosingTouchTable(message,limit,getPage) {

        limitGlobal = limit;

        columnOptions(message);

        $scope.showAllBtn = false;

        teamMembers.getTeamMembers()
            .then(function(companyData) {

                var url = '/insights/losing/touch/info/';
                var obj = companyData.Data.companyMembers;
                var companyArr = Object.keys(obj).map(function(k) {
                    return obj[k]
                });

                url = fetchUrlWithParameter(url, "companyMembers", companyArr);
                url = fetchUrlWithParameter(url, 'limit', limit);

                if (message == 'self') {

                    $scope.forTeam = false;
                    $scope.showAllBtn = true;

                    var hierarchyList = [];
                    hierarchyList.push(companyData.Data.userId);
                    url = fetchUrlWithParameter(url, 'hierarchyList', hierarchyList);
                } else if (message == 'team') {
                    if(companyData.Data.teamMembers.length<=0){
                        toastr.error("No-one reporting to you.")
                    }
                    $scope.forTeam = true;
                    url = fetchUrlWithParameter(url, 'hierarchyList', companyData.Data.teamMembers);
                }

                if(!getPage) {
                    url = fetchUrlWithParameter(url,'getPage',1)
                } else {
                    $scope.showAllBtn = false;
                    url = fetchUrlWithParameter(url,'getPage',getPage)
                }

                $http.get(url)
                    .success(function(response) {
                        $scope.totalItems = response.totalList;

                        var len = response.Data.contacts.length;
                        var obj = [];
                        for (var i = 0; i < len; i++) {

                        var cImg, profilePic, ownerProfilePic, ownerImg, relation, relationTitle
                        var contactImageLink = response.Data.contacts[i].contactImageLink?encodeURIComponent(response.Data.contacts[i].contactImageLink):null

                            if (response.Data.contacts[i].personId) {
                                cImg = '/getImage/' + response.Data.contacts[i].personId;
                                profilePic = false;
                                //Boundary condition. Sometimes the profile images are not found. Showing first 2 char of emailId.
                                if(!imageExists(cImg)){
                                    profilePic = true;
                                    cImg = response.Data.contacts[i].contactEmailId.substr(0, 2).toUpperCase()
                                }

                            }else if(contactImageLink){
                                profilePic = false;
                                cImg = '/getContactImage/'+response.Data.contacts[i].contactEmailId+'/'+contactImageLink
                            } else {
                                profilePic = true;
                                cImg = response.Data.contacts[i].contactEmailId?response.Data.contacts[i].contactEmailId.substr(0, 2).toUpperCase():response.Data.contacts[i].contactName.substr(0, 2).toUpperCase()
                            }

                            if (response.Data.contacts[i].ownerId) {
                                ownerImg = '/getImage/' + response.Data.contacts[i].ownerId;
                                ownerProfilePic = false;
                            } else {
                                ownerProfilePic = true;
                                ownerImg = response.Data.contacts[i].ownerFullName.substr(0, 2).toUpperCase()
                            }

                            var influencerCss = 'fa fa-share-alt grey-color';
                            var decisionMakerCss = 'fa fa-user grey-color';

                            if (response.Data.contacts[i].contactRelation && response.Data.contacts[i].contactRelation.decisionmaker_influencer == 'influencer') {
                                influencerCss = 'fa fa-share-alt orange-color';
                                relation = 'influencer';
                            } else if (response.Data.contacts[i].contactRelation && response.Data.contacts[i].contactRelation.decisionmaker_influencer == 'decision_maker') {
                                decisionMakerCss = 'fa fa-user orange-color';
                                relation = "decision_maker";
                            } else{
                                relation = null;
                            }

                            $scope.ifTweets = false;

                            if (response.Data.contacts[i].twitterRefId && response.Data.contacts[i].twitterTitle && response.Data.contacts[i].lastInteractionType === 'twitter') {
                                $scope.ifTweets = true;
                            }

                            if(response.Data.contacts[i].trackInfo && response.Data.contacts[i].trackInfo){
                                var emailOpens = response.Data.contacts[i].trackInfo.emailOpens ? response.Data.contacts[i].trackInfo.emailOpens : 0;
                                var readOn = response.Data.contacts[i].trackInfo.isRed?"First read on "+moment(response.Data.contacts[i].trackInfo.lastOpenedOn).tz(timezone).format("DD MMM YYYY, h:mm a")+" ("+emailOpens+")":"Unread";
                                if(response.Data.contacts[i].action == 'sender' && response.Data.contacts[i].trackInfo.trackOpen){
                                    var trackOpen = true;
                                }
                            }

                            if(!response.Data.contacts[i].trackInfo && !response.Data.contacts[i].trackInfo && response.Data.contacts[i].action == 'receiver'){
                                readOn = "Send mails through Relatas to see when mail was read."
                            }

                        var showTrackInfo = false;
                        if(response.Data.contacts[i].lastInteractionType == 'email'){
                            showTrackInfo = true;
                        }

                        var interactionIconType = getInteractionIconType(response.Data.contacts[i].lastInteractionType)

                            obj.push({
                                name: response.Data.contacts[i].name,
                                contactName: response.Data.contacts[i].contactName,
                                contactEmailId: response.Data.contacts[i].contactEmailId?response.Data.contacts[i].contactEmailId:response.Data.contacts[i].contactMobileNumber,
                                designation: response.Data.contacts[i].designation,
                                company: response.Data.contacts[i].company == "Others" ? null : response.Data.contacts[i].company,
                                lastInteracted: response.Data.contacts[i].lastInteracted ? moment(response.Data.contacts[i].lastInteracted).format("DD MMM YYYY") : null,
                                contactValue: response.Data.contacts[i].contactValue,
                                suggestion: response.Data.contacts[i].suggestion,
                                publicProfileUrl: typeof response.Data.contacts[i].publicProfileUrl != "undefined" ? '/' + response.Data.contacts[i].publicProfileUrl : '#',
                                contactId: response.Data.contacts[i].contactId,
                                contactImage: cImg,
                                relation: relation,
                                relationTitle: relationTitle,
                                no_pic: profilePic,
                                contactOwner: response.Data.contacts[i].ownerFullName,
                                ownerImage: ownerImg,
                                owner_no_pic: ownerProfilePic,
                                twitterRefId: response.Data.contacts[i].twitterRefId || "No Tweets to reply to",
                                twitterTitle: response.Data.contacts[i].twitterTitle || "No Tweets to reply to",
                                ifTweets: $scope.ifTweets,
                                decisionMakerCss: decisionMakerCss,
                                influencerCss: influencerCss,
                                radioLabelA:'radioLabelA'+i,
                                radioLabel:'radioLabel'+i,
                                index: i + 1,
                                ownerEmailId: response.Data.contacts[i].ownerEmailId,
                                ownerId:response.Data.contacts[i].ownerId,
                                ownerFullName: response.Data.contacts[i].ownerFullName,
                                refId: response.Data.contacts[i].refId,
                                title: response.Data.contacts[i].title,
                                description: response.Data.contacts[i].description,
                                emailContentId: response.Data.contacts[i].emailContentId,
                                lastInteractionType: response.Data.contacts[i].lastInteractionType,
                                emailFetch: response.Data.contacts[i].description ? false : true,
                                setFor: message,
                                fetchingProblem:false,
                                readOn:readOn,
                                interactionIconType:interactionIconType,
                                showTrackInfo:showTrackInfo,
                                mobileNumber : response.Data.contacts[i].contactMobileNumber?response.Data.contacts[i].contactMobileNumber.replace(/[^a-zA-Z0-9]/g,''):null,
                                emailOpens:emailOpens,
                                trackOpen:trackOpen ? trackOpen : null,
                                trackId:response.Data.contacts[i].trackId,
                                userId:response.Data.contacts[i].personId,
                                lastInteractionDate:response.Data.contacts[i].lastInteractionDate
                            });
                        }

                        if (obj.length > 0) {
                            vm.persons = obj
                            // vm.persons.sort(function (o1, o2) {
                            //     console.log(o1.lastInteractionDate);
                            //     return new Date(o1.lastInteractionDate) > new Date(o2.lastInteractionDate) ? -1 : new Date(o1.lastInteractionDate) < new Date(o2.lastInteractionDate) ? 1 : 0;
                            // });

                        } else {
                            $scope.textload = "There is no Data";
                            $scope.interactions = [];
                            if (vm.dtInstance && vm.dtInstance.DataTable) {
                                vm.dtInstance.DataTable.clear().draw();
                            }
                        }

                        //Show aggregated numbers
                        $scope.inTouch = response.Data.aggregatedNumbers.selfLosingTouch;
                    });
            });
    }

    $scope.viewPastEmail = function(item){
        if((item.description == null || item.fetchingProblem) && item.emailShow && item.lastInteractionType == 'email') {
            if(item.fetchingProblem) {
                item.description = null;
                item.emailFetch = true;
            }
            $http.get('/message/get/email/single/web?emailContentId=' + item.emailContentId + '&googleAccountEmailId=' + item.ownerEmailId)
                .success(function (response) {
                    if (!response.SuccessCode) {
                        item.description = response.Message || '';

                        var temp = response.Data.data.replace(/\n/g, "<br />"); //to remove internal css
                        var start, end;
                        start = temp.indexOf("<style");
                        end = temp.indexOf("</style>");
                        end = end != -1 ? end + 8 : end;
                        var replace = temp.slice(start , end);
                        item.description = item.description.replace(replace, "")
                        temp = item.description;

                        var index = temp.indexOf('/track/email/open/')
                        if(index != -1){
                            item.description = removeRelatasTrackImg(item.description, index)
                        }

                        item.emailFetch = false;
                        item.fetchingProblem = true;
                    } else {
                        item.description = response.Data.data.replace(/\n/g, "<br />");
                        item.emailFetch = false;
                        item.fetchingProblem = false;
                    }
                })
        }
    }

    $scope.ignoreLosingTouch = function(contactId,contactEmailId,ignoreFor){
        var days;
        if(ignoreFor === 30) {
            days = moment().add(ignoreFor, "days").toDate()
        } else if(ignoreFor === 'never') {
            days = moment().add(99, "years").toDate();
        }

        $http.post('/insights/ignore/contact',{contactId:contactId,contactEmailId:contactEmailId,ignoreFor:days,insightsSection:'losingTouch'})
            .success(function(response){
                if(response){
                    paintLosingTouchTable('self',limitGlobal);
                }
            });
    }

    $scope.showMoreInsights = function() {
        paintLosingTouchTable('self', false);
    }

    $scope.showEmailDiv = function(item, full) {
        if(item.lastInteractionType == 'email')
            $scope.resetFields('reply', item);
        else
            $scope.resetFields('fresh', item);
        for (var i = 0; i < full.length; i++)
            if (item !== full[i])
                full[i].emailShow = false;
        item.emailShow ? item.emailShow = false : item.emailShow = true;
        if(item.emailShow  && item.trackOpen){
            $scope.updateEmailOpen(item.ownerEmailId,item.trackId,item.userId);
        }
    }

    $scope.showTwitterForm = function(item, full) {
        //$scope.resetFields();
        for (var i = 0; i < full.length; i++)
            if (item !== full[i])
                full[i].showTwitter = false;
        item.showTwitter ? item.showTwitter = false : item.showTwitter = true;
    }

    $scope.resetFields = function(flag, item) {
        if(flag == 'reply' && item.setFor == 'self'){
            $scope.compose_email_subject = 'Re: ' + item.title;
        }
        else{
            $scope.compose_email_subject = "";
        }

        $scope.compose_email_body = "";
        $scope.compose_email_doc_tracking = true;
        $scope.compose_email_track_viewed = true;
        $scope.compose_email_remaind = true;
        $scope.url = "";
        $scope.companyName = "";
        $scope.designation = "";
    };

    $scope.goToContactSelected = function(emailId,mobileNumber) {
        if(mobileNumber && mobileNumber !='undefined'){
            window.location.replace('/contact/selected?context='+emailId+'&mobileNumber='+mobileNumber)
        } else {
            window.location.replace('/contact/selected?context='+emailId)
        }
    }

    $scope.replyTwitterPost = function(refId, tweet) {
        var action = 'reply';
        $http.get('/social/feed/twitter/tweet/update/web?action=' + action + '&postId=' + refId + '&status=' + tweet)
            .success(function(response) {
                if (response.SuccessCode) {
                    console.log(response)
                }
            });
    }

    $scope.updateEmailOpen = function(emailId,trackId,userId){
        if(checkRequired(emailId) && checkRequired(trackId) && checkRequired(userId)){
            $http.get('/track/email/open/'+emailId+'/track/id/'+trackId+'/user/'+userId)
                .success(function(response){
                });
        }
    };

    $scope.sendEmail = function(subject, body, docTrack, trackViewed, remind, rName, rEmailId, receiverId, index, suggestion, self_hierarchy, item) {

        if (!checkRequired(subject)) {
            toastr.error("Please enter an email subject.")
        } else if (!checkRequired(body)) {
            toastr.error("Please enter an email body.")
        } else {
            if(self_hierarchy == 'self')
                $scope.actionTaken(null, rName, rEmailId, null, 'mail', 'losingTouch', index, suggestion, self_hierarchy);
            else
                $scope.actionTaken(null, rName, rEmailId, null, 'followUp', 'losingTouch', index, null, self_hierarchy);
            var obj = {
                receiverEmailId: rEmailId,
                receiverName: rName,
                message: body,
                subject: subject,
                receiverId: receiverId,
                docTrack: docTrack,
                trackViewed: trackViewed,
                remind: remind
            };

            if(item){
                if(item.refId){
                    obj.updateReplied = true;
                    obj.refId = item.refId;
                    obj.id = item.refId //Used for Outlook
                    //obj.subject = item.title;
                }
            }
            $("#send-email-but").addClass("disabled");
            $(".send-email-but").addClass("disabled");
            $http.post("/messages/send/email/single/web", obj)
                .success(function(response) {
                    if (response.SuccessCode) {
                        $("#send-email-but").removeClass("disabled");
                        $(".send-email-but").removeClass("disabled");
                        $scope.resetFields();
                        $(".compose-email-inline").slideToggle(200);
                        toastr.success(response.Message);
                    } else {
                        $("#send-email-but").addClass("disabled")
                        toastr.error(response.Message);
                    }
                });
        }
    };

    $scope.actionTaken = function(userId, rName, rEmailId, rmobileNumber, type, source, index, suggestion, self_hierarchy) {
        var actionTaken = {
            contact: {
                userId: null,
                name: rName ? rName : null,
                personEmailId: rEmailId ? rEmailId : null,
                mobileNumber: rmobileNumber ? rmobileNumber : null,
            },
            actionType: type,
            suggestion: suggestion ? suggestion : null,
            insightSource: source,
            suggestionIndexValue: index,
            self_hierarchy: self_hierarchy
        }
        $http.post("/insights/action/taken", { actionTaken: actionTaken })
            .success(function(response) {
                if (response.SuccessCode) {
                    console.log(response);
                }
            });
    }

    $scope.updateInProcessValue = function(inProcess, email) {

        if (isNumber(inProcess)) {
            var obj = {
                type: 'inProcess',
                value: inProcess,
                email: email
            }

            $http.post('/contacts/update/value/type', obj)
                .success(function(response) {
                    if (response) {} else {
                        toastr.error("Error! Please try again later")
                    }
                });
        } else {
            //toastr.error("Please enter a valid number")
        }
    }

    // $scope.updateRelation = function(email,value) {

    //     var relationKey = 'decisionmaker_influencer';
    //     var reqObj = {email: email, type: value, relationKey: relationKey};

    //     $http.post('/contacts/update/relationship/type/by/email', reqObj)
    //         .success(function (response) {
    //             if(response.SuccessCode){
    //                 paintLosingTouchTable('self',limitGlobal);
    //             }
    //         })
    // }

    $scope.updateRelation = function(contact, relation) {
        var value = null;
        var dm = 0;
        var inf = 0;
        var influencerCss = 'fa fa-share-alt grey-color';
        var decisionMakerCss = 'fa fa-user grey-color';

        if (relation == "influencer") {
            if (contact.relation != "influencer"){
                influencerCss = 'fa fa-share-alt orange-color';
                value = "influencer";
            }
        } else if (relation == "decision_maker") {
            if (contact.relation != "decision_maker"){
                decisionMakerCss = 'fa fa-user orange-color';
                value = "decision_maker";
            }
        }
        //update
        toastr.remove();
        $http.post('/insights/contact/relation', { contactId: contact.contactId, value: value })
            .success(function(data) {
                if (data.SuccessCode) {
                    // toastr.success("successfully updated")
                    contact.relation = value;
                    contact.decisionMakerCss = decisionMakerCss;
                    contact.influencerCss = influencerCss;
                } else {
                    toastr.error("try again")
                }
            })
    }

    $scope.viewEmail = function(item, flag){
        if(flag == 'yes')
            item.showEmailBody = item.showEmailBody ? false: true;
        if((item.description == null || item.fetchingProblem) && item.showEmailBody) {
            if(item.fetchingProblem) {
                item.description = null;
                item.emailFetch = true;
            }
            $http.get('/message/get/email/single/web?emailContentId=' + item.emailContentId + '&googleAccountEmailId=' + item.ownerEmailId)
                .success(function (response) {
                    if (response.SuccessCode) {
                        item.description = response.Data.data.replace(/\n/g, "<br />");

                        var temp = response.Data.data.replace(/\n/g, "<br />"); //to remove internal css
                        var start, end;
                        start = temp.indexOf("<style");
                        end = temp.indexOf("</style>");
                        end = end != -1 ? end + 8 : end;
                        var replace = temp.slice(start , end);
                        //item.description = item.description.replace(replace, "")
                        temp = item.description;
                        var index = temp.indexOf('/track/email/open/')
                        if(index != -1){
                            item.description = removeRelatasTrackImg(item.description, index)
                        }
                        item.description = $sce.trustAsHtml(item.description);

                        item.emailFetch = false;
                        item.fetchingProblem = false;
                    }
                    else {
                        item.description = response.Message || '';
                        item.emailFetch = false;
                        item.fetchingProblem = true;
                    }
                })
        }
        if(item.showEmailBody && item.trackOpen){
            $scope.updateEmailOpen(item.ownerEmailId,item.trackId,item.userId);
        }
    }

})

reletasApp.controller('selfPagination',function ($scope,sharedServiceSelection) {
    $scope.pageChanged = function(getFor) {
        sharedServiceSelection.paginationBroadcast(getFor,$scope.currentPage)
    };
})

reletasApp.controller('teamPagination',function ($scope,sharedServiceSelection) {
    $scope.pageChanged = function(getFor) {
        sharedServiceSelection.paginationBroadcast(getFor,$scope.currentPage)
    };
})

angular.element(document).ready(function(){
//var pageloadCount = 0;
//function loadModal($http) {

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        'positionClass': 'toast-top-full-width',
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "3000",
        "hideDuration": "1000",
        "timeOut": "8000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
});

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue) {

    if (parameterValue instanceof Array)
        if (parameterValue.length > 0)
            parameterValue = parameterValue.join(",")
        else
            parameterValue = null

    if (parameterValue != undefined && parameterValue != null) {
        if (baseUrl.indexOf("?") == -1)
            baseUrl += "?"
        else
            baseUrl += "&"
        baseUrl += parameterName + "=" + parameterValue
    }

    return baseUrl
}

function isNumber(o) {
    return !isNaN(o - 0) && o !== null && o !== "" && o !== false;
}

function imageExists(url){
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status != 404;
}

function getInteractionIconType(interactionType) {
    switch (interactionType){
        case 'meeting': return 'fa fa-calendar-check-o margin0';break;
        case 'email': return 'fa fa-envelope margin0';break;
        case 'call': return 'fa fa-phone margin0';break;
        case 'sms': return 'fa fa-reorder margin0';break;
        case 'twitter': return 'fa fa-twitter-square margin0';break;
    }
}

function removeRelatasTrackImg(data, index){
    var temp = data;
    var f = temp.lastIndexOf('<img style=', index);
    var e = index + temp.substring(index).indexOf('\">');
    e = e != -1 ? e + 2 : e;
    var slice = temp.slice(f, e)

    return temp = temp.replace(slice, "")
    //return temp = temp;
}

$.fn.dataTable.ext.order['dom-value'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $(td).attr("lastInteractionDate");
    } );
}