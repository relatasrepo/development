var relatasApp = angular.module('relatasApp', ['ngRoute', 'ngCookies','ngSanitize','ngLodash']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.controller("header_controller", function($scope, $http, meetingObjService) {
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = false
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function() {
        return "/todaySeries/leftMeetingBar"
    };

    $scope.searchFromHeader = function(searchContent, yourNetwork, extendedNetwork, forCompanies) {

        var str = encodeURIComponent(searchContent);

        if (typeof str == 'string' && str.length > 0) {
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;

            window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
        } else toastr.error("Please enter search content")
    };
    meetingObjService.searchFromHeader = $scope.searchFromHeader;

});

var timezone;

relatasApp.service('meetingObjService', function() {
    return {}
});

relatasApp.factory('sharedServiceSelection', function($rootScope, $http) {
    var sharedService = {};
    sharedService.message = '';

    sharedService.updateActionTakenInfo = function(recordId,actionTakenType,$http,sharedServiceSelection,filter){
        updateActionTakenInfo(recordId,actionTakenType,$http,sharedServiceSelection,filter);
    }

    sharedService.emailBroadcast = function (mailDetails) {
        this.mailDetails = mailDetails;
        this.broadcastEmailObject();
    };

    sharedService.broadcastEmailObject = function () {
        $rootScope.$broadcast('handleEmailBroadcast')
    };

    return sharedService;
});

relatasApp.controller("logedinUser", function($scope, $http,$rootScope,meetingObjService) {

    $http.get('/profile/get/current/web')
        .success(function(response) {
            setTimeout(function() {
                $(".seeMoreIcon").addClass("disabled-see-more")
            }, 1000)

            if (response.SuccessCode) {
                $scope.l_usr = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.userName = response.Data.firstName;
                $scope.firstName = response.Data.firstName
                $scope.profilePicUrl = response.Data.profilePicUrl;
                identifyMixPanelUser(response.Data);
                if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                    timezone = response.Data.timezone.name;
                }

                console.log($scope.profilePicUrl)

                meetingObjService.liuData = response.Data;

            } else {

            }
        }).error(function(data) {
    })
});

relatasApp.controller("mailAction", function($scope,$http,$rootScope,sharedServiceSelection,$window,searchService){

    $http.get('/insights/mail/actions/meta')
        .success(function (response) {

            if(response.SuccessCode){
                $scope.emailContentIds = response.Data.emailContentIds;
                sentimentData($scope,response.Data.positive,response.Data.negative,sharedServiceSelection)
                mailTypeData($scope,response.Data.important,response.Data.onlyToMe,response.Data.followUp,sharedServiceSelection)
            } else {
                showNoMailsView($scope);
            }
        });

    $scope.doNothing = function () {

    }

    $scope.viewAllInteractions = function (emailId) {
        window.location = '/interactions?context='+emailId;
    }

    $scope.isMobileView = false;

    if ($(window).width() <= 768) {
        $scope.isMobileView = true;
    }

    $scope.replyToThisMail = function (item,filter,index,isReply) {

        $rootScope.mailSelected = null; // reset previous selections
        $rootScope.mailSelected = index;
        $rootScope.actionBoardToggle = true;
        $scope.showMeeting = false;
        $scope.showEmail = true;
        item.filter = filter;
        item.openForm = true;
        item.showSummary = false;

        if ($(window).width() <= 768) {
            $scope.isMobileView = true;
            $(".action-board").show(100);
        }

        highlightSelectedRow($scope.mails,item)
        replyToThisMail($scope,$http,sharedServiceSelection,item);
    };

    $scope.actionBoardClose = function () {
        $rootScope.actionBoardToggle = false;
        $rootScope.mailSelected = null;
    };

    $scope.sortType = "date";
    $scope.sortReverse = true;
    $scope.skip = 0;
    $scope.prevBtn = {"display":"none"}

    getEmails()

    $scope.nextPage = function (skip) {

        $scope.skip = skip;

        if((skip >= $scope.totalPages) || (($scope.totalPages-skip)<=10)){
            $scope.nextBtn = {"visibility":"hidden"}
        } else {
            $scope.nextBtn = {"visibility":"visible"}
        }

        $scope.prevBtn = {"visibility":"visible"}

        getEmails(skip)
    };

    $scope.prevPage = function (skip) {
        $scope.skip = skip;
        if(skip <= 0){
            $scope.prevBtn = {"visibility":"hidden"}
        } else {
            $scope.prevBtn = {"visibility":"visible"}
        }

        $scope.nextBtn = {"visibility":"visible"}
        getEmails(skip)
    };

    $scope.filter = null;
    sharedServiceSelection.refreshMailTable = function (filter) {

        if($scope.filter != filter){
            $scope.skip = 0;
        }

        $scope.filter = filter
        getEmails($scope.skip)
    }

    $scope.filterEmailsBy = function (filter) {

        if($scope.filter != filter){
            $scope.skip = 0;
        }

        $scope.filter = filter;
        getEmails($scope.skip)
    }

    $scope.numberOfAllMails = 0;

    function getEmails(skip){

        skip = !skip?$scope.skip:skip;
        
        $scope.mails = [];

        var url = '/insights/mail/actions?skip='+$scope.skip;
        // var url = '/insights/mail/actions?skip='+0+"&filter=important";

        if($scope.filter){
            url = fetchUrlWithParameter(url+"&filter="+$scope.filter)
            url = fetchUrlWithParameter(url+"&emailContentIds="+$scope.emailContentIds)
        }
        
        $http.get(url)
            .success(function (response) {
                $scope.totalPages = 0;
                if(response.SuccessCode){

                    var from = (parseInt(skip)+1);
                    var to = (parseInt(skip)+10);
                    $scope.totalPages = response.interactionsCount && response.interactionsCount[0]?response.interactionsCount[0].count:0;

                    if(to>$scope.totalPages){
                        to = $scope.totalPages
                    }

                    $scope.showingPages = from+" - "+ to;

                    _.each(response.Data,function (el,index) {
                        $scope.mails.push(formatMails(el,index+1));
                    })

                }
            });
    }

});

function showNoMailsView($scope) {
    $scope.positiveNumber = 0;
    $scope.negativeNumber=0;
    $scope.importantNumber = 0;
    $scope.onlyToMeNumber = 0;
    $scope.followUpNumber = 0;

    $scope.showMailActionBoard = true;
}

function mailTypeData($scope,important,onlyToMe,followUp,sharedServiceSelection){

    $scope.importantNumber = important
    $scope.onlyToMeNumber = onlyToMe;
    $scope.followUpNumber = followUp;

    var series = [{meta:"Follow up",value:$scope.followUpNumber},{meta:"Only to me",value:$scope.onlyToMeNumber},{meta:"Important",value:$scope.importantNumber}],
        label = ['Follow up','Only to me','Important'];

    donutDraw($scope,series,label,".chart-mail-types",sharedServiceSelection)
}

function sentimentData($scope,positive,negative,sharedServiceSelection){

    $scope.positiveNumber = positive;
    $scope.negativeNumber=negative

    var series2 = [{meta:"Positive",value:$scope.positiveNumber},{meta:"Negative",value:$scope.negativeNumber}],
        label2 = ['Positive','Negative'];

    donutDraw($scope,series2,label2,".chart-mail-sentiment",sharedServiceSelection)
}

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords,fetchWatchlistType){
            return $http.post('/search/user/contacts', { "contactName" : keywords,"fetchWatchlistType":fetchWatchlistType });
        }
    }
}]);

function replyToThisMail($scope,$http,sharedServiceSelection,item) {
    sharedServiceSelection.emailBroadcast(item);
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function donutDraw($scope,series,label,className,sharedServiceSelection) {

    var data = {
        series: series,
        labels: label
    }

    var options = {
        donut: true,
        donutWidth: 12,
        showLabel:false,
        plugins: [
            tooltip
        ],
        height: '105px',
        width: '105px'
    }

    var chart = new Chartist.Pie(className,data,options);

    chart.on('draw', function(data) {
        donutOnDraw(data)
    });

    chart.on("created", function () {
        donutEventHandler(sharedServiceSelection)
    });
}

function donutEventHandler(sharedServiceSelection) {

    var filter = null;

    $('.chart-mail-types .ct-chart-donut .ct-series-a path, .chart-mail-types .ct-chart-donut .ct-series-b path, .chart-mail-types .ct-chart-donut .ct-series-c path').click(function (evt) {
        evt.stopImmediatePropagation();

        var value = $(this).attr('ct:value');
        var type = $(this).attr('ct:meta');

        if(type == "Only to me"){
            filter = "onlyToMe"
        }

        if(type == "Important"){
            filter = "important"
        }

        if(type == "Follow up"){
            filter = "followUp"
        }

        filterMailsBy(filter,sharedServiceSelection)
    });

    $('.chart-mail-sentiment .ct-chart-donut .ct-series-a path, .chart-mail-sentiment .ct-chart-donut .ct-series-b path').click(function (evt) {
        evt.stopImmediatePropagation();

        var value = $(this).attr('ct:value');
        var type = $(this).attr('ct:meta');

        filterMailsBy(type,sharedServiceSelection)
    });
}

function filterMailsBy(filter,sharedServiceSelection) {
    sharedServiceSelection.refreshMailTable(filter)
}

function donutOnDraw(data){
    if(data.type === 'slice') {

        var pathLength = data.element._node.getTotalLength();

        data.element.attr({
            'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
        });

        var animationDefinition = {
            'stroke-dashoffset': {
                id: 'anim' + data.index,
                dur: 1000,
                from: -pathLength + 'px',
                to:  '0px',
                easing: Chartist.Svg.Easing.easeOutQuint,
                fill: 'freeze'
            }
        };

        if(data.index !== 0) {
            animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
        }

        data.element.attr({
            'stroke-dashoffset': -pathLength + 'px'
        });

        data.element.animate(animationDefinition, false);
    }
}

function formatMails(data,index){

    var fullName = data.emailId

    if(data.contact && data.contact.personName){
        fullName = data.contact.personName;
    } else if(data.firstName && data.lastName){
        fullName = data.firstName +" "+data.lastName
    } else if(data.firstName){
        fullName = data.firstName
    }

    var sentimentClass = "",sentimentClassBorder = "";

    if(data.sentiment == "Positive"){
        sentimentClass = "positive-css"
        sentimentClassBorder = "positive-border"
    }

    if(data.sentiment == "Negative"){
        sentimentClass = "negative-css"
        sentimentClassBorder = "negative-border"
    }

    if(data.type == "Only to me"){
        sentimentClass = "only-to-me"
    }

    if(data.type == "Follow up"){
        sentimentClass = "follow-up-color"
    }
    
    return {
        sentimentClassBorder:sentimentClassBorder,
        sentimentClass:sentimentClass,
        index:index,
        userId:data.userId,
        emailId:data.emailId,
        fullName:fullName,
        fullNameTrunc:getTextLength(fullName,15),
        // subject:getTextLength(data.title,25),
        subject:data.title,
        subjectFull:data.title,
        type:data.type,
        emailContentId:data.emailContentId,
        date:data.interactionDate,
        refId:data.refId,
        sentiment:data.sentiment,
        description:data.description,
        source:data.source,
        interactionDate:data.interactionDate,
        summary:data.summary?data.summary:null,
        mailSummaryExist:data.summary?true:false,
        showSummary:data.summary?true:false,
        dateFormatted:moment(data.interactionDate).format(standardDateFormat()),
        dateFormatted2:moment(data.interactionDate).format(standardDateFormat()) +" at "+moment(data.interactionDate).format("h:mm A"),
        hideEmailId:fullName == data.emailId,
        to:_.uniqBy(data.to,"emailId"),
        cc:_.uniqBy(data.cc,"emailId")
    }
}

function highlightSelectedRow(mails,activeMail){

    _.each(mails,function (el) {
        el.mailSelected = ""
    });

    activeMail.mailSelected = "active";
}

var tooltip = Chartist.plugins.tooltip();