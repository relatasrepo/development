var reletasApp = angular.module('reletasApp', ['ngRoute', 'ngSanitize', 'angular-loading-bar','ngLodash','datatables']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope) {
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent, yourNetwork, extendedNetwork, forCompanies) {

        var str = searchContent;

        if (typeof str == 'string' && str.length > 0) {
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
        } else toastr.error("Please enter search content")
    };
});

reletasApp.controller("logedinUser", function($scope, $http, share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function(response) {
            $(".seeMoreIcon").addClass("disabled-see-more")
            if (response.SuccessCode) {
                $scope.l_usr = response.Data;
                identifyMixPanelUser(response.Data);
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                    timezone = response.Data.timezone.name;
                }
                share.lUseEmailId = response.Data.emailId
                $scope.uniqueName = response.uniqueName;

            } else {

            }
        }).error(function(data) {

        })
});

reletasApp.service('share', function() {
    return {}
});

var timezone;

function getParams(url) {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while (match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

reletasApp.controller('peopleToConnect', function($scope, $http, $sce, DTOptionsBuilder, DTColumnDefBuilder,lodash) {

    $scope.textload = "Loading data. Please wait..";
    $scope.textProcess = function() {
        return $scope.textload
    }
    var vm = this;
    vm.persons = [];
    vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10).withLanguage({
            "oPaginate": {
                "sNext": "»",
                "sPrevious": "«"
            }
        })
        .withOption('columns', [{ "orderDataType": "dom-value" }, null, null, { "orderDataType": "dom-value" }, null])
        .withOption('order', [
            [1, "asce"]
        ])
        .withOption("bDestroy", true)
        .withOption('oLanguage', {
            "sEmptyTable": $scope.textProcess,
            "sLengthMenu": "Show _MENU_ for page",
            "processing": true
        });

    var columnOptions = function() {
        vm.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4).notSortable(),
            DTColumnDefBuilder.newColumnDef(5).notSortable(),
            DTColumnDefBuilder.newColumnDef(6).notSortable(),
            DTColumnDefBuilder.newColumnDef(7).notSortable()
        ];

        vm.dtInstance = {};

        vm.dtInstanceCallback = dtInstanceCallback;

        function dtInstanceCallback(dtInstance) {
            vm.dtInstance = dtInstance;
        }

        $scope.show_loadMore = false;

        var skip = 0;
        var limit = 100;

        $scope.textload = "Loading Data. Please wait..";
        if (vm.dtInstance && vm.dtInstance.DataTable) {
            vm.dtInstance.DataTable.clear().draw();
        }
    };

    $scope.noDataToShow = false;

    //Default Self
    paintCRRTable('self', 10);
    var userId = null;

    function paintCRRTable(message, li) {
        columnOptions();

        $http.get('/people/to/connect/info', {params : {infoFor:'detailsPage'}}).then(function(response) {
            if (response.data.SuccessCode) {

                var data = response.data.Data;
                var len = data.length;
                var obj = [];

                if(response.data.Data.length>0){
                    $http.get('/profile/get/current/web')
                        .success(function (response) {

                            if(response.SuccessCode){

                                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                                    var timezone = response.Data.timezone.name;
                                    // $scope.remindToConnect = moment(relation.remindToConnect).tz(timezone).format("DD MMM YYYY h:mm a")
                                    //$scope.remindToConnect = moment(relation.remindToConnect).tz(timezone).format("DD MMM YYYY")

                                    for (var i = 0; i < len; i++) {

                                        var cImg, profilePic;
                                        var influencerCss = 'fa fa-share-alt grey-color';
                                        var decisionMakerCss = 'fa fa-user grey-color';
                                        var contactImageLink = data[i].contactImageLink?encodeURIComponent(data[i].contactImageLink):null

                                        if (data[i].userId) {
                                            cImg = '/getImage/' + data[i].userId;
                                            profilePic = false;
                                        }else if(contactImageLink){
                                            profilePic = false;
                                            cImg = '/getContactImage/'+data[i].contactEmail+'/'+contactImageLink
                                        } else {
                                            profilePic = true;
                                            cImg = data[i].personName?data[i].personName.substr(0, 2).toUpperCase():data[i].emailId.substr(0, 2).toUpperCase()
                                        }

                                        if (data[i].relation && data[i].relation == 'influencer') {
                                            influencerCss = 'fa fa-share-alt orange-color';
                                        } else if (data[i].relation && data[i].relation == 'decision_maker') {
                                            decisionMakerCss = 'fa fa-user orange-color';
                                        }

                                        if(data[i].trackInfo){
                                            var emailOpens = data[i].trackInfo.emailOpens ? data[i].trackInfo.emailOpens : 0;
                                            var readOn = data[i].trackInfo.isRed?"First read on "+moment(data[i].trackInfo.lastOpenedOn).tz(timezone).format("DD MMM YYYY, h:mm a")+" ("+emailOpens+")":"Unread";
                                            if(data[i].action == 'sender' && data[i].trackInfo.trackOpen){
                                                var trackOpen = true;
                                            }
                                        }

                                        obj.push({
                                            company: data[i].company,
                                            value: data[i].value,
                                            email:data[i].contactEmail,
                                            lastInteracted: data[i].lastInteracted,
                                            designation: data[i].designation,
                                            contactName: data[i].personName,
                                            relation: data[i].relation,
                                            suggestion: data[i].suggestion,
                                            no_pic: profilePic,
                                            contactImage: cImg,
                                            decisionMakerCss:decisionMakerCss,
                                            influencerCss:influencerCss,
                                            contactId:data[i].contactId,
                                            publicProfileUrl: typeof data[i].publicProfileUrl != "undefined" ? '/' + data[i].publicProfileUrl : '/',
                                            index:i + 1,
                                            mobileNumber:data[i].mobileNumber,
                                            userId:data[i].userId,
                                            title:data[i].title ? data[i].title : null,
                                            refId:data[i].refId ? data[i].refId : null,
                                            reminder_notify:data[i].reminder_notify,
                                            interactionId:data[i].interactionId,
                                            emailContentId:data[i].emailContentId,
                                            ownerEmailId:data[i].ownerEmailId,
                                            description: data[i].description || null,
                                            showEmailBody:false,
                                            emailFetch:data[i].description ? false : true,
                                            remindToconnect:data[i].remindToconnect ? moment(data[i].remindToconnect).tz(timezone).format("DD MMM YYYY") : null,
                                            fetchingProblem: false,
                                            readOn:readOn,
                                            emailOpens:emailOpens,
                                            ownerId:data[i].ownerId,
                                            trackId:data[i].trackId,
                                            trackOpen:trackOpen ? trackOpen : null
                                        });
                                    }

                                    if (obj.length > 0) {
                                        vm.persons = obj
                                    } else {
                                        $scope.textload = "You have not set any reminders";
                                        $scope.interactions = [];
                                        if (vm.dtInstance && vm.dtInstance.DataTable) {
                                            vm.dtInstance.DataTable.clear().draw();
                                        }
                                        $scope.noDataToShow = true;
                                    }
                                }
                            }
                            else{
                                $scope.textload = "You have not set any reminders";

                                $scope.interactions = [];

                                if (vm.dtInstance && vm.dtInstance.DataTable) {
                                    vm.dtInstance.DataTable.clear().draw();
                                }
                                $scope.noDataToShow = true;
                            }
                        }).error(function (data) {
                    })
                } else {
                    $scope.textload = "You have not set any reminders";
                    $scope.interactions = [];
                    if (vm.dtInstance && vm.dtInstance.DataTable) {
                        vm.dtInstance.DataTable.clear().draw();
                    }
                    $scope.noDataToShow = true;
                }
            }
        });
    }

    $scope.noUniqueContacts = false;
    $scope.remindToConnectContacts = false;
    $http.get('/remind/to/connect/all')
        .success(function (response) {
            if(response.count>0){
                $scope.noOfContacts = response.count
                $scope.remindToConnectContacts = true;
            } else {
                $scope.remindToConnectContacts = false
            }
        });

    //Might be slower
    $scope.setReminder = function () {
        $http.get('/dashboard/unique/contacts/interacted')
            .success(function (response) {

                if(response.Data.uniqueContacts.length>0){
                    $scope.noUniqueContacts = false;
                } else {
                    $scope.noUniqueContacts = true;
                }
                var contactIds = lodash.map(response.Data.uniqueContacts, 'contactId');

                $http.post("/dashboard/contacts/remind", { contactIds: contactIds })
                    .success(function(response) {
                        if (response.SuccessCode) {
                            toastr.success("Successfully updated")
                            paintCRRTable();
                            $scope.noDataToShow = false

                        } else
                            toastr.error("Error! Please try again later")
                    });
            });
    }

    $scope.updateEmailOpen = function(emailId,trackId,userId){

        if(checkRequired(emailId) && checkRequired(trackId) && checkRequired(userId)){
            $http.get('/track/email/open/'+emailId+'/track/id/'+trackId+'/user/'+userId)
                .success(function(response){

                });
        }
    };

    $scope.viewEmail = function(item, flag){
        if(flag == 'yes')
            item.showEmailBody = item.showEmailBody ? false: true;
        if((item.description == null || item.fetchingProblem) && item.showEmailBody) {
            if(item.fetchingProblem) {
                item.description = null;
                item.emailFetch = true;
            }
            $http.get('/message/get/email/single/web?emailContentId=' + item.emailContentId + '&googleAccountEmailId=' + item.ownerEmailId)
                .success(function (response) {
                    if (response.SuccessCode) {
                        item.description = response.Data.data.replace(/\n/g, "<br />");

                        var temp = response.Data.data.replace(/\n/g, "<br />"); //to remove internal css
                        var start, end;
                        start = temp.indexOf("<style");
                        end = temp.indexOf("</style>");
                        end = end != -1 ? end + 8 : end;
                        var replace = temp.slice(start , end);
                        //item.description = item.description.replace(replace, "");

                        temp = item.description;
                        var index = temp.indexOf('/track/email/open/')
                        if(index != -1){
                            item.description = removeRelatasTrackImg(item.description, index)
                        }
                        item.description = $sce.trustAsHtml(item.description);
                        item.emailFetch = false;
                        item.fetchingProblem = false;
                    }
                    else {
                        item.description = response.Message || '';
                        item.emailFetch = false;
                        item.fetchingProblem = true;
                    }
                })
        }
        if(item.showEmailBody && item.trackOpen){
            $scope.updateEmailOpen(item.ownerEmailId,item.trackId,item.userId);
        }
    }

    $scope.actionTaken = function(userId, rName, rEmailId, rmobileNumber, type, source, index, suggestion, self_hierarchy) {
        var actionTaken = {
            contact: {
                userId: userId ? userId : null,
                name: rName ? rName : null,
                personEmailId: rEmailId ? rEmailId : null,
                mobileNumber: rmobileNumber ? rmobileNumber : null,
            },
            actionType: type,
            suggestion: suggestion ? suggestion : null,
            insightSource: source,
            suggestionIndexValue: index,
            self_hierarchy: self_hierarchy
        }
        $http.post("/insights/action/taken", { actionTaken: actionTaken })
            .success(function(response) {
                if (response.SuccessCode) {
                    // console.log(response);
                }
            });
    }

    $scope.removeItem = function(item, table){
        for(var i=0;i<table.length;i++){
            if(item == table[i])
                table.splice(i,1);
        }

    };
    
    $scope.updateRelation = function(contact, relation) {
        var value = null;
        var dm = 0;
        var inf = 0;
        var influencerCss = 'fa fa-share-alt grey-color';
        var decisionMakerCss = 'fa fa-user grey-color';

        if (relation == "influencer") {
            if (contact.relation != "influencer"){
                influencerCss = 'fa fa-share-alt orange-color';
                value = "influencer";
            }
        } else if (relation == "decision_maker") {
            if (contact.relation != "decision_maker"){
                decisionMakerCss = 'fa fa-user orange-color';
                value = "decision_maker";
            }
        }
        //update
        toastr.remove();
        $http.post('/insights/contact/relation', { contactId: contact.contactId, value: value })
            .success(function(data) {
                if (data.SuccessCode) {
                    // toastr.success("successfully updated")
                    contact.relation = value;
                    contact.decisionMakerCss = decisionMakerCss;
                    contact.influencerCss = influencerCss;
                } else {
                    toastr.error("try again")
                }
            })
    }

    $scope.updateInProcessValue = function(inProcess, contactId) {

        if (isNumber(inProcess)) {
            var obj = {
                value: inProcess,
                contactId: contactId
            }

            $http.post('/insights/contact/inProcess', obj)
                .success(function(response) {
                    if (response) {} else {
                        toastr.error("Error! Please try again later")
                    }
                });
        } else {
            //toastr.error("Please enter a valid number")
        }
    }

    $scope.ignore = function(item){
        if(item.reminder_notify == 'reminder'){
            $http.post('/people/to/connect/ignore', {contactId:item.contactId, update:'reminder', interactionId:item.interactionId})
                .success(function(response) {
                    if (response) {} else {
                        toastr.error("Error! Please try again later")
                    }
                });
        }
        else if(item.reminder_notify == 'notify'){
            $http.post('/people/to/connect/ignore', {contactId:item.contactId, update:'notify', interactionId:item.interactionId})
                .success(function(response) {
                    if (response) {} else {
                        toastr.error("Error! Please try again later")
                    }
                });
        }
    }

    $scope.showMore = function() {
        paintCRRTable('self', 0);
    }

    $scope.showEmailDiv = function(item, full) {
        $scope.resetFields();
        for (var i = 0; i < full.length; i++)
            if (item !== full[i])
                full[i].emailShow = false;
            if(item.title)
                $scope.compose_email_subject = 'Re: ' + item.title;
        item.emailShow ? item.emailShow = false : item.emailShow = true;
        if(item.trackOpen && item.emailShow){
            $scope.updateEmailOpen(item.ownerEmailId,item.trackId,item.userId);
        }
    }

    $scope.resetFields = function() {
        $scope.compose_email_subject = "";
        $scope.compose_email_body = "";
        $scope.compose_email_doc_tracking = true;
        $scope.compose_email_track_viewed = true;
        $scope.compose_email_remaind = true;
        $scope.url = "";
        $scope.companyName = "";
        $scope.designation = "";
    };

    $scope.sendEmail = function(subject, body, docTrack, trackViewed, remind, rName, rEmailId, receiverId, index, suggestion, self_hierarchy, refId, mobileNumber, item) {
        if (!checkRequired(subject)) {
            toastr.error("Please enter an email subject.")
        } else if (!checkRequired(body)) {
            toastr.error("Please enter an email body.")
        } else {
            $scope.actionTaken(receiverId, rName, rEmailId, mobileNumber, 'mail', 'peopleToConnect', index, suggestion, self_hierarchy);
            var obj = {
                receiverEmailId: rEmailId,
                receiverName: rName,
                message: body,
                subject: subject,
                receiverId: receiverId,
                docTrack: docTrack,
                trackViewed: trackViewed,
                remind: remind
            };
            if(refId){
                obj.updateReplied = true;
                obj.refId = refId;
                //obj.subject = item.title;
            }
            else{
                $scope.ignore(item)
            }

            $("#send-email-but").addClass("disabled");
            $(".send-email-but").addClass("disabled");
            $http.post("/messages/send/email/single/web", obj)
                .success(function(response) {
                    if (response.SuccessCode) {
                        $("#send-email-but").removeClass("disabled");
                        $(".send-email-but").removeClass("disabled");
                        $scope.resetFields();
                        $(".compose-email-inline").slideToggle(200);
                        toastr.success(response.Message);
                    } else {
                        $("#send-email-but").addClass("disabled")
                        toastr.error(response.Message);
                    }
                });
        }
    };
});

function isNumber(o) {
    return !isNaN(o - 0) && o !== null && o !== "" && o !== false;
}

function removeRelatasTrackImg(data, index){
    var temp = data;
    var f = temp.lastIndexOf('<img style=', index);
    var e = index + temp.substring(index).indexOf('\">');
    e = e != -1 ? e + 2 : e;
    var slice = temp.slice(f, e)

    return temp = temp.replace(slice, "")
    //return temp = temp;
}

reletasApp.directive('errortoggle', function() {
    return {
        link: function(scope, element, attrs) {
            element.bind('load', function(){
                
                if(scope.item)
                    scope.item.no_pic = false
                scope.$apply()
            })
        }
    }
});