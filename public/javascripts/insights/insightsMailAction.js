var relatasApp = angular.module('relatasApp', ['ngRoute', 'ngCookies','ngSanitize','ngLodash']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.controller("header_controller", function($scope, $http, meetingObjService) {
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = false
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function() {
        return "/todaySeries/leftMeetingBar"
    };

    $scope.searchFromHeader = function(searchContent, yourNetwork, extendedNetwork, forCompanies) {

        //var str = searchContent.replace(/[^\w\s]/gi, '');

        var str = encodeURIComponent(searchContent);

        if (typeof str == 'string' && str.length > 0) {
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;

            window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
        } else toastr.error("Please enter search content")
    };
    meetingObjService.searchFromHeader = $scope.searchFromHeader;

});

var timezone;
var showEmailGlobal = false;

relatasApp.service('meetingObjService', function() {
    return {}
});

relatasApp.factory('sharedServiceSelection', function($rootScope, $http) {
    var sharedService = {};
    sharedService.message = '';

    sharedService.updateActionTakenInfo = function(recordId,actionTakenType,$http,sharedServiceSelection,filter){
        updateActionTakenInfo(recordId,actionTakenType,$http,sharedServiceSelection,filter);
    }

    sharedService.emailBroadcast = function (mailDetails) {
        this.mailDetails = mailDetails;
        this.broadcastEmailObject();
    };

    sharedService.broadcastEmailObject = function () {
        $rootScope.$broadcast('handleEmailBroadcast')
    };

    return sharedService;
});

relatasApp.controller("logedinUser", function($scope, $http,$rootScope,meetingObjService) {

    $http.get('/profile/get/current/web')
        .success(function(response) {
            setTimeout(function() {
                $(".seeMoreIcon").addClass("disabled-see-more")
            }, 1000)

            if (response.SuccessCode) {
                $scope.l_usr = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.userName = response.Data.firstName;
                $scope.firstName = response.Data.firstName
                $scope.profilePicUrl = response.Data.profilePicUrl;
                identifyMixPanelUser(response.Data);
                if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                    timezone = response.Data.timezone.name;
                }

                meetingObjService.liuData = response.Data;

            } else {

            }
        }).error(function(data) {
    })
});

relatasApp.controller("mailAction", function($scope,$http,$rootScope,sharedServiceSelection,$window,searchService){

    sharedServiceSelection.refreshView = function(filter) {
        $scope.fetchMailsByFilter($scope.filterBy);
    }

    sharedServiceSelection.fetchModuleShared = function(filter){
        $scope.fetchMailsByFilter($scope.filterBy);
    };

    $scope.actionBoardClose = function () {
        $rootScope.actionBoardToggle = false;
        $rootScope.meetingSelected = null; //Reset previous selections.
        $rootScope.mailSelected = null;
    };

    $scope.filterBy = 'all';
    $scope.fetchMailsByFilter = function(filter){
        var url = '/fetch/email/by/filter?filter='+filter;
        $scope.filterBy = filter;
        
        $scope.isLoading = true;
        
        $http.get(url)
            .success(function(response){
                var mailActionItems = response.Data;
                var timezone = response.timezone ? response.timezone : 'UTC';
                $scope.mailActionItems = []

                var d = 'Declined: ',
                    a = 'Accepted: ',
                    t = 'Tentatively Accepted: ',
                    inv = 'Invitation: ',
                    auto = 'Automatic reply: ',
                    Ac = 'ACCEPTED: ',
                    ca = 'Canceled Event: ',
                    oo = 'Out Of Office Re: ',
                    ui = 'Updated Invitation: ';

                for(var i=0;i<mailActionItems.length;i++){

                    var title = mailActionItems[i].title

                    if(!_.includes(title, d) &&
                        !_.includes(title, a) &&
                        !_.includes(title, t) &&
                        !_.includes(title, inv) &&
                        !_.includes(title, auto) &&
                        !_.includes(title, Ac) &&
                        !_.includes(title, ca) &&
                        !_.includes(title, oo) &&
                        !_.includes(title, ui)){
                        $scope.mailActionItems.push(buildMailsToDisplay(mailActionItems[i],timezone));
                    }
                };

                $scope.currentPageMails = 0;
                $scope.pageSizeMails = 10;
                $scope.numberOfResponsesPending = mailActionItems.length;
                $scope.totalPagesMails = Math.ceil($scope.numberOfResponsesPending/$scope.pageSizeMails);

                $scope.mailActionItems.sort(function (o1, o2) {
                    return new Date(o2.sortDate) - new Date(o1.sortDate);
                });

                $scope.isLoading = false;

                $scope.numberOfPages = function(){
                    return Math.ceil($scope.numberOfResponsesPending/$scope.pageSizeMails);
                };
                $scope.actionBoardClose();
                $scope.mailActionNavClassToggle($scope.filterBy);
            });
    }

    $scope.mailActionNavClassToggle = function (filter) {

        $scope.underlineColorAll = "";
        $scope.underlineColorImp = "";
        $scope.underlineColorMr = "";
        $scope.underlineColorFu = "";

        if(filter === "all"){
            $scope.underlineColorAll = "all"
            $scope.underlineColorImp = "";
            $scope.underlineColorMr = "";
            $scope.underlineColorFu = "";
        }
        if(filter === "important"){
            $scope.underlineColorAll = ""
            $scope.underlineColorImp = "imp";
            $scope.underlineColorMr = "";
            $scope.underlineColorFu = "";
        }
        if(filter === "mailResponsePending"){
            $scope.underlineColorAll = "";
            $scope.underlineColorImp = "";
            $scope.underlineColorMr = "mail-resp";
            $scope.underlineColorFu = "";
        }
        if(filter === "followUp"){
            $scope.underlineColorAll = "";
            $scope.underlineColorImp = "";
            $scope.underlineColorMr = "";
            $scope.underlineColorFu = "follow-up";
        }

        $scope.mailActionNav = {};
        $scope.mailActionNav.all = filter != 'all' ? "mail-action-nav-toggle":"mail-action-nav";
        $scope.mailActionNav.important = filter != 'important' ? "mail-action-nav-toggle":"mail-action-nav";
        $scope.mailActionNav.responsePending = filter != 'mailResponsePending' ? "mail-action-nav-toggle":"mail-action-nav";
        $scope.mailActionNav.followUp = filter != 'followUp' ? "mail-action-nav-toggle":"mail-action-nav";
    }

    $scope.fetchMailsByFilter($scope.filterBy);
    $scope.mailActionNavClassToggle($scope.filterBy);
    $scope.replyToThisMail = function (item,filter,index,isReply) {

        $rootScope.mailSelected = null; // reset previous selections
        $rootScope.mailSelected = index;
        $rootScope.actionBoardToggle = true;
        $scope.showMeeting = false;
        $scope.showEmail = true;
        item.filter = filter;
        item.openForm = true;

        replyToThisMail($scope,$http,sharedServiceSelection,item);

        getPastInteractions($scope,$http,sharedServiceSelection,item)
    };

    $scope.actionBoardClose = function () {
        $rootScope.actionBoardToggle = false;
        $rootScope.mailSelected = null;
    };

    $scope.ignoreInsight = function(item){
        updateActionTakenDetails($scope,$http,sharedServiceSelection,item,'ignore');
    };

    $scope.numberOfAllMails = 0;

    function showNumberOfMails(filter){

        var url = '/fetch/email/by/filter?filter='+filter+'&dataFor=dashboard';

        if(filter === 'responsePendingAndImportant'){
            url = '/insights/your/responses/pending?days=7&filter=internalAndExternal&getPage=1&infoFor=detailsPage';
        }
        
        $http.get(url)
            .success(function(response){

                if(filter === 'responsePendingAndImportant'){
                    $scope.numberOfMailResponsePending = response.Data.explain.length;
                    $scope.numberOfAllMails = $scope.numberOfAllMails+$scope.numberOfMailResponsePending;
                }
                // if(filter == 'mailResponsePending'){
                //     $scope.numberOfMailResponsePending = response.Data.length;
                //     $scope.numberOfAllMails = $scope.numberOfAllMails+$scope.numberOfMailResponsePending;
                // }

                if(filter === 'followUp'){
                    $scope.numberOfFollowUp = response.Data.length;
                    $scope.numberOfAllMails = $scope.numberOfAllMails+$scope.numberOfFollowUp;

                }

                if(filter === 'important'){
                    $scope.numberOfImportant = response.Data.length;
                    $scope.numberOfAllMails = $scope.numberOfAllMails+$scope.numberOfImportant;
                }
            });
    }

    /*
    * WARNING! This code is extremely inefficient. Need to change this.
    * */

    showNumberOfMails('important');

    setTimeOutCallback(1000,function () {
        showNumberOfMails('followUp');
    })

    setTimeOutCallback(2000,function () {
        // showNumberOfMails('mailResponsePending');
        showNumberOfMails('responsePendingAndImportant');
    });

});

function getPastInteractions($scope,$http,sharedServiceSelection,item) {

    $scope.timelineItems = [];

    var emailId = item.personEmailId?item.personEmailId:item.emailId;

    nonSocialInteractions($scope,$http,emailId,$scope.timezone,function (nonSocialInteractions) {

        socialInteractions($scope,$http,emailId,$scope.timezone,function (socialInteractions) {

            $scope.timelineItems = nonSocialInteractions.concat(socialInteractions)
            $scope.timelineItems.sort(function (o1, o2) {
                return new Date(o2.dataObj.interaction.interactionDate) - new Date(o1.dataObj.interaction.interactionDate)
            });

            $scope.showMoreInteractions = false;

            if($scope.timelineItems.length >= 5){
                $scope.showMoreInteractions = true;
            }

        });
    });

    var userId = item.userId?item.userId:null;

    if(!userId){
        userId = emailId
    }

    if(userId){
        $scope.numberOfContacts = 0;
        var url ='/contacts/filter/common/web?id='+userId+'&skip=0&limit=36';
        getCommonConnections($scope, $http, url, false);
    }

    $http.get('/profile/get/current/web')
        .success(function (response) {

            var liuEmailId = response.Data.emailId;

            var url ='/contacts/filter/common/companyName/web?companyName='
                +response.Data.companyName+'&emailId='
                +liuEmailId+'&fetchWith='
                +emailId+'&skip=0'
                +'&limit=36';

            getCommonConnections_company($scope, $http, url, false);

        });

    var tempCounter = 99;

    $scope.donutChartId = tempCounter++;

    $scope.interaction_initiations_other_name = item?item.name:'';

    interactionsGraphs($scope,$http,emailId,$scope.donutChartId)

}

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords,fetchWatchlistType){
            return $http.post('/search/user/contacts', { "contactName" : keywords,"fetchWatchlistType":fetchWatchlistType });
        }
    }
}]);

function replyToThisMail($scope,$http,sharedServiceSelection,item) {
    sharedServiceSelection.emailBroadcast(item);
    // sharedServiceSelection.actionBoardDisplay("email");
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function getTextLength(text,maxLength){
    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}