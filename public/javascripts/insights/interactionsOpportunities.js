var reletasApp = angular.module('reletasApp', ['ngRoute', 'ngSanitize', 'angular-loading-bar','ngLodash','datatables']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope) {
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent, yourNetwork, extendedNetwork, forCompanies) {

        var str = searchContent;

        if (typeof str == 'string' && str.length > 0) {
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
        } else toastr.error("Please enter search content")
    };
});

var timezone;

function getParams(url) {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while (match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var contextEmailId = getParams(window.location.href).context;
var showContext = true;
if (!checkRequired(contextEmailId)) {
    showContext = false;
}

reletasApp.factory('teamMembers', function($http, $q) {
    var data = [],
        lastRequestFailed = true,
        promise;
    return {
        getTeamMembers: function() {
            if (!promise || lastRequestFailed) {
                // $http returns a promise, so we don't need to create one with $q
                promise = $http.get('/company/members')
                    .then(function(res) {
                        lastRequestFailed = false;
                        data = res.data;
                        return data;
                    }, function(res) {
                        return $q.reject(res);
                    });
            }
            return promise;
        }
    }
});

reletasApp.service('share', function() {
    return {
        setCompanyId:function(companyId){
            this.companyId = companyId;
        }
    }
});

reletasApp.factory('sharedServiceSelection', function($rootScope,$http,teamMembers) {
    var sharedService = {};

    sharedService.message = '';

    sharedService.prepForBroadcast = function(msg) {

        this.message = msg;
        this.broadcastItem();

        //Save hierarchy selection in the session

        teamMembers.getTeamMembers()
            .then(function(companyData) {

                var obj = companyData.Data.companyMembers;
                var companyArr = Object.keys(obj).map(function(k) {
                    return obj[k]
                });

                var url = '/session/hierarchy/selection';
                var hierarchyList = [];
                hierarchyList.push(companyData.Data.userId);
                url = fetchUrlWithParameter(url, "companyMembers", companyArr);

                if (msg == 'self') {
                    url = fetchUrlWithParameter(url, 'hierarchyList', hierarchyList);
                    url = fetchUrlWithParameter(url, 'getFor', 'self');
                } else if (msg == 'team') {

                    if(companyData.Data.teamMembers.length<=0){
                        //$scope.disableHierarchyButton = true
                        toastr.error("No-one reporting to you.")
                    }
                    var index = companyData.Data.teamMembers.indexOf(companyData.Data.userId);

                    if (index > -1) {
                        companyData.Data.teamMembers.splice(index, 1);
                    }
                    url = fetchUrlWithParameter(url, 'teamMembers', companyData.Data.teamMembers);
                    url = fetchUrlWithParameter(url, 'getFor', 'team');
                }
                $http.post(url)
            })
    };

    sharedService.broadcastItem = function() {
        $rootScope.$broadcast('handleBroadcast');
    };

    return sharedService;
});

reletasApp.controller("logedinUser", function($scope, $http, share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function(response) {
            $(".seeMoreIcon").addClass("disabled-see-more")
            if (response.SuccessCode) {
                $scope.l_usr = response.Data;
                $scope.firstName = response.Data.firstName
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.profilePicUrl = '/getImage/'+response.Data._id+'/'+new Date().toISOString()

                identifyMixPanelUser(response.Data);
                if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                    timezone = response.Data.timezone.name;
                }
                share.lUseEmailId = response.Data.emailId
                share.setCompanyId(response.Data.companyId)
            } else {

            }
        }).error(function(data) {

        })
});

reletasApp.controller("interactionTypes",function ($scope,$http,teamMembers,lodash,$interval,$timeout) {

    $scope.numOfDays = 90;
    $scope.teamSelection = {value:"all"};

    paintInteractionTypesGraph('/insights/interactions/by/type/for/team'+'?fromDate='+$scope.numOfDays,$http)

    $scope.forDays = function (days) {
        var url = '/insights/interactions/by/type/for/team'
        $scope.numOfDays = days
        if(days){
            url = url+'?fromDate='+days
            paintInteractionTypesGraph(url,$http)
        }  else {
            paintInteractionTypesGraph('/insights/interactions/by/type/for/team',$http)
        }

        paintInteractionsVsOpportunities($scope,$http,'/insights/opportunities/for/team'+'?fromDate='+$scope.numOfDays)
    }

    $scope.getGraph = function (select) {

        if(select.name == $scope.rootUser.userEmailId){
            $scope.initGraphIdShow = !$scope.initGraphIdShow
        } else {
            _.each($scope.graphs,function (g) {
                if(g.userEmailId == select.name){
                    g.show = !g.show;
                }
            });
        }

        interactionsByRelationship($scope,$http,select,true,$scope.numOfDays)
        getGraph($scope,$http,lodash,$scope.cacheResponse.interactionsNext60daysOpp,$scope.cacheResponse.closedOppPast60days,select.name,select.id)
    }

    function getGraph($scope,$http,lodash,interactionsNext60daysOpp,closedOppPast60days,user,id) {
        opportunitiesAndInteractions($scope,$http,lodash,interactionsNext60daysOpp,closedOppPast60days,user,id,$scope.cacheResponse.dateRange);
    }

    $scope.triggerChange = function (forContacts) {
        var url = '/insights/interactions/by/type/for/team'+'?fromDate='+$scope.numOfDays

        $scope.teamSelection["value"] = forContacts;

        if(forContacts == "external"){
            url = url+"&external=true"
        }

        if(forContacts == "internal"){
            url = url+"&external=false"
        }

        if(forContacts == "all") {
            url = '/insights/interactions/by/type/for/team'+'?fromDate='+$scope.numOfDays
        }

        paintInteractionTypesGraph(url,$http)

    }
    paintInteractionsVsOpportunities($scope,$http,'/insights/opportunities/for/team'+'?fromDate='+$scope.numOfDays)
    
    // $('#interactionTypes').on('click', '.ct-chart-bar .ct-series-a line, .ct-chart-bar .ct-series-b line, .ct-chart-bar .ct-series-c line', function(evt) {
    //     var index = $(this).index();
    //     var label = $(this).closest('.ct-chart-bar').find('.ct-labels foreignobject:nth-child('+(index+1)+') span').text();
    //     var value = $(this).attr('ct:value');
    //     var meta = $(this).parent().attr('ct:meta');
    //     graphClicked(index,label,value,meta,$scope,$interval);
    // });

    $scope.initGraph = function (node) {
        opportunitiesAndInteractions($scope,$http,lodash,$scope.cacheResponse.interactionsNext60daysOpp,$scope.cacheResponse.closedOppPast60days,node.userEmailId,node.id,$scope.cacheResponse.dateRange);
    }

})

function paintInteractionsVsOpportunities($scope,$http,url) {

    $http.get(url)
        .success(function (response) {

            $scope.cacheResponse = response;
            $scope.allOpenOpportunities = [];
            $scope.users = [];

            _.each(response.allOpenOpportunities,function (openOpp) {
                _.each(openOpp,function (opp) {
                    $scope.allOpenOpportunities.push(opp)
                    $scope.users.push(opp.userEmailId)
                })
            })

            _.each(response.openOppNext60days,function (openOpp) {
                _.each(openOpp,function (opp) {
                    $scope.allOpenOpportunities.push(opp)
                    $scope.users.push(opp.userEmailId)
                })
            })

            _.each(response.closedOppPast60days,function (openOpp) {
                _.each(openOpp,function (opp) {
                    $scope.allOpenOpportunities.push(opp)
                    $scope.users.push(opp.userEmailId)
                })
            })

            $scope.users = _.uniq($scope.users)

            $scope.graphs = []
            var i = 0,j=0;

            _.each(response.teamData,function (user) {
                var id = user.emailId.substring(0,3)+i++;
                $scope.graphs.push({id:id,userEmailId:user.emailId,fullName:user.firstName+" "+user.lastName,show:false,_id:user._id})
            });

            response.teamData.reverse();
            $scope.graphs.reverse();
            $scope.liu = response.teamData[0]

            userSelection($scope,$http,response.teamData,$scope.graphs);
            opportunitiesAndInteractions($scope,$http,null,$scope.cacheResponse.interactionsNext60daysOpp,$scope.cacheResponse.closedOppPast60days,response.teamData[0].emailId,"initGraphId",$scope.cacheResponse.dateRange);
            interactionsByRelationship($scope,$http,$scope.liu,null,$scope.numOfDays)

            $scope.rootUser = response.teamData[0];

            $scope.graphs.splice(0,1);
        });
}

function interactionsByRelationship($scope,$http,user,nextUser,from){

    $http.get("/insights/interactions/by/relationship?userId="+user._id+'&fromDate='+from)
        .success(function (response) {

            if(nextUser){
                _.each($scope.graphs,function (graph) {

                    if(graph.userEmailId == user.name){
                        graph["count"] = response
                    }
                })
            } else {
                user["count"] = response;
                user["count"]["fullName"] = user.firstName+" "+user.lastName
            }
        });
}

function userSelection($scope,$http,users,graphs){

    var user1 = users[0].emailId ;
    $scope.selection = {};
    $scope.selection["ids"] = {};
    $scope.selection["ids"][user1] = false;
    $scope.categories = [];

    _.each(graphs,function (user) {
        var obj = {};
        obj["name"] = user.userEmailId
        obj["fullName"] = user.fullName
        obj["id"] = user.id
        obj["_id"] = user._id
        $scope.categories.push(obj)
    })

    $scope.categories[0].checked = true;
}

function opportunitiesAndInteractions($scope,$http,lodash,interactions,closedOppPast60days,user,id,dateRange){

    for(var i=1;i<interactions.length;i++){
        interactions[i]._id["formattedDate"] = moment(interactions[i]._id.$dayOfYear).format("MMM Do YY")
    }

    var emailMatchRegexp = /#(.*)/;

    var result =  _.chain(interactions)
        .groupBy(function (interaction) {
            return interaction._id.formattedDate+'#'+interaction._id.ownerEmailId
        })
        .toPairs()
        .map(function(currentItem) {

            var match = emailMatchRegexp.exec(currentItem[0]);

            if(user == match[1]){
                var interactionsCount = 0;
                _.each(currentItem[1],function (el) {
                    if(user == el._id.ownerEmailId){
                        interactionsCount = interactionsCount+el.count
                    }
                });

                return {
                    ownerEmailId:currentItem[1][0]._id.ownerEmailId,
                    count:interactionsCount,
                    date:currentItem[0],
                    sortDate:new Date(currentItem[1][0]._id.$dayOfYear),
                    formatted:moment(currentItem[1][0]._id.$dayOfYear).format("MMM Do YY")
                }
            }
        })
        .value();

    var exitingDates = _.compact(_.map(result,"formatted"))
    var nonExistingDates = _.difference(dateRange,exitingDates);

    var zeroInteractionsDates = [];
    _.each(nonExistingDates,function (dt) {

        var dtItems = dt.split(" ")
        var month = Number(dateDictionary[dtItems[0]])
        var day = Number(dtItems[1].slice(0, -2))
        var year = Number('20'+dtItems[2])

        zeroInteractionsDates.push({
            ownerEmailId:user,
            count:0,
            sortDate:new Date(year,month-1,day),
            formatted:dt
        })
    })

    result = _.compact(result)

    var completeRangeWithCounts = _.concat(result,zeroInteractionsDates)

    completeRangeWithCounts.sort(function (o1, o2) {
        return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
    });

    var dates = _.map(completeRangeWithCounts,"sortDate")
    var count = _.map(completeRangeWithCounts,"count")


    id = "#"+id

    var chart = new Chartist.Bar(id, {
        labels: dates,
        series: [
            {
                meta:"Interactions",
                data:count
            }
        ]
    }, {
        axisY: {
            showGrid: true,
            onlyInteger:true
        },
        axisX: {
            offset: 10,
            showGrid: false
        },
        height: '250px',
        plugins: [
            tooltip,
            axisSettings2
        ]
    });

// Listen for draw events on the bar chart
    var opp = [].concat.apply([], closedOppPast60days)
    var oppAmounts = _.map(opp,"closedPast60Amount");
    var maxAmount = getMaxOfArray(oppAmounts)

    OnDraw(closedOppPast60days,user);
    
    function OnDraw(closedOppPast60days,user) {

        chart.on('draw', function(data) {
            // If this draw event is of type bar we can use the data to create additional content
            if(data.type === 'bar') {
                _.each(closedOppPast60days,function (opp) {
                    _.each(opp,function (o) {
                        if(o.userEmailId == user){
                            _.each(o.closedDates,function (date) {
                                if(moment(data.axisX.ticks[data.index]).isSame(moment(date.closedDate),"day")){

                                    var scaledVal = o.closedPast60Amount/maxAmount;

                                    var node = new Chartist.Svg('circle',{
                                        cx: data.x2,
                                        cy: data.y2-15,
                                        r:getCircleRadius(scaledVal),
                                        title:o.closedPast60Amount
                                    }, 'ct-slice-pie-1');

                                    data.group.append(node,function (a,b,c) {
                                        var html_to_insert = "<title>"+o.closedPast60Amount+", Date:"+moment(data.axisX.ticks[data.index]).format('MM-DD-YYYY')+"</title>"
                                        node._node.insertAdjacentHTML('beforeend', html_to_insert)
                                    });
                                }
                            })
                        }
                    })
                })

                data.element.attr({
                    style: 'stroke-width: 3px'
                });
            }
        });
    }
}

$('body').on('click', '.ct-slice-pie-1', function(){
    console.log("Hello")
    console.log($(this)["context"])
});

function paintInteractionTypesGraph(url,$http) {

    $http.get(url)
        .success(function (response) {

            var interactions = response.interactions;

            var result =  _.chain(interactions)
                .groupBy("_id.name")
                .toPairs()
                .map(function(currentItem) {
                    var data = {
                        owner:currentItem[0],
                        types:[]
                    }

                    if(currentItem[1]){
                        data["types"] = currentItem[1]
                    }

                    return data;
                })
                .value();

            var allInteractionTypes = ["call","email","meeting","meeting-comment","sms","twitter"] //Order for reference.

            _.each(result,function (el) {
                var existingInteractionTypes = _.map(el.types,'_id.type')

                var nonExistingInteractionTypes = _.difference(allInteractionTypes,existingInteractionTypes)
                _.each(nonExistingInteractionTypes,function (type) {
                    el.types.push({
                        _id:{
                            "ownerEmailId":el.owner,
                            "type":type
                        },
                        "count":0
                    })
                })
            })

            var teamMembers = _.map(result,'owner')
            var calls = [],emails = [],meetings = [],smss = [],twitters = [],meetingComments = [];

            _.each(teamMembers,function (tm) {
                _.each(result,function (data) {

                    if(tm === data.owner){

                        data.types.sort(function(a, b){
                            if(a._id.type < b._id.type) return -1;
                            if(a._id.type > b._id.type) return 1;
                            return 0;
                        });

                        _.each(data.types,function (type) {
                            if(type._id.type == 'email'){
                                emails.push(type.count)
                            }

                            if(type._id.type == 'call'){
                                calls.push(type.count)
                            }
                            if(type._id.type == 'meeting'){
                                meetings.push(type.count)
                            }
                            if(type._id.type == 'meeting-comment'){
                                meetingComments.push(type.count)
                            }
                            if(type._id.type == 'sms'){
                                smss.push(type.count)
                            }
                            if(type._id.type == 'twitter'){
                                twitters.push(type.count)
                            }
                        });
                    }
                });
            });

            for(var i=0;i<emails.length;i++){
                for(var j=0;j<meetingComments.length;j++){
                    emails[i] = emails[i]+meetingComments[i]
                }
            }

            // console.log(emails)
            // console.log(tempEmailsCount)
            //
            // emails = tempEmailsCount;

            var newSeries = [];

            _.each(allInteractionTypes,function (type) {
                if(type == 'email'){
                    newSeries.push({
                        meta:type,
                        data:emails
                    })
                }
                if(type == 'call'){
                    newSeries.push({
                        meta:type,
                        data:calls
                    })
                }
                if(type == 'meeting'){
                    newSeries.push({
                        meta:type,
                        data:meetings
                    })
                }
                // if(type == 'meeting-comment'){
                //     newSeries.push({
                //         meta:type,
                //         data:meetingComments
                //     })
                // }

                if(type == 'sms'){
                    newSeries.push({
                        meta:"SMS",
                        data:smss
                    })
                }
                if(type == 'twitter'){
                    newSeries.push({
                        meta:"Social",
                        data:twitters
                    })
                }
            });

            var strokeWidth = 'stroke-width: 16px';

            if(teamMembers.length>20 && teamMembers.length<30){
                strokeWidth = 'stroke-width: 12px'
            }

            if(teamMembers.length>30){
                strokeWidth = 'stroke-width: 8px'
            }

            new Chartist.Bar('#interactionTypes', {
                labels: teamMembers,
                series:newSeries
            }, {
                seriesBarDistance: 10,
                // reverseData: true,
                horizontalBars: true,
                stackBars: true,
                axisY: {
                    offset: 20
                },
                chartPadding: {
                    top: 20,
                    right: 20,
                    bottom: 30,
                    left: 35
                },
                // height: '400px',
                plugins: [
                    Chartist.plugins.legend({
                        position: 'bottom',
                        onClick: function (a,b)
                        {
                        }
                    }),
                    tooltip,
                    axisSettings
                ]
            },responsiveOptions).on('draw', function(data) {
                if(data.type === 'bar') {
                    data.element.attr({
                        style: strokeWidth
                    });
                }
            });
        });
}

function getCircleRadius(scaledVal) {

    if(Math.ceil(scaledVal*100)<=25){
        return 4
    }

    if(Math.ceil(scaledVal*100)<=50 && Math.ceil(scaledVal*100)>25){
        return 6
    }

    if(Math.ceil(scaledVal*100)<=75 && Math.ceil(scaledVal*100)>50){
        return 8
    }

    if(Math.ceil(scaledVal*100)<=100 && Math.ceil(scaledVal*100)>75){
        return 10
    }
}

var tooltip = Chartist.plugins.tooltip();

var axisSettings = Chartist.plugins.ctAxisTitle({
    axisX: {
        axisTitle: 'Interactions',
        axisClass: 'ct-axis-title',
        offset: {
            x: 0,
            y: 40
        },
        textAnchor: 'middle'
    },
    axisY: {
        // axisTitle: 'Team Members',
        axisTitle: '',
        axisClass: 'ct-axis-title',
        offset: {
            x: 0,
            y: -5
        },
        textAnchor: 'middle',
        flipTitle: false
    }
})

var axisSettings2 = Chartist.plugins.ctAxisTitle({

    axisX: {
        axisTitle: 'Days',
        axisClass: 'ct-axis-title',
        offset: {
            x: 0,
            y: 10
        },
        textAnchor: 'middle'
    },
    axisY: {
        axisTitle: 'Interactions',
        axisClass: 'ct-axis-title',
        offset: {
            x: 0,
            y: -5
        },
        textAnchor: 'middle',
        flipTitle: false
    }
})

function graphClicked(index,label,count,meta,$scope,$interval) {

    $scope.interactionTypeShow = true;

    $scope.label = {
        value:label
    };
    $scope.count = {
        value:count
    };
    $scope.meta = {
        value:meta+'s: '
    };
    $scope.$apply();

}

var responsiveOptions = [
    ['screen and (max-width: 900px)', {
        axisY: {
            labelInterpolationFnc: function (value) {

                var name = value.split(" ")
                return name[0];
            }
        }
    }]
];

var dateDictionary = {
    'Jan': '01',
    'Feb': '02',
    'Mar': '03',
    'Apr': '04',
    'May': '05',
    'Jun': '06',
    'Jul': '07',
    'Aug': '08',
    'Sep': '09',
    'Oct': '10',
    'Nov': '11',
    'Dec': '12'
}

var SumArray = function (arr) {
    var sum = [];
    if (arr != null && this.length == arr.length) {
        for (var i = 0; i < arr.length; i++) {
            sum.push(this[i] + arr[i]);
        }
    }

    return sum;
}
