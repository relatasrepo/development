
$(document).ready(function(){
    var relatasSocialAccounts;
    $.ajax({
        url:'/getRelatasHeaders',
        type:'GET',
        datatype:'JSON',
        success:function(accInfo){
            relatasSocialAccounts = accInfo;
            $("#relatasHeader").attr('src',accInfo.relatasHeader);
            $("#relatasTwitter").attr('src',accInfo.relatasTwitter);
            $("#relatasFacebook").attr('src',accInfo.relatasFacebook);
            $("#relatasLinkedin").attr('src',accInfo.relatasLinkedin);
            $("#relatasGray").attr('src',accInfo.relatasGrayLogo);
        }
    });

    $.ajax({
        url:'/getRelatasSocialAccounts',
        type:'GET',
        datatype:'JSON',
        success:function(accInfo){
            $("#relatasSocialTwitter").attr('href',accInfo.twitter)
            $("#relatasSocialFacebook").attr('href',accInfo.facebook)
            $("#relatasSocialLinkedin").attr('href',accInfo.linkedin)
            $("#relatasSocialTwitter2").attr('href',accInfo.twitter)
            $("#relatasSocialFacebook2").attr('href',accInfo.facebook)
            $("#relatasSocialLinkedin2").attr('href',accInfo.linkedin)
        }
    });
    
    $('#login-with-office365').on("click",function(){
        
        var reqObj = {
            onSuccess:'/onboarding/new',
            oAuthStep2:'/authorizeTest/',
            onFailure:'/',
            action:'customOutlookSignup'
        };

        window.location = '/outlook/v2.0/authenticate'+jsonToQueryString(reqObj);
    });

    $('#loginOutlook').on("click",function(){

        var reqObj = {
            onSuccess:'/onboarding/new',
            oAuthStep2:'/authorizeTest/',
            onFailure:'/',
            action:'customOutlookSignup'
        };

        window.location = '/outlook/v2.0/authenticate'+jsonToQueryString(reqObj);
    });

});

function jsonToQueryString(json) {
    return '?' +
        Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
}