var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','angular-loading-bar','ngLodash']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.factory('sharedServiceSelection', function($rootScope, $http) {
    var sharedService = {};
    sharedService.opportunity = '';

    sharedService.opportunitySaved = function (recordId) {
        this.recordId = recordId;
        this.broadcastOpportunitySaved();
    };

    sharedService.broadcastOpportunitySaved = function () {
        $rootScope.$broadcast('handleOpportunitySaved');
    };

    sharedService.message = '';

    sharedService.contactBroadcast = function () {
        this.broadcastContactObject();
    };

    sharedService.broadcastContactObject = function () {
        $rootScope.$broadcast('handleBroadcast')
    };

    sharedService.contactDetailsBroadcast = function (contact) {
        this.contact  = contact
        this.broadcastContactDetailsObject();
    };

    sharedService.broadcastContactDetailsObject = function () {
        $rootScope.$broadcast('getContactDetailsBroadcast')
    };

    sharedService.refreshLeftBarBroadcast = function () {
        this.refreshLeftBarBroadcastObject();
    };

    sharedService.refreshLeftBarBroadcastObject = function () {
        $rootScope.$broadcast('handleLeftBarBroadcast')
    };

    sharedService.setOpenOpportunitiesValue = function (amount,noOfOpenOpportunities) {
        this.openOpportunitiesValue = amount
        this.noOfOpenOpportunities = noOfOpenOpportunities
        this.broadcastValue();
    };

    sharedService.broadcastValue = function () {
        $rootScope.$broadcast('handleOpenOpportunitiesValue')
    };

    return sharedService;

});

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        setMeeting:function(m){
            this.meetingOb = m;
            this.isOk = true;
        },
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        getUserId:function(){
            return this.userId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setCompanyId:function(companyId){
            this.companyId = companyId;
        },
        getCompanyId:function(){
            return this.companyId;
        },
        setTown:function(geoLocationTown){
            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        },
        updateTaskStatus:function($scope,$http,taskId,status,fetchtasks){
            var reqObj = {
                taskId:taskId,
                status:!status ? 'notStarted':'complete'
            };
            $http.post('/task/update/status/web',reqObj)
                .success(function(response){
                    if(response.SuccessCode){
                        toastr.success(response.Message);
                        if(fetchtasks)
                            $scope.timelineTasks($scope.cuserId,$scope.cEmailId)
                    }
                    else toastr.error(response.Message);
                })
        }
    };
});

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        //var str = searchContent.replace(/[^\w\s]/gi, '');
        //var str = searchContent;
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }

        else toastr.error("Please enter search content")
    };
});

var timezone,l_userId;

relatasApp.controller("logedinUser", function ($scope, $http, share, $rootScope,$compile) {

    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        l_userId = response.Data._id;
    })

});

function checkMailNotifications($scope,$http) {

    $scope.notificationsExist = false;
    var previousNotifications = null;

    $http.get('/insights/mail/actions/meta')
        .success(function(response){
            if(response.SuccessCode){
                
                $scope.numOfImpMails = response.Data.important;
                previousNotifications = window.localStorage['insightsShown']?JSON.parse(window.localStorage['insightsShown']):null;
                if(previousNotifications){

                    if(previousNotifications && previousNotifications.dateCreated){
                        var activeSince = moment(previousNotifications.dateCreated)
                        var now = moment();

                        if(now.diff(activeSince, 'hours')>=12){
                            window.localStorage.removeItem("insightsShown");
                        }

                        if(response.Data.important != previousNotifications.numOfImpMails){
                            window.localStorage.removeItem("insightsShown");
                            $scope.notificationsExist = true;
                            window.localStorage['insightsShown'] = JSON.stringify({insightsMailShown:true,dateCreated:new Date(),numOfImpMails:response.Data.important})
                        }
                    }
                } else {
                    $scope.notificationsExist = true;
                    window.localStorage['insightsShown'] = JSON.stringify({insightsMailShown:true,dateCreated:new Date(),numOfImpMails:response.Data.important})
                }
            }
        });

}

relatasApp.controller("contact_details", function ($scope,$http,$rootScope,share,sharedServiceSelection,$window) {

    // checkMailNotifications($scope,$http);
    
    $scope.goToInsightsMails = function () {
        window.location = "/insights/mails"
    }

    $scope.inightsMailUrl = "/insights/mails"

    var autocomplete;
    function initAutocomplete() {

        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            {types:['(cities)']});

        autocomplete.addListener('place_changed', function(){
            $rootScope.detailsValue.p_location = $("#autocomplete").val();
            $rootScope.detailsValue.lat = autocomplete.getPlace().geometry.location.lat()
            $rootScope.detailsValue.lng = autocomplete.getPlace().geometry.location.lng()
            return false;
        });

        var autocomplete2 = new google.maps.places.Autocomplete(
            (document.getElementById('autocompleteCity')),
            {types:['(cities)']});

        autocomplete2.addListener('place_changed', function(){
            share.setTown($("#autocompleteCity").val())
            return false;
        });

        setTimeOutCallback(2500,function () {

            var autocomplete3 = new google.maps.places.Autocomplete(
                (document.getElementById('p_location')),
                {types:['(cities)']});

            autocomplete3.addListener('place_changed', function(){
                // share.setTown($("#p_location").val())
                return false;
            });
        })
    }

    window.initAutocomplete = initAutocomplete;

    share.setLastInteractedOn = function(dateText){
        $scope.lastInteraction = dateText;
    };

    $scope.openEmilContainer = function(){
        share.openEmilContainer()
    };

    $scope.editContact = function () {
        $scope.editContactModal = true
    }

    $scope.closeModal = function () {
        $scope.editContactModal= false
    }

    $scope.cancel = function () {
        $scope.closeModal();
    }

    $scope.$on('handleOpenOpportunitiesValue', function() {
        $scope.openOpportunitiesValue = sharedServiceSelection.openOpportunitiesValue
        $scope.noOfOpenOpportunities = sharedServiceSelection.noOfOpenOpportunities + " open"
    })

    $(document).on('keyup',function(evt) {
        if (evt.keyCode == 27) {
            $scope.$apply(function (){
                $scope.closeModal();
            })
        }
    });

    $scope.saveContact = function(name,designation,company,location,countryCode,mobileNumber,emailId) {

        if(!name || name === ''|| name === ' '){
            toastr.error("Contact name can not be empty");
        } else if(!location || location === ''|| location === ' '){
            toastr.error("Location name can not be empty");
        } else if(countryCode && mobileNumber){
            if(!isNumber(mobileNumber) || !isNumber(countryCode)){
                toastr.error("Country code and mobile number should be a valid number");
            } else if(emailId){
                if(!validateEmail(emailId)){
                    toastr.error("Email Id is not valid");
                } else {
                    saveContactDetails(name,designation,company,location,countryCode,mobileNumber,emailId,$rootScope.detailsValue.lat,$rootScope.detailsValue.lng)
                }
            } else {
                toastr.error("Please enter a valid country code & mobile number");
            }
        } else if($scope.editMobile && !$rootScope.noEditMobileNumber && emailId){
            toastr.error("Please enter a valid country code & mobile number")
        }else {
            saveContactDetails(name,designation,company,location,countryCode,mobileNumber,emailId,$rootScope.detailsValue.lat,$rootScope.detailsValue.lng)
        }
    }

    function saveContactDetails(name,designation,company,location,countryCode,mobileNumber,emailId,lat,lng) {

        var obj = {
            personName:name,
            designation:designation === 'Designation '?'':designation,
            companyName:company === 'Company '?'':company,
            location:location,
            lat:lat,
            lng:lng,
            mobileNumber:mobileNumber.replace(/\D/g, ''),
            personEmailId:validateEmail($scope.p_emailId)?$scope.p_emailId:emailId,
            contactId:$scope.p_id,
            matchByEmail:!$scope.p_id && validateEmail($scope.p_emailId)?true:false
        }

        $scope.p_name = name
        $scope.designation = designation
        $scope.companyName = company
        $scope.p_location = location;
        $scope.p_mobileNumber = mobileNumber;
        $scope.p_emailId = emailId;

        $http.post('/save/contact/details',obj)
            .success(function (response) {
                if(response.SuccessCode){
                    sharedServiceSelection.refreshLeftBarBroadcast();
                    $scope.closeModal()
                    toastr.success("Contact saved successfully")
                } else {
                    toastr.error(response.Message)
                }
            });
    }

    function isCConnectionFunctionExists(callback){
        if(typeof share.commonConnections == 'function'){
            callback();
        }
        else{
            setTimeout(function(){
                return isCConnectionFunctionExists(callback);
            },100)
        }
    }

    function isCConnectionFunctionExists_company(callback){
        if(typeof share.commonConnections_company == 'function'){
            callback();
        }
        else{
            setTimeout(function(){
                return isCConnectionFunctionExists(callback);
            },100)
        }
    }

    function isUserObjExists(callback){
        if(typeof share.l_user == 'object'){
            callback();
        }
        else{
            setTimeout(function(){
                return isUserObjExists(callback);
            },100)
        }
    }

    function isTimelineExists(callback){
        if(typeof share.timelineTimeline == 'function'){
            isUserObjExists(function(){
                callback();
            })
        }
        else{
            setTimeout(function(){
                return isTimelineExists(callback);
            },100)
        }
    }

    function isTopTrendingPostExists(callback){
        if(typeof share.topTrendingSocialPost == 'function'){
            callback();
        }
        else{
            setTimeout(function(){
                return isTopTrendingPostExists(callback);
            },100)
        }
    }

    share.updateRTC = function (time) {
        $scope.remindToConnect = time;
    }

    share.updateUserInfo = function(relation,connectedVia,profile,appConfig){

        $rootScope.isRelatasUser = checkRequired(relation.personId);
        $scope.lastInteraction = '';

        if(checkRequired(relation)){
            if(checkRequired(relation.personId)){
                $scope.p_pic = '/getImage/'+relation.personId
                $scope.no_pic = false;
                $scope.cUserId = relation.personId;
            }
            else if(checkRequired(relation.personName) && checkRequired(relation.personEmailId)){
                var contactImageLink = relation.contactImageLink?encodeURIComponent(relation.contactImageLink):null
                var contactImg = '/getContactImage/'+relation.personEmailId+'/'+contactImageLink
                $scope.no_pic = false;
                $scope.p_pic = contactImg
            } else {
                $scope.no_pic = true;
                if(profile && !relation.personEmailId && !relation.personName ){
                    $scope.p_pic = profile.firstName.substr(0,2).toUpperCase()
                } else {

                    $scope.p_pic = relation.personEmailId?relation.personEmailId.substr(0,2).toUpperCase():relation.personName.substr(0,2).toUpperCase()
                }
            }

            $scope.remindToConnect = ''

            if(checkRequired(relation.remindToConnect)) {
                $scope.remindToConnect = moment(relation.remindToConnect).tz(timezone).format("DD MMM YYYY, h:mm a")
            }

            $scope.companyName = relation.companyName?relation.companyName:'';
            $scope.userRelatas = relation.personId;

            $scope.designation = relation.designation?relation.designation: '';
            // $scope.p_position = $scope.getPosition(relation.companyName,relation.designation);

            $scope.p_name = relation.personName.replace("(null)", "") || '';
            if(!checkRequired($scope.p_name)){
                $scope.p_name = '';
            }
            if(profile && checkRequired(profile.firstName)){

                var str = profile.firstName.replace("(null)", "");
                $scope.p_firstName = str;
            }else $scope.p_firstName = $scope.p_name.replace("(null)", "");

            share.setSelectedParticipantName(checkRequired($scope.p_name) ? $scope.p_name : $scope.p_firstName);

            share.contactEmailId = relation.personEmailId;
            $scope.p_location = relation.location || '';
            $scope.p_mobileNumber = relation.mobileNumber || '';
            $scope.p_emailId = relation.personEmailId || '';
            $scope.p_favorite = relation.favorite || false;
            $scope.p_favoriteColor = relation.favorite == true ? 'color: #f86b4f;' : 'color: #777777!important';
            $scope.p_favoriteColor_a = relation.favorite == true ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
            $scope.p_id = relation._id;

            var existsUrl = '#';
            if(checkRequired(profile) && checkRequired(profile.publicProfileUrl)){
                if(profile.publicProfileUrl.charAt(0) != 'h'){
                    existsUrl = '/'+profile.publicProfileUrl
                }
            }
            else if(!checkRequired(existsUrl) && checkRequired(relation.publicProfileUrl)){
                if(relation.publicProfileUrl.charAt(0) != 'h'){
                    existsUrl = '/'+relation.publicProfileUrl
                }
            }
            $scope.scheduleMeeting = existsUrl;

            $scope.appFeature = false; //From the App configuration customize the settings
            $scope.respLimitHours = '';

            if(appConfig){
                $scope.appFeature = true;
            }

            if(isNumber($scope.p_mobileNumber)){
                $rootScope.noEditMobileNumber = true;
            } else {
                $rootScope.noEditMobileNumber = false;
            }

            $rootScope.detailsValue = {
                p_name:$scope.p_name,
                designation:$scope.designation,
                companyName:$scope.companyName,
                p_location:$scope.p_location,
                p_mobileNumberCC:'',
                p_mobileNumber:$scope.p_mobileNumber,
                p_emailId:$scope.p_emailId
            };

            $scope.editUnderline = '';
            if(!$scope.p_emailId && isNumber($scope.p_mobileNumber)){
                $scope.p_emailId = ' '
                $scope.editUnderline = 'edit-underline';
            }

        }
        // share.updateSendEmailContent("","",true,true,true,$scope.p_name,$scope.p_emailId,$scope.cUserId,$scope.companyName,$scope.designation,$scope.appFeature);
        $scope.l_userId = l_userId;
        $scope.p_id = relation._id;

        $scope.participantClick(relation.personId,relation.personEmailId,share.p_name,$scope.p_mobileNumber,share.l_user,relation);

        share.showRelation(relation,connectedVia,$scope.p_emailId,l_userId,$scope.p_id,$scope.p_mobileNumber);

    };

    $scope.participantClick = function(userId,emailId,nameR,mobileNumber,liu,contactDetails){

        isTimelineExists(function(){
            isUserObjExists(function(){
                share.timelineTimeline(userId,emailId,mobileNumber,liu,contactDetails);
            })
        });

        isTopTrendingPostExists(function(){
            share.topTrendingSocialPost(userId,emailId);
        })
    };

    share.getContactInfo = function(emailId,companyName,designation,name,url,userId,mobileNumber,filter){
        $rootScope.isDnt = share.isDnt;
        $rootScope.dntContactDetails = emailId?emailId:mobileNumber;
        share.participantClickPast90Days(userId,emailId,mobileNumber);
    };

    $scope.updateFavorite = function(p_id,favorite){

        var reqObj = {contactId:p_id,favorite:!favorite};

        $http.post('/contacts/update/favorite/web',reqObj)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.p_favorite = reqObj.favorite;
                    $scope.p_favoriteColor = reqObj.favorite == true ? 'color: #f86b4f;' : 'color: #777777!important';
                    $scope.p_favoriteColor_a = reqObj.favorite == true ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
                }
                else{
                    toastr.error(response.Message);
                }
            })
    }

});

relatasApp.controller("contact_relation",function($scope, $http, share, $route){

    var selectedCss = "cursor: pointer;color: #fff;background: #f86b4f !important;border: none;"
    var nonSelectedCss = "color: #777777!important;cursor: pointer;"

    share.showRelation = function(relation,connectedVia,p_emailId,l_userId,p_id,p_mobileNumber){

        $scope.l_userId = l_userId;
        $scope.p_id = p_id;
        if(connectedVia && connectedVia.length > 0){

        }
        else connectedVia = [];

        if(!relation.relation) {
            relation.relation = {};
        }

        var fetchWith = p_emailId;
        var contactMobile = null;
        var onlyWithMobile = false;
        if(!p_emailId && p_mobileNumber){
            fetchWith = p_mobileNumber;
            onlyWithMobile = true;
        }
        if(p_mobileNumber){
            contactMobile = p_mobileNumber;
        }

        $scope.tag = relation.hashtag;

        // getContactValues($scope,$http,fetchWith,relation,p_emailId,p_mobileNumber);

        /*Function to get Hashtags*/

        function getHashtags(fetchWith) {
            var url = '/contacts/details/' + fetchWith + '/relationship?fetchProfile=yes';
            if(contactMobile && !onlyWithMobile){
                url = url + '&mobileNumber=' + contactMobile;
            }
            $http.get(url)
                .success(function (response) {
                    $scope.tag = response.Data.relation.hashtag;
                });
        }

        $scope.p_emailIdtest = p_emailId;
        $scope.p_mobileNumber = p_mobileNumber;

        $scope.p_cxoColor = relation.relation.cxo?selectedCss:nonSelectedCss
        $scope.cxoStatus = relation.relation.cxo

        $scope.p_contactColor = relation.relation.prospect_customer == "contact" ? selectedCss : nonSelectedCss;
        $scope.p_leadColor = relation.relation.prospect_customer == "lead" ? selectedCss : nonSelectedCss;
        $scope.p_customerColor = relation.relation.prospect_customer == "customer" ? selectedCss : nonSelectedCss;
        $scope.p_prospectColor = relation.relation.prospect_customer == "prospect" ? selectedCss : nonSelectedCss;
        $scope.p_influencerColor = relation.relation.influencer == true ? selectedCss : nonSelectedCss;
        $scope.p_partnerColor = relation.relation.partner == true ? selectedCss : nonSelectedCss;
        $scope.p_decision_makerColor = relation.relation.decision_maker == true ? selectedCss : nonSelectedCss;
        $scope.p_relation_customer = "customer";
        $scope.p_relation_prospect = "prospect";
        $scope.p_relation_influencer = "influencer";
        $scope.p_relation_decision_maker = "decision_maker";
        $scope.p_relation_partner = "partner";
        $scope.p_relation_key_prospect_customer = 'prospect_customer';
        $scope.p_relation_key_decisionmaker_influencer = 'decisionmaker_influencer';

        $scope.p_relation_contact_value = relation.relation.prospect_customer == "contact" ? null :"contact";
        $scope.p_relation_lead_value = relation.relation.prospect_customer == "lead" ? null :"lead";
        $scope.p_relation_customer_value = relation.relation.prospect_customer == "customer" ? null :"customer";
        $scope.p_relation_prospect_value = relation.relation.prospect_customer == "prospect" ? null :"prospect";

        $scope.p_relation_influencer_value = relation.relation.influencer;
        $scope.p_relation_decision_maker_value = relation.relation.decision_maker;
        $scope.p_relation_partner_value = relation.relation.partner;

        if(!relation.relation.prospect_customer){
            $scope.p_relation_contact_value = relation.relation.prospect_customer == "contact" ? null :"contact";
            $scope.p_contactColor = selectedCss;
        }

        $scope.checkFunctionExistance('bindPName',function(){
            share.bindPName(share.getSelectedParticipantName())
        });

        share.interaction_initiations_name(share.getSelectedParticipantName())
        // share.bindPName_top_trending_post(share.p_name)
        // share.setSelectedParticipantName(share.p_name);
    };

    $scope.checkFunctionExistance = function(fName,callback){
        if(share[fName]){
            callback()
        }
        else{
            setTimeout(function(){
                $scope.checkFunctionExistance(fName,callback);
            },100)
        }
    };

    $scope.dntThisContact = function(l_userId,p_id,p_emailId,p_mobileNumber){

        var url = '/contacts/delete';
        if(p_id){
            url = url+"?contacts="+p_id;
        }

        // url = '/contacts/delete?contacts=54211d8bf3d7476e23eb2017,557769850c669a5715ba84c6,57441c16777f65de6863dcd7,57441c16777f65de6863d858,5b220b68c13ec1756a3d87e1';
        // url = '/contacts/start/tracking?contacts==abhijit_shriyan@infosys.com,09790714982,912222801130';

        $http.get(url)
            .success(function (response) {
                toastr.success("Contact successfully moved to DNT list");
                share.updateLeftList(p_emailId,p_mobileNumber);
            })
    }

    $scope.updateCXO = function(l_userId,p_id,cxoStatus){

        cxoStatus = !cxoStatus;
        var important = false;
        
        if(cxoStatus){
            important = true;
            $scope.p_cxoColor = 'color: #fff!important;background: #f86b4f !important;cursor: pointer;border: none;';
        } else {
            important = false;
            $scope.p_cxoColor = 'color: #777777!important;cursor: pointer';
        }

        $scope.cxoStatus = cxoStatus;
        $http.post('/contacts/update/cxo',{contactId:p_id,value:cxoStatus})
            .success(function(response){

                if($scope.p_relation_decision_maker_value){
                    important = true;
                } else if($scope.p_relation_partner_value){
                    important = true;
                } else if($scope.p_relation_influencer_value) {
                    important = true;
                } else if($scope.p_customerColor == selectedCss) {
                    important = true;
                } else if($scope.p_prospectColor == selectedCss) {
                    important = true;
                } else if($scope.p_leadColor == selectedCss) {
                    important = true;
                }

                $scope.updateFavorite(p_id,!important) // Coz of stupid code in the API. Not fixing now.;
            });
    }

    $scope.updateRelation = function(l_userId,p_id,relation,relationKey,value){

        var reqObj = {contactId:p_id,type:value,relationKey:relationKey};

        if(reqObj.relationKey === "decisionmaker_influencer"){
            if(relation == "decision_maker"){
                reqObj.value = !$scope.p_relation_decision_maker_value;
            } else if(relation == "partner"){
                reqObj.value = !$scope.p_relation_partner_value;
            } else {
                reqObj.value = !$scope.p_relation_influencer_value;
            }

            reqObj.type = relation;

            setPartners($scope,$http,reqObj,selectedCss,nonSelectedCss,relation,value)
        } else {

            $http.post('/contacts/update/reltionship/type',reqObj)
                .success(function(response){

                    if(response.SuccessCode){
                        if(relation == 'customer' || relation == 'prospect' || relation == 'contact' || relation == 'lead'){
                            $scope.p_customerColor = reqObj.type == "customer" ? selectedCss : nonSelectedCss;
                            $scope.p_prospectColor = reqObj.type == "prospect" ? selectedCss : nonSelectedCss;
                            $scope.p_contactColor = reqObj.type == "contact" ? selectedCss : nonSelectedCss;
                            $scope.p_leadColor = reqObj.type == "lead" ? selectedCss : nonSelectedCss;

                            $scope.p_relation_customer_value = value == "customer" ? null :"customer";
                            $scope.p_relation_prospect_value = value == "prospect" ? null :"prospect";
                            $scope.p_relation_contact_value = value == "contact" ? null :"contact";
                            $scope.p_relation_lead_value = value == "lead" ? null :"lead";
                        }

                        var important = false;

                        if($scope.p_relation_decision_maker_value){
                            important = true;
                        } else if($scope.p_relation_partner_value){
                            important = true;
                        } else if($scope.p_relation_influencer_value) {
                            important = true;
                        } else if($scope.p_customerColor == selectedCss) {
                            important = true;
                        } else if($scope.p_prospectColor == selectedCss) {
                            important = true;
                        } else if($scope.p_leadColor == selectedCss) {
                            important = true;
                        }

                        $scope.updateFavorite(reqObj.contactId,!important);
                    }
                    else{
                        toastr.error(response.Message);
                    }
                })
        }
    };

});

function setPartners($scope,$http,reqObj,selectedCss,nonSelectedCss,relation,value){

    $http.post('/contacts/update/reltionship/partner',reqObj)
        .success(function(response){

            if(relation == 'influencer'){
                $scope.p_influencerColor = reqObj.value == true ? selectedCss : nonSelectedCss;
                $scope.p_relation_influencer_value = reqObj.value;
            } else if(relation == "decision_maker") {
                $scope.p_decision_makerColor = reqObj.value == true ? selectedCss : nonSelectedCss;
                $scope.p_relation_decision_maker_value = reqObj.value ;
            } else {
                $scope.p_relation_partner_value = reqObj.value
                $scope.p_partnerColor = reqObj.value == true ? selectedCss : nonSelectedCss;
            }

            var important = false;

            if($scope.p_relation_decision_maker_value){
                important = true;
            } else if($scope.p_relation_partner_value){
                important = true;
            } else if($scope.p_relation_influencer_value) {
                important = true;
            } else if($scope.p_customerColor == selectedCss) {
                important = true;
            } else if($scope.p_prospectColor == selectedCss) {
                important = true;
            } else if($scope.p_leadColor == selectedCss) {
                important = true;
            }

            $scope.updateFavorite(reqObj.contactId,!important);
        })
}

relatasApp.controller("dntController", function ($scope, $http, share,$rootScope) {

    $scope.startTracking = function (contact) {
        var url = '/contacts/start/tracking?contacts='+contact;

        $http.get(url)
            .success(function (response) {
                toastr.success("Contact successfully removed from DNT list");
                share.updateLeftList(contact,contact)
            })
    }
});

relatasApp.controller("participantPast90Days", function ($scope, $http, share,$rootScope) {
    share.interaction_initiations_name = function(p_name){
        $scope.interaction_initiations_other_name = p_name;
    };
    share.participantClickPast90Days_again = function(){
        share.participantClickPast90Days($scope.userIdSelected,$scope.emailIdSelected);
    };
    share.participantClickPast90Days = function(userId,emailId,mobileNumber){

        if(share.isDnt){

        } else {

            $scope.userIdSelected = userId;
            $scope.emailIdSelected = emailId;

            var fetchWith = emailId
            disableEmail = false;
            if(!emailId) {
                disableEmail = true;
                fetchWith = mobileNumber.replace(/[^a-zA-Z0-9]/g,'')
            }

            var url = '/meeting/participant/'+fetchWith+'/relationship?fetchProfile=yes';
            if(isNumber(mobileNumber)){
                url = url+'&mobileNumber='+mobileNumber.replace(/[^a-zA-Z0-9]/g,'')
            }

            $scope.dataLoading = true;

            $http.get(url)
                .success(function(response){

                    $scope.dataFor = response.Data.dataFor;
                    if(response.SuccessCode){

                        share.updateUserInfo(response.Data.relation,response.Data.connectedVia,response.Data.profile,response.Data.appConfigFeature);

                        if(response.Data.relation && response.Data.relation.relationshipStrength_updated){
                            $scope.relationshipStrength_updated = response.Data.relation.relationshipStrength_updated;
                        }
                        else $scope.relationshipStrength_updated = 0;

                        if(response.Data.interactionInitiations && response.Data.interactionInitiations.length){
                            $scope.interaction_initiations_total = 0;

                            for(var i=0; i<response.Data.interactionInitiations.length; i++){
                                $scope.interaction_initiations_total += response.Data.interactionInitiations[i].count
                                if(response.Data.interactionInitiations[i]._id == 'you'){
                                    $scope.interaction_initiations_you = response.Data.interactionInitiations[i].count
                                }
                                if(response.Data.interactionInitiations[i]._id == 'others'){
                                    $scope.interaction_initiations_other = response.Data.interactionInitiations[i].count
                                }
                            }

                            var a = ($scope.interaction_initiations_you*100)/$scope.interaction_initiations_total
                            var b = ($scope.interaction_initiations_other*100)/$scope.interaction_initiations_total

                            a = Math.round(a)
                            b = Math.round(b)
                            $scope.drawDonut(a,b)
                        }
                        else{
                            $scope.interaction_initiations_total = 0
                            $scope.interaction_initiations_you = 0
                            $scope.interaction_initiations_other = 0
                            $scope.relationshipStrength_updated = 0
                            $scope.drawDonut(0,0)
                        }

                        if(response.Data.relationshipStrength && response.Data.relationshipStrength.length > 0){
                            var obj = response.Data.relationshipStrength[0];
                            var doc = response.Data.doc;

                            var x = obj.maxCount - obj.minCount;

                            var ratio = x/(doc.maxRatio-doc.minRatio);

                            var s = Math.round(doc.minRatio+((obj.maxCount - obj.minCount)/ratio))

                            $scope.relationshipStrength = s ? s : 0;

                        }
                        else $scope.relationshipStrength = 0;

                        if($scope.relationshipStrength_updated <= 0){
                            $scope.relationshipStrength_updated = $scope.relationshipStrength;
                        }
                        function updateRatio(value){
                            $scope.relationshipStrength_updated = value;
                            var reqObj = {contactId:response.Data.relation._id,relationshipStrength_updated:value};
                            $http.post('/contacts/update/relationship/strength/web',reqObj)
                                .success(function(response){
                                    if(response.SuccessCode){
                                        $("#relationshipStrength_updated").text(value);
                                    }
                                    else toastr.error(response.Message);
                                });
                        }

                        if($scope.slider){
                            $scope.slider.destroy();
                        };

                        $scope.slider = new Slider('#ex1', {
                            formatter: function(value) {
                                return 'Current value: ' + value;
                            },
                            min:0,
                            max:100,
                            value:$scope.relationshipStrength_updated
                        });
                        $('#ex1').on('slideStop', function(ev){
                            updateRatio(ev.value)
                        });

                        $scope.getInteractionTypeObj = function(obj,total){
                            switch (obj._id){
                                case 'google-meeting':
                                case 'meeting':return {
                                    priority:0,
                                    value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                    type:obj._id,
                                    count:obj.count,
                                    title:obj.count+' interactions'
                                };
                                    break;

                                case 'call':return {
                                    priority:1,
                                    value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                    type:obj._id,
                                    count:obj.count,
                                    title:obj.count+' interactions'
                                };
                                    break;

                                case 'sms':return {
                                    priority:2,
                                    value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                    type:obj._id,
                                    count:obj.count,
                                    title:obj.count+' interactions'
                                };
                                    break;

                                case 'email':return {
                                    priority:3,
                                    value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                    type:obj._id,
                                    count:obj.count,
                                    title:obj.count+' interactions'
                                };
                                    break;

                                case 'facebook':return {
                                    priority:4,
                                    value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                    type:obj._id,
                                    count:obj.count,
                                    title:obj.count+' interactions'
                                };
                                    break;

                                case 'twitter':return {
                                    priority:5,
                                    value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                    type:obj._id,
                                    count:obj.count,
                                    title:obj.count+' interactions'
                                };
                                    break;

                                case 'linkedin':return {
                                    priority:6,
                                    value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                    type:obj._id,
                                    count:obj.count,
                                    title:obj.count+' interactions'
                                };
                                    break;

                                default : return null;
                            }
                        };

                        $scope.calculatePercentage = function(count,total){
                            return Math.round((count*100)/total);
                        };

                        if(response.Data.interactionTypes && response.Data.interactionTypes.length > 0){
                            $scope.interactionsByType = [];
                            var existingTypes = [];
                            var allTypes = ['meeting','call','sms','email','facebook','twitter','linkedin'];
                            for(var t=0; t<response.Data.interactionTypes[0].typeCounts.length; t++){
                                var interactionTypeObj = $scope.getInteractionTypeObj(response.Data.interactionTypes[0].typeCounts[t],response.Data.interactionTypes[0].maxCount);
                                if(interactionTypeObj != null){
                                    existingTypes.push(interactionTypeObj.type);
                                    $scope.interactionsByType.push(interactionTypeObj);
                                }
                            }
                            var mobileCountStatus = 0;
                            var mobileCountStatusLists = ['call','sms','email'];
                            for(var u=0; u<allTypes.length; u++){
                                if(existingTypes.indexOf(allTypes[u]) == -1){
                                    var newObj = $scope.getInteractionTypeObj({_id:allTypes[u],count:0},response.Data.interactionTypes[0].maxCount)
                                    if(newObj != null){
                                        if(mobileCountStatusLists.indexOf(allTypes[u]) != -1){
                                            mobileCountStatus ++;
                                        }
                                        $scope.interactionsByType.push(newObj);
                                    }
                                }
                            }

                            if(mobileCountStatus > 0){
                                $scope.showDownloadMobileApp = true;
                            }else $scope.showDownloadMobileApp = false;
                            $scope.showSocialSetup = response.Data.showSocialSetup;

                            $scope.interactionsByType.sort(function (o1, o2) {
                                return o1.priority < o2.priority ? -1 : o1.priority > o2.priority ? 1 : 0;
                            });
                            $scope.interactionsByType = {
                                a:$scope.interactionsByType[0],
                                b:$scope.interactionsByType[1],
                                c:$scope.interactionsByType[2],
                                d:$scope.interactionsByType[3],
                                e:$scope.interactionsByType[4],
                                f:$scope.interactionsByType[5],
                                g:$scope.interactionsByType[6]
                            };

                        }
                        else{
                            $scope.interaction_initiations_total = 0;
                            $scope.interactionsByType = {"a":{"priority":0,"value":"height:0%","type":"meeting","count":0,"title":"0 interactions"},"b":{"priority":1,"value":"height:0%","type":"call","count":0,"title":"0 interactions"},"c":{"priority":2,"value":"height:0%","type":"sms","count":0,"title":"0 interactions"},"d":{"priority":3,"value":"height:0%","type":"email","count":0,"title":"0 interactions"},"e":{"priority":4,"value":"height:0%","type":"facebook","count":0,"title":"0 interactions"},"f":{"priority":5,"value":"height:0%","type":"twitter","count":0,"title":"0 interactions"},"g":{"priority":6,"value":"height:0%","type":"linkedin","count":0,"title":"0 interactions"}}
                        }

                        $scope.dataLoading = false;
                    }
                });
        }
    };

    $scope.drawDonut = function(a,b){

        var canvas  = document.getElementById("donut-chart");
        var chart = canvas.getContext("2d");

        function drawdountChart(canvas)
        {

            this.x , this.y , this.radius , this.lineWidth , this.strockStyle , this.from , this.to = null;
            this.set = function( x, y, radius, from, to, lineWidth, strockStyle)
            {
                this.x = x;
                this.y = y;
                this.radius = radius;
                this.from=from;
                this.to= to;
                this.lineWidth = lineWidth;
                this.strockStyle = strockStyle;
            }

            this.draw = function(data)
            {
                canvas.beginPath();
                canvas.lineWidth = this.lineWidth;
                canvas.strokeStyle = this.strockStyle;
                canvas.arc(this.x , this.y , this.radius , this.from , this.to);
                canvas.stroke();
                var numberOfParts = data.numberOfParts;
                var parts = data.parts.pt;
                var colors = data.colors.cs;
                var df = 0;
                for(var i = 0; i<numberOfParts; i++)
                {
                    canvas.beginPath();
                    canvas.strokeStyle = colors[i];
                    canvas.arc(this.x, this.y, this.radius, df, df + (Math.PI * 2) * (parts[i] / 100));
                    canvas.stroke();
                    df += (Math.PI * 2) * (parts[i] / 100);
                }
            }
        }
        var data =
        {
            numberOfParts: 2,
            parts:{"pt": [a , b ]},//percentage of each parts
            colors:{"cs": ["#f86b4f", "rgba(248, 107, 79, 0.65)"]}//color of each part
        };

        var drawDount = new drawdountChart(chart);
        drawDount.set(100, 100, 35, 0, Math.PI*2, 8, "#fff");

        if(data.parts.pt[0] == 0 && data.parts.pt[1] == 0){
            data = {
                numberOfParts: 2,
                parts:{"pt": [99 , 0 ]},//percentage of each parts
                colors:{"cs": ["#bbb", "#bbb"]}//color of each part
            };
        }

        drawDount.draw(data);
    }

});

var start = 1;
var disableEmail = false;

var itemsScroll = 6;
var itemsToShow = 6;

if(window.innerWidth < 480){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 1
    itemsScroll = 1
}else if(window.innerWidth < 900){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 3
    itemsScroll = 3
}
var end = itemsToShow;

function addHide(elementId,addHide){
    addHide ? $("#"+elementId).addClass('hide') : $("#"+elementId).removeClass('hide')
}

function getCommonConnections_company($scope, $http, url, isHides){

    $http.get(url, {
            ignoreLoadingBar: true
        })
        .success(function (response) {

            $scope.teamcConnections = [];
            $scope.totalTeamConnections = 0;
            $scope.loadingTeamConnections = false;

            if(response.SuccessCode) {

                if (response.Data.contacts && response.Data.contacts.length > 0) {

                    for (var i = 0; i < response.Data.contacts.length; i++) {
                        var li = '';
                        var obj;
                        if (checkRequired(response.Data.contacts[i].personId) && checkRequired(response.Data.contacts[i].personId.firstName)) {
                            var name = getTextLength(response.Data.contacts[i].personId.firstName + ' ' + response.Data.contacts[i].personId.lastName, 13);
                            var image = '/getImage/' + response.Data.contacts[i].personId._id;
                            var url = response.Data.contacts[i].personId.publicProfileUrl;
                            var emailId = response.Data.contacts[i].personEmailId;
                            if (url.charAt(0) != 'h') {
                                url = '/' + url;
                            }
                            obj = {
                                fullName: response.Data.contacts[i].personId.firstName + ' ' + response.Data.contacts[i].personId.lastName,
                                name: name,
                                image: image,
                                url: url,
                                emailId: emailId,
                                cursor: 'cursor:pointer',
                                idName: 'com_con_item_c_' + response.Data.contacts[i]._id,
                                className: 'hide'
                            };

                            $scope.teamcConnections.push(obj);
                        }
                        else {
                            obj = {
                                fullName: response.Data.contacts[i].personName,
                                name: getTextLength(response.Data.contacts[i].personName, 13),
                                image: null,
                                noPicFlag: true,
                                cursor: 'cursor:default',
                                url: null,
                                emailId: response.Data.contacts[i].personEmailId,
                            };
                            if (isHides && i < itemsToShow) {
                                obj.className = ''
                            }

                            obj.nameNoImg = obj.name.substr(0, 2).toUpperCase();
                            $scope.teamcConnections.push(obj);
                        }
                    }

                    $scope.currentPage = 0;
                    $scope.pageSize = 3;
                    $scope.numberOfteamcConnections = $scope.teamcConnections.length;
                    $scope.totalTeamConnections = $scope.teamcConnections.length;

                    $scope.numberOfPagesP = function(){
                        return Math.ceil($scope.teamcConnections.length/$scope.pageSize);
                    };

                }
            }
        });
}

function hidePrevNext(idNameP,idNameN,hide){
    if(hide){
        $("#"+idNameP).addClass('hide')
        $("#"+idNameN).addClass('hide')
    }
    else{
        $("#"+idNameP).removeClass('hide')
        $("#"+idNameN).removeClass('hide')
    }
}

relatasApp.controller("interactions_opportunities", function ($scope, $http,$rootScope,share,searchService,sharedServiceSelection,$sce){

    $scope.addOpportunity = function(){
        mixpanelTracker("Contacts>Add new opp ");
        window.location = "/opportunities/all?createOppForContact="+$scope.cEmailId
    }

    share.timelineTimeline = function(userId,emailId,mobileNumber,liu,contactDetails){

        sharedServiceSelection.contactDetailsBroadcast(contactDetails);

        function liuExists(){
            if(share.l_user){
                share.getImpactNumbers(contactDetails,liu)
            }
            else{
                setTimeout(function(){
                    liuExists();
                },1000)
            }
        }

        liuExists();

        $scope.cEmailId = emailId;
        $scope.cPhone = mobileNumber;
        $scope.contactDetails = contactDetails;
        $scope.liu = liu;
        $scope.add_cc = [];

        $scope.to = contactDetails.personName

        if(liu){
            $scope.body = '\n\n\n'+getSignature(liu.firstName+' '+liu.lastName,liu.designation,liu.companyName,liu.publicProfileUrl)
            $scope.replyBody = '\n\n\n'+getSignature(liu.firstName+' '+liu.lastName,liu.designation,liu.companyName,liu.publicProfileUrl)
        }

        var companyId = liu?liu.companyId:null

        $scope.hashtags = $scope.contactDetails.hashtag?$scope.contactDetails.hashtag: [];

        $http.get("/contact/get/all/data?participantEmailId="+emailId+"&mobileNumber="+mobileNumber+"&companyId="+ companyId)
            .success(function (response) {

                if(response.SuccessCode){

                    interactionsTimeline($scope,$http,share,$rootScope,userId,emailId,
                        response.Data.timezone?response.Data.timezone:"UTC",
                        response.Data.interactions[0]?response.Data.interactions[0].interactions:[],
                        response.Data.interactions[0]?response.Data.interactions[0].firstName:"",
                        response.Data.interactions[0]?response.Data.interactions[0].publicProfileUrl:""
                    )

                    opportunitiesTimeline($scope,$http,response.Data.opportunities,response.Data.companyDetails,sharedServiceSelection,share)
                    notesTimeline($scope,$http,response.Data.notes,response.Data.companyDetails)
                }
            });
    }

    $scope.viewItem = function(action,refId,dataObj,_id,emailAction){

        if(dataObj.interaction.interactionType == "email"){
            dataObj.isReplyModalOpen = true;
            viewItem($scope,$http,action,refId,dataObj,_id,emailAction,$sce)
        } else if(dataObj.interaction.interactionType == "meeting") {
            window.location = "/today/details/"+refId
        }
    };

    $scope.openCC = function () {
        $scope.openCCItem = !$scope.openCCItem;
    }

    $scope.getContactProfile = function (item) {
        getContactProfileInfo($http,item)
    }

    getOppStages();

    function getOppStages(){

        if(share.opportunityStages){
            $scope.stages = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(1000,function () {
                getOppStages()
            })
        }
    }

    $scope.schedule = function () {
        mixpanelTracker("Contacts>Schedule meeting with contact ");
        if($scope.scheduleMeeting && $scope.contactDetails.personId && $scope.contactDetails.personId != "" && $scope.contactDetails.personId != " "){
            window.location = $scope.scheduleMeeting
        }
    };

    $scope.isOppModalOpen = false;
    $scope.isEmailModalOpen = false;
    $scope.isNoteModalOpen = false;
    $scope.isHashtagModalOpen = false;

    $scope.editOpportunity = function(opportunity){
        window.location = "/opportunities/all?opportunityId="+opportunity.opportunityId
    }

    $scope.openEmailModal = function () {
        $scope.isEmailModalOpen = true;

        mixpanelTracker("Contacts>Send Email ");

        function checkLiuExists() {
            if(share.l_user || $scope.liu){

                var liu = share.l_user;

                $scope.body = '\n\n\n'+getSignature(liu.firstName+' '+liu.lastName,liu.designation,liu.companyName,liu.publicProfileUrl)
                $scope.replyBody = '\n\n\n'+getSignature(liu.firstName+' '+liu.lastName,liu.designation,liu.companyName,liu.publicProfileUrl)

            } else {
                setTimeOutCallback(1000,function () {
                    checkLiuExists()
                });
            }
        }

        checkLiuExists()
    }

    $scope.registerDatePickerIdRenewal = function(minDate,maxDate){
        pickDatesForRenewal($scope,minDate,maxDate,'#opportunityCloseDateSelector4',1,"years")
    }

    $scope.registerDatePickerId = function(minDate,maxDate){

        $('#opportunityCloseDateSelector').datetimepicker({
            value:$scope.opp.closeDateFormatted,
            timepicker:false,
            validateOnBlur:false,
            minDate: minDate,
            maxDate: maxDate,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    timezone = timezone?timezone:"UTC";
                    $scope.opp.closeDate = moment(dp).format();
                    $scope.opp.closeDateFormatted = moment(dp).format("DD MMM YYYY");
                });
            }
        });
    }

    $scope.openNoteModal =function () {
        mixpanelTracker("Contacts>Add notes ");
        $scope.isNoteModalOpen = true;
    }

    $scope.openHashtagModal = function () {
        mixpanelTracker("Contacts>Add new hashtag ");
        $scope.isHashtagModalOpen = true;
    }
    
    $rootScope.setRTC = function () {
        mixpanelTracker("Contacts>remind to connect ");
        $('.remindToConnectSelector').datetimepicker({
            value:new Date(),
            timepicker:true,
            // format: 'd.m.Y H:i',
            minDate: new Date(),
            format: 'd.m.Y',
            onSelectTime: function (dp, $input){
                var obj = {
                    remind:dp,
                    contactId:$scope.contactDetails._id,
                    contact:$scope.contactDetails.personEmailId?$scope.contactDetails.personEmailId:$scope.contactDetails.mobileNumber
                }


                $http.post('/contacts/remindtoconnect',obj)
                    .success(function(response){
                        if(response){
                            share.updateRTC(moment(dp).format("DD MMM YYYY, h:mm a"))
                            toastr.success("Reminder to connect set successfully")
                        } else {
                            toastr.error("Error! Please try again later")
                        }
                    });
            }
        });
    }

    $scope.closeModal = function (event) {
        $scope.mailOptions = false;
        $scope.isOppModalOpen = false;
        $scope.isEmailModalOpen = false;
        $scope.isNoteModalOpen = false;
        $scope.isHashtagModalOpen = false;
        if($scope.recentActivity && $scope.recentActivity.length){
            $scope.recentActivity.forEach(function(item){
                item.dataObj.isReplyModalOpen = false
            })
        }
    };

    $scope.actionToggle = "actionToggle";

    $scope.openMenu = function () {
        $scope.actionToggle = $scope.actionToggle ==""?"actionToggle":""
    }

    $scope.sendEmail = function(subject,body,reminder){

        function checkLiuExists() {
            if(share.l_user || $scope.liu){

                $scope.liu = share.l_user;

                sendEmail($scope,$http,subject,body,null,share,$scope.liu._id,$scope.cEmailId,$scope.cPhone,$scope.liu,$scope.contactDetails)
            } else {
                setTimeOutCallback(1000,function () {
                    checkLiuExists()
                });
            }
        }

        checkLiuExists()
    };

    $scope.replyEmail = function(replyBody,item){

        function checkLiuExists() {
            if(share.l_user || $scope.liu){

                $scope.liu = share.l_user;
                sendEmail($scope,$http,item.dataObj.compose_email_subject,replyBody,item,share,$scope.liu._id,$scope.cEmailId,$scope.cPhone,$scope.liu,$scope.contactDetails)
            } else {
                setTimeOutCallback(1000,function () {
                    checkLiuExists()
                });
            }
        }

        checkLiuExists()

    };

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.removeRecipient = function(contact,type){
        removeRecipient($scope,$http,contact,type)
    }

    $scope.removeCC = function(contact){
        var rmIndex = $scope.add_cc.indexOf(contact);
        $scope.add_cc.splice(rmIndex, 1);
    }

    $scope.addNoteForContact = function (note) {
        addNoteForContact($scope,$http,note,$scope.cEmailId)
    }

    $scope.showHashtags = function(hashtag){
        window.location = '/contact/connect?searchContent='+hashtag+'&yourNetwork=true&extendedNetwork=true&forCompanies=false';
    };

    $scope.deleteHashtag = function(hashtag){

        var hashtagIndex = $scope.hashtags.indexOf(hashtag);
        $http.get("/api/hashtag/delete?contactId="+$scope.contactDetails._id.toString()+"&hashtag="+hashtag.toString())
            .success(function(response){
                if(response.SuccessCode){
                    toastr.success("Hashtag Deleted");
                }
                else {
                    toastr.error("Hashtag Delete Failed. Please try again later");
                }
            });
        $scope.hashtags.splice(hashtagIndex, 1);
    };

    $scope.viewAllInteractions = function () {

        var fetchWith = $scope.cEmailId;
        if(isNumber($scope.cPhone) && !$scope.cEmailId){
            fetchWith = $scope.cPhone
        }
        var url = '/interactions?context='+fetchWith;
        if($scope.cPhone){
            url = url+'&mobileNumber='+$scope.cPhone.replace(/[^a-zA-Z0-9]/g,'')
        }

        window.location = url
    }

    $scope.addHashtag = function (hashtag) {
        addHashtag ($http,$scope.contactDetails._id,hashtag)
        $scope.hashtags.push(hashtag.replace(/[^\w\s]/gi, ''))
        toastr.success("Hashtag added!")
    };

    $(document).on('keyup',function(evt) {
        if (evt.keyCode == 27) {
            $scope.$apply(function (){
                $scope.closeModal();
            })
        }
    });

    $scope.addCCToList = function (item) {
        if(validateEmail(item.emailId)){
            $scope.add_cc.push(item.emailId)
            $scope.showResultscc = false;
            $scope.email_cc = '';
        } else {
            toastr.error("Please select a contact with valid email ID")
        }
    }

});

relatasApp.controller("commonConnections", function ($scope,$http,$rootScope,share,sharedServiceSelection){

    $scope.$on('getContactDetailsBroadcast', function() {
        // var id = sharedServiceSelection.contact.personId?sharedServiceSelection.contact.personId:sharedServiceSelection.contact.personEmailId
        /*Fix contacts issue and then use the above url*/

        $scope.hideCC = false;
        var id = sharedServiceSelection.contact.personId?sharedServiceSelection.contact.personId:null
        if(id){
            var url ='/contacts/filter/common/web?id='+id+'&skip=0';
            $scope.loadingCommonConnections = true;
            $scope.cConnections = []

            setTimeOutCallback(3000,function () {
                getCommonConnections($scope,$http,url)
            })
        } else {
            $scope.hideCC = true;
        }
    });

    $scope.goToContact = function (emailId) {
        goToContact(emailId)
    }
});

relatasApp.controller("teamConnections", function ($scope,$http,$rootScope,share,sharedServiceSelection){

    $scope.$on('getContactDetailsBroadcast', function() {

        var id = sharedServiceSelection.contact.personEmailId?sharedServiceSelection.contact.personEmailId:''

        var url ='/contacts/filter/common/companyName/web/v2?companyName='
            +share.l_user.companyName+'&emailId='
            +share.l_user.emailId+'&fetchWith='
            +id+'&skip=0'
            +'&limit=12';
        $scope.loadingTeamConnections = true;
        $scope.teamcConnections = [];
        setTimeOutCallback(2000,function () {
            getCommonConnections_company($scope, $http, url, false);
        })
    })

    $scope.goToContact = function (emailId) {
        goToContact(emailId)
    }

});

relatasApp.controller("contactImpact",function($scope,$http,$rootScope,share){

    var cEmailId = null;
    $scope.goToOpp = function () {
        window.location = "/opportunities/all?emailId="+cEmailId
    }
    
    $scope.loadingImpactValues = true;
    
    share.getImpactNumbers = function (contact,liu) {
        $scope.loadingImpactValues = true;

        $scope.ownerOpp = "";
        $scope.pOpp = "";
        $scope.dmOpp = "";
        $scope.iOpp = "";
        $scope.ownerAmt = "" ;
        $scope.pAmt = "" ;
        $scope.dmAmt = "";
        $scope.iAmt = "";
        $scope.impactValues = {}

        if(contact.personEmailId && liu && liu.companyId){

            cEmailId = contact.personEmailId;

            setTimeOutCallback(4000,function () {

                $http.get("/opportunities/contact/impact/value?emailId="+contact.personEmailId+"&companyId="+liu.companyId+"&netGrossMargin="+$rootScope.companyDetails.netGrossMargin)
                    .success(function (response) {
                        $scope.loadingImpactValues = false;

                        if(response.SuccessCode && response.Data){
                            $scope.impactValues = response.Data

                            var maxOcc=100,minOcc=0;
                            var maxAmt=100,minAmt=0;
                            var arrOcc = Object.keys( response.Data.occurrence ).map(function ( key ) { return response.Data.occurrence[key]; });
                            var arrAmt = Object.keys( response.Data.amount ).map(function ( key ) { return response.Data.amount[key]; });

                            minOcc = Math.min.apply( null, arrOcc );
                            maxOcc = Math.max.apply( null, arrOcc );

                            minAmt = Math.min.apply( null, arrAmt );
                            maxAmt = Math.max.apply( null, arrAmt );


                            response.Data.amount.oppOwnerAmount = response.Data.amount.oppOwnerAmount.r_formatNumber(2)
                            response.Data.amount.partnersAmount = response.Data.amount.partnersAmount.r_formatNumber(2)
                            response.Data.amount.dmAmount = response.Data.amount.dmAmount.r_formatNumber(2)
                            response.Data.amount.influencersAmount = response.Data.amount.influencersAmount.r_formatNumber(2)

                            $scope.ownerOpp = {'width':scaleBetween(response.Data.occurrence.oppOwnerCount, minOcc, maxOcc)+'%'};
                            $scope.pOpp = {'width':scaleBetween(response.Data.occurrence.partnersCount, minOcc, maxOcc)+'%'};
                            $scope.dmOpp = {'width':scaleBetween(response.Data.occurrence.dmCount, minOcc, maxOcc)+'%'};
                            $scope.iOpp = {'width':scaleBetween(response.Data.occurrence.influencersCount, minOcc, maxOcc)+'%'};

                            $scope.ownerAmt = {'width':scaleBetween(response.Data.amount.oppOwnerAmount, minAmt, maxAmt)+'%'};
                            $scope.pAmt = {'width':scaleBetween(response.Data.amount.partnersAmount, minAmt, maxAmt)+'%'};
                            $scope.dmAmt = {'width':scaleBetween(response.Data.amount.dmAmount, minAmt, maxAmt)+'%'};
                            $scope.iAmt = {'width':scaleBetween(response.Data.amount.influencersAmount, minAmt, maxAmt)+'%'};

                        }
                    })
            })

        } else {
            $scope.loadingImpactValues = false;
        }
    }
})

relatasApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        if(input){
            return input.slice(start);
        }
    }
});

function goToContact(emailId) {
    window.location = '/contacts/all?contact='+emailId+'&acc=true'
}

function getCommonConnections($scope, $http, url){

    $scope.cConnections = [];
    $scope.cConnectionsIds = [];

    $scope.currentPage = 0;
    $scope.pageSize = 3;
    $scope.numberOfContacts = 0;
    $scope.totalNumberCC = 0

    $http.get(url, {
            ignoreLoadingBar: true
        })
        .success(function (response) {
            $scope.loadingCommonConnections = false;
            $scope.isConnectionOpen = false;
            if(response.SuccessCode){
                $scope.grandTotal = response.Data.total;
                if(response.Data.contacts && response.Data.contacts.length > 0){

                    var html = '';

                    $scope.currentLength += response.Data.returned;

                    $scope.currentPage = 0;
                    $scope.pageSize = 3;
                    $scope.numberOfContacts = response.Data.contacts.length;
                    $scope.numberOfPages=function(){
                        return Math.ceil(response.Data.contacts.length/$scope.pageSize);
                    };

                    for(var i=0; i<response.Data.contacts.length; i++){
                        var li = '';
                        var obj;
                        if(checkRequired(response.Data.contacts[i].personId) && checkRequired(response.Data.contacts[i].personName)){

                            var name = getTextLength(response.Data.contacts[i].personName,13);
                            var image = '/getImage/'+response.Data.contacts[i].personId;

                            obj = {
                                fullName:response.Data.contacts[i].personName,
                                name:name,
                                image:image,
                                url:url,
                                cursor:'cursor:pointer',
                                idName:'com_con_item_'+response.Data.contacts[i]._id,
                                className:'hide'
                            };

                            obj.emailId = response.Data.contacts[i].personEmailId;
                            obj.mobileNumber = response.Data.contacts[i].mobileNumber?response.Data.contacts[i].mobileNumber.replace(/[^a-zA-Z0-9]/g,''):null

                            $scope.cConnections.push(obj);
                            $scope.cConnectionsIds.push('com_con_item_'+response.Data.contacts[i]._id);
                        }
                        else{
                            var contactImageLink = response.Data.contacts[i].contactImageLink?encodeURIComponent(response.Data.contacts[i].contactImageLink):null
                            obj = {
                                fullName:response.Data.contacts[i].personName,
                                name:getTextLength(response.Data.contacts[i].personName,13),
                                image:'/getContactImage/'+response.Data.contacts[i].personEmailId+'/'+contactImageLink,
                                // noPicFlag:true,
                                cursor:'cursor:pointer',
                                url:null,
                                idName:'com_con_item_'+response.Data.contacts[i]._id,
                                className:'hide'
                            };

                            obj.emailId = response.Data.contacts[i].personEmailId;
                            obj.mobileNumber = response.Data.contacts[i].mobileNumber?response.Data.contacts[i].mobileNumber.replace(/[^a-zA-Z0-9]/g,''):null
                            obj.nameNoImg = response.Data.contacts[i].personName.substr(0,2).toUpperCase();

                            $scope.cConnections.push(obj);
                            $scope.cConnectionsIds.push('com_con_item_'+response.Data.contacts[i]._id);
                        }
                    }

                    $scope.totalNumberCC = $scope.cConnections.length;

                }
                else $scope.isAllConnectionsLoaded = true;
            }
            else $scope.isAllConnectionsLoaded = true;
        })
}

function sendEmail($scope,$http,subject,body,prevMessage,share,userId,emailId,mobileNumber,liu,contactDetails,internalMailRecName){

    var respSentTimeISO = moment().format();
    var forOutlookBody = body

    if(!checkRequired(subject)){
        toastr.error("Please enter the subject.")
    }
    else if(!checkRequired(body)){
        toastr.error("Please enter the message.")
    }
    else{

        if(prevMessage && prevMessage.dataObj != null){
            body = body+'\n\n\n'+prevMessage.dataObj.bodyContent;
            $scope.reminder = prevMessage.dataObj.compose_email_remaind
        }

        var obj = {
            email_cc:$scope.add_cc && $scope.add_cc.length>0?$scope.add_cc:null,
            receiverEmailId:internalMailRecName?emailId:$scope.cEmailId,
            receiverName:$scope.contactDetails.personName,
            message:body,
            subject:subject,
            receiverId:$scope.contactDetails.personId,
            docTrack:true,
            trackViewed:true,
            remind:$scope.reminder,
            respSentTime:respSentTimeISO,
            isLeadTrack:false,
            newMessage:prevMessage?false:true,
            originalBody:forOutlookBody
        };

        if(internalMailRecName){
            obj.receiverEmailId = emailId
            obj.receiverName = internalMailRecName
            obj.receiverId = ''
        }

        if(prevMessage && prevMessage != null && prevMessage.updateReplied){
            obj.updateReplied = true;
            obj.refId = prevMessage.dataObj.interaction.refId
        }

        //Used for Outlook
        if(prevMessage && prevMessage.dataObj && prevMessage.dataObj.interaction && prevMessage.dataObj.interaction.refId){
            obj.id = prevMessage.dataObj.interaction.refId
        }

        $http.post("/messages/send/email/single/web",obj)
            .success(function(response){
                if(response.SuccessCode){
                    if(!internalMailRecName){
                        toastr.success("Email sent successfully");
                    }
                    share.timelineTimeline(userId,contactDetails.personEmailId,mobileNumber,liu,contactDetails);
                    setTimeout($scope.closeModal(),function () {
                    },3000)
                }
                else{
                    toastr.error("Email not sent. Please try again later");
                }
            })
    }
}

function notesTimeline($scope,$http,notes,companyDetails) {
    $scope.notes = notes
    $scope.notes.forEach(function(a) {
        a.text = _.unescape(a.text);
        a.dateFormatted = a.date ? moment(a.date).format("DD MMM YYYY") : null;
    });

    $scope.notes.sort(function (o1, o2) {
        return o1.date < o2.date ? 1 : o1.date > o2.date ? -1 : 0;
    });
}

function opportunitiesTimeline($scope,$http,opportunities,companyDetails,sharedServiceSelection,share) {
    $scope.opportunityTimeline = opportunities;

    var sumOfOpportunity = 0;
    var noOfOpenOpportunities = 0;
    var ngmReq = companyDetails && companyDetails.netGrossMargin;

    checkForStages()

    function checkForStages() {

        if($scope.stages){

            $scope.currency = "USD";
            share.currenciesObj = {};
            _.each(companyDetails.currency,function (el) {
                if(el.isPrimary){
                    share.currenciesObj[el.symbol] = el;
                    $scope.currency = el.symbol
                }
            })

            $scope.opportunityTimeline.forEach(function(a){
                a.ngmReq = ngmReq;
                a.amountWithNgm = a.amount;

                if(ngmReq){
                    a.amountWithNgm = (a.amount*a.netGrossMargin)/100
                }

                a.amountWithNgm = parseFloat(a.amountWithNgm.r_formatNumber(2))

                a.convertedAmt = a.amount;
                a.convertedAmtWithNgm = a.amountWithNgm

                if(a.currency && a.currency !== $scope.currency){

                    if(share.currenciesObj[a.currency] && share.currenciesObj[a.currency].xr){
                        a.convertedAmt = a.amount/share.currenciesObj[a.currency].xr
                    }

                    if(a.netGrossMargin || a.netGrossMargin == 0){
                        a.convertedAmtWithNgm = (a.convertedAmt*a.netGrossMargin)/100
                    }

                    a.convertedAmt = parseFloat(a.convertedAmt.toFixed(2))

                }

                a.closeDateFormatted = a.closeDate ? moment(a.closeDate).format("DD MMM YYYY") : null;
                a.wonOrLost = a.isClosed ?  (a.isWon ? 'Won' : 'Lost') : null;
                a.stageStyle = a.relatasStage;
                a.stageStyle2 = oppStageStyle(a.relatasStage,$scope.stages.indexOf(a.relatasStage),true);
                a.stage = a.relatasStage;
                a.isStale = a.closeDate && a.relatasStage !="Closed Lost" && a.relatasStage !="Closed Won" && a.relatasStage !="Close Lost" && a.relatasStage !="Close Won" && new Date(a.closeDate)<new Date()

                if(a.stageName !="Closed Lost" && a.stageName !="Closed Won" && a.stageName !="Close Lost" && a.stageName !="Close Won"){
                    sumOfOpportunity = sumOfOpportunity+a.convertedAmtWithNgm;
                    noOfOpenOpportunities++
                }
            });

        } else {
            setTimeOutCallback(1000,function () {
                checkForStages()
            })
        }
    }

    if($scope.currency){
        sharedServiceSelection.setOpenOpportunitiesValue($scope.currency+' '+getAmountInThousands(sumOfOpportunity,2,$scope.currency =="INR"),noOfOpenOpportunities);
    }

    $scope.opportunityTimeline.sort(function (o1, o2) {
        return o1.closeDate < o2.closeDate ? 1 : o1.closeDate > o2.closeDate ? -1 : 0;
    });

    $scope.productType = companyDetails && companyDetails.productList[0] && companyDetails.productList[0].name?companyDetails.productList[0].name:""
    $scope.zoneSelection = companyDetails && companyDetails.geoLocations[0] && companyDetails.geoLocations[0].region?companyDetails.geoLocations[0].region:""

    if(companyDetails && companyDetails.productList.length>0){
        companyDetails.productList.sort(function(a, b){
            if(a.name < b.name) return -1;
            if(a.name > b.name) return 1;
            return 0;
        })
    }
}

function viewItem($scope,$http,action,refId,dataObj,_id,emailAction,$sce) {
    switch(action){
        case 'viewDoc' : window.location = '/readDocument/'+refId;
            break;
        case 'viewStats' : $scope.showDocAnalytics(dataObj.emailId,refId,dataObj,_id)// view stats
            break;
        case 'viewEmail' :
            //if(!dataObj.composeMailBox && !dataObj.emailBodyBox){
            if(emailAction == 'reply'){
                dataObj.isView = false;
                if(!dataObj.composeMailBox){
                    //dataObj.doIgnore = doIgnore;
                    dataObj.composeMailBox = true;
                    dataObj.emailBodyBox = true;
                }
                else{
                    //dataObj.doIgnore = doIgnore;
                    dataObj.composeMailBox = false;
                    dataObj.emailBodyBox = false;
                }
            }
            else{
                if(!dataObj.isView){
                    //dataObj.doIgnore = false;
                    dataObj.isView = true;
                    dataObj.composeMailBox = false;
                    dataObj.emailBodyBox = true;
                    dataObj.loading = true;
                }
                else{
                    dataObj.composeMailBox = false;
                    dataObj.emailBodyBox = false;
                    dataObj.isView = false;
                }
            }
            if(checkRequired(dataObj.bodyContent) && typeof dataObj.bodyContent == "String"){
                dataObj.interaction.description = dataObj.bodyContent.replace(/\n/g, "<br />")
                dataObj.interaction.source = 'relatas'
            }

            if(checkRequired(dataObj.interaction.emailContentId) && dataObj.interaction.source != 'relatas'){
                $http.get('/message/get/email/single/web?emailContentId='+dataObj.interaction.emailContentId+'&googleAccountEmailId='+dataObj.interaction.googleAccountEmailId)
                    .success(function(response){
                        dataObj.loading = false;
                        if(response.SuccessCode){

                            dataObj.bodyContent = $sce.trustAsHtml(response.Data.data);

                            setTimeOutCallback(10,function () {
                                var myDomElement = document.getElementsByClassName("reply-to-this");
                                var selection = $( myDomElement ).find( "style" );
                                if(selection.length){
                                    selection.remove();
                                }
                            });

                            if(dataObj.updateOpened && dataObj.updateMailRead){
                                updateEmailOpen($http,dataObj.emailId,dataObj.trackId,dataObj.userId);
                            }
                        }
                        else dataObj.bodyContent = response.Message || '';
                    })
            }
            else{
                dataObj.loading = false;
                dataObj.bodyContent = dataObj.interaction.description.replace(/\n/g, "<br />");
                if(dataObj.updateOpened && dataObj.updateMailRead){
                    updateEmailOpen($http,dataObj.emailId,dataObj.trackId,dataObj.userId);
                }
            }
            dataObj.updateMailRead = dataObj.updateMailRead ? false : true;
            break;
        case 'viewTask' :// view stats
            if(!$scope[dataObj.bindHtmlKey]){
                var image = '/images/default.png';
                var ownerName = '';
                var dueDate = moment(dataObj.interaction.interactionDate).tz($scope.timezone);
                dueDate = dueDate.format("Do MMM");
                var title = dataObj.interaction.title || ''
                var description = dataObj.interaction.description || ''
                if(dataObj.interaction.action == 'sender'){
                    image = '/getImage/'+ $scope.cuserId;
                    ownerName = $scope.firstName;
                }
                else{
                    image = '/getImage/'+ share.l_user._id;
                    ownerName = $scope.rName;
                }

                $scope[dataObj.bindHtmlKey] = true;

                $("#"+_id).text("Close");
                var html2 = '<br><br><div style="border:1px solid #eee;padding:10px 0 0 20px; margin-bottom: 10px;margin-top: 7px;">' +
                    '<div class="row">' +
                    '<div class="col-xs-12">' +
                    '<div style="padding:0 0 0 10px" class="timeline-text-trunc">' +
                    '<strong>Owner: </strong><img class="image-placeholder-small" src='+image+' title='+ownerName+'><br><br>' +
                    '<strong>Due Date: </strong><span>'+dueDate+'</span><br>'+
                    '<strong>Title: </strong><span>'+title+'</span><br>'+
                    '<strong>Description: </strong><span>'+description+'</span><br><br>'+
                    '</div>' +
                    '</div>' +
                    //'<button id='+id+' style="float:right;margin-right: 36px;margin-bottom: 5px;" class="btn-green btn">Close</button>' +
                    '</div>' +
                    '</div>';
                $("#"+dataObj.bindHtmlKey).html(html2);
                $("#"+dataObj.bindHtmlKey).show();
            }
            else{
                $scope[dataObj.bindHtmlKey] = false;
                $("#"+dataObj.bindHtmlKey).hide();
                $("#"+_id).text(" View Task")
            }
            break;
        default:'';
            break;
    }
}

function interactionsTimeline($scope,$http,share,$rootScope,userId,emailId,timezone,interactions,firstName,publicProfileUrl){

    $scope.timezone = timezone;
    $scope.recentActivity = [];
    $scope.ifInteractions = false;

    if(interactions.length > 0){
        $scope.ifInteractions = interactions.length >= 5;
        $scope.show_show_more = interactions.length >= 5;
        if(checkRequired($scope.rName)){

        }
        else if(checkRequired(firstName) && firstName.length > 0){
            firstName.forEach(function(name){
                if(checkRequired(name)){
                    $scope.rName = name;
                }
            })
        }
        else $scope.rName = emailId;

        if(checkRequired(publicProfileUrl) && publicProfileUrl.length > 0){
            $scope.scheduleMeeting = '/'+publicProfileUrl[1];
        }

        if(interactions.length > 0){
            $scope.lastInteraction = moment(interactions[0].interactionDate).tz($scope.timezone).format("DD MMM YYYY")
            share.setLastInteractedOn(moment(interactions[0].interactionDate).tz($scope.timezone).format("Do MMM YYYY"));

            for(var i=0; i<interactions.length; i++){

                if($scope.contactDetails.personName){
                    var contactName = $scope.contactDetails.personName
                } else if($scope.contactDetails.personEmailId){
                    contactName = $scope.contactDetails.personEmailId
                } else {
                    contactName = $scope.contactDetails.mobileNumber
                }

                $scope.rName = contactName;
                
                var obj = buildInteractionObjectForTimeline(interactions[i],userId,$scope.firstName,$scope.rName,$scope.timezone,i);
                $scope.recentActivity.push(obj);
            }
        }
    }
}

relatasApp.directive('editInPlace', function() {
    return {
        restrict: 'E',
        scope: { value: '=' },
        template: '<span ng-click="edit()" ng-bind="value"></span><input ng-model="value"></input>',
        link: function ( $scope, element, attrs ) {
            // Let's get a reference to the input element, as we'll want to reference it.
            var inputElement = angular.element( element.children()[1] );

            // This directive should have a set class so we can style it.
            element.addClass( 'edit-in-place' );

            // Initially, we're not editing.
            $scope.editing = false;

            // ng-click handler to activate edit-in-place
            $scope.edit = function () {
                $scope.editing = true;

                // We control display through a class on the directive itself. See the CSS.
                element.addClass( 'active' );

                // And we must focus the element.
                // `angular.element()` provides a chainable array, like jQuery so to access a native DOM function,
                // we have to reference the first element in the array.
                inputElement[0].focus();
            };

            // When we leave the input, we're done editing.
            inputElement.prop( 'onblur', function() {
                $scope.editing = false;
                element.removeClass( 'active' );
            });
        }
    };
});

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function imageExists(url){
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status != 404;
}

function removeRelatasTrackImg(data, index){
    var temp = data;
    var f = temp.lastIndexOf('<img style=', index);
    var e = index + temp.substring(index).indexOf('\">');
    e = e != -1 ? e + 2 : e;
    var slice = temp.slice(f, e)

    return temp = temp.replace(slice, "")
    //return temp = temp;
}

function searchResults($scope,$http,keywords,share,searchService,type) {

    var selector = "showResults"+type;
    var typeSelector = type+"s"

    if(keywords && keywords.length > 2){

        searchService.search(keywords).success(function(response){

            if(response.SuccessCode){
                $scope[selector] = true;
                processSearchResults($scope,$http,response.Data,type);
            } else {
                $scope[selector] = false;
                $scope[typeSelector] = [];

                var obj = {
                    fullName: '',
                    name: '',
                    image: '/getContactImage/' + null + '/' + null,
                    emailId:keywords
                }

                $scope[typeSelector].push(obj)

            }
        }).error(function(){
            console.log('error');
        });
    } else {
        $scope[selector] = false;
        $scope[typeSelector] = [];
    }
}

function addHashtag ($http,p_id,hashtag){
    var str = hashtag.replace(/[^\w\s]/gi, '');
    var obj = { contactId:p_id, hashtag: str};
    $http.post("/api/hashtag/new",obj)
        .success(function (response) {

        });
}

function processSearchResults($scope,$http,response,type) {

    var typeSelector = type+"s";
    var contactsArray = response;
    $scope[typeSelector] = [];

    if(contactsArray.length>0){
        for(var i=0;i<contactsArray.length;i++){

            var obj = {};

            if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                var name = getTextLength(contactsArray[i].personName,20);
                var image = '/getImage/'+contactsArray[i].personId._id;

                obj = {
                    fullName:contactsArray[i].personName,
                    name:name,
                    image:image
                };

                obj.emailId = contactsArray[i].personEmailId;
                obj.twitterUserName = contactsArray[i].twitterUserName;

            }
            else {
                var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                obj = {
                    fullName: contactsArray[i].personName,
                    name: getTextLength(contactsArray[i].personName, 20),
                    image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                    emailId:contactsArray[i].personEmailId,
                    twitterUserName: contactsArray[i].twitterUserName
                    // noPicFlag:true
                };
            }

            if(obj.twitterUserName){
                obj.tweetAccExists = true;
            }

            obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
            obj._id = contactsArray[i]._id;
            obj.personId = contactsArray[i].personId && contactsArray[i].personId._id?contactsArray[i].personId._id:null;
            obj.type = type

            if(!userExists(obj.emailId) && validateEmail(obj.emailId)){
                $scope[typeSelector].push(obj)
            }

            function userExists(username) {
                return $scope[typeSelector].some(function(el) {
                    return el.emailId === username;
                });
            }
        }
    }
}

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            
            scope.imageNotLoaded = function(obj){

                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else{
                    obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    obj.noPPic = obj.name.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                //call the function that was passed
                scope.$apply(attrs.imageonload);
            });
        }
    };
});

relatasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

function addNoteForContact($scope,$http,contactNote,cEmailId) {

    contactNote = nl2br(contactNote)

    var url = '/message/create/by/id';

    var obj = {
        messageReferenceType:"contact",
        text:contactNote,
        cEmailId:cEmailId
    }

    $http.post(url, obj)
        .success(function (response) {
            if(response.SuccessCode){
                $scope.notes.push(response.Data)
                $scope.closeModal();
                toastr.success("Note added");
            }
            else {
                toastr.error("Failed to add note, try again later");
            }
        });
}

function getContactProfile (contactsArray,opportunityId){

    var contacts = [];

    if(contactsArray && contactsArray.length>0) {
        for (var i = 0; i < contactsArray.length; i++) {
            var obj = {};

            if (checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].fullName)) {

                var name = getTextLength(contactsArray[i].fullName, 10);
                var image = '/getImage/' + contactsArray[i].personId;

                obj = {
                    name: name,
                    image: image
                };
            }
            else {
                var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                obj = {
                    name: getTextLength(contactsArray[i].fullName, 10),
                    image: '/getContactImage/' + contactsArray[i].emailId + '/' + contactImageLink
                    // noPicFlag:true
                };
            }

            obj.fullName = contactsArray[i].fullName;
            obj.emailId = contactsArray[i].emailId;
            obj.opportunityId = opportunityId;
            obj.contactId = contactsArray[i].contactId;

            contacts.push(obj)
        }
    }

    return contacts;
}

function deleteHashtag($http,p_id,hashtag,contactEmailId) {

    if(p_id){
        $http.get("/api/opportunities/hashtag/delete?contactId=" + p_id + "&hashtag=" + hashtag+"&contactEmailId="+contactEmailId+"&type="+hashtag)
            .success(function (response) {
                if (response.SuccessCode) {
                    // toastr.success("Hashtag Deleted");
                }
                else {
                    // toastr.error("Hashtag Delete Failed. Please try again later");
                }
            });
    }
}

function updateRelationship($http,contact,relation,relationKey,relationType,value){

    if(contact.contactId){
        var id = contact.contactId
    } else {
        id = contact._id
    }

    var reqObj = {contactId:id,type:relation,relationKey:relationKey,relation:relationType,value:value};

    $http.post('/opportunities/contacts/update/reltionship/type',reqObj)
        .success(function(response){

        });
}

function getContactProfileInfo($http,item){
    $http.get('/contact/get/details?emailId='+item.emailId).success(function (response) {
        if(response){

            if(checkRequired(response.Data.contacts[0].personId) && checkRequired(response.Data.contacts[0].personName)){
                var name = getTextLength(response.Data.contacts[0].personName,20);
                var image = '/getImage/'+response.Data.contacts[0].personId._id;

                item["fullName"] = response.Data.contacts[0].personName
                item["name"] = checkRequired(response.Data.contacts[0].personName)?response.Data.contacts[0].personName:response.Data.contacts[0].personEmailId
                item["image"] = image
            }
            else {
                var contactImageLink = response.Data.contacts[0].contactImageLink ? encodeURIComponent(response.Data.contacts[0].contactImageLink) : null
                item["fullName"] = response.Data.contacts[0].personName
                item["name"] = checkRequired(response.Data.contacts[0].personName)?response.Data.contacts[0].personName:response.Data.contacts[0].personEmailId
                item["image"] = '/getContactImage/' + response.Data.contacts[0].personEmailId + '/' + contactImageLink
            }

            item["nameNoImg"] = item["name"].substr(0,2).toUpperCase()
        }
    })
}
