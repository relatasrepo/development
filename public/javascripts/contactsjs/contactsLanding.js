
reletasApp.controller("header_controller", function($scope,$http,share){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        //var str = searchContent.replace(/[^\w\s]/gi, '');
        //var str = searchContent;
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }

        else toastr.error("Please enter search content")
    };
    share.searchFromHeader = $scope.searchFromHeader;
});

var timezone ;
var styles = [
    {
        height: 53,
        url: "/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "/images/m1.png",
        width: 53
    }
]

reletasApp.controller("logedinUser", function ($scope, $http,share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {
            $(".seeMoreIcon").addClass("disabled-see-more")
            if(response.SuccessCode){
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.l_usr = response.Data;
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
                share.lUseEmailId = response.Data.emailId

                if(response.Data.contactsUpload && response.Data.contactsUpload.isUploaded){

                }else $("#uploadContactsAlert").removeClass("hide");
            }
            else{

            }
        }).error(function (data) {

        })
});

reletasApp.controller("contacts_data_controller",function($scope, $http, share){
    $scope.selectFile = function(){
        $("#selectedFile").trigger("click");
    };
    $scope.upload = function(file){
        var formData = new FormData();
        formData.append('contacts', file, file.name);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/contacts/upload/file', true);
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            var response = JSON.parse(xhr.response)
            if(response.SuccessCode){
                $("#uploadContactsAlert").addClass("hide");
                toastr.success(response.Message)
            }
            else{
                toastr.error(response.Message)
            }
        };
        // Send the Data.
        xhr.send(formData);
    };
    $("#selectedFile").on("change",function(){
        if($(this)[0] && $(this)[0].files && $(this)[0].files.length > 0){
            $scope.upload($(this)[0].files[0])
        }
        else toastr.error("Please select file to upload")
    });

    $scope.mySearchFunction = function(searchContent){
        if(typeof searchContent == 'string' && checkRequired(searchContent.trim())){
            share.searchFromHeader(searchContent,false,true,false);
            //window.location = '/contact/connect?searchContent='+searchContent;
        }
    };
    $scope.goToContactsSearch = function(searchContent){
        if(typeof searchContent == 'string' && checkRequired(searchContent.trim())){
            share.searchFromHeader(searchContent,false,true,false);
            //window.location = '/contact/connect?searchContent='+searchContent;
        }
    };

    share.updateTotalContacts = function(count){
        $scope.grandTotal = count || 0;
    }

    fetchTotalContacts($http,$scope);

    //refresh Google Contacts
    $scope.refreshGoogleContacts = function () {
        $http.get('/refresh/google/contacts')
            .success(function(response){

                if(response.SuccessCode){
                    toastr.success("Your contacts sync has been successful. Contacts added - "+response.Data.contactsAdded+ ".")
                    fetchTotalContacts($http,$scope);
                } else {
                    toastr.error("Contacts sync failed. Please try again later")
                }
            })
    };
});

reletasApp.controller("left_contacts_bar", function ($scope, $http,share) {
    $scope.contactsList = [];
    $scope.currentLength = 0;
    $scope.lastFetched = 0;
    $scope.filterBy = 'favorite'; //Default
    $scope.show_contacts_back = false;
    var isFilter = false;
    $scope.selectedFilter = {
        selection:'favorite'
    };

    $scope.searchContacts_filter = function(searchContent,checkedBoxes){

        var checked = [];
        for(var type in checkedBoxes){
            if(checkedBoxes[type]){
                $scope.selectedFilter["selection"] = checkedBoxes[type];
                checked.push(type);
            }
        }
        if(checked.length > 0){
            $scope.filterBy = checked.join(',')
            isFilter = true;
            $scope.contactsList = [];
            $scope.currentLength = 0;
            $scope.getContacts('/contacts/filter/custom/web',0,25,$scope.filterBy,searchContent);
        }
        else{
            isFilter = false;
            $scope.refreshContactsList()
        }
    };

    $scope.searchContacts = function(searchContent){

       if(checkRequired(searchContent)){
            $scope.filterBy = isFilter ? $scope.filterBy : 'search';
            $scope.contactsList = [];
            $scope.currentLength = 0;
            $scope.getContacts('/contacts/filter/custom/web',0,25,$scope.filterBy,searchContent);
        }
    };

    $scope.refreshContactsList = function(){
        if(!isFilter){
            $scope.filterSelectedleft = 'filter-selected';
            $scope.filterSelectedright = '';
        }

        $scope.filterBy = isFilter ? $scope.filterBy : 'favorite';
        $scope.searchContent = "";
        $(".searchContentClass").val("");
        $scope.contactsList = [];
        $scope.currentLength = 0;
        $scope.getContacts('/contacts/filter/custom/web',0,25,$scope.filterBy,'');
    };

    $scope.getContacts = function(url,skip,limit,filterBy,searchContent,isBack){

        if(checkRequired(searchContent)){
            searchContent = searchContent.replace(/[^\w\s]/gi, '');
            url = url+'?skip='+skip+'&limit='+limit+'&filterBy='+filterBy+'&searchContent='+searchContent
        }
        else url = url+'?skip='+skip+'&limit='+limit+'&filterBy='+filterBy;

        parseContactsFetched(url,isBack)
    };

    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    $scope.getContacts('/contacts/filter/custom/web',0,25,$scope.filterBy);

    $scope.loadMoreContacts = function(skip,limit,searchContent,isBack){
        if(isBack){
            $scope.currentLength = $scope.currentLength-25;
            skip = skip-25
        }
        if(skip < 25){
            skip = 0
            $scope.currentLength = 0;
        }
        $scope.getContacts('/contacts/filter/custom/web',skip,limit,$scope.filterBy,searchContent,isBack);
    };

    $scope.calculateFromTo = function(current){
        return current < 0 ? 0 : current
    };

    $scope.showInteractions = function(emailId,companyName,designation,name,url,userId,disableContext,mobileNumber){

        var fetchWith = emailId
        if(mobileNumber){
          mobileNumber = mobileNumber.replace(/[^a-zA-Z0-9]/g,'')
        }

        if(isNumber(mobileNumber) && !checkRequired(emailId)){
            fetchWith = mobileNumber
        }

        if(checkRequired(fetchWith) && checkRequired(mobileNumber) && mobileNumber !='undefined'){
            window.location = '/contact/selected?context='+fetchWith+"&mobileNumber="+mobileNumber;
        } else {
            window.location = '/contact/selected?context='+fetchWith;
        }
    };

    $scope.filterSelectedleft = 'filter-selected';
    $scope.sortContactsBy = function (sortBy) {

        $scope.selectedFilter["selection"] = sortBy;

        $scope.filterBy = sortBy;
        $scope.currentLength = 0

        var url = '/contacts/filter/custom/web?'+"skip="+0+"&limit="+25;
        url = url+"&filterBy="+sortBy;
        var isBack = true;
        if(sortBy == 'favorite'){
            $scope.filterSelectedleft = 'filter-selected';
            $scope.filterSelectedright = ''
        } else {
            $scope.filterSelectedleft = '';
            $scope.filterSelectedright = 'filter-selected'
        }

        parseContactsFetched(url,isBack)
    }

    function parseContactsFetched(url,isBack) {
        
        $http.get(url)
            .success(function(response){

                $scope.contactsList = [];
                $scope.isConnectionOpen = false;
                if(response.SuccessCode){
                    $scope.grandTotal = response.Data.total;
                    share.updateTotalContacts(response.Data.total);
                    if(response.Data.contacts.length > 0){
                        if(!isBack || $scope.currentLength <= 0){
                            $scope.currentLength += response.Data.returned;
                        }

                        if($scope.currentLength > 25){
                            $scope.show_contacts_back = true;
                        }else $scope.show_contacts_back = false;

                        for(var i=0; i<response.Data.contacts.length; i++){
                            var li = '';
                            var obj;
                            if(checkRequired(response.Data.contacts[i].personId) && checkRequired(response.Data.contacts[i].personId.firstName)){
                                var name = getTextLength(response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName,25);
                                var image = '/getImage/'+response.Data.contacts[i].personId._id;
                                var url = response.Data.contacts[i].personId.publicProfileUrl;
                                var companyName = checkRequired(response.Data.contacts[i].personId.companyName) ? response.Data.contacts[i].personId.companyName : "";
                                var designation = checkRequired(response.Data.contacts[i].personId.designation) ? response.Data.contacts[i].personId.designation : "";

                                if(!checkRequired(url)){
                                    url = "";
                                }
                                else if(url.charAt(0) != 'h'){
                                    url = '/'+url;
                                }

                                obj = {
                                    userId:response.Data.contacts[i].personId._id,
                                    fullName:response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName,
                                    companyName:getTextLength(companyName,25),
                                    designation:getTextLength(designation,25),
                                    fullCompanyName:companyName,
                                    fullDesignation:designation,
                                    emailId:response.Data.contacts[i].personEmailId,
                                    mobileNumber:response.Data.contacts[i].mobileNumber,
                                    name:name,
                                    image:image,
                                    url:url,
                                    cursor:'cursor:pointer',
                                    idName:'com_con_item_'+response.Data.contacts[i]._id,
                                    interactionType:response.Data.contacts[i].interactionType?getInteractionTypeImage(response.Data.contacts[i].interactionType):'',
                                    lastInteractedDate:response.Data.contacts[i].lastInteractedDate?moment(response.Data.contacts[i].lastInteractedDate).format("DD MMM YYYY"):''
                                };

                                if(checkRequired(response.Data.contacts[i].hashtag)) {
                                    if (response.Data.contacts[i].hashtag.length > 0) {
                                        obj.hashtag = "#";
                                    }
                                }

                                $scope.contactsList.push(obj)

                            }
                            else{
                                var companyName2 = checkRequired(response.Data.contacts[i].companyName) ? response.Data.contacts[i].companyName : ""
                                var designation2 = checkRequired(response.Data.contacts[i].designation) ? response.Data.contacts[i].designation : ""

                                //Don't display names with string 'null'
                                if(response.Data.contacts[i].personName && response.Data.contacts[i].personName != '  null'){

                                    var str = response.Data.contacts[i].personName.replace("(null)", "");
                                    var contactImageLink = response.Data.contacts[i].contactImageLink?encodeURIComponent(response.Data.contacts[i].contactImageLink):null

                                    obj = {
                                        userId:null,
                                        fullName:str,
                                        companyName:getTextLength(companyName2,25),
                                        designation:getTextLength(designation2,25),
                                        fullCompanyName:companyName2,
                                        fullDesignation:designation2,
                                        name:getTextLength(str,25),
                                        emailId:response.Data.contacts[i].personEmailId,
                                        mobileNumber:response.Data.contacts[i].mobileNumber,
                                        image:'/getContactImage/'+response.Data.contacts[i].personEmailId+'/'+contactImageLink,
                                        // noPicFlag:true,
                                        // noPPic:(response.Data.contacts[i].personName.substr(0,2)).capitalizeFirstLetter(),
                                        cursor:'cursor:pointer',
                                        url:null,
                                        idName:'com_con_item_'+response.Data.contacts[i]._id,
                                        interactionType:response.Data.contacts[i].interactionType?getInteractionTypeImage(response.Data.contacts[i].interactionType):'',
                                        lastInteractedDate:response.Data.contacts[i].lastInteractedDate?moment(response.Data.contacts[i].lastInteractedDate).format("DD MMM YYYY"):''
                                    };

                                    if(checkRequired(response.Data.contacts[i].hashtag)) {
                                        if (response.Data.contacts[i].hashtag.length > 0) {
                                            obj.hashtag = "#";
                                        }
                                    }
                                    $scope.contactsList.push(obj);
                                }
                            }
                        }
                    }
                    else {
                        $scope.isAllConnectionsLoaded = true;
                        //$scope.commonConnectionsNotExist = response.Message;
                    }
                }
                else{
                    $scope.isAllConnectionsLoaded = true;
                    //$scope.commonConnectionsNotExist = response.Message;
                }
            })
    }
});

reletasApp.controller("popup_content_cntlr",function($scope, $http, todayService){
    $scope.intItems = [];
    $scope.close_interaction_popup = function(){
        $("#map_cluster_click_popup").hide();
    };
    todayService.updatePopup = function(peopleMet,totalInteractions,interactionItems){
        $("#peopleMet").text(peopleMet);
        $("#totalInteractions").text(totalInteractions);
        $("#interactionItems").html(interactionItems);
        $("#map_cluster_click_popup").show();
    }
});

reletasApp.controller("global_map_all_interactions", function ($scope, $http, todayService) {
    $scope.time = 0;
    $scope.map_cluster_click_popup = false;
    $("#map_cluster_click_popup").hide();
    $scope.peopleMet = 0
    $scope.totalInteractions = 0
    $scope.interactionItems = []

    $scope.showUserLocation = function() {
        $scope.geocoder = new google.maps.Geocoder();
        $scope.myLatlng = new google.maps.LatLng(13.5333, 2.0833);

        $scope.mapOptions = {
            center: $scope.myLatlng,
            zoom: 2
        };

        $scope.map = new google.maps.Map(document.getElementById("map"), $scope.mapOptions);

        $scope.markerCluster = new MarkerClusterer($scope.map, [], {zoomOnClick: false, minimumClusterSize: 1, styles: styles});

        $scope.interactionsCount();
        google.maps.event.addListener($scope.markerCluster, 'clusterclick', function (cluster) {
            $scope.c = cluster.getMarkers();
            if (checkRequired($scope.c[0])) {
                $scope.showGlobalInteractionInfo($scope.c);
            }
        });
        google.maps.event.addListener($scope.map, 'zoom_changed', function () {
            if ($scope.map.getZoom() < 2) $scope.map.setZoom(2);
        });
    };

    $scope.interactionsCount = function() {
        $http.get('/dashboard/interactions/globalmap')
            .success(function(response){
                if(response.SuccessCode){
                    if (response.Data && response.Data.length > 0) {
                        for (var interaction = 0; interaction < response.Data.length; interaction++) {
                            if (checkRequired(response.Data[interaction]._id)) {
                                var result = response.Data[interaction]._id;
                                result.count = response.Data[interaction].count;

                                console.log(result)
                                $scope.showLocations(result);
                            }
                        }
                    }
                }
            })
    };

    $scope.showLocations = function(result) {
        if (checkRequired(result) && result.count !=0) {
            result.locationLatLang = null;
            if (checkRequired(result.locationLatLang)) {
                if (checkRequired(result.locationLatLang.latitude) && checkRequired(result.locationLatLang.longitude)) {
                    $scope.createMarkerLocationLatLang(result, result.locationLatLang.latitude, result.locationLatLang.longitude);
                } else if (checkRequired(result.location)) {
                    $scope.createMarkerGeocode(result, 0);
                } else  $scope.createMarkerCurrentLocation(result)
            } else if (checkRequired(result.location)) {
                $scope.createMarkerGeocode(result, 0);
            } else  $scope.createMarkerCurrentLocation(result)
        }
    };

    $scope.createMarkerLocationLatLang = function(result, lat, lang) {
        var l = '';
        if(checkRequired(result.currentLocation)){
            l = result.currentLocation.city;
            if(checkRequired(result.currentLocation.country)){
                l += ","+result.currentLocation.country
            }
        }
        if(!checkRequired(l)){
            l = result.location;
        }

        result.location = l;
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(lat), parseFloat(lang)),
            map: $scope.map,
            title: "Meeting location: " + result.location || '',
            user: result
        });
        $scope.markerCluster.addMarker(marker);
    };

    $scope.createMarkerCurrentLocation = function(result) {
        if (checkRequired(result.currentLocation)) {
            if (checkRequired(result.currentLocation.latitude)) {
                var l = result.currentLocation.city;
                if(checkRequired(result.currentLocation.country)){
                    l += ","+result.currentLocation.country
                }
                if(!checkRequired(l)){
                    l = result.location;
                }
                result.location = l;
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(result.currentLocation.latitude, result.currentLocation.longitude),
                    map: $scope.map,
                    title: "Meeting location: " + result.currentLocation.city || '',
                    user: result
                });
                $scope.markerCluster.addMarker(marker)
            }
        }
    };

    $scope.createMarkerGeocode = function(result, limit) {

        $scope.geocoder.geocode({ 'address': result.location}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var addr = results[0].address_components;
                var city = '';
                var country = '';
                var address = '';
                for(var i=0; i<addr.length; i++){
                    if(addr[i].types.indexOf("locality") != -1){
                        city = addr[i].long_name;
                    }
                    if(addr[i].types.indexOf("country") != -1){
                        country = addr[i].long_name;
                    }
                }
                if(checkRequired(city)){
                    address = city;
                }
                if(checkRequired(country)){
                    if(checkRequired(city)){
                        address += ", "+country
                    }
                    else address = country;
                }

                // console.log(result.location)

                result.location = address;
                var newAddress = results[0].geometry.location;

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(parseFloat(newAddress.lat()), parseFloat(newAddress.lng())),
                    map: $scope.map,
                    title: "Meeting location: " + address || '',
                    user: result
                });
                var latLangObj = {
                    userId: result._id,
                    latitude: newAddress.lat(),
                    longitude: newAddress.lng()
                };
                $scope.markerCluster.addMarker(marker);
                $scope.updateLocationLatLang(latLangObj);

            } else if (status == 'OVER_QUERY_LIMIT') {
                $scope.time += 500;
                limit++;
                if (limit < 3) {
                    setTimeout(function () {
                        $scope.createMarkerGeocode(result, limit);
                    },  $scope.time);
                }
            }
        });
    };

    $scope.updateLocationLatLang = function(latLangObj) {
        $http.post('/updateLocationLatLang',latLangObj)
            .success(function(response){

            });
    };

    $scope.showGlobalInteractionInfo = function(cluster) {

        var infoContent = cluster;

        $scope.interactionItems = [];
        $scope.totalInteractions = 0;
        $scope.peopleMet = cluster.length;
        infoContent.sort(function(a,b)
        {
            if (a.user.count < b.user.count) return 1;
            if (a.user.count > b.user.count) return -1;
            return 0;
        });
        var mmmmm = '';
        for (var m = 0; m < infoContent.length; m++) {
            $scope.totalInteractions += parseInt(infoContent[m].user.count || 0);
            var name = infoContent[m].user.firstName + ' ' + infoContent[m].user.lastName;
            var totalm = infoContent[m].user.count < 10 ? '0' + infoContent[m].user.count : infoContent[m].user.count;
            //totalm = infoContent[m].user.location+"("+totalm+")";
            var userUrl = '/interactions?context=' + $scope.getValidUniqueUrl(infoContent[m].user.emailId)
            //var summaryUrl = infoContent[m].user.emailId;

            mmmmm += '<div class="map-row clearfix"><span class="pull-left" ng-click="goToUrl(/interactions?context=summaryUrl)" title=' + name.replace(/\s/g, '&nbsp;') + '><a style="color:#333333" href=' + userUrl + '>' + getTextLength(name, 14) + '</a></span><span class="pull-right fs-14"><a style="color:#333333" href='+ userUrl + '>' + totalm + '</a></span></div>'
        }

        todayService.updatePopup($scope.peopleMet,$scope.totalInteractions,mmmmm);
        // $scope.map_cluster_click_popup = true;
    };

    $scope.getValidUniqueUrl = function(name){
        if(name.charAt(0) == 'h'){
            name = name.split('/');
            name = name[name.length - 1];
            return name;
        }
        else return name;
    };

    $scope.showUserLocation();

});

reletasApp.service('todayService', function () {
    return {}
});

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function fetchTotalContacts($http,$scope){
    $http.get('/contacts/filter/custom/web?skip=0&limit=25&filterBy=all')
        .success(function(response){
            $scope.contactsTotal = response.Data.total
        });
}

function getInteractionTypeImage (type){
    switch(type){
        case 'google-meeting':
        case 'meeting':
            return "fa-calendar-check-o green-color";
            break;
        case 'document-share':
            return "fa-file-pdf-o";
            break;
        case 'message':
        case 'meeting-comment':
        case 'email':
            return "fa-envelope green-color";
            break;
        case 'sms':
            return "fa-reorder green-color";
            break;
        case 'call':
            return "fa-phone green-color";
            break;
        case 'task':
            return "fa-check-square-o";
            break;
        case 'twitter':
            return "fa-twitter-square twitter-color";
            break;
        case 'facebook':
            return "fa-facebook-official fb-color";
            break;
        case 'linkedin':
            return "fa-linkedin-square linkedin-color";
            break;
        case 'calendar-password':
            return "fa-calendar-check-o green-color";
            break;
        default :return '';
    }
};
