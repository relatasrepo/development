

reletasApp.controller("dashboard_past_7_day_interactions_by_contact_type", function ($scope, $http, share) {
    $http.get('/dashboard/past/days/interacted/info')
        .success(function (response) {
            if(response.SuccessCode){
                $scope.companies = $scope.getValidStringNumber(response.Data.companies || 0);
                $scope.favorite = $scope.getValidStringNumber(response.Data.favorite || 0);
                if(response.Data.types && response.Data.types.length > 0){
                    $scope.customer = $scope.getValidStringNumber(response.Data.types[0].customer || 0);
                    $scope.prospect = $scope.getValidStringNumber(response.Data.types[0].prospect || 0);
                    $scope.influencer = $scope.getValidStringNumber(response.Data.types[0].influencer || 0);
                    $scope.decision_maker = $scope.getValidStringNumber(response.Data.types[0].decision_maker || 0);
                }
                else {
                    $scope.customer = $scope.getValidStringNumber(0);
                    $scope.prospect = $scope.getValidStringNumber(0);
                    $scope.influencer = $scope.getValidStringNumber(0);
                    $scope.decision_maker = $scope.getValidStringNumber(0);
                }
            }
            else{
                $scope.companies = $scope.getValidStringNumber(0);
                $scope.favorite = $scope.getValidStringNumber(0);
                $scope.customer = $scope.getValidStringNumber(0);
                $scope.prospect = $scope.getValidStringNumber(0);
                $scope.influencer = $scope.getValidStringNumber(0);
                $scope.decision_maker = $scope.getValidStringNumber(0);
            }
        }).error(function (data) {

        });



    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };
});

reletasApp.controller("top_companies_locations_interaction_by_type", function ($scope, $http, share) {
    $scope.interactionsByCompany = [];
    $scope.interactionsByLocation = [];
    $scope.interactionsByType = [];
    $scope.totalInteractionsCount = 0;
    $http.get('/dashboard/past/days/interacted/info/type')
        .success(function (response) {
            if(response.SuccessCode){
                if(response.Data && response.Data.interactionsByCompany && response.Data.interactionsByCompany.length > 0 && response.Data.interactionsByCompany[0] && response.Data.interactionsByCompany[0].typeCounts && response.Data.interactionsByCompany[0].typeCounts.length > 0){
                    response.Data.interactionsByCompany[0].typeCounts.forEach(function(item){
                        var obj = {
                            companyName:item._id,
                            count:item.count,
                            percentage:'width:'+$scope.calculatePercentage(item.count,response.Data.interactionsByCompany[0].maxCount,100)+'%'
                        };
                        $scope.interactionsByCompany.push(obj)
                    });
                }
                if(response.Data && response.Data.interactionsByLocation && response.Data.interactionsByLocation.length > 0 && response.Data.interactionsByLocation[0] && response.Data.interactionsByLocation[0].typeCounts && response.Data.interactionsByLocation[0].typeCounts.length > 0){
                    $scope.interactionsByLocation = {
                        "name": "",
                        "children": []
                    };
                    response.Data.interactionsByLocation[0].typeCounts.forEach(function(item){
                        var obj = {
                            name:item._id,
                            count:item.count,
                            mcap:$scope.calculatePercentage(item.count,response.Data.interactionsByLocation[0].totalCount,100)
                        };
                        $scope.interactionsByLocation.children.push(obj);
                    });

                    fillLocations($scope.interactionsByLocation)
                }
                if(response.Data && response.Data.interactionsByType && response.Data.interactionsByType.length > 0 && response.Data.interactionsByType[0] && response.Data.interactionsByType[0].typeCounts && response.Data.interactionsByType[0].typeCounts.length > 0){
                    $scope.totalInteractionsCount = response.Data.interactionsByType[0].totalCount || 0;
                    var existingTypes = [];
                    var allTypes = ['meeting','call','sms','email','facebook','twitter','linkedin'];
                    for(var t=0; t<response.Data.interactionsByType[0].typeCounts.length; t++){
                        var interactionTypeObj = $scope.getInteractionTypeObj(response.Data.interactionsByType[0].typeCounts[t],response.Data.interactionsByType[0].maxCount,100);
                        if(interactionTypeObj != null){
                            existingTypes.push(interactionTypeObj.type);
                            $scope.interactionsByType.push(interactionTypeObj);
                        }
                    }
                    var mobileCountStatus = 0;
                    var mobileCountStatusLists = ['call','sms','email'];
                    for(var u=0; u<allTypes.length; u++){
                        if(existingTypes.indexOf(allTypes[u]) == -1){
                            var newObj = $scope.getInteractionTypeObj({_id:allTypes[u],count:0},response.Data.interactionsByType[0].maxCount,100)
                            if(newObj != null){
                                if(mobileCountStatusLists.indexOf(allTypes[u]) != -1){
                                    mobileCountStatus ++;
                                }
                                $scope.interactionsByType.push(newObj);
                            }
                        }
                    }

                    if(mobileCountStatus > 0){
                        $scope.showDownloadMobileApp = true;
                    }else $scope.showDownloadMobileApp = false;
                    $scope.showSocialSetup = response.Data.showSocialSetup;

                    $scope.interactionsByType.sort(function (o1, o2) {
                        return o1.priority < o2.priority ? -1 : o1.priority > o2.priority ? 1 : 0;
                    });

                    $scope.interactionsByType = {
                        a:$scope.interactionsByType[0],
                        b:$scope.interactionsByType[1],
                        c:$scope.interactionsByType[2],
                        d:$scope.interactionsByType[3],
                        e:$scope.interactionsByType[4],
                        f:$scope.interactionsByType[5],
                        g:$scope.interactionsByType[6]
                    };
                }
            }
            else{

            }
        }).error(function (data) {

        });

    $scope.getInteractionTypeObj = function(obj,total,percentageFor){
        switch (obj._id){
            case 'google-meeting':
            case 'meeting':return {
                priority:0,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'call':return {
                priority:1,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'sms':return {
                priority:2,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'email':return {
                priority:3,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'facebook':return {
                priority:4,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'twitter':return {
                priority:5,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'linkedin':return {
                priority:6,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            default : return null;
        }
    };

    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };
    $scope.calculatePercentage = function(count,total,percentageFor){
        return Math.round((count*percentageFor)/total);
    };
});
