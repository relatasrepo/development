
function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var contextEmailId = getParams(window.location.href).context;
var contactContextEmailId = getParams(window.location.href).contact;
var fromAccounts = getParams(window.location.href).acc;
var contextMobileNumber = getParams(window.location.href).mobileNumber;

var showContext = true;
if(!checkRequired(contextEmailId)){
    showContext = false;
}

relatasApp.controller("left_contacts_bar", function ($scope, $http,$rootScope,share,sharedServiceSelection, $location) {

    $scope.contactsList = [];
    $scope.currentLength = 0;
    $scope.lastFetched = 0;
    // $scope.filterBy = 'favorite';
    $scope.filterBy = 'recentlyInteracted';
    // $scope.filterBy = 'all';
    $scope.selectedFilter = {
        selection:'favorite'
    };

    $scope.toggleContactPopup = function () {
        resetContactCreateForm();
        mixpanelTracker("Contacts>Add new contact ");
        $scope.addContactModal = !$scope.addContactModal;
    }

    $scope.saveContact = function () {
        var data = {
            personName:$("#fullName").val(),
            personEmailId:$("#p_emailId").val(),
            companyName:$("#companyName").val(),
            designation:$("#designation").val(),
            mobileNumber:$("#p_mobileNumber").val(),
            location:$("#p_location").val()
        }

        var errExists = false;

        if(!$("#fullName").val()){
            errExists = true
            toastr.error("Please enter a name for the contact.")
        } else if(!validateEmail($("#p_emailId").val())){
            toastr.error("Please enter a valid email ID.")
            errExists = true
        }

        if(!checkRequired(data.location)){
            errExists = true
            toastr.error("Please enter a city for this contact.")
        }

        if(!errExists){
            $http.post("/contacts/create",data)
                .success(function (response) {
                    toastr.success("Contact added successfully.")
                    resetContactCreateForm();
                    $scope.addContactModal = false;
                })
        }
    }

    // $scope.filterSelection = "Important";
    $scope.filterSelection = "Recent";
    $scope.cachePrevFilterSelction = '';

    $scope.searchContent = null
    $scope.setFilter = function (filter) {

        mixpanelTracker("Contacts>Filter "+filter);
        var resetSearchContent = $('#searchContent').val();
        $scope.searchContent = resetSearchContent;

        $scope.filterBy = filter;
        $scope.currentLength = 0;
        $scope.grandTotal = 0;
        share.isDnt = false;

        var url = '/contacts/filter/custom/web';

        if(filter == 'all'){
            $scope.filterSelection = "All";
        } else if(filter == 'decision_maker'){
            $scope.filterSelection = "Decision Maker";
        } else if(filter == 'recentlyInteracted'){
            $scope.filterSelection = "Recent";
        } else if(filter == 'prospect'){
            $scope.filterSelection = "Prospects";
        } else if(filter == 'lead'){
            $scope.filterSelection = "Leads";
        } else if(filter == 'influencer'){
            $scope.filterSelection = "Influencers";
        } else if(filter == 'customer'){
            $scope.filterSelection = "Customers";
        } else if(filter == 'partner') {
            $scope.filterSelection = "Partner";
        } else if(filter == "dnt") {
            share.isDnt = true;
            $scope.filterSelection = "Do Not Track";
        } else {
            $scope.filterSelection = "Important";
        }

        $scope.getContacts(url,0,25,$scope.filterBy,null);
    }

    $scope.show_contacts_back = false;
    var isFilter = false;

    $scope.searchContacts_filter = function(searchContent,checkedBoxes){
        var checked = [];
        for(var type in checkedBoxes){
            if(checkedBoxes[type]){
                $scope.selectedFilter["selection"] = checkedBoxes[type];
                checked.push(type);
            }
        }
        if(checked.length > 0){
            $scope.filterBy = checked.join(',')
            isFilter = true;
            $scope.contactsList = [];
            $scope.currentLength = 0;
            $scope.getContacts('/contacts/filter/custom/web',0,25,$scope.filterBy,searchContent);
        }
        else{
            isFilter = false;
            $scope.refreshContactsList()
        }
    };

    $scope.searchContacts = function(searchContent,filter){

        var str = searchContent;
        if(fromAccounts && checkRequired(contactContextEmailId)){
            str = searchContent;
        } else if (searchContent.indexOf("@") >= 0) {
            str = searchContent;
        } else  {
            str = searchContent.replace(/[^\w\s]/gi, '')
        }

        if(checkRequired(str)){

            // $scope.filterBy = !filter ? $scope.filterBy : 'search';
            $scope.filterBy = $scope.filterBy=='dnt'?'dnt':'search'; // Search for all contacts even though the dropdown selection might be different
            $scope.contactsList = [];
            $scope.currentLength = 0;
            $scope.getContacts('/contacts/filter/custom/web',0,25,$scope.filterBy,str);
        }
    };

    $scope.$on('handleLeftBarBroadcast', function() {
        $scope.refreshContactsList();
    });

    $scope.refreshContactsList = function(){
        $scope.filterBy = isFilter ? $scope.filterBy : 'recentlyInteracted';
        $scope.searchContent = "";
        $(".searchContentClass").val();
        $scope.contactsList = [];
        $scope.currentLength = 0;
        $scope.getContacts('/contacts/filter/custom/web',0,25,$scope.filterBy,'');
    };

    $scope.getContacts = function(url,skip,limit,filterBy,searchContent,isBack){
        if(checkRequired(searchContent)){
            url = url+'?skip='+skip+'&limit='+limit+'&filterBy='+filterBy+'&searchContent='+searchContent
        }
        else url = url+'?skip='+skip+'&limit='+limit+'&filterBy='+filterBy;

        parseContactsFetched(url,isBack)
    };

    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    if(fromAccounts && checkRequired(contactContextEmailId)) {
        $scope.searchContacts(contactContextEmailId, 'search')
    } else {
        $scope.getContacts('/contacts/filter/custom/web',0,25,$scope.filterBy);
    }

    $scope.loadMoreContacts = function(skip,limit,searchContent,isBack){

        searchContent = $('#searchContent').val();

        if(isBack){
            $scope.currentLength = $scope.currentLength-25;
            skip = skip-25
        }

        if(skip < 25){
            skip = 0
            $scope.currentLength = 0;
        }

        searchContent = searchContent.replace(/[^\w\s]/gi, '');

        $scope.getContacts('/contacts/filter/custom/web',skip,limit,$scope.filterBy,searchContent,isBack);
    };

    $scope.calculateFromTo = function(current){
        return current < 0 ? 1 : current
    };

    $scope.showInteractions = function(emailId,companyName,designation,name,url,userId,disableContext,mobileNumber){

        $("html, body").animate({ scrollTop: 15 }, "slow");

        sharedServiceSelection.contactBroadcast();
        $rootScope.noEditMobileNumber = true;
        if(disableContext){
            showContext = false;
        }

        $rootScope.isDnt = share.isDnt;
        $rootScope.dntContactDetails = ""

        if(!share.isDnt){
            isGetContactInfo(function(){
                share.getContactInfo(emailId,companyName,designation,name,url,userId,mobileNumber);
            });
        } else {
            $rootScope.dntContactDetails = emailId?emailId:mobileNumber;
        }
    };

    function isGetContactInfo(callback){
        if(typeof share.getContactInfo == 'function'){
            callback();
        }
        else{
            setTimeout(function(){
                return isGetContactInfo(callback);
            },100)
        }
    }

    if(showContext){
        if(contextMobileNumber && contextMobileNumber !='undefined'){
            $scope.showInteractions(contextEmailId,null,null,null,null,null,null,contextMobileNumber);
        } else {
            $scope.showInteractions(contextEmailId);
        }
    }

    $scope.filterSelectedleft = 'filter-selected';

    share.updateLeftList = function (p_emailId,p_mobileNumber) {
        $scope.contactsList = $scope.contactsList.filter(function (co) {
            if(co.emailId !== p_emailId && co.mobileNumber !== p_mobileNumber){
                return co;
            }
        });

        if($scope.grandTotal<=25){
            $scope.currentLength = $scope.currentLength-1;
        }

        $scope.grandTotal = $scope.grandTotal-1;

        if($scope.contactsList && $scope.contactsList.length>0){
    
            var obj = {};
            obj.emailId = $scope.contactsList[0].emailId;
            obj.fullCompanyName =  $scope.contactsList[0].fullCompanyName;
            obj.fullDesignation = $scope.contactsList[0].fullDesignation;
            obj.fullName = $scope.contactsList[0].fullName;
            obj.url = $scope.contactsList[0].url;
            obj.userId = $scope.contactsList[0].userId;
            obj.mobileNumber = $scope.contactsList[0].mobileNumber;

            share.getContactInfo(obj.emailId,obj.fullCompanyName,obj.fullDesignation,obj.fullName,obj.url,obj.userId,obj.mobileNumber);
        }
    }

    function parseContactsFetched(url,isBack) {
        $http.get(url)
            .success(function(response){

                $scope.contactsList = [];
                $scope.isConnectionOpen = false;
                if(response.SuccessCode){
                    $scope.grandTotal = response.Data.total;
                    if(response.Data.contacts.length > 0){
                        $rootScope.noContactsFound = false;
                        if((!isBack && isBack != undefined) || $scope.currentLength <= 0){
                            $scope.currentLength += response.Data.returned;
                        }

                        if($scope.currentLength > 25){
                            $scope.show_contacts_back = true;
                        }else $scope.show_contacts_back = false;

                        for(var i=0; i<response.Data.contacts.length; i++){
                            var li = '';
                            var obj;

                            if(checkRequired(response.Data.contacts[i].personId)){
                                var name = getTextLength(response.Data.contacts[i].personName,20);
                                var image = '/getImage/'+response.Data.contacts[i].personId._id;
                                var url = response.Data.contacts[i].personId.publicProfileUrl;
                                var companyName = checkRequired(response.Data.contacts[i].companyName) ? response.Data.contacts[i].companyName : "";
                                var designation = checkRequired(response.Data.contacts[i].designation) ? response.Data.contacts[i].designation : "";

                                if(!checkRequired(url)){
                                    url = "";
                                }
                                else if(url.charAt(0) != 'h'){
                                    url = '/'+url;
                                }

                                obj = {
                                    userId:response.Data.contacts[i].personId._id,
                                    fullName:response.Data.contacts[i].personName,
                                    companyName:getTextLength(companyName,25),
                                    designation:getTextLength(designation,25),
                                    fullCompanyName:companyName,
                                    fullDesignation:designation,
                                    emailId:response.Data.contacts[i].personEmailId,
                                    mobileNumber:response.Data.contacts[i].mobileNumber,
                                    name:name,
                                    image:image,
                                    url:url,
                                    cursor:'cursor:pointer',
                                    idName:'com_con_item_'+response.Data.contacts[i]._id,
                                    interactionType:response.Data.contacts[i].interactionType?getInteractionTypeImage(response.Data.contacts[i].interactionType):'',
                                    lastInteractedDate:response.Data.contacts[i].lastInteractedDate?moment(response.Data.contacts[i].lastInteractedDate).format("DD MMM YYYY"):''
                                };

                                if(checkRequired(response.Data.contacts[i].hashtag)) {
                                    if (response.Data.contacts[i].hashtag.length > 0) {
                                        obj.hashtag = "#";
                                    }
                                }

                                $scope.contactsList.push(obj)
                                if(i == 0 && !showContext){
                                    share.getContactInfo(obj.emailId,obj.fullCompanyName,obj.fullDesignation,obj.fullName,obj.url,obj.userId,obj.mobileNumber);
                                }
                            }
                            else{
                                var companyName2 = checkRequired(response.Data.contacts[i].companyName) ? response.Data.contacts[i].companyName : ""
                                var designation2 = checkRequired(response.Data.contacts[i].designation) ? response.Data.contacts[i].designation : ""

                                if(response.Data.contacts[i].personName != '  null'){

                                    var str = response.Data.contacts[i].personName.replace("(null)", "");
                                    var contactImageLink = response.Data.contacts[i].contactImageLink?encodeURIComponent(response.Data.contacts[i].contactImageLink):null

                                    obj = {
                                        userId:null,
                                        fullName:str,
                                        companyName:getTextLength(companyName2,25),
                                        designation:getTextLength(designation2,25),
                                        fullCompanyName:companyName2,
                                        fullDesignation:designation2,
                                        name:getTextLength(str,20),
                                        emailId:response.Data.contacts[i].personEmailId,
                                        mobileNumber:response.Data.contacts[i].mobileNumber,
                                        image:'/getContactImage/'+response.Data.contacts[i].personEmailId+'/'+contactImageLink,
                                        // noPicFlag:true,
                                        // noPPic:(response.Data.contacts[i].personName.substr(0,2)).capitalizeFirstLetter(),
                                        cursor:'cursor:default',
                                        url:null,
                                        idName:'com_con_item_'+response.Data.contacts[i]._id,
                                        interactionType:response.Data.contacts[i].interactionType?getInteractionTypeImage(response.Data.contacts[i].interactionType):'',
                                        lastInteractedDate:response.Data.contacts[i].lastInteractedDate?moment(response.Data.contacts[i].lastInteractedDate).format("DD MMM YYYY"):''
                                    };

                                    if(checkRequired(response.Data.contacts[i].hashtag)) {
                                        if (response.Data.contacts[i].hashtag.length > 0) {
                                            obj.hashtag = "#";
                                        }
                                    }
                                    
                                    $scope.contactsList.push(obj);
                                    if(i == 0 && !showContext){
                                        share.getContactInfo(obj.emailId,obj.fullCompanyName,obj.fullDesignation,obj.fullName,obj.url,obj.userId,obj.mobileNumber);
                                    } else {
                                    }
                                }
                            }
                        }
                    }
                    else {

                        $rootScope.noContactsFound = true;
                        $scope.isAllConnectionsLoaded = true;
                        //$scope.commonConnectionsNotExist = response.Message;
                    }
                }
                else{
                    $scope.isAllConnectionsLoaded = true;
                    //$scope.commonConnectionsNotExist = response.Message;
                }
            })
    }
});

function getInteractionTypeImage (type){
    switch(type){
        case 'google-meeting':
        case 'meeting':
            return "fa-calendar-check-o green-color";
            break;
        case 'document-share':
            return "fa-file-pdf-o";
            break;
        case 'message':
        case 'meeting-comment':
        case 'email':
            return "fa-envelope green-color";
            break;
        case 'sms':
            return "fa-reorder green-color";
            break;
        case 'call':
            return "fa-phone green-color";
            break;
        case 'task':
            return "fa-check-square-o";
            break;
        case 'twitter':
            return "fa-twitter-square twitter-color";
            break;
        case 'facebook':
            return "fa-facebook-official fb-color";
            break;
        case 'linkedin':
            return "fa-linkedin-square linkedin-color";
            break;
        case 'calendar-password':
            return "fa-calendar-check-o green-color";
            break;
        default :return '';
    }
};

function resetContactCreateForm() {
    $("#fullName").val("");
    $("#p_emailId").val("");
    $("#companyName").val("");
    $("#designation").val("");
    $("#p_mobileNumber").val("");
    $("#p_location").val("")
}
