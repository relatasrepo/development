
function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var inputSearch = getParams(window.location.href);
var timezone ;

reletasApp.controller("logedinUser", function ($scope, $http,share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {
            if(response.SuccessCode){
                share.l_user = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.l_usr = response.Data;
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
                share.lUseEmailId = response.Data.emailId
            }
            else{

            }
        }).error(function (data) {

    })
});

reletasApp.controller("header_controller", function($scope,$http,share){
    $scope.yourNetworkIn = true;
    $scope.extendedNetworkIn = true;
    $scope.forCompaniesIn = false;
    $scope.getMiddleBarTemplate = function(){
        return ""
    };

    $scope.isSearchFromOtherPage = function(){
      return $("#pageType").attr("page") != "contactConnect";
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies,forInsights,forDashboard,forDashboardTopCompanies){

        var firstCharSearch = searchContent.substring(0,1);
        var str = searchContent.replace(/[^a-zA-Z ]/g, "");

        if(typeof str == 'string' && str.length > 0){
            if(searchContent.length >= 3){
                if($scope.isSearchFromOtherPage() && firstCharSearch !== '#'){
                    window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies+'&forInsights='+forInsights+'&forDashboard='+forDashboard+'&forDashboardTopCompanies='+forDashboardTopCompanies;
                }
                else{
                    share.searchUsers(searchContent,yourNetwork,extendedNetwork,forCompanies,forInsights,forDashboard,forDashboardTopCompanies)
                }
            }
        }
        else toastr.error("Please enter search content")
    };

    if(inputSearch && inputSearch.searchContent && inputSearch.yourNetwork && inputSearch.extendedNetwork && inputSearch.forCompanies){
        $scope.searchContent = inputSearch.searchContent;
        $scope.searchContentIn = decodeURIComponent(inputSearch.searchContent);
        $scope.yourNetwork = JSON.parse(inputSearch.yourNetwork)
        $scope.extendedNetwork = JSON.parse(inputSearch.extendedNetwork)
        $scope.forCompanies = JSON.parse(inputSearch.forCompanies);
        if(inputSearch.forInsights)
            $scope.forInsights = JSON.parse(inputSearch.forInsights);
        else
            $scope.forInsights = null;
        if(inputSearch.forDashboard)
            $scope.forDashboard = JSON.parse(inputSearch.forDashboard);
        else
            $scope.forDashboard = null;
        if(inputSearch.forDashboardTopCompanies)
            $scope.forDashboardTopCompanies = JSON.parse(inputSearch.forDashboardTopCompanies);
        else
            $scope.forDashboardTopCompanies = null;
        $scope.yourNetworkIn = $scope.yourNetwork
        $scope.extendedNetworkIn = $scope.extendedNetwork
        $scope.forCompaniesIn = $scope.forCompanies
        $scope.searchFromHeader($scope.searchContent,$scope.yourNetwork,$scope.extendedNetwork,$scope.forCompanies,$scope.forInsights,$scope.forDashboard,$scope.forDashboardTopCompanies);
    }

    //XLS contacts upload
    //share.uploadContacts = function(){
    //    console.log("clicked")
    //    $("#selectedFile").trigger("click");
    //};
    //
    //$scope.upload = function(file){
    //    var formData = new FormData();
    //    formData.append('contacts', file, file.name);
    //    var xhr = new XMLHttpRequest();
    //    xhr.open('POST', '/contacts/upload/file', true);
    //    // Set up a handler for when the request finishes.
    //    xhr.onload = function () {
    //        var response = JSON.parse(xhr.response)
    //        if(response.SuccessCode){
    //            $("#uploadContactsAlert").addClass("hide");
    //            toastr.success(response.Message)
    //        }
    //        else{
    //            toastr.error(response.Message)
    //        }
    //    };
    //    // Send the Data.
    //    xhr.send(formData);
    //};
    //$("#selectedFile").on("change",function(){
    //    if($(this)[0] && $(this)[0].files && $(this)[0].files.length > 0){
    //        $scope.upload($(this)[0].files[0])
    //    }
    //    else toastr.error("Please select file to upload")
    //});

});

reletasApp.controller("search_results",function($scope, $http,share){
    //$scope.contactsList = [];
    $scope.filterByList = [];
    share.searchUsers = function(searchContent,yourNetwork,extendedNetwork,forCompanies,forInsights,forDashboard,forDashboardTopCompanies){
        //$scope.filterBy = ['yourNetwork','extendedNetwork'];
        $scope.filterByList = []
    if(yourNetwork){
        $scope.filterByList.push('yourNetwork');
    }
    if(extendedNetwork){
        $scope.filterByList.push('extendedNetwork');
    }
    if(forCompanies){
        $scope.filterByList.push('companies');
    }
    if(forInsights){
        $scope.filterByList.push('insights');
    }
    if(forDashboard){
        $scope.filterByList.push('dashboard');
    }
    if(forDashboardTopCompanies){
        $scope.filterByList.push('dashboardTopCompanies');
    }
        if(!$scope.filterByList.length > 0){
            toastr.error('Please select Search criteria');
        }
        else{
            $scope.filterBy = $scope.filterByList.join(',');

            var str = decodeURIComponent(searchContent);

            var firstCharSearch = str.substring(0,1);

            str = str.replace(/[^\w\s]/gi, '');
            if(firstCharSearch == '#' || firstCharSearch == '%23'){
                var url = '/search/hashtag/header?filterBy='+$scope.filterBy+'&searchContent='+str;

            }else {
                var url = '/search/contact/header?filterBy='+$scope.filterBy+'&searchContent='+str;
            }

            if(forDashboard){ //to search extended network for dashboard 3a
                var url = '/search/contact/header?filterBy='+$scope.filterBy+'&searchContent='+searchContent;
            }

            $http.get(url)
                .success(function(response){

                    $scope.contactsList = [];
                    $scope.isConnectionOpen = false;
                    $scope.totalResults = response.Data.total || 0;
                    if($scope.totalResults >= 200){
                        $scope.left = "13%"
                        $scope.totalResultsMessage = 'Displaying the top '+$scope.totalResults+' results. Please enter more details to get better search results';
                    }
                    else
                    {
                        $scope.left = "30%"
                        $scope.totalResultsMessage = 'Found '+$scope.totalResults+' search results';
                    }

                    if($scope.totalResults > 0){
                        $scope.show_selected_right = true;
                    }
                    else{
                        $scope.left = "30%"
                        $scope.totalResultsMessage = 'Found '+$scope.totalResults+' search results';
                        $scope.selectSearchItem(null)
                        share.hideConnectionSection(true)
                        //share.hideCommonSection(true)
                        //share.hideCompanySection(true)
                        share.hideRecommended(true)
                        $scope.show_selected_right = false;
                    }
                    if(response.SuccessCode){

                        if(response.Data.contacts.length > 0){

                            for(var i=0; i<response.Data.contacts.length; i++){
                                var li = '';
                                var obj = {};
                                if(response.Data.contacts[i].personId.length > 0){
                                    response.Data.contacts[i].personId.forEach(function(data){
                                        if(checkRequired(data)){
                                            obj.image = '/getImage/'+response.Data.contacts[i].personId;
                                        }
                                    })
                                }

                                //Remove extra personId if occurs. This was creating a bug for some profile images where the personid was attached twice
                                if(obj.image) {
                                    obj.image = obj.image.split(',')[0];
                                }

                                if( Object.prototype.toString.call( response.Data.contacts[i].mobileNumber ) === '[object Array]' ) {
                                    obj.mobileNumber = response.Data.contacts[i].mobileNumber[0] ? response.Data.contacts[i].mobileNumber[0].replace(/[^a-zA-Z0-9]/g,''):null;
                                } else {
                                    obj.mobileNumber = response.Data.contacts[i].mobileNumber ? response.Data.contacts[i].mobileNumber.replace(/[^a-zA-Z0-9]/g,''):null;
                                }

                                var contactImageLink = response.Data.contacts[i].contactImageLink?encodeURIComponent(response.Data.contacts[i].contactImageLink):null

                                if(!checkRequired(obj.image) && !contactImageLink) {
                                    //obj.image = '/images/default.png';
                                    obj.noPicFlag = true;
                                } else {
                                    obj.image = '/getContactImage/'+response.Data.contacts[i].personEmailId+'/'+contactImageLink
                                }
                                if(checkRequired(response.Data.contacts[i].personName)) {
                                    obj.noPPic = (response.Data.contacts[i].personName.substr(0,2)).toUpperCase();
                                }

                                obj.name = '';
                                if(checkRequired(response.Data.contacts[i].personName)){
                                    //Replace string 'null' from name
                                    var str = response.Data.contacts[i].personName.replace("(null)", "");

                                    obj.name = getTextLength(str,25)
                                    obj.fullName = str;
                                }

                                obj.companyName = '';
                                obj.fullCompanyName = '';
                                if(response.Data.contacts[i].companyName.length > 0){
                                    response.Data.contacts[i].companyName.forEach(function(data){
                                        if(checkRequired(data)){
                                            obj.companyName = getTextLength(data,25);
                                            obj.fullCompanyName = data
                                        }
                                    })
                                }

                                obj.designation = '';
                                obj.fullDesignation = '';
                                if(response.Data.contacts[i].designation.length > 0){
                                    response.Data.contacts[i].designation.forEach(function(data){
                                        if(checkRequired(data)){
                                            obj.designation = getTextLength(data,25);
                                            obj.fullDesignation = data
                                        }
                                    })
                                }

                                obj.location = '';
                                obj.locationFull = '';
                                if(response.Data.contacts[i].location.length > 0){
                                    response.Data.contacts[i].location.forEach(function(data){
                                        if(checkRequired(data)){
                                            obj.location = getTextLength(data,25);
                                            obj.locationFull = data
                                        }
                                    })
                                }

                                obj.url = '';
                                if(response.Data.contacts[i].publicProfileUrl.length > 0){
                                    response.Data.contacts[i].publicProfileUrl.forEach(function(data){
                                        if(checkRequired(data)){
                                            obj.url = data;
                                            if(obj.url.charAt(0) != 'h'){
                                                url = '/'+url;
                                            }
                                        }
                                    })
                                }

                                var comparePersonId = checkExistsinArray(response.Data.contacts[i].personId, response.userId);

                                if(response.Data.contacts[i].hashtag.length>0){
                                    response.Data.contacts[i].hashtag.forEach(function(data){
                                        if(checkRequired(data)){
                                            obj.hashtag = data;
                                            obj.hashSymbol = '#';
                                        };
                                    });
                                } else if(comparePersonId){
                                    response.Data.contacts[i].hashtag.forEach(function(data){
                                        if(checkRequired(data)){
                                            obj.hashtag = data;
                                            obj.hashSymbol = '#';
                                        };
                                    });
                                }

                                if(checkRequired(response.Data.contacts[i].publicProfileUrlStr)){
                                    obj.publicProfileUrlStr = "/" + response.Data.contacts[i].publicProfileUrlStr;
                                } else {
                                    obj.publicProfileUrlStr = null;
                                }

                                obj.p_id = response.Data.contacts[i]._id;
                                obj.title = getTextLength($scope.getPosition(obj.fullCompanyName,obj.fullDesignation),30);
                                obj.fullTitle = $scope.getPosition(obj.fullCompanyName,obj.fullDesignation);
                                obj.emailId = response.Data.contacts[i].personEmailId;
                                obj.owner = response.Data.contacts[i].owner.indexOf(response.userId) != -1;
                                obj.connected = response.Data.contacts[i].owner;
                                if(obj.owner){

                                  var fetchWith = obj.emailId?obj.emailId:obj.mobileNumber
                                    obj.imagePointerClass = 'cursor-pointer'
                                    if(obj.mobileNumber){
                                        obj.contactUrl = '/contact/selected?context='+fetchWith+'&mobileNumber='+obj.mobileNumber.replace(/[^a-zA-Z0-9]/g,'')
                                    } else {
                                        obj.contactUrl = '/contact/selected?context='+fetchWith
                                    }
                                }
                                else{
                                    obj.imagePointerClass = '';
                                    obj.contactUrl = null;
                                }

                                var isOwner = response.Data.contacts[i].owner.indexOf(response.userId) != -1;

                                if(isOwner){
                                    obj.myConnectionCSS = 'no-image-placeholder-highlight';
                                    obj.myConnectionCSSList = 'image-placeholder-small-text-highlight';
                                    obj.ShowHashtags = true;

                                } else {
                                    obj.myConnectionCSS = 'no-image-placeholder';
                                    obj.myConnectionCSSList = 'image-placeholder-small-text';
                                }

                                obj.searchContent = searchContent
                                obj.yourNetwork = yourNetwork
                                obj.extendedNetwork = extendedNetwork
                                obj.forCompanies = forCompanies

                                $scope.contactsList.push(obj);

                                if(inputSearch && checkRequired(inputSearch.selectedEmailId)){
                                        if(inputSearch.selectedEmailId == obj.emailId){
                                            $scope.selectSearchItem(obj)
                                        }
                                }
                                else if(i == 0){
                                    $scope.selectSearchItem(obj)
                                }

                            }
                        }
                        else {
                            $scope.isAllConnectionsLoaded = true;
                            //$scope.commonConnectionsNotExist = response.Message;
                        }
                    }
                    else{
                        $scope.isAllConnectionsLoaded = true;
                        $scope.left = "30%"
                        $scope.totalResultsMessage = 'Found '+$scope.totalResults+' search results';
                    }
                })
        }
    };

    $scope.selectSearchItem = function(contact){
        if(contact != null){
            $scope.selectedContact = contact;
            $scope.selectedContact.name = getTextLength(contact.fullName,30)
            share.bestPaths(contact);
            share.commonConnectionsNumber(contact);
            //share.getBestPathCompany(contact.emailId,contact);
            //share.selectedContactName1(getTextLength(contact.fullName,10))
            //share.selectedContactName2(getTextLength(contact.fullName,10))
            //share.selectedContactName3(getTextLength(contact.fullName,10))
            share.selectedContactName_connection(getTextLength(contact.fullName,10))
        }
    };

    $scope.getPosition = function(companyName,designation){
        var position = '';

        if(checkRequired(companyName) && checkRequired(designation)){
            position = designation+', '+companyName
        }
        else if(checkRequired(companyName) && !checkRequired(designation)){
            position = companyName
        }
        else if(!checkRequired(companyName) && checkRequired(designation)){
            position = designation
        }

        return position;
    };

    $scope.goToContact = function(url){
        if(checkRequired(url)){
            window.location = url;
        }
    }

    $scope.addHashtag = function (p_id,hashtag) {

        var str = hashtag.replace(/[^\w\s]/gi, '');

        var obj = { contactId:p_id, hashtag: str};
        $http.post("/api/hashtag/new",obj)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.hashtag = '';
                    toastr.success("Hashtag - "+hashtag.italics()+" Added!");
                }
                else {
                    toastr.error("Error! Hashtag not Added.");
                }
            });
        //share.getContactInfo(p_emailId);
    };

    function checkExistsinArray(arr,obj) {
        return (arr.indexOf(obj) != -1);
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

});

reletasApp.controller("best_common_count",function($scope,$http,share){

    share.commonConnectionsNumber = function(contact){

        $http.post('/search/connect/best/path',{contactEmailId:contact.emailId,connected:contact.connected})
            .success(function(response){
                if(response.SuccessCode){
                    if(response.Data.length>0){
                        $scope.commonConnectionsCount = "Common Connections : "+response.Data.length;
                    } else {
                        if(!contact.owner){
                            $scope.commonConnectionsCount = "No Common Connections Found";
                        } else {
                            $scope.commonConnectionsCount = '';
                        }
                    }
                }  else {
                    if(!contact.owner){
                        $scope.commonConnectionsCount = "No Common Connections Found";
                    } else {
                        $scope.commonConnectionsCount = '';
                    }
                }
            });
    }

});

reletasApp.controller("best_connection_controller",function($scope, $http,share){
    $scope.contacts_section = false;
    share.selectedContactName_connection = function(name){
        $scope.contactName = name;
    };
    share.hideConnectionSection = function(flag){
        $scope.contacts_section = !flag;
    };

    share.bestPaths = function(contact){
        
        var fetchWith = contact.emailId;
        if(!contact.emailId) {
            fetchWith = contact.mobileNumber
        }

        $http.post('/search/connect/best/path',{contactEmailId:fetchWith,connected:contact.connected})
            .success(function(response){
                if(response.SuccessCode){
                    $scope.lUser = {
                        name:share.l_user.firstName+' '+share.l_user.lastName,
                        image:'/getImage/'+share.l_user._id
                    };
                    share.hideConnectionSection(false);
                    share.hideRecommended(true);
                    contact.name = getTextLength(contact.name,18)
                    contact.designation = getTextLength(contact.designation,18)
                    $scope.selectedUser = contact;
                    $scope.bestPaths = [];

                    for(var i=0; i<response.Data.length; i++){
                        var iDate = moment(response.Data[i].lastInteracted).tz(response.timezone);
                        var now = moment().tz(response.timezone);
                        var diff = now.diff(iDate);
                        diff = moment.duration(diff).asMinutes();
                        var initiateIntroMessageContent = "Hi, "+response.Data[i]._id.firstName;
                        initiateIntroMessageContent += "\nHope you are doing well. I'm looking to connect with "+contact.fullName+" to build a business relationship.";
                        initiateIntroMessageContent += "\nCan you help make the introductions?\n\nThanks, "+share.l_user.firstName;
                        initiateIntroMessageContent += "\nwww.relatas.com/"+share.l_user.publicProfileUrl;

                        var forwordIntroMessageContent = "Hi, "+contact.fullName;
                        forwordIntroMessageContent += "\nI wanted to forward the request I received through Relatas to make an introduction.";
                        forwordIntroMessageContent += "\nHope you have a great interaction.\n\nRegards, "+response.Data[i]._id.firstName;

                        var obj = {
                                duration:moment.duration(diff,"minutes").humanize()+' ago',
                                name:response.Data[i]._id.firstName+' '+response.Data[i]._id.lastName,
                                receiverId:response.Data[i]._id._id,
                                image:'/getImage/'+response.Data[i]._id._id,
                                className:i==0?'connect-row1':'',
                                initiateIntroMessageContent:initiateIntroMessageContent,
                                forwordIntroMessageContent:forwordIntroMessageContent,
                                pathUserEmailId:response.Data[i]._id.emailId,
                                luserEmailId:share.l_user.emailId,
                                subject:"Relatas Introduction "+share.l_user.firstName+" - "+response.Data[i]._id.firstName+" - "+contact.fullName
                        };
                        $scope.bestPaths.push(obj);
                    }
                }
                else{
                    $scope.contacts_company_section = false;
                    if(response.Error == "NO_COMMON_CONNECTIONS_FOUND"){
                        // no paths
                        share.hideConnectionSection(true)
                        share.hideRecommended(false);
                        share.appendNoRecommended(contact)
                    }
                    else if(response.Error == "USER_ALREADY_CONNECTED"){
                        // already connected
                        share.hideConnectionSection(true)
                        share.hideRecommended(true)
                    }
                }
            })
    };

    $scope.initiateIntroduction = function(path){
        if(path && checkRequired(path.initiateIntroMessageContent)){
            var obj = {
                receiverEmailId:path.pathUserEmailId,
                receiverName:path.name,
                message:path.initiateIntroMessageContent,
                subject:path.subject,
                receiverId:path.receiverId,
                docTrack:false,
                trackViewed:false,
                remind:false
            };
            $(".disable-on-action").addClass("disabled")
            $http.post("/messages/send/email/single/web",obj)
                .success(function(response){
                    $(".disable-on-action").removeClass("disabled")
                    if(response.SuccessCode){
                        $(".initiate-intro").trigger("click");
                        toastr.success(response.Message);
                    }
                    else{
                        toastr.error(response.Message);
                    }
                })
        }
    }

    //XLS Contacts Upload

    $scope.uploadContacts = function(){
        $("#selectedFile").trigger("click");
    };

    $scope.upload = function(file){
        var formData = new FormData();
        formData.append('contacts', file, file.name);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/contacts/upload/file', true);
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            var response = JSON.parse(xhr.response)
            if(response.SuccessCode){
                $("#uploadContactsAlert").addClass("hide");
                toastr.success(response.Message)
            }
            else{
                toastr.error(response.Message)
            }
        };
        // Send the Data.
        xhr.send(formData);
    };
    $("#selectedFile").on("change",function(){
        if($(this)[0] && $(this)[0].files && $(this)[0].files.length > 0){
            $scope.upload($(this)[0].files[0])
        }
        else toastr.error("Please select file to upload")
    });

    $scope.interactionsContext = function(emailId){
        window.location.replace('/contact/selected?context='+emailId)
    };

});

reletasApp.controller("other_common_connections",function($scope, $http,share){
    share.selectedContactName2 = function(name){
        $scope.contactName = name;
    };
    share.hideCommonSection = function(flag){
        $scope.otherCommon = !flag;
    };
    $scope.uploadContacts = share.uploadContacts;
    $scope.otherCommon = false;
    share.getBestPathCommon = function(cEmailId,notIn,contact){
        $http.get('/search/connect/best/path/to/common?contactEmailId='+cEmailId+'&notIn='+notIn)
            .success(function(response){
                if(response.SuccessCode){

                    if(response.Data && response.Data.length > 0){
                        $scope.otherCommon = true;
                        $scope.lUser = {
                            name:share.l_user.firstName+' '+share.l_user.lastName,
                            image:'/getImage/'+share.l_user._id
                        };

                        contact.name = getTextLength(contact.name,18)
                        contact.designation = getTextLength(contact.designation,18)
                        $scope.selectedUser = contact;
                        $scope.pathOne = {};
                        $scope.pathTwo = {};
                        for(var i=0; i<response.Data.length; i++){
                            var iDate = moment(response.Data[i].lastInteracted).tz(response.timezone);
                            var now = moment().tz(response.timezone);
                            var diff = now.diff(iDate);
                            diff = moment.duration(diff).asMinutes();
                            if(response.Data[i].personId && response.Data[i].personId.firstName){
                                var obj = {
                                    duration:moment.duration(diff,"minutes").humanize()+' ago',
                                    name:response.Data[i].personId.firstName+' '+response.Data[i].personId.lastName,
                                    image:'/getImage/'+response.Data[i].personId._id,
                                    className:i==0?'connect-row1':''
                                };
                                if(i == 0 ){
                                    $scope.pathOne = obj
                                }
                                else $scope.pathTwo = obj
                            }
                            else{
                                var obj2 = {
                                    duration:moment.duration(diff,"minutes").humanize()+' ago',
                                    name:response.Data[i].personName,
                                    image:'/images/default.png',
                                    className:i==0?'connect-row1':'',
                                    noPicFlag:true
                                };
                                if(i == 0){
                                    $scope.pathOne = obj2;
                                }
                                else $scope.pathTwo = obj2
                            }
                        }
                        if(response.Data.length == 1){
                            $scope.otherCommon2 = false;
                            share.hideRecommended(true)
                        }
                    }
                }else $scope.otherCommon = false;
            })
    }
});


/*Add a new Hashtag*/
//
//reletasApp.controller("add_hashtags", function($scope, $http){
//
//    $scope.addHashtag = function (p_id,hashtag) {
//
//        var contactId = p_id.toString();
//        var str = hashtag.replace(/[^\w\s]/gi, '');
//        var obj = { contactId:contactId, hashtag: str};
//        $http.post("/api/hashtag/new",obj)
//            .success(function(response){
//                if(response.SuccessCode){
//                    $scope.hashtag = '';
//                    console.log("Pass");
//                    //console.log($scope.getHashtag());
//
//                }
//                else {
//                    console.log("Fail");
//                }
//            });
//    };
//
//});

reletasApp.controller("no_recommended",function($scope, $http,share){
    $scope.no_recommended_hide = false;
    share.selectedContactName3 = function(name){
        $scope.contactName = name
    };
    share.hideRecommended = function(flag){
        $scope.no_recommended_hide = !flag;
    };

    share.appendNoRecommended = function(contact){
        $scope.isCorporateUser = share.l_user.corporateUser

        $scope.lUser = {
            name:share.l_user.firstName+' '+share.l_user.lastName,
            image:'/getImage/'+share.l_user._id,
            publicProfileUrl: share.l_user.publicProfileUrl
        };

        contact.name = getTextLength(contact.name,18)
        contact.designation = getTextLength(contact.designation,18)
        $scope.selectedUser = contact;

        $scope.contactName = getTextLength($scope.selectedUser.fullName,5)

        var reqObj = {
            onSuccess:'/contact/connect?searchContent='+contact.searchContent+'&selectedEmailId='+contact.emailId+'&yourNetwork='+contact.yourNetwork+'&extendedNetwork='+contact.extendedNetwork+'&forCompanies='+contact.forCompanies,
            onFailure:'/contact/connect?searchContent='+contact.searchContent+'&selectedEmailId='+contact.emailId+'&yourNetwork='+contact.yourNetwork+'&extendedNetwork='+contact.extendedNetwork+'&forCompanies='+contact.forCompanies
        };
        $scope.addAccountsUrl = '/authenticate/google/add/web'+jsonToQueryString(reqObj);
        $scope.messageContent = "Hi,\nI started using Relatas [www.relatas.com] and think you will really love Relatas for finding new contacts and also to see who you are losing touch with. I'm using Relatas to build existing and new relationships.\n\n And the best part - it simply works. No need for data entry. \n\n\nRegards,\n"+share.l_user.firstName+"\n www.relatas.com/"+share.l_user.publicProfileUrl
    };
    $scope.addAccounts = function(url){
       window.location = url;
    };
    $scope.uploadContacts = share.uploadContacts;

    function jsonToQueryString(json) {
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }

    function extractEmails (text)
    {
        return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
    }

    $scope.sendInvite = function(emailIds,message){
        var subject = 'Invitation to join Relatas from '+share.l_user.firstName;
        if(!checkRequired(emailIds)){
            toastr.error("Please provide recipients")
        }
        else if(!checkRequired(message)){
            toastr.error("Please provide message")
        }
        else{
            var recipients = extractEmails(emailIds)
            if(checkRequired(recipients) && recipients.length > 0){
                var obj = {
                    receiverEmailId:recipients.join(','),
                    receiverName:"Hey,",
                    message:message,
                    subject:subject,
                    receiverId:null,
                    docTrack:false,
                    trackViewed:false,
                    remind:false
                };

                $http.post("/messages/send/email/single/web",obj)
                    .success(function(response){
                        if(response.SuccessCode){
                            $scope.toEmailId = '';
                            $scope.messageContent = '';
                            toastr.success(response.Message);
                        }
                        else{
                            toastr.error(response.Message);
                        }
                    })
            }
            else toastr.error("Please provide valid email id")
        }
    }
});