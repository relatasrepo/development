
var start = 1;
var itemsScroll = 6;
var itemsToShow = 6;

if(window.innerWidth < 480){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 1
    itemsScroll = 1
}else if(window.innerWidth < 900){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 3
    itemsScroll = 3
}
var end = itemsToShow;

// reletasApp.controller("losing_touch_scroll", function ($scope, $http, share) {
//     $scope.losingTouchConnections = [];
//     $scope.losingTouchConnectionsIds = [];
//     $scope.currentLength = 0;
//     $scope.isFirstTime = true;
//     $scope.isConnectionOpen = false;
//     $scope.start = start;
//     $scope.end = end;
//     $scope.isAllConnectionsLoaded = false;
//
//     // commonConnectionsMessage(true)
//     getCommonConnections($scope, $http, '/contacts/filter/losingtouch/web?skip=0&limit=12', true);
//
//     $scope.NextConn = function(){
//         var statusNext =  $scope.statusNext;
//         var statusPrev =  $scope.statusPrev;
//         var total = $scope.losingTouchConnectionsIds.length;
//
//         if($scope.end < total){
//             var itemsToStayPrev = $scope.start+itemsScroll;
//             var itemsToStayNext = $scope.end+itemsScroll;
//
//             for(var i=0; i<$scope.losingTouchConnectionsIds.length; i++){
//
//                 if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
//                     $scope.losingTouchConnections[i].className = ''
//                     //addHide($scope.losingTouchConnectionsIds[i],false);
//                 }
//                 else{
//                     $scope.losingTouchConnections[i].className = 'hide'
//                     //addHide($scope.losingTouchConnectionsIds[i],true);
//                 }
//             }
//
//             $scope.end = $scope.end+itemsScroll;
//             $scope.start = $scope.start+itemsScroll;
//
//             $scope.statusNext = statusNext+itemsScroll;
//             $scope.statusPrev = statusNext;
//
//             if($scope.statusNext > total){
//                 $scope.statusNext = total;
//             }
//             if($scope.statusNext <= itemsScroll){
//                 $scope.statusNext = itemsScroll;
//             }
//
//             if($scope.statusPrev <= 0){
//                 $scope.statusPrev = 1;
//             }
//         }
//         $scope.showHideArrows();
//         if($scope.fetchCommonConnectionsNext() && $scope.isAllConnectionsLoaded == false && $scope.isConnectionOpen == false){
//             $scope.isConnectionOpen = true;
//             var url = '/contacts/filter/losingtouch/web?skip='+$scope.currentLength+'&limit=12';
//             getCommonConnections($scope, $http, url, false);
//         }
//     };
//
//     $scope.fetchCommonConnectionsNext = function(){
//         if($scope.end >= $scope.losingTouchConnectionsIds.length - 12){
//             return true;
//         }else return false;
//     };
//
//     $scope.PrevConn = function(){
//
//         var statusPrev = $scope.statusPrev;
//         var total = $scope.losingTouchConnectionsIds.length;
//
//         if($scope.start >1){
//             var itemsToStayPrev = $scope.start-itemsScroll;
//             var itemsToStayNext = $scope.end-itemsScroll;
//
//             for(var i=0; i<$scope.losingTouchConnectionsIds.length; i++){
//
//                 if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
//                     $scope.losingTouchConnections[i].className = ''
//                     //addHide($scope.losingTouchConnectionsIds[i],false);
//                 }
//                 else{
//                     $scope.losingTouchConnections[i].className = 'hide'
//                     //addHide($scope.losingTouchConnectionsIds[i],true);
//                 }
//             }
//
//             $scope.end = $scope.end-itemsScroll;
//             $scope.start = $scope.start-itemsScroll;
//
//             $scope.statusNext = statusPrev;
//             $scope.statusPrev = statusPrev-itemsScroll+1;
//
//             if($scope.statusNext > total){
//                 $scope.statusNext = total;
//             }
//             if($scope.statusNext <= itemsScroll){
//                 $scope.statusNext = itemsScroll;
//             }
//
//             if($scope.statusPrev <= 0){
//                 $scope.statusPrev = 1;
//             }
//             $scope.showHideArrows()
//         }
//     };
//
//     $scope.showHideArrows = function(){
//         if($scope.grandTotal >0){
//             if($scope.start <=1 ){
//                 addHide("als-prev_l",true)
//             }
//             else addHide("als-prev_l",false)
//
//             if($scope.end >= $scope.grandTotal){
//                 addHide("als-next_l",true)
//             }
//             else addHide("als-next_l",false)
//         }
//         else{
//             addHide("als-prev_l",true)
//             addHide("als-next_l",true)
//         }
//     };
//
//     $scope.connectionClick = function(url){
//         if(checkRequired(url)){
//             trackMixpanel("clicked on common connection",url,function(){
//                 window.location.replace(url);
//             })
//         }
//     }
//
//     $scope.interactionsContext = function(emailId){
//         window.location.replace('/contact/selected?context='+emailId)
//     }
//
//     $scope.goToUrl = function(url){
//         window.location = url;
//     }
// });

function hidePrevNext(idNameP,idNameN,hide){
    if(hide){
        $("#"+idNameP).addClass('hide')
        $("#"+idNameN).addClass('hide')
    }
    else{
        $("#"+idNameP).removeClass('hide')
        $("#"+idNameN).removeClass('hide')
    }
}

function addHide(elementId,addHide){
    addHide ? $("#"+elementId).addClass('hide') : $("#"+elementId).removeClass('hide')
}

function getCommonConnections($scope, $http, url, isHides){
    if(!$scope.grandTotal){
        $scope.grandTotal = 0;
    }

    $scope.no_people_near_you_show = false;
    $http.get(url)
        .success(function (response) {
            $scope.isConnectionOpen = false;
            if(response.SuccessCode){
                $scope.grandTotal = response.Data.total;
                $scope.timezone = response.Data.timezone;
                if(response.Data.contacts && response.Data.contacts.length > 0){
                    var html = '';
                    hidePrevNext('als-prev_l','als-next_l',true)
                    $scope.currentLength += response.Data.returned;

                    for(var i=0; i<response.Data.contacts.length; i++){
                        var li = '';
                        var obj = {name:''};
                        if(checkRequired(response.Data.contacts[i].firstName) && response.Data.contacts[i].firstName.length > 0){
                            response.Data.contacts[i].firstName.forEach(function(name){
                                if(checkRequired(name)){
                                    obj.name = name
                                }
                            })
                        }
                        if(checkRequired(response.Data.contacts[i].lastName) && response.Data.contacts[i].lastName.length > 0){
                            response.Data.contacts[i].lastName.forEach(function(name){
                                if(checkRequired(name)){
                                    obj.name += ' '+name
                                }
                            })
                        }
                        if(!checkRequired(obj.name) && response.Data.contacts[i]._id.emailId){
                            obj.name = response.Data.contacts[i]._id.emailId;
                        }
                        else if(!checkRequired(obj.name) && response.Data.contacts[i]._id.mobileNumber){
                            obj.name = response.Data.contacts[i]._id.mobileNumber;
                        }

                        if(checkRequired(obj.name)){
                            obj.fullName = obj.name;
                            obj.name = getTextLength(obj.name,13);
                        }
                        if(checkRequired(response.Data.contacts[i].userId) && response.Data.contacts[i].userId.length > 0){
                            response.Data.contacts[i].userId.forEach(function(id){
                                if(checkRequired(id)){
                                    obj.image = '/getImage/'+id;
                                }
                            })
                        }
                        var contactImageLink = response.Data.contacts[i].contactImageLink?encodeURIComponent(response.Data.contacts[i].contactImageLink):null

                        if(!checkRequired(obj.image) && contactImageLink){
                            obj.cursor = 'cursor:pointer';
                            obj.image = '/getContactImage/'+response.Data.contacts[i]._id.emailId+'/'+contactImageLink;
                            obj.noPicFlag = false;
                        }else if(!contactImageLink){
                            obj.cursor = 'cursor:default';
                            obj.image = '';
                            obj.noPicFlag = true;
                        }else obj.cursor = 'cursor:pointer';

                        if(checkRequired(response.Data.contacts[i].publicProfileUrl) && response.Data.contacts[i].publicProfileUrl.length > 0){
                            response.Data.contacts[i].publicProfileUrl.forEach(function(id){
                                if(checkRequired(id)){
                                    if(id.charAt(0) != 'h'){
                                        obj.url = '/'+id;
                                    }
                                }
                            })
                        }
                        if(!checkRequired(obj.url)){
                            obj.url = null;
                        }
                        obj.interactionsCount = response.Data.contacts[i].count || 0;
                        obj.idName = 'losing_touch_item_'+response.Data.contacts[i].lastInteracted;
                        obj.className = 'hide';
                        obj.noPPic = (obj.name.substr(0,2)).toUpperCase();
                        obj.emailId = response.Data.contacts[i]._id.emailId;
                        if(obj.interactionsCount != 0){
                            var iDate = moment(response.Data.contacts[i].lastInteracted).tz($scope.timezone);
                            var now = moment().tz($scope.timezone);
                            var diff = now.diff(iDate);
                            diff = moment.duration(diff).asDays();
                            obj.duration = moment.duration(diff,"days").humanize()+' ago';
                        }
                        else obj.duration = "no interactions";

                        if(isHides && i<itemsToShow){
                            obj.className = ''
                        }
                        $scope.losingTouchConnections.push(obj);

                        $scope.losingTouchConnectionsIds.push('losing_touch_item_'+response.Data.contacts[i].lastInteracted);

                    }
                    if(isHides){
                        $scope.statusPrev = 1;
                        $scope.statusNext = itemsScroll;
                    }
                    $scope.showHideArrows()
                }
                else if(isHides){
                    hidePrevNext('als-prev_l','als-next_l',true)
                    $scope.no_people_near_you_show = true;

                    $scope.no_people_near_you_message = response.Message;
                }else{
                    $scope.no_people_near_you_show = true;
                    $scope.no_people_near_you_message = response.Message;
                    $scope.isAllConnectionsLoaded = true;
                }
            }
            else if(isHides){
                hidePrevNext('als-prev_l','als-next_l',true)
                $scope.no_people_near_you_show = true;
                $scope.no_people_near_you_message = response.Message;
            }
            else{
                $scope.no_people_near_you_show = true;
                $scope.no_people_near_you_message = response.Message;
                $scope.isAllConnectionsLoaded = true;
            }
        })
}