
function Validation(){

}

Validation.prototype.Registration = function(userProfile,callback){

   if ((validateRequireField(userProfile.firstName) == true) && (validateRequireField(userProfile.lastName) == true) &&
         (validateRequireField(userProfile.publicProfileUrl) == true) &&
         (validateRequireField(userProfile.emailId) == true) &&
          (validateRequireField(userProfile.password) == true)) {
        if (validateEmailField(userProfile.emailId) == true) {

                     callback(true);

       }else callback(4011);
    
   }else callback(4010);
};

Validation.prototype.RegistrationThirdParty = function(userProfile,callback){
    if ((validateRequireField(userProfile.firstName) == true) && (validateRequireField(userProfile.lastName) == true) &&
         (validateRequireField(userProfile.publicProfileUrl) == true)
         ) {
    // callback(true);
          //  if (validateEmailField(userProfile.emailId) == true) {
               // callback(true);

                   // if (validatePhoneNumber(userProfile.mobile) == true) {
                       callback(true);
                    //}else callback(4012);

           // }else callback(4011);

   }else callback(4010);
};

Validation.prototype.LogIn = function(userProfile,callback){
    if(((validateEmailField(userProfile.emailId) == true) && (validatePasswordField(userProfile.password) == true) &&
        (validateRequireField(userProfile.emailId) == true) && (validateRequireField(userProfile.password) == true)))
    {
        callback(true);
    }
    else
    {
        callback(false);
    }
};

Validation.prototype.validateInvitationDetails = function(invitationDetails, callback){
  
   if ((validateRequireField(invitationDetails.senderId) == true) && 
       (validateRequireField(invitationDetails.senderName) == true) && 
       (validateRequireField(invitationDetails.senderEmailId) == true) && 
       (validateRequireField(invitationDetails.to.receiverId) == true) && 
       (validateRequireField(invitationDetails.to.receiverName) == true) && 
       (validateRequireField(invitationDetails.to.receiverEmailId) == true) &&
       (validateRequireField(invitationDetails.scheduleTimeSlots) == true) && 
       (validateRequireField(invitationDetails.scheduleTimeSlots[0].title) == true) && 
       (validateRequireField(invitationDetails.scheduleTimeSlots[0].location) == true) &&
       (validateRequireField(invitationDetails.scheduleTimeSlots[0].description) == true)) {
       callback(true);
   }
   else callback(4010);
}

Validation.prototype.validateInvitationDetailsEmail = function(invitationDetails, callback){
  
   if ((validateRequireField(invitationDetails.senderName) == true) && 
       (validateRequireField(invitationDetails.senderEmailId) == true) && 
       (validateRequireField(invitationDetails.to.receiverId) == true) && 
       (validateRequireField(invitationDetails.to.receiverName) == true) && 
       (validateRequireField(invitationDetails.to.receiverEmailId) == true) &&
       (validateRequireField(invitationDetails.scheduleTimeSlots) == true)) {
       callback(true);
   }
   else callback(4010);
}

Validation.prototype.validateEventData = function(eventDetails,callback){
    if((validateRequireField(eventDetails.eventName) == true) &&
        (validateRequireField(eventDetails.eventLocationUrl) == true) &&
        (validateRequireField(eventDetails.eventLocation) == true) &&
        (validateRequireField(eventDetails.startDateTime) == true) &&
        (validateRequireField(eventDetails.endDateTime) == true) &&
        (validateRequireField(eventDetails.eventCategory) == true) &&
        (validateRequireField(eventDetails.accessType) == true) &&
        (validateRequireField(eventDetails.locationName) == true) &&
        (validateRequireField(eventDetails.eventDescription) == true) &&
        (validateRequireField(eventDetails.createdDate) == true) &&
        (validateRequireField(eventDetails.userId) == true) &&
        (validateRequireField(eventDetails.userEmailId) == true) &&
        (validateRequireField(eventDetails.userName) == true)){
        callback(true);
    }
    else callback(4010);
}

/* MOBILE DATA VALIDATIONS */

// returns ** true-nonEmpty** ** false-empty**
Validation.prototype.isEmptyObject = function(obj){
   return validateRequireField(obj) ? Object.keys(obj).length != 0 : false
};

Validation.prototype.validateMobileSignUpProfileMandatoryFields = function(profile, source){
    if(!validateRequireField(profile.emailId)){
        return getStatus('EMAIL_ID_NOT_FOUND');
    }else if(!validateEmailField(profile.emailId)){
        return getStatus('INVALID_EMAIL_ID');
    }else if(!validateRequireField(profile.uniqueName)){
        return getStatus('UNIQUE_NAME_NOT_FOUND');
    }else if(!validateRelatasIdentity(profile.uniqueName)){
        return getStatus('INVALID_UNIQUE_NAME');
    }else if(!validateRequireField(profile.firstName)){
        return getStatus('FIRST_NAME_NOT_FOUND');
    }else if(!validateRequireField(profile.lastName)){
        return getStatus('LAST_NAME_NOT_FOUND')
    }else if(!validateRequireField(profile.phoneNumber)){
        return getStatus('PHONE_NUMBER_NOT_FOUND')
    }else if(!phoneNumber(profile.phoneNumber)){
        return getStatus('INVALID_PHONE_NUMBER')
    } else if(source === "GOOGLE" && !validateRequireField(profile.googleId)){
        return getStatus('GOOGLE_ID_NOT_FOUND')
    }else if(!validateRequireField(profile.account)){
        return getStatus('GOOGLE_ACCOUNT_NOT_FOUND')
    }else if(!validateRequireField(profile.account.token)){
        return getStatus('GOOGLE_ACCOUNT_TOKEN_NOT_FOUND')
    }else if(!validateRequireField(profile.account.refreshToken)){
        return getStatus('GOOGLE_ACCOUNT_REFRESH_TOKEN_NOT_FOUND')
    }else if(!validateRequireField(profile.account.id)){
        return getStatus('GOOGLE_ACCOUNT_ID_NOT_FOUND')
    }else if(!validateRequireField(profile.account.emailId)){
        return getStatus('GOOGLE_ACCOUNT_EMAIL_ID_NOT_FOUND')
    }else return true;
};

Validation.prototype.validateGoogleAccount = function(gAccountDetails){
    if(!validateRequireField(gAccountDetails)){
        return getStatus('GOOGLE_ACCOUNT_NOT_FOUND')
    }else if(!validateRequireField(gAccountDetails.token)){
        return getStatus('GOOGLE_ACCOUNT_TOKEN_NOT_FOUND')
    }else if(!validateRequireField(gAccountDetails.refreshToken)){
        return getStatus('GOOGLE_ACCOUNT_REFRESH_TOKEN_NOT_FOUND')
    }else if(!validateRequireField(gAccountDetails.id)){
        return getStatus('GOOGLE_ACCOUNT_ID_NOT_FOUND')
    }else if(!validateRequireField(gAccountDetails.emailId)){
        return getStatus('GOOGLE_ACCOUNT_EMAIL_ID_NOT_FOUND')
    }else return true;
};

var mandatoryFieldKeys = [
    'EMAIL_ID_NOT_FOUND',
    'INVALID_EMAIL_ID',
    'UNIQUE_NAME_NOT_FOUND',
    'INVALID_UNIQUE_NAME',
    'FIRST_NAME_NOT_FOUND',
    'LAST_NAME_NOT_FOUND',
    'PHONE_NUMBER_NOT_FOUND',
    'INVALID_PHONE_NUMBER',
    'GOOGLE_ID_NOT_FOUND',
    'GOOGLE_ACCOUNT_NOT_FOUND',
    'GOOGLE_ACCOUNT_TOKEN_NOT_FOUND',
    'GOOGLE_ACCOUNT_REFRESH_TOKEN_NOT_FOUND',
    'GOOGLE_ACCOUNT_ID_NOT_FOUND',
    'GOOGLE_ACCOUNT_EMAIL_ID_NOT_FOUND'
];

function getStatus(key){

    if(mandatoryFieldKeys.indexOf(key) != -1){
        return {key:key,status:4010}
    }
    else return true
}

function validateRelatasIdentity(rIdentity){
    var regex = new RegExp("^[a-zA-Z0-9._ ]*$");
    if(regex.test(rIdentity) && rIdentity != ''){
        return true;
    }
    else{
        return false;
    }
}

Validation.prototype.validateTask = function (task) {

    if (!validateRequireField(task)) {
        return getStatusTask('TASK_DETAILS_NOT_FOUND',5000)
    } else if (!validateRequireField(task.taskName)) {
        return getStatusTask('TASK_NAME_NOT_FOUND',5000)
    } else if (!validateRequireField(task.assignedToEmailId) && !validateRequireField(task.assignedTo)) {
        return getStatusTask('TASK_ASSIGNED_TO_NOT_FOUND',5000);
    } else if (!validateRequireField(task.dueDate)) {
        return getStatusTask('TASK_DUE_DATE_NOT_FOUND',5000)
    }else if (!validateRequireField(task.refId) && task.taskFor != 'other') {
        return getStatusTask('TASK_REF_ID_NOT_FOUND [invitationId or call id]',5000)
    } else if (!validateRequireField(task.taskFor)) {
        return getStatusTask('TASK_FOR_NOT_FOUND [meeting or call or other]',5000)
    } else return true;
};

Validation.prototype.validateLinkedinAccount = function (linkedin) {

    if (!validateRequireField(linkedin)) {
        return getStatusTask('LINKEDIN_DETAILS_NOT_FOUND',5000)
    } else if (!validateRequireField(linkedin.id)) {
        return getStatusTask('LINKEDIN_ID_NOT_FOUND',5000)
    } else if (!validateRequireField(linkedin.token)) {
        return getStatusTask('LINKEDIN_TOKEN_NOT_FOUND',5000)
    } else if (!validateRequireField(linkedin.emailId)) {
        return getStatusTask('LINKEDIN_EMAIL_ID_NOT_FOUND',5000)
    }else if (!validateRequireField(linkedin.name)) {
        return getStatusTask('LINKEDIN_NAME_NOT_FOUND',5000)
    } else return true;
};

Validation.prototype.validateFacebookAccount = function (facebook) {

    if (!validateRequireField(facebook)) {
        return getStatusTask('FACEBOOK_DETAILS_NOT_FOUND',5000)
    } else if (!validateRequireField(facebook.id)) {
        return getStatusTask('FACEBOOK_ID_NOT_FOUND',5000)
    } else if (!validateRequireField(facebook.token)) {
        return getStatusTask('FACEBOOK_TOKEN_NOT_FOUND',5000)
    } else if (!validateRequireField(facebook.emailId)) {
        return getStatusTask('FACEBOOK_EMAIL_ID_NOT_FOUND',5000)
    }else if (!validateRequireField(facebook.name)) {
        return getStatusTask('FACEBOOK_NAME_NOT_FOUND',5000)
    } else return true;
};

Validation.prototype.validateTwitterAccount = function (twitter) {

    if (!validateRequireField(twitter)) {
        return getStatusTask('TWITTER_DETAILS_NOT_FOUND',5000)
    } else if (!validateRequireField(twitter.id)) {
        return getStatusTask('TWITTER_ID_NOT_FOUND',5000)
    } else if (!validateRequireField(twitter.token)) {
        return getStatusTask('TWITTER_TOKEN_NOT_FOUND',5000)
    }else if (!validateRequireField(twitter.refreshToken)) {
        return getStatusTask('TWITTER_REFRESH_TOKEN_NOT_FOUND',5000)
    } else if (!validateRequireField(twitter.displayName)) {
        return getStatusTask('TWITTER_DISPLAY_NAME_NOT_FOUND',5000)
    }else if (!validateRequireField(twitter.userName)) {
        return getStatusTask('TWITTER_USERNAME_NOT_FOUND',5000)
    } else return true;
};

function getStatusTask(key,status){
    return {key:key,status:status}
}

Validation.prototype.validateMeetingNewTimeLocation = function (details) {

    if (!validateRequireField(details)) {
        return getStatusTask('NO_REQUEST_BODY',5000)
    } else if (!validateRequireField(details.invitationId)) {
        return getStatusTask('INVITATION_ID_NOT_FOUND',5000)
    } else if (!validateRequireField(details.newSlotDetails)) {
        return getStatusTask('NEW_DATES_AND_LOCATIONS_SLOT_NOT_FOUND',5000)
    } else if (!validateRequireField(details.newSlotDetails.startDateTime)) {
        return getStatusTask('NEW_START_DATE_TIME_NOT_FOUND',5000)
    }else if (!validateRequireField(details.newSlotDetails.endDateTime)) {
        return getStatusTask('NEW_END_DATE_TIME_NOT_FOUND',5000)
    } else if (!validateRequireField(details.newSlotDetails.suggestedLocation)) {
        return getStatusTask('MEETING_NEW_SUGGESTED_LOCATION_NOT_FOUND',5000)
    } else if (!validateRequireField(details.newSlotDetails.locationType)) {
        return getStatusTask('MEETING_NEW_SUGGESTED_LOCATION_TYPE_NOT_FOUND',5000)
    } else return true;

};

Validation.prototype.validateCreateMeetingFields = function (details,withoutLogin) {

    if (!validateRequireField(details)) {
        return getStatusTask('MEETING_INFO_NOT_FOUND',4010)
    } else if (!validateRequireField(details.title)) {
        return getStatusTask('MEETING_TITLE_NOT_FOUND',4010)
    } else if (!validateRequireField(details.locationType)) {
        return getStatusTask('MEETING_LOCATION_TYPE_NOT_FOUND',4010)
    } else if (!validateRequireField(details.location)) {
        return getStatusTask('MEETING_LOCATION_NOT_FOUND',4010)
    }else if (!validateRequireField(details.description)) {
        return getStatusTask('MEETING_DESCRIPTION_NOT_FOUND',4010)
    } else if (!validateRequireField(details.start)) {
        return getStatusTask('MEETING_START_DATE_MISSING',4010)
    } else if (!validateRequireField(details.end)) {
        return getStatusTask('MEETING_END_DATE_MISSING',4010)
    } else{
        if(withoutLogin){
            if (!validateRequireField(details.senderEmailId)) {
                return getStatusTask('MEETING_SENDER_EMAIL_ID_NOT_FOUND',4010)
            } else if (!validateRequireField(details.senderFirstName)) {
                return getStatusTask('MEETING_SENDER_NAME_NOT_FOUND',4010)
            } else if (!validateRequireField(details.senderLastName)) {
                return getStatusTask('MEETING_SENDER_NAME_NOT_FOUND',4010)
            }
            else return true;
        }
        else return true;
    }

};

Validation.prototype.validateSendMessageFields = function (details) {

    if (!validateRequireField(details)) {
        return getStatusTask('NO_REQUEST_BODY',5000)
    } else if (!validateRequireField(details.receiverEmailId)) {
        return getStatusTask('RECEIVER_EMAIL_ID_NOT_FOUND',5000)
    } else if (!validateRequireField(details.receiverName)) {
        return getStatusTask('RECEIVER_NAME_NOT_FOUND',5000)
    } else if (!validateRequireField(details.message)) {
        return getStatusTask('MESSAGE_NOT_FOUND',5000)
    } else if (!validateRequireField(details.subject)) {
        return getStatusTask('SUBJECT_NOT_FOUND',5000)
    } else return true;

};


Validation.prototype.validateEmail = function(email, callback){


    var emailText = email;
    var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
    
    if (pattern.test(emailText))
        callback(true);
    else 
        callback(false);
};

Validation.prototype.validateEmailFlag = function(email){


    var emailText = email;
    var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
    if (pattern.test(emailText))
    {
       return true;
    }
    else
    {
        return false;
    }
};

function validateRequireField(requireField)
 {
    if (requireField == null || requireField == "" || requireField == undefined )
    {
        return false;
    }
    else
    {

        return true;
    }
 }


function validatePasswordField(password)
 {
    var Password =  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;
    if(password.match(Password))
    {
       
        return true;
    }
    else
    {
        
        return false;
    }
 }


function validateEmailField(email)
 {
    var emailText = email;
    var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
    if (pattern.test(emailText))
    {
       
        return true;
    }
    else
    { 
        
        return false;
    }
 }

 function validateCharacterType(username)
    {
        var testString = /^[a-zA-Z]+$/;
        var userName = username;
        if (userName.match(testString))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
/*function validatePhoneNumber(inputtxt)
{
    var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if(inputtxt.match(phoneno))
    {
        return true;
    }
    else
    {

        return false;
    }
}*/

function phoneNumber(no){
    var targ=no.replace(/[^\d]/g,''); // remove all non-digits
    if(targ.length >=10 && targ.length <=15){
        return true;
    }
    else return false;
}

 function validatePhoneNumber(inputtxt)
{  

  var phoneno = /^\d{10}$/;
  if(inputtxt.match(phoneno))  
        {  
            
      return true;  
        }  
      else  
        {  
            
       
        return false;  
        }  
}


module.exports = Validation;
