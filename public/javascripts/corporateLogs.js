
$(document).ready(function(){
    var table;
    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    applyDataTable();
    getLogData();
    function getLogData(){
        var url = window.location.href.split('/')[window.location.href.split('/').length-1];
        url = '/corporate/admin/log/'+url+'/getLog';
        $.ajax({
            url:url,
            type:'GET',
            datatype:'JSON',
            success:function(logs){

                    if(checkRequiredField(logs.file) && logs.file.length > 0){
                        displayLogs(logs.file)
                    }
            }
        })
    }

    function displayLogs(logs){
        for(var log=0; log<logs.length; log++){
            addRowsToTable([
               logs[log].corporateName,
               logs[log].corporateEmailId,
               logs[log].changedFrom,
               logs[log].changedTo,
               logs[log].changedBy,
                getEventDateFormat(new Date(logs[log].editDate))
            ])
        }
    }

    function addRowsToTable(rowArr){
        table.row.add( rowArr ).draw();
    }

    function getEventDateFormat(date){
        date = new Date(date);
        var day = date.getDate();

        var format = getVakidNumber(day)+'-'+monthNameSmall[date.getMonth()]+'-'+date.getFullYear();
        return format;
    }
    function getVakidNumber(num){
        if(num < 10){
            return '0'+num;
        }else return num;
    }

    function applyDataTable(){
        table = $('#log-table').DataTable({
            "dom": '<"top"iflp<"clear">>',
            "order": [[ 1, "asc" ]],

            "oLanguage": {
                "sEmptyTable": "No Logs Found"
            }
        });
    }

    function checkRequiredField(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    };
});