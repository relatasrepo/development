$(document).ready(function() {
    var monthNameFull = ['January','February','March','April','May','June','July','August','September','October','November','December']
    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    var userProfile;
    var event;
    var map;
    var addToCallFlag = false;
    var removeCallFlag = false;
    var stopNavigationFlag = false;
    function sidebarHeight(){
        var sideHeight = $(document).height()
        $("#sidebarHeight").css({'min-height':sideHeight})
    }
    getEventMsg();
    function getEventMsg(){
        $.ajax({
            url:'/getEventMessage',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){
                clearEventMsg()
               if(validateRequired(result)){
                   showMessagePopUp(result.message,result.type);
                   if(result.type != 'error')
                       mixpanelTrack("RSVP Event");
                       mixpanelIncrement("# RSVP");
               }
            }
        })
    }
    function clearEventMsg(){
        $.ajax({
            url:'/clearEventMessage',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){

            }
        })
    }


    $("#logout").on("click",function(){

        $.ajax({
            url:'/isLoggedInUser',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){
                if(result){
                    window.location.replace('/logout');
                }else{
                    $("#eventsBody").slideToggle();
                    $("#lightBoxPopup").slideToggle();
                }
            }
        })
    });
    getStoredSession();
    function getStoredSession(){
        $.ajax({
            url:'/getSavedEventDetails',
            type:'GET',
            datatpe:'JSON',

            success:function(response){
                getUserProfile();
              if(validateRequired(response)){
                  if(response.addToCallFlag){
                      addToCallFlag = response.addToCallFlag;
                  }
                  if(response.removeCallFlag){
                      removeCallFlag = response.removeCallFlag;
                  }
              }
                clearSession()

            }
        })
    }
    // Function to get user contacts info
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;

                getRequestedEvent();
                if(result == false){

                    stopNavigationFlag = true;
                    stopNavigation(true);
                    $("#logout").text('Sign Up/ Login');
                    $('#profilePic').attr("src",'/images/default.png');
                }else{
                    $("#logout").text('LOGOUT');
                    identifyMixPanelUser(result);
                    if(validateRequired(result.firstName)){
                        if( result.profilePicUrl.charAt(0) == '/' || result.profilePicUrl.charAt(0) == 'h'){

                            $('#profilePic').attr("src",result.profilePicUrl);
                        }else $('#profilePic').attr("src","/"+result.profilePicUrl);
                    }

                    imagesLoaded("#profilePic",function(instance,img) {
                        if(instance.hasAnyBroken){
                            $('#profilePic').attr("src","/images/default.png");
                        }
                    });
                }
            },
            error: function (event, request, settings) {


            },
            timeout: 20000
        })
    }

    function identifyMixPanelUser(profile){

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page":window.location.href
        });
    }
    function getRequestedEvent(){
        $.ajax({
            url:'/getRequestedEvent',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(thisEvent){
                if(!validateRequired(thisEvent)){
                    showMessagePopUp("Event not found.",'error');
                }else
                if(thisEvent == false){

                }
                else if(validateRequired(thisEvent)){
                    event = thisEvent;
                    getUserSubscribeDetailsForThisEvent(event._id);
                    if(event.createdBy.userId == userProfile._id){
                        $("#editEvent").show();
                        $("#deleteEvent").show();
                        var url = '/event/rsvp/'+event._id+'/'+event.createdBy.userId;
                        $("#confirmedParticipantsLink").html('<a style="color: #615f5f;" href='+url+'><button style="border-radius: 4px;background-color: #1bcaff !important;" class="blue-btn">RSVP list</button></a>');
                    }
                    else{
                        $("#editEvent").hide();
                        $("#deleteEvent").hide();
                    }
                    var eImage;
                    if(validateRequired(event.image)){
                        $("#eventImage").attr("src",event.image.imageUrl);
                        eImage = event.image.imageUrl;
                    }else{
                        $("#eventImage").attr("src",'https://relatas-live-imagestorage.s3.amazonaws.com/defaultEventImageGray.png');
                    }

                    displayEventDocs(event.docs);
                    var createdDate = new Date(event.createdDate);
                    $("#evenTitle").text(event.eventName);
                    $("#eventTitle").text(event.eventName);
                    $("#eventLocation").text(event.eventLocation);

                    getEventCreatorProfile(event.createdBy.userId);
                    $("#createdDate").text(getDateStringToShow(createdDate));
                    $("#eventDescription").html(event.eventDescription || "Not Available");
                    $("#eventDateTime").text(getEventDateFormat(new Date(event.startDateTime))+' - '+getEventDateFormat(new Date(event.endDateTime)));

                    var linkedinUrl = "http://www.linkedin.com/shareArticle?mini=true&url="+window.location+"&title="+event.eventName+"&summary="+event.speakerDesignation || event.eventName+"&source=Relatas"
                    var twitterUrl ="http://twitter.com/share?text=Checkout this event "+event.eventName+":&url="+window.location
                    var facebookUrl = "https://www.facebook.com/sharer/sharer.php?u="+window.location

                    $("#linkedinShare").attr('href',linkedinUrl);
                    $("#twitterShare").attr('href',twitterUrl);
                    $("#facebookShare").attr('href',facebookUrl);

                    $("#speakerName").text(event.speakerName || '');
                    $("#speakerDesignation").text(event.speakerDesignation || '');

                    getAllUsersConfirmedEvent(event._id);
                    createMap(event);
                }
                else{
                    showMessagePopUp("Event not found.",'error');
                }
            }
        })
    }

    function getEventCreatorProfile(userId){
        $.ajax({
            url:"/getUserBasicInfo/"+userId,
            type:"GET",
            datatype:"JSON",
            success:function(profile){
                if(validateRequired(profile)){
                    var name = profile.firstName+' '+profile.lastName;
                    var useUrl = '/'+getValidUniqueUrl(profile.publicProfileUrl)
                    var html = '<a href='+useUrl+'>'+name+'</a>'
                    $("#hostedBy").html("Event Posted by "+html);

                }
            }
        })
    }

    function getValidUniqueUrl(uniqueName){
        var url = window.location+''.split('/');
        var patt = new RegExp(url[2]);
        if(patt.test(uniqueName)){

            url = uniqueName.split('/');
            for (var i = url.length - 1; i >= 0; i--) {
                if (url[i] == 'u') {
                    uniqueName = url[i+1];
                }
            }
            return uniqueName;

        }else {
            return uniqueName;
        }
    }

    function displayEventDocs(docs){
        if(validateRequired(docs)){
            if(docs.length > 0){
                $("#eventDocs").show();
                for(var i=0; i<docs.length; i++){
                    var docUrl = '/readDocument/'+docs[i].documentId;
                   $("#eventDocumentList").html('<div><a href='+docUrl+'><img style="padding-left: 1%;" src="/images/icon_file_pdf.png"></a><br><a href='+docUrl+'><span>'+docs[i].documentName+'</span></a></div>');
                }
            }
        }
    }

    $("#editEvent").on("click",function(){
        var eventId = event._id;
        if(event.createdBy.userId == userProfile._id){
            window.location.replace('/event/edit/'+eventId);
        }
        else{
            showMessagePopUp("You are not creator of this event to take actions on this event.",'error');
        }
    });
        var deleteFlag = false;
    $("#deleteEvent").on("click",function(){
        var eventId = event._id;
        if(event.createdBy.userId == userProfile._id){
            $.ajax({
                url:'/event/delete/'+eventId,
                type:'GET',
                datatype:'JSON',
                traditional:true,
                success:function(response){
                    if(response){
                        showMessagePopUp("Event has been successfully deleted.",'success');
                        deleteFlag = true;
                    }
                    else{
                        showMessagePopUp("An error occurred while deleting event. Please try again.",'error');
                    }
                }
            })

        }
        else{
            showMessagePopUp("You are not creator of this event to take actions on this event.",'error');
        }
    });

    function getUserSubscribeDetailsForThisEvent(eventId){
        $.ajax({
            url:'/event/getGoogleId/'+eventId,
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(response){
                if(response == 'loginRequired'){
                    $("#confirmRSVP2").show();
                }
                if (!validateRequired(response)) {
                    $("#addEventToCalendar").show();
                    $("#confirmRSVP2").show();
                    addCallFlagFunction();
                }else
               if(response == false){

               }
               else{
                   if (validateRequired(response)) {
                       if (validateRequired(response.eventsList)) {
                           if (validateRequired(response.eventsList[0])) {
                               if (response.eventsList[0].googleEventId) {
                                   if(response.eventsList[0].isDeleted){

                                       $("#addEventToCalendar").show();
                                       addCallFlagFunction();
                                   }
                                   else{
                                       if(response.eventsList[0].confirmed){
                                           $("#confirmRSVP2").hide();
                                           $("#removeEventFromCalendar").show();
                                           $("#addEventToCalendar").hide();
                                       }else{
                                           $("#removeEventFromCalendar").show();
                                           $("#confirmRSVP").show();
                                           $("#confirmRSVP2").show();
                                           addCallFlagFunction();
                                       }
                                   }
                               }
                               else{

                                   $("#addEventToCalendar").show();
                                   addCallFlagFunction();
                               }
                           } else  $("#addEventToCalendar").show();
                       } else  $("#addEventToCalendar").show();
                   } else  $("#addEventToCalendar").show();
               }
            }
        })
    }

    function addCallFlagFunction(){
        if(addToCallFlag){
            addToCallFlag = false;
            setTimeout(function(){
                addEventToCalendar();
            },100)

        }else
        if(removeCallFlag){
            removeCallFlag = false;
            setTimeout(function(){
                removeEventFromCalendar();
            },100)
        }

    }

    function getAllUsersConfirmedEvent(eventId){
        var userList = [];
        $.ajax({
            url:"/getAllUsersConfirmedEvent/"+eventId,
            type:"GET",
            datatype:"JSON",
            success:function(sList){
                for(var i=0; i<sList.length; i++){
                    userList.push(sList[i].userId);
                }
                getUserList(userList);
            }
        })
    }

    function getUserList(userIdArr){

        var data = {
            data:JSON.stringify(userIdArr)
        }
        $.ajax({
            url:'/userBasicInfoByArrayOfIds',
            type:'POST',
            datatype:'JSON',
            data:data,
            success:function(userList){
                $("#participantsList").html('')
                if(validateRequired(userList)){
                    if(userList.length > 0){
                        for(var i=0; i<userList.length; i++){
                            var name = userList[i].firstName+' '+userList[i].lastName;
                            name.replace(/\s/g, '&nbsp;');
                            if(validateRequired(userList[i].profilePicUrl)){
                                if( userList[i].profilePicUrl.charAt(0) == '/' || userList[i].profilePicUrl.charAt(0) == 'h'){

                                    $("#participantsList").append('<img title='+name+' style="width: 40px;height:40px;padding: 1%;border-radius: 4px;"  class="profile-picture-border" src='+userList[i].profilePicUrl+'>');
                                }else $("#participantsList").append('<img title='+name+' style="width: 40px;height:40px;padding: 1%;border-radius: 4px;"  class="profile-picture-border" src='+'/'+userList[i].profilePicUrl+'>');
                            }else $("#participantsList").append('<img title='+name+' style="width: 40px;height:40px;padding: 1%;border-radius: 4px;"  class="profile-picture-border" src="/images/default.png">');

                        }
                    }
                }
            }
        })
    }

    function createMap(event){
        var eType = event.eventLocation;
       var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();
        var latitude,longitude,myLatlng,mapOptions;
        var address;
        var latlng;
        if(eType != 'Online'){
            var locationUrl = event.eventLocationUrl.split("@");
            if(validateRequired(event.locationName)){
                 address = event.locationName
            }
            else{
                var addrArr = locationUrl[0].split("/");

                address = addrArr[5]
            }

            var latLangArr = locationUrl[1].split(",");
             latitude = parseFloat(latLangArr[0]);
             longitude = parseFloat(latLangArr[1]);
             latlng = new google.maps.LatLng(latitude, longitude);
              myLatlng = new google.maps.LatLng(latitude, longitude);

             mapOptions = {
                center: latlng,
                zoom: 13
             };
            geocoder.geocode( { latLng:latlng}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    var newAddress = results[0].formatted_address;
                   if(validateRequired(newAddress)){
                       $("#eventLocation").text(getTextLength(event.locationName || ''+','+newAddress,100));
                       $("#eventLocation").attr("title",event.locationName || ''+','+newAddress);
                   }
                }
            });
        }
        else{
            myLatlng = new google.maps.LatLng(13.5333, 2.0833);
            mapOptions = {
                center: myLatlng,
                zoom: 2
            };
        }


        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        setTimeout(function(){
            sidebarHeight();
        },500)

        if(eType != 'Online'){
            var position = latlng;
            var fAddress = address.replace(/\+/g, ' ');
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude,longitude),
                map: map,
                title: "Event: " + event.eventName,
                currentEvent:event
            });
            infowindow.setContent(fAddress);
            infowindow.open(map, marker);

        }
    }

    function getTextLength(text,maxLength){

        var textLength = text.length;
        if(textLength >maxLength){
            var formattedText = text.slice(0,maxLength)
            return formattedText+'..';
        }
        else return text;
    }

    $("#addEventToCalendar").on("click",function(){
        addEventToCalendar()
    });

    $("#confirmRSVP").on("click",function(){
        addEventToCalendar()
    });

    $("#confirmRSVP2").on("click",function(){
        addEventToCalendar()
    });

    $("#removeEventFromCalendar").on("click",function(){
        removeEventFromCalendar()
    });

    function addEventToCalendar(){
        if(validateRequired(event)){
            $("#addEventToCalendar").attr("disabled",true);
            $.ajax({
                url:'/event/addToCalendar/'+event._id,
                type:'GET',
                datatype:'JSON',
                success:function(response){

                    if(response == 'loginRequired'){
                        addToCallFlag = true;
                        removeCallFlag = false;
                        $("#eventsBody").slideToggle();
                        $("#lightBoxPopup").slideToggle();

                        //showMessagePopUp("Please login to continue.",'error');
                    }else
                    if(response){
                        mixpanelTrack("RSVP Event");
                        mixpanelIncrement("# RSVP");
                        setTimeout(function(){
                            $("#addEventToCalendar").hide();
                            $("#confirmRSVP").hide();
                            $("#confirmRSVP2").hide();
                            $("#removeEventFromCalendar").show();
                            showMessagePopUp("Event successfully added to your calendar.",'success');
                            getAllUsersConfirmedEvent(event._id);
                            $("#addEventToCalendar").attr("disabled",false);
                        },3000)

                    }
                    else{
                        showMessagePopUp("An error occurred while adding event to calendar. Please try again.",'error');
                    }
                }
            })
        }
    }

    function removeEventFromCalendar(){
        if(validateRequired(event)){
            $("#removeEventFromCalendar").attr("disabled",true);
            $.ajax({
                url:'/event/removeFromCalendar/'+event._id,
                type:'GET',
                datatype:'JSON',
                success:function(response){
                    $("#removeEventFromCalendar").attr("disabled",false);
                    if(response == 'loginRequired'){
                        removeCallFlag =true;
                        addToCallFlag = false;
                        $("#eventsBody").slideToggle();
                        $("#lightBoxPopup").slideToggle();
                    }else
                    if(response){
                        $("#addEventToCalendar").show();
                        $("#confirmRSVP").hide();
                        $("#confirmRSVP2").hide();
                        $("#removeEventFromCalendar").hide();
                        showMessagePopUp("Event successfully removed from your calendar.",'success');
                        getAllUsersConfirmedEvent(event._id);
                    }
                    else{
                        showMessagePopUp("An error occurred while removing event. Please try again.",'error');
                    }
                }
            })
        }
    }
    function clearSession(){

        $.ajax({
            url:'/clearSavedEventDetails',
            type:'GET',
            datatpe:'JSON',
            success:function(response){

            }
        })
    }
    function storeToSession(){
        var data = {
            addToCallFlag:addToCallFlag,
            removeCallFlag: removeCallFlag
        }

        $.ajax({
            url:'/saveEventDetails',
            type:'POST',
            datatpe:'JSON',
            data:data,
            success:function(response){

            }
        })
    }

    $('#loginUsingLinkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        storeToSession();
        window.location.replace('/linkedinLoginFromViewEventDetailsPage');
    });
    $('#login-with-linkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        storeToSession();
        window.location.replace('/linkedinLoginFromViewEventDetailsPage');
    });
    $('#sign-up-with-linkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        storeToSession();
        window.location.replace('/linkedinLoginFromViewEventDetailsPage');
    });

    $('#loginUsingGoogle').on('click',function(){
        mixpanelTrack("Signup Google")

        storeToSession();
        window.location.replace('/getGoogleTokenEventDetailsPage');
    });
    $('#login-with-google').on('click',function(){
        mixpanelTrack("Signup Google")

        storeToSession();
        window.location.replace('/getGoogleTokenEventDetailsPage');
    });
    $('#sign-up-with-google').on('click',function(){
        mixpanelTrack("Signup Google")

        storeToSession();
        window.location.replace('/getGoogleTokenEventDetailsPage');
    });

    $("#shw").click(function(){
        $("#show-hide").slideToggle();
    });
    $('#loginFormButton').on('click',function(){
        var emailId1 = $("#emailId2").val();
        var password1 = $("#password").val();

        if (validateRequired(emailId1) && validateRequired(password1)) {
            emailId1 = emailId1.toLowerCase();
            var credentials={
                emailId:emailId1,
                password:password1
            }

            $.ajax({
                url:'/loginFromPublicProfile',
                type:'POST',
                datatype:'JSON',
                data:credentials,
                traditional:true,
                success:function(result){
                    if (result) {
                        getUserProfile();
                        stopNavigationFlag = false;
                        stopNavigation(false);
                        $("#eventsBody").slideToggle();
                        $("#lightBoxPopup").slideToggle();

                    }
                    else{
                        showMessagePopUp("Looks like either your email or password is incorrect. Please try again.",'error')
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            })
        }
        else{
            showMessagePopUp("Please enter your credentials",'error')
        }
    });

    function getEventDateFormat(date){
        var day = date.getDate();
        var format = getTimeFormatWithMeridian(date)+', '+day+''+getDayString(day)+' '+monthNameSmall[date.getMonth()]+' '+date.getFullYear();

        return format;
    }
    function getDayString(day) {
        if (day == 1 || day == 21 || day == 31) {
            return 'st';
        }
        else if (day == 2 || day == 22 || day == 23) {
            return 'nd';
        }
        else if (day == 3 || day == 23) {
            return 'rd';
        }
        else return 'th';
    };

    function getTimeFormatWithMeridian(date){
        var hrs = date.getHours();
        var min = date.getMinutes();
        var meridian;
        if(hrs > 12){
            hrs -= 12;
            meridian = 'PM';
        }
        else {
            if(hrs == 12){
                meridian = 'PM'
            }else
                meridian = 'AM';
        }
        if (min < 10) {
            min = "0"+min;
        };
        var time = hrs+":"+min+" "+meridian;
        return time;
    }

    function getDateStringToShow(date){
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var hours = date.getHours();
        var min = date.getMinutes();

        var dayString = getDayString(d);
        var data = monthNameFull[date.getMonth()]+' '+date.getDate()+' '+date.getFullYear();
        return data;
    }

    // Function to validate required field
    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }

    function showMessagePopUp(message,msgClass)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':'20%'})
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message)
        $("#closePopup-message").focus();
    }

    $("body").on("click","#closePopup-message",function(){

        $("#user-name").popover('destroy');
        if(deleteFlag){
          window.location.replace('/events');
        }
    });

    $("#lightBoxClose").on("click",function(){
        $("#eventsBody").slideToggle();
        $("#lightBoxPopup").slideToggle();
    })

    $("#signUp-Now").click(function(){
        $("#signUpNow-show-hide").slideToggle();
    });

    $("#sendUsingLinkedin").on("click",function(){
        $("#loginUsingLinkedin").trigger("click");
    });

    $("#userCreateAccount").on("click",function(){
        var emailId = $("#userEmailId").val();
        var firstName = $("#userFirstName").val();
        var lastName = $("#userLastName").val();
        if(validateRequired(emailId) && validateRequired(firstName) && validateRequired(lastName)){
            var user = {
                firstName:firstName,
                lastName:lastName,
                emailId:emailId
            }
            checkEmailAddress(user);
        }else{
            showMessagePopUp("Please fill all the fields.",'error');
        }
    });

    function checkEmailAddress(data){

        $.ajax({
            url:'/checkEmail/'+data.emailId,
            type:'GET',
            traditional:true,
            success:function(result){
                if(result == true){
                    createPartialAcc(data);
                }
                else{
                    showMessagePopUp("Email address already exist in the Relatas, Please change it.",'error');
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })

    }

    function createPartialAcc(user){
        $.ajax({
            url:'/createPartialAccount',
            type:'POST',
            datatype:'JSON',
            data:user,
            success:function(response){
                if(response){
                    storeToSession();
                    window.location.replace('/emailLoginViewEventDetailsPage');
                }else showMessagePopUp("failed",'error');
            }
        })
    }

    function stopNavigation(navigate){
        if(navigate){
            $("#goToDashboard").attr('href','#');
            $("#goToEditProfile").attr('href','#');
            $("#goToCalendar").attr('href','#');
            $("#goToContacts").attr('href','#');
            $("#goToDocuments").attr('href','#');
            $("#goHome").attr('href','#');
            $("#goToEvents").attr('href','#');
        }
        else{
            $("#goToDashboard").attr('href','/dashboard');
            $("#goToEditProfile").attr('href','/editProfile');
            $("#goToCalendar").attr('href','/calendar');
            $("#goToContacts").attr('href','/contacts');
            $("#goToDocuments").attr('href','/docs');
            $("#goToEvents").attr('href','/events');
            $("#goHome").attr('href','/');
        }
    }

    function msgSignIn(){
        if(stopNavigationFlag){
            showMessagePopUp("Please Sign in/ Sign up to navigate further.",'tip')
        }
    }

    $("#goHome").on('click',function(){
        msgSignIn()
    })
    $("#goToDashboard").on('click',function(){
        msgSignIn()
    })
    $("#goToEditProfile").on('click',function(){
        msgSignIn()
    })
    $("#goToCalendar").on('click',function(){
        msgSignIn()
    })
    $("#goToDocuments").on('click',function(){
        msgSignIn()
    })
    $("#goToContacts").on('click',function(){
        msgSignIn()
    })

    sidebarHeight()

    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }
    function mixpanelIncrement(eventName){
        mixpanel.people.increment(eventName);
    }
});