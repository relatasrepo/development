$(document).ready(function() {

    $.ajaxSetup({ cache: false });

    var eventDetails,table,timezoneProfile,event,actualEvent,timeZone,icsFile,eventP,domainName,AWScredentials,s3bucket,docUrl,popoverContent = null;
    var gotoCalendar = false, isAccepted = false,userAccepted = false, canceled = false, Mstatus = false,signUpFlag = false,popoverSource = null,slotStartDates = [];
    var participantsOptions = '',acceptedSlot = 'none';

    var windowUrl = ""+window.location;
    var pageUrl = windowUrl.split('/')[5];
    var params = getParams(window.location.href)

    $.ajax({
        url:'/getDomainName',
        type:'GET',
        success:function(domain){
            domainName = domain;

        },
        timeout: 20000
    });

    function getParams(url){
        var regex = /[?&]([^=#]+)=([^&#]*)/g,
            params = {},
            match;
        while(match = regex.exec(url)) {
            params[match[1]] = match[2];
        }
        return params;
    }

    function closePopOver() {
        if (popoverContent != null) {
            popoverContent.popover('destroy');
            popoverContent = null;
        }
    }

    switch(pageUrl){
        case 'linkedin':$("#relatas-tab-content").css({display:'none'});
            $("#relatas-icon-tab-item").removeClass('active');
            $("#linkedin-tab-content").css({display:'block'});
            $("#linkedin-icon-tab-item").addClass('active');

            break;
        case 'twitter':$("#relatas-tab-content").css({display:'none'});
            $("#relatas-icon-tab-item").removeClass('active');
            $("#twitter-tab-content").css({display:'block'});
            $("#twitter-icon-tab-item").addClass('active');

            break;
        case 'facebook':$("#relatas-tab-content").css({display:'none'});
            $("#relatas-icon-tab-item").removeClass('active');
            $("#fb-tab-content").css({display:'block'});
            $("#facebook-icon-tab-item").addClass('active');

            break;
        default:$("#relatas-tab-content").css({display:'block'});
            $("#relatas-tab-content").addClass('active');
            break;

    }

    getAWSCredentials();
    function getAWSCredentials() {
        $.ajax({
            url: '/getAwsCredentials',
            type: 'GET',
            datatye: 'JSON',
            success: function (credentials) {
                AWScredentials = credentials;
                AWS.config.update(credentials);

                AWS.config.region = credentials.region;

                s3bucket = new AWS.S3({ params: {Bucket: credentials.icsBucket} });
                docUrl = 'https://' + AWScredentials.icsBucket + '.s3.amazonaws.com/';

            },
            timeout: 20000
        })
    }
    var tz = jstz.determine();
    timeZone = tz.name();

    loadLoggedinUserProfile();
    function getRequestedMeeting(){
        $.ajax({
            url: '/getRequestedMeeting',
            type: 'GET',
            datatype: "json",
            traditional: true,
            success: function (invitation) {
                if(invitation == 'canceled'){
                    showMessagePopUp("Your meeting was canceled",'error');
                }else
                if(invitation == 'not authorised'){
                    showMessagePopUp("Your are not authorised to access this meeting. Please register/ login with the email id to which the meeting invitation was sent.",'error');
                }else
                if(invitation == 'loginRequired'){
                    $("#lightBoxPopup").slideToggle();
                }else
                if(!invitation){
                    showMessagePopUp("An error occurred while loading meeting details.",'error')
                }
                else{
                    if(invitation.deleted){
                        showMessagePopUp('This meeting no longer exists.','error');
                    }else{
                        eventP = invitation;
                        constructMeetingObject(invitation)
                        showParticipantsStatus(invitation)
                    }
                }
            },
            timeout: 20000
        })
    }

    function constructMeetingObject(invitation){
        participantsOptions = '';
        $(".todo-options").html('');
        actualEvent = invitation;
        var eventColor = '#d09054';
        var senderName = invitation.senderName;
        var allDays = false;
        var invitationId = invitation.invitationId;
        var senderId = invitation.senderId;
        var senderPicUrl = invitation.senderPicUrl;
        var timeSlots = invitation.scheduleTimeSlots;
        var senderEmailId = invitation.senderEmailId;
        var receiverEmailId = '';
        var receiverName = '';
        var receiverId = '';
        var exist = false;
        $(".todo-options").append('<option value='+invitation.senderId+'>'+invitation.senderName+'</option>')
        participantsOptions += '<option value='+invitation.senderId+'>'+invitation.senderName+'</option>'
        if(invitation.selfCalendar){
            for(var k=0; k<invitation.toList.length; k++){
                var rFName = invitation.toList[k].receiverFirstName || '';
                var rLName = invitation.toList[k].receiverLastName || '';
                receiverName = rFName+' '+rLName;
                if(validateRequired(invitation.toList[k].receiverFirstName) && validateRequired(invitation.toList[k].receiverId)){
                    $(".todo-options").append('<option value='+invitation.toList[k].receiverId+'>'+receiverName+'</option>')
                    participantsOptions += '<option value='+invitation.toList[k].receiverId+'>'+receiverName+'</option>'
                }
                if(invitation.toList[k].receiverId == loggedinUserProfile._id){

                    receiverEmailId = invitation.toList[k].receiverEmailId || '';
                    receiverId = invitation.toList[k].receiverId || '';
                    userAccepted = invitation.toList[k].isAccepted;
                    exist = true;
                    if(invitation.toList[k].canceled){
                        canceled = true;
                    }
                }

                if(exist) break;
            }

        }else{
            exist = true;
            receiverEmailId = invitation.to.receiverEmailId;
            receiverName = invitation.to.receiverName;
            receiverId = invitation.to.receiverId;
            if(invitation.to.receiverId == loggedinUserProfile._id && invitation.to.canceled){
                canceled = true;
            }
            $(".todo-options").append('<option value='+invitation.to.receiverId+'>'+invitation.to.receiverName+'</option>');
            participantsOptions += '<option value='+invitation.to.receiverId+'>'+invitation.to.receiverName+'</option>'

        }

        if(!exist){

            for(var l=0; l<invitation.toList.length; l++){
                if(invitation.toList[l].receiverId){
                    var rFName2 = invitation.toList[l].receiverFirstName || '';
                    var rLName2 = invitation.toList[l].receiverLastName || '';
                    receiverEmailId = invitation.toList[l].receiverEmailId || '';
                    receiverName = rFName2+' '+rLName2;
                    receiverId = invitation.toList[l].receiverId || '';
                    exist = true;
                }else{
                    var rFName3 = invitation.toList[0].receiverFirstName || '';
                    var rLName3 = invitation.toList[0].receiverLastName || '';
                    receiverEmailId = invitation.toList[0].receiverEmailId || '';
                    receiverName = rFName3+' '+rLName3;
                    receiverId = invitation.toList[0].receiverId || '';
                    exist = true;
                }
            }
        }

        Mstatus = invitation.readStatus;

        var documentName;
        var documentUrl;
        if(validateRequired(invitation.docs)){
            if(validateRequired(invitation.docs.documentName)){
                documentName = invitation.docs.documentName;
                documentUrl = invitation.docs.documentUrl;
            }
        }
        if(invitation.scheduleTimeSlots[0]){
            if(invitation.scheduleTimeSlots[0].isAccepted == true){
                acceptedSlot = 0;
                isAccepted = true;
                invitation.scheduleTimeSlots[1] = null;
                invitation.scheduleTimeSlots[2] = null;
            }
        }
        if(invitation.scheduleTimeSlots[1]){
            if(invitation.scheduleTimeSlots[1].isAccepted == true){
                acceptedSlot = 1;
                isAccepted = true;
                invitation.scheduleTimeSlots[0] = null;
                invitation.scheduleTimeSlots[2] = null;
            }
        }
        if(invitation.scheduleTimeSlots[2]){
            if(invitation.scheduleTimeSlots[2].isAccepted == true){
                acceptedSlot = 2;
                isAccepted = true;
                invitation.scheduleTimeSlots[1] = null;
                invitation.scheduleTimeSlots[0] = null;
            }
        }
        for(var j=0; j<invitation.scheduleTimeSlots.length; j++){
            if(validateRequired(invitation.scheduleTimeSlots[j])){
                slotStartDates.push(invitation.scheduleTimeSlots[j].start.date);
                event = {
                    id:invitationId,
                    title:'NEW REQUEST',
                    invitationTitle:invitation.scheduleTimeSlots[j].title ,
                    location:invitation.scheduleTimeSlots[j].location,
                    locationType:invitation.scheduleTimeSlots[j].locationType,
                    description:invitation.scheduleTimeSlots[j].description,
                    start:new Date(invitation.scheduleTimeSlots[j].start.date),
                    end: new Date(invitation.scheduleTimeSlots[j].end.date),
                    invitees:invitation.participants,
                    color:eventColor,
                    timeSlots:timeSlots,
                    isMeeting:true,
                    senderId:senderId,
                    name:senderName,
                    senderPicUrl:senderPicUrl,
                    senderEmailId:senderEmailId,
                    receiverId:receiverId,
                    receiverEmailId:receiverEmailId,
                    receiverName:receiverName,
                    document:invitation.docs,
                    icsFile:invitation.icsFile,
                    isSuggested:invitation.suggested,
                    selfCalendar:invitation.selfCalendar,
                    allDay:allDays
                }
            }
        }
        eventDetails = event;

        sederProfileFind()
        showInvitationDetails(event);
        showCancelAndDeleteButtons(eventP);
        if(eventP.selfCalendar && !userAccepted){
            showPastAndFutureMeetings();
        }else if(!isAccepted){
            showPastAndFutureMeetings();
        }
        /*if(loggedinUserProfile._id == eventP.senderId){
            bindToDoList(eventP.toDo,false);
        }
        else{
            bindToDoList(eventP.toDo,true);
        }*/
        bindToDoList(eventP.toDo,false);
        addData();
    }

    function showParticipantsStatus(invitation){

        var participantsHtml = '<tr>';
        var listFlag = true;
        if(invitation.suggested && invitation.suggestedBy){
            var isAcceptedCheck = false;
            var imageUrlNew = '/images/default.png';
            var uId = 'none'
            var nameNew= ''
            if(invitation.selfCalendar){

                if(invitation.toList.length > 1){
                    listFlag = false;
                    displayParticipantStatusWithToList(invitation.toList,participantsHtml);
                }else{
                    if(invitation.toList[0].isAccepted){
                        isAcceptedCheck = true;
                    }
                    if(invitation.senderId == invitation.suggestedBy.userId){

                        if(validateRequired(invitation.toList[0].receiverId)){
                            imageUrlNew = '/getImage/'+invitation.toList[0].receiverId
                            uId = invitation.toList[0].receiverId;
                        }


                        nameNew = invitation.toList[0].receiverFirstName;
                    }else{
                        nameNew = invitation.senderName;
                        imageUrlNew = '/getImage/'+invitation.senderId;
                        uId = invitation.senderId;
                    }
                }

            }else if(invitation.senderId == invitation.suggestedBy.userId){
                isAcceptedCheck = isAccepted;
                if(validateRequired(invitation.to.receiverId)){
                    imageUrlNew = '/getImage/'+invitation.to.receiverId;
                    uId = invitation.to.receiverId;
                }

                nameNew = invitation.to.receiverName;
            }else{
                isAcceptedCheck = isAccepted;
                nameNew = invitation.senderName;
                imageUrlNew = '/getImage/'+invitation.senderId;
                uId = invitation.senderId;
            }

            if(isAcceptedCheck && listFlag){
                participantsHtml += '<td width="40px" class="paddind4px"><img user='+uId+' class="accepted" src='+imageUrlNew+' title='+nameNew.replace(/\s/g, '&nbsp;')+'></td>';
            }else if(listFlag){
                participantsHtml += '<td width="40px" class="paddind4px"><img user='+uId+' class="pending" src='+imageUrlNew+' title='+nameNew.replace(/\s/g, '&nbsp;')+'></td>';
            }
            if(listFlag){
                participantsHtml += '</tr>'
                $("#participant-status").html(participantsHtml);
            }

        }else
        if(invitation.selfCalendar){
            var list = invitation.toList;
            displayParticipantStatusWithToList(list,participantsHtml);
        }else{
            var timeSlots = invitation.scheduleTimeSlots;
            var accepted = false;
            for(var j=0; j<timeSlots.length; j++){
                if(validateRequired(timeSlots[j])){
                    if(timeSlots[j].isAccepted){
                        accepted = true;
                    }
                }
            }
            var imageUrl2 = '/images/default.png'
            var nameName = '';
            var uId2 = 'none';
            if(validateRequired(invitation.to.receiverId)){
                imageUrl2 = '/getImage/'+invitation.to.receiverId
                uId2 = invitation.to.receiverId;
            }
            if(validateRequired(invitation.to.receiverName)){
                nameName = invitation.to.receiverName;
            }else{
                nameName = invitation.to.receiverEmailId;
            }

            if(accepted){
                participantsHtml += '<td width="40px" class="paddind4px"><img user='+uId2+' class="accepted" src='+imageUrl2+' title='+nameName.replace(/\s/g, '&nbsp;')+'></td>';
            }else
                participantsHtml += '<td width="40px" class="paddind4px"><img user='+uId2+' class="pending" src='+imageUrl2+' title='+nameName.replace(/\s/g, '&nbsp;')+'></td>';
            participantsHtml += '</tr>'
            $("#participant-status").html(participantsHtml);
        }

    }

    function displayParticipantStatusWithToList(list,participantsHtml){
        for(var i=0; i<list.length; i++){
            if(list[i]){
                var imageUrl = '/images/default.png';
                var name = '';
                var uId = 'none'
                if(validateRequired(list[i].receiverId)){
                    imageUrl = '/getImage/'+list[i].receiverId
                    uId = list[i].receiverId;
                }
                if(validateRequired(list[i].receiverFirstName) || validateRequired(list[i].receiverLastName)){
                    var name1 =  list[i].receiverFirstName || '';
                    var name2 = list[i].receiverLastName || '';
                    name = name1+' '+name2;
                }else{
                    name = list[i].receiverEmailId;
                }
                if(list[i].isAccepted){
                    participantsHtml += '<td width="40px" class="paddind4px"><img user='+uId+' class="accepted" src='+imageUrl+' title='+name.replace(/\s/g, '&nbsp;')+'></td>';
                }else
                    participantsHtml += '<td width="40px" class="paddind4px"><img user='+uId+' class="pending" src='+imageUrl+' title='+name.replace(/\s/g, '&nbsp;')+'></td>';
            }
        }
        participantsHtml += '</tr>'
        $("#participant-status").html(participantsHtml);
    }

    function showMessagePopUp(message,msgClass)
    {
        destroyActivePopup(msgClass != 'error');
        var html = _.template($("#message-popup").html())();
        $("#user-msg").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-msg").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':'20%','margin-left':'-15%','z-index':'10'});

        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message)

    }

    $("body").on("click","#closePopup-message",function(){
        $("#user-msg").popover('destroy');
        if(mFlag){
            window.location.reload();
        }
        if(gotoCalendar){
            window.location.replace('/calendar')
        }
    });

    /****************************************************************************/

    var loggedinUserProfile;
    var publicProfile;
    var newSuggestedLocation,comment;
    var dateShow1, dateShow2, dateShow3;
    var title,location,description;
    var mFlag = false;
    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var monthNameFull = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    var day = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];



// Function to load loggedin user profile
    function loadLoggedinUserProfile(signUp){
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (userData) {

                loggedinUserProfile = userData;
                if(validateRequired(loggedinUserProfile) && validateRequired(loggedinUserProfile.timezone) && validateRequired(loggedinUserProfile.timezone.name)){
                    timezoneProfile = loggedinUserProfile.timezone.name;
                }
                getRequestedMeeting();
                if (userData == null) {}else{
                    if(userData){
                        identifyMixPanelUser(userData,signUp);
                    }
                    var fName = userData.firstName || '';
                    var lName = userData.lastName || '';
                    $('.user-msg-name').text(userData.firstName || '');
                    $('#loggedin-user-name').text(fName+' '+lName);
                    $('#loggedin-user-designation').text(userData.designation || '');
                    $('#loggedin-user-companyName').text(userData.companyName || '');
                    if(validateRequired(userData.profilePicUrl)){
                        if( userData.profilePicUrl.charAt(0) == '/' || userData.profilePicUrl.charAt(0) == 'h'){

                            $('#profilePic').attr("src",userData.profilePicUrl);
                        }else $('#profilePic').attr("src","/"+userData.profilePicUrl);
                    }else $('#profilePic').attr("src",'/images/default.png');

                    imagesLoaded("#profilePic",function(instance,img) {
                        if(instance.hasAnyBroken){
                            $('#profilePic').attr("src","/images/default.png");
                        }
                    });

                }
            },
            error: function (event, request, settings) {
                showMessagePopUp("Error while requesting loggedin user data",'error')
            },
            timeout: 20000
        });
    }
    function identifyMixPanelUser(profile,signUp){

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page":window.location.href
        });
        if(signUp){
            mixpanelIncrement("#Login");
        }
        if(signUpFlag){
            signUpFlag = false;
            mixpanelTrack("Signup Relatas");
        }
    }
    function sederProfileFind(){
        if(loggedinUserProfile._id == eventP.senderId){

            var id;
            if(eventP.selfCalendar){
                for(var i=0; i<eventP.toList.length; i++){
                    if(validateRequired(eventP.toList[i].receiverId)){
                        id = eventP.toList[i].receiverId;
                    }
                }
            }else if(eventP.to && eventP.to.receiverId){
                id = eventP.to.receiverId;
            }

            if(validateRequired(id)){
                getSenderProfile(id,false);
            }else{
                showMessagePopUp("Invitation receiver is not registered user with Relatas.",'error')
                $('#sender-right-side-profilePic').attr('src','/images/default.png');
                $('#relatas-tab').addClass('invisible');
            }
        }
        else{

            getSenderProfile(eventP.senderId,false);
        }
    }

    $("body").on("click",".accepted, .pending",function(){
         var userId = $(this).attr('user');
        if(validateRequired(userId) && userId != 'none'){
            getSenderProfile(userId,true);
        }
    });

    function getValidUniqueUrl(uniqueName){
        var url = window.location+''.split('/');
        var patt = new RegExp(url[2]);
        if(patt.test(uniqueName)){

            url = uniqueName.split('/');
            for (var i = url.length - 1; i >= 0; i--) {
                if (url[i] == 'u') {
                    uniqueName = url[i+1];
                }
            }
            return uniqueName;

        }else {
            return uniqueName;
        }
    }

    // Function to get sender profile information
    /* Here i'm using ajax POST to send sender id (Sender is not logged in) */
    function getSenderProfile(senderId,nextUser){
        var user = {
            id:senderId
        };
        if(senderId == undefined || senderId == null || senderId == '') {
            showMessagePopUp("Invitation sender is not registered user with Relatas",'error')
            $('#sender-right-side-profilePic').attr('src','/images/default.png');
            $('#relatas-tab').addClass('invisible');
        }
        else{
            $.ajax({
                url:'/getSenderProfile',
                type:'POST',
                datatype:'JSON',
                data:user,
                success:function(profile){
                    if (!profile) {
                        showMessagePopUp("Error in getting invitation sender profile",'error');
                    }else{
                        if(!nextUser)
                             publicProfile = profile;

                        setTimeout(function(){
                            getCommonConnections(profile._id);
                            getLinkedinPofile(profile._id);
                            getTwitterInfo(profile._id);
                            getFacebookInfo(profile._id);
                        },1000)

                        $('#relatas-tab').removeClass('invisible');
                        $("#uniqueUrl").attr('href','/'+getValidUniqueUrl(profile.publicProfileUrl));

                        if( profile.profilePicUrl.charAt(0) == '/' || profile.profilePicUrl.charAt(0) == 'h'){

                            $('#sender-right-side-profilePic').attr("src",profile.profilePicUrl);
                        } else  $('#sender-right-side-profilePic').attr("src","/"+profile.profilePicUrl);
                        imagesLoaded("#sender-right-side-profilePic",function(instance,img) {
                            if(instance.hasAnyBroken){
                                $('#sender-right-side-profilePic').attr("src","/images/default.png");
                            }
                        });

                        $('#sender-right-side-name').text(profile.firstName+" "+profile.lastName);
                        $('#sender-right-side-designation').text(profile.designation);
                        $('#sender-right-side-companyName').text(profile.companyName);
                        $('#sender-right-side-location').text(getTextLength(profile.location ,25));
                        $('#sender-right-side-location').attr("title",profile.location)
                        $('#sender-right-side-emailId').text(getTextLength(profile.emailId,25));
                        $('#sender-right-side-emailId').attr("title",profile.emailId)
                        $('#sender-right-side-phoneNumber').text(profile.mobileNumber || '');
                        $('#sender-right-side-skypeId').text(getTextLength(profile.skypeId,25));
                        $('#sender-right-side-skypeId').attr("title",profile.skypeId)

                        if(validateRequired(profile.skypeId)){
                            $('#sender-right-side-skypeId').attr("href","skype:"+profile.skypeId+"?call")
                            $('#sender-right-side-skypeId').attr("target","_blank")
                        }
                    }
                },
                error: function (event, request, settings) {
                    showMessagePopUp("Error in getting invitation sender profile",'error');
                },
                timeout: 40000
            })
        }
    }

    function isSelfCalendarMeet(){
        if(event.selfCalendar){
            return true;
        }else return false;
    }
    function showInvitationDetails(event){
        $("#meetingMessages").html('');
        if(!isSelfCalendarMeet()){
            normalInvitationShowDetails(event);
        }else if(event.senderId != loggedinUserProfile._id){
            selfCalendarInvitationShowDetails(event)
        }else {
            normalInvitationShowDetails(event);
        }
    }

    function selfCalendarInvitationShowDetails(event){

        if(eventP.toList.length > 1){
            $('#time-slot-tr-1-suggest').addClass('invisible')
            $('#time-slot-tr-2-suggest').addClass('invisible')
            $('#time-slot-tr-3-suggest').addClass('invisible')
        }

        if(isAccepted == true && userAccepted){
            $('#time-slot-tr-1-confirm').addClass('invisible')
            $('#time-slot-tr-2-confirm').addClass('invisible')
            $('#time-slot-tr-3-confirm').addClass('invisible')
        }
        $('#confirmation-invitation-title').text(event.invitationTitle);
        $('#confirmation-invitation-description').text(event.description);
        showSuggestMessage()


        $('#confirmation-inviter-name').text(event.name);
        $('#confirmation-inviter-pic').attr('src',event.senderPicUrl);

        $('#showMeetingPlace').text(event.location);
        $('#confirmation-window').show();

        if (event.timeSlots[0]) {
            var start = new Date(event.timeSlots[0].start.date);
            var end = new Date(event.timeSlots[0].end.date)
            var nowDate = new Date()
            if(isAccepted == true && userAccepted){
                $('#time-slot-tr-1-confirm').addClass('invisible')
                if(nowDate < start){
                    //$('#time-slot-tr-1-suggest').removeClass('invisible')
                }
                else $('#time-slot-tr-1-suggest').addClass('invisible')
            }
            else{

                if(nowDate < start){
                    // $('#time-slot-tr-1-suggest').removeClass('invisible')
                    $('#time-slot-tr-1-confirm').removeClass('invisible')
                }
                else{
                    // $('#time-slot-tr-1-suggest').removeClass('invisible')
                    $('#time-slot-tr-1-confirm').addClass('invisible')
                }
            }

            var zone = '('+getTimeZone()+')'
            $('#time-slot-tr-1-date').text(getDayFormat(start));
            $('#time-slot-tr-1-time').text(getTimeFormatWithMeridian(start)+' - '+getTimeFormatWithMeridian(end)+' '+zone);
            $('#time-slot-tr-1-locationType').text(event.timeSlots[0].locationType);
            if(event.isSuggested){
                $('#time-slot-tr-1-location').text(event.timeSlots[0].suggestedLocation);
            }
            else $('#time-slot-tr-1-location').text(event.timeSlots[0].location);

            $('#time-slot-tr-1-confirm').attr('invitationId',event.id);
            $('#time-slot-tr-1-confirm').attr('slotId',event.timeSlots[0]._id);
            $('#time-slot-tr-1').css({display:'table-row'});


        }else $('#time-slot-tr-1').css({display:'none'});

        if (event.timeSlots[1]) {
            var start = new Date(event.timeSlots[1].start.date);
            var end = new Date(event.timeSlots[1].end.date)
            var nowDate = new Date()
            if(isAccepted == true && userAccepted){
                $('#time-slot-tr-2-confirm').addClass('invisible')
                if(nowDate < start){
                    // $('#time-slot-tr-2-suggest').removeClass('invisible')
                }
                else $('#time-slot-tr-2-suggest').addClass('invisible')
            }
            else{

                if(nowDate < start){
                    // $('#time-slot-tr-2-suggest').removeClass('invisible')
                    $('#time-slot-tr-2-confirm').removeClass('invisible')
                }
                else{
                    //$('#time-slot-tr-2-suggest').removeClass('invisible')
                    $('#time-slot-tr-2-confirm').addClass('invisible')
                }
            }
            var zone = '('+getTimeZone()+')'
            $('#time-slot-tr-2-date').text(getDayFormat(start));
            $('#time-slot-tr-2-time').text(getTimeFormatWithMeridian(start)+' - '+getTimeFormatWithMeridian(end)+' '+zone);
            $('#time-slot-tr-2-locationType').text(event.timeSlots[1].locationType);
            $('#time-slot-tr-2-location').text(event.timeSlots[1].location);
            $('#time-slot-tr-2-confirm').attr('invitationId',event.id);
            $('#time-slot-tr-2-confirm').attr('slotId',event.timeSlots[1]._id);
            $('#time-slot-tr-2').css({display:'table-row'});
        }else $('#time-slot-tr-2').css({display:'none'});


        if (event.timeSlots[2]) {
            var start = new Date(event.timeSlots[2].start.date);
            var end = new Date(event.timeSlots[2].end.date)
            var nowDate = new Date()
            if(isAccepted == true && userAccepted){
                $('#time-slot-tr-3-confirm').addClass('invisible')
                if(nowDate < start){
                    // $('#time-slot-tr-3-suggest').removeClass('invisible')
                }
                else $('#time-slot-tr-3-suggest').addClass('invisible')
            }
            else{

                if(nowDate < start){
                    // $('#time-slot-tr-3-suggest').removeClass('invisible')
                    $('#time-slot-tr-3-confirm').removeClass('invisible')
                }
                else{
                    // $('#time-slot-tr-3-suggest').removeClass('invisible')
                    $('#time-slot-tr-3-confirm').addClass('invisible')
                }
            }
            var zone = '('+getTimeZone()+')'
            $('#time-slot-tr-3-date').text(getDayFormat(start));
            $('#time-slot-tr-3-time').text(getTimeFormatWithMeridian(start)+' - '+getTimeFormatWithMeridian(end)+'  '+zone);
            $('#time-slot-tr-3-locationType').text(event.timeSlots[2].locationType);
            $('#time-slot-tr-3-location').text(event.timeSlots[2].location);
            $('#time-slot-tr-3-confirm').attr('invitationId',event.id);
            $('#time-slot-tr-3-confirm').attr('slotId',event.timeSlots[2]._id);
            $('#time-slot-tr-3').css({display:'table-row'});
        }else  $('#time-slot-tr-3').css({display:'none'});
        showMeetingDocInfo(event)
        showHideConfirmButtons();
    }

    function normalInvitationShowDetails(event){
        if(isAccepted == true){
            $('#time-slot-tr-1-confirm').addClass('invisible')
            $('#time-slot-tr-1-suggest').addClass('invisible')
            $('#time-slot-tr-2-confirm').addClass('invisible')
            $('#time-slot-tr-2-suggest').addClass('invisible')
            $('#time-slot-tr-3-confirm').addClass('invisible')
            $('#time-slot-tr-3-suggest').addClass('invisible')

        }
        $('#confirmation-invitation-title').text(event.invitationTitle);
        $('#confirmation-invitation-description').text(event.description);

        showSuggestMessage()

        $('#confirmation-inviter-name').text(event.name);
        $('#confirmation-inviter-pic').attr('src',event.senderPicUrl);

        $('#showMeetingPlace').text(event.location);
        $('#confirmation-window').show();

        if (event.timeSlots[0]) {
            var start = new Date(event.timeSlots[0].start.date);
            var end = new Date(event.timeSlots[0].end.date)
            var nowDate = new Date()
            if(isAccepted == true){
                $('#time-slot-tr-1-confirm').addClass('invisible')

                if(nowDate < start){
                    $('#time-slot-tr-1-suggest').removeClass('invisible')
                }
                else $('#time-slot-tr-1-suggest').addClass('invisible')
            }
            else{
                if(nowDate < start){
                    $('#time-slot-tr-1-suggest').removeClass('invisible')
                    $('#time-slot-tr-1-confirm').removeClass('invisible')
                    // cancel button
                    $('#time-slot-tr-1-cancel').removeClass('invisible')
                }
                else{
                    $('#time-slot-tr-1-suggest').removeClass('invisible')
                    $('#time-slot-tr-1-confirm').addClass('invisible')
                    $('#time-slot-tr-1-cancel').removeClass('invisible')
                }
            }

            var zone = '('+getTimeZone()+')'
            $('#time-slot-tr-1-date').text(getDayFormat(start));
            $('#time-slot-tr-1-time').text(getTimeFormatWithMeridian(start)+' - '+getTimeFormatWithMeridian(end)+' '+zone);
            $('#time-slot-tr-1-locationType').text(event.timeSlots[0].locationType);
            if(event.isSuggested){
                $('#time-slot-tr-1-location').text(validateRequired(event.timeSlots[0].suggestedLocation) ? event.timeSlots[0].suggestedLocation : event.timeSlots[0].location);
            }
            else $('#time-slot-tr-1-location').text(event.timeSlots[0].location);

            $('#time-slot-tr-1-confirm').attr('invitationId',event.id);
            $('#time-slot-tr-1-cancel').attr('invitationId',event.id);
            $('#time-slot-tr-1-confirm').attr('slotId',event.timeSlots[0]._id);
            $('#time-slot-tr-1').css({display:'table-row'});

        }else $('#time-slot-tr-1').css({display:'none'});

        if (event.timeSlots[1]) {

            var start = new Date(event.timeSlots[1].start.date);
            var end = new Date(event.timeSlots[1].end.date)
            var nowDate = new Date()
            if(isAccepted == true){
                $('#time-slot-tr-2-confirm').addClass('invisible')

                if(nowDate < start){
                    $('#time-slot-tr-2-suggest').removeClass('invisible')
                }
                else $('#time-slot-tr-2-suggest').addClass('invisible')
            }
            else{

                if(nowDate < start){
                    $('#time-slot-tr-2-suggest').removeClass('invisible')
                    $('#time-slot-tr-2-confirm').removeClass('invisible')
                    $('#time-slot-tr-2-cancel').removeClass('invisible')
                }
                else{
                    $('#time-slot-tr-2-suggest').removeClass('invisible')
                    $('#time-slot-tr-2-confirm').addClass('invisible')
                    $('#time-slot-tr-2-cancel').removeClass('invisible')
                }
            }
            var zone = '('+getTimeZone()+')'
            $('#time-slot-tr-2-date').text(getDayFormat(start));
            $('#time-slot-tr-2-time').text(getTimeFormatWithMeridian(start)+' - '+getTimeFormatWithMeridian(end)+' '+zone);
            $('#time-slot-tr-2-locationType').text(event.timeSlots[1].locationType);
            $('#time-slot-tr-2-location').text(event.timeSlots[1].location);
            $('#time-slot-tr-2-confirm').attr('invitationId',event.id);
            $('#time-slot-tr-2-cancel').attr('invitationId',event.id);
            $('#time-slot-tr-2-confirm').attr('slotId',event.timeSlots[1]._id);
            $('#time-slot-tr-2').css({display:'table-row'});
        }else $('#time-slot-tr-2').css({display:'none'});

        if (event.timeSlots[2]) {

            var start = new Date(event.timeSlots[2].start.date);
            var end = new Date(event.timeSlots[2].end.date)
            var nowDate = new Date()
            if(isAccepted == true){
                $('#time-slot-tr-3-confirm').addClass('invisible')

                if(nowDate < start){
                    $('#time-slot-tr-3-suggest').removeClass('invisible')
                }
                else $('#time-slot-tr-3-suggest').addClass('invisible')
            }
            else{

                if(nowDate < start){
                    $('#time-slot-tr-3-suggest').removeClass('invisible')
                    $('#time-slot-tr-3-confirm').removeClass('invisible')
                    $('#time-slot-tr-3-cancel').removeClass('invisible')
                }
                else{
                    $('#time-slot-tr-3-suggest').removeClass('invisible')
                    $('#time-slot-tr-3-confirm').addClass('invisible')
                    $('#time-slot-tr-3-cancel').removeClass('invisible')
                }
            }
            var zone = '('+getTimeZone()+')'
            $('#time-slot-tr-3-date').text(getDayFormat(start));
            $('#time-slot-tr-3-time').text(getTimeFormatWithMeridian(start)+' - '+getTimeFormatWithMeridian(end)+'  '+zone);
            $('#time-slot-tr-3-locationType').text(event.timeSlots[2].locationType);
            $('#time-slot-tr-3-location').text(event.timeSlots[2].location);
            $('#time-slot-tr-3-confirm').attr('invitationId',event.id);
            $('#time-slot-tr-3-cancel').attr('invitationId',event.id);
            $('#time-slot-tr-3-confirm').attr('slotId',event.timeSlots[2]._id);
            $('#time-slot-tr-3').css({display:'table-row'});
        }else  $('#time-slot-tr-3').css({display:'none'});
        showMeetingDocInfo(event)

        if(Mstatus){
            $('#time-slot-tr-1-confirm').addClass('invisible')
            //$('#time-slot-tr-1-suggest').addClass('invisible')
            $('#time-slot-tr-2-confirm').addClass('invisible')
            // $('#time-slot-tr-2-suggest').addClass('invisible')
            $('#time-slot-tr-3-confirm').addClass('invisible')
            // $('#time-slot-tr-3-suggest').addClass('invisible')
        }
        showHideConfirmButtons();
    }

    function showSuggestMessage(){
        if(eventP.suggested && !isAccepted){
            $("#suggestMsg").show()
            if(eventP.suggestedBy && eventP.suggestedBy.userId == eventP.senderId){
                $("#suggestMsg").text(eventP.senderName+' has suggested new time/ location as indicated below. Please confirm.')
            }else{
                if(isSelfCalendarMeet()){
                    var rFName2 = eventP.toList[0].receiverFirstName || '';
                    var rLName2 = eventP.toList[0].receiverLastName || '';
                    var name = rFName2+' '+rLName2;
                    $("#suggestMsg").text(name+' has suggested new time/ location as indicated below. Please confirm.')
                }else{
                    $("#suggestMsg").text(eventP.to.receiverName+' has suggested new time/ location as indicated below. Please confirm.')
                }
            }
        }
    }

    function showHideConfirmButtons(){
        if(eventP.suggested){
            if(eventP.suggested && !isAccepted){
                if(eventP.suggestedBy && eventP.suggestedBy.userId == loggedinUserProfile._id){
                    hideAllButtons();
                }
                else{
                    // cancel buttons
                    /*$('#time-slot-tr-1-cancel').removeClass('invisible')
                     $('#time-slot-tr-2-cancel').removeClass('invisible')
                     $('#time-slot-tr-3-cancel').removeClass('invisible')*/
                }
            }else if(eventP.suggested && isAccepted){
                // cancel buttons
                /*$('#time-slot-tr-1-cancel').addClass('invisible')
                 $('#time-slot-tr-2-cancel').addClass('invisible')
                 $('#time-slot-tr-3-cancel').addClass('invisible')*/

                if(eventP.suggestedBy && eventP.suggestedBy.userId == loggedinUserProfile._id){
                    $('#time-slot-tr-1-suggest').removeClass('invisible')
                    $('#time-slot-tr-2-suggest').removeClass('invisible')
                    $('#time-slot-tr-3-suggest').removeClass('invisible')

                    if(isSelfCalendarMeet()){
                        if(eventP.toList.length > 1){
                            $('#time-slot-tr-1-suggest').addClass('invisible')
                            $('#time-slot-tr-2-suggest').addClass('invisible')
                            $('#time-slot-tr-3-suggest').addClass('invisible')
                        }
                    }
                }else{
                    if(isSelfCalendarMeet()){
                        if(eventP.toList.length > 1){
                            $('#time-slot-tr-1-suggest').addClass('invisible')
                            $('#time-slot-tr-2-suggest').addClass('invisible')
                            $('#time-slot-tr-3-suggest').addClass('invisible')
                        }
                    }
                }
            }
        }else{
            if(isSameUser()){
                $('#time-slot-tr-1-confirm').addClass('invisible')
                $('#time-slot-tr-2-confirm').addClass('invisible')
                $('#time-slot-tr-3-confirm').addClass('invisible')

                if(isSelfCalendarMeet()){
                    if(eventP.toList.length > 1 && !isAccepted){
                        $('#time-slot-tr-1-suggest').removeClass('invisible')
                        $('#time-slot-tr-2-suggest').removeClass('invisible')
                        $('#time-slot-tr-3-suggest').removeClass('invisible')
                    }
                    else
                    {
                        $('#time-slot-tr-1-suggest').addClass('invisible')
                        $('#time-slot-tr-2-suggest').addClass('invisible')
                        $('#time-slot-tr-3-suggest').addClass('invisible')
                    }
                }
            }
            /*if(isAccepted){
             // cancel buttons
             $('#time-slot-tr-1-cancel').addClass('invisible')
             $('#time-slot-tr-2-cancel').addClass('invisible')
             $('#time-slot-tr-3-cancel').addClass('invisible')
             }
             else{
             if(isSameUser()){
             // cancel buttons
             $('#time-slot-tr-1-cancel').addClass('invisible')
             $('#time-slot-tr-2-cancel').addClass('invisible')
             $('#time-slot-tr-3-cancel').addClass('invisible')
             }
             else{
             // cancel buttons
             $('#time-slot-tr-1-cancel').removeClass('invisible')
             $('#time-slot-tr-2-cancel').removeClass('invisible')
             $('#time-slot-tr-3-cancel').removeClass('invisible')
             }
             }*/
        }
        getAcceptMeetingStatus();
    }

    function hideAllButtons(){
        $('#time-slot-tr-1-confirm').addClass('invisible')
        $('#time-slot-tr-2-confirm').addClass('invisible')
        $('#time-slot-tr-3-confirm').addClass('invisible')
        $('#time-slot-tr-1-cancel').addClass('invisible')
        $('#time-slot-tr-2-cancel').addClass('invisible')
        $('#time-slot-tr-3-cancel').addClass('invisible')
        /* $('#time-slot-tr-1-suggest').addClass('invisible')
         $('#time-slot-tr-2-suggest').addClass('invisible')
         $('#time-slot-tr-3-suggest').addClass('invisible')*/
    }

    function showMeetingDocInfo(event){
        $('#documentInfo tbody').html('');
        $('#related-documents').css({display:'none'});
        if(validateRequired(event.document[0])){

            if(validateRequired(event.document[0].documentName)){

                var documentHtml = '';
                for(var i=0;i<event.document.length;i++){
                    var docUrl = '/readDocument/'+ event.document[0].documentId;
                    documentHtml += '<tr><td>';
                    documentHtml += '<a class="meeting-attachment1" href='+docUrl+' id="docUrl">';
                    documentHtml += '<img class="meeting-attachment-icon" src="/images/icon_file_pdf.png">';
                    documentHtml += '<span class="meeting-attachment-title1">'+event.document[i].documentName+'</span></a></td>';
                    documentHtml += '<td><div class="conf-doc-detail"> <span>Uploaded by :'+event.name+' </span></div></td>';
                    documentHtml += '<td><div class="conf-doc-detail"><span>File expires on none</span></div></td></tr>';
                }
                $('#documentInfo tbody').html(documentHtml);
                $('#related-documents').css({display:'block'});
            }
        }
        var icsFlag = true;
        if(isSelfCalendarMeet()){
            if(userAccepted || eventP.toList.length == 1 ? isAccepted : false){
                icsFlag = true;
            }else{
                icsFlag = false;
            }
        }else{
            if(!isAccepted){
                icsFlag = false;
            }
        }
        if(icsFlag){
            if(validateRequired(event.icsFile)){
                var documentHtmlIcs = '';
                if(validateRequired(event.icsFile.url)){
                    documentHtmlIcs += '<tr><td>';
                    documentHtmlIcs += '<a target="_blank" class="meeting-attachment1" href='+event.icsFile.url+' >';
                    documentHtmlIcs += '<img style="width: 30px" class="meeting-attachment-icon" src="/images/ics.ico">';
                    documentHtmlIcs += '<span class="meeting-attachment-title1">Download now (.ics)</span></a></td></tr>';
                }
                $('#icsInfo tbody').html(documentHtmlIcs);
                $('#related-ics-file').css({display:'block'});
            }
        }
        var messageArray = [];
        var userIdArr = [];
        for(var slot=0; slot<event.timeSlots.length; slot++){
            if(event.timeSlots[slot]){
                if(event.timeSlots[slot].isAccepted){
                    if(validateRequired(event.timeSlots[slot].message)){
                        var msgTop = $.nl2br(event.timeSlots[slot].message);
                        $("#meetingMessages").append('<p style="font-size: initial;">'+msgTop+'</p><br>');
                    }
                    if(validateRequired(event.timeSlots[slot].messages)){
                        if(event.timeSlots[slot].messages.length > 0){
                            for(var j=0; j<event.timeSlots[slot].messages.length; j++){
                                var msgBelow = $.nl2br(event.timeSlots[slot].messages[j].message);
                                var date = new Date(event.timeSlots[slot].messages[j].messageDate);
                                var dateShow = date.getDate()+'-'+monthNameSmall[date.getMonth()]+'-'+date.getFullYear();
                                //getUserBasicInfo(msgBelow,dateShow,event.timeSlots[slot].messages[j].userId);
                                var obj = {
                                    msgBelow:msgBelow,
                                    dateShow:dateShow,
                                    date:date,
                                    userId:event.timeSlots[slot].messages[j].userId
                                };
                                userIdArr.push(event.timeSlots[slot].messages[j].userId)
                                messageArray.push(obj);
                            }
                        }
                    }
                }
            }
        }
        if(validateRequired(eventP.comments) && eventP.comments.length > 0){
            for(var k=0; k<eventP.comments.length; k++){
                var msgBelowNew = $.nl2br(eventP.comments[k].message);
                var dateNew = new Date(eventP.comments[k].messageDate);
                var dateShowNew = dateNew.getDate()+'-'+monthNameSmall[dateNew.getMonth()]+'-'+dateNew.getFullYear();
                //getUserBasicInfo(msgBelowNew,dateShowNew,eventP.comments[k].userId);
                var obj2 = {
                    msgBelow:msgBelowNew,
                    dateShow:dateShowNew,
                    date:dateNew,
                    userId:eventP.comments[k].userId
                };
                userIdArr.push(eventP.comments[k].userId)
                messageArray.push(obj2);
            }
        }
        if(messageArray.length > 0){
            messageArray.sort(function (o1, o2) {
                return o1.date < o2.date ? 1 : o1.date > o2.date ? -1 : 0;
            });
            getUserList(userIdArr,messageArray)
        }
    }

    function getUserList(idArr,messageArray){
        idArr = removeDuplicates_id(idArr,true);
        idArr = {data:JSON.stringify(idArr)};
        $.ajax({
            url:'/userBasicInfoByArrayOfIds',
            type:'POST',
            data:idArr,
            success:function(list){
                if(validateRequired(list) && list.length > 0){
                    for(var m=0; m<messageArray.length; m++){
                        for(var u=0; u<list.length; u++){
                            if(list[u]._id == messageArray[m].userId){
                                getUserBasicInfo(messageArray[m].msgBelow,messageArray[m].dateShow,list[u]);
                            }
                        }
                    }
                }
            }
        })
    }

    function getUserBasicInfo(message,date,profile){
        if(validateRequired(profile)){
            var msg = date+': '+profile.firstName+' '+profile.lastName
            var msgBelow = '<b>'+msg+'</b><br>'+message
            $("#meetingMessages").append('<p style="font-size: initial;">'+msgBelow+'</p><br>');
        }else{
            var  msgBelowNew = date+': '+message;
            $("#meetingMessages").append('<p style="font-size: initial;">'+msgBelowNew+'</p><br>');
        }

    }
    function removeDuplicates_id(arr,notId) {

        var end = arr.length;

        for (var i = 0; i < end; i++) {
            for (var j = i + 1; j < end; j++) {
                if (!notId ? arr[i]._id == arr[j]._id : arr[i] == arr[j]) {
                    var shiftLeft = j;
                    for (var k = j + 1; k < end; k++, shiftLeft++) {
                        arr[shiftLeft] = arr[k];
                    }
                    end--;
                    j--;
                }
            }
        }

        var whitelist = [];
        for (var l = 0; l < end; l++) {
            whitelist[l] = arr[l];
        }

        return whitelist;
    }
    function showPastAndFutureMeetings(){

        if(eventP.scheduleTimeSlots.length > 0){
            var obj = {
                dates:JSON.stringify(slotStartDates),
                invitationId:eventP.invitationId
            };

            $.ajax({
                url:'/meeting/getDatesBetween/currentInputDay',
                type:'POST',
                data:obj,
                datatype:'JSON',
                success:function(obj){

                    if(obj != false && validateRequired(obj)){
                        if(obj.events.length > 0){
                            for(var event=0; event<obj.events.length; event++){
                                var meeting = {
                                    invitationId:obj.events[event]._id,
                                    scheduleTimeSlots:[
                                        {
                                            start: {
                                                date: obj.events[event].startDateTime
                                            },
                                            end: {
                                                date: obj.events[event].endDateTime
                                            },
                                            title: obj.events[event].eventName,
                                            location: obj.events[event].locationName,
                                            locationType: 'Event',
                                            description: obj.events[event].eventDescription
                                        }
                                    ]
                                };
                                obj.meetings.push(meeting);
                            }
                        }

                        if(validateRequired(obj.googleEvents) && obj.googleEvents.length > 0){
                            for(var google=0; google<obj.googleEvents.length; google++){
                                var googleEvent = {
                                    invitationId:obj.googleEvents[google]._id,
                                    scheduleTimeSlots:[
                                        {
                                            start: {
                                                date: obj.googleEvents[google].interactionDate
                                            },
                                            end: {
                                                date: obj.googleEvents[google].endDate
                                            },
                                            title: obj.googleEvents[google].title || '',
                                            location: '',
                                            locationType: 'Google',
                                            description: obj.googleEvents[google].title || ''
                                        }
                                    ]
                                };
                                obj.meetings.push(googleEvent);
                            }
                        }

                        if(obj.meetings.length > 0){
                            displayBeforeAndAfterMeetingsOfTimeSlot(obj.meetings);
                        }
                    }
                }
            });
        }
    }

    function getBeforeAfterSlotsSeparate(slotArr){
        var separateArr = [];
        var beforeArr = [];
        var afterArr = [];
        for(var i=0; i<slotArr.length; i++){
            if(slotArr[i].occurrence == 'before'){
                beforeArr.push(slotArr[i]);
            }else{
                afterArr.push(slotArr[i]);
            }
        }
        separateArr.push(beforeArr,afterArr);
        return separateArr;
    }

    function displayBeforeAndAfterMeetingsOfTimeSlot(meetings){
        for(var i=0; i<eventP.scheduleTimeSlots.length; i++){
            if(eventP.scheduleTimeSlots[i] != null){
                var sameDaySlots = getBeforeOrAfterMeeting(eventP.scheduleTimeSlots[i],meetings);

                if(sameDaySlots.length > 0){
                    var separated = getBeforeAfterSlotsSeparate(sameDaySlots);

                    if(separated.length > 0){
                        for(var j=0; j<separated.length; j++){
                            if(separated[j].length > 0){
                                var maxSlot;
                                maxSlot = separated[j].reduce(function (a, b) {
                                    if(a.occurrence == 'before')
                                        return new Date(a.slot.start.date) > new Date(b.slot.start.date) ? a : b;
                                    else
                                        return new Date(a.slot.start.date) < new Date(b.slot.start.date) ? a : b;
                                });

                                appendBeforeAfterMeetingsToUi(maxSlot,i+1);
                            }
                        }
                    }
                }
            }
        }
    }

    function getBeforeOrAfterMeeting(slot,meetings){
        var sameDaySlots = [];
        for(var i=0; i<meetings.length; i++){

            var date1 = new Date(slot.start.date);

            for(var j=0; j<meetings[i].scheduleTimeSlots.length;j++){
                if(meetings[i].scheduleTimeSlots[j] != null){
                    var date2 = new Date(meetings[i].scheduleTimeSlots[j].start.date);
                    if(isSameDay(date1,date2)){
                        if(!meetings[i].readStatus){
                            var obj = {
                                id:meetings[i].invitationId,
                                occurrence:date1 > date2 ? 'before' : 'after',
                                slot:meetings[i].scheduleTimeSlots[j]
                            }

                            sameDaySlots.push(obj);
                        }else if(meetings[i].scheduleTimeSlots[j].isAccepted){
                            var obj = {
                                id:meetings[i].invitationId,
                                occurrence:date1 > date2 ? 'before' : 'after',
                                slot:meetings[i].scheduleTimeSlots[j]
                            }

                            sameDaySlots.push(obj);
                        }

                    }
                }
            }
        }
        return sameDaySlots;
    }

    function isSameDay(date1,date2){
        return date1.getDate()+'-'+date1.getMonth()+'-'+date1.getFullYear() == date2.getDate()+'-'+date2.getMonth()+'-'+date2.getFullYear();
    }

    function appendBeforeAfterMeetingsToUi(slot,slotNum){

        var date = getTimeFormatWithMeridian(new Date(slot.slot.start.date))+' - '+getTimeFormatWithMeridian(new Date(slot.slot.end.date));
        var text = slot.slot.title;
        var otherOccurrence = slot.occurrence == 'before' ? 'after' : 'before';
        var message = otherOccurrence == 'before' ? 'No Past Meeting' : 'No Future Meeting';
        $("#table-before-after-"+slotNum).show();
        $("#table-"+slot.occurrence+"-image-"+slotNum).show();
        $("#table-"+slot.occurrence+"-image-"+slotNum).attr("src",getLocationTypeImage(slot.slot.locationType));
        if(slot.slot.locationType == 'Google' || slot.slot.locationType == 'Event'){
            $("#table-"+slot.occurrence+"-image-"+slotNum).css({'margin-top':'-1%'});
        }
        $("#table-"+slot.occurrence+"-details-"+slotNum).html(date+': &nbsp'+getTextLength(text,20));
        $("#table-"+slot.occurrence+"-details-"+slotNum).attr("title",text);

        if($("#table-"+otherOccurrence+"-details-"+slotNum).text() == ''){
            $("#table-"+otherOccurrence+"-image-"+slotNum).hide();
            $("#table-"+otherOccurrence+"-details-"+slotNum).text(message);
        }
    }
    function getLocationTypeImage(locationType){
        var image = '';
        switch (locationType) {
            case 'In-Person': image = "/images/icon_location_small.png";
                break;
            case 'Phone':image = "/images/icon_phone.png";
                break;
            case 'Skype':image = "/images/icon_skype.png";
                break;
            case 'Conference Call':image = "/images/conference_call.png";
                break;
            case 'Event':image = "/images/event_new.png";
                break;
            case 'Google':image = "/images/social_g+.png";
                break;
        }
        return image;
    }
    var pleaseWait;
    $('#time-slot-tr-1-confirm').on('click',function(){
        if(!isSameUser() || (eventP.suggested && !isAccepted)){
            var start = new Date(eventDetails.timeSlots[0].start.date)
            var end = new Date(eventDetails.timeSlots[0].end.date)
            if(isPastMeeting(start)){
                pleaseWait = $(this);
                eventDetails.invitationTitle = eventDetails.timeSlots[0].title;
                eventDetails.location = eventDetails.timeSlots[0].location;
                eventDetails.locationType = eventDetails.timeSlots[0].locationType;
                eventDetails.description = eventDetails.timeSlots[0].description;
                showConfirmPopup(eventDetails,start,end,0,$(this));
            }
            else showMessagePopUp("Meeting time is over, Please suggest new time/ location.",'error')
        }
        else showMessagePopUp("You are the meeting sender you can not accept this meeting.",'error')

    });
    $('#time-slot-tr-2-confirm').on('click',function(){
        if(!isSameUser() || (eventP.suggested && !isAccepted)){
            var start = new Date(eventDetails.timeSlots[1].start.date)
            var end = new Date(eventDetails.timeSlots[1].end.date)
            if(isPastMeeting(start)){
                pleaseWait = $(this);
                eventDetails.invitationTitle = eventDetails.timeSlots[1].title;
                eventDetails.location = eventDetails.timeSlots[1].location;
                eventDetails.locationType = eventDetails.timeSlots[1].locationType;
                eventDetails.description = eventDetails.timeSlots[1].description;
                showConfirmPopup(eventDetails,start,end,1,$(this));
            }
            else showMessagePopUp("Meeting time is over, Please suggest new time/ location.",'error')
        }
        else showMessagePopUp("You are the meeting sender you can not accept this meeting.",'error')
    });
    $('#time-slot-tr-3-confirm').on('click',function(){
        if(!isSameUser() || (eventP.suggested && !isAccepted)){
            var start = new Date(eventDetails.timeSlots[2].start.date)
            var end = new Date(eventDetails.timeSlots[2].end.date)
            if(isPastMeeting(start)){
                pleaseWait = $(this);
                eventDetails.invitationTitle = eventDetails.timeSlots[2].title;
                eventDetails.location = eventDetails.timeSlots[2].location;
                eventDetails.locationType = eventDetails.timeSlots[2].locationType;
                eventDetails.description = eventDetails.timeSlots[2].description;
                showConfirmPopup(eventDetails,start,end,2,$(this));
            }
            else showMessagePopUp("Meeting time is over, Please suggest new time/ location.",'error')
        }
        else showMessagePopUp("You are the meeting sender you can not accept this meeting.",'error')
    });

    function showConfirmPopup(eventDetails,start,end,slotNumber,element){
        destroyActivePopup(true);
        popoverSource = element;
        var html = _.template($("#confirm-popup").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover",
            trigger: 'focus'
        });

        element.popover('show');
        $("#confirmWithMessage").attr("slotNumber",slotNumber);
        $("#confirmWithoutMessage").attr("slotNumber",slotNumber);
    }

    $("body").on("click","#confirmWithMessage",function(){
        var slotNumber = $(this).attr("slotNumber");
        var message = $("#confirmMessage").val().trim();
        var start = new Date(eventDetails.timeSlots[slotNumber].start.date)
        var end = new Date(eventDetails.timeSlots[slotNumber].end.date)
        if(validateRequired(message)){
            message = $.nl2br(message)
            destroyActivePopup(true);
            changeStateOfConfirmButtons(true)
            createEvent(eventDetails,start,end,slotNumber,message);
        }
        else showMessagePopUp("Please Enter message to send.",'error');
    });

    $("body").on("click","#confirmWithoutMessage",function(){
        var slotNumber = $(this).attr("slotNumber");
        var message = '';
        var start = new Date(eventDetails.timeSlots[slotNumber].start.date);
        var end = new Date(eventDetails.timeSlots[slotNumber].end.date);
        destroyActivePopup(true);
        changeStateOfConfirmButtons(true)
        createEvent(eventDetails,start,end,slotNumber,message);
    });

    function isPastMeeting(date){
        var now = new Date();
        if(date < now){
            return false;
        } else return true;
    }

    $('#cancelInvitation').on('click',function(){
        var eventinfo = {
            invitationId:eventDetails.id
        }
        $.ajax({
            url: '/declineMeeting',
            type: 'POST',
            datatype: 'JSON',
            traditional: true,
            data: eventinfo,
            success: function (msg) {
                if (msg == false) {
                    showMessagePopUp("Rejecting invitation failed",'error');
                }
                else {
                    showMessagePopUp("Rejecting invitation success",'success');
                }
            },
            error: function (event, request, settings) {
            },
            timeout: 40000
        });
    });

    $("#time-slot-tr-1-suggest").click(function(){
        showSuggestPopup( $(this),0)
    });

    $('#time-slot-tr-2-suggest').click(function(){
        showSuggestPopup( $(this),1)
    });

    $('#time-slot-tr-3-suggest').click(function(){
        showSuggestPopup( $(this),2)
    });

    var lastLocation;

    function showSuggestPopup(element,slotNo){
        destroyActivePopup(true);
        popoverSource = element;
        var html = _.template($("#suggest-template-details").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover",
            trigger: 'focus'
        });

        element.popover('show');
        $("#suggest-submit").attr("slotNo",slotNo);
        $("#startDate").val(getDateFormat(new Date(eventDetails.timeSlots[slotNo].start.date)));
        $("#endDate").val(getDateFormat(new Date(eventDetails.timeSlots[slotNo].end.date)));
        $("#startTime").val(getTimeFormat(new Date(eventDetails.timeSlots[slotNo].start.date)));
        $("#endTime").val(getTimeFormat(new Date(eventDetails.timeSlots[slotNo].end.date)));
        $("#newSuggestLocation").val('');


    }
    $("body").on("click","#closePopup-suggest",function(){
        popoverSource.popover('destroy');
        popoverSource = null;
    });
    $("body").on("click","#suggest-submit",function(){
        var suggestedLocation = $("#newSuggestLocation").val();
        comment = $("#comment").val();
        comment = comment.trim();
        newSuggestedLocation = suggestedLocation.trim();
        var selectedStartDate = $("#startDate").val();
        var selectedEndDate = $("#endDate").val();
        var selectedStartTime =  $("#startTime").val();
        var selectedEndTime =  $("#endTime").val();
        var startDateArr = selectedStartDate.split('-');    //year-month-day
        var endDateArr = selectedEndDate.split('-');    //year-month-day
        var startArr = selectedStartTime.split(':'); // hours:min
        var endArr = selectedEndTime.split(':');
        var startDate = new Date(parseInt(startDateArr[0]),parseInt(startDateArr[1])-1,parseInt(startDateArr[2]),parseInt(startArr[0]),parseInt(startArr[1]),0,0);
        var endDate = new Date(parseInt(endDateArr[0]),parseInt(endDateArr[1])-1,parseInt(endDateArr[2]),parseInt(endArr[0]),parseInt(endArr[1]),0,0);
        var timeSlot = $("#suggest-submit").attr("slotNo");

        var originalStartDate = new Date(eventDetails.timeSlots[timeSlot].start.date);
        var originalEndDate = new Date(eventDetails.timeSlots[timeSlot].end.date);
        lastLocation = eventP.scheduleTimeSlots[timeSlot].location;
        var dateNow = new Date()
        if(startDate == 'Invalid Date' || endDate == 'Invalid Date' || startDate >= endDate || startDate < dateNow){
            showMessagePopUp("Please select valid Date & Time",'error');
        }
        else{
            if(timeSlot == 0){
                dateShow1 = [startDate,endDate];

                sendSuggestInvitation()
                destroyActivePopup(true);
            }
            if(timeSlot ==1){
                dateShow2 = [startDate,endDate];
                sendSuggestInvitation()
                destroyActivePopup(true);
            }
            if(timeSlot ==2){
                dateShow3 = [startDate,endDate];
                sendSuggestInvitation()
                destroyActivePopup(true);
            }
        }


    });


    function sendSuggestInvitation(){
        if (eventDetails.senderId == undefined || eventDetails.senderId == null || eventDetails.senderId == '') {
            showMessagePopUp("Suggest new time/ location feature is available only to registered users.",'error');
        }
        else{
            var invitationDetails = construcInvitationDetails();
            invitationDetails.comment = comment;
            invitationDetails.lastLocation = lastLocation;
            if(validateRequired(loggedinUserProfile) && validateRequired(loggedinUserProfile.timezone) && validateRequired(loggedinUserProfile.timezone.name)){
                timezoneProfile = loggedinUserProfile.timezone.name;
            }else getTimezoneProfile()

            invitationDetails.timezone = timezoneProfile;

            if (validateRequired(dateShow1) || validateRequired(dateShow2) || validateRequired(dateShow3)) {
                $.ajax({
                    url: '/sendSuggestionInvitation',
                    type: 'POST',
                    datatype: 'JSON',
                    data: invitationDetails,
                    traditional: true,
                    success: function (result) {
                        if (result == false) {
                            showMessagePopUp("New suggested time/ location failed while sending",'error');
                        }
                        if (result == 'success') {
                            showMessagePopUp("New suggested time/ location sent. Please check your mail",'success');
                            mFlag = true;
                        }
                        if (result == 'error') {
                            showMessagePopUp("Error occurred while sending new suggested time/ location. Please try again",'error')

                        }
                    },
                    error: function (event, request, settings) {
                        var status = event.status;
                        switch (status) {
                            case 4010:
                                showMessagePopUp("please specify all fields",'error')
                                break;
                        }
                    },
                    timeout: 20000
                })
            }
            else {
                showMessagePopUp("select at least one suggest time",'error')
            }
        }
    }

    function createEvent(event,starts,ends,timeSlotNumber,message){

        events = event;
        events.title = event.invitationTitle;
        events.start = starts;
        events.end = ends;
        events.color = '#0000FF';
        events.isMeeting = false;

        renderRelatasGoogleEvent(events,starts, ends, timeSlotNumber,message);
    }

// function to create event on google while user clicks on confirm(user able to confirm one schedule)
    function renderRelatasGoogleEvent(eventss,start, end, timeSlotNumber,message){

        if (validateRequired(eventss) && validateRequired(start) && validateRequired(end)) {
            var senderPrimaryEmail = '';
            if(validateRequired(publicProfile)){
                if(validateRequired(publicProfile.google[0])){
                    senderPrimaryEmail = publicProfile.google[0].emailId;
                }
            }
            var toListUserId;
            if(eventP.suggested && isSelfCalendarMeet()){
                toListUserId = eventP.toList[0].receiverId;
            }

            if(isSameUser()){
                if(isSelfCalendarMeet()){
                    var rFName3 = eventP.toList[0].receiverFirstName || '';
                    var rLName3 = eventP.toList[0].receiverLastName || '';
                    eventss.senderEmailId = eventP.toList[0].receiverEmailId || '';
                    eventss.name = rFName3+' '+rLName3;
                    eventss.senderId = eventP.toList[0].receiverId;
                }else{
                    eventss.senderEmailId = eventP.to.receiverEmailId;
                    eventss.name = eventP.to.receiverName;
                    eventss.senderId = eventP.to.receiverId;
                }
                eventss.receiverName = eventP.senderName;
                eventss.receiverId = eventP.senderId;
                eventss.receiverEmailId = eventP.senderEmailId;
            }

            var eventDetails = {
                title:eventss.title,
                start:start,
                end:end,
                isSuggested:eventP.suggested ? true : false,
                senderEmailId:eventss.senderEmailId,
                senderPrimaryEmail:senderPrimaryEmail,
                senderName:eventss.name,
                senderId:eventss.senderId,
                receiverName:eventss.receiverName,
                receiverId:eventss.receiverId,
                toListUserId:toListUserId,
                description:eventss.description,
                location:eventss.location,
                locationType:eventss.locationType,
                invitationId:eventss.id,
                receiverEmailId:eventss.receiverEmailId,
                timeSlotNumber:timeSlotNumber,
                slotId:eventss.timeSlots[timeSlotNumber]._id,
                message:message || '',
                timezone:timeZone
            };


            if(loggedinUserProfile.timezone){
                if(loggedinUserProfile.timezone.name){
                    eventDetails.timezone = loggedinUserProfile.timezone.name;
                }
            }

            $.ajax({
                url: '/createEventGoogle',
                type: 'POST',
                datatype: 'JSON',
                traditional: true,
                data: eventDetails,
                success: function (msg) {

                    if(msg == 'noAccount'){
                        changeStateOfConfirmButtons(false)
                        showMessagePopUp("You don't have google account associated with your account",'error')
                    }
                    else if (msg == false) {
                        changeStateOfConfirmButtons(false)
                        showMessagePopUp("Accepting invitation failed (CAUSE : Creating event in google failed)",'error')

                    }else {
                        mixpanelIncrement("# Met");
                        changeMeetingStatus(eventDetails);

                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 40000
            });
        }
        else{
            showMessagePopUp("No meeting details found",'error');
            changeStateOfConfirmButtons(false)
        }
    }

    /* Function to change meeting readStatus false(fresh invitation user not opened) to true(invitation red done)  */
    function changeMeetingStatus(invitationDetails){
        var invitation = invitationDetails;

        if(isSelfCalendarMeet()){
            invitation.selfCalendar = true;
            invitation.isAccepted = isAccepted;
            invitation.toListLength = eventP.toList.length;
        }

        $.ajax({
            url: '/updateEventStatus',
            type: 'POST',
            datatype: 'JSON',
            traditional: true,
            data: invitation,
            success: function (msg) {

                if (!msg) {
                    showMessagePopUp("Accepting invitation failed",'error')
                    changeStateOfConfirmButtons(false)
                }
                else if (msg == 'ics failed') {
                    showMessagePopUp("Thank you for confirming the meeting. An email confirmation has been sent to your email id, Generating .ics failed",'success');

                    mFlag = true;
                    changeStateOfConfirmButtons(false)
                }
                else{
                    icsFile = msg;
                    if(isSelfCalendarMeet() && isAccepted){
                        sendMails(invitationDetails)
                    }else
                        uploadIcsFile(msg,invitationDetails);
                }
            },
            error: function (event, request, settings) {
                showMessagePopUp("Accepting invitation failed",'error')
                changeStateOfConfirmButtons(false)
            },
            timeout: 40000
        });
    }



    function getTextLength(text,maxLength){
        if(!text) return '';

        var textLength = text.length;
        if(textLength != 0){
            if(textLength >maxLength){
                var formattedText = text.slice(0,maxLength)
                return formattedText+'..';
            }
            else return text;
        } else return '';

    }
// Function to generate json object for suggested times
    function construcInvitationDetails(){
        var invitationDetails = {
            invitationId:eventDetails.id,
            senderId:'',
            senderName:'',
            senderEmailId:'',
            receiverId:eventDetails.senderId || '',
            receiverName:eventDetails.name,
            receiverEmailId:eventDetails.senderEmailId,
            docs:eventDetails.document,
            participants:eventDetails.invitees
        }

        if (validateRequired(dateShow1)) {

            invitationDetails.startDate1 = dateShow1[0];
            invitationDetails.endDate1 = dateShow1[1];
            invitationDetails.title1 = eventDetails.timeSlots[0].title;
            invitationDetails.location1 = eventDetails.timeSlots[0].location;
            invitationDetails.suggestedLocation1 = newSuggestedLocation || '';
            invitationDetails.locationType1 = eventDetails.timeSlots[0].locationType;
            invitationDetails.description1 = eventDetails.timeSlots[0].description;
        }
        if (validateRequired(dateShow2)) {
            invitationDetails.startDate2 = dateShow2[0];
            invitationDetails.endDate2 = dateShow2[1];
            invitationDetails.title2 = eventDetails.timeSlots[1].title;
            invitationDetails.location2 = eventDetails.timeSlots[1].location;
            invitationDetails.suggestedLocation2 = newSuggestedLocation || '';
            invitationDetails.locationType2 = eventDetails.timeSlots[1].locationType;
            invitationDetails.description2 = eventDetails.timeSlots[1].description;
        }
        if (validateRequired(dateShow3)) {
            invitationDetails.startDate3 = dateShow3[0];
            invitationDetails.endDate3 = dateShow3[1];
            invitationDetails.title3 = eventDetails.timeSlots[2].title;
            invitationDetails.location3 = eventDetails.timeSlots[2].location;
            invitationDetails.suggestedLocation3 = newSuggestedLocation || '';
            invitationDetails.locationType3 = eventDetails.timeSlots[2].locationType;
            invitationDetails.description3 = eventDetails.timeSlots[2].description;
        }

        return invitationDetails;
    }

    function isSameUser(){
        if(loggedinUserProfile._id == eventDetails.senderId){
            return true;
        }else return false;
    }

    var comConnectionsLength;
    var currentStart = 0;
    var currentEnd = 0;
    // Logic to get common connections
    function getCommonConnections(publicUserId){
        var user = {
            id:publicUserId,
            requestFrom:true
        };

        $.ajax({
            url:'/getCommonConnections',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(result){

                if (result == false) {

                    $('.small-login-container').css({display:'block'});
                    $('.linkedin-social-details').css({display:'none'});
                }
                else if (result.message) {

                    $('.small-login-container').css({display:'none'});
                    $('.social-connections-list').html('<span style="color: white"> &nbsp;&nbsp;'+result.message+'</span>')
                }
                else{

                    $('.small-login-container').css({display:'none'});
                    $('.linkedin-social-details').css({display:'block'});
                    if(validateRequired(result.relationToViewer) && validateRequired(result.relationToViewer.relatedConnections) && validateRequired(result.relationToViewer.relatedConnections.values) && result.relationToViewer.relatedConnections.values.length > 0){

                        var connections = result.relationToViewer.relatedConnections.values;
                        comConnectionsLength = connections.length >= 9 ? 9 : connections.length ;
                        if(connections.length > 2){
                            $("#connection-next").show()
                        }

                        for(var i=0; i<connections.length; i++){
                            var linkedinUrl = connections[i].siteStandardProfileRequest.url;
                            var firstName = connections[i].firstName || '';
                            var lastName = connections[i].lastName || '';
                            var title = firstName+' '+lastName;
                            var iUrl = connections[i].pictureUrl || '/images/default.png';
                            var commonConnetionHtml ='';
                            var id = 'connection-'+i;
                            if(i > 2)
                                commonConnetionHtml = ' <td class="social-connection-item" style="display: none" id='+id+'>';
                            else
                                commonConnetionHtml = ' <td class="social-connection-item" id='+id+'>';

                            commonConnetionHtml += '<a target="_blank" href='+linkedinUrl+'><div class="profile-picture-small social-connection-profile-picture">';
                            //commonConnetionHtml += '<img class="profile-picture-border" src="/images/white_circular_border_small.png" title='+title.replace(/\s/g, '&nbsp;')+'>';
                            commonConnetionHtml += '<img class="profile-pic-img-small" style="border: 2px solid white;" id="lImage1" src='+iUrl+' title='+title.replace(/\s/g, '&nbsp;')+'><br></div></a>';
                            commonConnetionHtml += '</td>';
                            if(i < 9){
                                $('.social-connections-list').append(commonConnetionHtml);
                            }
                            if(i <= 2)
                                currentEnd = i;
                        }
                    }else{
                        $("#connection-next").hide();
                        $("#connection-prev").hide();
                        $('.small-login-container').css({display:'none'});
                        $('.social-connections-list').html('<span style="color: white"> &nbsp;&nbsp;No common connections found</span>')
                    }

                    if(validateRequired(result.currentShare)){
                        var statusContent = '';
                        if(validateRequired(result.currentShare.content)){
                            statusContent = result.currentShare.content.description || result.currentShare.comment || '';
                            if(!validateRequired(result.currentShare.content.description)){
                                if(validateRequired(result.currentShare.attribution) && validateRequired(result.currentShare.attribution.share)){
                                    statusContent = result.currentShare.attribution.share.comment || '';
                                }else statusContent = result.currentShare.comment;
                            }
                        }else if(validateRequired(result.currentShare.attribution) && validateRequired(result.currentShare.attribution.share)){
                            statusContent = result.currentShare.attribution.share.comment || '';
                        }else{
                            statusContent = result.currentShare.comment || '';
                        }
                        if(validateRequired(statusContent)){
                            var linkedinStatusUpdaeHtml = '<div class="social-post-title">';
                            linkedinStatusUpdaeHtml += '<span>'+statusContent+'</span></div>';
                            $('#latestPost').html(linkedinStatusUpdaeHtml);
                        }
                    }

                    if(validateRequired(result.positions) && validateRequired(result.positions.values) && result.positions.values.length > 0){
                        var positions = result.positions.values;
                        var linkedinCompaniesHtml = '';
                        linkedinCompaniesHtml +='';
                        for(var p=0;p<positions.length;p++){
                            if(p<=2){
                                linkedinCompaniesHtml +='<span style="color: white">'+positions[p].company.name+'</span></br>';
                            }
                        }
                        $('.social-latest-positions').html(linkedinCompaniesHtml);
                    }
                }
            },
            error: function (event, request, settings) {

            }
        })
    }

    $("#connection-next").on('click',function(){
        if(currentEnd < comConnectionsLength-1){
            var nowStart = currentEnd+1;
            var nowEnd = (nowStart+2) >= comConnectionsLength ? comConnectionsLength : nowStart+2;
            displayPrevNext(nowStart,nowEnd)
        }
    });
    $("#connection-prev").on('click',function(){
        if(currentStart > 0){
            var nowStart = currentStart-3 <= 0 ? 0 : currentStart-3;
            var nowEnd = (nowStart+2) >= comConnectionsLength ? comConnectionsLength : nowStart+2;
            displayPrevNext(nowStart,nowEnd)
        }
    });
    function displayPrevNext(nowStart,nowEnd){
        for(var c=0; c<comConnectionsLength; c++){
            if(c < nowStart){
                $("#connection-"+c).hide();
            }else if(c >= nowStart && c<=nowEnd){
                $("#connection-"+c).show();
            }else $("#connection-"+c).hide();
        }

        if(nowStart == 0)
            $("#connection-prev").hide();
        else  $("#connection-prev").show();

        if(nowEnd == comConnectionsLength ||  nowEnd == comConnectionsLength-1)
            $("#connection-next").hide();
        else $("#connection-next").show();

        currentStart = nowStart;
        currentEnd = nowEnd;
    }
// Logic to get twitter info
    function getTwitterInfo(publicUserId){
        var user = {
            id:publicUserId,
            requestFrom:true
        }
        $.ajax({
            url:'/getTwitterInfo',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(result){
                if (result == false) {
                    $('.small-login-container').css({display:'block'});
                }
                else if (result.message) {
                    $('#twitter-msg').html('<span style="color: white"> &nbsp;&nbsp;No tweets found</span>');
                }
                else{
                    for(var i=0;i<result.length;i++){
                        if (i<5) {
                            $('#twitter-msg').append('<span style="color: white">'+result[i].text+'</span></br><hr></br>');
                        }
                    }
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000

        })
    };

// Logic to get facebook info
    function getFacebookInfo(publicUserId){
        var user = {
            id:publicUserId,
            requestFrom:true
        }
        $.ajax({
            url:'/getFacebookInfo',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(result){

                if (result == false) {

                    $('.small-login-container').css({display:'block'});
                }
                else if (result.message) {

                    $('.small-login-container').css({display:'none'});
                    $('#facebook-msg').html('<span style="color: white"> &nbsp;&nbsp;No common facebook friends found.</span>');
                }
                else{

                    for(var i=0;i<result.length;i++){

                        $('#facebook-msg').append('<span style="color: white">'+result[i].name+'</span></br><hr></br>');

                    }
                    getFacebookStatus(user);
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    };

    // Function to get linkedin profile of public user
    function getLinkedinPofile(publicUserId){
        var user = {
            id:publicUserId
        }
        $.ajax({
            url:'/linkedinPro',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(result){
                $('.social-latest-school').html('');
                if (result == false) {
                }
                else if (result.message) {
                }
                else{
                    var linkedinSchoolHtml ='';
                    var schools = result.educations.values;
                    if(validateRequired(schools)){
                        if(validateRequired(schools[0])){
                            schools.forEach(function(school){
                                linkedinSchoolHtml += '<span style="color: white">'+school.schoolName+'</span></br>';
                            })
                        }
                        $('.social-latest-school').html(linkedinSchoolHtml);
                    }

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    // function to get facebook status
    function getFacebookStatus(user){
        $.ajax({
            url:'/facebookStatus',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(result){
                if (!result) {
                }
                else if (result.message) {
                }
                else {
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    // Function to generate string date format for html input type date
    function getDateFormat(date){
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var dateFormat = year + "-" + month + "-" + day;
        return dateFormat;
    }

// Function to generate string time format for html input type time
    function getTimeFormat(date) {

        var hrs = date.getHours();
        var min = date.getMinutes();
        if (hrs < 10) {
            hrs = "0" + hrs
        }

        if (min < 10) {
            min = "0" + min;
        }

        var time = hrs +':'+ min

        return time;
    }
// Function to get suggested times(start, end)

    function getDayFormat(date){
        var weekDay = day[date.getDay()];
        var toDay = date.getDate();
        var month = monthNameFull[date.getMonth()];
        return weekDay+" "+toDay+" "+month;
    }

    function getTimeFormatWithMeridian(date){
        var hrs = date.getHours();
        var min = date.getMinutes();
        var meridian;
        if(hrs > 12){
            hrs -= 12;
            meridian = 'PM';
        }
        else {
            if(hrs == 12){
                meridian = 'PM'
            }else
                meridian = 'AM';
        }
        if (min < 10) {
            min = "0"+min;
        };
        var time = hrs+":"+min+" "+meridian;
        return time;
    }

// Function to validate one variable is empty or not

    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }

    function getTimeZone(){
        var start = new Date()
        var zone = start.toTimeString().replace(/[()]/g,"").split(' ');
        if(zone.length > 3){
            var tz = '';
            for(var i=2; i<zone.length; i++){

                tz += zone[i].charAt(0);
            }
            return tz || zone[2]
        }
        else return zone[2]

    }
    sidebarHeight();
    function sidebarHeight(){
        var sideHeight = $(document).height()
        $("#sidebarHeight").css({'min-height':sideHeight})
        $("#fb-tab-content").css({'min-height':sideHeight-270})
        $("#twitter-tab-content").css({'min-height':sideHeight-270})
        $("#linkedin-tab-content").css({'min-height':sideHeight-270})
        $("#relatas-tab-content").css({'min-height':sideHeight-270})

    }
    jQuery.nl2br = function(varTest){
        return varTest.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
    };

    var docName;
    function uploadIcsFile(file,meeting){
        if (file) {
            var key = meeting.invitationId+'.ics';
            var params = {Key: key, ContentType: 'text/calendar', Body: file};

            var request = s3bucket.putObject(params);
            request.on('httpUploadProgress', function (progress) {
            });

            request.send(function (err, data) {
                if(err){
                    showMessagePopUp("Generating ICS failed.",'error');
                }
                else{
                    meeting.icsAwsKey = key;
                    meeting.icsUrl = docUrl+key;
                    icsFile = file;
                    updateMeetingWithIcsFile(meeting);
                }
            });
        }else {
            showMessagePopUp("Thank you for confirming the meeting. An email confirmation has been sent to your email id, generating .ics failed.",'success')

            mFlag = true;
            changeStateOfConfirmButtons(false)
        }
    }

    function updateMeetingWithIcsFile(meeting){

        $.ajax({
            url:'/updateMeeting/icsfile',
            type:'POST',
            datatype:'JSON',
            data:meeting,
            traditional:true,
            success:function(result){
                sendMails(meeting)
                if(result){
                    changeStateOfConfirmButtons(false)
                    showMessagePopUp("Thank you for confirming the meeting. An email confirmation has been sent to your email id",'success')

                    mFlag = true;
                }else {
                    changeStateOfConfirmButtons(false)
                    showMessagePopUp("Thank you for confirming the meeting. An email confirmation has been sent to your email id, generating .ics failed.",'success')

                    mFlag = true;
                }
            },
            error: function (event, request, settings) {

            }
        })
    }

    function sendMails(meeting){
        meeting.file = icsFile;

        $.ajax({
            url:'/sendEmailWithAttachment',
            type:'POST',
            datatype:'JSON',
            data:meeting,
            traditional:true,
            success:function(result){
                mixpanelTrack("Accept Meeting");
                if(result){

                    changeStateOfConfirmButtons(false)
                    showMessagePopUp("Thank you for confirming the meeting. An email confirmation has been sent to your email id",'success')

                    mFlag = true;
                }else {
                    changeStateOfConfirmButtons(false)
                    showMessagePopUp("Thank you for confirming the meeting. An email confirmation has been sent to your email id, generating .ics failed.",'success')

                    mFlag = true;

                }
            },
            error: function (event, request, settings) {

            },
            timeout:2000
        })
    }

    function changeStateOfConfirmButtons(state){
        $('#time-slot-tr-1-confirm').attr("disabled", state);
        $('#time-slot-tr-2-confirm').attr("disabled", state);
        $('#time-slot-tr-3-confirm').attr("disabled", state);
        if(state){
            if(pleaseWait){
                pleaseWait.text('Pl. Wait..');
            }
        }else{
            if(pleaseWait){
                pleaseWait.text('Confirm');
            }
        }
    }

    function validateMeetingAccept(){
        if(eventP.suggested && eventP.suggestedBy){
            if(eventP.suggestedBy.userId == loggedinUserProfile._id){
                return false;
            }else{
                return true;
            }
        }else{
            if(eventP.senderId == loggedinUserProfile._id){
                return false;
            }else return true;
        }
    }

    function acceptMeeting(status){
        if(eventP.scheduleTimeSlots.length ==1 && !userAccepted){
            if(validateMeetingAccept()){
                var startCheck = new Date(eventDetails.timeSlots[0].start.date)

                if(isPastMeeting(startCheck)){
                    pleaseWait = $('#time-slot-tr-1-confirm');
                    eventDetails.invitationTitle = eventDetails.timeSlots[0].title;
                    eventDetails.location = eventDetails.timeSlots[0].location;
                    eventDetails.locationType = eventDetails.timeSlots[0].locationType;
                    eventDetails.description = eventDetails.timeSlots[0].description;

                    var slotNumber = 0;
                    var message = '';
                    var start = new Date(eventDetails.timeSlots[slotNumber].start.date);
                    var end = new Date(eventDetails.timeSlots[slotNumber].end.date);
                    if(validateRequired(popoverSource)){
                        popoverSource.popover('destroy');
                    }
                    changeStateOfConfirmButtons(true)
                    createEvent(eventDetails,start,end,slotNumber,message);
                }
                else showMessagePopUp("Meeting time is over, Please suggest new time/ location.",'error')
            }
            else showMessagePopUp("You are not authorised to accept this meeting.",'error')
        }
    }

    function getAcceptMeetingStatus(){
        $.ajax({
            url:'/meeting/getAcceptStatus',
            type:'GET',
            success:function(status){

                if(status == true){

                    acceptMeeting(status)
                    clearAcceptMeetingStatus();
                }
            }
        });
    }

    function clearAcceptMeetingStatus(){
        $.ajax({
            url:'/meeting/clearAcceptMeeting',
            type:'GET',
            success:function(status){

            }
        });
    }

    function destroyActivePopup(destyoy){
        if(popoverSource != null && destyoy)
        {
            popoverSource.popover('destroy');
            popoverSource = null;
            // return;
        }
    }

    function getTimezoneProfile(){
        var tz = jstz.determine();
        timezoneProfile = tz.name();
        return timezoneProfile;
    }

    $('#loginUsingLinkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        window.location.replace('/linkedinLoginFromMeetingDetailsPage');
    });

    $('#login-with-linkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        window.location.replace('/linkedinLoginFromMeetingDetailsPage');
    });

    $('#sign-up-with-linkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        window.location.replace('/linkedinLoginFromMeetingDetailsPage');
    });


    $("#sendUsingLinkedin").on("click",function(){
        $('#loginUsingLinkedin').trigger("click")
    })

    $('#loginUsingGoogle').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenMeetingDetails');
    });

    $('#login-with-google').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenMeetingDetails');
    });
    $('#sign-up-with-google').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenMeetingDetails');
    });

    $('#loginUsingGoogle2').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenMeetingDetails');
    });

    $('#loginFormButton').on('click',function(){
        var emailId1 = $("#emailId2").val();
        var password1 = $("#password").val();

        if (validateRequired(emailId1) && validateRequired(password1)) {
            emailId1 = emailId1.toLowerCase();
            var credentials={
                emailId:emailId1,
                password:password1
            }

            $.ajax({
                url:'/loginFromPublicProfile',
                type:'POST',
                datatype:'JSON',
                data:credentials,
                traditional:true,
                success:function(result){
                    if (result) {
                        loadLoggedinUserProfile();
                        $("#lightBoxPopup").slideToggle();
                        $("#confirmation-window").slideToggle();

                    }
                    else{
                        showMessagePopUp("Looks like either your email or password is incorrect. Please try again.",'error')
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            })
        }
        else{
            showMessagePopUp("Please enter your credentials",'error');
        }
    });

    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }
    function mixpanelIncrement(eventName){
        mixpanel.people.increment(eventName);
    }

    /*******************************************************************************/
    var uniqueId;
    $('#userFirstName').change(function(event){
        var text = $('#userFirstName').val().toLowerCase()
        var text2 = $('#userLastName').val().toLowerCase()
        $("#userPublicProfileUrl").val(getValidRelatasIdentity(text+''+text2))
    })
    $('#userLastName').change(function(event){
        var text = $('#userFirstName').val().toLowerCase()
        var text2 = $('#userLastName').val().toLowerCase()
        $("#userPublicProfileUrl").val(getValidRelatasIdentity(text+''+text2))
    })

    $("#userPublicProfileUrl").focusout(function(){
        var text = $("#userPublicProfileUrl").val()
        checkIdentityAvailability(text)
    });
    var flagi = false;
    function checkIdentityAvailability(identity){

        var details = {
            publicProfileUrl:getValidRelatasIdentity(identity)
        }

        $.ajax({
            url:'/checkUrl',
            type:'POST',
            datatype:'JSON',
            data:details,
            traditional:true,
            success:function(result){
                if(result == true){
                    $("#userPublicProfileUrl").val(getValidRelatasIdentity(identity))
                    identityFlag = 1;
                    if(flagi){
                        flagi = false;
                        showMessagePopUp("Your selected name is not available. Next best name suggested is:  "+identity+"",'tip')
                    }
                }
                else{

                    a(identity)

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }


    //On click event on saveDetailsButton
    $('#saveDetailsButton').on("click",function(){
        var data = formData()
        onSaveFormData(data)

    });

    function formData(){
        var mail = $('#userEmailId').val();

        var data={
            'firstName':$('#userFirstName').val() || '',
            'lastName':$('#userLastName').val() || '',
            'publicProfileUrl':$('#userPublicProfileUrl').val() || '',
            'emailId':mail.toLowerCase() || '',
            'profilePicUrl':'/images/default.png',
            'password':$('#userPassword').val() || ''

        };

        return data;
    }


    function onSaveFormData(data){
        var result = validate(data);
        if(result == true){

            data.publicProfileUrl = getValidRelatasIdentity(data.publicProfileUrl)

            var details = {
                publicProfileUrl:data.publicProfileUrl
            }
            $.ajax({
                url:'/checkUrl',
                type:'POST',
                datatype:'JSON',
                data:details,
                traditional:true,
                success:function(result){
                    if(result == true){
                        checkEmailAddress(data)
                    }
                    else{
                        showMessagePopUp("Public profile url already exist in the Relatas, Please change it",'error')

                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 30000
            })

        }else{
            result.focus();
        }
    }


    function checkEmailAddress(data){

        $.ajax({
            url:'/checkEmail/'+data.emailId,
            type:'GET',
            traditional:true,
            success:function(result){
                if(result == true){
                    data = createNewUser(data)

                    if ($('#acceptTerms').prop('checked')) {
                        saveFormData(data);
                    }
                    else{
                        $('#acceptTerms').focus();
                        showMessagePopUp("Please accept terms and conditions",'error')
                    }
                }
                else{
                    showMessagePopUp("Email address already exist in the Relatas, Please change it",'error')

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })

    }

    function createNewUser(data){
        uniqueId = data.publicProfileUrl;
        var user = {
            'firstName':data.firstName,
            'lastName'  :data.lastName,
            'publicProfileUrl':data.publicProfileUrl,
            'screenName':data.publicProfileUrl,
            'emailId':data.emailId.toLowerCase(),
            'password':data.password,
            'registeredUser':false,
            'profilePicUrl':'/images/default.png',
            'serviceLogin':'relatas',
            'public': true,
            'meetingDetails':true
        };

        user.google=googleString()
        user.linkedin=linkedinString()
        user.facebook=facebookString()
        user.twitter=twitterString()

        return user;

    }


// Function to store form data in session
    function saveFormData(data){

        $.ajax({
            url: '/registerAccount',
            type: 'POST',
            datatype: 'JSON',
            traditional: true,
            data: data,
            success: function (msg) {
                if (msg == true) {
                    signUpFlag = true;
                    showMessagePopUp("Congratulations your account has been created. Please add details to profile page and Connect your social networks to make your profile smart and productive. Click on CONFIRM to confirm invitation.",'success')

                    $("#lightBoxPopup").slideToggle();
                    $("#confirmation-window").slideToggle();
                    loadLoggedinUserProfile(true);


                }
                else{
                    showMessagePopUp("Error occurred while saving, Please try again",'error')

                }

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        });
    }

    var identityFlag = 1;
    function a(identity){
        identity = getValidRelatasIdentity(identity)+''+identityFlag
        identityFlag++;
        flagi = true;
        checkIdentityAvailability(identity)
    }

    function getValidRelatasIdentity(rIdentity) {
        rIdentity = rIdentity.toLowerCase();
        rIdentity = rIdentity.replace(/\s/g,'');
        return rIdentity;

    }


    //Functions to validate required feilds
    function validate(data){
        if(!validateRequired(data.firstName.trim())){
            showMessagePopUp("Please provide First name",'error')
            return $('#userFirstName');
        }else if(!validateRequired(data.lastName.trim())){
            showMessagePopUp("Please provide Last name",'error')
            return $('#userLastName');
        }else  if(!validateRequired(data.publicProfileUrl.trim())){
            showMessagePopUp("Please provide valid Unique Relatas identity",'error')
            return $('#userPublicProfileUrl');
        } else  if(!validateRequired(data.emailId.trim())){
            showMessagePopUp("Please provide Email id",'error')
            return $('#userEmailId');
        }else if(!validateRequired(data.password.trim())){
            showMessagePopUp("Please provide password",'error')
            return $('#userPassword');
        }else if (!validateEmailField(data.emailId)) {
            showMessagePopUp("Please provide valid email id",'error')
            return $('#userEmailId');
        }
        else{
            return true;
        }
    }

    function validateEmailField(email)
    {
        var emailText = email;

        var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
        if (pattern.test(emailText))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Function to generate valid json string for goole account
    function googleString(){

        var google ='';

        google +='{ "id":"",';
        google +='"token":"",';
        google +='"refreshToken":"",';
        google +='"name":""}';

        var googleNew ="[]";
        return googleNew;
    }

    // Function to generate valid json string for linkedin account
    function linkedinString(){
        var linkedin='{';

        linkedin +='"id":"",';
        linkedin +='"token":"",';
        linkedin +='"emailId":"",';
        linkedin +='"name":""';
        linkedin +='}';
        return linkedin;
    }

// Function to generate valid json string for facebook account
    function facebookString(){
        var facebook='{';

        facebook +='"id":"",';
        facebook +='"token":"",';
        facebook +='"emailId":"",';
        facebook +='"name":""';

        facebook +='}';
        return facebook;
    }

    // Function to generate valid json string for twitter account
    function twitterString(){
        var twitter='{';

        twitter +='"id":"",';
        twitter +='"token":"",';
        twitter +='"refreshToken":"",';
        twitter +='"displayName":"",';
        twitter +='"userName":""';

        twitter +='}';
        return twitter;
    }
    /***************************************************************************************/

    /* Cancel and delete meeting */
    function showCancelAndDeleteButtons(meeting){
        if(meeting.senderId == loggedinUserProfile._id){
            buttonTextChange('delete','delete',isAccepted,meeting.invitationId,eventP.selfCalendar);
        }
        else{
            buttonTextChange('cancel','cancel',userAccepted,meeting.invitationId,eventP.selfCalendar,isAccepted);
        }
    }

    function buttonTextChange(text,action,status,invitationId,isSelf,isAccepted){
        $("#time-slot-tr-1-cancel").text('X')
        $("#time-slot-tr-2-cancel").text('X')
        $("#time-slot-tr-3-cancel").text('X')

        $("#time-slot-tr-1-cancel").attr('action',action)
        $("#time-slot-tr-2-cancel").attr('action',action)
        $("#time-slot-tr-3-cancel").attr('action',action)
        if(!status){
            if(isSelf || !isAccepted){
                $("#time-slot-tr-1-cancel").attr('meetingId',invitationId)
                $("#time-slot-tr-2-cancel").attr('meetingId',invitationId)
                $("#time-slot-tr-3-cancel").attr('meetingId',invitationId)

                $("#time-slot-tr-1-cancel").show()
                $("#time-slot-tr-1-cancel").removeClass('invisible')
                $("#time-slot-tr-2-cancel").show()
                $("#time-slot-tr-2-cancel").removeClass('invisible')
                $("#time-slot-tr-3-cancel").show()
                $("#time-slot-tr-3-cancel").removeClass('invisible')
            }
        }
    }

    $("#time-slot-tr-1-cancel, #time-slot-tr-2-cancel, #time-slot-tr-3-cancel").on('click',function(){

        switch ($(this).attr('action')){
            case 'delete':deleteMeeting($(this),$(this).attr('meetingId'),null,null);
                break;
            case 'cancel':cancelMeeting($(this), $(this).attr('meetingId'), null, eventP.selfCalendar ? true : false);
                break;
            default :console.log('default')
                break;
        }
    });

    function deleteMeeting(element, eventIdIn, sender, selfCalendar) {
        var html = _.template($("#delete-meeting-popup").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });
        popoverContent = element;
        element.popover('show');
        $(".popover").css({'width': '20%'});
        $("#delete-meeting-but").attr("event_id_delete", eventIdIn);
    }

    function cancelMeeting(element, eventIdIn, sender, selfCalendar) {

        var html = _.template($("#cancel-meeting-popup").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });

        popoverContent = element;
        element.popover('show');

        $(".popover").css({'width': '40%'})
        $("#cancel-meeting-but").attr("event_id_cancel", eventIdIn);
        $("#cancel-meeting-but").attr("self_calendar", selfCalendar);
    }

    function getUserTimezone(){
        return timezoneProfile || timeZone || 'UTC'
    }

    $("body").on("click", "#delete-meeting-but", function (e) {
        var id = $(this).attr("event_id_delete");

        closePopOver();
        $.ajax({
            url: "/meetings/action/delete?mId=" + id+"&timezone="+getUserTimezone(),
            type: "GET",
            success: function (response) {
                closePopOver();
                if (response) {
                    showMessagePopUp("Meeting successfully deleted. Message sent to all participants.", 'success');
                    gotoCalendar = true;
                }
                else {
                    showMessagePopUp("An error occurred. Please try again", 'error');
                }
            }
        })
    });

    $("body").on("click", "#cancel-meeting-but", function (e) {
        var obj = {
            invitationId: $(this).attr("event_id_cancel"),
            message: $("#cancel-meeting-message").val(),
            selfCalendar: $(this).attr("self_calendar"),
            timezone:getUserTimezone()
        };

        closePopOver();
        $.ajax({
            url: "/meetings/action/cancel",
            type: "POST",
            datatype: "JSON",
            data: obj,
            success: function (response) {
                closePopOver();
                if (response) {
                    gotoCalendar = true;
                    showMessagePopUp("Your meeting has been canceled.", 'success');
                }
                else {
                    showMessagePopUp("An error occurred. Please try again", 'error');
                }
            }
        })
    });

    $("body").on("click", "#closePopup-delete-meeting", function (e) {
        closePopOver();
    });

    $("body").on("click", "#closePopup-cancel-meeting", function (e) {
        closePopOver();
    });

    /* Meeting To Do */

    $(".action-due-date").datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        minDate: new Date(),
        onClose: function (dp,$input) {

        }
    });

    $("#submit-todo-list").on("click",function(){
        var todoList = getTableData($("#table-to-do-info"));
        var data = {
            items:JSON.stringify(todoList),
            invitationId:eventP.invitationId,
            meetingObj:JSON.stringify(eventP),
            acceptedSlot:acceptedSlot,
            creatorName:loggedinUserProfile.firstName+' '+loggedinUserProfile.lastName,
            timezone:getUserTimezone()
        };

        if(todoList.length > 0){
            $.ajax({
                url:'/meeting/todo/add',
                type:'POST',
                datatype:'JSON',
                data:data,
                success:function(response){
                    if(response){
                        //getRequestedMeeting()
                        mFlag = true;
                        //$("html, body").animate({ scrollTop: 0 }, 1000);
                        showMessagePopUp('Action items have been successfully sent.','success');
                    }
                    else showMessagePopUp('An error occurred. Please try again','error');
                }
            })
        }
    });

    function getTableData(table) {
        var data = [];
        var rows = table.children()[1].rows;
        for(var row=0; row<rows.length; row++){

            var owner = $(rows[row]).attr('owner');
            var receiver = $(rows[row]).attr('receiver');
            var updateCount = $(rows[row]).attr('updateCount');
            var googleEventId = $(rows[row]).attr('googleEventId');
            var oldStatus = $(rows[row]).attr('status');
            var oldAssignedTo = $(rows[row]).attr('assignedTo');
            var oldActionItem = $(rows[row]).attr('actionItem');

            var cols = $(rows[row]).children();
            var action = $($(cols[1])[0]).children().val();
            var _id = $($(cols[1])[0]).children().attr('_id');
            var assignTo = $($(cols[2])[0]).children().val();
            var dueDate = $($(cols[3])[0]).children().val();
            var oldDueDate = $($(cols[3])[0]).children().attr('oldDue');
            var createdBy = $($(cols[3])[0]).children().attr('createdBy');
            var status = $($(cols[4])[0]).children().val();

            if(validateRequired(action) && assignTo != 'invalid' && validateRequired(dueDate)){
                if(!validateRequired(_id)){
                    var obj = {
                        actionItem:action,
                        assignedTo:assignTo,
                        dueDate:new Date(dueDate),
                        createdDate:new Date(),
                        _id:_id
                    };
                    data.push(obj);
                }
                else if(owner == 'true' && (oldActionItem != action || oldAssignedTo != assignTo || oldStatus != status || new Date(dueDate).toISOString() != new Date(oldDueDate).toISOString())){
                    var obj2= {
                        actionItem:action,
                        assignedTo:assignTo,
                        dueDate:new Date(dueDate),
                        _id:_id,
                        owner:true,
                        receiver:receiver == 'true',
                        updateCount:updateCount,
                        status:status,
                        googleEventId:googleEventId
                    };

                    data.push(obj2);
                }
                else if(receiver == 'true' && (oldStatus != status || new Date(dueDate).toISOString() != new Date(oldDueDate).toISOString())){
                    var deuDateUpdated = false;
                    if(new Date(dueDate).toISOString() != new Date(oldDueDate).toISOString()){
                        deuDateUpdated = true;
                    }
                    var obj3= {
                        actionItem:action,
                        assignedTo:assignTo,
                        dueDate:new Date(dueDate),
                        deuDateUpdated:deuDateUpdated,
                        createdBy:createdBy,
                        _id:_id,
                        receiver:true,
                        owner:owner == 'true',
                        status:status,
                        updateCount:updateCount,
                        googleEventId:googleEventId
                    };

                    data.push(obj3);
                }
            }
            else{
               // showMessagePopUp("Please provide full information for meeting ToDo.",'error');
               // return []
            }
        }

        return data;
    }

    function isTaskOwner(ownerId){
        return ownerId == loggedinUserProfile._id
    }

    function isTaskReceiver(receiverId){
        return isTaskOwner(receiverId);
    }

    function getValidNumber(num){
        return num >= 10 ? num+'' : '0'+num;
    }

    function bindToDoList(toDoList,readOnly){

         //$("#table-to-do-info tbody").html('')
        if(validateRequired(toDoList) && toDoList.length > 0){
           // tableStatus()
            generateToDoUI(readOnly ? toDoList.length : toDoList.length+1,readOnly,toDoList.length);
            if(readOnly) $("#submit-todo-list").empty();

            var table2 = $("#table-to-do-info");
            var rows = table2.children()[1].rows;

            for(var row=0; row<toDoList.length; row++){
                var dueDate = '';
                if(validateRequired(toDoList[row].dueDate)){
                    dueDate = new Date(toDoList[row].dueDate);
                    dueDate = dueDate.getFullYear()+'/'+getValidNumber(dueDate.getMonth()+1)+'/'+getValidNumber(dueDate.getDate())+' '+getValidNumber(dueDate.getHours())+':'+getValidNumber(dueDate.getMinutes());
                }
                $(rows[row]).attr('owner',isTaskOwner(toDoList[row].createdBy._id))
                $(rows[row]).attr('receiver',isTaskOwner(toDoList[row].assignedTo))
                $(rows[row]).attr('updateCount',toDoList[row].updateCount || 1)
                $(rows[row]).attr('googleEventId',toDoList[row].googleEventId);
                $(rows[row]).attr('status',toDoList[row].status);
                $(rows[row]).attr('assignedTo',toDoList[row].assignedTo);
                $(rows[row]).attr('actionItem',toDoList[row].actionItem);
                var cols = $(rows[row]).children();

                $($(cols[0])[0]).find('a').attr('href','/'+getValidUniqueUrl(toDoList[row].createdBy.publicProfileUrl));
                $($(cols[0])[0]).find('img').attr('src','/getImage/'+toDoList[row].createdBy._id);
                $($(cols[0])[0]).find('img').attr('title',toDoList[row].createdBy.firstName+' '+toDoList[row].createdBy.lastName);

                $($(cols[1])[0]).children().val(toDoList[row].actionItem || '');
                $($(cols[1])[0]).children().attr('_id',toDoList[row]._id);
                $($(cols[1])[0]).children().attr('readonly',!isTaskOwner(toDoList[row].createdBy._id));


                $($(cols[2])[0]).children().val(toDoList[row].assignedTo || 'invalid');
                $($(cols[2])[0]).children().attr('_id',toDoList[row]._id);
                $($(cols[2])[0]).children().attr('disabled',!isTaskOwner(toDoList[row].createdBy._id));

                $($(cols[3])[0]).children().val(dueDate || '');
                $($(cols[3])[0]).children().attr('_id',toDoList[row]._id);
                $($(cols[3])[0]).children().attr('oldDue',toDoList[row].dueDate);
                $($(cols[3])[0]).children().attr('createdBy',toDoList[row].createdBy._id);
                if(isTaskOwner(toDoList[row].createdBy._id) || loggedinUserProfile._id == toDoList[row].assignedTo){

                        $($(cols[3])[0]).children().attr('readonly',false);
                        $($(cols[3])[0]).children().datetimepicker({
                            dayOfWeekStart: 1,
                            lang: 'en',
                            minDate: new Date(),
                            onClose: function (dp,$input) {
                                // $("#startDateTime").val($("#startDateTimeImage").val())
                            }
                        });

                }

                if(loggedinUserProfile._id == toDoList[row].assignedTo || isTaskOwner(toDoList[row].createdBy._id)){
                    if(toDoList[row].status == 'complete'){
                        //$($(cols[3])[0]).children().attr('readonly',true);
                        $($(cols[4])[0]).children().attr('disabled',true);
                    }else{
                        $($(cols[4])[0]).children().attr('toDoId',toDoList[row]._id);
                        $($(cols[4])[0]).children().attr('googleEventId',toDoList[row].googleEventId);
                        $($(cols[4])[0]).children().attr('disabled',false);
                    }

                    $($(cols[4])[0]).children().attr('status',toDoList[row].status || 'notStarted');
                    $($(cols[4])[0]).children().val(toDoList[row].status || 'notStarted');
                }
                else{
                    $($(cols[4])[0]).children().attr('disabled',true);
                    $($(cols[4])[0]).children().val(toDoList[row].status || 'notStarted');
                }

                $($(cols[5])[0]).attr('toDoId',toDoList[row]._id);
                $($(cols[5])[0]).attr('googleEventId',toDoList[row].googleEventId);

                if(toDoList[row].status == 'complete' || loggedinUserProfile._id != toDoList[row].createdBy._id)
                    $($(cols[5])[0]).html('');
            }

            applyDataTable()
        }else if(!readOnly){
            generateToDoUI(1);
            applyDataTable()

        }else{
            $("#related-to-do-items").empty()
        }
    }

    function getUi(readOnly,old){
        var html = '<tr>';
        html += '<td class="no-border">';
        html += '<a target="_blank"><img class="owner" src="/images/default.png"></a>';
        html += '</td>';
        html += '<td class="no-border">';
        html +=  readOnly || old ? '<input type="text" id="actionItem" placeholder=" Action Item" class="no-border-width-100 form-control" readonly>' : '<input type="text" id="actionItem" placeholder=" Action Item" class="no-border-width-100 form-control">';
        html += ' </td>';
        html += ' <td class="no-border">';
        html += readOnly || old ? '<select id="selectOptions" class="todo-options form-control" disabled>' : '<select id="selectOptions" class="todo-options form-control">';
        html += ' <option value="invalid">Assign To</option>';
        html += ' </select>';
        html += ' </td>';
        html += ' <td class="no-border">';
        html += readOnly || old ? '<input type="text" placeholder=" Due Date" id="picker" class="action-due-date no-border-width-100 form-control" readonly>' : '<input type="text" placeholder=" Due Date" id="picker" class="action-due-date no-border-width-100 form-control">';
        html += ' </td>';
        html += ' <td class="no-border">';
        html += !readOnly ? '<select id="todoStatus" class="form-control" disabled>' : '<select id="todoStatus" class="form-control">';
        html += ' <option value="notStarted">Not started</option><option value="inProgress">In progress</option><option value="complete">Completed</option>';
        html += ' </select>';
        html += ' </td>';
        html += '<td class="no-border">';
        html += readOnly ? '' : '<span id="add-action-item" class="plus-sign" title="Add Action Item"></span>';
        html += ' </td>';
        html += ' </tr>';
        return html;
    }

    function generateToDoUI(numberOfBoxes,readOnly,length){

        for(var i=0; i<numberOfBoxes; i++){

            var row = $(getUi(readOnly,i < length));
            if(validateRequired(table)){
                addRowsToTable(row)
            }else
            $("#table-to-do-info tbody").append(row);
            var addBut = row.find('.action-due-date');
            row.find('.todo-options').append(participantsOptions);
            if(!readOnly && i == length || numberOfBoxes==1){
                addBut.datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'en',
                    minDate: new Date(),
                    onClose: function (dp,$input) {
                        // $("#startDateTime").val($("#startDateTimeImage").val())
                    }
                });
            }

            row.find('span').on('click',function(){
                generateToDoUI(1);
                $(this).parent().html('<span id="remove-action-item" class="cross-sign" title="Remove Action Item">&nbsp;X</span>')
            });

            if(i != numberOfBoxes-1){
                if(!readOnly){
                    row.find('span').parent().html('<span id="remove-action-item" class="cross-sign" title="Remove Action Item">&nbsp;X</span>')
                }
            }
        }

    }

    function addRowsToTable(rowArr){
        table.row.add( rowArr ).draw();
    }

    /*$("#add-action-item").on('click',function(){
        generateToDoUI(1);
    });*/

    $("body").on("keypress change","#actionItem, #selectOptions, #picker",function(){
        if(!validateRequired($(this).attr('_id'))){
            var parentTr = $(this).parent().parent();

            var cols = parentTr.children();
            var AItem = $($(cols[1])[0]).children().val();
            var option = $($(cols[2])[0]).children().val();
            var dueDate = $($(cols[3])[0]).children().val();
            var done = $($(cols[3])[0]).children().attr('done')

            if(validateRequired(AItem) && validateRequired(dueDate) && option != 'invalid' && done != 'done'){
                $($(cols[3])[0]).children().attr('done','done');
                $($(cols[5])[0]).html('<span id="add-action-item" class="plus-sign" title="Add Action Item">&nbsp;+</span>');
                $(this).parent().parent().find('#add-action-item').on('click',function(){
                    generateToDoUI(1);
                    $(this).parent().html('<span id="remove-action-item" class="cross-sign" title="Remove Action Item">&nbsp;X</span>')
                })
            }
        }
    });

    $("body").on("click", "#remove-action-item", function (e) {
        var toDoId = $(this).parent().attr('toDoId');
        var id = $(this).parent().attr('googleEventId');
        if(validateRequired(toDoId)){
            var reqData = {
                toDoId:toDoId,
                invitationId:eventP.invitationId,
                googleEventId:id
            };

            deleteToDoItem(reqData,$(this).parent().parent());
            $(this).parent().parent().html('<td class="no-border" style="width: 54%;background: rgb(223, 223, 223);" colspan="6"> Please Wait.... </td>')
        }
        else{
            deleteRow($(this).parent().parent())
        }
    });

    function deleteRow(row){
       table.row(row).remove().draw()
    }

    function deleteToDoItem(reqData,row){
        Pace.track(function() {
            $.ajax({
                url: '/meetings/todo/delete',
                type: 'POST',
                datatype: 'JSON',
                data: reqData,
                success: function (response) {
                    if (response) {
                        deleteRow(row);
                        showMessagePopUp("Action item have been successfully deleted.", 'success');
                    }
                    else showMessagePopUp("An error occurred. Please try again.", 'error');
                }
            })
        })
    }

/*  $("body").on("change", "#todoStatus", function (e) {

        var toDoId = $(this).attr('toDoId');
        var oldStatus = $(this).attr('status');
        var status = $(this).val();
        var element = $(this);
        if(validateRequired(oldStatus) && validateRequired(toDoId) && validateRequired(status)){
            if(status != oldStatus){
                var obj = {
                    toDoId:toDoId,
                    status:status,
                    invitationId:eventP.invitationId
                };
                Pace.track(function() {
                    $.ajax({
                        url: '/meetings/todo/update/status',
                        type: 'POST',
                        datatype: 'JSON',
                        data: obj,
                        success: function (response) {
                            if (response) {
                                element.attr('status',status);
                                if(status == 'complete'){
                                    element.attr('disabled','disabled');
                                    element.parent().parent().find('#remove-action-item').parent().html('');
                                }
                                showMessagePopUp("Action item status successfully updated.", 'success');
                            }
                            else showMessagePopUp("An error occurred. Please try again.", 'error');
                        }
                    })
                })
            }
        }

    });*/

    function applyDataTable(){

        table = $('#table-to-do-info').DataTable({
            "dom": '<"top"iflp<"clear">>',
            "order": [[ 1, "desc" ]],
            "fnDrawCallback": function(oSettings) {

                if (oSettings.fnRecordsTotal() < 11) {
                    $('#table-to-do-info_paginate').hide();
                }
                else  $('#table-to-do-info_paginate').show();
            },
            "columns": [
                { "bSortable": false },
                { "orderDataType": "dom-text", type: 'string' },
                { "orderDataType": "dom-select" },
                { "orderDataType": "dom-text", type: 'date' },
                { "orderDataType": "dom-select" },
                { "bSortable": false }
            ],
            "oLanguage": {
                "sEmptyTable": "No TODO List Found"
            }
        });
    }

    /* Create an array with the values of all the input boxes in a column */
    $.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('input', td).val();
        } );
    };

    /* Create an array with the values of all the select options in a column */
    $.fn.dataTable.ext.order['dom-select'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('select', td).val();
        } );
    };

    /* SEND MESSAGE TO PARTICIPANTS */

    $('#meeting-todo').on('click',function(){
        $("#todo-div").show();
        $("#send-mail-div").hide()
    });

    $('#send-mail').on('click',function(){
        $("#todo-div").hide()
        $("#send-mail-div").show()
    });

    switch(params.open){
        case 'todo':$('#meeting-todo').trigger('click');
            break;
        case 'mail':$('#send-mail').trigger('click');
            break;
    }

    function addData(){
        $("#userMessage").jqte();
        $("#showMoreMsgSubject").val('RE: '+eventP.scheduleTimeSlots[0].title);
    }

    function getParticipantsListWithMessage(msg){
        var messages = [];
        if(eventP.senderId != loggedinUserProfile._id){
            var receiver2 = {
                senderEmailId:loggedinUserProfile.emailId,
                userId:eventP.senderId || '',
                relatasContact:validateRequired(eventP.senderId),
                senderName:loggedinUserProfile.firstName+' '+loggedinUserProfile.lastName,
                receiverEmailId:eventP.senderEmailId,
                receiverName:eventP.senderName,
                //message:'Hi '+eventP.senderName+'<br>'+msg,
                message:msg,
                responseFlag:true,
                headerImage:false
            };
            messages.push(receiver2)
        }
        if (eventP.selfCalendar) {

            for (var k = 0; k < eventP.toList.length; k++) {
                if (eventP.toList[k].receiverId != loggedinUserProfile._id) {
                    var rFName = eventP.toList[k].receiverFirstName || '';
                    var rLName = eventP.toList[k].receiverLastName || '';
                    var receiver = {
                        senderEmailId:loggedinUserProfile.emailId,
                        userId:eventP.toList[k].receiverId || '',
                        relatasContact:validateRequired(eventP.toList[k].receiverId),
                        senderName:loggedinUserProfile.firstName+' '+loggedinUserProfile.lastName,
                        receiverEmailId:eventP.toList[k].receiverEmailId,
                        receiverName:rFName + ' ' + rLName,
                        //message:'Hi '+rFName+'<br>'+msg,
                        message:msg,
                        responseFlag:true,
                        headerImage:false
                    };
                    messages.push(receiver)
                }
            }
        }
        else{
            if (eventP.to.receiverId != loggedinUserProfile._id) {

                var receiver3 = {
                    senderEmailId:loggedinUserProfile.emailId,
                    userId:eventP.to.receiverId || '',
                    relatasContact:validateRequired(eventP.to.receiverId),
                    senderName:loggedinUserProfile.firstName+' '+loggedinUserProfile.lastName,
                    receiverEmailId:eventP.to.receiverEmailId,
                    receiverName:eventP.to.receiverName,
                    //message:'Hi '+eventP.to.receiverName+'<br>'+msg,
                    message:msg,
                    responseFlag:true,
                    headerImage:false
                };
                messages.push(receiver3)
            }
        }
        return messages;
    }

    $("#submit-send-email").on('click',function(){
        var subject = $("#showMoreMsgSubject").val();
        var msg = $("#userMessage").val().trim();
        /*if(!validateRequired(subject)){
            subject = 'RE: '+eventP.scheduleTimeSlots[0].title;
        }*/
        if(validateRequired(msg) && validateRequired(subject)){
                var msgs = getParticipantsListWithMessage(msg)

                if(validateRequired(msgs[0])){
                    var stringMsg = JSON.stringify(msgs);

                    $.ajax({
                        url:'/sendEmailMessage',
                        type:'POST',
                        datatype:'JSON',
                        data:{data:stringMsg,subject:subject},
                        success:function(response){

                            if(response){

                                showMessagePopUp("Your message has been sent successfully.",'success');
                                // table.row(mElement.parents('tr')).remove().draw();
                                $("#showMoreMsgSubject").val('');
                                $("#userMessage").jqteVal("");
                            }
                            else{
                                showMessagePopUp("Your message has not been sent. Please try again.",'error');
                            }
                        }
                    })
                }
        }
        else showMessagePopUp("You have forgotten to enter either Subject OR message. Please try again.",'error');
    })
});