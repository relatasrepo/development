$(document).ready(function () {

    $.ajaxSetup({cache: false});

    var documentsSharedByMe,userProfile,documentsUploaded,documentsSharedByYou,documentsSharedWithYou,domainName,s3bucket,docName,docUrl,AWScredentials;
    var monthNameSmall = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    sidebarHeight();

    $.ajax({
        url: '/getDomainName',
        type: 'GET',
        success: function (domain) {
            domainName = domain;
        }
    });

    getAWSCredentials();
    getUserProfile();

    function getAWSCredentials() {
        $.ajax({
            url: '/getAwsCredentials',
            type: 'GET',
            datatye: 'JSON',
            success: function (credentials) {

                AWScredentials = credentials;
                AWS.config.update(credentials);

                //AWS.config.region = "us-east-1";
                AWS.config.region = credentials.region;

                s3bucket = new AWS.S3({params: {Bucket: credentials.bucket}});
                docUrl = 'https://' + AWScredentials.bucket + '.s3.amazonaws.com/';
            }
        })
    }


    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            success: function (profile) {
                userProfile = profile;
                if(profile){
                    identifyMixPanelUser(profile);
                }
                getMyDocuments();
                getDocumentsSharedByOthers();
                $("#profilePic").attr('src', profile.profilePicUrl)
                imagesLoaded("#profilePic", function (instance, img) {
                    if (instance.hasAnyBroken) {
                        $('#profilePic').attr("src", "/images/default.png");
                    }
                });
            }
        })
    }

    function identifyMixPanelUser(profile){

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page":window.location.href
        });
    }
    // Function to get user own documents
    function getMyDocuments() {

        $.ajax({
            url: '/myDocuments',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {

                documentsSharedByMe = result.documents;
                var documents = result.documents;
                var documentsLength = documents.length;

                if (documentsLength > 0) {
                    displayDocumentsSharedByMe(documents);
                    $('#total-uploaded-documents').text('Total: ' + documentsLength);
                    var documentUploadedHtml = '';
                    for (var i = 0; i < documentsLength; i++) {
                        var sharedWithLength = documents[i].sharedWith.length;
                        var documentUrl = '/readDocument/' + documents[i]._id;
                        var docName = documents[i].documentName || '';
                        var openUnopenCount = getOpenedUnopened(documents[i])

                        documentUploadedHtml += '<tr class="odd"><td><a class="meeting-attachment1" href=' + documentUrl + '><img class="meeting-attachment-icon" src="/images/icon_file_pdf.png">';
                        documentUploadedHtml += '<span class="meeting-attachment-title1" title=' + docName.replace(/\s/g, '&nbsp;') + '>' + getTextLength(docName, 16) + '</span></a></td>';
                        documentUploadedHtml += '<td><span class="document-table-bold-val">' + getDateFormat(documents[i].sharedDate) + '</span></td>';
                        documentUploadedHtml += '<td>' + sharedWithLength + '</td>';
                        documentUploadedHtml += '<td>' + openUnopenCount.readCount + '</td>';
                        documentUploadedHtml += '<td>' + openUnopenCount.unreadCount + '</td>';
                        documentUploadedHtml += '<td><img id="docAnalytics" class="meeting-attachment-icon" src="/images/analytics.png" style="height: 30px;width: 30px; cursor: pointer" docId=' + documents[i]._id + '></td>';


                        documentUploadedHtml += '<td width="125"><a href="javascript:;" id="shareDocument" docId=' + documents[i]._id + ' class="document-table-btn">Click to share</a></td>';
                        documentUploadedHtml += '<td width="50"><a href="javascript:;" id="deleteDocument" awsKey=' + documents[i].awsKey + ' docId=' + documents[i]._id + ' docName=' + docName.replace(/\s/g, '&nbsp;') + ' class="document-table-icon-btn"><img src="/images/icon_delete.png" /></a></td></tr>';


                    }
                    $('#document-uploaded-table tbody').html(documentUploadedHtml);
                    sidebarHeight();
                    applyDataTableForDocumentsUploaded()
                } else displayNodocuments();

            },
            error: function (event, request, settings) {
                // alert('error');

            },
            timeout: 20000
        })
    }

    function displayNodocuments() {

        $('#document-uploaded-table tbody').html('<tr><td><span>No documents uploaded by you.</span></td></tr>');
        $('#documents-shared-by-you-table tbody').html('<tr><td><span>No documents shared by you.</span></td></tr>');
    }

    function getOpenedUnopened(document) {
        var sharedWithLength = document.sharedWith.length;
        var shareWith = document.sharedWith;
        var readCount = 0;
        if (sharedWithLength > 0) {
            for (var i in shareWith) {
                if (shareWith[i].accessCount > 0) {
                    readCount++;
                }
            }
        }
        var unreadCount = sharedWithLength - readCount;
        return {readCount: readCount, unreadCount: unreadCount}
    }

    var ijArray = []
    // Function to display user own documents details
    function displayDocumentsSharedByMe(documents) {
        ijArray = []
        var documentsLength = documents.length;

        var shareTotal = 0;
        var documentsSharedByMeHtml = '';
        for (var i = 0; i < documentsLength; i++) {
            var sharedWithLength = documents[i].sharedWith.length;
            var documentUrl = '/readDocument/' + documents[i]._id;
            for (var j = 0; j < sharedWithLength; j++) {
                var name = documents[i].sharedWith[j].firstName || '';
                name = name.replace(/\s/g, '-');
                shareTotal++;
                var expiryDates = checkRequired(documents[i].sharedWith[j].expiryDate) ? getDateFormat(documents[i].sharedWith[j].expiryDate) : ''
                var sharedDate = getDateFormat(documents[i].sharedWith[j].sharedOn || documents[i].sharedDate);
                documentsSharedByMeHtml += '<tr><td><a class="meeting-attachment1" href=' + documentUrl + '><img class="meeting-attachment-icon" src="/images/icon_file_pdf.png">';
                documentsSharedByMeHtml += '<span class="meeting-attachment-title1" title=' + documents[i].documentName.replace(/\s/g, '&nbsp;') + '>' + getTextLength(documents[i].documentName, 16) + '</span></a></td>';
                documentsSharedByMeHtml += '<td><a href="javascript:;" id="showAnalytics" docId=' + documents[i]._id + ' userId=' + documents[i].sharedWith[j].userId + ' class="document-table-btn document-table-btn1">View Analytics</a></td>';
                documentsSharedByMeHtml += '<td><span class="document-table-regular-val" id=' + 'titleSMe' + i + j + '>' + getTextLength(documents[i].sharedWith[j].firstName || documents[i].sharedWith[j].emailId, 16) + '</span></td>';
                documentsSharedByMeHtml += '<td><span class="document-table-regular-val">' + sharedDate + '</span></td>';
                documentsSharedByMeHtml += '<td><img docId=' + documents[i]._id + ' shareWithId=' + documents[i].sharedWith[j]._id + ' id=' + 'datePicker' + i + j + ' style="cursor: pointer" src="/images/icon_calendar_red.png" />&nbsp;<span class="document-table-regular-val" id=' + 'showDatepickerDate' + i + j + ' >' + expiryDates + '</span></td>';
                if (documents[i].sharedWith[j].accessInfo) {
                    if (documents[i].sharedWith[j].accessInfo.pageReadTimings) {
                        if (documents[i].sharedWith[j].accessInfo.pageReadTimings[0]) {
                            documentsSharedByMeHtml += '<td></td>'
                        } else documentsSharedByMeHtml += '<td style="text-align: center;" ><a style="cursor: pointer" id="resendDoc" docId=' + documents[i]._id + ' shareEmail=' + documents[i].sharedWith[j].emailId + ' name=' + name + ' class="document-table-btn document-table-btn1">Resend</a></td>'
                    } else documentsSharedByMeHtml += '<td style="text-align: center;" ><a style="cursor: pointer" id="resendDoc" docId=' + documents[i]._id + ' shareEmail=' + documents[i].sharedWith[j].emailId + ' name=' + name + ' class="document-table-btn document-table-btn1">Resend</a></td>'
                } else documentsSharedByMeHtml += '<td style="text-align: center;" ><a style="cursor: pointer" id="resendDoc" docId=' + documents[i]._id + ' shareEmail=' + documents[i].sharedWith[j].emailId + ' name=' + name + ' class="document-table-btn document-table-btn1">Resend</a></td>'

                var ij = {
                    i: i,
                    j: j,
                    name: documents[i].sharedWith[j].firstName || documents[i].sharedWith[j].emailId
                }
                ijArray.push(ij)
                if (documents[i].sharedWith[j].accessStatus == true) {
                    documentsSharedByMeHtml += '<td style="text-align: center"><input id="changeAccess" docId=' + documents[i]._id + ' userId=' + documents[i].sharedWith[j].userId + ' type="checkbox" checked /></td></tr>';
                } else documentsSharedByMeHtml += '<td style="text-align: center"><input id="changeAccess" docId=' + documents[i]._id + ' userId=' + documents[i].sharedWith[j].userId + ' type="checkbox" /></td></tr>';
            }
            /*  if(sharedWithLength <=0 ){
             documentsSharedByMeHtml = '<tr><td><span>No documents shared by you.</span></td></tr>';
             }*/
        }
        $('#your-total-uploaded-documents').text('Total: ' + shareTotal);
        $('#documents-shared-by-you-table tbody').html(documentsSharedByMeHtml);
        setTitleGetDocumentsSharedByMeTitleName()
        generateDateTimePicker();
        applyDataTableForDocumentsSharedByYou()
    }


    function setTitleGetDocumentsSharedByMeTitleName() {
        for (var i in ijArray) {
            $("#titleSMe" + ijArray[i].i + '' + ijArray[i].j).attr("title", ijArray[i].name)
        }
    }

    function setTitleGetDocumentsSharedByOthersTitleName() {
        for (var i in getDocumentsSharedByOthersTitleName) {
            $("#titleName" + getDocumentsSharedByOthersTitleName[i].i).attr("title", getDocumentsSharedByOthersTitleName[i].name)
        }
    }

    var getDocumentsSharedByOthersTitleName = [];

    function getDocumentsSharedByOthers() {
        getDocumentsSharedByOthersTitleName = []
        $.ajax({
            url: '/documentsSharedWithMe',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {

                var documents = result.documentsSharedWithMe;
                var documentsLength = documents.length;
                var sharedOn;
                if (documentsLength > 0) {
                    $('#documents-shared-with-you').text('Total: ' + documentsLength);

                    var documentsSharedWithMeHtml = '';
                    for (var i = 0; i < documentsLength; i++) {
                        var documentUrl = '/readDocument/' + documents[i]._id;
                        var sharedWithPerson;
                        var expiryDates;
                        var dateNow = new Date();
                        dateNow.setHours(0, 0, 0, 0);

                        for (var j in documents[i].sharedWith) {
                            if (checkRequired(userProfile)) {
                                if (documents[i].sharedWith[j].userId == userProfile._id) {
                                    sharedWithPerson = documents[i].sharedWith[j];
                                }

                            }
                        }

                        if (checkRequired(sharedWithPerson)) {
                            sharedOn = getDateFormat(sharedWithPerson.sharedOn || documents[i].sharedDate)
                            expiryDates = checkRequired(sharedWithPerson.expiryDate) ? new Date(sharedWithPerson.expiryDate) : 'None';
                        } else sharedOn = getDateFormat(documents[i].sharedDate)
                        if (expiryDates != 'None') {

                            expiryDates = expiryDates >= dateNow ? getDateFormat(expiryDates) : 'Expired';
                        }
                        documentsSharedWithMeHtml += '<tr><td><a class="meeting-attachment1" href=' + documentUrl + '><img class="meeting-attachment-icon" src="/images/icon_file_pdf.png">';
                        documentsSharedWithMeHtml += '<span class="meeting-attachment-title1" title=' + documents[i].documentName.replace(/\s/g, '&nbsp;') + '>' + getTextLength(documents[i].documentName, 16) + '</span></a></td>';
                        documentsSharedWithMeHtml += '<td><span class="document-table-regular-val" id=' + 'titleName' + i + '>' + getTextLength(documents[i].sharedBy.firstName, 16) + '</span></td>';
                        documentsSharedWithMeHtml += '<td><span class="document-table-regular-val">' + sharedOn + '</span></td>';
                        documentsSharedWithMeHtml += '<td style="text-align: left"><span class="document-table-regular-val">' + expiryDates + '</span></td></tr>';
                        getDocumentsSharedByOthersTitleName.push({
                            name: documents[i].sharedBy.firstName,
                            i: i
                        })
                    }
                    $('#documents-shared-with-me tbody').html(documentsSharedWithMeHtml)
                    setTitleGetDocumentsSharedByOthersTitleName()
                    sidebarHeight();
                    applyDataTableForDocumentsSharedWithYou()
                } else displayNodocumentsSharedWithMe();
            },
            error: function (event, request, settings) {


            },
            timeout: 20000
        })

    }

    function displayNodocumentsSharedWithMe() {
        $('#documents-shared-with-me tbody').html('<tr><td><span>No documents shared  with you</span></td></tr>');
    }

    var popoverSource;
    $("body").on("click", "#showAnalytics", function () {
        if (popoverSource != null) {
            popoverSource.popover('destroy');
            popoverSource = null;
            // return;
        }
        //$(".popover").remove();
        var html = _.template($("#analytics-template-details").html())();
        $(this).popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $(".popover").css({width: '323px'});
        $(this).popover('show');
        popoverSource = $(this);
        var docId = $(this).attr('docId');
        var userId = $(this).attr('userId');
        $.ajax({
            url: '/analytics/' + docId + '/' + userId,
            type: 'GET',
            traditional: true,
            success: function (result) {
                if (result == false) {
                    $("#analyticMsg").text("This document has not been accessed yet.");
                }
                else {
                    var prt = result.accessInfo.pageReadTimings;
                    var abc = prt.slice()
                    var prtNoDups = removeDuplicates(abc);
                    //console.log(removeDuplicates(prt));
                    var separate = [];
                    for (var i in prtNoDups) {

                        var totalTime = 0;
                        var count = 0;
                        var pageNo = 0;
                        for (var j in prt) {
                            if (prt[j].pageNumber == prtNoDups[i].pageNumber) {

                                totalTime = totalTime + parseFloat(prt[j].Time);
                                count += 1;
                                pageNo = prtNoDups[i].pageNumber;
                            }
                        }
                        //  if(totalTime > 0){
                        var before = totalTime;

                        // totalTime = totalTime/count
                        totalTime = Math.round(totalTime / 60) // im minutes
                        if (totalTime == 0) {
                            if (before > 0) {
                                totalTime = 0.5
                            }
                        }
                        // }

                        var tootlTip = 'Page:' + pageNo + '\nMinutes:' + totalTime;
                        separate.push([
                            pageNo,
                            totalTime,
                            tootlTip,
                            'color:#03A2EA'
                        ])
                        var timeMax = 0;
                        jQuery.map(separate, function (obj) {
                            if (obj[1] > 0)
                                timeMax = obj[1];
                        });
                    }
                    if (checkRequired(separate[0])) {

                        drawAnalytics(separate, prtNoDups.length, timeMax)
                    }
                    else {
                        if (popoverSource)
                            popoverSource.popover('destroy');
                        showMessagePopUp("This document not accessed yet.", 'error')
                    }
                    $("#firstAccessed").text("First Accessed: " + getDateFormat(result.firstAccessed) + ' ' + getTimeFormat(result.firstAccessed));
                    $("#lastAccessed").text("Last Accessed : " + getDateFormat(result.lastAccessed) + ' ' + getTimeFormat(result.lastAccessed));
                    $("#accessCount").text("Number of times accessed: " + result.accessCount || 0);
                    $("#analyticsDetails").show()
                    updateDocAccessed(docId,userId);
                }
            }
        });
    });

    $("body").on("click", "#closePopup-analytics", function () {
        // $(".popover").remove();
        popoverSource.popover('destroy');
    });

    function getTextLength(text, maxLength) {
        var textLength;
        if (checkRequired(text)) {
            textLength = text.length;
        }
        else {
            text = '';
            textLength = 0;
        }

        if (textLength > maxLength) {
            var formattedText = text.slice(0, maxLength)
            return formattedText + '..';
        }
        else return text;
    }

    function sortArray(arr) {
        var end = arr.length, val = 0;
        for (var i = 0; i < end; i++) {
            for (var j = i + 1; j < end; j++) {
                if (arr[i].pageNumber > arr[j].pageNumber) {
                    val = arr[i];
                    arr[i] = arr[j];
                    arr[j] = val;
                }
            }
        }
        return arr;
    }


    function generateDateTimePicker() {
        setTimeout(function () {
            for (var i in ijArray) {

                $("#datePicker" + ijArray[i].i + '' + ijArray[i].j).datetimepicker({
                    timepicker: false,
                    dayOfWeekStart: 1,
                    lang: 'en',
                    minDate: new Date(),

                    onClose: function (dp, $input) {
                        var dateNow = new Date()
                        var selectedDate = new Date($input.val())
                        if (checkRequired($input.val())) {
                            selectedDate.setHours(0, 0, 0, 0)
                            dateNow.setHours(0, 0, 0, 0)
                            var expObj = {
                                expiryDate: selectedDate,
                                expirySec: Math.round((selectedDate - dateNow) / 1000),
                                docId: $input.attr('docId'),
                                sharedWithId: $input.attr('shareWithId')
                            }

                            $("#" + $input.attr('id').replace('datePicker', 'showDatepickerDate')).text(getDateFormat(selectedDate))
                            setExpiryDate(expObj)
                        }

                    }
                });

            }
        }, 1000)
    }


    function setExpiryDate(expObj) {

        $.ajax({
            url: '/setDocExpiry',
            type: 'POST',
            datatype: 'JSON',
            data: expObj,
            success: function (response) {
                if (!response) {
                    showMessagePopUp('Updating expiry date failed', 'error');
                }
                else {
                    if (checkRequired(documentsUploaded))
                        documentsUploaded.destroy()
                    //getMyDocuments();
                }
            }
        })
    }


    $("body").on("change", "#changeAccess", function () {

        $.ajax({
            url: '/changeAccess/' + $(this).attr('docId') + '/' + $(this).attr('userId') + '/' + $(this).is(':checked'),
            type: 'GET',
            traditional: true,
            success: function (result) {
                if (!result) {
                    //showMessagePopUp("Failed to change doc access",'error')
                    //alert('Failed to change doc access');

                    showMessagePopUp("The user has not checked the document yet. If you wish to revoke access please set the expiry date accordingly.", 'error')
                    $(this).attr('checked', !$(this).is(':checked'))
                }
            }
        });
    });

    var awsKey2;
    var docId2;
    var docName2;
    var element2;

    $("body").on("click", "#deleteDocument", function () {
        awsKey2 = $(this).attr('awsKey');
        docId2 = $(this).attr('docId');
        docName2 = $(this).attr('docName');
        element2 = $(this);

        if (popoverSource != null) {
            popoverSource.popover('destroy');
            popoverSource = null;
            //return;
        }
        var docId = $(this).attr("docId");
        //$(".popover").remove();
        var html = _.template($("#delete-popup").html())();
        $(this).popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });

        $(this).popover('show');
        $('.arrow').hide()
        popoverSource = $(this);


    });

    $("body").on("click", "#deleteDoc", function () {

        if (checkRequired(awsKey2) && checkRequired(docId2) && checkRequired(docName2) && checkRequired(element2)) {
            if (awsKey2 == undefined || awsKey2 == null) {
                popoverSource.popover('destroy');
                deleteDocumentFromDatabase(docId2, docName);
            }
            else {
                var params = {
                    Bucket: AWScredentials.bucket, // required
                    Key: awsKey2 // required
                };
                s3bucket.deleteObject(params, function (err, data) {
                    if (err) {
                        showMessagePopUp("document deletion failed", 'error')
                        //alert('document deletion failed');
                    }
                    else {
                        popoverSource.popover('destroy');
                        deleteDocumentFromDatabase(docId2, docName, element2);
                    }
                });
            }
        }
    })

    $("body").on("click", "#closePopup-delete", function () {
        // $(".popover").remove();
        popoverSource.popover('destroy');
    })
    function deleteDocumentFromDatabase(docId, docName, element) {

        $.ajax({
            url: '/deleteDocument/' + docId + '/' + docName,
            type: 'GET',
            traditional: true,
            success: function (result) {
                if (!result) {
                    showMessagePopUp("document deletion failed", 'error')
                    //alert('document deletion failed');
                    awsKey2 = null;
                    docId2 = null;
                    docName2 = null;
                    element2 = null;
                }
                else {
                    showMessagePopUp("document successfully deleted", 'success')
                    //alert('document successfully deleted');
                    if (checkRequired(documentsUploaded)) {
                        documentsUploaded.row(element.parents('tr')).remove().draw();
                    }
                    awsKey2 = null;
                    docId2 = null;
                    docName2 = null;
                    element2 = null;
                }
            }
        });
    }

    var popoverSource2;
    $("body").on("click", "#shareDocument", function () {
        if (popoverSource2 != null) {
            popoverSource2.popover('destroy');
            popoverSource2 = null;
            //return;
        }
        var docId = $(this).attr("docId");
        //$(".popover").remove();
        var html = _.template($("#shareDocument-template-details").html())();
        $(this).popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });

        $(this).popover('show');
        popoverSource2 = $(this);
        var comName = userProfile.companyName || '';
        var userUrl = domainName + '/' + getValidUniqueUrl(userProfile.publicProfileUrl);
        $("#shareDocument-submit").attr("docId", docId);
        $("#userMsgShare").val("Hi, \nI'm " + userProfile.firstName + " from " + comName + ". I've shared the below attached document with you through Relatas which I would like you to review and provide feedback. Post which I would like to schedule a call with you. \nYou may schedule a time with me on my Relatas calendar at: " + userUrl + '.\nThank you\n' + userProfile.firstName);

        $("#shareDocumentDatePicker").datetimepicker({
            timepicker: false,
            format: 'Y/m/d',
            dayOfWeekStart: 1,
            lang: 'en',
            minDate: new Date()
        });
    });

    $("body").on("click", "#closePopup-shareDocument", function () {
        // $(".popover").remove();
        popoverSource2.popover('destroy');
    })

    function getValidUniqueUrl(uniqueName) {
        var url = window.location + ''.split('/');
        var patt = new RegExp(url[2]);
        if (patt.test(uniqueName)) {

            url = uniqueName.split('/');
            for (var i = url.length - 1; i >= 0; i--) {
                if (url[i] == 'u') {
                    uniqueName = url[i + 1];
                }
            }
            return uniqueName;

        } else {
            return uniqueName;
        }
    }

    $("body").on("click", "#shareDocument-submit", function () {
        var docId = $(this).attr("docId");
        var shareEmails = $("#shareDocumentEmails").val();
        var shareFirstName = $("#shareDocumentFirstName").val();
        var shareLastName = $("#shareDocumentLastName").val();
        var msg = $("#userMsgShare").val();
        var dateNow = new Date()
        var selectedDate = $("#shareDocumentDatePicker").val()
        var sec;
        dateNow.setHours(0, 0, 0, 0)
        if (selectedDate != '') {
            selectedDate = new Date(selectedDate)
            selectedDate.setHours(0, 0, 0, 0)
            sec = Math.round((selectedDate - dateNow) / 1000)
        }
        else {
            selectedDate = 'noDate';
            sec = 'noSec';
        }


        if (checkRequired(shareEmails) && checkRequired(shareFirstName) && checkRequired(shareLastName) && checkRequired(msg)) {
            if (checkRequired(shareEmails) && validateEmailField(shareEmails)) {
                shareEmails = shareEmails.toLowerCase();
                var details = {
                    docId: docId,
                    shareEmails: shareEmails,
                    personName: shareFirstName + ' ' + shareLastName,
                    relatasContact: true,
                    userMsg: $.nl2br(msg),
                    sharedOn: new Date(),
                    expiryDate: selectedDate,
                    expirySec: sec
                }


                $(".popover").remove();
                shareDocumentAjaxCall(details, true);
            }
            else {
                showMessagePopUp("Please enter valid email id", 'error');
                //  alert('Please specify email to share this document');
            }
        }
        else {
            showMessagePopUp("Fields with * is mandatory.", 'error');
        }
    });

    $("body").on("click", "#resendDoc", function () {
        var docId = $(this).attr("docId");
        var firstName = $(this).attr("name");
        firstName = firstName.replace(/\-/g, ' ');
        var shareEmails = $(this).attr("shareEmail");
        var msg = "Remainder for already shared document";
        var subject = "Document resend"
        var details = {
            docId: docId,
            shareEmails: shareEmails,
            personName: firstName,
            relatasContact: true,
            userMsg: $.nl2br(msg),
            subject: subject,
            sharedOn: new Date(),
            expiryDate: 'noDate',
            expirySec: 'noSec',
            resend: true
        }

        shareDocumentAjaxCall(details, false)
    })

    function shareDocumentAjaxCall(dataToShare, share) {
        $.ajax({
            url: '/shareDocument',
            type: 'POST',
            datatype: 'JSON',
            data: dataToShare,
            success: function (docShareInfo) {
                // True or false or error.
                if (docShareInfo == 'exist') {
                    mixpanelTrack("Share Document");
                    mixpanelIncrement("# Doc S");
                    if (share) {
                        dShareFlag = true;
                        showMessagePopUp("Congratulations your document  has been sent.", 'success');
                    }
                    else {
                        showMessagePopUp("Congratulations your document resend success.", 'success');
                    }
                } else if (!docShareInfo) {
                    if (share) {
                        showMessagePopUp("Sharing document failed", 'error');
                        // alert('Sharing document failed');
                    }
                    else {
                        showMessagePopUp("Document resend failed", 'error');
                    }
                }
                else {
                    mixpanelTrack("Share Document");
                    mixpanelIncrement("# Doc S");
                    if (share) {
                        dShareFlag = true;
                        showMessagePopUp("Congratulations your document  has been sent.", 'success');
                    }
                    else {
                        showMessagePopUp("Congratulations your document resend success.", 'success');
                    }
                }

            }
        })
    }

    jQuery.nl2br = function (varTest) {
        return varTest.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
    };
    var dShareFlag = false;
    var uploadFlag = true;
    $("#addFile").on("click", function () {
        if (uploadFlag) {
            $("#selectShareFile").trigger("click");

        } else {
            showMessagePopUp("Please wait, File is uploading", 'error')
        }
    });
    var fileChooser = document.getElementById('selectShareFile');
    $('#selectShareFile').on('change', function () {


        var file = fileChooser.files[0];
        if (file) {

            var dateNow = new Date();
            var d = dateNow.getDate();
            var m = dateNow.getMonth();
            var y = dateNow.getFullYear();
            var hours = dateNow.getHours();
            var min = dateNow.getMinutes();
            var sec = dateNow.getSeconds();
            var timeStamp = y + ':' + m + ':' + d + ':' + hours + ':' + min + ':' + sec;
            var dIdentity = timeStamp + '_' + file.name;
            var docNameTimestamp = dIdentity.replace(/\s/g, '');
            docName = file.name;
            $("#docProgress").css({display: 'block'});
            uploadFlag = false;
            var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
            var request = s3bucket.putObject(params);
            console.log("S3 bucket:", request);

            request.on('httpUploadProgress', function (progress) {

                $("#docProgress").val(progress.loaded / progress.total * 100)
            });
            request.send(function (err, data) {
                if (err) {
                    uploadFlag = true;
                    console.log(err)
                    showMessagePopUp("Doc upload failed", 'error')
                    //alert('Doc upload failed')
                }
                else {
                    uploadFlag = true;
                    var emails = $("#shareEmails").val();
                    docUrl = docUrl + docNameTimestamp;
                    var docD = {
                        documentName: docName,
                        documentUrl: docUrl,
                        awsKey: docNameTimestamp,
                        participants: emails + ',h',
                        invitationId: 'no',
                        sharedDate: new Date()
                    }

                    $.ajax({
                        url: '/uploadDocument',
                        type: 'POST',
                        datatype: 'JSON',
                        data: docD,
                        traditional: true,
                        success: function (result) {
                            window.location.reload();
                        }
                    })
                }
            });
        } else {
            showMessagePopUp("Please select file to upload.", 'error')
            //alert('Please select file to upload.');
        }
    })

//Function to show Analytics for documents shared by me
    function showAnalyticsForDocumentsSharedByMe(docId, firstName) {
        documentsSharedByMe.forEach(function (document) {
            if (document._id == docId) {
                document.sharedWith.forEach(function (shared) {
                    if (shared.firstName == firstName) {
                        // in "shared.accessInfo" contains document access info
                    }
                });
            }
        });
    }

    function showMessagePopUp(message, msgClass) {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");

        $(".popover").css({'margin-top': '25%'})

        if (msgClass == 'error') {
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        } else if (msgClass == 'success') {
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        } else if (msgClass == 'tip') {
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        //setTimeout(function(){
        $("#message").text(message)
        //},1000);
        $("#closePopup-message").focus();

    }

    $("body").on("click", "#closePopup-message", function () {
        $("#user-name").popover('destroy');
        if (dShareFlag) {
            window.location.reload()
        }
    });

    function removeDuplicates(arr) {
        var end = arr.length;

        for (var i = 0; i < end; i++) {
            for (var j = i + 1; j < end; j++) {
                if (arr[i].pageNumber == arr[j].pageNumber) {
                    var shiftLeft = j;
                    for (var k = j + 1; k < end; k++, shiftLeft++) {
                        arr[shiftLeft] = arr[k];
                    }
                    end--;
                    j--;
                }
            }
        }

        var whitelist = [];
        for (var i = 0; i < end; i++) {
            whitelist[i] = arr[i];
        }
        //whitelist.reverse();
        return whitelist;
    }

    function getTimeFormat(date) {
        var dateObj = new Date(date)
        var hrs = dateObj.getHours();
        var min = dateObj.getMinutes();
        var meridian;
        /*  if(hrs > 12){
         hrs -= 12;
         meridian = 'PM';
         }
         else {
         meridian = 'AM';
         }*/
        if (min < 10) {
            min = "0" + min;
        }
        ;
        var time = hrs + ":" + min + " ";
        return time;
    }

    function getDateFormat(date) {
        var dateObj = new Date(date)
        return dateObj.getDate() + ' ' + monthNameSmall[dateObj.getMonth()] + ',' + dateObj.getFullYear()
    }

    function checkRequired(data) {
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else {
            return true;
        }
    }

    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {
            return true;
        } else {

            return false;
        }
    }


    function sidebarHeight() {
        var sideHeight = $(document).height()
        $("#sidebarHeight").css({'min-height': sideHeight})
    }


    $("body").on("click", "#docAnalytics", function () {
        if (popoverSource)
            popoverSource.popover('destroy');
        var docId = $(this).attr("docId");
        if (checkRequired(docId)) {
            var html = _.template($("#document-analytics-template").html())();
            $(this).popover({
                title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
                html: true,
                content: html,
                container: 'body',
                placement: 'bottom',
                id: "myPopover"
            });
            $(this).popover('show');
            $(".popover").css({'width': '45%'})
            $(".popover").css({'left': '65%'})
            popoverSource = $(this)
            $("#closePopup-document-graph").focus()
            getDocById(docId)

        }

    })

    $("body").on("click", "#closePopup-document-graph", function () {
        if (popoverSource)
            popoverSource.popover('destroy');
    });


    $("body").on("click", "#closePopup-document-analytics", function () {
        if (popoverSource)
            popoverSource.popover('destroy');
    });

    function getDocById(docId) {
        $.ajax({
            url: '/getDoc/' + docId,
            type: 'GET',
            traditional: true,
            success: function (document) {
                if (document == false || !checkRequired(document)) {
                    if (popoverSource)
                        popoverSource.popover('destroy');
                    showMessagePopUp("Error in fetching doc access info.", 'error')

                }
                else {
                    generateTotalDocReadStatusTable(document)
                    // generateTotalDocReadStatus(document)
                }
            }
        })
    }


    function generateTotalDocReadStatusTable(document) {
        if (checkRequired(document)) {
            var peopleViewed = getOpenedUnopened(document);
            var totalViewes = 0;
            var sharedWithArr = document.sharedWith;
            var tableHtml = ''
            if (checkRequired(sharedWithArr[0])) {
                for (var i = 0; i < sharedWithArr.length; i++) {
                    totalViewes += sharedWithArr[i].accessCount || 0;
                    var name = sharedWithArr[i].firstName || '';
                    var views = getValidNum(sharedWithArr[i].accessCount || 0);
                    var lastViewed = checkRequired(sharedWithArr[i].lastAccessed) ? getDate_Format(sharedWithArr[i].lastAccessed) : 'Not viewed';


                    tableHtml += '<tr><td style="width: 30%" title=' + name.replace(/\s/g, '&nbsp;') + '>' + getTextLength(name, 10) + '</td><td style="text-align: center;width: 30%">' + views + '</td><td style="text-align: center;width: 40%">' + lastViewed + '</td></tr>'
                }

            }
            $("#peopleViewed").text(getValidNum(peopleViewed.readCount));
            $("#totalViews").text(getValidNum(totalViewes));
            $("#dTable tbody").html(tableHtml);
        }
        else {
            if (popoverSource)
                popoverSource.popover('destroy');
        }
    }

    function updateDocAccessed(docId,userId){
        var obj = {
             userId:userId,
             docId:docId
        }

        if(checkRequired(docId) && checkRequired(userId)){
            $.ajax({
                url: '/documents/updateAccessed',
                type: 'POST',
                datatype: 'JSON',
                data: obj,
                success: function (result){

                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            })
        }
    }

    function getValidNum(num) {
        if (num < 10) {
            return '0' + num
        }
        else return num
    }

    function getDate_Format(date) {
        var dateObj = new Date(date)
        return dateObj.getDate() + '-' + monthNameSmall[dateObj.getMonth()] + '-' + dateObj.getFullYear()
    }

    function generateTotalDocReadStatus(document) {
        if (checkRequired(document)) {
            var totalPrt = [];
            var timesAccessed = 0;
            var sharedWithArr = document.sharedWith;
            if (checkRequired(sharedWithArr[0])) {

                for (var i = 0; i < sharedWithArr.length; i++) {
                    if (sharedWithArr)
                        var localArr = []
                    var prt = sharedWithArr[i].accessInfo.pageReadTimings;
                    if (checkRequired(sharedWithArr[i].accessCount)) {
                        timesAccessed += sharedWithArr[i].accessCount;
                    }
                    if (checkRequired(prt[0])) {
                        for (var l = 0; l < prt.length; l++) {
                            prt[l].user = i;
                            localArr.push(prt[l])

                        }
                    }

                    totalPrt = totalPrt.concat(localArr)
                }
                generateReadAnalytics(totalPrt, timesAccessed)
            } else {
                if (popoverSource)
                    popoverSource.popover('destroy');
                showMessagePopUp("This document not shared yet.", 'error')
            }
        }
    }

    function generateReadAnalytics(totalPrt, timesAccessed) {

        var abc = totalPrt.slice();

        var totalPrtNoDuplicates = removeDuplicates(abc);

        var totalAnalytics = [];
        for (var j in totalPrtNoDuplicates) {
            var totalTime = 0;
            var count = 0;
            var pageNo = 0;
            var array = [];
            for (var k in totalPrt) {

                if (totalPrt[k].pageNumber == totalPrtNoDuplicates[j].pageNumber) {
                    totalTime += parseFloat(totalPrt[k].Time);
                    array.push(totalPrt[k].user)
                    count += 1;
                    pageNo = totalPrtNoDuplicates[j].pageNumber;
                }
            }


            var before = totalTime;
            array = distinct(array)
            count = array.length

            totalTime = totalTime / count
            totalTime = Math.round(totalTime / 60)// im minutes
            if (totalTime == 0) {
                if (before > 0) {
                    totalTime = 0.5
                }
            }

            var tootlTip = 'Page:' + pageNo + '\nMinutes:' + totalTime;
            totalAnalytics.push([
                pageNo,
                totalTime,
                tootlTip,
                'color:#03A2EA'
            ])
            var timeMax = 0;
            jQuery.map(totalAnalytics, function (obj) {
                if (obj[1] > 0)
                    timeMax = obj[1];
            });
        }
        if (checkRequired(totalAnalytics[0])) {
            $("#accessCount-document-graph").text('Number of times accessed: ' + timesAccessed)
            drawSort(totalAnalytics, totalPrtNoDuplicates.length, timeMax)
        }
        else {
            if (popoverSource)
                popoverSource.popover('destroy');
            showMessagePopUp("This document not accessed yet.", 'error')
        }
    }

    function distinct(arr) {
        var end = arr.length;

        for (var i = 0; i < end; i++) {
            for (var j = i + 1; j < end; j++) {
                if (arr[i] == arr[j]) {
                    var shiftLeft = j;
                    for (var k = j + 1; k < end; k++, shiftLeft++) {
                        arr[shiftLeft] = arr[k];
                    }
                    end--;
                    j--;
                }
            }
        }

        var whitelist = [];
        for (var i = 0; i < end; i++) {
            whitelist[i] = arr[i];
        }
        return whitelist;
    }

    function drawSort(a, maxPages, timeMax) {

        var data = new google.visualization.DataTable();

        data.addColumn('number', 'Pages');
        data.addColumn('number', 'Minutes');
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addColumn({type: 'string', role: 'style'});
        data.addRows(a);
        var options = {
            legend: {title: 'Min', backgroundColor: '#03A2EA'},
            hAxis: {title: 'Page', minValue: 0, maxValue: maxPages + 2},
            vAxis: {title: 'Average Page Open Time', minValue: 0, maxValue: timeMax + 2}
        };


        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_sort_div'));
        chart.draw(data, options);

    }

    function drawAnalytics(a, maxPages, timeMax) {

        console.log('Inside draw analytics');
        console.log("A:", a);
        console.log("Max pages:", maxPages);
        console.log("timeMax:", timeMax);

        var data = new google.visualization.DataTable();

        data.addColumn('number', 'Pages');
        data.addColumn('number', 'Minutes');
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addColumn({type: 'string', role: 'style'});
        data.addRows(a);
        var options = {
            width: 250,
            hAxis: {title: 'Page', minValue: 0, maxValue: maxPages + 2},
            vAxis: {title: 'Total Page Open Time', minValue: 0, maxValue: timeMax + 2}
        };


        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('analyticsGraph'));
        chart.draw(data, options);

    }

    // Script for table pagination
    function applyDataTableForDocumentsUploaded() {
        documentsUploaded = $('#document-uploaded-table').DataTable({

            "dom": '<"top"iflp<"clear">>',
            "fnDrawCallback": function (oSettings) {
                if (checkRequired(documentsUploaded)) {
                    if (documentsUploaded.rows() < 11) {
                        $('#document-uploaded-table_paginate').hide();
                    }
                    else  $('#document-uploaded-table_paginate').show();
                } else if ($('#document-uploaded-table tr').length < 11) {
                    $('#document-uploaded-table_paginate').hide();
                }
                else  $('#document-uploaded-table_paginate').show();
            },
            "oLanguage": {
                "sEmptyTable": "No documents"
            },
            "order": [[0, "asc"]]
        });

    }

    function applyDataTableForDocumentsSharedByYou() {
        documentsSharedByYou = $('#documents-shared-by-you-table').DataTable({

            "dom": '<"top"iflp<"clear">>',
            "fnDrawCallback": function (oSettings) {
                if (checkRequired(documentsSharedByYou)) {
                    if (documentsSharedByYou.rows() < 11) {
                        $('#documents-shared-by-you-table_paginate').hide();
                        meetingsArr
                    }
                    else  $('#documents-shared-by-you-table_paginate').show();
                } else if ($('#documents-shared-by-you-table tr').length < 11) {
                    $('#documents-shared-by-you-table_paginate').hide();
                }
                else  $('#documents-shared-by-you-table_paginate').show();
            },
            "oLanguage": {
                "sEmptyTable": "No documents"
            },
            "order": [[0, "asc"]]
        });

    }

    function applyDataTableForDocumentsSharedWithYou() {
        documentsSharedWithYou = $('#documents-shared-with-me').DataTable({

            "dom": '<"top"iflp<"clear">>',
            "fnDrawCallback": function (oSettings) {
                if (checkRequired(documentsSharedWithYou)) {
                    if (documentsSharedWithYou.rows() < 11) {
                        $('#documents-shared-with-me_paginate').hide();
                    }
                    else  $('#documents-shared-with-me_paginate').show();
                } else if ($('#documents-shared-with-me tr').length < 11) {
                    $('#documents-shared-with-me_paginate').hide();
                }
                else  $('#documents-shared-with-me_paginate').show();
            },
            "oLanguage": {
                "sEmptyTable": "No documents"
            },
            "order": [[0, "asc"]]
        });
    }
    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }
    function mixpanelIncrement(eventName){
        mixpanel.people.increment(eventName);
    }
});