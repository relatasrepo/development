/**
 * Created by naveen on 12/4/15.
 */

var reletasApp = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    }

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        //searchContent = searchContent.replace(/[^\w\s]/gi, '');
        //var str = searchContent;
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

reletasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        setMeeting:function(m){
            this.meetingOb = m;
            this.isOk = true;
        },
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        getUserId:function(){
            return this.userId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        updateTaskStatus:function($scope,$http,taskId,status,fetchtasks){
            var reqObj = {
                taskId:taskId,
                status:!status ? 'notStarted':'complete'
            };
            $http.post('/task/update/status/web',reqObj)
                .success(function(response){
                    if(response.SuccessCode){
                        toastr.success(response.Message);
                        if(fetchtasks)
                            $scope.timelineTasks($scope.cuserId,$scope.cEmailId)
                    }
                    else toastr.error(response.Message);
                })
        }
    };
});

/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

reletasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {
            setTimeout(function(){
                $(".seeMoreIcon").addClass("disabled-see-more")
            },2000)

            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $scope.l_usr.initials = response.Data.firstName.substring(0,2);
                if(!imageExists($scope.l_usr.profilePicUrl)){
                    $scope.l_usr.imageExists = true;
                }
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                share.abc = response.Data;
                $scope.userName = response.Data.firstName;
                $scope.firstName = response.Data.firstName
                $scope.profilePicUrl = '/getImage/'+response.Data._id+'/'+new Date().toISOString()
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
                share.checkIsCorporate(response.Data)
            }
        }).error(function (data) {

        })
});

reletasApp.controller("support_controller", function($scope, $http){
    $http.get('/profile/get/current/web')
        .success(function (response) {
            $scope.user = response.Data;
            $scope.firstName= $scope.user.firstName;
            $scope.profilePicUrl = $scope.user.profilePicUrl
        });

    $scope.sendEmail = function(messageSupport) {

        if (!checkRequired(messageSupport)) {
            toastr.error("Help body is missing.")
        }
        else {
        var obj = {
            receiverEmailId: "hello@relatas.com",
            receiverName: "Relatas",
            message: messageSupport,
            subject: "Support " + $scope.user.firstName
        };
        //$("#send-email-but").addClass("disabled")
        $http.post("/messages/send/email/single/web", obj)
            .success(function (response) {
                if (response.SuccessCode) {
                    $scope.messageSupport = "";
                    toastr.success(response.Message);
                }
                else {
                    toastr.success(response.Message);
                }
            })
    }

    };
});

reletasApp.controller("external_links",function ($scope, $http, share,$rootScope) {
    share.checkIsCorporate = function (profile) {
        $scope.isCorporate = profile.corporateUser;
    }
});