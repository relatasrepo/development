$(document).ready(function() {

    var userProfile;
    var monthNameFull = ['January','February','March','April','May','June','July','August','September','October','November','December']
    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    var table;
    var map;
    var markerCluster;

    isLoggedIn(false);
    getUserProfile();
    createMap();
    getUpComingPublicEvents();
    getAllLocations();

    $("#logout").on("click",function(){
        $.ajax({
            url:'/isLoggedInUser',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){
                if(result){
                    window.location.replace('/logout');
                }else{
                    $("#eventsBody").slideToggle();
                    $("#lightBoxPopup").slideToggle();
                }
            }
        })
    });

    function getAllLocations(){
        $.ajax({
            url:'/allEventsLocations',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(locations){
                var locationListHtml = '<div class="pull-left" style="margin-left: 1%">';
                locationListHtml += '<div class="checkbox"><label><input type="checkbox" id="All" value="All">All</label></div>';
               if(validateRequired(locations)){
                   if(validateRequired(locations[0])){
                        displaySubscribeLocationList(locations);
                   }else {
                       locationListHtml += '</div>';
                       $("#subscribeEventLocations").html(locationListHtml)
                       isLoggedIn(true)
                   }
               }
               else {
                   locationListHtml += '</div>';
                   $("#subscribeEventLocations").html(locationListHtml)
                   isLoggedIn(true)
               }
            }
        })
    }

    function displaySubscribeLocationList(locations){

        var locationListHtml = '<div class="pull-left" style="margin-left: 1%">';
            locationListHtml += '<div class="checkbox"><label><input type="checkbox" id="All" value="All">All</label></div>';
        for(var i=0; i<locations.length; i++){

            var id = locations[i].eventLocation.replace(/\s/g,"");
            var value = locations[i].eventLocation.replace(/\s/g,"@");
            locationListHtml +='<div class="checkbox"><label>';
            locationListHtml +='<input type="checkbox" id='+id+' value='+value+'>'+locations[i].eventLocation+'</label></div>';
        }
        locationListHtml += '</div>';
        $("#subscribeEventLocations").html(locationListHtml)
        isLoggedIn(true)
    }
    // Function to get user contacts info
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;
                if(result == false){
                    stopNavigationFlag = true;
                    stopNavigation(true);
                    $("#logout").text('Sign Up/ Login');
                    $('#profilePic').attr("src",'/images/default.png');
                }else{
                    $("#logout").text('LOGOUT');
                    identifyMixPanelUser(result);
                    $('#profilePic').attr("src", result.profilePicUrl);
                    imagesLoaded("#profilePic",function(instance,img) {
                        if(instance.hasAnyBroken){
                            $('#profilePic').attr("src","/images/default.png");
                        }
                    });
                }
            },
            error: function (event, request, settings) {


            },
            timeout: 20000
        })
    }
    function identifyMixPanelUser(profile){

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page":window.location.href
        });
    }
var stopNavigationFlag = false;
    function isLoggedIn(forSubscribe){
        $.ajax({
            url:'/isLoggedInUser',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){
               if(result){
                   stopNavigationFlag = false;
                   stopNavigation(false)
                   if(forSubscribe){
                       getUserSubscribeToEventsDetails()
                   }
               }
               else{
                   stopNavigationFlag = true;
                   stopNavigation(true)
               }
            }
        })
    }

    function stopNavigation(navigate){
        if(navigate){
            $("#goToDashboard").attr('href','#');
            $("#goToEditProfile").attr('href','#');
            $("#goToCalendar").attr('href','#');
            $("#goToContacts").attr('href','#');
            $("#goToDocuments").attr('href','#');
            $("#goHome").attr('href','#');
        }
        else{
            $("#goToDashboard").attr('href','/dashboard');
            $("#goToEditProfile").attr('href','/editProfile');
            $("#goToCalendar").attr('href','/calendar');
            $("#goToContacts").attr('href','/contacts');
            $("#goToDocuments").attr('href','/docs');
            $("#goHome").attr('href','/');
        }
    }

    function msgSignIn(){
       if(stopNavigationFlag){
           showMessagePopUp("Please Sign in/ Sign up to navigate further.",'tip')
       }
    }

    $("#goHome").on('click',function(){
        msgSignIn()
    })
    $("#goToDashboard").on('click',function(){
        msgSignIn()
    })
    $("#goToEditProfile").on('click',function(){
        msgSignIn()
    })
    $("#goToCalendar").on('click',function(){
        msgSignIn()
    })
    $("#goToDocuments").on('click',function(){
        msgSignIn()
    })
    $("#goToContacts").on('click',function(){
        msgSignIn()
    })

    function getUserSubscribeToEventsDetails(){
        if(validateRequired(markerCluster)){
            markerCluster.clearMarkers();
        }
        $.ajax({
            url:'/getUserSubscribeToEventsDetails',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){

                if(validateRequired(result)){

                    result.eventTypes.forEach(function(type){
                        var id =type.eventType.replace(/\//g,"");
                         $("#"+id.replace(/\s/g,"")).attr("checked",type.status);
                    })
                    result.locations.forEach(function(type){
                        var id =type.location
                        $("#"+id.replace(/\s/g,"")).attr("checked",type.status);
                    })
                    if(validateRequired(result.eventsList)){
                        if(validateRequired(result.eventsList[0])){

                            getAllSubscribedEvents(result.eventsList)
                        }
                    }
                }
            }
        })
    }

    function getUpComingPublicEvents(){
        $.ajax({
            url: '/getUpComingPublicEvents',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (events) {
                if(events){
                    if(validateRequired(events[0])){
                        displayUpComingPublicEvents(events)
                    }
                    else{
                         //$("#upcomingEventsTable tbody").html("<tr><td></td><td></td><td></td><td>No Upcoming events</td><td></td><td></td><td></td><td></td></tr>");
                    }
                }
                else{
                    //$("#upcomingEventsTable tbody").html("<tr><td></td><td></td><td></td><td>No Upcoming events</td><td></td><td></td><td></td><td></td></tr>");
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function displayUpComingPublicEvents(events){
        var eventsTableHtml = '';
        var userIdArr = [];
        var today = new Date();
        events.sort(function(a,b){
            return new Date(b.createdDate) - new Date(a.createdDate);
        });
        for(var i=0; i<events.length; i++){
            var startDate = new Date(events[i].startDateTime);
            var endtDates = new Date(events[i].endDateTime);
            var isoDate = startDate.toISOString();
            var dateStr = startDate.getFullYear()+'/'+startDate.getMonth()+1+'/'+startDate.getDate();
            if(endtDates >= today){
                var eventLocation = events[i].eventLocation || 'Not Available';
                var eventType = events[i].eventCategory || 'Not Available';
                var title = events[i].eventName || 'Not Available';
                var eventUrl = '/event/'+events[i]._id;
                var endDate = new Date(events[i].endDateTime);
                var anchorId = events[i].createdBy.userId;
                userIdArr.push(anchorId);
                var eventAnchor = events[i].createdBy.userName || 'Not Available';
                eventsTableHtml += '<tr><td style="display: none">'+i+'</td><td style="display: none">'+dateStr+'</td><td title='+title.replace(/\s/g, '&nbsp;')+'><a style="cursor:pointer;color:#333348" id="eventDetails" eid='+events[i]._id+'>'+getTextLength(title,25)+'</a></td><td>'+eventLocation+'</td><td>'+eventType+'</td><td><span id="sortdDate" value='+isoDate+'>'+getEventDateFormat(startDate)+'</span></td><td><a class='+anchorId+' style="cursor:pointer;color:#333348">'+eventAnchor+'</a></td>';
                eventsTableHtml += '<td><button id="eventDetails" class="btn btn-xs blue-btn" eId='+events[i]._id+'>View Details</button></td></tr>';
            }
        }
        $("#upcomingEventsTable tbody").html(eventsTableHtml);
        getUserList(userIdArr);
    }

    function getUserList(userIdArr) {

        userIdArr = removeDuplicates(userIdArr);

        if(userIdArr.length > 0){
            var data = {
                data: JSON.stringify(userIdArr)
            }
            $.ajax({
                url: '/userBasicInfoByArrayOfIds',
                type: 'POST',
                datatype: 'JSON',
                data: data,
                success: function (userList) {
                    if (validateRequired(userList) && userList.length>0) {
                        for(var user=0; user<userList.length; user++){
                            if(validateRequired(userList[user])){
                                $("."+userList[user]._id).attr('href','/'+userList[user].publicProfileUrl);
                            }
                        }
                    }
                }
            })
        }
    }

    function getTextLength(text,maxLength){
        var textLength = text.length;
        if(textLength >maxLength){
            var formattedText = text.slice(0,maxLength)
            return formattedText+'..';
        }
        else return text;
    }
    function createMap(){
        var  myLatlng = new google.maps.LatLng(13.5333, 2.0833);
        var mapOptions = {
            center: myLatlng,
            zoom: 2
            };

          map = new google.maps.Map(document.getElementById("map"), mapOptions);
            google.maps.event.addListener(map, 'zoom_changed', function () {
                if (map.getZoom() < 2) map.setZoom(2);
            });
        var styles = [
            {
                height: 53,
                url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m2.png",
                width: 53
            },
            {
                height: 53,
                url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m2.png",
                width: 53
            },
            {
                height: 53,
                url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m2.png",
                width: 53
            },
            {
                height: 53,
                url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m2.png",
                width: 53
            },
            {
                height: 53,
                url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m2.png",
                width: 53
            }]
        markerCluster = new MarkerClusterer(map,[],{zoomOnClick: false,minimumClusterSize:1,styles:styles});


        google.maps.event.addListener(markerCluster, 'clusterclick', function(cluster) {
            var c = cluster.getMarkers()
            if(validateRequired(c[0])){
                showGlobalEventInfo(c);
            }
        });
        $.ajax({
            url:'/isLoggedInUser',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){
                if(result){

                }
                else{
                    getAllEventsForMap()
                }
            }
        })
    }

    function showGlobalEventInfo(c){

        $("#mapPopup").popover('destroy');

        var html = _.template($("#googleMeetingInfo-popup").html())();
        $("#mapPopup").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#mapPopup").popover('show');
        $(".popover").css({'margin-top':'10%'})
        $(".popover").css({'margin-left':'30%'})
        $(".popover").css({width:'100%'})
        $(".arrow").addClass("invisible")
        $("#totalEvents").text(""+ c.length);
        var eTableHtml = '';
        $("#popupTimeZone").text('('+getTimeZone()+')')
        for(var i=0; i< c.length; i++){
            var eventUrl = '/event/'+c[i].currentEvent._id;
            var startDate = new Date(c[i].currentEvent.startDateTime)
             eTableHtml += '<tr><td><a title='+c[i].currentEvent.eventName.replace(/\s/g, '&nbsp;')+' style="color:#333333" href='+eventUrl+'>'+getTextLength(c[i].currentEvent.eventName || '',16)+'</a></td><td style="text-align: center">'+getEventDateFormatPopup(startDate)+'</td></tr>';
        }

        $("#eTable tbody").html(eTableHtml);
    }
    $("body").on("click","#closePopup-googleMeetingInfo",function(){
        $("#mapPopup").popover('destroy');
    });
    function getAllSubscribedEvents(eventList){
        var eventIdList = [];
        for(var i=0; i<eventList.length; i++){
            if(eventList[i].isDeleted){

            }else{
                eventIdList.push(eventList[i].eventId);
            }
        }
        var data = {
             list:JSON.stringify(eventIdList)
        }
        if(validateRequired(eventIdList[0]) && validateRequired(data.list)){
            $.ajax({
                url:'/getAllSubscribedEvents',
                type:'POST',
                datatype:'JSON',
                data:data,
                traditional:true,
                success:function(result){
                  if(validateRequired(result[0])){
                      addEventsToGoogleMap(result);
                  }
                }
            })
        }
    }

    function getAllEventsForMap(){
        $.ajax({
            url:'/getAllEvents',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(eventList){

               if(validateRequired(eventList)){
                   if(validateRequired(eventList[0])){
                        addEventsToGoogleMap(eventList);
                   }
               }
            }
        })
    }

    function addEventsToGoogleMap(events){
        markerCluster.clearMarkers();
        for(var i=0; i<events.length; i++){
            if(events[i].eventLocation != 'Online' && validateRequired(events[i].eventLocationUrl)){

                var locationUrl = events[i].eventLocationUrl.split("@");
                var latLangArr = locationUrl[1].split(",");
                var latitude = parseFloat(latLangArr[0]);
                var longitude = parseFloat(latLangArr[1]);
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(latitude, longitude),
                    map: map,
                    title: "Event: " + events[i].eventName,
                    currentEvent:events[i]
                });
                markerCluster.addMarker(marker)
            }
            else{

            }
        }
    }

    $("#subScribeToEvents").on("click",function(){
        var typeDiv = $('#subscribeEventTypes').children();
        var locationDiv = $('#subscribeEventLocations').children();
        var innerArr = []
         var JSONString = ''
             JSONString += '{"eventTypes":'
             JSONString += '['
        typeDiv.each(function(){
             var allDivs = $(this).children();
             allDivs.each(function(){
                 var checkBoxDivs = $(this).children();
                 checkBoxDivs.each(function(){
                     var checkBoxDiv = $(this).children();
                     checkBoxDiv.each(function(){
                         var inner = ''

                         inner += '{"eventType":'+JSON.stringify($(this).attr('value'))+','
                         inner += '"status":'+JSON.stringify($(this).prop("checked"))+'}'

                            innerArr.push(inner)
                     })
                 })
             })
         })
         for(var i=0; i<innerArr.length; i++){
             JSONString += innerArr[i]
             if(i == innerArr.length-1){

             }
             else{
                 JSONString += ','
             }
         }
         JSONString += '],'
        JSONString += '"locations":'
        JSONString += '['

        var innerArr2 = []
        locationDiv.each(function(){
            var allDivs = $(this).children();
            allDivs.each(function(){
                var checkBoxDivs = $(this).children();
                checkBoxDivs.each(function(){
                    var checkBoxDiv = $(this).children();

                    checkBoxDiv.each(function(){
                        var inner = ''

                        inner += '{"location":'+JSON.stringify($(this).attr('value').replace(/\@/g," "))+','
                        inner += '"status":'+JSON.stringify($(this).prop("checked"))+'}'

                        innerArr2.push(inner)
                    })
                })
            })
        })
        for(var j=0; j<innerArr2.length; j++){
            JSONString += innerArr2[j]
            if(j == innerArr2.length-1){

            }
            else{
                JSONString += ','
            }
        }
        JSONString += ']}';
        var obj = {
            ArrString:JSONString
        }
        $.ajax({
            url:'/subscribeToEvents',
            type:'POST',
            datatype:'JSON',
            data:obj,
            traditional:true,
            success:function(result){
                if(result == 'loginRequired'){

                    $("#eventsBody").slideToggle();
                    $("#lightBoxPopup").slideToggle();
                    if($('body').scrollTop()>0){
                        $('body').scrollTop(0);         //Chrome,Safari
                    }else{
                        if($('html').scrollTop()>0){    //IE, FF
                            $('html').scrollTop(0);
                        }
                    }
                }else
                if(result){
                    getUserSubscribeToEventsDetails()
                    showMessagePopUp("You have successfully subscribed.",'success');
                }
                else {
                    showMessagePopUp("An error occurred. Please try again.",'error');
                }
            }
        })
    });

    $('#loginUsingLinkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        window.location.replace('/linkedinLoginFromEventsPage');
    });
    $('#login-with-linkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        window.location.replace('/linkedinLoginFromEventsPage');
    });
    $('#sign-up-with-linkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        window.location.replace('/linkedinLoginFromEventsPage');
    });

    $('#loginUsingGoogle').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenEventsPage');
    });
    $('#login-with-google').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenEventsPage');
    });
    $('#sign-up-with-google').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenEventsPage');
    });

    $('#createAnEvent').on('click',function(){
        $.ajax({
            url:'/isLoggedInUser',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){
                if(result){
                    window.location.replace('/createAnEvent');
                }
                else{
                    $("#lightBoxPopup").slideToggle();
                    $("#eventsBody").slideToggle();
                }
            }
        })
    });
    $('body').on('click','#eventDetails',function(){
        var eId = $(this).attr("eId");
        if(validateRequired(eId)){
            $.ajax({
                url:'/isLoggedInUser',
                type:'GET',
                datatype:'JSON',
                traditional:true,
                success:function(result){
                    if(result){
                        window.location.replace('/event/'+eId);
                    }
                    else{
                        $("#lightBoxPopup").slideToggle();
                        $("#eventsBody").slideToggle();
                    }
                }
            })
        }
    });

    $('#loginFormButton').on('click',function(){
        var emailId1 = $("#emailId2").val();
        var password1 = $("#password").val();

        if (validateRequired(emailId1) && validateRequired(password1)) {
            emailId1 = emailId1.toLowerCase();
            var credentials={
                emailId:emailId1,
                password:password1
            }

            $.ajax({
                url:'/loginFromPublicProfile',
                type:'POST',
                datatype:'JSON',
                data:credentials,
                traditional:true,
                success:function(result){
                    if (result) {
                        getUserProfile();
                        getUserSubscribeToEventsDetails();
                        $("#lightBoxPopup").slideToggle();
                        $("#eventsBody").slideToggle();
                        stopNavigation(false);
                        stopNavigationFlag = false;

                    }
                    else{
                        showMessagePopUp("Looks like either your email or password is incorrect. Please try again.",'error')
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            })
        }
        else{
            showMessagePopUp("Please enter your credentials",'error')
        }
    });


    function showMessagePopUp(message,msgClass)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':'20%','margin-left':'-10%'})
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message)
        $("#closePopup-message").focus();
    }

    $("body").on("click","#closePopup-message",function(){
        $("#user-name").popover('destroy');
    });

    // Function to validate required field
    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }

    function sidebarHeight(){
        var sideHeight = $(document).height()
        $("#sidebarHeight").css({'min-height':sideHeight})
    }
    $("#shw").click(function(){
        $("#show-hide").slideToggle();
    });

    function getEventDateFormat(date){
        var day = date.getDate()
        var format = day+''+getDayString(day)+' '+monthNameSmall[date.getMonth()] +' | '+getTimeFormatWithMeridian(date)+' ('+getTimeZone()+')';
        return format;
    }


    function getEventDateFormatPopup(date){
        var day = date.getDate()
        var format = day+''+getDayString(day)+' '+monthNameSmall[date.getMonth()] +' | '+getTimeFormatWithMeridian(date);
        return format;
    }

    function getTimeZone(){
        var start = new Date()
        var zone = start.toTimeString().replace(/[()]/g,"").split(' ');
        if(zone.length > 3){
            var tz = '';
            for(var i=2; i<zone.length; i++){

                tz += zone[i].charAt(0);
            }
            return tz || zone[2]
        }
        else return zone[2]

    }
    function getDayString(day) {
        if (day == 1 || day == 21 || day == 31) {
            return 'st';
        }
        else if (day == 2 || day == 22 || day == 23) {
            return 'nd';
        }
        else if (day == 3 || day == 23) {
            return 'rd';
        }
        else return 'th';
    };
    function getTimeFormatWithMeridian(date){
        var hrs = date.getHours();
        var min = date.getMinutes();
        var meridian;
        if(hrs > 12){
            hrs -= 12;
            meridian = 'PM';
        }
        else {
            if(hrs == 12){
                meridian = 'PM'
            }else
                meridian = 'AM';
        }
        if (min < 10) {
            min = "0"+min;
        };
        var time = hrs+":"+min+" "+meridian;
        return time;
    }

    setTimeout(function(){
        applyDataTable()
        sidebarHeight()
    },1200)

    function applyDataTable(){
        table = $('#upcomingEventsTable').DataTable({

            "dom": '<"top"iflp<"clear">>',
            "fnDrawCallback": function(oSettings) {
                if(validateRequired(table)){
                    if (table.rows() < 11) {
                        $('#upcomingEventsTable_paginate').hide();
                    }
                    else  $('#upcomingEventsTable_paginate').show();
                }else
                if ($('#upcomingEventsTable tr').length < 11) {
                    $('#upcomingEventsTable_paginate').hide();
                }
                else  $('#upcomingEventsTable_paginate').show();
            },
            "order": [[ 0, "asc" ]],
            "columns": [
                null,
                null,
                null,
                null,
                null,
                { "orderDataType": "dom-value" },
                null,
                null
            ],
            "oLanguage": {
            "sEmptyTable": "No Upcoming events"
        }
        });
    }

    /* Create an array with the values of all the select options in a column */
    $.fn.dataTable.ext.order['dom-value'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('#sortdDate', td).attr("value");
        } );
    }

    $("#sendUsingLinkedin").on("click",function(){

        $("#loginUsingLinkedin").trigger("click");
    });

    $("#signUp-Now").click(function(){
        $("#signUpNow-show-hide").slideToggle();
    });


    $("#lightBoxClose").on("click",function(){
        $("#eventsBody").slideToggle();
        $("#lightBoxPopup").slideToggle();
    })

    $("#userCreateAccount").on("click",function(){
        var emailId = $("#userEmailId").val();
        var firstName = $("#userFirstName").val();
        var lastName = $("#userLastName").val();
        if(validateRequired(emailId) && validateRequired(firstName) && validateRequired(lastName)){
            var user = {
                firstName:firstName,
                lastName:lastName,
                emailId:emailId
            }
            checkEmailAddress(user);
        }else{
            showMessagePopUp("Please fill all the fields.",'error');
        }
    });

    function checkEmailAddress(data){

        $.ajax({
            url:'/checkEmail/'+data.emailId,
            type:'GET',
            traditional:true,
            success:function(result){
                if(result == true){
                    createPartialAcc(data);
                }
                else{
                    showMessagePopUp("Email address already exist in the Relatas, Please change it.",'error');
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })

    }

    function createPartialAcc(user){
        $.ajax({
            url:'/createPartialAccount',
            type:'POST',
            datatype:'JSON',
            data:user,
            success:function(response){
                if(response){
                    window.location.replace('/emailLoginEventsPage');
                }else showMessagePopUp("failed",'error');
            }
        })
    }
    function removeDuplicates(arr) {
        var end = arr.length;
        for (var i = 0; i < end; i++) {
            for (var j = i + 1; j < end; j++) {
                if (arr[i] == arr[j]) {
                    var shiftLeft = j;
                    for (var k = j + 1; k < end; k++, shiftLeft++) {
                        arr[shiftLeft] = arr[k];
                    }
                    end--;
                    j--;
                }
            }
        }

        var whitelist = [];
        for (var l = 0; l < end; l++) {
            whitelist[l] = arr[l];
        }
        return whitelist;
    }
    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }
});