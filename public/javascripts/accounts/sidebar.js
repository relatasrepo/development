/**
 * Created by naveen on 24/8/17.
 */

relatasApp.controller("sidebar_settings", function ($scope, $http, $rootScope, share) {

    share.fiscalYears = function (fiscalYears) {
        initAccountTargetsValue(fiscalYears);
    }

    function initAccountTargetsValue(fiscalYears){
        $scope.datesAndTargets = [];

        $scope.datesAndTargets.push({
            year:moment(fiscalYears.start).format("MMM YY")+" - "+moment(fiscalYears.end).format("MMM YY"),
            amount:0,
            rawStartDate:fiscalYears.start,
            rawEndDate:fiscalYears.end
        });

        _.each(fiscalYears.nextFysStartEnd,function (el) {
            $scope.datesAndTargets.push({
                year:moment(el.start).format("MMM YY")+" - "+moment(el.end).format("MMM YY"),
                amount:0,
                rawStartDate:el.start,
                rawEndDate:el.end
            });
        })
    }

    $scope.reConfirmRemoval = function () {
        if(!$scope.account.reportingHierarchyAccess){
            alert("Do you wish to remove Management access to this account?")
        }
    }

    share.viewMasterData = function(account){

        $scope.account = account;
        $scope.account.showMasterData = true;
        $scope.account.show = false;

        var url = '/company/get/master/account?accName='+account.name.toLowerCase();

        $http.get(url)
            .success(function (response) {

                $scope.typeHeaders = [];
                $scope.lists = [];

                if(response && response.Data){
                    $scope.masterDataAccs = response.Data;

                    $scope.typeHeaders = response.Data.importantHeaders;
                    $scope.lists = [response.Data.data];
                }
            });
    }

    share.account_settings = function(account,usersWithAccess){

        $scope.account = account;
        $scope.usersWithAccess = [];
        $scope.account.show = true;
        $scope.account.showMasterData = false;

        if(usersWithAccess && usersWithAccess.length>0){
            _.each(usersWithAccess,function (el) {
                if($rootScope.currentUser.emailId != el.emailId){
                    $scope.usersWithAccess.push(el)
                }
            })
        }

        if(!account.target || account.target.length == 0){
            if(share.getCurrentFiscalYear){
                initAccountTargetsValue(share.getCurrentFiscalYear);
            }
        } else {

            account.target.sort(function (o1, o2) {
                return new Date(o1.rawEndDate) < new Date(o2.rawEndDate) ? 1 : new Date(o1.rawEndDate) > new Date(o2.rawEndDate) ? -1 : 0;
            });

            $scope.data = {
                costOfDelivery:account.target[0].costOfDelivery,
                costOfSales:account.target[0].costOfSales
            }

            if(new Date(account.target[0].rawEndDate)< new Date(share.getCurrentFiscalYear.end)){
                $scope.datesAndTargets = [];
                $scope.datesAndTargets.push({
                    year:moment(share.getCurrentFiscalYear.start).format("MMM YY")+" - "+moment(share.getCurrentFiscalYear.end).format("MMM YY"),
                    amount:0,
                    rawStartDate:share.getCurrentFiscalYear.start,
                    rawEndDate:share.getCurrentFiscalYear.end
                });

                $scope.data = {
                    costOfDelivery:0,
                    costOfSales:0
                }

            } else {
                $scope.datesAndTargets = account.target;
            }

        }

        var year = moment(share.getCurrentFiscalYear.start).format("MMM YY")+" - "+moment(share.getCurrentFiscalYear.end).format("MMM YY")
        _.each($scope.datesAndTargets,function (tr) {
            if(tr.year != year){
                tr.disableEdit = true;
            } else {
                tr.disableEdit = false;
            }
        })

        getActivityLog($scope,$http,share)
    }

    $scope.saveAccountSettings = function () {
        $scope.account.show = false;

        $scope.datesAndTargets.sort(function (o1, o2) {
            return new Date(o1.rawEndDate) < new Date(o2.rawEndDate) ? 1 : new Date(o1.rawEndDate) > new Date(o2.rawEndDate) ? -1 : 0;
        });

        $scope.data = {
            costOfDelivery:$scope.datesAndTargets[0].costOfDelivery,
            costOfSales:$scope.datesAndTargets[0].costOfSales
        }

        var obj = {
            account:$scope.account.fullName,
            costOfDelivery:$scope.data.costOfDelivery,
            costOfSales:$scope.data.costOfSales,
            target:$scope.datesAndTargets,
            reportingHierarchyAccess:$scope.account.reportingHierarchyAccess
        }
        
        $http.post("/account/monetary/settings",obj)
            .success(function (response) {

                toastr.success("Account settings saved successfully");

                if($scope.datesAndTargets && $scope.datesAndTargets[0] && $scope.datesAndTargets[0].amount){
                    share.setAccountAsImportant()
                }
                share.updateAccountCosts($scope.data.costOfSales,$scope.data.costOfDelivery)
            });
    }

    $scope.closeModal = function () {
        $scope.account.show = false;
        $scope.account.showMasterData = false;
    }

    $scope.removeAccess = function (user) {

        $http.get('/account/revoke/access?account='+$scope.account.fullName+"&emailId="+user.emailId)
            .success(function (response) {
                if(response.SuccessCode){
                }
            });

        if($scope.usersWithAccess.length>1){
            $scope.usersWithAccess =  $scope.usersWithAccess.filter(function (el) {
                return el.emailId != user.emailId
            })
        }
    }

    $scope.grantAccess = function (user) {

        user.checked = true;
        $scope.usersWithAccess.push(user);

        $http.get('/account/grant/access?account='+$scope.account.fullName+"&emailId="+user.emailId)
            .success(function (response) {
                if(response.SuccessCode){
                    $scope.requestedForAccess = true;
                    share.accountAccessRequestedStatus(user);
                    getActivityLog($scope,$http,share)
                    sendEmail($scope,$rootScope,$http,share,"grantAccess",user.emailId)
                }
            });

        $scope.account.accessRequired =  $scope.account.accessRequired.filter(function (el) {
            return el.emailId != user.emailId
        });

    }

});

function getActivityLog($scope,$http,share){

    $scope.activityLog = [];

    $http.get("/account/activity/log?account="+$scope.account.fullName)
        .success(function (response) {
            if(response.SuccessCode){
                if(response.Data.length>0){
                    _.each(response.Data,function (el) {
                        $scope.activityLog.push(formatActivityLog(el,share))
                    })
                }
            }
        });
}

function formatActivityLog(data,share){
    var obj = {};

    var activity = "";
    if(data.type == "accountAccessGranted"){
        activity = activity+"Account access granted to "
    } else if(data.type == "accountAccessRevoked"){
        activity = activity+"Account access revoked for "
    } else if(data.type == "accountTransferred"){
        activity = activity+"Account transferred to "
    } else if(data.type == "accountOrgHead"){
        activity = activity+"Account hierarchy head identified for "+data.updateFor
    } else if(data.type == "updateContactName"){
        activity = activity+"Contact name edited for "+data.updateFor
    } else if(data.type == "updateContactDesignation"){
        activity = activity+"Contact designation edited for "+data.updateFor
    } else if(data.type == "removeFromHierarchy"){
        activity = activity+"Contact removed from hierarchy "+data.updateFor
    } else if(data.type == "updateRelationship"){
        activity = activity+"Updated relationship for "+data.updateFor
    } else if(data.type == "updateType"){
        activity = activity+"Updated type for "+data.updateFor
    }

    if(share.teamDictionary[data.toEmailId]){
        activity = activity+share.teamDictionary[data.toEmailId].emailId;
    } else {
        var value = "";

        if(data.updateValue == "decision-maker"){
            value = "Decision maker"
        }
        if(data.updateValue == "friendly"){
            value = "Friendly"
        }
        if(data.type == "updateType" && data.updateValue == null){
            value = "Neutral"
        }
        if(data.updateValue == "detractor"){
            value = "Detractor"
        }

        activity = activity+': '+value;
    }

    obj.activity = activity
    obj.who = share.teamDictionary[data.fromEmailId];
    obj.date = moment(data.date).format(standardDateFormat())
    return obj;
}
