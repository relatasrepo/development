relatasApp.controller("allInteractions", function($scope, $http, $rootScope, share){

    $scope.skipValue = 25;
    $scope.skip = 0;
    var userIdsCache = [];

    share.showAllInteractions = function (account,userIds) {
        $scope.account = account;
        $scope.showAllInteractions = true;
        userIdsCache = userIds
        getInteractions($scope,$http,$rootScope,share,userIds);
    }

    share.closeAllInteractions = function () {
        $scope.closeAllInteractions()
    }

    $scope.closeAllInteractions = function () {
        $scope.showAllInteractions = false;
        $scope.interactions = [];
    }

    $scope.prevPage = function (skip) {
        $scope.skip = skip;
        if(skip <= 0){
            $scope.prevBtn = {"visibility":"hidden"}
        } else {
            $scope.prevBtn = {"visibility":"visible"}
        }

        $scope.nextBtn = {"visibility":"visible"}

        getInteractions($scope,$http,$rootScope,share,userIdsCache,skip);
    }

    $scope.nextPage = function (skip) {
        $scope.skip = skip;
        if((skip >= $scope.totalPages) || (($scope.totalPages-skip)<=10)){
            $scope.nextBtn = {"visibility":"hidden"}
        } else {
            $scope.nextBtn = {"visibility":"visible"}
        }

        $scope.prevBtn = {"visibility":"visible"}

        getInteractions($scope,$http,$rootScope,share,userIdsCache,skip)
    }

    $scope.interactionDetails = function (interaction) {
        $rootScope.showInteractionDetails = false;

        if(interaction.cursor != "not-allowed" && interaction.dataObj.interaction.interactionType == "meeting") {
            window.location = "/today/details/"+interaction.refId
        } else {
            if($rootScope.currentUser.emailId == interaction.owner.emailId){
                $rootScope.showInteractionDetails = true;
                share.viewInteraction(interaction);
            } else {
                $rootScope.showInteractionDetails = false;
            }
        }
    }
});

function getInteractions($scope,$http,$rootScope,share,userIds,skip){

    var url = "/account/get/interactions?account="+$scope.account.fullName;

    if(skip){
        url = url+"&skip="+skip;
    }

    url = fetchUrlWithParameter(url,"userIdList",userIds)
    url = fetchUrlWithParameter(url, "contacts", $scope.account.contacts);

    $scope.interactions = [];

    $scope.totalInteractions = 0;

    $http.get(url)
        .success(function(response){

            if(response.Data && response.Data.length>0){

                $scope.totalPages = response.interactionsCount;

                skip = skip?skip:0;
                var from = (parseInt(skip)+1);
                var to = (parseInt(skip)+$scope.skipValue);

                if(to>$scope.totalPages){
                    to = $scope.totalPages
                }
                
                $scope.showingPages = from+" - "+ to;

                _.each(response.Data,function (interaction,index) {
                    var interactionObj = buildInteractionObjectForTimeline(interaction,null,interaction.firstName,$scope.rName,"UTC",index)
                    interactionObj["owner"] = share.teamDictionary[interaction.ownerEmailId]

                    if($rootScope.currentUser && $rootScope.currentUser.emailId == interactionObj.owner.emailId){
                        interactionObj["cursor"] = "cursor"
                        interactionObj["noAccessClass"] = "access-hover"
                    } else {
                        interactionObj["noAccessClass"] = "no-access-class"
                        interactionObj["cursor"] = "not-allowed"
                        interactionObj["title"] = "You don't have access to this interaction. Access available only to the interaction owner."
                        interactionObj["subject"] = "You don't have access to this interaction. Access available only to the interaction owner."
                    }

                    interactionObj["emailId"] = interactionObj.dataObj.interaction.emailId
                    interactionObj["ownerName"] = interactionObj.owner.fullName

                    $scope.interactions.push(interactionObj)
                })
            }

        });
}