var accountName = getParams(window.location.href).accountName

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/customer/middle/bar"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

            if(typeof str == 'string' && str.length > 0){
                if(searchContent.length >= 3) {
                    yourNetwork = yourNetwork ? true : false;
                    extendedNetwork = extendedNetwork ? true : false;
                    forCompanies = forCompanies ? true : false;
                    window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;

                }
            }
            else toastr.error("Please enter search content")
    };
});

relatasApp.controller("logedinUser", function ($scope, $http, $rootScope,share) {

    getTeam($scope,$http,function (teamArray,teamDictionary,usersDictionaryWithUserId) {
        share.teamDictionary = teamDictionary;
        share.teamArray = teamArray;
        share.usersDictionaryWithUserId = usersDictionaryWithUserId;
    });

    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        
        $rootScope.$broadcast("fetchedUserProfile", response.Data.corporateUser)

        share.getCurrentFiscalYear = getCurrentFiscalYear("UTC",response.companyDetails.fyMonth)
        share.fiscalYears(share.getCurrentFiscalYear)

        $rootScope.currency = "USD"
        share.currenciesObj = {};

        if(response.companyDetails && response.companyDetails.currency){
            _.each(response.companyDetails.currency,function (el) {
                share.currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    $rootScope.currency = el.symbol;
                }
            })
        }

        share.currency = $rootScope.currency

        if(response.Data.contactsUpload && response.Data.contactsUpload.isUploaded){

        }else $("#uploadContactsAlert").removeClass("hide");
    })

});

relatasApp.controller("left_contacts_bar", function ($scope, $http, $rootScope, share) {

    $scope.contactsList = [];
    $scope.currentLength = 0;
    $scope.lastFetched = 0;
    $scope.filterBy = 'all';
    $scope.show_contacts_back = false;
    $scope.show_filter = false;
    $scope.usersInHierarchy = []
    var isFilter = false;
    $scope.accountListLoading = true;

    $scope.searchContacts = function(searchContent){

        $scope.searchContent = searchContent;

        if(checkRequired(searchContent)){
            $scope.filterBy = isFilter ? $scope.filterBy : 'search';
            $scope.contactsList = [];
            $scope.currentLength = 0;
            var url = fetchUrlWithParameter("/company/middle/bar/accounts", "hierarchylist", $scope.usersInHierarchy)
            $scope.getContacts(null,url,0,25,$scope.filterBy,searchContent,null,function (account) {
                $scope.accountSelected(account)
            });
        }
    };

    $scope.refreshContactsList = function(){
        $scope.setFilter("Recent")
    };

    $scope.filterSelection = "Recent";
    $scope.filters = [];

    var filtersTemp = ["All","My","Recent","Important"];

    _.each(filtersTemp,function (el) {
        var selected = "selected"

        if(el != "Recent"){
            selected = ""
        }

        $scope.filters.push({
            class:selected,
            filter:el
        });
    });

    $scope.setFilter = function (options) {
        mixpanelTracker("Accounts>list filter "+$scope.filterBy);
        $scope.filterBy = isFilter ? $scope.filterBy : 'all';
        $("#searchContent").val("");
        $scope.searchContent = null;
        $scope.contactsList = [];
        $scope.currentLength = 0;
        $scope.filterSelection = options;
        
        $scope.searchContent = JSON.parse(JSON.stringify($scope.searchContent));
        
        highlightFilterSelection($scope,options);

        var url = fetchUrlWithParameter("/company/middle/bar/accounts", "hierarchylist", $scope.usersInHierarchy)

        $scope.getContacts(options,url,0,25,$scope.filterBy,null,null,function (account) {
            $scope.accountSelected(account)
        })
    }

    $scope.getContacts = function(options,url,skip,limit,filterBy,searchContent,isBack,callback){

        $scope.isBack = isBack;
        $scope.skip = skip;
        $scope.limit = limit;
        $scope.accountListLoading = true;

        if(url.indexOf("?") == -1)
            url = url + "?"
        else
            url = url + "&"
        if(checkRequired(searchContent)){
            url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy+'&searchContent='+searchContent
        }
        else url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy;

        if(options){
            url = url+"&options="+options
        }
        
        $http.get(url)
            .success(function(response){
                $scope.contactsList = [];
                $scope.isConnectionOpen = false;
                if(response.SuccessCode){

                    $scope.grandTotal = response.Data.total;

                    if(response.Data.accounts.length > 0){

                        if(!isBack || $scope.currentLength <= 0){
                            $scope.currentLength += response.Data.returned;
                            $scope.returned = response.Data.returned
                        }

                        if($scope.currentLength > 25){
                            $scope.show_contacts_back = true;
                        }else $scope.show_contacts_back = false;

                        for(var i=0; i<response.Data.accounts.length; i++){

                            var name = response.Data.accounts[i].name;
                            var hasAccess = false;
                            var accessIcon = "fa fa-lock";
                            var ownerEmailId = response.Data.accounts[i].ownerEmailId;
                            var ownerId = response.Data.accounts[i].ownerId;

                            var usersWithAccess = [];

                            _.each(response.Data.accounts[i].access,function (el) {
                                if(el.emailId != ownerEmailId){
                                    usersWithAccess.push(el.emailId)
                                }
                            })

                            if(response.Data.accounts[i].reportingHierarchyAccess && response.Data.accounts[i].heiarchyWithAccess && response.Data.accounts[i].heiarchyWithAccess.length>0){
                                _.each(response.Data.accounts[i].heiarchyWithAccess,function (el) {
                                    if(share.usersDictionaryWithUserId[el]){
                                        usersWithAccess.push(share.usersDictionaryWithUserId[el].emailId)
                                    }
                                })
                            }

                            if(_.includes(usersWithAccess,response.liuEmailId) || ownerEmailId == response.liuEmailId){
                                hasAccess = true;
                                accessIcon = ""
                            } else {
                                hasAccess = false;
                                accessIcon = "fa fa-lock"
                            }
                            
                            var costOfSalesWithComma = 0,
                                costOfDeliveryWithComma = 0;

                            if(response.Data.accounts[i].costOfSales){
                                costOfSalesWithComma = getAmountInThousands(response.Data.accounts[i].costOfSales,2)
                            }

                            if(response.Data.accounts[i].costOfDelivery){
                                costOfDeliveryWithComma = getAmountInThousands(response.Data.accounts[i].costOfDelivery,2)
                            }

                            var selection = ""

                            var obj;

                            obj = {
                                isTesting:response.Data.accounts[i].isTesting,
                                isOwner:ownerEmailId == response.liuEmailId,
                                userId:name,
                                fullName:name,
                                designation:"",
                                fullDesignation:"",
                                name:(getTextLength(name,25)).capitalizeFirstLetter(),
                                emailId:name,
                                noPicFlag:true,
                                image:"https://logo.clearbit.com/"+ getTextLength(name,25) +".com",
                                noPPic:(name.substr(0,2)).capitalizeFirstLetter(),
                                cursor:'cursor:default',
                                url:null,
                                idName:'com_con_item_'+name,
                                hasAccess:hasAccess,
                                target:response.Data.accounts[i].target,
                                partner:response.Data.accounts[i].partner,
                                important:response.Data.accounts[i].important,
                                costOfSales:response.Data.accounts[i].costOfSales,
                                costOfDelivery:response.Data.accounts[i].costOfDelivery,
                                costOfSalesWithComma:costOfSalesWithComma,
                                costOfDeliveryWithComma:costOfDeliveryWithComma,
                                accessIcon:accessIcon,
                                usersWithAccess:_.uniq(usersWithAccess),
                                hierarchyAccess:response.Data.accounts[i].heiarchyWithAccess,
                                reportingHierarchyAccess:response.Data.accounts[i].reportingHierarchyAccess,
                                selection:selection,
                                ownerId:ownerId,
                                ownerEmailId:ownerEmailId,
                                accountAccessRequested:response.Data.accounts[i].accountAccessRequested,
                                contacts:response.Data.accounts[i].contacts,
                                hierarchy:response.Data.accounts[i].hierarchy,
                                lastInteractedDate:response.Data.accounts[i].lastInteractedDate,
                                access:response.Data.accounts[i].access
                            };

                            $scope.contactsList.push(obj);
                            $http.get("/companylogos/" + getTextLength(name,25) +".com")
                        }
                        
                        if(options == "Recent"){
                            $scope.contactsList.sort(function (o1, o2) {
                                return new Date(o1.lastInteractedDate) < new Date(o2.lastInteractedDate) ? 1 : new Date(o1.lastInteractedDate) > new Date(o2.lastInteractedDate) ? -1 : 0;
                            });
                        }
                    }
                    else {
                        $scope.isAllConnectionsLoaded = true;
                    }
                }
                else{
                    $scope.grandTotal = 0;
                    $scope.isAllConnectionsLoaded = true;
                }

                if(callback){

                    if($scope.contactsList[0]){
                        $scope.contactsList[0]["selection"] = "contact-selected";
                        callback($scope.contactsList[0])
                    } else {
                        callback(null)
                    }
                }
                $scope.accountListLoading = false;
            })
    };

    $scope.validNum = function(num){
        return num < 10 ? '0'+num : num;
    }

    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    function checkLiuLoaded(){

        var url = '/company/middle/bar/accounts';

        if($rootScope.currentUser){
            $scope.getContacts($scope.filterSelection,url,0,25,$scope.filterBy,null,null,function (account) {

                if(accountName){
                    $scope.getContacts(null,url,0,25,$scope.filterBy,accountName,null,function (account) {
                        $scope.accountSelected(account)
                    })
                } else if(account){
                    $scope.accountSelected(account)
                } else {
                    $scope.setFilter("All")
                }
            });
        } else {
            setTimeOutCallback(1000,function () {
                checkLiuLoaded();
            })
        }
    }

    checkLiuLoaded();

    $scope.loadMoreContacts = function(skip,limit,searchContent,isBack){
        
        if(!$scope.searchContent){
            searchContent = null;
        }

        if(isBack){
            $scope.currentLength = $scope.currentLength-25;
            skip = skip-25
        }
        if(skip < 25){
            skip = 0
            $scope.currentLength = 0;
        }

        $scope.getContacts($scope.filterSelection,'/company/middle/bar/accounts',skip,limit,$scope.filterBy,searchContent,isBack,function (account) {
            $scope.accountSelected(account)
        });

    };

    $scope.calculateFromTo = function(current){
        return current < 0 ? 0 : current
    };

    $scope.accountSelected = function(account){
        mixpanelTracker("Accounts>Review accounts ");
        $("html, body").animate({ scrollTop: 15 }, "slow");
        if(account){
            share.accountHierarchy(account);
        }
        share.closeAllInteractions();
        share.closeHierarchy()

        // share.interactionInitiations_load_test()

        $(".unassigned-list").hide();

        removeSelectionForOthers($scope,account)
        
        if(account){
            checkTeamLoaded();
        }

        function checkTeamLoaded(){
            if(share.teamDictionary && share.teamArray){
                var userIds = _.map(share.teamArray,"userId");
                share.getAccessControl(account,userIds)
                share.initAccountDetails(account,userIds)
            } else {
                setTimeOutCallback(1000,function () {
                    checkTeamLoaded()
                })
            }
        }
    };

});

relatasApp.controller("account_snapshot", function ($scope, $http, $rootScope, share) {

    $scope.goToExtLink = function(accountWebsites) {
        window.open(accountWebsites[0]);
    };

    $scope.requestAccess = function () {

        $http.get("/account/request/access?account="+$scope.account.fullName)
            .success(function (response) {
                if(response.SuccessCode){
                    $scope.requestedForAccess = true;
                    $scope.requestedDate = moment().format(standardDateFormat());
                    sendEmail($scope,$rootScope,$http,share,"accountAccessRequired")
                }
            });
    }

    $scope.viewMasterData = function (account){
        mixpanelTracker("Accounts>View Master acc details ");
        share.viewMasterData(account);
    }

    share.initAccountDetails = function (account,userIds){

        $rootScope.noAccess = true;
        $rootScope.ifLiuIsAccountOwner = false;
        // account.fullName = "relatas";

        var accountName = account.fullName;
        $rootScope.account_name = account.fullName.capitalizeFirstLetter();

        account.accessRequired = [];

        $scope.account = account;
        $scope.accountWebsites = [];
        var accountWebsites = [];

        var totalContacts = account.contacts.length;
        _.each(account.contacts,function (url,index) {

            if(url.indexOf("@")>0){
                var link = "http://"+url.substr(url.indexOf("@") + 1);
                if(!_.includes(accountWebsites,link)){
                    accountWebsites.push(link)
                }
            }
        });

        var websiteToShow = null

        if(accountWebsites.length>1){
            websiteToShow = getWebsiteByDomainTypeOrder(accountWebsites)
        } else {
            if(accountWebsites[0]){
                websiteToShow = accountWebsites[0]
            }
        }

        $scope.accountWebsites.push(websiteToShow)

        $scope.importantSelection = {"border":"1px solid #ccc"}
        $scope.partnerSelection = {"border":"1px solid #ccc"}
        if($scope.account.important){
            $scope.importantSelection = {'background':'#f86b4f',"color": '#fff',"border-color":"#f86b4f"}
        }

        if($scope.account.partner){
            $scope.partnerSelection = {'background':'#f86b4f',"color": '#fff',"border-color":"#f86b4f"}
        }

        if(!account.isOwner){
            $scope.importantSelection["cursor"] = "not-allowed"
            $scope.partnerSelection["cursor"] = "not-allowed"
        }

        if(account.accountAccessRequested && account.accountAccessRequested.length>0){
            _.each(account.accountAccessRequested,function (el) {
                $scope.requestedForAccess = false;
                if(el.emailId == $rootScope.currentUser.emailId){
                    $scope.requestedForAccess = true;
                    $scope.requestedDate = moment(el.date).format("DD MMM YYYY")
                }

                var obj = share.teamDictionary[el.emailId]

                obj.dateFormatted = moment(el.date).format("DD MMM YYYY")

                if(el.emailId){
                    $scope.account.accessRequired.push(obj);
                }
            })
        } else {
            $scope.requestedForAccess = false;
            $scope.requestedDate = ""
        }

        $scope.usersWithAccess = [];
        $scope.companyName = account.fullName;
        $scope.companyLogo = "https://logo.clearbit.com/"+$scope.companyName+".com";
        $scope.companyNoName = ($scope.companyName.substr(0,2)).capitalizeFirstLetter();

        if(!imageExists($scope.companyLogo)){
            $scope.displayLogo = true;
        }

        if(account.usersWithAccess && account.usersWithAccess.length>0){
            _.each(account.usersWithAccess,function (emailId) {

                var obj = share.teamDictionary[emailId];

                if(obj){
                    obj.checked = true;
                    if(account.usersWithAccess.length == 1){
                        obj.disabled = true;
                    }

                    $scope.usersWithAccess.push(obj)
                }
            })
        }

        if($scope.usersWithAccess.length>0 || ($scope.account.accountAccessRequested.length>0 && $scope.account.isOwner)){
            $scope.showSharedAccess = true;
        } else {
            $scope.showSharedAccess = false;
        }

        function checkLiuAccess() {

            if($rootScope.currentUser){

                /*
                * Account ha 2 levels of acess
                * 1. If the LIU is the owner
                * 2. LIU is not owner, but the account's access has been given to the LIU by account owner
                * 3. Reporting hierarchy will have access to account by default.
                *
                * */

                if(account.ownerEmailId == $rootScope.currentUser.emailId || _.includes(account.hierarchyAccess,$rootScope.currentUser.id)){
                    $rootScope.ifLiuIsAccountOwner = true;
                }

                var tz = share.liuData.timezone && share.liuData.timezone.name?share.liuData.timezone.name:"UTC";

                // if(_.includes(account.usersWithAccess,$rootScope.currentUser.emailId) || share.teamDictionary[$rootScope.currentUser.emailId].corporateAdmin){
                if(_.includes(account.usersWithAccess,$rootScope.currentUser.emailId) || account.hasAccess || account.ownerEmailId == $rootScope.currentUser.emailId){
                    $rootScope.noAccess = false;

                    share.accountSnapshot(account,userIds,getCurrentFiscalYear(tz,share.companyDetails.fyMonth))
                    share.interactionsOppsAndNotes(account,userIds)
                    share.forOppGrowth(account,userIds)
                    share.forSnapshot(account,userIds)
                    share.interactionInitiations(account,userIds)
                    share.getAccountAndTeamInteractions(account,userIds)
                    share.accountCosts(account,userIds)
                    share.accountInsights(account,userIds)
                    share.loadAccessRequests();
                }
            }else {
                setTimeOutCallback(1000,function () {
                    checkLiuAccess()
                })
            }
        }

        checkLiuAccess();

        if(share.teamDictionary[account.ownerEmailId]){
            $scope.account_owner = share.teamDictionary[account.ownerEmailId].fullName;
        }
    }

    $scope.updatePartnerRelationship = function () {
        $scope.account.partner = !$scope.account.partner

        if($scope.account.partner){
            $scope.partnerSelection = {'background':'#f86b4f',"color": '#fff',"border-color":"#f86b4f"}
        } else {
            $scope.partnerSelection = ""
        }

        updateAccountRelationship($scope,$http,{partner:$scope.account.partner,important:$scope.account.important,account:$scope.account.fullName})
    }

    $scope.updateImportantRelationship = function () {
        $scope.account.important = !$scope.account.important

        if($scope.account.important){
            $scope.importantSelection = {'background':'#f86b4f',"color": '#fff',"border-color":"#f86b4f"}
        } else {
            $scope.importantSelection = ""
        }

        updateAccountRelationship($scope,$http,{partner:$scope.account.partner,important:$scope.account.important,account:$scope.account.fullName})
    }

    share.setAccountAsImportant = function () {
        $scope.importantSelection = {'background':'#f86b4f',"color": '#fff',"border-color":"#f86b4f"}
        updateAccountRelationship($scope,$http,{partner:$scope.account.partner,important:true,account:$scope.account.fullName})
    }

    var accountSnapshotLoaded = false;

    share.accountSnapshot = function(account,userIds,fyDetails){

        $scope.loading = true

        if(fyDetails){
            $scope.currentFy = moment(fyDetails.start).format("MMM YY")+"-"+moment(fyDetails.end).format("MMM YY");
        }

        account.target = account.target?account.target:0;
        
        var urlDealsRisk = '/account/deals/at/risk?account='+account.fullName

        urlDealsRisk = fetchUrlWithParameter(urlDealsRisk, "contacts", account.contacts);

        getDealsAtRisk($scope,$http,fetchUrlWithParameter(urlDealsRisk,"userIdList",userIds),share)

        var url = "/account/by/month/year?account="+account.fullName;
        url = fetchUrlWithParameter(url, "userIdList", userIds);
        url = fetchUrlWithParameter(url, "contacts", account.contacts);

        function getTargets(){
            if(accountSnapshotLoaded){
                drawTargets($scope, $http,url,share,account)
            } else {
                setTimeOutCallback(1000,function () {
                    getTargets();
                })
            }
        }

        getTargets();
    }

    share.accountOppSnapshot = function(opportunities,totalAccOpps){

        $scope.achievement = 0;
        $scope.pipeline = 0;
        $scope.totalDeals = "-";
        $scope.fyWon = "-";
        $scope.achievementWithCommas = 0;
        $scope.pipelineWithCommas = 0;
        var ngmReq = share.companyDetails && share.companyDetails;

        accountSnapshotLoaded = true;

        if(opportunities && opportunities.length>0){
            $scope.totalDeals = totalAccOpps;
            _.each(opportunities,function (op) {

                op.ngmReq = ngmReq;
                op.amountWithNgm = op.amount;

                if(ngmReq){
                    op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                }

                if(_.includes(['Close Won','Closed Won','Close Lost','Closed Lost'],op.relatasStage)){

                    if(ngmReq){
                        $scope.achievement = $scope.achievement+((op.amount*op.netGrossMargin)/100);
                    } else {
                        $scope.achievement = $scope.achievement+op.amount;
                    }

                } else {

                    if(ngmReq){
                        $scope.pipeline = $scope.pipeline+((op.amount*op.netGrossMargin)/100);
                    } else {
                        $scope.pipeline = $scope.pipeline+op.amount;
                    }
                }

                op.amountWithNgm = op.amountWithNgm.r_formatNumber(2)
            })
        } else {
            $scope.achievement = "-";
            $scope.pipeline = "-";
        }

        if($scope.achievement != "-"){
            $scope.achievementWithCommas = getAmountInThousands($scope.achievement,2,share.currency=="INR")
        }

        if($scope.pipeline != "-"){
            $scope.pipelineWithCommas = getAmountInThousands($scope.pipeline,2,share.currency=="INR")
        }
    }

});

relatasApp.controller("account_costs", function ($scope, $http, $rootScope, share) {

    share.accountCosts = function(account){
        $scope.account = account;
    }

    share.updateAccountCosts = function (costOfSales,costOfDelivery){
        $scope.account.costOfSales= costOfSales
        $scope.account.costOfDelivery = costOfDelivery

        $scope.account.costOfSalesWithComma = numberWithCommas(costOfSales)
        $scope.account.costOfDeliveryWithComma = numberWithCommas(costOfDelivery)

    }

    share.fetchInteractionsForContacts = function(OppsWon){

        var numOfOpps = OppsWon.length;
        var totalInteractions = 0;
        $scope.averageInteractionsPerDealWon = "-"

        if(OppsWon && OppsWon.length>0){
            _.each(OppsWon,function (op) {
                totalInteractions = totalInteractions+parseInt(op.interactionCount)
                if(op.partners && op.partners.length>0){
                    totalInteractions = totalInteractions+getContactsInteractionCountInOpp(op.partners)
                }
                if(op.influencers && op.influencers.length>0){
                    totalInteractions = totalInteractions+getContactsInteractionCountInOpp(op.influencers)
                }
                if(op.decisionMakers && op.decisionMakers.length>0){
                    totalInteractions = totalInteractions+getContactsInteractionCountInOpp(op.decisionMakers)
                }
            })

            if(totalInteractions){
                $scope.averageInteractionsPerDealWon = (totalInteractions/numOfOpps).r_formatNumber(2)
            } else {
                $scope.averageInteractionsPerDealWon = "-"
            }
        } else {
            $scope.averageInteractionsPerDealWon = "-"
        }

        share.intsPerDollar($scope.averageInteractionsPerDealWon)
    }
});

relatasApp.controller("account_insights", function ($scope, $http, $rootScope, share) {

    share.accountInsights = function(account,userIds) {
        $scope.account = account;
        
        var url = "/account/insights/new?account="+account.fullName;

        if(userIds){
            url = fetchUrlWithParameter(url, "userIdList", userIds)
        }

        url = fetchUrlWithParameter(url, "contacts", account.contacts);

        $http.get(url)
            .success(function (response) {
                if (response.SuccessCode) {
                    $scope.oppsPastCloseDate = response.Data.opps?response.Data.opps.length:0;
                    $scope.lastInteractionSentiment = response.Data.interaction && response.Data.interaction[0]?response.Data.interaction[0]._id:0;

                    if($scope.oppsPastCloseDate !=0 && $scope.lastInteractionSentiment !=0){
                        $scope.noInsightsFound = false;
                    } else {
                        $scope.noInsightsFound = true;
                    }
                } else {
                    $scope.noInsightsFound = true;
                }
            });
    }

});

relatasApp.controller("interactions_team_verbose", function ($scope, $http, $rootScope, share) {

    $scope.interactionInitiationsHierarchy = function (hierarchy) {
        share.interactionInitiations_load_test(hierarchy)
    }

    $scope.closeModal = function () {
        $scope.isOppModalOpen = false;
        $scope.showInteractionInitiations = false;
        $scope.viewSelection();
    }

    $scope.teamBlock = false;
    $scope.viewSelection = function (selection) {
        mixpanelTracker("Accounts>them-us-hierarchy "+selection);
        if(selection == "viewHierarchy"){
            $scope.active.hierarchy = "button-selected"
            $scope.active.company = "button"
            $scope.active.team = "button"
            $scope.viewHierarchy = true;
            $scope.teamBlock = false;
        } else if(selection == "teamBlock"){
            $scope.teamBlock = true;
            $scope.active.team = "button-selected"
            $scope.active.company = "button"
            $scope.active.hierarchy = "button"
            $scope.viewHierarchy = false;
        } else {
            $scope.teamBlock = false;
            $scope.viewHierarchy = false;
            $scope.active.team = "button"
            $scope.active.hierarchy = "button"
            $scope.active.company = "button-selected"
        }
    }

    share.closeHierarchy = function(){
        $scope.viewHierarchy = false;
    }
    share.interactionInitiations_load_test = function(hierarchy){
        $scope.interactionInitiations(hierarchy);
    }

    $scope.interactionInitiations = function (hierarchy) {
        mixpanelTracker("Accounts>Interaction Initiations ");
        $scope.teamBlock = false;
        $scope.active = {
            team:"button",
            hierarchy:"button",
            company:"button-selected"
        }

        $scope.showInteractionInitiations = true;

        var accountInteractions = _.map($scope.accountData,"count")
        var teamInteractions = _.map($scope.teamMembersData,"count")

        var lastInteractedDictionary = {};
        var lastInteractedDictionaryCompany = {};

        _.each($scope.lastInteractedData,function (el) {
            lastInteractedDictionary[el._id] = el
        });

        _.each($scope.lastInteractedData,function (el) {
            lastInteractedDictionaryCompany[el.emailId] = el
        });

        var max_acc = _.max(accountInteractions),
            max_team = _.max(teamInteractions)

        $scope.teamGraphs = [];
        $scope.accGraphs = [];

        if($scope.accountData && $scope.accountData.length>0){
            _.each($scope.accountData,function (el) {

                var designationColor = "grey"
                var designation = ""
                var months = {
                    month1:"",
                    monthO1:"",
                    month2:"",
                    monthO2:"",
                    month3:"",
                    monthO3:"",
                    month4:"",
                    monthO4:""
                }

                var team = "";

                if(lastInteractedDictionaryCompany[el._id] && lastInteractedDictionaryCompany[el._id]._id){
                    team = lastInteractedDictionaryCompany[el._id]._id
                }

                $scope.accGraphs.push({
                    style:max_acc?{'width':scaleBetween(el.count,1,max_acc)+'%',background: 'rgb(255, 182, 30)'}:'',
                    emailId:el._id,
                    emailIdForImg:el._id.substr(0,2).toUpperCase(),
                    count:el.count,
                    team:team,
                    lastInteractedSort:el.lastInteractedDate?new Date(el.lastInteractedDate):"-",
                    lastInteracted:el.lastInteractedDate?moment(el.lastInteractedDate).format(standardDateFormat()):"-",
                    designationColor:designationColor,
                    designation:designation,
                    months:months,
                });

            });

            $scope.accGraphs.sort(function (o1, o2) {
                return o2.count < o1.count ? -1 : o2.count > o1.count ? 1 : 0;
            });
        }

        if(max_team && $scope.teamMembersData && $scope.teamMembersData.length>0){
            _.each($scope.teamMembersData,function (el) {

                var lastInteractedDate = "";

                if(lastInteractedDictionary[el._id] && lastInteractedDictionary[el._id].lastInteractedDate){
                    lastInteractedDate = moment(lastInteractedDictionary[el._id].lastInteractedDate).format(standardDateFormat())
                }

                $scope.teamGraphs.push({
                    style:{'width':scaleBetween(el.count,1,max_team)+'%',background: 'rgb(15, 203, 227)'},
                    owner:share.teamDictionary[el._id],
                    count:el.count,
                    lastInteractedSort:new Date(el.lastInteractedDate),
                    lastInteracted:moment(el.lastInteractedDate).format(standardDateFormat())
                })
            })

            $scope.teamGraphs.sort(function (o1, o2) {
                return o2.count < o1.count ? -1 : o2.count > o1.count ? 1 : 0;
            });
        }

        if(hierarchy) {
            $scope.viewSelection("viewHierarchy")
        }
    }

    $scope.goToContact = function(emailId){
        window.location = '/contacts/all?contact='+emailId+'&acc=true'
    };

    share.getAccountAndTeamInteractions = function(account,userIds) {
        var url = "/account/interactions/verbose/new";
        var url2 = "/account/relationship/relevance";

        if (userIds) {
            url = fetchUrlWithParameter(url, "userIdList", userIds)
            url2 = fetchUrlWithParameter(url2, "userIdList", userIds)
        }

        url = fetchUrlWithParameter(url, "contacts", account.contacts);
        url2 = fetchUrlWithParameter(url2, "contacts", account.contacts);

        $http.get(url2)
            .success(function (response) {

                if(response.Data && response.Data.length>0){
                    var data = response.Data;

                    $scope.dataExists = 0;

                    if(response.Data.decisionMakers || response.Data.influencers || response.Data.others || response.Data.partners){
                        $scope.dataExists = 1;
                    }
                    data["pieChartId"] = ".rr-chart"
                    wholePie(data)
                }
            });

        $http.get(url)
            .success(function (response) {
                if (response.SuccessCode) {

                    $scope.accountData = response.Data.account
                    $scope.teamMembersData = response.Data.teamMembers
                    $scope.lastInteractedData = response.Data.lastInteractedByTeam;

                    $scope.teamMembersData.sort(function (o1, o2) {
                        return new Date(o1.lastInteractedDate) < new Date(o2.lastInteractedDate) ? 1 : new Date(o1.lastInteractedDate) > new Date(o2.lastInteractedDate) ? -1 : 0;
                    });

                    $scope.accountData.sort(function (o1, o2) {
                        return new Date(o1.lastInteractedDate) < new Date(o2.lastInteractedDate) ? 1 : new Date(o1.lastInteractedDate) > new Date(o2.lastInteractedDate) ? -1 : 0;
                    });

                    $scope.totalAccInteractionsInit = _.sumBy(response.Data.account, 'count');

                    if($scope.totalAccInteractionsInit == 0){
                        $scope.totalAccInteractionsInit = ""
                    }
                    share.radarChart(response)

                    $scope.totalTeamInteractionsInit = _.sumBy(response.Data.teamMembers, 'count');

                    summaryChart(response.Data.account,".account-chart",shadeGenerator(255,152,0,response.Data.account.length,15))
                    summaryChart(response.Data.teamMembers,".team-chart",shadeGenerator(0,188,212,response.Data.teamMembers.length,15),share)
                }
            });
    }

});

function getDealsAtRisk($scope,$http,url,share){

    $http.get(url)
        .success(function (response) {

            $scope.loading = false;

            var allDeals = [];

            if(response.data && response.data.length>0){
                allDeals = _.uniqBy(_.map(response.data,"deals"),"opportunityId")
            }

            if(share){
                share.dealsAtRiskFn(allDeals)
            }
            $scope.dealsAtRisk = allDeals.length>0?allDeals.length:"-"
        })
}

function drawTargets($scope,$http,url,share,account,callback){

    $http.get(url)
        .success(function (response) {
            var total = _.map(response.Data,"total")
            var won = _.map(response.Data,"won")
            var lost = _.map(response.Data,"lost")
            var target = account.target[0]?account.target[0].amount:0;

            var pipeline = 0;

            var t = target;
            var w = _.sum(won);

            var valArr = [];
            valArr.push(t,w,pipeline)

            var vmax = Math.max.apply( null, valArr );

            $scope.fyWonStyle = {'width':scaleBetween(w,1,vmax)+'%',background: '#8ECECB'}

            if(!w || w ==0 ){
                $scope.fyWonStyle = {'width':0+'%',background: '#8ECECB'}
            }
            
            if(t){
                $scope.wonPercentage = ((w/t)*100).r_formatNumber(2)+'%'
            } else {
                $scope.wonPercentage = "-"
            }

            if(w>t){
                $scope.won = {'width':100+'%',background: '#8ECECB'}
                $scope.wonPercentage = getAmountInThousands(w,2,share.currency=="INR");
            }

            $scope.targetValue = t;

        })
}

relatasApp.controller("interactionInitiations", function ($scope, $http, $rootScope, share) {

    share.interactionInitiations = function(account,userIds){

        var accountName = account.fullName;
        $rootScope.interaction_initiations_other_name = accountName;
        $rootScope.account_name_xs = accountName.slice(0,2).toUpperCase();

        $scope.loading = true;

        var url = "/account/relationship/new?account="+accountName;

        if(userIds){
            url = fetchUrlWithParameter(url,"userIdList",userIds)
        }

        url = fetchUrlWithParameter(url, "contacts", account.contacts);

        $http.get(url)
            .success(function(response){
                $scope.loading = false;
                $scope.dataFor = response.Data.dataFor;
                if(response.SuccessCode){

                    if(response.Data.relation && response.Data.relation.relationshipStrength_updated){
                        $scope.relationshipStrength_updated = response.Data.relation.relationshipStrength_updated;
                    }
                    else $scope.relationshipStrength_updated = 0;

                    if(response.Data.interactionInitiations && response.Data.interactionInitiations.length){
                        $scope.interaction_initiations_total = 0;

                        for(var i=0; i<response.Data.interactionInitiations.length; i++){
                            $scope.interaction_initiations_total += response.Data.interactionInitiations[i].count
                            if(response.Data.interactionInitiations[i]._id == 'you'){
                                $rootScope.interaction_initiations_you = response.Data.interactionInitiations[i].count
                            }
                            if(response.Data.interactionInitiations[i]._id == 'others'){
                                $rootScope.interaction_initiations_other = response.Data.interactionInitiations[i].count
                            }
                        }

                        var a = ($rootScope.interaction_initiations_you*100)/$scope.interaction_initiations_total
                        var b = ($rootScope.interaction_initiations_other*100)/$scope.interaction_initiations_total

                        a = Math.round(a)
                        b = Math.round(b)
                        // $scope.drawDonut(a,b)
                    }
                    else{
                        $scope.interaction_initiations_total = 0
                        $rootScope.interaction_initiations_you = 0
                        $rootScope.interaction_initiations_other = 0
                        $scope.relationshipStrength_updated = 0
                        // $scope.drawDonut(0,0)
                    }

                    if(response.Data.relationshipStrength && response.Data.relationshipStrength.length > 0){
                        var obj = response.Data.relationshipStrength[0];
                        var doc = response.Data.doc;

                        var x = obj.maxCount - obj.minCount;

                        var ratio = x/(doc.maxRatio-doc.minRatio);

                        var s = Math.round(doc.minRatio+((obj.maxCount - obj.minCount)/ratio))

                        $scope.relationshipStrength = s ? s : 0;

                    }
                    else $scope.relationshipStrength = 0;

                    if($scope.relationshipStrength_updated <= 0){
                        $scope.relationshipStrength_updated = $scope.relationshipStrength;
                    }
                    function updateRatio(value){
                        $scope.relationshipStrength_updated = value;
                        var reqObj = {contactId:response.Data.relation._id,relationshipStrength_updated:value};
                        $http.post('/contacts/update/relationship/strength/web',reqObj)
                            .success(function(response){
                                if(response.SuccessCode){
                                    $("#relationshipStrength_updated").text(value);
                                }
                                else toastr.error(response.Message);
                            });
                    }

                    if(response.Data.interactionTypes && response.Data.interactionTypes.length > 0){
                        $scope.interactionsByType = [];
                        var existingTypes = [];
                        var allTypes = ['meeting','call','sms','email','facebook','twitter','linkedin'];
                        for(var t=0; t<response.Data.interactionTypes[0].typeCounts.length; t++){
                            var interactionTypeObj = getInteractionTypeObj(response.Data.interactionTypes[0].typeCounts[t],response.Data.interactionTypes[0].maxCount);
                            if(interactionTypeObj != null){
                                existingTypes.push(interactionTypeObj.type);
                                $scope.interactionsByType.push(interactionTypeObj);
                            }
                        }
                        var mobileCountStatus = 0;
                        var mobileCountStatusLists = ['call','sms','email'];
                        for(var u=0; u<allTypes.length; u++){
                            if(existingTypes.indexOf(allTypes[u]) == -1){
                                var newObj = getInteractionTypeObj({_id:allTypes[u],count:0},response.Data.interactionTypes[0].maxCount)
                                if(newObj != null){
                                    if(mobileCountStatusLists.indexOf(allTypes[u]) != -1){
                                        mobileCountStatus ++;
                                    }
                                    $scope.interactionsByType.push(newObj);
                                }
                            }
                        }

                        if(mobileCountStatus > 0){
                            $scope.showDownloadMobileApp = true;
                        }else $scope.showDownloadMobileApp = false;
                        $scope.showSocialSetup = response.Data.showSocialSetup;

                        $scope.interactionsByType.sort(function (o1, o2) {
                            return o1.priority < o2.priority ? -1 : o1.priority > o2.priority ? 1 : 0;
                        });
                        $scope.interactionsByType = {
                            a:$scope.interactionsByType[0],
                            b:$scope.interactionsByType[1],
                            c:$scope.interactionsByType[2],
                            d:$scope.interactionsByType[3],
                            e:$scope.interactionsByType[4],
                            f:$scope.interactionsByType[5],
                            g:$scope.interactionsByType[6]
                        };

                    }
                    else{
                        $scope.interaction_initiations_total = 0;
                        $scope.interactionsByType = {"a":{"priority":0,"value":"height:0%","type":"meeting","count":0,"title":"0 interactions"},"b":{"priority":1,"value":"height:0%","type":"call","count":0,"title":"0 interactions"},"c":{"priority":2,"value":"height:0%","type":"sms","count":0,"title":"0 interactions"},"d":{"priority":3,"value":"height:0%","type":"email","count":0,"title":"0 interactions"},"e":{"priority":4,"value":"height:0%","type":"facebook","count":0,"title":"0 interactions"},"f":{"priority":5,"value":"height:0%","type":"twitter","count":0,"title":"0 interactions"},"g":{"priority":6,"value":"height:0%","type":"linkedin","count":0,"title":"0 interactions"}}
                    }
                }
            });
    };

    $scope.drawDonut = function(a,b){

        var canvas  = document.getElementById("donut-chart");
        var chart = canvas.getContext("2d");

        function drawdonutChart(canvas) {

            this.x , this.y , this.radius , this.lineWidth , this.strockStyle , this.from , this.to = null;
            this.set = function( x, y, radius, from, to, lineWidth, strockStyle)
            {
                this.x = x;
                this.y = y;
                this.radius = radius;
                this.from=from;
                this.to= to;
                this.lineWidth = lineWidth;
                this.strockStyle = strockStyle;
            }

            this.draw = function(data) {
                canvas.beginPath();
                canvas.lineWidth = this.lineWidth;
                canvas.strokeStyle = this.strockStyle;
                canvas.arc(this.x , this.y , this.radius , this.from , this.to);
                canvas.stroke();
                var numberOfParts = data.numberOfParts;
                var parts = data.parts.pt;
                var colors = data.colors.cs;
                var df = 0;
                for(var i = 0; i<numberOfParts; i++)
                {
                    canvas.beginPath();
                    canvas.strokeStyle = colors[i];
                    canvas.arc(this.x, this.y, this.radius, df, df + (Math.PI * 2) * (parts[i] / 100));
                    canvas.stroke();
                    df += (Math.PI * 2) * (parts[i] / 100);
                }
            }
        }
        var data =
        {
            numberOfParts: 2,
            parts:{"pt": [a , b ]},//percentage of each parts
            colors:{"cs": ["#f86b4f", "rgba(248, 107, 79, 0.65)"]}//color of each part
        };

        var drawDount = new drawdonutChart(chart);

        drawDount.set(100, 100, 45, 0, Math.PI*2, 8, "#fff");

        if(data.parts.pt[0] == 0 && data.parts.pt[1] == 0){
            data = {
                numberOfParts: 2,
                parts:{"pt": [99 , 0 ]},//percentage of each parts
                colors:{"cs": ["#bbb", "#bbb"]}//color of each part
            };
        }

        drawDount.draw(data);
    }

});

relatasApp.controller("opportunitiesByStage",function ($scope,$http,share) {

    share.forSnapshot = function (account,userIds) {
        getPipelineSnapshot(userIds,account)
    }

    function getPipelineSnapshot(userIds,account) {
        var url = "/account/group/count"

        if(userIds){
            url = fetchUrlWithParameter(url,"userIdList",userIds)
        }

        url = fetchUrlWithParameter(url, "contacts", account.contacts);

        $http.get(url)
            .success(function (response) {

                var oppExist = false;
                if(response && response.SuccessCode && response.Data.length>0){
                    if(response.fy){
                        $scope.fiscalYear = moment(response.fy.fromDate).format("MMMM YY")+" - "+moment(response.fy.toDate).format("MMM YY")
                    }

                    oppExist = true;

                    var maxCount = _.max(_.map(response.Data,"count"))
                    var minCount = _.min(_.map(response.Data,"count"))
                    var maxAmount = _.max(_.map(response.Data,"totalAmount"))
                    var minAmount = _.min(_.map(response.Data,"totalAmount"))

                    $scope.prospectColorLeft = "white";
                    $scope.EvaluationColorLeft = "white";
                    $scope.proposalColorLeft = "white";
                    $scope.wonColorLeft = "white";
                    $scope.lostColorLeft = "white";

                    $scope.prospectColor = "white";
                    $scope.EvaluationColor = "white";
                    $scope.proposalColor = "white";
                    $scope.wonColor = "white";
                    $scope.lostColor = "white";

                    $scope.stages = _.map(share.opportunityStages,"name");

                    $scope.funnels = [];
                    var stagesWithData = [];

                    function getPipelineFunnel(){
                        if($scope.stages){

                            _.each(response.Data,function (el) {

                                _.each($scope.stages,function (st) {

                                    if(el._id == st){
                                        stagesWithData.push(st);
                                        $scope.funnels.push({
                                            name:st,
                                            countLength:{'width':scaleBetween(el.count, minCount, maxCount)+'%'},
                                            amountLength:{'width':scaleBetween(el.totalAmount, minAmount, maxAmount)+'%'},
                                            amount:el.totalAmount,
                                            oppsCount:el.count
                                        })
                                    }
                                })
                            });

                            var noDataStages = _.difference($scope.stages,stagesWithData)

                            _.each(noDataStages,function (st) {
                                $scope.funnels.push({
                                    name:st,
                                    countLength:{'width':0+'%'},
                                    amountLength:{'width':0+'%'},
                                    amount:0,
                                    oppsCount:0
                                })
                            })

                            _.each($scope.funnels,function (fl) {

                                _.each(share.opportunityStages,function (st) {
                                    if(fl.name == st.name){
                                        fl.order = st.order
                                    }
                                })
                            });

                            $scope.funnels = _.sortBy($scope.funnels,function (o) {
                                o.amount = o.amount.r_formatNumber(2)
                                return o.order
                            })

                        } else {
                            setTimeOutCallback(1000,function () {
                                getPipelineFunnel()
                            });
                        }
                    }

                    getPipelineFunnel()
                }

                $scope.noPipeline = oppExist;
            });
    }

});

relatasApp.controller("opp_growth",function ($scope,$http,share,$rootScope) {

    share.forOppGrowth = function (account,userIds,accessControl) {
        getOppGrowth(account,userIds,accessControl)
    }

    function getOppGrowth(account,userIds,accessControl) {
        setTimeOutCallback(500, function () {
            var url = "/account/conversion/rate"

            if(userIds){
                url = fetchUrlWithParameter(url,"userIdList",userIds);
            }

            url = fetchUrlWithParameter(url, "contacts", account.contacts);

            $http.get(url)
                .success(function (response) {
                    if (response && response.SuccessCode) {

                        $scope.noOpp = true;
                        var label = [], seriesOpen = [], seriesClose = [];

                        _.each(response.Data, function (el) {
                            if(el.count>0){
                                $scope.noOpp = false;
                            }
                            label.push(monthNames[moment(new Date(el.sortDate)).month()].substring(0,3))
                            seriesOpen.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
                        });

                        _.each(response.dataClosed, function (el) {
                            seriesClose.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
                        });

                        $scope.label2 = "Created",$scope.label1 = "Won";

                        drawLineChart($scope,share,seriesOpen,label,".opp-growth",seriesClose);

                    } else {
                        $scope.noOpp = true;
                    }
                })
        });
    }

    $scope.createOpp = function () {
        share.createOpp()
    }
    
});

relatasApp.controller("spiderChart",function ($scope,$http,share,$rootScope) {

    $scope.loading = true;
    share.dealsAtRiskFn = function(deals) {
        $scope.dealsAtRiskAmt = 0;
        $scope.dealsAtRiskCount = deals.length;

        _.each(deals,function (op) {
            op.amountWithNgm = (op.amount*op.netGrossMargin)/100;
            op.convertedAmt = op.amount;
            op.convertedAmtWithNgm = op.amountWithNgm

            if(op.currency && op.currency !== $rootScope.currency){

                if(share.currenciesObj[op.currency] && share.currenciesObj[op.currency].xr){
                    op.convertedAmt = op.amount/share.currenciesObj[op.currency].xr
                }

                if(op.netGrossMargin || op.netGrossMargin == 0){
                    op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                }

                op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

            }

            $scope.dealsAtRiskAmt = $scope.dealsAtRiskAmt+op.convertedAmtWithNgm;
        })

        $scope.dealsAtRiskAmt = getAmountInThousands($scope.dealsAtRiskAmt,2,share.currency=="INR")
    }

    share.intsPerDollar = function(averageInteractionsPerDealWon) {
        $scope.averageInteractionsPerDealWon = averageInteractionsPerDealWon
    }

    share.intsWithImportantContacts = function(data) {
        $scope.noDmsAmt = 0;
        $scope.noInflsAmt = 0;
        $scope.noDmsCount = 0;
        $scope.noInflsCount = 0;
        $scope.avgDaysToWin = 0;
        $scope.avgDaysSinceOpen = 0;
        $scope.allOppsCount = 0;

        var wonOpps = [];

        if(data.Data && data.Data.opportunities && data.Data.opportunities.length>0){
            $scope.allOppsCount = data.Data.opportunities.length;

            var openOpps = data.Data.opportunities.filter(function (op) {
                if(op.stageName != "Close Won" && op.stageName != "Close Lost") {
                    op.amountWithNgm = (op.amount*op.netGrossMargin)/100;
                    op.convertedAmt = op.amount;
                    op.convertedAmtWithNgm = op.amountWithNgm

                    if(op.currency && op.currency !== $rootScope.currency){

                        if(share.currenciesObj[op.currency] && share.currenciesObj[op.currency].xr){
                            op.convertedAmt = op.amount/share.currenciesObj[op.currency].xr
                        }

                        if(op.netGrossMargin || op.netGrossMargin == 0){
                            op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                        }

                        op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

                    }
                    return op;
                } else {

                    if(op.stageName == "Close Won"){
                        wonOpps.push(op)
                    }
                }
            })

            var daysToWin = 0,
            daysSinceOpen = 0;

            _.each(wonOpps,function (op) {
                var createDt = moment(op.createdDate);
                var closeDt = moment(op.closeDate);
                var daysToWinCloseDeal= 0;

                if(closeDt.diff(createDt, 'days') === 0){
                    daysToWinCloseDeal = daysToWinCloseDeal+1 //Minimum one day taken to close opp
                } else {
                    daysToWinCloseDeal = daysToWinCloseDeal+Math.abs(closeDt.diff(createDt, 'days'));
                }

                daysToWin = daysToWin+daysToWinCloseDeal;
            })

            _.each(openOpps,function (op) {
                var createDt = moment(op.createdDate);
                if(moment().diff(createDt, 'days') === 0){
                    daysSinceOpen = daysSinceOpen+1 //Minimum one day taken to close opp
                } else {
                    daysSinceOpen = daysSinceOpen+Math.abs(moment().diff(createDt, 'days'));
                }

                if(op.influencers.length == 0 ){
                    $scope.noInflsAmt = $scope.noInflsAmt+op.convertedAmtWithNgm;
                    $scope.noInflsCount++
                }

                if(op.decisionMakers.length == 0 ){
                    $scope.noDmsAmt = $scope.noDmsAmt+op.convertedAmtWithNgm;
                    $scope.noDmsCount++
                }
            });

            if(wonOpps.length>0){
                $scope.avgDaysToWin = parseFloat((daysToWin/wonOpps.length).r_formatNumber(2));
            }

            if(openOpps.length>0){
                $scope.avgDaysSinceOpen = parseFloat((daysSinceOpen/openOpps.length).r_formatNumber(2));
            }

            $scope.noDmsAmt = getAmountInThousands($scope.noDmsAmt,2,share.currency=="INR")
            $scope.noInflsAmt = getAmountInThousands($scope.noInflsAmt,2,share.currency=="INR")

        }
    }

    share.radarChart = function(data){

        share.intsWithImportantContacts(data);
        function checkHierarchyObjExists(){

            if(!_.isEmpty(share.hierarchyObjByEmailId)){
                spiderChartInit(share,40,function (RadarChart) {
                    $scope.loading = false;
                    $("#spiderChart2").empty();
                    spiderDataInit(RadarChart,data,share,200,200,"#spiderChart2",$scope);
                });
            } else {
                setTimeOutCallback(200,function () {
                    checkHierarchyObjExists()
                })
            }
        }
        checkHierarchyObjExists();
    }

    $scope.interactionInitiationsHierarchy = function (hierarchy) {
        mixpanelTracker("Accounts>Opps vs ints");
        share.interactionInitiations_load_test(hierarchy)
    }
});

function spiderChartInit(share,TranslateX,callback) {

    var relationshipColors = {
        "decision-maker":"#db856c",
        "influencer": "#008080a1",
        "none":"#ccc"
    }

    var RadarChart = {
        draw: function(id, d, options){
            var cfg = {
                radius: 2, //dot radii
                w: 300,
                h: 300,
                factor: 1,
                factorLegend: .85,
                levels: 5,
                maxValue: 100,
                radians: 2 * Math.PI,
                opacityArea: 0.5,
                ToRight: 5,
                TranslateX: TranslateX?TranslateX:80,
                TranslateY: 30,
                ExtraWidthX: 100,
                ExtraWidthY: 100,
                color: d3.scale.category10()
            };

            if('undefined' !== typeof options){
                for(var i in options){
                    if('undefined' !== typeof options[i]){
                        cfg[i] = options[i];
                    }
                }
            }
            cfg.maxValue = Math.max(cfg.maxValue, d3.max(d, function(i){return d3.max(i.map(function(o){return o.value;}))}));
            var allAxis = (d[0].map(function(i, j){return i.axis}));
            var total = allAxis.length;
            var radius = cfg.factor*Math.min(cfg.w/2, cfg.h/2);
            var Format = d3.format('%');
            d3.select(id).select("svg").remove();

            var g = d3.select(id)
                .append("svg")
                .attr("width", cfg.w+cfg.ExtraWidthX)
                .attr("height", cfg.h+cfg.ExtraWidthY)
                .append("g")
                .attr("transform", "translate(" + cfg.TranslateX + "," + cfg.TranslateY + ")");
            ;

            var tooltip;

            //Circular segments
            for(var j=0; j<cfg.levels-1; j++){
                var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
                g.selectAll(".levels")
                    .data(allAxis)
                    .enter()
                    .append("svg:line")
                    .attr("x1", function(d, i){return levelFactor*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
                    .attr("y1", function(d, i){return levelFactor*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
                    .attr("x2", function(d, i){return levelFactor*(1-cfg.factor*Math.sin((i+1)*cfg.radians/total));})
                    .attr("y2", function(d, i){return levelFactor*(1-cfg.factor*Math.cos((i+1)*cfg.radians/total));})
                    .attr("class", "line")
                    .style("stroke", "#ccc")
                    .style("stroke-opacity", "0.75")
                    .style("stroke-width", "0.3px")
                    .attr("transform", "translate(" + (cfg.w/2-levelFactor) + ", " + (cfg.h/2-levelFactor) + ")");
            }

            series = 0;

            var axis = g.selectAll(".axis")
                .data(allAxis)
                .enter()
                .append("g")
                .attr("class", "axis");

            axis.append("line")
                .attr("x1", cfg.w/2)
                .attr("y1", cfg.h/2)
                .attr("x2", function(d, i){return cfg.w/2*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
                .attr("y2", function(d, i){return cfg.h/2*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
                .attr("class", "line")
                .style("stroke", "grey")
                .style("stroke-width", "0.3px");


            // share.hierarchyObjByEmailId[d].fullName;
            // "("+share.hierarchyObjByEmailId[d].designation+")"+

            // .attr("dy", "1.5em")
            // .attr("transform", function(d, i){return "translate(0, -10)"})
            // .attr("x", function(d, i){return cfg.w/2*(1-cfg.factorLegend*Math.sin(i*cfg.radians/total))-60*Math.sin(i*cfg.radians/total);})
            // .attr("y", function(d, i){return cfg.h/2*(1-Math.cos(i*cfg.radians/total))-20*Math.cos(i*cfg.radians/total);});
            axis.append("text")
                .attr("class", "legend")
                // .text(function(d){
                //     return d
                // })
                .on('mouseover', function (d){

                    var fullName = share.hierarchyObjByEmailId[d].fullName?share.hierarchyObjByEmailId[d].fullName:d;
                    var designation = share.hierarchyObjByEmailId[d].designation?"("+share.hierarchyObjByEmailId[d].designation+")":null
                    var fullText = fullName
                    if(designation){
                        fullText = fullName+", "+designation
                    }

                    $(this)
                        .attr("class", "no-pointer")
                        .text(fullText)
                        .css({'margin-top':'100px'})
                        .css({'font-size':'11px'})
                        .css({'pointer-events':'none'})
                })
                .on('mouseout', function(d){
                    $(this)
                        .attr("class", "legend")
                        .text(".")
                        .attr("fill", function (d) {
                            // if (checkRequired(share.hierarchyObjByEmailId[d].relationshipType)){
                            //     return relationshipColors[share.hierarchyObjByEmailId[d].relationshipType]
                            // } else {
                            //     return relationshipColors["none"]
                            // }
                        })
                        .css("font-size", "70px")
                        .css({'pointer-events':'auto'})

                })
                // .append('svg:tspan')
                .attr("dy", "0.25em")
                .attr("transform", function(d, i){return "translate(0, -10)"})
                .attr("x", function(d, i){return cfg.w/2*(0.9-cfg.factorLegend*Math.sin(i*cfg.radians/total))-60*Math.sin(i*cfg.radians/total);})
                .attr("y", function(d, i){return cfg.h/2*(1-Math.cos(i*cfg.radians/total))-20*Math.cos(i*cfg.radians/total);})
                .text(function(d) {
                    if(!TranslateX){
                        return ".";
                    }
                })
                .attr("fill", function (d) {
                    if (share.hierarchyObjByEmailId[d] && checkRequired(share.hierarchyObjByEmailId[d].relationshipType)){
                        return relationshipColors[share.hierarchyObjByEmailId[d].relationshipType]
                    } else {
                        return relationshipColors["none"]
                    }
                })
                .style("font-family", "Lato")
                .style("font-size", "70px")
                .attr("text-anchor", "middle")


            d.forEach(function(y, x){
                dataValues = [];
                g.selectAll(".nodes")
                    .data(y, function(j, i){
                        dataValues.push([
                            cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)),
                            cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total))
                        ]);
                    });
                dataValues.push(dataValues[0]);
                g.selectAll(".area")
                    .data([dataValues])
                    .enter()
                    .append("polygon")
                    .attr("class", "radar-chart-serie"+series)
                    .style("stroke-width", "2px")
                    .style("stroke", cfg.color(series))
                    .attr("points",function(d) {
                        var str="";
                        for(var pti=0;pti<d.length;pti++){
                            str=str+d[pti][0]+","+d[pti][1]+" ";
                        }
                        return str;
                    })
                    .style("fill", function(j, i){return cfg.color(series)})
                    .style("fill-opacity", cfg.opacityArea)
                    .on('mouseover', function (d){
                        z = "polygon."+d3.select(this).attr("class");
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", 0.1);
                        g.selectAll(z)
                            .transition(200)
                            .style("fill-opacity", .7);
                    })
                    .on('mouseout', function(){
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", cfg.opacityArea);
                    });
                series++;
            });
            series=0;


            d.forEach(function(y, x){
                g.selectAll(".nodes")
                    .data(y).enter()
                    .append("svg:circle")
                    .attr("class", "radar-chart-serie"+series)
                    .attr('r', cfg.radius)
                    .attr("alt", function(j){return Math.max(j.value, 0)})
                    .attr("cx", function(j, i){
                        dataValues.push([
                            cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)),
                            cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total))
                        ]);
                        return cfg.w/2*(1-(Math.max(j.value, 0)/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total));
                    })
                    .attr("cy", function(j, i){
                        return cfg.h/2*(1-(Math.max(j.value, 0)/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total));
                    })
                    .attr("data-id", function(j){return j.axis})
                    .style("fill", cfg.color(series)).style("fill-opacity", .9)
                    .on('mouseover', function (d){

                        var fullName = share.hierarchyObjByEmailId[d.axis].fullName?share.hierarchyObjByEmailId[d.axis].fullName:d.axis;
                        var designation = share.hierarchyObjByEmailId[d.axis].designation?"("+share.hierarchyObjByEmailId[d.axis].designation+")":null
                        var fullText = fullName;
                        if(designation){
                            fullText = fullName+designation
                        }

                        newX =  parseFloat(d3.select(this).attr('cx')) - 60;
                        newY =  parseFloat(d3.select(this).attr('cy')) - 5;

                        tooltip
                            .attr('x', newX)
                            .attr('y', newY)
                            // .text(Format(d.axis))
                            .text(fullText)
                            // .attr('y', newY+30)
                            // .text(designation)
                            .transition(200)
                            .style('opacity', 1);

                        z = "polygon."+d3.select(this).attr("class");
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", 0.1);
                        g.selectAll(z)
                            .transition(200)
                            .style("fill-opacity", .7);
                    })
                    .on('mouseout', function(){
                        tooltip
                            .transition(200)
                            .style('opacity', 0);
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", cfg.opacityArea);
                    })
                    .append("svg:title")
                    .text(function(j){
                        // return Math.max(j.value, 0)
                    });

                series++;
            });
            // Tooltip
            tooltip = g.append('text')
                .style('opacity', 0)
                .style('font-family', 'Lato')
                .style('font-size', '11px');
        }
    };

    callback(RadarChart)
}

function spiderDataInit(RadarChart,data,share,w,h,id,$scope){
    w = w?w:200;
    h = h?h:200;
    id = id?id:"#spiderChart"

    var colorscale = d3.scale.category10();

//Legend titles
    var LegendOptions = ['Interactions','Opportunities'];
    var opps = [],
        interactions = [];

    if(data.Data.account && data.Data.account.length>0){

        _.each(data.Data.account,function (el) {
            interactions.push({
                axis: el._id,
                value: el.count,
                interactionsCount: el.count
            })
        })
    }

    if(data.Data.opportunities && data.Data.opportunities.length>0){
        opps = _
            .chain(data.Data.opportunities)
            .groupBy("contactEmailId")
            .map(function (values, key) {
                var oppsAmount = 0;
                _.each(values,function (op) {
                    op.amountWithNgm = op.amount;
                    op.amountWithNgm = (op.amount*op.netGrossMargin)/100;
                    oppsAmount = oppsAmount+op.amountWithNgm;
                })
                return {
                    axis: key,
                    oppsAmount: oppsAmount,
                    value: values.length
                };
            }).value()
    }

    var nonExistingInOpps = [],
        nonExistingInInts = [];

    if(interactions.length>opps.length){
        nonExistingInOpps = _.differenceBy(interactions,opps,"axis")
    } else {
        nonExistingInOpps = _.differenceBy(interactions,opps,"axis")
    }

    if(opps.length>interactions.length){
        nonExistingInInts = _.differenceBy(opps,interactions,"axis")
    } else {
        nonExistingInInts = _.differenceBy(opps,interactions,"axis")
    }

    if(nonExistingInOpps.length>0){
        _.each(nonExistingInOpps,function (el) {
            opps.push({
                axis:el.axis,
                value: 0,
                oppsAmount:0
            })
        })
    }

    if(nonExistingInInts.length>0){
        _.each(nonExistingInInts,function (el) {
            interactions.push({
                axis:el.axis,
                value: 0,
                interactionsCount:0
            })
        })
    }

    share.accIntObj = {};

    var minInt = _.minBy(interactions,"value");
    var maxInt = _.maxBy(interactions,"value");
    var minOpp = _.minBy(opps,"value");
    var maxOpp = _.maxBy(opps,"value");
    var minOppAllowed = minOpp.value>0?1:0;
    var minIntAllowed = minInt.value>0?1:0;
    var dataOpps = [],
        dataInts = []

    _.each(interactions,function (el) {

        share.accIntObj[el.axis] = el;

        dataInts.push({
            axis:el.axis,
            interactionsCount:el.interactionsCount,
            value:scaleBetween(el.value,minInt.value,maxInt.value,minIntAllowed)
        })
    })

    if(opps.length>0) {
        _.each(opps,function (el) {
            if(share.accIntObj[el.axis]){
                share.accIntObj[el.axis].oppsAmount = el.oppsAmount
            } else {
                share.accIntObj[el.axis] = el;
            }

            dataOpps.push({
                axis:el.axis,
                oppsAmount:el.oppsAmount,
                value: scaleBetween(el.value,minOpp.value,maxOpp.value,minOppAllowed)
            })
        })
    };

    $scope.noAccountIntsAndOpps = true;

    dataOpps = _.sortBy(dataOpps,"axis");
    dataInts = _.sortBy(dataInts,"axis");

    _.each(dataOpps,function (op) {
        if(op.value && op.value>0){
            $scope.noAccountIntsAndOpps = false;
        }
    })
    _.each(dataInts,function (op) {
        if(op.value && op.value>0){
            $scope.noAccountIntsAndOpps = false;
        }
    });
//Data
    var d = [dataInts,dataOpps];

//Options for the Radar chart, other than default
    var mycfg = {
        w: w,
        h: h,
        maxValue: 100,
        levels: 5,
        ExtraWidthX: 300
    }

//Call function to draw the Radar chart
//Will expect that data is in %'s
    RadarChart.draw(id, d, mycfg);

    var svg = d3.select('#body')
        .selectAll('svg')
        .append('svg')
        .attr("width", w+300)
        .attr("height", h)
}

function getContactsInteractionCountInOpp(data) {

    var count = 0;

    _.each(function (el) {
        count = count+el.interactionCount
    })

    return count;
}

function highlightFilterSelection($scope,options){

    _.each($scope.filters,function (el) {

        if(el.filter == options){
            el.class = "selected"
        } else {
            el.class = ""
        }
    });
}

function removeSelectionForOthers($scope,account){
    
    _.each($scope.contactsList,function (el) {

        if(el.fullName == account.fullName){
            el.selection = "contact-selected"
        } else {
            el.selection = ""
        }
    });
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];