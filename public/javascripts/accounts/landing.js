relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/customer/middle/bar"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

            if(typeof str == 'string' && str.length > 0){
                if(searchContent.length >= 3) {
                    yourNetwork = yourNetwork ? true : false;
                    extendedNetwork = extendedNetwork ? true : false;
                    forCompanies = forCompanies ? true : false;
                    window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;

                }
            }
            else toastr.error("Please enter search content")
    };
});

relatasApp.controller("logedinUser", function ($scope, $http, $rootScope,share) {

    getTeam($scope,$http,function (teamArray,teamDictionary,usersDictionaryWithUserId) {
        share.teamDictionary = teamDictionary;
        share.teamArray = teamArray;
        share.usersDictionaryWithUserId = usersDictionaryWithUserId;
    });

    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        
        $rootScope.$broadcast("fetchedUserProfile", response.Data.corporateUser)

        share.getCurrentFiscalYear = getCurrentFiscalYear(timezone,response.companyDetails.fyMonth)
        share.fiscalYears(share.getCurrentFiscalYear)

        $rootScope.currency = "fa fa-dollar"

        if(response.companyDetails.currency){

            if(response.companyDetails.currency.toLowerCase() === 'yen'){
                $rootScope.currency = 'fa fa-yen';
            }
            if(response.companyDetails.currency.toLowerCase() === 'inr'){
                $rootScope.currency = 'fa fa-inr';
            }
            if(response.companyDetails.currency.toLowerCase() === 'euro'){
                $rootScope.currency = 'fa fa-euro';
            }
        }

        if(response.Data.contactsUpload && response.Data.contactsUpload.isUploaded){

        }else $("#uploadContactsAlert").removeClass("hide");
    })

});

relatasApp.controller("left_contacts_bar", function ($scope, $http, $rootScope, share) {

    $scope.contactsList = [];
    $scope.currentLength = 0;
    $scope.lastFetched = 0;
    $scope.filterBy = 'all';
    $scope.show_contacts_back = false;
    $scope.show_filter = false;
    $scope.usersInHierarchy = []
    var isFilter = false;

    $scope.searchContacts = function(searchContent){

        $scope.searchContent = searchContent;

        if(checkRequired(searchContent)){
            $scope.filterBy = isFilter ? $scope.filterBy : 'search';
            $scope.contactsList = [];
            $scope.currentLength = 0;
            var url = fetchUrlWithParameter("/company/middle/bar/accounts", "hierarchylist", $scope.usersInHierarchy)
            $scope.getContacts(null,url,0,25,$scope.filterBy,searchContent,null,function (account) {
                $scope.accountSelected(account)
            });
        }
    };

    $scope.refreshContactsList = function(){
        $scope.setFilter("Recent")
    };

    $scope.filterSelection = "Recent";
    $scope.filters = [];

    var filtersTemp = ["All","My","Recent","Important"];

    _.each(filtersTemp,function (el) {
        var selected = "selected"

        if(el != "Recent"){
            selected = ""
        }

        $scope.filters.push({
            class:selected,
            filter:el
        });
    });

    $scope.setFilter = function (options) {
        $scope.filterBy = isFilter ? $scope.filterBy : 'all';
        $("#searchContent").val("");
        $scope.searchContent = null;
        $scope.contactsList = [];
        $scope.currentLength = 0;
        $scope.filterSelection = options;
        
        $scope.searchContent = JSON.parse(JSON.stringify($scope.searchContent));
        
        highlightFilterSelection($scope,options);

        var url = fetchUrlWithParameter("/company/middle/bar/accounts", "hierarchylist", $scope.usersInHierarchy)

        $scope.getContacts(options,url,0,25,$scope.filterBy,null,null,function (account) {
            $scope.accountSelected(account)
        })
    }

    $scope.getContacts = function(options,url,skip,limit,filterBy,searchContent,isBack,callback){

        $scope.isBack = isBack;
        $scope.skip = skip;
        $scope.limit = limit;

        if(url.indexOf("?") == -1)
            url = url + "?"
        else
            url = url + "&"
        if(checkRequired(searchContent)){
            url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy+'&searchContent='+searchContent
        }
        else url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy;

        if(options){
            url = url+"&options="+options
        }
        
        $http.get(url)
            .success(function(response){
                $scope.contactsList = [];
                $scope.isConnectionOpen = false;
                if(response.SuccessCode){

                    $scope.grandTotal = response.Data.total;

                    if(response.Data.accounts.length > 0){

                        if(!isBack || $scope.currentLength <= 0){
                            $scope.currentLength += response.Data.returned;
                            $scope.returned = response.Data.returned
                        }

                        if($scope.currentLength > 25){
                            $scope.show_contacts_back = true;
                        }else $scope.show_contacts_back = false;

                        for(var i=0; i<response.Data.accounts.length; i++){

                            var name = response.Data.accounts[i].name;
                            var hasAccess = false;
                            var accessIcon = "fa fa-lock";
                            var ownerEmailId = response.Data.accounts[i].ownerEmailId;
                            var ownerId = response.Data.accounts[i].ownerId;

                            var usersWithAccess = [];

                            _.each(response.Data.accounts[i].access,function (el) {
                                if(el.emailId != ownerEmailId){
                                    usersWithAccess.push(el.emailId)
                                }
                            })

                            if(response.Data.accounts[i].reportingHierarchyAccess && response.Data.accounts[i].heiarchyWithAccess && response.Data.accounts[i].heiarchyWithAccess.length>0){
                                _.each(response.Data.accounts[i].heiarchyWithAccess,function (el) {
                                    usersWithAccess.push(share.usersDictionaryWithUserId[el].emailId)
                                })
                            }

                            if(_.includes(usersWithAccess,response.liuEmailId) || ownerEmailId == response.liuEmailId){
                                hasAccess = true;
                                accessIcon = ""
                            } else {
                                hasAccess = false;
                                accessIcon = "fa fa-lock"
                            }
                            
                            var costOfSalesWithComma = 0,
                                costOfDeliveryWithComma = 0;

                            if(response.Data.accounts[i].costOfSales){
                                costOfSalesWithComma = getAmountInThousands(response.Data.accounts[i].costOfSales,2)
                            }

                            if(response.Data.accounts[i].costOfDelivery){
                                costOfDeliveryWithComma = getAmountInThousands(response.Data.accounts[i].costOfDelivery,2)
                            }

                            var selection = ""

                            var obj;

                            obj = {
                                isTesting:response.Data.accounts[i].isTesting,
                                isOwner:ownerEmailId == response.liuEmailId,
                                userId:name,
                                fullName:name,
                                designation:"",
                                fullDesignation:"",
                                name:(getTextLength(name,25)).capitalizeFirstLetter(),
                                emailId:name,
                                noPicFlag:true,
                                image:"https://logo.clearbit.com/"+ getTextLength(name,25) +".com",
                                noPPic:(name.substr(0,2)).capitalizeFirstLetter(),
                                cursor:'cursor:default',
                                url:null,
                                idName:'com_con_item_'+name,
                                hasAccess:hasAccess,
                                target:response.Data.accounts[i].target,
                                partner:response.Data.accounts[i].partner,
                                important:response.Data.accounts[i].important,
                                costOfSales:response.Data.accounts[i].costOfSales,
                                costOfDelivery:response.Data.accounts[i].costOfDelivery,
                                costOfSalesWithComma:costOfSalesWithComma,
                                costOfDeliveryWithComma:costOfDeliveryWithComma,
                                accessIcon:accessIcon,
                                usersWithAccess:_.uniq(usersWithAccess),
                                hierarchyAccess:response.Data.accounts[i].heiarchyWithAccess,
                                reportingHierarchyAccess:response.Data.accounts[i].reportingHierarchyAccess,
                                selection:selection,
                                ownerId:ownerId,
                                ownerEmailId:ownerEmailId,
                                accountAccessRequested:response.Data.accounts[i].accountAccessRequested,
                                contacts:response.Data.accounts[i].contacts,
                                lastInteractedDate:response.Data.accounts[i].lastInteractedDate
                            };

                            $scope.contactsList.push(obj);
                            $http.get("/companylogos/" + getTextLength(name,25) +".com")
                        }
                        
                        if(options == "Recent"){
                            $scope.contactsList.sort(function (o1, o2) {
                                return new Date(o1.lastInteractedDate) < new Date(o2.lastInteractedDate) ? 1 : new Date(o1.lastInteractedDate) > new Date(o2.lastInteractedDate) ? -1 : 0;
                            });
                        }
                    }
                    else {
                        $scope.isAllConnectionsLoaded = true;
                    }
                }
                else{
                    $scope.grandTotal = 0;
                    $scope.isAllConnectionsLoaded = true;
                }

                if(callback){

                    if($scope.contactsList[0]){
                        $scope.contactsList[0]["selection"] = "contact-selected";
                        callback($scope.contactsList[0])
                    } else {
                        callback(null)
                    }
                }
            })
    };

    $scope.validNum = function(num){
        return num < 10 ? '0'+num : num;
    }

    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    function checkLiuLoaded(){

        if($rootScope.currentUser){
            $scope.getContacts($scope.filterSelection,'/company/middle/bar/accounts',0,25,$scope.filterBy,null,null,function (account) {
                if(account){
                    $scope.accountSelected(account)
                } else {
                    $scope.setFilter("All")
                }
            });
        } else {
            setTimeOutCallback(1000,function () {
                checkLiuLoaded();
            })
        }
    }

    checkLiuLoaded();

    $scope.loadMoreContacts = function(skip,limit,searchContent,isBack){
        
        if(!$scope.searchContent){
            searchContent = null;
        }

        if(isBack){
            $scope.currentLength = $scope.currentLength-25;
            skip = skip-25
        }
        if(skip < 25){
            skip = 0
            $scope.currentLength = 0;
        }

        $scope.getContacts($scope.filterSelection,'/company/middle/bar/accounts',skip,limit,$scope.filterBy,searchContent,isBack,function (account) {
            $scope.accountSelected(account)
        });

    };

    $scope.calculateFromTo = function(current){
        return current < 0 ? 0 : current
    };

    $scope.accountSelected = function(account){

        $("html, body").animate({ scrollTop: 15 }, "slow");

        share.closeAllInteractions();

        removeSelectionForOthers($scope,account)
        
        if(account){
            checkTeamLoaded();
        }

        function checkTeamLoaded(){
            if(share.teamDictionary && share.teamArray){
                var userIds = _.map(share.teamArray,"userId");
                share.getAccessControl(account,userIds)
            } else {
                setTimeOutCallback(1000,function () {
                    checkTeamLoaded()
                })
            }
        }
    };

});

relatasApp.controller("account_snapshot", function ($scope, $http, $rootScope, share) {

    var accountSnapshotLoaded = false;

    share.accountSnapshot = function(account,userIds){

        account.target = account.target?account.target:0;
        
        var urlDealsRisk = '/account/deals/at/risk?account='+account.fullName
        url = fetchUrlWithParameter(urlDealsRisk, "contacts", account.contacts);

        getDealsAtRisk($scope,$http,fetchUrlWithParameter(urlDealsRisk,"userIdList",userIds))

        var url = "/account/by/month/year?account="+account.fullName;
        url = fetchUrlWithParameter(url, "userIdList", userIds);
        url = fetchUrlWithParameter(url, "contacts", account.contacts);

        function getTargets(){
            if(accountSnapshotLoaded){
                drawTargets($scope, $http,url,share,account)
            } else {
                setTimeOutCallback(1000,function () {
                    getTargets();
                })
            }
        }

        getTargets();
    }

    share.accountOppSnapshot = function(opportunities,totalAccOpps){

        $scope.achievement = 0;
        $scope.pipeline = 0;
        $scope.totalDeals = "-";
        $scope.fyWon = "-";
        $scope.achievementWithCommas = 0;
        $scope.pipelineWithCommas = 0;

        accountSnapshotLoaded = true;

        if(opportunities && opportunities.length>0){
            $scope.totalDeals = totalAccOpps;
            _.each(opportunities,function (op) {

                if(_.includes(['Close Won','Closed Won'],op.relatasStage)){

                    if(op.netGrossMargin && $rootScope.netGrossMargin){
                        $scope.achievement = $scope.achievement+((op.amount*op.netGrossMargin)/100);
                    } else {
                        $scope.achievement = $scope.achievement+op.amount;
                    }

                } else {

                    if(op.netGrossMargin && $rootScope.netGrossMargin){
                        $scope.pipeline = $scope.pipeline+((op.amount*op.netGrossMargin)/100);
                    } else {
                        $scope.pipeline = $scope.pipeline+op.amount;
                    }
                }
            })
        } else {
            $scope.achievement = "-";
            $scope.pipeline = "-";
        }

        if($scope.achievement != "-"){
            $scope.achievementWithCommas = getAmountInThousands($scope.achievement,2)
        }

        if($scope.pipeline != "-"){
            $scope.pipelineWithCommas = getAmountInThousands($scope.pipeline,2)
        }
    }

});

relatasApp.controller("account_costs", function ($scope, $http, $rootScope, share) {

    share.accountCosts = function(account){
        $scope.account = account;
    }

    share.updateAccountCosts = function (costOfSales,costOfDelivery){
        $scope.account.costOfSales= costOfSales
        $scope.account.costOfDelivery = costOfDelivery

        $scope.account.costOfSalesWithComma = numberWithCommas(costOfSales)
        $scope.account.costOfDeliveryWithComma = numberWithCommas(costOfDelivery)

    }

    share.fetchInteractionsForContacts = function(OppsWon){

        var numOfOpps = OppsWon.length;
        var totalInteractions = 0;
        $scope.averageInteractionsPerDealWon = "-"

        if(OppsWon && OppsWon.length>0){
            _.each(OppsWon,function (op) {
                totalInteractions = totalInteractions+parseInt(op.interactionCount)
                if(op.partners && op.partners.length>0){
                    totalInteractions = totalInteractions+getContactsInteractionCountInOpp(op.partners)
                }
                if(op.influencers && op.influencers.length>0){
                    totalInteractions = totalInteractions+getContactsInteractionCountInOpp(op.influencers)
                }
                if(op.decisionMakers && op.decisionMakers.length>0){
                    totalInteractions = totalInteractions+getContactsInteractionCountInOpp(op.decisionMakers)
                }
            })

            if(totalInteractions){
                $scope.averageInteractionsPerDealWon = (totalInteractions/numOfOpps).r_formatNumber(2)
            } else {
                $scope.averageInteractionsPerDealWon = "-"
            }
        } else {
            $scope.averageInteractionsPerDealWon = "-"
        }
    }
});

relatasApp.controller("account_insights", function ($scope, $http, $rootScope, share) {

    share.accountInsights = function(account,userIds) {
        $scope.account = account;
        
        var url = "/account/insights?account="+account.fullName;

        if(userIds){
            url = fetchUrlWithParameter(url, "userIdList", userIds)
        }

        url = fetchUrlWithParameter(url, "contacts", account.contacts);

        $http.get(url)
            .success(function (response) {
                if (response.SuccessCode) {
                    $scope.oppsPastCloseDate = response.Data.opps?response.Data.opps.length:0;
                    $scope.lastInteractionSentiment = response.Data.interaction && response.Data.interaction[0]?response.Data.interaction[0]._id:0;

                    if($scope.oppsPastCloseDate !=0 && $scope.lastInteractionSentiment !=0){
                        $scope.noInsightsFound = false;
                    } else {
                        $scope.noInsightsFound = true;
                    }
                } else {
                    $scope.noInsightsFound = true;
                }
            });
    }

});

relatasApp.controller("interactions_team_verbose", function ($scope, $http, $rootScope, share) {

    $scope.closeModal = function () {
        $scope.isOppModalOpen = false;
        $scope.showInteractionInitiations = false;
    }

    $scope.teamBlock = false;
    $scope.viewSelection = function (teamBlock) {
        if(teamBlock){
            $scope.teamBlock = true;
            $scope.active.team = "button-selected"
            $scope.active.company = "button"
        } else {
            $scope.teamBlock = false;
            $scope.active.team = "button"
            $scope.active.company = "button-selected"
        }
    }

    $scope.interactionInitiations = function () {
        $scope.teamBlock = false;
        $scope.active = {
            team:"button",
            company:"button-selected"
        }

        $scope.showInteractionInitiations = true;

        var accountInteractions = _.map($scope.accountData,"count")
        var teamInteractions = _.map($scope.teamMembersData,"count")

        var lastInteractedDictionary = {};
        var lastInteractedDictionaryCompany = {};

        _.each($scope.lastInteractedData,function (el) {
            lastInteractedDictionary[el._id] = el
        });

        _.each($scope.lastInteractedData,function (el) {
            lastInteractedDictionaryCompany[el.emailId] = el
        });

        var max_acc = _.max(accountInteractions),
            max_team = _.max(teamInteractions)

        $scope.teamGraphs = [];
        $scope.accGraphs = [];

        if($scope.accountData && $scope.accountData.length>0){
            _.each($scope.accountData,function (el) {

                var team = "";

                if(lastInteractedDictionaryCompany[el._id] && lastInteractedDictionaryCompany[el._id]._id){
                    team = lastInteractedDictionaryCompany[el._id]._id
                }

                $scope.accGraphs.push({
                    style:max_acc?{'width':scaleBetween(el.count,1,max_acc)+'%',background: 'rgb(255, 182, 30)'}:'',
                    emailId:el._id,
                    emailIdForImg:el._id.substr(0,2).toUpperCase(),
                    count:el.count,
                    team:team,
                    lastInteractedSort:el.lastInteractedDate?new Date(el.lastInteractedDate):"-",
                    lastInteracted:el.lastInteractedDate?moment(el.lastInteractedDate).format(standardDateFormat()):"-"
                });
            });

            $scope.accGraphs.sort(function (o1, o2) {
                return o2.count < o1.count ? -1 : o2.count > o1.count ? 1 : 0;
            });
        }

        if(max_team && $scope.teamMembersData && $scope.teamMembersData.length>0){
            _.each($scope.teamMembersData,function (el) {

                var lastInteractedDate = "";

                if(lastInteractedDictionary[el._id] && lastInteractedDictionary[el._id].lastInteractedDate){
                    lastInteractedDate = moment(lastInteractedDictionary[el._id].lastInteractedDate).format(standardDateFormat())
                }

                $scope.teamGraphs.push({
                    style:{'width':scaleBetween(el.count,1,max_team)+'%',background: 'rgb(15, 203, 227)'},
                    owner:share.teamDictionary[el._id],
                    count:el.count,
                    lastInteractedSort:new Date(el.lastInteractedDate),
                    lastInteracted:moment(el.lastInteractedDate).format(standardDateFormat())
                })
            })

            $scope.teamGraphs.sort(function (o1, o2) {
                return o2.count < o1.count ? -1 : o2.count > o1.count ? 1 : 0;
            });
        }
    }

    $scope.goToContact = function(emailId){
        window.location = '/contacts/all?contact='+emailId+'&acc=true'
    };

    share.getAccountAndTeamInteractions = function(account,userIds) {
        var url = "/account/interactions/verbose";
        var url2 = "/account/relationship/relevance";

        if (userIds) {
            url = fetchUrlWithParameter(url, "userIdList", userIds)
            url2 = fetchUrlWithParameter(url2, "userIdList", userIds)
        }

        url = fetchUrlWithParameter(url, "contacts", account.contacts);
        url2 = fetchUrlWithParameter(url2, "contacts", account.contacts);

        $http.get(url2)
            .success(function (response) {
                var data = response.Data;

                $scope.dataExists = 0;

                if(response.Data.decisionMakers || response.Data.influencers || response.Data.others || response.Data.partners){
                    $scope.dataExists = 1;
                }
                data["pieChartId"] = ".rr-chart"
                wholePie(data)
            });

        $http.get(url)
            .success(function (response) {
                if (response.SuccessCode) {

                    $scope.accountData = response.Data.account
                    $scope.teamMembersData = response.Data.teamMembers
                    $scope.lastInteractedData = response.Data.lastInteractedByTeam

                    $scope.totalAccInteractionsInit = _.sumBy(response.Data.account, 'count');

                    if($scope.totalAccInteractionsInit == 0){
                        $scope.totalAccInteractionsInit = ""
                    }

                    $scope.totalTeamInteractionsInit = _.sumBy(response.Data.teamMembers, 'count');

                    summaryChart(response.Data.account,".account-chart",shadeGenerator(255,152,0,response.Data.account.length,15))
                    summaryChart(response.Data.teamMembers,".team-chart",shadeGenerator(0,188,212,response.Data.teamMembers.length,15),share)
                }
            });
    }

});

function getDealsAtRisk($scope,$http,url){

    $http.get(url)
        .success(function (response) {
            $scope.dealsAtRisk = response.dealsAtRisk?response.dealsAtRisk:"-"
        })
}

function drawTargets($scope,$http,url,share,account,callback){

    $http.get(url)
        .success(function (response) {

            var total = _.map(response.Data,"total")
            var won = _.map(response.Data,"won")
            var lost = _.map(response.Data,"lost")
            var target = account.target[0]?account.target[0].amount:0;

            var pipeline = 0;

            var t = target;
            var w = _.sum(won);

            var valArr = [];
            valArr.push(t,w,pipeline)

            var vmax = Math.max.apply( null, valArr );

            $scope.fyWonStyle = {'width':scaleBetween(w,1,vmax)+'%',background: '#8ECECB'}

            if(!w || w ==0 ){
                $scope.fyWonStyle = {'width':0+'%',background: '#8ECECB'}
            }
            
            if(t){
                $scope.wonPercentage = ((w/t)*100).r_formatNumber(2)+'%'
            } else {
                $scope.wonPercentage = "-"
            }

            if(w>t){
                $scope.won = {'width':100+'%',background: '#8ECECB'}
                $scope.wonPercentage = getAmountInThousands(w,2);
            }

            $scope.targetValue = t;

        })
}

relatasApp.controller("interactionInitiations", function ($scope, $http, $rootScope, share) {

    share.interactionInitiations = function(account,userIds){

        var accountName = account.fullName;
        $rootScope.interaction_initiations_other_name = accountName;
        $rootScope.account_name_xs = accountName.slice(0,2).toUpperCase();

        var url = "/account/relationship?account="+accountName;

        if(userIds){
            url = fetchUrlWithParameter(url,"userIdList",userIds)
        }

        url = fetchUrlWithParameter(url, "contacts", account.contacts);

        $http.get(url)
            .success(function(response){

                $scope.dataFor = response.Data.dataFor;
                if(response.SuccessCode){

                    if(response.Data.relation && response.Data.relation.relationshipStrength_updated){
                        $scope.relationshipStrength_updated = response.Data.relation.relationshipStrength_updated;
                    }
                    else $scope.relationshipStrength_updated = 0;

                    if(response.Data.interactionInitiations && response.Data.interactionInitiations.length){
                        $scope.interaction_initiations_total = 0;

                        for(var i=0; i<response.Data.interactionInitiations.length; i++){
                            $scope.interaction_initiations_total += response.Data.interactionInitiations[i].count
                            if(response.Data.interactionInitiations[i]._id == 'you'){
                                $rootScope.interaction_initiations_you = response.Data.interactionInitiations[i].count
                            }
                            if(response.Data.interactionInitiations[i]._id == 'others'){
                                $rootScope.interaction_initiations_other = response.Data.interactionInitiations[i].count
                            }
                        }

                        var a = ($rootScope.interaction_initiations_you*100)/$scope.interaction_initiations_total
                        var b = ($rootScope.interaction_initiations_other*100)/$scope.interaction_initiations_total

                        a = Math.round(a)
                        b = Math.round(b)
                        // $scope.drawDonut(a,b)
                    }
                    else{
                        $scope.interaction_initiations_total = 0
                        $rootScope.interaction_initiations_you = 0
                        $rootScope.interaction_initiations_other = 0
                        $scope.relationshipStrength_updated = 0
                        // $scope.drawDonut(0,0)
                    }

                    if(response.Data.relationshipStrength && response.Data.relationshipStrength.length > 0){
                        var obj = response.Data.relationshipStrength[0];
                        var doc = response.Data.doc;

                        var x = obj.maxCount - obj.minCount;

                        var ratio = x/(doc.maxRatio-doc.minRatio);

                        var s = Math.round(doc.minRatio+((obj.maxCount - obj.minCount)/ratio))

                        $scope.relationshipStrength = s ? s : 0;

                    }
                    else $scope.relationshipStrength = 0;

                    if($scope.relationshipStrength_updated <= 0){
                        $scope.relationshipStrength_updated = $scope.relationshipStrength;
                    }
                    function updateRatio(value){
                        $scope.relationshipStrength_updated = value;
                        var reqObj = {contactId:response.Data.relation._id,relationshipStrength_updated:value};
                        $http.post('/contacts/update/relationship/strength/web',reqObj)
                            .success(function(response){
                                if(response.SuccessCode){
                                    $("#relationshipStrength_updated").text(value);
                                }
                                else toastr.error(response.Message);
                            });
                    }

                    if(response.Data.interactionTypes && response.Data.interactionTypes.length > 0){
                        $scope.interactionsByType = [];
                        var existingTypes = [];
                        var allTypes = ['meeting','call','sms','email','facebook','twitter','linkedin'];
                        for(var t=0; t<response.Data.interactionTypes[0].typeCounts.length; t++){
                            var interactionTypeObj = getInteractionTypeObj(response.Data.interactionTypes[0].typeCounts[t],response.Data.interactionTypes[0].maxCount);
                            if(interactionTypeObj != null){
                                existingTypes.push(interactionTypeObj.type);
                                $scope.interactionsByType.push(interactionTypeObj);
                            }
                        }
                        var mobileCountStatus = 0;
                        var mobileCountStatusLists = ['call','sms','email'];
                        for(var u=0; u<allTypes.length; u++){
                            if(existingTypes.indexOf(allTypes[u]) == -1){
                                var newObj = getInteractionTypeObj({_id:allTypes[u],count:0},response.Data.interactionTypes[0].maxCount)
                                if(newObj != null){
                                    if(mobileCountStatusLists.indexOf(allTypes[u]) != -1){
                                        mobileCountStatus ++;
                                    }
                                    $scope.interactionsByType.push(newObj);
                                }
                            }
                        }

                        if(mobileCountStatus > 0){
                            $scope.showDownloadMobileApp = true;
                        }else $scope.showDownloadMobileApp = false;
                        $scope.showSocialSetup = response.Data.showSocialSetup;

                        $scope.interactionsByType.sort(function (o1, o2) {
                            return o1.priority < o2.priority ? -1 : o1.priority > o2.priority ? 1 : 0;
                        });
                        $scope.interactionsByType = {
                            a:$scope.interactionsByType[0],
                            b:$scope.interactionsByType[1],
                            c:$scope.interactionsByType[2],
                            d:$scope.interactionsByType[3],
                            e:$scope.interactionsByType[4],
                            f:$scope.interactionsByType[5],
                            g:$scope.interactionsByType[6]
                        };

                    }
                    else{
                        $scope.interaction_initiations_total = 0;
                        $scope.interactionsByType = {"a":{"priority":0,"value":"height:0%","type":"meeting","count":0,"title":"0 interactions"},"b":{"priority":1,"value":"height:0%","type":"call","count":0,"title":"0 interactions"},"c":{"priority":2,"value":"height:0%","type":"sms","count":0,"title":"0 interactions"},"d":{"priority":3,"value":"height:0%","type":"email","count":0,"title":"0 interactions"},"e":{"priority":4,"value":"height:0%","type":"facebook","count":0,"title":"0 interactions"},"f":{"priority":5,"value":"height:0%","type":"twitter","count":0,"title":"0 interactions"},"g":{"priority":6,"value":"height:0%","type":"linkedin","count":0,"title":"0 interactions"}}
                    }
                }
            });
    };

    $scope.drawDonut = function(a,b){

        var canvas  = document.getElementById("donut-chart");
        var chart = canvas.getContext("2d");

        function drawdonutChart(canvas) {

            this.x , this.y , this.radius , this.lineWidth , this.strockStyle , this.from , this.to = null;
            this.set = function( x, y, radius, from, to, lineWidth, strockStyle)
            {
                this.x = x;
                this.y = y;
                this.radius = radius;
                this.from=from;
                this.to= to;
                this.lineWidth = lineWidth;
                this.strockStyle = strockStyle;
            }

            this.draw = function(data) {
                canvas.beginPath();
                canvas.lineWidth = this.lineWidth;
                canvas.strokeStyle = this.strockStyle;
                canvas.arc(this.x , this.y , this.radius , this.from , this.to);
                canvas.stroke();
                var numberOfParts = data.numberOfParts;
                var parts = data.parts.pt;
                var colors = data.colors.cs;
                var df = 0;
                for(var i = 0; i<numberOfParts; i++)
                {
                    canvas.beginPath();
                    canvas.strokeStyle = colors[i];
                    canvas.arc(this.x, this.y, this.radius, df, df + (Math.PI * 2) * (parts[i] / 100));
                    canvas.stroke();
                    df += (Math.PI * 2) * (parts[i] / 100);
                }
            }
        }
        var data =
        {
            numberOfParts: 2,
            parts:{"pt": [a , b ]},//percentage of each parts
            colors:{"cs": ["#f86b4f", "rgba(248, 107, 79, 0.65)"]}//color of each part
        };

        var drawDount = new drawdonutChart(chart);

        drawDount.set(100, 100, 45, 0, Math.PI*2, 8, "#fff");

        if(data.parts.pt[0] == 0 && data.parts.pt[1] == 0){
            data = {
                numberOfParts: 2,
                parts:{"pt": [99 , 0 ]},//percentage of each parts
                colors:{"cs": ["#bbb", "#bbb"]}//color of each part
            };
        }

        drawDount.draw(data);
    }

});

relatasApp.controller("opportunitiesByStage",function ($scope,$http,share) {

    share.forSnapshot = function (account,userIds) {
        getPipelineSnapshot(userIds,account)
    }

    function getPipelineSnapshot(userIds,account) {
        var url = "/account/group/count"

        if(userIds){
            url = fetchUrlWithParameter(url,"userIdList",userIds)
        }

        url = fetchUrlWithParameter(url, "contacts", account.contacts);

        $http.get(url)
            .success(function (response) {

                var oppExist = false;
                if(response && response.SuccessCode && response.Data.length>0){
                    if(response.fy){
                        $scope.fiscalYear = moment(response.fy.fromDate).format("MMMM YY")+" - "+moment(response.fy.toDate).format("MMM YY")
                    }

                    oppExist = true;

                    var maxCount = _.max(_.map(response.Data,"count"))
                    var minCount = _.min(_.map(response.Data,"count"))
                    var maxAmount = _.max(_.map(response.Data,"totalAmount"))
                    var minAmount = _.min(_.map(response.Data,"totalAmount"))

                    $scope.prospectColorLeft = "white";
                    $scope.EvaluationColorLeft = "white";
                    $scope.proposalColorLeft = "white";
                    $scope.wonColorLeft = "white";
                    $scope.lostColorLeft = "white";

                    $scope.prospectColor = "white";
                    $scope.EvaluationColor = "white";
                    $scope.proposalColor = "white";
                    $scope.wonColor = "white";
                    $scope.lostColor = "white";

                    $scope.stages = _.map(share.opportunityStages,"name");

                    $scope.funnels = [];
                    var stagesWithData = [];

                    function getPipelineFunnel(){
                        if($scope.stages){

                            _.each(response.Data,function (el) {

                                _.each($scope.stages,function (st) {

                                    if(el._id == st){
                                        stagesWithData.push(st);
                                        $scope.funnels.push({
                                            name:st,
                                            countLength:{'width':scaleBetween(el.count, minCount, maxCount)+'%'},
                                            amountLength:{'width':scaleBetween(el.totalAmount, minAmount, maxAmount)+'%'},
                                            amount:el.totalAmount,
                                            oppsCount:el.count
                                        })
                                    }
                                })
                            });

                            var noDataStages = _.difference($scope.stages,stagesWithData)

                            _.each(noDataStages,function (st) {
                                $scope.funnels.push({
                                    name:st,
                                    countLength:{'width':0+'%'},
                                    amountLength:{'width':0+'%'},
                                    amount:0,
                                    oppsCount:0
                                })
                            })

                            _.each($scope.funnels,function (fl) {
                                _.each(share.opportunityStages,function (st) {
                                    if(fl.name == st.name){
                                        fl.order = st.order
                                    }
                                })
                            });

                            $scope.funnels = _.sortBy($scope.funnels,function (o) {
                                return o.order
                            })

                        } else {
                            setTimeOutCallback(1000,function () {
                                getPipelineFunnel()
                            });
                        }
                    }

                    getPipelineFunnel()
                }

                $scope.noPipeline = oppExist;
            });
    }

});

relatasApp.controller("opp_growth",function ($scope,$http,share,$rootScope) {

    share.forOppGrowth = function (account,userIds,accessControl) {
        getOppGrowth(account,userIds,accessControl)
    }

    function getOppGrowth(account,userIds,accessControl) {
        setTimeOutCallback(500, function () {
            var url = "/account/conversion/rate"

            if(userIds){
                url = fetchUrlWithParameter(url,"userIdList",userIds);
            }

            url = fetchUrlWithParameter(url, "contacts", account.contacts);

            $http.get(url)
                .success(function (response) {
                    if (response && response.SuccessCode) {

                        $scope.noOpp = true;
                        var label = [], seriesOpen = [], seriesClose = [];

                        _.each(response.Data, function (el) {
                            if(el.count>0){
                                $scope.noOpp = false;
                            }
                            label.push(monthNames[moment(new Date(el.sortDate)).month()].substring(0,3))
                            seriesOpen.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
                        });

                        _.each(response.dataClosed, function (el) {
                            seriesClose.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
                        });

                        $scope.label2 = "Created",$scope.label1 = "Won";

                        drawLineChart($scope,share,seriesOpen,label,".opp-growth",seriesClose);

                    } else {
                        $scope.noOpp = true;
                    }
                })
        });
    }

    $scope.createOpp = function () {
        share.createOpp()
    }
    
});

function getContactsInteractionCountInOpp(data) {

    var count = 0;

    _.each(function (el) {
        count = count+el.interactionCount
    })

    return count;
}

function highlightFilterSelection($scope,options){

    _.each($scope.filters,function (el) {

        if(el.filter == options){
            el.class = "selected"
        } else {
            el.class = ""
        }
    });
}

function removeSelectionForOthers($scope,account){
    
    _.each($scope.contactsList,function (el) {

        if(el.fullName == account.fullName){
            el.selection = "contact-selected"
        } else {
            el.selection = ""
        }
    });
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];