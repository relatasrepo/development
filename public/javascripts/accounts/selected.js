var defaultNoOfDays = 180

reletasApp.controller("dropWatchController", function($rootScope, $window){
      var numberOfDays = {}
      setInterval(function(){
        if($window.noOfDays)
          Object.keys($window.noOfDays).forEach(function(key){
            if(numberOfDays[key] !== $window.noOfDays[key]){
              if(isNaN($window.noOfDays[key]))
                $window.noOfDays[key] = 0
              $rootScope.$broadcast(key + "DaysChanged", $window.noOfDays[key])
              numberOfDays[key] = $window.noOfDays[key]
            }
          })
      }, 1000)

})

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue){
  if(parameterValue instanceof Array)
    if(parameterValue.length > 0)
      parameterValue = parameterValue.join(",")
    else
      parameterValue = null

  if(parameterValue != undefined && parameterValue != null){
    if(baseUrl.indexOf("?") == -1)
     baseUrl += "?" 
    else
     baseUrl += "&" 
    baseUrl+= parameterName + "=" + parameterValue
  }

  return baseUrl 
}

reletasApp.controller("interactionsTypeController", function($scope, $http, $rootScope,$location, $window){
    function paintTypeInteracted(usersInHierarchy, days){
    var companyName = getCompanyNameFromUrl($(location).attr('href'))
    var url = fetchUrlWithParameter("/company/interacted/typeforcompany/", "company", companyName)
    url = fetchUrlWithParameter(url, "hierarchylist", usersInHierarchy)
    url = fetchUrlWithParameter(url, "days", days)
        if(usersInHierarchy){
            url = fetchUrlWithParameter(url, "onSelect", true)
        } else {
            url = fetchUrlWithParameter(url, "onSelect", false)
        }
      $http.get(url).success(function( response){
        if(response.SuccessCode)
          if(response.Data && response.Data.interactionsByType && response.Data.interactionsByType.length > 0 && response.Data.interactionsByType[0] && response.Data.interactionsByType[0].typeCounts && response.Data.interactionsByType[0].typeCounts.length > 0){
              $scope.totalInteractionsCount = response.Data.interactionsByType[0].totalCount || 0;
              var existingTypes = [];
              var allTypes = ['meeting','call','sms','email','facebook','twitter','linkedin'];
              $scope.interactionsByType = []
              for(var t=0; t<response.Data.interactionsByType[0].typeCounts.length; t++){
                  var interactionTypeObj = $scope.getInteractionTypeObj(response.Data.interactionsByType[0].typeCounts[t],response.Data.interactionsByType[0].maxCount,100);
                  if(interactionTypeObj != null){
                      existingTypes.push(interactionTypeObj.type);
                      $scope.interactionsByType.push(interactionTypeObj);
                  }
              }
              var mobileCountStatus = 0;
              var mobileCountStatusLists = ['call','sms','email'];
              for(var u=0; u<allTypes.length; u++){
                  if(existingTypes.indexOf(allTypes[u]) == -1){
                      var newObj = $scope.getInteractionTypeObj({_id:allTypes[u],count:0},response.Data.interactionsByType[0].maxCount,100)
                      if(newObj != null){
                          if(mobileCountStatusLists.indexOf(allTypes[u]) != -1){
                              mobileCountStatus ++;
                          }
                          $scope.interactionsByType.push(newObj);
                      }
                  }
              }

              if(mobileCountStatus > 0){
                  $scope.showDownloadMobileApp = true;
              }else $scope.showDownloadMobileApp = false;
              $scope.showSocialSetup = response.Data.showSocialSetup;

              $scope.interactionsByType.sort(function (o1, o2) {
                  return o1.priority < o2.priority ? -1 : o1.priority > o2.priority ? 1 : 0;
              });

              $scope.interactionsByType = {
                  a:$scope.interactionsByType[0],
                  b:$scope.interactionsByType[1],
                  c:$scope.interactionsByType[2],
                  d:$scope.interactionsByType[3],
                  e:$scope.interactionsByType[4],
                  f:$scope.interactionsByType[5],
                  g:$scope.interactionsByType[6]
              };
          }else{
            $scope.totalInteractionsCount = 0
            $scope.interactionsByType = []
          }


      })
    }

    $scope.getInteractionTypeObj = function(obj,total,percentageFor){
        switch (obj._id){
            case 'google-meeting':
            case 'meeting':return {
                priority:0,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'call':return {
                priority:1,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'sms':return {
                priority:2,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'email':return {
                priority:3,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'facebook':return {
                priority:4,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'twitter':return {
                priority:5,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'linkedin':return {
                priority:6,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            default : return null;
        }
    };
    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };
    $scope.calculatePercentage = function(count,total,percentageFor){
        return Math.round((count*percentageFor)/total);
    };

  var dropType = "monthlyInteraction"
  $rootScope.$on("hierarchyModified", function(event, data){
    paintTypeInteracted(data,$window.noOfDays[dropType] || defaultNoOfDays)
  })

  $rootScope.$on("monthlyInteractionDaysChanged", function(event, data){
    paintTypeInteracted(($rootScope.usersInHierarchy || null), data)
  })

  //$rootScope.$on("hierarchyModified", function(event, data){
  //  paintTypeInteracted(data)
  //})

  paintTypeInteracted(null, defaultNoOfDays)
})

function getCompanyNameFromUrl(href){
    var parser = document.createElement('a')
    parser.href = href
    var queries = parser.search.replace(/^\?/, '').split('&')
    var searchObject = {}
    for( i = 0; i < queries.length; i++ ) {
        split = queries[i].split('=');
        searchObject[split[0]] = split[1];
    }
    return searchObject.account
}

reletasApp.controller("logoController", function($scope, $http, $location, $rootScope){
    $scope.displayLogo = true
    $scope.companyName = getCompanyNameFromUrl($(location).attr('href'))
    $scope.companyLogo = "https://logo.clearbit.com/"+$scope.companyName+".com"
    $scope.companyNoName = ($scope.companyName.substr(0,2)).capitalizeFirstLetter();

    // if(imageExists($scope.companyLogo)){
    //     $scope.noProfilePic = false;
    // } else {
    //     $scope.noProfilePic = true;
    //     $scope.companyNoName = ($scope.companyName.substr(0,2)).capitalizeFirstLetter()
    // }

})

function imageExists(url){
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
    return http.status != 404;
}

reletasApp.controller("hierarchyController", function($scope, $http, $location, $rootScope){
    $scope.companyName = getCompanyNameFromUrl($(location).attr('href'))
    $(document).ready(function(){
      function convertArrayToTree(data){
        var root = data.find(function(item) {
          return item.hierarchyParent === null;
        });

        var tree = {
          _id: root._id,
          firstName: root.firstName,
          lastName: root.lastName,
          designation: root.designation,
          emailId: root.emailId
        };

        var parents = [tree];
        while (parents.length > 0) {
          var newParents = [];
          parents.forEach(function(parent) {
            var childrenList = data.filter(function(item) {
              return item.hierarchyParent == parent._id
            }).forEach(function(child) {
              var c = { _id: child._id, firstName: child.firstName, lastName: child.lastName, designation: child.designation, emailId: child.emailId};
              parent.children = parent.children || [];
              parent.children.push(c);
              newParents.push(c);
            });
          });
          parents = newParents;
        }
        return tree
      }

      $.getJSON("/company/user/hierarchy", function(data){

          var data2 = {};
          var data2 = convertArrayToTree(data.Data)
          var data3 = {};
          data3 = {children:[data2]};
          function addItem(parentUL, branch) {

              for (var key in branch.children) {

                  //Last modfified 2 March 2016 by Naveen
                  //Check for all the people who have been selected in the session and then check the respective checkbox
                  var chk = arrayContains(branch.children[key]._id,data.sessionHierarchy)
                  var checkedId = 'reject';
                  if(chk){
                      checkedId = chk
                  }

                  var item = branch.children[key];
                  var $item = $('<li>', {
                      id: "item" + item._id
                  });

                  if(checkedId != 'reject'){
                      $item.append($('<input>', {
                          type: "checkbox",
                          id: "item" + item._id,
                          name: "item" + item._id,
                          checked: 'checked="checked"'
                      }));
                  } else {
                      $item.append($('<input>', {
                          type: "checkbox",
                          id: "item" + item._id,
                          name: "item" + item._id
                      }));
                  }
                  $item.append($('<label>', {
                      for: "item" + item._id,
                      text: item.firstName +' '+item.lastName
                  }));
                  $item.append($('<p>', {
                      text: item.designation
                  }));
                  parentUL.append($item);
                  if (item.children) {
                      var $ul = $('<ul>', {
//                          style: 'display: none' This shows hierarchy open by default
                      }).appendTo($item);
                      $item.append();
                      addItem($ul, item);
                  }
              }
          }

        $("#updateHierarchy").click(function(e){
            var selected = $("#root input:checked").map(function(i,el){return el.name.replace("item","");}).get();
            $rootScope.usersInHierarchy = selected
            $rootScope.$broadcast("hierarchyModified", selected)
        });

          $(function () {
              addItem($('#root'), data3);

              //Select all children if parent selected
              //
              //$("input[type='checkbox']").change(function () {
              //    $(this).siblings('ul')
              //        .find("input[type='checkbox']")
              //        .prop('checked', this.checked);
              //});

              $('#checkAllHierarchy').click( function () {
                  var checkBoxes = $("input");
                  checkBoxes.prop("checked", !checkBoxes.prop("checked"));
              })

              //Check only the parent. Last modified 02 March 2016 by Naveen. Earlier we select the parent under the hierarchy
              //$('#root>li>input:first-child').prop('checked', true);

              $('label').click(function(){
                  $(this).closest('li').children('ul').slideToggle();

              });
          });
      })
    });

})

reletasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/customer/middle/bar"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

       //searchContent = searchContent.replace(/[^\w\s]/gi, '');
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            if(searchContent.length >= 3) {
                yourNetwork = yourNetwork ? true : false;
                extendedNetwork = extendedNetwork ? true : false;
                forCompanies = forCompanies ? true : false;
                window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
            }
        }
        else toastr.error("Please enter search content")
    };
});
var styles = [
    {
        height: 53,
        url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
        width: 53
    }
]
var timezone;
reletasApp.controller("logedinUser", function ($scope, $http,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {

            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }

                if(response.Data.contactsUpload && response.Data.contactsUpload.isUploaded){

                }else $("#uploadContactsAlert").removeClass("hide");
            }
            else{

            }
        }).error(function (data) {

    })
});
//reletasApp.controller("interactionPercentageCtrl", function($scope, $location, $http){
  //var companyName = getCompanyNameFromUrl($(location).attr('href'))
  //$http.get("/company/select/interactions/percentage?company="+ companyName).then(function(res){
   //var responseData = res.data.Data
   //$scope.companyTotalInteraction = responseData.countCompanyPresent
   //$scope.companyInteractionPercentage = Math.round((responseData.countCompanyPresent/responseData.countCompanyPast) * 100)
   //$scope.selfTotalInteraction = responseData.countPresent
   //$scope.selfTotalInteractionPercentage = Math.round((responseData.countPresent/responseData.countPast)* 100)
  //})
//})
reletasApp.controller("networkCountController", function($rootScope, $scope, $location, $http){

  function paintNetworkCount(usersInHierarchy){
    var companyName = getCompanyNameFromUrl($(location).attr('href'))
    var url = fetchUrlWithParameter("/company/network/count?company=" + companyName, "hierarchylist", usersInHierarchy)
    $http.get(url).then(function(res){
      var responseData = res.data.Data
      $scope.myNetworkCount = responseData.myNetwork || 0
      $scope.extendedNetworkCount = responseData.extendedNetwork || 0
    })
  }

  paintNetworkCount()
  $rootScope.$on("hierarchyModified", function(event, data){
    paintNetworkCount(data)
  })
})

reletasApp.controller("monthlyInteractionController", function($rootScope, $scope, $location, $http, $window){

  function paintMonthlyInteraction(usersInHierarchy, days){
    var companyName = getCompanyNameFromUrl($(location).attr('href'))
    var url = fetchUrlWithParameter("/company/select/interactions/count/month?company=" + companyName, "hierarchylist", usersInHierarchy)
     url = fetchUrlWithParameter(url, "days", days)

      if(usersInHierarchy){
          url = fetchUrlWithParameter(url, "onSelect", true)
      } else {
          url = fetchUrlWithParameter(url, "onSelect", false)
      }

      $scope.noInteractions = false;

    $http.get(url).then(function(res){

        //if(res.data.Data.interactedByTeam.interactions.length<1){
        //    $scope.noInteractions = true
        //}

      var responseData = res.data.Data
            var months = []
            var interactions = []
            var selectedHierarchyInteractions = []
            var selectedMonth = 0
            if(responseData){
              months = responseData.interactedByTeam.months.slice()
              interactions = responseData.interactedByTeam.interactions.slice()
              selectedHierarchyInteractions = responseData.interactedBySelf.interactions.slice()
              selectedMonth = responseData.interactedByTeam.interactions.length - 1
              setTimeout(function(){

                  $scope.selectedMonth = '( '+responseData.interactedByTeam.months[selectedMonth] + ' )'

                $scope.companyTotalInteraction = responseData.interactedByTeam.interactions[selectedMonth]
                $scope.selfTotalInteraction = responseData.interactedBySelf.interactions[selectedMonth]
                if(responseData.interactedByTeam.interactions[selectedMonth - 1] != 0)
                  $scope.companyInteractionPercentage = Math.round((($scope.companyTotalInteraction - responseData.interactedByTeam.interactions[selectedMonth - 1])/responseData.interactedByTeam.interactions[selectedMonth - 1]) * 100)
                else
                  $scope.companyInteractionPercentage = 0
                if(responseData.interactedBySelf.interactions[selectedMonth - 1] != 0)
                  $scope.selfTotalInteractionPercentage = Math.round((($scope.selfTotalInteraction - responseData.interactedBySelf.interactions[selectedMonth - 1])/responseData.interactedBySelf.interactions[selectedMonth - 1]) * 100)
                else
                  $scope.selfTotalInteractionPercentage = 0
                if($scope.companyInteractionPercentage != "NA" && isNaN($scope.companyInteractionPercentage))
                  $scope.companyInteractionPercentage = 0
                if($scope.selfTotalInteractionPercentage != "NA" && isNaN($scope.selfTotalInteractionPercentage))
                  $scope.selfTotalInteractionPercentage = 0
                $scope.$apply()
              },1)
            }
            months.unshift('x')
            interactions.unshift('Company Wide')
            selectedHierarchyInteractions.unshift('Selected Hierarchy')

        setTimeout(function(){
              var chart = c3.generate({
                  data: {
                      x : 'x',
                      columns: [
                          months,
                          interactions,
                          selectedHierarchyInteractions
                      ],
                      types: {
                        'Company Wide': 'area-spline',
                        'Selected Hierarchy':'area-spline'
                      },
                      colors: {
                        'Company Wide': '#49B5A6',
                        'Selected Hierarchy': '#f86b4f'
                      },
                      onmouseover: function(d) {

                          var selectedMonth = d.x
                          $scope.selectedMonth = '( '+responseData.interactedByTeam.months[selectedMonth] + ' )'
                          $scope.companyTotalInteraction = responseData.interactedByTeam.interactions[selectedMonth]
                          $scope.selfTotalInteraction = responseData.interactedBySelf.interactions[selectedMonth]
                          if(selectedMonth == 0){
                            $scope.companyInteractionPercentage = 0
                            $scope.selfTotalInteractionPercentage = 0;
                            //selectedMonth = responseData.interactedByTeam.months.length - 1
                          }else{
                            if(responseData.interactedByTeam.interactions[selectedMonth - 1] != 0)
                              $scope.companyInteractionPercentage = Math.round((($scope.companyTotalInteraction - responseData.interactedByTeam.interactions[selectedMonth - 1])/responseData.interactedByTeam.interactions[selectedMonth - 1]) * 100)
                            else
                              $scope.companyInteractionPercentage = 0
                            if(responseData.interactedBySelf.interactions[selectedMonth - 1] != 0)
                              $scope.selfTotalInteractionPercentage = Math.round((($scope.selfTotalInteraction - responseData.interactedBySelf.interactions[selectedMonth - 1])/responseData.interactedBySelf.interactions[selectedMonth - 1]) * 100)
                            else
                              $scope.selfTotalInteractionPercentage = 0
                            if($scope.companyInteractionPercentage != "NA" && isNaN($scope.companyInteractionPercentage))
                              $scope.companyInteractionPercentage = 0
                            if($scope.selfTotalInteractionPercentage != "NA" && isNaN($scope.selfTotalInteractionPercentage))
                              $scope.selfTotalInteractionPercentage = 0
                          }
                          $scope.$apply()
                      }
                  },
                  axis: {
                      y: {
                          padding: {bottom: 0},
                          min: 0
                      },
                      x: {
                          padding: {left: 0},
                          min: 0,
                          show: true,
                          type: 'category' /*Needed to load x*/
                      }
                  },
                  color: {
                      pattern: ['#49B5A6']
                  }

              });
            })
    })
  }

  paintMonthlyInteraction(null, defaultNoOfDays)
  var dropType = "monthlyInteraction"
  $rootScope.$on("hierarchyModified", function(event, data){
    paintMonthlyInteraction(data,$window.noOfDays[dropType] || defaultNoOfDays)
  })

  $rootScope.$on("monthlyInteractionDaysChanged", function(event, data){
    paintMonthlyInteraction(($rootScope.usersInHierarchy || null), data)
  })
})

reletasApp.controller("left_contacts_bar", function ($scope, $location, $http, $rootScope, share) {
    $scope.contactsList = [];
    $scope.currentLength = 0;
    $scope.lastFetched = 0;
    $scope.filterBy = 'all';
    $scope.show_contacts_back = false;
    $scope.show_filter = false;
    var isFilter = false;
    var parser = document.createElement('a')
    parser.href = $location.absUrl()
    queries = parser.search.replace(/^\?/, '').split('&')
    searchObject = {}
    for( i = 0; i < queries.length; i++ ) {
        split = queries[i].split('=');
        searchObject[split[0]] = split[1];
    }
    $scope.searchContacts_filter = function(searchContent,checkedBoxes){

        var checked = [];
        for(var type in checkedBoxes){
            if(checkedBoxes[type]){
                checked.push(type);
            }
        }
        if(checked.length > 0){
            $scope.filterBy = checked.join(',')
            isFilter = true;
            $scope.contactsList = [];
            $scope.currentLength = 0;
            //$scope.getContacts('/company/middle/bar/accounts/'+ searchObject.account,0,25,$scope.filterBy,searchContent);
            var url = fetchUrlWithParameter('/company/middle/bar/accounts/'+ searchObject.account, "hierarchylist", $rootScope.usersInHierarchy || null)
            $scope.getContacts(url,0,25,$scope.filterBy,searchContent);

        }
        else{
            isFilter = false;
            $scope.refreshContactsList()
        }
    };

    $scope.searchContacts = function(searchContent){
        if(checkRequired(searchContent)){
            $scope.filterBy = isFilter ? $scope.filterBy : 'search';
            $scope.contactsList = [];
            $scope.currentLength = 0;
            var url = fetchUrlWithParameter('/company/middle/bar/accounts/'+ searchObject.account, "hierarchylist", $rootScope.usersInHierarchy || null)
            $scope.getContacts(url,0,25,$scope.filterBy,searchContent);
        }
    };

    $scope.refreshContactsList = function(){
        $scope.filterBy = isFilter ? $scope.filterBy : 'all';
        $scope.searchContent = "";
        $(".searchContentClass").val("");
        $scope.contactsList = [];
        $scope.currentLength = 0;
        var url = fetchUrlWithParameter('/company/middle/bar/accounts/'+ searchObject.account, "hierarchylist", $rootScope.usersInHierarchy || null)
        $scope.getContacts(url,0,25,$scope.filterBy,'');
    };

    $scope.getContacts = function(url,skip,limit,filterBy,searchContent,isBack){
        if(url.indexOf("?") == -1)
          url = url + "?"
        else
          url = url + "&"
        if(checkRequired(searchContent)){
            url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy+'&searchContent='+searchContent
        }
        else url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy;

        $http.get(url)
            .success(function(response){
                $scope.contactsList = [];
                $scope.isConnectionOpen = false;
                if(response.SuccessCode){

                    $scope.grandTotal = response.Data.total;
                    if(response.Data.contacts.length > 0){

                        if(!isBack || $scope.currentLength <= 0){
                            $scope.currentLength += response.Data.returned;
                            $scope.returned = response.Data.returned
                        }

                        if($scope.currentLength > 25){
                            $scope.show_contacts_back = true;
                        }else $scope.show_contacts_back = false;
                        // $scope.contactsList = response.Data.contacts;
                        for(var i=0; i<response.Data.contacts.length; i++){

                            var li = '';
                            var obj;
                            obj = {
                                userId:response.Data.contacts[i]._id,
                                fullName:response.Data.contacts[i].name,
                                companyName:response.Data.contacts[i].companyName,
                                designation:response.Data.contacts[i].designation,
                                //fullCompanyName:$scope.validNum(response.Data.contacts[i].contactsCount)+" Contacts",
                                //fullDesignation:"",
                                name:getTextLength(response.Data.contacts[i].name,25),
                                emailId:response.Data.contacts[i]._id,
                                image:response.Data.contacts[i].profilePic || (response.Data.contacts[i]._id.substr(0,2)).capitalizeFirstLetter(),
                                noPicFlag:true,
                                noPPic:(response.Data.contacts[i]._id.substr(0,2)).capitalizeFirstLetter(),
                                cursor:'cursor:default',
                                url:null,
                                idName:'com_con_item_'+response.Data.contacts[i]._id,
                                interactionType:response.Data.contacts[i].interactionType?getInteractionTypeImage(response.Data.contacts[i].interactionType):'fa-clock-o',
                                lastInteractedDate:response.Data.contacts[i].lastInteractedDate?moment(response.Data.contacts[i].lastInteractedDate).format("DD MMM YYYY"):'90+ days'
                            };
                            $scope.contactsList.push(obj);
                        }

                        $scope.contactsList.sort(function(a, b){

                            var nameA=a.fullName.toLowerCase(), nameB=b.fullName.toLowerCase();
                            if (nameA < nameB) //sort string ascending
                                return -1;
                            if (nameA > nameB)
                                return 1;
                            return 0; //default return value (no sorting)
                        });
                    }
                    else {
                        $scope.isAllConnectionsLoaded = true;
                        //$scope.commonConnectionsNotExist = response.Message;
                    }
                }
                else{
                    $scope.isAllConnectionsLoaded = true;
                    //$scope.commonConnectionsNotExist = response.Message;
                }
            })
    };

    $scope.validNum = function(num){
        return num < 10 ? '0'+num : num;
    }

    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    $scope.getContacts('/company/middle/bar/accounts/'+ searchObject.account,0,25,$scope.filterBy);

    $scope.loadMoreContacts = function(skip,limit,searchContent,isBack){
        if(isBack){
            $scope.currentLength = $scope.currentLength-25;
            skip = skip-25
        }
        if(skip < 25){
            skip = 0
            $scope.currentLength = 0;
        }
        $scope.getContacts('/company/middle/bar/accounts/'+ searchObject.account,skip,limit,$scope.filterBy,searchContent,isBack);
    };

    $scope.calculateFromTo = function(current){
        return current < 0 ? 0 : current
    };

    $scope.showInteractions = function(emailId,companyName,designation,name,url,userId,disableContext){

        isGetContactInfo(function(){
            share.getContactInfo(emailId,companyName,designation,name,url,userId);
        });
    };

    $scope.accountSelected = function(account){
        //window.location = '/customer/select?account='+account.userId
        //Previously it was customer select. Now we are redirecting the user to contact selected page. UserId here is an email ID

        window.location = '/contacts/all?contact='+account.userId+'&acc=true'
    };

    function isGetContactInfo(callback){
        if(typeof share.getContactInfo == 'function'){
            callback();
        }
        else{
            setTimeout(function(){
                return isGetContactInfo(callback);
            },100)
        }
    }
    $rootScope.$on("hierarchyModified", function(event, data){
      $scope.usersInHierarchy = data
      $scope.refreshContactsList()
    })
});

reletasApp.controller("popup_content_cntlr",function($scope, $http, share){
    $scope.intItems = [];
    $scope.close_interaction_popup = function(){
        $("#map_cluster_click_popup").hide();
    };
    share.updatePopup = function(peopleMet,totalInteractions,interactionItems){
        $("#peopleMet").text(peopleMet);
        $("#totalInteractions").text(totalInteractions);
        $("#interactionItems").html(interactionItems);
        $("#map_cluster_click_popup").show();
    }
});

//reletasApp.controller("global_map_all_interactions", function ($scope, $http, share) {
//    $scope.time = 0;
//    $scope.map_cluster_click_popup = false;
//    $("#map_cluster_click_popup").hide();
//    $scope.peopleMet = 0
//    $scope.totalInteractions = 0
//    $scope.interactionItems = []
//
//    $scope.showUserLocation = function() {
//        $scope.geocoder = new google.maps.Geocoder();
//        $scope.myLatlng = new google.maps.LatLng(13.5333, 2.0833);
//
//        $scope.mapOptions = {
//            center: $scope.myLatlng,
//            zoom: 2
//        };
//
//        $scope.map = new google.maps.Map(document.getElementById("map"), $scope.mapOptions);
//
//        $scope.markerCluster = new MarkerClusterer($scope.map, [], {zoomOnClick: false, minimumClusterSize: 1, styles: styles});
//
//        $scope.interactionsCount();
//        google.maps.event.addListener($scope.markerCluster, 'clusterclick', function (cluster) {
//            $scope.c = cluster.getMarkers();
//            if (checkRequired($scope.c[0])) {
//                $scope.showGlobalInteractionInfo($scope.c);
//            }
//        });
//        google.maps.event.addListener($scope.map, 'zoom_changed', function () {
//            if ($scope.map.getZoom() < 2) $scope.map.setZoom(2);
//        });
//    };
//
//    $scope.interactionsCount = function() {
//        $http.get('/dashboard/interactions/globalmap')
//            .success(function(response){
//                if(response.SuccessCode){
//                    if (response.Data && response.Data.length > 0) {
//                        for (var interaction = 0; interaction < response.Data.length; interaction++) {
//                            if (checkRequired(response.Data[interaction]._id)) {
//                                var result = response.Data[interaction]._id;
//                                result.count = response.Data[interaction].count;
//                                $scope.showLocations(result);
//                            }
//                        }
//                    }
//                }
//            })
//    };
//
//    $scope.showLocations = function(result) {
//        if (checkRequired(result) && result.count !=0) {
//            if (checkRequired(result.locationLatLang)) {
//                if (checkRequired(result.locationLatLang.latitude) && checkRequired(result.locationLatLang.longitude)) {
//                    $scope.createMarkerLocationLatLang(result, result.locationLatLang.latitude, result.locationLatLang.longitude);
//                } else if (checkRequired(result.location)) {
//                    $scope.createMarkerGeocode(result, 0);
//                } else  $scope.createMarkerCurrentLocation(result)
//            } else if (checkRequired(result.location)) {
//                $scope.createMarkerGeocode(result, 0);
//            } else  $scope.createMarkerCurrentLocation(result)
//        }
//    };
//
//    $scope.createMarkerLocationLatLang = function(result, lat, lang) {
//        var l = '';
//        if(checkRequired(result.currentLocation)){
//            l = result.currentLocation.city;
//            if(checkRequired(result.currentLocation.country)){
//                l += ","+result.currentLocation.country
//            }
//        }
//        if(!checkRequired(l)){
//            l = result.location;
//        }
//
//        result.location = l;
//        var marker = new google.maps.Marker({
//            position: new google.maps.LatLng(parseFloat(lat), parseFloat(lang)),
//            map: $scope.map,
//            title: "Meeting location: " + result.location || '',
//            user: result
//        });
//        $scope.markerCluster.addMarker(marker);
//    };
//
//    $scope.createMarkerCurrentLocation = function(result) {
//        if (checkRequired(result.currentLocation)) {
//            if (checkRequired(result.currentLocation.latitude)) {
//                var l = result.currentLocation.city;
//                if(checkRequired(result.currentLocation.country)){
//                    l += ","+result.currentLocation.country
//                }
//                if(!checkRequired(l)){
//                    l = result.location;
//                }
//                result.location = l;
//                var marker = new google.maps.Marker({
//                    position: new google.maps.LatLng(result.currentLocation.latitude, result.currentLocation.longitude),
//                    map: $scope.map,
//                    title: "Meeting location: " + result.currentLocation.city || '',
//                    user: result
//                });
//                $scope.markerCluster.addMarker(marker)
//            }
//        }
//    };
//
//    $scope.createMarkerGeocode = function(result, limit) {
//        $scope.geocoder.geocode({ 'address': result.location}, function (results, status) {
//            if (status == google.maps.GeocoderStatus.OK) {
//                var addr = results[0].address_components;
//                var city = '';
//                var country = '';
//                var address = '';
//                for(var i=0; i<addr.length; i++){
//                    if(addr[i].types.indexOf("locality") != -1){
//                        city = addr[i].long_name;
//                    }
//                    if(addr[i].types.indexOf("country") != -1){
//                        country = addr[i].long_name;
//                    }
//                }
//                if(checkRequired(city)){
//                    address = city;
//                }
//                if(checkRequired(country)){
//                    if(checkRequired(city)){
//                        address += ", "+country
//                    }
//                    else address = country;
//                }
//                result.location = address;
//                var newAddress = results[0].geometry.location;
//
//                var marker = new google.maps.Marker({
//                    position: new google.maps.LatLng(parseFloat(newAddress.lat()), parseFloat(newAddress.lng())),
//                    map: $scope.map,
//                    title: "Meeting location: " + address || '',
//                    user: result
//                });
//                var latLangObj = {
//                    userId: result._id,
//                    latitude: newAddress.lat(),
//                    longitude: newAddress.lng()
//                };
//                $scope.markerCluster.addMarker(marker);
//                $scope.updateLocationLatLang(latLangObj);
//
//            } else if (status == 'OVER_QUERY_LIMIT') {
//                $scope.time += 500;
//                limit++;
//                if (limit < 3) {
//                    setTimeout(function () {
//                        $scope.createMarkerGeocode(result, limit);
//                    },  $scope.time);
//                }
//            }
//        });
//    };
//
//    $scope.updateLocationLatLang = function(latLangObj) {
//        $http.post('/updateLocationLatLang',latLangObj)
//            .success(function(response){
//
//            });
//    };
//
//    $scope.showGlobalInteractionInfo = function(cluster) {
//
//        var infoContent = cluster;
//        $scope.interactionItems = [];
//        $scope.totalInteractions = 0;
//        $scope.peopleMet = cluster.length;
//        infoContent.sort(function(a,b)
//        {
//            if (a.user.count < b.user.count) return 1;
//            if (a.user.count > b.user.count) return -1;
//            return 0;
//        });
//        var mmmmm = '';
//        for (var m = 0; m < infoContent.length; m++) {
//            $scope.totalInteractions += parseInt(infoContent[m].user.count || 0);
//            var name = infoContent[m].user.firstName + ' ' + infoContent[m].user.lastName;
//            var totalm = infoContent[m].user.count < 10 ? '0' + infoContent[m].user.count : infoContent[m].user.count;
//            totalm = infoContent[m].user.location+"("+totalm+")";
//            var userUrl = '/' + $scope.getValidUniqueUrl(infoContent[m].user.publicProfileUrl)
//            var summaryUrl = '/interactions/summary/' + infoContent[m].user._id;
//            mmmmm += '<div class="map-row clearfix"><span class="pull-left" ng-click="goToUrl(interactionUrl)" title=' + name.replace(/\s/g, '&nbsp;') + '><a style="color:#333333" href=' + userUrl + '>' + getTextLength(name, 14) + '</a></span><span class="pull-right fs-14"><a style="color:#333333" href=' + summaryUrl + '>' + totalm + '</a></span></div>'
//        }
//
//        share.updatePopup($scope.peopleMet,$scope.totalInteractions,mmmmm);
//        // $scope.map_cluster_click_popup = true;
//    };
//
//    $scope.getValidUniqueUrl = function(name){
//        if(name.charAt(0) == 'h'){
//            name = name.split('/');
//            name = name[name.length - 1];
//            return name;
//        }
//        else return name;
//    };
//
//    $scope.showUserLocation();
//
//});

reletasApp.controller("participantPast90Days", function ($rootScope, $scope, $window, $http, share) {
  function paintIniterationInitiative(usersInHierarchy, days){
    var parser = document.createElement('a')
    var companyName = getCompanyNameFromUrl($(location).attr('href'))

    var url = fetchUrlWithParameter("/company/interactions/initiative?company=" + companyName, "hierarchylist", usersInHierarchy)
    url = fetchUrlWithParameter(url, "days", days)
      if(usersInHierarchy){
          url = fetchUrlWithParameter(url, "onSelect", true)
      } else {
          url = fetchUrlWithParameter(url, "onSelect", false)
      }

    $.getJSON(url, function(data){
      if(data.Data){
        $scope.myInitiatives = data.Data.initiatedByMe
        $scope.othersInitiative = data.Data.initiatedByOthers
      }else{
        $scope.myInitiatives = 0
        $scope.othersInitiative = 0
      }
      $scope.totalInteractionsInitiatives = $scope.myInitiatives + $scope.othersInitiative
      $scope.$apply()
      var initiateByMePercentage = Math.round(($scope.myInitiatives/ ($scope.othersInitiative + $scope.myInitiatives)) *100)
      var initiatedByOthersPercentage = Math.round(($scope.othersInitiative/ ($scope.othersInitiative + $scope.myInitiatives)) *100)
      var canvas  = document.getElementById("donut-chart");
      var chart = canvas.getContext("2d");

      function drawdountChart(canvas)
      {

          this.x , this.y , this.radius , this.lineWidth , this.strockStyle , this.from , this.to = null;
          this.set = function( x, y, radius, from, to, lineWidth, strockStyle)
          {
              this.x = x;
              this.y = y;
              this.radius = radius;
              this.from=from;
              this.to= to;
              this.lineWidth = lineWidth;
              this.strockStyle = strockStyle;
          }

          this.draw = function(data)
          {
              canvas.beginPath();
              canvas.lineWidth = this.lineWidth;
              canvas.strokeStyle = this.strockStyle;
              canvas.arc(this.x , this.y , this.radius , this.from , this.to);
              canvas.stroke();
              var numberOfParts = data.numberOfParts;
              var parts = data.parts.pt;
              var colors = data.colors.cs;
              var df = 0;
              for(var i = 0; i<numberOfParts; i++)
              {
                  canvas.beginPath();
                  canvas.strokeStyle = colors[i];
                  canvas.arc(this.x, this.y, this.radius, df, df + (Math.PI * 2) * (parts[i] / 100));
                  canvas.stroke();
                  df += (Math.PI * 2) * (parts[i] / 100);
              }
          }
      }
      var data ={
          numberOfParts: 2,
          parts:{"pt": [initiateByMePercentage , initiatedByOthersPercentage ]},//percentage of each parts
          colors:{"cs": ["#f86b4f", "rgba(248, 107, 79, 0.65)"]}//color of each part
      };

      var drawDount = new drawdountChart(chart);
      drawDount.set(100, 100, 65, 0, Math.PI*2, 15, "#fff");
      drawDount.draw(data);
    })
  }

  paintIniterationInitiative(null, defaultNoOfDays)
  var dropType = "monthlyInteraction"
  $rootScope.$on("hierarchyModified", function(event, data){
    paintIniterationInitiative(data, $window.noOfDays[dropType] || defaultNoOfDays)
  })

  $rootScope.$on(dropType + "DaysChanged", function(event, data){
    paintIniterationInitiative(($rootScope.usersInHierarchy || null), data)
  })
});

var start = 1;

var itemsScroll = 6;
var itemsToShow = 6;

if(window.innerWidth < 480){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 1
    itemsScroll = 1
}else if(window.innerWidth < 900){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 3
    itemsScroll = 3
}
var end = itemsToShow;

reletasApp.controller("commonConnections_company", function ($scope, $http, share) {

    $scope.companyName = getCompanyNameFromUrl($(location).attr('href'))

    $scope.cConnections = [];
    $scope.cConnectionsIds = [];
    $scope.currentLength = 0;
    $scope.isFirstTime = true;
    $scope.isConnectionOpen = false;
    $scope.start = start;
    $scope.end = end;
    $scope.isAllConnectionsLoaded = false;

    share.bindPName = function(p_name){
        $scope.p_name = p_name;
    }

    share.commonConnections_company = function(userId,emailId,url){

        $scope.cConnections = [];
        $scope.cConnectionsIds = [];
        $scope.currentLength = 0;
        $scope.isFirstTime = true;
        $scope.isConnectionOpen = false;
        $scope.start = start;
        $scope.end = end;
        $scope.isAllConnectionsLoaded = false;
        $scope.userId = userId;
        $scope.emailId = emailId;

        getCommonConnections_company($scope, $http, url, true);
    };

    $scope.NextConn = function(){
        var statusNext =  $scope.statusNext;
        var statusPrev =  $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.end < total){
            var itemsToStayPrev = $scope.start+itemsScroll;
            var itemsToStayNext = $scope.end+itemsScroll;
            for(var i=0; i<$scope.cConnectionsIds.length; i++){

                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    addHide($scope.cConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end+itemsScroll;
            $scope.start = $scope.start+itemsScroll;

            $scope.statusNext = statusNext+itemsScroll;
            $scope.statusPrev = statusNext;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
        }
        $scope.showHideArrows();
        if($scope.fetchCommonConnectionsNext() && $scope.isAllConnectionsLoaded == false && $scope.isConnectionOpen == false){
            $scope.isConnectionOpen = true;
            var url ='/contacts/filter/common/companyName/web?companyName='+share.l_user.companyName+'&emailId='+share.l_user.emailId+'&contactEmailId='+$scope.emailId+'&skip='+$scope.currentLength+'&limit=12'

            getCommonConnections_company($scope, $http, url, false);
        }
    };

    $scope.fetchCommonConnectionsNext = function(){
        if($scope.end >= $scope.cConnectionsIds.length - 12){
            return true;
        }else return false;
    };

    $scope.PrevConn = function(){

        var statusPrev = $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.start >1){
            var itemsToStayPrev = $scope.start-itemsScroll;
            var itemsToStayNext = $scope.end-itemsScroll;

            for(var i=0; i<$scope.cConnectionsIds.length; i++){

                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    addHide($scope.cConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end-itemsScroll;
            $scope.start = $scope.start-itemsScroll;

            $scope.statusNext = statusPrev;
            $scope.statusPrev = statusPrev-itemsScroll+1;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
            $scope.showHideArrows()
        }
    };

    $scope.showHideArrows = function(){
        if($scope.grandTotal >0){
            if($scope.start <=1 ){
                addHide("als-prev_c",true)
            }
            else addHide("als-prev_c",false)

            if($scope.end >= $scope.grandTotal){
                addHide("als-next_c",true)
            }
            else addHide("als-next_c",false)
        }
        else{
            addHide("als-prev_c",true)
            addHide("als-next_c",true)
        }
    };

    $scope.connectionClick = function(url){
        if(checkRequired(url)){
            trackMixpanel("clicked on company common connection ",url,function(){
                window.location.replace(url);
            })
        }
    }
});

function getCommonConnections_company($scope, $http, url, isHides){
    $http.get(url)
        .success(function (response) {

            $scope.isConnectionOpen = false;
            if(response.SuccessCode){
                $scope.grandTotal = response.Data.total;
                if(response.Data.contacts && response.Data.contacts.length > 0){
                    var html = '';
                    hidePrevNext('als-prev_c','als-next_c',true)
                    $scope.no_connections_by_company = false
                    $scope.currentLength += response.Data.returned;
                    for(var i=0; i<response.Data.contacts.length; i++){
                        var li = '';
                        var obj;
                        if(checkRequired(response.Data.contacts[i].personId) && checkRequired(response.Data.contacts[i].personId.firstName)){
                            var name = getTextLength(response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName,13);
                            var image = '/getImage/'+response.Data.contacts[i].personId._id;
                            var url = response.Data.contacts[i].personId.publicProfileUrl;
                            var emailId = response.Data.contacts[i]._id.emailId;
                            if(url.charAt(0) != 'h'){
                                url = '/'+url;
                            }
                            obj = {
                                fullName:response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName,
                                name:name,
                                image:image,
                                url:url,
                                emailId:emailId,
                                cursor:'cursor:pointer',
                                idName:'com_con_item_c_'+response.Data.contacts[i]._id,
                                className:'hide'
                            };
                            if(isHides && i<itemsToShow){
                                obj.className = ''
                            }
                            obj.emailId = response.Data.contacts[i]._id.emailId;
                            $scope.cConnections.push(obj);
                            $scope.cConnectionsIds.push('com_con_item_c_'+response.Data.contacts[i]._id);
                        }
                        else{
                            obj = {
                                fullName:response.Data.contacts[i].personName,
                                name:getTextLength(response.Data.contacts[i].personName,13),
                                image:"/images/default.png",
                                cursor:'cursor:default',
                                url:null,
                                emailId:response.Data.contacts[i]._id.emailId,
                                idName:'com_con_item_c_'+response.Data.contacts[i]._id,
                                className:'hide'
                            };
                            if(isHides && i<itemsToShow){
                                obj.className = ''
                            }
                            obj.emailId = response.Data.contacts[i]._id.emailId;
                            $scope.cConnections.push(obj);
                            $scope.cConnectionsIds.push('com_con_item_c_'+response.Data.contacts[i]._id);
                        }
                    }
                    if(isHides){
                        $scope.statusPrev = 1;
                        $scope.statusNext = itemsScroll;
                    }
                    $scope.showHideArrows()
                }
                else if(isHides){
                    hidePrevNext('als-prev_c','als-next_c',true)
                    $scope.no_connections_by_company = true
                }else $scope.isAllConnectionsLoaded = true;
            }
            else if(isHides){
                $scope.no_connections_by_company = true
                hidePrevNext('als-prev_c','als-next_c',true)

            }else $scope.isAllConnectionsLoaded = true;
        })
}

function commonConnectionsMessage(hide){
    if(hide){
        $("#commonConnectionsName_msg").addClass('hide')
        $("#demo3").removeClass('hide')
    }
    else{
        $("#commonConnectionsName_msg").removeClass('hide')
        $("#demo3").addClass('hide')
    }
}

function hidePrevNext(idNameP,idNameN,hide){
    if(hide){
        $("#"+idNameP).addClass('hide')
        $("#"+idNameN).addClass('hide')
    }
    else{
        $("#"+idNameP).removeClass('hide')
        $("#"+idNameN).removeClass('hide')
    }
}

reletasApp.controller("interaction_timeline", function ($scope, $http, share){

});

reletasApp.controller("task-assign",function($scope, $http, share){

});

reletasApp.controller("docAnalytics",function($scope, $http, share){
    share.showDocAnalytics = function(emailId,refId){
        $http.get('/analytics/'+refId+'/byEmail/'+emailId)
            .success(function(response){
                if(response && response.accessInfo && response.accessInfo.pageReadTimings){

                    var prt = response.accessInfo.pageReadTimings;
                    var abc = prt.slice();
                    var prtNoDups = removeDuplicates(abc);

                    var separate = [];
                    for (var i in prtNoDups) {

                        var totalTime = 0;
                        var count = 0;
                        var pageNo = 0;
                        for (var j in prt) {
                            if (prt[j].pageNumber == prtNoDups[i].pageNumber) {

                                totalTime = totalTime + parseFloat(prt[j].Time);
                                count += 1;
                                pageNo = prtNoDups[i].pageNumber;
                            }
                        }
                        //  if(totalTime > 0){
                        var before = totalTime;

                        // totalTime = totalTime/count
                        totalTime = Math.round(totalTime / 60) // im minutes
                        if (totalTime == 0) {
                            if (before > 0) {
                                totalTime = 0.5
                            }
                        }
                        // }

                        var tootlTip = 'Page:' + pageNo + '\nMinutes:' + totalTime;
                        separate.push([
                            pageNo,
                            totalTime,
                            tootlTip,
                            'color:#03A2EA'
                        ])
                        var timeMax = 0;
                        jQuery.map(separate, function (obj) {
                            if (obj[1] > 0)
                                timeMax = obj[1];
                        });
                    }
                    $scope.firstAccessed = "First Accessed: " + $scope.getDateFormat(response.firstAccessed) + ' ' + $scope.getTimeFormat(response.firstAccessed);
                    $scope.lastAccessed = "Last Accessed : " + $scope.getDateFormat(response.lastAccessed) + ' ' + $scope.getTimeFormat(response.lastAccessed)
                    $scope.numerOfTimesAccessed = "Number of times accessed: " + response.accessCount || 0

                    $("#analytics-box").toggle(100);
                    var position = $("#"+refId).offset();
                    $("#analytics-box").css({top:position.top});

                    if (checkRequired(separate[0])) {
                        $scope.drawAnalytics(separate, prtNoDups.length, timeMax);
                    }
                    else {
                        toastr.info("This document not accessed yet.");
                    }
                }
                else toastr.info("Document not accessed yet.");
            })
    };

    $scope.drawAnalytics = function(a, maxPages, timeMax) {

        var data = new google.visualization.DataTable();

        data.addColumn('number', 'Pages');
        data.addColumn('number', 'Minutes');
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addColumn({type: 'string', role: 'style'});
        data.addRows(a);
        var options = {
            width: 250,
            hAxis: {title: 'Page', minValue: 0, maxValue: maxPages + 2},
            vAxis: {title: 'Total Page Open Time', minValue: 0, maxValue: timeMax + 2}
        };


        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById("doc_chart"));
        chart.draw(data, options);

    };

    $scope.getDateFormat = function(date) {
        var dateObj = moment(date).tz(timezone)
        return dateObj.format("DD MMM, YYYY")
        //return dateObj.getDate() + ' ' + monthNameSmall[dateObj.getMonth()] + ',' + dateObj.getFullYear()
    };

    $scope.getTimeFormat = function(date) {
        var dateObj = moment(date).tz(timezone);
        return dateObj.format("hh:mm a")
    };
});

reletasApp.controller("send_email_controller",function($scope, $http, share){

    share.openEmilContainer = function(){
        $("#compose-email-box-top").toggle(100)
    };
    $scope.openEmilContainer = share.openEmilContainer;
    $scope.resetFields = function(){
        $scope.compose_email_subject = "";
        $scope.compose_email_body = "";
        $scope.compose_email_doc_tracking = true;
        $scope.compose_email_track_viewed = true;
        $scope.compose_email_remaind = true;
        $scope.url = "";
        $scope.companyName = "";
        $scope.designation = "";
    };

    share.updateSendEmailContent = function(compose_email_subject,compose_email_body,compose_email_doc_tracking,compose_email_track_viewed,compose_email_remaind,p_name,cEmailId,cuserId,url,companyName,designation){
        $scope.compose_email_subject = compose_email_subject;
        $scope.compose_email_body = compose_email_body;
        $scope.compose_email_doc_tracking = compose_email_doc_tracking;
        $scope.compose_email_track_viewed = compose_email_track_viewed;
        $scope.compose_email_remaind = compose_email_remaind;
        $scope.p_name = p_name;
        $scope.cEmailId = cEmailId;
        $scope.cuserId = cuserId;
        $scope.url = url;
        $scope.companyName = companyName;
        $scope.designation = designation;
    };

    $scope.sendEmail = function(subject,body,docTrack,trackViewed,remind,rName,rEmailId,receiverId){

        if(!checkRequired(subject)){
            toastr.error("Please enter an email subject.")
        }
        else if(!checkRequired(body)){
            toastr.error("Please enter an email body.")
        }
        else{
            var obj = {
                receiverEmailId:rEmailId,
                receiverName:rName,
                message:body,
                subject:subject,
                receiverId:receiverId,
                docTrack:docTrack,
                trackViewed:trackViewed,
                remind:remind
            };
            $("#send-email-but").addClass("disabled")
            $http.post("/messages/send/email/single/web",obj)
                .success(function(response){
                    if(response.SuccessCode){
                        $("#send-email-but").removeClass("disabled");
                        $("#compose-email-box-top").addClass('hide')
                        $scope.resetFields()
                        toastr.success(response.Message);

                    }
                    else{
                        $("#send-email-but").removeClass("disabled")
                        toastr.error(response.Message);
                    }
                })
        }
    };
});

function arrayContains(needle, arrhaystack) {
    //return (arrhaystack.indexOf(needle) > -1);
    if(arrhaystack.indexOf(needle) > -1){
        return needle
    };

}

function getInteractionTypeImage (type){
    switch(type){
        case 'google-meeting':
        case 'meeting':
            return "fa-calendar-check-o green-color";
            break;
        case 'document-share':
            return "fa-file-pdf-o";
            break;
        case 'message':
        case 'meeting-comment':
        case 'email':
            return "fa-envelope green-color";
            break;
        case 'sms':
            return "fa-reorder green-color";
            break;
        case 'call':
            return "fa-phone green-color";
            break;
        case 'task':
            return "fa-check-square-o";
            break;
        case 'twitter':
            return "fa-twitter-square twitter-color";
            break;
        case 'facebook':
            return "fa-facebook-official fb-color";
            break;
        case 'linkedin':
            return "fa-linkedin-square linkedin-color";
            break;
        case 'calendar-password':
            return "fa-calendar-check-o green-color";
            break;
        default :return '';
    }
}
