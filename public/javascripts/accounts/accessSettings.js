relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/customer/middle/bar"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

            if(typeof str == 'string' && str.length > 0){
                if(searchContent.length >= 3) {
                    yourNetwork = yourNetwork ? true : false;
                    extendedNetwork = extendedNetwork ? true : false;
                    forCompanies = forCompanies ? true : false;
                    window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;

                }
            }
            else toastr.error("Please enter search content")
    };
});

relatasApp.controller("logedinUser", function ($scope, $http, $rootScope,share) {

    $http.get('/profile/get/current/web')
        .success(function (response) {

            if(response.SuccessCode){
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.l_usr = response.Data;
                $rootScope.currentUser = response.Data;
                $rootScope.$broadcast("fetchedUserProfile", response.Data.corporateUser)
                identifyMixPanelUser(response.Data);
                timezone = "UTC"
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone?response.Data.timezone.name:"UTC";
                }

                share.getCurrentFiscalYear = getCurrentFiscalYear(timezone,response.companyDetails.fyMonth)

                $rootScope.companyDetails = response.companyDetails;
                $rootScope.netGrossMargin = response.companyDetails.netGrossMargin;


                if(response.companyDetails && response.companyDetails.opportunitySettings){
                    $rootScope.isProductRequired = response.companyDetails.opportunitySettings.productRequired;
                    $rootScope.isRegionRequired = response.companyDetails.opportunitySettings.regionRequired;
                    $rootScope.isVerticalRequired = response.companyDetails.opportunitySettings.verticalRequired;
                    $rootScope.isSourceRequired = response.companyDetails.opportunitySettings.sourceRequired;
                }

                if(response.Data.contactsUpload && response.Data.contactsUpload.isUploaded){

                }else $("#uploadContactsAlert").removeClass("hide");
            }
            else{

            }
        }).error(function (data) {

    })
});

relatasApp.controller("access_control", function ($scope, $http, $rootScope, share) {

    $("body").on("click","#showLeftbar",function(){
        $("#sidebar-wrapper").toggle(100);
    });

    $scope.getAllData = function(){
        getTeam($scope,$http,function (teamArray,teamDictionary,usersDictionaryWithUserId) {
            share.teamDictionary = teamDictionary;
            share.teamArray = teamArray;

            $scope.teamArray = teamArray;

            $scope.teamArray.unshift({
                fullName:"All team members",
                isAll:true
            })
            share.usersDictionaryWithUserId = usersDictionaryWithUserId;
            $scope.accountsForAccess = [];

            function checkLiuExists(){

                if($rootScope.currentUser){

                    var url = '/account/get/owners';
                    if(teamDictionary[$rootScope.currentUser.emailId].corporateAdmin){
                        url = url+"?isCorporateAdmin=true"
                    }

                    $scope.getContacts(null,url,0,25,$scope.filterBy,null,null,function (account) {
                    });
                } else {
                    setTimeOutCallback(1000,function () {
                        checkLiuExists()
                    })
                }
            }

            checkLiuExists();

            getAccounts($scope,$http,share,"/account/access/requested",function (accounts) {
                $scope.accountsForAccess = accounts;
            });

        });
    }

    $scope.getAllData();

    $scope.grantAllRequests = function () {

        _.each($scope.accountsForAccess,function (account) {
            _.each(account.accountAccessRequested,function (el) {

                if($scope.grantAccess){
                    el.accessStatus = true;
                } else {
                    el.accessStatus = false;
                }
            })
        })
    };

    $scope.grantAccessAccount = function (account) {
        var obj = {
            accounts:[]
        };

        if(account){

            _.each(account.accountAccessRequested,function (el) {
                obj.accounts.push({
                    name:account.fullName,
                    ownerEmailId:account.ownerEmailId,
                    emailId:el.profile.emailId,
                    accessStatus:el.accessStatus
                })
            });

            grantAccess($rootScope,$scope,$http,share,obj)
        }
    }

    $scope.grantAccessToAll = function () {

        var obj = {
            accounts:[]
        };

        _.each($scope.accountsForAccess,function (account) {
            _.each(account.accountAccessRequested,function (el) {
                if(el.accessStatus){
                    obj.accounts.push({
                        name:account.fullName,
                        ownerEmailId:account.ownerEmailId,
                        emailId:el.profile.emailId,
                        accessStatus:true
                    })
                }
            });
        })

        grantAccess($rootScope,$scope,$http,share,obj)
    }

    $scope.accessControl = true;
    $scope.accessSelected = "selected"
    // $scope.accountTransfer = true;
    $scope.showAccessControl = function () {
        $scope.accessControl = true;
        $scope.accountTransfer = false;
        $scope.transferSelected = ""
        $scope.accessSelected = "selected"
    }

    $scope.showAccountTransfer = function () {
        $scope.accessSelected = ""
        $scope.transferSelected = "selected"
        $scope.accountTransfer = true;
        $scope.accessControl = false;
    }

    $scope.selectedUser2 = {
        fullName: "All team members",
        isAll:true
    }

    $scope.selectTeamMember = function (user) {
        $scope.selectedUser2 = user;
        $scope.showList = false;
        $scope.searchContent = null;
        $("#searchContent").val("");
        var url = '/account/get/owners';

        if(!user.isAll) {
            url = '/account/get/owners?getAllAccounts='+user.userId;
        }

        if($rootScope.isCorporateAdmin && user.isAll){
            url = '/account/get/owners'+"?isCorporateAdmin=true";
        }

        function checkLiuExists(url){

            if($rootScope.currentUser){

                $scope.getContacts(null,url,0,25,$scope.filterBy,null,null,function (account) {
                });
            } else {
                setTimeOutCallback(1000,function () {
                    checkLiuExists(url)
                })
            }
        }

        checkLiuExists(url);
    }

    closeAllDropDownsAndModals($scope,".absolute")

    $scope.prevPage = function (skip) {

        $("#selectAll").prop('checked', false);
        $scope.thisAcc = false;

        $scope.skip = skip;
        if(skip <= 0){
            $scope.prevBtn = {"visibility":"hidden"}
        } else {
            $scope.prevBtn = {"visibility":"visible"}
        }

        $scope.nextBtn = {"visibility":"visible"}

        function checkLiuExists(){

            if($rootScope.currentUser){

                var url = '/account/get/owners';
                if(share.teamDictionary[$rootScope.currentUser.emailId].corporateAdmin){
                    url = url+"?isCorporateAdmin=true"
                }

                $scope.getContacts(null,url,skip,25,$scope.filterBy,null,null,function (account) {
                });

            } else {
                setTimeOutCallback(1000,function () {
                    checkLiuExists()
                })
            }
        }

        checkLiuExists();
    }

    $scope.nextPage = function (skip) {
        $("#selectAll").prop('checked', false);
        $scope.thisAcc = false;
        $scope.skip = skip;
        if((skip >= $scope.totalPages) || (($scope.totalPages-skip)<=10)){
            $scope.nextBtn = {"visibility":"hidden"}
        } else {
            $scope.nextBtn = {"visibility":"visible"}
        }

        $scope.prevBtn = {"visibility":"visible"}

        function checkLiuExists(){

            if($rootScope.currentUser){

                var url = '/account/get/owners';
                if(share.teamDictionary[$rootScope.currentUser.emailId].corporateAdmin){
                    url = url+"?isCorporateAdmin=true"
                }

                $scope.getContacts(null,url,skip,25,$scope.filterBy,null,null,function (account) {
                });

            } else {
                setTimeOutCallback(1000,function () {
                    checkLiuExists()
                })
            }
        }

        checkLiuExists();
    }
    
    $scope.searchAccounts = function (searchContent) {
        $("#selectAll").prop('checked', false);
        $scope.thisAcc = false;
        $scope.searchContent = searchContent;

        function checkLiuExists(){

            if($rootScope.currentUser){

                var url = '/account/get/owners';
                if(share.teamDictionary[$rootScope.currentUser.emailId].corporateAdmin){
                    url = url+"?isCorporateAdmin=true"
                }

                if($scope.selectedUser2 && $scope.selectedUser2.userId) {
                    var url2 = '/account/get/owners'
                    url2 = url2+'?getAllAccounts='+$scope.selectedUser2.userId;
                    url2 = url2+'&searchContent='+searchContent;
                    if(share.teamDictionary[$rootScope.currentUser.emailId].corporateAdmin){
                        url2 = url2+"&isCorporateAdmin=true"
                    }

                    $scope.getContacts(null,url2,0,25,$scope.filterBy,null,null,function (account) {
                    });

                } else {
                    $scope.getContacts(null,url,0,25,$scope.filterBy,searchContent,null,function (account) {
                    });
                }

            } else {
                setTimeOutCallback(1000,function () {
                    checkLiuExists()
                })
            }
        }

        checkLiuExists();
    }

    $scope.getContacts = function(options,url,skip,limit,filterBy,searchContent,isBack,callback){

        $scope.isBack = isBack;
        $scope.skip = skip;
        $scope.limit = limit;
        $scope.skipValue = 25;

        if(url.indexOf("?") == -1)
            url = url + "?"
        else
            url = url + "&"
        if(checkRequired(searchContent)){
            url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy+'&searchContent='+searchContent
        } else if(filterBy){
            url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy;
        }

        if(options){
            url = url+"&options="+options
        }
        $scope.selectedAccounts = 0;
        $scope.contactsList = [];
        $scope.loadingTargets = false;
        $http.get(url)
            .success(function(response){
                $scope.contactsList = [];
                $("#selectAll").prop('checked', false);
                $scope.isConnectionOpen = false;
                $scope.thisAcc = false;
                $scope.loadingTargets = true;
                if(response.SuccessCode){
                    
                    $scope.disablePagination = response.Data.disablePagination;

                    skip = skip?skip:0;
                    var from = (parseInt(skip)+1);
                    var to = (parseInt(skip)+$scope.skipValue);
                    $scope.totalPages = response.Data.total;

                    if(to>$scope.totalPages){
                        to = $scope.totalPages
                    }

                    $scope.showingPages = from+" - "+ to;

                    $scope.grandTotal = response.Data.total;

                    if(response.Data.accounts.length > 0){

                        if(!isBack || $scope.currentLength <= 0){
                            $scope.currentLength += response.Data.returned;
                            $scope.returned = response.Data.returned
                        }

                        if($scope.currentLength > 25){
                            $scope.show_contacts_back = true;
                        }else $scope.show_contacts_back = false;

                        for(var i=0; i<response.Data.accounts.length; i++){

                            if(response.Data.accounts[i].name){
                                var name = response.Data.accounts[i].name;
                                var hasAccess = false;
                                var accessIcon = "fa fa-lock";
                                var ownerEmailId = response.Data.accounts[i].ownerEmailId;
                                var ownerId = response.Data.accounts[i].ownerId;

                                var usersWithAccess = [];

                                _.each(response.Data.accounts[i].access,function (el) {
                                    if(el.emailId != ownerEmailId){
                                        usersWithAccess.push(el.emailId)
                                    }
                                })

                                if(response.Data.accounts[i].reportingHierarchyAccess && response.Data.accounts[i].heiarchyWithAccess && response.Data.accounts[i].heiarchyWithAccess.length>0){
                                    _.each(response.Data.accounts[i].heiarchyWithAccess,function (el) {
                                        usersWithAccess.push(share.usersDictionaryWithUserId[el].emailId)
                                    })
                                }

                                if(_.includes(usersWithAccess,response.liuEmailId) || ownerEmailId == response.liuEmailId){
                                    hasAccess = true;
                                    accessIcon = ""
                                } else {
                                    hasAccess = false;
                                    accessIcon = "fa fa-lock"
                                }

                                var costOfSalesWithComma = 0,
                                    costOfDeliveryWithComma = 0;

                                if(response.Data.accounts[i].costOfSales){
                                    costOfSalesWithComma = getAmountInThousands(response.Data.accounts[i].costOfSales,2)
                                }

                                if(response.Data.accounts[i].costOfDelivery){
                                    costOfDeliveryWithComma = getAmountInThousands(response.Data.accounts[i].costOfDelivery,2)
                                }

                                var selection = ""

                                var obj;

                                obj = {
                                    isOwner:ownerEmailId == response.liuEmailId,
                                    userId:name,
                                    fullName:name,
                                    designation:"",
                                    fullDesignation:"",
                                    name:(getTextLength(name,25)).capitalizeFirstLetter(),
                                    emailId:name,
                                    noPicFlag:true,
                                    image:"https://logo.clearbit.com/"+ getTextLength(name,25) +".com",
                                    noPPic:(name.substr(0,2)).capitalizeFirstLetter(),
                                    cursor:'cursor:default',
                                    url:null,
                                    idName:'com_con_item_'+name,
                                    hasAccess:hasAccess,
                                    target:response.Data.accounts[i].target,
                                    partner:response.Data.accounts[i].partner,
                                    important:response.Data.accounts[i].important,
                                    costOfSales:response.Data.accounts[i].costOfSales,
                                    costOfDelivery:response.Data.accounts[i].costOfDelivery,
                                    costOfSalesWithComma:costOfSalesWithComma,
                                    costOfDeliveryWithComma:costOfDeliveryWithComma,
                                    accessIcon:accessIcon,
                                    usersWithAccess:usersWithAccess,
                                    hierarchyAccess:response.Data.accounts[i].heiarchyWithAccess,
                                    reportingHierarchyAccess:response.Data.accounts[i].reportingHierarchyAccess,
                                    selection:selection,
                                    ownerId:ownerId,
                                    profile:share.teamDictionary[ownerEmailId],
                                    ownerEmailId:ownerEmailId,
                                    accountAccessRequested:response.Data.accounts[i].accountAccessRequested,
                                    contacts:response.Data.accounts[i].contacts,
                                    lastInteractedDate:response.Data.accounts[i].lastInteractedDate
                                };

                                $scope.contactsList.push(obj);
                                $http.get("/companylogos/" + getTextLength(name,25) +".com")
                            }
                        }

                        if(options == "Recent"){
                            $scope.contactsList.sort(function (o1, o2) {
                                return new Date(o1.lastInteractedDate) < new Date(o2.lastInteractedDate) ? 1 : new Date(o1.lastInteractedDate) > new Date(o2.lastInteractedDate) ? -1 : 0;
                            });
                        }
                    }
                    else {
                        $scope.isAllConnectionsLoaded = true;
                    }
                }
                else{
                    $scope.grandTotal = 0;
                    $scope.isAllConnectionsLoaded = true;
                }

                if(callback){

                    if($scope.contactsList[0]){
                        $scope.contactsList[0]["selection"] = "contact-selected";
                        callback($scope.contactsList[0])
                    } else {
                        callback(null)
                    }
                }
            })
    };

    $scope.selectedAll = function () {
        $scope.accountsToTransfer = [];

        if($scope.thisAcc){

            _.each($scope.contactsList,function (el) {
                el.thisAcc = true;
                $scope.accountsToTransfer.push(el)
            });

            $scope.selectedAccounts = $scope.accountsToTransfer.length;
        } else {

            _.each($scope.contactsList,function (el) {
                el.thisAcc = false;
            });

            $scope.selectedAccounts = 0;
        }
    };

    $scope.accountsToTransfer = [];

    $scope.transferAll = function (account) {
        $scope.isOppModalOpen = !$scope.isOppModalOpen
        $scope.showTeamMembers = !$scope.showTeamMembers
    }

    $scope.selectUser = function (user) {
        $scope.selectedUser = user;
        $scope.transferToThisUser = true;
        $scope.searchUsers = [];
        $scope.teamMate = '';
    }

    $scope.closeModal = function () {
        $scope.transferAll()
    }

    $scope.startTransferring = function () {

        var obj = {
            accounts:$scope.accountsToTransfer,
            transferTo:$scope.selectedUser
        }

        sendEmailNotificationGA($scope,$rootScope,$http,share,"accountTransfer",obj.accounts,$scope.selectedUser)

        $http.post('/account/transfer/ownership/bulk',obj)
            .success(function(response){
                $scope.transferAll();
                toastr.success("Accounts transferred successfully")
                $scope.getAllData();
            });
    }

    $scope.transferThis = function (account) {

        if(account.thisAcc){
            $scope.accountsToTransfer.push(account)
        } else {

            $scope.accountsToTransfer = $scope.accountsToTransfer.filter(function (ac) {
                return ac.fullName != account.fullName
            });
        }

        $scope.selectedAccounts = $scope.accountsToTransfer.length;
    }

    $scope.searchForTeamMate = function (user) {
        $scope.searchUsers = [];
        if(share.teamDictionary){
            for(var key in share.teamDictionary){
                if(user.length>1){
                    if(share.teamDictionary[key].fullName.match(new RegExp(user, 'i')) || share.teamDictionary[key].emailId.match(new RegExp(user, 'i'))){
                        $scope.searchUsers.push(share.teamDictionary[key])
                    }
                }
            }
        }
    }

});

function grantAccess($rootScope,$scope,$http,share,obj){

    $http.post("/account/grant/access/bulk",obj)
        .success(function(response){
            if(response.SuccessCode){
                toastr.success("Access granted successfully")

                sendEmailNotificationGA($scope,$rootScope,$http,share,"grantAccess",obj.accounts)

                getAccounts($scope,$http,share,"/account/access/requested",function (accounts) {
                    $scope.accountsForAccess = accounts;
                });
            } else {
                toastr.error("Access was not granted. Please try again later.")
            }
        });
}

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

function highlightFilterSelection($scope,options){

    _.each($scope.filters,function (el) {

        if(el.filter == options){
            el.class = "selected"
        } else {
            el.class = ""
        }
    });
}

function closeAllDropDownsAndModals($scope,id,isModal) {
    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                $scope.showList = false;
            })
        }
    });
}