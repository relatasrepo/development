/**
 * Created by naveen on 24/8/17.
 */


function drawLineChart($scope,share,series,label,className,series2) {

    var seriesArray = [series];

    if(series && series2){
        seriesArray = [ series, series2]
    }

    new Chartist.Line(className, {
        labels: label,
        series: seriesArray
    }, {
        low: 0,
        showArea: true,
        plugins: [
            tooltip
        ]
    });
}

function summaryChart(data,className,pattern,share){

    var columns = _.map(data,function (el) {

        if(share && share.teamDictionary && share.teamDictionary[el._id]){
            return [share.teamDictionary[el._id].fullName,el.count]
        } else {
            return [el._id,el.count]
        }
    });

    var chart = c3.generate({
        bindto: className,
        data: {
            columns: columns,
            type : 'donut'
        },
        donut: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            },
            width:10
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern
        },
        size: {
            height: 95,
            width:95
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                return {top: top, left: parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))}
            }
        }
    });

}

function wholePie(data){

    // var important = ['Important', 0],
    var dms = ['Decision Maker', 0],
        infls = ['Influencers', 0],
        partners = ['Partners', 0],
        others = ['Others', 0]

    dms = ['Decision Maker', data.decisionMakers]
    infls = ['Influencers', data.influencers]
    partners = ['Partners', data.partners]
    others = ['Others', data.others]
    
    var pattern = ['#fadad0','#d1c0d1','#60a7b8','#c9deeb']
    var chart = c3.generate({
        bindto: data.pieChartId,
        data: {
            columns: [dms,infls,partners,others],
            type : 'pie'
        },
        pie: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            }
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: 175,
            width:175
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                var left = parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))
                return {top: top, left: -125}
            }
        }
    });

}

var tooltip = Chartist.plugins.tooltip();
