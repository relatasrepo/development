relatasApp.controller("interactions_opps_notes", function ($scope, $http, $rootScope,searchService,share,$sce) {
    autoInitGoogleLocationAPI(share);
    $scope.loadAllOpps = function (onlyThree) {
        if(onlyThree){
            $scope.opportunities = $scope.opportunities.slice(0,3);
        } else {
            $scope.opportunities = $scope.opportunitiesAll;

            $scope.opportunities.sort(function (o1, o2) {
                return o1.createdDate < o2.createdDate ? 1 : o1.createdDate > o2.createdDate ? -1 : 0;
            });
        }
    }

    $scope.ifOppClose = function (stage) {
        if(_.includes(["Close Lost","Close Won"], stage)){
            $scope.reasonsRequired = true;
            $scope.mailOptions = true;
        } else {
            $scope.mailOptions = false;
        }
        $scope.registerDatePickerId()
    }

    $scope.registerDatePickerIdRenewal = function(minDate,maxDate){

        $('#opportunityCloseDateSelector4').datetimepicker({
            value:"",
            timepicker:false,
            validateOnBlur:false,
            minDate: minDate,
            maxDate: maxDate,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    timezone = timezone?timezone:"UTC";
                    $scope.renewalCloseDate = moment(dp).tz(timezone).format();
                    $scope.renewalCloseDateFormatted = moment(dp).tz(timezone).format("DD MMM YYYY");
                });
            }
        });

    }
    
    $scope.createNote = function () {
        $scope.isNoteModalOpen = true;
        $scope.showInteractionDetails = false;
    }

    $scope.viewInteraction = function (interaction) {
        setSignature($rootScope,$scope);
        $scope.showInteractionDetails = false;
        if(interaction.cursor != "not-allowed" && interaction.dataObj.interaction.interactionType == "meeting") {
            window.location = "/today/details/"+interaction.refId
        } else {
            if($scope.currentUser.emailId == interaction.owner.emailId){
                $scope.showInteractionDetails = true;
                $scope.item = interaction;
                viewEmail($scope,$http,interaction.dataObj,$sce)
            } else {
                $scope.showInteractionDetails = false;
            }
        }
    }

    $scope.addNoteForContact = function (note) {
        
        createNote($scope,$http,note,$scope.noteContact,function (response) {
            if(response.SuccessCode){
                $scope.notes.push(response.Data)
                $scope.closeModal();
                toastr.success("Successfully created note!");
            }
            else {
                toastr.error("Failed to add note, try again later");
            }
        });
    }

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.goBackToOpp = function () {

        $scope.renewalAmount = $("#renewalAmount").val()

        if($scope.opp.renewThisOpp && $scope.renewalAmount && String($scope.renewalAmount).match(/[a-z]/i)){
            toastr.error("Please enter only numbers for Renewal amount")
            $scope.reasonsRequired = true;
            $scope.mailOptions = true;
        } else if($scope.opp.renewThisOpp && !$scope.renewalAmount){
            toastr.error("Please enter Renewal amount")
            $scope.reasonsRequired = true;
            $scope.mailOptions = true;
        } else if($scope.opp.renewThisOpp && !$scope.renewalCloseDate){
            toastr.error("Renewal close date is required")
            $scope.reasonsRequired = true;
            $scope.mailOptions = true;
        } else if($scope.companyDetails && $scope.companyDetails.closeReasons && $scope.companyDetails.closeReasons.length>0){

            if(!$scope.opp.closeReasons || ($scope.opp.closeReasons && $scope.opp.closeReasons.length === 0)){

                toastr.error("Please select at least one reason")
            } else {
                $scope.reasonsRequired = !$scope.reasonsRequired
            }
        } else {
            $scope.reasonsRequired = !$scope.reasonsRequired
        }
    }

    $scope.addToReasonList = function (reason) {

        if(!$scope.opp.closeReasons || $scope.opp.closeReasons.length == 0){
            $scope.opp.closeReasons = [];
        }

        var list = [];
        list.push(reason)
        $scope.opp.closeReasons = _.uniq(list)
    }

    $scope.ifOppClose = function (stage) {
        if(_.includes(["Close Lost","Close Won"], stage)){
            $scope.reasonsRequired = true;
            $scope.mailOptions = true;
        } else {
            $scope.mailOptions = false;
        }
        $scope.registerDatePickerId()
    }

    $scope.changeSelection = function () {
        $scope.opp.BANT = {
            budget:$scope.budget,
            authority:$scope.authority,
            need:$scope.need,
            time:$scope.time
        }
    }
    
    $scope.registerDatePickerId = function(minDate,maxDate){

        $('#opportunityCloseDateSelector').datetimepicker({
            value:$scope.opp?$scope.opp.closeDateFormatted:"",
            timepicker:false,
            validateOnBlur:false,
            minDate: minDate,
            maxDate: maxDate,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    timezone = timezone?timezone:"UTC";
                    $scope.opp.closeDate = moment(dp).tz(timezone).format();
                    $scope.opp.closeDateFormatted = moment(dp).tz(timezone).format("DD MMM YYYY");
                });
            }
        });
    }

    $scope.removeRecipient = function(contact,type){
        removeRecipient($scope,$http,contact,type)
    }

    $scope.saveOpportunity =function () {
        $scope.town = share.getTown();

        if(!$scope.town){
            if($scope.opp && $scope.opp.geoLocation && $scope.opp.geoLocation.town){
                $scope.town = $scope.opp.geoLocation.town
            }
        }

        if($scope.opp && $scope.opp._id){
            saveExistingOpportunity($scope,$http,share,$rootScope);
        } else {

            if(!$scope.opp){
                $scope.opp = {}
            }
            
            $scope.renewalAmount = $("#renewalAmount").val()

            addNewOpportunity($scope,$http,share,$rootScope)
        }
    }

    $scope.selectContact = function(contact){

        $scope.contact = contact.fullName + " ("+contact.emailId+")";
        $scope.showResultscontact = false;

        $("#oppContact").val($scope.contact)
        $("#noteContact").val($scope.contact)

        if(!$scope.opp){
            $scope.opp = {}
        }

        $scope.noteContact = contact.emailId;
        $scope.opp.contactEmailId = contact.emailId;
        $scope.opp.mobileNumber = contact.mobileNumber;
    }

    getOppStages();

    function getOppStages(){

        if(share.opportunityStages){
            $scope.stages = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(1000,function () {
                getOppStages()
            })
        }
    }

    $scope.closeModal = function () {
        $scope.isOppModalOpen = false;
        $scope.isNoteModalOpen = false;
        $scope.showInteractionDetails = false;
    }

    share.closeModal = function () {
        $scope.closeModal()
    }

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.isOppModalOpen = false;
    $scope.editOpportunity = function (opp) {
        getLog($scope,$http,opp);
        autoInitGoogleLocationAPI(share);
        $scope.createdDateFormatted = opp.createdDate ? moment(opp.createdDate).format("DD MMM YYYY") : null
        $scope.renewalAmount = 0;
        $scope.renewalCloseDate = "";
        
        if($rootScope.currentUser.emailId == opp.owner.emailId || $scope.account.hasAccess){
            $scope.contact = opp.contactEmailId;
            $scope.isOppModalOpen = true;
            $scope.opp = opp;
        }
    }

    $scope.loadSourceOpp = function (opp) {

        var sourceOpp = {};

        _.each($scope.opportunitiesAll,function (el) {
            if(opp.sourceOpportunityId == el.opportunityId){
                sourceOpp = el;
            }
        });

        $scope.editOpportunity(sourceOpp)
    }

    share.createOpp = function () {
        $scope.opp = {};
        autoInitGoogleLocationAPI(share)
        $scope.isOppModalOpen = true;
    }

    $scope.createOpp = function () {
        $scope.opp = {};
        autoInitGoogleLocationAPI(share)
        $scope.isOppModalOpen = true;
    }

    $scope.loadAllUpcomingMeetings = function (onlyThree) {
        if(onlyThree){
            $scope.futureMeetings = $scope.futureMeetings.slice(0,3);
        } else {
            $scope.futureMeetings = $scope.meetingsAll;
        }
    }
    
    $scope.loadAllInteractions = function () {
        share.showAllInteractions($scope.account,$scope.userIds)
        $("html, body").animate({ scrollTop: 10 }, "slow");
    }

    share.interactionsOppsAndNotes = function(account,userIds){
        $scope.account = account;
        $scope.userIds = userIds;

        $scope.showingAllOpps = false;
        $scope.showingAllMeetings = false;
        
        var url = "/account/get/interactions/opps/notes?account="+account.fullName;

        if(userIds){
            url = fetchUrlWithParameter(url,"userIdList",userIds);
        }

        url = fetchUrlWithParameter(url, "contacts", account.contacts);

        $http.get(url)
            .success(function (response) {

                $scope.pastInteractions = [];
                $scope.futureMeetings = [];
                $scope.opportunities = [];
                $scope.meetingsAll = [];
                $scope.notes = [];
                $scope.meetingsLen = 0,$scope.oppsLen = 0,$scope.interactionsLen = 0;
                if(response && response.SuccessCode){

                    if(response.Data.interactions.futureMeetings && response.Data.interactions.futureMeetings.length>0){

                        $scope.meetingsLen = response.Data.interactions.futureMeetings.length;

                        _.each(response.Data.interactions.futureMeetings,function (interaction,index) {
                            var meeting = buildInteractionObjectForTimeline(interaction,null,interaction.firstName,$scope.rName,"UTC",index)

                            meeting["owner"] = share.teamDictionary[interaction.ownerEmailId]
                            $scope.futureMeetings.push(meeting);
                            $scope.meetingsAll.push(meeting);
                        });

                        $scope.futureMeetings = $scope.futureMeetings.slice(0,3);
                    }

                    if(response.Data.interactions.pastInteractions && response.Data.interactions.pastInteractions.length>0){

                        $scope.interactionsLen = response.Data.interactions.pastInteractions.length;

                        _.each(response.Data.interactions.pastInteractions,function (interaction,index) {
                            var interactionObj = buildInteractionObjectForTimeline(interaction,null,interaction.firstName,$scope.rName,"UTC",index)
                            interactionObj["owner"] = share.teamDictionary[interaction.ownerEmailId]

                            if($rootScope.currentUser && interactionObj.owner && $rootScope.currentUser.emailId == interactionObj.owner.emailId){
                                interactionObj["cursor"] = "cursor"
                                interactionObj["noAccessClass"] = "access-hover"
                            } else {
                                interactionObj["cursor"] = "not-allowed"
                                interactionObj["noAccessClass"] = "no-access-class"
                                interactionObj["title"] = "You don't have access to this interaction. Access available only to the interaction owner."
                            }

                            $scope.pastInteractions.push(interactionObj)
                        });

                        $scope.pastInteractions.sort(function (o1, o2) {
                            return new Date(o1.interactionDate) < new Date(o2.interactionDate) ? 1 : new Date(o1.interactionDate) > new Date(o2.interactionDate) ? -1 : 0;
                        });

                        $scope.pastInteractions = $scope.pastInteractions.slice(0,3)
                    }

                    if(response.Data.opps && response.Data.opps.length>0){
                        $scope.oppsLen = response.Data.opps.length;
                        opportunitiesList($scope,response.Data.opps,share,$rootScope)
                    } else {
                        opportunitiesList($scope,[],share,$rootScope)
                    }

                    if(response.Data.notes && response.Data.notes.length>0){
                        notesTimeline($scope,response.Data.notes,share)
                    }
                }
            })
    }
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

function notesTimeline($scope,notes,share) {
    $scope.notes = notes
    $scope.notes.forEach(function(a) {
        a["owner"] = share.teamDictionary[a.messageOwner.emailId]
        a.text = a.text.replace(/<br ?\/?>/g, "\n")
        a.dateFormatted = a.date ? moment(a.date).format("DD MMM YYYY") : null;
    });

    $scope.notes.sort(function (o1, o2) {
        return o1.date < o2.date ? 1 : o1.date > o2.date ? -1 : 0;
    });
}

function opportunitiesList($scope,opportunities,share,$rootScope) {

    $scope.opportunities = opportunities;
    $scope.opportunitiesAll = [];

    share.opportunitiesDictionary = {}
    var daysToWinCloseAllDeals = 0;
    var dealsWon = 0;
    var OppsWon = [];
    $rootScope.daysToWinCloseAllDeals = "-"

    var totalAccOpps = $scope.opportunities.length;

    if(opportunities && opportunities.length>0){


        var opportunityStages = {};

        if(share.opportunityStages){
            _.each(share.opportunityStages,function (op) {
                opportunityStages[op.name] = op.order;
            })
        }

        $scope.opportunities.forEach(function(a){

            var createDt = moment(a.createdDate);
            var closeDt = moment(a.closeDate);
            var daysToWinCloseDeal= 0;

            if(closeDt.diff(createDt, 'days') === 0){
                daysToWinCloseDeal = daysToWinCloseDeal+1 //Minimum one day taken to close opp
            } else {
                daysToWinCloseDeal = daysToWinCloseDeal+Math.abs(closeDt.diff(createDt, 'days'));
            }

            if(_.includes(['Close Won','Closed Won'],a.relatasStage)){
                dealsWon++;
                daysToWinCloseAllDeals = daysToWinCloseAllDeals+daysToWinCloseDeal;
                OppsWon.push(a)
            }

            a["cursor"] = "not-allowed"
            a["title"] = "You don't have access to this opportunity. Access available only to the opportunity owner."
            if($rootScope.currentUser.emailId == a.userEmailId || $scope.account.hasAccess){
                a["cursor"] = "cursor";
                a["title"] = ""
            }

            a.isNotOwner = false;

            if($rootScope.currentUser.emailId != a.userEmailId){
                a.isNotOwner = true
            }

            a["owner"] = share.teamDictionary[a.userEmailId]

            a.closeDateFormatted = a.closeDate ? moment(a.closeDate).format("DD MMM YYYY") : null;
            a.wonOrLost = a.isClosed ?  (a.isWon ? 'Won' : 'Lost') : null;
            a.stageStyle = a.relatasStage;
            a.stageStyle2 = oppStageStyle(a.relatasStage,opportunityStages[a.relatasStage]-1,true);
            a.stage = a.relatasStage;
            a.isStale = a.closeDate && a.relatasStage !="Closed Lost" && a.relatasStage !="Closed Won" && a.relatasStage !="Close Lost" && a.relatasStage !="Close Won" && new Date(a.closeDate)<new Date()

            $scope.opportunitiesAll.push(a)

        });

        if(dealsWon>0){
            $rootScope.daysToWinCloseAllDeals = (daysToWinCloseAllDeals/dealsWon).r_formatNumber(2)
        }

        share.fetchInteractionsForContacts(OppsWon)
        share.accountOppSnapshot($scope.opportunities,totalAccOpps)

        $scope.opportunities = $scope.opportunities.slice(0,3);

        $scope.opportunities.sort(function (o1, o2) {
            return o1.createdDate < o2.createdDate ? 1 : o1.createdDate > o2.createdDate ? -1 : 0;
        });
    } else {
        share.accountOppSnapshot([],0)
        share.fetchInteractionsForContacts([])
    }

}

function searchResults($scope,$http,keywords,share,searchService,type) {

    var selector = "showResults"+type;
    var typeSelector = type+"s"

    if(keywords && keywords.length > 2){

        searchService.search(keywords).success(function(response){

            if(response.SuccessCode){
                $scope[selector] = true;
                processSearchResults($scope,$http,response.Data,type);
            } else {
                $scope[selector] = false;
                $scope[typeSelector] = [];

                var obj = {
                    fullName: '',
                    name: '',
                    image: '/getContactImage/' + null + '/' + null,
                    emailId:keywords
                }

                $scope[typeSelector].push(obj)

            }
        }).error(function(){
            console.log('error');
        });
    } else {
        $scope[selector] = false;
        $scope[typeSelector] = [];
    }
}

function processSearchResults($scope,$http,response,type) {

    var typeSelector = type+"s";
    var contactsArray = response;
    $scope[typeSelector] = [];

    if(contactsArray.length>0){
        for(var i=0;i<contactsArray.length;i++){

            var obj = {};

            if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                var name = getTextLength(contactsArray[i].personName,20);
                var image = '/getImage/'+contactsArray[i].personId._id;

                obj = {
                    fullName:contactsArray[i].personName,
                    name:name,
                    image:image
                };

                obj.emailId = contactsArray[i].personEmailId;
                obj.twitterUserName = contactsArray[i].twitterUserName;
                obj.mobileNumber = contactsArray[i].mobileNumber;

            }
            else {
                var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                obj = {
                    fullName: contactsArray[i].personName,
                    name: getTextLength(contactsArray[i].personName, 20),
                    image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                    emailId:contactsArray[i].personEmailId,
                    twitterUserName: contactsArray[i].twitterUserName,
                    mobileNumber: contactsArray[i].mobileNumber
                    // noPicFlag:true
                };
            }

            if(obj.twitterUserName){
                obj.tweetAccExists = true;
            }

            obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
            obj._id = contactsArray[i]._id;
            obj.personId = contactsArray[i].personId && contactsArray[i].personId._id?contactsArray[i].personId._id:null;
            obj.type = type

            if(!userExists(obj.name) && validateEmail(obj.emailId)){
                $scope[typeSelector].push(obj)
            }

            function userExists(username) {
                return $scope[typeSelector].some(function(el) {
                    return el.name === username;
                });
            }
        }
    }
}

function saveExistingOpportunity($scope,$http,share,$rootScope){

    var checkStr = String($scope.opp.amount)
    var checkStrNGM = String($scope.opp.netGrossMargin)

    if(!$scope.opp.opportunityName){
        toastr.error("Please the opportunity name")
    } else if(!$scope.opp.closeDateFormatted){
        toastr.error("Please select the closing date")
    } else if(!$scope.opp.amount){
        toastr.error("Please enter the value of this opportunity")
    } else if(!$scope.opp.stage){
        toastr.error("Please select the current stage")
    } else if($scope.opp.amount && checkStr.match(/[a-z]/i)){
        toastr.error("Please enter only numbers for opportunity amount")
    } else if($scope.opp.netGrossMargin && !isNumber($scope.opp.netGrossMargin) && checkStrNGM.match(/[a-z]/i)){
        toastr.error("Please enter only numbers for Net Gross Margin")
    } else if($rootScope.netGrossMargin && !$scope.opp.netGrossMargin){
        toastr.error("Net Gross Margin setting is mandatory")
    } else if($scope.opp.netGrossMargin && ($scope.opp.netGrossMargin>100 || $scope.opp.netGrossMargin<0 || $scope.opp.netGrossMargin == 0)){
        toastr.error("Net Gross Margin must be between 1 - 100")
    } else if((_.includes($scope.opp.stage, 'Won') || _.includes($scope.opp.stage, 'Lost')) && (!checkRequired($scope.opp.closeReasons) || (checkRequired($scope.opp.closeReasons) && $scope.opp.closeReasons.length ==0 ))){
        toastr.error("Please select reasons for the deal closure");
        $scope.reasonsRequired = true;
    } else if($scope.opp.renewThisOpp && !$scope.renewalAmount){
        toastr.error("Renewal amount is required")
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    } else if($scope.opp.renewThisOpp && !$scope.renewalCloseDate){
        toastr.error("Renewal close date is required")
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    } else {

        if(_.includes($scope.opp.stage, 'Won')){
            $scope.opp.isClosed = true;
            $scope.opp.isWon = true;

            if(new Date($scope.opp.closeDate)>new Date()){
                $scope.opp.closeDate = new Date();
            }
        }

        if(_.includes($scope.opp.stage, 'Lost')){
            $scope.opp.isClosed = true;
            $scope.opp.isWon = false;
            if(new Date($scope.opp.closeDate)>new Date()){
                $scope.opp.closeDate = new Date();
            }
        }

        $scope.town = $scope.town?$scope.town.replace(/[^a-zA-Z ]/g, ""):$scope.town
        $scope.opp.name = $scope.opp.opportunityName;
        var daysToClose = moment.duration(moment(new Date()).diff(moment($scope.opp.createdDate)));

        $http.post("/salesforce/edit/opportunity/for/contact", {
                opportunity: $scope.opp,
                contactEmailId: $scope.opp.contactEmailId,
                contactMobile: $scope.opp.mobileNumber,
                town:$scope.town,
                zone:$scope.opp.geoLocation?$scope.opp.geoLocation.zone:null,
                mailOrgHead:$scope.mailOrgHead,
                mailRm:$scope.mailRm,
                companyId:$rootScope.companyDetails._id,
                hierarchyParent:$rootScope.currentUser.hierarchyParent,
                renewalCloseDate:$scope.renewalCloseDate,
                renewalAmount:$scope.renewalAmount
            })
            .success(function (response) {
                if(response.SuccessCode){

                    oppTargetMetNotifications($http,$scope);
                    
                    var userIds = _.map(share.teamArray,"userId");
                    share.getAccessControl($scope.account,userIds);
                    toastr.success("Opportunity added successfully");
                    
                    $scope.renewalAmount = 0;
                    $scope.renewalCloseDate = "";

                    $scope.closeModal()
                    
                    if(response.companyHead && response.reportingManager){
                        if($scope.mailOrgHead || $scope.mailRm){

                            var subject = "Deal worth "+$scope.opp.amount+ " successfully closed."
                            if($scope.opp.stage == "Close Lost"){
                                subject = "Deal worth "+$scope.opp.amount+ " Lost."
                            }

                            var intro = "Hi "+response.companyHead.firstName +" & "+response.reportingManager.firstName

                            if(response.companyHead.emailId == response.reportingManager.emailId){
                                intro = "Hi "+response.companyHead.firstName
                            }

                            if(!checkRequired($scope.opp.closeReasonDescription)){
                                $scope.opp.closeReasonDescription = "No Comments"
                            }

                            var oppDetails = "\n\nOpportunity Name :"+$scope.opp.$scope.opp.opportunityName+
                                "\nDeal Size :"+$scope.opp.amount+
                                "\nClose date :"+moment($scope.opp.closeDate).tz(timezone).format("DD MMM YYYY")+
                                "\nDays to Closure :"+Math.ceil(daysToClose.asDays())+
                                "\nStage :"+$scope.opp.stage+
                                "\nContact :"+$scope.opp.contactEmailId+
                                "\nReason: "+$scope.opp.closeReasons.join(',')+
                                "\nReason details: "+$scope.opp.closeReasonDescription
                                // "\nView Opportunity: "+"http://"+$rootScope.companyDetails.url+"/opportunities/all"

                                +'\n\n\n'+getSignature($scope.liu.firstName+' '+$scope.liu.lastName,$scope.liu.designation,$scope.liu.companyName,$scope.liu.publicProfileUrl)
                                // +'\n\n Powered by Relatas';

                            var body = intro+",\n\n"+"Deal with "+$scope.opp.contactEmailId+" has closed."+oppDetails;

                            $scope.add_cc = [response.companyHead.emailId];

                        }
                    }

                } else {
                    toastr.error("Opportunity save failed. Please try again later")
                }
            });
    }
}

function autoInitGoogleLocationAPI(share){

    window.initAutocomplete = initAutocomplete;

    initAutocomplete();

    function initAutocomplete() {

        if(document.getElementById('autocompleteCity')){

            var autocomplete2 = new google.maps.places.Autocomplete(
                (document.getElementById('autocompleteCity')),
                {types:['(cities)']});

            autocomplete2.addListener('place_changed', function(){
                share.setTown($("#autocompleteCity").val())
                return false;
            });
        }
    }
}

function addRecipient($scope,$http,contact,type){

    var opportunityId = $scope.opp?$scope.opp._id:null;
    var selector = "showResults"+type;
    var typeSelector = type+"s";

    $scope.partner = '';
    var hashtag = type;
    var relation = "decision_maker"
    if(type === 'partners'){
        hashtag = "partner"
    } else if(type === 'decisionMakers'){
        hashtag = "decisionMaker"
    } else if(type === 'influencers'){
        hashtag = "influencers"
        relation = "influencer"
    }

    updateRelationship($http,contact,relation,"decisionmaker_influencer");

    if(validateEmail(contact.emailId) && opportunityId){
        $http.post('/opportunities/add/people',{type:typeSelector,contact:contact,opportunityId:opportunityId,opportunityId2:$scope.opp.opportunityId})
            .success(function (response) {
                if(response.SuccessCode){
                    $scope.opp[typeSelector].push(contact)
                    $scope[selector] = false;
                } else {

                    $scope[selector] = false;
                }
            });
    } else {

        $scope[typeSelector] = [];

        if(!$scope.opp){
            $scope.opp = {}
        }

        $scope.opp[typeSelector] = [];
        $scope.opp[typeSelector].push(contact)

        $scope[selector] = false;
    }
}

function updateRelationship($http,contact,relation,relationKey,relationType){

    if(contact.contactId){
        var id = contact.contactId
    } else {
        id = contact._id
    }

    var reqObj = {contactId:id,type:relation,relationKey:relationKey,relation:relationType};

    $http.post('/opportunities/contacts/update/reltionship/type',reqObj)
        .success(function(response){

        });
}

function addNewOpportunity($scope,$http,share,$rootScope){

    var checkStr = String($scope.opp.amount);
    var checkStrNGM = String($scope.opp.netGrossMargin)

    if(!$scope.opp.opportunityName){
        toastr.error("Please enter the opportunity name")
    } else if(!$scope.opp.closeDateFormatted){
        toastr.error("Please select the closing date")
    } else if(!$scope.opp.amount){
        toastr.error("Please enter the opportunity value")
    } else if(!$scope.opp.stage){
        toastr.error("Please select the current stage")
    } else if($scope.opp.amount && checkStr.match(/[a-z]/i)){
        toastr.error("Please enter only numbers for opportunity amount")
    } else if($rootScope.isRegionRequired && (!$scope.opp.geoLocation || !$scope.opp.geoLocation.zone || !$scope.opp.geoLocation.town)){
        toastr.error("Region selection is mandatory")
    } else if($rootScope.isProductRequired && !$scope.opp.productType){
        toastr.error("Product selection is mandatory")
    } else if($rootScope.isVerticalRequired && !$scope.opp.vertical){
        toastr.error("Vertical selection is mandatory")
    } else if(!$scope.opp.contactEmailId){
        toastr.error("Please assign a contact")
    } else if($scope.opp.netGrossMargin && !isNumber($scope.opp.netGrossMargin) && checkStrNGM.match(/[a-z]/i)){
        toastr.error("Please enter only numbers for Net Gross Margin")
    } else if($rootScope.netGrossMargin && !$scope.opp.netGrossMargin){
        toastr.error("Net Gross Margin setting is mandatory")
    } else if($scope.opp.netGrossMargin && ($scope.opp.netGrossMargin>100 || $scope.opp.netGrossMargin<0)){
        toastr.error("Net Gross Margin must be between 1 - 100")
    } else if($scope.opp.renewThisOpp && !$scope.renewalAmount){
        toastr.error("Renewal amount is required")
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    } else if($scope.opp.renewThisOpp && !$scope.renewalCloseDate){
        toastr.error("Renewal close date is required")
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    } else {

        $scope.town = $scope.town?$scope.town.replace(/[^a-zA-Z ]/g, ""):$scope.town
        $scope.opp.name = $scope.opp.opportunityName;

        var data = {
            opportunity: $scope.opp,
            contactEmailId: $scope.opp.contactEmailId,
            contactMobile: $scope.opp.mobileNumber?$scope.opp.mobileNumber:null,
            town:$scope.town,
            zone:$scope.opp.geoLocation?$scope.opp.geoLocation.zone:null,
            renewalCloseDate:$scope.renewalCloseDate,
            renewalAmount:$scope.renewalAmount
        }

        createOpportunity($http,$scope,data,function (response) {
            if(response.SuccessCode){

                var userIds = _.map(share.teamArray,"userId");
                share.getAccessControl($scope.account,userIds);
                toastr.success("Opportunity added successfully");
                $scope.closeModal();
            } else {
                toastr.error("Opportunity not created. Please try again later")
            }
        });
    }

}

function removeRecipient($scope,$http,contact,type){

    var opportunityId = $scope.opp._id
    var selector = "showResults"+type;
    var typeSelector = type+"s";

    var hashtag = type;
    var relation = "decision_maker"

    if(type === 'partners'){
        hashtag = "partner"
    } else if(type === 'decisionMakers'){
        hashtag = "decisionMaker"
    } else if(type === 'influencers'){
        hashtag = "influencers"
        relation = "influencer"
    }

    $http.post('/opportunities/remove/people',{type:type,contact:contact,opportunityId:opportunityId,opportunityId2:$scope.opp.opportunityId})
        .success(function (response) {
            if(response.SuccessCode){
                updateRelationship($http,contact,null,"decisionmaker_influencer",relation)
                var rmIndex = $scope.opp[typeSelector].indexOf(contact);
                $scope.opp[typeSelector].splice(rmIndex, 1);
            }
        });
}