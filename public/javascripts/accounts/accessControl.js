/**
 * Created by naveen on 24/8/17.
 */

relatasApp.controller("access_control", function ($scope, $http, $rootScope, share) {

    share.loadAccessRequests = function () {
        // getAccounts($scope,$http,share,"/account/access/requested",function (accounts) {
        //     console.log(accounts)
        //     if(accounts && accounts.length>0){
        //         $scope.accountAccessRequired = true;
        //         $scope.numberOfAccounts = accounts.length +" "+"accounts";
        //     }
        // });
    }

    $scope.goToAccessSettings = function () {
        window.location = "/accounts/access/settings"
    }

    $scope.requestedForAccess = false;
    $scope.account_settings = function () {
        mixpanelTracker("Accounts>settings ");
        share.account_settings($scope.account,$scope.usersWithAccess)
    }

    share.accountAccessRequestedStatus = function (user) {

        if($scope.account.accountAccessRequested && $scope.account.accountAccessRequested.length){

            $scope.account.accountAccessRequested = $scope.account.accountAccessRequested.filter(function (el) {
                return el.emailId != user.emailId
            })
        }
    }

    $scope.requestAccess = function () {

        $http.get("/account/request/access?account="+$scope.account.fullName)
            .success(function (response) {
                if(response.SuccessCode){
                    $scope.requestedForAccess = true;
                    $scope.requestedDate = moment().format(standardDateFormat());

                    sendEmail($scope,$rootScope,$http,share,"accountAccessRequired")
                }
            });
    }

    share.getAccessControl = function (account,userIds){

        $scope.noAccess = true;
        $scope.ifLiuIsAccountOwner = false;
        // account.fullName = "relatas";

        var accountName = account.fullName;
        $rootScope.account_name = account.fullName.capitalizeFirstLetter();

        account.accessRequired = [];

        $scope.account = account;
        $scope.accountWebsites = [];
        var accountWebsites = [];

        var totalContacts = account.contacts.length;
        _.each(account.contacts,function (url,index) {

            if(url.indexOf("@")>0){
                var link = "http://"+url.substr(url.indexOf("@") + 1);
                if(!_.includes(accountWebsites,link)){
                    accountWebsites.push(link)
                }
            }
        });

        var websiteToShow = null

        if(accountWebsites.length>1){
            websiteToShow = getWebsiteByDomainTypeOrder(accountWebsites)
        } else {
            if(accountWebsites[0]){
                websiteToShow = accountWebsites[0]
            }
        }

        $scope.accountWebsites.push(websiteToShow)

        $scope.importantSelection = {"border":"1px solid #ccc"}
        $scope.partnerSelection = {"border":"1px solid #ccc"}
        if($scope.account.important){
            $scope.importantSelection = {'background':'#f86b4f',"color": '#fff',"border-color":"#f86b4f"}
        }

        if($scope.account.partner){
            $scope.partnerSelection = {'background':'#f86b4f',"color": '#fff',"border-color":"#f86b4f"}
        }

        if(!account.isOwner){
            $scope.importantSelection["cursor"] = "not-allowed"
            $scope.partnerSelection["cursor"] = "not-allowed"
        }

        if(account.accountAccessRequested && account.accountAccessRequested.length>0){
            _.each(account.accountAccessRequested,function (el) {
                $scope.requestedForAccess = false;
                if(el.emailId == $rootScope.currentUser.emailId){
                    $scope.requestedForAccess = true;
                    $scope.requestedDate = moment(el.date).format("DD MMM YYYY")
                }

                var obj = share.teamDictionary[el.emailId]

                obj.dateFormatted = moment(el.date).format("DD MMM YYYY")

                if(el.emailId){
                    $scope.account.accessRequired.push(obj);
                }
            })
        } else {
            $scope.requestedForAccess = false;
            $scope.requestedDate = ""
        }

        $scope.usersWithAccess = [];
        $scope.companyName = account.fullName;
        $scope.companyLogo = "https://logo.clearbit.com/"+$scope.companyName+".com";
        $scope.companyNoName = ($scope.companyName.substr(0,2)).capitalizeFirstLetter();

        if(!imageExists($scope.companyLogo)){
            $scope.displayLogo = true;
        }

        if(account.usersWithAccess && account.usersWithAccess.length>0){
            _.each(account.usersWithAccess,function (emailId) {

                var obj = share.teamDictionary[emailId];

                if(obj){
                    obj.checked = true;
                    if(account.usersWithAccess.length == 1){
                        obj.disabled = true;
                    }

                    $scope.usersWithAccess.push(obj)
                }
            })
        }

        if($scope.usersWithAccess.length>0 || ($scope.account.accountAccessRequested.length>0 && $scope.account.isOwner)){
            $scope.showSharedAccess = true;
        } else {
            $scope.showSharedAccess = false;
        }

        function checkLiuAccess() {

            if($rootScope.currentUser){

                if(account.ownerEmailId == $rootScope.currentUser.emailId || _.includes(account.hierarchyAccess,$rootScope.currentUser.id)){
                    $scope.ifLiuIsAccountOwner = true;
                }

                var tz = share.liuData.timezone && share.liuData.timezone.name?share.liuData.timezone.name:"UTC";

                if(_.includes(account.usersWithAccess,$rootScope.currentUser.emailId) || account.hasAccess || account.ownerEmailId == $rootScope.currentUser.emailId){
                    $scope.noAccess = false;
                }
            }else {
                setTimeOutCallback(1000,function () {
                    checkLiuAccess()
                })
            }
        }

        checkLiuAccess();

        if(share.teamDictionary[account.ownerEmailId]){
            $scope.account_owner = share.teamDictionary[account.ownerEmailId].fullName;
        }
    }

});

function sendEmail($scope,$rootScope,$http,share,mailType,toEmailId){

    var corporateAdmin = null;
    // _.each(share.teamArray,function (el) {
    //     if(el.corporateAdmin){
    //         corporateAdmin = el
    //     }
    // });

    //Don't send email to corporateAdmin.

    var contactDetails = {
        contactEmailId: $scope.account.ownerEmailId,
        personId:null,
        personName:null,
        cc:corporateAdmin && $scope.account.ownerEmailId != corporateAdmin.emailId?[corporateAdmin.emailId]:null
    }

    var subject = "Required: Access to Account(s) - "+$scope.account.fullName.toUpperCase() + " on Relatas";
    var body = "Hi "+share.teamDictionary[$scope.account.ownerEmailId].fullName+",\n\n";

    body = body+"Please grant me access to Account(s) "+$scope.account.fullName.toUpperCase() +".\n\n";

    if($rootScope.companyDetails && $rootScope.companyDetails.url) {
        body = "\n\n"+body+"----------------------------------------------"+"\n\n"
        body= body + "To grant access, go to - "+"https://"+$rootScope.companyDetails.url+"/accounts/access/settings"+"\n\n"
    }

    if(mailType == "grantAccess" && toEmailId){
        contactDetails.contactEmailId = toEmailId;
        subject = "Granted: Access to Account(s) - "+$scope.account.fullName.toUpperCase()+" on Relatas";
        body = "Hi "+share.teamDictionary[toEmailId].fullName+",\n\n";
        body = body+"You have been given access to the following account(s) "+$scope.account.fullName.toUpperCase()+" on Relatas. Please login at "+"https://"+$rootScope.companyDetails.url+" to view the details.\n\n";
    }

    body = body+'\n'+getSignature($rootScope.currentUser.firstName+' '+$rootScope.currentUser.lastName,$rootScope.currentUser.designation,$rootScope.currentUser.companyName,$rootScope.currentUser.publicProfileUrl)

    notifyRelevantPeople($http,subject,body,contactDetails)
}