relatasApp.controller('hierarchy', function($scope,$http,$rootScope,share) {

    $scope.resetReportee = function () {
        $scope.item = {}
    }

    $scope.editContactDetails = function (contact) {
        contact.editContact = true;
    }

    $scope.saveContactDetails = function (contact) {

        contact.name = $scope.account.name.toLowerCase();

        $http.post('/account/admin/contact/update',contact)
            .success(function (response) {
                contact.editContact = false;
                share.accountHierarchy(response)
            })
    }

    $scope.setupRelationAndTypes = function(relation,type){

        $scope.dmCss = ""
        $scope.influencerCss = ""
        $scope.detractorCss = ""
        $scope.friendlyCss = ""
        $scope.neutralCss = ""

        if(relation.toLowerCase() == "decision-maker") {
            $scope.dmCss = "decision-maker";
        }

        if(relation.toLowerCase() == "influencer") {
            $scope.influencerCss = "influencer";
        }

        if(type.toLowerCase() == "detractor") {
            $scope.detractorCss = "detractor";
        }

        if(type.toLowerCase() == "friendly") {
            $scope.friendlyCss = "friendly";
        }

        if(type.toLowerCase() == "neutral") {
            $scope.neutralCss = "neutral";
        }
    }

    $scope.setRelationshipType = function(relation) {
        var toUpdate = ""
        if(relation.toLowerCase() == "decision maker") {
            $scope.dmCss = $scope.dmCss == "decision-maker"?"":"decision-maker";
            $scope.influencerCss = ""
            toUpdate = $scope.dmCss;
        }

        if(relation.toLowerCase() == "influencer") {
            $scope.influencerCss = $scope.influencerCss == "influencer"?"":"influencer";
            $scope.dmCss = ""
            toUpdate = $scope.influencerCss;
        }

        var obj ={
            name:$scope.account.name.toLowerCase(),
            emailId:$scope.user.emailId,
            relationshipType:toUpdate.toLowerCase()
        }

        $http.post('/account/admin/relationship/update',obj)
            .success(function (response) {
                share.accountHierarchy(response)
            })
    }

    $scope.setType = function(type) {
        var toUpdate = ""

        if(type.toLowerCase() == "detractor") {
            $scope.detractorCss = $scope.detractorCss == "detractor"?"":"detractor";
            $scope.friendlyCss = ""
            $scope.neutralCss = ""
            toUpdate = $scope.detractorCss;
        }

        if(type.toLowerCase() == "friendly") {
            $scope.friendlyCss = $scope.friendlyCss == "friendly"?"":"friendly";
            $scope.detractorCss = ""
            $scope.neutralCss = ""
            toUpdate = $scope.friendlyCss;
        }

        if(type.toLowerCase() == "neutral") {
            $scope.neutralCss = $scope.neutralCss == "neutral"?"":"neutral";
            $scope.detractorCss = ""
            $scope.friendlyCss = ""
            toUpdate = $scope.neutralCss;
        }

        var obj ={
            name:$scope.account.name.toLowerCase(),
            emailId:$scope.user.emailId,
            type:toUpdate.toLowerCase()
        }

        $http.post('/account/admin/hierarchy/type/update',obj)
            .success(function (response) {
                share.accountHierarchy(response)
            })
    }

    share.accountHierarchy = function (account) {

        share.hierarchyObj = {};
        $scope.account = account;
        $scope.existingHierarchy = [];

        $scope.existingHierarchy = account.hierarchy?account.hierarchy.filter(function (el) {
            if(el.hierarchyParent || el.orgHead){
                return el;
            }
        }):[];

        $scope.allContacts = [];
        share.allIdsInTree = [];

        _.each(account.contacts,function (el) {

            var id = el.split("@")
            var newId = "";
            _.each(id,function (el) {
                newId = newId+el
            });

            share.allIdsInTree.push(newId)

            $scope.allContacts.push({
                noImage:1,
                fullName:el.fullName?el.fullName:el,
                designation:"",
                emailId:el,
                id: newId,
                _id: newId
            })

            share.hierarchyObj[newId] = {
                noImage:1,
                fullName:el.fullName?el.fullName:el.emailId,
                designation:"",
                emailId:el,
                id: newId,
                _id: newId
            }
        })

        if(account.hierarchy && account.hierarchy.length>0){
            _.each(account.hierarchy,function (el) {
                var id = el.emailId.split("@")
                var newId = "";
                _.each(id,function (str) {
                    newId = newId+str
                });

                share.hierarchyObj[newId] = {
                    noImage:1,
                    fullName:el.fullName?el.fullName:el.emailId,
                    designation:el.designation,
                    emailId:el.emailId,
                    relationshipType:el.relationshipType,
                    type:el.type,
                    id: newId,
                    _id: newId,
                    orgHead:el.orgHead
                }
            })
        }

        _.each($scope.existingHierarchy,function (el) {
            var id = el.emailId.split("@")
            var newId = "";
            _.each(id,function (el) {
                newId = newId+el
            });

            el.id = newId
            el._id = newId
        })


        share.hierarchyObjByEmailId = {};

        for(var key in share.hierarchyObj){
            share.hierarchyObjByEmailId[share.hierarchyObj[key].emailId] = share.hierarchyObj[key]
        }

        $scope.unassigned = _.differenceBy($scope.allContacts,$scope.existingHierarchy,"emailId");
        kickOffHierarchyPaint();

    }

    function kickOffHierarchyPaint() {
        $('#chart-container').empty();
        $scope.hierarchyExists = {};
        $scope.hierarchyExists.settings = false;

        if($scope.existingHierarchy.length>0) {
            var hierarchy = convertArrayToTree($scope.existingHierarchy);
            if(hierarchy){
                $scope.hierarchyExists.settings = true;
                chartDraw($scope,$http,share,hierarchy);
            }
        }
    }

    $('#unassignedList').on('click', function() {
        $(".unassigned-list").toggle(10)
    });

    $scope.closeModal = function () {
        $scope.editUserData = false;
    }

    $scope.removeUser = function (user) {

        var toRemoveNames = user.fullName?user.fullName:user.emailId;

        if (window.confirm('This will remove '+toRemoveNames+" and the corresponding reportees from the hierarchy")) {

            $http.post('/account/admin/users/remove',{
                hierarchyParent:user.id,
                emailId:user.emailId,
                name:$scope.account.name.toLowerCase(),
                orgHead:user.orgHead?false:user.orgHead,
                orgHeadRm:user.orgHead?true:false})
                .success(function (response) {
                    $scope.user = {}
                    $scope.editUserData = false;
                    share.accountHierarchy(response)
                });
        }
    }

    $scope.addUser = function (child) {
        $scope.item = child;
        getFullPathToParent(share.oc,$scope.user._id,"",function (path) {
            var hierarchyParent = $scope.user._id;
            var hierarchyPath = path+","+$scope.user._id;

            var updateObj = {
                hierarchyParent:hierarchyParent,
                hierarchyPath:hierarchyPath,
                emailId:child.emailId
            }

            share.hierarchyAddUser = updateObj;
        });
        $scope.searchList = [];
    }

    $scope.addUserToHierarchy = function (child) {
        $scope.orgHead = child;
        $scope.searchList = [];
        $scope.keyword = ""
        $(".no-hr-text").hide(100)
    }

    $scope.assignOrgHead = function () {

        if($scope.orgHead){
            var data = {
                emailId:$scope.orgHead.emailId,
                hierarchyParent: null,
                hierarchyPath:null,
                orgHead:true,
                name:$scope.account.name.toLowerCase(),
                hierarchy:$scope.account.hierarchy
            }

            $http.post("/account/admin/set/org/head",data)
                .success(function (response) {
                    $scope.orgHead = null;
                    share.accountHierarchy(response)
                })
        }
    }

    $('#resetOrgHead').on('click', function() {
        $(".no-hr-text").show(100)
        $scope.$apply(function (){
            $scope.orgHead = null
        })
    });

    $scope.searchForUser = function (query) {

        if(query && query.length>0){
            query = query.toLowerCase();

            var results = $scope.unassigned.map(function (el) {
                if(_.includes(el.emailId.toLowerCase(),query)){
                    return el;
                }
            });

            $scope.searchList = _.compact(results);
        } else {
            $scope.searchList = []
        }
    }

    $scope.saveUserData = function (contact) {
        var user = {};
        if(share.hierarchyAddUser) {
            user = share.hierarchyAddUser;
            user.name = $scope.account.name.toLowerCase();
        }

        contact.name = $scope.account.name.toLowerCase();

        $http.post('/account/admin/user/update',{user:share.hierarchyAddUser?user:null,contact:contact})
            .success(function (response) {
                share.accountHierarchy(response)
                $scope.item = {};
                $scope.keyword = "";
                $scope.closeModal()
            });
    }

})

relatasApp.directive('searchResults', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="searchList.length>0">' +
        '<div ng-repeat="item in searchList track by $index"> ' +
        '<div class="clearfix cursor" style="margin: 10px 0;" title="{{item.fullName}}, {{item.emailId}}" ng-click="addUser(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="item.noImage === 0" ng-src="{{item.image}}" title="{{item.fullName}}, {{item.emailId}}" class="contact-image">' +
        '<span ng-if="item.noImage === 1">' +
        '<span class="contact-no-image">{{item.fullName.substring(0,2)}}</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">{{item.name}}</p><p class="margin0">{{item.emailId}}</p></div>' +
        '<div class="pull-left">' +
        '<p class="margin0">{{item.fullName}}</p>' +
        '<p class="margin0">{{item.emailId}}</p>' +
        '</div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchResultsNoHierarchy', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="searchList.length>0">' +
        '<div ng-repeat="item in searchList track by $index"> ' +
        '<div class="clearfix cursor" style="margin: 10px 0;" title="{{item.fullName}}, {{item.emailId}}" ng-click="addUserToHierarchy(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="item.noImage === 0" ng-src="{{item.image}}" title="{{item.fullName}}, {{item.emailId}}" class="contact-image">' +
        '<span ng-if="item.noImage === 1">' +
        '<span class="contact-no-image">{{item.fullName.substring(0,2)}}</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">{{item.name}}</p><p class="margin0">{{item.emailId}}</p></div>' +
        '<div class="pull-left">' +
        '<p class="margin0">{{item.fullName}}</p>' +
        '<p class="margin0">{{item.emailId}}</p>' +
        '</div>' +
        '</div></div></div>'
    };
});

function chartDraw($scope,$http,share,datasource) {

    $(function() {
        var oc = $('#chart-container').orgchart({
            'data' : datasource,
            'nodeContent': 'title',
            'draggable': true,
            'dropCriteria': function($draggedNode, $dragZone, $dropZone) {
                return true;
            }
        });
        share.oc = oc;
        oc.$chart.on('nodedrop.orgchart', function(event, extraParams) {
            setTimeout(function () {
                var draggedNodeId = extraParams.draggedNode.attr('id');
                var dropZoneId = extraParams.dropZone.attr('id');

                updateHierarchyPath($scope,$http,share,dropZoneId,draggedNodeId)
            },10)
        });

        $('.orgchart').addClass('noncollapsable');
        editHierarchy($scope,share,oc,$http);
        updateHierarchy(oc)
    });
}

function getFullPathToChild(oc,share,callback) {

    var objById = {};
    _.each(share.allIdsInTree,function (id) {
        getChildPathForNode(oc,id,"",function (path) {
            objById[id] = {
                path:path === ""?null:path
            }
        })
    })

    callback(objById)
}

function getChildPathForNode(oc,id,path,callback) {
    var currentNode = $("#"+id),
        parentId = oc.getRelatedNodes(currentNode,'parent').attr('id')

    if(parentId){
        path = ","+parentId+path;
        getChildPathForNode(oc,parentId,path,callback)
    } else {
        if(callback){
            callback(path)
        }
    }
}

function updateHierarchy(oc) {
    $('#btn-export-hier').on('click', function() {
        if (!$('pre').length) {
            var hierarchy = oc.getHierarchy();
            $('#btn-export-hier').after('<pre>').next().append(JSON.stringify(hierarchy, null, 2));
        }
    });
}

function editHierarchy($scope,share,oc,$http) {

    oc.$chartContainer.on('click', '.node', function() {
        var $this = $(this);

        console.log("HOLA")

        $scope.$apply(function (){
            $scope.editUserData = true;
            $scope.user = getUserDetails($scope,share,$this.attr('id'))
            $scope.setupRelationAndTypes($scope.user.relationshipType?$scope.user.relationshipType:"",
                $scope.user.type?$scope.user.type:"")
        });
        $('#selected-node').val($this.find('.title').text()).data('node', $this);
    });

    $(".node").on({

        mouseenter: function () {
            var $this = $(this);
            $this.find('.contact-details').show(200)

            $scope.$apply(function (){
                $scope.selectedContact = getUserDetails($scope,share,$this.attr('id'))

                function checkTeamLoaded(){
                    if(share.teamDictionary){
                        setTimeOutCallback(5,function () {
                            getContactRelatedInsights($scope.selectedContact.emailId,$scope,$http,share.teamDictionary,share,$this)
                        })
                    } else {
                        setTimeOutCallback(100,function () {
                            checkTeamLoaded();
                        })
                    }
                }
                checkTeamLoaded();
            });
        },
        mouseleave: function () {
            var $this = $(this);
            $this.find('.contact-details').hide(50)
        }
    });

    oc.$chartContainer.on('click', '.orgchart', function(event) {
        if (!$(event.target).closest('.node').length) {
            $('#selected-node').val('');
        }
    });

    $('input[name="chart-state"]').on('click', function() {
        $('.orgchart').toggleClass('view-state', this.value !== 'view');
        $('#edit-panel').toggleClass('view-state', this.value === 'view');
        if ($(this).val() === 'edit') {
            $('.orgchart').find('tr').removeClass('hidden')
                .find('td').removeClass('hidden')
                .find('.node').removeClass('slide-up slide-down slide-right slide-left');
        } else {
            $('#btn-reset').trigger('click');
        }
    });

    $('input[name="node-type"]').on('click', function() {
        var $this = $(this);
        if ($this.val() === 'parent') {
            $('#edit-panel').addClass('edit-parent-node');
            $('#new-nodelist').children(':gt(0)').remove();
        } else {
            $('#edit-panel').removeClass('edit-parent-node');
        }
    });

    $('#btn-add-input').on('click', function() {
        $('#new-nodelist').append('<li><input type="text" class="new-node"></li>');
    });

    $('#btn-remove-input').on('click', function() {
        var inputs = $('#new-nodelist').children('li');
        if (inputs.length > 1) {
            inputs.last().remove();
        }
    });

    $('#btn-delete-nodes').on('click', function() {
        var $node = $('#selected-node').data('node');
        if (!$node) {
            alert('Please select one node in orgchart');
            return;
        } else if ($node[0] === $('.orgchart').find('.node:first')[0]) {
            if (!window.confirm('Are you sure you want to delete the whole chart?')) {
                return;
            }
        }

        if (window.confirm('Are you sure you want to remove the team member and subsequent reportees?')) {
            oc.removeNodes($node);

            $('#selected-node').val('').data('node', null);
            $scope.$apply(function () {
                $scope.editUserData = false;
            })
        } else {
            return;
        }
    });

    $('#btn-reset').on('click', function() {
        $('.orgchart').find('.focused').removeClass('focused');
        $('#selected-node').val('');
        $('#new-nodelist').find('input:first').val('').parent().siblings().remove();
        $('#node-type-panel').find('input').prop('checked', false);
    });
}

function getUserDetails($scope,share,userId) {
    return share.hierarchyObj[userId]
}

function getContactRelatedInsights(emailId,$scope,$http,teamDictionary,share,$this) {
    var url = "/account/insights/contact";
    url = fetchUrlWithParameter(url,"userIdList",_.map(share.teamArray,"userId"));
    url = fetchUrlWithParameter(url, "contacts", emailId);

    var pLeft = $(".parent-wrapper").position().left,
        pRight = offsetRight($(".parent-wrapper")),
        cLeft = $this.find(".contact-details").position().left,
        cRight = offsetRight($(".contact-details"))

    // console.log(pLeft,cLeft,diffByPercentage(pLeft,cLeft))
    // console.log(pRight,cRight,diffByPercentage(pRight,cRight))

    if ($(".parent-wrapper").prop('scrollWidth') > $(".parent-wrapper").width() ) {
        // console.log("YES!!");
        // $this.find(".contact-details").css("left", "-295px");
        // $this.find(".contact-details").css("right", "inherit");
    } else {
        // console.log("NO!!");
        // $this.find(".contact-details").css("background-color", "#3f5566");
    }

    function offsetRight(parentId){
         return ($(window).width() - (parentId.offset().left + parentId.outerWidth()));
    }

    function diffByPercentage(a,b){
        return (a - b) / b * 100;
    }

    $(".contact-details").empty();
    $(".contact-details").css({"padding-top":"25%"}).append("Loading data...");

    $http.get(url)
        .success(function (response) {
            $scope.dataValues = response.Data.opps
            var interactions = "",
                opps = "";
            var oppTbHeader = '<tr><th>OPPORTUNITIES</th><th>VALUE</th><th>STAGE</th></tr>';
            var oppTable = ""

            if(response.Data.opps && response.Data.opps.length>0){
                _.each(response.Data.opps,function (op) {
                    opps = opps+ '<tr>' +
                        '<td>'+op.opportunityName+'</td>' +
                        '<td>'+share.currency+' '+getAmountInThousands(op.amount,2,share.currency =="INR")+'</td>' +
                        '<td>'+op.stageName+'</td>' +
                        '</tr>'
                });

                oppTable = '<table class="width100">'+oppTbHeader+opps+'</table>';
            } else {
                opps = opps+ '<tr><td></td><td></td><td style="width: 100%;text-align: center">No opportunities found.</td></tr>'
                oppTable = '<table class="width100">'+opps+'</table>';
            }

            if(response.Data.interactions && response.Data.interactions.length>0) {

                _.each(response.Data.interactions,function (int) {
                    if(teamDictionary[int._id]){
                        interactions = interactions +
                            '<td class="clearfix owners" title="'+teamDictionary[int._id].fullName+'">'+'<span>'+teamDictionary[int._id].fullName.substr(0,2).toUpperCase()+
                            '</span><br>'+moment(int.lastInteractedDate).format(standardDateFormat())+'<br>['+int.count+
                            ']</td>'
                    }
                });
            } else {
                interactions = interactions+ '<tr><td></td><td></td><td style="'+ 'width: 100%;' +
                    '    text-align: center;' +
                    '    padding: 0;' +
                    '">No interactions found.</td></tr>'
            }

            var intTable = '<table>'+'<tr>'+interactions+'</tr>'+'</table>'

            $(".contact-details").empty();
            $(".contact-details").css({"padding-top":"0%"}).append(oppTable+'<hr>'+intTable)
        })

}

function convertArrayToTree(data){

    var root = data.find(function(item) {
        return item.orgHead;
    });

    if(root) {

        var tree = {
            _id: root._id,
            id: root._id,
            firstName: root.fullName?root.fullName:root.emailId,
            lastName: "",
            name: root.fullName?root.fullName:root.emailId,
            designation: root.designation,
            title: root.designation,
            emailId: root.emailId,
            relationshipType: root.relationshipType,
            isAccountHierarchy: true,
            type: root.type,
            role: root.role
        };

        var parents = [tree];
        while (parents.length > 0) {
            var newParents = [];
            parents.forEach(function(parent) {
                data.filter(function(item) {
                    return item.hierarchyParent === parent._id
                }).forEach(function(child) {
                    var c = {
                        role:child.role,
                        _id: child._id,
                        id: child._id,
                        title: child.designation,
                        name: child.fullName?child.fullName:child.emailId +" "+"",
                        firstName: child.fullName?child.fullName:child.emailId,
                        lastName: "",
                        designation: child.designation,
                        emailId: child.emailId,
                        relationshipType: child.relationshipType,
                        isAccountHierarchy: true,
                        type: child.type
                    };
                    parent.children = parent.children || [];
                    parent.children.push(c);
                    newParents.push(c);
                });
            });
            parents = newParents;
        }

        return tree
    } else {
        return null
    }
}

function getFullPathToParent(oc,currentId,path,callback) {

    var currentNode = $("#"+currentId),
        parentId = oc.getRelatedNodes(currentNode,'parent').attr('id')

    if(parentId){
        path = ","+parentId+path;
        getFullPathToParent(oc,parentId,path,callback)
    } else {
        if(callback){
            callback(path)
        }
    }
}

function buildTeamProfile(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noImage:1,
            firstName:el.firstName,
            lastName:el.lastName
        })
    });

    return team;
}

function updateHierarchyPath($scope,$http,share,selfParentId,selfId) {
    var url = "/account/admin/update/hierarchy/path"
    var updateObj = {
        selfId:selfId,
        hierarchyParent:selfParentId,
        selfHierarchyParent:selfId,
        hierarchyPath:selfParentId,
        emailId:share.hierarchyObj[selfId].emailId,
        name:$scope.account.name.toLowerCase()
    }

    $http.post(url,updateObj)
        .success(function (response) {
            share.accountHierarchy(response)
        })
}