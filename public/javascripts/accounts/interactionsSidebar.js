relatasApp.controller("interactions_sidebar_settings", function($scope, $http, $rootScope, share,$sce){

    $scope.closeModal = function () {
        $rootScope.showInteractionDetails = false;
        share.closeModal()
    }

    share.viewInteraction = function (interaction) {
        setSignature($rootScope,$scope);
        $scope.item = interaction;
        viewEmail($scope,$http,interaction.dataObj,$sce)
    }

    $scope.replyEmail = function (replyBody,item) {
        replyToEmail($scope,$http,item.dataObj.compose_email_subject,replyBody,item,item.dataObj.interaction.emailId,null,function () {

            $scope.replyBody = "";
            $scope.closeModal();
        })
    }

});

function setSignature($rootScope,$scope){
    $scope.body = '\n\n\n'+getSignature($rootScope.currentUser.firstName+' '+$rootScope.currentUser.lastName,$rootScope.currentUser.designation,$rootScope.currentUser.companyName,$rootScope.currentUser.publicProfileUrl)
    $scope.replyBody = '\n\n\n'+getSignature($rootScope.currentUser.firstName+' '+$rootScope.currentUser.lastName,$rootScope.currentUser.designation,$rootScope.currentUser.companyName,$rootScope.currentUser.publicProfileUrl)
    
}


