$(document).ready(function(){
    $.ajaxSetup({ cache: false });

    var facebookUserData,twitterUserData,linkedinUserData;
    var userProfile;
    getUserProfile();
    var a =0;

    function getUserProfile() {
        $.ajax({
            url: '/dashboard/userProfileWithContactsCount',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;
                checkStatus();
                if(userProfile.firstName){
                    $("#welcome-msg").text('Welcome '+userProfile.firstName);
                    $("#contacts-count").text(userProfile.contactsLength);
                    linkedin(userProfile.linkedin);
                    facebook(userProfile.facebook);
                    twitter(userProfile.twitter);
                }
                if(a < 3){
                    a++;
                    setTimeout(function(){
                        getUserProfile();
                    },2)
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function checkStatus(){
        var url = window.location.href.split('?');

        if(checkRequired(url[1])){
            var status = url[1].split('=');
            status = status[1];
            status = status.replace('#_','');
            switch (status){
                case 'connect-accounts':$(".connect-accounts").trigger('click');
                    break;
            }
        }
    }

    $(".subscribe-events").on('click',function(){

        $("#action-content").show();
        $("#action-content-linkedin-contacts").hide();
        $("#social-content").hide()
        getAllLocations();
        $("#subscribe-events-content").show();
    });

    $("#linkedin-contacts").on("click",function(){

        var htmlContent = '<div class="caption">'+
        '<a href="https://medium.com/relatas/mass-email-linkedin-contacts-in-5-steps/" class="know-more" target="_blank">Learn how to import your Linkedin Contacts</a>'+
        '</div>';
        $("#action-content").show();
        $("#social-content").hide();
        $("#subscribe-events-content").hide();
        $("#action-content-linkedin-contacts").show();
        $("#action-content-linkedin-contacts").html(htmlContent);
    });

    $(".connect-accounts").on('click',function(){
        $("#subscribe-events-content").hide();
        $("#action-content-linkedin-contacts").hide();
        $("#action-content").show();
        $("#social-content").show()
    });

    function getAllLocations(){
        $.ajax({
            url:'/allEventsLocations',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(locations){
                var locationListHtml = '<div class="pull-left" style="margin-left: 1%">';
                locationListHtml += '<div class="checkbox"><label><input type="checkbox" id="All" value="All">All</label></div>';
                if(checkRequired(locations)){
                    if(checkRequired(locations[0])){
                        displaySubscribeLocationList(locations);
                    }else {
                        locationListHtml += '</div>';
                        $("#subscribeEventLocations").html(locationListHtml);
                        getUserSubscribeToEventsDetails();
                        isLoggedIn(true)
                    }
                }
                else {
                    locationListHtml += '</div>';
                    $("#subscribeEventLocations").html(locationListHtml);
                    getUserSubscribeToEventsDetails();
                    isLoggedIn(true)
                }
            }
        })
    }

    function displaySubscribeLocationList(locations){

        var locationListHtml = '<div class="pull-left" style="margin-left: 1%">';
        locationListHtml += '<div class="checkbox"><label><input type="checkbox" id="All" value="All">All</label></div>';

        for(var i=0; i<locations.length; i++){

            var id = locations[i].eventLocation.replace(/\s/g,"");
            var value = locations[i].eventLocation.replace(/\s/g,"@");
            locationListHtml +='<div class="checkbox"><label>';
            locationListHtml +='<input type="checkbox" id='+id+' value='+value+'>'+locations[i].eventLocation+'</label></div>';
        }
        locationListHtml += '</div>';
        $("#subscribeEventLocations").html(locationListHtml)
        getUserSubscribeToEventsDetails()

    }


    function getUserSubscribeToEventsDetails(){

        $.ajax({
            url:'/getUserSubscribeToEventsDetails',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){

                if(checkRequired(result)){

                    result.eventTypes.forEach(function(type){
                        var id =type.eventType.replace(/\//g,"");

                        $("#"+id.replace(/\s/g,"")).attr("checked",type.status);
                    });
                    result.locations.forEach(function(type){
                        var id =type.location
                        $("#"+id.replace(/\s/g,"")).attr("checked",type.status);
                    });
                }
            }
        })
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }


    function facebook(facebook){
        if (checkRequired(facebook)) {
            if (checkRequired(facebook.id)) {
                //$("#facebook-connect").hide();

                facebookUserData = facebook
                $('#addFacebookAccountH3').text(facebook.name);
                $('#addFacebookAccount').attr('checked','checked');
                $( "<style> #activeFacebook { display:block; }</style>" ).appendTo('head');
            }else  $('#addFacebookAccountH3').text('Add your Facebook Account');
        }else  $('#addFacebookAccountH3').text('Add your Facebook Account');
    }

    function linkedin(linkedin){
        if (checkRequired(linkedin)) {
            if (checkRequired(linkedin.id)) {
                //$("#linkedin-connect").hide();
                linkedinUserData = linkedin
                $('#addLinkedinAccountH3').text(linkedin.name);
                $('#addLinkedinAccount').attr('checked','checked');
                $( "<style> #activeLinkedin { display:block; }</style>" ).appendTo('head');
            } else $('#addLinkedinAccountH3').text('Add your Linkedin Account');
        }else $('#addLinkedinAccountH3').text('Add your Linkedin Account');
    }

    function twitter(twitter){
        if (checkRequired(twitter)) {
            if (checkRequired(twitter.id)) {
                //$("#twitter-connect").hide();
                twitterUserData = twitter
                $('#addTwitterAccountH3').text(twitter.displayName);
                $('#addTwitterAccount').attr('checked','checked');
                $( "<style> #activeTwitter { display:block; }</style>" ).appendTo('head');
            } else $('#addTwitterAccountH3').text('Add your Twitter Account');
        }else $('#addTwitterAccountH3').text('Add your Twitter Account');
    }

    /* Authentications  */

    $('#addLinkedinAccount').change(function(){
        if($('#addLinkedinAccount').prop('checked')) {
            authenticateLinkedin()
        }
        else{

            $.ajax({
                url:'/removeLinkedinAccount',
                type:'GET',
                success:function(isSuccess){
                    linkedinUserData = null;
                    getUserProfile();

                }
            });
        }
    });

    $("#addLinkedinAccountH3").on("click",function(){
        authenticateLinkedin()
    });

    $("#addFacebookAccountH3").on("click",function(){
        authenticateFacebook()
    });

    $("#addTwitterAccountH3").on("click",function(){
        authenticateTwitter()
    })

    $('#addFacebookAccount').change(function(){
        if($('#addFacebookAccount').prop('checked')) {
            authenticateFacebook()
        }
        else{
            $.ajax({
                url:'/removeFacebookAccount',
                type:'GET',
                success:function(isSuccess){
                    facebookUserData = null;
                    getUserProfile();
                }
            });
        }
    });

    $('#addTwitterAccount').change(function(){
        if($('#addTwitterAccount').prop('checked')) {
            authenticateTwitter()
        }
        else{
            $.ajax({
                url:'/removeTwitterAccount',
                type:'GET',
                success:function(isSuccess){
                    twitterUserData = null;
                    getUserProfile();
                }
            });
        }
    });


    function authenticateLinkedin(){
        if(checkRequired(linkedinUserData)){

        }else{
            window.location.replace('/linkedinPostSignUpPage');
        }
    }

    function authenticateFacebook(){
        if(checkRequired(facebookUserData)){

        }else{
            window.location.replace('/facebookPostSignUpPage');
        }
    }

    function authenticateTwitter(){
        if(checkRequired(twitterUserData)){

        }else{

            window.location.replace('/twitterPostSignUpPage');
        }
    }
    /* Subscribe to events  */

    $("#subScribeToEvents").on("click",function(){
        var typeDiv = $('#subscribeEventTypes').children();
        var locationDiv = $('#subscribeEventLocations').children();
        var innerArr = []
        var JSONString = ''
        JSONString += '{"eventTypes":'
        JSONString += '['
        typeDiv.each(function(){
            var allDivs = $(this).children();
            allDivs.each(function(){
                var checkBoxDivs = $(this).children();
                checkBoxDivs.each(function(){
                    var checkBoxDiv = $(this).children();
                    checkBoxDiv.each(function(){
                        var inner = ''

                        inner += '{"eventType":'+JSON.stringify($(this).attr('value'))+','
                        inner += '"status":'+JSON.stringify($(this).prop("checked"))+'}'

                        innerArr.push(inner)
                    })
                })
            })
        })
        for(var i=0; i<innerArr.length; i++){
            JSONString += innerArr[i]
            if(i == innerArr.length-1){

            }
            else{
                JSONString += ','
            }
        }
        JSONString += '],'
        JSONString += '"locations":'
        JSONString += '['

        var innerArr2 = []
        locationDiv.each(function(){
            var allDivs = $(this).children();
            allDivs.each(function(){
                var checkBoxDivs = $(this).children();
                checkBoxDivs.each(function(){
                    var checkBoxDiv = $(this).children();

                    checkBoxDiv.each(function(){
                        var inner = ''

                        inner += '{"location":'+JSON.stringify($(this).attr('value').replace(/\@/g," "))+','
                        inner += '"status":'+JSON.stringify($(this).prop("checked"))+'}'

                        innerArr2.push(inner)
                    })
                })
            })
        })
        for(var j=0; j<innerArr2.length; j++){
            JSONString += innerArr2[j]
            if(j == innerArr2.length-1){

            }
            else{
                JSONString += ','
            }
        }
        JSONString += ']}';
        var obj = {
            ArrString:JSONString
        };

        $.ajax({
            url:'/subscribeToEvents',
            type:'POST',
            datatype:'JSON',
            data:obj,
            traditional:true,
            success:function(result){
                if(result == 'loginRequired'){

                }else
                if(result){
                    getUserSubscribeToEventsDetails()
                    alert("You have successfully subscribed.");
                }
                else {
                    alert("An error occurred. Please try again.");
                }
            }
        })
    });

    $(".next").on('click',function(){
        window.location.replace('/onBoarding');
    });


    $.ajax({
        url:'/postSignUp/interactions/googleMeeting',
        type:'GET',
        success:function(isSuccess){

        }
    });
});