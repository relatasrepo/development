$(document).ready(function(){

    var userTimezone,userProfile;
    getUserProfile();

    // Function to get user contacts info
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;
                if(checkRequired(userProfile) && checkRequired(userProfile.timezone) && checkRequired(userProfile.timezone.name)){
                    userTimezone = userProfile.timezone.name;
                }
                else{
                    var tz = jstz.determine();
                    userTimezone = tz.name();
                }
                if( userProfile.profilePicUrl.charAt(0) == '/' || userProfile.profilePicUrl.charAt(0) == 'h'){
                    $('#profilePic').attr("src",userProfile.profilePicUrl);
                } else  $('#profilePic').attr("src","/"+userProfile.profilePicUrl);
                $("#user-name").text(userProfile.firstName+' '+userProfile.lastName);
                var headline = '';
                if(checkRequired(userProfile.designation) && checkRequired(userProfile.companyName)){
                    headline = userProfile.designation+', '+userProfile.companyName;
                }
                else if(checkRequired(userProfile.designation)){
                    headline = userProfile.designation
                }
                else if(checkRequired(userProfile.companyName)){
                    headline = userProfile.companyName
                }

                $("#user-company-designation").text(headline);
                var location = '';
                if(checkRequired(userProfile.location)){
                    location = userProfile.location;
                }
                else if(checkRequired(userProfile.currentLocation) && checkRequired(userProfile.currentLocation.city) && checkRequired(userProfile.currentLocation.country)){
                    location = userProfile.currentLocation.city+', '+userProfile.currentLocation.country;
                }
                else if(checkRequired(userProfile.currentLocation) && checkRequired(userProfile.currentLocation.city)){
                    location = userProfile.currentLocation.city
                }

                $("#user-location").text(location);

                $('#profilePic').error(function(){
                   $(this).attr('src','/images/default.png')
                })
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    lastInteracted()
    // Function to get user contacts info
    function lastInteracted() {
        $.ajax({
            url: '/interactions/count/lastInteractionDate',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if(checkRequired(result) && result.length > 0 && checkRequired(result[0].lastInteracted)){
                    $("#total-interactions").text('Total Interactions: '+result[0].count || 0);
                    $("#last-interaction").text('Last Interaction: '+moment(result[0].lastInteracted).format('DD-MMM-YYYY'));
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 10000
        })
    }

    lastFiveInteracted()
    function lastFiveInteracted() {
        $.ajax({
            url: '/interactions/count/lastFiveInteractions',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if(checkRequired(result) && result.length > 0){
                    for(var i=0; i<result.length; i++){
                        $("#five-interactions").append(moment(result[i].interactionDate).format('DD-MMM-YYYY')+' -- '+result[i].type+' -- '+result[i].title+'<br>')
                    }
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 10000
        })
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }

});