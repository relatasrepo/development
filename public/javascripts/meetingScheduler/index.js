var relatasApp = angular.module('relatasApp', ['angular-loading-bar', 'ngSanitize']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.service('share', function() {
    return {
        setTargetForFy:function(value){
            this.targetForFy = value;
        }
    }
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }

        else toastr.error("Please enter search content")
    };
});

relatasApp.controller("logedinUser", function($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {

        $rootScope.liuEmailId = response.Data.emailId; 
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        $rootScope.opportunityStagesFilter = [{name:"All"}]
        $rootScope.opportunityStagesFilter = $rootScope.opportunityStagesFilter.concat(share.opportunityStages)
        share.companyDetails.currency.forEach(function (el) {
            share.currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                share.primaryCurrency = el.symbol;
            }
        });

        if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
            share.timezone = response.Data.timezone.name;
        }

        $rootScope.primaryCurrency = share.primaryCurrency;
    })
});

relatasApp.controller("teamList", function($scope, $http, share,$rootScope) {

    $scope.colClass = "col-sm-4";

    $scope.registerDatePickerId = function(id){
        $('#'+id).datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    $scope[id] = moment(dp).format(standardDateFormat());
                });
            }
        });
    }

    $scope.openMeeting = function(meeting){
        $scope.meeting = meeting;
        $scope.meeting.openModal = true
    }

    $scope.openFilter = function(header){
        if(header.name != "Sl #"){
            if(header.cursor){
                header.open = !header.open
            }
        }

        if(header.name == "Sl #"){
            // $scope.sortType = "sl"
            $scope.sortReverse = !$scope.sortReverse
        }
    }

    $scope.sortType = "sl"
    $scope.sortReverse = false

    $scope.filtersApplied = [];
    $scope.status = {};
    $scope.status.confirmed = false;
    $scope.status.declined = false;
    $scope.status.postponed = false;

    $scope.closeFilters = function(header) {
        header.open = false;
    }

    $scope.applyFilters = function(header){
        header.open = false;
        var usersSelected = false;
        if(header && header.name == "Status"){

            var statusExists = false;
            _.each($scope.filtersApplied,function (fa) {
                if(fa.type == "status"){
                    statusExists = true;
                    if($scope.status.confirmed){
                        fa.value.push('confirmed')
                    } else {
                        fa.value = _.remove(fa.value, function(n) {
                            return n != 'confirmed';
                        });
                    }

                    if($scope.status.postponed){
                        fa.value.push('postponed')
                    } else {

                        fa.value = _.remove(fa.value, function(n) {
                            return n != 'postponed';
                        });
                    }

                    if($scope.status.declined){
                        fa.value.push('declined')
                    } else {
                        fa.value = _.remove(fa.value, function(n) {
                            return n != 'declined';
                        });
                    }
                }
            });

            if(!statusExists){
                var value = [];
                if($scope.status.confirmed){
                    value.push('confirmed')
                }

                if($scope.status.postponed){
                    value.push('postponed')
                }

                if($scope.status.declined){
                    value.push('declined')
                }

                $scope.filtersApplied.push({
                    type:"status",
                    value:value
                })
            }
        }

        if(header && header.name == "Owner"){
            usersSelected = true;
            $scope.filtersApplied.push({
                type:"toEmailId",
                value:_.map($scope.owners,function (ow) {
                    if(ow.selected){
                        return ow.name;
                    }
                })
            })
        }

        if(header && header.name == "Date"){
            $scope.filtersApplied.push({
                type:"meetingDate",
                startDate:$scope.startDate,
                endDate:$scope.endDate
            })
        }

        var dateSelected = false;
        $scope.filtersApplied.forEach(function (fa) {
            if(fa.type == "toEmailId"){
                usersSelected = true;
            }

            if(fa.type == "meetingDate"){
                dateSelected = true;
            }
        });

        if(!usersSelected){
            $scope.filtersApplied.push({
                type:"toEmailId",
                value: $scope.teamEmailIds
            })
        }

        if(!dateSelected){
            $scope.filtersApplied.push({
                type:"meetingDate",
                startDate:new Date(),
                endDate:new Date()
            })
        }

        fetchMeetings($scope,$http,share,$rootScope,'/meetings/scheduler/get/by/filters',$scope.filtersApplied.length>0?$scope.filtersApplied:null);
    }

    $scope.closeModal = function(meeting){
        if($scope.meeting){
            $scope.meeting.openModal = false
        }
    }

    $scope.updateMeeting = function(meeting){

        getBrowserLocation(function (data) {

            if(!meeting.location.lat && !meeting.location.lng){
                if(data && meeting.status == "confirmed"){
                    meeting.location.lat = data.lat,
                        meeting.location.lng = data.lng
                }
            }

            if(meeting.note){
                if(!meeting.notes){
                    meeting.notes = []
                };

                meeting.notes.push({
                    date: new Date(),
                    text:meeting.note,
                    fromEmailId: $rootScope.liuEmailId,
                    dateFormatted: moment().format("DD MMM YYYY, hh:mm a"),
                    by: share.usersDictionary[$rootScope.liuEmailId]
                })
            }

            $http.post('/meetings/scheduler/update',meeting)
                .success(function (response) {
                    $scope.closeModal();
                    fetchMeetings($scope,$http,share,$rootScope);
                    toastr.success("Details updated successfully.")
                });
        })
    }

    $scope.selectAll = function(){

        $scope.selectAllisChecked = !$scope.selectAllisChecked;

        $scope.tableRows.forEach(function (mt) {
            if(mt.fromEmailId === $rootScope.liuEmailId){
                mt.delete = $scope.selectAllisChecked?true:false;
            }
        })
    }

    $scope.deleteMeetings = function(meeting){
        var toDeleteIds = _.map($scope.tableRows,function (mt) {
            if(mt.delete){
                return mt._id
            }
        });

        toDeleteIds = _.compact(toDeleteIds);

        if(toDeleteIds && toDeleteIds.length>0){

            $http.post('/meetings/scheduler/delete',{_ids:toDeleteIds})
                .success(function (response) {
                    $scope.closeModal();
                    fetchMeetings($scope,$http,share,$rootScope);
                    toastr.success("Meetings deleted successfully.")
                });
        } else {
            toastr.error("Please select at least one meeting to delete.")
        }
    }

    $scope.getLiuHierarchy = function (hierarchy) {

        var url = '/company/user/hierarchy/insights';

        if(hierarchy && hierarchy !== "Org. Hierarchy"){
            url = '/company/users/for/hierarchy';
            url = fetchUrlWithParameter(url,"hierarchyType",hierarchy.replace(/[^A-Z0-9]+/ig, "_"))
        }

        $http.get(url)
            .success(function (response) {
                $scope.team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.team = buildTeamProfiles(response.Data,$scope)

                    if($scope.team.length>1){
                        $scope.selection = {fullName:"Show all team members",emailId:"Show all team members",nameNoImg:"All",noPicFlag:true};
                    } else {
                        $scope.selection = $scope.team[0];
                    }

                    share.selection = $scope.selection

                    share.team = $scope.team;
                    var usersDictionary = {};

                    share.teamChildren = {};
                    _.each(response.listOfMembers,function (el) {

                        share.teamChildren[el.userEmailId] = el.teamMatesEmailId
                    });

                    if(response.companyMembers.length>0){
                        var companyMembers = buildAllTeamProfiles(response.companyMembers)

                        _.each(companyMembers,function (member) {
                            usersDictionary[member.emailId] = member
                        });

                        share.teamMembers = companyMembers;

                    }

                    share.usersDictionary = usersDictionary;
                }
            });
    }

    $scope.getLiuHierarchy("All Access");

    $scope.openViewFor = function (viewFor,redirectFrom) {
    };

    share.openViewFor = function(viewFor, redirectFrom){
        $scope.openViewFor(viewFor,redirectFrom)
    }

    fetchMeetings($scope,$http,share,$rootScope);

    $scope.ExcelExport= function (event) {

        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var fileData = reader.result;
            var wb = XLSX.read(fileData, {type : 'binary'});

            wb.SheetNames.forEach(function(sheetName){
                uploadMeetings($scope, $http, share, XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName]),$rootScope)
            })
        };
        reader.readAsBinaryString(input.files[0]);
    };

});

function fetchMeetings($scope,$http,share,$rootScope,url,filterObj) {

    function checkTeamMembersLoaded(){
        if(share.team && $rootScope.liuEmailId){
            $scope.tableRows = [];
            if(!url){
                url = '/meetings/scheduler/get/today';
            }

            $http.post(url,filterObj?filterObj:null)
                .success(function (response) {
                    $scope.tableRows = [];
                    $scope.canDelete = false;
                    var scheduled = 0,confirmed = 0,declined = 0,postponed = 0;

                    $scope.meetingsDateRange = "Meetings for "+moment().format(standardDateFormat());

                    if(filterObj && filterObj.length>0){
                        _.each(filterObj,function (fl) {
                            if(fl.type == "meetingDate"){
                                $scope.meetingsDateRange = "Meetings from "+moment(fl.startDate).format(standardDateFormat())+
                                    " - "+moment(fl.endDate).format(standardDateFormat());
                            }
                        })
                    }

                    if(response.SuccessCode && response.Data && response.Data.length>0){
                        $scope.tableRows = response.Data;
                        scheduled = response.Data.length;

                        _.each($scope.tableRows,function (tm,index) {
                            tm.sl = index+1;
                            if(tm.status === "confirmed"){
                                confirmed++;
                            }

                            if(tm.status === "declined"){
                                declined++;
                            }

                            if(tm.status === "postponed"){
                                postponed++;
                            }

                            if(tm.fromEmailId == $rootScope.liuEmailId){
                                $scope.canDelete = true;
                            }

                            if(tm.notes && tm.notes.length>0){
                                tm.notes.forEach(function (nt) {
                                    nt.dateFormatted = moment(nt.date).format("DD MMM YYYY, hh:mm a");
                                    nt.by = share.usersDictionary[tm.fromEmailId]
                                });
                            }

                            tm.member = share.usersDictionary[tm.toEmailId];
                            tm.contactsFormatted = tm.contacts.join(",");
                            tm.dateFormatted = moment(tm.meetingDate).format(standardDateFormat());
                        });
                    };

                    $scope.tableHeaders = [
                        {name:"Sl #",cursor:"cursor",maxWidth:"maxWidth35"},
                        {name:"Owner",cursor:"cursor",maxWidth:"maxWidth125"},
                        {name:"Account",maxWidth:"maxWidth"},
                        {name:"Contacts",maxWidth:"maxWidth200"},
                        {name:"Location"},
                        {name:"Date",cursor:"cursor",maxWidth:"maxWidth35"},
                        {name:"Status",cursor:"cursor",maxWidth:"maxWidth"},
                        {name:"Notes",maxWidth:"maxWidth35"}];

                    if($scope.canDelete){
                        $scope.tableHeaders.push({name:"Delete",maxWidth:"maxWidth"})
                    }

                    todayNumbers($scope,scheduled,confirmed,declined,postponed)
                })
        } else {
            setTimeOutCallback(100,function () {
                checkTeamMembersLoaded();
            });
        }
    };

    checkTeamMembersLoaded();
}

function uploadMeetings($scope, $http, share, meetings,$rootScope) {

    var meetingsNotUploaded = [],
        meetingsToUpload = [];

    meetings.forEach(function (el) {
        el.toEmailId = el.toEmailId.trim();
        if(!_.includes($scope.teamEmailIds,el.toEmailId)){
            meetingsNotUploaded.push(el.toEmailId)
        } else {
            meetingsToUpload.push(el)
        }
    });

    $http.post('/meetings/scheduler/save',meetingsToUpload)
        .success(function (response) {

            meetingsNotUploaded = _.uniq(meetingsNotUploaded);

            if(meetingsNotUploaded.length>0){
                toastr.warning("Meetings assigned successfully except for "+meetingsNotUploaded.join(","))
            } else {
                toastr.success("Meetings assigned successfully.")
            }

            fetchMeetings($scope,$http,share,$rootScope);
        });
}

function todayNumbers($scope,scheduled,confirmed,declined,postponed) {

    $scope.cols = [{
        name:"Scheduled",
        value:scheduled,
        percentage:"null"
    },{
        name:"Confirmed",
        value:confirmed,
        percentage:scheduled && confirmed?calculatePercentage(confirmed,scheduled)+"%":"0%"
    },{
        name:"Declined",
        value:declined,
        percentage:scheduled && declined?calculatePercentage(declined,scheduled)+"%":"0%"
    },{
        name:"Postponed",
        value:postponed,
        percentage:scheduled && postponed?calculatePercentage(postponed,scheduled)+"%":"0%"
    }];
}

function buildTeamProfiles(data,$scope) {
    var team = [];
    $scope.teamEmailIds = [];
    $scope.owners = [];

    _.each(data,function (el) {
        $scope.teamEmailIds.push(el.emailId);

        $scope.owners.push({
            name:el.emailId
        });

        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase()
        })
    });

    return team;
}

function buildAllTeamProfiles(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase()
        })
    });

    return team;
}