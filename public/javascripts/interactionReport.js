$(document).ready(function(){

    var interactedArrStr,userProfile,geocoder,myLatlng,mapOptions,map,markerCluster;
    getInteractionsNext();
    getInteractionsMost();
    getInteractionsByMonth();
    getInteractionsBySubType();

    checkStatus();
    function checkStatus(){
        var url = window.location.href.split('?');

        if(checkRequredField(url[1])){
            var status = url[1].split('=');
            status = status[1];
            if(status == 'showMap'){
                $("#global-map").show();
                $("#global-map-img").hide();
                getUserProfile();
            }
            else{
                $("#global-map").hide();
                $("#global-map-img").show();
            }
        }
        else{
            $("#global-map").hide();
            $("#global-map-img").show();
        }
    }

    var styles = [
        {
            height: 53,
            url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
            width: 53
        }
    ]

    // Function to get user contacts info
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;
                showUserLocation();
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function getInteractionsNext(){
        $.ajax({
            url:'/dashboard/message/getNewInteractions',
            type:'GET',
            success:function(msgObj){

                    interactedArrStr = msgObj.interactedArrStr;

                    $("#nonInteracted").text(msgObj.nonInteracted);
                    $("#totalInteractions").text(msgObj.totalInteractions);
                    $("#interacted").text(msgObj.interacted);

            },
            error: function (event, request, settings) {

            }
        })
    }

    function getInteractionsMost(){

        $.ajax({
            url:"/getInteractions/all/individual",
            type:"GET",
            datatype:"JSON",
            success:function(interactions){
               if(checkRequredField(interactions) && interactions.length > 0){
                   var emailIdArr = []
                   for(var i=0;i<interactions.length; i++){
                       emailIdArr.push(interactions[i]._id);
                   }
                   if(emailIdArr.length > 0)
                       getUserProfileByEmailsArr(emailIdArr,interactions);
               }
            }
        });
    }

    function getUserProfileByEmailsArr(emailIdArr,interactions){
        $.ajax({
            url:"/getUserProfile/byEmailIdArr",
            type:"POST",
            data:{emailIdArr:JSON.stringify(emailIdArr)},
            datatype:"JSON",
            success:function(userList){

              if(checkRequredField(userList) && userList.length > 0){

              }else userList = [];
                      for(var j=0; j<interactions.length; j++){
                          var user;
                          for(var u=0; u<userList.length; u++){
                             if(interactions[j]._id == userList[u].emailId){
                                 user = userList[u];
                             }
                          }
                          var html = '';
                          if(j == 0)
                             html = '<div class="col-md-offset-1 col-md-2 col-sm-6 top-contacts">';
                          else
                             html = '<div class="col-md-2 col-sm-6 top-contacts">';

                          var name= '';
                          if(checkRequredField(user)){
                              name = user.firstName+' '+user.lastName;
                              var imageUrl = '/getImage/'+user._id;
                              html += '<img src='+imageUrl+' alt="..." class="img-circle">';
                          }
                          else{
                              name = interactions[j]._id;
                              html += '<img src="/images/default.png" alt="..." class="img-circle">';
                          }

                          html += '<div class="top-contacts-overlay" id="interaction-count-1">'+interactions[j].count+'</div>';

                          html += '<h4 title='+name.replace(/\s/g, '&nbsp;')+'>'+getTextLength(name,12)+'</h4></div>';
                          user = null;
                          name = null;
                          $("#top-five-interactions").append(html);
                      }
            }
        });
    }


    function drawChartPieChart(array) {

        var data = google.visualization.arrayToDataTable(array);
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        var options = {
            is3D: true
        };
        chart.draw(data,options);
        var chObj = $('svg',chart.ea).children();
        var chil = $(chObj[2]).children();
        var arr = [];
        for(var i=1;i<chil.length;i++){
            arr.push($($(chil[i]).children()[2]).attr('fill').toUpperCase().replace('#',''));
        }

        for(var j=1;j<array.length;j++){
                array[j][2] = arr[j-1]
        }

        var url = '';
        if(checkRequredField(array) && array.length > 1){
            url = JSON.stringify(array);
            var newUrl = $("#pieChartUrl").attr('href');

              var params = getParams(newUrl);
              var u = params.u+'/'+url;

            $("#pieChartUrl").attr('href','https://www.facebook.com/sharer/sharer.php?u='+u);

            var par2 = getParams($("#linkedinUrl").attr('href'));
            var l = par2.url+'/'+url;
            $("#linkedinUrl").attr('href','http://www.linkedin.com/shareArticle?mini=true&url='+l+'&title='+par2.title+'&summary='+par2.summary+'&source='+l);

          // var tw = getParams($("#twitterUrl").attr('href'));
          // var t = tw.url+'/'+url;
          // $("#twitterUrl").attr('href','http://twitter.com/share?text=Smart Calendar and Intelligent Document Tracking | Relatas&url='+t)
        }
    }

    function getParams(url){
        var regex = /[?&]([^=#]+)=([^&#]*)/g,
            params = {},
            match;
        while(match = regex.exec(url)) {
            params[match[1]] = match[2];
        }
        return params;
    }

    function getInteractionsBySubType(){
        $.ajax({
            url:'/getInteractions/all/bySubType',
            type:'GET',
            datatype:'JSON',
            success:function(interactions){
                if(checkRequredField(interactions) && interactions.length > 0){

                    var interactionArr = [
                        ['Type','interactions']
                    ];
                    for(var i=0; i<interactions.length; i++){
                        var single = [
                            interactions[i]._id,
                            interactions[i].count
                        ];
                        if(interactions[i]._id != null)
                            interactionArr.push(single);

                    }
                    drawChartPieChart(interactionArr);
                }
                else{

                }
            }
        })
    }

    function drawVisualization(arr) {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable(arr);

        var options = {
            colors: ['#FFD700','#1345F8'],
            hAxis: {title: "Month"},
            seriesType: "bars",
            series: {1: {type: "line"}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }

    function getTextLength(text,maxLength){

        var textLength = text.length;
        if(textLength >maxLength){
            var formattedText = text.slice(0,maxLength)
            return formattedText+'..';
        }
        else return text;
    }

    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    function getMontIndex(response,month){
        var currentMonthIndex = -1;

        for(var i=0; i<response.length; i++){
            if(month == response[i]._id){
                currentMonthIndex = i;
                break;
            }
        }

        return currentMonthIndex;
    }

    function getCurrentMonthIndex(response,month){

        var currentMonthIndex = getMontIndex(response,month)

        if(currentMonthIndex == -1 && response.length > 1){

            month = month-1 > 0 ? month-1 : 12;
           return getCurrentMonthIndex(response,month);

        }
        else{
            return currentMonthIndex;
        }
    }

    function getInteractionsByMonth(){

        $.ajax({
            url:"/getInteractions/all/individual/byMonth",
            type:"GET",
            datatype:"JSON",
            success:function(response){

                if(checkRequredField(response) && response.length > 0){

                    var month = new Date().getMonth()+1;
                    var currentMonthIndex = getCurrentMonthIndex(response,month);

                    var newArr;
                    if(currentMonthIndex !=-1 && currentMonthIndex != response.length-1){
                        var beforeArr = [];
                        var afterArr = [];
                        var c1 = currentMonthIndex;
                        var c2 = currentMonthIndex;

                        for(var j=0; j<=c1; j++){
                            beforeArr[j] = response[j];
                        }
                        for(var k=c2+1; k<response.length; k++){
                            afterArr.push(response[k]);
                        }

                         newArr = afterArr.concat(beforeArr)

                    }
                    else{
                        newArr = response;
                    }
                    newArr.reverse();

                    var arr = [['Month','interactions','people']]
                    var length = newArr.length > 4 ? 4 : newArr.length;
                    for(var count=0; count<length; count++){

                        var part = [monthNameSmall[newArr[count]._id - 1]+'-'+newArr[count].year[0],newArr[count].count,newArr[count].users.length];
                        arr.push(part);
                    }

                    if(arr.length > 1)
                      drawVisualization(arr)
                }
            }
        });
    }

    $("body").on("click","#nonInteracted",function(){
        selectNonInteractedContacts();
    });

    $("body").on("click", "#go-to-Contacts", function(){
        selectNonInteractedContacts();
    });

    function selectNonInteractedContacts(){

        $.ajax({
            url:"/contacts/nonInteracted/emails",
            type:"POST",
            datatype:"JSON",
            data:{arrStr:interactedArrStr},
            success:function(response){
                if(response)
                    window.location.assign('/contacts');
            }
        });
    }

    /* Global interaction map content */
    function showUserLocation() {

        geocoder = new google.maps.Geocoder();
        myLatlng = new google.maps.LatLng(13.5333, 2.0833);

        mapOptions = {
            center: myLatlng,
            zoom: 2
        };

        map = new google.maps.Map(document.getElementById("map"), mapOptions);

        markerCluster = new MarkerClusterer(map, [], {zoomOnClick: false, minimumClusterSize: 1, styles: styles});

        interactionsCount(userProfile._id);
        google.maps.event.addListener(markerCluster, 'clusterclick', function (cluster) {
            var c = cluster.getMarkers()
            if (checkRequredField(c[0])) {
                showGlobalInteractionInfo(c);
            }
        });

        google.maps.event.addListener(map, 'zoom_changed', function () {
            if (map.getZoom() < 2) map.setZoom(2);
        });
    }

    var time = 0;

    function interactionsCount(userId) {

        $.ajax({
            url: '/interactionsCount/individual/' + userId,
            type: 'GET',
            success: function (individualCounts) {

                if (checkRequredField(individualCounts) && individualCounts.length > 0) {
                    for (var interaction = 0; interaction < individualCounts.length; interaction++) {
                        if (checkRequredField(individualCounts[interaction]._id)) {
                            var result = individualCounts[interaction]._id;
                            result.count = individualCounts[interaction].count;
                            showLocations(result);
                        }
                    }
                }
            }
        })
    }


    $("body").on("click", "#closePopup-googleMeetingInfo", function () {
        $("#mapPopup").popover('destroy');
    });

    var b = 0;
    function showLocations(result) {
        if (checkRequredField(result) && result.count !=0) {
            if (checkRequredField(result.locationLatLang)) {
                if (checkRequredField(result.locationLatLang.latitude) && checkRequredField(result.locationLatLang.longitude)) {
                    createMarkerLocationLatLang(result, result.locationLatLang.latitude, result.locationLatLang.longitude);
                } else if (checkRequredField(result.location)) {
                    createMarkerGeocode(result, 0);
                } else  createMarkerCurrentLocation(result)
            } else if (checkRequredField(result.location)) {
                createMarkerGeocode(result, 0);
            } else  createMarkerCurrentLocation(result)
        }
    }

    function updateLocationLatLang(latLangObj) {
        $.ajax({
            url: '/updateLocationLatLang',
            type: 'POST',
            datatype: 'JSON',
            data: latLangObj,
            success: function (response) {

            }
        })
    }

    function createMarkerCurrentLocation(result) {
        if (checkRequredField(result.currentLocation)) {
            if (checkRequredField(result.currentLocation.latitude)) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(result.currentLocation.latitude, result.currentLocation.longitude),
                    map: map,
                    title: "Meeting location: " + result.currentLocation.city || '',
                    user: result
                });
                markerCluster.addMarker(marker)
            }
        }
    }

    function createMarkerGeocode(result, limit) {
        geocoder.geocode({ 'address': result.location}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var newAddress = results[0].geometry.location;
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(parseFloat(newAddress.lat()), parseFloat(newAddress.lng())),
                    map: map,
                    title: "Meeting location: " + result.location || '',
                    user: result
                });
                var latLangObj = {
                    userId: result._id,
                    latitude: newAddress.lat(),
                    longitude: newAddress.lng()
                };
                markerCluster.addMarker(marker);
                updateLocationLatLang(latLangObj);

            } else if (status == 'OVER_QUERY_LIMIT') {
                time += 500;
                limit++;
                if (limit < 3) {
                    setTimeout(function () {
                        createMarkerGeocode(result, limit);
                    }, time);
                }
            }
        });
    }

    function createMarkerLocationLatLang(result, lat, lang) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(lat), parseFloat(lang)),
            map: map,
            title: "Meeting location: " + result.location || '',
            user: result
        });
        markerCluster.addMarker(marker);
    }


    function showGlobalInteractionInfo(cluster) {
        $("#mapPopup").popover('destroy');
        var infowindow = new google.maps.InfoWindow({
            content: '<div id="content"></div>'
        });
        var infoContent = cluster;
        var totalInteractionCount = 0;
        var mmmmm = ''
        for (var m = 0; m < infoContent.length; m++) {
            totalInteractionCount += parseInt(infoContent[m].user.count || 0);
            var name = infoContent[m].user.firstName + ' ' + infoContent[m].user.lastName;
            var totalm = infoContent[m].user.count < 10 ? '0' + infoContent[m].user.count : infoContent[m].user.count;
            var userUrl = '/' + getValidUniqueUrl(infoContent[m].user.publicProfileUrl)
            var summaryUrl = '/interactions/summary/' + infoContent[m].user._id;
            mmmmm += '<tr><td style="width: 30%" title=' + name.replace(/\s/g, '&nbsp;') + '><a style="color:#333333" href=' + userUrl + '>' + getTextLength(name, 11) + '</a></td><td style="text-align: center;width: 30%"><a style="color:#333333" href=' + summaryUrl + '>' + totalm + '</a></td></tr>'
        }

        var html = _.template($("#googleMeetingInfo-popup").html())();
        $("#mapPopup").popover({
            title: "Interactions " + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#mapPopup").popover('show');
        $(".popover").css({'margin-top': '10%'})
       // $(".popover").css({'max-width': '20%'})

        $(".arrow").addClass("invisible")
        var totalPeople = infoContent.length;
        $("#totalPeople").text(totalPeople < 10 ? '0' + totalPeople : totalPeople);
        $("#totalMeetings").text(totalInteractionCount < 10 ? '0' + totalInteractionCount : totalInteractionCount);
        $("#gTable tbody").html(mmmmm);
    }


    $("body").on("click", "#close", function () {
        $("#mapPopup").popover('destroy');
    });
    /* Global interaction map content */


    function getValidUniqueUrl(uniqueName) {
        var url = window.location + ''.split('/');
        var patt = new RegExp(url[2]);
        if (patt.test(uniqueName)) {
            url = uniqueName.split('/');
            for (var i = url.length - 1; i >= 0; i--) {
                if (url[i] == 'u') {
                    uniqueName = url[i + 1];
                }
            }
            return uniqueName;
        } else {
            return uniqueName;
        }
    }


    function checkRequredField(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }
});