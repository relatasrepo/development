$(document).ready(function(){
 
        for (i = new Date().getFullYear(); i > 1900; i--) {
            $('#year').append($('<option />').val(i).html(i));
        }
   
        for (i = 1; i < 32; i++) {
            $('#day').append($('<option />').val(i).html(i));
        }
        
        for (i = 0; i < 12; i++) {
            var months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
            var monthNum = [1,2,3,4,5,6,7,8,9,10,11,12];
            $('#month').append($('<option />').val(monthNum[i]).html(months[i]));
        }

        for (var i = 0; i < 24; i++) {
            var timeArray = ['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'];

            var timeArrayHr = ['00:00 hr','01:00 hr','02:00 hr','03:00 hr','04:00 hr','05:00 hr','06:00 hr','07:00 hr','08:00 hr','09:00 hr','10:00 hr','11:00 hr','12:00 hr','13:00 hr','14:00 hr','15:00 hr','16:00 hr','17:00 hr','18:00 hr','19:00 hr','20:00 hr','21:00 hr','22:00 hr','23:00 hr'];

            $('#startTime1').append($('<option />').val(timeArray[i]).html(timeArrayHr[i]));
            $('#endTime1').append($('<option />').val(timeArray[i]).html(timeArrayHr[i]));
            $('#startTime2').append($('<option />').val(timeArray[i]).html(timeArrayHr[i]));
            $('#endTime2').append($('<option />').val(timeArray[i]).html(timeArrayHr[i]));

        }

    for (var i = 0; i < 25; i++) {

        var timeArray2 = ['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00','23:59'];

        var timeArrayHr2 = ['00:00 hr','01:00 hr','02:00 hr','03:00 hr','04:00 hr','05:00 hr','06:00 hr','07:00 hr','08:00 hr','09:00 hr','10:00 hr','11:00 hr','12:00 hr','13:00 hr','14:00 hr','15:00 hr','16:00 hr','17:00 hr','18:00 hr','19:00 hr','20:00 hr','21:00 hr','22:00 hr','23:00 hr','23:59 hr'];

        $('.times').append($('<option />').val(timeArray2[i]).html(timeArrayHr2[i]));
    }
    $('.times2').val('23:59');

});
