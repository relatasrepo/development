
document.addEventListener("DOMContentLoaded", function(event) {
    var label = ["Jul", "Aug", "Sep"];
    var label2 = ["Oct", "Nov", "Dec"];
    var label3 = ["Jan", "Feb", "Mar"];
    var label4 = ["Apr", "May", "Jun"];

    var won = [{meta:"won",value:5}, {meta:"won",value:9}, {meta:"won",value:7}, {meta:"won",value:8}];
    var won2 = [{meta:"won",value:2}, {meta:"won",value:4}, {meta:"won",value:6}, {meta:"won",value:5}];
    var won3 = [{meta:"won",value:3}, {meta:"won",value:3}, {meta:"won",value:2}, {meta:"won",value:6}];
    var won4 = [{meta:"won",value:1}, {meta:"won",value:1}, {meta:"won",value:2}, {meta:"won",value:2}];

    var pipeline = [2, 5, 8, 8];
    var pipeline2 = [2, 4, 7, 8];
    var pipeline3 = [1, 3, 2, 9];
    var pipeline4 = [3, 1, 5, 8];

    var target = [1, 2, 3, 4];
    var target2 = [1, 2, 3, 4];
    var target3 = [1, 2, 3, 4];
    var target4 = [1, 2, 3, 4];

    drawChart(label,won,target,pipeline,".ct-chart-q1")
    drawChart(label2,won2,target2,pipeline2,".ct-chart-q2")
    drawChart(label3,won3,target3,pipeline3,".ct-chart-qcurrent")
    drawChart(label4,won4,target4,pipeline4,".ct-chart-qnext")
});

function myFunction() {

    var x = document.getElementById("moreBtns");
    if (x.style.display === "none") {
        x.style.display = "inline-block";
    } else {
        x.style.display = "none";
    }
}

function drawChart(label,data,target,pipeline,id) {
    new Chartist.Line(id, {
        labels: label,
        series: [data,target,pipeline]
    }, {
        low: 0,
        showArea: true,
        plugins: [
            Chartist.plugins.tooltip()
        ]
    });
}
