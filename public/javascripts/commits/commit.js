var notificationCategory = getParams(window.location.href).notifyCategory;
var notificationDate = getParams(window.location.href).notifyDate;

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("commit", function($scope,$http,share,$rootScope,searchService){

    $scope.getCommitDataBy = function(){
        $scope.dataBy = "org";
        share.selectedPortfolio = null;
        $scope.canViewSummary = false;
        $scope.showByPa = false;

        _.each($scope.groupedPortfolios,function (po) {
            _.each(po.data,function (el) {
                el.selected = false;
            })
        });

        setTimeOutCallback(1000,function () {
            $(".table-review").css("top", "98px");
            $(".action-board").css("top", "98px");
        });
        mixpanelTracker("Commits by Org");
        share.initCommits();
    }

    $scope.getDataBySomething = function(data,range,dontOpenPopup){
        $scope.canViewSummary = false;
        $scope.selectedEmailId = null;
        $scope.headEmailId = null;

        if(!range){
            if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
                range = $scope.selectedCommitRange.name
            }
        }

        data.selected = data.name;

        var portTarget = {};
        share.portTargetsFy = {};
        var portWon = {};
        var portCommit = {};
        var head = null;
        $scope.portfolioNameSelected = data.name;

        $scope.teamList = [];
        var teamList = [];
        if(!dontOpenPopup){
            $scope.showByPa = true;
        }

        share.selectedPortfolio = data;

        if(data && data.users && data.users[0]){
            _.each(data.users,function(user){
                if(user.isHead){
                    head = user.ownerEmailId;
                    $scope.setSummaryAccess(null,head)
                };

                user.targets[0].values.sort(function (o1, o2) {
                    return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
                });

                portWon[user.ownerEmailId] = user.wonAmtMonth;
                portCommit[user.ownerEmailId] = user.commitMonth;

                if(range == "Week"){
                    portWon[user.ownerEmailId] = user.wonAmtWeek
                    portCommit[user.ownerEmailId] = user.commitWeek;
                }

                if(range == "Quarter"){
                    portWon[user.ownerEmailId] = user.wonAmtQtr
                    portCommit[user.ownerEmailId] = user.commitQtr;
                }

                share.portTargetsFy[user.ownerEmailId] = user.targets[0].values

                _.each(user.targets,function(tr){
                    _.each(tr.values,function(va){

                        if(!portTarget[user.ownerEmailId]){
                            portTarget[user.ownerEmailId] = 0;
                        }

                        if(range == "Quarter"){
                            if(new Date(va.date) >= new Date(user.qStart) && new Date(va.date) <= new Date(user.qEnd)){
                                portTarget[user.ownerEmailId] = portTarget[user.ownerEmailId]+va.amount
                            }
                        } else {

                            if(moment(va.date).format("MMM YYYY") == $scope.currentMonth){
                                portTarget[user.ownerEmailId] = va.amount
                            }
                        }
                    });
                });

                teamList.push(_.cloneDeep(share.usersDictionary[user.ownerEmailId]));
            });

            $scope.totalTarget = 0;
            $scope.totalTargetNum = 0;
            $scope.totalCommit = 0;
            $scope.totalGap = 0;
            var commitCount = 0;

            teamList = _.compact(teamList);

            _.each(teamList,function(tm){

                tm.title = tm.fullName +", "+tm.emailId;
                if(head && tm.emailId == head){
                    tm.fullName = tm.fullName;
                    tm.underlineStyle = "text-decoration: underline;";
                    tm.title = tm.fullName +", "+tm.emailId+" - Portfolio Head"
                }

                tm.wonPercentage = 0+'%';
                tm.wonPercentageStyle = {'width':tm.wonPercentage,background: '#8ECECB',height:"inherit","max-width": "100%"};

                if(portCommit && portCommit[tm.emailId]){
                    tm.commit = getAmountInThousands(portCommit[tm.emailId],2,share.primaryCurrency=="INR");
                    tm.commitSort = portCommit[tm.emailId];

                    if(portCommit[tm.emailId]){
                        commitCount++
                    }

                } else {
                    tm.commit = 0;
                    tm.commitSort = 0;
                    tm.commitPercentage = "0%";
                    tm.commitPercentageStyle = {
                        background: "#638ca6",
                        height: "inherit",
                        width: "0%"
                    };
                }

                if(share.commitObj && share.commitObj[tm.emailId]){
                    tm.targetAndAchievement = share.commitObj[tm.emailId].targetAndAchievement;
                }

                if(!tm.targetAndAchievement){
                    tm.targetAndAchievement = {}
                }

                if(portTarget[tm.emailId] && tm.commit){

                    var achv = tm.commitSort/portTarget[tm.emailId]*100;
                    var forPerc = achv;
                    if(achv>100){
                        forPerc = 100;
                    };

                    tm.commitPercentage = Math.round(achv).r_formatNumber(2)+'%';
                    tm.commitPercentageStyle = {
                        background: "#638ca6",
                        height: "inherit",
                        width: Math.round(forPerc).r_formatNumber(2)+'%'
                    };
                }

                if(portWon[tm.emailId] && portTarget[tm.emailId]){
                    tm.wonPercentage = Math.round((portWon[tm.emailId]/portTarget[tm.emailId])*100).r_formatNumber(2)+'%';
                    tm.wonPercentageStyle = {background: "#8ECECB", height: "inherit", "max-width": "100%", width: tm.wonPercentage}
                }

                tm.targetAndAchievement.wonAmt = portWon[tm.emailId]?portWon[tm.emailId]:0;
                tm.targetAndAchievement.wonWithCommas = getAmountInThousands(tm.targetAndAchievement.wonAmt,2,share.primaryCurrency=="INR");

                if(portWon[tm.emailId] && !portTarget[tm.emailId]){
                    tm.wonPercentage = '100%';
                    tm.wonPercentageStyle = {background: "#8ECECB", height: "inherit", "max-width": "100%", width: '100%'}
                }

                if(!share.commitObj[tm.emailId]){
                    share.commitObj[tm.emailId] = {
                        commitSort: 0
                    }
                }

                tm.commit = tm.commit?tm.commit:0;
                tm.commitPercentage = tm.commitPercentage?tm.commitPercentage:0;
                tm.commitPercentageStyle = tm.commitPercentageStyle?tm.commitPercentageStyle:{background: "#638ca6", height: "inherit", width: "0%"};

                if(range !== "Week"){
                    tm.targetAndAchievement.target = getAmountInThousands(portTarget[tm.emailId],2,share.primaryCurrency=="INR");
                } else {
                    tm.targetAndAchievement.target = 0;
                }

                $scope.totalTarget = $scope.totalTarget+(portTarget[tm.emailId]?portTarget[tm.emailId]:0);
                $scope.totalCommit = $scope.totalCommit+share.commitObj[tm.emailId].commitSort;
            });

            $scope.teamList = teamList;

            var commitPer = "0%";
            $scope.gapCss = "high";

            $scope.totalGap = $scope.totalCommit-$scope.totalTarget;

            if($scope.totalTarget && $scope.totalCommit){
                commitPer = (($scope.totalCommit/$scope.totalTarget)*100).toFixed()+"%";
            }

            if($scope.totalGap<0){
                $scope.gapCss = "low"
            }

            $scope.totalTargetNum = $scope.totalTarget;
            $scope.totalTarget = getAmountInThousands($scope.totalTarget,2,share.primaryCurrency=="INR")
            $scope.totalCommit = getAmountInThousands($scope.totalCommit,2,share.primaryCurrency=="INR")+" ("+commitPer+")"
            $scope.totalGap = getAmountInThousands($scope.totalGap,2,share.primaryCurrency=="INR")
            var teamCommitPerc = ((commitCount/$scope.teamList.length)*100).toFixed()+"%";
            $scope.teamCommitPerc = commitCount+"/"+$scope.teamList.length+" ("+teamCommitPerc+")";

        }

        getSummary($scope,$http,share,$rootScope);
    }

    $scope.getAllPortfolios = function(){
        $http.get("/user/get/portfolios")
            .success(function (response) {

                $scope.groupedPortfolios = _
                    .chain(response.reportees)
                    .groupBy('_id.type')
                    .map(function(values, key) {

                        var data = [];
                        _.each(values,function(va){
                            data.push({
                                name:va._id.name,
                                users:va.users
                            })
                        });

                        var sort = 1;

                        if(key == "Regions"){
                            sort = 2
                        }

                        if(key == "Verticals"){
                            sort = 3
                        }

                        if(key == "Business Units"){
                            sort = 4
                        }

                        data.sort(function (a,b) {
                            if(a.name < b.name) return -1;
                            if(a.name > b.name) return 1;
                            return 0;
                        })

                        return  {
                            type:key,
                            data:data,
                            sort:sort
                        }

                    })
                    .value();

                $scope.groupedPortfolios.sort(function (o1,o2) {
                    return o1.sort > o2.sort ? 1 : o1.sort < o2.sort ? -1 : 0;
                });

                $scope.widthStyle = "width:200px;left: -85px;"
                $scope.colClass = "col-xs-12";

                if($scope.groupedPortfolios.length == 2){
                    $scope.widthStyle = "width:325px;left: -175px;"
                    $scope.colClass = "col-xs-6";
                }

                if($scope.groupedPortfolios.length == 3){
                    $scope.widthStyle = "width:425px;left: -250px;"
                    $scope.colClass = "col-xs-4";
                }

                if($scope.groupedPortfolios.length == 4){
                    $scope.widthStyle = "width:575px;left: -285px;";
                    $scope.colClass = "col-xs-3";
                }

                share.portfolioDetails = {
                    groupedPortfolios: _.cloneDeep($scope.groupedPortfolios),
                    widthStyle: _.cloneDeep($scope.widthStyle),
                    colClass: _.cloneDeep($scope.colClass),
                    qStart: response.qStart,
                    qEnd: response.qEnd
                }

            });
    }

    $scope.getAllPortfolios();

    share.loadPortfolios = function(company,reportees){

        function checkTeamLoaded(){
            if($scope.teamList){
                buildPortfolioList(reportees);
            } else {
                setTimeOutCallback(1000,function () {
                    checkTeamLoaded();
                })
            }
        }

        checkTeamLoaded()

        function buildPortfolioList(reportees){

            $scope.portfolios = [];
            if(company.productList && company.productList.length>0){
                _.each(company.productList,function (el) {
                    if(_.includes(reportees,el.headEmailId)){
                        $scope.portfolios.push({
                            name:el.name,
                            type:"productType",
                            type_format:"Products"
                        })
                    }
                });
            }

            if(company.verticalList && company.verticalList.length>0){
                _.each(company.verticalList,function (el) {
                    if(_.includes(reportees,el.headEmailId)){
                        $scope.portfolios.push({
                            name:el.name,
                            type:"vertical",
                            type_format:"Verticals"
                        })
                    }
                });
            }

            if(company.businessUnits && company.businessUnits.length>0){
                _.each(company.businessUnits,function (el) {
                    if(_.includes(reportees,el.headEmailId)){
                        $scope.portfolios.push({
                            name:el.name,
                            type:"businessUnit",
                            type_format:"Business Units"
                        })
                    }
                });
            }

            if(company.geoLocations && company.geoLocations.length>0){
                _.each(company.geoLocations,function (el) {
                    if(_.includes(reportees,el.headEmailId)){
                        $scope.portfolios.push({
                            name:el.region,
                            type:"region",
                            type_format:"Regions"
                        })
                    }
                });
            }

            $scope.groupedFilters = _
                .chain($scope.portfolios)
                .groupBy('type')
                .map(function(values, key) {
                    return {
                        type:key,
                        type_format:values[0]?values[0].type_format:"",
                        values:values.sort(function (a,b) {
                            if(a.name < b.name) return -1;
                            if(a.name > b.name) return 1;
                            return 0;
                        })
                    };

                })
                .value();

        }
    }

    $scope.isAllSelected = function(portfolio){
        if(portfolio){
            var allselected = true;
            portfolio.values.forEach(function (va) {
                if(!va.selected){
                    allselected = false
                }
            })

            portfolio.selected = allselected;
        }
    }

    $scope.openPortfolios = function(){
        $scope.displayPortfolios = true;
        $scope.showByPa = true;
        mixpanelTracker("Commits by portfolios");
    }

    closeAllDropDownsAndModals($scope,".list-unstyled");
    closeAllDropDownsAndModals($scope,".portfolio-ls",null,share);

    $scope.selectAll = function(type){
        if(type && type.selected){
            _.each(type.values,function (va) {
                va.selected = true;
            });
        } else {
            _.each(type.values,function (va) {
                va.selected = false;
            });
        }
    }

    $scope.dataBy = "org";

    $scope.closePa = function(){
        $scope.showByPa = false;
        if(!share.selectedPortfolio){
            $scope.dataBy = "org";
        }
    }

    $scope.getOppsBasedOnPortfolios = function(){

        var portfolios = [];
        _.each($scope.groupedFilters,function (el) {
            el.values.forEach(function (po) {
                if(po.selected){
                    portfolios.push(po.name+"_type_"+po.type)
                }
            })
        });

        if(portfolios.length>0){
            $scope.portfolioSelected = "portfolioSelected";
        } else {
            $scope.portfolioSelected = "";
        }

        if($scope.teamCommitsViewing){
            $scope.viewTeamCommits(portfolios,true)
        } else {
            $scope.viewAllMyAccess([$scope.owner.userId],portfolios);
        }
    };

    $scope.closing = {
        this:"all"
    }

    $scope.getDataForRange = function(range){
        $scope.selectedCommitRange = range;
        $scope.seeReview($scope.owner)
        getSummary($scope,$http,share,$rootScope);
    }

    $scope.goToAccount = function(accountName){
        window.location = "/accounts/all?accountName="+accountName
    }

    $scope.closeOppInsightsModal = function(){
        $scope.showOppInsights = false;
        $rootScope.oppTabView = false;
        $rootScope.regionTabView = false;
        $scope.rolesList = [];
    };

    $scope.commitMovement = function(pastMonthCommit){
        commitMovement($scope,$http,share,$rootScope,pastMonthCommit)
    }

    $scope.goToTeamTab = function(op){
        $scope.getInteractionHistory(op,'internalTeam');
    }

    $scope.showOppsFor = function(user){
        var flip = false;
        if(!$scope.selectedUser){
            flip = true;
        }

        if(!$scope.selectedUser || ($scope.selectedUser && $scope.selectedUser.emailId != user.emailId)) {
            flip = true
        } else {
            flip = false
        }

        $scope.selectedUser = user;

        getOppsFromLocalStorage(function (oppsInCommitStage) {
            if(flip){
                if(oppsInCommitStage && oppsInCommitStage.length>0){
                    $scope.oppsInCommitStage = oppsInCommitStage.filter(function (op) {
                        return op.userEmailId == user.emailId;
                    })
                }
            } else {
                $scope.oppsInCommitStage = oppsInCommitStage;
            }
        });

        $('.action-board').animate({scrollTop: '+=450px'}, 500);
    }

    $scope.viewAllMyAccess = function(userIds,portfolios){

        if(portfolios){

        } else {

            $scope.allMyAccess = true;
            $scope.selfSelection = "";
            $scope.teamSelection = "";
            $scope.allSelection = "btn-selected";
        }

        var url = "/review/all/my/access/opps";
        var emailIds = share.teamDictionaryByUserId[$scope.owner.userId].emailId;

        if(userIds){
            emailIds = [];
            _.each(userIds,function (userId) {
                emailIds.push(share.teamDictionaryByUserId[userId].emailId)
            })
            url = fetchUrlWithParameter(url,"userIds",userIds);
        } else {
            url = fetchUrlWithParameter(url,"userIds",$scope.owner.userId);
        }

        url = fetchUrlWithParameter(url,"userEmailId",emailIds);

        if(portfolios){
            url = fetchUrlWithParameter(url, "portfolios", portfolios);
        }

        $http.get(url)
            .success(function (response) {
                window.localStorage['oppsInCommitStage'] = "notSet";

                if(response && response.SuccessCode){

                    $scope.oppsInCommitStage = response.Data.opps;
                    _.each($scope.oppsInCommitStage,function (op) {

                        op.closingThisWeek = {background:"inherit"};
                        op.riskMeter = response.Data.dealsAtRisk && response.Data.dealsAtRisk[op.opportunityId]?response.Data.dealsAtRisk[op.opportunityId]*100:0

                        if(new Date(op.closeDate)>= new Date(moment().startOf("month")) && new Date(op.closeDate) <= new Date(moment().endOf("month"))){
                            op.closingThisWeek = {
                                background:"#f2f2f2"
                            }
                        }
                        op = formatOpp(op,share,$scope)
                    });

                    window.localStorage['oppsInCommitStage'] = JSON.stringify($scope.oppsInCommitStage);

                    if(portfolios){

                    } else {

                        $scope.closing = {
                            this:"range"
                        }
                    }

                    $scope.filterOpps($scope.closing.this);
                }
            });
    }

    $scope.oppRenewalStatusSet = function (opp) {
        opp.renewalStatusSet = true
    }

    $scope.selectContact = function(contact){
        $scope.opp.searchContent  = contact.fullName + " ("+contact.emailId+")"
        $scope.searchContent = contact.fullName + " ("+contact.emailId+")";
        $scope.newOppContact = contact;
        $scope.showResultscontact = false;
        $scope.opp.contactEmailIdReq = false;

        if($scope.isExistingOpp ||$scope.opp.contactEmailId){
            $scope.opp.contactEmailId = contact.emailId
            $scope.opp.mobileNumber = contact.mobileNumber
        }
    }

    $scope.filterOpps = function(filter,removeOppForUser){
        mixpanelTracker("Commits>opps>filter");
        try {
            checkLocalStorageSet();

            function checkLocalStorageSet(){

                $scope.commitPipeline = 0;
                var oppsInCommitStage = JSON.parse(window.localStorage['oppsInCommitStage']);

                if(oppsInCommitStage !== "notSet"){

                    if(filter == 'range'){
                        $scope.oppsInCommitStage = oppsInCommitStage.filter(function (op) {
                            return op.closingThisWeek && op.closingThisWeek.background !== "inherit"
                        })
                    } else {
                        $scope.oppsInCommitStage = oppsInCommitStage;
                    }

                    if(removeOppForUser){
                        $scope.oppsInCommitStage = $scope.oppsInCommitStage.filter(function (op) {
                            return !removeOppForUser[op.userEmailId];
                        })
                    }

                    if($scope.oppsInCommitStage && $scope.oppsInCommitStage.length>0){
                        _.each($scope.oppsInCommitStage,function (op) {
                            $scope.commitPipeline = $scope.commitPipeline+op.amountWithNgm;
                        })
                    }

                    $scope.commitPipeline = getAmountInThousands($scope.commitPipeline,2,share.primaryCurrency=="INR");

                } else {
                    setTimeOutCallback(300,function () {
                        checkLocalStorageSet();
                    })
                }
            };
        } catch (e) {
            $scope.oppsInCommitStage = [];
            console.log("error", e);
        }

        $scope.closing.this = filter;
    }

    $scope.commitForNextRanges = function(){
        $scope.committingForNext = !$scope.committingForNext;
        if($scope.committingForNext){
            $rootScope.weeklyCommitCutOff = "Commit Close Date: "+moment(moment(share.commitCutOffObj.week).add(1,'week')).tz("UTC").format(standardDateFormat())
            $rootScope.monthlyCommitCutOff = "Commit Close Date: "+moment(moment(share.commitCutOffObj.month).add(1,'month')).tz("UTC").format(standardDateFormat())
            $rootScope.quarterlyCommitCutOff = "Commit Close Date: "+moment(moment(share.commitCutOffObj.quarter.startOfQuarter).add(2,"month")).tz("UTC").format(standardDateFormat());

            $scope.loadingData = true;

            $http.get("/review/next/commits?range="+$scope.selectedCommitRange.name)
                .success(function (response) {

                    $scope.next = {
                        selfCommitValue: 0
                    }
                    if(response && response.Data){
                        $scope.next = {
                            selfCommitValue: response.Data.month.userCommitAmount
                        }

                        if($scope.selectedCommitRange.name == "Week"){
                            $scope.next = {
                                selfCommitValue: response.Data.week.userCommitAmount
                            }
                        }

                        if($scope.selectedCommitRange.name == "Quarter"){
                            $scope.next = {
                                selfCommitValue: response.Data.quarter.userCommitAmount
                            }
                        }
                    }
                    $scope.loadingData = false;
                });

        } else {
            $rootScope.weeklyCommitCutOff = "Commit Close Date: "+moment(share.commitCutOffObj.week).tz("UTC").format(standardDateFormat())
            $rootScope.monthlyCommitCutOff = "Commit Close Date: "+moment(share.commitCutOffObj.month).tz("UTC").format(standardDateFormat())
            $rootScope.quarterlyCommitCutOff = "Commit Close Date: "+moment(share.commitCutOffObj.quarter.startOfQuarter).tz("UTC").format(standardDateFormat())
        }
    }

    $scope.sortType = "fullName"
    $scope.sortReverse = false;

    $scope.oppSortType = "closeDate"
    $scope.oppSortReverse = true;
    $scope.teamCommitsViewing = false;

    $scope.loadingData = true;
    $scope.actionloadingData = true;
    $scope.currentMonth = moment().format('MMM YYYY');

    $scope.initAutocomplete = function(){

        checkDOMLoaded();

        function checkDOMLoaded(){
            if(document.getElementById('autocompleteCity')){

                var autocomplete2 = new google.maps.places.Autocomplete(
                    (document.getElementById('autocompleteCity')),
                    {types:['(cities)']});
                autocomplete2.addListener('place_changed', function(err,places){

                    if(!$scope.opp){
                        $scope.opp = {}
                    }

                    if(!$scope.opp.geoLocation){
                        $scope.opp.geoLocation = {}
                    }

                    $scope.opp.geoLocation.lat = autocomplete2.getPlace().geometry.location.lat()
                    $scope.opp.geoLocation.lng = autocomplete2.getPlace().geometry.location.lng()

                    $scope.opp.geoLocation.town = $("#autocompleteCity").val();

                    if(share.setTown){
                        share.setTown(null)
                    }

                    if($("#autocompleteCity").val() && $("#autocompleteCity").val() != "" && $("#autocompleteCity").val() != " "){
                        if(share.setTown){
                            share.setTown($("#autocompleteCity").val())
                        }
                    }
                    return false;
                });
            } else {
                setTimeOutCallback(100,function () {
                    checkDOMLoaded();
                })
            }
        }
    }

    // autoInitGoogleLocationAPI(share,$scope);

    $scope.initAutocomplete();

    $scope.getAllCommitCutOffDates = function () {

        $http.get("/review/get/all/commit/cutoff")
            .success(function (response) {
                if(response){
                    share.commitCutOffObj = response;
                    $rootScope.weeklyCommitCutOff = "Commit Close Date: "+moment(response.week).tz("UTC").format(standardDateFormat())
                    $rootScope.monthlyCommitCutOff = "Commit Close Date: "+moment(response.month).tz("UTC").format(standardDateFormat())
                    $rootScope.quarterlyCommitCutOff = "Commit Close Date: "+moment(response.quarter.startOfQuarter).tz("UTC").format(standardDateFormat())
                    
                }
            })
    }

    getOppStages();

    function getOppStages(){

        if(share.opportunityStages){
            $scope.stagesSelection = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(500,function () {
                getOppStages()
            })
        }
    }

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.removeRecipient = function(contact,type){
        if(!$scope.opp.isNotOwner){
            removeRecipient($scope,$http,contact,type)
        }
    }

    $scope.reCalClosingThisSelection = function (user) {
    }

    $scope.selectedUserValues = function (user) {

        if(user.selected){
            if(isNumber(user.oppsValueUnderCommitStageSort)){
                $scope.oppValueSelected = $scope.oppValueSelected+user.oppsValueUnderCommitStageSort;
            }

            if(isNumber(user.valueSort)){
                $scope.commitValueSelected = $scope.commitValueSelected+user.valueSort;
            }

            if(isNumber(user.target)){
                $scope.target = $scope.target+user.target;
            }

            if(isNumber(user.won)){
                $scope.achievement = $scope.achievement+user.won;
            }

            if(user.oppsValClosingThisSelection){
                $scope.closingThisSelection.num = $scope.closingThisSelection.num+user.oppsValClosingThisSelection;
            }

        } else {
            $scope.target = $scope.target-user.target;
            $scope.achievement = $scope.achievement-user.won;
            $scope.commitValueSelected = $scope.commitValueSelected-user.valueSort;
            $scope.oppValueSelected = $scope.oppValueSelected-user.oppsValueUnderCommitStageSort;
            if(user.oppsValClosingThisSelection){
                $scope.closingThisSelection.num = $scope.closingThisSelection.num-user.oppsValClosingThisSelection;
            }
        }

        $scope.targetForDisplay = getAmountInThousands($scope.target,2,share.primaryCurrency=="INR")
        $scope.achievementForDisplay = getAmountInThousands($scope.achievement,2,share.primaryCurrency=="INR")
        $scope.commitValueSelectedForDisplay = getAmountInThousands($scope.commitValueSelected,2,share.primaryCurrency=="INR")
        $scope.oppValueSelectedForDisplay = getAmountInThousands($scope.oppValueSelected,2,share.primaryCurrency=="INR");
        $scope.closingThisSelection.formatted = getAmountInThousands($scope.closingThisSelection.num,2,share.primaryCurrency=="INR")
        $scope.oppsInCommitStage = [];

        var removeObj = {};
        _.each($scope.commitsByUsers,function (co) {
            if(co.selected === false){
                removeObj[co.profile.emailId] = true;
            }
        });

        var range = "range";
        if($scope.closing && $scope.closing.this){
            range = $scope.closing.this;
        }

        // $scope.filterOpps(range,removeObj);
    }

    $scope.getAllCommitCutOffDates();
    $scope.selfSelection = "btn-selected";
    $scope.allSelection = "";
    $scope.teamSelection = "";

    $scope.viewSelfCommits = function () {
        $scope.teamCommitsViewing = false;
        $scope.allMyAccess = false;
        $scope.selfSelection = "btn-selected";
        $scope.teamSelection = "";
        $scope.allSelection = "";
        $scope.oppSortType = "closeDateSort"
        $scope.oppSortReverse = true;
        $scope.seeReview($scope.owner);
    }

    $scope.assignTaskOpp = function(op){
        $scope.getInteractionHistory(op,'tasks');
    }

    $scope.closeTeamTask = function(user){
        $scope.teamTask = false;
    }

    $scope.assignTaskTeamMember = function(user){
        $scope.teamTask = true;
        var url = '/tasks/get/all'
        url = url+"?emailId="+user.emailId+"&filter="+"weeklyReview"
        initServices($scope, $http,$rootScope,share,searchService);

        $http.get(url)
            .success(function (response) {
                if (response && response.SuccessCode){
                    getTasks($scope,response.Data.tasks);
                }
            })
    }

    $scope.viewTeamCommits = function (portfolios,donNotRebuild) {
        mixpanelTracker("Commits for team");
        $scope.dataBy = "org";
        share.selectedPortfolio = null;
        $(".table-review").css("top", "98px");
        $(".action-board").css("top", "98px");
        $scope.teamCommitsViewing = true;
        $scope.allMyAccess = false;
        $scope.selfSelection = "";
        $scope.allSelection = "";
        $scope.teamSelection = "btn-selected";
        $scope.oppsInCommitStage = [];
        var users = $scope.owner.children.teamMatesUserId;
        users.push($scope.owner.userId);
        users = _.uniq(users);
        var url = '/review/commits/month/team';
        url = fetchUrlWithParameter(url,"shType",$scope.selectedHierarchy.name.replace(/[^A-Z0-9]+/ig, "_"));
        $scope.loadingData = true;
        $scope.actionloadingData = true;

        if(!donNotRebuild){
            share.loadPortfolios($rootScope.companyDetails,$scope.owner.children.teamMatesEmailId,true);
        }

        if(portfolios){
            url = fetchUrlWithParameter(url, "portfolios", portfolios);
        }

        if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
            if($scope.selectedCommitRange.name == "Week"){
                url = '/review/commits/week/team';
                url = fetchUrlWithParameter(url,"shType",$scope.selectedHierarchy.name.replace(/[^A-Z0-9]+/ig, "_"));
                fetchCommit(null,$scope,$http,share,$rootScope,users,url,'week')
            } else if ($scope.selectedCommitRange.name == "Quarter"){
                url = '/review/commits/quarter/team';
                url = fetchUrlWithParameter(url,"shType",$scope.selectedHierarchy.name.replace(/[^A-Z0-9]+/ig, "_"));
                fetchCommit(null,$scope,$http,share,$rootScope,users,url,"quarter")
            } else {
                fetchCommit(null,$scope,$http,share,$rootScope,users,url)
            }
        } else {
            fetchCommit(null,$scope,$http,share,$rootScope,users,url)
        }
    }

    $scope.getDatForRange = function(range){
        $scope.actionloadingData = true;
        $scope.selectedHierarchy = $scope.hierarchyList[0];
        $scope.selectedCommitRange = range;
        $scope.seeReview($scope.owner);

        mixpanelTracker("Commits by range "+range.name);

        share.displayCommits(range.name,$scope.selectedHierarchy.name);

        checkCommitCutOffLoaded();
        function checkCommitCutOffLoaded(){
            if(share.commitCutOffObj){

                if(range.name.toLowerCase() == "week"){
                    $scope.currentMonth = moment(share.commitCutOffObj.week).startOf("isoWeek").format('DD MMM')+" - "+moment(share.commitCutOffObj.week).endOf("isoWeek").format('DD MMM');
                } else if(range.name.toLowerCase() == "quarter"){
                    $scope.currentMonth = moment(share.commitCutOffObj.startOfQuarter).format('MMM YYYY')+" - "+moment(moment(share.commitCutOffObj.endOfQuarter).subtract(1,'day')).format('MMM YYYY')
                } else {
                    $scope.currentMonth = moment().format('MMM YYYY');
                }
            } else {
                setTimeOutCallback(500,function () {
                    checkCommitCutOffLoaded();
                })
            }
        }
    }

    share.setDateRange = function (start,end,mode) {

        if(start){

            if(mode == "qtr"){
                // $scope.dataForRange = moment(start).format('MMM') +"-"+moment(end).format('MMM YY')
            } else {
                $scope.currentMonthSelection = {
                    start:start,
                    end:end
                }

                $scope.dataForRange = moment(start).format('MMM')
            }

        } else {
            setDateRange();
        }
    }

    $scope.closeModal = function(){
        $scope.commitModalOpen = false;
    }

    $scope.openCommitModal = function(){
        $scope.commitModalOpen = true;
        $scope.committingForNext = false;
        mixpanelTracker("Commits> Update Commits");

        var users = [share.liuData._id];
        var url = '/review/commits/month';
        $scope.loadingData = true;

        if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
            if($scope.selectedCommitRange.name == "Week"){
                url = '/review/commits/week';
                fetchCommit(share.selectedPortfolio,$scope,$http,share,$rootScope,users,url,'week')
            } else if ($scope.selectedCommitRange.name == "Quarter"){
                url = '/review/commits/quarter';
                fetchCommit(share.selectedPortfolio,$scope,$http,share,$rootScope,users,url,"quarter")
            } else {
                fetchCommit(share.selectedPortfolio,$scope,$http,share,$rootScope,users,url)
            }
        } else {
            fetchCommit(share.selectedPortfolio,$scope,$http,share,$rootScope,users,url)
        }
    }

    $scope.updateCommit = function(){

        $scope.saving = true;
        var selfCommitValue = $scope.commits.selfCommitValue;
        if($scope.committingForNext){
            selfCommitValue = $scope.next.selfCommitValue
        }

        selfCommitValue = parseFloat(selfCommitValue);

        if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
            if($scope.selectedCommitRange.name == "Week"){
                saveWeeklyCommits($scope,$rootScope,$http,share,{selfCommitValue:selfCommitValue},function () {
                    $scope.saving = false;
                    $scope.commitModalOpen = !$scope.commitModalOpen;
                    if(!$scope.committingForNext){
                        $scope.setNewCommit(selfCommitValue);
                    }
                });
            } else if ($scope.selectedCommitRange.name == "Quarter"){
                saveQuarterlyCommits($scope,$rootScope,$http,share,{selfCommitValue:selfCommitValue},function () {
                    $scope.saving = false;
                    $scope.commitModalOpen = !$scope.commitModalOpen;
                    if(!$scope.committingForNext){
                        $scope.setNewCommit(selfCommitValue);
                    }
                });
            } else {
                saveMonthlyCommit($scope,$rootScope,$http,share,{selfCommitValue:selfCommitValue},function () {
                    $scope.saving = false;
                    $scope.commitModalOpen = !$scope.commitModalOpen;
                    if(!$scope.committingForNext){
                        $scope.setNewCommit(selfCommitValue);
                    }
                });
            }
        } else {
            saveMonthlyCommit($scope,$rootScope,$http,share,function () {
                $scope.saving = false;
                $scope.commitModalOpen = !$scope.commitModalOpen;
                $scope.setNewCommit(selfCommitValue);
            });
        }
    }

    $scope.commitRanges = [{
        name: "Week"
    },{
        name: "Month"
    },{
        name: "Quarter"
    }]

    $scope.getDatForHierarchy = function(hierarchy){
        share.displayCommits($scope.selectedCommitRange.name,hierarchy.name)
    }

    share.initCommits = function(){
        if(window.location.pathname == "/team/commit/review"){
            $http.get('/company/user/all/hierarchy/types')
              .success(function (response) {
                  if(response && response.SuccessCode){
                      $scope.hierarchyList = [];

                      _.each(response.Data,function (el) {
                          $scope.hierarchyList.push({
                              name:el
                          })
                      });

                      $scope.selectedHierarchy = $scope.hierarchyList[0];
                      share.displayCommits(null,$scope.selectedHierarchy.name)
                  }
              })
        }
    };

    share.initCommits();

    $scope.stages = _.map(share.opportunityStages,"name");

    $scope.setNewCommit = function (selfCommitValue){
        if($scope.teamList && $scope.teamList.length>0){
            _.each($scope.teamList,function (tm) {
                if(tm.userId === share.liuData._id){
                    tm.commitSort = parseFloat(selfCommitValue);
                    tm.commit = getAmountInThousands(tm.commitSort,2,share.primaryCurrency=="INR");
                    if(tm.commitSort && tm.targetAndAchievement.targetOriginal){
                        var commitPercentage = scaleBetween(tm.commitSort,0,tm.targetAndAchievement.targetOriginal);
                        commitPercentage = Math.round(commitPercentage);

                        if(commitPercentage>100){
                            tm.commitPercentageStyle = {'width':'100%',background: '#638ca6',height:"inherit"}
                            tm.commitPercentage = commitPercentage.r_formatNumber(2)+'%';
                        } else {
                            tm.commitPercentageStyle = {'width':commitPercentage+'%',background: '#638ca6',height:"inherit"}
                        }
                        tm.commitPercentage = commitPercentage.r_formatNumber(2)+'%';
                    } else if(tm.commitSort){
                        tm.commitPercentageStyle = {'width':'100%',background: '#638ca6',height:"inherit"}
                    }
                }
            })
        }
    }

    share.displayCommits = function (range,hierarchy) {

        if(window.location.pathname == "/team/commit/review"){

            $scope.loadingData = true;
            var url = '/company/user/hierarchy';

            if(hierarchy && hierarchy !== "Org. Hierarchy"){
                url = '/company/users/for/hierarchy';
                url = fetchUrlWithParameter(url,"hierarchyType",hierarchy.replace(/[^A-Z0-9]+/ig, "_"))
            }

            url = fetchUrlWithParameter(url,"forCommits",true)

            if(range){
                url = fetchUrlWithParameter(url,"range",range.toLowerCase())
            }

            $http.get(url)
              .success(function (response) {

                  var team = [];
                  if(response && response.SuccessCode && response.Data && response.Data.length>0) {
                      team = buildTeamProfilesWithCommits(response.Data, response.listOfMembers, share, $scope,range?range.toLowerCase():null);
                      $scope.commitPipeline = 0;
                      $scope.totalAchievement = 0;

                      _.each(team,function (el) {
                          el.wonPercentage = 0+'%';
                          el.commitPercentage = 0+'%';
                          el.commitSort = parseFloat(el.commitSort);

                          if(el.targetAndAchievement){

                              if(!share.selectedPortfolio){
                                  $scope.totalAchievement = $scope.totalAchievement+el.targetAndAchievement.won;
                              }

                              $scope.commitPipeline = $scope.commitPipeline+parseFloat(el.targetAndAchievement.commitPipeline);

                              if(el.commitSort && el.targetAndAchievement.targetOriginal){
                                  var commitPercentage = scaleBetween(el.commitSort,0,el.targetAndAchievement.targetOriginal);
                                  commitPercentage = Math.round(commitPercentage);

                                  if(commitPercentage>100){
                                      el.commitPercentageStyle = {'width':'100%',background: '#638ca6',height:"inherit"}
                                      el.commitPercentage = commitPercentage.r_formatNumber(2)+'%';
                                  } else {
                                      el.commitPercentageStyle = {'width':commitPercentage+'%',background: '#638ca6',height:"inherit"}
                                  }
                                  el.commitPercentage = commitPercentage.r_formatNumber(2)+'%';
                              } else if(el.commitSort) {
                                  el.commitPercentageStyle = {'width':'100%',background: '#638ca6',height:"inherit"}
                                  el.commitPercentage = 100+'%';
                              }

                              if(el.targetAndAchievement.won && el.targetAndAchievement.targetOriginal){
                                  el.wonPercentage = Math.round(scaleBetween(el.targetAndAchievement.won,0,el.targetAndAchievement.targetOriginal)).r_formatNumber(2)+'%';
                              }

                              if(el.targetAndAchievement.won && !el.targetAndAchievement.targetOriginal){
                                  el.wonPercentage = '100%'
                              }

                              if(el.commit && !el.targetAndAchievement.targetOriginal){
                                  el.commitPercentage = '100%'
                              }

                              el.wonPercentageStyle = {'width':el.wonPercentage,background: '#8ECECB',height:"inherit","max-width": "100%"}

                              el.targetAndAchievement.won = parseFloat(el.targetAndAchievement.won.r_formatNumber(2))
                              el.targetAndAchievement.wonWithCommas = getAmountInThousands(parseFloat(el.targetAndAchievement.won.r_formatNumber(2)),2,share.primaryCurrency=="INR");
                              el.targetAndAchievement.commitPipelineWithCommas = getAmountInThousands(parseFloat(el.targetAndAchievement.commitPipeline.r_formatNumber(2)),2,share.primaryCurrency=="INR");
                          }
                      });

                      if(!share.selectedPortfolio){
                          $scope.totalAchievement = getAmountInThousands($scope.totalAchievement,2,share.primaryCurrency=="INR")
                      }

                      $scope.commitPipeline = getAmountInThousands(parseFloat($scope.commitPipeline.r_formatNumber(2)),2,share.primaryCurrency=="INR")

                      $scope.teamList = team;

                      share.commitObj = {};
                      _.each($scope.teamList,function(tm){
                          tm.title = tm.fullName +", "+tm.emailId;
                          share.commitObj[tm.emailId] = tm;
                      });

                      $scope.totalTeamMembers = team.length;
                      $scope.totalTargetNum = _.sumBy(team,"targetSort");
                      $scope.totalTarget = getAmountInThousands(_.sumBy(team,"targetSort"),2,share.primaryCurrency=="INR");
                      $scope.totalCommit = getAmountInThousands(_.sumBy(team,"commitSort"),2,share.primaryCurrency=="INR");
                      $scope.totalDiff = getAmountInThousands(_.sumBy(team,"differenceSort"),2,share.primaryCurrency=="INR");

                      if(share.selectedPortfolio){
                          $scope.getDataBySomething(share.selectedPortfolio,$scope.selectedCommitRange.name,true)
                      }
                  }

                  $scope.loadingData = false;
              });
        }
    }

    $scope.seeReview = function (user) {
        mixpanelTracker("Commits review for user ");
        $scope.owner = user;
        $scope.teamCommitsViewing = false;
        $scope.allMyAccess = false;
        $scope.actionloadingData = true;
        resetAllMemberSelection($scope);
        user.selected = "selected";
        $scope.selfSelection = "btn-selected";
        $scope.teamSelection = "";
        $scope.allSelection = "";
        $scope.oppsInCommitStage = [];

        if($scope.dataBy != 'org' && !share.selectedPortfolio){
            $scope.dataBy = 'org';
            $scope.canViewSummary = false;
        }

        $scope.setSummaryAccess($scope.owner.emailId)

        window.localStorage['oppsInCommitStage'] = "notSet";

        if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
            if($scope.selectedCommitRange.name == "Week"){
                weeklyPastCommitHistory($scope,$rootScope,$http,share,user.userId);
            } else if ($scope.selectedCommitRange.name == "Quarter"){
                quarterlyCommitHistory($scope,$rootScope,$http,share,user.userId);
            } else {
                monthlyCommitHistory($scope,$rootScope,$http,share,user.userId);
            }
        } else {
            monthlyCommitHistory($scope,$rootScope,$http,share,user.userId);
        }
    }

    $scope.setSummaryAccess = function(selectedEmailId,headEmailId){
        if(selectedEmailId){
            $scope.selectedEmailId = selectedEmailId;
        }

        if(headEmailId){
            $scope.headEmailId = headEmailId;
        }

        $scope.canViewSummary = $scope.selectedEmailId === $scope.headEmailId;

        var isManager = false;
        var children = [];

        if(share.commitObj[share.liuData.emailId] && share.commitObj[share.liuData.emailId].children && share.commitObj[share.liuData.emailId].children.teamMatesEmailId){
            children = share.commitObj[share.liuData.emailId].children.teamMatesEmailId;
            if(_.includes(children, $scope.headEmailId)){
                isManager = true;
            }
        }

        if($scope.dataBy !== 'org' && isManager){
            $scope.canViewSummary = true;
        }

        if($scope.canViewSummary){
            $(".table-review").css("top", "165px");
            $(".action-board").css("top", "165px");
        } else {
            $(".table-review").css("top", "98px");
            $(".action-board").css("top", "98px");
        }

    }

    $scope.setRenewalAmount = function(amount){

        if(!$scope.opp.renewed){
            $scope.opp.renewed = {
                amount:0,
                netGrossMargin:0,
                closeDate:null,
                createdDate:null
            }
        }
        $scope.opp.renewed.amount = checkRequired(amount)?parseFloat(amount):0;
    }

    $scope.registerDatePickerId = function(minDate,maxDate){

        if($scope.opp){
            if($scope.opp.stage == "Close Won" || $scope.opp.stage == "Close Lost"){
                maxDate = new Date();
            } else {
                maxDate = new Date(moment().add(4,"year"))
            }
        } else {
            $scope.opp = {}
        }

        $('#opportunityCloseDateSelector').datetimepicker({
            value:$scope.opp.closeDateFormatted,
            timepicker:false,
            validateOnBlur:false,
            minDate: minDate,
            maxDate: maxDate,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    $scope.opp.closeDate = moment(dp).format();
                    $scope.opp.closeDateFormatted = moment(dp).format("DD MMM YYYY");
                });
            }
        });
    }

    $scope.registerDatePickerIdRenewal = function(minDate,maxDate){
        pickDatesForRenewal($scope,minDate,maxDate,'#opportunityCloseDateSelector4',1,"years")
    }

    $scope.addToReasonList = function (reason) {

        if(!$scope.opp.closeReasons || $scope.opp.closeReasons.length == 0){
            $scope.opp.closeReasons = [];
        }

        if(reason.selected){
            $scope.opp.closeReasons.push(reason.name)
        } else {
            $scope.opp.closeReasons = $scope.opp.closeReasons.filter(function (el) {
                return el != reason.name
            })
        }

        $scope.opp.closeReasons = _.uniq($scope.opp.closeReasons)
    }

    $scope.ifOppClose = function (stage) {


        if(_.includes(["Close Lost","Close Won"], stage)){
            $scope.reasonsRequired = true;
            $scope.viewModeFor = "close details";
            $scope.mailOptions = true;
            $scope.closingOpp = true;
            $scope.opp.closeDate = new Date();
            $scope.opp.closeDateFormatted = moment().format(standardDateFormat());

        } else {

            $scope.closingOpp = false;
            $scope.opp.renewalAmountReq = false;
            $scope.opp.renewalCloseDateReq = false;

            $scope.mailOptions = false;
        }
        $scope.registerDatePickerId()
    }

    $scope.toggleContactPopup = function() {
        $scope.showContactCreateModal = false;
    }

    $scope.createContactWindow = function() {
        $scope.showContactCreateModal = true;
    }

    $scope.saveContact = function() {

        if(!$scope.coords){
            $scope.coords = {}
        }

        var data = {
            personName:$("#fullName").val(),
            personEmailId:$("#p_emailId").val(),
            companyName:$("#companyName").val(),
            designation:$("#designation").val(),
            mobileNumber:$("#p_mobileNumber").val(),
            location:$scope.coords.address,
            lat:$scope.coords.lat,
            lng:$scope.coords.lng
        }

        var errExists = false;

        if(!data.personName){
            errExists = true
            toastr.error("Please enter a name for the contact.")
        } else if(!validateEmail(data.personEmailId)){
            toastr.error("Please enter a valid email ID.")
            errExists = true
        }

        if(!checkRequired(data.location)){
            errExists = true
            toastr.error("Please enter a city for this contact.")
        }

        if(!errExists){
            $http.post("/contacts/create",data)
                .success(function (response) {
                    toastr.success("Contact added successfully.")
                    $scope.showContactCreateModal = false;
                })
        }
    }

    $scope.saveOpp = function() {

        if(!$scope.opp.searchContent){
            toastr.warning("Contact for opportunity can not be empty. Previously selected contact will be set");
            $scope.opp.searchContent = $scope.opp.contactEmailId
        }

        $scope.renewalAmount = $("#renewalAmount").val()

        if($scope.renewalAmount){
            $scope.renewalAmount = parseFloat($scope.renewalAmount);
        }

        $scope.isExistingOpp = true;
        if (!checkOppFieldsRequirementBasic($scope, $rootScope, share,true)) {
            $scope.town = $scope.town ? $scope.town.replace(/[^a-zA-Z ]/g, "") : $scope.opp.geoLocation.town
            $scope.opp.name = $scope.opp.opportunityName;

            if ($scope.town) {
                $scope.opp.geoLocation.town = $scope.town;
            }

            var obj = {
                opportunity: $scope.opp,
                contactEmailId: $scope.opp.contactEmailId,
                contactMobile: $scope.opp.mobileNumber,
                town: $scope.town,
                zone: $scope.opp.geoLocation ? $scope.opp.geoLocation.zone : null,
                mailOrgHead: $scope.mailOrgHead,
                mailRm: $scope.mailRm,
                companyId: $rootScope.companyDetails._id,
                hierarchyParent: share.liuData.hierarchyParent,
                renewalCloseDate: $scope.renewalCloseDate,
                renewalAmount: $scope.renewalAmount
            }

            $http.post("/salesforce/edit/opportunity/for/contact", obj).success(function (response) {
                toastr.success("Opportunity successfully updated")
            })
        }
    }

    share.getInteractionHistory = function (op,viewModeFor) {
        $scope.getInteractionHistory(op,viewModeFor)
    }

    $scope.getInteractionHistory = function (op,viewModeFor) {
        getInteractionHistory($scope,$rootScope,searchService,$http,share,op,viewModeFor)
    }

    $scope.getTeamCommits = function(){

    }

    $scope.openView = function(viewFor){
        setTabView($scope,viewFor);
        $scope.viewModeFor = viewFor;
    }

    share.drawIntGrowth = function (data) {
        drawIntGrowth($scope,share,data)
    }

    if(notificationCategory && notificationDate) {
        updateNotificationOpenDate($http, {"dayString":notificationDate, "category": notificationCategory}, function(response) {
        });    
    }    

});

function getSummary($scope,$http,share,$rootScope,selectedPortfolio,callback){
    var obj = {
        range:$scope.selectedCommitRange.name
    }

    if(selectedPortfolio && callback){
        obj.selectedPortfolio = _.cloneDeep(selectedPortfolio);
    } else {

        if(share.selectedPortfolio){
            _.each(share.selectedPortfolio.users,function(pu){
                if(pu.isHead){
                    obj.selectedPortfolio = _.cloneDeep(pu)
                    // obj.selectedPortfolio = pu
                }
            });
        }

        if(!obj.selectedPortfolio){
            obj.selectedPortfolio = _.cloneDeep(share.selectedPortfolio.users[0])
        }

        obj.selectedPortfolio.accessLevel.forEach(function (ac) {

            if(ac.type == "Products"){
                ac.values = _.map($rootScope.companyDetails.productList,"name");
            }

            if(ac.type == "Verticals"){
                ac.values = _.map($rootScope.companyDetails.verticalList,"name");
            }

            if(ac.type == "Business Units"){
                ac.values = _.map($rootScope.companyDetails.businessUnits,"name");
            }

            if(ac.type == "Regions"){
                ac.values = _.map($rootScope.companyDetails.geoLocations,"region");
            }
        })
    }

    $http.post('/get/portfolio/summary',obj).success(function (response) {

        var wonPer = "0%";
        $scope.totalAchievement = response.won;
        $scope.totalPipeline = response.pipeline;

        if($scope.totalTargetNum && $scope.totalAchievement){
            wonPer = (($scope.totalAchievement/$scope.totalTargetNum)*100).toFixed()+"%";
        }

        $scope.totalAchievement = getAmountInThousands($scope.totalAchievement,2,share.primaryCurrency=="INR") +" ("+wonPer+")";
        $scope.totalPipeline = getAmountInThousands($scope.totalPipeline,2,share.primaryCurrency=="INR");

        if(callback){
            callback(response);
        }
    });
}

function drawIntGrowth($scope,share,data) {
    var labels = [];
    var className = ".int-growth"

    var series = [],
        series2 = [];

    if(data.length>0){

        data.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(data,function (el) {
            labels.push(moment(el.date).format("MMM"))
            series.push({
                name: moment(el.date).format("MMM"),
                value:el.initiatedByUs
            })

            series2.push({
                name: moment(el.date).format("MMM"),
                value:el.initiatedByThem
            })
        });

        drawLineChart($scope,share,series,labels,className,series2)
    }
}

function getInteractionHistory($scope,$rootScope,searchService,$http,share,op,viewModeFor) {
    if(viewModeFor){
        $scope.viewModeFor = viewModeFor;
    } else {
        $scope.viewModeFor = "insights";
    }

    setTabView($scope,$scope.viewModeFor)

    $scope.oppOwner = {};
    $scope.teamTask = false;

    checkTeamLoaded();

    function checkTeamLoaded(){
        if(share.usersDictionary){
            if(share.usersDictionary[op.userEmailId]){
                $scope.oppOwner.value = share.usersDictionary[op.userEmailId].fullName+" ("+share.usersDictionary[op.userEmailId].emailId+")"
            } else {
                $scope.oppOwner.value = op.userEmailId
            }
        } else {
            setTimeOutCallback(1000,function () {
                checkTeamLoaded();
            })
        }
    }

    $scope.showOppInsights = true;
    $scope.interactionsCount = 0;
    var url = '/review/opp/insights';
    var users = [op.userId];
    var contacts = getContactsFromOpp(op,$scope);

    if(op.usersWithAccess && op.usersWithAccess[0]){
        users = users.concat(_.map(op.usersWithAccess,function (el) {
            if(share.usersDictionary && share.usersDictionary[el.emailId] && share.usersDictionary[el.emailId].userId){
                return share.usersDictionary[el.emailId].userId
            }
        }));
    }

    url = fetchUrlWithParameter(url,"hierarchylist",users);
    url = fetchUrlWithParameter(url,"contacts",contacts);
    url = fetchUrlWithParameter(url,"opportunityId",op.opportunityId);

    if(op.createdDate){
        url = fetchUrlWithParameter(url,"createdDate",String(op.createdDate));
    }

    if(_.includes(["Close Lost","Close Won"], op.stage)){
        if(op.closeDate){
            url = fetchUrlWithParameter(url,"closeDate",String(op.closeDate));
        }
    }

    $scope.insights = [];

    $http.get(url)
        .success(function (response) {
            if(response && response.SuccessCode){

                $scope.opp = formatOpp(response.Data.opp,share);

                if($scope.opp.usersWithAccess){
                    mapUsersWithAccessToOrgRoles($scope,share,$scope.opp)
                }

                $scope.opp.isNotOwner = op.isNotOwner;
                $scope.opp.isOppClosed = op.isOppClosed;

                $scope.newOppContact = {};
                $scope.newOppContact.emailId = $scope.opp.contactEmailId;
                initServices($scope, $http,$rootScope,share,searchService);
                getAllRelatedData($scope,$http,share,$scope.opp);
                $scope.interactionsCount = response.Data.interactionsCount
                $scope.interactions = response.Data.interactions.allInteractions;

                if(response.Data.insights){
                    _.each(response.Data.insights,function (el) {
                        $scope.insights.push({
                            text: el.text,
                            riskClass: el.risk == 'high'?'red':'green'
                        })
                    })
                }

                if($scope.interactions.length>0){
                    _.each($scope.interactions,function (el) {
                        el.interactionDateFormatted = moment(el.interactionDate).format(standardDateFormat());
                        el.member = share.usersDictionary[el.ownerEmailId]
                    });
                }

                if(response.Data.interactions && response.Data.interactions.interactionsFlow){
                    share.drawIntGrowth(response.Data.interactions.interactionsFlow);
                } else {
                    share.drawIntGrowth([])
                }
            } else {
                share.drawIntGrowth([]);
            }
        });
}

function commitMovement($scope,$http,share,$rootScope,pastMonthCommit) {

    $scope.initialCommitPipeline = {
        percentage:0,
        count:0,
        amount:0
    };
    $scope.selfCommit = 0;
    $scope.won = {
        percentage:0,
        count:0,
        amount:0
    };
    $scope.idle = {
        percentage:0,
        count:0,
        amount:0
    };
    $scope.lost = {
        percentage:0,
        count:0,
        amount:0
    };
    $scope.pipeline = {
        percentage:0,
        count:0,
        amount:0
    };
    $scope.commitMet = "not-met";

    var url = "/review/pipeline/movement";
    var opportunityIds = [];
    if(pastMonthCommit){

        $scope.commitMvMtLoading = true;

        _.each(pastMonthCommit.opportunities,function (op) {
            opportunityIds.push(op.opportunityId)
            $scope.initialCommitPipeline.amount = $scope.initialCommitPipeline.amount+op.amount;
        })

        $scope.initialCommitPipeline.count = opportunityIds.length;

        if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
            if($scope.selectedCommitRange.name == "Week"){
                $scope.selfCommit = pastMonthCommit.week.userCommitAmount
            } else if ($scope.selectedCommitRange.name == "Quarter"){
                $scope.selfCommit = pastMonthCommit.quarter.userCommitAmount
            } else {
                $scope.selfCommit = pastMonthCommit.month.userCommitAmount
            }
        } else {
            $scope.selfCommit = pastMonthCommit.month.userCommitAmount
        }
        url = fetchUrlWithParameter(url,"range",$scope.selectedCommitRange.name);

        if(opportunityIds.length>0){
            url = fetchUrlWithParameter(url,"opportunityIds",opportunityIds);

            $http.get(url)
                .success(function (response) {
                    if(response && response.SuccessCode && response.Data && response.Data.length>0){
                        var endOfLastMonth = new Date(moment(moment().startOf('month')).subtract(1,'hr'));

                        _.each(response.Data,function (op) {
                            if(op.stageName == "Close Won" && (new Date(op.closeDate)<= endOfLastMonth)){
                                $scope.won.amount = $scope.won.amount+op.amount;
                                $scope.won.count++;
                            } else if(op.stageName == "Close Lost" && (new Date(op.closeDate)<= endOfLastMonth)){
                                $scope.lost.amount = $scope.lost.amount+op.amount;
                                $scope.lost.count++;
                            } else if(op.stageName == $rootScope.commitStage){
                                $scope.idle.amount = $scope.idle.amount+op.amount;
                                $scope.idle.count++;
                            } else {
                                $scope.pipeline.amount = $scope.pipeline.amount+op.amount;
                                $scope.pipeline.count++;
                            }
                        });
                    }

                    if($scope.won.amount>=$scope.selfCommit){
                        $scope.commitMet = "met"
                    }

                    $scope.won.percentage = (($scope.won.amount/$scope.initialCommitPipeline.amount)*100);
                    $scope.lost.percentage = (($scope.lost.amount/$scope.initialCommitPipeline.amount)*100);
                    $scope.idle.percentage = (($scope.idle.amount/$scope.initialCommitPipeline.amount)*100);
                    $scope.pipeline.percentage = (($scope.pipeline.amount/$scope.initialCommitPipeline.amount)*100);

                    $scope.won.percentage = $scope.won.percentage != 0?$scope.won.percentage.toFixed(2)+"%":$scope.won.percentage+"%"
                    $scope.lost.percentage = $scope.lost.percentage != 0?$scope.lost.percentage.toFixed(2)+"%":$scope.lost.percentage+"%"
                    $scope.idle.percentage = $scope.idle.percentage != 0?$scope.idle.percentage.toFixed(2)+"%":$scope.idle.percentage+"%"
                    $scope.pipeline.percentage = $scope.pipeline.percentage != 0?$scope.pipeline.percentage.toFixed(2)+"%":$scope.pipeline.percentage+"%";

                    $scope.initialCommitPipeline.amount = getAmountInThousands($scope.initialCommitPipeline.amount,2,share.primaryCurrency=="INR")
                    $scope.won.amount = getAmountInThousands($scope.won.amount,2,share.primaryCurrency=="INR")
                    $scope.lost.amount = getAmountInThousands($scope.lost.amount,2,share.primaryCurrency=="INR")
                    $scope.idle.amount = getAmountInThousands($scope.idle.amount,2,share.primaryCurrency=="INR")
                    $scope.pipeline.amount = getAmountInThousands($scope.pipeline.amount,2,share.primaryCurrency=="INR")
                    $scope.selfCommit = getAmountInThousands($scope.selfCommit,2,share.primaryCurrency=="INR");

                    $scope.commitMvMtLoading = false;

                });
        } else {
            $scope.commitMvMtLoading = false;
        }
    }
}

function getContactsFromOpp(o) {
    var contacts = [o.contactEmailId];

    if(o.partners && o.partners.length>0) {
        _.each(o.partners,function (el) {
            if(el){
                contacts.push(el.emailId)
            }
        })
    }

    if(o.influencers && o.influencers.length>0) {
        _.each(o.influencers,function (el) {
            if(el){
                contacts.push(el.emailId)
            }
        })
    }

    if(o.decisionMakers && o.decisionMakers.length>0) {
        _.each(o.decisionMakers,function (el) {
            if(el){
                contacts.push(el.emailId)
            }
        })
    }

    return contacts;
}

function saveQuarterlyCommits($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){
        $http.post("/review/meeting/update/commit/value/quarterly",{commitValue:commits.selfCommitValue,committingForNext:$scope.committingForNext})
            .success(function (response) {

                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // quarterlyCommitHistory($scope,$rootScope,$http,share,share.liuData._id)
                } else {
                    toastr.error("Commits not updated. Please try again later")
                }
                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only positive numbers")
    }
}

function saveWeeklyCommits($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){

        $http.post("/review/meeting/update/commit/value/weekly",{week:commits.selfCommitValue,committingForNext:$scope.committingForNext})
            .success(function (response) {

                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // weeklyPastCommitHistory($scope,$rootScope,$http,share,share.liuData._id)
                } else {
                    toastr.error("Commits not updated. Please try again later")
                }
                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only positive numbers")
    }
}

function saveMonthlyCommit($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){
        $http.post("/review/meeting/update/commit/value/monthly",{commitValue:commits.selfCommitValue,committingForNext:$scope.committingForNext})
            .success(function (response) {
                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // monthlyCommitHistory($scope,$rootScope,$http,share)
                } else {
                    toastr.error("Commits not updated. Please try again later")
                }

                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only numbers for opportunity amount")
    }
}

function resetAllMemberSelection($scope){
    _.each($scope.teamList,function (tm) {
        tm.selected = "";
    })
}

function monthlyCommitHistory($scope,$rootScope,$http,share,userId){

    var url = '/review/monthly/commits/history';
    var obj = {}

    if(userId){
        url = fetchUrlWithParameter(url,"userId",userId);
        url = fetchUrlWithParameter(url,"userEmailId",share.teamDictionaryByUserId[userId].emailId);
        obj = {
            userId:userId,
            userEmailId:share.teamDictionaryByUserId[userId].emailId,
            hierarchyType:$scope.selectedHierarchy.name.replace(/[^A-Z0-9]+/ig, "_")
        }
    }

    if($scope.selectedHierarchy && $scope.selectedHierarchy.name){
        url = fetchUrlWithParameter(url,"hierarchyType",$scope.selectedHierarchy.name.replace(/[^A-Z0-9]+/ig, "_"))
    }

    if(share.selectedPortfolio){
        _.each(share.selectedPortfolio.users,function(pu){
            if(pu.ownerEmailId == obj.userEmailId){
                obj.selectedPortfolio = pu
            }
        });
    }

    $http.post(url,obj)
        .success(function (response) {
            var monthNames = [];
            var commitColName = response.commitStage;
            $rootScope.commitStage = response.commitStage;

            commitColName = commitColName+" stage ($)"
            var relatasCommitCol = [commitColName],
                selfCommitCol = ['Self commit'],
                oppCreatedCol = ['Opp created (count)'],
                oppWonAmountCol = ['Opp won ($)'],
                oppWonCol = ['Opp won (count)'],
                targetCol = ['Target'],
                commitsMadeObj = {},
                monthNamesObj = {};

            $scope.oppsInCommitStage = response && response.oppsInCommitStage?response.oppsInCommitStage:[];

            if($scope.oppsInCommitStage.length>0){
                _.each($scope.oppsInCommitStage,function (op) {
                    op.closingThisWeek = {background:"inherit"};
                    op.riskMeter = response.dealsAtRisk && response.dealsAtRisk[op.opportunityId]?response.dealsAtRisk[op.opportunityId]*100:0

                    if(new Date(op.closeDate)>= new Date(moment().startOf("month")) && new Date(op.closeDate) <= new Date(moment().endOf("month"))){
                        op.closingThisWeek = {
                            background:"#f2f2f2"
                        }
                    }

                    op = formatOpp(op,share,$scope);
                })
            }

            window.localStorage['oppsInCommitStage'] = JSON.stringify($scope.oppsInCommitStage);

            response.commits.sort(function (o1, o2) {
                return new Date(o1.commitForDate) > new Date(o2.commitForDate) ? 1 : new Date(o1.commitForDate) < new Date(o2.commitForDate) ? -1 : 0;
            });

            response.oppsConversion.created.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            response.oppsConversion.oppsWon.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            response.targets.sort(function (o1, o2) {
                return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
            });

            var pastMonthCommits = null,
                pastMonthYear = moment().subtract(1, 'month').month()+""+moment().subtract(1, 'month').year();

            _.each(response.commits,function (el,index) {
                if(el.monthYear == pastMonthYear){
                    pastMonthCommits = el;
                }
                monthNames.push(moment(el.date).format("MMM 'YY"));
                monthNamesObj[index] = el.date;
                commitsMadeObj[index] = el;
                selfCommitCol.push(el.month.userCommitAmount)
                relatasCommitCol.push(el.month.relatasCommitAmount)
            });

            _.each(response.oppsConversion.created,function (el) {
                oppCreatedCol.push(el.count)
            });

            _.each(response.oppsConversion.oppsWon,function (el) {
                oppWonAmountCol.push(el.amount?parseFloat(el.amount.r_formatNumber(2)):0)
                oppWonCol.push(el.count)
            });

            if(share.portTargetsFy && share.selectedPortfolio){
                _.each(share.portTargetsFy[share.teamDictionaryByUserId[userId].emailId],function (tr_p) {
                    _.each(response.targets,function (tr,index) {
                        if(moment(tr.date).format('DD MMM') == moment(tr_p.date).format('DD MMM')){
                            tr.target = tr_p.amount;
                        }
                    })
                })
            }

            _.each(response.targets,function (tr,index) {
                targetCol.push(tr.target)
            })

            setTimeOutCallback(100,function () {
                $scope.graphLoading = false;
            })

            setTimeOutCallback(100,function () {
                $scope.commitMovement(pastMonthCommits)
            })

            var colors = {
                "Target": "#FE9E83",
                "Self commit": '#638ca6',
                "Opp created (count)": '#3498db',
                'Opp won (count)': '#8ECECB',
                'Opp won ($)':oppStageStyle("Close Won",$scope.stages.indexOf($rootScope.commitStage),false,false,true)
            }

            colors[commitColName] = oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)

            var axes = {
            }

            axes[commitColName] = 'y2'
            axes["Opp won ($)"] = 'y2'
            axes["Self commit"] = 'y2'
            axes["Target"] = 'y2';

            $scope.commitHistoryExists = checkCommitHistoryExists(oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol);

            var chart = c3.generate({
                bindto: ".pipeline-comparison",
                data: {
                    columns: [oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol,targetCol],
                    axes: axes,
                    type: 'bar',
                    types: {
                        'Opp created (count)': 'spline',
                        "Opp won (count)": 'spline'
                    },
                    colors:colors,
                    bar: {
                        width: {
                            ratio: 0.5
                        }
                    },
                    onclick: function(e) {
                        $scope.$apply(function () {
                            closeAllOppTables($scope);
                            $rootScope.commitStage = share.commitStage;
                            // displayOppsMonthly(e,$scope,share,response,commitsMadeObj)
                        });
                    }
                },
                axis: {
                    y2: {
                        label: {
                            text:'Opp amount',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    y: {
                        label: {
                            text:'No. of Opps',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    x : {
                        tick: {
                            fit: true,
                            format: function (x) { return monthNames[x];}
                        }
                    }
                },
                tooltip: {
                    format: {
                        title: function (d) {

                            if(commitsMadeObj[d]){
                                var startDate = commitsMadeObj[d].commitForDate
                                var dateRange = moment(startDate).format('MMM YY')
                                return dateRange;
                            }
                        }
                    }
                }
            });

            $scope.closing = {
                this:"range"
            }

            setTimeOutCallback(100,function () {
                $scope.actionloadingData = false;
            });
            $scope.filterOpps('range');
        })
}

function checkCommitHistoryExists(oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol) {
    var commitHistoryExists = false;
    _.each(oppWonCol,function (el) {
        if(!commitHistoryExists && isNumber(el) && el>0){
            commitHistoryExists = true;
        }
    })

    if(!commitHistoryExists){
        _.each(oppCreatedCol,function (el) {
            if(!commitHistoryExists && isNumber(el) && el>0){
                commitHistoryExists = true;
            }
        })
    }

    if(!commitHistoryExists){
        _.each(selfCommitCol,function (el) {
            if(!commitHistoryExists && isNumber(el) && el>0){
                commitHistoryExists = true;
            }
        })
    }
    if(!commitHistoryExists){
        _.each(oppWonAmountCol,function (el) {
            if(!commitHistoryExists && isNumber(el) && el>0){
                commitHistoryExists = true;
            }
        })
    }

    return commitHistoryExists;

}

function pipelineComparison($scope,$rootScope,$http,share,response){

    var weekNames = [];
    var commitColName = $rootScope.commitStage?$rootScope.commitStage:response.Data.commitStage;
    commitColName = commitColName+" stage ($)"
    var relatasCommitCol = [commitColName],
        selfCommitCol = ['Self commit ($)'],
        oppCreatedCol = ['Opp created (count)'],
        oppWonAmountCol = ['Opp won ($)'],
        oppWonCol = ['Opp won (count)']

    formatCommitData(relatasCommitCol,selfCommitCol,response,share)
    formatConversionData(oppCreatedCol,oppWonCol,oppWonAmountCol,response,weekNames,share)

    var relatasCommitColLength = relatasCommitCol.length,
        prefilledDataRelatasCommit = []

    _.each(relatasCommitCol,function (re,index) {
        if(index == relatasCommitColLength-1 && response.Data.oppsInStageClosingThisWeek && response.Data.oppsInStageClosingThisWeek.length>0){
            prefilledDataRelatasCommit.push(_.sumBy(response.Data.oppsInStageClosingThisWeek,"amount"))
        } else {
            prefilledDataRelatasCommit.push(re)
        }
    })

    var colors = {
        "Self commit ($)": '#638ca6',
        "Opp created (count)": '#3498db',
        'Opp won (count)': '#8ECECB',
        'Opp won ($)':oppStageStyle("Close Won",$scope.stages.indexOf($rootScope.commitStage),false,false,true)
    }

    colors[commitColName] = oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)

    var axes = {
    }

    axes[commitColName] = 'y2'
    axes["Opp won ($)"] = 'y2'
    axes["Self commit ($)"] = 'y2'

    $scope.commitHistoryExists = checkCommitHistoryExists(oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol);

    var chart = c3.generate({
        bindto: ".pipeline-comparison",
        data: {
            columns: [oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol],
            axes: axes,
            type: 'bar',
            types: {
                'Opp created (count)': 'spline',
                "Opp won (count)": 'spline'
            },
            colors:colors,
            bar: {
                width: {
                    ratio: 0.5
                }
            },
            onclick: function(e) {
                $scope.$apply(function () {

                });
            }
        },
        axis: {
            y2: {
                label: {
                    text:'Opp amount',
                    position: 'outer-center'
                },
                show: true
            },
            y: {
                label: {
                    text:'No. of Opps',
                    position: 'outer-center'
                },
                show: true
            },
            x : {
                tick: {
                    fit: true,
                    format: function (x) { return weekNames[x];}
                }
            }
        },
        tooltip: {
            format: {
                title: function (d) {

                    if(share.weekByIndexNumberForCommits[d]){
                        var date = moment(moment(buildDateObj(share.weekByIndexNumberForCommits[d].weekYear))).add(1,"d");
                        date = moment(date).startOf("isoWeek");
                        var endDate = moment(date).endOf("isoWeek")
                        return moment(date).format('DD MMM')+"-"+moment(endDate).format('DD MMM');
                    }
                }
            }
        }
    });
}

function formatCommitData(relatasCommitCol,selfCommitCol,response,share){

    var rangeType = 'week';
    share.oppsCommitedByWeek = {}
    share.weekByIndexNumberForCommits = {}

    if(response.Data && response.Data.commits){
        response.Data.commits.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(response.Data.commits,function (el,index) {

            share.weekByIndexNumberForCommits[index] = {
                weekYear:el.commitWeekYear,
                noCommits:el.noCommits,
                date:el.date
            };
            share.oppsCommitedByWeek[el.commitWeekYear] = el.opportunities;

            if(el[rangeType] && el[rangeType].relatasCommitAmount){
                relatasCommitCol.push(el[rangeType].relatasCommitAmount)
            } else {
                relatasCommitCol.push(0)
            }

            if(el[rangeType] && el[rangeType].userCommitAmount){
                selfCommitCol.push(el[rangeType].userCommitAmount)
            } else {
                selfCommitCol.push(0)
            }

        })
    }
}

function formatConversionData(oppCreatedCol,oppWonCol,oppWonAmountCol,response,weekNames,share){
    if(response.Data && response.Data.oppsConversion){

        response.Data.oppsConversion.created.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        response.Data.oppsConversion.oppsWon.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(response.Data.oppsConversion.oppsWon,function (el) {
            oppWonCol.push(el.count)
            oppWonAmountCol.push(el.amount?parseFloat(el.amount.r_formatNumber(2)):0)
        })

        _.each(response.Data.oppsConversion.created,function (el) {
            var date = moment(moment(buildDateObj(el.weekYear))).add(1,"d");
            date = moment(date).startOf("isoWeek");
            var endDate = moment(date).endOf("isoWeek");
            weekNames.push(moment(date).format('DD MMM')+"-"+moment(endDate).format('DD MMM'))
            oppCreatedCol.push(el.count)
        })
    }
}

function displayOppsMonthly(event,$scope,share,response,commitsMadeObj){

    $scope.opps = [];
    $scope.currentWeek = false;
    $scope.weekByWeek = true;
    $scope.commitsByUsers = [];
    $scope.loadOppsForWeek = true;
    $scope.weekByWeek = true;
    $scope.loadingOpps = true;

    var opps = commitsMadeObj[event.index].opportunities

    if(opps && opps.length>0){
        _.each(opps,function (op) {

            op.ngmReq = response.netGrossMarginReq;
            op.amountWithNgm = op.amountNonNGM?op.amountNonNGM:op.amount;

            if(response.netGrossMarginReq){
                op.amountWithNgm = (op.amountWithNgm*op.netGrossMargin)/100
            }

            op.closingThisWeek = {};

            if(new Date(op.closeDate)>= (new Date(moment(commitsMadeObj[event.index].commitForDate))) && new Date(op.closeDate) <= (new Date(commitsMadeObj[event.index].commitForDate))){
                op.closingThisWeek = {
                    background:"#f2f2f2"
                }
            }

            if(response.commitStage == op.stageName){
                op.amountWithCommas = getAmountInThousands(op.amountWithNgm,2);

                var age = moment(op.closeDate).diff(moment(op.createdDate), 'day');

                if(op.relatasStage != "Close Won" && op.relatasStage != "Close Lost"){
                    age = moment().diff(moment(op.createdDate), 'day');
                }

                op.age = age;
                op.closeDateSort = moment(op.closeDate).unix()
                op.closeDate = moment(op.closeDate).format(standardDateFormat());

                op.account = fetchCompanyFromEmail(op.contactEmailId)

                op.opportunityNameTruncated = getTextLength(op.opportunityName,20)

                $scope.opps.push(op)
            }
        })
    }
}

function buildTeamProfilesWithCommits(data,listOfMembers,share,$scope,range) {

    var childrenForTeamMember = {}

    if(listOfMembers && listOfMembers.length>0){
        _.each(listOfMembers,function (el) {
            childrenForTeamMember[el.userEmailId] = el;
        })
    }

    var team = [];

    _.each(data,function (el) {
        var children = {};

        if(childrenForTeamMember[el.emailId]){
            children = childrenForTeamMember[el.emailId];
            children.teamMatesEmailId.push(el.emailId)
            children.teamMatesUserId.push(el._id)
        } else {
            children = {
                userEmailId:el.emailId,
                teamMatesEmailId:[el.emailId],
                teamMatesUserId:[el._id]
            }
        }

        var commit = 0,
            difference = 0,
            target = 0;

        if(range && range == 'week'){

            if(el.commit && el.commit.week && el.commit.week.userCommitAmount){
                commit = el.commit.week.userCommitAmount
            }
        } else if(range && range == 'quarter'){

            if(el.commit && el.commit.quarter && el.commit.quarter.userCommitAmount){
                commit = el.commit.quarter.userCommitAmount
            }
        } else {

            if(el.commit && el.commit.month && el.commit.month.userCommitAmount){
                commit = el.commit.month.userCommitAmount
            }
        }

        if(el.targetAndAchievement && el.targetAndAchievement.target){
            target = el.targetAndAchievement.target
            el.targetAndAchievement.targetOriginal = el.targetAndAchievement.target;
            el.targetAndAchievement.target= getAmountInThousands(el.targetAndAchievement.target,2,share.primaryCurrency=="INR");
        }

        difference = target - commit;

        if(!el.targetAndAchievement){
            el.targetAndAchievement = {
                "userEmailId": el.emailId,
                "userId": el.userId,
                "pipeline": 0,
                "lost": 0,
                "won": 0,
                "commitPipeline": 0,
                "probability": 0,
                "target": 0
            }
        };

        team.push({
            fullName:el.firstName+' '+el.lastName,
            // image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            children:children,
            targetAndAchievement:el.targetAndAchievement,
            commit:getAmountInThousands(commit,2,share.primaryCurrency=="INR"),
            commitSort:commit,
            difference:getAmountInThousands(difference,2,share.primaryCurrency=="INR"),
            differenceSort:difference,
            targetSort:target,
            noPicFlag:true,
            nameNoImg:el.firstName?el.firstName.substr(0,2).toUpperCase():el.ownerEmailId.substr(0,2).toUpperCase()
        })

    });

    return team;
}

function weeklyPastCommitHistory($scope,$rootScope,$http,share,userId){

    var url = "/review/past/commits";

    if(userId){
        url = fetchUrlWithParameter(url,"userId",userId)
    }

    var obj = {
        userId:userId,
        userEmailId:share.teamDictionaryByUserId[userId].emailId
    }

    if(share.selectedPortfolio){
        _.each(share.selectedPortfolio.users,function(pu){
            if(pu.ownerEmailId == obj.userEmailId){
                obj.selectedPortfolio = pu
            }
        });
    }

    $http.post(url,obj)
        .success(function (response) {
            if(response.SuccessCode){
                $rootScope.fySelection = "FY "+moment(response.Data.fyRange.start).year()+"-"+moment(response.Data.fyRange.end).year()
            }

            $scope.oppsInCommitStage = response.Data && response.Data.oppsInCommitStage?response.Data.oppsInCommitStage:[];

            if($scope.oppsInCommitStage.length>0){
                _.each($scope.oppsInCommitStage,function (op) {

                    op.closingThisWeek = {background:"inherit"};

                    if(new Date(op.closeDate)>= new Date(moment().startOf("isoweek")) && new Date(op.closeDate) <= new Date(moment().endOf("isoweek"))){
                        op.closingThisWeek = {
                            background:"#f2f2f2"
                        }
                    }

                    op = formatOpp(op,share,$scope);
                })
            }

            window.localStorage['oppsInCommitStage'] = JSON.stringify($scope.oppsInCommitStage);
            $scope.closing = {
                this:"range"
            }

            $scope.filterOpps('range');
            setTimeOutCallback(100,function () {
                $scope.actionloadingData = false;
            })
            pipelineComparison($scope,$rootScope,$http,share,response)
        });
}

function quarterlyCommitHistory($scope,$rootScope,$http,share,userId){

    var url = '/review/quarterly/commits/history';

    if(userId){
        url = fetchUrlWithParameter(url,"userId",userId)
    }

    var obj = {
        userId:userId,
        userEmailId:share.teamDictionaryByUserId[userId].emailId
    }

    if(share.selectedPortfolio){
        _.each(share.selectedPortfolio.users,function(pu){
            if(pu.ownerEmailId == obj.userEmailId){
                obj.selectedPortfolio = pu
            }
        });
    }

    $http.post(url,obj)
        .success(function (response) {

            var qtrNames = [];
            var commitColName = $rootScope.commitStage?$rootScope.commitStage:response.commitStage;
            commitColName = commitColName+" stage ($)"
            var relatasCommitCol = [commitColName],
                selfCommitCol = ['Self commit'],
                oppCreatedCol = ['Opp created (count)'],
                oppWonAmountCol = ['Opp won ($)'],
                oppWonCol = ['Opp won (count)'],
                targetCol = ['Target'],
                commitsMadeObj = {}

            $scope.oppsInCommitStage = response && response.oppsInCommitStage?response.oppsInCommitStage:[];

            if($scope.oppsInCommitStage.length>0){
                _.each($scope.oppsInCommitStage,function (op) {

                    op.riskMeter = response && response.dealsAtRisk && response.dealsAtRisk[op.opportunityId]?response.dealsAtRisk[op.opportunityId]*100:0;

                    op.closingThisWeek = {background:"inherit"};

                    if(new Date(op.closeDate)>= new Date(response.qtrStart) && new Date(op.closeDate) <= new Date(response.qtrEnd)){
                        op.closingThisWeek = {
                            background:"#f2f2f2"
                        }
                    }
                    op = formatOpp(op,share,$scope)
                })
            }

            window.localStorage['oppsInCommitStage'] = JSON.stringify($scope.oppsInCommitStage);

            response.commits.sort(function (o1, o2) {
                return new Date(o1.commitForDate) > new Date(o2.commitForDate) ? 1 : new Date(o1.commitForDate) < new Date(o2.commitForDate) ? -1 : 0;
            });

            response.oppsConversion.created.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            response.oppsConversion.oppsWon.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            var targetsObj = {};

            _.each(response.commits,function (el,index) {

                targetsObj[el.userEmailId] = el.target;
                if(el.qtrEnd && el.qtrStart){
                    qtrNames.push(moment(moment(el.qtrStart).add(1,"day")).format("MMM YY") +" - "+moment(moment(el.qtrEnd).subtract(1,"day")).format("MMM YY"))
                } else {
                    qtrNames.push(moment(el.date).format("MMM YY") +" - "+moment(moment(el.date).add(2,'month')).format("MMM YY"))
                }

                commitsMadeObj[index] = el;
                selfCommitCol.push(el.quarter.userCommitAmount)
                relatasCommitCol.push(el.quarter.relatasCommitAmount)
                targetCol.push(el.target)
            });

            _.each(response.oppsConversion.created,function (el) {
                oppCreatedCol.push(el.count)
            });

            _.each(response.oppsConversion.oppsWon,function (el) {
                oppWonAmountCol.push(el.amount?parseFloat(el.amount.r_formatNumber(2)):0)
                oppWonCol.push(el.count)
            });

            setTimeOutCallback(100,function () {
                $scope.graphLoading = false;
            })

            setTimeOutCallback(100,function () {
                $scope.commitMovement(response.commits.slice(-2)[0])
            })

            var colors = {
                "Target": "#FE9E83",
                "Self commit": '#638ca6',
                "Opp created (count)": '#3498db',
                'Opp won (count)': '#8ECECB',
                'Opp won ($)':oppStageStyle("Close Won",$scope.stages.indexOf($rootScope.commitStage),false,false,true)
            }

            colors[commitColName] = oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)

            var axes = {
            }

            axes[commitColName] = 'y2'
            axes["Opp won ($)"] = 'y2'
            axes["Self commit"] = 'y2'
            axes["Target"] = 'y2'

            $scope.commitHistoryExists = checkCommitHistoryExists(oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol);

            var chart = c3.generate({
                bindto: ".pipeline-comparison",
                data: {
                    columns: [oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol,targetCol],
                    axes: axes,
                    type: 'bar',
                    types: {
                        'Opp created (count)': 'spline',
                        "Opp won (count)": 'spline'
                    },
                    colors:colors,
                    bar: {
                        width: {
                            ratio: 0.5
                        }
                    },
                    onclick: function(e) {
                        $scope.$apply(function () {
                            closeAllOppTables($scope);
                            $rootScope.commitStage = share.commitStage;
                            displayOppsQuarterly(e,$scope,share,response,commitsMadeObj)
                        });
                    }
                },
                axis: {
                    y2: {
                        label: {
                            text:'Opp amount',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    y: {
                        label: {
                            text:'No. of Opps',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    x : {
                        tick: {
                            fit: true,
                            format: function (x) { return qtrNames[x];}
                        }
                    }
                },
                tooltip: {
                    format: {
                        title: function (d) {
                            if(commitsMadeObj[d]){
                                var startDate = commitsMadeObj[d].commitForDate
                                var dateRange = moment(startDate).format('MMM')+"-"+moment(moment(startDate).add(1,"month")).format('MMM')+"-"+moment(moment(startDate).add(2,"month")).format('MMM YY')
                                return dateRange;
                            }
                        }
                    }
                }
            });
            $scope.closing = {
                this:"range"
            }

            setTimeOutCallback(100,function () {
                $scope.actionloadingData = false;
            })
            $scope.filterOpps('range');
        })
}

function formatOpp(op,share,$scope){

    op.isNotOwnerClass = '';

    if($scope){
        if(op.userEmailId !== $scope.owner.emailId){
            op.isNotOwnerClass = "background-color: #6EC7D5 !important"
        }
    }

    op.amount = parseFloat(op.amount);
    op.amountWithNgm = op.amount;

    if(!op.amountNonNGM && (op.netGrossMargin || op.netGrossMargin === 0)){
        op.amountWithNgm = parseFloat(((op.amount*op.netGrossMargin)/100).toFixed(2))
    }

    op.stage = op.stageName;

    op.topLine = getAmountInThousands(op.amount,2,share.primaryCurrency=="INR");
    if(op.amountNonNGM){
        op.topLine = getAmountInThousands(op.amountNonNGM,2,share.primaryCurrency=="INR");
    }
    op.amountWithCommas = getAmountInThousands(op.amountWithNgm,2,share.primaryCurrency=="INR");
    op.age = moment().diff(moment(op.createdDate), 'day');
    if(op.createdDate){
        op.createdDateFormatted = moment(op.createdDate).format(standardDateFormat());
    }

    if(_.includes(["Close Lost","Close Won"], op.stage)){
        op.daysToClosure = moment(op.closeDate).diff(moment(op.createdDate), 'day')
        op.daysClosureStyle = {color: '#4eb3a6'}
    } else {

        op.daysToClosure = moment(op.closeDate).diff(moment(), 'day')

        op.daysClosureStyle = {color: '#4eb3a6'}
        if(op.daysToClosure<0){
            op.daysClosureStyle = {color: '#cc4527'}
        }
    }

    op.closeDateSort = moment(op.closeDate).unix()
    // op.closeDate = moment(op.closeDate).format(standardDateFormat());
    op.closeDateFormatted = moment(op.closeDate).format(standardDateFormat())
    op.lastInteracted = moment(op.closeDate).format(standardDateFormat())

    if(op.geoLocation && op.geoLocation.zone){
        op.region = op.geoLocation.zone;
    }
    op.account = fetchCompanyFromEmail(op.contactEmailId)
    op.member = share.usersDictionary[op.userEmailId];

    op.opportunityNameTruncated = getTextLength(op.opportunityName,20);

    op.riskClass = "green";
    op.riskMeter = op.riskMeter?parseFloat(op.riskMeter.toFixed(2)):"";

    if(parseFloat(op.riskMeter)>50){
        op.riskClass = "red"
    }
    
    op.isStaleClass = new Date(moment(op.closeDate).endOf('day'))<new Date()?"red-color":"grey-color";
    op.isNotOwner = false;
    op.canTransferOpp = false;
    if(op.userEmailId === share.liuData.emailId || share.liuData.corporateAdmin){
        op.isNotOwner = false;
        op.canTransferOpp = true;
    }

    if(op.fullName){
        op.searchContent = op.fullName + " ("+op.contactEmailId+")";
    } else {
        op.searchContent = op.contactEmailId;
    }

    op.tasks = "Assign"

    return op;
}

function fetchCommit(selectedPortfolio,$scope,$http,share,$rootScope,users,url,mode,startDate) {

    var emailIds = [$scope.owner.emailId]

    if(users){
        if(!$scope.teamCommitsViewing) {
            url = fetchUrlWithParameter(url,"userId",users);
        } else {
            url = fetchUrlWithParameter(url,"hierarchylist",users)
        }

        _.each(users,function (userId) {
            if(share.teamDictionaryByUserId[userId]){
                emailIds.push(share.teamDictionaryByUserId[userId].emailId)
            }
        })
    }

    url = fetchUrlWithParameter(url,"userEmailId",_.uniq(emailIds));

    url = fetchUrlWithParameter(url,"userId",$scope.owner.userId);

    if(!mode){
        mode = "month"
    }

    $http.get(url)
        .success(function (response) {

            $scope.commits = {
                oppValue:0,
                selfCommitValue:0,
                opps:[],
                selfCommitValueFormat:0,
                response:null,
                graph:{}
            }

            if(response){

                $rootScope.commitStage = response.commitStage
                share.commitStage = response.commitStage;

                var editAccess = false;
                var selfCommitValue = 0;
                var commitCutOffDate = null;
                var nextCommitCutOffDate = null;

                checkCommitCutOffLoaded();
                function checkCommitCutOffLoaded(){
                    if(share.commitCutOffObj){

                        if(mode == "week"){
                            commitCutOffDate = new Date(share.commitCutOffObj.week);
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"week"));
                        } else if(mode == "quarter"){
                            commitCutOffDate = new Date(share.commitCutOffObj.quarter)
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"quarter"));
                        } else {
                            commitCutOffDate = new Date(share.commitCutOffObj.month);
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"month"));
                        }
                    } else {
                        setTimeOutCallback(500,function () {
                            checkCommitCutOffLoaded();
                        })
                    }
                }

                $scope.canCommitForNext = true;

                if(commitCutOffDate && new Date(commitCutOffDate) >= new Date()){
                    editAccess = true;
                    // $scope.canCommitForNext = false;
                } else {
                    // $scope.canCommitForNext = true;
                    $scope.nextCommitCutOffDate = nextCommitCutOffDate;
                }

                var oppsToDisplay = response.opps;

                if(mode == "week"){
                    selfCommitValue = response.commitsCurrentWeek.week.userCommitAmount?response.commitsCurrentWeek.week.userCommitAmount:0
                    if(new Date()> new Date(response.commitWeek)){
                        oppsToDisplay = response.commitsCurrentWeek.opportunities
                    }

                } else if(mode == "quarter"){
                    if(response.commitCurrentQuarter.quarter.userCommitAmount){
                        selfCommitValue = response.commitCurrentQuarter.quarter.userCommitAmount;
                    }
                    if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                        oppsToDisplay = response.commitCurrentQuarter.opportunities
                    }

                } else {
                    if(response.commitCurrentMonth.month.userCommitAmount){
                        selfCommitValue = response.commitCurrentMonth.month.userCommitAmount;
                    }

                    if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                        oppsToDisplay = response.commitCurrentMonth.opportunities
                    }
                }

                var oppValue = getOppValInCommitStageForSelectedMonth(oppsToDisplay,response.commitMonth,share.primaryCurrency,share.currenciesObj);

                $scope.commits = {
                    oppValue:oppValue?getAmountInThousands(oppValue,2,share.primaryCurrency=="INR"):0,
                    editAccess:editAccess,
                    selfCommitValue:selfCommitValue,
                    opps:oppsToDisplay,
                    selfCommitValueFormat:getAmountInThousands(selfCommitValue,2,share.primaryCurrency=="INR"),
                    response:response
                }

                if(mode != 'week'){


                    if(share.selectedPortfolio){
                        _.each(share.selectedPortfolio.users,function(pu){
                            if(pu.ownerEmailId == share.liuData.emailId){
                                $scope.liuPortfolio = pu
                            }
                        });
                    }

                    if(selectedPortfolio && $scope.liuPortfolio){
                        var obj = {
                            range:$scope.selectedCommitRange.name
                        }

                        if(share.selectedPortfolio){

                            if(share.selectedPortfolio){
                                _.each(share.selectedPortfolio.users,function(pu){
                                    if(pu.ownerEmailId == share.liuData.emailId){
                                        obj.selectedPortfolio = pu
                                    }
                                });
                            }

                            getSummary($scope,$http,share,$rootScope,obj.selectedPortfolio,function (response_port) {

                                var insights = {
                                    "SuccessCode": 1,
                                    "ErrorCode": 0,
                                    "result": "",
                                    "Data":{
                                        month:[],
                                        qtr:[],
                                        commits:[]
                                    }
                                }

                                var targets = {};
                                var values = obj.selectedPortfolio.targets[0].values;

                                if(obj.range == "Quarter"){
                                    targets[obj.selectedPortfolio.ownerEmailId] = 0;
                                    if(values.length>0){
                                        _.each(values,function (tr) {
                                            if(new Date(tr.date) >= new Date(response.qStart) && new Date(tr.date) <= new Date(response.qEnd)){
                                                targets[obj.selectedPortfolio.ownerEmailId] = targets[obj.selectedPortfolio.ownerEmailId]+tr.amount
                                            }
                                        })
                                    }
                                } else {
                                    targets[obj.selectedPortfolio.ownerEmailId] = 0;
                                    if(values.length>0){
                                        _.each(values,function (tr) {
                                            if(moment(tr.date).format('MMM YYYY') == moment().format('MMM YYYY')){
                                                targets[obj.selectedPortfolio.ownerEmailId] = tr.amount
                                            }
                                        })
                                    }
                                }

                                var data = {
                                    "userEmailId": share.liuData.emailId,
                                    "userId": share.liuData._id,
                                    "pipeline": response_port.pipeline,
                                    "lost": 0,
                                    "won": response_port.won,
                                    "probability": 100,
                                    "target": targets[share.liuData.emailId]
                                }

                                if(obj.range == "Month"){
                                    insights.Data.month.push(data)
                                }

                                if(obj.range == "Quarter"){
                                    insights.Data.qtr.push(data)
                                }

                                $scope.commits.graph = setGraphValues(insights,response,share,obj.range.toLowerCase(),response_port.pipeline,$scope);
                            });

                        }
                    } else {

                        getTargetsAndAchievements($http,users,startDate,null,function (insights) {
                            if(insights){
                                $scope.commits.graph = setGraphValues(insights,response,share,mode,oppValue,$scope);
                            }
                        });
                    }
                }

                if(!$scope.commitModalOpen && $scope.teamCommitsViewing){
                    teamCommitsList($scope,share,response,mode,users,$http)
                }
            }

            setTimeOutCallback(100,function () {
                $scope.actionloadingData = false;
                $scope.loadingData = false;
            })
        });
}

function teamCommitsList($scope,share,data,mode,userIds,$http) {

    var insightsUrl = "/insights/current/fy";

    if(data.sh_users && data.sh_users.length>0){
        userIds = data.sh_users;
    }

    if(userIds){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"userIds",userIds)
    }

    $scope.sortTypeTeam = "fullName"
    $scope.sortReverseTeam = false;

    $http.get(insightsUrl)
        .success(function (insightsResponse) {

            $scope.commitsByUsers = [];
            $scope.closingThisSelection = {num:0,formatted:0};

            var targetsObj = {};

            if(insightsResponse && insightsResponse.Data && insightsResponse.Data.length>0){
                _.each(insightsResponse.Data,function (el) {
                    targetsObj[el._id] = el;
                })
            }

            var startDate = moment().startOf("month"),
                end = moment().endOf("month");

            var oppList = data.opps;
            $scope.oppsInCommitStage = [];
            var opportunitiesObj = {};
            var opportunitiesClosingThisSelectionObj = {};

            if(oppList && oppList.length>0){
                _.each(oppList,function (op) {
                    op.closingThisWeek = {background:"inherit"};

                    op.riskMeter = data && data.dealsAtRisk && data.dealsAtRisk[op.opportunityId]?data.dealsAtRisk[op.opportunityId]*100:0;

                    if(mode == "week"){
                        var weekStartDateTime = moment().startOf("isoWeek");
                        var weekEndDateTime = moment().endOf("isoWeek");
                        if(new Date(op.closeDate)>= new Date(weekStartDateTime) && new Date(op.closeDate) <= new Date(weekEndDateTime)){
                            op.closingThisWeek = {
                                background:"#f2f2f2"
                            }
                        }

                    } else if(mode == "quarter"){

                        if(new Date(op.closeDate)>= new Date(data.qStart) && new Date(op.closeDate) <= new Date(data.qEnd)){
                            op.closingThisWeek = {
                                background:"#f2f2f2"
                            }
                        }
                    } else {
                        if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate) <= new Date(end)){
                            op.closingThisWeek = {
                                background:"#f2f2f2"
                            }
                        }
                    }

                    if(opportunitiesObj[op.userEmailId]){
                        opportunitiesObj[op.userEmailId].push(op)
                    } else {
                        opportunitiesObj[op.userEmailId] = [];
                        opportunitiesObj[op.userEmailId].push(op)
                    }

                    var opp_f = formatOpp(op,share,$scope);

                    if(opp_f.closingThisWeek.background === "#f2f2f2"){
                        if(!opportunitiesClosingThisSelectionObj[opp_f.userEmailId]){
                            opportunitiesClosingThisSelectionObj[opp_f.userEmailId] = op.amountWithNgm
                        } else {
                            opportunitiesClosingThisSelectionObj[opp_f.userEmailId] = opportunitiesClosingThisSelectionObj[opp_f.userEmailId]+op.amountWithNgm;
                        }
                    }
                    $scope.oppsInCommitStage.push(opp_f);
                })
            }

            window.localStorage['oppsInCommitStage'] = JSON.stringify($scope.oppsInCommitStage);

            share.opportunitiesObj = opportunitiesObj;

            if(data.commitsCurrentRaw && data.commitsCurrentRaw.length>0) {

                _.each(data.commitsCurrentRaw, function (el) {

                    el.profile = share.usersDictionary[el.userEmailId];

                    if(mode == "week"){
                        el.value = el.week.userCommitAmount
                    } else if(mode == "quarter"){
                        el.value = el.quarter.userCommitAmount
                    } else {
                        el.value = el.month.userCommitAmount
                    }
                    if (isNumber(parseFloat(el.value))) {
                        el.valueSort = parseFloat(el.value)
                    }
                    el.value = getAmountInThousands(el.value, 2)

                    el.emailId = el.profile ? el.profile.emailId : el.userEmailId;
                    el.fullName = el.profile ? el.profile.fullName : el.userEmailId;
                    el.userId = el.profile ? el.profile.userId : el.userId;
                    el.commitDateTime = moment(el.date).format(standardDateFormat());
                    el.oppsValueUnderCommitStage = 0;
                    el.oppsValueUnderCommitStageSort = 0;
                    $scope.commitsByUsers.push(el)
                });

            }

            var selectedUsers = _.map(userIds,function (el) {
                return share.teamDictionaryByUserId[el];
            })
            var nonExistingUsers = _.differenceBy(_.map(selectedUsers,"emailId"),_.map($scope.commitsByUsers,"emailId"));

            if(nonExistingUsers && nonExistingUsers.length>0){
                _.each(nonExistingUsers,function (el) {
                    $scope.commitsByUsers.push({
                        value:0,
                        valueSort:0,
                        profile:share.usersDictionary[el],
                        commitDateTime:"-",
                        oppsValueUnderCommitStage:0,
                        oppsValueUnderCommitStageSort:0
                    })
                })
            }

            $scope.commitValueSelected = 0;
            $scope.oppValueSelected = 0;
            $scope.target = 0;
            $scope.achievement = 0;

            _.each($scope.commitsByUsers,function (el) {

                el.fullName = el.profile ? el.profile.fullName : el.userEmailId;
                if(el.profile && targetsObj[el.profile.userId]){
                    el.fyTarget = getAmountInThousands(targetsObj[el.profile.userId].target,2,share.primaryCurrency=="INR")
                    el.fyAchivement = getAmountInThousands(targetsObj[el.profile.userId].won,2,share.primaryCurrency=="INR")
                    el.won =targetsObj[el.profile.userId].won;
                    el.target =targetsObj[el.profile.userId].target;
                }

                el.wonPercentage = 0+'%';

                if(el.won && el.target){
                    el.wonPercentage = Math.round(scaleBetween(el.won,0,el.target)).r_formatNumber(2)+'%';
                }

                if(el.won && !el.target){
                    el.wonPercentage = '100%'
                }


                if(el.target<120000){
                    el.commitMet = "red"
                }

                el.wonPercentageStyle = {'width':el.wonPercentage,background: '#8ECECB',height:"inherit"}

                el.oppsValueUnderCommitStage = 0;
                el.oppsValueUnderCommitStageSort = 0
                el.oppsValClosingThisSelection = 0

                var oppsListForUser = [];
                if(el.profile && opportunitiesObj[el.profile.emailId]){
                    oppsListForUser = opportunitiesObj[el.profile.emailId]
                }

                if(oppsListForUser.length>0){
                    _.each(oppsListForUser,function (op) {
                        if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate) <= new Date(end)){
                            if(op.amount && isNumber(el.oppsValueUnderCommitStageSort)){
                                el.oppsValueUnderCommitStageSort = el.oppsValueUnderCommitStageSort+op.amountWithNgm
                            }
                        }

                        el.oppsValClosingThisSelection = el.oppsValClosingThisSelection+op.amountWithNgm
                    })
                }

                el.selected = true;
                $scope.selectedUserValues(el);
                el.oppsValueUnderCommitStage = getAmountInThousands(el.oppsValueUnderCommitStageSort,2,share.primaryCurrency=="INR");
            })

            window.localStorage['commitsByUsers'] = JSON.stringify($scope.commitsByUsers);


            $scope.closing = {
                this:"range"
            }

            $scope.commitsByUsers = $scope.commitsByUsers.filter(function (fl) {
                return fl.profile
            });

            $scope.filterOpps('range');

        });
}

function getTargetsAndAchievements($http,userIds,startDate,viewMode,callback){
    var insightsUrl = "/insights/current/month/stats";

    if(userIds){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"userIds",userIds)
    }

    if(startDate){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"startDate",startDate)
    }

    if(viewMode){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"viewMode",viewMode)
    }

    $http.get(insightsUrl)
        .success(function (insightsResponse) {
            callback(insightsResponse)
        });
}

function setGraphValues(insightsData,commitData,share,mode,oppValue,$scope){

    var stages = [];
    var obj = {};

    obj.idForTooltip = 0
    obj.idForHover = 0

    obj.targetVal = 0
    obj.targetValNum = 0
    obj.achievementVal = 0
    obj.achievementValNum = 0
    obj.selfCommitVal = 0
    obj.relatasCommitVal = 0

    function getOppStages(){
        if(share.opportunityStages){
            stages = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(1000,function () {
                getOppStages()
            })
        }
    }

    getOppStages();

    if(mode == "month"){
        if(insightsData && insightsData.Data && insightsData.Data.month){
            _.each(insightsData.Data.month,function (el) {
                if(el.target){
                    obj.targetValNum = obj.targetValNum+el.target
                }
                if(el.won){
                    obj.achievementValNum = obj.achievementValNum+el.won
                }
            })
        }

        if(commitData){
            if(commitData.commitCurrentMonth.month.userCommitAmount){
                obj.selfCommitVal = commitData.commitCurrentMonth.month.userCommitAmount;
            }

            if(commitData.commitCurrentMonth.month.relatasCommitAmount){
                obj.relatasCommitVal = commitData.commitCurrentMonth.month.relatasCommitAmount;
            }
        }
    } else {

        if(insightsData && insightsData.Data && insightsData.Data.qtr){
            _.each(insightsData.Data.qtr,function (el) {
                if(el.target){
                    obj.targetValNum = obj.targetValNum+el.target
                }

                obj.achievementValNum = obj.achievementValNum+el.won
            })
        }

        if(commitData){
            if(commitData.commitCurrentQuarter.quarter.userCommitAmount){
                obj.selfCommitVal = commitData.commitCurrentQuarter.quarter.userCommitAmount;
            }

            if(commitData.commitCurrentQuarter.quarter.relatasCommitAmount){
                obj.selfCommitVal = commitData.commitCurrentQuarter.quarter.relatasCommitAmount;
            }
        }
    }

    var allValues = [obj.targetValNum,obj.achievementValNum,obj.selfCommitVal,obj.relatasCommitVal]
    var max = _.max(allValues);

    var mon_targetStyle = {background:"#f4ada7",width:scaleBetween(obj.targetValNum,0,max)+"%"},
        mon_achievementStyle = {background:"#8ECECB",width:scaleBetween(obj.achievementValNum,0,max)+"%"},
        mon_relatasCommitStyle = {background:oppStageStyle(commitData.commitStage,stages.indexOf(commitData.commitStage),false,false,true),width:scaleBetween(obj.selfCommitVal,0,max)+"%"},
        mon_selfCommitStyle = {background:"#648ca6",width:scaleBetween(obj.selfCommitVal,0,max)+"%"},
        commitPipelineStyle = {background:"#638ca6",width:scaleBetween(oppValue,0,max)+"%"}

    $scope.shortfall = 0;

    if(obj.targetValNum){
        $scope.shortfall = obj.targetValNum
    }

    if(obj.achievementValNum && obj.targetValNum){
        $scope.shortfall = obj.targetValNum-obj.achievementValNum;
    }

    $scope.shortfall = getAmountInThousands($scope.shortfall,2,share.primaryCurrency=="INR")
    
    obj.target = mon_targetStyle
    obj.commitPipeline = commitPipelineStyle
    obj.commitPipelineVal = getAmountInThousands(oppValue,2,share.primaryCurrency=="INR")
    obj.achievement = mon_achievementStyle
    obj.selfCommit = mon_relatasCommitStyle
    obj.relatasCommit = mon_selfCommitStyle
    obj.displayToolTip = false;

    obj.targetVal = getAmountInThousands(obj.targetValNum,2,share.primaryCurrency=="INR")
    obj.achievementVal = getAmountInThousands(obj.achievementValNum,2,share.primaryCurrency=="INR")

    return obj;
}

function getOppValInCommitStageForSelectedMonth(opps,commitDt,primaryCurrency,currenciesObj) {

    var oppVal = 0,
        startDate = moment(commitDt).startOf("month"),
        end = moment(commitDt).endOf("month");

    _.each(opps,function (op) {

        if(!op.amountNonNGM){
            op.amountNonNGM = parseFloat(op.amount)
        }

        op.convertedAmt = op.amountNonNGM?op.amountNonNGM:parseFloat(op.amount);
        op.convertedAmtWithNgm = parseFloat(op.amount);

        if(op.currency && op.currency !== primaryCurrency){
            if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                op.convertedAmt = op.amountNonNGM/currenciesObj[op.currency].xr
            }

            if(op.netGrossMargin || op.netGrossMargin == 0){
                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
            } else {
                op.convertedAmtWithNgm = op.convertedAmt;
            }

            op.convertedAmt = parseFloat(op.convertedAmt)
            op.convertedAmtWithNgm = parseFloat(op.convertedAmtWithNgm)
        }

        if(new Date(op.closeDate)>=new Date(startDate) && new Date(op.closeDate)<=new Date(end)){
            oppVal = oppVal+op.convertedAmtWithNgm;
        }
    })

    return oppVal

}

function setDateRange(){

    if(share.companyDetails){
        var weekStartDateTime = moment().startOf("isoWeek");
        var weekEndDateTime = moment().endOf("isoWeek");

        var startOfWeek = moment(weekStartDateTime).format('DD MMM')
        var endOfWeek = moment(weekEndDateTime).format('DD MMM')

        if(new Date(weekStartDateTime)>new Date()){
            weekStartDateTime = moment(weekStartDateTime).subtract(1,"week")
            weekEndDateTime = moment(weekEndDateTime).subtract(1,"week")
            startOfWeek = moment(weekStartDateTime).format('DD MMM')
            endOfWeek = moment(weekEndDateTime).format('DD MMM')
        }

        $scope.currentSelection = {
            weekStartDateTime:new Date(weekStartDateTime),
            weekEndDateTime:new Date(weekEndDateTime)
        }

        $scope.dataForRange = startOfWeek+"-"+endOfWeek;
    } else {
        setTimeOutCallback(1000,function () {
            setDateRange()
        })
    }
}

function pickDatesForRenewal($scope,minDate,maxDate,id,addTime,timeType,timezone){

    $(id).datetimepicker({
        value:"",
        timepicker:false,
        validateOnBlur:false,
        minDate: new Date(moment().subtract(2,"week")),
        maxDate: maxDate,
        onSelectDate: function (dp, $input){
            $scope.$apply(function () {
                var timezone = timezone?timezone:"UTC";
                $scope.renewalCloseDate = moment(dp).format();
                $scope.renewalCloseDateFormatted = moment(dp).format(standardDateFormat());
                $("#opportunityCloseDateSelector4").val($scope.renewalCloseDateFormatted)

                $scope.opp.renewed = {
                    closeDate:moment($scope.renewalCloseDate).format(standardDateFormat()),
                    amount:$("#renewalAmount").val()
                }
            });
        }
    });
}

function setTabView($scope,viewFor){
    resetAllTabsOppInsights($scope);
    $scope[viewFor+'Selected'] = "selected"
}

function resetAllTabsOppInsights($scope){
    $scope.basicSelected = ""
    $scope.insightsSelected = ""
    $scope.internalTeamSelected = ""
    $scope.tasksSelected = ""
    $scope.notesSelected = ""
}

function getOppsFromLocalStorage(callback) {
    try {
        var oppsInCommitStage = JSON.parse(window.localStorage['oppsInCommitStage']);
        callback(oppsInCommitStage);
    } catch (e) {
        console.log("error", e);
        callback([]);
    }
}

function closeAllDropDownsAndModals($scope,id,isModal,share) {
    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){

                if(share){
                    if(!share.selectedPortfolio){
                        $scope.dataBy = "org"
                    }
                }

                $scope.fy = false;
                $scope.showByPa = false;
                if(isModal){
                    $scope.closeModal()
                }
            })
        }
    });
}

function filterOutOpps(op,regionAccess,productAccess,verticalAccess,companyId,buAccess){

    var opps = [];
    var filteredApply = false;

    if(regionAccess && regionAccess.length>0){
        filteredApply = true
        if(_.includes(regionAccess,op.geoLocation.zone)){
            opps.push(op)
        }
    }

    if(productAccess && productAccess.length>0) {
        filteredApply = true
        if (_.includes(productAccess, op.productType)) {
            opps.push(op)

        }
    }

    if(verticalAccess && verticalAccess.length>0){
        filteredApply = true
        if(_.includes(verticalAccess,op.vertical)){
            opps.push(op)

        }
    }

    if(buAccess && buAccess.length>0){
        filteredApply = true
        if(_.includes(buAccess,op.businessUnit)){
            opps.push(op)

        }
    }

    if(!filteredApply){
        return op;
    } else {
        return opps[0]?opps[0]:null;
    }
}

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}
