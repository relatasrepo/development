var relatasApp = angular.module('relatasApp', ['angular-loading-bar','ngLodash']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.service('share', function() {
    return {
    }
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }

        else toastr.error("Please enter search content")
    };
});

relatasApp.controller("logedinUser", function($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        share.companyDetails.currency.forEach(function (el) {
            share.currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                share.primaryCurrency = el.symbol;
            }
        });

        $rootScope.primaryCurrency = share.primaryCurrency;

        setTimeOutCallback(100,function () {
            share.populateFilters(share.companyDetails);
            share.setCurrentQuarter(getCurrentFiscalYear("UTC",response.companyDetails.fyMonth))
        })
    })
});

relatasApp.controller("liu_hierarchy", function($scope, $http, share,$rootScope) {

    $scope.getDataFor = function (member) {
        $scope.selection = member && member != "all"?member:{fullName:"Show all team members",emailId:"Show all team members",nameNoImg:"All",noPicFlag:true};
        share.selection = $scope.selection;
        share.getOpps(member.emailId?member.emailId:null);
        share.resetPrevfilters();
        share.rangeType = null // Reset range type
        $scope.selectFromList = !$scope.selectFromList
        share.setAllTeamMembers($scope.selection.fullName == "Show all team members");
        share.setLoaders();
        share.setLoaders2();

        if($scope.selection.fullName != "Show all team members"){
            share.forOppGrowth($scope.selection.userId)
            share.forSnapshot($scope.selection.userId)
            share.getCurrentInsights($scope.selection.emailId)
        } else {
            share.forOppGrowth(_.map(share.team,"userId"))
            share.forSnapshot(_.map(share.team,"userId"))
            share.getCurrentInsights(_.map(share.team,"emailId"))
        }

        if($scope.selection.fullName != "Show all team members"){
            share.forAccsCreated($scope.selection.userId)
        } else {
            share.forAccsCreated(_.map(share.team,"userId"))
        }
    }

    closeAllDropDownsAndModals($scope,".list");

    $scope.getLiuHierarchy = function () {

        $http.get('/company/user/hierarchy')
            .success(function (response) {
                $scope.team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.team = buildTeamProfiles(response.Data)
                    $scope.selection = $scope.team[0];
                    share.team = $scope.team;

                    var usersDictionary = {};

                    if(response.companyMembers.length>0){
                        var companyMembers = buildAllTeamProfiles(response.companyMembers)

                        _.each(companyMembers,function (member) {
                            usersDictionary[member.emailId] = member
                        })
                    }

                    share.usersDictionary = usersDictionary;
                }
            });
    }

    $scope.getLiuHierarchy();

    share.resetUserSelection = function(){
        $scope.selection = $scope.team[0];
        $scope.getDataFor($scope.selection)
        $scope.selectFromList = false;
    }

});

relatasApp.controller("wrapper_controller", function($scope, $http, share,$rootScope) {

    // $scope.menu = menuItems(loadDataOnlyForInternalTeam());
    $scope.menu = menuItems(true);
    $scope.selectedTab = $scope.menu[0];
    $scope.viewFor = $scope.menu[0].name.toLowerCase();

    check_right_data_panel_loaded();

    function check_right_data_panel_loaded(){
        if(share.viewFor){
            share.viewFor($scope.viewFor);
        } else {
            setTimeOutCallback(100,function () {
                share.viewFor($scope.viewFor);
            })
        }
    }

    $scope.openViewFor = function (viewFor,dontLoadOppsHere) {
        $scope.viewFor = viewFor.name.toLowerCase();
        share.viewFor($scope.viewFor);
        share.resetPrevfilters()
        share.resetUserSelection()
        menuToggleSelection(viewFor.name,$scope.menu)
    };

    share.openViewFor = function(viewFor,dontLoadOppsHere){
        $scope.openViewFor(viewFor,dontLoadOppsHere)
    }

    share.viewFor = function (viewFor) {
        $scope.viewFor = viewFor;
    }

})

relatasApp.controller("exceptionalAccess", function($scope,$http,share,$rootScope){

});

relatasApp.controller("opportunities", function($scope,$http,share,$rootScope){

    $scope.loadingMetaData = true;

    share.setLoaders = function(){
        $scope.loadingMetaData = true;
    }

    spiderChartInit(share,35,function (RadarChart) {
        var opps = [],
            interactions = [];

        interactions = [
            {axis:"Apple", value: 20},
            {axis:"Intel", value: 30},
            {axis:"HP", value: 50},
            {axis:"Intercom", value: 90},
            {axis:"Wipro", value: 150},
            {axis:"Intercom", value: 175},
            {axis:"Cognizant", value: 200},
            {axis:"Accenture", value: 225},
            {axis:"Mindtree", value: 250},
            {axis:"Amazon", value: 300},
            {axis:"Cisco", value: 350}
        ]

        opps = [
            {axis:"Apple", value: 20},
            {axis:"Intel", value: 10},
            {axis:"HP", value: 5},
            {axis:"Intercom", value: 15},
            {axis:"Wipro", value: 50},
            {axis:"Intercom", value: 105},
            {axis:"Cognizant", value: 95},
            {axis:"Accenture", value: 125},
            {axis:"Mindtree", value: 50},
            {axis:"Amazon", value: 20},
            {axis:"Cisco", value: 150}
        ]
        spiderDataInit(RadarChart,[],share,200,200,"#spiderChart2",opps,interactions);
    });

    spiderChartInit(share,35,function (RadarChart) {
        var opps = [],
            interactions = [];

        opps = [
            {axis:"Apple #1", value: 5},
            {axis:"Apple #2", value: 15},
            {axis:"Intel #1", value: 7},
            {axis:"Intel #2", value: 3},
            {axis:"HP", value: 5},
            {axis:"Intercom", value: 15},
            {axis:"Wipro #2", value: 5},
            {axis:"Wipro IFSC", value: 25},
            {axis:"Wipro #3", value: 10},
            {axis:"Intercom #1", value: 15},
            {axis:"Intercom #2", value: 35},
            {axis:"Cognizant BFSI", value: 25},
            {axis:"Cognizant", value: 25},
            {axis:"Cognizant", value: 35},
            {axis:"Accenture", value: 25},
            {axis:"Mindtree #1", value: 20},
            {axis:"Mindtree #2", value: 30},
            {axis:"Amazon", value: 20},
            {axis:"Cisco #1", value: 50},
            {axis:"Cisco #4", value: 25},
            {axis:"Cisco #5", value: 50},
            {axis:"Cisco #2", value: 25}
        ]

        interactions = [
            {axis:"Apple #1", value: 5},
            {axis:"Apple #2", value: 10},
            {axis:"Intel #1", value: 15},
            {axis:"Intel #2", value: 20},
            {axis:"HP", value: 25},
            {axis:"Intercom", value: 26},
            {axis:"Wipro #2", value: 28},
            {axis:"Wipro IFSC", value: 29},
            {axis:"Wipro #3", value: 30},
            {axis:"Intercom #1", value: 30},
            {axis:"Intercom #2", value: 35},
            {axis:"Cognizant BFSI", value: 35},
            {axis:"Cognizant", value: 35},
            {axis:"Cognizant", value: 40},
            {axis:"Accenture", value: 45},
            {axis:"Mindtree #1", value: 45},
            {axis:"Mindtree #2", value: 45},
            {axis:"Amazon", value: 55},
            {axis:"Cisco #1", value: 55},
            {axis:"Cisco #4", value: 65},
            {axis:"Cisco #5", value: 65},
            {axis:"Cisco #2", value: 68}
        ]
        spiderDataInit(RadarChart,[],share,200,200,"#spiderChart1",opps,interactions);
    });

    $scope.getDetails = function (colType) {
        if(colType.colType == "Deals At Risk"){

            if(share.selection && share.selection.emailId == "Show all team members"){
                alert("Deals at risk insights not available for team. Please select individual team members to view deals at risk");
            } else {
                var userId = share.selection?share.selection.userId:share.liuData.userId;
                share.forDealsAtRisk(userId);
            }
        }
    }

    $scope.selectAll = function (colType) {
        _.each(colType.values,function (el) {
            el.selected = colType.selectingAll;
            $scope.selectFilter(colType.type,el)
        })
    }

    share.filterOpps = function (item,fromDashboard) {
        $scope.filterOpps(item,fromDashboard)
    }

    share.setCurrentQuarter = function (data) {

        var startOfQuarter = data.quarter.obj[data.quarter.currentQuarter].start,
            endOfQuarter = data.quarter.obj[data.quarter.currentQuarter].end;

        share.startOfQuarter = startOfQuarter;
        share.endOfQuarter = endOfQuarter;

        $scope.dateRange = {
            text:moment(startOfQuarter).format("MMM YYYY")+"-"+moment(endOfQuarter).format("MMM YYYY"),
            show:true
        };
    }

    $scope.goToOpp = function (op) {
        window.location = "/opportunities/all?opportunityId="+op.opportunityId
    }

    $scope.filterOpps = function (item,fromDashboard) {

        if(!$scope.filtersApplied || $scope.filtersApplied.length>0){
            $scope.filtersApplied = [];
        }

        if(item.colType == "Won"){
            $scope.filtersApplied.push({
                name:"Close Won",
                type:"stageName"
            });
        }

        if(item.colType == "Lost"){
            $scope.filtersApplied.push({
                name:"Close Lost",
                type:"stageName"
            });
        }

        if(item.colType == "Deals At Risk"){
            $scope.filtersApplied.push({
                name:"Close Won",
                type:"source"
            });
        }

        if(item.colType == "Renewal"){
            $scope.filtersApplied.push({
                name:"renewal",
                type:"source"
            });
        }

        var start = share.startOfQuarter
        var end = share.endOfQuarter

        if(item.colType == "Closing"){

            _.each(share.companyDetails.opportunityStages,function (el) {
                if(el.name !== "Close Won" && el.name !== "Close Lost"){
                    $scope.filtersApplied.push({
                        name:el.name,
                        type:"stageName"
                    });
                }
            });

            $scope.start = {
                month:moment(start).month,
                year:moment(start).year,
            }

            $scope.end = {
                month:moment(end).month,
                year:moment(end).year,
            }

            $scope.filtersApplied.push({
                name:moment(start).format("MMM YYYY")+"-"+moment(end).format("MMM YYYY"),
                type:"closeDate",
                start:start,
                end:end
            });

        } else {

            $scope.filtersApplied.push({
                name:moment(start).format("MMM YYYY")+"-"+moment(end).format("MMM YYYY"),
                type:"closeDate",
                start:start,
                end:end
            })
        }

        $scope.applyFilters($scope.filtersApplied[0],true);
        share.rangeType = "This Quarter"
    }

    $scope.dropDownSelection = null;
    $scope.openFilterDropDown = function (type) {
        type.open = true;
        resetOtherDropDowns($scope,type)
    }

    $scope.sortType = "closeDate";
    $scope.sortReverse = false;
    $scope.sortTable = function (item) {
        $scope.sortReverse = !$scope.sortReverse;
        $scope.sortType = item.type
    }

    $scope.sortTableByNumbers = function (item) {
        if(item.type == 'amount' || item.type == 'netGrossMargin' || item.type == 'convertedAmt' || item.type == 'convertedAmtWithNgm'){
            $scope.sortReverse = !$scope.sortReverse;
            $scope.sortType = item.type
        }
    }

    closeAllDropDownsAndModals($scope,".drop-down");

    share.resetPrevfilters = function () {
        $scope.start = {};
        $scope.end = {};

        $scope.filtersApplied = [];
        _.each($scope.headers,function (he) {
            if(he.values && he.values.length>0){
                _.each(he.values,function (va) {
                   va.selected = false;
                });
            }
        })
    }

    $scope.months = monthsAndYear().months
    $scope.years = monthsAndYear().years;

    $scope.start = {};
    $scope.end = {};

    $scope.selectFilter = function (type,filter,colType) {

        if(!$scope.filtersApplied){
            $scope.filtersApplied = [];
        }

        if(!filter.selected){
            $scope.filtersApplied = $scope.filtersApplied.filter(function (fl) {
                return fl.name !== filter.name
            })
        }

        if(filter.selected){

            var found = false;

            _.each($scope.filtersApplied,function (fl) {
                if(fl.name == filter.name && fl.type == type){
                    found = true;
                    return false;
                }
            });

            if(!found){

                if(type == 'userEmailId'){

                    share.selection = share.usersDictionary[filter.name]?share.usersDictionary[filter.name]:{fullName:filter.name,emailId:filter.name}

                    $scope.filtersApplied.push({
                        name:filter.name,
                        type:type,
                        displayName:share.usersDictionary[filter.name].fullName
                    });
                } else {
                    $scope.filtersApplied.push({
                        name:filter.name,
                        type:type,
                        displayName:filter.name
                    });
                }
            }
        }

        if(colType && colType.values){

            var selectingAll = true;
            _.each(colType.values,function (el) {
                if(!el.selected){
                    selectingAll = false;
                    return false;
                }
            })

            colType.selectingAll = selectingAll;
        }

        $scope.filtersApplied = _.uniqBy($scope.filtersApplied,"name");
    };

    $scope.applyFilters = function (colType,dontUpdateMetaData,index) {

        if(!$scope.filtersApplied){
            $scope.filtersApplied = [];
        }

        var closeDateExists = false;

        if(colType.type === "closeDate"){

            $scope.filtersApplied = $scope.filtersApplied.filter(function (fl) {
                return fl.type != "closeDate"
            });

            if($scope.start && typeof $scope.start.month == "string"){

                var start = moment().year(parseInt($scope.start.year)).month(parseInt($scope.start.month)-1)
                var end = moment().year(parseInt($scope.end.year)).month(parseInt($scope.end.month)-1);

                $scope.filtersApplied.push({
                    name:moment(start).format("MMM YYYY")+"-"+moment(end).format("MMM YYYY"),
                    type:colType.type,
                    start:start,
                    end:end
                });

                share.rangeType = moment(start).format("MMM YYYY")+"-"+moment(end).format("MMM YYYY")
            }

        }

        $scope.dateRange.show = !$scope.dateRange.show;

        colType.open = !colType.open;

        _.each($scope.headers,function (he) {
            he.open = false;
        })

        var userEmailIdExists = false;
        _.each($scope.filtersApplied,function (el) {
            if(el.type == "userEmailId"){
                userEmailIdExists = true;
            }

            if(el.type == "closeDate"){
                closeDateExists = true;
            }
        });

        if(share.selection && share.selection.fullName !== "Show all team members"){
            $scope.filtersApplied.push({
                name:share.selection.emailId,
                displayName:share.selection.fullName,
                type:"userEmailId"
            })
        } else if(!userEmailIdExists && !share.selection){
            $scope.filtersApplied.push({
                name:share.liuData.emailId,
                displayName:share.liuData.fullName,
                type:"userEmailId"
            })
        }

        if(!closeDateExists){
            // share.rangeType = "All FYs"

            $scope.filtersApplied.push({
                name:moment(share.quarterRange.qStart).format("MMM YYYY")+"-"+moment(share.quarterRange.qEnd).format("MMM YYYY"),
                type:"closeDate",
                start:share.quarterRange.qStart,
                end:share.quarterRange.qEnd
            });
        }

        _.each($scope.stageMetaInfo,function (el) {
            el.rangeType = "This Quarter"
        })

        $scope.filtersApplied = _.uniqBy($scope.filtersApplied,"name");

        share.getOpps(null,$scope.filtersApplied,dontUpdateMetaData);
    }

    share.populateFilters = function(companyDetails){

        $scope.companyDetails = companyDetails;

        $scope.filterLists = [],$scope.filterListObj = {};
        for(var key in companyDetails){

            if(_.includes(["opportunityStages","accountTypes","businessUnits","geoLocations","productList","solutionList","sourceList","typeList","verticalList"], key)){

                var values = companyDetails[key];
                if(key === "geoLocations"){
                    values = [];
                    _.each(companyDetails[key],function (el) {
                        values.push({
                            name:el.region
                        })
                    });
                }

                values.forEach(function (el) {
                    if(key == "typeList"){
                        el.displayName = el.name; //this is needed for sorting.
                    }
                    el.selected = false;
                });

                var typeFormat = getTypeFormat(key);

                $scope.filterListObj[typeFormat] = {
                    type:key,
                    typeFormatted:typeFormat,
                    values:values
                }

                $scope.filterLists.push({
                    type:key,
                    typeFormatted:typeFormat,
                    values:values
                })
            }
        }

        setOppTableHeader($scope,share,$scope.filterListObj);

    }

    share.drawOppTabPipeline = function (wonAmt,lostAmt,wonCount,lostCount,pipelineAmt,pipelineCount,closing30DaysAmt,closing30DaysCount,dealsAtRiskCount,totalDealValueAtRisk,dealsRiskAsOfDate,renewalAmt,renewalCount,staleAmt,staleCount){
        drawPipeline($scope,wonAmt,lostAmt,wonCount,lostCount,pipelineAmt,pipelineCount,closing30DaysAmt,closing30DaysCount,dealsAtRiskCount,totalDealValueAtRisk,dealsRiskAsOfDate,renewalAmt,renewalCount,staleAmt,staleCount,share);
    }

    share.drawOppsTable = function (opps,contacts,owners,productsAll,accountsAll) {

        if(!$scope.filtersApplied || $scope.filtersApplied.length == 0){
            $scope.filtersApplied = [];
        }

        $scope.opps = opps;

        checkHeadersLoaded()
        function checkHeadersLoaded(){
            if($scope.headers){

                _.each($scope.headers,function (he) {

                    if(he.name == "Contact"){

                        if(!he.values || !he.values[0]){
                            he.values = _.uniqBy(contacts,"name");
                        }
                    }

                    if(he.name == "Owner"){
                        he.values = _.uniqBy(owners,"name");
                        he.values.forEach(function (el) {
                            el.displayName = share.usersDictionary[el.name]?share.usersDictionary[el.name].fullName:el.name
                        })
                    }

                    if(he.name == "Product" && productsAll && productsAll.length>0){
                        if(!he.values || !he.values[0]){
                            he.values = _.uniqBy(productsAll,"name");
                        }
                    }

                    if(he.name == "Account"){
                        if(!he.values || !he.values[0]){
                            he.values = _.uniqBy(accountsAll,"name");
                        }
                    }
                });

                // Prepare Excel data:
                $scope.fileName = 'Opps-'+moment().format("DDMMMMYY");
                $scope.exportData = [];
                // Headers:
                $scope.exportData.push(getOppXLSHeaders());
                // Data:

                angular.forEach($scope.opps, function(el, key) {

                    $scope.exportData.push([
                        el.opportunityName,
                        el.userEmailId,
                        el.contactEmailId,
                        fetchCompanyFromEmail(el.contactEmailId),
                        el.currency,
                        parseFloat(el.amount),
                        parseFloat(el.netGrossMargin),
                        parseFloat(el.amountWithNgm.toFixed(2)),
                        share.currenciesObj[el.currency] && share.currenciesObj[el.currency].xr?share.currenciesObj[el.currency].xr:1,
                        parseFloat(el.convertedAmt),
                        parseFloat(el.convertedAmtWithNgm.toFixed(2)),
                        el.stageName,
                        new Date(el.closeDate),
                        el.productType,
                        el.businessUnit,
                        el.solution,
                        el.type,
                        el.geoLocation?el.geoLocation.zone:"",
                        el.geoLocation?el.geoLocation.town:"",
                        el.sourceType,
                        el.vertical,
                        _.map(el.partners,"emailId").join(","),
                        new Date(el.createdDate),
                        el.createdByEmailId,
                        el.opportunityId
                    ]);
                });

                $scope.loadingMetaData = false;
            } else {
                setTimeOutCallback(500,function () {
                    checkHeadersLoaded();
                })
            }
        }
    }

    $scope.removeFilter = function (filter) {

        $scope.filtersApplied = $scope.filtersApplied.filter(function (fl) {
           return fl.name !== filter.name
        });

        if($scope.filtersApplied.length == 0){
            share.rangeType = "This Quarter";
        } else {
            share.rangeType = "All FYs";
            _.each($scope.filtersApplied,function (fl) {
                if(fl.type == "closeDate"){
                    share.rangeType = fl.name;
                }
            })
        }

        share.getOpps(null,$scope.filtersApplied)
    }

});

relatasApp.controller("currentInsights", function($scope,$http,share,$rootScope){
    checkLiuDataLoaded();

    function checkLiuDataLoaded(){
        if(share.liuData){

            setTimeOutCallback(1000,function () {
                currentInsights($scope,$http,share,$rootScope,share.liuData.emailId);
            })
        } else {
            setTimeOutCallback(200,function () {
                checkLiuDataLoaded()
            })
        }
    }

    share.getCurrentInsights = function(emailId){
        currentInsights($scope,$http,share,$rootScope,emailId)
    }

    share.currentInsightsData = function(pipelinePercentage,achievementPercentage){
        $scope.pipelinePercentage = pipelinePercentage;
        $scope.achievementPercentage = achievementPercentage;
    }

    $scope.loadingMetaData = true;
})

function spiderChartInit(share,TranslateX,callback) {

    var relationshipColors = {
        "decision-maker":"#db856c",
        "influencer": "#008080a1",
        "none":"#ccc"
    }

    var RadarChart = {
        draw: function(id, d, options){
            var cfg = {
                radius: 2, //dot radii
                w: 300,
                h: 300,
                factor: 1,
                factorLegend: .85,
                levels: 5,
                maxValue: 100,
                radians: 2 * Math.PI,
                opacityArea: 0.5,
                ToRight: 5,
                TranslateX: TranslateX?TranslateX:80,
                TranslateY: 30,
                ExtraWidthX: 100,
                ExtraWidthY: 100,
                color: d3.scale.category10()
            };

            if('undefined' !== typeof options){
                for(var i in options){
                    if('undefined' !== typeof options[i]){
                        cfg[i] = options[i];
                    }
                }
            }
            cfg.maxValue = Math.max(cfg.maxValue, d3.max(d, function(i){return d3.max(i.map(function(o){return o.value;}))}));
            var allAxis = (d[0].map(function(i, j){return i.axis}));
            var total = allAxis.length;
            var radius = cfg.factor*Math.min(cfg.w/2, cfg.h/2);
            var Format = d3.format('%');
            d3.select(id).select("svg").remove();

            var g = d3.select(id)
                .append("svg")
                .attr("width", cfg.w+cfg.ExtraWidthX)
                .attr("height", cfg.h+cfg.ExtraWidthY)
                .append("g")
                .attr("transform", "translate(" + cfg.TranslateX + "," + cfg.TranslateY + ")");
            ;

            var tooltip;

            //Circular segments
            for(var j=0; j<cfg.levels-1; j++){
                var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
                g.selectAll(".levels")
                    .data(allAxis)
                    .enter()
                    .append("svg:line")
                    .attr("x1", function(d, i){return levelFactor*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
                    .attr("y1", function(d, i){return levelFactor*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
                    .attr("x2", function(d, i){return levelFactor*(1-cfg.factor*Math.sin((i+1)*cfg.radians/total));})
                    .attr("y2", function(d, i){return levelFactor*(1-cfg.factor*Math.cos((i+1)*cfg.radians/total));})
                    .attr("class", "line")
                    .style("stroke", "#ccc")
                    .style("stroke-opacity", "0.75")
                    .style("stroke-width", "0.3px")
                    .attr("transform", "translate(" + (cfg.w/2-levelFactor) + ", " + (cfg.h/2-levelFactor) + ")");
            }

            series = 0;

            var axis = g.selectAll(".axis")
                .data(allAxis)
                .enter()
                .append("g")
                .attr("class", "axis");

            axis.append("line")
                .attr("x1", cfg.w/2)
                .attr("y1", cfg.h/2)
                .attr("x2", function(d, i){return cfg.w/2*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
                .attr("y2", function(d, i){return cfg.h/2*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
                .attr("class", "line")
                .style("stroke", "grey")
                .style("stroke-width", "0.65px")
                .on('mouseover', function (d) {

                    d3.select(this).style("stroke-width", "2px");

                    tooltip
                        .attr('x', parseFloat(d3.select(this).attr('x2')))
                        .attr('y', parseFloat(d3.select(this).attr('y2')) - 5)
                        .text(d)
                        .transition(200)
                        .style('opacity', 1);
                })
                .on('mouseout', function (d) {
                    d3.select(this).style("stroke-width", ".65px");

                    tooltip
                        .transition(200)
                        .style('opacity', 0);
                    g.selectAll("polygon")
                        .transition(200)
                        .style("fill-opacity", cfg.opacityArea);
                });

            axis.append("text")
                .attr("class", "legend")
                .on('mouseover', function (d){

                    var fullText = ""

                    $(this)
                        .attr("class", "no-pointer")
                        .text(fullText)
                        .css({'margin-top':'100px'})
                        .css({'font-size':'11px'})
                        .css({'pointer-events':'none'})
                })
                .on('mouseout', function(d){
                    $(this)
                        .attr("class", "legend")
                        .text(".")
                        .attr("fill", function (d) {
                        })
                        .css("font-size", "70px")
                        .css({'pointer-events':'auto'})

                })
                .attr("dy", "0.25em")
                .attr("transform", function(d, i){return "translate(0, -10)"})
                .attr("x", function(d, i){return cfg.w/2*(0.9-cfg.factorLegend*Math.sin(i*cfg.radians/total))-60*Math.sin(i*cfg.radians/total);})
                .attr("y", function(d, i){return cfg.h/2*(1-Math.cos(i*cfg.radians/total))-20*Math.cos(i*cfg.radians/total);})
                .text(function(d) {
                    if(!TranslateX){
                        return ".";
                    }
                })
                .attr("fill", function (d) {
                })
                .style("font-family", "Lato")
                .style("font-size", "70px")
                .attr("text-anchor", "middle")


            d.forEach(function(y, x){
                dataValues = [];
                g.selectAll(".nodes")
                    .data(y, function(j, i){
                        dataValues.push([
                            cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)),
                            cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total))
                        ]);
                    });
                dataValues.push(dataValues[0]);
                g.selectAll(".area")
                    .data([dataValues])
                    .enter()
                    .append("polygon")
                    .attr("class", "radar-chart-serie"+series)
                    .style("stroke-width", "2px")
                    .style("stroke", cfg.color(series))
                    .attr("points",function(d) {
                        var str="";
                        for(var pti=0;pti<d.length;pti++){
                            str=str+d[pti][0]+","+d[pti][1]+" ";
                        }
                        return str;
                    })
                    .style("fill", function(j, i){return cfg.color(series)})
                    .style("fill-opacity", cfg.opacityArea)
                    .on('mouseover', function (d){
                        z = "polygon."+d3.select(this).attr("class");
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", 0.1);
                        g.selectAll(z)
                            .transition(200)
                            .style("fill-opacity", .7);
                    })
                    .on('mouseout', function(){
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", cfg.opacityArea);
                    });
                series++;
            });
            series=0;


            d.forEach(function(y, x){
                g.selectAll(".nodes")
                    .data(y).enter()
                    .append("svg:circle")
                    .attr("class", "radar-chart-serie"+series)
                    .attr('r', cfg.radius)
                    .attr("alt", function(j){return Math.max(j.value, 0)})
                    .attr("cx", function(j, i){
                        dataValues.push([
                            cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)),
                            cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total))
                        ]);
                        return cfg.w/2*(1-(Math.max(j.value, 0)/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total));
                    })
                    .attr("cy", function(j, i){
                        return cfg.h/2*(1-(Math.max(j.value, 0)/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total));
                    })
                    .attr("data-id", function(j){return j.axis})
                    .style("fill", cfg.color(series)).style("fill-opacity", .9)
                    .on('mouseover', function (d){

                        var fullText = d.axis;

                        newX =  parseFloat(d3.select(this).attr('cx')) - 60;
                        newY =  parseFloat(d3.select(this).attr('cy')) - 5;

                        tooltip
                            .attr('x', newX)
                            .attr('y', newY)
                            .text(fullText)
                            .transition(200)
                            .style('opacity', 1);

                        z = "polygon."+d3.select(this).attr("class");
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", 0.1);
                        g.selectAll(z)
                            .transition(200)
                            .style("fill-opacity", .7);
                    })
                    .on('mouseout', function(){
                        tooltip
                            .transition(200)
                            .style('opacity', 0);
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", cfg.opacityArea);
                    })
                    .append("svg:title")
                    .text(function(j){
                        // return Math.max(j.value, 0)
                    });

                series++;
            });
            // Tooltip
            tooltip = g.append('text')
                .style('opacity', 0)
                .style('font-family', 'Lato')
                .style('font-size', '11px');

            var angleSlice = Math.PI * 2 / total;

            var radarLine = d3.svg.line.radial()
                .interpolate("basis")
                .radius(function(d) { return rScale(d.value); })
                .angle(function(d,i) {	return i*angleSlice; });

            var rScale = d3.scale.linear()
                .range([0, radius])
                .domain([0, cfg.maxValue]);

        }
    };

    callback(RadarChart)
}

function spiderDataInit(RadarChart,data,share,w,h,id,opps,interactions){
    w = w?w:200;
    h = h?h:200;
    id = id?id:"#spiderChart"

    var colorscale = d3.scale.category10();

//Legend titles
    var LegendOptions = ['Accounts','Opportunities'];

    var nonExistingInOpps = [],
        nonExistingInInts = [];

    if(interactions.length>opps.length){
        nonExistingInOpps = _.differenceBy(interactions,opps,"axis")
    } else {
        nonExistingInOpps = _.differenceBy(interactions,opps,"axis")
    }

    if(opps.length>interactions.length){
        nonExistingInInts = _.differenceBy(opps,interactions,"axis")
    } else {
        nonExistingInInts = _.differenceBy(opps,interactions,"axis")
    }

    if(nonExistingInOpps.length>0){
        _.each(nonExistingInOpps,function (el) {
            opps.push({
                axis:el.axis,
                value: 0
            })
        })
    }

    if(nonExistingInInts.length>0){
        _.each(nonExistingInInts,function (el) {
            interactions.push({
                axis:el.axis,
                value: 0
            })
        })
    }

    var minInt = _.minBy(interactions,"value");
    var maxInt = _.maxBy(interactions,"value");
    var minOpp = _.minBy(opps,"value");
    var maxOpp = _.maxBy(opps,"value");
    var minOppAllowed = minOpp.value>0?1:0;
    var minIntAllowed = minInt.value>0?1:0;
    var dataOpps = [],
        dataInts = []

    _.each(interactions,function (el) {

        dataInts.push({
            axis:el.axis,
            value:scaleBetween(el.value,minInt.value,maxInt.value,minIntAllowed)
        })
    })

    if(opps.length>0) {
        _.each(opps,function (el) {
            dataOpps.push({
                axis:el.axis,
                value: scaleBetween(el.value,minOpp.value,maxOpp.value,minOppAllowed)
            })
        })
    };

//Data
    var d = [dataInts,dataOpps];

//Options for the Radar chart, other than default
    var mycfg = {
        w: w,
        h: h,
        maxValue: 100,
        levels: 5,
        ExtraWidthX: 300
    }

//Call function to draw the Radar chart
//Will expect that data is in %'s
    RadarChart.draw(id, d, mycfg);

    var svg = d3.select('#body')
        .selectAll('svg')
        .append('svg')
        .attr("width", w+300)
        .attr("height", h)
}

function getOppXLSHeaders(){
    return ["Opp Name" ,
        "Opp Owner" ,
        "Contact (selling to)" ,
        "Account",
        "Currency",
        "Amount",
        "Margin",
        "Bottomline",
        "Exchange Rate",
        "Top Line (Primary Currency)",
        "Bottom Line (Primary Currency)",
        "Stage",
        "Close Date",
        "Product",
        "BU ",
        "Solution",
        "Type",
        "Region",
        "City",
        "Source",
        "Vertical",
        "Partners",
        "Created Date",
        "Created By" ,
        "Opportunity Id" ]
}

relatasApp.controller("dashboard", function($scope,$http,share,$rootScope){

    share.setLoaders2 = function(){
        $scope.loadingMetaData = true;
    }

    checkLiuDataLoaded()

    function checkLiuDataLoaded(){
        if(share.liuData){
            getOpps($scope,$http,share,$rootScope,share.liuData.emailId)
        } else {
            setTimeOutCallback(200,function () {
                checkLiuDataLoaded()
            })
        }
    }

    $scope.loadingMetaData = true;

    share.newOppsCreated = function (data) {

        function checkQuarterRangeLoaded(){
            if(share.quarterRange){

                $scope.newOppsAdded = _.sumBy(data,function (el) {
                    if(new Date(el.sortDate) >= new Date(share.quarterRange.qStart) && new Date(el.sortDate) <= new Date(share.quarterRange.qEnd)){
                        return el.count
                    }
                })
            } else {
                setTimeOutCallback(1000,function () {
                    checkQuarterRangeLoaded()
                })
            }
        }

        checkQuarterRangeLoaded()
    }

    share.getOpps = function (emailId,filtersApplied,dontUpdateMetaData) {
        getOpps($scope,$http,share,$rootScope,emailId,filtersApplied,dontUpdateMetaData)
    }

    // share.setOppFlowHeader = function (response) {
    //     $scope.oppFlowHeader = moment(response.qStart).format("MMMM") +"- "+moment(moment(response.qStart).add(1,"month")).format("MMMM") +"- "+moment(response.qEnd).format("MMMM YYYY");
    // }

    $scope.filterOpps = function (item) {
        share.filterOpps(item,true)
        share.openViewFor({
            name:"Opportunity",
            selected:""
        },true)
    }

    share.setAllTeamMembers = function (settings) {
        $scope.allTeamMembers = settings
    }

    $scope.showTable = function (table) {

        if(share.selection && share.selection.emailId == "Show all team members"){
            alert("Pipeline velocity insights not available for team. Please select individual team members to view pipeline velocity");
        } else {

            if(table == "pipelineVelocity"){
                $scope.pipelineVelocityCss = "insight-selection"
                var userId = share.selection?share.selection.userId:share.liuData.userId;
                share.showPipelineVelocity(true,userId);
            }
        }
    }

    $scope.getDetails = function (colType) {

        if(colType.colType == "Deals At Risk"){

            if(share.selection && share.selection.emailId == "Show all team members"){
                alert("Deals at risk insights not available for team. Please select individual team members to view deals at risk");
            } else {
                var userId = share.selection?share.selection.userId:share.liuData.userId;
                share.forDealsAtRisk(userId);
            }
        }
    }

});

relatasApp.controller("pipeline_velocity",function ($scope,$http,share,$rootScope) {

    $scope.closePipelineVelocity = function () {
        $scope.openPipelineVelocity = false;
    }

    $scope.takeAction = function (opp) {

        if(!$rootScope.noAccess){
            $scope.opp = opp;
            $scope.showModal = true;
        }
    }

    $scope.closeModal = function () {
        $scope.showModal = false;
    }

    $scope.sortType = 'closeDate';
    $scope.sortReverse = false;

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.registerDatePickerId = function(){

        $('#opportunityCloseDateSelector4').datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    updateOpportunity($scope,$http,$scope.opp,"closeDate",dp,false,function (result) {
                        if(result){
                            share.refreshDealsAtRisk();
                            $scope.opp.formattedCloseDate = moment(dp).format("DD MMM YYYY");
                        } else {

                        }
                    })
                });
            }
        });
    }

    share.showPipelineVelocity = function (value,userId) {
        $scope.openPipelineVelocity = value;
        getPipelineVelocity(userId)
    }

    $scope.goTo = function () {
        window.location = "/opportunities/all"
    }

    share.forPipelineVelocity = function (userId,accessControl) {
        getPipelineVelocity(userId,accessControl)
    }

    function getPipelineVelocity (userId,accessControl){

        var url = '/insights/pipeline/velocity'
        if(userId){
            url = url+"?userId="+userId;
        }

        if(accessControl){

            if(share.liuData && share.liuData.orgHead){
                url = fetchUrlWithParameter(url+"&accessControl="+true)
                url = fetchUrlWithParameter(url+"&companyId="+share.liuData.companyId)
            } else {
                url = fetchUrlWithParameter(url+"&accessControl="+true)
            }
        }

        $http.get(url)
            .success(function (response) {
                if(response && response.SuccessCode){

                    var opportunityStages = {};

                    if(share.opportunityStages){
                        _.each(share.opportunityStages,function (op) {
                            opportunityStages[op.name] = op.order;
                        })
                    }

                    $scope.expectedPipeline = response.Data.expectedPipeline
                    $scope.deals = response.Data.oppNextQ && response.Data.oppNextQ[0] && response.Data.oppNextQ[0].opportunities?response.Data.oppNextQ[0].opportunities:[];
                    $scope.currentQuarter = response.Data.currentQuarter;

                    var allValues = [];
                    var target = response.Data.currentTargets[0]? response.Data.currentTargets[0].target:0

                    var pipeline = 0,won=0;
                    _.each(response.Data.currentOopPipeline,function (op) {

                        if(op._id == "Close Won"){
                            won = won+op.sumOfAmount
                        }

                        if(op._id != "Close Won" && op._id != "Close Lost"){
                            pipeline = pipeline+op.sumOfAmount
                        }
                    });

                    $scope.staleOppsExist = false;
                    $scope.nextQuarterOppsExist = false;

                    var gap = target - won;

                    allValues.push(target)
                    allValues.push(pipeline)
                    allValues.push(won)
                    allValues.push(gap)

                    $scope.targetCount = numberWithCommas(target.r_formatNumber(2),share.primaryCurrency == "INR");
                    $scope.pipelineCount = numberWithCommas(pipeline.r_formatNumber(2),share.primaryCurrency == "INR");
                    $scope.wonCount = numberWithCommas(won.r_formatNumber(2),share.primaryCurrency == "INR");
                    $scope.gapCount = numberWithCommas(gap.r_formatNumber(2),share.primaryCurrency == "INR");

                    var max = _.max(allValues);
                    var min = _.min(allValues);

                    $scope.target = {'width':scaleBetween(target,min,max)+'%',background: '#FE9E83'}
                    $scope.pipeline = {'width':scaleBetween(pipeline,min,max)+'%',background: '#767777'}
                    $scope.won = {'width':scaleBetween(won,min,max)+'%',background: '#8ECECB'}
                    $scope.gap = {'width':scaleBetween(gap,min,max)+'%',background: '#e74c3c'}

                    if(won>target){
                        $scope.expectationsExceed = true;
                    }

                    if($scope.deals.length>0){

                        $scope.nextQuarterOppsExist = true;

                        _.each($scope.deals,function (deal) {
                            deal.amountWithCommas = numberWithCommas(parseFloat(deal.amount.r_formatNumber(2)),share.primaryCurrency == "INR")
                            deal.formattedCloseDate = moment(deal.closeDate).format("DD MMM YYYY")
                            deal.company = fetchCompanyFromEmail(deal.contactEmailId)
                            deal.isStale = false
                            deal.suggestion = "Suggest moving this opportunity closing next quarter to current quarter."
                        })
                    }

                    if(response.Data.staleOpps && response.Data.staleOpps.length>0){

                        $scope.staleOppsExist = true;

                        _.each(response.Data.staleOpps,function (deal) {
                            deal.amountWithCommas = numberWithCommas(parseFloat(deal.amount).r_formatNumber(2),share.primaryCurrency == "INR")
                            deal.formattedCloseDate = moment(deal.closeDate).format("DD MMM YYYY")
                            deal.company = fetchCompanyFromEmail(deal.contactEmailId)
                            deal.isStale = true
                            deal.suggestion = "This is a stale opportunity. \n Move this deal to current quarter to meet your target or close the opportunity."

                            $scope.deals.push(deal)
                        })
                    }

                    if(response.Data.currentTargets && response.Data.currentTargets[0] && response.Data.currentTargets[0].target || accessControl){

                        $scope.actionRequired = true;
                        if(response.Data.expectedPipeline>response.Data.currentTargets[0].target || accessControl){
                            $scope.actionRequired = true;
                        }
                    }

                    if(!target && !pipeline){
                        $scope.targetPipelineNone = true;
                    }

                    if(pipeline>target){
                        $scope.targetPipelineNone = false;
                    }

                    if(won>target){
                        $scope.actionRequired = false;
                    }

                    if(target) {

                        if(!won){
                            $scope.actionRequired = true;
                            if(pipeline>=target){
                                $scope.actionRequired = false;
                            }

                        } else {
                            gap = target-won;
                            var wonPercentage = (won/target)*100;
                            var targetPipelineGap = pipeline-won;

                            //targetPipelineGap is the remaining pipeline after achievement, which still can be won
                            //Gap is the minimum won amount required to meet quarter target.

                            if(targetPipelineGap>gap && wonPercentage>=100){
                                $scope.actionRequired = false;
                                $scope.expectationsExceed = true;
                            }

                            if(target>pipeline && gap>0){
                                $scope.actionRequired = true;
                            }
                        }
                    }

                    _.each($scope.deals,function (deal) {
                        deal.stageStyle2 = oppStageStyle(deal.stageName,opportunityStages[deal.stageName]-1,true);
                    })


                } else {
                    $scope.deals = []
                    $scope.target = {}
                    $scope.pipeline = {}
                    $scope.won = {}
                    $scope.gap = {}

                    $scope.targetCount = 0;
                    $scope.pipelineCount = 0;
                    $scope.wonCount = 0;
                    $scope.gapCount = 0;
                }
            });
    }

});

relatasApp.controller("deals_at_risk",function ($scope,$http,share,searchService,$rootScope) {

    function getDealsAtRisk(userId,accessControl) {

        var url = "/insights/deals/at/risk"
        if(userId){
            url = url+"?userIds="+userId;
        }

        if(accessControl && !$rootScope.orgHead){
            url = fetchUrlWithParameter(url+"&accessControl="+true)
        }

        $scope.showDealsAtRisk = true;
        $scope.loadingDealsAtRisk = true;

        $http.get(url)
            .success(function (response) {
                if(response && response.deals){
                    dealsAtRiskGraph($scope,share,response.deals,response.averageRisk)
                }
            });
    }

    share.forDealsAtRisk = function (userId,accessControl) {
        getDealsAtRisk(userId,accessControl)
    }

    share.setTeamMembers = function (usersDictionary,usersArray) {

        var ids = _.map(usersArray,"_id");
        var url = "/insights/deals/at/risk/team/meta";
        url = fetchUrlWithParameter(url,'userIds',ids)

        $http.get(url)
            .success(function (response) {
                var team_atRisk = 0;
                $scope.teamDealsAtRisk = response;
                if(response && response.length>0) {
                    _.each(response,function (el) {
                        team_atRisk = team_atRisk+el.count;
                    })
                }
                share.teamRiskData(team_atRisk)
            });
    };

    share.refreshDealsAtRisk = function () {
        getDealsAtRisk();
    }

    $scope.closeDealsAtRisk = function () {
        $scope.showDealsAtRisk = false;
    }

    $scope.goToContact = function(emailId){
        window.location = '/contacts/all?contact='+emailId+'&acc=true'
    };

    $scope.sortType = 'closeDate';
    $scope.sortReverse = false;

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.oppUpdateStage = function (stage) {

        updateOpportunity($scope,$http,$scope.opp,"stageName",stage,false,function (result) {
            toastr.success("Stage updated successfully");
            $scope.isStagnant = "fa-check-circle-o";
            $scope.stagnantStatus = true;
            $scope.stagnantDaysAgo = "Stage was last updated today";
            share.refreshDealsAtRisk();
            share.refreshPipelineSnaphot();
        });
    }

    $scope.getSuggestions = function (deal) {

        if(!$rootScope.noAccess){

            $scope.openDMs(); // Default open DMs

            $scope.dmActiveClass = "active"
            $scope.iActiveClass = "inactive"
            $scope.stagActiveClass = "inactive"
            $scope.staleActiveClass = "inactive"
            $scope.ciActiveClass = "inactive"
            $scope.mActiveClass = "inactive"
            $scope.compIActiveClass = "inactive"
            $scope.ltActiveClass = "inactive"

            $scope.totalValue = $scope.totalPipeLineValue?numberWithCommas($scope.totalPipeLineValue.r_formatNumber(2),share.primaryCurrency == "INR"):"";

            var percentageAtRisk = calculatePercentage(deal.amount,$scope.totalPipeLineValue);

            if(percentageAtRisk == 0){
                $scope.percentageOfTotal = "< 1% of "
            } else {
                $scope.percentageOfTotal = percentageAtRisk+"% of "
            }

            if(!$scope.totalPipeLineValue || !deal.amount){
                $scope.percentageOfTotal = false;
            }

            $scope.dmExist = "fa-exclamation-circle";
            $scope.InfExist = "fa-exclamation-circle";
            $scope.isStaleOpp = "fa-exclamation-circle";
            $scope.isStagnant = "fa-exclamation-circle";
            $scope.metDmInfl = "fa-exclamation-circle";
            $scope.IntScr = "fa-exclamation-circle";
            $scope.ltWithOwner = "fa-exclamation-circle";
            $scope.contatIntr = "fa-exclamation-circle";

            if(!deal.ltWithOwner){
                $scope.ltWithOwner = "fa-check-circle-o"
                $scope.ltTrue = false;
            } else {
                $scope.ltTrue = true;
            }

            if(!deal.skewedTwoWayInteractions){
                $scope.contatIntr = "fa-check-circle-o"
            }

            if(deal.daysSinceStageUpdated<45){
                $scope.isStagnant = "fa-check-circle-o"
                $scope.stagnantStatus = true;
            } else {
                $scope.stagnantStatus = false;
            }

            $scope.stagnantDaysAgo = "Stage was last updated "+deal.daysSinceStageUpdated + " days back";

            if(deal.daysSinceStageUpdated == 0){
                $scope.stagnantDaysAgo = "Stage was last updated today";
            }

            $scope.metDecisionMaker_infuencer = false;
            if(deal.metDecisionMaker_infuencer){
                $scope.metDecisionMaker_infuencer = true;
                $scope.metDmInfl = "fa-check-circle-o"
            }

            $scope.companyIntr = false;

            if(deal.averageInteractionsPerDeal){
                $scope.companyIntr = true;
                $scope.IntScr = "fa-check-circle-o"
            }

            if(share.usersDictionary[deal.userEmailId]){
                deal.owner = share.usersDictionary[deal.userEmailId]
            }

            $scope.opportunityName = deal.opportunityName;
            $scope.contactEmailId = deal.contactEmailId;

            $scope.closeDate = moment(deal.closeDate).format("DD MMM YYYY");
            $scope.oppCreatedDate = deal.createdDate?moment(deal.createdDate).format("DD MMM YYYY"):'';

            $scope.opp = deal;

            $scope.showModal = true;
            $scope.noDMs = !deal.decisionMakersExist;
            $scope.noInfl = !deal.influencersExist;

            if(deal.decisionMakersExist){
                $scope.dmExist = "fa-check-circle-o"
            }

            if(deal.influencersExist){
                $scope.InfExist = "fa-check-circle-o"
            }

            if(new Date(deal.closeDate)< new Date()){
                $scope.staleOpp = true;
            } else {
                $scope.staleOpp = false
                $scope.isStaleOpp = "fa-check-circle-o"
            }

            $scope.stageUpdated = deal.stageName;

            getLiu();
        }

    }

    $scope.openDMs = function () {
        $scope.showStagOpp = false;
        $scope.showDms = true;
        $scope.showContInt = false;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "active"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"

    }

    $scope.openInfl = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showInfl = true;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "active"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $scope.openStaleOpp = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showStaleOpp = true;

        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "active"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $scope.openDmOrInfMet = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showMetDmInf = true;

        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "active"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"

        if($scope.opp.dmsInfls[0]){
            getLiu($scope.opp.dmsInfls[0]);
        }
    }

    $scope.openIntScore = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showIntScr = true;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "active"
        $scope.ltActiveClass = "inactive"

    }

    $scope.openLt = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showIntScr = false;
        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.closeThis = false;
        $scope.showLt = true;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "active"

    }

    $scope.openStagOpp = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = true;
        $scope.showStaleOpp = false;
        $scope.closeThis = false;
        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "active"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    };

    $scope.openContactIntr = function () {
        $scope.showContInt = true;
        $scope.showStagOpp = false;
        $scope.showStaleOpp = false;
        $scope.closeThis = false;
        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "active"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $(document).on('keyup',function(evt) {
        if (evt.keyCode == 27) {
            $scope.$apply(function (){
                $scope.showModal = false;
                resetSuggestions($scope)
            })
        }
    });

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.removeRecipient = function(contact,type){
        removeRecipient($scope,$http,contact,type)
    }

    $scope.closeModal = function () {
        $scope.showModal = false;
    }

    $scope.oppCloseLost = function () {
        updateOpportunity($scope,$http,$scope.opp,"closeDate",new Date(),true,function (result) {
            if(result){
                share.refreshClosingSoonDeals()
                share.refreshDealsAtRisk()
                share.refreshPipelineSnaphot()
                share.refreshTarget()
                $scope.closeModal();

                toastr.success("Opportunity closed.")
            } else {
                toastr.error("An error occurred while closing this opportunity. Please try later")
            }
        })
    }

    $scope.registerDatePickerId = function(){

        $('#opportunityCloseDateSelector2').datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    updateOpportunity($scope,$http,$scope.opp,"closeDate",dp,false,function (result) {
                        if(result){
                            share.refreshDealsAtRisk();
                            $scope.closeDate = moment(dp).format("DD MMM YYYY");
                            $scope.isStaleOpp = "fa-check-circle-o";
                            $scope.staleOpp = false;
                        } else {

                        }
                    })
                });
            }
        });
    }

    function getLiu(emailId) {

        if(share.liuData){

            var signature = "\n\n"+getSignature(share.liuData.firstName + ' '+ share.liuData.lastName,
                share.liuData.designation,
                share.liuData.companyName,
                share.liuData.publicProfileUrl)

            var interactionsFor = emailId?emailId:$scope.contactEmailId;

            getLastInteractedDetailsMessage($scope,$http,interactionsFor,share.liuData.firstName,share.liuData.publicProfileUrl,function (message) {

                $scope.subject = message.subject;
                $scope.body = message.body+signature;

                $scope.subjectLt = message.subject;
                $scope.subjectCi = message.subject;
                $scope.bodyLt = "\n\n"+signature;

            });

        } else {
            setTimeOutCallback(100,function () {
                getLiu()
            });
        }
    }

    $scope.sendEmail = function (subject,body,reason,contactEmailId) {

        var contactDetails = {
            contactEmailId: contactEmailId?contactEmailId:$scope.opp.contactEmailId,
            personId:null,
            personName:null
        }

        sendEmail($scope,$http,subject,body,contactDetails,reason)
    }

});

relatasApp.controller("acc_growth",function ($scope,$http,share) {
    $scope.loadingMetaData = true;
    share.forAccsCreated = function (userId,accessControl) {
        getAccGrowth(userId,accessControl)
    }

    function getAccGrowth(userId,accessControl){
        var url = "/insights/accounts/interaction/growth"
        if(userId){
            url = fetchUrlWithParameter(url+"?userIds="+userId)
        }

        if(accessControl){
            url = fetchUrlWithParameter(url+"&accessControl="+true) // Access control unnecessary here.
        }

        setTimeOutCallback(1500, function () {

            $http.get(url)
                .success(function (response) {
                    $scope.loadingMetaData = false;
                    if (response && response.SuccessCode && response.Data.length>0) {

                        var label = [], series = [];

                        response.Data.sort(function (o1, o2) {
                            return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
                        });

                        _.each(response.Data, function (el) {
                            var month = monthNames[moment(new Date(el.sortDate)).month()];
                            var monthYear = monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year();
                            label.push(month.substring(0, 3));
                            series.push({meta:monthYear,value:el.accountDetails.length});
                        });

                        $scope.noAcc = _.sumBy(series,"value") === 0;
                        drawLineChart($scope, share, series, label, ".acc-growth")
                    }
                })
        });
    }

    getAccGrowth();
});

relatasApp.controller("opp_growth",function ($scope,$http,share) {
    $scope.loadingMetaData = true;
    share.forOppGrowth = function (userId,accessControl) {
        getOppGrowth(userId,accessControl)
    }

    function getOppGrowth(userId,accessControl) {
        setTimeOutCallback(500, function () {
            var url = "/insights/opp/growth"
            if(userId){
                url = fetchUrlWithParameter(url+"?userIds="+userId)
            }

            if(accessControl){
                url = fetchUrlWithParameter(url+"&accessControl="+true); // Access control unnecessary here.
            }

            $http.get(url)
                .success(function (response) {
                    if (response && response.SuccessCode) {

                        $scope.noOpp = true;
                        var label = [], seriesOpen = [], seriesClose = [];

                        _.each(response.Data, function (el) {
                            if(el.count>0){
                                $scope.noOpp = false;
                            }
                            label.push(monthNames[moment(new Date(el.sortDate)).month()].substring(0,3))
                            seriesOpen.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
                        });

                        _.each(response.dataClosed, function (el) {
                            seriesClose.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
                        });

                        $scope.label2 = "Created";
                        $scope.label1 = "Won";
                        $scope.loadingMetaData = false;
                        drawLineChart($scope,share,seriesOpen,label,".opp-growth",seriesClose);
                        share.newOppsCreated(response.Data)

                    } else {
                        $scope.noOpp = true;
                    }
                })
        });
    }

    getOppGrowth();

});

relatasApp.controller("opportunitiesByStage",function ($scope,$http,$rootScope,share) {

    share.forSnapshot = function (userId,accessControl) {
        getPipelineSnapshot(userId,accessControl)
    }

    $scope.loadingMetaData = true;

    function getPipelineSnapshot(userId,accessControl) {

        var url = "/opportunities/group/count"
        if(userId){
            url = url+"?userIds="+userId;
        }

        if(accessControl){
            url = fetchUrlWithParameter(url+"&accessControl="+true)
        }

        $http.get(url)
            .success(function (response) {

                var oppExist = false;
                if(response && response.SuccessCode && response.Data.length>0){
                    if(response.fy){
                        $scope.fiscalYear = moment(response.fy.fromDate).format("MMM YY")+" - "+moment(response.fy.toDate).format("MMM YY")
                    }

                    oppExist = true;

                    var maxCount = _.max(_.map(response.Data,"count"))
                    var minCount = _.min(_.map(response.Data,"count"))
                    var maxAmount = _.max(_.map(response.Data,"totalAmount"))
                    var minAmount = _.min(_.map(response.Data,"totalAmount"))

                    $scope.prospectColorLeft = "white";
                    $scope.EvaluationColorLeft = "white";
                    $scope.proposalColorLeft = "white";
                    $scope.wonColorLeft = "white";
                    $scope.lostColorLeft = "white";

                    $scope.prospectColor = "white";
                    $scope.EvaluationColor = "white";
                    $scope.proposalColor = "white";
                    $scope.wonColor = "white";
                    $scope.lostColor = "white";

                    $scope.funnels = [];
                    var stagesWithData = [];

                    function getPipelineFunnel(){

                        if(share.opportunityStages && share.opportunityStages.length>0){
                            $rootScope.stages = _.map(share.opportunityStages,"name")
                            _.each(response.Data,function (el) {

                                _.each($rootScope.stages,function (st) {

                                    if(el._id == st){
                                        stagesWithData.push(st);
                                        $scope.funnels.push({
                                            name:st,
                                            countLength:{'width':scaleBetween(el.count, minCount, maxCount)+'%'},
                                            amountLength:{'width':scaleBetween(el.totalAmount, minAmount, maxAmount)+'%'},
                                            amount:el.totalAmount.r_formatNumber(2),
                                            oppsCount:el.count
                                        })
                                    }
                                })
                            });

                            var noDataStages = _.difference($rootScope.stages,stagesWithData)

                            _.each(noDataStages,function (st) {
                                $scope.funnels.push({
                                    name:st,
                                    countLength:{'width':0+'%'},
                                    amountLength:{'width':0+'%'},
                                    amount:0,
                                    oppsCount:0
                                })
                            })

                            _.each($scope.funnels,function (fl) {
                                _.each(share.opportunityStages,function (st) {
                                    if(fl.name == st.name){
                                        fl.order = st.order
                                    }
                                })
                            });

                            $scope.funnels = _.sortBy($scope.funnels,function (o) {
                                return o.order
                            })

                            $scope.loadingMetaData = false;

                        } else {
                            setTimeOutCallback(1000,function () {
                                getPipelineFunnel()
                            });
                        }
                    }

                    getPipelineFunnel()
                }

                $scope.noPipeline = oppExist;
                // share.oppExist(oppExist)
            });
    }

    getPipelineSnapshot()

});

relatasApp.controller("opp_prop_viz",function ($scope,$http,$rootScope,share) {

    $scope.loadingMetaData = true;

    share.opp_props = function(oppTypes,sourceTypes,products) {
        var oppType = groupAndChainForTeamSummary(oppTypes)
        donutChart(oppType,".typeGraph",shadeGenerator(63,81,181,oppType.length,15),185,185,185*.10);

        var sourceType = groupAndChainForTeamSummary(sourceTypes)
        donutChart(sourceType,".sourceTypeGraph",shadeGenerator(205,220,57,sourceType.length,15),185,185,185*.10);
        $scope.loadingMetaData = false;
        pieChart(_.flattenDeep(products),$scope);
    }
});

relatasApp.directive('excelExport', function () {
    return {
        restrict: 'A',
        scope: {
            fileName: "@",
            data: "&exportData"
        },
        replace: true,
        template: '<button class="btn btn-white margin0" ng-click="download()">Download <i class="fa fa-download"></i></button>',
        link: function (scope, element) {

            scope.download = function() {

                function datenum(v, date1904) {
                    if(date1904) v+=1462;
                    var epoch = Date.parse(v);
                    return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                };

                function getSheet(data, opts) {
                    var ws = {};
                    var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
                    for(var R = 0; R != data.length; ++R) {
                        for(var C = 0; C != data[R].length; ++C) {
                            if(range.s.r > R) range.s.r = R;
                            if(range.s.c > C) range.s.c = C;
                            if(range.e.r < R) range.e.r = R;
                            if(range.e.c < C) range.e.c = C;
                            var cell = {v: data[R][C] };
                            if(cell.v == null) continue;
                            var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

                            if(typeof cell.v === 'number') cell.t = 'n';
                            else if(typeof cell.v === 'boolean') cell.t = 'b';
                            else if(cell.v instanceof Date) {
                                cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                                cell.v = datenum(cell.v);
                            }
                            else cell.t = 's';

                            ws[cell_ref] = cell;
                        }
                    }
                    if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                    return ws;
                };

                function Workbook() {
                    if(!(this instanceof Workbook)) return new Workbook();
                    this.SheetNames = [];
                    this.Sheets = {};
                }

                var wb = new Workbook(), ws = getSheet(scope.data());
                /* add worksheet to workbook */
                wb.SheetNames.push(scope.fileName);
                wb.Sheets[scope.fileName] = ws;
                var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

                function s2ab(s) {
                    var buf = new ArrayBuffer(s.length);
                    var view = new Uint8Array(buf);
                    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                    return buf;
                }

                saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), scope.fileName+'.xlsx');

            };

        }
    };
});

function getTypeFormat(type){

    var typeFormat = "Account"
    if(type == "geoLocations"){
        typeFormat = "Region"
    }

    if(type == "businessUnits"){
        typeFormat = "BU"
    }
    if(type == "productList"){
        typeFormat = "Product"
    }

    if(type == "solutionList"){
        typeFormat = "Solution"
    }
    if(type == "sourceList"){
        typeFormat = "Source"
    }

    if(type == "typeList"){
        typeFormat = "Type"
    }

    if(type == "verticalList"){
        typeFormat = "Vertical"
    }
    if(type == "opportunityStages"){
        typeFormat = "Stage"
    }

    return typeFormat;
}

function currentInsights($scope,$http,share,$rootScope,emailId) {

    var url = "/reports/current/insights"

    if(emailId) {
        url = fetchUrlWithParameter(url, "forUserEmailId",emailId)
    } else if(!emailId && share.selection && share.selection.fullName !== "Show all team members") {
        url = fetchUrlWithParameter(url, "forUserEmailId",share.liuData.emailId)
    }

    $http.get(url)
        .success(function (response) {
            $scope.currentInsights = response.Data;

            if(response.Data.staleOpps && response.Data.staleOpps[0]){
                $scope.staleCount = response.Data.staleOpps[0].count
            }

            $scope.todayDate = moment().format(standardDateFormat());

            for(var key in $scope.currentInsights){
                if(typeof $scope.currentInsights[key] == "number"){
                    $scope.currentInsights[key+"Commas"] = getAmountInThousands($scope.currentInsights[key],2,share.primaryCurrency=="INR");
                }
            }

            $scope.currentInsights.avgDaysToCloseOppGraph = {width:'0%'};
            $scope.currentInsights.avgDealSizeGraph = {width:'0%'};
            $scope.currentInsights.avgInteractionsPerAmountWonGraph = {width:'0%'};

            if($scope.currentInsights.minDaysToCloseOpp && $scope.currentInsights.minDaysToCloseOpp == $scope.currentInsights.maxDaysToCloseOpp){
                $scope.currentInsights.avgDaysToCloseOppGraph = {width:100+'%'};
            } else if($scope.currentInsights.minDaysToCloseOpp && $scope.currentInsights.maxDaysToCloseOpp){
                $scope.currentInsights.avgDaysToCloseOppGraph = {width:scaleBetween($scope.currentInsights.avgDaysToCloseOpp,$scope.currentInsights.minDaysToCloseOpp,$scope.currentInsights.maxDaysToCloseOpp)+'%'};
            }

            if($scope.currentInsights.minDealSize && $scope.currentInsights.minDealSize == $scope.currentInsights.maxDealSize){
                $scope.currentInsights.avgDealSizeGraph = {width:scaleBetween($scope.currentInsights.avgDealSize,1,$scope.currentInsights.maxDealSize)+'%'};
            } else if($scope.currentInsights.minDealSize && $scope.currentInsights.maxDealSize){
                $scope.currentInsights.avgDealSizeGraph = {width:scaleBetween($scope.currentInsights.avgDealSize,$scope.currentInsights.minDealSize,$scope.currentInsights.maxDealSize)+'%'};
            }

            if($scope.currentInsights.maxDaysToCloseOpp && $scope.currentInsights.maxDaysToCloseOpp){
                $scope.currentInsights.avgInteractionsPerAmountWonGraph = {width:scaleBetween($scope.currentInsights.avgInteractionsPerAmountWon,$scope.currentInsights.minIntPerThousandAmountWon,$scope.currentInsights.maxIntPerThousandAmountWon)+'%'};
            }
            $scope.loadingMetaData = false;

        });
}

function getOpps($scope,$http,share,$rootScope,emailId,filters,dontUpdateMetaData){

    var filterObj = {};
    var url = "/reports/get/opportunities"

    if(filters && filters.length>0){
        filterObj.filters = filters
    } else if(emailId && share.usersDictionary[emailId] && share.usersDictionary[emailId].userId) {
        filterObj.forUserEmailId = emailId
        filterObj.forUserId = share.usersDictionary[emailId].userId;
    } else if(!emailId && share.selection && share.selection.fullName !== "Show all team members") {
        filterObj.forUserEmailId = share.liuData.emailId
        filterObj.forUserId = share.liuData._id;
    }

    if(share.selection && share.selection.fullName === "Show all team members"){
        filterObj.allUserEmailId = [];
        filterObj.allUserId = [];

        _.each(share.team,function (tm) {
            filterObj.allUserEmailId.push(tm.emailId);
            filterObj.allUserId.push(tm.userId);
        });
    }

    $http.post(url,filterObj)
        .success(function (response) {
            // $scope.loadingData = false;
            function check_liu_loaded() {
                if(share.usersDictionary){
                    $scope.loadingMetaData = false;
                    if(response && response.opps){
                        $rootScope.oppsExists = response.opps.length>0;
                        drawAllDataGraphs($scope,$http,$rootScope,share,response,dontUpdateMetaData,function (createdThisMonthOpps) {

                            if(response.metaData[0] && response.metaData[1]){

                                response.metaData.sort(function (o1,o2) {
                                    return o1.month - o2.month;
                                });

                                drawSankeyGraph(response,share,$scope);

                                if(response.metaData[1]){
                                    formatSalesByAccount(response.metaData[1]);
                                }
                            }
                        });
                    }
                } else {
                    setTimeOutCallback(200,function () {
                        check_liu_loaded();
                    })
                }
            }

            check_liu_loaded();
        })
}

function formatSalesByAccount(data) {
    var accounts = [];

    _.each(data.data,function (el) {
        if(el.stageName === "Close Won"){

            if(el.accounts && el.accounts.length>0){
                accounts.push(el.reasons);
            } else {
                accounts.push(el.reasons);
            }
        }
    });

    accounts = _.flattenDeep(accounts)
}

function drawAllDataGraphs($scope,$http,$rootScope,share,response,dontUpdateMetaData,callback){

    var opps = response.opps

    var thisQuarterOpps = [],
        createdThisMonthOpps = [],
        thisQuarterOppsObj = {},
        monthStartDate = moment().startOf("month");

    var wonAmt = 0,
        wonCount = 0,
        lostCount = 0,
        dealsAtRiskCount = 0,
        totalDealValueAtRisk = 0,
        pipelineCount = 0,
        renewalCount = 0,
        staleCount = 0,
        staleAmt = 0,
        closing30DaysCount = 0,
        dealsRiskAsOfDate = null,
        lostAmt = 0,
        closing30DaysAmt = 0,
        pipelineAmt = 0,
        renewalAmt = 0,
        targetAmt = 0,
        wonReasons = [],
        accounts = [],
        products = [],
        accountsAll = [],
        productsAll = [],
        filterLists = [],
        locations = [],
        contacts = [],
        owners = [],
        lostReasons = [],
        sourceTypes = [],
        oppTypes = [];

    if(response.targets && response.targets.length>0){
        targetAmt = _.sumBy(response.targets,"target")
    }

    var renewalTypes = [];

    _.each(share.companyDetails.typeList,function (tl) {
        if(tl.isTypeRenewal){
            renewalTypes.push(tl.name)
        }
    })

    var date30End = moment().add(30,"days");
    share.oppsReadOnly = [];

    _.each(opps,function (op) {

        op.stageColor = "";

        if(op.stageName == "Close Won") {
            op.stageColor = "won"
        }

        if(op.stageName == "Close Lost") {
            op.stageColor = "lost"
        }

        share.oppsReadOnly.push(op);
        var acc = fetchCompanyFromEmail(op.contactEmailId);

        op.amount = parseFloat(op.amount);
        op.amountWithNgm = op.amount;

        if(op.netGrossMargin || op.netGrossMargin == 0){
            op.amountWithNgm = (op.amount*op.netGrossMargin)/100
        }

        op.convertedAmt = op.amount;
        op.convertedAmtWithNgm = op.amountWithNgm

        if(op.currency && op.currency !== share.primaryCurrency){

            if(share.currenciesObj[op.currency] && share.currenciesObj[op.currency].xr){
                op.convertedAmt = op.amount/share.currenciesObj[op.currency].xr
            }

            if(op.netGrossMargin || op.netGrossMargin == 0){
                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
            }

            op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

        }

        var monthYear =  moment(op.closeDate).format("MMM YYYY");
        op.monthYear = monthYear;
        if(!thisQuarterOppsObj[monthYear]){
            thisQuarterOppsObj[monthYear] = [];
        }

        thisQuarterOppsObj[monthYear].push(op)

        thisQuarterOpps.push(op)

        if(_.includes(["Close Won"], op.stageName)){
            wonAmt = wonAmt+op.convertedAmtWithNgm
            wonCount++;
            if(op.closeReasons && op.closeReasons.length>0){
                _.each(op.closeReasons,function (cr) {
                    var wonObj = {
                        name:cr?cr:"Others",
                        amount:op.convertedAmtWithNgm
                    }
                    wonReasons.push(wonObj)
                })
            }

            if(op.geoLocation && op.geoLocation.town){
                locations.push(op.geoLocation.town)
            }

            var obj = {}
            obj[op.productType?op.productType:"Others"] = op.convertedAmtWithNgm;
            var accObj = {}
            accObj[acc] = op.convertedAmtWithNgm
            accounts.push(accObj)
            products.push(obj);

            oppTypes.push({
                name:op.type,
                amount:op.convertedAmtWithNgm
            })

            sourceTypes.push({
                name:op.sourceType,
                amount:op.convertedAmtWithNgm
            })

        } else if(_.includes(["Close Lost"], op.stageName)){
            lostAmt = lostAmt+op.convertedAmtWithNgm
            lostCount++

            if(op.closeReasons && op.closeReasons.length>0){
                _.each(op.closeReasons,function (cr) {
                    var lostObj = {
                        name:cr?cr:"Others",
                        amount:op.convertedAmtWithNgm
                    }
                    lostReasons.push(lostObj)
                })
            }
        } else {

            if(new Date(op.closeDate)>= new Date() && new Date(op.closeDate)<= new Date(date30End)){
                closing30DaysCount++;
                closing30DaysAmt = closing30DaysAmt+op.convertedAmtWithNgm;
            }

            pipelineAmt = pipelineAmt+op.convertedAmtWithNgm
            pipelineCount++
        }

        if(op.relatasStage !== "Close Lost" && op.relatasStage !== "Close Won"){
            if(new Date(op.closeDate)< new Date(moment().startOf("day"))){
                staleCount++;
                staleAmt = staleAmt+op.convertedAmtWithNgm
            }
        }

        if(!op.currency){
            op.currency = share.primaryCurrency
        }

        if(_.includes(renewalTypes,op.type)){
            renewalAmt = renewalAmt+op.amountWithNgm
            renewalCount++;
        }

        op.amountFormatted = op.amount.r_formatNumber(2)
        op.amountWithNgm = parseFloat(op.amountWithNgm.r_formatNumber(2))
        op.convertedAmtWithNgm = parseFloat(op.convertedAmtWithNgm.r_formatNumber(2))

        op.account = acc?acc:"Others";

        op.closeDateFormatted = moment(op.closeDate).format(standardDateFormat());
        contacts.push({
            name:op.contactEmailId,
            displayName:op.contactEmailId
        });

        owners.push({
            name:op.userEmailId,
            displayName:op.userEmailId
        })

        if(acc && acc != "Others"){
            accountsAll.push({
                name:acc?acc:"Others",
                displayName:acc?acc:"Others"
            })
        }

        op.owner = share.usersDictionary[op.userEmailId]?share.usersDictionary[op.userEmailId]:{fullName:op.userEmailId};

        productsAll.push({
            name:op.productType,
            displayName:op.productType
        });

        if(new Date(op.createdDate)>= new Date(monthStartDate) && new Date(op.createdDate)<= new Date()){
            createdThisMonthOpps.push(op)
        }

    });

    pipelineVelocity($scope,share,thisQuarterOpps,response.targets,thisQuarterOppsObj)

    if(response.dealsAtRisk && response.dealsAtRisk[0]){
        if(response.dealsAtRisk[0].count){
            dealsAtRiskCount = response.dealsAtRisk[0].count
        }

        if(response.dealsAtRisk[0].totalDealValueAtRisk){
            totalDealValueAtRisk = response.dealsAtRisk[0].totalDealValueAtRisk
        }

        if(response.dealsAtRisk[0].date){
            dealsRiskAsOfDate = response.dealsAtRisk[0].date
        }
    }

    if(renewalAmt){
        renewalAmt = parseFloat(renewalAmt.toFixed(2))
    }

    if(wonAmt){
        wonAmt = parseFloat(wonAmt.toFixed(2))
    }

    if(lostAmt){
        lostAmt = parseFloat(lostAmt.toFixed(2))
    }

    if(pipelineAmt){
        pipelineAmt = parseFloat(pipelineAmt.toFixed(2))
    }

    $rootScope.dealsAtRiskCount = dealsAtRiskCount;
    $rootScope.totalDealValueAtRisk = totalDealValueAtRisk;

    share.drawOppsTable(opps,contacts,owners,productsAll,accountsAll)
    if(!dontUpdateMetaData){
        drawPipeline($scope,wonAmt,lostAmt,wonCount,lostCount,pipelineAmt,pipelineCount,closing30DaysAmt,closing30DaysCount,dealsAtRiskCount,totalDealValueAtRisk,dealsRiskAsOfDate,renewalAmt,renewalCount,staleAmt,staleCount,share,targetAmt);
        share.drawOppTabPipeline(wonAmt,lostAmt,wonCount,lostCount,pipelineAmt,pipelineCount,closing30DaysAmt,closing30DaysCount,dealsAtRiskCount,totalDealValueAtRisk,dealsRiskAsOfDate,renewalAmt,renewalCount,staleAmt,staleCount)
    }

    var reasons = {
        won:groupAndChainForTeamSummary(wonReasons),
        lost:groupAndChainForTeamSummary(lostReasons)
    };

    $scope.noWon = reasons.won.length === 0;
    $scope.noLost = reasons.lost.length === 0;

    donutChart(reasons.won,".donutGraphWon",shadeGenerator(0,150,136,reasons.won.length,15),60,60)
    donutChart(reasons.lost,".donutGraphLost",shadeGenerator(244,67,54,reasons.lost.length,15),60,60);

    $(".accChart").empty();
    treemapChart(_.flattenDeep(accounts),$scope,share);

    // setTimeOutCallback(1000,function () {
    //     googleMaps($scope,locations)
    // });

    if(callback){
        callback(createdThisMonthOpps)
    }

    share.opp_props(oppTypes,sourceTypes,products)

}

function pipelineVelocity($scope,share,thisQuarterOpps,targets,thisQuarterOppsObj){
    $scope.targetGraph = targets
    var currentMonthYear = moment().format("MMM YYYY")

    _.each($scope.targetGraph,function (tr) {
        tr.monthYear = moment(tr.date).format("MMM YYYY")
        var thisMonthOpps = thisQuarterOppsObj[tr.monthYear];

        var won = 0,
            lost = 0,
            pipeline = 0,
            min = 0,
            max = 0;

        _.each(thisMonthOpps,function (op) {
            if(op.relatasStage == "Close Won"){
                won = won+op.convertedAmtWithNgm
            } else if(op.relatasStage == "Close Lost"){
                lost = lost+op.convertedAmtWithNgm
            } else {
                pipeline = pipeline+op.convertedAmtWithNgm
            }
        })

        max = won+lost+pipeline+tr.target;

        tr.won = won;
        tr.lost = lost;
        tr.pipeline = pipeline;

        if(tr.monthYear == currentMonthYear){
            tr.highLightCurrentMonth = true
        }

        tr.heightWon = {'height':scaleBetween(tr.won,min,max)+'%'}
        tr.heightLost = {'height':scaleBetween(tr.lost,min,max)+'%'}
        tr.heightTotal = {'height':scaleBetween(tr.pipeline,min,max)+'%'}
        tr.heightTarget = {'height':scaleBetween(tr.target,min,max)+'%'}

        tr.won = numberWithCommas(tr.won.r_formatNumber(2),share.primaryCurrency == "INR");
        tr.lost = numberWithCommas(tr.lost.r_formatNumber(2),share.primaryCurrency == "INR")
        tr.openValue = numberWithCommas(tr.pipeline.r_formatNumber(2),share.primaryCurrency == "INR")
        tr.target = numberWithCommas(tr.target?tr.target.r_formatNumber(2):0,share.primaryCurrency == "INR")

        tr.month = moment(tr.date).format("MMM")

    })

    $scope.targetGraph.sort(function (o1,o2) {
        return o1.date > o2.date ? 1 : o1.date < o2.date ? -1 : 0;
    })
}

function drawPipeline($scope,wonAmt,lostAmt,wonCount,lostCount,pipelineAmt,pipelineCount,closing30DaysAmt,closing30DaysCount,dealsAtRiskCount,totalDealValueAtRisk,dealsRiskAsOfDate,renewalAmt,renewalCount,staleAmt,staleCount,share,targetAmt) {

    $scope.rangeType = share.rangeType?share.rangeType:"This Quarter"

    var achievementPercentage = 0,
        pipelinePercentage = 0;
    $scope.stageMetaInfo = [];
    // $scope.staleCount = staleCount

    if(targetAmt){
        achievementPercentage = parseFloat(((wonAmt/targetAmt)*100).r_formatNumber(2))
        achievementPercentage = achievementPercentage>100?100:achievementPercentage

        pipelinePercentage = parseFloat(((pipelineAmt/targetAmt)*100).r_formatNumber(2))
        pipelinePercentage = pipelinePercentage>100?100:pipelinePercentage
    } else {
        $scope.noTargetsSet = true;
    }

    $scope.pipelinePercentage = pipelinePercentage;
    $scope.achievementPercentage = achievementPercentage;

    share.currentInsightsData(pipelinePercentage,achievementPercentage)

    $scope.stageMetaInfo = [
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colType:"Won",
            colIcon:"fas fa-trophy",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(wonAmt,2,share.primaryCurrency =="INR"),
            oppCount:wonCount,
            textColor:"green",
            currency:share.primaryCurrency,
            asOfDate:true,
            percentage:achievementPercentage,
            style:{width:achievementPercentage+"%"}
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colType:"Lost",
            colIcon:"fas fa-trophy",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(lostAmt,2,share.primaryCurrency =="INR"),
            oppCount:lostCount,
            textColor:"red",
            currency:share.primaryCurrency,
            asOfDate:true
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colType:"Closing",
            colIcon:"fas fa-chart-bar",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(pipelineAmt,2,share.primaryCurrency =="INR"),
            oppCount:pipelineCount,
            textColor:"blue",
            currency:share.primaryCurrency,
            asOfDate:true,
            percentage:pipelinePercentage,
            style:{width:pipelinePercentage+"%"}
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colType:"Deals At Risk",
            colIcon:"fas fa-dollar-sign",
            rangeType:dealsRiskAsOfDate?"As of "+moment(dealsRiskAsOfDate).format(standardDateFormat()):null,
            amountValue:getAmountInThousands(totalDealValueAtRisk,2,share.primaryCurrency =="INR"),
            oppCount:dealsAtRiskCount,
            textColor:"red",
            currency:share.primaryCurrency,
            asOfDate:dealsRiskAsOfDate?moment(dealsRiskAsOfDate).format(standardDateFormat()):null,
            cursor:"cursor",
            allTeamMembers:share.selection && share.selection.fullName == "Show all team members"
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colType:"Renewal",
            colIcon:"fas fa-dollar-sign",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(renewalAmt,2,share.primaryCurrency =="INR"),
            oppCount:renewalCount,
            textColor:"yellow",
            currency:share.primaryCurrency,
            asOfDate:true
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colType:"Stale",
            colIcon:"fas fa-dollar-sign",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(staleAmt,2,share.primaryCurrency =="INR"),
            oppCount:staleCount,
            textColor:"red",
            currency:share.primaryCurrency,
            asOfDate:true
        }
    ]
}

function drawSankeyGraph(response,share,$scope){

    $("#oppFlow").empty();
    var formattedData = formatDataForSankeyGraph(response,share); // This will be replaced.
    share.quarterRange = {
        qStart:response.qStart,
        qEnd:response.qEnd
    }

    $scope.oppFlowHeader = moment(response.qStart).format("MMM") +"- "+moment(moment(response.qStart).add(1,"month")).format("MMM") +"- "+moment(moment(response.qEnd).subtract(1,"d")).format("MMM YYYY");

    share.totalCommitAmt = 0;
    share.totalPipelineAmt = 0;
    share.totalNewAmt = 0;

    $scope.startAmount = getAmountInThousands(share.startAmount,2,share.primaryCurrency == "INR");
    $scope.endAmount = getAmountInThousands(share.endAmount,2,share.primaryCurrency == "INR");

    var newExists = false,
        commitStartExists = false,
        pipelineStartExists = false,
        commitExists = false,
        pipelineExists = false,
        closeWonExists = false,
        closeLostExists = false;

    _.each(formattedData,function (el) {

        if(el.source == "New"){
            newExists = true;
        }

        if(el.source == "Pipeline"){
            pipelineStartExists = true;
        }

        if(el.source == "Commit"){
            commitStartExists = true;
        }

        if(el.target == "Pipeline "){
            pipelineExists = true;
        }

        if(el.target == "Commit "){
            commitExists = true;
        }

        if(el.target == "Close Won"){
            closeWonExists = true;
        }

        if(el.target == "Close Lost"){
            closeLostExists = true;
        }

        if(el.source == "New"){
            share.totalNewAmt = share.totalNewAmt+el.sourceAmt
        } else if(el.source == "Commit"){
            share.totalCommitAmt = share.totalCommitAmt+el.sourceAmt
        } else {
            share.totalPipelineAmt = share.totalPipelineAmt+el.sourceAmt
        }
    });

    var commitStage = "Commit" //response.commitStage;
    var commitStageEnd = "Commit " //commitStage+" ";

    var colorScheme = {
        "Pipeline":"#cbcfd2",
        "Pipeline ":"#cbcfd2",
        "Close Won": "#8ECECB", //"#47b758",
        "Close Lost":"#e74c3c",
        "New":"#2499c9"
    }

    colorScheme[commitStage] = "#f1c40f";
    colorScheme[commitStageEnd] = "#f1c40f";

    var allNodes = [];

    if(closeLostExists){
        allNodes.push({"name": "Close Lost"})
    }

    if(closeWonExists){
        allNodes.push({"name": "Close Won"})
    }

    if(newExists){
        allNodes.push({"name": "New"})
    }

    if(pipelineStartExists){
        allNodes.push({"name": "Pipeline"})
    }

    if(commitStartExists){
        allNodes.push({"name": commitStage})
    }

    if(pipelineExists){
        allNodes.push({"name": "Pipeline "})
    }

    if(commitExists){
        allNodes.push({"name": commitStageEnd})
    }

    share.sankeyData = {
        "nodes":allNodes,
        "links": formattedData
    };

    var data = {
        "nodes":allNodes,
        "links": formattedData
        // "links": {}
    }

    $scope.noOppFlowData = false;
    if(formattedData && formattedData.length>0){
        sankeyGraphSettings($scope,share,data,colorScheme)

    } else {
        $scope.noOppFlowData = true;
    }

}

function sankeyGraphSettings($scope,share,data,colorScheme) {

    var viewBox = "0 0 515 350";

    if($(window).width() && $(window).width()>1380){
        viewBox = "0 0 675 350";
    }

    if($(window).width() && $(window).width()>1900){
        viewBox = "0 0 855 350";
    }

    var nodeHash = {};
    data.nodes.forEach(function(d){
        nodeHash[d.name] = d;
    });
    // loop links and swap out string for object
    data.links.forEach(function(d){
        d.source = nodeHash[d.source];
        d.target = nodeHash[d.target];
    });

    var margin = {top: 1, right: 1, bottom: 6, left: 1},
        width = 515 - margin.left - margin.right,
        height = 295 - margin.top - margin.bottom;

    var formatNumber = d3.format(",.0f"),
        format = function(d) { return share.primaryCurrency +" "+formatNumber(d); },
        color = d3.scale.category20();

    var svg = d3.select("#oppFlow")
        .append("div")
        .classed("svg-container", true)
        .append("svg")
        .attr("preserveAspectRatio", "xMinYMin meet")
        .attr("viewBox", viewBox)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .classed("svg-content-responsive", true);

    var sankey = d3.sankey()
        .nodeWidth(5)
        .nodePadding(10)
        .size([width, height]);

    var path = sankey.link();

    sankey
        .nodes(data.nodes)
        .links(data.links)
        .layout(32);

    var tooltip = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    var css = '.stop-left { stop-color: #3f51b5; } .stop-right {stop-color: #009688;} .filled {fill: url(#mainGradient);}',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');

    style.type = 'text/css';
    style.appendChild(document.createTextNode(css));
    head.appendChild(style);

    var link = svg.append("g").selectAll(".link")
        .data(data.links)
        .enter().append("path")
        .attr("class", "link")
        .attr("d", path)
        .style("stroke-width", function(d) { return Math.max(1, d.dy); })
        .style("stroke", function(d) {
            // if(d.target.name === "Close Won"){
            //     return "#47b758";
            // }
            //
            // if(d.target.name === "Close Lost"){
            //     return "#e74c3c";
            // }
        })
        .style("stroke-opacity", function(d) {
            if(d.target.name === "Close Won" || d.target.name === "Close Lost"){
                // return 0.8;
            }
        })
        .sort(function(a, b) { return b.dy - a.dy; })
        .on("mouseover", function(d) {
            var html = getToolTip(d,share)
            tooltip.transition()
                .duration(200)
                .style("opacity", .99);
            tooltip	.html(html)
                .style("left", (d3.event.pageX - 50) + "px")
                .style("top", (d3.event.pageY - 55) + "px");
        })
        .on("mouseout", function(d) {
            tooltip.transition()
                .duration(500)
                .style("opacity", 0);
        });

    var node = svg.append("g").selectAll(".node")
        .data(data.nodes)
        .enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
        .call(d3.behavior.drag()
            .origin(function(d) { return d; })
            .on("dragstart", function() { this.parentNode.appendChild(this); })
            .on("drag", dragmove));

    node.append("rect")
        .attr("height", function(d) { return d.dy; })
        .attr("width", sankey.nodeWidth())
        .style("fill", function(d) {
            return d.color = colorScheme[d.name];
        })
        .style("stroke", function(d) {
            return colorScheme[d.name];
        })
        .append("title")
        .text(function(d) {return d.name + "\n" + format(d.value); })
    // .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

    node.append("text")
        .attr("x", -6)
        .attr("y", function(d) { return d.dy / 2; })
        .attr("dy", ".35em")
        .attr("text-anchor", "end")
        .attr("transform", null)
        .text(function(d) { return d.name; }) //Text in the middle
        .filter(function(d) { return d.x < width / 2; })
        .attr("x", 6 + sankey.nodeWidth())
        .attr("text-anchor", "start");

    function dragmove(d) {
        d3.select(this).attr("transform", "translate(" + d.x + "," + (d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))) + ")");
        sankey.relayout();
        link.attr("d", path);
    }
}

function getToolTip(data,share) {

    var movementCount = data.numberOfOpps + " opps";
    var amountDiff = 0;
    var amountDiffPercentage = 0;

    if(data.sourceAmt && data.targetAmt){
        if(data.source.name == "New"){
            amountDiff = data.targetAmt;
            amountDiffPercentage = (amountDiff/share.totalNewAmt)*100;
            amountDiffPercentage = parseFloat(amountDiffPercentage.toFixed(2))+"%";
        } else if(data.source.name == "Pipeline"){
            amountDiff = data.targetAmt;
            amountDiffPercentage = (amountDiff/share.totalPipelineAmt)*100;
            amountDiffPercentage = parseFloat(amountDiffPercentage.toFixed(2))+"%";
        } else {
            amountDiff = data.targetAmt;
            amountDiffPercentage = (amountDiff/share.totalCommitAmt)*100;
            amountDiffPercentage = parseFloat(amountDiffPercentage.toFixed(2))+"%";
        }
    }

    var directionElement = "<i class='grey fas fa-arrow-right'></i>";

    if(data.target.name == "Close Lost"){
        directionElement = "<i class='red fas fa-arrow-right'></i>"
    }

    if(data.target.name == "Close Won"){
        directionElement = "<i class='green fas fa-arrow-right'></i>"
    }

    var wrapperStart = "<div class='tooltip-wrapper'>";
    var wrapperEnd = "</div>";
    var source = "<div class='source'>"+data.source.name+"</div>"
    var target = "<div class='target'>"+data.target.name+"</div>"
    var direction = "<div class='direction'>"+directionElement+"</div>"
    var moreInfoStart = "<div class='more-info'>"
    var moreInfoEnd = "</div>"
    var spanNumber = "<span>"+amountDiffPercentage+" | "+share.primaryCurrency+" "+getAmountInThousands(amountDiff,2,share.primaryCurrency == "INR")+" | "+movementCount+"</span>"+"<span>"+"</span>"
    var moreInfo = moreInfoStart+spanNumber+moreInfoEnd;

    return wrapperStart+source+direction+target+moreInfo+wrapperEnd;
}

function formatDataForSankeyGraph(data,share){

    data.metaData.sort(function (o1,o2) {
        return o1.month - o2.month;
    });

    var startOfMonth = {},
        endOfMonth = {};

    _.each(data.oppStages,function (el) {
        startOfMonth[el] = []
        endOfMonth[el] = []
    });

    var startMonth = [],
        oppsStart = {},
        oppsEnd = {},
        endMonth = [];

    startMonth = data.metaData[0];
    endMonth = data.metaData[1].oppMetaDataFormat;

    share.startAmount = 0;
    share.endAmount = 0;

    var monthStartDate = data.qStart;
    var monthEndDate = data.qEnd;
    var oppCreatedAndClosingThisQuarter= {}

    if(endMonth){
        _.each(endMonth.data,function (st) {
            _.each(st.oppIds,function (el) {
                el.amount = parseFloat(el.amount);
                el.stageName = st.stageName;

                oppsEnd[el.opportunityId] = el;

                if(st.stageName !== "Close Won" && st.stageName !== "Close Lost"){
                    oppCreatedAndClosingThisQuarter[el.opportunityId] = el.closeDate;
                } else if(st.stageName === "Close Won" || st.stageName === "Close Lost"){
                    if(new Date(el.closeDate)>= new Date(monthStartDate) && new Date(el.closeDate)<= new Date(monthEndDate)){
                        oppCreatedAndClosingThisQuarter[el.opportunityId] = el.closeDate;
                    }
                }
            })
        });
    }

    //Opps created in the selection will be added from data.metaData[1].createdThisSelection source. Not from the opp meta data snapshot.
    // This was to reduce computations when large number of users are involved. Until we move to the new meta data on top of the existing
    // oppMetaData, this will be the implementation...

    _.each(startMonth.data,function (st) {
        _.each(st.oppIds,function (el) {

            // if(oppCreatedAndClosingThisQuarter[el.opportunityId]){ // Considering opps closing this month.
            if(oppCreatedAndClosingThisQuarter[el.opportunityId]){ // Considering opps closing this selection and also removing opps created in the month.
                el.amount = parseFloat(el.amount);
                el.stageName = st.stageName;
                oppsStart[el.opportunityId] = el
                share.startAmount = share.startAmount+parseFloat(el.amount)
            }
       })
    });

    if(data.metaData[1].createdThisSelection && data.metaData[1].createdThisSelection.length>0){
        _.each(data.metaData[1].createdThisSelection,function (el) {
            oppsStart[el.opportunityId] = el;
            share.startAmount = share.startAmount+parseFloat(el.amount)
        })
    }

    var ending = [],
        totalCommitAmt = 0,
        totalPipelineAmt = 0,
        srcCommitAmt = 0,
        targetCommitAmt = 0,
        srcPipelineAmt = 0,
        targetPipelineAmt = 0;

    for(var key1 in oppsStart){

        var source = oppsStart[key1];
        var target = oppsEnd[key1];

        if(target && target.stageName){

            var src = source.stageName, // Start
                trgt = target.stageName;

            if(!source.fromSnapShot){
                src = "New"
                srcCommitAmt = srcCommitAmt+source.amount;
                totalCommitAmt = totalCommitAmt+source.amount;
            } else if(src == data.commitStage){
                src = "Commit"
                srcCommitAmt = srcCommitAmt+source.amount;
                totalCommitAmt = totalCommitAmt+source.amount;
            } else {
                src = "Pipeline"
                srcPipelineAmt = srcPipelineAmt+source.amount;
                totalPipelineAmt = totalPipelineAmt+source.amount;
            }

            if(trgt == data.commitStage){
                trgt = "Commit "
                targetCommitAmt = targetCommitAmt+target.amount;
                totalCommitAmt = totalCommitAmt+target.amount;
            } else if(trgt !== "Close Won" && trgt !== "Close Lost") {
                targetPipelineAmt = targetPipelineAmt+target.amount;
                totalPipelineAmt = totalPipelineAmt+target.amount;
                trgt = "Pipeline "
            }

            share.endAmount = share.endAmount+parseFloat(source.amount)

            ending.push({
                source:src,
                target:trgt,
                value:1,
                sourceAmt:source.amount,
                targetAmt:target.amount
            });
        }
    }

    share.targetPipelineAmt = targetPipelineAmt;
    share.srcCommitAmt = srcCommitAmt;
    share.targetCommitAmt = targetCommitAmt;
    share.srcPipelineAmt = srcPipelineAmt;

    var group = _
        .chain(ending)
        .groupBy('source')
        .map(function(value, key) {
            var obj = {};
            var endings = groupByTargets(key,value,share);
            obj[key] = key;
            obj.value = endings
            return obj;
        })
        .value();

    return _.flatten(_.map(group,"value"));

}

function groupByTargets(pKey,data,share){

    var group = _
        .chain(data)
        .groupBy('target')
        .map(function(value, key) {
            var obj = {};

            var targetAmt = 0,
                sourceAmt = 0;

            _.each(value,function (va) {
                targetAmt = targetAmt+va.targetAmt;
                sourceAmt = sourceAmt+va.sourceAmt;
            });

            obj.source = pKey;
            obj.target = key;
            obj.value = sourceAmt;
            obj.numberOfOpps = value.length;
            obj.sourceAmt = sourceAmt;
            obj.targetAmt = targetAmt;
            obj.diffAmt = sourceAmt-targetAmt;

            return obj;
        })
        .value();

    return group;

}

function menuItems(forRelatas){

    return [{
        name:"Dashboard",
        selected:"selected"
    },
        //     {
        //     name:"Commit",
        //     selected:""
        // },{
        //     name:"Team",
        //     selected:""
        // },
        {
            name:"Opportunity",
            selected:""
        },{
            name:"Downloads",
            selected:""
        }]

}

function menuToggleSelection(select,menu) {

    _.each(menu,function (item) {
        if(item.name == select){
            item.selected = "selected"
        } else {
            item.selected = ""
        }
    })
}

function donutChart(data,className,pattern,height,width,thickness){

    var columns = _.map(data,function (el) {
        return [el.name,el.amount]
    });

    if(!height && !width){
        height = 155;
        width = 155;
    }

    if(!thickness) {
        thickness = 0.12*height
    }

    var chart = c3.generate({
        bindto: className,
        data: {
            columns: columns,
            type : 'donut'
        },
        donut: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            },
            width:thickness
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: height,
            width:width
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                return {top: top, left: parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))}
            }
        }
    });

}

function treemapChart(accounts,$scope,share) {

    var accountsData = {};
    var dataAcc = [];

    $scope.noAccounts = accounts.length === 0;

    if(accounts.length>0) {
        accounts.forEach(function (x) {
            for (var key in x) {
                accountsData[key] = (accountsData[key] || 0) + x[key];
            }
        });

        for (var key in accountsData) {
            dataAcc.push({
                name: key,
                total: parseFloat(accountsData[key].toFixed(2))
            })
        }

        var data = {
            "name": "cluster",
            "children": dataAcc
        };

        // var range = shadeGenerator(0,150,136,dataAcc.length,10);
        var range = shadeGenerator(96,125,139,dataAcc.length,10);

        // var color = d3.scale.category20c();
        var color = d3.scale.ordinal()
            .domain([0, dataAcc.length])
            .range(range);

        var treemap =
            d3.layout.treemap()
                .size([100, 100])
                .sticky(true)
                .value(function(d) { return d.total; });

        var div = d3.select(".accChart");

        function position() {
            this
                .style("left", function(d) { return d.x + "%"; })
                .style("top", function(d) { return d.y + "%"; })
                .style("width", function(d) { return d.dx + "%"; })
                .style("height", function(d) { return d.dy + "%"; });
        }

        function getLabel(d) {
            return d.name;
        }

        var tooltip = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);

        var node =
            div.datum(data).selectAll(".node")
                .data(treemap.nodes)
                .enter().append("div")
                .attr("class", "node")
                .call(position)
                .style("background", function(d) {
                    return color(getLabel(d));
                })
                .text(getLabel)
                .on("mouseover", function(d) {
                tooltip.transition()
                    .duration(200)
                    .style("opacity", .99);
                tooltip	.html(treeMapTooltip(d,share))
                    .style("left", (d3.event.pageX - 0) + "px")
                    .style("top", (d3.event.pageY - 0) + "px");
                })
                .on("mouseout", function(d) {
                    tooltip.transition()
                        .duration(500)
                        .style("opacity", 0);
                })
    }

}

function pieChart(data,$scope){

    var productsData = {};
    var dataProd = [];
    var columns = [];

    $scope.noProds = data.length === 0;

    data.forEach(function(x) {
        for(var key in x){
            productsData[key] = (productsData[key] || 0)+x[key];
        }
    });

    for(var key in productsData){
        columns.push([key,productsData[key]])
        dataProd.push({
            name:key,
            total:productsData[key]
        })
    }

    // var pattern = ['#fadad0','#d1c0d1','#60a7b8','#c9deeb']
    var pattern = shadeGenerator(3,169,244,data.length,15)

    var chart = c3.generate({
        bindto: ".productChart",
        data: {
            columns: columns,
            type : 'pie'
        },
        pie: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            }
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: 200,
            width:200
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                var left = parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))
                return {top: top+50, left: 0}
            }
        }
    });

}

function googleMaps($scope,locations){

    $scope.geocoder = new google.maps.Geocoder();
    $scope.myLatlng = new google.maps.LatLng(13.5333, 2.0833); //Global center
    // $scope.myLatlng = new google.maps.LatLng(20.5937, 78.9629); //India

    $scope.mapOptions = {
        center: $scope.myLatlng,
        zoom: 2
    };

    $scope.map = new google.maps.Map(document.getElementById("map"), $scope.mapOptions);
    $scope.markerCluster = new MarkerClusterer($scope.map, [], {zoomOnClick: false, minimumClusterSize: 1, styles: getMarkerIcons()});

    $scope.noRegionsWon = locations.length == 0;

    _.each(locations,function (location,index) {
        createMarkerGeocode($scope,{location:location},0,function () {
        });
    })

    google.maps.event.addListener($scope.markerCluster, 'clusterclick', function (cluster) {
        $scope.c = cluster.getMarkers();
        if (checkRequired($scope.c[0])) {

            $scope.location = {};
            $scope.location.name = $scope.c[0].user.location
            $scope.location.count = $scope.c.length
        }
    });
    google.maps.event.addListener($scope.map, 'zoom_changed', function () {
        if ($scope.map.getZoom() < 2) $scope.map.setZoom(2);
    });
}

function getMarkerIcons() {
    return [
        {
            height: 53,
            url: "/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "/images/m1.png",
            width: 53
        }
    ]
}

function createMarkerGeocode($scope,result, limit,callback) {

    $scope.geocoder.geocode({ 'address': result.location}, function (results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
            callback();
            var addr = results[0].address_components;
            var city = '';
            var country = '';
            var address = '';
            for(var i=0; i<addr.length; i++){
                if(addr[i].types.indexOf("locality") != -1){
                    city = addr[i].long_name;
                }
                if(addr[i].types.indexOf("country") != -1){
                    country = addr[i].long_name;
                }
            }
            if(checkRequired(city)){
                address = city;
            }
            if(checkRequired(country)){
                if(checkRequired(city)){
                    address += ", "+country
                }
                else address = country;
            }

            result.location = address;
            var newAddress = results[0].geometry.location;

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(parseFloat(newAddress.lat()), parseFloat(newAddress.lng())),
                map: $scope.map,
                title: "City: " + address || '',
                user: result
            });

            $scope.markerCluster.addMarker(marker);

        } else if (status == 'OVER_QUERY_LIMIT') {
            $scope.time += 500;
            limit++;
            setTimeout(function () {
                createMarkerGeocode($scope,result, limit,callback);
            },  1);
        }
    });
};

function buildTeamProfiles(data) {
    var team = [];
    var liu = {}
    _.each(data,function (el) {
        if(!el.hierarchyParent){
            liu = {
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                noPicFlag:true,
                nameNoImg:el.firstName.substr(0,2).toUpperCase()
            };
        } else {
            team.push({
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                noPicFlag:true,
                nameNoImg:el.firstName.substr(0,2).toUpperCase()
            })
        }
    });

    team.unshift(liu);
    return team;
}

function buildAllTeamProfiles(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase()
        })
    });

    return team;
}

function closeAllDropDownsAndModals($scope,id,isModal) {
    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                $scope.selectFromList = false;
                resetOtherDropDowns($scope,{name:"closeThis"})
            })
        }
    });
}

function setOppTableHeader($scope,share,filterListObj){

    $scope.headers = [
        {
            filterReq:false,
            cursor:"",
            name:"Opp Name",
            type:"opportunityName"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Bottom Line("+share.primaryCurrency+")",
            type:"convertedAmtWithNgm",
            align:"text-right"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Stage",
            type:"stageName"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Close Date",
            type:"closeDate"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Account",
            type:"account"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Contact",
            type:"contactEmailId"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Product",
            type:"productType"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Type",
            type:"type"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Region",
            type:"geoLocation"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Currency",
            type:"currency"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Amount",
            type:"amount",
            align:"text-right"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Margin(%)",
            type:"netGrossMargin",
            align:"text-right"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Bottom Line",
            type:"convertedAmt",
            align:"text-right"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Owner",
            type:"userEmailId"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"BU",
            type:"businessUnit"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Vertical",
            type:"vertical"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Solution",
            type:"solution"
        }
    ]

    $scope.cellWidth = getWidthOfCell();

    _.each($scope.headers,function (he) {
        var type = getWidthOfCell()[he.type]
        he.styleWidth = type?type:""

        if(filterListObj[he.name]){
            he.values = filterListObj[he.name].values
        }
    })
}

function getWidthOfCell(){
    return {
        currency:'min-width:'+65+'px;max-width:'+65+'px',
        account:'min-width:'+80+'px;max-width:'+80+'px',
        amount:'min-width:'+100+'px;max-width:'+100+'px',
        convertedAmt:'min-width:'+100+'px;max-width:'+100+'px',
        blc:'min-width:'+120+'px;max-width:'+120+'px',
        netGrossMargin:'min-width:'+75+'px;max-width:'+75+'px',
        margin:'min-width:'+75+'px;max-width:'+75+'px',
        closeDate:'min-width:'+100+'px;max-width:'+100+'px',
        type:'min-width:'+75+'px;max-width:'+75+'px'
    }
}

function monthsAndYear(){

    return {
        months:[{
            name:"Jan",
            val:1
        },{
            name:"Feb",
            val:2
        },{
            name:"Mar",
            val:3
        },{
            name:"Apr",
            val:4
        },{
            name:"May",
            val:5
        },{
            name:"Jun",
            val:6
        },{
            name:"Jul",
            val:7
        },{
            name:"Aug",
            val:8
        },{
            name:"Sep",
            val:9
        },{
            name:"Oct",
            val:10
        },{
            name:"Nov",
            val:11
        },{
            name:"Dec",
            val:12
        }],
        years:[{
            name:"2015",
            val:2015
        },{
            name:"2016",
            val:2016
        },{
            name:"2017",
            val:2017
        },{
            name:"2018",
            val:2018
        },{
            name:"2019",
            val:2019
        },{
            name:"2020",
            val:2020
        }]
    }
}

function resetOtherDropDowns($scope,type){

    _.each($scope.headers,function (he) {
        if(he.name !== type.name){
            he.open = false;
        }
    })
}

function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function treeMapTooltip(d,share){
    var wrapperStart = "<div class='tooltip-wrapper'>"
    var wrapperEnd = "</div>"

    return wrapperStart+"<div class='left'>" +d.name+
    "</div>"+
    "<div class='right'>" +share.primaryCurrency+" "+d.total+
    "</div>" +
    "</div>"+wrapperEnd
}

function dealsAtRiskGraph($scope,share,deals,averageRisk,accessControl,totalDeals) {

    var atRisk = 0,safe = 0;
    $scope.total = deals.length;
    $scope.deals = [];
    $scope.totalDealValue = 0;
    var allRisks = _.map(deals,'riskMeter');
    $scope.totalPipeLineValue = 0;

    var dealsWithRiskValue = {};
    var amountAtRisk = 0;

    var maxRisk = _.max(allRisks);
    var minRisk = _.min(allRisks);

    var ngmReq = share.companyDetails && share.companyDetails.netGrossMargin;

    processData()

    function processData(){

        var opportunityStages = {};

        if(share.opportunityStages){
            _.each(share.opportunityStages,function (op) {
                opportunityStages[op.name] = op.order;
            })

            _.each(deals,function (de) {

                var counter = 0;

                _.forIn(de, function(value, key) {
                    if(value === true){
                        counter++
                    }
                });

                de.ngmReq = ngmReq;
                de.amountWithNgm = de.amount;

                if(ngmReq){
                    de.amountWithNgm = (de.amount*de.netGrossMargin)/100
                }


                de.stageStyle2 = oppStageStyle(de.stageName,opportunityStages[de.stageName]-1,true);

                $scope.totalPipeLineValue = $scope.totalPipeLineValue+parseFloat(de.amount);

                $scope.totalDealValue = isNumber(de.amount)?$scope.totalDealValue+parseFloat(de.amount):$scope.totalDealValue+0;

                if(de.riskMeter >= averageRisk && counter<3){
                    $scope.deals.push(de)
                    atRisk++
                    amountAtRisk = amountAtRisk+parseFloat(de.amountWithNgm)
                } else {
                    safe++
                }

                de.amountWithNgm = de.amountWithNgm.r_formatNumber(2)

                var riskPerc = scaleBetween(de.riskMeter,minRisk,maxRisk);

                if(minRisk == maxRisk){
                    riskPerc = 100
                }

                var suggestion = "Opportunity at highest risk";

                if(riskPerc > 70 && riskPerc < 90){
                    suggestion = "Opportunity at high risk";
                }

                if(riskPerc > 50 && riskPerc < 70){
                    suggestion = "Opportunity at medium risk";
                }

                if(riskPerc < 50){
                    suggestion = "Opportunity at low risk";
                }

                de.amountWithCommas = numberWithCommas(parseFloat(de.amount).r_formatNumber(2),share.primaryCurrency == "INR")
                de.riskSuggestion = suggestion;
                de.riskPercentage = riskPerc+'%';
                de.riskPercentageStyle = {
                    'width':riskPerc+'%'
                }

                if(de.riskMeter >= averageRisk){
                    de.riskClass = 'risk-high'
                } else {
                    de.riskClass = 'risk-low'
                }

                dealsWithRiskValue[de.opportunityId] = {
                    riskMeter:de.riskMeter,
                    riskPercentage:de.riskPercentage,
                    riskPercentageStyle:de.riskPercentageStyle,
                    riskSuggestion:de.riskSuggestion,
                    riskClass:de.riskClass,
                    averageRisk:averageRisk,
                    averageInteractionsPerDeal: de.averageInteractionsPerDeal,
                    metDecisionMaker_infuencer: de.metDecisionMaker_infuencer,
                    ltWithOwner: de.ltWithOwner,
                    skewedTwoWayInteractions: de.skewedTwoWayInteractions
                }

            });

            $scope.loadingDealsAtRisk = false;

        } else {
            setTimeOutCallback(1000,function () {
                processData()
            })
        }
    }

}

function drawLineChart($scope,share,series,label,className,series2) {

    var seriesArray = [series];

    if(series && series2){
        seriesArray = [ series, series2]
    }

    new Chartist.Line(className, {
        labels: label,
        series: seriesArray
    }, {
        low: 0,
        showArea: true,
        plugins: [
            tooltip
        ]
    });
}

function donutDraw($scope,share,series,label,className) {

    var chart = new Chartist.Pie(className, {
        series: series,
        labels: label
    }, {
        donut: true,
        donutWidth: 6,
        showLabel:false,
        plugins: [
            tooltip
        ]
    });

    chart.on('draw', function(data) {
        if(data.type === 'slice') {

            var pathLength = data.element._node.getTotalLength();

            data.element.attr({
                'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
            });

            var animationDefinition = {
                'stroke-dashoffset': {
                    id: 'anim' + data.index,
                    dur: 1000,
                    from: -pathLength + 'px',
                    to:  '0px',
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    fill: 'freeze'
                }
            };

            if(data.index !== 0) {
                animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
            }

            data.element.attr({
                'stroke-dashoffset': -pathLength + 'px'
            });

            data.element.animate(animationDefinition, false);
        }
    });
}

function groupAndChainForTeamSummary(data) {

    var totalAmount = 0;
    var group = _
        .chain(_.flatten(data))
        .groupBy(function (el) {
            if(el && el.name){
                return el.name;
            } else if((el && el.name == null) || (el && !el.name && el.amount)) {
                return "Others"
            }
        })
        .map(function(values, key) {

            if(checkRequired(key)){
                var amount = _.sumBy(values, 'amount');
                totalAmount = amount;

                return {
                    nameTruncated:key,
                    name:key,
                    amount:amount,
                    amountWithCommas:getAmountInThousands(amount,3)
                }
            }
        })
        .value();

    var sortProperty = "amount";

    group.sort(function (o1, o2) {
        return o2[sortProperty] > o1[sortProperty] ? 1 : o2[sortProperty] < o1[sortProperty] ? -1 : 0;
    });

    return group;

}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function getColorRange() {
    return ["#f2b290",
        "#45dbf1",
        "#f0b979",
        "#81ceef",
        "#dac987",
        "#edace9",
        "#8ed79b",
        "#cbbaf2",
        "#d3e196",
        "#b5c4ee",
        "#bdedb2",
        "#eeb1c9",
        "#8befd3",
        "#e4b9a9",
        "#64dbd4",
        "#e8cbcc",
        "#7fd4b0",
        "#ccc8dd",
        "#b4caa7",
        "#cbe7ed",
        "#d2c5ab",
        "#9bd9d6",
        "#ede9c9",
        "#abc9cb",
        "#cbf0d7"];
}

var tooltip = Chartist.plugins.tooltip();