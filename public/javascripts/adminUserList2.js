
$(document).ready(function() {
    var table;
    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    getUserList();

    function getUserList(){
        $.ajax({
            url:'/getAllUsers/basicProfile',
            type:'GET',
            datatye:'JSON',
            success:function(userList){

                if(checkRequired(userList)){

                    if(userList.length > 0){
                        applyDataTable()
                        displayUserList(userList)
                    }
                }
            }
        })
    }

    function displayUserList(list){
        var rows = []
        if(checkRequired(list) && list.length > 0){
         var length = list.length-1;
                list.forEach(function(obj,index){
                    (function(o,i){
                        setTimeout(function(){
                            if(checkRequired(o.value) && checkRequired(o.value.firstName)){
                                rows.push(generateRow(o.value,i))
                                if(i == length){
                                    addRowsToTable(rows);
                                }
                            }
                        },0)
                    })(obj,index)
                })
        }
    }

    function generateRow(user,i){
        var regOn = user.registeredDate || '';
        var partialRegOn = user.createdDate;
        var firstName = user.firstName || '';
        var lastName = user.lastName || '';
        var designation = user.designation || '';
        var companyName = user.companyName || '';
        var emailId = user.emailId || '';
        var signUp = user.serviceLogin || '';
        var location = user.location || '';
        var contacts = user.contacts;
        var lastLogin = user.lastLoginDate || '';
        var lastLoginDate = checkRequired(user.lastLoginDate) ? getEventDateFormat(new Date(user.lastLoginDate)) : '';

        return [
            '<span>'+(i +1)+'</span>',
            '<span class="sortdDate" value='+partialRegOn+'>'+getEventDateFormat(new Date(partialRegOn))+'</span>',
            '<span class="sortdDate" value='+regOn+'>'+getEventDateFormat(new Date(regOn))+'</span>',
            '<span>'+lastLoginDate+'</span>',
            //'<span>'+lastLogin+'</span>',
            '<span>'+firstName+'</span>',
            '<span>'+lastName+'</span>',
            '<span>'+contacts+'</span>',
            '<span>'+signUp+'</span>',
            '<span>'+location+'</span>',
            '<span>'+companyName+'</span>',
            '<span>'+designation+'</span>',
            '<span>'+emailId+'</span>',
            '<span>'+user.uniqueName || ''+'</span>'
        ]
    }

    function addRowsToTable(rowArr){
        table.rows.add( rowArr ).draw();
    }

    function getEventDateFormat(date){
        var day = date.getDate()
        var format = getVakidNumber(day)+'-'+monthNameSmall[date.getMonth()]+'-'+date.getFullYear();
        return format;
    }
    function getVakidNumber(num){
        if(num < 10){
            return '0'+num;
        }else return num;
    }

    function applyDataTable(){
        table = $('#userListTable').DataTable({

            "dom": 'T<"top"iflp<"clear">>',
            "tableTools": {
                "sSwfPath": "/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    {
                        "sExtends":"pdf"
                    },
                    {
                        "sExtends":"copy",
                        "sButtonText":"Copy"
                    },
                    {
                        "sExtends":"csv",
                        "sButtonText":"CSV"
                    },
                    {
                        "sExtends":"xls",
                        "sButtonText":"XLS"
                    },
                    {
                        "sExtends":"print",
                        "sButtonText":"Print"
                    }
                ]
            },
            "order": [[ 1, "asc" ]],
            "lengthMenu": [ [10, 25, 100, -1], [10, 25, 100,"ALL"] ],
            "iDisplayLength":100,
            "columns": [
                null,
                { "orderDataType": "dom-value" },
                { "orderDataType": "dom-value" },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ]

        });
    }

    /* Create an array with the values of all the select options in a column */
    $.fn.dataTable.ext.order['dom-value'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('.sortdDate', td).attr("value");
        } );
    };


    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }
});