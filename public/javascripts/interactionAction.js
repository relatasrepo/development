$(document).ready(function(){

    var userTimezone,userProfile,interactions,table;
    getUserProfile();
    $("#userMessage").jqte();

    // Function to get user contacts info
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;
                if(checkRequired(userProfile) && checkRequired(userProfile.timezone) && checkRequired(userProfile.timezone.name)){
                    userTimezone = userProfile.timezone.name;
                }
                else{
                    var tz = jstz.determine();
                    userTimezone = tz.name();
                }
                if( userProfile.profilePicUrl.charAt(0) == '/' || userProfile.profilePicUrl.charAt(0) == 'h'){
                    $('#profilePic').attr("src",userProfile.profilePicUrl);
                } else  $('#profilePic').attr("src","/"+userProfile.profilePicUrl);

                $('#profilePic').error(function(){
                    $(this).attr('src','/images/default.png')
                })
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    pastSevenDaysInteractions();
    // Function to get user contacts info
    function pastSevenDaysInteractions() {
        $.ajax({
            url: '/interactions/pastSevenDays/status/get',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
            interactions = result;
                applyDataTable();
                var params = getParams(window.location.href);
                dis(params)
            },
            error: function (event, request, settings) {

            },
            timeout: 10000
        })
    }

    function getParams(url){
        var regex = /[?&]([^=#]+)=([^&#]*)/g,
            params = {},
            match;
        while(match = regex.exec(url)) {
            params[match[1]] = match[2];
        }
        return params;
    }

    function dis(params){
        switch(params.open){
            case 'left':$('#you-not').trigger('click');
                break;
            case 'right':$('#others-not').trigger('click');
                break;
            default :$('#you-not').trigger('click');
        }
    }

    $('#you-not').on('click',function(){
        $('#you-not').addClass('selected')
        $('#others-not').removeClass('selected')
        if(checkRequired(table)){
            table.rows().remove().draw();
        }
        if(checkRequired(interactions) && checkRequired(interactions.youNotReplied) && interactions.youNotReplied.length > 0){
            display(interactions.youNotReplied)
        }
    });

    $('#others-not').on('click',function(){
        $('#others-not').addClass('selected');
        $('#you-not').removeClass('selected');
        if(checkRequired(table)){
            table.rows().remove().draw();
        }
        if(checkRequired(interactions) && checkRequired(interactions.othersNotReplied) && interactions.othersNotReplied.length > 0){
            display(interactions.othersNotReplied)
        }
    });

    function display(arr){
        //$("#status-table tbody").html('');
        pageHeight();

        if(arr.length > 0){
            var rows = [];
            for(var i=0; i<arr.length; i++){
                var name = '';
                var title = checkRequired(arr[i].title) ? arr[i].title : arr[i].description || '';
                if(checkRequired(arr[i].userId)){
                    name = arr[i].userId.firstName+' '+arr[i].userId.lastName
                }
                else{
                    name = arr[i].emailId;
                }
                var row = [
                    '<span id="nameTitle" title='+name.replace(/\s/g, '&nbsp;')+'>'+getTextLength(name,40)+'</span>',
                    '<span id="sortDate" value='+arr[i].interactionDate+'>'+moment(arr[i].interactionDate).format('DD-MMM-YYYY')+'</span>',
                    '<span>'+arr[i].type+'</span>',
                    '<span id="title" title='+title.replace(/\s/g, '&nbsp;')+'>'+getTextLength(title,55)+'</span>',
                    '<span class="link-span" id="action" style="cursor: pointer" interactionId='+arr[i]._id+'>Message</span>'
                ];
                rows.push(row)
            }
            addRowsToTable(rows);
        }
    }

    $("body").on('click',"#action",function(){
        var interactionId = $(this).attr('interactionId');
        if(checkRequired(interactionId)){
            $.ajax({
                url:'/interactions/get/byId?id='+interactionId,
                type:'GET',
                success:function(interaction){

                    if(interaction){
                        showMessageDetails(interaction)
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 10000
            })
        }
    });
   var profile;
    function showMessageDetails(interaction){
        $("html, body").animate({ scrollTop: $(document).height()-10 }, 1000);
        if(checkRequired(interaction.userId) && checkRequired(interaction.userId._id)){
            profile = interaction.userId;
        }else{
            profile = {
                 _id:'',
                emailId:interaction.emailId,
                firstName:'',
                lastName:''
            }
        }
        $("#messageDetails").show();
        $("#subject").text(interaction.title || '');
       // $("#actual-message").html(checkRequired(interaction.description) ? interaction.description : interaction.title || '');
        $("#messageDetails").show();
        if(checkRequired(interaction.title)){
            if(isReplay(interaction.title.substr(0, 2))){
                $("#MsgSubject").val('RE: '+interaction.title || '');
            }
            else $("#MsgSubject").val(interaction.title || '');
        }
    }
    var reArr = ['re','Re','RE','rE'];
    function isReplay(text){

       return reArr.indexOf(text) == -1
    }

    $("#sendMessage").on('click',function(){

        var massMailArr = [];
        var msg = $("#userMessage").val().trim();
        var subject = $("#MsgSubject").val();
        if(checkRequired(msg) && checkRequired(subject)){
            //msg = 'Hi '+profile.firstName+'<br>'+msg;

            var msgObj = {
                senderEmailId:userProfile.emailId,
                userId:profile._id || '',
                relatasContact: checkRequired(profile._id),
                senderName:userProfile.firstName+' '+userProfile.lastName,
                receiverEmailId:profile.emailId,
                receiverName:profile.firstName+' '+profile.lastName,
                message:msg,
                responseFlag:true,
                headerImage:false
            }
            massMailArr.push(msgObj)
            if(checkRequired(massMailArr[0])){
                var stringMsg = JSON.stringify(massMailArr);

                $.ajax({
                    url:'/sendEmailMessage',
                    type:'POST',
                    datatype:'JSON',
                    data:{data:stringMsg,subject:subject},
                    success:function(response){

                        if(response){

                            showMessagePopUp("Your message has been sent successfully.",'success');
                            // table.row(mElement.parents('tr')).remove().draw();
                            $("#MsgSubject").val('')
                            $("#messageDetails").hide();
                            $("#userMessage").jqteVal("");
                        }
                        else{
                            showMessagePopUp("Your message has not been sent. Please try again.",'error');
                        }
                    }
                })
            }
        }else showMessagePopUp("You have forgotten to enter either Subject OR message. Please try again.",'error');
    });

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }
    pageHeight()
    function pageHeight() {
        var sideHeight = $(window).height();
        $("#content-height").css({'min-height': sideHeight})
    }

    function getTextLength(text,maxLength){
        if(!checkRequired(text)) return '';

        var textLength = text.length;
        if(textLength >maxLength){
            var formattedText = text.slice(0,maxLength)
            return formattedText+'..';
        }
        else return text;
    }

    function addRowsToTable(rowArr){

        table.rows.add( rowArr ).draw();
    }


    function applyDataTable(){
        table = $('#status-table').DataTable({
            "dom": '<"top"iflp<"clear">>',
            "order": [[ 1, "asc" ]],
            "columns": [
                null,
                { "orderDataType": "dom-value" },
                null,
                null,
                null
            ],
            "oLanguage": {
                "sEmptyTable": "No Interactions Found"
            }
        });
    }

    /* Create an array with the values of all the select options in a column */
    $.fn.dataTable.ext.order['dom-value'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('#sortDate', td).attr("value");
        } );
    }

    function showMessagePopUp(message, msgClass) {

        var html = _.template($("#message-popup").html())();
        $("#you-not").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });

        $("#you-not").popover('show');
        $(".popover").css({'margin-left':"20%"});
        $(".arrow").addClass("invisible");
        if (msgClass == 'error') {
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        } else if (msgClass == 'success') {
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        } else if (msgClass == 'tip') {
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message);
        $("#closePopup-message").focus();

    }

    $("body").on("click", "#closePopup-message", function () {
        $("#you-not").popover('destroy');
    });

});