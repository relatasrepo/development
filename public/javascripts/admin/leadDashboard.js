var relatasApp = angular.module('relatasApp', ['angular-loading-bar','ngLodash'],function ($interpolateProvider,$httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

})

relatasApp.controller('header_controller', function($scope, $http) {
    $scope.obj = { noImage: 0 };
    $http({
        method: 'GET',
        url: '/profile/get/edit'
    }).then(function successCallback(response) {
        $scope.profileName = response.data.Data.firstName;
        $scope.profileImage = response.data.Data.profilePicUrl;
    });
});

relatasApp.controller('lead_controller', function($scope, $http) {
    $http.get("/admin/leads/get/all")
        .success(function (response) {
            $scope.leads = [];
            $scope.sortByAlpha = true;
            if(response && response.length>0){
                _.each(response,function (el) {
                    el.type = el.type?el.type:"enterPriseDemo";
                    el.fullName = el.fullName?el.fullName:el.firstName+" "+el.lastName;
                    el.date = moment(el.createdDate).format("DD MMM YYYY")
                    $scope.leads.push(el);
                })
            }
        })
});