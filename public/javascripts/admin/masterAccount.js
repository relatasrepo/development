var relatasApp = angular.module('newHierarchy', ['angular-loading-bar','ngRoute','ngSanitize','ngLodash','ngCsv'],function ($interpolateProvider,$httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

})

relatasApp.controller('header_controller', function($scope, $http,share) {
    share.initLiuProfile = function () {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/current/web'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
            share.companyDetails = response.data.companyDetails
        });
    }

    share.initLiuProfile();
});

relatasApp.controller('hierarchy', function($scope,$http,$rootScope,share) {
    $rootScope.isCorporateAdmin = true;
    $scope.closeModal = function () {
        $scope.newRole = false;
    }

    $scope.searchForType = function(query){


        // TODO
    }

    $scope.editThisType = function(data,index){
        if(data){
            $scope.editAccObj = $scope.typeHeaders;
            _.each($scope.editAccObj,function (el) {
                for(var key in data){
                    if(el.name === key){
                        el.value = data[key]
                    }
                }
            });
        }

        $scope.rel_index_for_ops = index;
        $scope.showAddMoreAccount = !$scope.showAddMoreAccount;
    }

    $scope.saveThisType = function(data){

        if(data){
            data.acc_identifier_rel = $scope.data.model
        }

        $http.post("/corporate/admin/edit/account/type",data)
            .success(function (response) {
            });
    }

    $scope.oppLinkMandatoryFun = function(data){
        $http.post("/corporate/admin/master/opp/link",{name:data.model,oppLinkMandatory:$scope.oppLinkMandatory})
            .success(function (response) {
            });
    }

    share.paintAccountList = function () {

        $http.get("/corporate/admin/get/master/account/types")
            .success(function (response) {

                $scope.typeHeaders = [];
                $scope.lists = [];

                if(response && response[0]){
                    $scope.data = {
                        model: null,
                        availableOptions: response
                    };
                    setPagination($scope,$scope.data.availableOptions[0].size)
                    $scope.masterDataTypeObj = {};

                    _.each(response,function (el) {
                        $scope.masterDataTypeObj[el.name] = el;
                    });

                    $scope.defaultAccount = $scope.data.availableOptions[0].name;
                    $scope.data.model = $scope.defaultAccount;
                    fetchAccounts(null,$scope.defaultAccount);
                }
            })
    }

    share.paintAccountList();

    $scope.saveAccountType = function () {

        if(!$scope.accountListName || $scope.accountListName == "" || $scope.accountListName == " "){
            alert("Please enter master data type")
        } else if(!share.rowObj){
            alert("Please select a valid XLS to upload")
        } else {
            if(share.rowObj && $scope.accountListName) {

                share.rowObj.forEach(function (el,index) {
                    el["R Code"] = ($scope.accountListName.substr(0,1)).toUpperCase()+(index+1)
                    el.relatasAccount = el.relatasAccount?fetchCompanyFromEmail(el.relatasAccount).toLowerCase():""
                });

                $http.post("/corporate/admin/save/master/account/list",{data:share.rowObj,accountListName:$scope.accountListName})
                    .success(function (response) {
                        document.getElementById("file").value = "";
                        share.paintAccountList();
                        $scope.toggleUploadBox();
                    })
            }
        }

    }

    $scope.ExcelExport= function (event) {

        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var fileData = reader.result;
            var wb = XLSX.read(fileData, {type : 'binary'});

            wb.SheetNames.forEach(function(sheetName){
                share.rowObj =XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);

            })
        };
        reader.readAsBinaryString(input.files[0]);
    };

    function fetchAccounts(accountName,accountType,skip) {

        var url = "/corporate/admin/get/master/account/list";

        if(accountType){
            url = fetchUrlWithParameter(url,"accountType",accountType);
        }

        if(skip){
            url = fetchUrlWithParameter(url,"skip",skip);
        }

        if(accountName){
            url = fetchUrlWithParameter(url,"search",accountName);
        }

        $http.get(url)
            .success(function (response) {

                $scope.typeHeaders = [];
                $scope.lists = [];

                if(response && response[0] && response[0].data){

                    for(var key in response[0].data[0]){
                        $scope.typeHeaders.push({
                            name:key,
                            value:""
                        })
                    }

                    $scope.oppLinkMandatory = response[0].oppLinkMandatory;
                    $scope.importantHeaders = response[0].importantHeaders;
                    $scope.lists = response[0].data;
                }
            });
    }

    $scope.editAccount = function(account){
        account.edit = true;
    }

    $scope.addMoreToAccount = function(){
        _.each($scope.typeHeaders,function (th) {
            th.value = "";
        });
        $scope.editAccObj = $scope.typeHeaders;

        $scope.showAddMoreAccount = !$scope.showAddMoreAccount;
    }

    $scope.toggleUploadBox = function(){
        $scope.showUploadBox = !$scope.showUploadBox;
    }

    $scope.setImportantHeaders = function(){
        $scope.showImpColBox = !$scope.showImpColBox;
    }

    $scope.saveUserBuiltAccountType = function(){
        var allEmptyRows = false;
        if($scope.accountListName){
            share.rowObj = $scope.rows;
            _.each(share.rowObj,function (row) {

                for(var key in row){
                    if(key !== "$$hashKey"){
                        if(checkRequired(row[key])){
                            allEmptyRows = true;
                        }
                    }
                }
            });

            if(allEmptyRows){
                $scope.saveAccountType();
            } else {
                toastr.error("All the rows can not be empty");
            }
        } else {
            toastr.error("Please give master data type name");
        }
    }

    $scope.updateCols = function(col,index){

        var newRow = [];
        $scope.rows.forEach(function (row) {
            _.forEach(row,function (value,key) {
                const newKeys = {};
                if(key === col.original){
                    newKeys[key] = col.name;
                    const renamedObj = renameKeys(row, newKeys);
                    newRow.push(renamedObj);
                }
            });
        });
        $scope.rows = newRow;
        col.original = col.name;
    }

    $scope.removeCol = function(col,index){

        $scope.cols = $scope.cols.filter(function (el) {
            return el.name !== col.name;
        });

        var newRow = [];
        $scope.rows.forEach(function (row) {
            var obj = {};
            for(var key in row){
                if(key !== col.name){
                    obj[key] = row[key]
                }
            };
            newRow.push(obj);
        });
        $scope.rows = newRow;
    }

    $scope.removeRow = function(row,index){
        $scope.rows.splice(index, 1);
    }

    // $scope.cols = ["col 1","col 2","col 3"];
    $scope.cols = [{original:"col 1",name:"col 1"},{original:"col 2",name:"col 2"},{original:"col 3",name:"col 3"}];
    $scope.rows = [{"col 1":"","col 2":"","col 3":""}];

    $scope.addColumn = function(){
        var col = "col "+($scope.cols.length+1);
        $scope.cols.push({original:col,name:col});
        $scope.rows.forEach(function (row) {
            row[col] = "";
        });
    }

    $scope.addRow = function(){
        var obj = {};
        _.each($scope.cols,function (co) {
            obj[co.name] = "";
        });
        $scope.rows.push(obj)
    }

    $scope.createNew = function(){
        $scope.showCreateNew = true;
        $scope.showUploadXls = false;
    }

    $scope.uploadXls = function(){
        $scope.showCreateNew = false;
        $scope.showUploadXls = true;
    }

    // $scope.showUploadBox = true;
    // $scope.showCreateNew = true;
    $scope.uploadXls();

    $scope.saveImportantHeaders = function(){
        $scope.showImpColBox = !$scope.showImpColBox;
        var obj = {
            importantHeaders:$scope.importantHeaders,
            acc_identifier_rel:$scope.data.model
        }

        $http.post("/corporate/admin/save/master/data/important/headers",obj)
            .success(function (response) {
            });
    }

    $scope.fetchForAccount = function(accountType){
        setPagination($scope,$scope.masterDataTypeObj[accountType].size)
        fetchAccounts(null,accountType);
    }

    function setPagination($scope,size,to){
        $scope.size = size;
        if(!$scope.from && $scope.from != 0){
            $scope.from = 0;
        }

        if(to){
            $scope.to = to;
        } else if(size>49){
            $scope.to = 50-$scope.from;
        } else {
            $scope.to = size-$scope.from;
        }
        paginationBtnSettings($scope);
    }

    function paginationBtnSettings($scope){

        $scope.prevDisabled = "cursor";
        $scope.nextDisabled = "cursor";

        $scope.prevDisabledNg = false;
        $scope.nextDisabledNg = false;

        if($scope.from === 0){
            $scope.prevDisabled = "opaque";
            $scope.prevDisabledNg = true;
        }

        if($scope.to >= $scope.size){
            $scope.nextDisabled = "opaque";
            $scope.nextDisabledNg = true;
        }
    }

    $scope.goPrev = function(accountType){
        $scope.to = $scope.to-50;
        $scope.from = $scope.from-50;
        setPagination($scope,$scope.size,$scope.to)
        fetchAccounts(null,accountType,$scope.to)
    }

    $scope.goNext = function(accountType){
        $scope.to = $scope.to+50;
        $scope.from = $scope.from+50;
        setPagination($scope,$scope.size,$scope.to)
        fetchAccounts(null,accountType,$scope.to)
    }

    $scope.saveAccount = function(){
        var obj = {
            acc_identifier_rel:$scope.data.model
        };

        _.each($scope.editAccObj,function (el) {
            if(el.name == "relatasAccount"){
                el.value = el.value?fetchCompanyFromEmail(el.value).toLowerCase():""
            }
            obj[el.name] = el.value;
        });

        _.each($scope.lists,function (el,index) {
            if(index === $scope.rel_index_for_ops){
                for(var key1 in obj){
                    for(var key2 in el){
                        if(key1 == key2){
                            el[key2] = obj[key1]
                        }
                    }
                }
            }
        });

        if(!checkRequired(obj["R Code"])){
            obj["R Code"] = ($scope.data.model.substr(0,1)).toUpperCase()+($scope.lists.length+1)
        }

        $http.post("/corporate/admin/edit/master/account/data",obj)
            .success(function (response) {
                toastr.success("Successfully saved.");
                $scope.showAddMoreAccount = !$scope.showAddMoreAccount;
                fetchAccounts(null,$scope.data.model)
            });
    }

    $scope.closeAccount = function(account){
        account.edit = false;
    }

    $scope.linkAccountType = function(account){
        $scope.linkToExistingAcc = !$scope.linkToExistingAcc;
    }

    $scope.saveLinkToAccount = function(emailDomainForLink){

        if(!emailDomainForLink){
            toastr.error("Please enter a valid domain name.")
        } else {

            var obj = {
                acc_identifier_rel:$scope.data.model
            };

            _.each($scope.editAccObj,function (el) {
                obj[el.name] = el.value;
            });

            obj.domain = fetchCompanyFromEmail(emailDomainForLink).toLowerCase();

            $http.post("/corporate/admin/link/master/acc/to/relatas/acc",obj)
                .success(function (response) {
                    toastr.success("Successfully saved.")
                    $scope.linkToExistingAcc = false;
                });
        }
    }

    $scope.deleteAccount = function(index){

        var obj = {
            acc_identifier_rel:$scope.data.model
        };

        _.each($scope.editAccObj,function (el) {
            obj[el.name] = el.value;
        });

        if (window.confirm('Delete data forever?')) {
            $http.post("/corporate/admin/remove/master/account",obj)
                .success(function (response) {
                    $scope.lists.splice(index, 1);
                    $scope.showAddMoreAccount = false;
                    toastr.success("Successfully deleted.")
                });
        }
    }

    $scope.deleteAccountType = function(){

        var accountListName = $('#repeatSelect').find(":selected").text();

        if (window.confirm('This will remove the account type: '+accountListName)) {

            $http.post("/corporate/admin/remove/master/account/type",{accountListName:accountListName})
                .success(function (response) {
                    share.paintAccountList();
                    toastr.success("Account type was successfully deleted.")
                });
        }
    }

    $scope.showNewAccModal = function(account){
        $scope.addNewAccount = true;
    }

    $scope.closeNewAccModal = function(account){
        $scope.addNewAccount = false;
    }

    $scope.searchForAccount = function(accountName){
        var accountListName = $('#repeatSelect').find(":selected").text();
        if(accountName && accountName.length>2){
            $scope.lists = $scope.lists.filter(function (el) {
                return _.includes(el.name.toLowerCase(),accountName.toLowerCase())
            });

            if($scope.lists.length === 0){
                fetchAccounts(accountName,accountListName);
            }
        }
    }
})

relatasApp.service('share', function () {
    return {}
});

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue){
    if(parameterValue instanceof Array)
        if(parameterValue.length > 0)
            parameterValue = parameterValue.join(",")
        else
            parameterValue = null

    if(parameterValue != undefined && parameterValue != null){
        if(baseUrl.indexOf("?") == -1)
            baseUrl += "?"
        else
            baseUrl += "&"
        baseUrl+= parameterName + "=" + parameterValue
    }

    return baseUrl
}

var getEmailIdDomain = function(emailId){
    return emailId.replace(/.*@/, "");
}

function renameKeys(obj, newKeys) {
    const keyValues = Object.keys(obj).map(key => {
        const newKey = newKeys[key] || key;
        return { [newKey]: obj[key] };
    });
    return Object.assign({}, ...keyValues);
}