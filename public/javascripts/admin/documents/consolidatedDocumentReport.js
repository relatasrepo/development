angular.module('adminUserList', ['ngResource', 'ngLodash', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('userList', function($scope, $resource, $http) {

        $scope.listOfCompanies = [];
        $scope.report = {},
        $scope.report.activeTable = "uploadedDocs"

        $scope.uploadedReportTableHeader = [{
            name:"company Name",
            width:"width:25%",
            sortableName:"companyName",
        },{
            name:"User Email Id",
            width:"width:25%",
            sortableName:"userEmailId",
        },{
            name:"Total Docs Uploaded",
            sortableName:"totalUploadedDocs",
        },{
            name:"Total Docs Shared",
            sortableName:"totalDocsShared",
        },{
            name:"Total Docs Opened",
            sortableName:"totalDocsOpened",
        },{
            name:"Last Uploaded Date",
            sortableName:"lastUploadedDate",
        },{
            name:"Last Shared Date",
            sortableName:"",
        },{
            name:"Last Opened Date",
            sortableName:"",
        }]

        $scope.createdReportTableHeader = [{
            name:"company Name",
            width:"width:30%",
            sortableName:"companyName",
        },{
            name:"User Email Id",
            width:"width:30%",
            sortableName:"userEmailId",
        },{
            name:"Total Docs Created",
            sortableName:"totalDocsCreated",
        },{
            name:"Last Created Date",
            sortableName:"lastDocCreated",
        }]

        $http.get('/admin/getListOfCompanies').then(function(response) {
            $scope.listOfCompanies = response.data;
            $scope.listOfCompanies.unshift({_id:null, companyName:"All"});
            $scope.selectedCompany = $scope.listOfCompanies[0];
        });


        $scope.getUploadedDocumentReport = function() {
            $http.get('/consolidate/uploaded/document/report').then(function(response) {
                $scope.uploadedDocResponse = response.data;

                _.each($scope.uploadedDocResponse, function(doc) {

                    doc.lastUploadedTime = doc.lastUploadedDate ? moment(doc.lastUploadedDate).format("hh:mm a") : '';

                    if(doc.lastAccessedDate.length > 0) {
                        var lastAccessed = _.flatten(doc.lastAccessedDate);
                        lastAccessed.sort(function(d1, d2) {
                            return new Date(d2) - new Date(d1);
                        });
                        doc.lastAccessed = lastAccessed[0];
                        doc.lastAccessedTime = doc.lastAccessed ? moment(doc.lastAccessed).format("hh:mm a") : '';
                    }

                    if(doc.lastSharedDate.length > 0) {
                        var lastShared = _.flatten(doc.lastSharedDate);
                        lastShared.sort(function(d1, d2) {
                            return new Date(d2) - new Date(d1);
                        });
                        doc.lastShared = lastShared[0];
                        doc.lastSharedTime = doc.lastShared ? moment(doc.lastShared).format("hh:mm a") : ''
                    }
                });

                $scope.uploadedDocResponse.sort(function(doc1, doc2) {
                    return doc2.lastShared - doc1.lastShared;
                });
                
                $scope.uploadedDocCompanyReport = $scope.uploadedDocResponse;
            });
        }

        $scope.getCreatedDocumentReport = function() {
            $http.get('/consolidate/created/document/report').then(function(response) {
                $scope.createdDocResponse = response.data;

                _.each($scope.createdDocResponse, function(doc) {
                    doc.lastDocCreatedTime = doc.lastDocCreated ? moment(doc.lastDocCreated).format("hh:mm a") : '';
                })

                $scope.createdDocCompanyReport = $scope.createdDocResponse;

            });
        }

        $scope.order = function(header){
            $scope.orderByField = header.sortableName;
            $scope.reverseSort = !$scope.reverseSort;
        }

        $scope.getReportFor = function(report) {
            $scope.report.activeTable = report.activeTable;

            if(report.activeTable == 'uploadedDocs' && !$scope.uploadedDocResponse)
                $scope.getUploadedDocumentReport();

            else if(report.activeTable == 'createdDocs' && !$scope.createdDocResponse)
                $scope.getCreatedDocumentReport();
        }

        $scope.filterByCompany = function(companyId) {
            if($scope.report.activeTable == 'uploadedDocs') {
                if(!companyId)
                    $scope.uploadedDocCompanyReport = $scope.uploadedDocResponse
                else {
                    $scope.uploadedDocCompanyReport = _.filter($scope.uploadedDocResponse, function(doc) {
                        return doc._id.companyId == companyId;
                    })
                }

            } else {
                if(!companyId) 
                    $scope.createdDocCompanyReport = $scope.createdDocResponse;
                else {
                    $scope.createdDocCompanyReport = _.filter($scope.createdDocResponse, function(doc) {
                        return doc._id.companyId == companyId;
                    })
                }
            }
        }

        $scope.getUploadedDocumentReport();
        
    })
    /* .filter('byEmailCompanyName', function() {
        return function(names, search) {  
            if(angular.isDefined(search)) {
                var results = [];
                var i;
                var searchVal = search.toLowerCase();
                for(i = 0; i < names.length; i++){
                        var firstName = names[i].firstName.toLowerCase();
                        if(names[i].lastName !== undefined && names[i].lastName !== null)
                            var lastName = names[i].lastName.toLowerCase();
                        else
                            var lastName = '';
                        if(names[i].companyName !== undefined && names[i].companyName !== null)
                            var company = names[i].companyName.toLowerCase();
                        else
                            var company = '';
                        if(names[i].emailId !== undefined && names[i].emailId !== null)
                            var email = names[i].emailId.toLowerCase();
                        else
                            var email = '';
                        if(firstName.indexOf(searchVal) >=0 || lastName.indexOf(searchVal) >=0  || company.indexOf(searchVal) >=0 || email.indexOf(searchVal) >=0){
                        results.push(names[i]);
                        }
                    }
                return results;
            } else {
                return names;
            }
        };
    }); */
