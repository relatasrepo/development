angular.module('adminUserList', ['ngResource', 'ngLodash', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('userList', function($scope, $resource, $http) {

        $scope.listOfCompanies = [];
        $scope.orderByField = 'companyName';
        $scope.reverseSort = false;
        $scope.report = {},
        $scope.report.activeTable = "uploadedDocs"

        $scope.uploadedReportTableHeader = [{
            name:"company Name",
            sortableName:"companyName",
        },{
            name:"User Email Id",
            sortableName:"userEmailId",
        },{
            name:"Document Name",
            sortableName:"documentName",
        },{
            name:"Category",
            sortableName:"category",
        },{
            name:"Shared By",
            sortableName:"sharedBy",
        },{
            name:"Shared With",
            sortableName:"sharedWith",
        },{
            name:"Date Shared On",
            width:"width: 8%;",
            sortableName:"sharedOnDate",
        },{
            name:"Link Access Count",
            sortableName:"linkAccessCount",
        },{
            name:"Document Opened On",
            sortableName:"firstAccess",
        },{
            name:"Number of times Opened",
            width:"width: 4%;",
            sortableName:"docAccessCount",
        },{
            name:"Doc Read Time (min)",
            sortableName:"totalReadTime",
        }]

        $scope.createdReportTableHeader = [{
            name:"company Name",
            sortableName:"companyName",
        },{
            name:"User Email Id",
            sortableName:"documentCreatedBy",
        },{
            name:"Document Name",
            sortableName:"documentName",
        },{
            name:"Doc Created On",
            sortableName:"documentCreatedDate",
        },{
            name:"Doc Stage",
            sortableName:"documentStage",
        },{
            name:"Doc Version",
            sortableName:"documentVersion",
        }]

        $http.get('/admin/getListOfCompanies').then(function(response) {
            $scope.listOfCompanies = response.data;
            $scope.listOfCompanies.unshift({_id:null, companyName:"All"});
            $scope.selectedCompany = $scope.listOfCompanies[0];
        });


        $scope.getUploadedDocumentReport = function() {
            $http.get('/detail/uploaded/document/report').then(function(response) {
                $scope.uploadedDocResponse = response.data;

                _.each($scope.uploadedDocResponse, function(doc) {
                    doc.sharedOnTime = doc.sharedOnDate ? moment(doc.sharedOnDate).format("hh:mm a") : '';
                    doc.firstAccessTime = doc.firstAccess ? moment(doc.firstAccess).format("hh:mm a") : '';
                });

                $scope.uploadedDocResponse.sort(function(doc1, doc2) {
                    return doc2.sharedOnDate = doc1.sharedOnDate;
                })

                $scope.uploadedDocCompanyReport = $scope.uploadedDocResponse;
            });
        }

        $scope.getCreatedDocumentReport = function() {
            $http.get('/detail/created/document/report').then(function(response) {
                $scope.createdDocResponse = response.data;

                _.each($scope.createdDocResponse, function(doc) {
                    doc.documentCreatedTime = doc.documentCreatedDate ? moment(doc.documentCreatedDate).format("hh:mm a") : '';
                });

                $scope.createdDocCompanyReport = $scope.createdDocResponse;
            });
        }

        $scope.order = function(header){
            $scope.orderByField = header.sortableName;
            $scope.reverseSort = !$scope.reverseSort;
        }

        $scope.getReportFor = function(report) {
            $scope.report.activeTable = report.activeTable;

            if(report.activeTable == 'uploadedDocs' && !$scope.uploadedDocResponse)
                $scope.getUploadedDocumentReport();

            else if(report.activeTable == 'createdDocs' && !$scope.createdDocResponse)
                $scope.getCreatedDocumentReport();
        }

        $scope.getUploadedDocumentReport();

        $scope.filterByCompany = function(companyId) {
            if($scope.report.activeTable == 'uploadedDocs') {
                if(!companyId)
                    $scope.uploadedDocCompanyReport = $scope.uploadedDocResponse
                else {
                    $scope.uploadedDocCompanyReport = _.filter($scope.uploadedDocResponse, function(doc) {
                        return doc.companyId == companyId;
                    })
                }

            } else {
                if(!companyId) 
                    $scope.createdDocCompanyReport = $scope.createdDocResponse;
                else {
                    $scope.createdDocCompanyReport = _.filter($scope.createdDocResponse, function(doc) {
                        return doc.companyId == companyId;
                    })
                }
            }
        }
        
    })
    /* .filter('byEmailCompanyName', function() {
        return function(names, search) {  
            if(angular.isDefined(search)) {
                var results = [];
                var i;
                var searchVal = search.toLowerCase();
                for(i = 0; i < names.length; i++){
                        var firstName = names[i].firstName.toLowerCase();
                        if(names[i].lastName !== undefined && names[i].lastName !== null)
                            var lastName = names[i].lastName.toLowerCase();
                        else
                            var lastName = '';
                        if(names[i].companyName !== undefined && names[i].companyName !== null)
                            var company = names[i].companyName.toLowerCase();
                        else
                            var company = '';
                        if(names[i].emailId !== undefined && names[i].emailId !== null)
                            var email = names[i].emailId.toLowerCase();
                        else
                            var email = '';
                        if(firstName.indexOf(searchVal) >=0 || lastName.indexOf(searchVal) >=0  || company.indexOf(searchVal) >=0 || email.indexOf(searchVal) >=0){
                        results.push(names[i]);
                        }
                    }
                return results;
            } else {
                return names;
            }
        };
    }); */
