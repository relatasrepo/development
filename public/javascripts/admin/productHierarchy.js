var relatasApp = angular.module('newHierarchy', ['angular-loading-bar','ngLodash'])

relatasApp.controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    });

relatasApp.controller('hierarchy', function($scope,$http,$rootScope,share) {

    $scope.resetReportee = function () {
        $scope.item = {}
    }

    share.refreshChart = function () {
        kickOffHierarchyPaint()
    }

    function kickOffHierarchyPaint() {
        $('#chart-container').empty();
        getTeamData($scope,$http,function (response) {

            if(response && response.SuccessCode){

                share.teamObj = {};
                share.hierarchyObj = {};
                $scope.hierarchyExists = response.Data.length>0
                if(response.Data.length>0){
                    var hierarchy = convertArrayToTree(response.Data);
                    chartDraw($scope,$http,share,hierarchy);
                    share.unassigned = _.differenceBy(response.companyMembers,response.Data,"emailId");
                } else {
                    share.unassigned = response.companyMembers
                }

                $scope.unassigned = buildTeamProfile(share.unassigned);

                _.each(response.companyMembers,function (el) {
                    share.teamObj[el._id] = el;
                })
                _.each(response.Data,function (el) {
                    share.hierarchyObj[el._id] = el;
                })
            }
        });
    }

    kickOffHierarchyPaint();

    $('#unassignedList').on('click', function() {
        $(".unassigned-list").toggle(10)
    });

    $scope.closeModal = function () {
        $scope.editUserData = false;
    }

    $scope.removeUser = function (user) {
        var toRemove = user.userId?[user.userId]:[user._id];

        if(user.children && user.children.teamMatesUserId && user.children.teamMatesUserId.length>0){
            toRemove = toRemove.concat(user.children.teamMatesUserId)
        }

        var toRemoveNames = "";
        _.each(toRemove,function (el,index) {
            toRemoveNames = toRemoveNames+share.teamObj[el].firstName+", "
        })

        if (window.confirm('This will remove '+toRemoveNames+" from the hierarchy")) {

            $http.post('/corporate/admin/product/users/remove',{userIds:toRemove})
                .success(function (response) {
                    $scope.editUserData = false;
                    kickOffHierarchyPaint();
                    $scope.user = {}
                });
        }
    }

    $scope.addUser = function (child) {
        $scope.item = child;
        getFullPathToParent(share.oc,$scope.user._id,"",function (path) {
            var hierarchyParent = $scope.user._id;
            var hierarchyPath = path+","+$scope.user._id;

            var updateObj = {
                hierarchyParent:hierarchyParent,
                hierarchyPath:hierarchyPath,
                userId:child.userId,
                emailId:child.emailId
            }

            share.hierarchyAddUser = updateObj;
        });
        $scope.searchList = [];
    }

    $scope.addUserToHierarchy = function (child) {
        $scope.orgHead = child;
        $scope.searchList = [];
        $scope.keyword = ""
        $(".no-hr-text").hide(100)
    }

    $scope.assignOrgHead = function () {

        if($scope.orgHead){
            var data = {
                userId: $scope.orgHead.userId,
                emailId:$scope.orgHead.emailId,
                hierarchyParent: null,
                hierarchyPath:null,
                orgHead:true
            }

            $http.post("/corporate/admin/product/set/org/head",data)
                .success(function (response) {
                    toastr.success("Updated data successfully");
                    $scope.orgHead = null;
                    kickOffHierarchyPaint();
                })
        }
    }

    $('#resetOrgHead').on('click', function() {
        $(".no-hr-text").show(100)
        $scope.$apply(function (){
            $scope.orgHead = null
        })
    });

    $scope.searchForUser = function (query) {

        if(query && query.length>0){
            query = query.toLowerCase();

            var results = $scope.unassigned.map(function (el) {
                if(_.includes(el.emailId.toLowerCase(),query) || _.includes(el.lastName.toLowerCase(),query) || _.includes(el.firstName.toLowerCase(),query)){
                    return el;
                }
            });

            $scope.searchList = _.compact(results);
        } else {
            $scope.searchList = []
        }
    }

    $scope.saveUserData = function () {
        var user = share.hierarchyAddUser;
        $http.post('/corporate/admin/product/user/update',user)
            .success(function (response) {
                kickOffHierarchyPaint();
                $scope.item = {};
                $scope.keyword = "";
                toastr.success("Updated data successfully")
            });
    }

})

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            scope.imageNotLoaded = function(obj) {
                obj.noImage = 1;
            };
            element.bind('load', function() {
            });
            element.bind('error', function() {
                scope.$apply(attrs.imageonload);
            });
        }
    };
})

relatasApp.directive('searchResults', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="searchList.length>0">' +
        '<div ng-repeat="item in searchList track by $index"> ' +
        '<div class="clearfix cursor" style="margin: 10px 0;" title="{{item.fullName}}, {{item.emailId}}" ng-click="addUser(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="item.noImage === 0" ng-src="{{item.image}}" title="{{item.fullName}}, {{item.emailId}}" class="contact-image">' +
        '<span ng-if="item.noImage === 1">' +
        '<span class="contact-no-image">{{item.firstName.substring(0,2)}}</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">{{item.name}}</p><p class="margin0">{{item.emailId}}</p></div>' +
        '<div class="pull-left">' +
        '<p class="margin0">{{item.fullName}}</p>' +
        '<p class="margin0">{{item.emailId}}</p>' +
        '</div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchResultsNoHierarchy', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="searchList.length>0">' +
        '<div ng-repeat="item in searchList track by $index"> ' +
        '<div class="clearfix cursor" style="margin: 10px 0;" title="{{item.fullName}}, {{item.emailId}}" ng-click="addUserToHierarchy(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="item.noImage === 0" ng-src="{{item.image}}" title="{{item.fullName}}, {{item.emailId}}" class="contact-image">' +
        '<span ng-if="item.noImage === 1">' +
        '<span class="contact-no-image">{{item.firstName.substring(0,2)}}</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">{{item.name}}</p><p class="margin0">{{item.emailId}}</p></div>' +
        '<div class="pull-left">' +
        '<p class="margin0">{{item.fullName}}</p>' +
        '<p class="margin0">{{item.emailId}}</p>' +
        '</div>' +
        '</div></div></div>'
    };
});

relatasApp.service('share', function () {
    return {}
});

function chartDraw($scope,$http,share,datasource) {
    $(function() {
        var oc = $('#chart-container').orgchart({
            'data' : datasource,
            'nodeContent': 'title',
            // 'pan': true,
            // 'zoom': true,
            'draggable': true,
            'dropCriteria': function($draggedNode, $dragZone, $dropZone) {
                return true;
            }
        });
        share.oc = oc;
        oc.$chart.on('nodedrop.orgchart', function(event, extraParams) {
            setTimeout(function () {
                var draggedNodeId = extraParams.draggedNode.attr('id');
                var dropZoneId = extraParams.dropZone.attr('id');

                getFullPathToParent(oc,draggedNodeId,"",function (path) {
                    var userObj = getUserDetails($scope,share,draggedNodeId);
                    var pathForChildren = path+","+draggedNodeId;
                    if(userObj.children && userObj.children.teamMatesUserId && userObj.children.teamMatesUserId.length>0){
                        updateHierarchyPath($scope,$http,share,dropZoneId,draggedNodeId,path,pathForChildren,userObj.children.teamMatesUserId)
                    } else {
                        updateHierarchyPath($scope,$http,share,dropZoneId,draggedNodeId,path,pathForChildren)
                    }
                });
            },10)
        });

        $('.orgchart').addClass('noncollapsable');
        editHierarchy($scope,share,oc);
        updateHierarchy(oc)
    });
}

function updateHierarchy(oc) {
    $('#btn-export-hier').on('click', function() {
        if (!$('pre').length) {
            var hierarchy = oc.getHierarchy();
            $('#btn-export-hier').after('<pre>').next().append(JSON.stringify(hierarchy, null, 2));
        }
    });
}

function editHierarchy($scope,share,oc) {

    oc.$chartContainer.on('click', '.node', function() {
        var $this = $(this);

        $scope.$apply(function (){
            $scope.editUserData = true;
            $scope.user = getUserDetails($scope,share,$this.attr('id'))
        });
        $('#selected-node').val($this.find('.title').text()).data('node', $this);
    });

    oc.$chartContainer.on('click', '.orgchart', function(event) {
        if (!$(event.target).closest('.node').length) {
            $('#selected-node').val('');
        }
    });

    $('input[name="chart-state"]').on('click', function() {
        $('.orgchart').toggleClass('view-state', this.value !== 'view');
        $('#edit-panel').toggleClass('view-state', this.value === 'view');
        if ($(this).val() === 'edit') {
            $('.orgchart').find('tr').removeClass('hidden')
                .find('td').removeClass('hidden')
                .find('.node').removeClass('slide-up slide-down slide-right slide-left');
        } else {
            $('#btn-reset').trigger('click');
        }
    });

    $('input[name="node-type"]').on('click', function() {
        var $this = $(this);
        if ($this.val() === 'parent') {
            $('#edit-panel').addClass('edit-parent-node');
            $('#new-nodelist').children(':gt(0)').remove();
        } else {
            $('#edit-panel').removeClass('edit-parent-node');
        }
    });

    $('#btn-add-input').on('click', function() {
        $('#new-nodelist').append('<li><input type="text" class="new-node"></li>');
    });

    $('#btn-remove-input').on('click', function() {
        var inputs = $('#new-nodelist').children('li');
        if (inputs.length > 1) {
            inputs.last().remove();
        }
    });

    $('#btn-delete-nodes').on('click', function() {
        var $node = $('#selected-node').data('node');
        if (!$node) {
            alert('Please select one node in orgchart');
            return;
        } else if ($node[0] === $('.orgchart').find('.node:first')[0]) {
            if (!window.confirm('Are you sure you want to delete the whole chart?')) {
                return;
            }
        }

        if (window.confirm('Are you sure you want to remove the team member and subsequent reportees?')) {
            oc.removeNodes($node);

            $('#selected-node').val('').data('node', null);
            $scope.$apply(function () {
                $scope.editUserData = false;
            })
        } else {
            return;
        }
    });

    $('#btn-reset').on('click', function() {
        $('.orgchart').find('.focused').removeClass('focused');
        $('#selected-node').val('');
        $('#new-nodelist').find('input:first').val('').parent().siblings().remove();
        $('#node-type-panel').find('input').prop('checked', false);
    });
}

function getUserDetails($scope,share,userId) {
    return share.hierarchyObj[userId]
}

function getTeamData($scope,$http,callback) {
    $http.get('/company/product/hierarchy')
        .success(function (response) {
        callback(response)
    });
}

function convertArrayToTree(data){

    var root = data.find(function(item) {
        return item.hierarchyParent === null;
    });

    var tree = {
        _id: root._id,
        id: root._id,
        firstName: root.firstName,
        lastName: root.lastName,
        name: root.firstName +" "+root.lastName,
        designation: root.designation,
        title: root.designation,
        emailId: root.emailId
    };

    var parents = [tree];
    while (parents.length > 0) {
        var newParents = [];
        parents.forEach(function(parent) {
            data.filter(function(item) {
                return item.hierarchyParent === parent._id
            }).forEach(function(child) {
                var c = { _id: child._id,id: child._id,title: child.designation,name: child.firstName +" "+child.lastName, firstName: child.firstName, lastName: child.lastName, designation: child.designation, emailId: child.emailId};
                parent.children = parent.children || [];
                parent.children.push(c);
                newParents.push(c);
            });
        });
        parents = newParents;
    }

    return tree
}

function getFullPathToParent(oc,currentId,path,callback) {

    var currentNode = $("#"+currentId),
        parentId = oc.getRelatedNodes(currentNode,'parent').attr('id')

    if(parentId){
        path = ","+parentId+path;
        getFullPathToParent(oc,parentId,path,callback)
    } else {
        if(callback){
            callback(path)
        }
    }
}

function buildTeamProfile(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noImage:0,
            firstName:el.firstName,
            lastName:el.lastName
        })
    });

    return team;
}

function updateHierarchyPath($scope,$http,share,selfParentId,selfId,pathForSelf,pathForChildren,childrenIds) {

    var url = "/corporate/admin/product/update/hierarchy/path"
    var updateObj = {
        selfId:selfId,
        selfParentId:selfParentId,
        selfHierarchyParent:selfId,
        hierarchyParentForChildren:selfId,
        pathForSelf:pathForSelf,
        pathForChildren:pathForChildren,
        childrenIds:childrenIds
    }

    $http.post(url,updateObj)
        .success(function (response) {
            share.refreshChart()
        })
}