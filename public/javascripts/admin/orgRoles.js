var relatasApp = angular.module('newHierarchy', ['angular-loading-bar','ngLodash'],function ($interpolateProvider,$httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

})

relatasApp.controller('header_controller', function($scope, $http) {
    $scope.obj = { noImage: 0 };
    $http({
        method: 'GET',
        url: '/profile/get/edit'
    }).then(function successCallback(response) {
        $scope.profileName = response.data.Data.firstName;
        $scope.profileImage = response.data.Data.profilePicUrl;
    });
});

relatasApp.controller('hierarchy', function($scope,$http,$rootScope,share) {
    $rootScope.isCorporateAdmin = true;
    $scope.closeModal = function () {
        $scope.newRole = false;
    }

    $scope.addNewRole = function () {
        $scope.rolesList.unshift({
            roleName:"Edit role name",
            editRole:true,
            users:[]
        })
    }

    $scope.editRoleName = function (role) {
        role.editRole = !role.editRole;
        $scope.newRoleName = role.roleName;
    }

    $scope.updateRoleName = function (role,newRoleName) {
        role.editRole = !role.editRole;
        role.prevRoleName = role.roleName;
        role.roleName = newRoleName;
    }

    $scope.removeRole = function (role) {

        if (window.confirm('Are you sure you want to delete the selected role?')) {
            $http.post("/corporate/admin/role/remove",{roleName:role.roleName})
                .success(function (response) {
                    $scope.rolesList = $scope.rolesList.filter(function (el) {
                        return el.$$hashKey != role.$$hashKey;
                    })
                })
        }

    }

    $scope.removeUser = function (role,user) {
        role.users = role.users.filter(function (el) {
            return el.emailId != user.emailId;
        })
    }

    $scope.openTeamMemberAdd = function (role) {
        role.showTeamMemberAdd = !role.showTeamMemberAdd;
    }

    getTeamData($scope,$http,function (team) {
        $scope.team = team;
        paintRoles();
    });


    function paintRoles(){
        getRoles($scope,$http,function (roles) {
            roles.editRole = true;
            $scope.rolesList = roles;
            $scope.rolesList.sort(function (o2,o1) {
                return new Date(o1.createdDate) > new Date(o2.createdDate) ? 1 : new Date(o1.createdDate) < new Date(o2.createdDate) ? -1 : 0;
            });

            var otherUsers = _.differenceBy($scope.team,_.flatten(_.map($scope.rolesList,"users")),"emailId");

            $scope.rolesList.push({
                roleName:"Others",
                users:otherUsers,
                nonEditable:true
            });

            console.log($scope.rolesList)
        });
    }

    $scope.addUser = function (role,user) {

        var userExists = false;
        _.each(role.users,function (el) {
            if(el.emailId == user.emailId){
                userExists  = true;
            }
        })

        if(!userExists){
            role.users.unshift(user);
        }

        role.searchList = [];
        role.keyword = "";
    }

    $scope.searchForUser = function (role,query) {

        if(query && query.length>0){
            query = query.toLowerCase();

            var results = $scope.team.map(function (el) {
                if(_.includes(el.emailId.toLowerCase(),query) || _.includes(el.lastName.toLowerCase(),query) || _.includes(el.firstName.toLowerCase(),query)){
                    return el;
                }
            });

            role.searchList = _.compact(results);
        } else {
            role.searchList = []
        }
    }

    $scope.saveUserData = function (role) {

        if(role && role.users && role.users.length>0){
            role.users = _.uniqBy(role.users,"emailId");
            $http.post('/corporate/admin/role/update',role)
                .success(function (response) {
                    paintRoles()
                });
        } else {
            toastr.error("Please add team members before saving the team role.")
        }
    }

})

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            scope.imageNotLoaded = function(obj) {
                obj.noImage = 1;
            };
            element.bind('load', function() {
            });
            element.bind('error', function() {
                scope.$apply(attrs.imageonload);
            });
        }
    };
})

relatasApp.directive('searchResults', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="item.searchList.length>0">' +
        '<div ng-repeat="teamMember in item.searchList track by $index"> ' +
        '<div class="clearfix cursor" style="margin: 10px 0;" title="{{teamMember.fullName}}, {{teamMember.emailId}}" ng-click="addUser(item,teamMember)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="teamMember.noImage === 0" ng-src="{{teamMember.image}}" title="{{teamMember.fullName}}, {{teamMember.emailId}}" class="contact-image">' +
        '<span ng-if="teamMember.noImage === 1">' +
        '<span class="contact-no-image">{{teamMember.firstName.substring(0,2)}}</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">{{teamMember.name}}</p><p class="margin0">{{teamMember.emailId}}</p></div>' +
        '<div class="pull-left">' +
        '<p class="margin0">{{teamMember.fullName}}</p>' +
        '<p class="margin0">{{teamMember.emailId}}</p>' +
        '</div>' +
        '</div></div></div>'
    };
});

relatasApp.service('share', function () {
    return {}
});

function getTeamData($scope,$http,callback) {
    $http.get('/corporate/admin/all/team/members')
        .success(function (response) {
            callback(buildTeamProfile(response))
        });
}

function getRoles($scope,$http,callback) {
    $http.get('/organisation/get/roles')
        .success(function (response) {
            callback(response.Data)
        });
}

function buildTeamProfile(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noImage:0,
            firstName:el.firstName,
            lastName:el.lastName
        })
    });

    return team;
}

function triggerChange(){
    window.onbeforeunload = function (e) {
        var message = "Please click on 'Save' post you have added team members to the role.",
            e = e || window.event;
        // For IE and Firefox
        if (e) {
            e.returnValue = message;
        }

        // For Safari
        return message;
    };
}