
$(document).ready(function(){
    var table;
    applyDataTable();
    getContacts(0,20);
    function getContacts(skip,limit){

        $.ajax({
            url:"/contacts/filter/custom/web?skip="+skip+"&limit="+limit,
            type: "GET",
            success:function(response){

                if(response.SuccessCode == 1){
                    if(response.Data.returned > 0){
                        //$("#left-contact-list").html('');
                        var html = '<div>';
                        for(var i=0; i<response.Data.contacts.length; i++){
                            var isUser = typeof response.Data.contacts[i].personId == 'object';
                            var name = isUser ? response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName : response.Data.contacts[i].personName || '-';
                            var image = isUser ? '/getImage/'+response.Data.contacts[i].personId._id : '/images/default.png';
                            var emailId = response.Data.contacts[i].personEmailId || '';

                            var mobileNumber = 'none';
                            if(isUser && checkRequired(response.Data.contacts[i].personId.mobileNumber)){
                                mobileNumber = response.Data.contacts[i].personId.mobileNumber;
                            }
                            else if(checkRequired(response.Data.contacts[i].mobileNumber)){
                                mobileNumber = response.Data.contacts[i].mobileNumber
                            }

                            html += '<div style="padding: 10px;" id="contact-div" class="contact" mobileNumber='+mobileNumber+' emailId='+emailId+'><img src='+image+' style="width: 40px">';
                            html += '<span>&nbsp;&nbsp;'+getTextLength(name,15)+'</span>';
                            html += '</div>';
                        }
                        html += '</div>';

                        html += '<span id="more_contacts" skipped='+response.Data.skipped+' style="cursor:pointer">More..</span>';

                        $("#left-contact-list").append(html);
                    }
                }
            }
        })
    }

    $("body").on('click','#more_contacts',function(){
        var lastSkipped = $(this).attr('skipped');
        getContacts(parseInt(lastSkipped)+20,20);
    });

    $("body").on('click','#contact-div',function(){
        var emailId = $(this).attr('emailId');
        var mobileNumber = $(this).attr('mobileNumber');

        if(checkRequired(emailId)){
            getContactProfile('emailId',emailId)
            getAllInteractions('emailId',emailId,mobileNumber);
        }
    });

    var url = window.location.href;
    url = url.split('/');
    var idType = url[url.length - 2]
    var id = url[url.length - 1]
    getContactProfile(idType,id)
    getAllInteractions(idType,id,null);
    function getContactProfile(idType,id){
        $.ajax({
            url:"/contacts/get/profile/"+idType+"/"+id,
            type: "GET",
            success:function(response){
                if(response.SuccessCode == 1){
                    if(checkRequired(response.Data.contact)){
                        $("#c-favorite").text(response.Data.contact.favorite ? 'Favorite: Yes' : 'Favorite: No');
                        if(!checkRequired(response.Data.profile)){
                            $("#c-image").attr('src','/images/default.png');
                            var name = response.Data.contact.personName || '';
                            $("#c-name").text(name);
                            var company_name = response.Data.contact.companyName || ''
                            var designation = response.Data.contact.designation || ''
                            $("#c-head-line").text(designation+', '+company_name);
                            $("#c-location").text(checkRequired(response.Data.contact.location) ? response.Data.contact.location : '');
                            $("#c-mobile-number").text(checkRequired(response.Data.contact.mobileNumber) ? response.Data.contact.mobileNumber : '');
                            $("#c-emailId").text(checkRequired(response.Data.contact.personEmailId) ? response.Data.contact.personEmailId : '');
                            $("#c-message").text("Send Message to "+name+" to reconnect");
                            $("#c-message-in").text("It's been long since we last interacted. It would be great to find some time to catch up you.")
                        }
                    }

                    if(checkRequired(response.Data.profile)){
                        $("#c-image").attr('src','/getImage/'+response.Data.profile._id);
                        $("#c-name").text(response.Data.profile.firstName+' '+response.Data.profile.lastName);
                        var company_name2 = response.Data.profile.companyName || ''
                        var designation2 = response.Data.profile.designation || ''
                        $("#c-head-line").text(designation2+', '+company_name2);
                        $("#c-location").text(checkRequired(response.Data.profile.location) ? response.Data.profile.location : '');
                        $("#c-mobile-number").text(checkRequired(response.Data.profile.mobileNumber) ? response.Data.profile.mobileNumber : '');
                        $("#c-emailId").text(checkRequired(response.Data.profile.emailId) ? response.Data.profile.emailId : '');
                        $("#c-emailId").attr('_id',response.Data.profile._id);
                        $("#c-message").text("Send Message to "+response.Data.profile.firstName+" to reconnect");
                        $("#c-message-in").text("It's been long since we last interacted. It would be great to find some time to catch up you my calendar is available here "+response.Data.profile.userUrl+".")
                    }

                    if(checkRequired(response.Data.lastInteraction)){
                        $("#c-last-interaction").text('Last Interaction: '+moment(response.Data.lastInteraction.interactionDate).format('DD-MMM-YYYY'))
                    }
                }
            }
        })
    }

    function getAllInteractions(idType,id,mobileNumber){
        $.ajax({
            url:"/interactions/get/all/summary/"+idType+"/"+id+"?mobileNumber="+mobileNumber,
            type:"GET",
            success:function(response){
                table
                    .rows()
                    .remove()
                    .draw();
                if(response.SuccessCode == 1){
                    if(response.Data.length > 0){
                        for(var i=0; i<response.Data.length; i++){

                            var actionTaken = '<span>&nbsp;</span>';
                            if(response.Data[i].type == 'email'){
                                if(checkRequired(response.Data[i].trackInfo)){
                                    actionTaken = moment(response.Data[i].trackInfo.lastOpenedOn).format('DD-MMM-YYYY');
                                }
                                else actionTaken = '<button id="send-mail">RE-SEND</button>';
                            }
                            else if(response.Data[i].type == 'twitter'){
                                actionTaken = '<table class="links"><tr><td><a id="twitter_replay" refId='+response.Data[i].refId+'>Replay</a></td><td><a id="twitter_favorite" refId='+response.Data[i].refId+'>Favorite</a></td><td><a id="twitter_re_tweet" refId='+response.Data[i].refId+'>Re-Tweet</a></td></tr></table><div style="display: none" id='+response.Data[i].refId+'><span id="err-msg-table"></span></div>';
                            }
                            else if(response.Data[i].type == 'meeting'){
                                if(checkRequired(response.Data[i].trackInfo)){
                                    actionTaken = moment(response.Data[i].trackInfo.lastOpenedOn).format('DD-MMM-YYYY');
                                }
                            }
                            else if(response.Data[i].type == 'facebook'){
                                actionTaken = '<table class="links"><tr><td><a id="facebook_like" refId='+response.Data[i].refId+'>Like</a></td><td><a id="facebook_comment" refId='+response.Data[i].refId+'>Comment</a></td><td><a id="facebook_share" refId='+response.Data[i].refId+'>Share</a></td></tr></table><div style="display: none" id='+response.Data[i].refId+'><span id="err-msg-table"></span></div>';
                            }
                            else if(response.Data[i].type == 'document-share'){
                                if(checkRequired(response.Data[i].trackInfo)){
                                    actionTaken = moment(response.Data[i].trackInfo.lastOpenedOn).format('DD-MMM-YYYY');
                                }
                            }
                            else if(response.Data[i].type == 'task'){
                                if(checkRequired(response.Data[i].trackInfo)){
                                    actionTaken = moment(response.Data[i].trackInfo.lastOpenedOn).format('DD-MMM-YYYY');
                                }
                            }

                            var arr = [
                                "<span class='date_sort' date="+response.Data[i].interactionDate+">"+moment(response.Data[i].interactionDate).format('DD-MMM-YYYY')+"</span>",
                                response.Data[i].type || '<span>&nbsp;</span>',
                                response.Data[i].title || '<span>&nbsp;</span>',
                                actionTaken
                            ];
                            addRowsToTable(arr);
                        }
                    }
                }
            }
        })
    }

   $("#send-to-user").on('click',function(){
       var message = $("#message-data").val();
       var subject = $("#subject-data").val();
       if(checkRequired(subject) && checkRequired(message)){
           var dataToSend = {
               receiverEmailId:$("#c-emailId").text(),
               receiverId:$("#c-emailId").attr('_id') || '',
               receiverName:$("#c-name").text(),
               message:message,
               subject:subject
           }
           $.ajax({
               url:"/messages/send/email/single/web",
               type:"POST",
               datatype:"JSON",
               data:dataToSend,
               success:function(response){
                   if(response.SuccessCode == 1){
                       $("#error-msg").text('Message successfully sent')
                   }
                   else $("#error-msg").text('Something went wrong. Please try again');
               }
           })
       }
       else $("#error-msg").text('Please enter both message and subject');
   });

    $("body").on('click','#send-mail',function(){
        var emailId = $("#c-emailId").text();

        if(checkRequired(emailId)){
            $("#send-message-section").show();
        }
    });

    $("body").on('click','#send-mail1',function(){
        var emailId = $("#c-emailId").text();

        if(checkRequired(emailId)){
            $("#send-message-section").show();
        }
    });

    $("body").on('click','#send-mail2',function(){
        var emailId = $("#c-emailId").text();

        if(checkRequired(emailId)){
            $("#send-message-section").show();
        }
    });

    /* SOCIAL FEED ACTIONS */

    $("body").on('click','#twitter_replay',function(){
        var refId = $(this).attr('refId');
        var status = "this is reply text";
        var url = "/social/feed/twitter/tweet/update/web?postId="+refId+"&action=reply&status="+status;
        socialFeedActions(url,refId)
    });

    $("body").on('click','#twitter_favorite',function(){
        var refId = $(this).attr('refId')
        var url = "/social/feed/twitter/tweet/update/web?postId="+refId+"&action=favorite";
        socialFeedActions(url,refId)
    });

    $("body").on('click','#twitter_re_tweet',function(){
        var refId = $(this).attr('refId')
        var url = "/social/feed/twitter/tweet/update/web?postId="+refId+"&action=re-tweet";
        socialFeedActions(url,refId)
    });

     // FB
    $("body").on('click','#facebook_like',function(){
        var refId = $(this).attr('refId')
        if(checkRequired(refId)){
            var id = refId.split('_')[1]
            var url = "/social/feed/facebook/feed/update/web?postId="+id+"&action=like";
            socialFeedActions(url,refId)
        }
    });

    $("body").on('click','#facebook_comment',function(){
        var refId = $(this).attr('refId');

        if(checkRequired(refId)){
            var id = refId.split('_')[1]
            var url = "/social/feed/facebook/feed/update/web?postId="+id+"&action=comment&comment=my first comment from graph API";
            socialFeedActions(url,refId)
        }
    });

    $("body").on('click','#facebook_share',function(){
        var refId = $(this).attr('refId');

        if(checkRequired(refId)){
            var id = refId.split('_')[1]
            var url = "/social/feed/facebook/feed/update/web?postId="+id+"&action=share";
            socialFeedActions(url,refId)
        }
    });

    function socialFeedActions(url,refId){

        //$("#"+refId+" #err-msg-table").text('Error please try again');
        $.ajax({
            url:url,
            type:"GET",
            datatype:"JSON",
            success:function(response){

                if(response.SuccessCode == 1){
                    $("#"+refId).fadeIn('slow');
                    $("#"+refId+" #err-msg-table").text('Action Success.');
                    $("#"+refId+" #err-msg-table").attr('class','msg-succ');
                    $("#"+refId).fadeOut(10000);
                }
                else{
                    $("#"+refId).fadeIn('slow');
                    $("#"+refId+" #err-msg-table").text(response.Message);
                    $("#"+refId+" #err-msg-table").attr('class','msg-err');
                    $("#"+refId).fadeOut(10000);
                }
            }
        })
    }

    function getTextLength(text,maxLength){
        if(!checkRequired(text)){
            return ''
        }
        var textLength = text.length;
        if(textLength >maxLength){
            var formattedText = text.slice(0,maxLength)
            return formattedText+'..';
        }
        else return text;
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }

    function addRowsToTable(rowArr){
        table.row.add( rowArr ).draw();
    }


    function applyDataTable(){
        table = $('#interactions-table').DataTable({
            "dom": '<"top"iflp<"clear">>',
            "order": [[ 0, "desc" ]],
            "columns": [
                { "orderDataType": "dom-value" },
                null,
                null,
                null
            ],
            "oLanguage": {
                "sEmptyTable": "No Interactions Found"
            }
        });
    }

    /* Create an array with the values of all the select options in a column */
    $.fn.dataTable.ext.order['dom-value'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('.date_sort', td).attr("date");
        } );
    };
});