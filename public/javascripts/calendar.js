$(document).ready(function () {
    var loggedinUserProfile, meetingId, timezoneProfile, timeZone, eventId, title, mFlag = false, popoverContent = null;
    loadLoggedinUserProfile();
    $.ajaxSetup({cache: false});

    function closePopOver() {
        if (popoverContent != null) {
            popoverContent.popover('destroy');
            popoverContent = null;
        }
    }

    var tz = jstz.determine();
    timeZone = tz.name();

    $("body").on("click", "#event-delete", function (e) {
        var eventIdIn = $(this).attr('event_id');
        var sender = $(this).attr('sender');
        var selfCalendar = $(this).attr("self_calendar");
        closePopOver();
        if($(this).attr('type') == 'event'){
            deleteEvent($(this), eventIdIn)
        }else
        if (sender == true || sender == 'true') {
            deleteMeeting($(this), eventIdIn, sender, selfCalendar)
        }
        else {
            cancelMeeting($(this), eventIdIn, sender, selfCalendar)
        }
    });

    function deleteMeeting(element, eventIdIn, sender, selfCalendar) {
        var html = _.template($("#delete-meeting-popup").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });
        popoverContent = element;
        element.popover('show');
        $(".popover").css({'width': '20%'});
        $("#delete-meeting-but").attr("event_id_delete", eventIdIn);
    }

    function cancelMeeting(element, eventIdIn, sender, selfCalendar) {

        var html = _.template($("#cancel-meeting-popup").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });

        popoverContent = element;
        element.popover('show');

        $(".popover").css({'width': '40%'})
        $("#cancel-meeting-but").attr("event_id_cancel", eventIdIn);
        $("#cancel-meeting-but").attr("self_calendar", selfCalendar);
    }

    function deleteEvent(element, eventIdIn) {
        var html = _.template($("#delete-event-popup").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });
        popoverContent = element;
        element.popover('show');
        $(".popover").css({'width': '20%'});
        $("#delete-event-but").attr("event_id_delete", eventIdIn);
    }

    $("body").on("click", "#delete-event-but", function (e) {
        var id = $(this).attr("event_id_delete");
        closePopOver();
        $.ajax({
            url:'/event/removeFromCalendar/'+id,
            type:'GET',
            datatype:'JSON',
            success:function(response){

                if(response){
                    calendar.fullCalendar('removeEvents', id);
                    showMessagePopUp("Event has been successfully deleted from your calendar.",'success');
                }
                else{
                    showMessagePopUp("An error occurred while removing event. Please try again.",'error');
                }
            }
        })
    });

    $("body").on("click", "#delete-meeting-but", function (e) {
        var id = $(this).attr("event_id_delete");
        closePopOver();
        $.ajax({
            url: "/meetings/action/delete?mId=" + id+"&timezone="+getTimezone(),
            type: "GET",
            success: function (response) {
                closePopOver();
                if (response) {
                    showMessagePopUp("Meeting successfully deleted. Message sent to all participants.", 'success');
                    calendar.fullCalendar('removeEvents', id);
                }
                else {
                    showMessagePopUp("An error occurred. Please try again", 'error');
                }
            }
        })
    });

    $("body").on("click", "#cancel-meeting-but", function (e) {
        var obj = {
            invitationId: $(this).attr("event_id_cancel"),
            message: $("#cancel-meeting-message").val(),
            selfCalendar: $(this).attr("self_calendar"),
            timezone:getTimezone()
        };

        closePopOver();
        $.ajax({
            url: "/meetings/action/cancel",
            type: "POST",
            datatype: "JSON",
            data: obj,
            success: function (response) {
                closePopOver();
                if (response) {
                    updateEventToCancel(obj.invitationId);
                    showMessagePopUp("Your cancellation of meeting success.", 'success');
                }
                else {
                    showMessagePopUp("An error occurred. Please try again", 'error');
                }
            }
        })
    });

    function updateEventToCancel(invitationId){
        var event = calendar.fullCalendar( 'clientEvents', invitationId );
        if(validateRequired(event) && event.length > 0){
            event[0].title = 'CANCELED';
            event[0].deleteBut = false;
            event[0].meeting = false;
            delete event[0].mUrl;

            calendar.fullCalendar('updateEvent', event[0]);
        }
    }

    $("body").on("click", ".fc-event-title ", function (e) {
        if (validateRequired($(this).attr('mUrl'))) {
            window.location.replace($(this).attr('mUrl'))
        }
    });

    var calendar = $('#myCalendar').fullCalendar({
        height: 400,
        width: 500,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },

        selectable: true,
        selectHelper: true,
        editable: true,
        buttonIcons: true,
        handleWindowResize: true,
        /*viewRender: function(view, element) {
         console.log(view)
         console.log(element.find('.fc-agenda-slots tbody').children());
         },*/
        eventAfterRender: function (event, element, view) {

            if (event.deleteBut) {

                var html = '<span type='+event.type+' title='+event.spanTitle.replace(/\s/g, '&nbsp;')+' self_calendar=' + event.selfCalendar + ' event_id=' + event._id + ' sender=' + event.sender + ' id="event-delete" style="cursor:pointer;margin-top:' + event.marginTop + ';float:right;color: rgb(51, 51, 51);font-size: initial;">x</span>'

                $(element[0]).children().append(html);
            }
            if (!event.meeting) {
                $($($(element[0]).children()[0]).children()[1]).addClass('no-cursor')
            }
            else {
                $($($(element[0]).children()[0]).children()[1]).attr('mUrl', event.mUrl);
            }
        },
        dayClick: function (date, allDay, jsEvent, view) {
            $(".popover").remove();
            $('#pop-up').hide();
        }
    });

    $("body").on("click", "#event-info-closePopup", function () {
        $(".popover").remove();
    });

    $("body").on("click", "#event-info-showDetails", function () {
        $(".popover").remove();

        if (meetingId) {
            window.location.assign('/meeting/' + meetingId);
        } else if (eventId) {
            window.location.assign('/event/' + eventId);
        }
    });

    function getEventColor(id,defaultColor){
        if(id.substr(0, 5) == 'relat'){
            return '#d09054';
        }else if(id.substr(0, 6) == 'todoid'){
            return '#7d96b3';
        }else return defaultColor;
    }

    function getEvents() {  // starting of function getEvents
        $.ajax({
            url: '/calendarEvents',
            type: 'GET',
            datatype: "json",
            traditional: true,
            success: function (events) {

                if (events == false) {

                }
                else {
                    var eventSource = [];
                    for (var i in events) {

                        var localStart = new Date(events[i].start.dateTime || events[i].start.date).toISOString();
                        var localEnd = new Date(events[i].end.dateTime || events[i].end.date).toISOString();

                        if (events[i].id.substr(0, 12) != 'relatasevent') {
                            var event = {
                                id: events[i].id,
                                title: events[i].summary || 'No title',
                                start: new Date(localStart),
                                end: new Date(localEnd),
                                description: events[i].description,
                                editable: false,
                                deleteBut: false,
                                location: events[i].location,
                                color:getEventColor( events[i].id,events[i].color),
                                allDay: events[i].start.dateTime ? false : true

                            };

                            if (events[i].id.substr(0, 5) == 'relat') {
                                event.mUrl = '/meeting/' + events[i].id.substr(7, events[i].id.length)
                                event.meeting = true;
                            } else if(events[i].id.substr(0, 6) == 'todoid'){
                                var a = events[i].id.split('meetingid');
                                event.mUrl = '/meeting/' + a[a.length-1]
                                event.meeting = true;
                            }else event.meeting = false;

                            eventSource.push(event);
                        }
                        else if(events[i].id.substr(0, 12) == 'relatasevent'){
                            var t = events[i].summary || 'No title';
                            var event2 = {
                                id: events[i].id.substr(12, events[i].id.length),
                                title: t,
                                start: new Date(localStart),
                                end: new Date(localEnd),
                                description: events[i].description,
                                editable: false,
                                deleteBut: true,
                                marginTop: t.length > 25 ? '-15%' : '-5%',
                                spanTitle:'Delete Event?',
                                type:'event',
                                location: events[i].location,
                                color: getEventColor( events[i].id,events[i].color),
                                allDay: events[i].start.dateTime ? false : true

                            };

                            if (events[i].id.substr(0, 5) == 'relat') {
                                event2.mUrl = '/event/' + event2.id;
                                event2.meeting = true;
                                event2.isEvent = true;
                            } else event2.isEvent = false;

                            eventSource.push(event2);
                        }
                    }
                    calendar.fullCalendar('addEventSource', eventSource);
                    sidebarHeight();
                }
            },
            error: function (event, request, settings) {

            }
        });
    }

    function isAcceptedMeeting(meeting){
        var accepted = false;
        for (var j = 0; j < meeting.scheduleTimeSlots.length; j++) {
            if(meeting.scheduleTimeSlots[j].isAccepted){
                accepted = true;
            }
        }
        return accepted;
    }

    function newInvitationsAsSender() {
        $.ajax({
            url: '/newInvitationsAsSender',
            type: 'GET',
            datatype: "json",
            traditional: true,
            success: function (invitations) {

                if (invitations) {
                    if (invitations.length > 0) {
                        invitations = removeDuplicates_id(invitations)
                        var eventSource = [];
                        for (var i = 0; i < invitations.length; i++) {
                            if (invitations[i].deleted || invitations[i].status == 'reScheduled') {

                            } else if(!isAcceptedMeeting(invitations[i])){

                                for (var j = 0; j < invitations[i].scheduleTimeSlots.length; j++) {
                                    var canceled = false;

                                    if(invitations[i].selfCalendar){
                                        if(invitations[i].toList.length == 1 && invitations[i].toList[0].canceled){
                                            canceled = true;
                                        }
                                        else{
                                            var totalParticipants = invitations[i].toList.length;
                                            var numberOfParticipantsCanceled = 0;
                                            for (var toList = 0; toList < totalParticipants; toList++) {
                                                if (invitations[i].toList[toList].canceled) {
                                                    numberOfParticipantsCanceled++;
                                                }
                                            }
                                            if(numberOfParticipantsCanceled == totalParticipants){
                                                canceled = true;
                                            }
                                        }
                                    }
                                    else{
                                        if(invitations[i].to.canceled == true){
                                            canceled = true;
                                        }
                                    }
                                    var event = {
                                        id: invitations[i].invitationId,
                                        title: '( '+(canceled ? 'CANCELLED' : 'BOOKED')+' ) ' + invitations[i].scheduleTimeSlots[j].title,
                                        invitationTitle: invitations[i].scheduleTimeSlots[j].title,
                                        location: invitations[i].scheduleTimeSlots[j].location,
                                        locationType: invitations[i].scheduleTimeSlots[j].locationType,
                                        description: invitations[i].scheduleTimeSlots[j].description,
                                        start: new Date(invitations[i].scheduleTimeSlots[j].start.date),
                                        end: new Date(invitations[i].scheduleTimeSlots[j].end.date),
                                        invitees: invitations[i].participants,
                                        color: '#d09054',
                                        isBooked: true,
                                        editable: false,
                                        deleteBut: true,
                                        meeting: !canceled,
                                        marginTop: invitations[i].scheduleTimeSlots[j].title.length > 8 ? '-15%' : '-5%',
                                        document: invitations[i].docs,
                                        isSuggested: invitations[i].suggested,
                                        allDay: false,
                                        selfCalendar: invitations[i].selfCalendar == true,
                                        sender: invitations[i].senderId == loggedinUserProfile._id,
                                        spanTitle:'Delete Meeting?'
                                    };
                                    if(!canceled){
                                        event.mUrl = '/meeting/' + invitations[i].invitationId;
                                    }
                                    eventSource.push(event);

                                }
                            }
                        }
                        calendar.fullCalendar('addEventSource', eventSource);
                    }
                }
            }
        })
    }

    function removeDuplicates_id(arr) {

        var end = arr.length;

        for (var i = 0; i < end; i++) {
            for (var j = i + 1; j < end; j++) {
                if (arr[i]._id == arr[j]._id) {
                    var shiftLeft = j;
                    for (var k = j + 1; k < end; k++, shiftLeft++) {
                        arr[shiftLeft] = arr[k];
                    }
                    end--;
                    j--;
                }
            }
        }

        var whitelist = [];
        for (var l = 0; l < end; l++) {
            whitelist[l] = arr[l];
        }

        return whitelist;
    }

// function to get new invitations
    function getNewInvitations() {
        var eventSource = [];
        $.ajax({
            url: '/newInvitationsAsReceiver',
            type: 'GET',
            datatype: "json",
            traditional: true,

            success: function (invitations) {
                if (invitations.length > 0) {
                    invitations = removeDuplicates_id(invitations)
                    for (var i = 0; i < invitations.length; i++) {
                        if (invitations[i].deleted || invitations[i].status == 'reScheduled') {

                        } else {
                            var eventColor = '#d09054';  //'#808080';
                            var senderName = invitations[i].senderName;
                            var allDays = false;
                            var invitationId = invitations[i].invitationId;
                            var senderId = invitations[i].senderId;
                            var senderPicUrl = invitations[i].senderPicUrl;
                            var timeSlots = invitations[i].scheduleTimeSlots;
                            var senderEmailId = invitations[i].senderEmailId;
                            var receiverEmailId = '';
                            var receiverName = '';
                            var receiverId = '';
                            var canceled = false;
                            if (invitations[i].selfCalendar) {

                                for (var k = 0; k < invitations[i].toList.length; k++) {

                                    if (invitations[i].toList[k].receiverId == loggedinUserProfile._id) {

                                        receiverEmailId = invitations[i].toList[k].receiverEmailId || '';
                                        receiverName = invitations[i].toList[k].receiverName || '';
                                        receiverId = invitations[i].toList[k].receiverId || '';

                                        if(invitations[i].toList[k].canceled){
                                            canceled = true;
                                        }
                                    }
                                }
                            }
                            else {
                                receiverEmailId = invitations[i].to.receiverEmailId;
                                receiverName = invitations[i].to.receiverName;
                                receiverId = invitations[i].to.receiverId;
                                if(invitations[i].to.canceled){
                                    canceled = true;
                                }
                            }

                            var documentName;
                            var documentUrl;
                            if (validateRequired(invitations[i].docs)) {
                                if (validateRequired(invitations[i].docs.documentName)) {
                                    documentName = invitations[i].docs.documentName;
                                    documentUrl = invitations[i].docs.documentUrl;
                                }
                            }
                            for (var j = 0; j < invitations[i].scheduleTimeSlots.length; j++) {

                                var event = {
                                    id: invitationId,
                                    title: canceled ? 'CANCELED' : 'NEW REQUEST',
                                    invitationTitle: invitations[i].scheduleTimeSlots[j].title,
                                    location: invitations[i].scheduleTimeSlots[j].location,
                                    locationType: invitations[i].scheduleTimeSlots[j].locationType,
                                    description: invitations[i].scheduleTimeSlots[j].description,
                                    start: new Date(invitations[i].scheduleTimeSlots[j].start.date),
                                    end: new Date(invitations[i].scheduleTimeSlots[j].end.date),
                                    invitees: invitations[i].participants,
                                    color: eventColor,
                                    timeSlots: timeSlots,
                                    isMeeting: true,
                                    senderId: senderId,
                                    name: senderName,
                                    editable: false,
                                    deleteBut: !canceled ,
                                    marginTop: '-5%',
                                    meeting: !canceled,
                                    senderPicUrl: senderPicUrl,
                                    senderEmailId: senderEmailId,
                                    receiverId: receiverId,
                                    receiverEmailId: receiverEmailId,
                                    receiverName: receiverName,
                                    document: invitations[i].docs,
                                    isSuggested: invitations[i].suggested,
                                    selfCalendar: invitations[i].selfCalendar == true,
                                    sender: invitations[i].senderId == loggedinUserProfile._id,
                                    allDay: allDays,
                                    spanTitle:'Cancel Meeting?'
                                };
                                if(!canceled){
                                    event.mUrl = '/meeting/' + invitations[i].invitationId;
                                }

                                eventSource.push(event);
                            }
                        }
                    }
                    calendar.fullCalendar('addEventSource', eventSource);
                }
            }
        })
    }

// Function to load loggedin user profile
    function loadLoggedinUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,

            success: function (userData) {
                if (userData == null) {
                } else {

                    loggedinUserProfile = userData;
                    if(validateRequired(loggedinUserProfile) && validateRequired(loggedinUserProfile.timezone) && validateRequired(loggedinUserProfile.timezone.name)){
                        timezoneProfile = loggedinUserProfile.timezone.name;
                    }
                    getEvents();
                    getNewInvitations();
                    newInvitationsAsSender();
                    $('#title').html(userData.firstName);
                    $('.user-msg-name').text(userData.firstName);
                    $('#loggedin-user-name').text(userData.firstName + " " + userData.lastName);
                    $('#loggedin-user-designation').text(userData.designation);
                    $('#loggedin-user-companyName').text(userData.companyName);
                    $('#profilePic').attr('src', userData.profilePicUrl || '/images/default.png');
                    imagesLoaded("#profilePic", function (instance, img) {
                        if (instance.hasAnyBroken) {
                            $('#profilePic').attr("src", "/images/default.png");
                        }
                    });
                }
            },
            error: function (event, request, settings) {
                showMessagePopUp("Error while requesting loggedin user data", 'error')

            },
            timeout: 20000
        });
    }

    function showMessagePopUp(message, msgClass) {
        var html = _.template($("#message-popup").html())();
        $("#user-msg").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-msg").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top': '9%'});

        if (msgClass == 'error') {
            $(".popover").addClass('popupError');
            $("#popupTitle").text('ERROR')
        } else if (msgClass == 'success') {
            $(".popover").addClass('popupSuccess');
            $("#popupTitle").text('SUCCESS')
        } else if (msgClass == 'tip') {
            $(".popover").addClass('popupTip');
            $("#popupTitle").text('TIP')
        }
        $("#message").text(message)
    }

// Function to validate one variable is empty or not
    function validateRequired(fieldData) {
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }

    function sidebarHeight() {
        var sideHeight = $(document).height();
        $("#sidebarHeight").css({'min-height': sideHeight})
    }

    $("body").on("click", "#showMeetingDetails", function () {
        $(".popover").remove();

        if (meetingId) {
            window.location.assign('/meeting/' + meetingId);
        } else if (eventId) {
            window.location.assign('/event/' + eventId);
        }
    });

    $("body").on("click", "#closePopup", function () {
        $(".popover").remove();

    });

    $("body").on("click", "#closePopup-message", function () {
        $("#user-msg").popover('destroy');
        if (mFlag) {
            window.location.reload();
        }
    });

    $("body").on("click", "#closePopup-delete-meeting", function (e) {
        closePopOver();
    });

    $("body").on("click", "#closePopup-delete-event", function (e) {
        closePopOver();
    });

    $("body").on("click", "#closePopup-cancel-meeting", function (e) {
        closePopOver();
    });

    function getTimezone(){
        return timezoneProfile || timeZone || 'UTC';
    }

});