var relatasApp = angular.module('relatasApp', ["ngLodash"]).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

function Bridge() {
    
}

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        setMeeting:function(m){
            this.meetingOb = m;
            this.isOk = true;
        },
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        getUserId:function(){
            return this.userId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setCompanyId:function(companyId){
            this.companyId = companyId;
        },
        getCompanyId:function(){
            return this.companyId;
        }
    };
});

var rootSharedUserObjId;
relatasApp.controller("document_renderer_wrapper", function($scope, $http, share, $rootScope) {

    var docId = getParams(window.location.href).docId;
    var sharedUserObjId = getParams(window.location.href).sharedId || "";
    
    sessionStorage.setItem("documentId", docId);
    sessionStorage.setItem("sharedUserObjId", sharedUserObjId);

    function getDocument() {
        $http.get('/load/document/'+docId+'/'+sharedUserObjId) 
            .success(function(response) {
                $scope.parseResponse(response);
        })
    }

    $scope.parseResponse = function(response) {
        var isDocumementOwnerView;
        var response;
        $scope.documentLinkExpired = false;

        if(response.SuccessCode) {
            $rootScope.isOwner = response.isOwner;
            $scope.document = response.Data;
            isDocumementOwnerView = response.isOwner;
            sessionStorage.setItem("document", JSON.stringify($scope.document));
            sessionStorage.setItem("isOwner", response.isOwner);

            if(isDocumementOwnerView) {
                $scope.passWordAuthenticated = true;
                window.location = '/view/shared/document';
                // share.displayDocument($scope.document, $scope.document.documentUrl);

            } else {

                response = isDocumentValid($scope.document)
                if(response.isValid) {
                    $scope.passWordAuthenticated = false;
                    $rootScope.sharedUserObjId = $scope.document.sharedWith[0]._id;
                    rootSharedUserObjId = $rootScope.sharedUserObjId
                    share.getSharedUserDetails($scope.document);

                } else {
                    $scope.documentLinkExpired = true;
                    share.showReason(response.reason);
                }
            }
        } else {
            $scope.documentLinkExpired = true;
            share.showReason("The document has been recalled by the sender");
        }
    }

    share.displayDocumentWindow = function() {
        // $scope.documentLinkExpired = false;
        // $scope.passWordAuthenticated = true;
        window.location = '/view/shared/document';
    }

    getDocument();
});

relatasApp.controller("user_authentication_controller", function($scope, $http, share, $rootScope) {

    share.getSharedUserDetails = function(document) {
        $scope.document = document;

        var docExpiryDate = document.sharedWith[0].expiryDate;
        

        var obj = {
            documentName: document.documentName,
            companyName: document.sharedBy.companyName ? document.sharedBy.companyName : 
                                                fetchCompanyFromEmail(document.sharedBy.emailId),
            firstName: document.sharedBy.firstName || "",
            lastName: document.sharedBy.lastName || "",
            profilePicUrl: document.sharedBy.profilePicUrl ? document.sharedBy.profilePicUrl : '/images/default.png',
            sharedOn: moment(document.sharedWith[0].sharedOn).format("DD MMM YYYY"),
            expiryDate: docExpiryDate ?  moment(docExpiryDate).format("DD MMM YYYY") : "                                  -",
        };
        
        obj.companyImageUrl =  document.sharedBy.companyLogoUrl || "";
        obj.header = "Please review the " + obj.documentName + " shared by " + obj.companyName + " user.";
        obj.personName = obj.firstName + ' ' + obj.lastName;
        
        obj.companyName = obj.companyName.toUpperCase();
        $scope.docOwnerMetaData = obj;
    }

    $scope.verifyPassword = function(password) {
        password = password || "";
        
        if(password === $scope.document.sharedWith[0].documentPassword) {
            updateDocumentAccessCount($http, $scope, $rootScope);
            share.displayDocumentWindow();
            // share.displayDocument($scope.document, $scope.document.documentUrl);
            
        } else{
            $scope.showErrorMsg = true;

            if(password.length == 0)
                $scope.errorMessage = "Please enter the security code"
            else
                $scope.errorMessage = "Security code verification failed. Please try again";
        }
    }
});

relatasApp.controller("document_not_accessible_controller", function($scope, $http, share, $rootScope) {
    share.showReason = function(reason) {
        $scope.reason = reason;
    }
});

var docViewScope;
relatasApp.controller("document_view_controller", function($scope, $http, share, $rootScope) {
    var MARGIN_BETWEEN_PAGES = 10;

    share.displayDocument = function(pdfDocument, documentUrl) {
        $scope.document = pdfDocument;
        $scope.documentName = $scope.document.documentName;
        $scope.isOwner = $rootScope.isOwner;
        $scope.loadingDocuments = true;
        docViewScope = $scope;

        $(".progress-container").css({display: 'block'});
        $("#pdf-controllers").hide();

        renderPDF(documentUrl, document.getElementById('canvas_container'));


        
        /* var loadPdf = PDFJS.getDocument({url: documentUrl}, false, null, function(progress) {
            $(".progress-pdf-load").val(progress.loaded / progress.total * 100);
        });

        loadPdf.promise.then(function(pdf) {
            $scope.loadingDocuments = false;

            $(".progress-container").css({display: 'none'});
            $("#pageselector").val(1)
            $("#pagelength").text(pdf.numPages)
            $scope.pdf = pdf;
            $scope.totalDocumentPages = pdf.numPages;
            $scope.currentPage = 1;
            $scope.currentScale = 1.5;

            $("#pdf-controllers").show();
            renderPage($scope, $scope.pdf, $scope.currentPage, $scope.currentScale, true, true);
        },
        function(reason) {
            console.log("Reason:", reason);
        }) */
    }

    $scope.zoomIn = function() {
        $scope.currentScale += 0.05;
        // renderPage($scope, $scope.pdf, $scope.currentPage, $scope.currentScale, false);
        renderPage($scope.pdf, $scope.currentScale);
    }

    $scope.zoomOut = function() {
        if($scope.currentScale > 0.05) {
            $scope.currentScale -= 0.05;
            // renderPage($scope, $scope.pdf, $scope.currentPage, $scope.currentScale, false);
            renderPage($scope.pdf, $scope.currentScale);
        }
    }

    Bridge.prototype.updateAnalytics = function() {
        $scope.updateReadStatusOfDocument({
            "pageNumber": 2,
            pageReadDuration: 234,
        })
    }

    $scope.updateReadStatusOfDocument = function(readPageData) {
        
        console.log("send analytics")

        /* if(!$scope.isDocumementOwnerView) {
            var data = {
                pageNumberReaded: readPageData.pageNumber,
                pageReadDiff: readPageData.pageReadDuration,
                firstAccessed: readPageData.firstAccessed,
                lastAccessed: readPageData.lastAccessed,
                documentId: $scope.document._id,
                sharedUserObjId: $rootScope.sharedUserObjId,
                lUserId: $scope.document.sharedBy.userId,
                count: true
            };


            $http.post('/update/document/page/read/status', {data})
                .success(function(response) {
                });
        }*/
    } 

    function renderPDF(docUrl, canvasContainer, options) {
        $scope.currentScale = 1.5;
        options = options || { scale: $scope.currentScale };
        $scope.pageSpaceMap = {};
        $scope.currentPageStatus = {};
        
        function renderPage(page) {
            var canvasContainer = document.getElementById('canvas_container');
            var viewport;;
            var wrapper = document.createElement("div");
            wrapper.className = "canvas-wrapper";
            var canvas = document.createElement('canvas');

            canvas.setAttribute("id", "page-" + page.pageNumber);
            canvas.mozOpaque = true;

            var ctx = canvas.getContext('2d', {
                alpha: false
            });

            var outputScale = getOutputScale(ctx);

            if(outputScale.scaled)
                ctx.scale(outputScale.sx, outputScale.sy);
            
            if(page.getViewport(1).width > $(window).width()) {
                $scope.currentScale = 0.50;
                viewport = page.getViewport($scope.currentScale);
                canvas.width = viewport.width;   
                canvas.height = viewport.height;

            } else {
                $scope.currentScale = 1.5;
                viewport = page.getViewport($scope.currentScale);
                canvas.width = viewport.width;
                canvas.height = viewport.height;
            }
    
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };

            if(page.pageNumber > 1)
                $scope.pageSpaceMap[page.pageNumber] = $scope.pageSpaceMap[page.pageNumber-1] + canvas.height + MARGIN_BETWEEN_PAGES;
            else {
                $scope.pageSpaceMap[page.pageNumber] = canvas.height + MARGIN_BETWEEN_PAGES;
                $scope.currentPageStatus.pageNumber = 1;
            }

            wrapper.appendChild(canvas);

            canvasContainer.appendChild(wrapper);
            page.render(renderContext).then(function() {
                if(page.pageNumber === 1)
                    $scope.currentPageStatus.firstAccessed = new Date();
            });
        }

        function renderPages(pdfDoc) {
            $(".progress-container").css({display: 'none'});
            $("#pageselector").val(1);
            $("#pagelength").text(pdfDoc.numPages);
            $scope.pdf = pdfDoc;
            
            for(var num = 1; num <= pdfDoc.numPages; num++) {
                pdfDoc.getPage(num).then(renderPage);
            }
        }
    
        PDFJS.disableWorker = true;
        PDFJS.getDocument({url: docUrl}, false, null, function(progress) {
            $(".progress-pdf-load").val(progress.loaded / progress.total * 100);
        }).then(renderPages);
    }   
});

$(window).unload(function () {

    if(docViewScope.currentPageStatus && docViewScope.currentPageStatus.firstAccessed) {
        currentPageStatus = docViewScope.currentPageStatus;
        currentPageStatus.lastAccessed = new Date();
        currentPageStatus.pageReadDuration = (currentPageStatus.lastAccessed.valueOf() - currentPageStatus.firstAccessed.valueOf())/1000;

        if(currentPageStatus.pageReadDuration >= 1) {
            if(!docViewScope.isDocumementOwnerView) {
                var data = {
                    pageNumberReaded: currentPageStatus.pageNumber,
                    pageReadDiff: currentPageStatus.pageReadDuration,
                    firstAccessed: currentPageStatus.firstAccessed,
                    lastAccessed: currentPageStatus.lastAccessed,
                    documentId: docViewScope.document._id,
                    sharedUserObjId: rootSharedUserObjId,
                    lUserId: docViewScope.document.sharedBy.userId,
                    count: true
                };

                $.ajax({
                    type: 'POST',
                    datatype: 'JSON',
                    data: {data},
                    async: false,
                    url: '/update/document/page/read/status',
                    success: function (result) {
                    }
                });
                
            }
        }
    }
});

function isDocumentValid(document) {
    var responseObj = {};

    responseObj.isValid = true;
    var docExpiryDate = document.sharedWith[0].expiryDate;

    if(document.sharedWith.length > 0) {
        if(!document.sharedWith[0].accessStatus) {
            responseObj.isValid = false;
            responseObj.reason = "Sharing of the document has been stopped by the sender."
        }
    
        if(docExpiryDate) {
            var diff = moment(docExpiryDate).diff(moment(), 'days');
            if(diff < 0) {
                responseObj.isValid = false;
                responseObj.reason = "Document link expired."
            }
        }
    } else {
        responseObj.isValid = false;
        responseObj.reason = "The document is not accessible at this time."
    }
    

    return responseObj;
}

function updateDocumentAccessCount($http, $scope, $rootScope) {
    var data = {
        docId: $scope.document._id,
        sharedUserObjId: $rootScope.sharedUserObjId,
    };

    $http.post('/update/document/access/count', data)
        .success(function(response) {
        });
}

function renderPage(pdfDoc, scale) {
    for(var num = 1; num <= pdfDoc.numPages; num++) {

        pdfDoc.getPage(num).then(function(page) {
            var viewport = page.getViewport(scale);
            var canvas = document.getElementById("page-" + page.pageNumber);

            canvas.mozOpaque = true;

            var ctx = canvas.getContext('2d', {
                alpha: false
            });

            var outputScale = getOutputScale(ctx);

            if(outputScale.scaled)
                ctx.scale(outputScale.sx, outputScale.sy);
            
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };

            canvas.height = viewport.height;
            canvas.width = viewport.width;

            /* canvas.width = canvas.width * outputScale.sx | 0;
            canvas.height = canvas.height * outputScale.sy | 0;
 */
            page.render(renderContext).then(function() {
            });
        });
    }
}

/* function renderPage(scope, pdf, pageNumber, scale, updateAnalytics, initialRendering) {
    if(!scope.isOwner && updateAnalytics)
        updatePageReadAnalytics(scope);

    pdf.getPage(pageNumber).then(function(page) {
        scope.currentPageStatus = {};
        scope.currentPageStatus.pageNumber = pageNumber;

        var canvas = $('#document_holder').get(0);
        var dpr = window.devicePixelRatio || 1;
        
        var scaleRequired = scale;
        var viewport;

        if(initialRendering) {
            if(page.getViewport(1).width > $(window).width()) {
                // canvas.width = $(window).width() - 50;
                // scaleRequired = canvas.width / page.getViewport(1).width;
                scaleRequired = 0.50;
                viewport = page.getViewport(scaleRequired);
                canvas.width = viewport.width;   
                canvas.height = viewport.height;

            } else {
                scaleRequired = 1.5;
                viewport = page.getViewport(scaleRequired);
                canvas.width = viewport.width;
                canvas.height = viewport.height;
            }

            scope.currentScale = scaleRequired;
        } else {
            viewport = page.getViewport(scale);
            canvas.width = viewport.width;
            canvas.height = viewport.height;
        }

        var context = canvas.getContext("2d");

        var renderContext = {
            canvasContext: context,
            viewport: viewport
        };

        page.render(renderContext).then(function() {
            scope.currentPageStatus.firstAccessed = new Date();
        });
    });
} */

function updatePageReadAnalytics(scope, currentPageStatus) {
    // var currentPageStatus = {};

    if(currentPageStatus && currentPageStatus.firstAccessed) {
        currentPageStatus.lastAccessed = new Date();
        currentPageStatus.pageReadDuration = (currentPageStatus.lastAccessed.valueOf() - currentPageStatus.firstAccessed.valueOf())/1000;

        if(currentPageStatus.pageReadDuration >= 1) {
            scope.updateReadStatusOfDocument(currentPageStatus);
        }
    }
}

function getOutputScale(ctx) {
    var devicePixelRatio = window.devicePixelRatio || 1;
    var backingStoreRatio = ctx.webkitBackingStorePixelRatio || ctx.mozBackingStorePixelRatio || ctx.msBackingStorePixelRatio || ctx.oBackingStorePixelRatio || ctx.backingStorePixelRatio || 1;
    var pixelRatio = devicePixelRatio / backingStoreRatio;
    return {
        sx: pixelRatio,
        sy: pixelRatio,
        scaled: pixelRatio !== 1
    };
}

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

$(function() {
    $(document).on('scroll', _.debounce(function() {
        var scope = angular.element(document.getElementById('document_view')).scope();
        var scrollTop = $(document).scrollTop();
        var percentScrolled;

        for(var page in scope.pageSpaceMap) {
            if(scope.pageSpaceMap[page] > scrollTop) {
                percentScrolled = (scrollTop * 100)/scope.pageSpaceMap[page]
                
                if(percentScrolled > 80) {
                    scope.currentPage = parseInt(page) + 1;
                    
                    if(scope.currentPage != scope.currentPageStatus.pageNumber) {
                        updatePageReadAnalytics(scope, {"firstAccessed": scope.currentPageStatus.firstAccessed, "pageNumber": scope.currentPageStatus.pageNumber})
                        scope.currentPageStatus.firstAccessed = new Date();
                        scope.$apply(function () {
                            scope.currentPageStatus.pageNumber = scope.currentPage
                        });
                    }

                } else {
                    scope.currentPage = parseInt(page);
                    if(scope.currentPage != scope.currentPageStatus.pageNumber) {
                        updatePageReadAnalytics(scope, {"firstAccessed": scope.currentPageStatus.firstAccessed, "pageNumber": scope.currentPageStatus.pageNumber})
                        scope.currentPageStatus.firstAccessed = new Date();
                        scope.$apply(function () {
                            scope.currentPageStatus.pageNumber = scope.currentPage
                        });
                    }
                }
                break;
            }
        }
    }, 200));
})