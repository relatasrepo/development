var s3bucket, docUrl;

var documentId = getParams(window.location.href).docId;
var sharedWithObjId = getParams(window.location.href).sharedId;
var documentAction = getParams(window.location.href).action;
var opportunityId = getParams(window.location.href).opportunityId;

var notificationCategory = getParams(window.location.href).notifyCategory;
var notificationDate = getParams(window.location.href).notifyDate;

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("wrapper_controller", function($scope, $http, share, $rootScope) {
    
    share.loadRightPanel = function(index){
        $scope.menu = menuItems();
        $scope.viewFor = $scope.menu[index].name;

        menuToggleSelection($scope.viewFor, $scope.menu);
        getAWSCredentials(share, $http);
    }

    $scope.openView = function(viewFor) {
        mixpanelTracker("Documents>section "+viewFor);
        $scope.viewFor = viewFor.name;
        menuToggleSelection($scope.viewFor, $scope.menu);
        loadView();
    }

    function loadView() {
        if($scope.viewFor == 'Create')
            console.log("");
        else if($scope.viewFor == 'Upload + Analytics') {

            if(share.uploadedDocuments)
                share.loadMyDocuments()
            else 
                share.getDocuments(function() {
                    share.loadMyDocuments();
                });
        }
        else if($scope.viewFor == 'Analytics') {

            if(share.uploadedDocuments)
                share.getSharedDocuments();
            else 
                share.getDocuments(function() {
                    share.getSharedDocuments();;
                });
        }
    }

    if(documentAction == "createDocument") {
        share.loadRightPanel(1);
    } else {
        share.loadRightPanel(0);
    }

    share.sideBarVisibility = function(visible) {
        if(!visible)
            $scope.sideBarZindex = {"z-index": 9999};
        else 
            $scope.sideBarZindex = {};
    }

    share.showFullPageView = function(boolVal) {
        $scope.fullPageView = boolVal; 
    }

});

relatasApp.controller("upload_controller", function($rootScope,$scope, share, $http, $timeout, searchService) {
    $rootScope.toolBarList = [
        ['bold','italics'],
        ['ul','ol'],
        ['indent','outdent'],
        // ['justifyLeft','justifyCenter','justifyRight','justifyFull']
    ]

    $scope.initAutocomplete = function(){

        checkDOMLoaded();

        function checkDOMLoaded(){
            if(document.getElementById('p_location')){

                var autocomplete2 = new google.maps.places.Autocomplete(
                    (document.getElementById('p_location')),
                    {types:['(cities)']});
                autocomplete2.addListener('place_changed', function(err,places){

                    $scope.coords = {
                        lat:autocomplete2.getPlace().geometry.location.lat(),
                        lnf:autocomplete2.getPlace().geometry.location.lng()
                    }
                    return false;
                });
            } else {
                setTimeOutCallback(100,function () {
                    checkDOMLoaded();
                })
            }
        }
    }

    $scope.initAutocomplete();

    $scope.orderByField = 'uploadedDate';
    $scope.reverseSort = true;

    $scope.orderSharedWithTableByField = 'sharedDate';
    $scope.sharedWithTableReverseSort = true; 

    $scope.sharedWithTabHeaders = [{
                                    name:"Name",
                                    sortableName:"sharedWithName",
                                    style:"cursor:pointer;"
                                },{
                                    name:"Shared On",
                                    sortableName:"sharedDate",
                                    style:"cursor:pointer;"
                                },{
                                    name:"Expires On",
                                    sortableName:"expiryDate",
                                    style:"cursor:pointer;"
                                },{
                                    name:"Last View",
                                    sortableName:"lastViewDate",
                                    style:"cursor:pointer;"
                                },{
                                    name:"Sharing",
                                    sortableName:""
                                }];

    // $scope.documentCategory = ["Invoice", "Marketing", "Proposal", "Quotation"];

    $scope.headerNames = [{ name: "Document Name",
                            width: "width:39%; cursor:pointer; text-align:left",
                            sortableName: "documentName"},
                        { name: "Category",
                        width: "width:9%; cursor:pointer; text-align:left",
                        sortableName: "documentCategory"},
                        { name: "Date Uploaded",
                            width: "width:0%; cursor:pointer;",
                            sortableName: "uploadedDate" },
                        { name: "Shared",
                            width: "width:7%; cursor:pointer;",
                            sortableName: "sharedWithLength"},
                        { name: "Opened",
                            width: "width:7%; cursor:pointer;",
                            sortableName: "readCount" },
                        { name: "UnOpened",
                            width: "width:8%; cursor:pointer;",
                            sortableName: "unreadCount" },
                        { name: "Analytics",
                            width: "width:0%" },
                        { name: "Share",
                            width: "width:0%" },
                        { name: "Delete",
                            width: "width:0%" }
                        ];
    
    $scope.paintAnalytics = function() {
        if(share.uploadedDocuments)
            share.loadMyDocuments()
        else 
            share.getDocuments(function() {
                share.loadMyDocuments();
            });
    }


    $scope.uploadDocument = function() {
        mixpanelTracker("Documents>upload doc ");
        if($scope.documentCategory.length <= 0) {
            toastr.error("The admin has not created the document categories. Please reach out to your admin");

        }else if($scope.upload.disableUploadBtn) {
            toastr.error("Please select the document category before upload");

        } else {
            $("#selectShareFile").trigger("click");
        }
    }

    share.getDocuments = function(callback) {
        var url = "";

        if(notificationCategory && notificationCategory == "documentSharingStatus" && notificationDate) {
            url = "/get/document/not/accessed?fromDate=" + notificationDate;

            updateNotificationOpenDate($http, {"dayString":notificationDate, "category": notificationCategory}, function(response) {});
            
        } else {
            url = "/get/uploaded/documents";
        }

        $http.get(url)
        .success(function (response) {
            share.uploadedDocuments = response.documents;
            callback();
        });
    }

    checkCompanyLogoUploadStatus($scope, share);

    share.loadMyDocuments = function() {
        $("#doc-upload-progress").css({display: 'none'});
        $scope.myDocuments = parseMyDocuments(share.uploadedDocuments);
        
        if(documentId && sharedWithObjId) {
            var openDocument = _.find($scope.myDocuments, ["documentId", documentId]);

            if(openDocument) {
                $scope.viewAnalytics(openDocument);
            }
        }
    }

    $scope.viewAnalytics = function(document) {
        mixpanelTracker("Documents>view analytics ");
        $scope.documentsSharedByMe = parseSharedByMeDocuments(document);

        $scope.documentsSharedByMe.sort(function(d1, d2) {
            return new Date(d2.sharedDate) - new Date(d1.sharedDate);
        });
        
        $scope.documentMetaData = generateDocumentMetaData($scope, document);
        $scope.showMyDocumentAnalytics = true;

        if($scope.documentsSharedByMe.length > 0) {
            if(sharedWithObjId) {
                openSharedWithUser = _.find($scope.documentsSharedByMe, ["sharedWithObjectId", sharedWithObjId]);

                if(openSharedWithUser)
                    $scope.getAnalytics(openSharedWithUser);
                else
                    $scope.getAnalytics($scope.documentsSharedByMe[0]);
            } else {
                $scope.getAnalytics($scope.documentsSharedByMe[0]);
            }
        }
        // generateTotalDocReadStatusTable($scope, document);
    }

    $scope.getAnalytics = function(document) {

        mixpanelTracker("Documents>view analytics for contact ");
        $scope.analyticsRightSummary = {};
        $scope.analyticsRightSummary["currentSelectedDocument"] = document;

        $scope.selectedDocumentObjId = document.sharedWithObjectId;

        $scope.viewAnalyticsSummary = true;

        if(document.accessed)
            $scope.documentAccessed = true;
        else 
            $scope.documentAccessed = false;

        $scope.sharedWithPersonName = document.firstName + "'s";

        $http.get('/analytics/v2/' + document.documentId + '/' + document.sharedWithObjectId)
            .success(function(response) {

                if(response.SuccessCode)
                    parseAnalyticsData($scope, response.Analytics);
                else 
                    $scope.documentAccessed = false;
            });
    }

    $scope.closeMyDocumentAnalytics = function() {
        $scope.showMyDocumentAnalytics = false;
    }

    $scope.resendDocument = function() {
        mixpanelTracker("Documents>resend doc ");
        $scope.analyticsRightSummary.resend = true;
        var currentDoc = $scope.analyticsRightSummary["currentSelectedDocument"];

        $http.post('/resend/document', {documentId: currentDoc.documentId, sharedWithEmailId: currentDoc.sharedWithEmailId})
                .success(function(response) {
                    $scope.analyticsRightSummary.resend = false;
                    if(response.SuccessCode) {
                        toastr.success("Document resent successfully to " + currentDoc.sharedWithEmailId);
                    } else {
                        toastr.error("Document resending failed for " + currentDoc.sharedWithEmailId);
                    }
                });
    }

    $scope.registerDatePickerId = function (elementId) {
        $('#' + elementId).datetimepicker({
            timepicker: false,
            validateOnBlur: false,
            minDate: new Date(),
            onSelectDate: function (dp, $input) {
                dp = new Date(dp)
                dp.setHours(23)
                dp.setMinutes(59)
                dp.setSeconds(59)
                $(".xdsoft_datetimepicker").hide();
                $scope.$apply(function () {
                    $scope.email.documentExpiryDate = moment(dp).format("DD MMM YYYY"); 
                    /* $scope[elementId] = moment(dp).format("DD MMM YYYY");
                    $('#' + elementId + "-val").val($scope[elementId]); */
                });
            }
        });
    }

    $scope.shareDocument = function(document) {
        mixpanelTracker("Documents>share doc ");
        if(!$scope.companyLogoUploaded) {
            toastr.error("Company Logo has not been uploaded. Please ask the admin to upload the company logo before you share the document")
        } else {
            $scope.sharedDocument = document;
            $scope.emailContent = {};
            $scope.email = {};
            $scope.contactToList = [];
    
            $scope.email.firstName = "";
    
            var l_user = share.liuData;
    
            $scope.liuFullName = l_user.firstName + " " + l_user.lastName;
    
            // var signature = getUserSignature($scope.liuFullName, l_user.designation, l_user.companyName, share.domainName, l_user.publicProfileUrl);
            $("div[id^='taTextElement']").empty();

            $scope.emailContent.emailBody = "<p style='margin:0px;'>Hi "+ $scope.email.firstName + ",</p>" +
                                "<p style='margin:0px;'>Hope you are doing well. Please review the attached document link for " + document.documentName + ". " +
                                "The security code to open the document is in the mail below. Should you have any concerns please do reach out to me.</p><p style='margin:0px;'><br><br></p>" +
                                "<p style='margin:0px;'>Thanks,</p>"+ $scope.liuFullName;

            $("div[id^='taTextElement']").append($.parseHTML($scope.emailContent.emailBody))
    
            $scope.email.emailSubject = document.documentName + " | " + $scope.liuFullName;
            $scope.email.notify = true;
            $scope.openEmailWindow = true;
        }
    }

    $scope.recomposeEmailBody = function(firstName) {
        $("div[id^='taTextElement']").empty();

        $scope.emailContent.emailBody = "<p style='margin:0px;'>Hi "+ firstName + ",</p>" +
                            "<p style='margin:0px;'>Hope you are doing well. Please review the attached document link for " + $scope.sharedDocument.documentName + ". " +
                            "The security code to open the document is in the mail below. Should you have any concerns please do reach out to me.</p><p style='margin:0px;'><br><br></p>" +
                            "<p style='margin:0px;'>Thanks,</p>"+ $scope.liuFullName;

        $("div[id^='taTextElement']").append($scope.emailContent.emailBody)
    }

    $scope.closeEmailWindow = function() {
        $scope.openEmailWindow = false;
    }

    $scope.sortTable = function(item) {

        if(!_.includes(['Analytics', 'Share', 'Delete'], item.name)) {
            $scope.orderByField = item.sortableName;
            $scope.reverseSort = !$scope.reverseSort;
        }
    }

    $scope.sortSharedWithTable = function(item) {

        if(!_.includes(['Sharing'], item.name)) {
            $scope.orderSharedWithTableByField = item.sortableName;
            $scope.sharedWithTableReverseSort = !$scope.sharedWithTableReverseSort;
        }
    }

    $scope.searchContacts = function(keywords) {

        if(keywords && keywords.length > 2) {
            searchService.search(keywords).success(function(response){

                if(response.SuccessCode) {
                    processSearchResults($scope, response);
                    $scope.showContactsSuggestion = true;
                } else {
                    $scope.contactToList = [];
                }
            }).error(function(){
                console.log('error');
            });
        }
    }

    $scope.addToContact = function(contact) {
        
        $scope.email.firstName = contact.firstName;
        $scope.email.lastName = contact.lastName;
        $scope.showContactsSuggestion = false;
        $scope.email.toEmailId = contact.emailId;

        $("#document-share-to-emailId").val(contact.emailId)
        $scope.recomposeEmailBody($scope.email.firstName);
    }
    
    $scope.sendEmail = function(firstName, lastName, toEmailId, expiryDate, emailSubject, emailBody) {

        emailBody = $("div[id^='taTextElement']").html();
        $scope.resendEmailObj = {};
        
        if(checkRequired(firstName) && checkRequired(lastName) && checkRequired(toEmailId) 
                && checkRequired(emailSubject) && checkRequired(emailBody)) {

            toEmailId = $("#document-share-to-emailId").val();
            if(validateEmail(toEmailId)) {
                var document = $scope.sharedDocument;
    
                var documentShareInfo = {
                    documentId: document.documentId,
                    shareWithEmail: toEmailId,
                    firstName: firstName,
                    lastName: lastName,
                    userMsg: emailBody,
                    sharedOn: new Date(),
                    expiryDate: expiryDate,
                    emailSubject: emailSubject
                }
    
                var emailObj = {
                    email_cc: null,
                    receiverEmailId: documentShareInfo.shareWithEmail,
                    receiverName: documentShareInfo.firstName + ' ' + documentShareInfo.lastName,
                    message: documentShareInfo.userMsg,
                    subject: documentShareInfo.emailSubject,
                    docTrack: true,
                    trackViewed: true,
                    remind: null,
                    respSentTime: moment().format(),
                    isLeadTrack: false,
                    newMessage: true,
                    remind: $scope.email.notify
                }

                var sharedWithUser = _.find(document.sharedWith, {emailId:emailObj.receiverEmailId});
                if(sharedWithUser) {
                    $scope.resendEmailObj = {
                        "documentId": document.documentId,
                        "documentName": document.documentName,
                        "sharedDate": moment(sharedWithUser.sharedOn).format("DD MMM YYYY"),
                        "sharedWithEmailId": sharedWithUser.emailId,
                        "emailObj": emailObj,
                        "showResendEmailDlg": true
                    };

                } else {

                    $http.post('/share/document', documentShareInfo)
                    .success(function(response) {

                        if(response.SuccessCode) {
                            $scope.hideResendEmailDlg();
                            toastr.success("Email sent successfully");
                            $scope.openEmailWindow = false;
                            
                            share.getDocuments(function() {
                                share.loadMyDocuments();
                            });
                            
                        } else {
                            toastr.error("Sharing document failed");
                        }
                    });
                }

            } else {
                toastr.error("Email is invalid");
            }
        } else {
            toastr.error("Please fill all mandatory fieds");
        }
    }

    // @Deprecated function
    $scope.composeEmailObject = function(docId, emailObj, sharedDocument) {

        $scope.hideResendEmailDlg();
        var documentUrl = 'https://'+share.companyDetails.url + '/readDocument/'+ docId+'/'+sharedDocument.sharedWith[0]._id;
        var pdfIconUrl = 'https://'+share.companyDetails.url + '/images/pdf_icon.png'
        
        emailObj.message = emailObj.message +
        "<div style='margin-top:20px;'><div style='display:inline-block; margin-right: 20px;'> " +
            "<a href=" + documentUrl +" >" +
                "<img src=" + pdfIconUrl + " style='display:inline-block;width:40px'/> " +
            "</a>" +
        "</div> " +
        "<div style='display:inline-block; width:80%'>" +
            // "<p style='margin:0px'><b>" + $scope.sharedDocument.documentName + "</b></p>" +
            "<p style='margin:0px'> " + 
                "<a style='text-decoration:none; color:#222; font-weight: bold;' href=" + documentUrl +" >" +
                    "<span>" + $scope.sharedDocument.documentName + "</span>" +
                "</a> </p>" +
            "<p style='margin:0px'><b>Link: </b>" + documentUrl + "</p>" +
            "<p style='margin:0px'><b>Security Code: </b>" + 
                sharedDocument.sharedWith[0].documentPassword; +
            "</p>" +
        "</div></div>";

        $http.post("/messages/send/email/single/web",emailObj).success(function(response){
            if(response.SuccessCode) {
                toastr.success("Email sent successfully");
                $scope.openEmailWindow = false;
                
                share.getDocuments(function() {
                    share.loadMyDocuments();
                });
            }
            else 
                toastr.error("Email sending failed");
        });
    }

    $scope.resendEmail = function() {

        $http.post('/resend/document', {documentId: $scope.resendEmailObj.documentId, sharedWithEmailId: $scope.resendEmailObj.sharedWithEmailId})
        .success(function(response) {
            if(response.SuccessCode) {
                toastr.success("Email successfully resent");
                $scope.closeEmailWindow();
            }
        });

        $scope.hideResendEmailDlg();
    }

    $scope.hideResendEmailDlg = function() {
        $scope.resendEmailObj.showResendEmailDlg = false;
    }

    $scope.showDeleteConfirmDlg = function(document) {
        mixpanelTracker("Documents>delete doc");
        $scope.delDocument = document;
        $scope.docDeleteConfirmDlg = true;
    }

    $scope.hideDeleteConfirmDlg = function() {
        $scope.docDeleteConfirmDlg = false;
    }

    $scope.deleteDocument = function(document) {
        deleteDocumentFromDatabase($scope, $http, share, document.documentId, document.awsKey);
        /* if(document.awsKey)
            deleteDocumentFromAWS($scope, $http, share, document.documentId, document.documentName, document.awsKey)
        else
            deleteDocumentFromDatabase($scope, $http, share, document.documentId, document.documentName); */
    }

    $scope.updateDocumentExpiry = function(document) {
        var elementId  = 'date-' + document.sharedWithObjectId;

        $('#' + elementId).datetimepicker({
            timepicker: false,
            validateOnBlur: false,
            minDate: new Date(),
            onSelectDate: function (dp, $input) {
                dp = new Date(dp)
                dp.setHours(23)
                dp.setMinutes(59)
                dp.setSeconds(59)

                $('#input').datetimepicker('destroy');

                $scope.$apply(function () {
                    $scope[elementId] = moment(dp).format("DD MMM YYYY");
                    document.displayExpiryDate = $scope[elementId];
                    document.expiryDate = $scope[elementId];

                    var daysDiff = moment(document.expiryDate).diff(moment(), 'days')
                    if(daysDiff < 0)
                        document.accessStatusBtnDisable = true;
                    else 
                        document.accessStatusBtnDisable = false;

                    var expObj = {
                        expiryDate: $scope[elementId],
                        docId: document.documentId,
                        sharedWithId: document.sharedWithObjectId
                    }
                    $scope.updateDate(expObj);
                });
            }
        });
    }

    $scope.updateDate = function(expObj) {
        $http.post('/setDocExpiry', expObj)
            .success(function(response) {
                if(!response)
                    toastr.error("Document expiry date update failed");
                else {
                    toastr.success("Document expiry date has been updated");
                    share.getDocuments(function() {
                        share.loadMyDocuments();
                    });
                }
            })
    };

    $scope.changeDocumentSharingStatus = function(document) {
        mixpanelTracker("Documents>update doc sharing status ");
        var accessChangeUrl = '/change/access/status/'+document.documentId+'/'+document.sharedWithObjectId+'/'+document.accessStatus;

        $http.get(accessChangeUrl) 
            .success(function(response) {
                if(!response) {
                    toastr.error("Error while changing selected document access status")
                    document.accessStatus = !document.accessStatus;
                } else {
                    if(document.accessStatus)
                        toastr.success("The selected document has been shared with the user.")
                    else
                        toastr.success("The selected document is no longer shared with the user.")

                    share.getDocuments(function() {
                        share.loadMyDocuments();
                    });
                }
            })
    }

    $scope.onSelectCategory = function(selectedCategory) {
        if(checkRequired(selectedCategory)) {
            $scope.upload.disableUploadBtn = false;
            $scope.selectedCategory = selectedCategory;
        }
    }

    $scope.toggleContactPopup = function() {
        $scope.showContactCreateModal = false;
    }

    $scope.createContactWindow = function() {
        $("#fullName").val("");
        $("#p_emailId").val("");
        $("#companyName").val("");
        $("#designation").val("");
        $("#p_mobileNumber").val("");
        $("#p_location").val("");

        $scope.showContactCreateModal = true;
    }

    $scope.saveContact = function() {

        if(!$scope.coords){
            $scope.coords = {}
        }

        var data = {
            personName:$("#fullName").val(),
            personEmailId:$("#p_emailId").val(),
            companyName:$("#companyName").val(),
            designation:$("#designation").val(),
            mobileNumber:$("#p_mobileNumber").val(),
            location:$("#p_location").val(),
            lat:$scope.coords.lat,
            lng:$scope.coords.lng
        }

        var errExists = false;

        if(!data.personName){
            errExists = true
            toastr.error("Please enter a name for the contact.")
        } else if(!validateEmail(data.personEmailId)){
            toastr.error("Please enter a valid email ID.")
            errExists = true
        } else if(!data.location) {
            toastr.error("Please enter city for the contact.")
            errExists = true
        }

        if(!errExists){
            $http.post("/contacts/create",data)
                .success(function (response) {
                    toastr.success("Contact added successfully.")
                    $scope.showContactCreateModal = false;
                })
        }
    }

    $scope.parseHtml = function($html) {
        var textWithoutStyle = $html.replace(/style="[^"]*"/g, "");
        return "<div style='color:black; font-size: 13px;'>"  + textWithoutStyle + "</div>"
    }

    $scope.paintAnalytics();
})

relatasApp.directive("documentUpload", ['$document', 'share', function($document, share) {
    return {
        scope: {
        fileinput: "="
    },
    link: function(scope, element, attributes) {
            element.bind("change", function(changeEvent) {
                scope.fileinput = changeEvent.target.files[0];
                var reader = new FileReader();
                reader.readAsDataURL(scope.fileinput);
                reader.onload = function(loadEvent) {
                    uploadFile(scope.fileinput, share);
                }
            });
    }
}
}]);

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            
            scope.imageNotLoaded = function(obj){

                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else{
                    obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    obj.noPPic = obj.name.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                scope.$apply(attrs.imageonload);
            });
        }
    };
});

function checkCompanyLogoUploadStatus(scope, share) {
    scope.upload = {};
    scope.upload.disableUploadBtn = true;

    if(share.companyDetails) {
        scope.documentCategory =  _.filter(share.companyDetails.documentCategoryList, function(categoryList) {
                                        return categoryList.active;
                                    });
        scope.documentCategory = _.map(scope.documentCategory, 'name').sort();

        if(share.companyDetails.logo && share.companyDetails.logo.url)
            scope.companyLogoUploaded = true;
        else
            scope.companyLogoUploaded = false;
            
    } else {
        setTimeout(function() {
            checkCompanyLogoUploadStatus(scope, share)
        }, 50)
    }
}

function parseAnalyticsData(scope, analytics) {
    // scope.analyticsRightSummary = {};

    var pagesTotalReadTime  = {};
    var lastAccess = [];
    var totalViewTime = 0;

    _.each(analytics.accessInfo.pageReadTimings, function(page) {
        var pageNum = page.pageNumber;
        lastAccess.push(page.lastAccessed);
        totalViewTime += parseFloat(page.Time);
        pagesTotalReadTime[pageNum] =  pagesTotalReadTime[pageNum] ? pagesTotalReadTime[pageNum] + page.Time : page.Time
    });

    lastAccess.sort(sortAscending);
    scope.analyticsRightSummary["firstAccessed"] = analytics.firstAccessed ? moment(analytics.firstAccessed).format("DD MMM YYYY") : '------';
    scope.analyticsRightSummary["lastAccessed"] = analytics.lastAccessed ? moment(analytics.lastAccessed).format("DD MMM YYYY") : '------';
    scope.analyticsRightSummary["totalViewTime"] = getTimeInString(totalViewTime);
    scope.analyticsRightSummary["accessCount"] = analytics.accessCount || 0;

    var barChartData = [];
    var timeMax = 0;
    var pageMax = 0;


    _.each(_.keys(pagesTotalReadTime), function(key) {
        var arr = [];
        var minutes = (pagesTotalReadTime[key]/60).toFixed(2);
        minutes = parseFloat(minutes) < 0.03 ? 0.03 : parseFloat(minutes);
        var time = getTimeInString(pagesTotalReadTime[key]);

        arr.push(key, minutes, "Page:"+key+"\n"+time, "color:#03A2EA");
        barChartData.push(arr);

        if(parseInt(key) > pageMax)
            pageMax = parseInt(key);
        
        if(parseFloat(minutes) > timeMax)
            timeMax = parseFloat(minutes);
    })

    drawAnalytics(scope, barChartData, pageMax, timeMax);
}

function getTimeInString(seconds) {
    var viewTimeMinutes = Math.floor(seconds/60);
    var viewTimeHours = Math.floor(viewTimeMinutes/60);
    var viewTimeSeconds = Math.floor(seconds%60);
    viewTimeMinutes = viewTimeMinutes%60;

    var timeInString = viewTimeHours ? viewTimeHours +"h " + viewTimeMinutes +"m " + viewTimeSeconds +"s" : 
                            viewTimeMinutes ? viewTimeMinutes +"m " + viewTimeSeconds +"s" : viewTimeSeconds +"s";
                            
    return timeInString;
} 

function generateDocumentMetaData(scope, document) {
    var obj = {};
    var viewSummary = getPeopleViewCount(document);
    var avgTime = viewSummary.avgViewTime;
    var documentMetaData = [];

    obj.documentName = document.documentName;
    obj.sharedWith = scope.documentsSharedByMe.length;
    obj.peopleViewd = viewSummary.peopleViewd;
    obj.totalViews = viewSummary.totalViews;
    obj.firstView = viewSummary.firstAccess.length > 0 ? moment(viewSummary.firstAccess[0]).format("DD MMM YYYY") : "---------";
    obj.firstViewTime = viewSummary.firstAccess.length > 0 ? moment(viewSummary.firstAccess[0]).format("hh:mm a") : null;
    obj.lastView = viewSummary.lastAccess.length > 0 ? moment(viewSummary.lastAccess[0]).format("DD MMM YYYY") : "---------";
    obj.lastViewTime = viewSummary.lastAccess.length > 0 ? moment(viewSummary.lastAccess[0]).format("hh:mm a") : null;
    obj.avgViewTime = getTimeInString(avgTime); 
    
    scope.documentMetaDataDocName = obj.documentName;
    
    documentMetaData.push({
        name: "Shared with",
        count: obj.sharedWith,
        time: '',
        imageUrl: '/images/documents/ppl.svg'
    })

    documentMetaData.push({
        name: "People Viewed",
        count: obj.peopleViewd,
        time: '',
        imageUrl: '/images/documents/views.svg'
    })

    documentMetaData.push({
        name: "Total Views",
        count: obj.totalViews,
        time: '',
        imageUrl: '/images/documents/views.svg'
    })

    documentMetaData.push({
        name: "First Viewed",
        count: obj.firstView,
        time: obj.firstViewTime,
        imageUrl: '/images/documents/f_view.svg'
    })

    documentMetaData.push({
        name: "Last Viewed",
        count: obj.lastView,
        time: obj.lastViewTime,
        imageUrl: '/images/documents/l_view.svg'
    })

    documentMetaData.push({
        name: "Avg Time",
        count: obj.avgViewTime,
        time: '',
        imageUrl: '/images/documents/avg_time.svg'
    })
    return documentMetaData;
}

function getPeopleViewCount(document) {
    var peopleViewCount = 0;
    var totalViews = 0;
    var pageReadTime = 0;
    var firstAccess = [];
    var lastAccess = [];

    _.each(document.sharedWith, function(sharedWith) {
        if(sharedWith.accessed) {
            peopleViewCount += 1;
            totalViews += sharedWith.accessCount;
            pageReadTime += getTotalPageReadingTime(sharedWith.accessInfo);

            if(sharedWith.firstAccessed)
                firstAccess.push(sharedWith.firstAccessed);
            
            if(sharedWith.lastAccessed)
                lastAccess.push(sharedWith.lastAccessed);
        }
    })

    var avgViewTime = peopleViewCount > 0 ?  (pageReadTime/peopleViewCount) : 0;

    firstAccess.sort(sortAscending);
    lastAccess.sort(sortDecending);

    var returnObj = { "peopleViewd": peopleViewCount,
                        "totalViews" : totalViews,
                        "firstAccess": firstAccess,
                        "lastAccess": lastAccess,
                        "avgViewTime": avgViewTime};

    return returnObj;
}

function processSearchResults($scope,response) {

    var contactsArray = response.Data;
    $scope.contactToList = []

    if(contactsArray.length > 0) {
        _.each(contactsArray, function(contact) {
            var obj = {};
    
            var contactNames = contact.personName.split(" ");

            obj = {
                firstName: contactNames[0],
                lastName:  contactNames.length > 1 ? contactNames[1] : "",
                fullName: contact.personName,
                name: sliceText(contact.personName,20),
                emailId: contact.personEmailId
            }

            if(checkRequired(contact.personId) && checkRequired(contact.personName)){
                obj.image = '/getImage/'+contact.personId._id;
            }
            else {
                var contactImageLink = contact.contactImageLink ? encodeURIComponent(contact.contactImageLink) : null
                obj.image = '/getContactImage/' + contact.personEmailId + '/' + contactImageLink;
            }
    
            obj._id = contact._id;
            obj.personId = contact.personId && contact.personId._id?contact.personId._id:null;
    
            if(!userExists(obj.emailId) && validateEmail(obj.emailId)){
                $scope.contactToList.push(obj)
            }
    
            function userExists(username) {
                return $scope.contactToList.some(function(el) {
                    return el.emailId === username;
                });
            }
        })
    }
}

var sortAscending = function(d1, d2) {
    return new Date(d1) - new Date(d2);
}

var sortDecending =  function(d1, d2) {
    return new Date(d2) - new Date(d1);
}

function getTotalPageReadingTime(accessInfo) {
    var readTime = 0;

    _.each(accessInfo.pageReadTimings, function(page) {
        readTime += parseFloat(page.Time);
    })

    return parseFloat(readTime.toFixed(2));

}

function generateTotalDocReadStatusTable(scope, document) {
    var documentViewRecord = [];

    if (checkRequired(document)) {

        scope.peopleViewed = getOpenedUnopened(document);
        var totalViewes = 0;

        if (checkRequired(document.sharedWith[0])) {

            _.each(document.sharedWith, function(person) {
                var obj = {};

                totalViewes += person.accessCount || 0;
                obj.name = person.firstName || '';
                obj.views = person.accessCount || 0;
                obj.lastViewed = checkRequired(person.lastAccessed) ? moment(person.lastAccessed).format("DD MMM YYYY") : 'Not viewed';
                documentViewRecord.push(obj);
            });
        }

        scope.documentViewRecord = documentViewRecord;
        scope.readCount = document.readCount;
        scope.totalViewes = totalViewes;

        scope.showMyDocumentAnalytics = true;
    }
    else {
        scope.showMyDocumentAnalytics = false;
    }
}

function drawAnalytics(scope, barChartData, maxPages, timeMax) {
        scope.showAnalytics = true;

        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Pages');
        data.addColumn('number', 'Minutes');
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addColumn({type: 'string', role: 'style'});

        data.addRows(barChartData);
        var options = {
            width: 500,
            hAxis: {title: 'Page', minValue: 1, maxValue: maxPages + 2, showTextEvery:0},
            vAxis: {title: 'Total Page Open Time', minValue: 0, maxValue: timeMax + 2},
            colors: ['#03A2EA'],
            chartArea: {  width: "50%", height: "70%" }
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('analyticsGraph'));
        chart.draw(data, options);
    }

function getAWSCredentials(share, http) {
    http.get('/getAwsCredentials')
        .success(function (credentials) {
            share.AWScredentials = credentials;
            AWS.config.update(credentials);
            AWS.config.region = credentials.region;

            s3bucket = new AWS.S3({params: {Bucket: credentials.bucket}});

            docUrl = 'https://' + credentials.bucket + '.s3.amazonaws.com/';
        });
}

function deleteDocumentFromAWS(scope, http, share, docId, docName, awsKey) {
    var params = {
        Bucket: share.AWScredentials.bucket,
        Key: awsKey
    };

    s3bucket.deleteObject(params, function(error,data) {
        if(error)
            toastr.error("Document deletion failed");
        else 
            deleteDocumentFromDatabase(scope, http, share, docId, docName)
    })
}

function deleteDocumentFromDatabase(scope, http, share, docId, awsKey) {
    var documentUrl = '/deleteDocument/' + docId + '/' + awsKey;
    http.get(documentUrl)
        .success(function(response) {
            scope.hideDeleteConfirmDlg();

            if(response.SuccessCode) {
                toastr.success(response.Message);
                share.getDocuments(function() {
                    share.loadMyDocuments();
                });
            }
            else
                toastr.error("Document deletion failed");
        });
}

function uploadFile(file, share) {
    var scope = angular.element(document.getElementById('uploadDocument')).scope();

    if (file && checkFileConstraint(file, scope)) {
        scope.disableUploadBtn = true;

        var dateNow = new Date();
        var d = dateNow.getDate();
        var m = dateNow.getMonth();
        var y = dateNow.getFullYear();
        var hours = dateNow.getHours();
        var min = dateNow.getMinutes();
        var sec = dateNow.getSeconds();
        var timeStamp = y + ':' + m + ':' + d + ':' + hours + ':' + min + ':' + sec;
        var dIdentity = timeStamp + '_' + file.name;
        var docNameTimestamp = dIdentity.replace(/\s/g, '');
        var docName = file.name;
        var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
        var request = s3bucket.putObject(params);

        $("#doc-upload-progress").css({display: 'block'});

        request.on('httpUploadProgress', function (progress) {
            $("#doc-upload-progress").val(progress.loaded / progress.total * 100)
        });

        request.send(function (err, data) {
            if (err) {
                console.log(err);
            }
            else {
                var url = docUrl + docNameTimestamp;
                var docD = {
                    documentName: docName,
                    documentCategory: scope.selectedCategory,
                    documentUrl: url,
                    awsKey: docNameTimestamp,
                    invitationId: 'no',
                    access: 'public',
                    sharedDate: new Date()
                }

                $.ajax({
                    url: '/uploadDocument',
                    type: 'POST',
                    datatype: 'JSON',
                    data: docD,
                    traditional: true,
                    success: function (result) {
                        scope.disableUploadBtn = false;
                        share.getDocuments(function() {
                            share.loadMyDocuments();
                        });
                    }
                })
            }
        });
    } else if(!file){
        alert("Please select file to upload.", 'error')
    }
}

function checkFileConstraint(file, scope) {
    if(file.size > 5000000) {
        toastr.error("Upload files only within 5MB");
        return false;
    }

    var findDoc = _.find(scope.myDocuments, {documentName:file.name});

    if(findDoc) {
        toastr.error("\'" + findDoc.documentName + "\' was uploaded by you on " + findDoc.displayDate + ". Please search for the same under Search documents");
        return false
    }

    return true;
}

function parseMyDocuments(documents) {
    var myDocuments = [];

    _.each(documents, function(doc) {
        var obj = {};
        var openUnopenCount = getOpenedUnopened(doc);

        obj.sharedWith = doc.sharedWith;
        obj.sharedWithLength = doc.sharedWith.length;
        obj.documentUrl = '/readDocument/' + doc._id;
        obj.documentName = doc.documentName || '';
        obj.documentCategory = doc.documentCategory || '';
        obj.uploadedDate = doc.sharedDate;
        obj.displayDate = moment(doc.sharedDate).format(standardDateFormat())
        obj.readCount = openUnopenCount.readCount;
        obj.unreadCount = openUnopenCount.unreadCount;
        obj.documentId = doc._id;
        obj.awsKey = doc.awsKey;

        myDocuments.push(obj);

    });

    return myDocuments;
}

function parseSharedByMeDocuments(document) {
    var doc = document;
    var sharedDocuments = [];

    for(var j=0; j<doc.sharedWith.length; j++) {
        var obj = {};

        obj.accessed = doc.sharedWith[j].accessed;
        obj.firstName = doc.sharedWith[j].firstName || doc.sharedWith[j].emailId;
        obj.lastName = doc.sharedWith[j].lastName || "";
        obj.sharedWithName = obj.firstName + " " + obj.lastName;
        obj.sharedWithEmailId = sliceText(doc.sharedWith[j].emailId, 25);
        obj.expiryDate = doc.sharedWith[j].expiryDate;
        obj.displayExpiryDate = checkRequired(obj.expiryDate) ? moment(obj.expiryDate).format("DD MMM YYYY") : '';
        obj.sharedDate = doc.sharedWith[j].sharedOn || doc.sharedDate;
        obj.displaySharedDate = moment(obj.sharedDate).format("DD MMM YYYY");
        obj.sharedTime = doc.sharedWith[j].sharedOn ? moment(doc.sharedWith[j].sharedOn).format("hh:mm a") : '';
        obj.documentName = doc.documentName;
        obj.documentId = doc.documentId;
        obj.sharedWithUserId = doc.sharedWith[j].userId;
        obj.sharedWithObjectId = doc.sharedWith[j]._id;
        obj.daysDiff = moment(obj.expiryDate).diff(moment(), 'days');
        obj.accessStatus = doc.sharedWith[j].accessStatus == true && obj.daysDiff >=0 ? true : false;
        obj.accessStatusBtnDisable = obj.daysDiff < 0 ? true : false;
        obj.documentUrl = '/readDocument/' + doc.documentId + '/' + doc.sharedWith[j]._id;
        obj.lastViewDate = doc.sharedWith[j].lastAccessed || '';
        obj.displayLastViewDate = checkRequired(obj.lastViewDate) ? moment(obj.lastViewDate).format("DD MMM YYYY") : '';
        obj.lastViewTime = checkRequired(obj.lastViewDate) ? moment(obj.lastViewDate).format("hh:mm a") : '';
        
        if(checkRequired(obj.sharedWithName))
            sharedDocuments.push(obj);
    }


    return sharedDocuments;
}

function getOpenedUnopened(document) {
    var sharedWithLength = document.sharedWith.length;
    var shareWith = document.sharedWith;
    var readCount = 0;

    _.each(document.sharedWith, function(shareWith) {
        if(shareWith.accessCount > 0)
            readCount++;
    })

    var unreadCount = sharedWithLength - readCount;
    return {readCount: readCount, unreadCount: unreadCount}
}

function menuToggleSelection(select,menu) {

    _.each(menu,function (item) {
        if(item.name == select){
            item.selected = "selected"
        } else {
            item.selected = ""
        }
    })
}

function menuItems(){

    var list = [{
        name:"Upload + Analytics",
        selected:"selected"
    },{
        name:"Create",
        selected:""
    }];
    /* ,
    {
        name:"Analytics",
        selected:""
    } */
    return list;
}

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}


function checkRequired(data) {
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else {
        return true;
    }
}

function sliceText(text, maxLength) {
    var textLength;
    if (checkRequired(text)) {
        textLength = text.length;
    }
    else {
        text = '';
        textLength = 0;
    }

    if (textLength > maxLength) {
        var formattedText = text.slice(0, maxLength)
        return formattedText + '..';
    }
    else return text;
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}
