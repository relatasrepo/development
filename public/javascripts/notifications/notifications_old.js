///**
// * Created by naveen on 10/29/15.
// */


var reletasApp = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);


reletasApp.controller("header_controller", function($scope){

    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    }

    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){
        //var str = searchContent.replace(/[^\w\s]/gi, '');
        //var str = searchContent;
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

reletasApp.service('todayService', function () {
    return {}
});

reletasApp.controller("logedinUser", function ($scope, $http,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {
            setTimeout(function(){
                $(".seeMoreIcon").addClass("disabled-see-more")
            },2000)

            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
            }
            else{

            }
        }).error(function (data) {

        })
});

reletasApp.controller("notifications_page_data_controller", function($scope, $http, todayService){

    var totalNotificationsCount;
    $scope.counts={losingTouchCount:0,emailResponsesPending:0,emailAwaitingResponses:0};

    $http.get('/dashboard/status/counts/top/web')
        .success(function (response){
            if(response.SuccessCode){
                $scope.counts.losingTouchCount = $scope.getValidStringNumber(response.Data.losingTouchCount || 0);
                $scope.counts.emailResponsesPending = $scope.getValidStringNumber(response.Data.emailResponsesPending || 0);
                $scope.counts.emailAwaitingResponses = $scope.getValidStringNumber(response.Data.emailAwaitingResponses || 0);
                $scope.sumOf1 = parseFloat($scope.counts.losingTouchCount) + parseFloat($scope.counts.emailResponsesPending) + parseFloat($scope.counts.emailAwaitingResponses);
                $http.get('/today/left/section')
                    .success(function (response){
                        if(response.SuccessCode){
                            var checkText = response.Data.pendingTasks;
                            var noTasks = "Click on Add to assign tasks";
                            //$scope.sumOf = parseFloat($scope.openTasksCount)|| 0 + parseFloat($scope.sumOf1)|| 0;
                            if (angular.equals(checkText, noTasks)) {
                                $scope.openTasksCount = 0;
                            }
                            else {
                                $scope.openTasksCount = response.Data.pendingTasks.length;
                            }
                            $scope.sumOf1=$scope.sumOf1+$scope.openTasksCount;

                        }
                    });

                var dateNow = new Date().getTime()

                $http.get('/api/v2/calendar/'+dateNow)
                    .success(function (response){
                        $scope.meetingsInvitations = response.events.length;
                        $scope.sumOf1=$scope.sumOf1+$scope.meetingsInvitations;
                    });
            }
            else {
                $scope.counts.losingTouchCount = $scope.getValidStringNumber(0);
                $scope.counts.emailResponsesPending = $scope.getValidStringNumber(0);
                $scope.counts.emailAwaitingResponses = $scope.getValidStringNumber(0);
            }
        }).error(function (data){

        });

    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };

});