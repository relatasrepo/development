var relatasApp = angular.module('relatasApp', ["ngLodash"]).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.service('share', function () {
    return {
        getIsOk:function(){
        },
    };
});

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };  
});

relatasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        share.companyDetails.currency.forEach(function (el) {
            share.currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                share.primaryCurrency = el.symbol;
            }
        });
    });
});

relatasApp.controller("notificationController", function ($scope, $http, share,$rootScope) {
    $scope.notificationHeaderText = "Notification Summary (" + moment().format("DD MMM YYYY") + ")";
    $scope.rows = [];
    $scope.visibility = {
        showNotification: true
    }

    function getNotificationData() {

        $http.get('/get/notifications/today?dataFor=web')
            .success(function(response) {
                $scope.notificationHeaderText = "Notification Summary (" + moment(response.data.notificationDate).format("DD MMM YYYY") + ")";

                _.each(response.data.notifications, function(notification) {
                    if(notification.url) {
                        notification.style = "cursor: pointer",
                        notification.formattedDate = notification.sentDate ? moment(notification.sentDate).format("DD MMM YYYY") : "";
                    }
                })

                var totalRows = parseInt(Math.floor(response.data.notifications.length/3));

                if(totalRows > 0) {
                    $scope.visibility.showNotification = true;
                } else {
                    $scope.visibility.showNotification = false;
                }

                for(var row = 0; row <= totalRows; row++) {
                    var rowEles = [];

                    for(var i=0; i<3; i++) {
                        var notification = response.data.notifications.pop();

                        if(notification)
                            rowEles.push(notification);
                        else 
                            break;
                    }

                    $scope.rows.push(rowEles);
                }
            })
    }

    $scope.redirectNotification = function(notification) {
        if(notification && notification.url) {
            window.open(notification.url, "_blank");
        }
    }

    getNotificationData();
});


