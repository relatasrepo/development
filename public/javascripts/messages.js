$(document).ready(function(){
   var table,userProfile;

    var url = window.location.href;
    var messageId = null;
    var params = getParams(url);
    if(checkRequired(params) && checkRequired(params.id)){
        messageId = params.id;

        $("html, body").animate({ scrollTop: $(document).height()-10 }, "slow");
    }

    var id = url.split('/')[5];
    $("#userMessage").jqte();
    getUserProfile();
    applyDataTable();
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;
                getAllMessages(id)
                if(checkRequired(messageId)){
                    getMessage(null,messageId,true);
                }
                $('#profilePic').attr("src", result.profilePicUrl);
                if (result.serviceLogin == 'linkedin') {
                    $("#uploadContactsUrl").attr('href', '/addGoogleContacts');
                    $("#uploadContactsUrl2").attr('href', '/addGoogleContacts');
                }

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }


    function getAllMessages(id){
        $.ajax({
            url:'/messages/all/summary/'+id,
            type:'GET',
            success:function(messages){
                if(checkRequired(messages) && messages.length > 0){
                    createTableData(messages)
                }
            }
        })
    }

    function createTableData(messages) {

        if (checkRequired(table)) {
            table.clear().draw();
        }
        for (var msg = 0; msg < messages.length; msg++) {
            if(checkRequired(messages[msg].senderId) && checkRequired(messages[msg].receiverId)){

                var arr = [
                    messages[msg].senderId.firstName+' '+messages[msg].senderId.lastName,
                    messages[msg].receiverId.firstName+' '+messages[msg].receiverId.lastName,
                    '<span id="messageActionTitle" messageId='+messages[msg]._id+' class="link-span" style="cursor: pointer;">'+getTextLength(messages[msg].subject,30)+'</span>',
                    '<span id="sortDate" value='+messages[msg].sentOn+'>'+ moment(messages[msg].sentOn).format('MMM DD YYYY')+'</span>'
                ];
                addRowsToTable(arr);
            }
        }
    }

    function getTextLength(text,maxLength){
        if(!checkRequired(text)){
            return ''
        }
        var textLength = text.length;
        if(textLength >maxLength){
            var formattedText = text.slice(0,maxLength)
            return formattedText+'..';
        }
        else return text;
    }

    function getParams(url){
        var regex = /[?&]([^=#]+)=([^&#]*)/g,
            params = {},
            match;
        while(match = regex.exec(url)) {
            params[match[1]] = match[2];
        }
        return params;
    }

    function addRowsToTable(rowArr){
        table.row.add( rowArr ).draw();
    }

    function applyDataTable(){
        table = $('#messages-table').DataTable({
            "dom": '<"top"iflp<"clear">>',
            "aaSorting": [],
            "columns": [
                null,
                null,
                null,
                { "orderDataType": "dom-value" }
            ],
            "oLanguage": {
                "sEmptyTable": "No Messages Found"
            }
        });
    }
    /* Create an array with the values of all the select options in a column */
    $.fn.dataTable.ext.order['dom-value'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('#sortDate', td).attr("value");
        } );
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }


    /* SHOW MESSAGE SCRIPT */

    var profile,message;

    $("body").on("click", "#messageActionTitle", function () {
        getMessage($(this),null,false);
        $("#MsgSubject").focus();
        //$("html, body").animate({ scrollTop: $(document).height()-100 }, "slow");
    });

    function getMessage(element,id,isIdProvided){
        var mesId= isIdProvided ? id : element.attr('messageId');
        $.ajax({
            url:'/messages/getMessage/'+mesId,
            type:'GET',
            success:function(message){

                if(checkRequired(message)){

                        var prf = {}
                        if(message.senderId._id == userProfile._id){
                            prf = message.receiverId;
                        }
                        else  prf = message.senderId;

                        showMessageDetails(null,message,prf)

                }
            }
        })
    }

    function showMessageDetails(element,messageIn,profileIn){
        $("#messageDetails").show();
        message = checkRequired(messageIn) ? messageIn : JSON.parse(element.attr('message'));
        profile = checkRequired(profileIn) ? profileIn : JSON.parse($("#"+message._id).attr('profile'));
        $("#subject").text(message.subject || '');
        $("#actual-message").html(message.message);
        $("#messageDetails").show();
        if(checkRequired(message.subject)){
            if(message.subject.substr(0, 2) != 'RE'){
                $("#MsgSubject").val('RE: '+message.subject || '');
            }
            else $("#MsgSubject").val(message.subject || '');
        }

        updateMessageReadStatus(message._id);
    }

    $("#sendMessage").on('click',function(){
        var massMailArr = [];
        var msg = $("#userMessage").val().trim();
        var subject = $("#MsgSubject").val();
        if(checkRequired(msg)){
            //msg = 'Hi '+profile.firstName+'<br>'+msg;

            subject = checkRequired(subject) ? subject : message.subject

            var msgObj = {
                senderEmailId:userProfile.emailId,
                userId:profile._id || '',
                relatasContact: true,
                senderName:userProfile.firstName+' '+userProfile.lastName,
                receiverEmailId:profile.emailId,
                receiverName:profile.firstName+' '+profile.lastName,
                message:msg,
                responseFlag:true,
                headerImage:false
            }
            massMailArr.push(msgObj)
            if(checkRequired(massMailArr[0])){
                var stringMsg = JSON.stringify(massMailArr);

                $.ajax({
                    url:'/sendEmailMessage',
                    type:'POST',
                    datatype:'JSON',
                    data:{data:stringMsg,subject:subject},
                    success:function(response){

                        if(response){

                            showMessagePopUp("Your message has been sent successfully.",'success');
                           // table.row(mElement.parents('tr')).remove().draw();
                            $("#MsgSubject").val('')
                            $("#messageDetails").hide();
                            $("#userMessage").jqteVal("");
                        }
                        else{
                            showMessagePopUp("Your message has not been sent. Please try again.",'error');
                        }
                    }
                })
            }
        }else showMessagePopUp("Please enter your reply message.",'error')
    });

    function updateMessageReadStatus(messageId) {
        $.ajax({
            url: '/messages/update/readStatus/'+messageId,
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (requests) {

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    /* MESSAGE POPUP SCRIPT */
    function showMessagePopUp(message, msgClass) {

        var html = _.template($("#message-popup").html())();
        $("#user-msg").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });

        $("#user-msg").popover('show');
        $(".arrow").addClass("invisible");
        if (msgClass == 'error') {
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        } else if (msgClass == 'success') {
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        } else if (msgClass == 'tip') {
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message);
        $("#closePopup-message").focus();

    }

    $("body").on("click", "#closePopup-message", function () {
        $("#user-msg").popover('destroy');
    });
});