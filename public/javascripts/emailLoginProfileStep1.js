$(document).ready(function() {

    var googleUserData;
    loadGoogleUserData();
    $('#home').on("click", function () {
        window.location.replace('/');
    });

    $("#changeProfileImageBut").on("click",function(){
        storeToSession()
        $("#changeProfileImage").trigger('click');
    });

    $("#changeProfileImage").on("change",function(){
       $("#changeProfileImgForm").submit()

    });
getProfileImageUrl()
    var imageUrl;
    function getProfileImageUrl(){
        $.ajax({
            url:'/getProfileImageUrl',
            type:'GET',
            success:function(response){
               if(validateRequired(response)){
                   imageUrl = response;
                    $("#profilePic").attr('src',response)
                }
            }
        })
    }
    getPartialAccDetails(false);
    var partialAcc;
    function getPartialAccDetails(notBind){
        $.ajax({
            url:'/getPartialAccountDetails',
            type:'GET',
            success:function(response){
                if(response){
                   partialAcc = response;
                    if(notBind == false){

                    }else{
                        $('#firstName').val(response.firstName || '')
                        $('#lastName').val(response.lastName || '')
                        $('#publicProfileUrl').val(getValidRelatasIdentity(response.firstName+''+response.lastName)),
                        $('#emailId').val(response.emailId || '')
                    }
                }
            }
        })
    }

function storeToSession(){
    var data = formData()
    $.ajax({
        url:'/saveDetails',
        type:'POST',
        datatpe:'JSON',
        data:data,
        success:function(response){

        }
    })
}
    getSession()
    function getSession(){

        $.ajax({
            url:'/getSavedDetails',
            type:'GET',
            datatpe:'JSON',
            success:function(response){
                if(validateRequired(response)){

                    $('#firstName').val(response.firstName || '')
                    $('#lastName').val(response.lastName || '')
                    $('#publicProfileUrl').val(response.publicProfileUrl || '')
                    $('#emailId').val(response.emailId || '')

                    $('#password').val(response.password || '')

                }else{
                    getPartialAccDetails();
                }
            }
        })
    }
    $('#firstName').change(function(event){
        var text = $('#firstName').val().toLowerCase()
        var text2 = $('#lastName').val().toLowerCase()
        $("#publicProfileUrl").val(getValidRelatasIdentity(text+''+text2))
    })
    $('#lastName').change(function(event){
        var text = $('#firstName').val().toLowerCase()
        var text2 = $('#lastName').val().toLowerCase()
        $("#publicProfileUrl").val(getValidRelatasIdentity(text+''+text2))
    })

    $("#publicProfileUrl").focusout(function(){
        var text = $("#publicProfileUrl").val()
        checkIdentityAvailability(text)
    });
    var flag = false;
    function checkIdentityAvailability(identity){

        var details = {
            publicProfileUrl:getValidRelatasIdentity(identity)
        }

        $.ajax({
            url:'/checkUrl',
            type:'POST',
            datatype:'JSON',
            data:details,
            traditional:true,
            success:function(result){
                if(result == true){
                    $("#publicProfileUrl").val(getValidRelatasIdentity(identity))
                    i = 1;
                    if(flag){
                        flag = false;
                        showMessagePopUp("Your selected name is not available. Next best name suggested is:  "+identity+"",'tip')
                    }
                }
                else{

                    a(identity)

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }
    var i = 1;
    function a(identity){
        identity = getValidRelatasIdentity(identity)+''+i
        i++;
        flag = true;
        checkIdentityAvailability(identity)
    }


    function formData(){
        var mail = $('#emailId').val();
        var data={
            'firstName':$('#firstName').val(),
            'lastName':$('#lastName').val(),
            'publicProfileUrl':$('#publicProfileUrl').val(),
            'emailId':mail.toLowerCase(),
            'profilePicUrl':imageUrl || '/images/default.png',
            'password':$('#password').val()
        };

        return data;
    }

    // *************************** social login onfo
    function loadGoogleUserData(){
        $.ajax({
            url:'/googleUser',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(data){

                if(data == null || data == undefined  || data == '' || data.error){
                    $('#addGooleAccountText').text('abc@abc.com');
                    $('#addAnotherGoogleAccount1Text').text('Add another Gmail Account');
                    $('#addAnotherGoogleAccount2Text').text('Add another Gmail Account');
                    $('#addAnotherGoogleAccount3Text').text('Add another Gmail Account');
                }
                else{
                        googleUserData = data;
                        if (googleUserData[0]) {
                            $("#addGooleAccount").attr('checked','checked');
                            $('#addGooleAccountText').text(googleUserData[0].emails[0].value);
                        }
                        else{
                            $('#addGooleAccountText').text('abc@abc.com');
                            $("#addGooleAccount").attr('checked',false);
                        }
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }
    $("#primaryGmailAccount").on("click",function(){
        authenticateGoogle0()
    });


    $('#addGooleAccount').change(function(){
        if($('#addGooleAccount').prop('checked')) {
            authenticateGoogle0()
        }
        else{
            if(googleUserData[0]){
                showMessagePopUp("Prymary google account is mandatory",'error')

                $('#addGooleAccount').prop('checked',true);
            }
            else{
                googleUserData[0] = null;
                $.ajax({
                    url:'/removeGoogleUser/0',
                    type:'GET',
                    success:function(isSuccess){
                        loadGoogleUserData();
                    }
                });
            }

        }
    });

    function authenticateGoogle0(){
        storeToSession()
        if(validateRequired(googleUserData)){

        }else{

            window.location.replace('/googleEmailLoginStep1');
        }

    }

    //On click event on saveDetailsButton
    $('#saveDetailsButton').on("click",function(){
        var data = formData()
        onSaveFormData(data)

    });

    // Function to create total user info as json
    function createNewUser(data){

        var user = {
            'firstName':data.firstName,
            'lastName'  :data.lastName,
            'publicProfileUrl':data.publicProfileUrl,
            'screenName':data.publicProfileUrl,
            'emailId':data.emailId.toLowerCase(),
            'password':data.password,
            'registeredUser':true,
            'profilePicUrl':data.profilePicUrl,
            'serviceLogin':'relatas'
        };

        user.google=googleString()
        user.linkedin=linkedinString()
        user.facebook=facebookString()
        user.twitter=twitterString()

        return user;
    }

    function onSaveFormData(data){

        var result = validate(data);
        if(result == true){
            data.publicProfileUrl = getValidRelatasIdentity(data.publicProfileUrl)
            var details = {
                publicProfileUrl:data.publicProfileUrl
            }
            $.ajax({
                url:'/checkUrl',
                type:'POST',
                datatype:'JSON',
                data:details,
                traditional:true,
                success:function(result){
                    if(result == true){
                        checkEmailAddress(data)
                    }
                    else{
                        showMessagePopUp("Public profile url already exist in the Relatas, Please change it",'error');
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 30000
            })

        }else{
            result.focus();
        }
    }


    function checkEmailAddress(data){

           $.ajax({
               url:'/checkEmail/'+data.emailId+'/'+partialAcc._id,
               type:'GET',
               traditional:true,
               success:function(result){
                   if(result == true){
                       data = createNewUser(data);
                       data.userId = partialAcc._id;
                       nextStep(data);
                   }
                   else{
                       showMessagePopUp("Email address already exist in the Relatas, Please change it",'error');
                   }
               },
               error: function (event, request, settings) {

               },
               timeout: 30000
           })
    }

    function nextStep(data){

        if(validateRequired(googleUserData)){
            if(googleUserData[0]){
                if ($('#acceptTerms').prop('checked')) {
                    saveFormData(data);
                }
                else{
                    $('#acceptTerms').focus();
                    showMessagePopUp("Please accept terms and conditions",'error')
                }
            }
            else showMessagePopUp("Please add primary google account",'error')
        }
        else   showMessagePopUp("Please add primary google account",'error')
    }

var successFlag = false;
// Function to store form data in session
    function saveFormData(data){

        $.ajax({
            url: '/updatePartialAcc/relatas',
            type: 'POST',
            datatype: 'JSON',
            traditional: true,
            data: data,
            success: function (msg) {
                if (msg == true) {
                    identifyMixPanelUser(data)
                    successFlag = true;
                    showMessagePopUp("Congratulations your account has been created. Please add details to profile page and Connect your social networks to make your profile smart and productive.",'success');

                    mixpanelTrack("Signup Relatas");
                }
                else{
                    showMessagePopUp("Error occurred while saving, Please try again",'error')
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        });
    }
    function identifyMixPanelUser(profile){

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page":window.location.href
        });
        mixpanelIncrement("#Login");
    }
    $(".PhoneNo").bind("keypress", function (event) {
        if (event.charCode != 0) {
            var regex = new RegExp("^[0-9+-]*$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }
    });
    $('.PhoneNo').bind('copy paste cut', function (e) {
        e.preventDefault(); //disable cut,copy,paste
        //alert('cut,copy & paste options are disabled !!');
    });

    $('#profileDescription').bind("keypress",function(event){
        var text = $('#profileDescription').val()
        var words = text.split(/[\s]+/);

        if(words.length >= 400){
            event.preventDefault();
            showMessagePopUp("You exceeded max limit of 400 words.",'error')
            return false;
        }
    })
    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }
    function mixpanelIncrement(eventName){
        mixpanel.people.increment(eventName);
    }
    //Functions to validate required feilds
    function validate(data){

        if(!validateRequired(data.firstName.trim())){
            showMessagePopUp("Please provide First name",'error');
            return $('#firstName');
        }else if(!validateRequired(data.lastName.trim())){
            showMessagePopUp("Please provide Last name",'error');
            return $('#lastName');
        }else  if(!validateRelatasIdentity(data.publicProfileUrl.trim())){
            showMessagePopUp("Please provide valid Unique Relatas identity",'error');
            return $('#publicProfileUrl');
        } else  if(!validateRequired(data.emailId.trim())){
            showMessagePopUp("Please provide Email id",'error');
            return $('#emailId');
        }else if(!validateRequired(data.password.trim())){
            showMessagePopUp("Please provide password",'error');
            return $('#password');
        }else if (!validateEmailField(data.emailId)) {
            showMessagePopUp("Please provide valid email id",'error');
            return $('#emailId');
        }else return true;
    }


    function validateRelatasIdentity(rIdentity){
        var regex = new RegExp("^[a-zA-Z0-9._ ]*$");
        if(regex.test(rIdentity) && rIdentity != ''){
            return true;
        }
        else{
            return false;
        }
    }

    function getValidRelatasIdentity(rIdentity) {
        rIdentity = rIdentity.toLowerCase();
        rIdentity = rIdentity.replace(/\s/g,'');
        return rIdentity;
    }


    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {
            return true;
        } else {

            return false;
        }
    }


    // Function to validate required feild
    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }

    function showMessagePopUp(message,msgClass)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':'20%'})
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }
        //setTimeout(function(){
        $("#message").text(message)
        //},1000);
        $(".popover").focus();
    }

    $("body").on("click","#closePopup-message",function(){
        $("#user-name").popover('destroy');
        if(successFlag){
            successFlag = false;
            window.location.replace('/finalPage');
        }
    });

    // Function to generate valid json string for goole account
    function googleString(){

        var google ='';
        if (googleUserData == null || googleUserData == '' || googleUserData == undefined) {
            google +='{ "id":"",';
            google +='"token":"",';
            google +='"refreshToken":"",';
            google +='"name":""}';
        }
        else{
            for (var i = 0; i < googleUserData.length; i++) {
                if(googleUserData[i] == null || googleUserData[i]._json == undefined){}
                else{
                    var token=JSON.stringify(googleUserData[i].accessToken || '');
                    var id=JSON.stringify(googleUserData[i]._json.id || '');
                    var refreshToken=JSON.stringify(googleUserData[i].refreshToken || '');
                    var emailId=JSON.stringify(googleUserData[i].emails[0].value || '');
                    var name=JSON.stringify(googleUserData[i]._json.name || '');

                    google +='{ "id":'+id+',';
                    google +=' "token":'+token+',';
                    google +='"refreshToken":'+refreshToken+',';
                    google +='"emailId":'+emailId+',';
                    google +='"name":'+name+'';

                    if(i == googleUserData.length-1){
                        google +='}';
                    }
                    else{
                        google +='},';
                    }

                }
            }
        }
        var googleNew ="["+google+"]";
        return googleNew;
    }

    // Function to generate valid json string for linkedin account
    function linkedinString(){
        var linkedin='{';

            linkedin +='"id":"",';
            linkedin +='"token":"",';
            linkedin +='"emailId":"",';
            linkedin +='"name":""';

        linkedin +='}';
        return linkedin;
    }

// Function to generate valid json string for facebook account
    function facebookString(){
        var facebook='{';

            facebook +='"id":"",';
            facebook +='"token":"",';
            facebook +='"emailId":"",';
            facebook +='"name":""';

        facebook +='}';
        return facebook;
    }

    // Function to generate valid json string for twitter account
    function twitterString(){
        var twitter='{';

            twitter +='"id":"",';
            twitter +='"token":"",';
            twitter +='"refreshToken":"",';
            twitter +='"displayName":"",';
            twitter +='"userName":""';

        twitter +='}';
        return twitter;
    }
});
