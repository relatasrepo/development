/*

This file contain all functionality related to document viewer page

 */

$(document).ready(function(){

    $.ajaxSetup({ cache: false });

    var signUpFlag = false;
var sideHeight = $(document).height()
    $("#sidebarHeight").css({'min-height':sideHeight+61})
    $("#leftSidebarHeight").css({'min-height':sideHeight})
    var popupmsg;
    var pageNumberReaded;
    var pageReadDifference;

    var documentsSharedWithMe;
    var userProfile;
    var currentDocument;
    var pageNumbersReaded =[];
    var pageReadDiff = [];
    var loginFlage = false;
    $('#dashboard').on("click",function(){
        if(loginFlage){
            updateReadStatusOfDocument()
            window.location.replace('/dashboard');

        }
        else showMessagePopUp(popupmsg,'error');
    });
    var eFlag = true;
    $('#editProfile').on("click",function(){
        if(eFlag){
            updateReadStatusOfDocument()
            window.location.replace('/editProfile');

        }
        else showMessagePopUp(popupmsg,'error');
    });
    $('#calendar').on("click",function(){
        if(loginFlage){
            updateReadStatusOfDocument()
            window.location.replace('/calendar');

        }
        else showMessagePopUp(popupmsg,'error');
    });

    $('#myDocuments').on("click",function(){
        if(loginFlage){
            updateReadStatusOfDocument()
            window.location.replace('/docs');

        }
        else showMessagePopUp(popupmsg,'error');
    });
    $('#logout').on("click",function(){
        if(loginFlage){
            updateReadStatusOfDocument()
            window.location.replace('/logout');

        }
        else showMessagePopUp(popupmsg,'error');
    });
    $('#logoUrl').on("click",function(){
        if(loginFlage){
            updateReadStatusOfDocument()
            window.location.replace('/');

        }
        else showMessagePopUp(popupmsg,'error');
    });
    $('#doc').on("click",function(){
        if(loginFlage){
            updateReadStatusOfDocument()
            window.location.replace('/docs');

        }
        else showMessagePopUp(popupmsg,'error');
    });
isLoggedInUser();
    function isLoggedInUser(){
        $.ajax({
            url:'/isLoggedInUser',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){
                console.log("Logged In user:", result);
                
                if (result) {
                    loginFlage = true;
                    getUserProfile();
                }
                else{
                    $("#docCanvas").css({display:'none'})
                    $("#lightBoxPopup").css({display:'block'})
                    getNonUserInfo()
                    loginFlage = false;
                    eFlag = false;
                    $('#dashboard').attr("href","#")
                    $('#calendar').attr("href","#")
                    $('#myDocuments').attr("href","#")
                    $('#logout').attr("href","#")
                    $('#events').attr("href","#")
                    $('#contacts').attr("href","#")
                    $('#logoUrl').attr("href","#")
                    $('#editProfile').attr("href","#")
                    $('#doc').attr("href","#")
                    popupmsg = "Please signin to navigate further";

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 40000
        })
    };
/*******************************************************************************/
    $("[data-toggle='tooltip']").tooltip();
function getNonUserInfo(){
    $.ajax({
        url:'/nonUserInfo/docViewer',
        type:'GET',
        datatype:'JSON',
        traditional:true,
        success:function(result){
            if(result == true){
                $("#userEmailId").attr('readonly',false);
            }
            else if(validateRequired(result.emailId)){
                var firstName = result.firstName || '';
                var lastName = result.lastName || '';

                var UniqUrl = firstName.toLowerCase()+''+lastName.toLowerCase();
                $("#userEmailId").val(result.emailId);

                $("#userFirstName").val(firstName);
                $("#userLastName").val(lastName);
                $("#userPublicProfileUrl").val(UniqUrl || '');

            }
            else{
                $("#userEmailId").attr('readonly',false);
            }
        },
        error: function (event, request, settings) {


        },
        timeout: 20000
    })
}

    $('#userFirstName').change(function(event){
        var text = $('#userFirstName').val().toLowerCase()
        var text2 = $('#userLastName').val().toLowerCase()
        $("#userPublicProfileUrl").val(getValidRelatasIdentity(text+''+text2))
    })
    $('#userLastName').change(function(event){
        var text = $('#userFirstName').val().toLowerCase()
        var text2 = $('#userLastName').val().toLowerCase()
        $("#userPublicProfileUrl").val(getValidRelatasIdentity(text+''+text2))
    })

    $("#userPublicProfileUrl").focusout(function(){
        var text = $("#userPublicProfileUrl").val()
        checkIdentityAvailability(text)
    });
    var flagi = false;
    function checkIdentityAvailability(identity){

        var details = {
            publicProfileUrl:getValidRelatasIdentity(identity)
        }

        $.ajax({
            url:'/checkUrl',
            type:'POST',
            datatype:'JSON',
            data:details,
            traditional:true,
            success:function(result){
                if(result == true){
                    $("#userPublicProfileUrl").val(getValidRelatasIdentity(identity))
                    identityFlag = 1;
                    if(flagi){
                        flagi = false;
                        showMessagePopUp("Your selected name is not available. Next best name suggested is:  "+identity+"",'tip')
                    }
                }
                else{

                    a(identity)

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }


    //On click event on saveDetailsButton
    $('#saveDetailsButton').on("click",function(){
        var data = formData()
        onSaveFormData(data)

    });

    function formData(){
        var mail = $('#userEmailId').val();

        var data={
            'firstName':$('#userFirstName').val() || '',
            'lastName':$('#userLastName').val() || '',
            'publicProfileUrl':$('#userPublicProfileUrl').val() || '',
            'emailId':mail.toLowerCase() || '',
            'profilePicUrl':'/images/default.png',
            'password':$('#userPassword').val() || ''
        };

        return data;
    }


    function onSaveFormData(data){
        var result = validate(data);
        if(result == true){
            data.publicProfileUrl = getValidRelatasIdentity(data.publicProfileUrl)
            var details = {
                publicProfileUrl:data.publicProfileUrl
            }
            $.ajax({
                url:'/checkUrl',
                type:'POST',
                datatype:'JSON',
                data:details,
                traditional:true,
                success:function(result){
                    if(result == true){
                        checkEmailAddress(data)
                    }
                    else{
                        showMessagePopUp("Public profile url already exist in the Relatas, Please change it",'error');
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 30000
            })

        }else{
            result.focus();
        }
    }


    function checkEmailAddress(data){

        $.ajax({
            url:'/checkEmail/'+data.emailId,
            type:'GET',
            traditional:true,
            success:function(result){
                if(result == true){
                    data = createNewUser(data)
                    if ($('#acceptTerms').prop('checked')) {
                          saveFormData(data);
                    }
                    else{
                          $('#acceptTerms').focus();
                          showMessagePopUp("Please accept terms and conditions",'error')
                    }
                }
                else{
                    showMessagePopUp("Email address already exist in the Relatas, Please change it",'error');
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })

    }

    function createNewUser(data){
        var user = {
            'firstName':data.firstName,
            'lastName'  :data.lastName,
            'publicProfileUrl':data.publicProfileUrl,
            'screenName':data.publicProfileUrl,
            'emailId':data.emailId.toLowerCase(),
            'password':data.password,
            'registeredUser':false,
            'profilePicUrl':data.profilePicUrl || '/images/default.png',
            'serviceLogin':'relatas',
            'public': true,
            'docView':true
        };

        user.google=googleString()
        user.linkedin=linkedinString()
        user.facebook=facebookString()
        user.twitter=twitterString()

        return user;

    }

    var successFlag = false;
// Function to store form data in session
    function saveFormData(data){

        $.ajax({
            url: '/registerAccount',
            type: 'POST',
            datatype: 'JSON',
            traditional: true,
            data: data,
            success: function (msg) {
                if (msg == true) {
                    signUpFlag = true;
                    successFlag = true;
                    showMessagePopUp("Congratulations your account has been created. Please add details to profile page and Connect your social networks to make your profile smart and productive.",'success')

                }
                else{
                    showMessagePopUp("Error occurred while saving, Please try again",'error')

                }

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        });
    }

    var identityFlag = 1;
    function a(identity){
        identity = getValidRelatasIdentity(identity)+''+identityFlag
        identityFlag++;
        flagi = true;
        checkIdentityAvailability(identity)
    }

    function getValidRelatasIdentity(rIdentity) {
        rIdentity = rIdentity.toLowerCase();
        rIdentity = rIdentity.replace(/\s/g,'');
        return rIdentity;

    }

    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {
            return true;
        } else {

            return false;
        }
    }

    function validateRelatasIdentity(rIdentity){

        var regex = new RegExp("^[a-zA-Z0-9._ ]*$");
        if(regex.test(rIdentity) && rIdentity != ''){
            return true;

        }
        else{

            return false;
        }


    }

    //Functions to validate required feilds
    function validate(data){
        if(!validateRequired(data.firstName.trim())){
            showMessagePopUp("Please provide First name",'error');
            return $('#userFirstName');
        }else if(!validateRequired(data.lastName.trim())){
            showMessagePopUp("Please provide Last name",'error');
            return $('#userLastName');
        }else  if(!validateRelatasIdentity(data.publicProfileUrl.trim())){
            showMessagePopUp("Please provide valid Unique Relatas identity",'error');
            return $('#userPublicProfileUrl');
        } else  if(!validateRequired(data.emailId.trim())){
            showMessagePopUp("Please provide Email id",'error');
            return $('#userEmailId');

        }else if(!validateRequired(data.password.trim())){
            showMessagePopUp("Please provide password",'error');
            return $('#userPassword');

        }else if (!validateEmailField(data.emailId)) {
            showMessagePopUp("Please provide valid email id",'error');
            return $('#userEmailId');
        }
        else{
           return true;
        }
    }
/***************************************************************************************/


    // Function to get user contacts info
    function getUserProfile(signUp){
        $.ajax({
            url:'/userProfile',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(result){
                console.log("User profile:", result);
                $("#docCanvas").css({display:'block'})
                $("#lightBoxPopup").css({display:'none'})
                userProfile = result;
                if(result){
                    identifyMixPanelUser(result,signUp);
                }
                $('#profilePic').attr('src', result.profilePicUrl || '/images/default.png');
                imagesLoaded("#profilePic",function(instance,img) {
                    if(instance.hasAnyBroken){
                        $('#profilePic').attr("src","/images/default.png");
                    }
                });
                getRequestedDocument();
                if(result.registeredUser == false){
                    loginFlage = false;
                    eFlag = true;
                    $('#dashboard').attr("href","#")
                    $('#calendar').attr("href","#")
                    $('#myDocuments').attr("href","#")
                    $('#logout').attr("href","#")
                    $('#events').attr("href","#")
                    $('#contacts').attr("href","#")
                    $('#logoUrl').attr("href","#")
                    $('#editProfile').attr("href","/editProfile")
                    $('#doc').attr("href","#")
                    popupmsg = 'Please complete your profile by clicking on "Profile" to navigate to the other sections of Relatas';
                }
                else{
                    loginFlage = true;
                    eFlag = true;
                    $('#dashboard').attr("href","/dashboard")
                    $('#calendar').attr("href","/calendar")

                    $('#logout').attr("href","/logout")
                    $('#events').attr("href","#")
                    $('#contacts').attr("href","/contacts")
                    $('#logoUrl').attr("href","/")
                    $('#editProfile').attr("href","/editProfile")
                    $('#doc').attr("href","/docs")
                }
            },
            error: function (event, request, settings) {


            },
            timeout: 20000
        })
    }

    function identifyMixPanelUser(profile,signUp){

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page":window.location.href
        });
        if(signUp){
            mixpanelIncrement("#Login");
        }
        if(signUpFlag){
            signUpFlag = false;
            mixpanelTrack("Signup Relatas");
        }
    }
    var fileDocs;
    var i=0;
    var date1, date2,start,end;
    var pageDifference = []
    var currentFile;

    //
    // Disable workers to avoid yet another cross-origin issue (workers need the URL of
    // the script to be loaded, and currently do not allow cross-origin scripts)
    //

    function removeDuplicates(arr){

        var end = arr.length;

        for(var i = 0; i < end; i++)
        {
            for(var j = i + 1; j < end; j++)
            {
                if(arr[i] == arr[j])
                {
                    var shiftLeft = j;
                    for(var k = j+1; k < end; k++, shiftLeft++)
                    {
                        arr[shiftLeft] = arr[k];
                    }
                    end--;
                    j--;
                }
            }
        }

        var whitelist = [];
        for(var i = 0; i < end; i++){
            whitelist.push(arr[i]);
        }
        return whitelist;
    }

    PDFJS.disableWorker = true;

    var readedPages;

    var pdfDoc = null,
        pageNum = 1,
        pageRendering = false,
        pageNumPending = null,
        scale = 0.9,
        canvas = document.getElementById('the-canvas'),
        ctx = canvas.getContext('2d');
    var flag = true;
    /**
     * Get page info from document, resize canvas accordingly, and render page.
     * @param num Page number.
     */
    function zoom(){

    }


    var lAccess;
    function renderPage(num) {
        $("#"+num).css({color:'#0382EA'});
        readedPages.push(num)
        readedPages = removeDuplicates(readedPages)

        pageRendering = true;
        // Using promise to fetch the page
        pdfDoc.getPage(num).then(function(page) {
            var viewport = page.getViewport(scale);
            canvas.height = viewport.height;
            canvas.width = viewport.width;


              lAccess = new Date()
            if (flag) {
                flag = false
                date1 = new Date()
                start = date1;

            }

            // Render PDF page into canvas context
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);

            // Wait for rendering to finish
            renderTask.promise.then(function () {
                pageRendering = false;
                if (pageNumPending !== null) {
                    // New page rendering is pending
                    renderPage(pageNumPending);
                    pageNumPending = null;
                }
            });
        });

        // Update page counters
        document.getElementById('page_num').textContent = pageNum;
        document.getElementById('page_numBottom').textContent = pageNum;
    }

    /**
     * If another page rendering in progress, waits until the rendering is
     * finised. Otherwise, executes rendering immediately.
     */
    function queueRenderPage(num) {
        if (pageRendering) {
            pageNumPending = num;
        } else {
            renderPage(num);
        }
    }

    /**
     * Displays previous page.
     */
    function onPrevPage() {
        var date = date1
        date1 = new Date()
        end = date1
        var diff = date1.valueOf() - date.valueOf();
        var diffInSeconds = diff/1000;

        pageNumberReaded = pageNum;
        pageReadDifference = diffInSeconds;

        updateReadStatusOfDocument(pageNumberReaded,pageReadDifference);

        if (pageNum <= 1) {
            return;
        }
        pageNum--;
        queueRenderPage(pageNum);
    }
    document.getElementById('prev').addEventListener('click', onPrevPage);

    /**
     * Displays next page.
     */
    function onNextPage() {
        if (pageNum >= pdfDoc.numPages) {
            return;
        }

        var date = date1
        date1 = new Date()
        end = date1
        var diff = date1.valueOf() - date.valueOf();
        var diffInSeconds = diff/1000;

        pageNumberReaded = pageNum;
        pageReadDifference = diffInSeconds;

        updateReadStatusOfDocument(pageNumberReaded,pageReadDifference);

        pageNumbersReaded.push(pageNum);
        pageReadDiff.push(diffInSeconds);

        pageDifference.push(0)

        pageNum++;
        queueRenderPage(pageNum);
    }
    document.getElementById('next').addEventListener('click', onNextPage);

    /**
     * Asynchronously downloads PDF.
     */



    function loadDoc(docUrl){
        console.log("Document Url:", docUrl);
        PDFJS.getDocument(docUrl).then(function (pdfDoc_) {
            mixpanelTrack("Read Document");
            flag = true
            readedPages = []
            date1 = null
            date2 = null
            start = null
            end = null
            pageDifference = []
            pdfDoc = pdfDoc_;
            document.getElementById('page_count').textContent = pdfDoc.numPages;
            document.getElementById('page_countBottom').textContent = pdfDoc.numPages;
            displayPageList(pdfDoc.numPages)
            // Initial/first page rendering
            renderPage(pageNum);
        });
    }

    var analysisFlag = true;
    function getRequestedDocument(){
        $.ajax({
            url:'/loadDocument',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(document){
                console.log("Document from server", document);

                if(document == 'notAuthorised'){

                    showMessagePopUp("Either you don't have access or your access to this document has expired. Please request the owner to give you access.",'error')
                }else
                if(document){

                    if(validateRequired(document.sharedBy))
                    getDocSenderProfile(document.sharedBy.userId);

                    var docDate = new Date(document.sharedDate)
                    var month = docDate.getMonth()+1;
                    $("#docUploadedOn").text(getDateFormat(docDate));   
                        showDocumentExpiryDate(document)
                    $('#currentDocument').text(document.documentName);
                    currentFile = document.documentName;
                    currentDocument = document;
                    if(document.access == 'public'){
                        analysisFlag = false;
                    }
                    loadDoc(document.documentUrl);
                }
                else{
                    showMessagePopUp("Loading document failed",'error');
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

function showDocumentExpiryDate(document){
        var userId = userProfile._id;
        for (var i in document.sharedWith){
                if(userId == document.sharedWith[i].userId){
                    $("#expiryDate").text(validateRequired(document.sharedWith[i].expiryDate) ? getDateFormat(new Date(document.sharedWith[i].expiryDate)) : 'None')
                }
        }
}

    function getDocSenderProfile(docUploaderUserId){
        var user = {
            id:docUploaderUserId
        }
        $.ajax({
            url:'/getProfileUsingId',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(profile){
                if(profile != null && profile != undefined){
                    $("#docUploadedByImage").attr('src',profile.profilePicUrl);
                    imagesLoaded("#docUploadedByImage",function(instance,img) {
                        if(instance.hasAnyBroken){
                            $('#docUploadedByImage').attr("src","/images/default.png");
                        }
                    });
                    $("#docUploadedBy").text(profile.firstName);
                    $("#publicProfileUrl").attr('href','/'+getValidUniqueUrl(profile.publicProfileUrl));
                }
            }
        })
    }

    function getValidUniqueUrl(uniqueName){
        var url = window.location+''.split('/');
        var patt = new RegExp(url[2]);
        if(patt.test(uniqueName)){

            url = uniqueName.split('/');
            for (var i = url.length - 1; i >= 0; i--) {
                if (url[i] == 'u') {
                    uniqueName = url[i+1];
                }
            }
            return uniqueName;

        }else {
            return uniqueName;
        }
    }

var count = true;
    // Function to update read status
    function updateReadStatusOfDocument(pageNumberReadedThis,pageReadDifferenceThis){
        if(currentDocument.sharedBy.userId != userProfile._id) {
            if (validateRequired(pageNumberReadedThis) && validateRequired(pageReadDifferenceThis)) {

                var data = {
                    pageNumberReaded: pageNumberReadedThis || pageNumberReaded,
                    pageReadDiff: pageReadDifferenceThis || pageReadDifference,
                    firstAccessed: start,
                    lastAccessed: lAccess || start,
                    count: count,
                    documentId: currentDocument._id,
                    userId: userProfile._id,
                    lUserId:currentDocument.sharedBy.userId
                };

                if (analysisFlag) {
                    $.ajax({
                        url: '/updateReadStatusOfDocument',
                        type: 'POST',
                        datatype: 'JSON',
                        data: data,
                        traditional: true,
                        success: function (result) {
                            count = false;
                        },
                        error: function (event, request, settings) {

                        },
                        timeout: 20000
                    })
                }
            }else onNextPage();
        }
    }

    function displayPageList(totalPages){
            for(var i=1;i<=totalPages;i++){
                var page = 'Page  '+i;
                var text = '<li id='+i+' >'+page+'</li>';
                $("#pages").append(text)
            }
    }

    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }
    function mixpanelIncrement(eventName){
        mixpanel.people.increment(eventName);
    }

    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    function getDateFormat(date){
        var day = date.getDate();
        var month = date.getMonth() ;
        var year = date.getFullYear();

       // if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var dateFormat = day + "-" + monthNameSmall[month] + "-" +year;
        return dateFormat;
    }
    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }

    function showMessagePopUp(message,msgClass)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':'9%'})
        $(".popover").css({'margin-left':'-7%'})
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }
        //setTimeout(function(){
        $("#message").text(message)
        //},1000);
        $("#closePopup-message").focus();
    }

    $("body").on("click","#closePopup-message",function(){

        $("#user-name").popover('destroy');
        if(successFlag){
            successFlag = false;
            getUserProfile(true);
        }
    });


    $("#prevBottom").on("click",function(){
        $("#prev").trigger("click");
    });
    $("#nextBottom").on("click",function(){
        $("#next").trigger("click");
    });

    $('#loginUsingGoogle').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenDocumentViewer');
    });

    $('#loginUsingLinkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        window.location.replace('/linkedinLoginDocumentViewer');
    });

    $('#loginUsingGoogle2').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenDocumentViewer');
    });

    $('#loginUsingLinkedin2').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        window.location.replace('/linkedinLoginDocumentViewer');
    });

    $('#login-with-google').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenDocumentViewer');
    });
    $('#sign-up-with-google').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenDocumentViewer');
    });

    $('#login-with-linkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        window.location.replace('/linkedinLoginDocumentViewer');
    });
    $('#sign-up-with-linkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn")

        window.location.replace('/linkedinLoginDocumentViewer');
    });


    $('#loginFormButton').on('click',function(){
        var emailId1 = $("#emailId2").val();
        var password1 = $("#password").val();

        if (validateRequired(emailId1) && validateRequired(password1)) {
            emailId1 = emailId1.toLowerCase();
            var credentials={
                emailId:emailId1,
                password:password1
            }

            $.ajax({
                url:'/loginFromPublicProfile',
                type:'POST',
                datatype:'JSON',
                data:credentials,
                traditional:true,
                success:function(result){
                    if (result) {
                            getUserProfile();

                    }
                    else{
                        showMessagePopUp("Looks like either your email or password is incorrect. Please try again.",'error')
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            })
        }
        else{
            showMessagePopUp("Please enter your credentials",'error');
        }
    });


    // Function to generate valid json string for goole account
    function googleString(){

        var google ='';

            google +='{ "id":"",';
            google +='"token":"",';
            google +='"refreshToken":"",';
            google +='"name":""}';

        var googleNew ="[]";
        return googleNew;
    }

    // Function to generate valid json string for linkedin account
    function linkedinString(){
        var linkedin='{';

        linkedin +='"id":"",';
        linkedin +='"token":"",';
        linkedin +='"emailId":"",';
        linkedin +='"name":""';

        linkedin +='}';
        return linkedin;
    }

// Function to generate valid json string for facebook account
    function facebookString(){
        var facebook='{';

        facebook +='"id":"",';
        facebook +='"token":"",';
        facebook +='"emailId":"",';
        facebook +='"name":""';

        facebook +='}';
        return facebook;
    }

    // Function to generate valid json string for twitter account
    function twitterString(){
        var twitter='{';

        twitter +='"id":"",';
        twitter +='"token":"",';
        twitter +='"refreshToken":"",';
        twitter +='"displayName":"",';
        twitter +='"userName":""';

        twitter +='}';
        return twitter;
    }

});