/**
 * Created by naveen on 11/6/16.
 */

calendarRouter.controller("left_menu_bar", function($scope){
    if((/today/.test(window.location.pathname))){
        $scope.sideNavLanding = 'sidebar-highlight';
    } else if((/insights/.test(window.location.pathname))){
        $scope.sideNavInsights = 'sidebar-highlight';
    }else if((/customer/.test(window.location.pathname))){
        $scope.sideNavAccounts = 'sidebar-highlight';
    }else if((/help/.test(window.location.pathname))){
        $scope.sideNavHelp = 'sidebar-highlight';
    }else if((/contact/.test(window.location.pathname))){
        $scope.sideNavContacts = 'sidebar-highlight';
    }else if((/settings/.test(window.location.pathname))){
        $scope.sideNavHelp = 'sidebar-highlight';
    }else if((/people/.test(window.location.pathname))){
        $scope.sideNavInsights = 'sidebar-highlight';
    }else if((/opportunities/.test(window.location.pathname))){
        $scope.sideNavOpp = 'sidebar-highlight';
    }
});
