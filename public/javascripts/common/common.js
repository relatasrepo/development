/**
 * Created by naveen on 10/8/15.
 */

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

var nl2br = function (str, isXhtml) {

    if (typeof str === 'undefined' || str === null) {
        return ''
    }

    var breakTag = (isXhtml || typeof isXhtml === 'undefined') ? '<br ' + '/>' : '<br>'

    return (str + '')
        .replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2')
}

function enumerateDaysBetweenDates(startDate,endDate) {

    var start = moment(startDate)
    var end = moment(endDate)

    var now = start, dates = [];

    while (now <= end) {
        var date = now;
        date = date.add(2,'d')
        dates.push({
            month:date.format("MMM"),
            monthYear:date.format("MMM YYYY"),
            sortDate:new Date(date)
        });

        now.add(1,'months');
    }

    return dates;
};

function getBrowserLocation(callback) {

    if (navigator.geolocation && !_.includes(window.location.hostname, "localhost")) {
        navigator.geolocation.getCurrentPosition(function (location) {
            callback({
                lat:location.coords.latitude,
                lng:location.coords.longitude
            })
        });
    } else {
        callback("Geolocation is not supported.");
    }
}

function getPosition (position){
    return {
        lat:position.coords.latitude,
        lng:position.coords.longitude
    }
}

function isTestingEnvironment() {
    var domain = window.location.hostname;

    var localDomain = 'localhost';
    var showcaseDomain = 'showcase';
    var exampledevDomain = 'exampledev';
    var relatasDomain = 'relatas.relatas';

    var isTestingEnv= false;
    if((domain.indexOf(localDomain) > -1) || (domain.indexOf(showcaseDomain) > -1) || (domain.indexOf(exampledevDomain) > -1) || (domain.indexOf(relatasDomain) > -1)) {
        isTestingEnv= true;
    }

    return isTestingEnv;
}

var stringToColour = function(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}

function getTextLength(text,maxLength){
    if(!checkRequired(text))
        return "";

    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

function setTimeOutCallback(delay,callback){
    setTimeout(function () {
        callback()
    },delay)
}

function checkRequired(data){
    if (data === '' || data === null || data === undefined || data === "undefined") {
        return false;
    }
    else{
        return true;
    }
}

function get_connection_interactions_tweets($http,personId,twitterUserName,uName,name,designation,companyName,filter,location,meetingDate,meetingTitle,callback) {

    var url = "/contact/connections/interactions?id="+personId+"&skip=0&limit=30"+'&twitterUserName='+twitterUserName

    if(personId) {
        $http.get(url)
            .success(function (response) {

                var lastInteractionType = null;
                var subject = '';
                var over = '';
                var messageReConnect = '';
                var dateOnly = '';

                if(response && response.SuccessCode){

                    uName = uName?uName:response.Data.profile.publicProfileUrl;
                    designation = response.Data.profile.designation;
                    companyName = response.Data.profile.companyName;
                    var fullName = response.Data.profile.firstName +" "+ response.Data.profile.lastName
                    var calendarLink = " My calendar is readily available here: "+"www.relatas.com/"+uName

                    if(filter == "travellingToLocation"){
                        subject = "In "+location+" on "+meetingDate+". ";
                        messageReConnect = getMessageForTravellingTo(response.Data,meetingDate,location)
                    }

                    if(filter == "peopleNearMeetingLocation"){
                        subject = "In "+location+" today";
                        messageReConnect = getMessageForTravellingTo(response.Data,meetingDate,location)
                    }

                    if(!filter || filter == "losingTouch") {
                        subject = "Reconnecting "+response.Data.profile.firstName +" & "+name
                        messageReConnect = getMessageForLosingTouch(response.Data);

                        // messageReConnect = messageReConnect+calendarLink
                        messageReConnect = messageReConnect;
                    }

                    if(filter == "meetingFollowUp"){
                        subject = "Meeting Minutes : "+meetingTitle
                        var mfuMsg = ". \nHere are some of the meeting notes for your ready reference ";

                        if(lastInteractionType){
                            over = "over "+lastInteractionType
                        }

                        messageReConnect = "It was nice catching up with you "+over+dateOnly+" for our discussion on "+
                            meetingTitle+mfuMsg+". " +"\n<"+"please add meeting notes>"

                        if(meetingDate){
                            var endDate = moment();
                            var daysSinceMet = endDate.diff(new Date(meetingDate), 'days');

                            var formatMeetingDate = moment(meetingDate).format('DD MMM YYYY')
                            messageReConnect = "It was nice catching up with you "+over+ "on "+formatMeetingDate+" for our discussion on "+
                                meetingTitle+mfuMsg+". " +"\n<"+"please add meeting notes>"
                        }
                    }

                } else {
                    messageReConnect = "\nIt's been long since we last interacted. How have you been?" +
                        // " It would be great to find some time to catch up."+calendarLink;
                        " It would be great to find some time to catch up.";
                }

                messageReConnect = "Hi "+name+",\n"+messageReConnect+"\n\n"+getSignature(fullName,designation,companyName,uName)

                callback(response,{
                    messageReConnect:messageReConnect,
                    subject:subject,
                    disableSendBtn:false
                })
            });
    } else {
        callback(null,{
            messageReConnect:"Please add contact's email ID to send a mail through Relatas.",
            subject:'',
            disableSendBtn: true
        })
    }
}

function getSignature(fullName,designation,companyName,uName) {
    return "---\n"+fullName+"\n"+designation+", "+companyName+"\n"+"www.relatas.com/"+uName+'\n\n Powered by Relatas';
}

function getUserSignature(fullName,designation,companyName,domain,publicProfileUrl) {
    return "---\n"+fullName+"\n"+designation+", "+companyName+"\n"+domain+"/"+publicProfileUrl+'\n\n Powered by Relatas';
}

function numberWithCommas(x,ins) {

    if(ins){
        return numberWithCommas_ins(x);
    } else {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

function numberWithCommas_ins(x) {
    x=String(x).toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'),x.length);
    x = Math.floor(x);
    x=x.toString();
    var lastThree = x.substring(x.length-3);
    var otherNumbers = x.substring(0,x.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
}

function setQuarter(startMonth,timezone) {

    var qtrObj = {},
        array = [],
        months = [],
        currentYr = new Date().getFullYear(),
        currentMonth = new Date().getMonth(),
        fyStartDate = new Date(moment().startOf('month'));

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    var twelve_months_for_loop = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November"];

    fyStartDate.setMonth(twelve_months_for_loop.indexOf(startMonth))

    if(currentMonth<twelve_months_for_loop.indexOf(startMonth)){
        fyStartDate.setFullYear(currentYr-1)
    }

    months.push(fyStartDate)

    _.each(twelve_months_for_loop,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    qtrObj.quarter1 = {start:moment(months[0]).tz(timezone).startOf('month').format(),end:moment(months[2]).tz(timezone).endOf('month').format()}
    qtrObj.quarter2 = {start:moment(months[3]).tz(timezone).startOf('month').format(),end:moment(months[5]).tz(timezone).endOf('month').format()}
    qtrObj.quarter3 = {start:moment(months[6]).tz(timezone).startOf('month').format(),end:moment(months[8]).tz(timezone).endOf('month').format()}
    qtrObj.quarter4 = {start:moment(months[9]).tz(timezone).startOf('month').format(),end:moment(months[11]).tz(timezone).endOf('month').format()}

    var currentQuarter = "quarter4"

    if(startMonth =='January'){

        if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    if(startMonth =='April'){

        if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    array.push(qtrObj.quarter1,qtrObj.quarter2,qtrObj.quarter3,qtrObj.quarter4)
    return {array:array,obj:qtrObj,currentQuarter:currentQuarter};
}

function getMessageForLosingTouch(data) {
    var message = ''

    if(data.ccLastInteraction[0]){
        var fName = data.ccLastInteraction[0].firstName?data.ccLastInteraction[0].firstName:''
        var lName = data.ccLastInteraction[0].lastName?data.ccLastInteraction[0].lastName:''
        var ccLastInteractedDate = moment(data.ccLastInteraction[0].interactionDate).tz("UTC").format("DD MMM YYYY")

        if(data.commonConnections>100 && data.latestTweet) {
            message = 'Just saw your tweet " '+ stripHTMLTags(data.latestTweet.tweet) + ' " and remembered that we last '+
                getInteractionTypeResponse(data.lastInteraction)+
                ". We should catch-up, there's been much going on at my front. \n\n"+
                'I recently interacted with '+fName+' '+lName+' on '+ccLastInteractedDate+' and remembered we share a common connection. ' +
                'How about catching up sometime soon?'
        } else if(data.commonConnections>0 && !data.latestTweet){
            message = 'I recently interacted with '+fName+' '+lName+' on '+ccLastInteractedDate+' and remembered we share a common connection.\n\n'+
                'You and I last '+ getInteractionTypeResponse(data.lastInteraction)+" and it's time for us to catch up again. How about catching up sometime soon?"
        } else if(data.latestTweet){
            message = 'Just saw your tweet " '+ stripHTMLTags(data.latestTweet.tweet) + ' " and remembered that we last '+
                getInteractionTypeResponse(data.lastInteraction)+
                ". We should catch-up, there's been much going on at my front. \n\n"
        } else {
            message = "It's been sometime since we last "+getInteractionTypeResponse(data.lastInteraction)+
                " and then there's been silence from my end. Well, all good things must come to an end, so here's my email breaking the silence. :-)"
        }
    } if(data.lastInteraction[0]){
        message = "It's been sometime since we last "+getInteractionTypeResponse(data.lastInteraction)+
            " and then there's been silence from my end. Well, all good things must come to an end, so here's my email breaking the silence. :-)"
    }  else {
        message = "It's been a long time since we last interacted"+". How about catching up sometime soon?"
    }

    return message;
}

function getMessageForTravellingTo(data,meetingDate,location) {

    var message = '';
    var ccMessage ='';

    if(data.ccLastInteraction[0]){

        var fName = data.ccLastInteraction[0].firstName?data.ccLastInteraction[0].firstName:''
        var lName = data.ccLastInteraction[0].lastName?data.ccLastInteraction[0].lastName:''
        var ccLastInteractedDate = moment(data.ccLastInteraction[0].interactionDate).tz("UTC").format("DD MMM YYYY")

        message = "It's been sometime since we last "+getInteractionTypeResponse(data.lastInteraction)+
            ". How have things been at your end?\n\n"+
            "Are you around "+location+" on "+meetingDate+"? We could catch up over coffee.\n\n"

        if(data.commonConnections>0 && data.ccLastInteraction[0]){
            ccMessage = "Also, I noticed that we have "+data.commonConnections+" friends in common. Do you know "+fName+" "+lName+" well?\n\n"+
                "Let me know if we could catch up."
        }
    } else if(data.lastInteraction && data.lastInteraction[0]) {

        var lastIntDt = moment(data.lastInteraction[0].interactionDate).tz("UTC").format("DD MMM YYYY")
        message = "It's been sometime since we last interacted on "+lastIntDt+" and it's time for us to catch up again. How about catching up sometime soon?"
    } else {
        message = "It's been more than six months since we last interacted "+" and it's time for us to catch up again. How about catching up sometime soon?"
    }

    var subject = "In "+location+" on "+meetingDate+". ";

    return message+ccMessage;
}

function getInteractionTypeResponse(interaction) {

    if(interaction && interaction[0] && interaction[0].interactionDate){

        var interactionDate = moment(interaction[0].interactionDate).tz("UTC").format("DD MMM YYYY")

        if(interaction[0].type === 'email'){
            return 'exchanged emails on '+interactionDate+""
        } else if(interaction[0].type === 'call'){
            return 'spoke on '+interactionDate+""
        } else if(interaction[0].type === 'meeting'){
            return 'met on '+interactionDate+" "
        } else if(interaction[0].type === 'sms'){
            return 'exchanged SMS on '+interactionDate+""
        } else {
            return 'interacted '
        }
    } else {
        return 'interacted '
    }
}

function stripHTMLTags(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

function removeDuplicates(arr) {
    var end = arr.length;

    for (var i = 0; i < end; i++) {
        for (var j = i + 1; j < end; j++) {
            if (arr[i].pageNumber == arr[j].pageNumber) {
                var shiftLeft = j;
                for (var k = j + 1; k < end; k++, shiftLeft++) {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for (var i = 0; i < end; i++) {
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

var findOne = function (haystack, arr) {
    return arr.some(function (v) {
        return haystack.indexOf(v) >= 0;
    });
};

function getMaxOfArray(numArray) {
    return Math.max.apply(null, numArray);
}

function getMinOfArray(numArray) {
    return Math.min.apply(null, numArray);
}

function scaleBetween(unscaledNum, min, max,minAllowed,maxAllowed) {

    if(!minAllowed){
        minAllowed = 0;
    }

    if(!maxAllowed) {
        maxAllowed = 100;
    }

    if ((min == 0 && max == 0) || (min == max)) {
        return minAllowed
    } else {
        return (maxAllowed - minAllowed) * (unscaledNum - min) / (max - min) + minAllowed;
    }
}

function sessionTimeoutReload() {
    window.location = "/"
}

function getInteractionIconType(interactionType) {
    switch (interactionType){
        case 'meeting': return 'fa fa-calendar-check-o margin0';break;
        case 'email': return 'fa fa-envelope margin0';break;
        case 'call': return 'fa fa-phone margin0';break;
        case 'sms': return 'fa fa-reorder margin0';break;
        case 'twitter': return 'fa fa-twitter-square margin0';break;
    }
}

function fetchCompanyFromEmail(email){

    if(email){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            return removableTextList.indexOf(word.trim()) > -1
        })
        words = _.sortBy(words, function(word){return -(word.length);})

        return (personalMailDomains.indexOf(words[0]) > -1) ? "Others" : (words[0])
    } else {
        return null;
    }
}

function standardDateFormat(){
    return "DD MMM YYYY"
}

function oppTargetMetNotifications($http,$scope){

    // $http.get("/opportunities/by/month/year")
    //     .success(function (response) {
    //
    //         if(response.SuccessCode){
    //             var receivers = [];
    //             var salutation = "Hi "
    //             if(response.companyHead){
    //                 receivers.push(response.companyHead.emailId)
    //                 salutation = salutation+response.companyHead.firstName
    //             }
    //             if(response.reportingManager){
    //                 receivers.push(response.reportingManager.emailId)
    //                 salutation = salutation+response.reportingManager.firstName
    //             }
    //         }
    //
    //     });
}

function getErrorMessages($rootScope,$http,page,callback){

    $http.get('/error/messages/'+page)
        .success(function (response) {
            $rootScope.errorMessages = response.Data;
            if(callback){
                callback(response.Data)
            }
        })
}

function getLiuProfile($scope, $http, share,$rootScope,callback){

    $http.get('/profile/get/current/web')
        .success(function (response) {

            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $scope.l_usr.initials = response.Data.firstName.substring(0,2);
                if(!imageExists($scope.l_usr.profilePicUrl)){
                    $scope.l_usr.imageExists = true;
                }

                share.l_user = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.appConfig = response.appConfig;
                share.liuData = response.Data;

                $rootScope.currentUser = response.Data;

                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }

                share.loadAppConfig = response.appConfig;
                share.lUseEmailId = response.Data.emailId;

                if(response.companyDetails && response.companyDetails.opportunitySettings){
                    $rootScope.isProductRequired = response.companyDetails.opportunitySettings.productRequired;
                    $rootScope.isRegionRequired = response.companyDetails.opportunitySettings.regionRequired;
                    $rootScope.isVerticalRequired = response.companyDetails.opportunitySettings.verticalRequired;
                    $rootScope.isSourceRequired = response.companyDetails.opportunitySettings.sourceRequired;
                }

                $rootScope.dmSelectionRequired = false;
                $rootScope.inflSelectionRequired = false;
                $rootScope.partnerSelectionRequired = false;

                if(response.companyDetails && response.companyDetails.customerSetting && response.companyDetails.customerSetting.length>0){
                    _.each(response.companyDetails.customerSetting,function (el) {
                        if(el.name == "Decision Makers"){
                            $rootScope.dmSelectionRequired = el.mandatory;
                        } else if(el.name == "Influencers"){
                            $rootScope.inflSelectionRequired = el.mandatory;
                        } else if(el.name == "Partners/Resellers"){
                            $rootScope.partnerSelectionRequired = el.mandatory;
                        }
                    })
                }

                $rootScope.companyDetails = response.companyDetails
                share.companyDetails = response.companyDetails
                var regionOwner = response.Data.regionOwner
                var verticalOwner = response.Data.verticalOwner
                var productTypeOwner = response.Data.productTypeOwner

                $rootScope.hasExcpAccess = false;
                if((response.Data.regionOwner && response.Data.regionOwner.length>0) || (response.Data.productTypeOwner && response.Data.productTypeOwner.length>0) || (response.Data.verticalOwner && response.Data.verticalOwner.length>0)){
                    $rootScope.hasExcpAccess = true;
                }

                var regions = response.Data.regionOwner && response.Data.regionOwner.length>0?"Regions: "+regionOwner.join(',')+" \n": ''
                var products = response.Data.productTypeOwner && response.Data.productTypeOwner.length>0?"Products: "+productTypeOwner.join(',')+" \n": ''
                var verticals = response.Data.verticalOwner && response.Data.verticalOwner.length>0?"Verticals: "+verticalOwner.join(',')+" \n": ''

                $rootScope.orgHead = response.Data.orgHead;
                $rootScope.netGrossMargin = response.companyDetails.netGrossMargin;

                $rootScope.accessHelpTxt = "You have access to \n"+regions+products+verticals+"\n And/Or you are part of opportunity internal team."
                $rootScope.notificationForReportingManager = response.companyDetails.notificationForReportingManager;
                $rootScope.notificationForOrgHead = response.companyDetails.notificationForOrgHead;

                if($rootScope.notificationForOrgHead){
                    $rootScope.mailOrgHead = true;
                }

                if($rootScope.notificationForReportingManager){
                    $rootScope.mailRm = true;
                }

                if(share.liuDetails){
                    share.liuDetails(response.Data);
                }

                if(share.setUserId){
                    share.setUserId(response.Data._id);
                }

                if(share.setUserEmailId){
                    share.setUserEmailId(response.Data.emailId);
                }

                if(share.setCompanyDetails){
                    share.setCompanyDetails(response.companyDetails,response.Data)
                }

                if(response.companyDetails && response.companyDetails.opportunityStages && response.companyDetails.opportunityStages.length>0){
                    share.opportunityStages = response.companyDetails.opportunityStages;

                } else {
                    share.opportunityStages = [{order:1,name:'Prospecting'},
                        {order:2,name:'Evaluation'},
                        {order:3,name:'Proposal'},
                        {order:4,name:'Close Won'},
                        {order:5,name:'Close Lost'}];
                }

                share.opportunityStages = _.sortBy(share.opportunityStages, "order");

                if(share.loadOpps){
                    share.loadOpps();
                }

                if(share.setCompanyId){
                    share.setCompanyId(response.Data.companyId)
                }

                if(callback){
                    callback(response)
                }
            }
            else{

            }
        }).error(function (data) {
    })
}

function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
}

function setCookie(name,value,days) {
    var expires = "";
    var date = new Date();

    if (days) {
        date.setTime(date.getTime() + (days*24*60*60*1000));
    } else {
        date = new Date(moment().add(2,"h"))
    }

    expires = "; expires=" + date.toUTCString();

    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}

function oppStageStyle(stage,order,bg,border,colorCode){

    order = order+1;

    var colorAll = {
        1:{background:"#f1c40f","border-left": "3px solid #f1c40f"},
        2:{background:"#3498db","border-left": "3px solid #3498db"},
        3:{background:"#90C695","border-left": "3px solid #90C695"},
        4:{background:"#f791d7","border-left": "3px solid #f791d7"},
        5:{background:"#5b7fd2","border-left": "3px solid #5b7fd2"},
        6:{background:"#d2925b","border-left": "3px solid #d2925b"},
        7:{background:"#4286f4","border-left": "3px solid #4286f4"},
        "Close Lost":{background:"#e74c3c","border-left": "3px solid #e74c3c"},
        "Close Won":{background:"#8ECECB","border-left": "3px solid #8ECECB"}
    }

    var colorBorder = {
        1:{"border-left": "3px solid #f1c40f"},
        2:{"border-left": "3px solid #3498db"},
        3:{"border-left": "3px solid #90C695"},
        4:{"border-left": "3px solid #f791d7"},
        5:{"border-left": "3px solid #5b7fd2"},
        6:{"border-left": "3px solid #d2925b"},
        7:{"border-left": "3px solid #4286f4"},
        "Close Lost":{"border-left": "3px solid #e74c3c"},
        "Close Won":{"border-left": "3px solid #8ECECB"}
    }

    var colorBg = {
        1:{background:"#f1c40f"},
        2:{background:"#3498db"},
        3:{background:"#90C695"},
        4:{background:"#f791d7"},
        5:{background:"#5b7fd2"},
        6:{background:"#d2925b"},
        7:{background:"#4286f4"},
        "Close Lost":{background:"#e74c3c"},
        "Close Won":{background:"#8ECECB"}
    }

    var onlyColors = {
        1:"#f1c40f",
        2:"#3498db",
        3:"#90C695",
        4:"#f791d7",
        5:"#5b7fd2",
        6:"#d2925b",
        7:"#4286f4",
        "Close Lost":"#e74c3c",
        "Close Won":"#8ECECB"
    }

    if(stage == "Close Lost" || stage == "Close Won"){

        if(bg){
            return colorBg[stage]
        }

        if(border){
            return colorBorder[stage]
        }

        if(!bg && !border && !colorCode){
            return colorAll[order]
        }

        if(colorCode){
            return onlyColors[stage]
        }

    } else {

        if(isNaN(order)) {
            if(bg){
                return {background:"#aaa"}
            }

            if(border){
                return {"border-left": "3px solid #aaa"}
            }

            if(!bg && !border && !colorCode){
                return {background:"#aaa","border-left": "3px solid #aaa"}
            }

            if(colorCode){
                return onlyColors[order]
            }

        } else {

            if(bg){
                return colorBg[order]
            }

            if(border){
                return colorBorder[order]
            }

            if(!bg && !border && !colorCode){
                return colorAll[order]
            }
            
            if(colorCode){
                return onlyColors[order]
            }
        }

    }
}

function updateNotificationOpenDate($http, updateObj, callback) {
    updateObj.from = "web";

    $http.post('/update/notification/open/date', updateObj)
        .success(function(response) {
            callback(response);
        });
}

function notifyRelevantPeople($http,subject,body,contactDetails){

    var respSentTimeISO = moment().format();

    var obj = {
        email_cc:contactDetails.cc && contactDetails.cc.length>0 && contactDetails.cc != contactDetails.contactEmailId?contactDetails.cc:null,
        receiverEmailId:contactDetails.contactEmailId,
        receiverName:contactDetails.contactEmailId,
        message:body,
        subject:subject,
        receiverId:contactDetails.personId,
        docTrack:true,
        trackViewed:true,
        remind:null,
        respSentTime:respSentTimeISO,
        isLeadTrack:false,
        newMessage:true
    };

    $http.post("/messages/send/email/single/web",obj, {
            ignoreLoadingBar: true
        }).success(function(response){
        });
}

function replyToEmail($scope,$http,subject,body,prevMessage,emailId,internalMailRecName,callback){

    var respSentTimeISO = moment().format();
    var forOutlookBody = body;

    if(!checkRequired(subject)){
        toastr.error("Please enter the subject.")
    }
    else if(!checkRequired(body)){
        toastr.error("Please enter the message.")
    }
    else{

        if(prevMessage && prevMessage.dataObj != null){
            body = body+'\n\n\n'+prevMessage.dataObj.bodyContent;
            $scope.reminder = prevMessage.dataObj.compose_email_remaind
        }

        var obj = {
            email_cc:$scope.add_cc && $scope.add_cc.length>0?$scope.add_cc:null,
            receiverEmailId:emailId,
            receiverName:emailId,
            message:body,
            subject:subject,
            receiverId:null,
            docTrack:true,
            trackViewed:true,
            remind:$scope.reminder,
            respSentTime:respSentTimeISO,
            isLeadTrack:false,
            newMessage:prevMessage?false:true,
            originalBody:forOutlookBody
        };

        if(internalMailRecName){
            obj.receiverEmailId = emailId
            obj.receiverName = internalMailRecName
            obj.receiverId = ''
        }

        if(prevMessage && prevMessage != null && prevMessage.updateReplied){
            obj.updateReplied = true;
            obj.refId = prevMessage.dataObj.interaction.refId
        }

        //Used for Outlook
        if(prevMessage && prevMessage.dataObj && prevMessage.dataObj.interaction && prevMessage.dataObj.interaction.refId){
            obj.id = prevMessage.dataObj.interaction.refId
        }

        $http.post("/messages/send/email/single/web",obj)
            .success(function(response){
                if(response.SuccessCode){
                    if(!internalMailRecName){
                        toastr.success("Email sent successfully");
                    }
                }
                else{
                    toastr.error("Email not sent. Please try again later");
                }
                
                if(callback){
                    callback(response)
                }
            })
    }
}

function getWebsiteByDomainTypeOrder(accountWebsites){
    var websiteToShow = null;
    for(var i = 0;i<accountWebsites.length;i++){
        var last = accountWebsites[i].substr(accountWebsites[i].length - 6);
        if(_.includes(last,".com")){
            websiteToShow = accountWebsites[i]
            if(websiteToShow){
                break;
            }
        }

        if(_.includes(last,".co.")){
            websiteToShow = accountWebsites[i]
            if(websiteToShow){
                break;
            }
        }
    }
    
    return websiteToShow;
}

function getCommitDayTimeEnd(companyDetails,timezone) {

    var dateTime = moment().day("Monday");
    if(companyDetails.commitDay){
        dateTime = moment().day(companyDetails.commitDay)
    }

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    dateTime = moment(dateTime).tz(timezone)

    if(companyDetails.commitHour){
        dateTime = dateTime.hour(parseInt(companyDetails.commitHour))
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    } else {
        dateTime = dateTime.hour(18)
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    }

    if(new Date(dateTime)< new Date()){
        dateTime = moment(dateTime).add(1,'week') //well past commit time
    }

    return new Date(dateTime);
}

function getCurrentDocumentNumberForCompany(companyDetails) {

    return companyDetails.documentNumber;
}

function getMonthCommitCutOff(companyDetails) {

    //TODO implement admin settings
    var monthCommitCutOff = null;
    if(!monthCommitCutOff){
        monthCommitCutOff = 10
    }

    return new Date(moment(moment().startOf('month')).add(monthCommitCutOff,"day"));
}

function getQuarterCommitCutOff(share) {
    //TODO implement admin settings
    return new Date(share.quarterCommitCutOff.startOfQuarter);
}

function checkForAlphanumericChars(value){
    var alphanumericFound = false;

    if(value && String(value).match(/[a-z]/i)) {
        alphanumericFound = true;
    }

    return alphanumericFound
}

function buildDateObj(weekYear) {
    var year = weekYear.substr(-4);
    var week = weekYear.split(year)[0]
    return new Date(moment().week(week).year(year));
}

var updateEmailOpen = function($http,emailId,trackId,userId){

    if(checkRequired(emailId) && checkRequired(trackId) && checkRequired(userId)){
        $http.get('/track/email/open/'+emailId+'/track/id/'+trackId+'/user/'+userId)
            .success(function(response){

            });
    }
};

function catchGlobalErrors(){
    window.onerror = function(msg, url, line, col, error) {

        var extra = !col ? '' : '\ncolumn: ' + col;
        extra += !error ? '' : '\nerror: ' + error;

        console.log("Error: " + msg + "\nurl: " + url + "\nline: " + line + extra);

        // TODO: Report this error via ajax so you can keep track
        //       of what pages have JS issues

        return true;
    };
}

// catchGlobalErrors();

function clearLocalStorage(key){
    localStorage.removeItem(key);
}

Number.prototype.r_formatNumber = formatNumber;

function formatNumber(num) {

    num = this.valueOf();
    if(num && num.toString().length>2){

        if(num % 1 != 0){
            return num.toFixed(2)
        } else {
            return num
        }
    } else {
        return num;
    }
}

function checkOppFieldsRequirement($scope,$rootScope,share){
    var checkStr = String($scope.opp.amount);
    var checkStrNGM = String($scope.opp.netGrossMargin)
    $scope.errorsExist = false;
    $scope.opp.isNotOwner = false;

    resetErrorsField($scope)

    if(!$scope.opp.opportunityName){
        $scope.opp.opportunityNameReq = true;
        $scope.errorsExist = true;
        highlightTab($scope,1)
    }

    if(!$scope.opp.closeDateFormatted){
        $scope.opp.closeDateReq = true;
        $scope.errorsExist = true;
        highlightTab($scope,1)
    }

    if(!$scope.isExistingOpp){
        if(!$scope.newOppContact || !$scope.newOppContact.emailId){
            $scope.opp.contactEmailIdReq = true;
            $scope.errorsExist = true;
            highlightTab($scope,1)
        }
    } else {
        if(!$scope.opp.contactEmailId){
            $scope.opp.contactEmailIdReq = true;
            $scope.errorsExist = true;
            highlightTab($scope,1)
        }
    }

    if($scope.opp.searchContent && !validateEmail($scope.opp.searchContent)){
        var string = $scope.opp.searchContent.substring($scope.opp.searchContent.lastIndexOf("(")+1,$scope.opp.searchContent.lastIndexOf(")"));
        if(!validateEmail(string)){
            $scope.opp.contactEmailIdReq = true;
            $scope.errorsExist = true;
            highlightTab($scope,1)
        }
    }

    if(!$scope.opp.amount){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.amountReq = true;
    }
    if(!$scope.opp.stage){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.stageReq = true;
    }
    if($scope.opp.amount && checkStr.match(/[a-z]/i)){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.amountReq = true;
    }

    if($rootScope.isRegionRequired && (!$scope.opp.geoLocation || !$scope.opp.geoLocation.zone || !$scope.opp.geoLocation.town)){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.geoLocationReq = true;
    }

    if($rootScope.isProductRequired && !$scope.opp.productType){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.productTypeReq = true;
    }
    if(!$scope.showOppInsights && $rootScope.dmSelectionRequired && (!$scope.opp.decisionMakers || ($scope.opp.decisionMakers && $scope.opp.decisionMakers.length == 0))){
        $scope.errorsExist = true;
        highlightTab($scope,2)
        $scope.opp.dmRequired = true;
    }
    if(!$scope.showOppInsights && $rootScope.inflSelectionRequired && (!$scope.opp.influencers || ($scope.opp.influencers && $scope.opp.influencers.length == 0))){
        $scope.errorsExist = true;
        highlightTab($scope,2)
        $scope.opp.inflRequired = true;
    }

    if(!$scope.showOppInsights && $rootScope.partnerSelectionRequired && (!$scope.opp.partners || ($scope.opp.partners && $scope.opp.partners.length == 0))){
        $scope.errorsExist = true;
        highlightTab($scope,2)
        $scope.opp.partnerRequired = true;
    }

    if($rootScope.isVerticalRequired && !$scope.opp.vertical){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.verticalReq = true;
    }

    if((_.includes($scope.opp.stage, 'Won') || _.includes($scope.opp.stage, 'Lost')) && !$scope.opp.closeReasonDescription){
        $scope.reasonsRequired = true;
        $scope.errorsExist = true;
        $scope.opp.closeReasonsReq = true;
        highlightTab($scope,5)
    }

    if($scope.opp.netGrossMargin && !isNumber($scope.opp.netGrossMargin) && checkStrNGM.match(/[a-z]/i)){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.netGrossMarginReq = true;
    }
    if($rootScope.netGrossMargin && ($scope.opp.netGrossMargin<0 || !checkRequired($scope.opp.netGrossMargin))){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.netGrossMarginReq = true;
    }

    if(share.companyDetails && share.companyDetails.typeList.length>0 && share.companyDetails.opportunitySettings.typeRequired && !$scope.opp.type){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.typeReq = true;
    }
    if(share.companyDetails && share.companyDetails.solutionList.length>0 && share.companyDetails.opportunitySettings.solutionRequired && !$scope.opp.solution){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.solutionReq = true;
    }

    if(share.companyDetails && share.companyDetails.businessUnits.length>0 && share.companyDetails.opportunitySettings.businessUnitRequired && !$scope.opp.businessUnit){
        highlightTab($scope,1)
        $scope.errorsExist = true;
        $scope.opp.businessUnitReq = true;
    }
    if(share.companyDetails && share.companyDetails.sourceList.length>0 && share.companyDetails.opportunitySettings.sourceRequired && !$scope.opp.sourceType){
        highlightTab($scope,1)
        $scope.errorsExist = true;
        $scope.opp.sourceTypeReq = true;
    }
    if($scope.opp.netGrossMargin && ($scope.opp.netGrossMargin>100 || $scope.opp.netGrossMargin<0)){
        highlightTab($scope,1)
        $scope.errorsExist = true;
        $scope.opp.closeDateReq = true;
    }
    if($scope.opp.renewThisOpp && !$scope.renewalAmount){
        highlightTab($scope,5)
        $scope.errorsExist = true;
        $scope.opp.renewalAmountReq = true;
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    }

    if(_.includes($scope.opp.stage, 'Won') && $scope.opp.renewed && !$scope.opp.renewed.createdDate && share.companyDetails.opportunitySettings && share.companyDetails.opportunitySettings.renewalStatusRequired && !$scope.opp.renewalStatusSet){
        highlightTab($scope,5)
        $scope.opp.renewalStatusSelectionReq = true;
        $scope.errorsExist = true;
    }

    if($scope.opp.renewThisOpp && $scope.opp.renewed && !$scope.opp.renewed.createdDate && !$scope.renewalCloseDate){
        highlightTab($scope,5)
        $scope.errorsExist = true;
        $scope.opp.renewalCloseDateReq = true;
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    }

    if((_.includes($scope.opp.stage, 'Won') || _.includes($scope.opp.stage, 'Lost')) && (!checkRequired($scope.opp.closeReasons) || (checkRequired($scope.opp.closeReasons) && $scope.opp.closeReasons.length == 0 ))){
        $scope.reasonsRequired = true;
        $scope.errorsExist = true;
        $scope.opp.closeReasonsReq = true;
        highlightTab($scope,5)
    }

    if($scope.data && $scope.data.availableOptions && $scope.data.availableOptions.length>0){

        var mandatoryObj = {};
        if($scope.opp.masterData && $scope.opp.masterData.length>0){
            _.each($scope.opp.masterData,function (ma) {
                mandatoryObj[ma.type] = true;
            })
        }

        _.each($scope.data.availableOptions,function (av) {
            if(av.oppLinkMandatory && !mandatoryObj[av.name]){
                av.isRequired = true;
            } else {
                av.isRequired = false;
            }

            if(av.isRequired){
                $scope.reasonsRequired = true;
                $scope.errorsExist = true;
                highlightTab($scope,2)
            }
        });

    }

    return $scope.errorsExist;

}

function checkOppFieldsRequirementBasic($scope,$rootScope,share){
    var checkStr = String($scope.opp.amount);
    var checkStrNGM = String($scope.opp.netGrossMargin)
    $scope.errorsExist = false;
    $scope.opp.isNotOwner = false;

    resetErrorsField($scope)

    if(!$scope.opp.opportunityName){
        $scope.opp.opportunityNameReq = true;
        $scope.errorsExist = true;
        highlightTab($scope,1)
    }

    if(!$scope.opp.closeDateFormatted){
        $scope.opp.closeDateReq = true;
        $scope.errorsExist = true;
        highlightTab($scope,1)
    }

    if(!$scope.isExistingOpp){
        if(!$scope.newOppContact || !$scope.newOppContact.emailId){
            $scope.opp.contactEmailIdReq = true;
            $scope.errorsExist = true;
            highlightTab($scope,1)
        }
    } else {
        if(!$scope.opp.contactEmailId){
            $scope.opp.contactEmailIdReq = true;
            $scope.errorsExist = true;
            highlightTab($scope,1)
        }
    }

    if($scope.opp.searchContent && !validateEmail($scope.opp.searchContent)){
        var string = $scope.opp.searchContent.substring($scope.opp.searchContent.lastIndexOf("(")+1,$scope.opp.searchContent.lastIndexOf(")"));
        if(!validateEmail(string)){
            $scope.opp.contactEmailIdReq = true;
            $scope.errorsExist = true;
            highlightTab($scope,1)
        }
    }

    if(!$scope.opp.amount){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.amountReq = true;
    }
    if(!$scope.opp.stage){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.stageReq = true;
    }
    if($scope.opp.amount && checkStr.match(/[a-z]/i)){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.amountReq = true;
    }

    if($rootScope.isRegionRequired && (!$scope.opp.geoLocation || !$scope.opp.geoLocation.zone || !$scope.opp.geoLocation.town)){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.geoLocationReq = true;
    }

    if($scope.opp.netGrossMargin && !isNumber($scope.opp.netGrossMargin) && checkStrNGM.match(/[a-z]/i)){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.netGrossMarginReq = true;
    }

    if($rootScope.netGrossMargin && ($scope.opp.netGrossMargin<0 || !checkRequired($scope.opp.netGrossMargin))){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.netGrossMarginReq = true;
    }

    return $scope.errorsExist;

}

function highlightTab($scope,tab){
    $scope.oppDetailsNav.forEach(function (el) {

        if(tab == 0){
            el.errorExists = false;
        } else {

            if(tab == 1 && el.name == "Basic"){
                el.errorExists = true;
            }

            if(tab == 2 && el.name == "Customer"){
                el.errorExists = true;
            }

            if(tab == 5 && el.name == "Close Details"){
                el.errorExists = true;
            }
        }

    })
}

function getCurrentFiscalYear(timezone,fyMonth,forNextFys){
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

    if (!fyMonth) {
        fyMonth = "April"; //Default
    }

    var currentYr = new Date().getFullYear()
    var currentMonth = new Date().getMonth()

    var toDate = null;
    var fromDate = new Date(moment().startOf('month'));
    fromDate.setMonth(monthNames.indexOf(fyMonth))

    if(currentMonth<monthNames.indexOf(fyMonth)){
        fromDate.setFullYear(currentYr-1)
    }

    toDate = moment(fromDate).add(11, 'month');
    toDate = moment(toDate).endOf('month');

    var obj = {
        start:moment(fromDate).tz(timezone).format(),
        end: moment(toDate).tz(timezone).format(),
        quarter:setQuarter(fyMonth,timezone),
        nextFysStartEnd:[]
    }

    if(forNextFys){
        for(var i = 1;i<=forNextFys;i++){
            obj.nextFysStartEnd.push({
                start:moment(obj.start).add(i,"years").format(),
                end:moment(obj.end).add(i,"years").format()
            })
        }
    }

    return obj
}

function validateNetGrossMargin(el,evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var number = el.value.split('.');

    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    if( el.value && (parseFloat(el.value)) > 100){
        return false;
    }
    return true;
}

function validateFloatKeyPress(el, evt) {

    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
        // console.log(number)
        return false;
    } else {
        // console.log("??")
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

function niceBytes(x){
    const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    let l = 0, n = parseInt(x, 10) || 0;
    while(n >= 1024 && ++l)
        n = n/1024;
    return(n.toFixed(n >= 10 || l < 1 ? 0 : 1) + ' ' + units[l]);
}

function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}

function getWatchers(root) {
    root = angular.element(root || document.documentElement);
    var watcherCount = 0;

    function getElemWatchers(element) {
        var isolateWatchers = getWatchersFromScope(element.data().$isolateScope);
        var scopeWatchers = getWatchersFromScope(element.data().$scope);
        var watchers = scopeWatchers.concat(isolateWatchers);
        angular.forEach(element.children(), function (childElement) {
            watchers = watchers.concat(getElemWatchers(angular.element(childElement)));
        });
        return watchers;
    }

    function getWatchersFromScope(scope) {
        if (scope) {
            return scope.$$watchers || [];
        } else {
            return [];
        }
    }

    return getElemWatchers(root);
}

function getAmountInThousands(num,digits,ins) {
    num = parseFloat(num);

    var si = [
        { value: 1E18, symbol: " E" },
        { value: 1E15, symbol: " P" },
        { value: 1E12, symbol: " T" },
        { value: 1E9,  symbol: " B" },
        { value: 1E6,  symbol: " M" },
        { value: 1E3,  symbol: " K" }
    ]

    // ins = true;

    if(ins){
        // digits = 2;
        si = [
            { value: 1E7,  symbol: " Cr" },
            { value: 1E5,  symbol: " L" },
            { value: 1E3,  symbol: " K" }
        ]
    }

    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;

    for (i = 0; i < si.length; i++) {
        if (num >= si[i].value) {
            return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
        }
    }

    if(num){
        return num.toFixed(digits).replace(rx, "$1");
    } else {
        return num;
    }

}

function updateAccountRelationship($scope,$http,data) {

    $http.post("/account/update/relationship",data)
        .success(function (response) {

        });
}

function shadeGenerator(r,g,b,numOfColors,intensity,ifHex){

    var saturationLevel = intensity;

    r = r%256;
    g = g%256;
    b = b%256;

    var colors = [];

    for(var i=0;i<numOfColors;i++) {
        r+=saturationLevel;
        g+=saturationLevel;
        b+=saturationLevel;

        var stringColor = "rgb("+r+","+g+","+b+")";

        if(ifHex){
            colors.push("#" + componentToHex(r) + componentToHex(g) + componentToHex(b))
        } else {
            colors.push(String(stringColor))
        }
    }

    return colors;
}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue){
    if(parameterValue instanceof Array)
        if(parameterValue.length > 0)
            parameterValue = parameterValue.join(",")
        else
            parameterValue = null

    if(parameterValue != undefined && parameterValue != null){
        if(baseUrl.indexOf("?") == -1)
            baseUrl += "?"
        else
            baseUrl += "&"
        baseUrl+= parameterName + "=" + parameterValue
    }

    return baseUrl
}

var interactionIconDetails = function(type,name,rName,title,action,_id,refId,emailId,ignore,interaction,timezone,remaindDays){

    remaindDays = remaindDays || 7;
    var actionType = "fa fa-arrow-right orange-color"

    switch(type){
        case 'google-meeting':
        case 'meeting':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-calendar-check-o green-color",
                iconTitle:"Meeting",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'document-share':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-file-pdf-o",
                iconTitle:"Document",
                title:action == 'sender' ? rName+' shared '+title+' with you' : 'You shared '+title+' with '+rName,
                but:action == 'sender' ? 'View':'View Stats',
                action:action == 'sender' ? 'viewDoc':'viewStats',
                showButton:true,
                actionType:actionType
            };
            break;
        case 'message':
        case 'meeting-comment':
        case 'email':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            var colorRed = false;
            var colorBlue = false;
            var titleN = null;
            var updateOpened = false;
            var updateReplied = false;
            var showButton = true;
            if(interaction.action == 'sender'){
                if(checkRequired(interaction.trackInfo)){
                    if(interaction.trackInfo.trackOpen){
                        updateOpened = true;
                    }
                    if(interaction.trackInfo.trackResponse){
                        updateReplied = true;
                    }
                    if(interaction.trackInfo.gotResponse){
                        showButton = false;
                    }
                }
            }
            if(interaction.action == 'receiver' && !interaction.ignore && interaction.interactionType == 'email' && interaction.source == 'relatas'){

                if(checkRequired(interaction.trackInfo)){
                    if(interaction.trackInfo.trackOpen){
                        // track info enabled
                        if(interaction.trackInfo.isRed){
                            // opened or no action
                            colorBlue = true;
                        }
                        else{
                            // not opened or no action
                            colorRed = true;
                        }
                    }

                    if(interaction.trackInfo.trackResponse){
                        // track response enabled
                        var interactionDate = moment(interaction.interactionDate).tz(timezone);
                        interactionDate.date(interactionDate.date() + remaindDays);
                        var now = moment().tz(timezone);

                        if(now.isAfter(interactionDate) || now.format("DD-MM-YYYY") == interactionDate.format("DD-MM-YYYY")){
                            if(interaction.trackInfo.gotResponse){
                                // got response
                                colorBlue = false;
                                colorRed = false;
                            }
                            else{
                                // not response
                                titleN = "Notification remainder, Subject: "+title;

                                if(!colorBlue){
                                    colorRed = true;
                                    colorBlue = false;
                                }
                            }
                        }
                        else{
                            if(interaction.trackInfo.gotResponse){
                                // got response
                                colorBlue = false;
                                colorRed = false;
                            }
                        }
                    }
                }
            }
            var subject = "Re: "+interaction.title;
            var body = "";
            if(checkRequired(interaction.description)){
                body = "\n\n"+interaction.description
            }

            if(interaction.title && interaction.title.substring(0,2).toLowerCase() == 're'){
                subject = interaction.title
            }
            if(checkRequired(interaction.trackInfo)){
                if(interaction.trackInfo.gotResponse){
                    showButton = false;
                }
            }
            return {
                iconClass:"fa-envelope green-color",
                iconTitle:"Email",
                title:titleN != null? titleN :title,
                but:'Reply',
                action:"viewEmail",
                showButton:showButton,
                colorRed:colorRed,
                colorBlue:colorBlue,
                subject:subject,
                subjectNormal:interaction.title,
                composeMailBox:false,
                emailBodyBox:false,
                compose_email_track_viewed:true,
                compose_email_remaind:true,
                compose_email_doc_tracking:false,
                compose_email_body:"",
                compose_email_subject:"",
                bodyContent:"",
                itemPointer:'cursor:pointer',
                updateOpened:updateOpened,
                emailId:null,
                userId:interaction.userId,
                trackId:interaction.trackId,
                updateReplied:updateReplied,
                actionType:actionType
            };
            break;
        case 'sms':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-reorder green-color",
                iconTitle:"SMS",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'call':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-phone green-color",
                iconTitle:"Call",
                title:action == 'sender' ? 'Received call from '+rName : 'You called '+rName,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'task':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            return {
                iconClass:"fa-check-square-o",
                iconTitle:"Tasks",
                title:action == 'sender' ? 'Task assigned to you by '+rName+'. <br>Task: '+title : 'You assigned a task to '+rName+'. <br>Task: '+title,
                but:'View Task',
                action:'viewTask',
                showButton:true,
                interaction:interaction,
                actionType:actionType
            };
            break;
        case 'twitter':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            return {
                iconClass:"fa-twitter-square twitter-color",
                iconTitle:"Twitter",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'facebook':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-facebook-official fb-color",
                iconTitle:"Facebook",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'linkedin':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-linkedin-square linkedin-color",
                iconTitle:"Linkedin",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'calendar-password':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            var calendarPasswordApprove = false;
            var calPassIgnoreBut = false;
            if(action == 'sender' && !ignore){
                calendarPasswordApprove = true;
                calPassIgnoreBut = true;
            }

            return {
                iconClass:"fa-calendar-check-o green-color",
                iconTitle:"Calendar Password Request",
                followUpBut:false,
                calendarPasswordApprove:calendarPasswordApprove,
                ignoreBut:calPassIgnoreBut,
                isCalPassReq:true,
                _id:_id,
                reqId:refId,
                name:name,
                emailId:emailId,
                actionType:actionType
            };
            break;
        default :return {image:'',title:'', showButton:false};
    }
};

var interactionIconDetails_new = function(type,name,rName,title,action,_id,refId,emailId,ignore,interaction,timezone,remaindDays){

    remaindDays = remaindDays || 7;
    var actionType = "fa fa-arrow-left orange-color"

    switch(type){
        case 'google-meeting':
        case 'meeting':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-calendar-check-o green-color",
                iconTitle:"Meeting",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'document-share':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-file-pdf-o",
                iconTitle:"Document",
                title:action == 'sender' ? rName+' shared '+title+' with you' : 'You shared '+title+' with '+rName,
                but:action == 'sender' ? 'View':'View Stats',
                action:action == 'sender' ? 'viewDoc':'viewStats',
                showButton:true,
                actionType:actionType
            };
            break;
        case 'message':
        case 'meeting-comment':
        case 'email':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            var colorRed = false;
            var colorBlue = false;
            var titleN = null;
            var updateOpened = false;
            var updateReplied = false;
            var showButton = true;
            if(interaction.action == 'sender'){
                if(checkRequired(interaction.trackInfo)){
                    if(interaction.trackInfo.trackOpen){
                        updateOpened = true;
                    }
                    if(interaction.trackInfo.trackResponse){
                        updateReplied = true;
                    }
                    if(interaction.trackInfo.gotResponse){
                        showButton = false;
                    }
                }
            }
            if(interaction.action == 'sender' && !interaction.ignore && interaction.interactionType == 'email' && interaction.source == 'relatas'){

                if(checkRequired(interaction.trackInfo)){
                    if(interaction.trackInfo.trackOpen){
                        // track info enabled
                        if(interaction.trackInfo.isRed){
                            // opened or no action
                            colorBlue = true;
                        }
                        else{
                            // not opened or no action
                            colorRed = true;
                        }
                    }

                    if(interaction.trackInfo.trackResponse){
                        // track response enabled
                        var interactionDate = moment(interaction.interactionDate).tz(timezone);
                        interactionDate.date(interactionDate.date() + remaindDays);
                        var now = moment().tz(timezone);

                        if(now.isAfter(interactionDate) || now.format("DD-MM-YYYY") == interactionDate.format("DD-MM-YYYY")){
                            if(interaction.trackInfo.gotResponse){
                                // got response
                                colorBlue = false;
                                colorRed = false;
                            }
                            else{
                                // not response
                                titleN = "Notification remainder, Subject: "+title;

                                if(!colorBlue){
                                    colorRed = true;
                                    colorBlue = false;
                                }
                            }
                        }
                        else{
                            if(interaction.trackInfo.gotResponse){
                                // got response
                                colorBlue = false;
                                colorRed = false;
                            }
                        }
                    }
                }
            }
            var subject = "Re: "+interaction.title;
            var body = "";
            if(checkRequired(interaction.description)){
                body = "\n\n"+interaction.description
            }

            if(interaction.title && interaction.title.substring(0,2).toLowerCase() == 're'){
                subject = interaction.title
            }
            if(checkRequired(interaction.trackInfo)){
                if(interaction.trackInfo.gotResponse){
                    showButton = false;
                }
            }
            return {
                iconClass:"fa-envelope green-color",
                iconTitle:"Email",
                title:titleN != null? titleN :title,
                but:'Reply',
                action:"viewEmail",
                showButton:showButton,
                colorRed:colorRed,
                colorBlue:colorBlue,
                subject:subject,
                subjectNormal:interaction.title,
                composeMailBox:false,
                emailBodyBox:false,
                compose_email_track_viewed:true,
                compose_email_remaind:true,
                compose_email_doc_tracking:false,
                compose_email_body:"",
                compose_email_subject:"",
                bodyContent:"",
                itemPointer:'cursor:pointer',
                updateOpened:updateOpened,
                emailId:null,
                userId:interaction.userId,
                trackId:interaction.trackId,
                updateReplied:updateReplied,
                actionType:actionType
            };
            break;
        case 'sms':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-reorder green-color",
                iconTitle:"SMS",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'call':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-phone green-color",
                iconTitle:"Call",
                title:action == 'sender' ? 'Received call from '+rName : 'You called '+rName,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'task':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            return {
                iconClass:"fa-check-square-o",
                iconTitle:"Tasks",
                title:action == 'sender' ? 'Task assigned to you by '+rName+'. <br>Task: '+title : 'You assigned a task to '+rName+'. <br>Task: '+title,
                but:'View Task',
                action:'viewTask',
                showButton:true,
                interaction:interaction,
                actionType:actionType
            };
            break;
        case 'twitter':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            return {
                iconClass:"fa-twitter-square twitter-color",
                iconTitle:"Twitter",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'facebook':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-facebook-official fb-color",
                iconTitle:"Facebook",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'linkedin':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-linkedin-square linkedin-color",
                iconTitle:"Linkedin",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'calendar-password':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            var calendarPasswordApprove = false;
            var calPassIgnoreBut = false;
            if(action == 'sender' && !ignore){
                calendarPasswordApprove = true;
                calPassIgnoreBut = true;
            }

            return {
                iconClass:"fa-calendar-check-o green-color",
                iconTitle:"Calendar Password Request",
                followUpBut:false,
                calendarPasswordApprove:calendarPasswordApprove,
                ignoreBut:calPassIgnoreBut,
                isCalPassReq:true,
                _id:_id,
                reqId:refId,
                name:name,
                emailId:emailId,
                actionType:actionType
            };
            break;
        default :return {image:'',title:'', showButton:false};
    }
};

function escapeHtml(unsafe) {
    return unsafe
    // .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
    // .replace(/"/g, "&quot;")
    // .replace(/'/g, "&#039;");
}

function oppEmptyObj(){
    return {
        "opportunityName": "",
        "closeDate": "",
        "contactEmailId": "",
        "relatasStage": "",
        "stageName": "",
        "stage": "",
        "geoLocation": "",
        "amount": "",
        "netGrossMargin": "",
        "type": "",
        "vertical": "",
        "closeReasons": "",
        "currency": "",
        "sourceType": "",
        "solution": "",
        "accounts": "",
        "businessUnit": ""
    }
}

function buildInteractionObjectForTimeline(interaction,userId,firstName,rName,timezone,index){
    var interactionIcon = interactionIconDetails(interaction.interactionType,
        firstName,
        rName,
        interaction.title,
        interaction.action,
        interaction._id,
        interaction.refId,
        interaction.emailId,
        interaction.ignore,
        interaction,
        timezone,
        null);

    var iDate = moment(interaction.interactionDate).tz(timezone);
    var now = moment().tz(timezone);
    var diff = now.diff(iDate);
    diff = moment.duration(diff).asMinutes();

    var colorClass = "";
    var  emailRead = false;
    var viewedOn,isEmailRead;

    if(interaction.action == 'receiver'
        && interaction.interactionType =='email'
        && interaction.trackInfo
        && interaction.trackInfo.lastOpenedOn
        && interaction.trackInfo.lastOpenedOn !== null){
        isEmailRead = true;
        viewedOn = moment(interaction.trackInfo.lastOpenedOn).format("DD MMM YYYY, h:mm a");
    } else {
        isEmailRead = false;
        viewedOn = '';
    }

    if(interactionIcon.colorRed){
        colorClass = 'color:#F86A52';
        emailRead = false;
    } else if(interactionIcon.colorBlue){
        colorClass = 'color:#2d3e48';
        emailRead = true;
    }

    var cursor = "no-action"

    if(interaction.interactionType == "email" || interaction.interactionType == "meeting"){
        cursor = "cursor"
    }

    var duration = moment.duration(diff,"minutes").humanize()+' ago'
    if(new Date(interaction.interactionDate)>new Date()){
        duration = "In "+moment.duration(diff,"minutes").humanize()
    }

    var interactionInitClass = "incoming";

    if(interaction.action == "receiver"){
        interactionInitClass = "outgoing";
    }

    return {
        emailId:interaction.emailId,
        interactionDate:interaction.interactionDate,
        cursor:cursor,
        duration:duration,
        dateText:iDate.format("DD MMM YYYY"),
        interactionIcon:interactionIcon.image,
        iconClass:interactionIcon.iconClass,
        iconTitle:interactionIcon.iconTitle,
        actionType:interactionIcon.actionType,
        subject:interactionIcon.title,
        title:interactionIcon.title?escapeHtml(interactionIcon.title.replace(/<br\s*[\/]?>/gi, "\n")):interactionIcon.title,
        but:interactionIcon.but,
        action:interactionIcon.action,
        refId:interaction.refId,
        showButton:interactionIcon.showButton ? 'display:block' : 'display:none',
        className: index==0 ? 'document-timeline' :'',
        viewitemId:interaction._id,
        colorClass:colorClass,
        emailRead:isEmailRead,
        interactionInitClass:interactionInitClass,
        viewedOn:viewedOn,
        emailOpens:interaction.trackInfo && interaction.trackInfo.emailOpens ? interaction.trackInfo.emailOpens : 0,
        callDuration:getDurationFormat(interaction),
        dataObj:{
            _id:interaction._id,
            body:interaction.description,
            bindHtmlKey:'content_'+interaction._id,
            interaction:interaction,
            subject:interactionIcon.subject,
            subjectNormal:interactionIcon.subjectNormal,
            composeMailBox:interactionIcon.composeMailBox,
            emailBodyBox:interactionIcon.emailBodyBox,
            compose_email_track_viewed:interactionIcon.compose_email_track_viewed,
            compose_email_remaind:interactionIcon.compose_email_remaind,
            compose_email_doc_tracking:interactionIcon.compose_email_doc_tracking,
            compose_email_body:interactionIcon.compose_email_body,
            compose_email_subject:interactionIcon.subject,
            bodyContent:interactionIcon.bodyContent,
            itemPointer:interactionIcon.itemPointer,
            updateOpened:interactionIcon.updateOpened,
            updateReplied:interactionIcon.updateReplied,
            emailId:interactionIcon.emailId,
            userId:userId?userId:null,
            trackId:interactionIcon.trackId,
            updateMailRead:true
        }
    }
}

function buildInteractionObjectForTimeline_new(interaction,userId,firstName,rName,timezone,index){
    var interactionIcon = interactionIconDetails_new(interaction.interactionType,
        firstName,
        rName,
        interaction.title,
        interaction.action,
        interaction._id,
        interaction.refId,
        interaction.emailId,
        interaction.ignore,
        interaction,
        timezone,
        null);

    var iDate = moment(interaction.interactionDate).tz(timezone);
    var now = moment().tz(timezone);
    var diff = now.diff(iDate);
    diff = moment.duration(diff).asMinutes();

    var colorClass = "";
    var  emailRead = false;
    var viewedOn,isEmailRead;

    if(interaction.action == 'receiver'
        && interaction.interactionType =='email'
        && interaction.trackInfo
        && interaction.trackInfo.lastOpenedOn
        && interaction.trackInfo.lastOpenedOn !== null){
        isEmailRead = true;
        viewedOn = moment(interaction.trackInfo.lastOpenedOn).format("DD MMM YYYY, h:mm a");
    } else {
        isEmailRead = false;
        viewedOn = '';
    }

    if(interactionIcon.colorRed){
        colorClass = 'color:#F86A52';
        emailRead = false;
    } else if(interactionIcon.colorBlue){
        colorClass = 'color:#2d3e48';
        emailRead = true;
    }

    var cursor = "no-action"

    if(interaction.interactionType == "email" || interaction.interactionType == "meeting"){
        cursor = "cursor"
    }

    var duration = moment.duration(diff,"minutes").humanize()+' ago'
    if(new Date(interaction.interactionDate)>new Date()){
        duration = "In "+moment.duration(diff,"minutes").humanize()
    }

    var interactionInitClass = "outgoing";

    if(interaction.action == "receiver"){
        interactionInitClass = "incoming";
    }

    return {
        emailId:interaction.emailId,
        interactionDate:interaction.interactionDate,
        cursor:cursor,
        duration:duration,
        dateText:iDate.format("DD MMM YYYY"),
        interactionIcon:interactionIcon.image,
        iconClass:interactionIcon.iconClass,
        iconTitle:interactionIcon.iconTitle,
        actionType:interactionIcon.actionType,
        subject:interactionIcon.title,
        title:interactionIcon.title?escapeHtml(interactionIcon.title.replace(/<br\s*[\/]?>/gi, "\n")):interactionIcon.title,
        but:interactionIcon.but,
        action:interactionIcon.action,
        refId:interaction.refId,
        showButton:interactionIcon.showButton ? 'display:block' : 'display:none',
        className: index==0 ? 'document-timeline' :'',
        viewitemId:interaction._id,
        colorClass:colorClass,
        emailRead:isEmailRead,
        interactionInitClass:interactionInitClass,
        viewedOn:viewedOn,
        emailOpens:interaction.trackInfo && interaction.trackInfo.emailOpens ? interaction.trackInfo.emailOpens : 0,
        callDuration:getDurationFormat(interaction),
        dataObj:{
            _id:interaction._id,
            body:interaction.description,
            bindHtmlKey:'content_'+interaction._id,
            interaction:interaction,
            subject:interactionIcon.subject,
            subjectNormal:interactionIcon.subjectNormal,
            composeMailBox:interactionIcon.composeMailBox,
            emailBodyBox:interactionIcon.emailBodyBox,
            compose_email_track_viewed:interactionIcon.compose_email_track_viewed,
            compose_email_remaind:interactionIcon.compose_email_remaind,
            compose_email_doc_tracking:interactionIcon.compose_email_doc_tracking,
            compose_email_body:interactionIcon.compose_email_body,
            compose_email_subject:interactionIcon.subject,
            bodyContent:interactionIcon.bodyContent,
            itemPointer:interactionIcon.itemPointer,
            updateOpened:interactionIcon.updateOpened,
            updateReplied:interactionIcon.updateReplied,
            emailId:interactionIcon.emailId,
            userId:userId?userId:null,
            trackId:interactionIcon.trackId,
            updateMailRead:true
        }
    }
}

var getInteractionTypeObj = function(obj,total){
    switch (obj._id){
        case 'google-meeting':
        case 'meeting':return {
            priority:0,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'call':return {
            priority:1,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'sms':return {
            priority:2,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'email':return {
            priority:3,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'facebook':return {
            priority:4,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'twitter':return {
            priority:5,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'linkedin':return {
            priority:6,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        default : return null;
    }
};

function getDurationFormat(interaction){
    var duration = "";
    if(interaction && interaction.duration != 0 && interaction.duration){
        duration = secondsToHms(interaction.duration)
    }

    if(interaction.duration == 0){
        duration = "Missed call"
    }

    return duration
}

function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hr " : " hrs ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " m " : " m ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " s" : " s") : "";
    return hDisplay + mDisplay + sDisplay;
}

var calculatePercentage = function(count,total){
    return Math.round((count*100)/total);
};

var calculatePercentageChange = function(current,prev){
    var num = String(Math.round((current-prev)/prev*100.0));
    num = num.replace(/\.00$/,'');
    return parseFloat(num);
};

var calculatePercentageChange_insights = function(current,prev){
    var num = String(Math.round((current-prev)/current*100.0));
    num = num.replace(/\.00$/,'');
    // return current>prev?-parseFloat(num):parseFloat(num);
    return parseFloat(num);
};

function clearInputFields() {
    $(".clearfield input").val("");
    $(".clearfield textarea").val("");
}

function imageExists(image_url){

    var http = new XMLHttpRequest();
    http.open('HEAD', image_url, false);

    try {
        http.send();
        return http.status != 404;
    } catch(err){
        return false;
    }

}

function loadDataOnlyForInternalTeam() {

    var loadAll = false;
    var domain = window.location.hostname;

    if((/relatas.relatas/.test(domain))){
        loadAll = true;
    } else if(/example/.test(domain)){
        loadAll = true;
    } else if(/sugar/.test(domain)){
        loadAll = true;
    } else if(/localhost/.test(domain)){
        loadAll = true;
    }

    return loadAll;
}

$(document).ready(function() {
    $('.body').hover( function() {
        $(this).siblings('.tooltip-xs').toggle();
    });

    $(window).load(function() {

        try {
            var firstWords = document.title.replace(/ .*/,'');

            if(mixpanel){
                mixpanel.track(firstWords, {
                    'page name' : document.title,
                    'url' : window.location.pathname
                });
            }
        } catch(err){
            console.log("Error",err);
        }
    });
});

$(document).ready(function() {
        //Stop closing of dropdown in mobile when clicked inside

    $("body").on('click', '#mLogout', function (e) {
        clearLocalStorage("refreshData")
        clearLocalStorage("teamMembers")
        clearLocalStorage("teamMembersDictionary")
        clearLocalStorage("relatasLocalDb")
    });

    $("body").on('click', '.dropdown-menu', function (e) {
        $(this).hasClass('keep-open') && e.stopPropagation();
    });

    $("body").on('click', '#menu-toggle', function (e) {
        e.preventDefault();
        $(".overlay").show();
    });

    $(window).resize(function () {
        var scrollWidth = $(window).width();
        if (scrollWidth >= 768) {
            $(".overlay").hide();
        }
    });

    //Set sidebar height to main col's height

    $(window).scroll(function() {
        var windowHeight = $(".set-height").height();
        $(".sidebar-height").css("min-height", windowHeight);
    });

    $('#open-mapInteractions').popover({
        html : true,
        content: function() {
            return $('#popover_content_wrapper').html();
        }
    });

    $("#ignore-response").click(function(){
        $(".response-hide").hide(100);
    });

    $("body").on("click",".email-reply",function(){
        $("#email-response").toggle(100);
        var position = $(this).offset();
        $("#email-response").css({top:position.top})
    });

    $("body").on("click",".open-comment",function(){
        $("#comment-section").toggle(100);
        var position = $(this).offset();
        $("#comment-section").css({top:position.top})
    });

    $("body").on("click","#close-doc-analytics",function(){
        $("#analytics-box").toggle(100);
        var position = $(this).offset();
        $("#analytics-box").css({top:position.top})
    });

    $(".clickable-block").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    window.setTimeout(function() {
        $(".alert-message").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 5000);

    $("#toggle-left-menu").click(function(){
        $(".toggle-sidebar").toggle();
    });

    $("body").on("click",".fa-bars",function(){
        $(".contact-list-hidden-xs").toggle();
        $(".contact-list-hidden-xs #sidebar-wrapper").toggle();
    });

    var isMobile = window.matchMedia("only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) and (-webkit-min-device-pixel-ratio: 2)");

        if (isMobile.matches) {
            $("#menu-toggle").click(function(){
                $("#ipad-open").toggleClass("open");
            });
        }

    if (matchMedia('(min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) and (-webkit-min-device-pixel-ratio: 2').matches) {
        $("#menu-toggle").click(function(){
            $("#ipad-open").toggleClass("open");
        });

        $("#menu-toggle").click(function(){
            $(".navbar").addClass("texting");
        });
    }

    var colHeight = $(".this-height").height();
    $(".col-height").css("height", colHeight);

    $("body").on("click",".open-add-task",function(){
        $(".add-task-landing").toggle(100);
    });

    $("body").on("click",".show-networks",function(){
        $(".toggle-networks").toggle();
    });

    $("body").on("click",".fa-filter",function(){
        $(".filter-options").slideToggle(200);
        return false;
    });

    $("body").on("click","#emailFormOpen",function(){
        $("#emailFormShow").slideToggle(200);
        $("#compose-email-box-top").hide();
        return false;
    });

    $("body").on("click",".long-agenda",function(){
        $(".show-more-long-agenda").slideToggle(5);
    });

    $("body").on("click",".open-leftbar",function(){
        $("#sidebar-wrapper").toggle();
    });

    $("body").on("click", ".fa-plus-circle", function(){
        $(".display-hashtags form").slideToggle(200);
    });

    $("body").on("click",".move-enterprise",function(){
        $(".move-to-enterprise").slideToggle(200);
    });

    $("body").on("click",".close-help",function(){
        $("#contact").hide();
    });

    $("body").on("click",".shw-heirarchy",function(){
        $(".toggle-heirarchy").slideToggle(200);
    });

    $("body").on("click",".email-inline",function(){
        $(".compose-email-inline").slideToggle(200);
    });

    $("body").on("click", ".click-test", function(){
        $(".table-email").slideToggle(50);
        //$(".table-email-container").nextAll(".table-email").slideToggle(50);
    });

    $("body").on("click", "#youtubeVid", function(){
            var src = 'https://www.youtube.com/watch?v=Ax7oKZ6DYQY&feature=youtu.be;autoplay=1';
            $('#myModal').modal('show');
            $('#myModal iframe').attr('src', src);
    });

    $("body").on("click", "#myModal button", function(){
        $('#myModal iframe').removeAttr('src');
    });

    $("body").on("contextmenu", "#my-video", function(e){
       return false;
    });

    $("body").on("click", ".initiate-intro", function(){
        $(this).parents(".connect-sprite").nextAll(".toggle-initiate-intro").slideToggle(50);
    });

    $("body").on("click", ".forward-intro", function(){
        $(this).parents(".connect-sprite").nextAll(".toggle-forward-intro").slideToggle(50);
    });

    $("body").on("click", ".invite-contact", function(){
       $(".toggle-invite-contact").slideToggle(50);
    });

    function popUpCloseDefault(className, btnName) {

        $(document).on('mouseup', function (e){
            var container = $(className);
            if (!container.is(e.target) && container.has(e.target).length === 0 && !$(e.target).is(btnName))
            {
                container.hide(100);
            }
        });
    }

    // popUpCloseDefault(".filter-options",".fa-filter");
    popUpCloseDefault(".toggle-networks",".show-networks");
    //popUpCloseDefault(".toggle-initiate-intro",".initiate-intro");
    popUpCloseDefault(".toggle-forward-intro",".forward-intro");
    popUpCloseDefault("#contact",".close-help");

    // Onboarding wizard
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
        /*if ($target.parent().hasClass('disabled')) {
         return false;
         }*/
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }

    $(".hasEvent").parents(':eq(2)').css({"background-color": "#cb3c19"});
    $(".non-working").closest('td').css({"background-color": "#f5f3f0"});

    //Enterprise change value of btns on dropdown select

    $("#dropdown-landing li a").click(function(){
        $("#dropdown-value-3:first-child").html($(this).text()+' <span class="caret"></span>');
    });

    $("#dropdown-selected-1 li a").click(function(){
        $("#dropdown-value-1:first-child").html($(this).text()+' <span class="caret"></span>');
    });

    $("#dropdown-selected-2 li a").click(function(){
        $("#dropdown-value-2:first-child").html($(this).text()+' <span class="caret"></span>');
    });

    $("#dropdown-selected-5 li a").click(function(){
        $("#dropdown-value-5:first-child").html($(this).text()+' <span class="caret"></span>');
    });
});


/*!
 * Bootstrap-select v1.7.5 (http://silviomoreto.github.io/bootstrap-select)
 *
 * Copyright 2013-2015 bootstrap-select
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-select/blob/master/LICENSE)
 */
!function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(this,function(a){!function(a){"use strict";function b(b){var c=[{re:/[\xC0-\xC6]/g,ch:"A"},{re:/[\xE0-\xE6]/g,ch:"a"},{re:/[\xC8-\xCB]/g,ch:"E"},{re:/[\xE8-\xEB]/g,ch:"e"},{re:/[\xCC-\xCF]/g,ch:"I"},{re:/[\xEC-\xEF]/g,ch:"i"},{re:/[\xD2-\xD6]/g,ch:"O"},{re:/[\xF2-\xF6]/g,ch:"o"},{re:/[\xD9-\xDC]/g,ch:"U"},{re:/[\xF9-\xFC]/g,ch:"u"},{re:/[\xC7-\xE7]/g,ch:"c"},{re:/[\xD1]/g,ch:"N"},{re:/[\xF1]/g,ch:"n"}];return a.each(c,function(){b=b.replace(this.re,this.ch)}),b}function c(a){var b={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},c="(?:"+Object.keys(b).join("|")+")",d=new RegExp(c),e=new RegExp(c,"g"),f=null==a?"":""+a;return d.test(f)?f.replace(e,function(a){return b[a]}):f}function d(b,c){var d=arguments,f=b,g=c;[].shift.apply(d);var h,i=this.each(function(){var b=a(this);if(b.is("select")){var c=b.data("selectpicker"),i="object"==typeof f&&f;if(c){if(i)for(var j in i)i.hasOwnProperty(j)&&(c.options[j]=i[j])}else{var k=a.extend({},e.DEFAULTS,a.fn.selectpicker.defaults||{},b.data(),i);k.template=a.extend({},e.DEFAULTS.template,a.fn.selectpicker.defaults?a.fn.selectpicker.defaults.template:{},b.data().template,i.template),b.data("selectpicker",c=new e(this,k,g))}"string"==typeof f&&(h=c[f]instanceof Function?c[f].apply(c,d):c.options[f])}});return"undefined"!=typeof h?h:i}String.prototype.includes||!function(){var a={}.toString,b=function(){try{var a={},b=Object.defineProperty,c=b(a,a,a)&&b}catch(d){}return c}(),c="".indexOf,d=function(b){if(null==this)throw new TypeError;var d=String(this);if(b&&"[object RegExp]"==a.call(b))throw new TypeError;var e=d.length,f=String(b),g=f.length,h=arguments.length>1?arguments[1]:void 0,i=h?Number(h):0;i!=i&&(i=0);var j=Math.min(Math.max(i,0),e);return g+j>e?!1:-1!=c.call(d,f,i)};b?b(String.prototype,"includes",{value:d,configurable:!0,writable:!0}):String.prototype.includes=d}(),String.prototype.startsWith||!function(){var a=function(){try{var a={},b=Object.defineProperty,c=b(a,a,a)&&b}catch(d){}return c}(),b={}.toString,c=function(a){if(null==this)throw new TypeError;var c=String(this);if(a&&"[object RegExp]"==b.call(a))throw new TypeError;var d=c.length,e=String(a),f=e.length,g=arguments.length>1?arguments[1]:void 0,h=g?Number(g):0;h!=h&&(h=0);var i=Math.min(Math.max(h,0),d);if(f+i>d)return!1;for(var j=-1;++j<f;)if(c.charCodeAt(i+j)!=e.charCodeAt(j))return!1;return!0};a?a(String.prototype,"startsWith",{value:c,configurable:!0,writable:!0}):String.prototype.startsWith=c}(),Object.keys||(Object.keys=function(a,b,c){c=[];for(b in a)c.hasOwnProperty.call(a,b)&&c.push(b);return c}),a.fn.triggerNative=function(a){var b,c=this[0];c.dispatchEvent?("function"==typeof Event?b=new Event(a,{bubbles:!0}):(b=document.createEvent("Event"),b.initEvent(a,!0,!1)),c.dispatchEvent(b)):(c.fireEvent&&(b=document.createEventObject(),b.eventType=a,c.fireEvent("on"+a,b)),this.trigger(a))},a.expr[":"].icontains=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.text()).toUpperCase();return f.includes(d[3].toUpperCase())},a.expr[":"].ibegins=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.text()).toUpperCase();return f.startsWith(d[3].toUpperCase())},a.expr[":"].aicontains=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.data("normalizedText")||e.text()).toUpperCase();return f.includes(d[3].toUpperCase())},a.expr[":"].aibegins=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.data("normalizedText")||e.text()).toUpperCase();return f.startsWith(d[3].toUpperCase())};var e=function(b,c,d){d&&(d.stopPropagation(),d.preventDefault()),this.$element=a(b),this.$newElement=null,this.$button=null,this.$menu=null,this.$lis=null,this.options=c,null===this.options.title&&(this.options.title=this.$element.attr("title")),this.val=e.prototype.val,this.render=e.prototype.render,this.refresh=e.prototype.refresh,this.setStyle=e.prototype.setStyle,this.selectAll=e.prototype.selectAll,this.deselectAll=e.prototype.deselectAll,this.destroy=e.prototype.remove,this.remove=e.prototype.remove,this.show=e.prototype.show,this.hide=e.prototype.hide,this.init()};e.VERSION="1.7.5",e.DEFAULTS={noneSelectedText:"Nothing selected",noneResultsText:"No results matched {0}",countSelectedText:function(a,b){return 1==a?"{0} item selected":"{0} items selected"},maxOptionsText:function(a,b){return[1==a?"Limit reached ({n} item max)":"Limit reached ({n} items max)",1==b?"Group limit reached ({n} item max)":"Group limit reached ({n} items max)"]},selectAllText:"Select All",deselectAllText:"Deselect All",doneButton:!1,doneButtonText:"Close",multipleSeparator:", ",styleBase:"btn",style:"btn-default",size:"auto",title:null,selectedTextFormat:"values",width:!1,container:!1,hideDisabled:!1,showSubtext:!1,showIcon:!0,showContent:!0,dropupAuto:!0,header:!1,liveSearch:!1,liveSearchPlaceholder:null,liveSearchNormalize:!1,liveSearchStyle:"contains",actionsBox:!1,iconBase:"glyphicon",tickIcon:"glyphicon-ok",template:{caret:'<span class="caret"></span>'},maxOptions:!1,mobile:!1,selectOnTab:!1,dropdownAlignRight:!1},e.prototype={constructor:e,init:function(){var b=this,c=this.$element.attr("id");this.$element.addClass("bs-select-hidden"),this.liObj={},this.multiple=this.$element.prop("multiple"),this.autofocus=this.$element.prop("autofocus"),this.$newElement=this.createView(),this.$element.after(this.$newElement),this.$button=this.$newElement.children("button"),this.$menu=this.$newElement.children(".dropdown-menu"),this.$menuInner=this.$menu.children(".inner"),this.$searchbox=this.$menu.find("input"),this.options.dropdownAlignRight&&this.$menu.addClass("dropdown-menu-right"),"undefined"!=typeof c&&(this.$button.attr("data-id",c),a('label[for="'+c+'"]').click(function(a){a.preventDefault(),b.$button.focus()})),this.checkDisabled(),this.clickListener(),this.options.liveSearch&&this.liveSearchListener(),this.render(),this.setStyle(),this.setWidth(),this.options.container&&this.selectPosition(),this.$menu.data("this",this),this.$newElement.data("this",this),this.options.mobile&&this.mobile(),this.$newElement.on({"hide.bs.dropdown":function(a){b.$element.trigger("hide.bs.select",a)},"hidden.bs.dropdown":function(a){b.$element.trigger("hidden.bs.select",a)},"show.bs.dropdown":function(a){b.$element.trigger("show.bs.select",a)},"shown.bs.dropdown":function(a){b.$element.trigger("shown.bs.select",a)}}),setTimeout(function(){b.$element.trigger("loaded.bs.select")})},createDropdown:function(){var b=this.multiple?" show-tick":"",d=this.$element.parent().hasClass("input-group")?" input-group-btn":"",e=this.autofocus?" autofocus":"",f=this.options.header?'<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>'+this.options.header+"</div>":"",g=this.options.liveSearch?'<div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off"'+(null===this.options.liveSearchPlaceholder?"":' placeholder="'+c(this.options.liveSearchPlaceholder)+'"')+"></div>":"",h=this.multiple&&this.options.actionsBox?'<div class="bs-actionsbox"><div class="btn-group btn-group-sm btn-block"><button type="button" class="actions-btn bs-select-all btn btn-default">'+this.options.selectAllText+'</button><button type="button" class="actions-btn bs-deselect-all btn btn-default">'+this.options.deselectAllText+"</button></div></div>":"",i=this.multiple&&this.options.doneButton?'<div class="bs-donebutton"><div class="btn-group btn-block"><button type="button" class="btn btn-sm btn-default">'+this.options.doneButtonText+"</button></div></div>":"",j='<div class="btn-group bootstrap-select'+b+d+'"><button type="button" class="'+this.options.styleBase+' dropdown-toggle" data-toggle="dropdown"'+e+'><span class="filter-option pull-left"></span>&nbsp;<span class="bs-caret">'+this.options.template.caret+'</span></button><div class="dropdown-menu open">'+f+g+h+'<ul class="dropdown-menu inner" role="menu"></ul>'+i+"</div></div>";return a(j)},createView:function(){var a=this.createDropdown(),b=this.createLi();return a.find("ul")[0].innerHTML=b,a},reloadLi:function(){this.destroyLi();var a=this.createLi();this.$menuInner[0].innerHTML=a},destroyLi:function(){this.$menu.find("li").remove()},createLi:function(){var d=this,e=[],f=0,g=document.createElement("option"),h=-1,i=function(a,b,c,d){return"<li"+("undefined"!=typeof c&""!==c?' class="'+c+'"':"")+("undefined"!=typeof b&null!==b?' data-original-index="'+b+'"':"")+("undefined"!=typeof d&null!==d?'data-optgroup="'+d+'"':"")+">"+a+"</li>"},j=function(a,e,f,g){return'<a tabindex="0"'+("undefined"!=typeof e?' class="'+e+'"':"")+("undefined"!=typeof f?' style="'+f+'"':"")+(d.options.liveSearchNormalize?' data-normalized-text="'+b(c(a))+'"':"")+("undefined"!=typeof g||null!==g?' data-tokens="'+g+'"':"")+">"+a+'<span class="'+d.options.iconBase+" "+d.options.tickIcon+' check-mark"></span></a>'};if(this.options.title&&!this.multiple&&(h--,!this.$element.find(".bs-title-option").length)){var k=this.$element[0];g.className="bs-title-option",g.appendChild(document.createTextNode(this.options.title)),g.value="",k.insertBefore(g,k.firstChild),void 0===a(k.options[k.selectedIndex]).attr("selected")&&(g.selected=!0)}return this.$element.find("option").each(function(b){var c=a(this);if(h++,!c.hasClass("bs-title-option")){var g=this.className||"",k=this.style.cssText,l=c.data("content")?c.data("content"):c.html(),m=c.data("tokens")?c.data("tokens"):null,n="undefined"!=typeof c.data("subtext")?'<small class="text-muted">'+c.data("subtext")+"</small>":"",o="undefined"!=typeof c.data("icon")?'<span class="'+d.options.iconBase+" "+c.data("icon")+'"></span> ':"",p=this.disabled||"OPTGROUP"===this.parentNode.tagName&&this.parentNode.disabled;if(""!==o&&p&&(o="<span>"+o+"</span>"),d.options.hideDisabled&&p)return void h--;if(c.data("content")||(l=o+'<span class="text">'+l+n+"</span>"),"OPTGROUP"===this.parentNode.tagName&&c.data("divider")!==!0){var q=" "+this.parentNode.className||"";if(0===c.index()){f+=1;var r=this.parentNode.label,s="undefined"!=typeof c.parent().data("subtext")?'<small class="text-muted">'+c.parent().data("subtext")+"</small>":"",t=c.parent().data("icon")?'<span class="'+d.options.iconBase+" "+c.parent().data("icon")+'"></span> ':"";r=t+'<span class="text">'+r+s+"</span>",0!==b&&e.length>0&&(h++,e.push(i("",null,"divider",f+"div"))),h++,e.push(i(r,null,"dropdown-header"+q,f))}e.push(i(j(l,"opt "+g+q,k,m),b,"",f))}else c.data("divider")===!0?e.push(i("",b,"divider")):c.data("hidden")===!0?e.push(i(j(l,g,k,m),b,"hidden is-hidden")):(this.previousElementSibling&&"OPTGROUP"===this.previousElementSibling.tagName&&(h++,e.push(i("",null,"divider",f+"div"))),e.push(i(j(l,g,k,m),b)));d.liObj[b]=h}}),this.multiple||0!==this.$element.find("option:selected").length||this.options.title||this.$element.find("option").eq(0).prop("selected",!0).attr("selected","selected"),e.join("")},findLis:function(){return null==this.$lis&&(this.$lis=this.$menu.find("li")),this.$lis},render:function(b){var c,d=this;b!==!1&&this.$element.find("option").each(function(a){var b=d.findLis().eq(d.liObj[a]);d.setDisabled(a,this.disabled||"OPTGROUP"===this.parentNode.tagName&&this.parentNode.disabled,b),d.setSelected(a,this.selected,b)}),this.tabIndex();var e=this.$element.find("option").map(function(){if(this.selected){if(d.options.hideDisabled&&(this.disabled||"OPTGROUP"===this.parentNode.tagName&&this.parentNode.disabled))return;var b,c=a(this),e=c.data("icon")&&d.options.showIcon?'<i class="'+d.options.iconBase+" "+c.data("icon")+'"></i> ':"";return b=d.options.showSubtext&&c.data("subtext")&&!d.multiple?' <small class="text-muted">'+c.data("subtext")+"</small>":"","undefined"!=typeof c.attr("title")?c.attr("title"):c.data("content")&&d.options.showContent?c.data("content"):e+c.html()+b}}).toArray(),f=this.multiple?e.join(this.options.multipleSeparator):e[0];if(this.multiple&&this.options.selectedTextFormat.indexOf("count")>-1){var g=this.options.selectedTextFormat.split(">");if(g.length>1&&e.length>g[1]||1==g.length&&e.length>=2){c=this.options.hideDisabled?", [disabled]":"";var h=this.$element.find("option").not('[data-divider="true"], [data-hidden="true"]'+c).length,i="function"==typeof this.options.countSelectedText?this.options.countSelectedText(e.length,h):this.options.countSelectedText;f=i.replace("{0}",e.length.toString()).replace("{1}",h.toString())}}void 0==this.options.title&&(this.options.title=this.$element.attr("title")),"static"==this.options.selectedTextFormat&&(f=this.options.title),f||(f="undefined"!=typeof this.options.title?this.options.title:this.options.noneSelectedText),this.$button.attr("title",a.trim(f.replace(/<[^>]*>?/g,""))),this.$button.children(".filter-option").html(f),this.$element.trigger("rendered.bs.select")},setStyle:function(a,b){this.$element.attr("class")&&this.$newElement.addClass(this.$element.attr("class").replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi,""));var c=a?a:this.options.style;"add"==b?this.$button.addClass(c):"remove"==b?this.$button.removeClass(c):(this.$button.removeClass(this.options.style),this.$button.addClass(c))},liHeight:function(b){if(b||this.options.size!==!1&&!this.sizeInfo){var c=document.createElement("div"),d=document.createElement("div"),e=document.createElement("ul"),f=document.createElement("li"),g=document.createElement("li"),h=document.createElement("a"),i=document.createElement("span"),j=this.options.header?this.$menu.find(".popover-title")[0].cloneNode(!0):null,k=this.options.liveSearch?document.createElement("div"):null,l=this.options.actionsBox&&this.multiple?this.$menu.find(".bs-actionsbox")[0].cloneNode(!0):null,m=this.options.doneButton&&this.multiple?this.$menu.find(".bs-donebutton")[0].cloneNode(!0):null;if(i.className="text",c.className=this.$menu[0].parentNode.className+" open",d.className="dropdown-menu open",e.className="dropdown-menu inner",f.className="divider",i.appendChild(document.createTextNode("Inner text")),h.appendChild(i),g.appendChild(h),e.appendChild(g),e.appendChild(f),j&&d.appendChild(j),k){var n=document.createElement("span");k.className="bs-searchbox",n.className="form-control",k.appendChild(n),d.appendChild(k)}l&&d.appendChild(l),d.appendChild(e),m&&d.appendChild(m),c.appendChild(d),document.body.appendChild(c);var o=h.offsetHeight,p=j?j.offsetHeight:0,q=k?k.offsetHeight:0,r=l?l.offsetHeight:0,s=m?m.offsetHeight:0,t=a(f).outerHeight(!0),u="function"==typeof getComputedStyle?getComputedStyle(d):!1,v=u?null:a(d),w=parseInt(u?u.paddingTop:v.css("paddingTop"))+parseInt(u?u.paddingBottom:v.css("paddingBottom"))+parseInt(u?u.borderTopWidth:v.css("borderTopWidth"))+parseInt(u?u.borderBottomWidth:v.css("borderBottomWidth")),x=w+parseInt(u?u.marginTop:v.css("marginTop"))+parseInt(u?u.marginBottom:v.css("marginBottom"))+2;document.body.removeChild(c),this.sizeInfo={liHeight:o,headerHeight:p,searchHeight:q,actionsHeight:r,doneButtonHeight:s,dividerHeight:t,menuPadding:w,menuExtras:x}}},setSize:function(){if(this.findLis(),this.liHeight(),this.options.header&&this.$menu.css("padding-top",0),this.options.size!==!1){var b,c,d,e,f=this,g=this.$menu,h=this.$menuInner,i=a(window),j=this.$newElement[0].offsetHeight,k=this.sizeInfo.liHeight,l=this.sizeInfo.headerHeight,m=this.sizeInfo.searchHeight,n=this.sizeInfo.actionsHeight,o=this.sizeInfo.doneButtonHeight,p=this.sizeInfo.dividerHeight,q=this.sizeInfo.menuPadding,r=this.sizeInfo.menuExtras,s=this.options.hideDisabled?".disabled":"",t=function(){d=f.$newElement.offset().top-i.scrollTop(),e=i.height()-d-j};if(t(),"auto"===this.options.size){var u=function(){var i,j=function(b,c){return function(d){return c?d.classList?d.classList.contains(b):a(d).hasClass(b):!(d.classList?d.classList.contains(b):a(d).hasClass(b))}},p=f.$menuInner[0].getElementsByTagName("li"),s=Array.prototype.filter?Array.prototype.filter.call(p,j("hidden",!1)):f.$lis.not(".hidden"),u=Array.prototype.filter?Array.prototype.filter.call(s,j("dropdown-header",!0)):s.filter(".dropdown-header");t(),b=e-r,f.options.container?(g.data("height")||g.data("height",g.height()),c=g.data("height")):c=g.height(),f.options.dropupAuto&&f.$newElement.toggleClass("dropup",d>e&&c>b-r),f.$newElement.hasClass("dropup")&&(b=d-r),i=s.length+u.length>3?3*k+r-2:0,g.css({"max-height":b+"px",overflow:"hidden","min-height":i+l+m+n+o+"px"}),h.css({"max-height":b-l-m-n-o-q+"px","overflow-y":"auto","min-height":Math.max(i-q,0)+"px"})};u(),this.$searchbox.off("input.getSize propertychange.getSize").on("input.getSize propertychange.getSize",u),i.off("resize.getSize scroll.getSize").on("resize.getSize scroll.getSize",u)}else if(this.options.size&&"auto"!=this.options.size&&this.$lis.not(s).length>this.options.size){var v=this.$lis.not(".divider").not(s).children().slice(0,this.options.size).last().parent().index(),w=this.$lis.slice(0,v+1).filter(".divider").length;b=k*this.options.size+w*p+q,f.options.container?(g.data("height")||g.data("height",g.height()),c=g.data("height")):c=g.height(),f.options.dropupAuto&&this.$newElement.toggleClass("dropup",d>e&&c>b-r),g.css({"max-height":b+l+m+n+o+"px",overflow:"hidden","min-height":""}),h.css({"max-height":b-q+"px","overflow-y":"auto","min-height":""})}}},setWidth:function(){if("auto"===this.options.width){this.$menu.css("min-width","0");var a=this.$menu.parent().clone().appendTo("body"),b=this.options.container?this.$newElement.clone().appendTo("body"):a,c=a.children(".dropdown-menu").outerWidth(),d=b.css("width","auto").children("button").outerWidth();a.remove(),b.remove(),this.$newElement.css("width",Math.max(c,d)+"px")}else"fit"===this.options.width?(this.$menu.css("min-width",""),this.$newElement.css("width","").addClass("fit-width")):this.options.width?(this.$menu.css("min-width",""),this.$newElement.css("width",this.options.width)):(this.$menu.css("min-width",""),this.$newElement.css("width",""));this.$newElement.hasClass("fit-width")&&"fit"!==this.options.width&&this.$newElement.removeClass("fit-width")},selectPosition:function(){var b,c,d=this,e=a('<div class="bs-container" />'),f=function(a){e.addClass(a.attr("class").replace(/form-control|fit-width/gi,"")).toggleClass("dropup",a.hasClass("dropup")),b=a.offset(),c=a.hasClass("dropup")?0:a[0].offsetHeight,e.css({top:b.top+c,left:b.left,width:a[0].offsetWidth})};this.$newElement.on("click",function(){d.isDisabled()||(f(a(this)),e.appendTo(d.options.container),e.toggleClass("open",!a(this).hasClass("open")),e.append(d.$menu))}),a(window).on("resize scroll",function(){f(d.$newElement)}),this.$element.on("hide.bs.select",function(){d.$menu.data("height",d.$menu.height()),e.detach()})},setSelected:function(a,b,c){c||(c=this.findLis().eq(this.liObj[a])),c.toggleClass("selected",b)},setDisabled:function(a,b,c){c||(c=this.findLis().eq(this.liObj[a])),b?c.addClass("disabled").children("a").attr("href","#").attr("tabindex",-1):c.removeClass("disabled").children("a").removeAttr("href").attr("tabindex",0)},isDisabled:function(){return this.$element[0].disabled},checkDisabled:function(){var a=this;this.isDisabled()?(this.$newElement.addClass("disabled"),this.$button.addClass("disabled").attr("tabindex",-1)):(this.$button.hasClass("disabled")&&(this.$newElement.removeClass("disabled"),this.$button.removeClass("disabled")),-1!=this.$button.attr("tabindex")||this.$element.data("tabindex")||this.$button.removeAttr("tabindex")),this.$button.click(function(){return!a.isDisabled()})},tabIndex:function(){this.$element.is("[tabindex]")&&(this.$element.data("tabindex",this.$element.attr("tabindex")),this.$button.attr("tabindex",this.$element.data("tabindex")))},clickListener:function(){var b=this,c=a(document);this.$newElement.on("touchstart.dropdown",".dropdown-menu",function(a){a.stopPropagation()}),c.data("spaceSelect",!1),this.$button.on("keyup",function(a){/(32)/.test(a.keyCode.toString(10))&&c.data("spaceSelect")&&(a.preventDefault(),c.data("spaceSelect",!1))}),this.$newElement.on("click",function(){b.setSize(),b.$element.on("shown.bs.select",function(){if(b.options.liveSearch||b.multiple){if(!b.multiple){var a=b.liObj[b.$element[0].selectedIndex];if("number"!=typeof a||b.options.size===!1)return;var c=b.$lis.eq(a)[0].offsetTop-b.$menuInner[0].offsetTop;c=c-b.$menuInner[0].offsetHeight/2+b.sizeInfo.liHeight/2,b.$menuInner[0].scrollTop=c}}else b.$menuInner.find(".selected a").focus()})}),this.$menuInner.on("click","li a",function(c){var d=a(this),e=d.parent().data("originalIndex"),f=b.$element.val(),g=b.$element.prop("selectedIndex");if(b.multiple&&c.stopPropagation(),c.preventDefault(),!b.isDisabled()&&!d.parent().hasClass("disabled")){var h=b.$element.find("option"),i=h.eq(e),j=i.prop("selected"),k=i.parent("optgroup"),l=b.options.maxOptions,m=k.data("maxOptions")||!1;if(b.multiple){if(i.prop("selected",!j),b.setSelected(e,!j),d.blur(),l!==!1||m!==!1){var n=l<h.filter(":selected").length,o=m<k.find("option:selected").length;if(l&&n||m&&o)if(l&&1==l)h.prop("selected",!1),i.prop("selected",!0),b.$menuInner.find(".selected").removeClass("selected"),b.setSelected(e,!0);else if(m&&1==m){k.find("option:selected").prop("selected",!1),i.prop("selected",!0);var p=d.parent().data("optgroup");b.$menuInner.find('[data-optgroup="'+p+'"]').removeClass("selected"),b.setSelected(e,!0)}else{var q="function"==typeof b.options.maxOptionsText?b.options.maxOptionsText(l,m):b.options.maxOptionsText,r=q[0].replace("{n}",l),s=q[1].replace("{n}",m),t=a('<div class="notify"></div>');q[2]&&(r=r.replace("{var}",q[2][l>1?0:1]),s=s.replace("{var}",q[2][m>1?0:1])),i.prop("selected",!1),b.$menu.append(t),l&&n&&(t.append(a("<div>"+r+"</div>")),b.$element.trigger("maxReached.bs.select")),m&&o&&(t.append(a("<div>"+s+"</div>")),b.$element.trigger("maxReachedGrp.bs.select")),setTimeout(function(){b.setSelected(e,!1)},10),t.delay(750).fadeOut(300,function(){a(this).remove()})}}}else h.prop("selected",!1),i.prop("selected",!0),b.$menuInner.find(".selected").removeClass("selected"),b.setSelected(e,!0);b.multiple?b.options.liveSearch&&b.$searchbox.focus():b.$button.focus(),(f!=b.$element.val()&&b.multiple||g!=b.$element.prop("selectedIndex")&&!b.multiple)&&(b.$element.triggerNative("change"),b.$element.trigger("changed.bs.select",[e,i.prop("selected"),j]))}}),this.$menu.on("click","li.disabled a, .popover-title, .popover-title :not(.close)",function(c){c.currentTarget==this&&(c.preventDefault(),c.stopPropagation(),b.options.liveSearch&&!a(c.target).hasClass("close")?b.$searchbox.focus():b.$button.focus())}),this.$menuInner.on("click",".divider, .dropdown-header",function(a){a.preventDefault(),a.stopPropagation(),b.options.liveSearch?b.$searchbox.focus():b.$button.focus()}),this.$menu.on("click",".popover-title .close",function(){b.$button.click()}),this.$searchbox.on("click",function(a){a.stopPropagation()}),this.$menu.on("click",".actions-btn",function(c){b.options.liveSearch?b.$searchbox.focus():b.$button.focus(),c.preventDefault(),c.stopPropagation(),a(this).hasClass("bs-select-all")?b.selectAll():b.deselectAll(),b.$element.triggerNative("change")}),this.$element.change(function(){b.render(!1)})},liveSearchListener:function(){var d=this,e=a('<li class="no-results"></li>');this.$newElement.on("click.dropdown.data-api touchstart.dropdown.data-api",function(){d.$menuInner.find(".active").removeClass("active"),d.$searchbox.val()&&(d.$searchbox.val(""),d.$lis.not(".is-hidden").removeClass("hidden"),e.parent().length&&e.remove()),d.multiple||d.$menuInner.find(".selected").addClass("active"),setTimeout(function(){d.$searchbox.focus()},10)}),this.$searchbox.on("click.dropdown.data-api focus.dropdown.data-api touchend.dropdown.data-api",function(a){a.stopPropagation()}),this.$searchbox.on("input propertychange",function(){if(d.$searchbox.val()){var f=d.$lis.not(".is-hidden").removeClass("hidden").children("a");f=d.options.liveSearchNormalize?f.not(":a"+d._searchStyle()+'("'+b(d.$searchbox.val())+'")'):f.not(":"+d._searchStyle()+'("'+d.$searchbox.val()+'")'),f.parent().addClass("hidden"),d.$lis.filter(".dropdown-header").each(function(){var b=a(this),c=b.data("optgroup");0===d.$lis.filter("[data-optgroup="+c+"]").not(b).not(".hidden").length&&(b.addClass("hidden"),d.$lis.filter("[data-optgroup="+c+"div]").addClass("hidden"))});var g=d.$lis.not(".hidden");g.each(function(b){var c=a(this);c.hasClass("divider")&&(c.index()===g.first().index()||c.index()===g.last().index()||g.eq(b+1).hasClass("divider"))&&c.addClass("hidden")}),d.$lis.not(".hidden, .no-results").length?e.parent().length&&e.remove():(e.parent().length&&e.remove(),e.html(d.options.noneResultsText.replace("{0}",'"'+c(d.$searchbox.val())+'"')).show(),d.$menuInner.append(e))}else d.$lis.not(".is-hidden").removeClass("hidden"),e.parent().length&&e.remove();d.$lis.filter(".active").removeClass("active"),d.$searchbox.val()&&d.$lis.not(".hidden, .divider, .dropdown-header").eq(0).addClass("active").children("a").focus(),a(this).focus()})},_searchStyle:function(){var a={begins:"ibegins",startsWith:"ibegins"};return a[this.options.liveSearchStyle]||"icontains"},val:function(a){return"undefined"!=typeof a?(this.$element.val(a),this.render(),this.$element):this.$element.val()},changeAll:function(b){"undefined"==typeof b&&(b=!0),this.findLis();for(var c=this.$element.find("option"),d=this.$lis.not(".divider, .dropdown-header, .disabled, .hidden").toggleClass("selected",b),e=d.length,f=[],g=0;e>g;g++){var h=d[g].getAttribute("data-original-index");f[f.length]=c.eq(h)[0]}a(f).prop("selected",b),this.render(!1)},selectAll:function(){return this.changeAll(!0)},deselectAll:function(){return this.changeAll(!1)},keydown:function(c){var d,e,f,g,h,i,j,k,l,m=a(this),n=m.is("input")?m.parent().parent():m.parent(),o=n.data("this"),p=":not(.disabled, .hidden, .dropdown-header, .divider)",q={32:" ",48:"0",49:"1",50:"2",51:"3",52:"4",53:"5",54:"6",55:"7",56:"8",57:"9",59:";",65:"a",66:"b",67:"c",68:"d",69:"e",70:"f",71:"g",72:"h",73:"i",74:"j",75:"k",76:"l",77:"m",78:"n",79:"o",80:"p",81:"q",82:"r",83:"s",84:"t",85:"u",86:"v",87:"w",88:"x",89:"y",90:"z",96:"0",97:"1",98:"2",99:"3",100:"4",101:"5",102:"6",103:"7",104:"8",105:"9"};if(o.options.liveSearch&&(n=m.parent().parent()),o.options.container&&(n=o.$menu),d=a("[role=menu] li",n),l=o.$menu.parent().hasClass("open"),!l&&(c.keyCode>=48&&c.keyCode<=57||c.keyCode>=96&&c.keyCode<=105||c.keyCode>=65&&c.keyCode<=90)&&(o.options.container?o.$newElement.trigger("click"):(o.setSize(),o.$menu.parent().addClass("open"),l=!0),o.$searchbox.focus()),o.options.liveSearch&&(/(^9$|27)/.test(c.keyCode.toString(10))&&l&&0===o.$menu.find(".active").length&&(c.preventDefault(),o.$menu.parent().removeClass("open"),o.options.container&&o.$newElement.removeClass("open"),o.$button.focus()),d=a("[role=menu] li"+p,n),m.val()||/(38|40)/.test(c.keyCode.toString(10))||0===d.filter(".active").length&&(d=o.$menuInner.find("li"),d=o.options.liveSearchNormalize?d.filter(":a"+o._searchStyle()+"("+b(q[c.keyCode])+")"):d.filter(":"+o._searchStyle()+"("+q[c.keyCode]+")"))),d.length){if(/(38|40)/.test(c.keyCode.toString(10)))e=d.index(d.find("a").filter(":focus").parent()),g=d.filter(p).first().index(),h=d.filter(p).last().index(),f=d.eq(e).nextAll(p).eq(0).index(),i=d.eq(e).prevAll(p).eq(0).index(),j=d.eq(f).prevAll(p).eq(0).index(),o.options.liveSearch&&(d.each(function(b){a(this).hasClass("disabled")||a(this).data("index",b)}),e=d.index(d.filter(".active")),g=d.first().data("index"),h=d.last().data("index"),f=d.eq(e).nextAll().eq(0).data("index"),i=d.eq(e).prevAll().eq(0).data("index"),j=d.eq(f).prevAll().eq(0).data("index")),k=m.data("prevIndex"),38==c.keyCode?(o.options.liveSearch&&e--,e!=j&&e>i&&(e=i),g>e&&(e=g),e==k&&(e=h)):40==c.keyCode&&(o.options.liveSearch&&e++,-1==e&&(e=0),e!=j&&f>e&&(e=f),e>h&&(e=h),e==k&&(e=g)),m.data("prevIndex",e),o.options.liveSearch?(c.preventDefault(),m.hasClass("dropdown-toggle")||(d.removeClass("active").eq(e).addClass("active").children("a").focus(),m.focus())):d.eq(e).children("a").focus();else if(!m.is("input")){var r,s,t=[];d.each(function(){a(this).hasClass("disabled")||a.trim(a(this).children("a").text().toLowerCase()).substring(0,1)==q[c.keyCode]&&t.push(a(this).index())}),r=a(document).data("keycount"),r++,a(document).data("keycount",r),s=a.trim(a(":focus").text().toLowerCase()).substring(0,1),s!=q[c.keyCode]?(r=1,a(document).data("keycount",r)):r>=t.length&&(a(document).data("keycount",0),r>t.length&&(r=1)),d.eq(t[r-1]).children("a").focus()}if((/(13|32)/.test(c.keyCode.toString(10))||/(^9$)/.test(c.keyCode.toString(10))&&o.options.selectOnTab)&&l){if(/(32)/.test(c.keyCode.toString(10))||c.preventDefault(),o.options.liveSearch)/(32)/.test(c.keyCode.toString(10))||(o.$menuInner.find(".active a").click(),m.focus());else{var u=a(":focus");u.click(),u.focus(),c.preventDefault(),a(document).data("spaceSelect",!0)}a(document).data("keycount",0)}(/(^9$|27)/.test(c.keyCode.toString(10))&&l&&(o.multiple||o.options.liveSearch)||/(27)/.test(c.keyCode.toString(10))&&!l)&&(o.$menu.parent().removeClass("open"),o.options.container&&o.$newElement.removeClass("open"),o.$button.focus())}},mobile:function(){this.$element.addClass("mobile-device").appendTo(this.$newElement),this.options.container&&this.$menu.hide()},refresh:function(){this.$lis=null,this.liObj={},this.reloadLi(),this.render(),this.checkDisabled(),this.liHeight(!0),this.setStyle(),this.setWidth(),this.$lis&&this.$searchbox.trigger("propertychange"),this.$element.trigger("refreshed.bs.select")},hide:function(){this.$newElement.hide()},show:function(){this.$newElement.show()},remove:function(){this.$newElement.remove(),this.$element.remove()}};var f=a.fn.selectpicker;a.fn.selectpicker=d,a.fn.selectpicker.Constructor=e,a.fn.selectpicker.noConflict=function(){return a.fn.selectpicker=f,this},a(document).data("keycount",0).on("keydown",'.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input',e.prototype.keydown).on("focusin.modal",'.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input',function(a){a.stopPropagation()}),a(window).on("load.bs.select.data-api",function(){a(".selectpicker").each(function(){var b=a(this);d.call(b,b.data())})})}(a)});
