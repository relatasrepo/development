function documentsCommon($scope, share) {

    var fieldTypes = ['Number', 'String', 'Alpha Numeric', 'Percentage', 'Formula', 'Email Id', 'System Referenced'];

    var opportunityReferencedAttributes =  ['Amount',
                                            'BusinessUnit',
                                            'Currency',
                                            'ContactEmailId',
                                            'Opportunity Name',
                                            'ProductType',
                                            'Source',
                                            'Solution',
                                            'UserEmailId',
                                            'Vertical'];

    var mlAccountInsightsReferencedAttributes =  ['Company Name',
                                                    'Account Name',
                                                    'User Email',
                                                    'Interactions Count',
                                                    'Opps Won',
                                                    'Opps Lost Count',
                                                    'Opps Pipeline',
                                                    'Opps Total',
                                                    'Recommended Product',
                                                    'Recommended User',
                                                    'Recommended Amount',
                                                    'Positive Sentiments',
                                                    'Negative Sentiments',
                                                    'Signed Documents Sent',
                                                    'Important Documents Requested',
                                                    'Unanswered Questions'];

    var mlOppsInsightsReferencedAttributes =  ['User Email',
                                                'Opportunity Name',
                                                // 'Account Name',
                                                'Contact Email',
                                                'RoadBlock Observed',
                                                'Negative Sentiment',
                                                'No Response To Customer'];

    var systemReferencedEntities = ['Opportunity',
                                        'Account Details',
        'ML Account Insights',
        'ML Opportunity Insights'];

    var systemReferencedEntitiesRelatas = ['Opportunity',
                                            'Account Details',
                                            'ML Account Insights',
                                            'ML Opportunity Insights'];

    var fontTypes = ['Arial', 'Helvetica', 'Times New Roman', 'Courier New', 'Courier', 'Verdana']

    // if($scope.userEmailId == "sachin@relatas.com" || $scope.userEmailId == "sudip@relatas.com" || $scope.userEmailId == "sureshhoel@gmail.com") {
    //     systemReferencedEntities = ['Opportunity',
    //         // 'Customer Profiles',
    //         'ML Account Insights',
    //         'ML Opportunity Insights'];
    //
    // }else {
    //     systemReferencedEntities = ['Opportunity'];
    // }

    var accountReferencedAttributes =  ['Account Name', 'Domain', 'Branch', 'Address', 'Phone'];
    var contactReferencedAttributes =  ['Name', 'Email Id', 'Phone Number','Company'];
    var userDetailsReferencedAttributes = ['First Name', 'Last Name', 'Full Name', 'Email Id', 'Phone Number', 'Company']

    var systemReferencedEntities = ['Opportunity','Master Data', 'Account Details', 'Contact Details', 'User Details'];
 
    var fieldGeneralOptions = [{
                                'Name': 'Mandatory',
                                'Value': false
                            },{
                                'Name': 'Field Value Editable in Document',
                                'Value': true
                            }, {
                                'Name': 'Add default value in Document',
                                'Value': false
                            }, {
                                'Name': 'Hide Input Border',
                                'Value': false
                            },{
                                'Name': 'Hide Field Label',
                                'Value': false
                            }];

    var fieldGeneralOptionsNew = [{
                                    'Name': 'Mandatory',
                                    'Value': false
                                },{
                                    'Name': 'Field Value Editable in Document',
                                    'Value': true
                                }, {
                                    'Name': 'Add default value in Document',
                                    'Value': false
                                }, {
                                    'Name': 'Hide Input Border',
                                    'Value': false
                                }];

    var textFieldSettings = {
        FIELD_LABEL: 'Field Label',
        PLACEHOLDER: 'Placeholder',
        FIELD_TYPE: 'Field Type',
        FORMULA: 'Formula',
        SYSTEM_REFERENCED: 'System Referenced',
        CUSTOMER_PROFILE_TEMPLATE_TYPE: 'Customer Profile Template Type',
        CUSTOMER_PROFILE_TEMPLATE_NAME: 'Customer Profile Template Name',
        SYSTEM_REFERENCED_ATTRIBUTES: 'System Referenced Attributes',
        MAX_INPUT_LENGTH: 'Max Input Length',
        COLUMN_SPAN: 'Column Span',
        MANDATORY: 'Mandatory',
    }

    var dateFieldSettings = {
        FIELD_LABEL: 'Field Label',
        COLUMN_SPAN: 'Column Span',
        MANDATORY: 'Mandatory',
    }

    var paragraghTextFieldSettings = {
        FIELD_LABEL: 'Field Label',
        PLACEHOLDER: 'Placeholder',
        FIELD_TYPE: 'Field Type',
        FORMULA: 'Formula',
        SYSTEM_REFERENCED: 'System Referenced',
        CUSTOMER_PROFILE_TEMPLATE_TYPE: 'Customer Profile Template Type',
        CUSTOMER_PROFILE_TEMPLATE_NAME: 'Customer Profile Template Name',
        SYSTEM_REFERENCED_ATTRIBUTES: 'System Referenced Attributes',
        COLUMN_SPAN: 'Column Span',
        MANDATORY: 'Mandatory',
    }

    var imageFieldSettings = {
        COLUMN_SPAN: 'Column Span',
        RELATIVE_PATH: 'Relative_Path',
        ABSOLUTE_PATH: 'Absolute_Path'
    }

    var tableFieldSettings = {
        COLUMN_SPAN: 'Column Span'
    }

    var headerFieldSettings = {
        FIELD_LABEL: 'Field Label',
        COLUMN_SPAN: 'Column Span',
        FONT_SIZE: 'Font Size'
    }

    var columnFieldSettings = {
        FIELD_LABEL: 'Field Label',
        PLACEHOLDER: 'Placeholder',
        FIELD_TYPE: 'Field Type',
        FORMULA: 'Formula',
        SYSTEM_REFERENCED: 'System Referenced',
        CUSTOMER_PROFILE_TEMPLATE_TYPE: 'Customer Profile Template Type',
        CUSTOMER_PROFILE_TEMPLATE_NAME: 'Customer Profile Template Name',
        SYSTEM_REFERENCED_ATTRIBUTES: 'System Referenced Attributes',
        MAX_INPUT_LENGTH: 'Max Input Length',
        COLUMN_SPAN: 'Column Span',
        MANDATORY: 'Mandatory',
    }

    this.generateAttributeList = function () {
        var docAttributeList = [];
        $scope.formFields.forEach(function (field) {
            if (!(field.Name === 'Image' || field.Name === 'Table' || field.Type === 'header')) {
                docAttributeList.push({
                    attributeId: field.id,
                    attributeName: field.Name,
                    attributeValue: field.Value,
                    attributeType: $scope.getFieldSetting(field, 'Field Type').Value,
                    attributeFormula: $scope.getFieldSetting(field, 'Formula').Value,
                    systemReferencedCollection: $scope.getFieldSetting(field, 'System Referenced').Value,
                    systemReferencedAttributeName: $scope.getFieldSetting(field, 'System Referenced Attributes').Value,
                    isAttributeMandatory: $scope.getFieldSetting(field, 'Mandatory').Value,
                    isAttributeCustomerVisible: $scope.getFieldSetting(field, 'Customer Visible').Value,
                    repeatOnEveryPage: false
                })
            }
        })

        return docAttributeList;
    }

    this.getColumnFieldSettings = function() {
        return columnFieldSettings;
    }

    this.validateFieldOnKeyPress = function(element, event) {
        var scope = angular.element($('#documents-controller')).scope();
    
        var field = $(element).data('val');
        var fieldType = scope.getFieldSetting(field, 'Field Type').Value;
        var inputLength= scope.getFieldSetting(field, 'Max Input Length').Value;
        
        if(inputLength && inputLength < element.value.length) {
            return false;
        }
    
        switch(fieldType) {
            case 'Formula':
            case 'System Generated':
                        return false;
            case 'Number':
                        return validateFloatKeyPress(element, event);
            case 'String': 
                        return validateStringOnkeyPress(element, event);
            default:
                        return true;
        }
    }

    function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');

    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
        return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}

function validateStringOnkeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    return ( charCode > 31 && (charCode === 32 || (charCode >= 65 && charCode <= 90) || (charCode >= 96 && charCode <= 122)) );
}

    share.document = {
        getFieldTypes: fieldTypes,
        getOpportunityReferencedAttributes: opportunityReferencedAttributes,
        getMLAccountInsightsReferencedAttributes:mlAccountInsightsReferencedAttributes,
        getMLOppsInsightsReferencedAttributes:mlOppsInsightsReferencedAttributes,
        getAccountReferencedAttributes: accountReferencedAttributes,
        getContactReferencedAttributes: contactReferencedAttributes,
        getUserDetailsReferencedAttributes: userDetailsReferencedAttributes,
        getFieldGeneralOptions: fieldGeneralOptions,
        getSystemReferencedEntities: systemReferencedEntities,
        getSystemReferencedEntitiesRelatas: systemReferencedEntitiesRelatas,
        getFontTypes: fontTypes

    }

    $scope.getFieldSetting = function (field, settingName) {
        
        var result = {};
        var settings = field.Settings;
        $.each(settings, function (index, set) {
            if (set.Name == settingName) {
                result = set;
                return;
            }
        });

        if (!Object.keys(result).length) {

            if(settings[settings.length - 1].Options) {
                $.each(settings[settings.length - 1].Options, function (index, set) {
                    if (set.Name == settingName) {
                        result = set;
                        return;
                    }
                });
            }
        }
        return result;
    }

    $scope.changeFieldSetting = function (value, settingName) {
        console.log("Change Field Settings:", value, " Setting name:", settingName);
        switch (settingName) {
            case 'Field Label':
            case 'Short Label':
            case 'Internal Name':
                $scope.current_field.Name = value;
                $scope.current_field.Settings[0].Value = $scope.current_field.Name;
                break;
            case 'Column Name':
                $scope.current_field.Name = value;
                break;
            case 'System Referenced Attributes':
            case 'Master Data Type':
            case 'Master Data Type Attributes':
                $scope.getFieldSetting($scope.current_field, settingName).PossibleValue = value;
                break;
            /* case 'Master Data Field Group':
                $scope.getFieldSetting($scope.current_field, settingName).Value = value;
                break; */
            default:
                break;
        }
    }

    $scope.getElementWidth = function (field) {
        $("#"+field.id).css("height", "auto");
        $("#"+field.id).css("width", "auto");
    }

    $scope.injectStyles = function(field) {

        if(field.Type !== 'table') {
            var textAlign = $scope.getFieldSetting(field, 'Input Value Align').Value;
            var fontSize = $scope.getFieldSetting(field, 'Font Size').Value;
            var labelColor = $scope.getFieldSetting(field, 'Field Label Color').Value;
            var textColor = $scope.getFieldSetting(field, 'Field Text Color').Value;
            var BackgroundColor = $scope.getFieldSetting(field, 'Field Background Color').Value;
            var fontFamily = $scope.getFieldSetting(field, 'Fonts').Value;

            if(field.newHeight && field.Type == 'textarea') {
                $("#input-"+field.id).css("height", field.newHeight);
            }

            $("#input-"+field.id).css("color", textColor);
            $("#input-"+field.id).css("background", BackgroundColor);
            $("#label-"+field.id).css("color", labelColor);

            $("#input-"+field.id).css("text-align", textAlign);
            $("#input-"+field.id).css("font-size", fontSize+"px");
            $("#input-"+field.id).css("font-family", fontFamily);
            
            if($scope.getFieldSetting(field, 'Hide Input Border').Value) {
                $("#input-"+field.id).addClass('hide-border');
            } else {
                $("#input-"+field.id).removeClass('hide-border');
            }

            if($scope.getFieldSetting(field, 'Hide Field Label').Value) {
                $("#label-"+field.id).addClass('hide-field-label');
            } else {
                $("#label-"+field.id).removeClass('hide-field-label');
            }
        } else if(field.Type == 'table') {
            var rowTextAlign = "";
            var fontSize;
            var textColor;
            var backgroundColor;
            var headerFontSize;
            var headerTextAlign;
            var headerBackgroundColor;
            var headerTextColor;
            var fontFamily;

            headerTextAlign = $scope.getFieldSetting(field, 'Header Value Align').Value;
            headerFontSize = $scope.getFieldSetting(field, 'Header Font Size').Value;
            headerTextColor = $scope.getFieldSetting(field, 'Header Text Color').Value;
            headerBackgroundColor = $scope.getFieldSetting(field, 'Header Background Color').Value;
            fontFamily = $scope.getFieldSetting(field, 'Fonts').Value;

            $(".table-th-"+field.id).css("text-align", headerTextAlign);
            $(".table-th-"+field.id).css("font-size", headerFontSize+"px");
            $(".table-th-"+field.id).css("color", headerTextColor);
            $(".table-th-"+field.id).css("background", headerBackgroundColor);

            field.Columns.forEach(function(column, index) {

                rowTextAlign = $scope.getFieldSetting(column, 'Row Value Align').Value;
                fontSize = $scope.getFieldSetting(column, 'Font Size').Value;
                textColor = $scope.getFieldSetting(column, 'Column Text Color').Value;
                backgroundColor = $scope.getFieldSetting(column, 'Column Background Color').Value;

                $(".input-table-"+field.id+String(index)).css("text-align", rowTextAlign);
                $(".input-table-"+field.id+String(index)).css("font-size", fontSize+"px");
                $(".input-table-"+field.id+String(index)).css("color", textColor);
                $(".input-table-"+field.id+String(index)).css("background", backgroundColor);
                $(".input-table-"+field.id+String(index)).css("font-family", fontFamily);

                _.each(field.Rows, function(row, rowIndex) {
                    // console.log(row[index].newHeight);
                    // console.log("Id:", "#input-table-"+index+""+rowIndex);

                    if(row[index].newHeight)
                        $("#input-table-"+index+""+rowIndex).css("height", row[index].newHeight);
                })
            })
        }
    }

    $scope.getFontWeight = function (field) {

        var fontSize = $scope.getFieldSetting(field, 'Font Size').Value;
        var textStyle = $scope.getFieldSetting(field, 'Text Style').Value;
        var textAlign= $scope.getFieldSetting(field, 'Text Align').Value;
        var textColor = $scope.getFieldSetting(field, 'Text Color').Value;
        var BackgroundColor = $scope.getFieldSetting(field, 'Background Color').Value;

        if(textStyle == 'bold italics') {
            $("#header-"+field.id).css({"font-size":fontSize+"px", 
                                        "text-align":textAlign,
                                        "font-weight": "bold", 
                                        "font-style": "italic"});
        } else {
            $("#header-"+field.id).css({"font-size":fontSize+"px", 
                                        "text-align":textAlign,
                                        "font-weight": textStyle, 
                                        "font-style":textStyle});
        }

        $("#header-"+field.id).css("color", textColor); 
        $("#header-"+field.id).css("background", BackgroundColor);

    }

    $scope.insertPadding = function(field, paddingRegion, padding) {
        var paddingRegionString;

        if(paddingRegion == 'Top')
            paddingRegionString = 'padding-top';
        else if(paddingRegion == 'Right')
            paddingRegionString = 'padding-right';
        else if(paddingRegion == 'Bottom')
            paddingRegionString = 'padding-bottom';
        else if(paddingRegion == 'Left')
            paddingRegionString = 'padding-left';
        
        if(field.Type != 'table' || field.Type != 'header') {
            $("#input-"+field.id).css(paddingRegionString, padding+"px");

        } else if(field.Type == 'table') {
            $(".table-th-"+field.id).css(paddingRegionString, padding+"px");

            field.Columns.forEach(function(column, index) {
                $(".input-table-"+field.id+String(index)).css(paddingRegionString, padding+"px");
            })
        } else if(field.Type == 'header') {
            $("#header-"+field.id).css(paddingRegionString, padding+"px");
        }

    }

    $scope.applyPadding = function(field) {
        var paddingTop, paddingRight, paddingBottom, paddingLeft;

        if(field.Type !== 'table') {
            paddingTop = $scope.getFieldSetting(field, 'Top').Value;
            paddingRight = $scope.getFieldSetting(field, 'Right').Value;
            paddingBottom = $scope.getFieldSetting(field, 'Bottom').Value;
            paddingLeft = $scope.getFieldSetting(field, 'Left').Value;

            $("#input-"+field.id).css("padding-top", paddingTop+"px");
            $("#input-"+field.id).css("padding-right", paddingRight+"px");
            $("#input-"+field.id).css("padding-bottom", paddingBottom+"px");
            $("#input-"+field.id).css("padding-left", paddingLeft+"px");
            
        } else if(field.Type == 'table') {

            paddingTop = $scope.getFieldSetting(field, 'Top').Value;
            paddingRight = $scope.getFieldSetting(field, 'Right').Value;
            paddingBottom = $scope.getFieldSetting(field, 'Bottom').Value;
            paddingLeft = $scope.getFieldSetting(field, 'Left').Value;

            $(".table-th-"+field.id).css("padding-top", paddingTop+"px");
            $(".table-th-"+field.id).css("padding-right", paddingRight+"px");
            $(".table-th-"+field.id).css("padding-bottom", paddingBottom+"px");
            $(".table-th-"+field.id).css("padding-left", paddingLeft+"px");

            field.Columns.forEach(function(column, index) {

                paddingTop = $scope.getFieldSetting(column, 'Top').Value;
                paddingRight = $scope.getFieldSetting(column, 'Right').Value;
                paddingBottom = $scope.getFieldSetting(column, 'Bottom').Value;
                paddingLeft = $scope.getFieldSetting(column, 'Left').Value;

                $(".input-table-"+field.id+String(index)).css("padding-top", paddingTop+"px");
                $(".input-table-"+field.id+String(index)).css("padding-right", paddingRight+"px");
                $(".input-table-"+field.id+String(index)).css("padding-bottom", paddingBottom+"px");
                $(".input-table-"+field.id+String(index)).css("padding-left", paddingLeft+"px");
            })
        } else if(field.Type == 'header') {
            $("#header-"+field.id).css(paddingRegionString, padding+"px");
        }
    }

    $scope.insertRows = function (table, fromDocument) {

        var row = [];

        table.Columns.forEach(function (column) {
            var fieldType = $scope.getFieldSetting(column, 'Field Type').Value;

            row.push({
                Value: column.Value,
                Type: fieldType,
            })

        })
        table.Rows.push(row);   


        setTimeout(function() {
            increase(table, fromDocument)
        }, 400);

        

    }

    function increase(table, fromDocument) {

        if(fromDocument) {

            var tableTopValue = Number($('#'+table.id).css('top').split('px')[0].trim());

            var td = $('#'+table.id+" table tr:last");
            // var temp = $('#'+table.id).find('td')[0];
            var pushHeight = Number($(td).outerHeight());

            _.each($('#'+table.id).siblings(), function(sibling) {
                var siblingTopValue = Number($(sibling).css('top').split('px')[0].trim());
    
                if(tableTopValue < siblingTopValue) {
                    var newSiblingPos = pushHeight+siblingTopValue + 'px';
                    $(sibling).css('top', newSiblingPos);
                }
            });
        }
    }

    function increaseOnTableExpand(table, fromDocument) {

        if(fromDocument) {

            var tableTopValue = Number($('#'+table.id).css('top').split('px')[0].trim());

            var td = $('#'+table.id+" table tr:last");
            // var temp = $('#'+table.id).find('td')[0];
            var pushHeight = Number($(td).outerHeight());

            _.each($('#'+table.id).siblings(), function(sibling) {
                var siblingTopValue = Number($(sibling).css('top').split('px')[0].trim());
    
                if(tableTopValue < siblingTopValue) {
                    var newSiblingPos = pushHeight+siblingTopValue + 'px';
                    $(sibling).css('top', newSiblingPos);
                }
            });
        }
    }

    share.setHeaderBackgroundColor = function(color) {
        $('.horizontal-menu-top').css('background', color)
    }

}