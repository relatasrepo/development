function getLog($scope,$http,share,opp) {

    $http.get("/opportunities/get/log?opportunityId="+opp.opportunityId)
        .success(function (response) {
            if(response && response.SuccessCode){
                opp.logs = response.Data;

                checkTeamLoaded();

                function checkTeamLoaded(){
                    if(share.teamDictionaryByUserId){

                        _.each(opp.logs,function (log) {
                            log.dateFormatted = moment(log.date).format(standardDateFormat()) +" at "+moment(log.date).format("h:mm A")
                            log.noteExist = log.notes?true:false
                            log.message = buildLogMessage(log);
                            log.what = buildLogWhatText(log);

                            if(log.action == "removed" || log.action == "added"){
                                log.oldValue = log.oldValue.toString();
                                log.newValue = log.newValue.toString();

                            } else if(log.action == "transfer"){
                                log.oldValue = share.team[log.fromEmailId].emailId
                                log.newValue = share.team[log.toEmailId].emailId
                            }

                            if(log.type == "geoLocation") {
                                var oldVal = "",newVal = ""
                                if(log.oldValue.town){
                                    oldVal = log.oldValue.town +", "+log.oldValue.zone
                                }
                                if(log.newValue.town){
                                    newVal = log.newValue.town +", "+log.newValue.zone
                                }

                                log.oldValue = oldVal;
                                log.newValue = newVal;

                                if(log.action == "Renewal"){
                                    log.newValue = "Created";
                                }
                            }

                            log.by = share.teamDictionaryByUserId[log.fromUserId].fullName+"("+share.teamDictionaryByUserId[log.fromUserId].emailId+")"
                        });

                    } else {
                        setTimeOutCallback(200,function () {
                            checkTeamLoaded()
                        })
                    }
                }
            }
        });
}

function formatLogs($scope,$http,share,opp,logs) {

    opp.logs = logs;

    checkTeamLoaded();

    if(!share.team){
        share.team = share.usersDictionary;
    }

    function checkTeamLoaded(){
        if(share.teamDictionaryByUserId){

            _.each(opp.logs,function (log) {
                log.dateFormatted = moment(log.date).format(standardDateFormat()) +" at "+moment(log.date).format("h:mm A")
                log.noteExist = log.notes?true:false
                log.message = buildLogMessage(log);
                log.what = buildLogWhatText(log);


                if(log.action == "removed" || log.action == "added" || _.includes(log.action.toLowerCase(),"add") || _.includes(log.action.toLowerCase(),"remove")){

                    if(log.oldValue){
                        log.oldValue = log.oldValue.toString();
                    }
                    if(log.newValue){
                        log.newValue = log.newValue.toString();
                    }

                } else if(log.type == "decisionMakers" || log.type == "influencers" || log.type == "partners"){
                    log.oldValue = log.oldValue.length>0?_.map(log.oldValue,"emailId").join(","):""
                    log.newValue = log.newValue.length>0?_.map(log.newValue,"emailId").join(","):""
                } else if(log.action == "transfer"){
                    log.oldValue = share.team[log.fromEmailId].emailId
                    log.newValue = share.team[log.toEmailId].emailId
                }

                if(log.action == "close"){
                    log.oldValue = "Open";
                    log.newValue = "Close";
                }

                if(log.action == "created"){
                    log.oldValue = "";
                    log.newValue = "Created";
                }

                if(log.type == "closeDate"){
                    log.oldValue = moment(log.oldValue).format(standardDateFormat());
                    log.newValue = moment(log.newValue).format(standardDateFormat());
                }

                if(log.type == "geoLocation") {

                    var oldVal = "",newVal = ""

                    if(log.oldValue && log.oldValue.zone){
                        oldVal = log.oldValue.town +","+ log.oldValue.zone
                    } else if(log.oldValue && log.oldValue){
                        oldVal = log.oldValue
                    }

                    if(log.newValue && log.newValue.zone){
                        newVal = log.newValue.town +","+ log.newValue.zone
                    } else if(log.newValue && log.newValue){
                        newVal = log.newValue
                    }

                    log.oldValue = oldVal;
                    log.newValue = newVal;
                }

                if(log.action == "Renewal"){
                    log.newValue = "Created";
                }

                if(share.teamDictionaryByUserId[log.fromUserId]){
                    log.by = share.teamDictionaryByUserId[log.fromUserId].fullName+"("+share.teamDictionaryByUserId[log.fromUserId].emailId+")"
                }
            });

            opp.logs = opp.logs.filter(function (el) {
                if(checkRequired(el.oldValue) || checkRequired(el.newValue)){
                    return el;
                }
            })

        } else {
            setTimeOutCallback(200,function () {
                checkTeamLoaded()
            })
        }
    }
}

function buildLogMessage(log){
    var message = "";

    if(log.action == "created"){
        message = "Opp created by "+log.fromEmailId
    }

    if(log.action == "Renewal"){
        message = "Opp renewed by "+log.fromEmailId
    }

    if(log.action == "transfer"){

        var transferredBy = log.fromEmailId;

        if(log.transferredBy){
            transferredBy = log.transferredBy
        }

        message = "Opp transferred by "+transferredBy
    }

    if(log.action == "decisionMakersAdded" || log.action == "decisionMakerAdded"){
        message = "Decision maker added "
    }
    if(log.action == "partnersAdded" || log.action == "partnersAdded"){
        message = "Partner added "
    }

    if(log.action == "influencersAdded" || log.action == "influencerAdded"){
        message = "Influencer added "
    }

    if(log.action == "decisionMakerRemoved" || log.action == "decisionMakersRemoved"){
        message = "Decision maker removed "
    }
    if(log.action == "partnersRemoved" || log.action == "partnerRemoved"){
        message = "Partner removed "
    }

    if(log.action == "influencersRemoved" || log.action == "influencerRemoved"){
        message = "Influencer removed "
    }

    if(log.action == "propertyChange"){
        if(log.type == "stageName"){
            message = "Stage updated "
        }

        if(log.type == "closeDate"){
            message = "Close date updated "
        }

        if(log.type == "amount"){
            message = "Amount updated "
        }

        if(log.type == "contactEmailId"){
            message = "Contact updated "
        }

        if(log.type == "netGrossMargin"){
            message = "Net gross margin updated "
        }
    }

    if(message != ""){
        message = message+ " on "+log.dateFormatted;

        return message
    }
}

function buildLogWhatText(log){
    var message = "";

    if(log.action == "created"){
        message = "Opportunity"
    }

    if(log.action == "Renewal"){
        message = "Opportunity renewed"
    }

    if(log.action == "transfer"){
        message = "Opportunity transferred"
    }

    if(log.action == "decisionMakersAdded" || log.action == "decisionMakerAdded"){
        message = "Decision maker "
    }
    if(log.action == "partnersAdded" || log.action == "partnersAdded"){
        message = "Partner "
    }

    if(log.action == "influencersAdded" || log.action == "influencerAdded"){
        message = "Influencer "
    }

    if(log.action == "decisionMakerRemoved" || log.action == "decisionMakersRemoved"){
        message = "Decision maker "
    }
    if(log.action == "partnersRemoved" || log.action == "partnerRemoved"){
        message = "Partner "
    }

    if(log.action == "influencersRemoved" || log.action == "influencerRemoved"){
        message = "Influencer "
    }

    if(log.action == "propertyChange"){
        if(log.type == "stageName"){
            message = "Stage "
        }

        if(log.type == "closeDate"){
            message = "Close date "
        }

        if(log.type == "amount"){
            message = "Amount "
        }

        if(log.type == "contactEmailId"){
            message = "Contact "
        }

        if(log.type == "netGrossMargin"){
            message = "Net gross margin "
        }

        if(log.type == "userEmailId"){
            message = "Owner"
        }

        if(log.type == "type"){
            message = "Type"
        }

        if(log.type == "vertical"){
            message = "Vertical"
        }

        if(log.type == "solution"){
            message = "Solution"
        }

        if(log.type == "productType"){
            message = "Product"
        }

        if(log.type == "sourceType"){
            message = "Source"
        }

        if(log.type == "currency"){
            message = "Currency"
        }

        if(log.type == "businessUnit"){
            message = "Business Unit"
        }

        if(log.type == "geoLocation"){
            message = "Region"
        }

        if(log.type == "opportunityName"){
            message = "Opportunity Name"
        }

        if(log.type == "partners"){
            message = "Partner"
        }

        if(log.type == "influencers"){
            message = "Influencer"
        }

        if(log.type == "decisionMakers"){
            message = "Decision Maker"
        }


    }

    if(log.action == "removed"){
        message = log.type
    }

    if(log.action == "added"){
        message = log.type
    }

    if(log.action == "close"){
        message = "Opportunity"
    }

    return message
}