

var color = d3.scale.category20c();

var treemap =
  d3.layout.treemap()
  // use 100 x 100 px, which we'll apply as % later
  .size([100, 100])
  .sticky(false)
  .value(function(d) { return d.mcap; });



function position() {
  this
    .style("left", function(d) { return d.x + "%"; })
    .style("top", function(d) { return d.y + "%"; })
    .style("width", function(d) { return d.dx + "%"; })
    .style("height", function(d) { return d.dy + "%"; });
}

function getLabel(d) {
  return d.name+'('+d.count+')';
}

document.getElementsByClassName('texxx').title = getLabel;

function fillLocations(locations){
  var div = d3.select(".treemap");
  var data = locations;
  $( ".treemap" ).empty()
  var node =
      div.datum(data).selectAll(".node")
          .data(treemap.nodes)
          .enter().append("div")
          .attr("class", "node texxx")
          .call(position)
          .style("background", function(d) { return color(getLabel(d)); })
          .text(getLabel)
          .attr("title", function(d) { return (getLabel(d)); });
}
