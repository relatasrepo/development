function createOpportunity($http,$scope,obj,callback){

    $http.post("/salesforce/add/new/opportunity/for/contact",obj)
        .success(function (response) {

            $scope.renewalAmount = 0;
            $scope.renewalCloseDate = "";

            var account = fetchCompanyFromEmail(obj.contactEmailId);

            if(account){
                // updateAccountRelationship($scope,$http,{important:true,account:account})
            }

            oppTargetMetNotifications($http,$scope);
            
            callback(response)
        });
}

function createNote($scope,$http,contactNote,cEmailId,callback){

    contactNote = nl2br(contactNote)
    var url = '/message/create/by/id';

    var obj = {
        messageReferenceType:"contact",
        text:contactNote,
        cEmailId:cEmailId
    }

    $http.post(url,obj)
        .success(function (response) {
            callback(response)
        });
}

function pickDatesForRenewal($scope,minDate,maxDate,id,addTime,timeType,timezone){
    
    $(id).datetimepicker({
        value:"",
        timepicker:false,
        validateOnBlur:false,

        minDate: new Date(moment().subtract(2,"week")),
        // minDate: minDate,
        maxDate: maxDate,
        onSelectDate: function (dp, $input){
            $scope.$apply(function () {
                var timezone = timezone?timezone:"UTC";
                $scope.renewalCloseDate = moment(dp).format();
                $scope.renewalCloseDateFormatted = moment(dp).format(standardDateFormat());
                $("#opportunityCloseDateSelector4").val($scope.renewalCloseDateFormatted)

                $scope.opp.renewed = {
                    closeDate:moment($scope.renewalCloseDate).format(standardDateFormat()),
                    amount:$("#renewalAmount").val()
                }
            });
        }
    });
}

function deleteOpp($scope,$http,opp,callback) {

    if(opp && opp._id){
        $http.post("/opportunities/delete",{oppId:opp._id})
            .success(function (response) {
                if(response && response.SuccessCode){
                    toastr.success("Opportunity deleted successfully")
                    callback(true)
                } else {
                    toastr.success("Opportunity deletion failed. Please try again later.")
                    callback(false)
                }
            })
    }

}
