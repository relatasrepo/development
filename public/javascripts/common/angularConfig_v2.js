var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','angular-loading-bar','ngLodash']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.service('share', function () {
    return {
        setTown:function(geoLocationTown){
            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        }
    }
});

relatasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){

                // if(obj.)
                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else if(obj.name){
                    obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    obj.noPPic = obj.name.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    scope.noPPic = obj.name.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                } else if(obj.fullName){

                    obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                //call the function that was passed
                scope.$apply(attrs.imageonload);
            });
        }
    };
});

relatasApp.directive('errortoggle', function() {
    return {
        link: function(scope, element, attrs) {
            element.bind('load', function(){
                
                if(scope.contact)
                    scope.contact.noPicFlag = false

                if(scope.item)
                    scope.item.noPicFlag = false
                scope.displayLogo = false;
                scope.$apply()
            })
        }
    }
});

relatasApp.directive('imagefromemailid', function() {
    return {
        link: function(scope, element, attrs) {
            element.bind('load', function(){

                console.log(scope.el)
            })
        }
    }
});

relatasApp.service('searchContacts', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.directive('searchedContacts', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="contactList.length>0">' +
        '<div ng-repeat="item in contactList track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchedContacts1', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="assigneeBucket.length>0">' +
        '<div ng-repeat="item in assigneeBucket track by $index"> ' +
        '<div class="clearfix cursor" ng-click="selectAssignee(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

function pickDateTime($scope,minDate,maxDate,id,timezone,scopeSelector,timeRequired){

    var settings = {
        value:"",
        timepicker:timeRequired?true:false,
        validateOnBlur:false,
        onSelectDate: function (dp, $input){
        $scope.$apply(function () {
            var timezone = timezone?timezone:"UTC";
            $scope[scopeSelector] = moment(dp).tz(timezone).format();
            $scope[scopeSelector+'Formatted'] = moment(dp).tz(timezone).format("DD MMM YYYY");
            });
        }
    }

    if(minDate){
        settings.minDate = minDate;
    }

    $(id).datetimepicker(settings);
}

function closeAllDropDownsAndModals($scope,id,isModal) {
    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target) && container.has(e.target).length === 0) {

            $scope.$apply(function (){
                $scope.showVerticalList = false;
                $scope.showProductList = false;
                $scope.selectFromList = false;
                $scope.showLocationList = false;
                $scope.showDateRangePicker = false;
                $scope.showCompanyList = false;
                $scope.showContactList = false;
                $scope.contactList = [];
                $scope.showSourceList = false;
                $scope.openPriorityList = false;
                $scope.openNewStatusList = false;
                $scope.displayDatePicker = false;
                $scope.fy = false;
                if(isModal){
                    $scope.closeModal()
                }
            })
        }
    });
}