
/*
reletasApp.directive('ngAction',['$http', function ($http) {
    return {
        link:function (scope, element, attrs) {
            $http.get('/task/create/template')
                .success(function(response){
                    $("body").append(response)
                });
            element.bind("click", function (event) {

                $("#add-task-box").toggle(100);
            });
        }
    }
}]);*/

reletasApp.controller("add_task_middlebar",function($scope, $http,meetingObjService){

    $scope.fetchContactsOnChange = function(searchContent){
        if(typeof searchContent == 'string' && searchContent.length > 2){
            $scope.fetchContacts(searchContent);
        }
        else $(".search-results").hide()
    };

    $scope.fetchContacts = function(searchContent){
        if(checkRequired(searchContent) && checkRequired(searchContent.trim())){
            $(".search-results").show();
            if(!$scope.isOnProgress){
                $scope.getContacts('/contacts/filter/custom/web',0,25,'search',searchContent);
            }
        }
        else{
            $(".search-results").hide()
            toastr.error("Please enter your contact name");
        }
    };

    $scope.selectContactTask = function(userId,emailId,name){
        $scope.cEmailId = emailId;
        $scope.cuserId = userId;
        $scope.cName = name;
        $scope.searchContactAssignTask = name;
        $("#searchContactAssignTask").val(name);
        $(".search-results").hide()
    };

    $scope.assignTaskFinal = function(title,description){

        if(!checkRequired($scope.dueDate) || moment().isAfter(moment($scope.dueDate)) || moment().isSame(moment($scope.dueDate))){
            toastr.error("Please select valid date and time")
        }
        else if(!(checkRequired(title) && checkRequired(title.trim()))){
            toastr.error("Please provide Task title");
        }
        else if(!(checkRequired(description) && checkRequired(description.trim()))){
            toastr.error("Please provide Task description");
        }
        else if(!checkRequired($scope.dueDate)){
            toastr.error("Please select valid date")
        }
        else{
            var reqObj = {
                "taskName":title,
                "assignedTo":$scope.cuserId,
                "assignedToEmailId":$scope.cEmailId,
                "dueDate":$scope.dueDate,
                "taskFor":"other"
            };
            $http.post('/task/create/new/web',reqObj)
                .success(function(response){
                    if(response.SuccessCode){
                        toastr.success(response.Message);
                        $scope.closePopup(true);
                        meetingObjService.fetchLeftBarTodayData();
                    }
                    else toastr.error(response.Message);
                })
        }
    };

    $scope.closePopup = function(close){
        $("#taskTitle").val("")
        $("#taskDescription").val("")
        $("#searchContactAssignTask").val("")
        $("#dueDate_add_task_middlebar").val("")
        $scope.taskTitle = ""
        $scope.taskDescription = ""
        if(close){
            $(".add-task-landing").hide();
        }
    };

    $scope.getContacts = function(url,skip,limit,filterBy,searchContent,isBack){
        $scope.isOnProgress = true;
        $http.get(url+'?skip='+skip+'&limit='+limit+'&filterBy='+filterBy+'&searchContent='+searchContent)
            .success(function(response){
                $scope.isOnProgress = false;
                $scope.contactsList = [];
                $scope.isConnectionOpen = false;
                if(response.SuccessCode){
                    $scope.grandTotal = response.Data.total;
                    if(response.Data.contacts.length > 0){
                        if(!isBack || $scope.currentLength <= 0){
                            $scope.currentLength += response.Data.returned;
                        }

                        for(var i=0; i<response.Data.contacts.length; i++){
                            var li = '';
                            var obj;
                            if(checkRequired(response.Data.contacts[i].personId) && checkRequired(response.Data.contacts[i].personId.firstName)){
                                var name = getTextLength(response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName,25);
                                var image = '/getImage/'+response.Data.contacts[i].personId._id;
                                var url = response.Data.contacts[i].personId.publicProfileUrl;
                                var companyName = checkRequired(response.Data.contacts[i].personId.companyName) ? response.Data.contacts[i].personId.companyName : "";
                                var designation = checkRequired(response.Data.contacts[i].personId.designation) ? response.Data.contacts[i].personId.designation : "";

                                if(url.charAt(0) != 'h'){
                                    url = '/'+url;
                                }
                                obj = {
                                    userId:response.Data.contacts[i].personId._id,
                                    fullName:response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName,
                                    companyName:getTextLength(companyName,25),
                                    designation:getTextLength(designation,25),
                                    fullCompanyName:companyName,
                                    fullDesignation:designation,
                                    emailId:response.Data.contacts[i].personEmailId,
                                    name:name,
                                    image:image,
                                    url:url,
                                    cursor:'cursor:pointer',
                                    idName:'com_con_item_'+response.Data.contacts[i]._id
                                };
                                $scope.contactsList.push(obj)
                            }
                            else{
                                var companyName2 = checkRequired(response.Data.contacts[i].companyName) ? response.Data.contacts[i].companyName : ""
                                var designation2 = checkRequired(response.Data.contacts[i].designation) ? response.Data.contacts[i].designation : ""

                                obj = {
                                    userId:null,
                                    fullName:response.Data.contacts[i].personName,
                                    companyName:getTextLength(companyName2,25),
                                    designation:getTextLength(designation2,25),
                                    fullCompanyName:companyName2,
                                    fullDesignation:designation2,
                                    name:getTextLength(response.Data.contacts[i].personName,25),
                                    emailId:response.Data.contacts[i].personEmailId,
                                    image:"/images/default.png",
                                    cursor:'cursor:default',
                                    url:null,
                                    idName:'com_con_item_'+response.Data.contacts[i]._id
                                };
                                $scope.contactsList.push(obj);

                            }
                        }
                    }
                    else {
                        $scope.isAllConnectionsLoaded = true;
                        //$scope.commonConnectionsNotExist = response.Message;
                    }
                }
                else{
                    $scope.isAllConnectionsLoaded = true;
                    //$scope.commonConnectionsNotExist = response.Message;
                }
            })
    };
    $('#dueDate_add_task_middlebar').datetimepicker({
        dayOfWeekStart: 1,
        format:'d-m-Y h:i a',
        lang: 'en',
        minDate: new Date(),
        onClose: function (dp, $input){
            var selected = moment(dp);
            $scope.dueDate = selected.format();
        }
    });
});
