function getTeam($scope,$http,callback) {

    $http.get('/company/getTeamMembers')
        .success(function (response) {
            $scope.team = [];
            if(response && response.SuccessCode && response.Data && response.Data.length>0){
                $scope.team = buildTeamProfiles(response.Data)
                $scope.selection = $scope.team[0];

                var usersDictionary = {},usersDictionaryWithUserId = {};

                if($scope.team.length>0){
                    _.each($scope.team,function (member) {
                        usersDictionary[member.emailId] = member;
                        usersDictionaryWithUserId[member.userId] = member;
                    });
                }

                if(callback){
                    callback($scope.team,usersDictionary,usersDictionaryWithUserId)
                }
            }
        });
}

function buildTeamProfiles(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            hierarchyPath:el.hierarchyPath,
            corporateAdmin:el.corporateAdmin
        })
    });
    return team;
}

relatasApp.directive('searchResultsContacts', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultscontact">' +
        '<div ng-repeat="item in contacts track by $index"> ' +
        '<div class="clearfix cursor" ng-click="selectContact(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchResultsP', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultspartner">' +
        '<div ng-repeat="item in partners track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchResultsD', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultsdecisionMaker">' +
        '<div ng-repeat="item in decisionMakers track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchResultsI', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultsinfluencer">' +
        '<div ng-repeat="item in influencers track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

function viewEmail($scope,$http,dataObj,$sce){
    if(checkRequired(dataObj.interaction.emailContentId) && dataObj.interaction.source != 'relatas'){
        $http.get('/message/get/email/single/web?emailContentId='+dataObj.interaction.emailContentId+'&googleAccountEmailId='+dataObj.interaction.googleAccountEmailId)
            .success(function(response){
                dataObj.loading = false;
                if(response.SuccessCode){

                    dataObj.bodyContent = $sce.trustAsHtml(response.Data.data);

                    setTimeOutCallback(10,function () {
                        var myDomElement = document.getElementsByClassName("reply-to-this");
                        var selection = $( myDomElement ).find( "style" );
                        if(selection.length){
                            selection.remove();
                        }
                    });
                }
                else dataObj.bodyContent = response.Message || '';
            })
    }
}

function sendEmailNotificationGA($scope,$rootScope,$http,share,mailType,accounts,selectedUser){

    var corporateAdmin = null;
    // _.each(share.teamArray,function (el) {
    //     if(el.corporateAdmin){
    //         corporateAdmin = el
    //     }
    // });
    //Don't send email to corporateAdmin.

    if(mailType == "accountTransfer" && selectedUser){

        var receiverGrp2 = _.groupBy(accounts,"ownerEmailId");

        for( var key2 in receiverGrp2){
            var account = "";
            _.each(accounts,function (ac,index) {

                if(key2 == ac.ownerEmailId){

                    if(index == 0){
                        account = ac.fullName;
                    } else {
                        account = ac.fullName +"\n "+account;
                    }
                }
            });

            var ccList = [key2];

            var mailDetails = {
                contactEmailId: null,
                personId:null,
                personName:null,
                cc:ccList.length>0?ccList:null
            }

            mailDetails.contactEmailId = selectedUser.emailId;
            subject = "Transfer of Account(s) on Relatas - ";
            body = "Hi "+selectedUser.fullName+",\n\n";
            body = body+"The following accounts have been transferred to you on Relatas. Please login at www.relatas.com to view details\n\n"+account +".\n\n";

            body = body+'\n'+getSignature($rootScope.currentUser.firstName+' '+$rootScope.currentUser.lastName,$rootScope.currentUser.designation,$rootScope.currentUser.companyName,$rootScope.currentUser.publicProfileUrl)

            notifyRelevantPeople($http,subject,body,mailDetails)
        }
    } else {

        var receiverGrp = _.groupBy(accounts,"emailId")

        for( var key in receiverGrp){
            var ownerEmailId = ""
            var account = "";
            _.each(receiverGrp[key],function (ac,index) {
                ownerEmailId = ac.ownerEmailId;
                if(receiverGrp[key].length == 0){
                    account = ac.name
                } else {

                    if(index == 0){
                        account = account+ac.name
                    } else {
                        account = account+"\n"+ac.name
                    }
                }
            });

            var contactDetails = {
                contactEmailId: key,
                personId:null,
                personName:null,
                cc:corporateAdmin && ownerEmailId != corporateAdmin.emailId?[corporateAdmin.emailId]:null
            }

            var subject = "Required: Account access for - "+account;
            var body = "Hi "+share.teamDictionary[ownerEmailId].fullName+",\n\n";

            body = body+"Please grant me access to accounts "+account +".\n\n";

            if(mailType == "grantAccess" && key){

                contactDetails.contactEmailId = key;
                subject = "Access to Account(s) on Relatas";
                body = "Hi "+share.teamDictionary[key].fullName+",\n\n";
                body = body+"You have been given access to following accounts on Relatas. Please login at www.relatas.com to view details\n\n "+account +"\n";
            }

            body = body+'\n'+getSignature($rootScope.currentUser.firstName+' '+$rootScope.currentUser.lastName,$rootScope.currentUser.designation,$rootScope.currentUser.companyName,$rootScope.currentUser.publicProfileUrl)

            notifyRelevantPeople($http,subject,body,contactDetails)
        }
    }

}

function getAccounts($scope,$http,share,url,callback){

    var list = [];
    $http.get(url)
        .success(function(response){

            if(response.SuccessCode){

                $scope.grandTotal = response.Data.length;

                if(response.Data.length > 0){

                    for(var i=0; i<response.Data.length; i++){

                        var name = response.Data[i].name;
                        var ownerEmailId = response.Data[i].ownerEmailId;
                        var ownerId = response.Data[i].ownerId;
                        var accountAccessRequested = [];

                        _.each(response.Data[i].accountAccessRequested,function (el) {
                            accountAccessRequested.push({
                                date:moment(el.date).format("DD MMM YYYY"),
                                sortdate:el.date,
                                profile:share.teamDictionary[el.emailId]
                            })
                        });

                        var obj;
                        obj = {
                            userId:name,
                            fullName:name,
                            designation:"",
                            fullDesignation:"",
                            name:(getTextLength(name,25)).capitalizeFirstLetter(),
                            emailId:name,
                            noPicFlag:true,
                            image:"https://logo.clearbit.com/"+ getTextLength(name,25) +".com",
                            noPPic:(name.substr(0,2)).capitalizeFirstLetter(),
                            usersWithAccess:response.Data[i].access,
                            hierarchyAccess:response.Data[i].heiarchyWithAccess,
                            reportingHierarchyAccess:response.Data[i].reportingHierarchyAccess,
                            ownerId:ownerId,
                            ownerEmailId:ownerEmailId,
                            accountAccessRequested:accountAccessRequested,
                            contacts:response.Data[i].contacts,
                            lastInteractedDate:response.Data[i].lastInteractedDate
                        };

                        list.push(obj);
                        $http.get("/companylogos/" + getTextLength(name,25) +".com")
                    }
                }
            }

            callback(list)
        })
};