/**
 * Created by naveen on 11/6/16.
 */

relatasApp.controller("left_menu_bar", function($scope){

    if((/action/.test(window.location.pathname))){
        $scope.sideNavLanding = 'sidebar-highlight';
        $scope.sideNavInsights = ''
    } else if((/today/.test(window.location.pathname))){
        $scope.sideNavLanding = 'sidebar-highlight';
    } else if((/insights/.test(window.location.pathname))){
        $scope.sideNavInsights = 'sidebar-highlight';
    }else if((/insights/.test(window.location.pathname))){
        $scope.sideNavAccounts = 'sidebar-highlight';
    }else if((/help/.test(window.location.pathname))){
        $scope.sideNavHelp = 'sidebar-highlight';
    }else if((/contacts/.test(window.location.pathname))){
        $scope.sideNavContacts = 'sidebar-highlight';
    }else if((/opportunities/.test(window.location.pathname))){
        $scope.sideNavOpp = 'sidebar-highlight';
    }else if((/recommendations/.test(window.location.pathname))) {
        $scope.sideNavRecommendation = 'sidebar-highlight';
    }else if((/accounts/.test(window.location.pathname))){
        $scope.sideNavAccounts = 'sidebar-highlight';
    }else if((/document/.test(window.location.pathname))){
        $scope.sideNavDocuments = 'sidebar-highlight';
    } else if((/commit/.test(window.location.pathname))){
        $scope.sideNavCommits = 'sidebar-highlight';
    }else if((/admin/.test(window.location.pathname))){
        $scope.sideNavAdmin = 'sidebar-highlight';
    }else if((/pulse/.test(window.location.pathname))){
        $scope.sideNavReports = 'sidebar-highlight';
    }
})

relatasApp.controller("refresh_data", function($scope,share,$http){

    $scope.refreshData = function () {

        mixpanelTracker("Sync data");
        $scope.dataNoSync = true
        $scope.message = "Syncing data... this may take few minutes..."

        setTimeOutCallback(40000,function () {
            $scope.message = "Data sync successful. Please refresh the page to see latest data."
            $scope.dataSynced = true;

            var data = {
                dataUpdatedAt: new Date()
            }

            window.localStorage['refreshData'] = JSON.stringify(data)
        });

        $http.get('/user/refresh/data')
            .success(function (response) {
            });
    }

    $scope.reloadPage = function () {
        $( ".refresh-data-widget").fadeOut( 1000, function() {
            $scope.dataRefreshed = true;
            window.location = "/contacts/all"
        });
    }

    dataNeedsToBeRefreshed($scope)
    clearLocalStorage("relatasLocalDb")

    $scope.message = ""

    function setMessage(){
        if(share.l_user){

            if(share.l_user.lastDataSyncDate){

                var lastSyncDt = new Date(share.l_user.lastDataSyncDate)

                if(share.l_user.lastMobileSyncDate && new Date(share.l_user.lastMobileSyncDate)>lastSyncDt){
                    lastSyncDt = new Date(share.l_user.lastMobileSyncDate)
                }

                $scope.message = "Data last synced on "+moment(lastSyncDt).format("dddd, MMMM Do YYYY, h:mm:ss A")
            } else {
                $scope.message = "Welcome aboard. Click here to update your interactions"
            }

        } else {
            setTimeOutCallback(100,function () {
                setMessage();
            })
        }
    }

    setMessage()

})

function dataNeedsToBeRefreshed($scope) {

    // clearLocalStorage("refreshData")
    try {
        var cachedData = JSON.parse(window.localStorage['refreshData']);
        if(cachedData && cachedData.dataUpdatedAt){
            var diff = moment().diff(moment(cachedData.dataUpdatedAt),"minutes")
            $scope.dataRefreshed = true;
            // if(diff>=0){
            // if(diff>180){
            if(diff>60){
                $scope.dataRefreshed = false;
                clearLocalStorage("refreshData")
            }
        }
    } catch (err){
        $scope.dataRefreshed = false;
    }
}