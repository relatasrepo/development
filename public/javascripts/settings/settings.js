var reletasApp = angular.module('reletasApp', ['ngRoute','angular-loading-bar','ngCookies']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

reletasApp.service('share', function() {
    return {
        setTargetForFy:function(value){
            this.targetForFy = value;
        }
    }
});

reletasApp.service('profileService', function () {
    return {

    };
});

var isUpdated = false;
function triggerChange(){
    isUpdated = true;
    window.onbeforeunload = function(event) {
        isUpdated = false;
        window.onbeforeunload = null;
        return "Please save your changes";
    };
}

reletasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/todaySeries/leftMeetingBar"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        //var str = searchContent.replace(/[^\w\s]/gi, '');
        //var str = searchContent;
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

var timezone,l_userId;
reletasApp.controller("logedinUser", function ($scope, $http,$rootScope,profileService) {

    profileService.getProfile = function(second){
        $http.get('/profile/get/edit')
            .success(function (response) {

                if(response.SuccessCode){
                    $scope.l_usr = response.Data;
                    
                    $scope.firstName = response.Data.firstName
                    $scope.profilePicUrl = response.Data.profilePicUrl;

                    profileService.l_user = response.Data;
                    l_userId = $scope.l_usr._id;
                    $rootScope.isCorporateAdmin = response.Data.corporateAdmin;

                    identifyMixPanelUser(response.Data);
                    if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                        timezone = response.Data.timezone.name;
                    }

                    var account = response.Data.google;
                    var googleOrOutlook = 'google';
                    if(response.Data.outlook && response.Data.outlook.length>0) {
                        account = response.Data.outlook;
                        googleOrOutlook = 'outlook'
                    }

                    profileService.getUsersInCompany(response.Data.companyId,response.Data.hierarchyParent);
                    profileService.bindProfileInfo(response.Data,second);
                    // profileService.integrateGoogleOrOutlookAccounts(account,googleOrOutlook);
                    profileService.syncGoogleOrOutlookAccounts(account,googleOrOutlook);
                    profileService.integrateSocialAccounts(response.Data); // Removed until Social is fixed on 17 March 16, Naveen
                    profileService.integrateContactInfo(response.Data.contacts);
                    profileService.integrateCalendarInfo(response.Data);
                    profileService.integrateNotificationsOn(response.Data.notification);
                    profileService.setProgressBarStatus(response.Data.prStatus);
                    profileService.getLastLoginDate(response.Data.penultimateLoginDate,response.Data.timezone.name?response.Data.timezone.name:"UTC");
                    // profileService.integrateActionableMailSetting(response.Data.actionableMailSetting);
                    // profileService.integrateDashboardSetting(response.Data.dashboardSetting)
                    profileService.salesforceAccount(response.Data.salesforce,response.Data.companyId);
                }
                else{

                }
            }).error(function (data) {

        })
    };
    profileService.getProfile();
});

reletasApp.controller("submit_data_controller",function($scope, $http, profileService, share){
    var profile = {
        notification:{
            notificationOn:'never',
            companyId:$("#companyId").val()
        }
    };

    $scope.saveProfileInfo = function(){
        var calendarInfo = profileService.getCalendarInfo();
        var profileInfo = profileService.getProfileInfo();
        if(profileService.accountsUpdated){
            profileService.saveAccountSelection();
        }
        // profile.actionableMailSetting = profileService.getActionableMailSetting();
        // profile.dashboardSetting = profileService.getDashboardSetting();

        if(calendarInfo != null && profileInfo != null){
            for(var key in calendarInfo){
                profile[key] = calendarInfo[key]
            }
            for(var keyP in profileInfo){
                profile[keyP] = profileInfo[keyP]
            }

            if(!checkRequired(profile.profileDescription)){
                profile.profileDescription = null;
            }

            if(!checkRequired(profile.firstName)){
                toastr.error("First name is missing. Please complete your profile.")
            }
            else if(!checkRequired(profile.lastName)){
                toastr.error("Last name is missing. Please complete your profile.")
            }
            else if(!checkRequired(profile.companyName)){
                toastr.error("Company name is missing. Please complete your profile.")
            }
            else if(!checkRequired(profile.designation)){
                toastr.error("Designation is missing. Please complete your profile.")
            }
            else if(!checkRequired(profile.location)){
                toastr.error("Location is missing. Please complete your profile.")
            }
            else if(!checkRequired(profile.timezone)){
                toastr.error("Time zone is missing. Please complete your profile.")
            }
            else if(!checkRequired(profile.mobileNumberWithoutCC)){
                toastr.error("Mobile number is missing. Please complete your profile.")
            }
            else{
                $http.post('/profile/update/settings',profile)
                    .success(function(response){
                        if(response.SuccessCode){
                            isUpdated = false;
                            window.onbeforeunload = null;
                            toastr.success("Profile changes saved successfully");
                            profileService.getProfile();
                        }
                        else toastr.success(response.Message);
                    })
            }
        }
    }
});

reletasApp.controller("progress_controller",function($scope, $http, $rootScope, profileService, share){

    profileService.setProgressBarStatus = function(profile){
        $scope.totalWidth = 'width: '+profile.totalProgress+'%';
        $scope.totalProgress = Math.round(profile.totalProgress);
    }
    
    profileService.getLastLoginDate = function (date,timezone) {
        $scope.lastLoginDate = "Last login: "+moment(date).tz(timezone).format("dddd, MMMM Do YYYY, h:mm:ss a");
    }

    $scope.openProfile = function () {
        $scope.editProfile = true;
        $scope.selections1 = 'active';
        $scope.selections2 = '';
        $scope.selections3 = '';
        $scope.selections4 = '';
        $scope.selections5 = '';
        $scope.selections6 = '';
        $scope.selections7 = '';
        $scope.selections8 = '';

        $scope.editAddOns = false;
        $scope.editAccManagement = false;
        $scope.editManagement = false;
        $scope.editNotifications = false;
        $scope.notificationSettingsView = false;
        $scope.editCalendarSettings = false;
        $scope.weeklyAbstractReportView = false;
        $scope.editContactSettings = false;

        $("#save-button").show();
    }
    
    // $scope.openAddOns = function () {
    $scope.openPortfolioView = function () {
        $scope.editAddOns = true;
        $scope.editProfile = false;
        $scope.editAccManagement = false;
        $scope.editManagement = false;
        $scope.editNotifications = false;
        $scope.notificationSettingsView = false;
        $scope.editCalendarSettings = false;
        $scope.weeklyAbstractReportView = false;
        $scope.editContactSettings = false;
        $scope.selections1 = '';
        $scope.selections2 = 'active';
        $scope.selections3 = '';
        $scope.selections4 = '';
        $scope.selections5 = '';
        $scope.selections6 = '';
        $scope.selections7 = '';
        $scope.selections8 = '';
        $("#save-button").hide();
    }

    $scope.openManagement = function () {
        $scope.editManagement = true;

        $scope.editAddOns = false;
        $scope.editAccManagement = false;
        $scope.editProfile = false;
        $scope.editNotifications = false;
        $scope.notificationSettingsView = false;
        $scope.editCalendarSettings = false;
        $scope.weeklyAbstractReportView = false;
        $scope.editContactSettings = false;
        $scope.selections1 = '';
        $scope.selections2 = '';
        $scope.selections3 = 'active';
        $scope.selections4 = '';
        $scope.selections5 = '';
        $scope.selections6 = '';
        $scope.selections7 = '';
        $scope.selections8 = '';
        $("#save-button").show();
    }

    $scope.openNotifications = function () {
        $scope.editNotifications = true;

        $scope.editManagement = false;
        $scope.editAccManagement = false;
        $scope.editAddOns = false;
        $scope.editProfile = false;
        $scope.notificationSettingsView = false;
        $scope.editCalendarSettings = false;
        $scope.weeklyAbstractReportView = false;
        $scope.editContactSettings = false;
        $scope.selections1 = '';
        $scope.selections2 = '';
        $scope.selections3 = '';
        $scope.selections5 = '';
        $scope.selections4 = 'active';
        $scope.selections6 = '';
        $scope.selections7 = '';
        $scope.selections8 = '';
        $("#save-button").show();
    }

    $scope.openNotificationSettings = function() {
        $scope.editManagement = false;
        $scope.editAccManagement = false;
        $scope.editAddOns = false;
        $scope.editProfile = false;
        $scope.editNotifications = false;
        $scope.notificationSettingsView = true;
        $scope.editCalendarSettings = false;
        $scope.weeklyAbstractReportView = false;
        $scope.editContactSettings = false;
        $scope.selections1 = '';
        $scope.selections2 = '';
        $scope.selections3 = 'active';
        $scope.selections5 = '';
        $scope.selections4 = '';
        $scope.selections6 = '';
        $scope.selections7 = '';
        $scope.selections8 = '';
        $("#save-button").hide();
    }

    $scope.openCalendarSettings = function() {
        $scope.editManagement = false;
        $scope.editAccManagement = false;
        $scope.editAddOns = false;
        $scope.editProfile = false;
        $scope.editNotifications = false;
        $scope.notificationSettingsView = false;
        $scope.editCalendarSettings = true;
        $scope.weeklyAbstractReportView = false;
        $scope.editContactSettings = false;
        $scope.selections1 = '';
        $scope.selections2 = '';
        $scope.selections3 = '';
        $scope.selections5 = '';
        $scope.selections4 = '';
        $scope.selections6 = 'active';
        $scope.selections7 = '';
        $scope.selections8 = '';
        $("#save-button").show();
    }

    $scope.openContactSettings = function() {
        $scope.editManagement = false;
        $scope.editAccManagement = false;
        $scope.editAddOns = false;
        $scope.editProfile = false;
        $scope.editNotifications = false;
        $scope.notificationSettingsView = false;
        $scope.editCalendarSettings = false;
        $scope.weeklyAbstractReportView = false;
        $scope.editContactSettings = true;
        $scope.selections1 = '';
        $scope.selections2 = '';
        $scope.selections3 = '';
        $scope.selections5 = '';
        $scope.selections4 = '';
        $scope.selections6 = '';
        $scope.selections7 = '';
        $scope.selections8 = 'active'
        $("#save-button").hide();
    }

    $scope.openWeeklyAbstractReport = function() {
        $scope.editManagement = false;
        $scope.editAccManagement = false;
        $scope.editAddOns = false;
        $scope.editProfile = false;
        $scope.editNotifications = false;
        $scope.notificationSettingsView = false;
        $scope.editCalendarSettings = false;
        $scope.weeklyAbstractReportView = true;
        $scope.editContactSettings = false;
        $scope.selections1 = '';
        $scope.selections2 = '';
        $scope.selections3 = '';
        $scope.selections5 = '';
        $scope.selections4 = '';
        $scope.selections6 = '';
        $scope.selections7 = 'active';
        $scope.selections8 = '';
        $("#save-button").hide();
    }

    $scope.openAccountManagement = function () {

        window.location = "/accounts/access/settings"

        // $scope.editAccManagement = true;

        $scope.editNotifications = false;
        $scope.editManagement = false;
        $scope.editAddOns = false;
        $scope.editProfile = false;
        $scope.notificationSettingsView = false;
        $scope.editContactSettings = false;
        $scope.selections1 = '';
        $scope.selections2 = '';
        $scope.selections3 = '';
        $scope.selections4 = '';
        $scope.selections5 = 'active';
        $scope.selections6 = '';
        $scope.selections7 = '';
        $scope.selections8 = '';
    }

    profileService.getUsersInCompany = function(companyId,currentReportingManager){

        $scope.disableRMselection = false
        if(companyId){
            $http.get('/settings/get/company/users?companyId='+companyId)
                .success(function(response){
                    if(response.SuccessCode){
                        $scope.companyMembers = response.Data.sort(function(a, b) {
                            if(a.firstName && b.firstName){
                                var textA = a.firstName.toUpperCase();
                                var textB = b.firstName.toUpperCase();
                                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                            }
                        });
                    }
                });

            $scope.noReportingManager = "Please contact corporate admin to update your reporting manager."
            if(currentReportingManager){
                $http.get('/current/reporting/manager?id='+currentReportingManager)
                    .success(function(response){
                        $scope.noReportingManager = null;
                        if(response){
                            $scope.profileImg = '/getImage/'+response._id;
                            $scope.fullName = response.firstName+" "+response.lastName;
                            $scope.designation = ",  "+response.designation;
                        }
                    });
            }
        } else {
            $scope.disableRMselection = true
        }
    };
    
    $scope.updateReportingManager = function () {
        
        var reportingManager = JSON.parse($scope.reportingManager)

        var pathUpdate = {
            companyId:reportingManager.companyId,
            reportingManager:reportingManager._id
        };

        $http.post('/settings/update/reporting/manager',pathUpdate)
            .success(function (response) {
                if(!response){
                    toastr.error("Reporting manager update failed. The selected user is not part of the org. hierarchy")
                }
                profileService.getProfile();
            });
    }

    $scope.productTour = function() {
        var corporateUserLink = "https://s3.amazonaws.com/relatas-live-storage/OnboardRelatasForBusiness.pdf";
        var retailUserLink = "https://s3.amazonaws.com/relatas-live-storage/OnboardRetail_Relatas.pdf";

        if(profileService.l_user.corporateUser) 
            window.open(corporateUserLink, '_blank');
        else 
            window.open(retailUserLink, '_blank');
    };

    $scope.privacyPolicy = function() {
        window.open("https://relatas.com/privacy", "_blank");
    }

    $scope.faq = function() {
        window.open("https://relatas.com/faq", "_blank");        
    }

    $scope.termsOfService = function() {
        window.open("https://relatas.com/terms", "_blank");
    },

    $scope.support = function() {
        var fullName = profileService.l_user.firstName + (checkRequired(profileService.l_user.lastName) ? profileService.l_user.lastName : "");
        var liu = profileService.l_user;
        $scope.supportEmail = {};
        $scope.supportEmail.to = "hello@relatas.com";
        $scope.supportEmail.subject = "Relatas support | " + fullName +" |" ;
        $scope.supportEmail.body = '\n\n\n'+getSignature(liu.firstName+' '+liu.lastName,liu.designation,liu.companyName,liu.publicProfileUrl)

        $scope.supportEmail.openEmailModel = true;
    }

    $scope.sendEmail = function(subject, body) {
        if(!checkRequired(subject)){
            toastr.error("Please enter the subject.")
        }
        else if(!checkRequired(body)){
            toastr.error("Please enter the message.");
            
        } else {
            var emailObj = {
                email_cc: null,
                receiverEmailId: $scope.supportEmail.to,
                receiverName: "Relatas",
                message: body,
                subject: subject,
                docTrack: true,
                trackViewed: true,
                remind: null,
                respSentTime: moment().format(),
                isLeadTrack: false,
                newMessage: true,
                remind: false
            }
        
            $http.post("/messages/send/email/single/web",emailObj)
                .success(function(response){
                    if(response.SuccessCode){
                        toastr.success("Email sent successfully");
                        setTimeout($scope.closeModal(),function () {
                        },3000)
                    }
                    else{
                        toastr.error("Email not sent. Please try again later");
                    }
                })
        }
        
    }

    $scope.closeModal = function() {
        $scope.supportEmail.openEmailModel = false;
    }
});

reletasApp.controller("profile_data_controller",function($scope, $http,$timeout, profileService){

    var autocomplete;
    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            {types:['(cities)']});

        autocomplete.addListener('place_changed', function(){
            var place = autocomplete.getPlace();
            $scope.locationLatLang = null;
            if(place && place.geometry && place.geometry.location){
                var locationLatLang = place.geometry.location;
                if(checkRequired(locationLatLang)){
                    $scope.locationLatLang = {locationLatLang:{latitude:locationLatLang.lat(),longitude:locationLatLang.lng()}};
                }
            }
            $scope.isLocationChanged = false;

            $scope.location = $("#autocomplete").val();

            return false;
        });
    }
    window.initAutocomplete = initAutocomplete;
    $scope.changeProfilePic = function(){
        $("#selectPic").trigger("click");
    };

    $("#selectPic").on("change",function(){
        if($(this)[0] && $(this)[0].files && $(this)[0].files.length > 0){
            $scope.upload($(this)[0].files[0])
        }
        else toastr.error("Please select file to upload")
    });
    
    $scope.upload = function(file){
        var formData = new FormData();

        formData.append('profilePic', file, file.name);
        formData.append('kuch', "loler");
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/profile/update/profilepic/web', {water:'love'});
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            var response = JSON.parse(xhr.response)
            if(response.SuccessCode){
                toastr.success(response.Message)
                profileService.getProfile(true);
            }
            else{
                toastr.error(response.Message)
            }
        };
        // Send the Data.
        xhr.send(formData);
    };

    profileService.bindProfileInfo = function(profile,second){

        $scope.profilePic = profile.profilePicUrl
        $scope.profile = profile;
        $scope.firstName = profile.firstName;
        $scope.designation = profile.designation
        $scope.skypeId = profile.skypeId
        $scope.lastName = profile.lastName
        $scope.companyName = profile.companyName
        $scope.location = profile.location
        $scope.emailId = profile.emailId
        $scope.profileDescription = profile.profileDescription
        $scope.uniqueUserName = profile.publicProfileUrl
        $scope.profileImg = profile.profilePicUrl
        $scope.appendTimezones();

        if(checkRequired(profile.timezone) && checkRequired(profile.timezone.name)){
            $scope.timezone = profile.timezone.name
        }
        else  $scope.timezone = "";

        $scope.mobileNumber_country = "";
        if(checkRequired(profile.mobileNumberWithoutCC) && checkRequired(profile.countryCode)){
            $scope.mobileNumber = profile.mobileNumberWithoutCC
            $scope.mobileNumber_country = '+'+profile.countryCode
        }
        else if(checkRequired(profile.mobileNumber) && profile.mobileNumber.length > 10){
            var numberIndex = profile.mobileNumber.length - 10;
            $scope.mobileNumber = profile.mobileNumber.substring(numberIndex,profile.mobileNumber.length)
            $scope.mobileNumber_country = profile.mobileNumber.substring(0,numberIndex)
            if(checkRequired($scope.mobileNumber_country) && $scope.mobileNumber_country.length > 0){
                if($scope.mobileNumber_country.charAt(0) != '+'){
                    $scope.mobileNumber_country = '+'+$scope.mobileNumber_country;
                }
            }
        }
        else $scope.mobileNumber = profile.mobileNumber;

        if(profile.corporateUser){
            $scope.isCorporateUser = profile.corporateUser;
        }
    };

    $(".phone_validate").bind("keypress", function (event) {
        if (event.charCode != 0) {
            var regex = new RegExp("^[0-9+]*$");
            if($(this).attr("inputData") != "countryCode"){
                regex = new RegExp("^[0-9]*$");
            }
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }

            var numbers = $(this).val();
            if(checkRequired(numbers)){
                numbers = numbers.replace(/\D/g, '');
                if($(this).attr("inputData") == "countryCode"){
                    if(numbers.length >= 5){
                        event.preventDefault();
                        return false;
                    }
                }
                else if(numbers.length >= 10){
                    event.preventDefault();
                    return false;
                }
            }
        }
    });

    $scope.appendTimezones = function(){
        var timezonesList = [
            'Pacific/Samoa',
            'Pacific/Honolulu',
            'US/Alaska',
            'America/Los_Angeles',
            'US/Arizona',
            'America/Managua',
            'America/Bogota',
            'US/Eastern',
            'America/Lima',
            'Canada/Atlantic',
            'America/Caracas',
            'America/La_Paz',
            'America/Santiago',
            'Canada/Newfoundland',
            'America/Sao_Paulo',
            'America/Argentina/Buenos_Aires',
            'America/Godthab',
            'America/Noronha',
            'Atlantic/Azores',
            'Atlantic/Cape_Verde',
            'Africa/Casablanca',
            'Europe/London',
            'Africa/Monrovia',
            'UTC',
            'Europe/Amsterdam',
            'Africa/Lagos',
            'Europe/Athens',
            'Africa/Harare',
            'Asia/Jerusalem',
            'Africa/Johannesburg',
            'Asia/Baghdad',
            'Europe/Minsk',
            'Africa/Nairobi',
            'Asia/Riyadh',
            'Asia/Tehran',
            'Asia/Muscat',
            'Asia/Baku',
            'Asia/Tbilisi',
            'Asia/Yerevan',
            'Asia/Kabul',
            'Asia/Karachi',
            'Asia/Tashkent',
            'Asia/Kolkata',
            'Asia/Katmandu',
            'Asia/Almaty',
            'Asia/Dhaka',
            'Asia/Yekaterinburg',
            'Asia/Rangoon',
            'Asia/Bangkok',
            'Asia/Jakarta',
            'Asia/Novosibirsk',
            'Asia/Hong_Kong',
            'Asia/Chongqing',
            'Asia/Krasnoyarsk',
            'Asia/Kuala_Lumpur',
            'Australia/Perth',
            'Asia/Singapore',
            'Asia/Ulan_Bator',
            'Asia/Urumqi',
            'Asia/Irkutsk',
            'Asia/Tokyo',
            'Asia/Seoul',
            'Australia/Adelaide',
            'Australia/Darwin',
            'Australia/Brisbane',
            'Australia/Canberra',
            'Pacific/Guam',
            'Australia/Hobart',
            'Australia/Melbourne',
            'Pacific/Port_Moresby',
            'Australia/Sydney',
            'Asia/Yakutsk',
            'Asia/Vladivostok',
            'Pacific/Auckland',
            'Pacific/Kwajalein',
            'Asia/Kamchatka',
            'Asia/Magadan',
            'Pacific/Fiji',
            'Asia/Magadan',
            'Pacific/Auckland',
            'Pacific/Tongatapu'
        ];

        var timezones = [];
        for(var i=0; i<timezonesList.length; i++){
            var date = moment().tz(timezonesList[i]);
            var zoneName = '';
            if(timezonesList[i].indexOf('/') != -1){
                zoneName = timezonesList[i].split('/')[1];
            }else zoneName = timezonesList[i];

            timezones.push({
                //date:date.format("z")+" time (GMT "+date.format("Z")+")",
                date:zoneName+" time (GMT "+date.format("Z")+")",
                zone:timezonesList[i]
            })
        }
        $scope.listItems = timezones;
    };

    $scope.autoCompleteCompanies = function(text){
        $scope.cList = [];
        if(checkRequired(text) && text.length > 2){
            var req = {
                method: 'GET',
                url: 'https://autocomplete.clearbit.com/v1/companies/suggest?query='+text,
                headers: {
                    'If-Modified-Since': undefined
                }
            };
            $http(req).then(function(response){
                    if(response && response.statusText == 'OK'){
                        var position = $("#exampleInput-c").position();
                        position.top = position.top+40
                        $(".search-results").show()
                        $(".search-results").css(position)
                        $scope.cList = response.data;
                    }
                },
                function(){

                });
        }
        else $(".search-results").hide()
    };

    $scope.selectCompany = function(company){
        $(".search-results").hide()
        if(checkRequired(company) && checkRequired(company.name)){
            $scope.companyName = company.name;
        }
    };

    $scope.updatedText = function(location){
        $scope.isLocationChanged = true;
    };

    profileService.getProfileInfo = function(){
        if($scope.isLocationChanged){
            toastr.error("Please select valid location");
            return null;
        }
        if(checkRequired($scope.mobileNumber) && !checkRequired($scope.mobileNumber_country)){
            toastr.error("Please enter country code");
            return null;
        }
        else{
            if($scope.mobileNumber){
                $scope.mobileNumber = $scope.mobileNumber;
                $scope.countryCode = $scope.mobileNumber_country
                $scope.mobileNumber = $scope.mobileNumber.replace(/\s/g, '');
                if($scope.countryCode.charAt(0) == '+'){
                    $scope.countryCode = $scope.countryCode.substring(1,$scope.countryCode.length)
                }
            }
            var pr = {
                firstName:$scope.firstName,
                lastName:$scope.lastName,
                companyName: $scope.companyName,
                designation:$scope.designation,
                location:$scope.location,
                timezone:$scope.timezone,
                mobileNumberWithoutCC:$scope.mobileNumber,
                countryCode:$scope.countryCode,
                mobileNumber:$scope.countryCode+$scope.mobileNumber,
                skypeId:$scope.skypeId,
                emailId:$scope.emailId,
                profileDescription:$scope.profileDescription
            }
            if(checkRequired($scope.locationLatLang)){
                pr.locationLatLang = $scope.locationLatLang;
            }
            return pr;
        }
    }

    $scope.watchProfileChanges = function() {
        $scope.profile.dataChanged = true;
    }

});

reletasApp.controller("google_accounts_controller",function($scope, $http, profileService,$cookies){
    profileService.integrateGoogleOrOutlookAccounts = function(accounts,googleOrOutlook){

        $scope.otherGoogleAccounts = [];
        $scope.gAccountlength = 0;
        if(accounts && accounts.length > 0){
            $scope.gAccountlength = accounts.length;
            $scope.primaryAccount = accounts[0].emailId;
            for(var i=1; i<accounts.length; i++){
                var obj = {
                    acc:accounts[i],
                    type:googleOrOutlook
                }
                $scope.otherGoogleAccounts.push(obj);
            }
        }
    };

    $scope.updateOrDeleteGoogleAccounts = function(url){
        $http.get(url)
            .success(function(response){
                if(response.SuccessCode){
                    profileService.getProfile();
                }
                else toastr.error(response.Message);
            })
    };

    $scope.makePrimaryAccount = function(id,type){

        var url = '/profile/update/primary/google?googleId='+id;
        if(type == 'outlook'){
            url = '/profile/update/primary/outlook?outlookId='+id
        }

        $scope.updateOrDeleteGoogleAccounts(url)
    };

    $scope.deleteGoogleAccount = function(id,type){

        var url = '/profile/update/remove/google?googleId='+id;
        if(type == 'outlook'){
            url = '/profile/update/remove/outlook?outlookId='+id
        }

        $scope.updateOrDeleteGoogleAccounts(url);
        // $scope.id.splice(index, 1);
    };

    $scope.addAccounts = function(accountType,count){
        if(accountType == 'google' && count <= 4){
            window.location = '/addAnotherGoogleAccount'+count+'Edit?finalPage=settings';
        } else if(accountType == 'outlook' && count <= 4){
            window.location = '/authenticate/more/outlook/accounts?finalPage=settings';
        }
    }

});

$(document).ready(function() {
    $('.left-menu>ul>li').click(function(e){
        e.stopPropagation();
        mixpanelTracker("Settings>"+$(this).text());
    });
});

reletasApp.controller("crm_accounts_controller",function($scope, $http, profileService,$cookies){

    var companyIdGlobal = null;
    profileService.salesforceAccount = function(salesforce,companyId){
        $scope.salesforce = {
            showAdd:salesforce && salesforce.emailId ? false : true,
            id:salesforce && salesforce.emailId ? salesforce.emailId : 'Salesforce',
            showRemove:salesforce && salesforce.emailId ? true : false,
            imgUrl:'../img/salesforce.png'
        };
        
        if(companyId){
            companyIdGlobal = companyId
        }
    }

    $scope.seeAllAccess = function(user){

        var accessLevel = {}
        _.each(user.accessLevel,function(ac){
            accessLevel[ac.type] = ac.values;
        })

        _.each($scope.allPortfolios,function(al){
            var allSelected = true;
            _.each(al.list,function(li){
                li.selected = accessLevel[al.name]?accessLevel[al.name].indexOf(li.name)>-1:false;
                if(!li.selected){
                    allSelected = false;
                }
            });

            al.allSelected = allSelected;
        });

        user.showAccess = true;
    }

    $http.get('/profile/get/current/web')
        .success(function (response) {

            $scope.companyResponse = response.companyDetails;
            $scope.getAllPortfolios();

        });

    $scope.getAllPortfolios = function(){
        $http.get("/user/get/portfolios?getHeads=true")
            .success(function (response) {

                $scope.groupedPortfolios = [];
                if(!response){

                } else {


                    response.owner.forEach(function(ow){
                        var user = null;
                        var headName = ""
                        var headEmailId = ""
                        ow.users.forEach(function(us){
                            // user = us;
                            if(us.ownerEmailId == profileService.l_user.emailId){
                                user = us;
                            } else if(us.isHead){
                                user = us;
                            }
                        });

                        if(!user){
                            user = ow.users[0]
                        };

                        headEmailId = user.ownerEmailId;
                        headName = user.ownerEmailId

                        if(response && response.fullNameDetails && response.fullNameDetails[user.ownerEmailId]){
                            headName = response.fullNameDetails[user.ownerEmailId]
                        }

                        if(response.portfoliosWithHeadObj){
                            if(response.portfoliosWithHeadObj && response.portfoliosWithHeadObj[user.type+user.name]){
                                headName = response.portfoliosWithHeadObj[user.type+user.name].name
                                headEmailId = response.portfoliosWithHeadObj[user.type+user.name].emailId
                            }
                        }

                        user.headName = headName;
                        user.headEmailId = headEmailId;
                        $scope.groupedPortfolios.push(user)
                    });

                    $scope.groupedPortfolios.sort(function (o1,o2) {
                        return o1.name > o2.name ? 1 : o1.name < o2.name ? -1 : 0;
                    });

                    $scope.allPortfolios = [
                        {
                            name:"Business Units",
                            list:$scope.companyResponse.businessUnits
                        },
                        {
                            name:"Regions",
                            list:$scope.companyResponse.geoLocations
                        },
                        {
                            name:"Products",
                            list:$scope.companyResponse.productList
                        },
                        {
                            name:"Verticals",
                            list:$scope.companyResponse.verticalList
                        }
                    ];

                    $scope.groupedPortfolios.sort(function (a,b) {
                        if(a.type.toLowerCase() < b.type.toLowerCase()) return -1;
                        if(a.type.toLowerCase() > b.type.toLowerCase()) return 1;
                        return 0;
                    })

                    $scope.allPortfolios.forEach(function (al) {
                        if(al.name == "Regions"){
                            al.list.forEach(function(re){
                                if(!re.name){
                                    re.name = re.region
                                }
                            });
                        }
                        al.list.sort(function (a,b) {
                            if(a.name.toLowerCase() < b.name.toLowerCase()) return -1;
                            if(a.name.toLowerCase() > b.name.toLowerCase()) return 1;
                            return 0;
                        })
                    });

                }

            });
    }

    $scope.addSalesforceAccounts = function(){
        if(companyIdGlobal){
            window.location = '/salesforce/oauth2/auth?finalPage=settings&companyId='+companyIdGlobal;
        } else {
            toastr.error('You are not a corporate user. Please contact your corporate admin to make you a corporate user.');
        }
    }

    $scope.removeSalesforceAccounts = function(){
        var url = '/salesforce/remove/account';
        $http.get(url)
            .success(function(response){
                if(response.SuccessCode){
                    profileService.getProfile();
                    toastr.success("Your salesforce account has been removed successfully.");
                }
                else {
                    toastr.error('Failed to remove account. Please try again later');
                }
            })
    }

    $http.get('/salesforce/accountAdded')
        .success(function(response){
            var msgShownForSalesforce= $cookies.get('msgShownForSalesforce');
            var msgShownExpire = moment().add(2, "hours").toDate();
            if(response.data.show && !msgShownForSalesforce){
                if(response.data.value) {
                    $cookies.put('msgShownForSalesforce', true,{expires:msgShownExpire});
                    toastr.success("Your salesforce account has been added successfully.");
                }
                else {
                    toastr.error("Failed to add account. Please try again later");
                }
            }
        })
});

reletasApp.controller("social_data_controller",function($scope, $http, profileService){
    profileService.integrateSocialAccounts = function(profile){
        $scope.twitter = {};
        $scope.linkedin = {};
        $scope.facebook = {};
        if(checkRequired(profile.twitter) && checkRequired(profile.twitter.id)){
            $scope.twitter = {
                name:profile.twitter.userName,
                butAction:'remove',
                butText:'Remove',
                butShow:true
            }
        }
        else{
            $scope.twitter = {
                name:'Twitter',
                butAction:'add',
                butText:'Add',
                butShow:true
            };

            if(checkRequired(profile.twitter) && checkRequired(profile.twitter.id)){
                mixpanel.track('Settings Twitter Add', {
                    'Name' : profile.twitter.userName
                });
            }
        }
        if(checkRequired(profile.linkedin) && checkRequired(profile.linkedin.id)){
            $scope.linkedin = {
                name:profile.linkedin.name,
                butAction:'remove',
                butText:'Remove',
                butShow:true
            }
        }
        else{
            $scope.linkedin = {
                name:'LinkedIn',
                butAction:'add',
                butText:'Add',
                butShow:true
            };

            if(checkRequired(profile.linkedin) && checkRequired(profile.linkedin.id)) {
                mixpanel.track('Settings Linkedin Add', {
                    'Name' : profile.linkedin.userName
                });
            }
        }
        if(checkRequired(profile.facebook) && checkRequired(profile.facebook.id)){
            $scope.facebook = {
                name:profile.facebook.name,
                butAction:'remove',
                butText:'Remove',
                butShow:true
            }
        }
        else{
            $scope.facebook = {
                name:'Facebook',
                butAction:'add',
                butText:'Add',
                butShow:true
            };

            if(checkRequired(profile.facebook) && checkRequired(profile.facebook.id)) {
                mixpanel.track('Settings Facebook Add', {
                    'Name' : profile.facebook.userName
                });
            }
        }
    };

    $scope.socialAction = function(accountType,action){
        
        if(action == 'remove'){
            $http.delete('/profile/update/remove/'+accountType+'/web')
                .success(function(response){
                    if(response.SuccessCode){
                        profileService.getProfile();
                    }
                    else toastr.error(response.Message);
                })
        }
        else if(action == 'add'){
            window.location = '/profile/update/add/'+accountType+'/web';
        }
    }
});

reletasApp.controller("notifications_data_controller",function($scope, $http, profileService){
    profileService.integrateNotificationsOn = function(notifications){
        if(checkRequired(notifications) && checkRequired(notifications.notificationOn)){
            $scope.warSetting = notifications.notificationOn;
        }
        else{
            $scope.warSetting = 'never';
        }
    };

    $scope.updateWarSettings = function() {
        var url = '/update/war/email/setting/' + $scope.warSetting;

        $http.put(url)
            .success(function(response){
            });
    }
});

reletasApp.controller("contacts_data_controller",function($scope, $http, profileService){
    profileService.integrateContactInfo = function(contactsCount){
        $scope.totalContacts = contactsCount || 0;
    };
    $scope.selectFile = function(){
        $("#selectedFile").trigger("click");
    };
    $scope.upload = function(file){
        var formData = new FormData();
        formData.append('contacts', file, file.name);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/contacts/upload/file', true);
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            var response = JSON.parse(xhr.response)
            if(response.SuccessCode){
                toastr.success(response.Message)
                setTimeout(function(){
                    profileService.getProfile();
                },1000)
            }
            else{
                toastr.error(response.Message)
            }
        };
        // Send the Data.
        xhr.send(formData);
    };
    $("#selectedFile").on("change",function(){
        if($(this)[0] && $(this)[0].files && $(this)[0].files.length > 0){
            $scope.upload($(this)[0].files[0])
        }
        else toastr.error("Please select file to upload")
    });

    profileService.syncGoogleOrOutlookAccounts = function (accounts,googleOrOutlook) {
        $scope.accType = googleOrOutlook
    }

    //Sync Google/Outlook Contacts
    $scope.syncContacts = function (accType) {

        var url = '/refresh/google/contacts';
        if(accType == 'outlook'){
            url = '/refresh/outlook/contacts';
        }

        $http.get(url)
            .success(function(response){
                if(response.SuccessCode){
                    toastr.success("Your contacts sync has been successful. Contacts added - "+response.Data.contactsAdded+ ".")
                } else {
                    toastr.error("Contacts sync failed. Please try again later")
                }
            })
    };
});

reletasApp.controller("account_email_domain",function($scope, $http, profileService){

    $scope.accountsUpdated = function(){
        profileService.accountsUpdated = true;
        triggerChange();
    };

    $scope.showToReportingManager = function(isSelected){
        return isSelected ? 'rm-visibility-selected' : 'rm-visibility-unselected'
    };

    $scope.changeShowReportingManager = function(account,selected){
        for(var i=0; i<$scope.contactsByAccout.length; i++){
            if($scope.contactsByAccout[i].account == account){
                $scope.accountsUpdated();
                $scope.contactsByAccout[i].showToReportingManager = !selected;
            }
        }
    };

    $scope.addTo = function(account, parent, event){

        $scope.accountsUpdated();
        var list = [];
        var others = [];
        for(var i=0; i<$scope.contactsByAccout.length; i++){
            if($scope.contactsByAccout[i].account == parent){
                for(var l=0; l<$scope.contactsByAccout[i].contacts.length; l++){

                    if($scope.contactsByAccout[i].contacts[l].selected == true){
                        list.push($scope.contactsByAccout[i].contacts[l])
                    }
                    else others.push($scope.contactsByAccout[i].contacts[l]);
                    //else others.push($scope.contactsByAccout[i].contacts[l])
                }
                $scope.contactsByAccout[i].contacts = others;
                $scope.contactsByAccout[i].total = $scope.contactsByAccout[i].contacts.length;
                if(!$scope.contactsByAccout[i].contacts.length > 0){
                    $scope.deletedAcc = $scope.contactsByAccout.splice(i,1);
                }
            }
        }

        for(var j=0; j<$scope.contactsByAccout.length; j++){
            if($scope.contactsByAccout[j].account == account){
                for(var z=0; z<list.length; z++){
                    list[z].selected = true;
                }
                $scope.contactsByAccout[j].contacts = $scope.contactsByAccout[j].contacts.concat(list);
                $scope.contactsByAccout[j].total += list.length;
                //break;
            }
        }
        //event.stopPropagation();
    };

    $scope.contactUnSelected = function(selected,contact,account){

        if(!selected && account != 'other'){
            $scope.accountsUpdated()
            var acc = contact.personEmailId.split("@")[1].split(".")[0];
            if(acc != account){
                for(var i=0; i<$scope.contactsByAccout.length; i++){
                    if($scope.contactsByAccout[i].account == account){
                        for(var j=0; j<$scope.contactsByAccout[i].contacts.length; j++){
                            if($scope.contactsByAccout[i].contacts[j].personEmailId == contact.personEmailId){
                                $scope.contactsByAccout[i].total -= 1;
                                $scope.contactsByAccout[i].contacts.splice(j,1);
                            }
                        }
                    }
                }
                var isOk = false;
                for(var k=0; k<$scope.contactsByAccout.length; k++){
                    if($scope.contactsByAccout[k].account == 'other'){
                        isOk = true;
                        $scope.contactsByAccout[k].contacts.push(contact);
                        $scope.contactsByAccout[k].total += 1
                    }
                }
                if(!isOk){
                    if($scope.deletedAcc && $scope.deletedAcc.all){
                        $scope.contactsByAccout.push({
                            account:'other',
                            all:$scope.deletedAcc.all || true,
                            selectAll:$scope.deletedAcc.selectAll || 'otherSelectAll',
                            selected:$scope.deletedAcc.selected || true,
                            showToReportingManager:$scope.deletedAcc.showToReportingManager || true,
                            total:1,
                            contacts:[contact]
                        })
                    }
                    else{
                        $scope.contactsByAccout.push({
                            account:'other',
                            all:true,
                            selectAll:'otherSelectAll',
                            selected:true,
                            showToReportingManager: true,
                            total:1,
                            contacts:[contact]
                        })

                    }
                }
            }
        }
    };

    $scope.saveAccountSelection = function(){

        var updateList = [];
        for(var key in $scope.contactsByAccout){

            //updateList.push({
            //    contactId:$scope.contactsByAccout[key].contacts[i]._id,
            //    showToReportingManager: $scope.contactsByAccout[key].showToReportingManager
            //});


            //  if(key != 'other'){

            //Last modified 22 Feb 2016 by Naveen. The below code was used when user's could move email IDs are from one account to another.
            for(var i=0; i<$scope.contactsByAccout[key].contacts.length; i++){
                updateList.push({
                    contactId:$scope.contactsByAccout[key].contacts[i]._id,
                    selected:true,
                    showToReportingManager:$scope.contactsByAccout[key].showToReportingManager,
                    accountName:$scope.contactsByAccout[key].account == 'other' ? null : $scope.contactsByAccout[key].account
                })
            }
        }
        //}

        if(updateList.length > 0){
            $http.post('/update/account/visibility',updateList)
                .success(function(response){

                });
        }

    };

    profileService.saveAccountSelection = $scope.saveAccountSelection;

    $scope.filterBy = function (filter) {
        profileService.getEmailAccounts(filter);
    }
    profileService.getEmailAccounts = function(filter){
        $http.get('/onboarding/get/contacts/accounts')
            .success(function(response){

                $scope.contactsByAccout = response.Data.emailAccounts;
                $scope.allAccountNames = response.Data.allAccountNames;

                if(response && response.Data && response.Data.corporate){
                    $scope.ngIf_corporate = true;
                }
                else $scope.ngIf_corporate = false;

                if(response.SuccessCode){
                    $scope.show_no_accounts = false;

                    if(filter && filter === 'visible'){
                        $scope.contactsByAccout = [];
                        $scope.numberOfVisibleAccounts = 0;
                        $scope.numberOfNonVisibleAccounts = 0;
                        for(var i=0;i<response.Data.emailAccounts.length;i++){
                            if(response.Data.emailAccounts[i].showToReportingManager){
                                $scope.numberOfVisibleAccounts++;
                                $scope.contactsByAccout.push(response.Data.emailAccounts[i])
                            } else {
                                $scope.numberOfNonVisibleAccounts++
                            }
                        }
                    } else if(filter && filter === 'hidden'){
                        $scope.contactsByAccout = [];
                        $scope.numberOfVisibleAccounts = 0;
                        $scope.numberOfNonVisibleAccounts = 0;
                        for(var i=0;i<response.Data.emailAccounts.length;i++){
                            if(response.Data.emailAccounts[i].showToReportingManager){
                                $scope.numberOfVisibleAccounts++;
                            } else {
                                $scope.numberOfNonVisibleAccounts++;
                                $scope.contactsByAccout.push(response.Data.emailAccounts[i])
                            }
                        }

                    } else {

                        $scope.numberOfVisibleAccounts = 0;
                        $scope.numberOfNonVisibleAccounts = 0;

                        for(var i=0;i<$scope.contactsByAccout.length;i++){
                            if($scope.contactsByAccout[i].showToReportingManager){
                                $scope.numberOfVisibleAccounts++;
                            } else {
                                $scope.numberOfNonVisibleAccounts++
                            }
                        }
                    }
                }
                else{
                    $scope.no_accounts_message = response.Message;
                    $scope.show_no_accounts = true;
                }
            })
    };

    $scope.changeSelectAll = function(account,changed){

        for(var i=0; i<$scope.contactsByAccout.length; i++){
            if($scope.contactsByAccout[i].account == account){
                $scope.accountsUpdated()
                for(var j=0 ;j<$scope.contactsByAccout[i].contacts.length; j++){
                    $scope.contactsByAccout[i].contacts[j].selected = changed;
                }
            }
        }
    };
    profileService.getEmailAccounts();
});

reletasApp.controller("calendar_data_controller",function($scope, $http, profileService){

    profileService.integrateCalendarInfo = function(profile){
        $scope.uniqueName = profile.uniqueName;
        
        $scope.week = {
            sunday:'week-end-days',
            monday:'week-day-days',
            tuesday:'week-day-days',
            wednesday:'week-day-days',
            thursday:'week-day-days',
            friday:'week-day-days',
            saturday:'week-end-days'
        };

        $scope.getWeekDaysSelected = function(className){
            var days = [];
            for(var key in $scope.week){
                if($scope.week[key] == className){
                    switch (key){
                        case 'sunday' :days.push(0);
                            break;
                        case 'monday' :days.push(1);
                            break;
                        case 'tuesday' :days.push(2);
                            break;
                        case 'wednesday' :days.push(3);
                            break;
                        case 'thursday' :days.push(4);
                            break;
                        case 'friday' :days.push(5);
                            break;
                        case 'saturday' :days.push(6);
                            break;
                    }
                }
            }
            return days;
        };

        $scope.mapDataToWeek = function(days,className){
            for(var i=0; i<days.length; i++){
                switch (days[i]){
                    case 0: $scope.week['sunday'] = className;
                        break;
                    case 1: $scope.week['monday'] = className;
                        break;
                    case 2: $scope.week['tuesday'] = className;
                        break;
                    case 3: $scope.week['wednesday'] = className;
                        break;
                    case 4: $scope.week['thursday'] = className;
                        break;
                    case 5: $scope.week['friday'] = className;
                        break;
                    case 6: $scope.week['saturday'] = className;
                        break;
                }
            }
        };

        if(profile.workHoursWeek && profile.workHoursWeek.weekdayHours && profile.workHoursWeek.weekdayHours.start){
            if(checkRequired(profile.workHoursWeek.weekdayHours.days) && profile.workHoursWeek.weekdayHours.days.length > 0){
                $scope.mapDataToWeek(profile.workHoursWeek.weekdayHours.days,'week-day-days');
            }
            $("#weekdayHours").attr('value',profile.workHoursWeek.weekdayHours.start+','+profile.workHoursWeek.weekdayHours.end)
            
        }
        else {
            $("#weekdayHours").attr('value','8,19')
        }
        if(profile.workHoursWeek && profile.workHoursWeek.weekendHours && profile.workHoursWeek.weekendHours.start){
            if(checkRequired(profile.workHoursWeek.weekendHours.days) && profile.workHoursWeek.weekendHours.days.length > 0){
                $scope.mapDataToWeek(profile.workHoursWeek.weekendHours.days,'week-end-days');
            }
            $("#weekendHours").attr('value',profile.workHoursWeek.weekendHours.start+','+profile.workHoursWeek.weekendHours.end)
        }
        else{
            $("#weekendHours").attr('value','8,19');
        }
        if(checkRequired(profile.profilePrivatePassword)){
            $scope.profilePrivatePassword = 'encrypted'
        }
        $('.range-slider').jRange({
            from: 5,
            to: 24,
            step: 1,
            format: $scope.formatSlider,
            width: 300,
            showLabels: true,
            showScale: false,
            isRange : true,
            onstatechange:triggerChange
        });
    };

    $scope.formatSlider = function(value, pointer){
        var m = moment()
        if(value == 24){
            m.hours(23)
            m.minutes(59)
        }
        else{
            m.hours(value)
            m.minutes(0)
        }
        m.seconds(0)
        return m.format("h:mm a")
    };

    $scope.changeDays = function(event,className,weekDay){
        $scope.week[weekDay] = $scope.week[weekDay] == 'week-end-days' ? 'week-day-days' : 'week-end-days'
    };

    profileService.getCalendarInfo = function(){
        var weekdayHours = $("#weekdayHours").attr('value');
        var weekendHours = $("#weekendHours").attr('value');
        getWeekDaysSelected = function(className){
            var days = [];
            for(var key in $scope.week){
                if($scope.week[key] == className){
                    switch (key){
                        case 'sunday' :days.push(0);
                            break;
                        case 'monday' :days.push(1);
                            break;
                        case 'tuesday' :days.push(2);
                            break;
                        case 'wednesday' :days.push(3);
                            break;
                        case 'thursday' :days.push(4);
                            break;
                        case 'friday' :days.push(5);
                            break;
                        case 'saturday' :days.push(6);
                            break;
                    }
                }
            }
            return days;
        };
        return {
            workHoursWeek:{
                weekdayHours:{
                    //days:$scope.getWeekDaysSelected('week-day-days'),
                    days:getWeekDaysSelected('week-day-days'),
                    start: weekdayHours.split(',')[0],
                    end: weekdayHours.split(',')[1]
                },
                weekendHours:{
                    //days:$scope.getWeekDaysSelected('week-end-days'),
                    days:getWeekDaysSelected('week-end-days'),
                    start: weekendHours.split(',')[0],
                    end: weekendHours.split(',')[1]
                }
            },
            calPass:$scope.profilePrivatePassword
        
        }
    };
});

reletasApp.controller("actionableMail_data_controller",function($scope, $http, profileService){
    profileService.integrateActionableMailSetting = function(actionableMailSetting){
        $scope.important = actionableMailSetting == 'important';
        $scope.importantContact = actionableMailSetting == 'importantContact';
        $scope.sentToMe = actionableMailSetting == 'sentToMe';
        $scope.all = actionableMailSetting == 'all' || !actionableMailSetting
    }

    profileService.getActionableMailSetting = function(){
        if($("#mailSetting1").is(":checked")){
            return 'all';
        }
        else if($("#mailSetting2").is(":checked")){
            return 'important';
        }
        else if($("#mailSetting3").is(":checked")){
            return 'importantContact';
        }
        else if($("#mailSetting4").is(":checked")){
            return 'sentToMe';
        }
    }
});

reletasApp.controller("notificationSettings_controller",function($scope, $http, $rootScope, share){
    $scope.settings = [];

    getLiuProfile($scope, $http, share,$rootScope, function(response) {
        var notificationSettings = share.liuData ? share.liuData.notificationSettings : {};

        $scope.settings.push({
            name: "Show Notifications on Web Browser",
            description: "Enable notifications to get real time and weekly notifications from Relatas",
            status: share.liuData ? share.liuData.webFirebaseSettings.notificationEnabled : false,
            key: "notificationEnable"
        },{
            name: "Document Visibility",
            description: "Document view notification lets you know in real time the user has seen your shared document",
            status: notificationSettings.documentView.web,
            key: "documentView"
        },{
            name: "Commit Reminder",
            description: "Get a reminder for the due date for commit end date",
            status: notificationSettings.commitPending.web,
            key: "commitPending"
        },{
            name: "Tasks Due",
            description: "Get a reminder for tasks due date today",
            status: notificationSettings.tasksForToday.web,
            key: "tasksForToday"
        },{
            name: "Losing Touch Contacts",
            description: "Get a notification all the contacts you are losing touch with",
            status: notificationSettings.losingTouch.web,
            key: "losingTouch"
        },{
            name: "Opportunities Closing Today",
            description: "Get a notification for all opportunities closing today",
            status: notificationSettings.oppClosingToday.web,
            key: "oppClosingToday"
        },{
            name: "Deals at Risk",
            description: "Get a notification for the deals at risk",
            status: notificationSettings.dealsAtRiskForUser.web,
            key: "dealsAtRiskForUser"
        },{
            name: "Opportunities Closing this week",
            description: "Get a notification for the opportunities closing this week",
            status: notificationSettings.oppClosingThisWeek.web,
            key: "oppClosingThisWeek"
        },{
            name: "Weekly Commit",
            description: "Get a notification for weekly commits by team members",
            status: notificationSettings.weeklyCommit.web,
            key: "weeklyCommit"
        },{
            name: "Target vs WON/LOST",
            description: "Get a notification for opportunities won/lost against targets",
            status: notificationSettings.targetWonLostManager.web,
            key: "targetWonLostManager"
        });

        $scope.updateSettings = function(setting) {

            if(setting.key === "notificationEnable") {
                Notification.requestPermission().then(function (permission) {
                    setting.status = permission == "granted" ? true : false;

                    $http.post("/update/firebase/token", { from: "web", notificationEnabled: setting.status })
                        .success(function(response) {
                        })
                });
            } else {
                var postObj = {
                    "settingName": setting.key,
                    "status": setting.status,
                    "updateFor": "web"
                };
    
                $http.post("/update/notification/settings", postObj)
                    .success(function(response) {
                    })
            }
        }
    });

});

reletasApp.controller("dashboardSetting_data_controller",function($scope, $http, profileService){
    profileService.integrateDashboardSetting = function(dashboardSetting){
        $scope.dashboard = {};
        // $scope.dashboard.responsePending = dashboardSetting ? (dashboardSetting.responsePending == false ? false : true) : true;
        // $scope.dashboard.followUp = dashboardSetting ? (dashboardSetting.followUp == false ? false : true) : true;
        // $scope.dashboard.meetingFollowUp = dashboardSetting ? (dashboardSetting.meetingFollowUp == false ? false : true) : true;
        // $scope.dashboard.remindToConnect = dashboardSetting ? (dashboardSetting.remindToConnect == false ? false : true) : true;
        // $scope.dashboard.travellingToLocation = dashboardSetting ? (dashboardSetting.travellingToLocation == false ? false : true) : true;
        // $scope.dashboard.losingTouch = dashboardSetting ? (dashboardSetting.losingTouch == false ? false : true) : true;
        
        $scope.dashboard.responsePending = dashboardSetting ? (dashboardSetting.responsePending == false ? false : true) : false;
        $scope.dashboard.followUp = dashboardSetting ? (dashboardSetting.followUp == false ? false : true) : false;
        $scope.dashboard.meetingFollowUp = dashboardSetting ? (dashboardSetting.meetingFollowUp == false ? false : true) : false;
        $scope.dashboard.remindToConnect = dashboardSetting ? (dashboardSetting.remindToConnect == false ? false : true) : false;
        $scope.dashboard.travellingToLocation = dashboardSetting ? (dashboardSetting.travellingToLocation == false ? false : true) : false;
        $scope.dashboard.losingTouch = dashboardSetting ? (dashboardSetting.losingTouch == false ? false : true) : false;

        $scope.dashboardAll = allValuesInObjectAreTrue($scope.dashboard)
    }

    $scope.toggleDashboardSetting = function(){
        $scope.dashboardAll = allValuesInObjectAreTrue($scope.dashboard)
    }

    $scope.toggleDashboardSettingAll = function(){
        $scope.dashboard.responsePending = $scope.dashboardAll;
        $scope.dashboard.followUp = $scope.dashboardAll;
        $scope.dashboard.meetingFollowUp = $scope.dashboardAll;
        $scope.dashboard.remindToConnect = $scope.dashboardAll;
        $scope.dashboard.travellingToLocation = $scope.dashboardAll;
        $scope.dashboard.losingTouch = $scope.dashboardAll;
    }

    profileService.getDashboardSetting = function(){
        return $scope.dashboard;
    }
});

function getTextLength(text,maxLength){
    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength);
        return formattedText+'..';
    }
    else return text;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

function allValuesInObjectAreTrue(obj) {
    for (var i in obj) {
        if (obj[i] === false) return false;
    }
    return true;
}