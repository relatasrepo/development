
var reletasApp = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}
var isAddSocial = false;
$(document).ready(function(){
    var p = getParams(window.location.href)

})

reletasApp.controller("header_controller", function($scope){
    $scope.getMiddleBarTemplate = function(){
        return ""
    }
});

reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

reletasApp.service('profileService', function () {
    return {
        nextTab : function(elem) {
            toastr.clear()
            //$(elem).next().find('a[data-toggle="tab"]').click();
        }

    };
});

var timezone,l_userId;

reletasApp.controller("logedinUser", function ($scope, $http, profileService) {

    profileService.getProfile = function(){
        $http.get('/profile/get/edit')
            .success(function (response) {
                if(response.SuccessCode){
                    var $active = $('.wizard .nav-tabs li.active');
                    $active.next().removeClass('disabled');
                    profileService.nextTab($active);
                    $("#profileStep").removeClass('active');
                    $("#profileStep").find('i').addClass('fa-check-circle-o');
                    $("#addSocialStep").addClass('active');
                    $("#step2").hide()
                    $("#tabs-2").hide()
                    $("#step3").show()
                    $("#tabs-3").show()
                    $scope.l_usr = response.Data;
                    profileService.l_user = response.Data;
                    l_userId = $scope.l_usr._id;

                    identifyMixPanelUser(response.Data);
                    if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                        timezone = response.Data.timezone.name;
                    }
                    profileService.integrateSocialAccounts(response.Data);
                }
                else{

                }
            }).error(function (data) {

        })
    };
    profileService.getProfile();
});

reletasApp.controller("onboardingEnterprise", function($scope, $http, profileService, $window) {
    $scope.saveButtonText = 'Accept Terms & Next';

    getPartialAccountDetails();

    $scope.jsonToQueryString = function(json) {
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    };

    $scope.mNewUser = function(){
        mixpanel.track('New User Register', {
            'First Name' : $scope.firstName,
            'Last Name':  $scope.lastName,
            'Email' : $scope.emailId,
            'Company': $scope.companyName,
            'Designation':$scope.designation
        });
    };

    $scope.saveEnterpriseProfile = function() {
        if(!checkRequired($scope.firstName)){
            toastr.error("Please enter first name")
        }
        else if(!checkRequired($scope.lastName)){
            toastr.error("Please enter last name")
        }
        else if(!checkRequired($scope.publicProfileUrl)){
            toastr.error("Please enter unique name")
        }
        else if($scope.addGoogleAccount || $scope.addOutlookAccount){
            toastr.error("Please authenticate with Google or Outlook to proceed")
        }
       /* else if(!checkRequired($scope.companyName)){
            toastr.error("Please enter company name")
        }
        else if(!checkRequired($scope.designation)){
            toastr.error("Please enter designation")
        }*/
        else{
            $scope.saveButtonText = 'Please Wait..'
            var data = {
                userId:$scope.userId,
                firstName:$scope.firstName,
                lastName:$scope.lastName,
                publicProfileUrl:$scope.publicProfileUrl.replace(/[^a-zA-Z0-9]/g,''),
                companyName: $scope.companyName || null,
                designation:$scope.designation || null,
                profilePicUrl:$scope.profilePicUrl,
                screenName:$scope.publicProfileUrl,
                registeredUser: true,
                userType:'registered',
                reportingManager:null,
                corporateUser:false,
                companyId:null,
                fromOnBoarding:true
            };
            checkIdentityAvailability(data.publicProfileUrl,function(isOk){
                if(isOk){
                    $http.post('/updatePartialAcc/google', data)
                        .success(function(response){
                            if(response){
                                $scope.saveButtonText = 'Accept Terms & Next'
                                if(profileService.scheduleFlow){
                                    window.location = profileService.onSuccess;
                                }
                                else{
                                    var $active = $('.wizard .nav-tabs li.active');
                                    $active.next().removeClass('disabled');
                                    profileService.nextTab($active);
                                    $("#profileStep").removeClass('active');
                                    $("#profileStep").find('i').addClass('fa-check-circle-o');
                                    $("#addSocialStep").addClass('active');
                                    $("#step2").hide()
                                    $("#tabs-2").hide()
                                    $("#step3").show()
                                    $("#tabs-3").show()
                                    profileService.getProfile();
                                }

                                //Google event tracker
                                ga('send', {
                                    hitType: 'event',
                                    eventCategory: 'Onboarding User',
                                    eventAction: ' Accept T&C',
                                    eventLabel: 'Success'
                                });

                            }
                            else{
                                $scope.saveButtonText = 'Accept Terms & Next'
                                toastr.error("An error occurred. Please try again")
                            }
                        })
                }
                else{
                    $scope.saveButtonText = 'Accept Terms & Next'
                    toastr.error("Unique name already exists. Please change it");
                }
            })
        }
        //$window.location.reload();
    };

    $scope.validateUniqueName = function(uName){
        checkIdentityAvailability(uName)
    }
    var flag = false;
    function checkIdentityAvailability(identity,callback){

        var details = {
            publicProfileUrl:getValidRelatasIdentity(identity),
            userId:$scope.userId
        };
        $http.post('/checkUrl/linkedin',details)
            .success(function(response){
                if(response == true){
                    $scope.publicProfileUrl = getValidRelatasIdentity(identity)

                    i = 1
                    if(flag){
                        flag = false;
                        toastr.warning("Your selected name is not available. Next best name suggested is:  "+identity+"")
                    }
                    if(callback){callback(true)}
                }
                else{
                    if(callback){
                        callback(false)
                    }
                    else a(identity)
                }
            });
    }
    var i = 1;
    function a(identity){
        identity = getValidRelatasIdentity(identity)+''+i
        i++;
        flag = true;
        checkIdentityAvailability(identity)
    }

    function getValidRelatasIdentity(rIdentity) {
        rIdentity = rIdentity.toLowerCase();
        rIdentity = rIdentity.replace(/[^a-zA-Z0-9]/g,'');
        return rIdentity;
    }

    $scope.autoCompleteCompanies = function(text){
        $scope.cList = [];
        if(checkRequired(text) && text.length > 2){
            var req = {
                method: 'GET',
                url: 'https://autocomplete.clearbit.com/v1/companies/suggest?query='+text,
                headers: {
                    'If-Modified-Since': undefined
                }
            };
            $http(req).then(function(response){
                    if(response && response.statusText == 'OK'){
                        var position = $("#exampleInput-c").position();
                        position.top = position.top+40
                        $(".search-results").show()
                        $(".search-results").css(position)
                        $scope.cList = response.data;
                        $("body").on("click",function(){
                            $(".search-results").hide()
                        })
                    }
                },
                function(){

                });
        }
        else $(".search-results").hide()
    };

    $scope.selectCompany = function(company){
        $(".search-results").hide()
        if(checkRequired(company) && checkRequired(company.name)){
            $scope.companyName = company.name;
        }
    };

    $scope.changeProfilePic = function(){
        $("#selectPic").trigger("click");
    };

    $("#selectPic").on("change",function(){
        if($(this)[0] && $(this)[0].files && $(this)[0].files.length > 0){
            $scope.upload($(this)[0].files[0])
        }
        else toastr.error("Please select file to upload")
    });
    
    $scope.upload = function(file){
        var formData = new FormData();
        formData.append('profilePic', file, file.name);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/profile/update/profilepic/web', true);
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            getPartialAccountDetails();
            var response = JSON.parse(xhr.response)
            if(response.SuccessCode){
                toastr.success(response.Message)
            }
            else{
                toastr.error(response.Message)
            }
        };
        // Send the Data.
        xhr.send(formData);
    };

    function getPartialAccountDetails() {

        $http.get('/getPartialAccountDetails')
            .success(function(response){
                
                $scope.userId = response._id
                $scope.firstName = response.firstName;
                $scope.lastName = response.lastName;
                $scope.publicProfileUrl = response.publicProfileUrl?response.publicProfileUrl.replace(/[^a-zA-Z0-9]/g,''):"";
                $scope.emailId = response.emailId
                $scope.profilePicUrl = response.profilePicUrl
                $scope.companyName = response.companyName
                $scope.designation = response.designation
                profileService.scheduleFlow = response.scheduleFlow || false;
                profileService.onSuccess = response.onSuccess || null;
                if(response && response.google && response.google.length > 0){
                    $scope.addGoogleAccount = false;
                    $scope.addOutlookAccount = false;
                } else if(response && response.outlook && response.outlook.length > 0){
                    $scope.addGoogleAccount = false;
                    $scope.addOutlookAccount = false;
                }
                else{

                    $scope.addGoogleAccount = true;
                    $scope.addOutlookAccount = true;

                    var reqObj = {}

                    $scope.authenticateAcc = function (accType) {
                        if(accType == 'google'){
                            reqObj = {
                                onSuccess:'/onboarding/new',
                                onFailure:'/onboarding/new',
                                action:'custom_onboarding',
                                userId:response._id,
                                registrationService:'custom',
                                emailId:$scope.emailId
                            };

                            window.location = '/authenticate/google/add/web/onboarding'+$scope.jsonToQueryString(reqObj)

                        } else if(accType == 'outlook'){
                            reqObj = {
                                onSuccess:'/onboarding/new',
                                oAuthStep2:'/authorizeTest/',
                                onFailure:'/onboarding/new',
                                action:'custom_onboarding',
                                userId:response._id,
                                registrationService:'custom',
                                emailId:$scope.emailId
                            };

                            window.location = '/outlook/v2.0/authenticate'+$scope.jsonToQueryString(reqObj)
                        }
                    };
                }
            });
    }

});

reletasApp.controller("enterprise_social_data_controller",function($scope, $http, profileService){
    profileService.integrateSocialAccounts = function(profile){
        $scope.twitter = {};
        $scope.linkedin = {};
        $scope.facebook = {};
        if(checkRequired(profile.twitter) && checkRequired(profile.twitter.id)){
            $scope.twitter = {
                name:profile.twitter.userName,
                butAction:'remove',
                butText:'Remove',
                butShow:true
            }
        }
        else{
            $scope.twitter = {
                name:'Twitter',
                butAction:'add',
                butText:'Add',
                butShow:true

            };

            mixpanel.track('Onboard Twitter Add', {
                    'Name' : profile.twitter.userName
            });
        }
        if(checkRequired(profile.linkedin) && checkRequired(profile.linkedin.id)){
            $scope.linkedin = {
                name:profile.linkedin.name,
                butAction:'remove',
                butText:'Remove',
                butShow:true
            }
        }
        else{
            $scope.linkedin = {
                name:'LinkedIn',
                butAction:'add',
                butText:'Add',
                butShow:true
            };

            mixpanel.track('Onboard Linkedin Add', {
                'Name' : profile.linkedin.name
            });
        }
        if(checkRequired(profile.facebook) && checkRequired(profile.facebook.id)){
            $scope.facebook = {
                name:profile.facebook.name,
                butAction:'remove',
                butText:'Remove',
                butShow:true
            }
        }
        else{
            $scope.facebook = {
                name:'Facebook',
                butAction:'add',
                butText:'Add',
                butShow:true
            };

            mixpanel.track('Onboard Facebook Add', {
                'Name' : profile.facebook.name
            });
        }
    };

    $scope.socialAction = function(accountType,action){
        if(action == 'remove'){
            $http.delete('/profile/update/remove/'+accountType+'/web')
                .success(function(response){
                    if(response.SuccessCode){
                        profileService.getProfile();
                    }
                    else toastr.error(response.Message);
                })
        }
        else if(action == 'add'){
            window.location = '/meetex/'+accountType+'/add/custom'+jsonToQueryString({onSuccess:'/onboarding/new',onFailure:'/onboarding/new'});

        }
    }

    function jsonToQueryString(json) {
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }

    $scope.afterSocial = function(){
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        profileService.nextTab($active);
        $("#addSocialStep").removeClass('active');
        $("#addSocialStep").find('i').addClass('fa-check-circle-o');
        $("#tourRelatasStep").addClass('active');
        $("#step2").hide()
        $("#tabs-2").hide()
        $("#step3").hide()
        $("#tabs-3").hide()
        $("#step4").show()
        $("#tabs-4").show()
    }
    mixpanel.track("Add Social - Next Button Onboarding Retail");
});

reletasApp.controller("tour_controller",function($scope, $http, profileService){
    $scope.nextStep = function(){
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        profileService.nextTab($active);
        $("#tourRelatasStep").removeClass('active');
        $("#tourRelatasStep").find('i').addClass('fa-check-circle-o');
        $("#completedStep").addClass('active');
        profileService.nextTab($active);
        $("#step2").hide()
        $("#tabs-2").hide()
        $("#step3").hide()
        $("#tabs-3").hide()
        $("#step4").hide()
        $("#tabs-4").hide()
        $("#step5").hide()
        $("#tabs-5").hide()
        $("#complete").show()
        $("#tabs-6").show()
       /* $("#selectAccounts").addClass('active');
        $("#step2").hide()
        $("#tabs-2").hide()
        $("#step3").hide()
        $("#tabs-3").hide()
        $("#step4").hide()
        $("#tabs-4").hide()
        $("#step5").show()
        $("#tabs-5").show()
        profileService.getEmailAccounts()*/

        mixpanel.track("Tour - Next Button Onboarding Retail");
    }
});
/*
reletasApp.controller("account_email_domain",function($scope, $http, profileService){
    $scope.showToReportingManager = function(isSelected){
        return isSelected ? 'rm-visibility-selected' : 'rm-visibility-unselected'
    };

    $scope.changeShowReportingManager = function(account,selected){
        for(var i=0; i<$scope.contactsByAccout.length; i++){
            if($scope.contactsByAccout[i].account == account){
                $scope.contactsByAccout[i].showToReportingManager = !selected;
            }
        }
    };

    $scope.addTo = function(account){
        var list = [];
        var others = [];
        for(var i=0; i<$scope.contactsByAccout.length; i++){
            if($scope.contactsByAccout[i].account == 'other'){
                for(var l=0; l<$scope.contactsByAccout[i].contacts.length; l++){

                    if($scope.contactsByAccout[i].contacts[l].selected == true){
                        list.push($scope.contactsByAccout[i].contacts[l])
                    }
                    else others.push($scope.contactsByAccout[i].contacts[l])
                    //else others.push($scope.contactsByAccout[i].contacts[l])
                }
                $scope.contactsByAccout[i].contacts = others;
                $scope.contactsByAccout[i].total = $scope.contactsByAccout[i].contacts.length;
                if(!$scope.contactsByAccout[i].contacts.length > 0){
                    $scope.deletedAcc = $scope.contactsByAccout.splice(i,1);
                }
                //break;
            }
        }

        for(var j=0; j<$scope.contactsByAccout.length; j++){
            if($scope.contactsByAccout[j].account == account){
                for(z=0; z<list.length; z++){
                    list[z].selected = true;
                }
                $scope.contactsByAccout[j].contacts = $scope.contactsByAccout[j].contacts.concat(list);
                $scope.contactsByAccout[j].total += list.length;
                //break;
            }
        }

        mixpanel.track("Add to an Account - Onboarding Retail");
    };

    $scope.contactUnSelected = function(selected,contact,account){

        if(!selected && account != 'other'){
            var acc = contact.personEmailId.split("@")[1].split(".")[0];
            if(acc != account){
                for(var i=0; i<$scope.contactsByAccout.length; i++){
                    if($scope.contactsByAccout[i].account == account){
                        for(var j=0; j<$scope.contactsByAccout[i].contacts.length; j++){
                            if($scope.contactsByAccout[i].contacts[j].personEmailId == contact.personEmailId){
                                $scope.contactsByAccout[i].total -= 1;
                                $scope.contactsByAccout[i].contacts.splice(j,1);
                            }
                        }
                    }
                }
                var isOk = false;
                for(var k=0; k<$scope.contactsByAccout.length; k++){
                    if($scope.contactsByAccout[k].account == 'other'){
                        isOk = true;
                        $scope.contactsByAccout[k].contacts.push(contact);
                        $scope.contactsByAccout[k].total += 1
                    }
                }
                if(!isOk){
                    if($scope.deletedAcc && $scope.deletedAcc.all){
                        $scope.contactsByAccout.push({
                            account:'other',
                            all:$scope.deletedAcc.all || true,
                            selectAll:$scope.deletedAcc.selectAll || 'otherSelectAll',
                            selected:$scope.deletedAcc.selected || true,
                            showToReportingManager:$scope.deletedAcc.showToReportingManager || true,
                            total:1,
                            contacts:[contact]
                        })
                    }
                    else{
                        $scope.contactsByAccout.push({
                            account:'other',
                            all:true,
                            selectAll:'otherSelectAll',
                            selected:true,
                            showToReportingManager: true,
                            total:1,
                            contacts:[contact]
                        })
                    }
                }
            }
        }
    };

    $scope.saveAccountSelection = function(){

        var updateList = []
        for(var key in $scope.contactsByAccout){
            //  if(key != 'other'){
            for(var i=0; i<$scope.contactsByAccout[key].contacts.length; i++){
                updateList.push({
                    contactId:$scope.contactsByAccout[key].contacts[i]._id,
                    selected:$scope.contactsByAccout[key].contacts[i].selected,
                    showToReportingManager:$scope.contactsByAccout[key].showToReportingManager,
                    accountName:$scope.contactsByAccout[key].account == 'other' ? null : $scope.contactsByAccout[key].account
                })
            }
            //}
        }
        if(updateList.length > 0){
            $http.post('/onboarding/update/selected/accounts',updateList)
                .success(function(response){

                });
        }

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        $("#selectAccounts").removeClass('active');
        $("#selectAccounts").find('i').addClass('fa-check-circle-o');
        $("#completedStep").addClass('active');
        profileService.nextTab($active);
        $("#step2").hide()
        $("#tabs-2").hide()
        $("#step3").hide()
        $("#tabs-3").hide()
        $("#step4").hide()
        $("#tabs-4").hide()
        $("#step5").hide()
        $("#tabs-5").hide()
        $("#complete").show()
        $("#tabs-6").show()

        mixpanel.track("Add Accounts - Next Button Onboarding Retail");
    };

    profileService.getEmailAccounts = function(){
        $http.get('/onboarding/get/contacts/accounts')
            .success(function(response){
                if(response.SuccessCode){
                    $scope.show_no_accounts = false;
                    $scope.contactsByAccout = response.Data.emailAccounts;
                    $scope.allAccountNames = response.Data.allAccountNames;
                }
                else{
                    $scope.no_accounts_message = response.Message;
                    $scope.show_no_accounts = true;
                }
            })
    };

    $scope.changeSelectAll = function(account,changed){
        for(var i=0; i<$scope.contactsByAccout.length; i++){
            if($scope.contactsByAccout[i].account == account){
                for(var j=0 ;j<$scope.contactsByAccout[i].contacts.length; j++){
                    $scope.contactsByAccout[i].contacts[j].selected = changed;
                }
            }
        }
    }
});*/

reletasApp.controller("start_using_relatas",function($scope, $http, profileService){
    $scope.startUsingRelatas = function(){
        $("#completedStep").removeClass('active');
        $("#completedStep").find('i').addClass('fa-check-circle-o');

        window.location = '/';

        mixpanel.track("Start using Relatas - Onboarding Retail");

        //Google event tracker
        ga('send', {
            hitType: 'event',
            eventCategory: 'Onboarding User',
            eventAction: ' Today Landing',
            eventLabel: 'Success'
        });

    }
});

function getTextLength(text,maxLength){
    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength);
        return formattedText+'..';
    }
    else return text;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}