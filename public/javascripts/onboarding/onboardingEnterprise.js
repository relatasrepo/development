
var reletasApp = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}
var isAddSocial = false;
$(document).ready(function(){
   var p = getParams(window.location.href)

});

reletasApp.controller("header_controller", function($scope){
    $scope.getMiddleBarTemplate = function(){
        return ""
    }
    $(".login_header").hide()
    $("#login_model_button").hide()
});

reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

reletasApp.service('share', function () {
    return {
        nextTab : function(elem) {
            toastr.clear()
        //$(elem).next().find('a[data-toggle="tab"]').click();
        }

    };
});

var timezone,l_userId;
reletasApp.controller("logedinUser", function ($scope, $http, share) {

    share.getProfile = function(){
        $http.get('/profile/get/edit')
            .success(function (response) {

                if(response.SuccessCode){
                    var $active = $('.wizard .nav-tabs li.active');
                    $active.next().removeClass('disabled');
                    share.nextTab($active);
                    $("#profileStep").removeClass('active');
                    $("#profileStep").find('i').addClass('fa-check-circle-o');
                    $("#addSocialStep").addClass('active');
                    $("#step2").hide()
                    $("#tabs-2").hide()
                    $("#step3").show()
                    $("#tabs-3").show()
                    $scope.l_usr = response.Data;
                    share.l_user = response.Data;
                    l_userId = $scope.l_usr._id;

                    identifyMixPanelUser(response.Data);
                    if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                        timezone = response.Data.timezone.name;
                    }
                    share.integrateSocialAccounts(response.Data);
                }
                else{

                }
            }).error(function (data) {

        })
    };
    share.getProfile();
});

reletasApp.controller("onboarding_steps_bar",function($scope, $http, share){

});

reletasApp.controller("onboardingEnterprise", function($scope, $http, share) {

    getPartialAccountDetails();

    $scope.mNewUser = function() {
        mixpanel.track('New Enterprise User Register', {
            'First Name': $scope.firstName,
            'Last Name': $scope.lastName,
            'Email': $scope.emailId,
            'Company': $scope.companyName,
            'Designation': $scope.designation
        });
    }

    $scope.saveEnterpriseProfile = function() {
        if(!checkRequired($scope.firstName)){
            toastr.error("Please enter first name")
        }
        else if(!checkRequired($scope.lastName)){
            toastr.error("Please enter last name")
        }
        else if(!checkRequired($scope.publicProfileUrl)){
            toastr.error("Please enter unique name")
        }
        else if(!checkRequired($scope.companyName)){
            toastr.error("Please enter company name")
        }
        else if(!checkRequired($scope.designation)){
            toastr.error("Please enter designation")
        }
        else{
            var data = {
                userId:$scope.userId,
                firstName:$scope.firstName,
                lastName:$scope.lastName,
                publicProfileUrl:$scope.publicProfileUrl.replace(/[^a-zA-Z0-9]/g,''),
                companyName: $scope.companyName,
                designation:$scope.designation,
                profilePicUrl:$scope.profilePicUrl,
                screenName:$scope.publicProfileUrl,
                registeredUser: true,
                userType:'registered',
                reportingManager:$scope.reportingManager,
                companyId:$("#companyId").val(),
                fromOnBoarding:true
            };
            checkIdentityAvailability(data.publicProfileUrl,function(isOk){
                 if(isOk){
                     $http.post('/updatePartialAcc/google', data)
                         .success(function(response){
                             if(response){
                                 var $active = $('.wizard .nav-tabs li.active');
                                 $active.next().removeClass('disabled');
                                 share.nextTab($active);
                                 $("#profileStep").removeClass('active');
                                 $("#profileStep").find('i').addClass('fa-check-circle-o');
                                 $("#addSocialStep").addClass('active');
                                 $("#step2").hide()
                                 $("#tabs-2").hide()
                                 $("#step3").show()
                                 $("#tabs-3").show()
                                 share.getProfile();

                                 //Google event tracker
                                 ga('send', {
                                     hitType: 'event',
                                     eventCategory: 'Onboarding User',
                                     eventAction: ' Accept T&C',
                                     eventLabel: 'Success'
                                 });
                             }
                             else{
                                 toastr.error("An error occurred. Please try again")
                             }
                         })
                 }
                 else{
                    toastr.error("Unique name already exists. Please change it");
                 }
            })
        }
    };

    $scope.validateUniqueName = function(uName){
        checkIdentityAvailability(uName)
    }
    var flag = false;
    function checkIdentityAvailability(identity,callback){

        var details = {
            publicProfileUrl:getValidRelatasIdentity(identity),
            userId:$scope.userId
        };
        $http.post('/checkUrl/linkedin',details)
            .success(function(response){
                if(response == true){
                    $scope.publicProfileUrl = getValidRelatasIdentity(identity)

                    i = 1
                    if(flag){
                        flag = false;
                        toastr.warning("Your selected name is not available. Next best name suggested is:  "+identity+"")
                    }
                    if(callback){callback(true)}
                }
                else{
                    if(callback){
                        callback(false)
                    }
                    else a(identity)
                }
            });
    }
    var i = 1;
    function a(identity){
        identity = getValidRelatasIdentity(identity)+''+i
        i++;
        flag = true;
        checkIdentityAvailability(identity)
    }

    function getValidRelatasIdentity(rIdentity) {
        rIdentity = rIdentity.toLowerCase();
        rIdentity = rIdentity.replace(/[^a-zA-Z0-9]/g,'');
        return rIdentity;
    }

    $scope.autoCompleteCompanies = function(text){
        $scope.cList = [];
        if(checkRequired(text) && text.length > 2){
            var req = {
                method: 'GET',
                url: 'https://autocomplete.clearbit.com/v1/companies/suggest?query='+text,
                headers: {
                    'If-Modified-Since': undefined
                }
            };
            $http(req).then(function(response){
                    if(response && response.statusText == 'OK'){
                        var position = $("#exampleInput-c").position();
                        position.top = position.top+40
                        $(".search-results").show()
                        $(".search-results").css(position)
                        $scope.cList = response.data;
                        $("body").on("click",function(){
                            $(".search-results").hide()
                        })
                    }
                },
                function(){

                });
        }
        else $(".search-results").hide()
    };

    $scope.selectCompany = function(company){
        $(".search-results").hide()
        if(checkRequired(company) && checkRequired(company.name)){
            $scope.companyName = company.name;
        }
    };

    $scope.getUsersInCompany = function(){
        $http.get('/onboarding/get/company/users?companyId='+$("#companyId").val())
            .success(function(response){
                if(response.SuccessCode){
                    $scope.companyMembers = response.Data;
                }
            })
    };

    $scope.getUsersInCompany();

    $scope.changeProfilePic = function(){
        $("#selectPic").trigger("click");
    };

    $("#selectPic").on("change",function(){
        if($(this)[0] && $(this)[0].files && $(this)[0].files.length > 0){
            $scope.upload($(this)[0].files[0])
        }
        else toastr.error("Please select file to upload")
    });

    $scope.upload = function(file){
        var formData = new FormData();
        formData.append('profilePic', file, file.name);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/profile/update/profilepic/web', true);
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            getPartialAccountDetails();
            var response = JSON.parse(xhr.response)
            if(response.SuccessCode){
                toastr.success(response.Message)
            }
            else{
                toastr.error(response.Message)
            }
        };
        // Send the Data.
        xhr.send(formData);
    };
    
    function getPartialAccountDetails() {
        $http.get('/getPartialAccountDetails')
            .success(function(response){
                $scope.userId = response._id
                $scope.firstName = response.firstName;
                $scope.lastName = response.lastName;
                $scope.publicProfileUrl = response.publicProfileUrl;
                $scope.emailId = response.emailId
                $scope.profilePicUrl = response.profilePicUrl
                $scope.companyName = response.companyName
                $scope.designation = response.designation
            });
    }
});

reletasApp.controller("enterprise_social_data_controller",function($scope, $http, share){
    share.integrateSocialAccounts = function(profile){
        $scope.twitter = {};
        $scope.linkedin = {};
        $scope.facebook = {};
        if(checkRequired(profile.twitter) && checkRequired(profile.twitter.id)){
            $scope.twitter = {
                name:profile.twitter.userName,
                butAction:'remove',
                butText:'Remove',
                butShow:true
            }
        }
        else{
            $scope.twitter = {
                name:'Twitter',
                butAction:'add',
                butText:'Add',
                butShow:true
            };
            mixpanel.track('Onboard Enterprise Twitter Add', {
                'Name' : profile.twitter.userName
            });
        }
        if(checkRequired(profile.linkedin) && checkRequired(profile.linkedin.id)){
            $scope.linkedin = {
                name:profile.linkedin.name,
                butAction:'remove',
                butText:'Remove',
                butShow:true
            }
        }
        else{
            $scope.linkedin = {
                name:'LinkedIn',
                butAction:'add',
                butText:'Add',
                butShow:true
            };

            mixpanel.track('Onboard Enterprise Linkedin Add', {
                'Name' : profile.linkedin.userName
            });
        }
        if(checkRequired(profile.facebook) && checkRequired(profile.facebook.id)){
            $scope.facebook = {
                name:profile.facebook.name,
                butAction:'remove',
                butText:'Remove',
                butShow:true
            }
        }
        else{
            $scope.facebook = {
                name:'Facebook',
                butAction:'add',
                butText:'Add',
                butShow:true
            };

            mixpanel.track('Onboard Enterprise Facebook Add', {
                'Name' : profile.facebook.userName
            });
        }
    };

    $scope.socialAction = function(accountType,action){
        if(action == 'remove'){
            $http.delete('/profile/update/remove/'+accountType+'/web')
                .success(function(response){
                    if(response.SuccessCode){
                        share.getProfile();
                    }
                    else toastr.error(response.Message);
                })
        }
        else if(action == 'add'){
            window.location = '/meetex/'+accountType+'/add/custom'+jsonToQueryString({onSuccess:'/onboarding/enterprise',onFailure:'/onboarding/enterprise'});
        }
    }

    function jsonToQueryString(json) {
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }

    $scope.afterSocial = function(){
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        share.nextTab($active);
        $("#addSocialStep").removeClass('active');
        $("#addSocialStep").find('i').addClass('fa-check-circle-o');
        $("#tourRelatasStep").addClass('active');
        $("#step2").hide()
        $("#tabs-2").hide()
        $("#step3").hide()
        $("#tabs-3").hide()
        $("#step4").show()
        $("#tabs-4").show()
    }
});

reletasApp.controller("tour_controller",function($scope, $http, share){
    $scope.nextStep = function(){
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        share.nextTab($active);
        $("#tourRelatasStep").removeClass('active');
        $("#tourRelatasStep").find('i').addClass('fa-check-circle-o');
        $("#completedStep").addClass('active');
        share.nextTab($active);
        $("#step2").hide()
        $("#tabs-2").hide()
        $("#step3").hide()
        $("#tabs-3").hide()
        $("#step4").hide()
        $("#tabs-4").hide()
        $("#step5").hide()
        $("#tabs-5").hide()
        $("#complete").show()
        $("#tabs-6").show()

    }
});

reletasApp.controller("start_using_relatas",function($scope, $http, share){
    $scope.startUsingRelatas = function(){
        $("#completedStep").removeClass('active');
        $("#completedStep").find('i').addClass('fa-check-circle-o');


        //Google event tracker
        ga('send', {
            hitType: 'event',
            eventCategory: 'Onboarding User',
            eventAction: ' Today Landing',
            eventLabel: 'Success'
        });

        window.location = '/insights';
    }
});

function getTextLength(text,maxLength){
    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength);
        return formattedText+'..';
    }
    else return text;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}