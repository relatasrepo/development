/**
 * Created by naveen on 11/4/15.
 */
var reletasApp = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope){

});

reletasApp.controller("signup_box",function($scope,$http){
    var reqObj;

    $http.get("/get/company/by/domain?url="+window.domain)
        .success(function (response) {
            $scope.displayGoogleBtn = true;
            $scope.displayOutlookBtn = true;

            if(response){
                if(response.serviceLogin.toLowerCase() == "linkedin" || response.serviceLogin.toLowerCase() == "google"){
                    $scope.displayGoogleBtn = true;
                    $scope.displayOutlookBtn = false;
                }

                if(response.serviceLogin.toLowerCase() == "outlook"){
                    $scope.displayOutlookBtn = true;
                    $scope.displayGoogleBtn = false;
                }

            }

        });
    
    $scope.signUp = function(signUpWith){
        if(signUpWith == 'google'){
            reqObj = {
                onSuccess:'/onboarding/enterprise',
                onFailure:'/',
                action:'customSignup'
            };
            $scope.addAccountsUrl = '/authenticate/google/signup/web'+jsonToQueryString(reqObj);
            window.location = $scope.addAccountsUrl;
        } else if(signUpWith == 'outlook'){
            reqObj = {
                onSuccess:'/onboarding/new',
                oAuthStep2:'/authorizeTest/',
                onFailure:'/',
                action:'customOutlookSignup'
            };
            window.location = '/outlook/v2.0/authenticate'+jsonToQueryString(reqObj);
        }
    };

    function jsonToQueryString(json) {
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }
});
