$(document).ready(function(){
     var userP;
    $("#getUserInfo").on('click',function(){
        console.log(location.host)

        var part = location.hostname.split('.');
        var subdomains = part.shift();
        var upperleveldomains = part.join('.');

        var fullDomain = location.host
        console.log(subdomains)
        console.log(upperleveldomains)

        var emailId = $("#emailId").val();
        var relatasSubdomain = fetchCompanyFromEmail(emailId);
        var url = "http://localhost:7000/admin/test/login?fromAdmin=true&emailId="+emailId

        if(upperleveldomains.includes('local')){
        } else if(fullDomain.includes('showcase') || fullDomain.includes('exampledev')) {
            url = "https://showcase.relatas.com/admin/test/login?fromAdmin=true&emailId="+emailId
        } else {
            url = "https://relatas.com/admin/test/login?fromAdmin=true&emailId="+emailId
        }

        window.open(url,'_blank');

        if(!checkRequired(emailId)){
            showMessagePopUp("Please enter email id.",'error','Invalid Email Id');
        }
        else if(validateEmailField(emailId)){
            // window.open(location.protocol + "//" + location.host,'_blank');
        }
        else{
            showMessagePopUp("Please enter valid email id.",'error','Invalid Email Id');
        }
    });

    $("#generatePassword").on('click', function () {
        var url = "/admin/generate/password?otp="+$("#pwd").val()
        $.get(url,function (response) {
            console.log(response)
        });
    });

    $("#deleteUser").on('click', function () {
        if (!userP.corporateUser) {

            var userId = $(this).attr('userId');
            if (checkRequired(userId)) {
                $("confirm").attr('userId', userId);
                var html = _.template($("#confirm-popup").html())();
                $(this).popover({
                    title: "Delete User",
                    html: true,
                    content: html,
                    container: 'body',
                    placement: 'bottom',
                    id: "myPopover"
                });

                $(this).popover('show');
                $(".arrow").addClass("invisible");
                // $(".popover").style({'max-width':'25%'});
                $("#deleteMsg").text('Do you want to delete user "' + $(this).attr('name') + '" ')
            }
            else showMessagePopUp("Please select user.", 'error', 'No User Info');

        }
        else showMessagePopUp(userP.firstName+" is Corporate User.", 'error', 'Deletion error');
    });

    function deleteUser(){
       $("#user-info-table").hide();
        $.ajax({
            url:'/admin/user/delete/permanently',
            type:'POST',
            datatype:'JSON',
            data:{
                userId:userP._id,
                emailId:userP.emailId,
                firstName:userP.firstName,
                uniqueName:userP.publicProfileUrl
            },
            success:function(status){

                if(checkRequired(status)){
                    if(status.status == 'access-denied'){
                        showMessagePopUp("Un authorised Access",'error','Authorization Error');
                    }
                    else if(status.status == 'no-id'){
                        showMessagePopUp("Please select user.", 'error', 'No User Info');
                    }
                    else if(status.status == true){
                        showMessagePopUp("User successfully deleted.",'success','Success Message');
                    }
                    else showMessagePopUp("An error occurred. Please try again.", 'error', 'Unknown Error');
                }
                else showMessagePopUp("An error occurred. Please try again.", 'error', 'Unknown Error');
            }
        })
    }

    /* MESSAGE POPUP SCRIPT */
    function showMessagePopUp(message, msgClass, popupTitle) {

        var html = _.template($("#message-popup").html())();
        $("#getUserInfo").popover({
            title: popupTitle+ '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });

        $("#getUserInfo").popover('show');
        $(".arrow").addClass("invisible");
        if (msgClass == 'error') {
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        } else if (msgClass == 'success') {
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        } else if (msgClass == 'tip') {
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message);
        $("#closePopup-message").focus();

    }

    $("body").on("click", "#close", function () {
        $("#getUserInfo").popover('destroy');
    });

    $("body").on("click", "#confirm", function () {
        $("#deleteUser").popover('destroy');
        deleteUser();
    });

    $("body").on("click", "#cancelDeletion", function () {
        $("#deleteUser").popover('destroy');
    });

    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {
            return true;
        } else {

            return false;
        }
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }
});

function fetchCompanyFromEmail(email){
    var domain = email.substring(email.lastIndexOf("@") +1)
        , words = domain.split(".")
        , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
        , removableTextList = ["com", "org", "edu", "in"]

    _.reject(words, function(word){
        return removableTextList.indexOf(word.trim()) > -1
    })

    words = _.sortBy(words, function(word){return -(word.length);})
    return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
}