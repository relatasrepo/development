$(document).ready(function () {

    $('#sortable').sortable();    
    $('#sortable').disableSelection();    

    $('.drag').draggable({cancel:null});
    $('.drag').click(function(){
        $(this).focus();
    });

    $('.resize').resizable();

    $("body").on('DOMNodeInserted', '.drag', function () {
        $(this).draggable({cancel:null});
        $(this).click(function() {
            $(this).focus();
        });

    });

    $(".generate-input-text").click(function() {
        generateInputText();
    });

    $(".generate-input-box").click(function() {
        generateTextBox();
    });

    $(".generate-table").click(function() {
        generateTable();
    });

    $(".generate-image").click(function() {
        generateImageHolder();
    });

    $(".generate-header").click(function() {
        generateHeader();
    });
});

var generateInputText = function() {
    var inputLabel = prompt("Label Name");

    var inputLabelText = $("<label class='drag ui-draggable'></label>").css("position","relative").text(inputLabel);
    var inputTextBox = $('<input class="drag ui-draggable" />').css({'postion':'relative', 'margin-left':'30px'}).attr({type: "text"});
    $('.place-element').append(inputLabelText).append(inputTextBox);
}

var generateTextBox = function() {
    var inputTextArea = $("<li><div class='dynamic-element resize'><textarea></textarea></div></li>");
    $('.place-element').append(inputTextArea);
}

var generateTable = function() {
    var newTable = $('<li class="drag ui-draggable"><div class="dynamic-element"><table></table></div></li>');
    var rows = 2;
    var cols = new Number(prompt("number of cols"));
    var tr = [];

    for(var i=0; i<rows; i++) {
        var row = $('<tr></tr>').appendTo(newTable);
        if (i==0) {
            for(var j=0; j<cols; j++) {
                $('<th contenteditable="true"></th>').text("col"+j+1).appendTo(row);
            }
        } else {
            for(var j=0; j<cols; j++) {
                $('<td contenteditable="true"></td>').text("col"+j+1).appendTo(row);
            }
        }
    }
    $('.place-element').append(newTable);
}

var generateImageHolder = function() {
    var imageHolder = $('<li class="drag ui-draggable"><div class="dynamic-element"><input type="file"/><img id="image-holder" src="#" alt="company logo" /></div></li>').css({"height":"150px", "width":"150px"});
    $('.place-element').append(imageHolder);    
}

var generateHeader = function() {
    var headerName = prompt("Header Name");
    var header = $("<li class='drag' ><div class='dynamic-element'><h5>"+ headerName + "</h5></div></li>");
    $('.place-element').append(header);
}


