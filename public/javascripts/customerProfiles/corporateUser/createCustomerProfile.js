
var relatasApp = angular.module('relatasApp', ["ngLodash"]).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
}]);

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        liuDetails:function(user){
            this.liuData = user;
        },
        getUserId:function(){
            return this.userId
        },
        setUserEmailId:function(emailId){
            this.userEmailId = emailId;
        },
        setTeamMembers:function(members){
            this.team = members;
        },
        setTargetForFy:function(value){
            this.targetForFy = value;
        },
        getTeamMembers:function(){
            return this.team;
        },
        getLiuUserEmailId:function(){
            return this.userEmailId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setTown:function(geoLocationTown){
            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        }
    };
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

var timezone;
relatasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        share.companyDetails.currency.forEach(function (el) {
            share.currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                share.primaryCurrency = el.symbol;
            }
        });
    });
    getErrorMessages($rootScope,$http,"opportunity");
});

var guid = 0;
var colId = 0;
var xhr;
var imagesCache = [];
var companyImgUrl = "/docTemplateLogos/";
var docAttributeList = [];

var templateType = decodeURI(getParams(window.location.href).customerProfileTemplateType);
var templateName = decodeURI(getParams(window.location.href).customerProfileTemplateName);
var customerProfileName = decodeURI(getParams(window.location.href).customerProfileName);

var isActivated = getParams(window.location.href).isActivated;

relatasApp.controller('createCustomerProfileController', function($scope,$http) {

    $scope.formFields = [];
    $scope.current_field = {};
    $scope.tableList = {};
    $scope.formulaMode = false;
    $scope.customerProfileId = getParams(window.location.href).customerProfileId;
    $scope.customerProfileTemplateId = getParams(window.location.href).customerProfileTemplateId;
    $scope.formulaString = '';
    $scope.customerProfileName = customerProfileName;

    $scope.customerProfileTemplateType = templateType === "undefined" ? "" : templateType;
    $scope.customerProfileTemplateName = templateName === "undefined" ? "" : templateName;
    $scope.customerProfileName = $scope.customerProfileName === "undefined" ? "" : $scope.customerProfileName;

    if($scope.customerProfileId) {
        console.log("Step: inside the if block: customerProfileId");

        $http.post('/customerProfile/get/by/id', {"customerProfileId": $scope.customerProfileId})
            .success(function (response) {
                console.log(response);
                if(response.Data) {
                    var attrList = response.Data;

                    attrList.customerProfileElementList.forEach(function(stringEle) {
                        $scope.formFields.push(JSON.parse(stringEle));
                    })

                } else {
                }
            })
    } else if($scope.customerProfileTemplateId) {
        console.log("Creating new profile")

        $http.post('/customerProfileTemplates/getById', {"customerProfileTemplateId": $scope.customerProfileTemplateId})
            .success(function (response) {
                console.log("/customerProfileTemplates/getById Response :", response);
                if(response.Data) {
                    console.log("Inside if:", response);

                    var attrList = response.Data;
                    attrList.customerProfileTemplateElementList.forEach(function(stringEle) {
                        $scope.formFields.push(JSON.parse(stringEle));
                    })

                    console.log("Attribute list:", attrList);

                    guid = attrList.customerProfileTemplateUiId || guid;

                }
            })
    }

    $scope.getFieldSetting = function(field, settingName) {
        var result = {};
        var settings = field.Settings;
        $.each(settings, function(index, set) {
            if (set.Name == settingName) {
                result = set;
                return;
            }
        });
        if (!Object.keys(result).length) {
            //Continue to search settings in the checkbox zone
            $.each(settings[settings.length - 1].Options, function(index, set) {
                if (set.Name == settingName) {
                    result = set;
                    return;
                }
            });
        }
        return result;
    }

    $scope.changeFieldSetting = function(Value, SettingName) {
        switch (SettingName) {
            case 'Field Label':
            case 'Short Label':
            case 'Internal Name':
                $scope.current_field.Name = Value;
                $scope.current_field.Settings[0].Value = $scope.current_field.Name;
                // $scope.current_field.Settings[1].Value = $scope.current_field.Name;
                // $scope.current_field.Settings[2].Value = 'x' + $scope.current_field.Name.replace(/\s/g, '_');
                break;
            case 'Column Name':
                $scope.current_field.Name = Value;

            default:
                break;
        }
    }

    // table methods
    $scope.columnData = {
        'Name': "",
        'Type': "column",
        'RowValues':[],
        'Settings':[{
            'Name': 'Column Name',
            'Value': '',
            'Type': 'text'
        },{
            'Name': 'Field Type',
            'Value': 'String',
            'Type': 'dropdown',
            'PossibleValue': ['Boolean',
                'Formula',
                'Numeric',
                'Alpha Numeric',
                'String',
                'System Referenced',
                'Auto Complete']
        }, {
            'Name': 'Formula',
            'Value': '',
            'Type': 'formula',
            'Visibility': false
        }, {
            'Name': 'System Referenced',
            'Value': 'Opportunity',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': ['Opportunity']
        }, {
            'Name': 'System Referenced Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': ['Opportunity Name',
                'Product Type',
                'Source',
                'Region',
                'Vertical',
                'Solution',
                'Business Unit',
                'Amount',
                'Margin %']
        },{
            'Name': 'Auto Complete',
            'Value': '',
            'Type': 'dropdown_autocomplete',
            'Visibility': false,
            'PossibleValue': ['Customer Name', 'Cutomer Address']
        }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
                'Name': 'Mandatory',
                'Value': false
            }, {
                'Name': 'Customer Visible',
                'Value': false
            }]
        }]
    }

    $scope.getColumnIndex = function(table, colIndex, event) {
        event.stopPropagation();
        $scope.activeField(table.Columns[colIndex]);
    }

    $scope.addColumn = function(ele, idx, table) {
        var tableId = table.id;

        $scope.current_field.Active = false;
        ele.Name  =  'col_'+idx;

        $scope.current_field = createNewColumn();
        //Merge setting from template object
        angular.merge($scope.current_field, ele);

        if (typeof idx == 'undefined') {
            table.Columns.push($scope.current_field);
        } else {
            table.Columns.splice(idx, 0, $scope.current_field);
            $('#fieldSettingTab_lnk').tab('show');
        }
    };

    var getTableHeaders = function(table, numberOfColumns) {

        if(numberOfColumns < table.Columns.length) {
            table.Columns.splice(numberOfColumns, table.Columns.length-numberOfColumns);

        } else if(numberOfColumns > table.Columns.length){
            for (var i=table.Columns.length; i<numberOfColumns; i++) {
                $scope.addColumn($scope.columnData, i, table);
            }
        }

    }

    var generateColumnsForTable = function(table, numberOfColumns) {
        getTableHeaders(table, numberOfColumns);
    }

    $scope.getElementWidth = function(colSpan) {
        switch(Number(colSpan)) {
            case 1: return 'col-md-3';
            case 2: return 'col-md-6';
            case 3: return 'col-md-9';
            case 4: return 'col-md-12';
            default: return 'col-md-6';
        }
    }

    $scope.getFontWeight = function(size) {
        if(size === 'h1') return 'size-h1';
        if(size === 'h2') return 'size-h2';
        if(size === 'h3') return 'size-h3';
        if(size === 'h4') return 'size-h4';
        if(size === 'h5') return 'size-h5';
        if(size === 'h6') return 'size-h6';
    }

    $scope.saveCustomerProfile = function() {
        var docElementList = [];

        console.log("inside create new customerProfile");

        $scope.formFields.forEach(function(element) {
            element.Style = $("#"+element.id).attr("style") || "";
            docElementList.push(JSON.stringify(element));
        });

        if($scope.customerProfileTemplateType === "" || $scope.customerProfileTemplateType === undefined) {
            toastr.error("Template type is required")
        }
        else if($scope.customerProfileTemplateName === "" || $scope.customerProfileTemplateName === undefined) {
            toastr.error("Template name is required")
        }
        else if($scope.customerProfileName === "" || $scope.customerProfileName === undefined) {
            toastr.error("CustomerProfile name is required")
        }
        else {
            generateAttributeList();

            $http.post('/customerProfile/update/existing', {"customerProfileTemplateType":$scope.customerProfileTemplateType,
                "customerProfileTemplateName":$scope.customerProfileTemplateName,
                "customerProfileName":$scope.customerProfileName,
                "customerProfileElementList":docElementList,
                "customerProfileAttrList": docAttributeList })
                .success(function(response) {
                });
        }
    }

    $scope.createNewCustomerProfile = function() {

        if($scope.customerProfileTemplateType === "" || $scope.customerProfileTemplateType === undefined) {
            toastr.error("Template type is required")
        }
        else if($scope.customerProfileTemplateName === "" || $scope.customerProfileTemplateName === undefined) {
            toastr.error("Template name is required")
        }
        else if($scope.customerProfileName === "" || $scope.customerProfileName === undefined) {
            toastr.error("CustomerProfile name is required")
        }
        else {
            $http.post('/customerProfiles/create/new', {"customerProfileTemplateType":$scope.customerProfileTemplateType,
                "customerProfileTemplateName":$scope.customerProfileTemplateName,
                "customerProfileName":$scope.customerProfileName })
                .success(function(response) {
                    console.log("Created new customerProfile response:", response);
                    $scope.customerProfileId = response.CustomerProfile._id;
                    console.log("customer profile id:", $scope.customerProfileId );
                });
        }
    }

    $scope.closeCustomerProfile = function() {
        window.location = "/customerProfileTemplates/show/all";
    }

    var generateAttributeList = function() {

        $scope.formFields.forEach(function (field) {
            if (!(field.Name === 'Image' || field.Name === 'Table')) {
                docAttributeList.push({
                    attributeId: field.id,
                    attributeName: field.Name,
                    attributeValue: field.Value,
                    attributeType: $scope.getFieldSetting(field, 'Field Type').Value,
                    attributeFormula: $scope.getFieldSetting(field, 'Formula').Value,
                    systemReferencedCollection: $scope.getFieldSetting(field, 'System Referenced').Value,
                    systemReferencedAttributeName: $scope.getFieldSetting(field, 'System Referenced Attributes').Value,
                    isAttributeMandatory: $scope.getFieldSetting(field, 'Mandatory').Value,
                    isAttributeCustomerVisible: $scope.getFieldSetting(field, 'Customer Visible').Value,
                    repeatOnEveryPage: false
                })
            }
        })
    }
});


function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

function validateOnKeyPress(el, evt, field) {
    console.log("Field type:", getFieldSetting(field, 'Field Type').Value);
    var fieldType = getFieldSetting(field, 'Field Type').Value;

    if(fieldType === 'Numeric') {
    } else if(fieldType === 'String') {
    } else if (fieldType === 'Alpha Numeric') {
    }
}

function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
        return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}

function validateStringOnkeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;

    if ( charCode > 31 && charCode === 31 || (charCode >= 65 && charCode <= 90) && (charCode >= 96 && charCode <= 122)) {
        return true;
    } else {
        return false;
    }
}

function validateAlphaNumericOnKeyPress(el, evt) {
    return (validateFloatKeyPress(el, evt) || validateStringOnkeyPress(el, evt));
}

$(function() {
    // // Code here
    var dh = $(document).height();
    $('#sidebar-tab-content').height(dh - 115);
    $('#main-content').height(dh - 10);

});