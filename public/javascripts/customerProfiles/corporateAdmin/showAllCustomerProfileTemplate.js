var relatasApp = angular.module('relatasApp', ["ngLodash"]).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        liuDetails:function(user){
            this.liuData = user;
        },
        getUserId:function(){
            return this.userId
        },
        setUserEmailId:function(emailId){
            this.userEmailId = emailId;
        },
        setTeamMembers:function(members){
            this.team = members;
        },
        setTargetForFy:function(value){
            this.targetForFy = value;
        },
        getTeamMembers:function(){
            return this.team;
        },
        getLiuUserEmailId:function(){
            return this.userEmailId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setTown:function(geoLocationTown){
            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        }
    };
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

var timezone;
relatasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        share.companyDetails.currency.forEach(function (el) {
            share.currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                share.primaryCurrency = el.symbol;
            }
        });
    });
    getErrorMessages($rootScope,$http,"opportunity");
});

relatasApp.controller("showCustomerProfileTemplatesController", function($scope, $http){
    $scope.headers = ["S.No.", "CustomerProfile Template Type", "CustomerProfile Template Name", "Created Date", "Is Deactivated?", "Is Default Template?"];
    $scope.docHeaders = ["S.No.", "CustomerProfile Template Type", "CustomerProfile Template Name", "Created Date", "CustomerProfile Name"];

    $http.get('/customerProfileTemplates/get/all/data')
        .success(function (response) {
            $scope.allCustomerProfileTemplates = response.Data;
            $scope.allCustomerProfileTemplates.forEach(function(el) {
                el.dateFormatted = moment(el.createdDate).format("DD MMM YYYY");
            })
        })

    $http.get('/customerProfiles/get/all/data')
        .success(function (response) {
            $scope.allCustomerProfiles = response.Data;
            $scope.allCustomerProfiles.forEach(function(el) {
                el.dateFormatted = moment(el.createdDate).format("DD MMM YYYY");
            })
        })

    $scope.showSelectedCustomerProfileTemplate = function(index){
        var templateType = $scope.allCustomerProfileTemplates[index].customerProfileTemplateType
        var templateName = $scope.allCustomerProfileTemplates[index].customerProfileTemplateName
        var id = $scope.allCustomerProfileTemplates[index]._id;
        var isActivated = $scope.allCustomerProfileTemplates[index].isTemplateDeactivated;
        var isVersionControlled = $scope.allCustomerProfileTemplates[index].isTemplateVersionControlled === "true" ? true : false;

        window.location = "/customerProfileTemplates/create/new?id="+id+"&templateType="+templateType+
                                                                        "&templateName="+templateName+
                                                                        "&isActivated="+isActivated+
                                                                        "&isVersionControlled="+isVersionControlled;
    }

    $scope.showSelectedCustomerProfile = function(index){
        var templateType = $scope.allCustomerProfiles[index].customerProfileTemplateType
        var templateName = $scope.allCustomerProfiles[index].customerProfileTemplateName
        var templateId = $scope.allCustomerProfiles[index]._id;
        var customerProfileName = $scope.allCustomerProfiles[index].customerProfileName;

        // window.location = "/customerProfile/create/new?id="+id+"&templateType="+templateType+"&templateName="+templateName+"&customerProfileName="+customerProfileName;
        window.location = "/customerProfile/create/new?customerProfileId="+templateId+"&customerProfileTemplateType="+templateType+"&customerProfileTemplateName="+templateName+"&customerProfileName="+customerProfileName;
    }
});

