
var relatasApp = angular.module('relatasApp', ["ngLodash"]).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
}]);

var timezone;
var guid = 0;
var colId = 0;
var attributeList = [];
// TBD update in html file


// *************************************************************************
// Function:        Relatas Application Service - Share
// Functionality :  getIsOk, setUserId, liuDetails
//
//
//
// *************************************************************************
relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        liuDetails:function(user){
            this.liuData = user;
        },
        getUserId:function(){
            return this.userId
        },
        setUserEmailId:function(emailId){
            this.userEmailId = emailId;
        },
        setTeamMembers:function(members){
            this.team = members;
        },
        setTargetForFy:function(value){
            this.targetForFy = value;
        },
        getTeamMembers:function(){
            return this.team;
        },
        getLiuUserEmailId:function(){
            return this.userEmailId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setTown:function(geoLocationTown){
            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        }
    };
});

// *************************************************************************
// Function:        Relatas Application Service - searchService
// Functionality :  $http.post('/search/user/contacts', { "contactName" : keywords}
//
//
//
// *************************************************************************
relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);


// *************************************************************************
// Function:        Relatas Application Controller - header_controller
// Functionality :  Updates the scope variable for
//
//
//
// *************************************************************************
relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

// *************************************************************************
// Function:        Relatas Application Controller - logedinUser
// Functionality :  updates the data in share service
//
//
//
// *************************************************************************
relatasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        share.companyDetails.currency.forEach(function (el) {
            share.currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                share.primaryCurrency = el.symbol;
            }
        });
    });
    getErrorMessages($rootScope,$http,"opportunity");
});

relatasApp.controller('customerProfileTemplatesController', function($scope,$http) {

    $scope.formFields = [];
    $scope.current_field = {};
    $scope.tableList = {};
    $scope.formulaMode = false;
    $scope.formulaString = '';
    $scope.customerProfileTemplateType = decodeURI(getParams(window.location.href).templateType);
    $scope.customerProfileTemplateName = decodeURI(getParams(window.location.href).templateName);
    $scope.customerProfileTemplateId = getParams(window.location.href).id;

    console.log("Passing Customer Profile Template ID as ", $scope.customerProfileTemplateId );
    $http.post('/customerProfileTemplates/getById', {"customerProfileTemplateId": $scope.customerProfileTemplateId})
        .success(function (response) {
            console.log("Response for getbyId", response);
            if(response.Data) {
                var attrList = response.Data;
                attrList.customerProfileTemplateElementList.forEach(function(stringEle) {
                    $scope.formFields.push(JSON.parse(stringEle));
                })

                guid = attrList.customerProfileTemplateUiId || guid;

            }
        })


    $scope.dragElements = [{
        'Name': "Textbox Title",
        'Type': "text",
        'Icon': "fa fa-text-height",
        'Settings': [{
            'Name': 'Field Label',
            'Value': 'Textbox Title',
            'Type': 'text'
        }, {
            'Name': 'Placeholder',
            'Value': 'Enter Text',
            'Type': 'text'
        }, {
            'Name': 'Field Type',
            'Value': 'String',
            'Type': 'dropdown',
            'PossibleValue': ['Auto Complete',
                'Alpha Numeric',
                'Boolean',
                'Formula',
                'Numeric',
                'String',
                'System Generated',
                'System Referenced',
            ]
        }, {
            'Name': 'Formula',
            'Value': '',
            'Type': 'formula',
            'Visibility': false
        }, {
            'Name': 'System Referenced',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': ['Opportunity']
        }, {
            'Name': 'System Referenced Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': ['Opportunity Name',
                'Product Type',
                'Source',
                'Region',
                'Vertical',
                'Solution',
                'Business Unit',
                'Amount',
                'Margin']
        },{
            'Name': 'Auto Complete',
            'Value': '',
            'Type': 'dropdown_autocomplete',
            'Visibility': false,
            'PossibleValue': ['Customer Name', 'Cutomer Address']
        }, {
            'Name': 'Max Input Length',
            'Value': '50',
            'Type': 'text'
        }, {
            'Name': 'Column Span',
            'Value': '1',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']
        }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
                'Name': 'Mandatory',
                'Value': false
            }, {
                'Name': 'Customer Visible',
                'Value': false
            }]
        }]
    }, {
        'Name': "Date",
        'Value':'',
        'Type': "date",
        'Icon': "fa fa-calendar-check-o",
        'Settings': [{
            'Name': 'Field Label',
            'Value': 'Field Label',
            'Type': 'text'
        }, {
            'Name': 'Placeholder',
            'Value': '',
            'Type': 'text'
        }, {
            'Name': 'Column Span',
            'Value': '1',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']
        }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
                'Name': 'Mandatory',
                'Value': false
            }, {
                'Name': 'Customer Visible',
                'Value': false
            }]
        }]
    }, {
        'Name': "Paragraph Text",
        'Value':'',
        "Type": "textarea",
        'Icon': "fa fa-square-o",
        'Settings': [{
            'Name': 'Field Label',
            'Value': '',
            'Type': 'text'
        }, {
            'Name': 'Placeholder',
            'Value': '',
            'Type': 'text'
        },{
            'Name': 'Field Type',
            'Value': 'String',
            'Type': 'dropdown',
            'PossibleValue': ['Auto Complete',
                'Alpha Numeric',
                'Boolean',
                'Formula',
                'Numeric',
                'String',
                'System Generated',
                'System Referenced']
        }, {
            'Name': 'Formula',
            'Value': '',
            'Type': 'formula',
            'Visibility': false
        }, {
            'Name': 'System Referenced Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': ['Opportunity Name',
                'Product Type',
                'Source',
                'Region',
                'Vertical',
                'Solution',
                'Business Unit',
                'Amount',
                'Margin']
        },{
            'Name': 'System Referenced',
            'Value': 'Opportunity',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': ['Opportunity']
        }, {
            'Name': 'Auto Complete',
            'Value': '',
            'Type': 'dropdown_autocomplete',
            'Visibility': false,
            'PossibleValue': ['Customer Name', 'Cutomer Address']
        }, {
            'Name': 'Column Span',
            'Value': '2',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']
        }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
                'Name': 'Mandatory',
                'Value': false
            }, {
                'Name': 'Customer Visible',
                'Value': false
            }]
        }
        ]
    }, {
        'Name': "Image",
        "Type": "image",
        'Icon': "fa fa-picture-o",
        'Settings':[{
            'Name': 'Upload Image',
            'Value': '',
            'Type': 'file'
        },{
            'Name': 'Column Span',
            'Value': '1',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']
        },{
            'Name': 'Relative_Path',
            'Value': '',
            'Type': 'url'
        },{
            'Name': 'Absolute_Path',
            'Value': '',
            'Type': 'url'
        }]
    }, {
        'Name': "Table",
        "Type": "table",
        'Icon': "fa fa-table",
        'Columns':[],
        'Settings':[{
            'Name': 'Number of columns',
            'Value': '3',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3','4','5','6','7','8', '9', '10']
        }, {
            'Name': 'Column Span',
            'Value': '3',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']
        }]
    }, {
        'Name': "Header",
        "Type": "header",
        'Icon': "fa fa-header",
        'Settings':[{
            'Name': 'Field Label',
            'Value': 'Header',
            'Type': 'text'
        }, {
            'Name': 'Font Size',
            'Value': 'h3',
            'Type': 'dropdown',
            'PossibleValue': ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']
        }, {
            'Name': 'Column Span',
            'Value': '1',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']
        }]
    }];

    var createNewField = function() {
        return {
            'id': ++guid,
            'Name': '',
            'Settings': [],
            'Active': true,
            'Hover': false,
            'Style':'',
        };
    }

    //Remove This...
    var generateUniqImageName = function(imageField) {
        var relativeImagePath, absoluteImagePath;

        relativeImagePath = $scope.getFieldSetting(imageField, 'Relative_Path');
        relativeImagePath.Value = $scope.customerProfileTemplateId+"_"+imageField.id+".jpg";

        absoluteImagePath = companyImgUrl+$scope.getFieldSetting(imageField, 'Relative_Path').Value;
        $scope.getFieldSetting(imageField, 'Absolute_Path').Value = absoluteImagePath;
    }

    var saveImages = function() {

        imagesCache.forEach(function(imageForm) {
            $scope.formFields.forEach(function(field){
                if(field.Name === 'Image') {
                    var imageName = $scope.getFieldSetting(field, 'Image Url').Value;
                    imageForm.append('imageName',field,  imageName)
                }
            });
        })
    }

    $scope.getFieldSetting = function(field, settingName) {
        var result = {};
        var settings = field.Settings;
        $.each(settings, function(index, set) {
            if (set.Name == settingName) {
                result = set;
                return;
            }
        });
        if (!Object.keys(result).length) {
            //Continue to search settings in the checkbox zone
            $.each(settings[settings.length - 1].Options, function(index, set) {
                if (set.Name == settingName) {
                    result = set;
                    return;
                }
            });
        }
        return result;
    }

    $scope.changeFieldSetting = function(Value, SettingName) {
        switch (SettingName) {
            case 'Field Label':
            case 'Short Label':
            case 'Internal Name':
                $scope.current_field.Name = Value;
                $scope.current_field.Settings[0].Value = $scope.current_field.Name;
                // $scope.current_field.Settings[1].Value = $scope.current_field.Name;
                // $scope.current_field.Settings[2].Value = 'x' + $scope.current_field.Name.replace(/\s/g, '_');
                break;
            case 'Column Name':
                $scope.current_field.Name = Value;

            default:
                break;
        }
    }

    $scope.removeElement = function(idx){
        if($scope.formFields[idx].Active) {
            $('#addFieldTab_lnk').tab('show');
            $scope.current_field = {};
        }
        $scope.formFields.splice(idx, 1);
    };

    $scope.addElement = function(ele, idx) {
        $scope.current_field.Active = false;

        $scope.current_field = createNewField();
        angular.merge($scope.current_field, ele);

        if (typeof idx == 'undefined') {
            $scope.formFields.push($scope.current_field);
        } else {
            $scope.formFields.splice(idx, 0, $scope.current_field);
            $('#fieldSettingTab_lnk').tab('show');
        }

        switch($scope.current_field.Name) {
            case 'Table':
                generateColumnsForTable($scope.current_field, $scope.getFieldSetting($scope.current_field, 'Number of columns').Value);
                break;
            case 'Image':
                generateUniqImageName($scope.current_field);
                break;

        }

    };

    $scope.activeField = function(f) {
        $scope.current_field.Active = false;
        $scope.current_field = f;
        f.Active = true;
        $('#fieldSettingTab_lnk').tab('show');
    };

    $scope.activeFieldForFormula = function(f, index, event) {
        event.stopPropagation();

        var formulaString;

        if(f.Name === 'Table') {

            formulaString = "t" + f.id + "_" + index + '+';
            $scope.getFieldSetting($scope.current_field, 'Formula').Value +=  formulaString;

        } else if(f.Name !== 'Image' && f.Name !== 'Header' && f.Name !== 'Date') {

            formulaString = "f" + f.id + '+';
            $scope.getFieldSetting($scope.current_field, 'Formula').Value +=  formulaString;
        }

    }

    $scope.formulaFocus = function(event) {
        $scope.formulaMode = true;
    }

    $scope.formulaBlur = function() {
        // $scope.formulaMode = false;
    }

    $scope.columnData = {
        'Name': "",
        'Type': "column",
        'RowValues':[],
        'Settings':[{
            'Name': 'Column Name',
            'Value': '',
            'Type': 'text'
        },{
            'Name': 'Field Type',
            'Value': 'String',
            'Type': 'dropdown',
            'PossibleValue': ['Boolean',
                'Formula',
                'Numeric',
                'Alpha Numeric',
                'String',
                'System Referenced',
                'Auto Complete']
        }, {
            'Name': 'Formula',
            'Value': '',
            'Type': 'formula',
            'Visibility': false
        }, {
            'Name': 'System Referenced',
            'Value': 'Opportunity',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': ['Opportunity']
        }, {
            'Name': 'System Referenced Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': ['Opportunity Name',
                'Product Type',
                'Source',
                'Region',
                'Vertical',
                'Solution',
                'Business Unit',
                'Amount',
                'Margin']
        },{
            'Name': 'Auto Complete',
            'Value': '',
            'Type': 'dropdown_autocomplete',
            'Visibility': false,
            'PossibleValue': ['Customer Name', 'Cutomer Address']
        }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
                'Name': 'Mandatory',
                'Value': false
            }, {
                'Name': 'Customer Visible',
                'Value': false
            }]
        }]
    }

    $scope.getColumnIndex = function(table, colIndex, event) {
        event.stopPropagation();
        $scope.activeField(table.Columns[colIndex]);
    }

    $scope.addColumn = function(ele, idx, table) {
        var tableId = table.id;

        $scope.current_field.Active = false;
        ele.Name  =  'col_'+idx;

        $scope.current_field = createNewColumn();
        //Merge setting from template object
        angular.merge($scope.current_field, ele);

        if (typeof idx == 'undefined') {
            table.Columns.push($scope.current_field);
        } else {
            table.Columns.splice(idx, 0, $scope.current_field);
            $('#fieldSettingTab_lnk').tab('show');
        }
    };

    $scope.getElementWidth = function(colSpan) {
        switch(Number(colSpan)) {
            case 1: return 'col-md-3';
            case 2: return 'col-md-6';
            case 3: return 'col-md-9';
            case 4: return 'col-md-12';
            default: return 'col-md-6';
        }
    }

    $scope.getFontWeight = function(size) {
        if(size === 'h1') return 'size-h1';
        if(size === 'h2') return 'size-h2';
        if(size === 'h3') return 'size-h3';
        if(size === 'h4') return 'size-h4';
        if(size === 'h5') return 'size-h5';
        if(size === 'h6') return 'size-h6';
    }

    $scope.getFile = function () {
        var reader = new FileReader();
        $scope.progress = 0;
        reader.readAsDataUrl($scope.file, $scope)
            .then(function(result) {
                $scope.imageSrc = result;
            });
    };

    $scope.checkVisibility = function(fieldType) {

        if(fieldType.Name === 'Number of columns') {
            generateColumnsForTable($scope.current_field, fieldType.Value);
        }

        switch(fieldType.Value) {

            case 'Formula':
                $scope.getFieldSetting($scope.current_field, 'Formula').Visibility = true;
                $scope.getFieldSetting($scope.current_field, 'System Referenced').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'Auto Complete').Visibility = false;
                break;

            case 'System Referenced':
                $scope.getFieldSetting($scope.current_field, 'Formula').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'Formula').Value = "";
                $scope.getFieldSetting($scope.current_field, 'System Referenced').Visibility = true;
                $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = true;
                $scope.getFieldSetting($scope.current_field, 'Auto Complete').Visibility = false;
                break;

            case 'Auto Complete':
                $scope.getFieldSetting($scope.current_field, 'Formula').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'Formula').Value = "";
                $scope.getFieldSetting($scope.current_field, 'System Referenced').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'Auto Complete').Visibility = true;
                break;

            case 'Boolean':
            case 'Numeric':
            case 'Alpha Numeric':
            case 'String':
                $scope.getFieldSetting($scope.current_field, 'Formula').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'Formula').Value = "";
                $scope.getFieldSetting($scope.current_field, 'System Referenced').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'Auto Complete').Visibility = false;

        }

        return true;
    }

    $scope.saveCustomerProfileTemplate = function () {
        $scope.formulaMode = false;
        var tempElementList = [];

        $scope.formFields.forEach(function(element) {
            element.Style = $("#"+element.id).attr("style") || "";
            tempElementList.push(JSON.stringify(element));
        });

        if($scope.customerProfileTemplateType === "" || $scope.customerProfileTemplateType === undefined) {
            toastr.error("Template type is required")
        }
        else if($scope.customerProfileTemplateName === "" || $scope.customerProfileTemplateName === undefined) {
            toastr.error("Template name is required")
        } else {
            generateAttributeList();
            console.log(" Template Id from front end", $scope.customerProfileTemplateId)
            console.log(" Template Id from front end", getParams(window.location.href).id)

            $http.post('/customerProfileTemplates/updateAll', {
                            "customerProfileTemplateId": $scope.customerProfileTemplateId,
                            "customerProfileTemplateElementList":tempElementList,
                            "isCustomerProfileTemplateDeactivated":false,
                            "customerProfileTemplateUiId":guid,
                            "customerProfileTemplateAttrList": attributeList
            }).success(function(response) {
                if(response.SuccessCode == 1) {
                    toastr.success("CustomerProfile template saved successfully")
                } else {
                    toastr.success("Error in saving customerProfile template")
                }
                // window.location = "/customerProfileTemplates/show/all";
            });
        }
    }

    $scope.createNewCustomerProfile = function() {
        var templateType = $scope.customerProfileTemplateType;
        var templateName = $scope.customerProfileTemplateName;
        var templateId = $scope.customerProfileTemplateId;

        window.location = "/customerProfile/create/new?customerProfileTemplateId="+templateId+"&customerProfileTemplateType="+templateType+"&customerProfileTemplateName="+templateName;
    }

    $scope.createNewCustomerProfileTemplate = function() {

        console.log("The value for customerProfileTemplateType  is", $scope.customerProfileTemplateType);
        console.log("The value for customerProfileTemplateName  is", $scope.customerProfileTemplateName);

        if($scope.customerProfileTemplateType === "" || $scope.customerProfileTemplateType === undefined) {
            toastr.error("Customer Profile Template Type is required")
        }
        else if($scope.customerProfileTemplateName === "" || $scope.customerProfileTemplateName === undefined) {
            toastr.error("Customer Profile Template Name is required")
        } else {

            $http.post('/customerProfileTemplate/create/new', {"customerProfileTemplateType":$scope.customerProfileTemplateType,
                "customerProfileTemplateName":$scope.customerProfileTemplateName})
                .success(function(response) {
                    $scope.customerProfileTemplateId = response.Template._id;
                });
        }

    }

    $scope.closeCustomerProfileTemplate = function() {
        window.location = "/customerProfileTemplates/show/all";
    }

    $scope.activateCustomerProfileTemplate = function () {
        var deactivated = false;
        $http.post('/customerProfileTemplates/updateIsCustomerProfileTemplateDeactivated', {"customerProfileTemplateType":$scope.templateType,
            "customerProfileTemplateId":$scope.customerProfileTemplateId,
            "isCustomerProfileTemplateDeactivated":deactivated
        })
        window.location = "/customerProfileTemplates/show/all";
    }

    $scope.deactivateCustomerProfileTemplate = function () {
        var deactivated = true;

        $http.post('/customerProfileTemplates/updateIsCustomerProfileTemplateDeactivated', {"customerProfileTemplateType":$scope.templateType,
            "customerProfileTemplateId":$scope.customerProfileTemplateId,
            "isCustomerProfileTemplateDeactivated":deactivated
        })
        window.location = "/customerProfileTemplates/show/all";
    }

    $scope.updateCustomerProfileTemplateAsDefault = function () {
        var isDefault = true;
        $http.post('/customerProfileTemplates/updateCustomerProfileTemplateAsDefault', {
            "customerProfileTemplateType":$scope.customerProfileTemplateType,
            "customerProfileTemplateId":$scope.customerProfileTemplateId,
            "isDefault":isDefault
        })
        window.location = "/customerProfileTemplates/show/all";
    }

    $scope.setCustomerProfileTemplateAsVersionControlled = function () {
        var isVersionControlled = true;
        $http.post('/customerProfileTemplates/setCustomerProfileTemplateAsVersionControlled', {
            "customerProfileTemplateType":$scope.customerProfileTemplateType,
            "customerProfileTemplateId":$scope.customerProfileTemplateId,
            "isTemplateVersionControlled":isVersionControlled
        })

        window.location = "/customerProfileTemplates/show/all";
    }

    $scope.resetCustomerProfileTemplateAsVersionControlled = function () {
        var isVersionControlled = false;
        $http.post('/customerProfileTemplates/setCustomerProfileTemplateAsVersionControlled', {
            "customerProfileTemplateType":$scope.customerProfileTemplateType,
            "customerProfileTemplateId":$scope.customerProfileTemplateId,
            "isTemplateVersionControlled":isVersionControlled
        })
        window.location = "/customerProfileTemplates/show/all";
    }

    var generateAttributeList = function() {

        $scope.formFields.forEach(function(field) {
            if( !(field.Name === 'Image' || field.Name === 'Table')) {
                attributeList.push({
                    attributeId:field.id,
                    attributeName:field.Name,
                    attributeType:$scope.getFieldSetting(field, 'Field Type').Value,
                    attributeFormula:$scope.getFieldSetting(field, 'Formula').Value,
                    systemReferencedCollection:$scope.getFieldSetting(field, 'System Referenced').Value,
                    systemReferencedAttributeName:$scope.getFieldSetting(field, 'System Referenced Attributes').Value,
                    isAttributeMandatory:$scope.getFieldSetting(field, 'Mandatory').Value,
                    isAttributeCustomerVisible:$scope.getFieldSetting(field, 'Customer Visible').Value,
                    repeatOnEveryPage:false
                })
            }
        })

        console.log(attributeList);
    }

    var getRandomInt = function(number) {
        return Math.floor(Math.random() * Math.floor(number));
    }

    var createNewColumn = function() {
        return {
            'id': ++colId,
            'Name': '',
            'Settings': [],
            'Active' : true
        }
    };

    var getTableHeaders = function(table, numberOfColumns) {

        if(numberOfColumns < table.Columns.length) {
            table.Columns.splice(numberOfColumns, table.Columns.length-numberOfColumns);

        } else if(numberOfColumns > table.Columns.length){
            for (var i=table.Columns.length; i<numberOfColumns; i++) {
                $scope.addColumn($scope.columnData, i, table);
            }
        }

    }

    var generateColumnsForTable = function(table, numberOfColumns) {
        getTableHeaders(table, numberOfColumns);
    }

});

//Sachin Do I need to rename this?
relatasApp.directive('elementDraggable', ['$customerProfile', function($document) {
    return {
        link: function(scope, element, attr) {
            element.on('dragstart', function(event) {

                event.originalEvent.dataTransfer.setData('templateIdx', $(element).data('index'));
            });
        }
    };
}]);
//Sachin Do I need to rename this?

relatasApp.directive('elementDrop', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {

            element.on('dragover', function(event) {
                event.preventDefault();
            });

            $('.drop').on('dragenter', function(event) {
                event.preventDefault();
            })
            element.on('drop', function(event) {
                event.stopPropagation();
                var self = $(this);
                scope.$apply(function() {
                    var idx = event.originalEvent.dataTransfer.getData('templateIdx');
                    var insertIdx = self.data('index')
                    scope.addElement(scope.dragElements[idx], insertIdx);
                });
            });
        }
    };
}]);

relatasApp.directive("fileinput", [function() {
    return {
        scope: {
            fileinput: "=",
            filepreview: "="
        },
        link: function(scope, element, attributes) {

            element.bind("change", function(changeEvent) {
                scope.fileinput = changeEvent.target.files[0];
                var reader = new FileReader();
                reader.onload = function(loadEvent) {
                    scope.hideInput = "hide-input"
                    upload(scope.fileinput, scope.filepreview, scope);
                    scope.$apply(function() {
                        // scope.filepreview = loadEvent.target.result;
                        upload(scope.fileinput, scope.filepreview, scope.filepreview, scope)
                    });
                }
                reader.readAsDataURL(scope.fileinput);
            });
        }
    }
}]);

var upload = function(file, fileName, scope){
    var formData = new FormData();
    var xhr = new XMLHttpRequest();
    var imageName = file.name;
    var extension = imageName.split('.')[imageName.split('.').length-1];

    formData.append('companypic', file, fileName);

    xhr.open('POST', '/profile/update/companyLogoForCustomerProfileTemplate');
    xhr.onload = function () {
        var response = JSON.parse(xhr.response)
        if(response.SuccessCode){
        }
        else{
            toastr.error(response.Message)
        }
    };
    xhr.send(formData);
};

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

$(function() {
    // // Code here
    var dh = $(document).height();
    $('#sidebar-tab-content').height(dh - 115);
    $('#main-content').height(dh - 10);

    $('.drag').draggable({
        cancel:null,
        containment:"#main-content",
        scroll: false
    });
    $('.resize').resizable();

    $('.drag').click(function(){
        $(this).focus();
    });

    $("body").on('DOMNodeInserted', '.drag', function () {
        $(this).draggable({cancel:null});
        $(this).click(function() {
            $(this).focus();
        });

    });

    // $("#dragdiv").draggable({
    //     cancel: "",
    //     start: function() {
    //         $('textarea').focus();
    //     },
    //     stop:  function() {
    //         $('textarea').focus();
    //     }
    // }).resizable();

    var readUrl = function(input) {
        if(input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#company_logo').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
});