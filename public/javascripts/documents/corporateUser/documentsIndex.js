var relatasApp = angular.module('relatasApp', ["ngLodash", "textAngular"]).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

var documentId = getParams(window.location.href).documentId;
var opportunityId = getParams(window.location.href).opportunityId;

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        liuDetails:function(user){
            this.liuData = user;
        },
        getUserId:function(){
            return this.userId
        },
        setUserEmailId:function(emailId){
            this.userEmailId = emailId;
        },
        setTeamMembers:function(members){
            this.team = members;
        },
        setTargetForFy:function(value){
            this.targetForFy = value;
        },
        getTeamMembers:function(){
            return this.team;
        },
        getLiuUserEmailId:function(){
            return this.userEmailId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setTown:function(geoLocationTown){
            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        }
    };
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

var timezone;
var PAGE_BREAK_PIXEL_VALUE = 1283;
var documentIncreasedHeight = 0;

relatasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        share.liuData = response.Data;
    });
    getDomainName(share, $http);
});

relatasApp.controller("documentHeader", function($scope, $http, share, $timeout) {

    $scope.versions = {
        documentVersions: [],
        currentVersion: '1',
    }

    $scope.documentStages = ['Draft', 'Sent', 'Accepted'];

    $scope.saveDocument = function() {
        if($scope.documentHeader && $scope.documentHeader.cloneDocument) {
            cloneExistingDocument();
        } else {
            share.saveDocument();
        }
    }

    function cloneExistingDocument() {

        var docNames = share.getAllDocumentsNames(); 

        if(docNames.indexOf($scope.documentName) >= 0) {
            toastr.error("Document name already exists");
        } else {
            $http.post('/document/clone', {documentId: $scope.documentId, documentName: $scope.documentName}) 
            .success(function(response) {
                if(response.SuccessCode) {
                    toastr.success("Document Cloned Successfully");
                } else {
                    toastr.error("Error in Cloning Document");
                }

                $scope.documentHeader.cloneDocument = false;
                share.getAllCreatedDocuments();                                                      
                $scope.closeDocument();
            })
        }
    }

    $scope.openNewDocument = function() {
        mixpanelTracker("Documents>open doc creator ");
        share.sideBarVisibility(false);
        share.showFullPageView(true);

        $scope.showDocumentHeader = true;
        share.showDocumentCreateWindow(true);
        share.showAllDocumentsWindow(false);
        share.setDocumentIdHeader(null);

        $scope.docTemplateType = "";
        $scope.docTemplateName = "";
        $scope.documentName = "";
    }

    $scope.cloneDocument = function() {
        $scope.documentHeader = {};
        $scope.documentHeader.cloneDocument = true;
    }

    $scope.getDocumentTemplateTypes = function () {
        $scope.documentTemplateTypes = [];
        $scope.showTemplateTypeSuggestion = true;

        $http.get('/documentTemplates/getDocumentTemplateTypes')
        .success(function (response) {
            if (response.DataTypes ) {
                response.DataTypes.forEach(function(data) {
                    $scope.documentTemplateTypes.push(data._id);
                    $scope.documentTemplateType = data._id;
                })
            }
        });
    }

    $scope.getTemplateNames = function(templateType) {
        // var templateType = $scope.documentTemplateTypes[selectedIndex];
        $scope.showTemplateTypeSuggestion = false;

        $scope.docTemplateType = templateType;
        $scope.docTemplateName = "";

        $scope.documentTemplateNames = [];
        $http.post('/documentTemplates/getAllDocumentTemplateNamesByType', {"documentTemplateType": templateType})
            .success(function (response) {
                $scope.showVersionButton = false;
                if (response.TemplateNames ) {
                    response.TemplateNames.forEach(function(data) {
                        $scope.documentTemplateNames.push(data.documentTemplateName);
                        
                    })
                }
            });
    }

    $scope.getDocument = function(templateName) {
        // var templateName = $scope.documentTemplateNames[selectedIndex];
        $scope.showTemplateNameSuggestion = false;
        $scope.docTemplateName = templateName;

        $http.post('/document/template/get/by/type/name', {"docTemplateType": $scope.docTemplateType, 
                                                            "docTemplateName": $scope.docTemplateName})
            .success(function (response) {
                if(response.data.length > 0) {
                    share.setDocumentData(response.data[0]);
                }
            })
    }
    
    $scope.closeDocument = function() {
        share.showAllDocumentsWindow(true);
        share.showDocumentCreateWindow(false);
        share.showDocumentHeader(false);
        
        share.sideBarVisibility(true);
        share.showFullPageView(false);
    }

    $scope.updateCurrentDocumentStage = function(selectedStage) {
        $scope.currentDocumentStage = selectedStage;
        $scope.showDocumentStageFilter = false;
    }

    $scope.getDocumentByVersion = function () {
        if ($scope.docTemplateType === "" || $scope.docTemplateType === undefined) {
            toastr.error("Template type is required")
        }
        else if ($scope.docTemplateName === "" || $scope.docTemplateName === undefined) {
            toastr.error("Template name is required")
        }
        else if ($scope.documentName === "" || $scope.documentName === undefined) {
            toastr.error("Document name is required")
        }
        else {

            $http.post('/document/getDocumentByVersion', {
                "documentTemplateType": $scope.docTemplateType,
                "documentTemplateName": $scope.docTemplateName,
                "documentName": $scope.documentName,
                "documentVersion": Number($scope.versions.currentVersion)
            })
                .success(function (response) {
                    $scope.showVersionButton = false;
                    if (response.Data && response.Data._id) {
                        share.updateDocumentHeader(response.Data);

                        share.buildDocumentUi(response.Data);
                    } else {
                    }
                });
        }
    }

    share.initializeVersion = function() {
        $scope.versions = {
            documentVersions: [],
            currentVersion: '1',
        }
    }

    share.populateHeaderForTemplate = function(document) {
        $scope.showDocumentHeader = true;
        $scope.docTemplateType = document.documentTemplateType;
        $scope.docTemplateName = document.documentTemplateName;
        $scope.documentName = document.documentName;
        $scope.documentId = document._id;
    }

    share.showDocumentHeader = function(visibility) {
        $scope.showDocumentHeader = visibility;
    }

    share.updateDocumentVersion = function(currentVersion) {
        $scope.showVersionButton = true;
        $scope.versions.currentVersion = String(currentVersion);
        generateVersions();
    }

    share.updateDocumentHeader = function(document) {
        $scope.documentCurrentVerion = document.currentDocumentVersion;
        $scope.currentDocumentStage = document.documentStage;
        $scope.documentVersionControlled = document.isTemplateVersionControlled;
        $scope.showVersionButton = true;
        $scope.versions.currentVersion = String(document.documentVersion);

        if( (document.currentDocumentVersion != document.documentVersion)) {
            $scope.showVersionButton = false;
            $scope.disableEdit = true;

        } else {
            $scope.showVersionButton = true;
            $scope.disableEdit = false;
        }

        if(document.documentStage == 'Accepted') {
            $scope.disableEdit = true;
            $scope.showVersionButton = false;
        }

        generateVersions();
    }

    share.getDocumentHeaderDetails = function() {
        return {
            docTemplateType: $scope.docTemplateType,
            docTemplateName: $scope.docTemplateName,
            documentName: $scope.documentName,
            templateId: $scope.templateId,
            currentVersion: $scope.versions.currentVersion,
            documentStage: $scope.currentDocumentStage,
            incrementVersionClicked: $scope.incrementVersionClicked,
            isTemplateVersionControlled: $scope.documentVersionControlled
        }
    }

    $scope.createNewDocument = function () {
        mixpanelTracker("Documents>create new ");
        var docNames = share.getAllDocumentsNames(); 

        if(docNames && docNames.indexOf($scope.documentName) >= 0) {
            toastr.error("Document Name already exists");
        } else {
            share.createNewDocument();
        }
    }

    $scope.incrementDocumentVersion = function () {
        $scope.showVersionButton = false;
        $scope.versions.currentVersion = String(Number($scope.versions.currentVersion) + 1);
        $scope.versions.documentVersions.push($scope.versions.currentVersion);
        $scope.incrementVersionClicked = true;
    }

    share.setDocumentIdHeader = function(documentId) {
        $scope.documentId = documentId;
    }

    $scope.downloadDocument = function () {

        share.hideUiForPdfDownload(true);

        var totalHeight = $('#main-content')[0].scrollHeight;
        var totalWidth = $('#main-content')[0].scrollWidth;

        $("#main-content").css({"min-height": totalHeight});
        $("#main-content").css({"min-width": totalWidth});
        $("#main-content").css("box-shadow", "none");
        $("#main-content").css("border", "none");

        let doc = new jsPDF('p','pt','a4');

        doc.internal.scaleFactor = 1.55;
        doc.addHTML($('#main-content'),{
            pagesplit: true
        },function() {
            doc.save($scope.documentName+".pdf");
            $timeout(function(){
                share.hideUiForPdfDownload(false);
            });
        });

    }

    $scope.printDocument = function() {
        documentIncreasedHeight = 0;
        var totalHeight = $('#main-content')[0].scrollHeight;
        
        $("#main-content").css({"min-height": totalHeight});

        /* var numberOfPages = Math.floor($("#main-content").height()/PAGE_BREAK_PIXEL_VALUE);
        if(numberOfPages > 0) {
            share.adjustElementsForPrint(numberOfPages);
        } */

        var totalHeight = $('#main-content')[0].scrollHeight;
        $("#main-content").css({"overflow": "unset"});
        $("#main-content").css({"zoom": "85%"});

        share.hideUiForPdfDownload(true);

        $('table textarea').each(function () {
            var text = $(this).val();

            var div = $('<div>' + text + '</div>');
            $(div).attr("id", $(this).attr("id")+"-div");
            $(div).attr("style", $(this).attr("style"));

            $(this).after(div);
            $(this).hide();
        });

        setTimeout(function() {
            window.print();
            setTimeout(restoreUi, 200);
        })
    }

    var restoreUi = function() {
        $timeout(function(){
            share.hideUiForPdfDownload(false);
            $("#main-content").css({"zoom": "100%"});

            $('table textarea').each(function () {
                $(this).show();
                var divId =  $(this).attr("id")+"-div"
                $('#'+divId).hide();
                
            });
        });
    }

    /* var restoreUi = function() {
        $timeout(function(){
            share.hideUiForPdfDownload(false);
            $("#main-content").css({"zoom": "100%"});
        });
    } */

    var generateVersions = function () {
        $scope.versions.documentVersions.length = 0;

        for (i = 1; i <= $scope.documentCurrentVerion; i++)
            $scope.versions.documentVersions.push(String(i))
    }

    share.openNewDocument = function() {
        $scope.openNewDocument();
    }
});

relatasApp.controller("showAllDocumentsController", function($scope, $http, share, $timeout){
    $scope.filteredDocuments = [];
    $scope.showAllDocController = {};

    share.showAllDocumentsWindow = function(visibility) {
        $scope.showAllDocuments = visibility;
    }

    var getAllDocuments = function(refresh) {
        $scope.filteredDocuments = [];

        if(!refresh) 
            share.showAllDocumentsWindow(true);

        setDocumentsAllTableHeader();

        $http.get('/documents/get/all/data')
        .success(function (response) {
            $scope.showAllDocController.documentNames = _.map(response.Data, 'documentName');

            $scope.allDocuments = response.Data;
            $scope.allDocuments.forEach(function(el) {
                el.docCreatedDateFormatted = moment(el.documentCreatedDate).format("DD MMM YYYY");
                el.documentUpdatedDateFormatted = moment(el.documentUpdatedDate).format("DD MMM YYYY");
                if(el.documentVersion == el.currentDocumentVersion){
                    $scope.filteredDocuments.push(el);
                }
            })
        })
    }

    share.getAllDocumentsNames = function() {
        return $scope.showAllDocController.documentNames;
    }

    $scope.showSelectedDocument = function(document) {
        share.showFullPageView(true);
        share.sideBarVisibility(false);

        $scope.showAllDocuments = false;
        share.populateHeaderForTemplate(document);
        share.openDocument(document);
    }

    function setDocumentsAllTableHeader(){
        // $scope.docHeaders = ["S.No.", "Latest Version", "Document Template Type", "Document Template Name", "Document Name", "Created By", "Created Date", "Updated Date",];
        $scope.orderByField = 'documentUpdatedDate';
        $scope.reverseSort = true;

        $scope.docHeaders = [
            {
                name:"S.No.",
                styleWidth:"width:1%;",
                align:"text-right"
            },
            {
                name:"Version",
                styleWidth:"width:1%; cursor:pointer;",
                align:"text-right",
                // sortableName: "currentDocumentVersion"
            },
            {
                name:"Type",
                styleWidth:"width:16%; cursor:pointer;text-align:left;",
                sortableName: "documentTemplateType"
            },
            /* {
                name:"Document Template Name",
                styleWidth:"width:16%; cursor:pointer;",
                sortableName: "documentTemplateName"                
            }, */
            {
                name:"Name",
                styleWidth:"width:25%; cursor:pointer;text-align:left;",
                sortableName: "documentName"                
            },
            {
                name:"Created By",
                styleWidth:"width:18%; cursor:pointer; text-align: left;",
                sortableName: "documentCreatedBy"
            },
            {
                name:"Created Date",
                styleWidth:"width:9%; cursor:pointer;",
                sortableName: "documentCreatedDate"
            },
            {
                name:"Updated Date",
                styleWidth:"width:9%; cursor:pointer;",
                sortableName: "documentUpdatedDate"
            },
            {
                name:"Stage",
                styleWidth:"width:5%; cursor:pointer;",
                sortableName: "documentStage"
            }]
    }

    $scope.sortTable = function(header) {
        if(header.name !== "S.No.") {
            $scope.orderByField = header.sortableName;
            $scope.reverseSort = !$scope.reverseSort;
        }
    }

    share.getAllCreatedDocuments = function() {
        getAllDocuments(true);
    }

    $timeout(function() {
        if(documentId) {
            getDocumentById(documentId, $http, $scope);
    
        } else if(opportunityId) {
            share.openNewDocument();

        } else {
            getAllDocuments(false);
        }
    })
});

var formFieldValues = {};
var tableColumns = {};
var fieldDataType = {};
relatasApp.controller('createDocumentController', function($scope,$http,share, searchService) {

    documentsCommon($scope, share);
    getUniqSystemReferenceNumber($scope, $http);
    
    share.buildDocument = function (document) {

        $scope.versions = {
            documentVersions: [],
            currentVersion: '1',
        }

        $scope.templateId = document.documentTemplateId;
        $scope.docTemplateType = document.documentTemplateType;
        $scope.docTemplateName = document.documentTemplateName;
        $scope.documentName = document.documentName;
        $scope.documentId = document._id;
        $scope.formulaString = '';
        $scope.documentName = documentName;
        share.buildDocumentUi(document);
    }

    share.openDocument = function(document) {
        share.showDocumentCreateWindow(true);
        share.buildDocument(document);
    }

    share.showDocumentCreateWindow = function(visibility) {
        $scope.createDocument = visibility;
        $scope.documentId = null;
    }

    share.createNewDocument = function () {

        var documentHeader, documentCreatedBy;
        var docElementList = [], docAttributeList= [];

        if($scope.formFields) {
            documentHeader = share.getDocumentHeaderDetails();
            documentCreatedBy = share.getUserId();
    
            $scope.formFields.forEach(function (element) {
                element.Style = $("#" + element.id).attr("style") || "";
                docElementList.push(JSON.stringify(element));
            });

            if (documentHeader.docTemplateType === "" || documentHeader.docTemplateType === undefined) {
                toastr.error("Template type is required")
            }
            else if (documentHeader.docTemplateName === "" || documentHeader.docTemplateName === undefined) {
                toastr.error("Template name is required")
            }
            else if (documentHeader.documentName === "" || documentHeader.documentName === undefined) {
                toastr.error("Document name is required")
            }
            else {
                docAttributeList = generateAttributeList();

                $http.post('/documents/create/new', {
                    "documentTemplateId": $scope.templateId,
                    "documentTemplateType": documentHeader.docTemplateType,
                    "documentTemplateName": documentHeader.docTemplateName,
                    "documentName": documentHeader.documentName,
                    "documentCreatedId": documentCreatedBy,
                    "documentElementList": docElementList,
                    "documentAttrList": docAttributeList,
                    "isTemplateVersionControlled": $scope.isTemplateVersionControlled

                }).success(function (response) {
                    share.updateDocumentHeader(response.Document);
                    incrementNumberOfDocumentsCreated();
                    $scope.documentId = response.Document._id;
                    share.setDocumentIdHeader($scope.documentId)
                    toastr.success("Document Created Successfully");
                    share.getAllCreatedDocuments();                                                  
                });
            }
            
        } else {
            toastr.error("Error in creating document")            
        }
    }

    share.saveDocument = function () {
        var docElementList = [];
        var docAttributeList = [];
        var documentHeader = share.getDocumentHeaderDetails();
        var oppReferenceIds = [];

        $scope.formFields.forEach(function (element) {

            if(element.Type !== 'table') {
                if(element.oppReferenceId) {
                    if( !(oppReferenceIds.indexOf(element.oppReferenceId) > -1))
                        oppReferenceIds.push(element.oppReferenceId);
                }
            } else if(element.Type == 'table') {
                _.each(element.oppReferenceIds, function(obj) {
                    if( !(oppReferenceIds.indexOf(obj.value) > -1))
                        oppReferenceIds.push(obj.value);
                })
            }

            element.Style = $("#" + element.id).attr("style") || "";
            docElementList.push(JSON.stringify(element));
        });

        if (documentHeader.docTemplateType === "" || documentHeader.docTemplateType === undefined) {
            toastr.error("Template type is required")
        }
        else if (documentHeader.docTemplateName === "" || documentHeader.docTemplateName === undefined) {
            toastr.error("Template name is required")
        }
        else if (documentHeader.documentName === "" || documentHeader.documentName === undefined) {
            toastr.error("Document name is required")
        }
        else if(validateFields($scope)) {
            docAttributeList = generateAttributeList();

            $http.post('/document/update/existing', {   
                "documentTemplateType": documentHeader.docTemplateType,
                "documentTemplateName": documentHeader.docTemplateName,
                "documentName": documentHeader.documentName,
                "documentElementList": docElementList,
                "documentAttrList": docAttributeList,
                "documentVersion": documentHeader.currentVersion,
                "currentDocumentVersion": documentHeader.currentVersion,
                "documentStage": documentHeader.documentStage,
                "incrementVersionClicked": documentHeader.incrementVersionClicked,
                "reference":{
                    "type":'opportunity',
                    "referenceIds": oppReferenceIds
                },
                "isTemplateVersionControlled": documentHeader.isTemplateVersionControlled
            })
                .success(function (response) {
                    toastr.success("Document saved Successfully");  
                    share.getAllCreatedDocuments();                                                      
                });
        } else {
            toastr.error("Some mandatory fields are missing");
        }
    }

    share.setDocumentData = function(documentTemplate) {
        $scope.formFields = [];
        $scope.cache = {};
        $scope.templateId = documentTemplate._id;
        $scope.numberOfDocumentsCreated = documentTemplate.numberOfDocumentsCreated;
        $scope.formFields = documentTemplate.docTemplateElementList.map((stringEle) => JSON.parse(stringEle));
        $scope.isTemplateVersionControlled = documentTemplate.isTemplateVersionControlled;

        if(opportunityId) {
            getOpportunity(opportunityId, function(opportunity) {
                $scope.cache["opportunity"] = opportunity || {};
                initSystemReferenceFields($scope, share, $http, $scope.formFields );
                updateFormFieldValues(); 
            })
        } else {
            // call to update system referenced fields(now only date) when new document is created
            initSystemReferenceFields($scope, share, $http, $scope.formFields );
            updateFormFieldValues();     
        }
    }

    share.buildDocumentUi = function(document) {
        $scope.formFields = [];

        if( (document.documentStage == 'Accepted') || (document.currentDocumentVersion != document.documentVersion)) {
            $scope.disableEdit = true;
        } else if((document.documentStage !== 'Accepted') || (document.currentDocumentVersion === document.documentVersion)) {
            $scope.disableEdit = false;
        }

        $scope.versions.currentVersion = String(document.currentDocumentVersion);

        document.documentElementList.forEach(function (stringEle) {
            $scope.formFields.push(JSON.parse(stringEle));
        });
        share.updateDocumentHeader(document);
        checkMasterDataUpdate($scope);
        getMasterDataDifference($scope, $http);
        updateFormFieldValues();
    }

    share.hideUiForPdfDownload = function(visibility) {
        $scope.pdfDownload = visibility;
    }
    
    share.processFormFieldElements = function(scope) {
        formFields = $scope.formFields;
        var pageHeight = 760;

        _.each(formFields, function(field) {
            field.top = $("#"+field.id).css("top");
            field.top = Number(field.top.slice(0,-2));
        })

        formFields =  _.orderBy(formFields, ['top'], ['asc']);

        var noOfPages = formFields[formFields.length-1].top/pageHeight;

        for(page = 1 ; page <= noOfPages ; page++ ) {
            var minHeight = pageHeight*page;
            var maxHeight = pageHeight*(page+1);
            var pageSpecificFields = [];

            _.each(formFields, function(field) {
                if(field.top >= minHeight && field.top <= maxHeight) {
                    pageSpecificFields.push(field);
                }

                pageSpecificFields = _.orderBy(pageSpecificFields, ['top'], ['asc']);

                if(pageSpecificFields.length > 0) {
                    $("#"+pageSpecificFields[0].id).addClass('page-break');
                }
            })

        }
    }

    share.adjustElementsForPrint = function(numberOfPages) {

        for(page=1; page<=numberOfPages; page++) {
            var pageBreakPixel = page*PAGE_BREAK_PIXEL_VALUE;

            _.each($scope.formFields, function(field) {

                if(field.Type == 'textarea') {
                    var textAreaDiv = $('#div-'+field.id).find('textarea')[0]

                    var textAreaTop = Number($('#'+field.id).css('top').split('px')[0].trim());
                    var textAreaBottom = textAreaTop + $(textAreaDiv).height();

                    if(textAreaTop < pageBreakPixel && textAreaBottom > pageBreakPixel) {  
                        var pushHeight = pageBreakPixel - textAreaTop;

                        _.each($('#'+field.id).siblings(), function(sibling) {
                            var siblingTop = Number($(sibling).css('top').split('px')[0].trim());
                            
                            if(textAreaTop < siblingTop) {
                                var newSiblingPos = pushHeight+siblingTop+15+'px';
                                documentIncreasedHeight = documentIncreasedHeight + pushHeight +15;
                                $(sibling).css('top', newSiblingPos);
                            }
                        }); 

                    }

                }

            })           
        }        
    }
    
    $scope.registerDatePickerId = function (dateEle) {
        var id = "input-" + dateEle.id;
        $('#' + id).datetimepicker({
            timepicker: false,
            validateOnBlur: false,
            minDate: new Date(),
            onSelectDate: function (dp, $input) {
                dp = new Date(dp)
                dp.setHours(23)
                dp.setMinutes(59)
                dp.setSeconds(59)
                
                $scope.$apply(function () {
                    $scope[id] = moment(dp).format("DD MMM YYYY");
                    $('#' + id).val($scope[id]);
                    dateEle.Error = "";
                });
            }
        });
    }
    
    var incrementNumberOfDocumentsCreated = function() {
        var newCount = $scope.numberOfDocumentsCreated + 1;
        $http.post('/document/template/increment/document/count', {"templateId": $scope.templateId, "documentCount": newCount})
            .success(function (response) {
            });
    }

    var getOpportunity = function(opportunityId, callback) {
        $http.get('/opportunities/get/opportunity/by/id?opportunityId='+opportunityId)
            .success(function(response) {
                callback(response[0])
            })
    }
    
    docAttributeList= []

    $scope.updateValues = function (field, rowIndex, colIndex) {
        // two times the same functions are called to make the value work

        if(field && _.includes(['table', 'textarea'], field.Type))
            alterParagraphHeight(field, rowIndex, colIndex);
        
        updateFormFieldValues(field, rowIndex);
        updateFormulaFields(rowIndex);

        updateFormFieldValues(field, rowIndex);
        updateFormulaFields(rowIndex);
    }

    var cachedHeight = 0;
    var cachedTextAreaId = 0;
    var alterParagraphHeight = function(field, rowIndex, colIndex) {
        var ta;

        if(field.Type == 'textarea') 
            ta = $("#input-"+field.id);

        else if(field.Type == 'table')
            ta = $("#input-table-"+colIndex+""+rowIndex)

        var prevHeight = ta.height();

        if(cachedTextAreaId !== field.id) {
            cachedTextAreaId = field.id;
            cachedHeight = prevHeight;
        }

        if(ta[0].clientHeight < ta[0].scrollHeight) {
            cachedHeight = ta[0].scrollHeight;
            var resizedHeight = ta[0].scrollHeight - ta[0].clientHeight;

            resizeSiblingElements(field, rowIndex, colIndex, ta[0].scrollHeight, resizedHeight);
            ta.css({"height":cachedHeight});
        }

        if(field.Type == 'textarea' || cachedHeight < prevHeight) {
            autosize(ta).on('autosize:resized', function(){
            
                var newHeight = ta.height();
                if(newHeight !== cachedHeight) {
                    var resizedHeight = newHeight - cachedHeight;
    
                    resizeSiblingElements(field, rowIndex, colIndex, newHeight, resizedHeight);
                    cachedHeight = newHeight;
                }
            });
        }
    }

    var resizeSiblingElements = function(field, rowIndex, colIndex, newHeight, resizedHeight) {

        var textAreaTopValue = Number($('#'+field.id).css('top').split('px')[0].trim());

        if(field.Type == 'textarea') 
                    field.newHeight = newHeight;

        else if(field.Type == 'table') {

            _.each(field.Rows[rowIndex], function(column) {
                if(!column["newHeight"] || (column["newHeight"] < newHeight)) {
                    column["newHeight"] = newHeight
                }
            })
            // field.Rows[rowIndex][colIndex].newHeight = newHeight;
        }
        
        _.each($('#'+field.id).siblings(), function(sibling) {
            var siblingTopValue = Number($(sibling).css('top').split('px')[0].trim());

            if((textAreaTopValue) < siblingTopValue) {
                var newSiblingPos = resizedHeight+siblingTopValue +'px';
                $(sibling).css('top', newSiblingPos);
            }
        });
    }

    // build hash map of all the form fields on document -> {formId: formValue}
    var updateFormFieldValues = function (field, rowIndex) {

        if (field) {
            if (checkFormFieldType(field)) {
                if(field.Type === 'table' && rowIndex != undefined) {
                    var columnSum;

                    for(i = 0; i < field.Columns.length; i++) {
                        columnSum = 0;

                        if($scope.getFieldSetting(field.Columns[i], "Field Type").Value == 'Percentage') {
                            formFieldValues["f"+field.Columns[i].id] = Number(field.Rows[rowIndex][i].Value)/100;

                            field.Rows.forEach(function(row) {
                                columnSum += Number(row[i].Value)/100;
                            })
                            formFieldValues["f"+field.Columns[i].id+"_sum"] = Number(columnSum);

                        } else {
                            formFieldValues["f"+field.Columns[i].id] = Number(field.Rows[rowIndex][i].Value);

                            field.Rows.forEach(function(row) {
                                columnSum += Number(row[i].Value);
                            })
                            formFieldValues["f"+field.Columns[i].id+"_sum"] = Number(columnSum);

                        }
                    }
                } 
                else {

                    if($scope.getFieldSetting(field, "Field Type").Value == 'Percentage') {
                        formFieldValues["f"+field.id] = Number(field.Value)/100;                
                    } else {
                        formFieldValues["f"+field.id] = Number(field.Value);                
                    }
                }
            }

        } else {
            $scope.formFields.forEach(function (field) {
                if(checkFormFieldType(field) && field.Type !== 'table') {
                    // formFieldValues["f"+field.id] = Number(field.Value); 
                    if($scope.getFieldSetting(field, "Field Type").Value == 'Percentage') {
                        formFieldValues["f"+field.id] = Number(field.Value)/100;                
                    } else {
                        formFieldValues["f"+field.id] = Number(field.Value);                
                    }

                } else if(field.Type === 'table') {
                    var columnSum;

                    // generating default row if the number of rows are zero
                    if(field.Rows.length == 0) {
                        $scope.insertRows(field);
                    }

                    for(i=0; i<field.Columns.length; i++) {
                        columnSum = 0;
                        tableColumns["f"+field.Columns[i].id] = true;

                        if($scope.getFieldSetting(field.Columns[i], "Field Type").Value == 'Percentage') {
                            field.Rows.forEach(function(row) {
                                columnSum += Number(row[i].Value)/100;
                            })
                            formFieldValues["f"+field.Columns[i].id+"_sum"] = Number(columnSum);

                        } else {
                            field.Rows.forEach(function(row) {
                                columnSum += Number(row[i].Value);
                            })
    
                            formFieldValues["f"+field.Columns[i].id+"_sum"] = columnSum;
                        }
                    }
                }
            })
        }

    }

    // on change of the value in input field update corresponding formula fields
    var updateFormulaFields = function(rowIndex) {
        var result;

        $scope.formFields.forEach(function(element) {

            if(checkFormFieldType(element) && element.Type !== 'table' && $scope.getFieldSetting(element, "Formula").Value !== '') {

                result = evaluateExpression($scope.getFieldSetting(element, "Formula").Value, false);

                formFieldValues["f"+element.id] = Number(result);
                element.Value = Number(result).toFixed(2) == 'NaN' ? '' : Number(result).toFixed(2); 
            } 
            else if(element.Type === 'table' && rowIndex !== undefined) {
                element.Columns.forEach(function(column, index) {
                    if($scope.getFieldSetting(column, "Formula").Value !== '') {

                        // evaluate the expression
                        result = evaluateExpression($scope.getFieldSetting(column, "Formula").Value, true);

                        // update the formula field's column sum with calculated result eg: (f9_sum - f9)+result
                        formFieldValues["f"+element.Columns[index].id+"_sum"] = Number((formFieldValues["f"+element.Columns[index].id+"_sum"] - formFieldValues["f"+element.Columns[index].id]))
                                                                                + Number(result);

                        // update existing form field value for the present formula column
                        formFieldValues["f"+element.Columns[index].id] = Number(result);
                        element.Rows[rowIndex][index].Value = Number(result).toFixed(2) == 'NaN' ? '' : Number(result).toFixed(2); 
                    }
                })
            }
        })
    }

    // evaluate formula and provide result
    var evaluateExpression = function(formula, fromTable) {

        var formulaArray = [];
        formula = formula.toString();

        var operators = _.filter(formula.split(/f?[0-9]+_?[a-z]*/), (operator) => operator !== "")
                        .map( (operator) => operator.trim())
                        .filter( (operator) => operator !== "");

        var operands = _.filter(formula.split(/[*+-/%()]/), (operand) => operand !== "" || operand !== " ")
                        .map( (operand) => operand.trim())
                        .filter( (operand) => operand !== "");

        var length = operators.length > operands.length ? operators.length : operands.length;

        // When the formula begins with operand
        if(formula && formula[0] == 'f') {
            if(operands.length)
                formulaArray.push(operands[0]);
                
            if(operators.length) 
                formulaArray.push(operators[0]);


            for (i=1; i<length; i++) {
                if(i<operands.length)
                    formulaArray.push(operands[i]);
    
                if(i<operators.length)
                    formulaArray.push(operators[i])
            }
            
        } 
        // formula begins with any operators ("(), %, +, -, /, *")
        else {
            if(operators.length) 
                formulaArray.push(operators[0]);

            if(operands.length)
                formulaArray.push(operands[0]);

            for (i=1; i<length; i++) {
                if(i<operators.length)
                    formulaArray.push(operators[i])

                if(i<operands.length)
                    formulaArray.push(operands[i]);
            }
        }

        formulaArray.forEach(function(value, index) {

            // if the fieldId (ex:f29) is table Id then add table sum to it
            // if(tableColumns[value] && !fromTable) {
            if(!isNaN(tableColumns[value]) && !fromTable) {
                formulaArray[index] =formFieldValues[value+"_sum"];

            } else if(!isNaN(formFieldValues[value])) {
            // } else if(formFieldValues[value]) {
                formulaArray[index] = formFieldValues[value];
            }
        })

        try {
            return eval(formulaArray.join("")).toString();
            
        } catch(e) {}
    }

    var checkFormFieldType = function(field){
        if(field === undefined) {
            return false;
        }

        if (field.Type === 'date' || field.Type === 'image'
            || field.Type === 'header' || field.Type === 'divider') {

            return false;
        }

        if (field.Type === 'text' || field.Type === 'textarea' || field.Type === 'table') {
            return true;
        }

    }

    $scope.getSystemReferencedData = function(field, colIndex, rowIndex) {
        $scope.SystemReferencedDataHeaders = [];        
        var systemReferencedType, attributeName;
        var fieldType;
        $scope.cache = {
            "masterData": {},
            "search": ""
        };

        if(field.Type === 'table' && colIndex !== undefined && rowIndex !== undefined) {
            fieldType = $scope.getFieldSetting(field.Columns[colIndex], 'Field Type').Value;
            systemReferencedType = $scope.getFieldSetting(field.Columns[colIndex], 'System Referenced').Value;
        }
        else {
            fieldType = $scope.getFieldSetting(field, 'Field Type').Value;
            systemReferencedType = $scope.getFieldSetting(field, 'System Referenced').Value;
        }

        share.selectedTableRowIndex = rowIndex;
        share.selectedTableColumnIndex = colIndex;
        share.selectedField = field;
        share.selectedSystemReferencedType  = systemReferencedType;

        if(fieldType == 'System Referenced') {
            if(systemReferencedType === 'Opportunity') {
                $scope.cache["systemReferencedType"] = 'Opportunity';
                getReferencedOpportunities(field, colIndex, rowIndex);

            } else if(systemReferencedType === 'Master Data') {
                $scope.cache["systemReferencedType"] = 'Master Data';
                $scope.cache.masterData.skip = 0;
                $scope.getReferencedMasterData(field, '', $scope.cache.masterData.skip)

            } else if(systemReferencedType === 'Account Details') {
                $scope.cache["systemReferencedType"] = 'Account Details';
                getReferencedAccounts(field, colIndex, rowIndex);

            } else if(systemReferencedType === 'Contact Details') {
                $scope.cache["systemReferencedType"] = 'Contact Details';
                $scope.contactsHeader = ["Person Name", "Person Email Id", "Mobile Number", "Company Name"];
                $scope.showContacts = true;
            }
        }
    }

    $scope.goPrev = function(search) {

        if($scope.cache.masterData.pageStart > 50) {
            $scope.cache.masterData.skip -= 50;
            $scope.getReferencedMasterData(null, search, $scope.cache.masterData.skip);
        
        } else {
            $scope.cache.masterData.prevDisabledNg = true;
        }
    }

    $scope.goNext = function(search) {
        if($scope.cache.masterData.size > ($scope.cache.masterData.skip + 50)) {
            $scope.cache.masterData.skip += 50;
            $scope.getReferencedMasterData(null, search, $scope.cache.masterData.skip);
        
        } else {
            $scope.cache.masterData.nextDisabledNg = true;
        }
    }

    $scope.editSystemReferencedData = function(field, colIndex, rowIndex) {
        if(field.Type === 'table') {
            // $("#input-table-"+colIndex+rowIndex).prop( "disabled", false );
            $("#input-table"+colIndex+rowIndex).focus();
        }

        // $("#input-"+field.id).prop( "disabled", false );
        $("#input-"+field.id).focus();
    }

    $scope.updateFields = function(systemReferencedValue) {

        $scope.showSuggestionTable = false;

        updateReferenceIdArray(systemReferencedValue.customerProfileAttrList);
        updateUiSystemReferecedFields(systemReferencedValue.customerProfileAttrList, share.selectedSystemReferencedType);
        $scope.updateValues(share.selectedField, share.selectedTableRowIndex, share.selectedTableColumnIndex);
    }

    $scope.updateFieldsForContact = function(contact) {
        $scope.showContacts = false;
        updateContactReferencedFields(contact, share.selectedSystemReferencedType);
    }

    $scope.deleteRow = function(table, rowIndex) {

        if(table.Rows.length <= 1) {
            toastr.warning('Please add new row to delete last row of a table');

        } else {
            rearrangeUiElements(table, rowIndex);

            table.Rows.splice(rowIndex, 1);
            updateFormFieldValues();
            updateFormulaFields();
    
            // remove referenced object if any from table
            if(table.oppReferenceIds) {
                table.oppReferenceIds = _.filter(table.oppReferenceIds, function(refObj) {
                    return refObj.rowIndex !== rowIndex;
                })
            }
        }
    }

    $scope.searchContacts = function(keywords){
        searchResults($scope,$http,keywords,share,searchService)
    };

    $scope.showMasterDataDifference = function(field) {

        var masterDataTypeName = field.masterDataType;
        var headers = [];
        $scope.masterDataTableHeaders = [];
        $scope.masterDataTableData = [];

        $scope.showMasterDataDiffTable = true;
        
        var masterDataType = _.find($scope.updatedMasterData, function(data) {
                    return data.name = masterDataTypeName;
                });

        _.each(masterDataType.data, function(dataValues) {
            headers = _.union(headers, _.keys(dataValues));
        })

        $scope.masterDataTableHeaders = headers;
        $scope.masterDataTableData = masterDataType.data;
    }

    $scope.closeSystemReferenceTable = function() {
        $scope.showSuggestionTable = false;
    }

    $scope.closeMasterDataDiffTable = function() {
        $scope.showMasterDataDiffTable = false;
    }

    var updateReferenceIdArray = function(attributeList) {
        var rowFound = false;

        if(share.selectedSystemReferencedType == 'Opportunity') {

            var opportunityId = _.result(_.find(attributeList, function(opportunity) {
                return opportunity.attributeName == "opportunityId"
            }), 'attributeValue');

            if(share.selectedField.Type == 'table') {
                if(!share.selectedField.oppReferenceIds) {
                    share.selectedField.oppReferenceIds = [];
                }

                // selectedTableRowIndex

                _.each(share.selectedField.oppReferenceIds, function(obj) {
                    if(obj.rowIndex == share.selectedTableRowIndex) {
                        rowFound = true;
                        obj.value = opportunityId;
                    }
                })

                if(!rowFound) {
                    share.selectedField.oppReferenceIds.push({
                        "rowIndex": share.selectedTableRowIndex,
                        "value": opportunityId
                    })
                }
            }
        } else if(share.selectedSystemReferencedType == 'Master Data') {
            /* var masterDataTypeRowId = _.result(_.find(attributeList, function(masterTypeColumn) {
                return masterTypeColumn.attributeName == "_id"
            }), 'attributeValue');

            if(share.selectedField.Type == 'table') {
                var obj = share.selectedField.Rows[share.selectedTableRowIndex][share.selectedTableColumnIndex];
                obj.masterDataTypeRowId = masterDataTypeRowId;
                share.selectedField.Rows[share.selectedTableRowIndex][share.selectedTableColumnIndex] = obj;
            } else {
                share.selectedField.masterDataTypeRowId = masterDataTypeRowId;
            } */
        }
    }

    var rearrangeUiElements = function(table, rowIndex) {
        var tableTopValue = Number($('#'+table.id).css('top').split('px')[0].trim());
        rowIndex = rowIndex || 0;

        var tr = $('#table-' + table.id + '-row-' + rowIndex);        
        var pushHeight = Number($(tr).outerHeight());

        _.each($('#'+table.id).siblings(), function(sibling) {
            var siblingTopValue = Number($(sibling).css('top').split('px')[0].trim());

            if(tableTopValue < siblingTopValue) {
                var newSiblingPos = siblingTopValue-pushHeight+'px';
                $(sibling).css('top', newSiblingPos);
            }
        });
    }

    var rearrangeUiElementsOnTableExpand = function(table, rowIndex) {
        var tableTopValue = Number($('#'+table.id).css('top').split('px')[0].trim());
        rowIndex = rowIndex || 0;

        var tr = $('#table-' + table.id + '-row-' + rowIndex);        
        var pushHeight = Number($(tr).outerHeight());

        _.each($('#'+table.id).siblings(), function(sibling) {
            var siblingTopValue = Number($(sibling).css('top').split('px')[0].trim());

            if(tableTopValue < siblingTopValue) {
                var newSiblingPos = siblingTopValue-pushHeight+'px';
                $(sibling).css('top', newSiblingPos);
            }
        });
    }

    // function to update all the fields which are referencing System referenced attributes attributes
    var updateUiSystemReferecedFields = function(selectedData, selectedSystemReferencedType) {
        var fieldType , fieldAttributeType;
        var rowIndex = share.selectedTableRowIndex;
        var opportunityId;

        if(selectedSystemReferencedType == 'Opportunity') {
            var opp = _.find(selectedData, function(opportunity) {
                return opportunity.attributeName == "opportunityId"
            })
            var opportunityId = opp.attributeValue;
        }

        if(selectedSystemReferencedType !== 'Master Data') {

            // checking for all the fields in the document
            $scope.formFields.forEach(function (field) {
    
                // update only for text, paragraph and table
                if(checkFormFieldType(field)) {
    
                    // field of type 'table' should be updated at column and row level
                    if(field.Type !== 'table' && share.selectedField.Type !== 'table') {
    
                        // check if the referenced field is 'customer profiles'
                        fieldType = $scope.getFieldSetting(field, 'System Referenced').Value;
                        if(fieldType === selectedSystemReferencedType) {
        
                            // get the attribute name from the document field
                            fieldAttributeType = $scope.getFieldSetting(field, 'System Referenced Attributes').Value;
        
                            // loop through all the attributes from selected customer profile and update corresponding type in the field
                            selectedData.forEach( function(selectedAttribute) {
                                if( (selectedAttribute.attributeName === fieldAttributeType) && selectedAttribute.attributeValue !== undefined) {
                                    field.Value = selectedAttribute.attributeValue;
    
                                    // update opportunity Id for each field
                                    if(fieldType == 'Opportunity') {
                                        field.oppReferenceId = opportunityId;
                                    }
                                }
                            })
                        }
                    } else if(field.Type === 'table' && share.selectedField.id === field.id) {
                        if(rowIndex !== undefined) {
                            field.Columns.forEach(function(column, index) {
    
                                fieldType = $scope.getFieldSetting(column, 'System Referenced').Value;
                                
                                if(fieldType === selectedSystemReferencedType) {
                
                                    // get the attribute name from the document field
                                    fieldAttributeType =  $scope.getFieldSetting(column, 'System Referenced Attributes').Value;
                
                                    // loop through all the attributes from selected customer profile and update corresponding type in the field
                                    selectedData.forEach( function(selectedAttribute) {
                                        if( (selectedAttribute.attributeName === fieldAttributeType) && selectedAttribute.attributeValue !== undefined) {
                                            field.Rows[rowIndex][index].Value = selectedAttribute.attributeValue;
                                        }
                                    })
                                }
                            })
                        }
                    }
                }
            })
        } else if(selectedSystemReferencedType == 'Master Data') {
            var selectedFieldGroupId;
            var masterDataTypeRowId = _.result(_.find(selectedData, function(masterTypeColumn) {
                return masterTypeColumn.attributeName == "_id"
            }), 'attributeValue');
            
            // checking for all the fields in the document
            $scope.formFields.forEach(function (field) {
    
                // update only for text, paragraph and table
                if(checkFormFieldType(field)) {
    
                    // field of type 'table' should be updated at column and row level
                    if(field.Type !== 'table' && share.selectedField.Type !== 'table') {
                    // if(field.Type !== 'table') {
                        selectedFieldGroupId = $scope.getFieldSetting(share.selectedField, 'Master Data Field Group').Value;
                        // check if the referenced field is 'customer profiles'
                        fieldType = $scope.getFieldSetting(field, 'System Referenced').Value;
                        if(fieldType === selectedSystemReferencedType) {
                            var groupId = $scope.getFieldSetting(field, 'Master Data Field Group').Value;

                            if(groupId == selectedFieldGroupId) {
                                    // get the attribute name from the document field
                                fieldAttributeType = $scope.getFieldSetting(field, 'Master Data Type Attributes').Value;
            
                                // loop through all the attributes from selected customer profile and update corresponding type in the field
                                selectedData.forEach( function(selectedAttribute) {
                                    if( (selectedAttribute.attributeName === fieldAttributeType) && selectedAttribute.attributeValue !== undefined) {
                                        field.Value = selectedAttribute.attributeValue;
                                        field.masterDataTypeRowId = masterDataTypeRowId;
                                        field.staleMasterData = false;
                                        /* // update opportunity Id for each field
                                        if(fieldType == 'Opportunity') {
                                            field.oppReferenceId = opportunityId;
                                        } */
                                    }
                                })
                            }
                        }

                    } else if(field.Type === 'table' && share.selectedField.id === field.id) {
                    // }else if(field.Type === 'table') {
                        selectedFieldGroupId = $scope.getFieldSetting(share.selectedField.Columns[share.selectedTableColumnIndex], 'Master Data Field Group').Value;
                        
                        if(rowIndex !== undefined) {
                            field.Columns.forEach(function(column, index) {
    
                                fieldType = $scope.getFieldSetting(column, 'System Referenced').Value;
                                
                                if(fieldType === selectedSystemReferencedType) {
                                    var groupId = $scope.getFieldSetting(column , 'Master Data Field Group').Value;

                                    if(groupId == selectedFieldGroupId) {
                                            // get the attribute name from the document field
                                        fieldAttributeType =  $scope.getFieldSetting(column, 'Master Data Type Attributes').Value;
                    
                                        // loop through all the attributes from selected customer profile and update corresponding type in the field
                                        selectedData.forEach( function(selectedAttribute) {
                                            if( (selectedAttribute.attributeName === fieldAttributeType) && selectedAttribute.attributeValue !== undefined) {
                                                field.Rows[rowIndex][index].Value = selectedAttribute.attributeValue;
                                                field.Rows[rowIndex][index].masterDataTypeRowId = masterDataTypeRowId;
                                                field.Rows[rowIndex][index].staleMasterData = false;
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                }
            })
        }
    }

    var updateContactReferencedFields = function(contact, selectedSystemReferencedType) {
        var fieldType , fieldAttributeType;
        var rowIndex = share.selectedTableRowIndex;

        var contactKeyMapping = {
            "Name" : "personName",
            "Email Id" : "personEmailId",
            "Phone Number" : "mobileNumber",
            "Company" : "companyName"
        }

        // checking for all the fields in the document
        $scope.formFields.forEach(function (field) {

            // update only for text, paragraph and table
            if(checkFormFieldType(field)) {

                // field of type 'table' should be updated at column and row level
                if(field.Type !== 'table' && share.selectedField.Type !== 'table') {

                    // check if the referenced field is 'customer profiles'
                    fieldType = $scope.getFieldSetting(field, 'System Referenced').Value;
                    if(fieldType === selectedSystemReferencedType) {
    
                        // get the attribute name from the document field
                        fieldAttributeType = $scope.getFieldSetting(field, 'System Referenced Attributes').Value;
                        field.Value = contactKeyMapping[fieldAttributeType] ? contact[contactKeyMapping[fieldAttributeType]] : "" ;
                    }
                } else if(field.Type === 'table' && share.selectedField.id === field.id) {
                    if(rowIndex !== undefined) {
                        field.Columns.forEach(function(column, index) {

                            fieldType = $scope.getFieldSetting(column, 'System Referenced').Value;
                            if(fieldType === selectedSystemReferencedType) {
            
                                // get the attribute name from the document field
                                fieldAttributeType =  $scope.getFieldSetting(column, 'System Referenced Attributes').Value;
            
                                field.Rows[rowIndex][index].Value = contactKeyMapping[fieldAttributeType] ? contact[contactKeyMapping[fieldAttributeType]] : "" ;
                            }
                        })
                    }
                }
            }
        })
    }

    var getReferencedOpportunities = function () {
        $scope.showSuggestionTable = true;
        $scope.loadingSuggestionTable = true;
        $http.get('/documents/opportunities/get/by/user/id', { })
            .success(function (searchedOpps) {
                $scope.loadingSuggestionTable = false;
                $scope.SystemReferencedDataHeaders = [];
                $scope.SystemReferencedDataValues = [];
                var oppKeys, referencedOpportunites = {};
                if(searchedOpps.length > 0) {
                    
                    oppKeys = ['User Email','Opportunity Name','Contact Email','Product','Vertical','Business Unit','Currency','Amount', 'opportunityId']
                    $scope.SystemReferencedDataHeaders = oppKeys;
                    
                    searchedOpps.forEach(function (refOpp) {
                        referencedOpportunites.attributeList = [];

                        for (j = 0; j < oppKeys.length; j++){
                            referencedOpportunites.attributeList.push({
                                attributeName: oppKeys[j],
                                attributeValue: refOpp[oppKeys[j]]
                            })
                        }
                        $scope.SystemReferencedDataValues.push({customerProfileAttrList: referencedOpportunites.attributeList});
                    })
                }
            });
    }

    $scope.getReferencedMasterData = function(field, search, skip) {
        if($scope.cache["systemReferencedType"] == 'Master Data') {
            if(!search) {
                $scope.showSuggestionTable = true;
                $scope.loadingSuggestionTable = true;
            }
    
            field = field || share.selectedField;
            $scope.cache.masterData.skip = skip || 0;
    
            var masterDataType, masterDataUrl, accountTypeUrl;
            if(field.Type !== 'table') 
                masterDataType = $scope.getFieldSetting(field, 'Master Data Type').Value;
            else 
                masterDataType = $scope.getFieldSetting(field.Columns[share.selectedTableColumnIndex], 'Master Data Type').Value;
    
            if(!$scope.cache.masterData.accountTypes) {
                accountTypeUrl = "/corporate/admin/get/master/account/types";
                $http.get(accountTypeUrl)
                    .success(function(response) {
                        $scope.cache.masterData.accountTypes = response;
                    })
            }
            
            masterDataUrl = checkRequired(search) ? '/corporate/admin/get/master/account/list?accountType='+masterDataType+'&search='+search+"&skip="+$scope.cache.masterData.skip :
                    '/corporate/admin/get/master/account/list?accountType='+masterDataType+"&skip="+$scope.cache.masterData.skip;
    
            if(!search || (search && search.length > 2)) {

                function checkAccountTypes() {
                    $http.get(masterDataUrl)
                    .success(function(response) {
                        $scope.loadingSuggestionTable = false;
        
                        if(response.length > 0 && response[0].data.length > 0) {
                            var masterData = response[0].data;
                            $scope.SystemReferencedDataHeaders = [];
                            $scope.SystemReferencedDataValues = [];
                            var dataTypeKeys, referencedMasterDataType = {};
        
                            dataTypeKeys = Object.keys(masterData[0]);
                            $scope.SystemReferencedDataHeaders = dataTypeKeys;
        
                            masterData.forEach(function (data) {
                                referencedMasterDataType.attributeList = [];
        
                                for (j = 0; j < dataTypeKeys.length; j++){
                                    referencedMasterDataType.attributeList.push({
                                        attributeName: dataTypeKeys[j],
                                        attributeValue: data[dataTypeKeys[j]]
                                    })
                                }
                                $scope.SystemReferencedDataValues.push({customerProfileAttrList: referencedMasterDataType.attributeList});
                            })
                            
                            if(checkRequired(search)) {
                                $scope.cache.masterData.size = response[0].size;
                                $scope.cache.masterData.pageStart = $scope.cache.masterData.skip+1;
                                $scope.cache.masterData.pageEnd = Math.min($scope.cache.masterData.size, $scope.cache.masterData.skip+50);

                                $scope.cache.masterData.paginationText = $scope.cache.masterData.pageStart + "-" + $scope.cache.masterData.pageEnd + " of " + $scope.cache.masterData.size;
                            } else {
                                var type = _.find($scope.cache.masterData.accountTypes, function(el) {
                                                if(el.name == masterDataType) 
                                                    return el.size;
                                            });

                                $scope.cache.masterData.size = type.size
                                $scope.cache.masterData.pageStart = $scope.cache.masterData.skip+1;
                                $scope.cache.masterData.pageEnd = Math.min($scope.cache.masterData.size, $scope.cache.masterData.skip+50);

                                $scope.cache.masterData.paginationText = $scope.cache.masterData.pageStart + "-" + $scope.cache.masterData.pageEnd + " of " + $scope.cache.masterData.size;
                            }
                        }
                    })
                }

                setTimeout(function() {
                    checkAccountTypes();
                }, 200)
            }
        }
    }

    var getReferencedAccounts = function () {
        $scope.showSuggestionTable = true;
        $scope.loadingSuggestionTable = true;

        $http.get('/accounts/getAllSystemReferencedAccounts', { })
            .success(function (searchedOpps) {
                $scope.loadingSuggestionTable = false;
                
                $scope.SystemReferencedDataHeaders = [];
                $scope.SystemReferencedDataValues = [];
                if(searchedOpps.Data.length > 0) {
                    $scope.SystemReferencedDataHeaders = Object.keys(searchedOpps.Data[0]);
                    var oppKeys, oppValues;
                    oppKeys = Object.keys(searchedOpps.Data[0]);

                    searchedOpps.Data.forEach(function (refOpp) {
                        var referencedOpportunites = {};
                        referencedOpportunites.attributeList = [];
                        oppValues = Object.values(refOpp);
                        for (j = 0; j < oppKeys.length; j++){
                            if(oppValues[j] !== "" && oppKeys[j] !== null && oppValues[j] !== null ){
                                referencedOpportunites.attributeList.push({
                                    attributeName: oppKeys[j],
                                    attributeValue:oppValues[j]
                                })
                            }
                        }
                        $scope.SystemReferencedDataValues.push({customerProfileAttrList: referencedOpportunites.attributeList});
                    })
                }
            });
    }
});

relatasApp.directive('contenteditable', function() {
    return {
    require: 'ngModel',
    restrict: 'A',
    link: function(scope, elm, attr, ngModel) {

        function updateViewValue() {
            ngModel.$setViewValue(this.innerHTML);
        }
        //Binding it to keyup, lly bind it to any other events of interest 
        //like change etc..
        elm.on('keyup', updateViewValue);

        scope.$on('$destroy', function() {
            elm.off('keyup', updateViewValue);
        });

        ngModel.$render = function(){
            elm.html(ngModel.$viewValue);
        }

        }
    }
});

function getDocumentById(documentId, $http, $scope) {
    $http.post('/document/get/by/id', {"documentId": documentId})
        .success(function (response) {
        $scope.showSelectedDocument(response.Data);
    });
}

function checkMasterDataUpdate($scope) {
    var formFields = $scope.formFields;
    $scope.masterData = [];

    _.each(formFields, function(field) {
        if(_.includes(['text', 'textarea'], field.Type)) {
            if($scope.getFieldSetting(field, 'System Referenced').Value == 'Master Data') {
                var masterDataType = field.masterDataType;
                var masterDataTypeColumn = field.masterDataTypeColumn;
                var masterDataTypeRowId = field.masterDataTypeRowId;

                var dataTypeIndex = _.findIndex($scope.masterData, ['name', masterDataType]); 
                if(dataTypeIndex === -1) {
                    var obj = {};
                    obj.name = masterDataType;
                    obj.data = [];
                    obj.data.push({
                        "id": masterDataTypeRowId,
                        [masterDataTypeColumn]: field.Value
                    });

                    $scope.masterData.push(obj);
                } else {
                    var dataTypeDataIndex = _.findIndex($scope.masterData[dataTypeIndex].data, ['id', masterDataTypeRowId]);
                    if(dataTypeDataIndex === -1) {
                        $scope.masterData[dataTypeIndex].data.push({
                            "id": masterDataTypeRowId,
                            [masterDataTypeColumn]: field.Value
                        });
                    } else {
                        var obj = $scope.masterData[dataTypeIndex].data[dataTypeDataIndex];
                        obj[masterDataTypeColumn] = field.Value;
                        $scope.masterData[dataTypeIndex].data[dataTypeDataIndex] = obj;
                    } 
                }

            }
        } else if(field.Type == 'table') {
            _.each(field.Rows, function(row) {
                _.each(field.Columns, function(column, index) {
                    if($scope.getFieldSetting(column, 'System Referenced').Value == 'Master Data') {
                        var masterDataType = column.masterDataType;
                        var masterDataTypeColumn = column.masterDataTypeColumn;
                        var masterDataTypeRowId = row[index].masterDataTypeRowId;
        
                        var dataTypeIndex = _.findIndex($scope.masterData, ['name', masterDataType]); 
                        if(dataTypeIndex === -1) {
                            var obj = {};
                            obj.name = masterDataType;
                            obj.data = [];
                            obj.data.push({
                                "id": masterDataTypeRowId,
                                [masterDataTypeColumn]: row[index].Value
                            });
        
                            $scope.masterData.push(obj);
                        } else {
                            var dataTypeDataIndex = _.findIndex($scope.masterData[dataTypeIndex].data, ['id', masterDataTypeRowId]);
                            if(dataTypeDataIndex === -1) {
                                $scope.masterData[dataTypeIndex].data.push({
                                    "id": masterDataTypeRowId,
                                    [masterDataTypeColumn]: row[index].Value
                                });
                            } else {
                                var obj = $scope.masterData[dataTypeIndex].data[dataTypeDataIndex];
                                obj[masterDataTypeColumn] = row[index].Value;
                                $scope.masterData[dataTypeIndex].data[dataTypeDataIndex] = obj;
                            } 
                        }
        
                    }
                })
            })
        }
    })

}

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

function searchResults($scope,$http,keywords,share,searchService) {

    if(keywords && keywords.length > 2){

        searchService.search(keywords).success(function(response){

            if(response.SuccessCode){
                $scope.contacts = response.Data;
            }
        }).error(function(){
            console.log('error');
        });
    }
}

function getUniqSystemReferenceNumber(scope, http) {
    http.get('/document/get/coporate/system/generated/number')
            .success(function (response) {
                if(response.Data.documentNumber) {
                    scope.systemReferenceNumber = response.Data.documentNumber;
                }
            });
}

function incrementSystemReferenceNumber(scope, http) {
    var newSystemReferenceNumber = scope.systemReferenceNumber+1;

    http.post('/document/set/coporate/system/generated/number', {documentNumber:newSystemReferenceNumber})
            .success(function (response) {
            });
}

function initSystemReferenceFields(scope, share, http, formFields) {

    function checkLiuDetails() {
        if(share.liuData != null) {
            _.each(formFields, function(field) {
                if(field.Type == 'date' && scope.getFieldSetting(field, 'Date Selection').Value == 'System Generated') {
                    
                    //update the date field value to reflect document created date;
                    field.Value = moment(new Date()).format("DD MMM YYYY");
                    scope.getFieldSetting(field, 'Field Value Editable in Document').Value = false;
                } 
                else if(field.Type == 'text' && scope.getFieldSetting(field, 'Field Type').Value == 'System Generated') {
                    scope.getFieldSetting(field, 'Field Value Editable in Document').Value = false;
                    field.Value = scope.systemReferenceNumber;
                    incrementSystemReferenceNumber(scope, http);

                } else if( (field.Type == 'text' || field.Type == 'textarea') && scope.getFieldSetting(field, 'System Referenced').Value == 'User Details') {
                    var userAttribute = scope.getFieldSetting(field, 'System Referenced Attributes').Value;
                    populateUserAttribute(field, userAttribute, share.liuData);
                
                } else if( (field.Type !== 'table' && scope.getFieldSetting(field, 'System Referenced').Value == 'Opportunity') && opportunityId) {
                    var oppFieldName = scope.getFieldSetting(field, 'System Referenced Attributes').Value;
                    field.oppReferenceId = opportunityId;
                    populateOppFields(field, oppFieldName, scope.cache.opportunity)
                }
            });

            
        } else {
            setTimeout(function() {
                checkLiuDetails();
            }, 200)
        }
    }

    checkLiuDetails();
}

var populateUserAttribute = function(field, userAttribute, user) {
    switch(userAttribute) {
        case "First Name": field.Value = user.firstName || '';
                            break;
        case "Last Name": field.Value = user.lastName || '';
                            break;
        case "Full Name": field.Value = getLiuFullName(user);
                            break;
        case "Email Id": field.Value = user.emailId || '';
                            break;
        case "Phone Number": field.Value = user.mobileNumberWithoutCC || '';
                            break;
        case "Company": field.Value = user.companyName || '';
                            break;
        
    }
}

var populateOppFields = function(field, oppAttribute, opp) {
    switch(oppAttribute) {
        case "Amount": field.Value = opp.amount || '';
                            break;
        case "BusinessUnit": field.Value = opp.businessUnit || '';
                            break;
        case "Currency": field.Value = opp.currency || '';
                            break;
        case "ContactEmailId": field.Value = opp.contactEmailId || '';
                            break;
        case "Opportunity Name": field.Value = opp.opportunityName || '';
                            break;
        case "ProductType": field.Value = opp.productType || '';
                            break;
        case "Source": field.Value = opp.source || '';
                            break;
        case "Solution": field.Value = opp.solution || '';
                            break;
        case "UserEmailId": field.Value = opp.userEmailId || '';
                            break;
        case "Vertical": field.Value = opp.vertical || '';
                            break;
        
    }
}

var getLiuFullName = function(user) {
    var firstName = user.firstName ? user.firstName : "";
    var lastName = user.lastName ? user.lastName : "";

    return firstName + " " + lastName;
}

var validateFields = function(scope) {
    var error = false;

    scope.formFields.forEach(function(field) {
        if(field.Type == 'text' || field.Type == 'textarea' || field.Type == 'date') {
            if(scope.getFieldSetting(field, 'Mandatory').Value && (field.Value == '' || field.Value == null || field.Value == undefined) ) {
                error = true;
            }
        }
    });

    return !error;
}

var getMasterDataDifference = function($scope, $http) {

    $scope.updatedMasterData = [];
    $http.post('/get/document/master/data/difference', {"masterData": $scope.masterData})
            .success(function (response) {
                $scope.updatedMasterData = response;
                updateUiForMasterDataDifference($scope);
            });
}

var updateUiForMasterDataDifference = function($scope) {

    _.each($scope.formFields, function(field) {
        if(_.includes(['text', 'textarea'], field.Type)) {
            if($scope.getFieldSetting(field, 'System Referenced').Value == 'Master Data') {
                var masterDataType = field.masterDataType;
                var dataTypeIndex = _.findIndex($scope.updatedMasterData, ['name', masterDataType]);
                var masterDataTypeRowId = field.masterDataTypeRowId;
                var masterDataTypeColumn = field.masterDataTypeColumn;

                if(dataTypeIndex !== -1) {
                    var dataTypeDataIndex = _.findIndex($scope.updatedMasterData[dataTypeIndex].data, ['id', masterDataTypeRowId]);
                    if(dataTypeDataIndex !== -1) {
                        var dataTypeRowData = $scope.updatedMasterData[dataTypeIndex].data[dataTypeDataIndex];
                        if(dataTypeRowData[masterDataTypeColumn] && dataTypeRowData[masterDataTypeColumn].valueChanged) 
                            field.staleMasterData = true;

                    } 
                }
            }
        } else if(field.Type == 'table') {
            _.each(field.Columns, function(column, index) {
                if($scope.getFieldSetting(column, 'System Referenced').Value == 'Master Data') {
                    var masterDataType = column.masterDataType;
                    var masterDataTypeColumn = column.masterDataTypeColumn;

                    _.each(field.Rows, function(row) {
                        var dataTypeIndex = _.findIndex($scope.updatedMasterData, ['name', masterDataType]);
                        var masterDataTypeRowId = row[index].masterDataTypeRowId;

                        if(dataTypeIndex !== -1) {
                            var dataTypeDataIndex = _.findIndex($scope.updatedMasterData[dataTypeIndex].data, ['id', masterDataTypeRowId]);
                            if(dataTypeDataIndex !== -1) {
                                var dataTypeRowData = $scope.updatedMasterData[dataTypeIndex].data[dataTypeDataIndex];
                                if(dataTypeRowData[masterDataTypeColumn] && dataTypeRowData[masterDataTypeColumn].valueChanged) {
                                    row[index].staleMasterData = true;
                                } 

                            } 
                        }

                    })
                    
                }
            })
        }
    })
}

function getDomainName(share, http) {
    http.get('/getDomainName')
        .success(function(domain) {
            share.domainName = domain;
        })
}

function checkRequired(data) {
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else {
        return true;
    }
}

$(function() {
    var dh = $(document).height();
    $('#sidebar-tab-content').height(dh - 115);
    $('#main-content').height(dh - 10);
});
