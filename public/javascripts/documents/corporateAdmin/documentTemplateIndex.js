var relatasApp = angular.module('relatasApp', ["ngLodash"]).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        docTemplateType:"",
        docTemplateName:"",

        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        liuDetails:function(user){
            this.liuData = user;
        },
        getUserId:function(){
            return this.userId
        },
        setUserEmailId:function(emailId){
            this.userEmailId = emailId;
        },
        setTeamMembers:function(members){
            this.team = members;
        },
        setTargetForFy:function(value){
            this.targetForFy = value;
        },
        getTeamMembers:function(){
            return this.team;
        },
        getLiuUserEmailId:function(){
            return this.userEmailId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setTown:function(geoLocationTown){
            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        },
        setDocTemplateName:function(docTemplateName) {
            this.docTemplateName = docTemplateName;
        },
        getDocTemplateName:function() {
            return this.docTemplateName;
        },
        setDocTemplateType:function(docTemplateType) {
            this.docTemplateType = docTemplateType;
        },
        getDocTemplateType:function() {
            return this.docTemplateType
        }
    };
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };

});

var timezone;
relatasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        share.companyDetails.currency.forEach(function (el) {
            share.currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                share.primaryCurrency = el.symbol;
            }
        });
    });
    getErrorMessages($rootScope,$http,"opportunity");
});

relatasApp.controller("documentTemplateHeader", function($scope, $http, share, $rootScope) {

    share.populateHeaderForTemplate = function(template) {
        $scope.docTemplateType = template.documentTemplateType;
        $scope.docTemplateName = template.documentTemplateName;
        $scope.isDocTemplateDeactivated = template.isDocTemplateDeactivated;
        $scope.isDefaultTemplate = template.isDefaultTemplate;
        $scope.isTemplateVersionControlled = template.isTemplateVersionControlled;

        $scope.documentTemplate = true;
        $scope.templateId = template._id;
    }

    $scope.openNewTemplate = function() {
        $scope.documentTemplate = true;
        share.showAllDocumentTemplatesWindow(false);
        share.showDocumentTemplateCreateWindow(true);
    }

    $scope.createTemplate = function() {
        $scope.savingTemplate = true;
        share.showAllDocumentTemplatesWindow(false);
        share.createNewTemplate();
    }

    $scope.saveDocTemplate = function() {
        $scope.savingTemplate = true;
        share.saveDocumentTemplate();
    }

    $scope.enableGrid = function(toggleGrid) {
        share.showGrid(toggleGrid)
        return toggleGrid;
    }

    $scope.closedocTemplate = function() {
        window.location = "/documentTemplates/index";                      
    }

    $scope.activateDocTemplate = function () {
        var deactivated = false;
        $http.post('/documentTemplates/updateIsDocTemplateDeactivated', {"documentTemplateType":$scope.docTemplateType,
                                                                            "templateId":$scope.templateId,
                                                                            "isDocTemplateDeactivated":deactivated
        })
        $scope.isDocTemplateDeactivated = !$scope.isDocTemplateDeactivated; 
    }

    $scope.deactivateDocTemplate = function () {
        var deactivated = true;

        $http.post('/documentTemplates/updateIsDocTemplateDeactivated', {"documentTemplateType":$scope.docTemplateType,
                                                                            "templateId":$scope.templateId,
                                                                            "isDocTemplateDeactivated":deactivated
        })

        $scope.isDocTemplateDeactivated = !$scope.isDocTemplateDeactivated; 
    }
    
    $scope.setDocumentTemplateAsDefault = function () {
        var isDefault = true;
        $http.post('/documentTemplates/setDocumentTemplateAsDefault', {
            "documentTemplateType":$scope.docTemplateType,
            "templateId":$scope.templateId,
            "isDefault":isDefault
        }).success(function(response) {
            toastr.success("This template saved as default successfully");
        }) 
    }

    // added Sachin for setting the Template as Default
    $scope.setDocumentTemplateAsVersionControlled = function () {
        var isVersionControlled = true;
        $http.post('/documentTemplates/setDocumentTemplateAsVersionControlled', {
            "documentTemplateType":$scope.docTemplateType,
            "templateId":$scope.templateId,
            "isTemplateVersionControlled":isVersionControlled
        })

    }

    $scope.resetDocumentTemplateAsVersionControlled = function () {
        var isVersionControlled = false;
        $http.post('/documentTemplates/setDocumentTemplateAsVersionControlled', {
            "documentTemplateType":$scope.docTemplateType,
            "templateId":$scope.templateId,
            "isTemplateVersionControlled":isVersionControlled
        })
    }

    $scope.cloneDocTemplate = function() {
        $scope.docTemplateName = $scope.docTemplateName+"_clone";
        $scope.templateId = undefined;
    }

    share.getTemplateHeaderDetails = function() {
        return {
            docTemplateType:$scope.docTemplateType,
            docTemplateName:$scope.docTemplateName,
            defaultTemplate:$scope.isDefaultTemplate,
            versionControlled:$scope.isTemplateVersionControlled,
        }
    }

    share.enableSaveButton = function(status) {
        $scope.savingTemplate = !status;
    }
});

relatasApp.controller("showDocTemplatesController", function($scope,$http, share){
    $scope.headers = ["S.No.", "Document Template Type", "Document Template Name", "Created Date", "Is Deactivated?", "Is Default Template?"];

    $scope.getAllDocumentTemplates = function() {
        $scope.showAllDocumentTemplates = true;

        $http.get('/documentTemplates/get/all/data')
        .success(function (response) {
            $scope.allDocTemplates = response.Data;
            $scope.allDocTemplates.forEach(function(el) {
                el.dateFormatted = moment(el.createdDate).format("DD MMM YYYY");
        });
            extractTemplates($scope.allDocTemplates);
    })

    }

    $scope.getAllDocumentTemplates();

    share.renderTemplate = function(template){
        share.showAllDocumentTemplatesWindow(false);
        share.populateHeaderForTemplate(template);
        // share.getTemplateById(template._id);
        share.renderTemplateUi(template);
    }

    $scope.showSelectedDocTemplate = function(template){
        // $scope.showAllDocumentTemplates = false;
        share.renderTemplate(template)
    }

    $scope.getTemplates = function(templateType) {

        return $scope.documentTemplateCollection[templateType];
    }

    share.showAllDocumentTemplatesWindow = function(visibility) {
        $scope.showAllDocumentTemplates = visibility;
    }

    var extractTemplates = function(documentTemplates) {
        $scope.documentTypes = [];
        $scope.documentTemplateCollection = {};

        // get all the template types
        documentTemplates.forEach(function(template) {
            $scope.documentTypes.push(template.documentTemplateType);
        })

        // extract uniq name from template type array
        $scope.documentTypes = _.uniq($scope.documentTypes);

        // initializing the template collection
        $scope.documentTypes.forEach(function(templateType) {
            $scope.documentTemplateCollection[templateType] = [];
        })

        // building template collection, template type as a key
        documentTemplates.forEach(function(template) {
            $scope.documentTemplateCollection[template.documentTemplateType].push(template);
        });

    } 

});

relatasApp.controller('createDocTemplatesController', function($scope, $timeout, $http,share) {
    
    documentsCommon($scope, share);

    var isActivated = getParams(window.location.href).isActivated;
    $scope.isTemplateVersionControlled = getParams(window.location.href).isVersionControlled;

    var guid = 0; 
    var companyImgUrl = "/docTemplateLogos/";
    var selectedTable = {}
    var selectedColIndex;
    
    share.showDocumentTemplateCreateWindow = function(visibility) {
        $scope.documentTemplate = visibility;
    }

    $scope.isDocTemplateDeactivated = isActivated === "true" ? true :false;

    $scope.dragElements = [{
        'Name': "Textbox Title",
        'Type': "text",
        'Icon': "fa fa-text-height",
        'Error': "",
        'DivStyle': "width: 160px;",
        "Editable": true,
        'Settings': [{
            'Name': 'Field Label',
            'Value': 'Textbox Title',
            'Type': 'text'
        }, {
            'Name': 'Placeholder',
            'Value': '',
            'Type': 'text'
        }, {
            'Name': 'Input Value Align',
            'Value': 'left',
            'Type': 'dropdown',
            'PossibleValue': ['left', 'center', 'right','justify']
        }, {
            'Name': 'Field Type',
            'Value': 'String',
            'Type': 'dropdown',
            'PossibleValue': ['Number', 'String', 'Alpha Numeric', 'Percentage', 'Formula', 'Email Id', 'System Referenced', 'System Generated']
        }, {
            'Name': 'Formula',
            'Value': '',
            'Type': 'formula',
            'Visibility': false
        }, {
            'Name': 'System Referenced',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': share.document.getSystemReferencedEntities
        }, {
            'Name': 'Master Data Type',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Master Data Type Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Master Data Field Group',
            'Value': '',
            'Visibility': false,
            'Type': 'text'
        },{
            'Name': 'System Referenced Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Max Input Length',
            'Value': '100',
            'Type': 'text'
        },{
            'Name': 'Fonts',
            'Value': '',
            'Type': 'dropdown',
            'PossibleValue': share.document.getFontTypes
        },{
            'Name': 'Font Size',
            'Value': '14',
            'Type': 'text',
        },{
            'Name': 'Field Label Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Field Text Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Field Background Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Top',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Right',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Bottom',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Left',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': share.document.getFieldGeneralOptions
        }]
    }, {
        'Name': "Date",
        'Value':'',
        'Type': "date",
        'Error': "",
        "Editable": true,
        'DivStyle': "width: 160px;",
        'Icon': "fa fa-calendar-check-o",
        'Settings': [{
            'Name': 'Field Label',
            'Value': 'Date',
            'Type': 'text'
        }, {
            'Name': 'Input Value Align',
            'Value': 'left',
            'Type': 'dropdown',
            'PossibleValue': ['left', 'center', 'right','justify']
        }, {
            'Name': 'Date Selection',
            'Value': 'User Selected',
            'Type': 'dropdown',
            'PossibleValue': ['User Selected', 'System Generated']                
        },{
            'Name': 'Fonts',
            'Value': '',
            'Type': 'dropdown',
            'PossibleValue': share.document.getFontTypes
        },{
            'Name': 'Font Size',
            'Value': '14',
            'Type': 'text',
        },{
            'Name': 'Field Label Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Field Text Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Field Background Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Top',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Right',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Bottom',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Left',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': share.document.getFieldGeneralOptions
        }]
    }, {
        'Name': "Paragraph Text",
        'Value':'',
        'Type': "textarea",
        'Error': "",
        'DivStyle': "width: 200px; height: 100px",
        "Editable": true,
        'Icon': "fa fa-square-o",
        'Settings': [{
            'Name': 'Placeholder',
            'Value': '',
            'Type': 'text'
        }, {
            'Name': 'Input Value Align',
            'Value': 'left',
            'Type': 'dropdown',
            'PossibleValue': ['left', 'center', 'right', 'justify']
        },{
            'Name': 'Field Type',
            'Value': 'String',
            'Type': 'dropdown',
            'PossibleValue': share.document.getFieldTypes
        }, {
            'Name': 'Formula',
            'Value': '',
            'Type': 'formula',
            'Visibility': false
        },{
            'Name': 'System Referenced',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': share.document.getSystemReferencedEntities
        }, {
            'Name': 'Master Data Type',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Master Data Type Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Master Data Field Group',
            'Value': '',
            'Visibility': false,
            'Type': 'text'
        },{
            'Name': 'System Referenced Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Max Input Length',
            'Value': '500',
            'Type': 'text'
        },{
            'Name': 'Fonts',
            'Value': '',
            'Type': 'dropdown',
            'PossibleValue': share.document.getFontTypes
        },{
            'Name': 'Font Size',
            'Value': '14',
            'Type': 'text',
        },{
            'Name': 'Field Label Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Field Text Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Field Background Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Top',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Right',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Bottom',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Left',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': share.document.getFieldGeneralOptions
        }]
    }, {
        'Name': "Image",
        "Type": "image",
        'Error': "You can upload images of type .jpg/ .png/ .jpeg and size should not be more than 100KB",
        'Icon': "fa fa-picture-o",
        // 'DivStyle': "width: 150px; height: 150px",
        'Settings':[{
            'Name': 'Upload Image',
            'Value': '',
            'Type': 'file'
        },{
            'Name': 'Preview Image',
            'Value': '',
            'Type': 'image'
        },{
            'Name': 'Upload image',
            'Value': '',
            'Type': 'button'
        },{
            'Name': 'Relative_Path',
            'Value': '',
            'Type': 'url'
        },{
            'Name': 'Absolute_Path',
            'Value': '',
            'Type': 'url'
        }]
    }, {
        'Name': "Table",
        'Type': "table",
        'Icon': "fa fa-table",
        'Error': "",
        'DivStyle': "width: 500px; height: 70px",
        'Columns':[],
        'Rows':[],
        'Settings':[{
            'Name': 'Header Font Size',
            'Value': '14',
            'Type': 'text'
        },{
            'Name': 'Header Text Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Header Background Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Header Value Align',
            'Value': 'left',
            'Type': 'dropdown',
            'PossibleValue': ['left', 'center', 'right']
        },{
            'Name': 'Top',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Right',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Bottom',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Left',
            'Value': 0,
            'Type': 'number'
        }]
    }, {
        'Name': "Header",
        'Type': "header",
        'Icon': "fa fa-header",
        'DivStyle': "width: 250px; height: 30px",
        'Error': "Header cannot be empty",
        'Settings':[{
            'Name': 'Field Label',
            'Value': 'Header',
            'Type': 'text'
        },{
            'Name': 'Fonts',
            'Value': '',
            'Type': 'dropdown',
            'PossibleValue': share.document.getFontTypes
        },{
            'Name': 'Font Size',
            'Value': '20',
            'Type': 'text',
        }, {
            'Name': 'Text Align',
            'Value': 'center',
            'Type': 'dropdown',
            'PossibleValue': ['center', 'left', 'right']                
        }, {
            'Name': 'Text Style',
            'Value': 'normal',
            'Type': 'dropdown',
            'PossibleValue': ['normal', 'bold', 'italic', 'bold italics']                
        },{
            'Name': 'Background Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Text Color',
            'Value': '',
            'Type': 'color'
        }]
    }, {
        'Name': "Divider",
        "Type": "divider",
        'DivStyle': "width: 700px; height: 2px",
        'Icon': "fa fa-minus",
        'Settings':[{
            'Name': 'Field Background Color',
            'Value': '',
            'Type': 'color'
        }]
    }
 ];

    share.getTemplateById = function(templateId) {
        // share.setHeaderBackgroundColor("#f2f2f2");
        $scope.formFields = [];
        $scope.current_field = {};
        share.showDocumentTemplateCreateWindow(true);

        $scope.templateId = templateId;

        $http.post('/documentTemplates/getById', {"templateId": templateId})
        .success(function (response) {
            if(response.Data.length > 0) {
                $scope.disableSave = response.Data[0].isDocumentCreated ? true : false;

                var attrList = response.Data[0];
                var fieldObject = {};

                attrList.docTemplateElementList.forEach(function(stringEle) {
                    fieldObject = JSON.parse(stringEle);

                    // Restoring the table
                    if(fieldObject.Type == 'table') {
                        fieldObject.Rows = [];
                        $scope.insertRows(fieldObject, false);
                    }

                    $scope.formFields.push(fieldObject);
                })

                guid = attrList.documentTemplateUiId || guid;
            }
        })
    }

    share.renderTemplateUi = function(template) {
        $scope.templateId = template._id;

        $scope.formFields = [];
        $scope.current_field = {};
        share.showDocumentTemplateCreateWindow(true);

        template.docTemplateElementList.forEach(function(stringEle) {
            fieldObject = JSON.parse(stringEle);

            // Restoring the table
            if(fieldObject.Type == 'table') {
                fieldObject.Rows = [];
                $scope.insertRows(fieldObject, false);
            }

            $scope.formFields.push(fieldObject);
        })

        guid = template.documentTemplateUiId || guid;
    }


    share.showGrid = function(toggleGrid) {
        $scope.toggleGrid = toggleGrid;
    }
    // share.getTemplateById($scope.templateId)

    var createNewField = function() {
        return {
            'id': ++guid,
            'Name': '',
            'Settings': [],
            'Active': true,
            'Hover': false,
            'Style':'',
            'DivStyle':''
        };
    }

    $scope.getUniqImageName = function(imageField, imageName) {
        var relativeImagePath, absoluteImagePath;

        relativeImagePath = $scope.getFieldSetting(imageField, 'Relative_Path');
        relativeImagePath.Value = imageName;

        absoluteImagePath = companyImgUrl+$scope.getFieldSetting(imageField, 'Relative_Path').Value;
        // $scope.getFieldSetting(imageField, 'Absolute_Path').Value = absoluteImagePath;
        return absoluteImagePath;
    }

    $scope.removeElement = function(idx, event){
        if($scope.formFields[idx].Active) {
            $('#addFieldTab_lnk').tab('show');
            $scope.current_field = {};
        }
        $scope.formFields.splice(idx, 1);
        
    };

    $scope.copyElement = function(field) {

        var copyField = jQuery.extend(true, {}, field);
        copyField.id = ++guid;
        
        if(field.Type === 'table') {
            _.each(copyField.Columns, function(column) {
                column.id = ++guid;
            })
        }

        $scope.formFields.push(copyField);

    }

    $scope.addElement = function(ele, idx) {
        const defaultColumns = 3;

        $scope.current_field.Active = false;

        $scope.current_field = createNewField();
        angular.merge($scope.current_field, ele);
        
        if (typeof idx == 'undefined') {
            $scope.formFields.push($scope.current_field);
        } else {
            $scope.formFields.splice(idx, 0, $scope.current_field);
            $('#fieldSettingTab_lnk').tab('show');
        }

        switch($scope.current_field.Name) {
            case 'Table':
                    generateColumnsForTable($scope.current_field, defaultColumns);
                    break;
            case 'Image':
                    var fileName = $scope.getUniqImageName($scope.current_field, "company-placeholder-logo.png");
                    $scope.getFieldSetting($scope.current_field, 'Absolute_Path').Value  = fileName;
            default: $scope.activeField($scope.current_field);
        }

    };

    $scope.activeField = function(f) {
        $scope.current_field.Active = false;
        $scope.current_field = f;
        f.Active = true;

        $('#fieldSettingTab_lnk').tab('show');

        if(f.Type == 'text' || f.Type == 'textarea' || f.Type == 'column') {
            highlightMasterDataGroup(f);
        }
    };

    $scope.activeFieldForFormula = function(f, index, event) {
        event.stopPropagation();

        var formulaString;

        if(f.Name !== 'Table' && f.Name !== 'Image' && f.Name !== 'Header' && f.Name !== 'Date') {

            formulaString = "f" + f.id + '+';
            $scope.getFieldSetting($scope.current_field, 'Formula').Value +=  formulaString;
        }

    }

    $scope.formulaFocus = function() {
        $scope.formulaMode = true;
    }

    $scope.formulaUnFocus = function() {
        $scope.formulaMode = false;
    }

    $scope.formulaBlur = function(event, field) {
        // come out of formula mode when enter is pressed
        if(event.keyCode === 13) {
            $scope.formulaMode = false;
            $("#"+field.id+"-formula").blur();        
        }
    }

    $scope.registerDatePickerId = function(dateEle){
        var id = "input-"+dateEle.id;
        $('#'+id).datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                dp = new Date(dp)
                dp.setHours(23)
                dp.setMinutes(59)
                dp.setSeconds(59)
                
                $scope.$apply(function () {
                    $scope[id] = moment(dp).format("DD MMM YYYY");
                    $('#'+id).val($scope[id])
                });
            }
        });
    }

    // table methods
    $scope.columnData = {
        'Name': "",
        'Type': "column",
        'Error': "",
        "Editable": true,
        'Value':"",
        'ThStyle':"",
        'TrStyle':"",
        'Settings':[{
            'Name': 'Column Name',
            'Value': '',
            'Type': 'text'
        }, {
            'Name': 'Row Value Align',
            'Value': 'left',
            'Type': 'dropdown',
            'PossibleValue': ['left', 'center', 'right']
        },{
            'Name': 'Field Type',
            'Value': 'String',
            'Type': 'dropdown',
            'PossibleValue': share.document.getFieldTypes,
        },{
            'Name': 'Formula',
            'Value': '',
            'Type': 'formula',
            'Visibility': false
        }, {
            'Name': 'System Referenced',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': share.document.getSystemReferencedEntities
        }, {
            'Name': 'Master Data Type',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Master Data Type Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Master Data Field Group',
            'Value': '',
            'Visibility': false,
            'Type': 'text'
        },{
            'Name': 'System Referenced Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Max Input Length',
            'Value': '100',
            'Type': 'text'
        },{
            'Name': 'Fonts',
            'Value': '',
            'Type': 'dropdown',
            'PossibleValue': share.document.getFontTypes
        },{
            'Name': 'Font Size',
            'Value': '14',
            'Type': 'text'
        },{
            'Name': 'Column Text Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Column Background Color',
            'Value': '',
            'Type': 'color'
        },{
            'Name': 'Top',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Right',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Bottom',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'Left',
            'Value': 0,
            'Type': 'number'
        },{
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
                'Name': 'Mandatory',
                'Value': false
            },{

                'Name': 'Field Value Editable in Document',
                'Value': true
            }, {
                'Name': 'Add default value in Document',
                'Value': false
            }]
        }]
    }

    
    var createNewColumn = function() {
        return {
            'id': ++guid,
            'Name': '',
            'Settings': [],
            'Active' : true
        }
    };

    $scope.getColumnIndex = function(table, colIndex, event) {
        event.stopPropagation();
        $scope.activeField(table.Columns[colIndex]);
        selectedTable = table;
        selectedColIndex = colIndex;
        // $scope.deleteRow(table, colIndex);
    }

    $scope.selectColumn = function(table, colIndex, event) {
        event.stopPropagation();
        selectedTable = table;
        selectedColIndex = colIndex;
    }

    $scope.addColumn = function(ele, idx, table) {

        $scope.current_field.Active = false;
        ele.Name  =  'col_'+idx;

        $scope.getFieldSetting(ele, 'Column Name').Value = ele.Name;
        $scope.current_field = createNewColumn();

        //Merge setting from template object
        angular.merge($scope.current_field, ele);
        
        if (typeof idx == 'undefined') {
            table.Columns.push($scope.current_field);
        } else {
            table.Columns.splice(idx, 0, $scope.current_field);
            $('#fieldSettingTab_lnk').tab('show');
        }

        //flush existing rows
        table.Rows = [];
        $scope.insertRows(table);
    };

    $scope.deleteColumn = function() {
        if(selectedTable && selectedColIndex !== undefined) {
            selectedTable.Columns.splice(selectedColIndex, 1);
            $('#addFieldTab_lnk').tab('show');
        }

        //deleting columns in a default generated row in both rows
        selectedTable.Rows = [];
        $scope.insertRows(selectedTable);
    }

    $scope.addColumnForTable = function(table, event) {
        event.stopPropagation();
        if(table !== undefined) {
            $scope.addColumn($scope.columnData, table.Columns.length, table);
        }
    }

    var getTableHeaders = function(table, numberOfColumns) {
        if(numberOfColumns < table.Columns.length) {
        table.Columns.splice(numberOfColumns, table.Columns.length-numberOfColumns);
            
        } else if(numberOfColumns > table.Columns.length){
            for (var i=table.Columns.length; i<numberOfColumns; i++) {
                $scope.addColumn($scope.columnData, i, table);
            }
        }
    }

    var generateColumnsForTable = function(table, numberOfColumns) {
        getTableHeaders(table, numberOfColumns);
    }

    $scope.checkVisibility = function(fieldType, fieldSettings) {

        if(fieldType.Name === 'Number of columns') {
            generateColumnsForTable($scope.current_field, fieldType.Value);            
        }

        switch(fieldType.Value) {

            case 'Formula':
                $scope.getFieldSetting($scope.current_field, 'Formula').Visibility = true;      
                $scope.getFieldSetting($scope.current_field, 'System Referenced').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = false;
                break;

            case 'System Referenced':
                $scope.getFieldSetting($scope.current_field, 'Formula').Visibility = false;      
                $scope.getFieldSetting($scope.current_field, 'Formula').Value = "";  
                $scope.formulaMode = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced').Visibility = true;
                // $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = true;
                break;

            case 'Boolean':
            case 'Numeric':
            case 'Alpha Numeric':
            case 'String':
                $scope.getFieldSetting($scope.current_field, 'Formula').Visibility = false;      
                $scope.getFieldSetting($scope.current_field, 'Formula').Value = ""; 
                $scope.formulaMode = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = false;

        }

        return true;
    }

    share.saveDocumentTemplate = function () {


        var templateMetaData = share.getTemplateHeaderDetails();
        $scope.formulaMode = false;
        var tempElementList = [];
        var docTemplateType = templateMetaData.docTemplateType;
        var docTemplateName = templateMetaData.docTemplateName;
        var docAttributeList = []
        var masterData = [];

        $scope.formFields.forEach(function(element) {
            // element.Style = $("#"+element.id).attr("style") || "";
            // element.DivStyle = $("#div-"+element.id).attr("style") || "";

            //Removing rows from the table

            if(element.Type == 'table') {
                
                for(var column=0; column<element.Columns.length; column++) {
                    element.Columns[column].ThStyle = $("#table-th-"+element.id+String(column)).attr("style") || "";
                    element.Columns[column].TrStyle = $("#table-td-"+element.id+String(column)).attr("style") || "";
                }
                // element.Rows = [];
            } 

            tempElementList.push(JSON.stringify(element));
        });


        if(docTemplateType === "" || docTemplateType === undefined) {
            toastr.error("Template type is required")            
        } 
        else if(docTemplateName === "" || docTemplateName === undefined) {
            toastr.error("Template name is required")            
        } else {            

            docAttributeList = generateAttributeList();

            $http.post('/documentTemplatesElements/add/new', {"documentTemplateType":docTemplateType,
            "documentTemplateName":docTemplateName,
            "docTemplateElementList":tempElementList,
            // "isDocTemplateDeactivated":false,
            "documentTemplateUiId":guid,
            "isDefault":templateMetaData.defaultTemplate,
            "isTemplateVersionControlled":templateMetaData.versionControlled,
            "docTemplateAttrList": docAttributeList

        }).success(function(response) {
            share.enableSaveButton(true);
            if(response.SuccessCode == 1) {
                toastr.success("Document template saved successfully");
            } else {
                toastr.error("Error in saving document template")                                                        
            }
        });
        }
    }

    share.createNewTemplate = function() {
        // share.setHeaderBackgroundColor("#f2f2f2");
        var templateMetaData = share.getTemplateHeaderDetails();
        var docAttributeList = [];        
        var tempElementList = [];

        $scope.formFields = $scope.formFields ? $scope.formFields : [] ;
        $scope.current_field = {};

        var docTemplateType = templateMetaData.docTemplateType;
        var docTemplateName = templateMetaData.docTemplateName;

        if($scope.formFields.length > 0) {
            $scope.formFields.forEach(function(element) {
                element.Style = $("#"+element.id).attr("style") || "";
                element.DivStyle = $("#div-"+element.id).attr("style") || "";
    
                //Removing rows from the table
                if(element.Type == 'table') {
                    
                    for(var column=0; column<element.Columns.length; column++) {
                        element.Columns[column].ThStyle = $("#table-th-"+element.id+String(column)).attr("style") || "";
                        element.Columns[column].TrStyle = $("#table-td-"+element.id+String(column)).attr("style") || "";
                    }
                    element.Rows = [];
    
                }

                tempElementList.push(JSON.stringify(element));
            });
            docAttributeList = generateAttributeList();
        }

        if(docTemplateType === "" || docTemplateType === undefined) {
            toastr.error("Template type is required")            
        } 
        else if(docTemplateName === "" || docTemplateName === undefined) {
            toastr.error("Template name is required")            
        } else {

            $http.post('/document/template/create/new', {"documentTemplateType":docTemplateType,
                                                        "documentTemplateName":docTemplateName,
                                                        "documentTemplateUiId":guid,
                                                        "docTemplateElementList":tempElementList,
                                                        "docTemplateAttrList": docAttributeList
                                                    })
                .success(function(response) {
                    share.enableSaveButton(true);

                    if (response.Template){
                        toastr.success("New template created successfully");
                        share.renderTemplate(response.Template);
                    }
                });
        }
    }

    $scope.setReferencedTemplateType = function (settings) { if(settings.Name == 'System Referenced') {
            if(settings.Value === 'Opportunity') {
    
                $scope.changeFieldSetting(share.document.getOpportunityReferencedAttributes , 'System Referenced Attributes');
                enableSettingsForMasterData(false);
    
            } else if(settings.Value === 'Master Data') {
                $scope.masterData = [];
                enableSettingsForMasterData(true);

                getMasterData(function(masterCollection) {
                    if(masterCollection.length > 0) {
                        _.each(masterCollection[0].masterData, function(collection) {
                            var obj = {};
                            obj.dataType = collection.name
                            obj.dataTypeAttributes = _.map(collection.data,  'name');
                            $scope.masterData.push(obj);
                        })

                        $scope.changeFieldSetting(_.map($scope.masterData,  'dataType') , 'Master Data Type');
                    }
                });

            } else if(settings.Value == 'User Details') {
                $scope.changeFieldSetting(share.document.getUserDetailsReferencedAttributes, 'System Referenced Attributes');   
                enableSettingsForMasterData(false);
            }
            else if(settings.Value === 'Account Details') {
    
                $scope.changeFieldSetting(share.document.getAccountReferencedAttributes , 'System Referenced Attributes');
                enableSettingsForMasterData(false);
            } else if(settings.Value == 'Contact Details') {
                $scope.changeFieldSetting(share.document.getContactReferencedAttributes , 'System Referenced Attributes');
                enableSettingsForMasterData(false);

            } 
        }

        if(settings.Name == 'Master Data Type') {
            var attributes = _.filter($scope.masterData, function(data) {
                return data.dataType == settings.Value;
            })

            _.remove(attributes[0].dataTypeAttributes, function(attriture) {
                return attriture == "_id";
            })

            $scope.changeFieldSetting(attributes[0].dataTypeAttributes , 'Master Data Type Attributes');

            if($scope.current_field.Type !== 'table') {
                $scope.current_field.masterDataType = settings.Value;
            }
        }else if(settings.Name == 'Master Data Type Attributes') {

            if($scope.current_field.Type !== 'table') {
                $scope.current_field.masterDataTypeColumn = settings.Value;
            }
        }


        /* if(settings.Name === 'Customer Profile Template Type' && settings.Value != '') {
            $scope.cpTemplateType = settings.Value;

            getCustomerProfileTemplateNames(settings.Value ,function (cpNames) {
                $scope.changeFieldSetting(cpNames , 'Customer Profile Template Name');

            });
        }

        if(settings.Name === 'Customer Profile Template Name' && settings.Value != '') {

            getCustomerProfileTemplateAttributes($scope.cpTemplateType,
                                                    settings.Value ,
                                                    function (cpAttributes) {
                $scope.changeFieldSetting(cpAttributes , 'System Referenced Attributes');
            });
        } */
    }

    $scope.changeCompanyImage = function(field, event) {
        event.stopPropagation();
        angular.element(document.querySelector("#select-pic-"+field.id)).triggerHandler('click');
        // $("#select-pic-"+field.id).trigger("click");
    }   

    $scope.uploadImage = function(imageId) {
        var uploadInputId = "#select-pic-"+imageId;

        $timeout(function(){
            angular.element($(uploadInputId)).trigger('click');
        });
    }

    $scope.changeBackground = function(field) {
        $scope.$apply(function() {
            field.Value = '#' + color;
        })
    }

    var highlightMasterDataGroup = function(field) {
        var groupId = $scope.getFieldSetting(field, 'Master Data Field Group').Value

        if($scope.getFieldSetting(field, 'System Referenced').Value == 'Master Data') {

            _.each($scope.formFields, function(f) {
                if(f.Type == 'text' || f.Type == 'textarea' || f.Type == 'table') {

                    if(f.Type != 'table' && $scope.getFieldSetting(f, 'System Referenced').Value == 'Master Data') {

                        if(groupId == $scope.getFieldSetting(f, 'Master Data Field Group').Value)
                            f.masterDataGroupIndicator = true;
                        else
                            f.masterDataGroupIndicator = false;

                    } else if(f.Type == 'table') {
                        _.each(f.Columns, function(column) {
                            if(groupId == $scope.getFieldSetting(column, 'Master Data Field Group').Value)
                                column.masterDataGroupIndicator = true;
                            else
                                column.masterDataGroupIndicator = false;
                        })
                    }
                    
                } else {
                    f.masterDataGroupIndicator = false;
                } 
            })
        }

    }

    var enableSettingsForMasterData = function (visibility) {
        $scope.getFieldSetting($scope.current_field, 'Master Data Type').Visibility = visibility;
        $scope.getFieldSetting($scope.current_field, 'Master Data Type Attributes').Visibility = visibility;
        $scope.getFieldSetting($scope.current_field, 'Master Data Field Group').Visibility = visibility;
        $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = !visibility;

        if(visibility) {
            $scope.changeFieldSetting([] , 'System Referenced Attributes');
        }

        if(!visibility) {
            $scope.current_field.masterDataType = '';
            $scope.current_field.masterDataTypeColumn = '';
        }
    }

    var getMasterData = function(callback) {

        $http.get('/corporate/admin/get/master/account/data')
            .success(function (response) {
                callback(response);
            })
    }
});

relatasApp.directive('elementDraggable', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {
            element.on('dragstart', function(event) {

                event.originalEvent.dataTransfer.setData('templateIdx', $(element).data('index'));
            });
        }
    };
}]);

relatasApp.directive('elementDrop', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {

            element.on('dragover', function(event) {
                event.preventDefault();
            });

            $('.drop').on('dragenter', function(event) {
                event.preventDefault();
            })
            element.on('drop', function(event) {
                event.stopPropagation();
                var self = $(this);
                scope.$apply(function() {
                    var idx = event.originalEvent.dataTransfer.getData('templateIdx');
                    var insertIdx = self.data('index')
                    scope.addElement(scope.dragElements[idx], insertIdx);
                });
            });
        }
    };
}]);

relatasApp.directive("imageUpload", ['$document', function($document) {
    return {
        scope: {
        fileinput: "=",
        filepreview: "="
    },
    link: function(scope, element, attributes) {
        element.bind("change", function(changeEvent) {
            scope.fileinput = changeEvent.target.files[0];
            var reader = new FileReader();
            reader.onload = function(loadEvent) {
                upload(scope.fileinput, scope.filepreview, scope);
            }
            
            reader.readAsDataURL(scope.fileinput);
        });
    }
}
}]);

var upload = function(file, imageField){

    var scope = angular.element($('#document-template')).scope();
    // var fileName = scope.getFieldSetting(imageField, 'Absolute_Path').Value

    var formData = new FormData();
    var xhr = new XMLHttpRequest(); 
    var imageName = file.name;
    var extension = imageName.split('.')[imageName.split('.').length-1];
    var fileName = scope.getUniqImageName(imageField, imageName);

    if(file.size < 101000 && (extension == 'png' || extension == 'jpeg' || extension == 'jpg')) {

        formData.append('companypic', file, fileName);
        
        xhr.open('POST', '/profile/update/companyLogoForDocumentTemplate');
            xhr.onload = function () {
                var response = JSON.parse(xhr.response)
                if(response.SuccessCode){
                    scope.getFieldSetting(imageField, 'Absolute_Path').Value  = fileName;
                    imageField.Error = "";

                    $(".image-"+imageField.id).trigger("click");
                    $(".hide-msg").hide();
                }
                else {
                    imageField.Error = "Error while saving image"
                    $(".hide-msg").hide();
                    $(".image-"+imageField.id).trigger("click");
                }
        };
        xhr.send(formData);
    } else {

        if(file.size > 101000) {
            imageField.Error = "Upload image within 100KB"
        } else {
            imageField.Error = extension+" is not supported"
        }
        $(".image-"+imageField.id).trigger("click");
        $(".hide-msg").hide();
    }
};

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var performValidation = function(scope) {
    var error = false;

    scope.formFields.forEach(function(field) {
       if(field.Type === 'header') {
            if(field.Name === "" || field.Name == undefined) {
                error = true;
            }
        }
    });

    return !error;
}

$(function() {
    var dh = $(document).height();
    var grid_size = 10;
    $('#sidebar').height(dh - 115);
    $('#main-content').height(dh - 10);

    $('.focus-input').click(function(){
        $(this).focus();
    });

    $(document).ready(function(){
        $("body").on('DOMNodeInserted', '.drag', function () {
            // $(this).draggable({cancel:null,
            //                 containment:"#main-content"});
            var scope = angular.element(document.getElementById('document-template')).scope();
    
            $(this).draggable({cancel:null,
                // containment:"#main-content",
                drag: function( event, ui ) {
                    
                },
                grid: [ grid_size, grid_size ],
                stop:function(ev,ui){
                    var draggedEleId = Number($(ui.helper).attr("id"));
                    var draggedEle = _.find(scope.formFields, function(field) {
                                            return Number(field.id) == draggedEleId;
                                        });
                    scope.getElementWidth(draggedEle);
                    draggedEle.Style = $(ui.helper).attr("style");

                }});
            
            $('.focus-input').click(function() {
                $(this).focus();
            });
    
            $('.resize').resizable({
                // containment:"#main-content",
                grid: [ grid_size, grid_size ],
                stop:function(event, ui) {
                    var elementId = $(ui.helper).attr("id");
                    var resizedEleId = Number(elementId.split('-')[1]);
                    var resizedEle = _.find(scope.formFields, function(field) {
                                            return Number(field.id) == resizedEleId;
                                        });
                    resizedEle.DivStyle = $(ui.helper).attr("style");
                }
            });

            $('.resize-handle').resizable({
                // containment:"#main-content",
                handles: "e",
                // handles: 'ne, se, sw, nw',
                grid: [ grid_size, grid_size ],
                stop:function(event, ui) {
                    var elementId = $(ui.helper).attr("id");
                    var resizedEleId = Number(elementId.split('-')[1]);
                    var resizedEle = _.find(scope.formFields, function(field) {
                                            return Number(field.id) == resizedEleId;
                                        });
                    resizedEle.DivStyle = $(ui.helper).attr("style");
                }
            });
        });

        // @link https://tovic.github.io/color-picker/#section:extend
        $(document).on('click', '.docTemplateColorPicker', function (event) {
            var scope = angular.element(document.getElementById('document-template')).scope();
            var picker = new CP($(this)[0]);

            picker.on("change", function(color) {
                this.source.value = '#' + color;
                var field = scope.current_field;
                var colorLabelName = this.source.attributes["data-val"].value;
                
                scope.$apply(function() {
                    scope.getFieldSetting(field, colorLabelName).Value = '#' + color;
                })

            });
        })

    })

});

