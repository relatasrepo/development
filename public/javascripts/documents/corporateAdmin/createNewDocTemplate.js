var relatasApp = angular.module('relatasApp', ["ngLodash"]).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
}]);

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        liuDetails:function(user){
            this.liuData = user;
        },
        getUserId:function(){
            return this.userId
        },
        setUserEmailId:function(emailId){
            this.userEmailId = emailId;
        },
        setTeamMembers:function(members){
            this.team = members;
        },
        setTargetForFy:function(value){
            this.targetForFy = value;
        },
        getTeamMembers:function(){
            return this.team;
        },
        getLiuUserEmailId:function(){
            return this.userEmailId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setTown:function(geoLocationTown){
            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        }
    };
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){
        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

var timezone;
relatasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        share.companyDetails.currency.forEach(function (el) {
            share.currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                share.primaryCurrency = el.symbol;
            }
        });
    });
    getErrorMessages($rootScope,$http,"opportunity");
});

var docAttributeList = [];
relatasApp.controller('CreateDocTemplatesController', function($scope,$http,share) {
    
    documentsCommon($scope, share);

    var templateType = decodeURI(getParams(window.location.href).templateType);
    var templateName = decodeURI(getParams(window.location.href).templateName);
    var isActivated = getParams(window.location.href).isActivated;
    $scope.templateId = getParams(window.location.href).id;
    $scope.isTemplateVersionControlled = getParams(window.location.href).isVersionControlled;

    var guid = 0; 
    var companyImgUrl = "/docTemplateLogos/";
    var selectedTable = {}
    var selectedColIndex;
    
    $scope.docTemplateType = templateType === "undefined" ? "" : templateType;
    $scope.docTemplateName = templateName === "undefined" ? "" : templateName;
    $scope.isDocTemplateDeactivated = isActivated === "true" ? true :false;

    $scope.dragElements = [{
        'Name': "Textbox Title",
        'Type': "text",
        'Icon': "fa fa-text-height",
        'Error': "",
        "Editable": true,
        'Settings': [{
            'Name': 'Field Label',
            'Value': 'Textbox Title',
            'Type': 'text'
        }, {
            'Name': 'Placeholder',
            'Value': 'Textbox',
            'Type': 'text'
        }, {
            'Name': 'Field Type',
            'Value': 'String',
            'Type': 'dropdown',
            'PossibleValue': share.document.getFieldTypes
        }, {
            'Name': 'Formula',
            'Value': '',
            'Type': 'formula',
            'Visibility': false
        }, {
            'Name': 'System Referenced',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': share.document.getSystemReferencedEntities
        }, {
            'Name': 'Customer Profile Template Type',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Customer Profile Template Name',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'System Referenced Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Max Input Length',
            'Value': '50',
            'Type': 'text'
        }, {
            'Name': 'Column Span',
            'Value': '1',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']
        }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': share.document.getFieldGeneralOptions
        }]
    }, {
        'Name': "Date",
        'Value':'',
        'Type': "date",
        'Error': "",
        "Editable": true,
        'Icon': "fa fa-calendar-check-o",
        'Settings': [{
            'Name': 'Field Label',
            'Value': 'Date',
            'Type': 'text'
        }, {
            'Name': 'Column Span',
            'Value': '1',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']                
        }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': share.document.getFieldGeneralOptions
        }]
    }, {
        'Name': "Paragraph Text",
        'Value':'',
        'Type': "textarea",
        'Error': "",
        "Editable": true,
        'Icon': "fa fa-square-o",
        'Settings': [{
            'Name': 'Field Label',
            'Value': 'Paragraph Text', 
            'Type': 'text'
        }, {
            'Name': 'Placeholder',
            'Value': 'Paragraph',
            'Type': 'text'
        },{
            'Name': 'Field Type',
            'Value': 'String',
            'Type': 'dropdown',
            'PossibleValue': share.document.getFieldTypes
        },{
            'Name': 'Field Label',
            'Value': 'Paragraph Text',
            'Type': 'text'
        }, {
            'Name': 'Formula',
            'Value': '',
            'Type': 'formula',
            'Visibility': false
        },{
            'Name': 'System Referenced',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': share.document.getSystemReferencedEntities
        }, {
            'Name': 'Customer Profile Template Type',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Customer Profile Template Name',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'System Referenced Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Max Input Length',
            'Value': '50',
            'Type': 'text'
        },{
            'Name': 'Column Span',
            'Value': '1',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']                
        }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': share.document.getFieldGeneralOptions
        }]
    }, {
        'Name': "Image",
        "Type": "image",
        'Error': "",
        'Icon': "fa fa-picture-o",
        'Settings':[{
            'Name': 'Upload Image',
            'Value': '',
            'Type': 'file'
        },{
            'Name': 'Column Span',
            'Value': '1',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']                
        },{
            'Name': 'Relative_Path',
            'Value': '',
            'Type': 'url'
        },{
            'Name': 'Absolute_Path',
            'Value': '',
            'Type': 'url'
        }]
    }, {
        'Name': "Table",
        'Type': "table",
        'Icon': "fa fa-table",
        'Error': "",
        'Columns':[],
        'Rows':[],
        'Settings':[{
            'Name': 'Column Span',
            'Value': '4',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']                
        }]
    }, {
        'Name': "Header",
        'Type': "header",
        'Icon': "fa fa-header",
        'Error': "Header cannot be empty",
        'Settings':[{
            'Name': 'Field Label',
            'Value': 'Header',
            'Type': 'text'
        }, {
            'Name': 'Font Size',
            'Value': 'h3',
            'Type': 'dropdown',
            'PossibleValue': ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']                
        }, {
            'Name': 'Column Span',
            'Value': '2',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2', '3', '4']                
        }]
    }
    //  {
    //     'Name': "Divider",
    //     "Type": "divider",
    //     'Icon': "far fa-expand",
    //     'Settings':[{
    //         'Name': 'Orientation',
    //         'Value': 'Horizontal',
    //         'Type': 'dropdown',
    //         'PossibleValue': ['Horizontal', 'Vertical']
    //     }, {
    //         'Name': 'Width',
    //         'Value': '2',
    //         'Type': 'text'
    //     }, {
    //         'Name': 'Height',
    //         'Value': '4',
    //         'Type': 'text'
    //     },{
    //         'Name': 'Column Span',
    //         'Value': '4',
    //         'Type': 'dropdown',
    //         'PossibleValue': ['1', '2', '3', '4']
    //     },]
    // }
 ];

    share.getTemplateById = function(templateId) {
        $scope.formFields = [];
        $scope.current_field = {};

        $http.post('/documentTemplates/getById', {"templateId": templateId})
        .success(function (response) {
            if(response.Data.length > 0) {
                $scope.disableSave = response.Data[0].isDocumentCreated ? true : false;

                var attrList = response.Data[0];
                var fieldObject = {};

                attrList.docTemplateElementList.forEach(function(stringEle) {
                    fieldObject = JSON.parse(stringEle);

                    // Restoring the table
                    if(fieldObject.Type == 'table') {
                        fieldObject.Rows = [];
                        $scope.insertRows(fieldObject);
                    }

                    $scope.formFields.push(fieldObject);
                })

                guid = attrList.documentTemplateUiId || guid;

            }
        })
    }

    share.getTemplateById($scope.templateId)

    var createNewField = function() {
        return {
            'id': ++guid,
            'Name': '',
            'Settings': [],
            'Active': true,
            'Hover': false,
            'Style':'',
        };
    }

    var generateUniqImageName = function(imageField) {
        var relativeImagePath, absoluteImagePath;

        relativeImagePath = $scope.getFieldSetting(imageField, 'Relative_Path');
        relativeImagePath.Value = $scope.templateId+"_"+imageField.id+".jpg";

        absoluteImagePath = companyImgUrl+$scope.getFieldSetting(imageField, 'Relative_Path').Value;
        $scope.getFieldSetting(imageField, 'Absolute_Path').Value = absoluteImagePath;
    }

    $scope.removeElement = function(idx, event){
        if($scope.formFields[idx].Active) {
            $('#addFieldTab_lnk').tab('show');
            $scope.current_field = {};
        }
        $scope.formFields.splice(idx, 1);
        
    };

    $scope.addElement = function(ele, idx) {
        const defaultColumns = 3;

        $scope.current_field.Active = false;

        $scope.current_field = createNewField();
        angular.merge($scope.current_field, ele);
        
        if (typeof idx == 'undefined') {
            $scope.formFields.push($scope.current_field);
        } else {
            $scope.formFields.splice(idx, 0, $scope.current_field);
            $('#fieldSettingTab_lnk').tab('show');
        }

        switch($scope.current_field.Name) {
            case 'Table':
                    generateColumnsForTable($scope.current_field, defaultColumns);
                    break;
            case 'Image':
                    generateUniqImageName($scope.current_field);
                    break;
        }

    };

    $scope.activeField = function(f) {
        $scope.current_field.Active = false;
        $scope.current_field = f;
        // $('#addFieldTab_lnk').tab('show');
        f.Active = true;
        $('#fieldSettingTab_lnk').tab('show');
    };

    // $scope.editField = function() {
    //     $scope.current_field.Active = true;
    //     $('#fieldSettingTab_lnk').tab('show');
    // }

    $scope.activeFieldForFormula = function(f, index, event) {
        event.stopPropagation();

        var formulaString;

        if(f.Name !== 'Table' && f.Name !== 'Image' && f.Name !== 'Header' && f.Name !== 'Date') {

            formulaString = "f" + f.id + '+';
            $scope.getFieldSetting($scope.current_field, 'Formula').Value +=  formulaString;
        }

    }

    $scope.formulaFocus = function() {
        $scope.formulaMode = true;
    }

    $scope.formulaBlur = function(event, field) {
        // come out of formula mode when enter is pressed
        if(event.keyCode === 13) {
            $scope.formulaMode = false;
            $("#"+field.id+"-formula").blur();        
        }
    }

    $scope.registerDatePickerId = function(dateEle){
        var id = "date_"+dateEle.id;
        $('#'+id).datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            // minDate: $scope.startDt && id == "endDt"?new Date($scope.startDt):new Date(),
            onSelectDate: function (dp, $input){
                dp = new Date(dp)
                dp.setHours(23)
                dp.setMinutes(59)
                dp.setSeconds(59)
                
                $scope.$apply(function () {
                    $scope[id] = moment(dp).format("DD MMM YYYY");
                    $('#'+id).val($scope[id])
                });
            }
        });
    }

    // table methods
    $scope.columnData = {
        'Name': "",
        'Type': "column",
        'Error': "",
        "Editable": true,
        'Value':"",
        'Settings':[{
            'Name': 'Column Name',
            'Value': '',
            'Type': 'text'
        },{
            'Name': 'Field Type',
            'Value': 'String',
            'Type': 'dropdown',
            'PossibleValue': share.document.getFieldTypes,
        },{
            'Name': 'Formula',
            'Value': '',
            'Type': 'formula',
            'Visibility': false
        }, {
            'Name': 'System Referenced',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': share.document.getSystemReferencedEntities
        }, {
            'Name': 'Customer Profile Template Type',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Customer Profile Template Name',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'System Referenced Attributes',
            'Value': '',
            'Type': 'dropdown_reference',
            'Visibility': false,
            'PossibleValue': []
        },{
            'Name': 'Max Input Length',
            'Value': '50',
            'Type': 'text'
        },{
            'Name': 'Delete',
            'Value': 'Delete Column',
            'Type': 'button'
        }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': share.document.getFieldGeneralOptions
        }]
    }

    
    var createNewColumn = function() {
        return {
            'id': ++guid,
            'Name': '',
            'Settings': [],
            'Active' : true
        }
    };

    $scope.getColumnIndex = function(table, colIndex, event) {
        
        event.stopPropagation();
        $scope.activeField(table.Columns[colIndex]);
        selectedTable = table;
        selectedColIndex = colIndex;
        // $scope.deleteRow(table, colIndex);
    }

    $scope.addColumn = function(ele, idx, table) {

        $scope.current_field.Active = false;
        ele.Name  =  'col_'+idx;

        $scope.getFieldSetting(ele, 'Column Name').Value = ele.Name;
        $scope.current_field = createNewColumn();

        //Merge setting from template object
        angular.merge($scope.current_field, ele);
        
        if (typeof idx == 'undefined') {
            table.Columns.push($scope.current_field);
        } else {
            table.Columns.splice(idx, 0, $scope.current_field);
            $('#fieldSettingTab_lnk').tab('show');
        }

        //flush existing rows
        table.Rows = [];
        $scope.insertRows(table);
    };

    $scope.deleteColumn = function() {
        if(selectedTable && selectedColIndex !== undefined) {
            selectedTable.Columns.splice(selectedColIndex, 1);
        }

        //deleting columns in a default generated row in both rows
        selectedTable.Rows = [];
        $scope.insertRows(selectedTable);
    }

    $scope.addColumnForTable = function(table, event) {
        event.stopPropagation();
        if(table !== undefined) {
            $scope.addColumn($scope.columnData, table.Columns.length, table);
        }
    }

    var getTableHeaders = function(table, numberOfColumns) {
        if(numberOfColumns < table.Columns.length) {
        table.Columns.splice(numberOfColumns, table.Columns.length-numberOfColumns);
            
        } else if(numberOfColumns > table.Columns.length){
            for (var i=table.Columns.length; i<numberOfColumns; i++) {
                $scope.addColumn($scope.columnData, i, table);
            }
        }
    }

    var generateColumnsForTable = function(table, numberOfColumns) {
        getTableHeaders(table, numberOfColumns);
    }

    $scope.checkVisibility = function(fieldType, fieldSettings) {

        if(fieldType.Name === 'Number of columns') {
            generateColumnsForTable($scope.current_field, fieldType.Value);            
        }

        switch(fieldType.Value) {

            case 'Formula':
                $scope.getFieldSetting($scope.current_field, 'Formula').Visibility = true;      
                $scope.getFieldSetting($scope.current_field, 'System Referenced').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = false;
                break;

            case 'System Referenced':
                $scope.getFieldSetting($scope.current_field, 'Formula').Visibility = false;      
                $scope.getFieldSetting($scope.current_field, 'Formula').Value = "";  
                $scope.formulaMode = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced').Visibility = true;
                $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = true;
                break;

            case 'Boolean':
            case 'Numeric':
            case 'Alpha Numeric':
            case 'String':
                $scope.getFieldSetting($scope.current_field, 'Formula').Visibility = false;      
                $scope.getFieldSetting($scope.current_field, 'Formula').Value = ""; 
                $scope.formulaMode = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced').Visibility = false;
                $scope.getFieldSetting($scope.current_field, 'System Referenced Attributes').Visibility = false;

        }

        return true;
    }

    $scope.saveDocTemplate = function () {
        $scope.formulaMode = false;
        var tempElementList = [];

        $scope.formFields.forEach(function(element) {
            element.Style = $("#"+element.id).attr("style") || "";

            //Removing rows from the table
            if(element.Type == 'table')
                element.Rows = [];
            tempElementList.push(JSON.stringify(element));
        });


        if($scope.docTemplateType === "" || $scope.docTemplateType === undefined) {
            toastr.error("Template type is required")            
        } 
        else if($scope.docTemplateName === "" || $scope.docTemplateName === undefined) {
            toastr.error("Template name is required")            
        } else if(performValidation($scope)){            

            generateAttributeList();

            $http.post('/documentTemplatesElements/add/new', {"documentTemplateType":$scope.docTemplateType,
            "documentTemplateName":$scope.docTemplateName,
            "docTemplateElementList":tempElementList,
            "isDocTemplateDeactivated":false,
            "documentTemplateUiId":guid,
            "docTemplateAttrList": docAttributeList

        }).success(function(response) {
            if(response.SuccessCode == 1) {
                toastr.success("Document template saved successfully");
            } else {
                toastr.error("Error in saving document template")                                                        
            }
        });
        }

        $scope.closedocTemplate();
    }

    $scope.closedocTemplate = function() {
        window.location = "/documentTemplates/show/all";                      
    }

    $scope.createNewTemplate = function() {
        $scope.formFields = [];
        $scope.current_field = {};

        if($scope.docTemplateType === "" || $scope.docTemplateType === undefined) {
            toastr.error("Template type is required")            
        } 
        else if($scope.docTemplateName === "" || $scope.docTemplateName === undefined) {
            toastr.error("Template name is required")            
        } else {

            $http.post('/document/template/create/new', {"documentTemplateType":$scope.docTemplateType,
                                                        "documentTemplateName":$scope.docTemplateName})
                .success(function(response) {
                    $scope.templateId = response.Template._id;
                });
        }

    }

    $scope.activateDocTemplate = function () {
        var deactivated = false;
        $http.post('/documentTemplates/updateIsDocTemplateDeactivated', {"documentTemplateType":$scope.docTemplateType,
                                                                            "templateId":$scope.templateId,
                                                                            "isDocTemplateDeactivated":deactivated
        })
        $scope.isDocTemplateDeactivated = !$scope.isDocTemplateDeactivated; 
    }

    $scope.deactivateDocTemplate = function () {
        var deactivated = true;

        $http.post('/documentTemplates/updateIsDocTemplateDeactivated', {"documentTemplateType":$scope.docTemplateType,
                                                                            "templateId":$scope.templateId,
                                                                            "isDocTemplateDeactivated":deactivated
        })

        $scope.isDocTemplateDeactivated = !$scope.isDocTemplateDeactivated; 
    }
    
    $scope.setDocumentTemplateAsDefault = function () {
        var isDefault = true;
        $http.post('/documentTemplates/setDocumentTemplateAsDefault', {
            "documentTemplateType":$scope.docTemplateType,
            "templateId":$scope.templateId,
            "isDefault":isDefault
        }).success(function(response) {
            toastr.success("This template saved as default successfully");
        }) 
    }

    // added Sachin for setting the Template as Default
    $scope.setDocumentTemplateAsVersionControlled = function () {
        var isVersionControlled = true;
        $http.post('/documentTemplates/setDocumentTemplateAsVersionControlled', {
            "documentTemplateType":$scope.docTemplateType,
            "templateId":$scope.templateId,
            "isTemplateVersionControlled":isVersionControlled
        })

        window.location = "/documentTemplates/show/all";
    }

    $scope.resetDocumentTemplateAsVersionControlled = function () {
        var isVersionControlled = false;
        $http.post('/documentTemplates/setDocumentTemplateAsVersionControlled', {
            "documentTemplateType":$scope.docTemplateType,
            "templateId":$scope.templateId,
            "isTemplateVersionControlled":isVersionControlled
        })
        window.location = "/documentTemplates/show/all";
    }

    $scope.createNewDocument = function() {
        var docTemplateType = templateType;
        var docTemplateName = templateName;
        var id = $scope.templateId;
        window.location = "/document/create/new?templateId="+id+"&templateType="+docTemplateType+"&templateName="+docTemplateName+"&documentCreatedBy="+share.getUserId();
    }

    $scope.resize = function(evt,ui) {
      }

    $scope.setReferencedTemplateType = function (settings) {
        console.log("setReferencedTemplateType:", settings);

        console.log("setReferencedTemplateType called with", settings.Name,settings.Value )
        if(settings.Name === 'System Referenced' && settings.Value === 'Opportunity') {
            $scope.changeFieldSetting(share.getOpportunityReferencedAttributes , 'System Referenced Attributes');
            enableSettingsForCustomerProfile(false);

        } else if(settings.Name === 'System Referenced' && settings.Value === 'Customer Profiles') {
            getCustomerProfileTemplateTypes(function (cpTypes) {
                $scope.changeFieldSetting(cpTypes , 'Customer Profile Template Type');
            });

            enableSettingsForCustomerProfile(true);
        } else if(settings.Name === 'System Referenced' && settings.Value === 'ML Account Insights') {

            $scope.changeFieldSetting(share.getMLAccountInsightsReferencedAttributes , 'System Referenced Attributes');
            enableSettingsForCustomerProfile(false);

        }

        if(settings.Name === 'Customer Profile Template Type' && settings.Value != '') {
            $scope.cpTemplateType = settings.Value;

            getCustomerProfileTemplateNames(settings.Value ,function (cpNames) {
                $scope.changeFieldSetting(cpNames , 'Customer Profile Template Name');

            });
        }

        if(settings.Name === 'Customer Profile Template Name' && settings.Value != '') {

            getCustomerProfileTemplateAttributes($scope.cpTemplateType,
                                                    settings.Value ,
                                                    function (cpAttributes) {
                $scope.changeFieldSetting(cpAttributes , 'System Referenced Attributes');

            });
        }
    }

    var enableSettingsForCustomerProfile = function (visibility) {
        $scope.getFieldSetting($scope.current_field, 'Customer Profile Template Type').Visibility = visibility;
        $scope.getFieldSetting($scope.current_field, 'Customer Profile Template Name').Visibility = visibility;

        if(visibility) {
            $scope.changeFieldSetting([] , 'System Referenced Attributes');
        }
    }

    var getCustomerProfileTemplateTypes = function (callback) {
        var cpTypes = [];

        $http.get('/customerProfileTemplates/getCustomerProfileTemplateTypes')
            .success(function (response) {
                if(response.DataTypes.length > 0) {

                    response.DataTypes.forEach(function (dataType) {
                        cpTypes.push(dataType._id);
                    })

                    callback(cpTypes);
                }
            })
    }

    var getCustomerProfileTemplateNames = function (templateType, callback) {
        var cpNames = [];

        $http.post('/customerProfileTemplates/getAllCustomerProfileTemplateNamesByType',
                                        {"customerProfileTemplateType": templateType})
            .success(function (response) {
                if(response.TemplateNames.length > 0) {

                    response.TemplateNames.forEach(function (templateName) {
                        cpNames.push(templateName._id)
                    })

                    callback(cpNames);
                }
            })
    }

    var getCustomerProfileTemplateAttributes = function (templateType,templateName,callback) {
        var cpAttributes = [];

        $http.post('/customerProfileTemplates/getAllCustomerProfileTemplateAttributes',
                                        {"customerProfileTemplateType": templateType,
                                        "customerProfileTemplateName": templateName},)
            .success(function (response) {
                if(response.TemplateAttributes.length > 0) {
                    cpAttributes = response.TemplateAttributes[0]._id;
                    callback(cpAttributes);
                }
                else {
                }
            })
    }
});

relatasApp.directive('elementDraggable', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {
            element.on('dragstart', function(event) {

                event.originalEvent.dataTransfer.setData('templateIdx', $(element).data('index'));
            });
        }
    };
}]);

relatasApp.directive('elementDrop', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {

            element.on('dragover', function(event) {
                event.preventDefault();
            });

            $('.drop').on('dragenter', function(event) {
                event.preventDefault();
            })
            element.on('drop', function(event) {
                event.stopPropagation();
                var self = $(this);
                scope.$apply(function() {
                    var idx = event.originalEvent.dataTransfer.getData('templateIdx');
                    var insertIdx = self.data('index')
                    scope.addElement(scope.dragElements[idx], insertIdx);
                });
            });
        }
    };
}]);

relatasApp.directive("fileinput", [function() {
    return {
        scope: {
        fileinput: "=",
        filepreview: "="
    },
    link: function(scope, element, attributes) {

        element.bind("change", function(changeEvent) {
        scope.fileinput = changeEvent.target.files[0];
        var reader = new FileReader();
        reader.onload = function(loadEvent) {
        scope.hideInput = "hide-input"
        upload(scope.fileinput, scope.filepreview, scope);
        scope.$apply(function() {
            // scope.filepreview = loadEvent.target.result;
            upload(scope.fileinput, scope.filepreview, scope.filepreview, scope)
        });
        }
        reader.readAsDataURL(scope.fileinput);
        });
    }
}
}]);

var upload = function(file, fileName, scope){
    var formData = new FormData();
    var xhr = new XMLHttpRequest(); 
    var imageName = file.name;
    var extension = imageName.split('.')[imageName.split('.').length-1];

    formData.append('companypic', file, fileName);
    
    xhr.open('POST', '/profile/update/companyLogoForDocumentTemplate');
        xhr.onload = function () {
            var response = JSON.parse(xhr.response)
            if(response.SuccessCode){
            }
            else {
                toastr.error(response.Message)
            }
    };
    xhr.send(formData);
};

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var performValidation = function(scope) {
    var error = false;

    scope.formFields.forEach(function(field) {
       if(field.Type === 'header') {
            if(field.Name === "" || field.Name == undefined) {
                error = true;
            }
        }
    });

    return !error;
}

$(function() {
    // // Code here
    var dh = $(document).height();
    $('#sidebar-tab-content').height(dh - 115);
    $('#main-content').height(dh - 10);

    $('.drag').draggable({
        cancel:null,
        containment:"#main-content",
        scroll: false
    });    
    
    $('.resize').resizable();    

    $('.drag').click(function(){
        $(this).focus();
    });

    $("body").on('DOMNodeInserted', '.drag', function () {
        $(this).draggable({cancel:null});
        $(this).click(function() {
            $(this).focus();
        });

    });

});