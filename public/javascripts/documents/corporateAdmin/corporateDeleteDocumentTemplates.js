var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','ngLodash']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

function Communicator(){
    return {}
}

relatasApp.controller("deleteDocumentTemplates", function ($scope,$http,$rootScope){
    paintMenu($scope);
    fetchDocumentTemplates($scope, $http);
    // getCompanyDetails($scope,$http);

    Communicator.prototype.refreshData = function () {
        getCompanyDetails($scope,$http);
    }

    $rootScope.isCorporateAdmin = true;

    $scope.selectMenu = function (item) {
        $scope.currentSelection = item.name;
        item.selected = "selected";
        resetOtherSelection($scope,item)
    }

    $scope.tableHeaderName = 'opportunityName';
    $scope.reverse = true;

    $scope.sortBy = function(tableHeaderName) {
        $scope.reverse = ($scope.tableHeaderName === tableHeaderName) ? !$scope.reverse : false;
        $scope.tableHeaderName = tableHeaderName;
    }

    $scope.makeContactMandatory = function(type){
        var url = '/corporate/admin/update/customer/setting';
        $http.post(url,$scope.contactSettings)
            .success(function (response) {
            });

    }
    $scope.editXr = function (currency) {
        currency.editThis = true;
    }

    $scope.transferOpps = function () {
        var opps = _.filter($scope.oppList,"select")
        startOppTransfer($scope,$http,opps)
    }

    $scope.deleteDocumentTemplates = function () {
        var documentTemplates = _.filter($scope.documentTemplateList,"select")

        if (window.confirm('This operation will permanently delete the document templates. Are you sure you want to delete the selected document templates?')) {
            $http.post("/corporate/admin/delete/document/templates",_.map(documentTemplates,"_id"))
                .success(function (response) {
                    alert("The selected document templates were successfully deleted.")
                    fetchDocumentTemplates($scope, $http);
                })
        }
    }

    $scope.openPopup = function (){
        $scope.showPopup = !$scope.showPopup
    }

    $scope.toggleSelectionDocumentTemplates = function () {
        $scope.documentTemplateList.forEach(function (el) {
            el.select = !el.select
        })
    }

    getTeamMembers($scope,$http);

});

function getTeamMembers($scope,$http){

    $scope.data = {
        model: null
    };

    $http.get("/corporate/admin/all/team/members")
        .success(function (response) {
            $scope.data.teamMembers = response;

            $scope.teamObj = {};

            $scope.data.teamMembers.forEach(function (el) {
                el.name = el.firstName +" "+el.lastName;
                $scope.teamObj[el._id] = el;
            });

            $scope.data.teamMembers.sort(function (a,b) {
                if(a.firstName < b.firstName) return -1;
                if(a.firstName > b.firstName) return 1;
                return 0;
            })
        })
}

function resetOtherSelection($scope,currentSelection) {
    $scope.menu.forEach(function (el) {
        if(el.name !== currentSelection.name){
            el.selected = ""
        }
    })
}

function paintMenu($scope) {

    $scope.menu = [{
        name:"Delete",
        selected:"selected"
    }]

    $scope.menu.sort(function (a,b) {
        if(a.name < b.name) return -1;
        if(a.name > b.name) return 1;
        return 0;
    })

    if(!$scope.currentSelection) {
        $scope.currentSelection = $scope.menu[0].name;
    }
}

function fetchDocumentTemplates($scope,$http) {
    $scope.loadingDocumentTemplates = true;
    $http.get("/documentTemplates/get/all/data")
        .success(function (response) {
            console.log("Document Templates:", response);

            $scope.documentTemplateList = response.Data;
            $scope.loadingDocumentTemplates = false;
        })
}