function initServices($scope, $http,$rootScope,share,searchService,$sce){

    $scope.oppDetailsNav = oppDetails();
    $scope.selectedTab = $scope.oppDetailsNav[0];
    $scope.viewFor = $scope.oppDetailsNav[0].name.toLowerCase();
    $scope.reasonsRequired = true;

    $scope.updateTaskName = function (task,taskName) {
        if($scope.task){
            $scope.task.taskName = taskName
        }
    }

    $scope.updateTaskPriority = function (priority) {
        if($scope.task){
            $scope.task.priority = priority
        }
    }

    $scope.toggleContactPopup = function() {
        $scope.showContactCreateModal = false;
    }

    $scope.createContactWindow = function() {
        $scope.showContactCreateModal = true;
        $scope.cc = {};
    }

    $scope.saveContact = function() {

        if(!$scope.coords){
            $scope.coords = {}
        }

        var data = {
            personName:$scope.cc.fullName,
            personEmailId:$scope.cc.p_emailId,
            companyName:$scope.cc.companyName,
            designation:$scope.cc.designation,
            mobileNumber:$scope.cc.p_mobileNumber,
            // location:$scope.coords.address,
            location:$scope.cc.location,
            lat:$scope.coords.lat,
            lng:$scope.coords.lng
        }

        var errExists = false;

        if(!data.personName){
            errExists = true
            toastr.error("Please enter a name for the contact.")
        } else if(!validateEmail(data.personEmailId)){
            toastr.error("Please enter a valid email ID.")
            errExists = true
        }

        if(!checkRequired(data.location)){
            errExists = true
            toastr.error("Please enter a city for this contact.")
        }

        if(!errExists){
            $http.post("/contacts/create",data)
                .success(function (response) {
                    toastr.success("Contact added successfully.")
                    $scope.showContactCreateModal = false;
                })
        }
    }

    $scope.openViewFor = function (viewFor) {

        mixpanelTracker("Opps>Edit>Tab "+viewFor.name);
        if(viewFor.closeOpp == 'de-active'){
            alert("Close Details available when the the opportunity stage is 'Closed'")
        } else if(viewFor.isNewOpp == 'de-active'){
            alert("Please SAVE the opportunity before you add the first NOTE for this opportunity")
        } else if(viewFor.isNewOpp == 'de-active'){
            alert("Please SAVE the opportunity before you add the first TASK for this opportunity")
        } else {
            $scope.viewFor = viewFor.name.toLowerCase();
            menuToggleSelection(viewFor.name,$scope.oppDetailsNav)

            if($scope.viewFor == 'documents') {
                getLinkedDocuments($scope, $http);
            }
        }
    }

    $scope.checkErrorMessageStatus = function () {
        for(var key in $scope.opp){
            if($scope.opp[key] && $scope.opp[key].length>0){
                $scope.opp[key+"Req"]= false;
            }
        }
    }

    $scope.goToBasic = function () {

        _.each($scope.oppDetailsNav,function (el) {
           if(el.name == "Basic"){
               el.selected = "selected"
           } else {
               el.selected = ""
           }
        });
        $scope.selectedTab = $scope.oppDetailsNav[0];
        $scope.viewFor = $scope.oppDetailsNav[0].name.toLowerCase();
    }

    initTaskObj($scope)
    setDocumentsTableHeader($scope);

    $scope.startDateFormatted = moment().format("DD MMM YYYY")
    $scope.dueDateFormatted = "Select"

    $scope.assignList = [];
    $scope.removeFromToList = function (contact) {
        $scope.toList = $scope.toList.filter(function (item) {
            return item.emailId != contact.emailId;
        })
    }

    $scope.closeNotesModule = function (){
        $scope.showNotesModule = false;
        $scope.note = "";
        clearInputFields();
    }

    $scope.closeTaskCreator = function () {
        $scope.createNewTask = false
        $rootScope.createFormIsOpen = false;
        $("div[id^='taTextElement']").empty();
    }

    $scope.selectThisAccount = function (account,group) {
        selectThisAccount($scope,$http,share,account,group)
    }

    $scope.addItems = function () {
    }

    $scope.searchForAccountName = function (account) {
        searchForAccountName($scope,$http,share,account)
    }

    $scope.removeFromAssigneeList = function (contact) {
        $scope.assignList = $scope.assignList.filter(function (item) {
            return item.emailId != contact.emailId;
        })
    }

    $scope.pickDueDate = function () {
        pickDateTime($scope,new Date(),null,".selectDate",false,"dueDate",false)
    }

    $scope.setTaskCompletion = function (task) {

        if(task.status == "complete"){
            task.completed = false;
            task.status = "inProgress"
            task.statusFormatted = $scope.taskStatusObj[task.status]
        } else {
            task.status = "complete"
            task.completed = true
            task.statusFormatted = $scope.taskStatusObj[task.status]
        }

        $http.post('/task/update/properties',task)
            .success(function (response) {
            });
    }

    $rootScope.toolBarList = [
        ['h1','h2','h3'],
        ['bold','italics'],
        ['ul','ol'],
        ['indent','outdent'],
        // ['justifyLeft','justifyCenter','justifyRight','justifyFull']
    ]

    getRoles($scope,$http,function (roles) {
        $scope.rolesList = roles;

        if(!share.teamMembers){
            share.teamMembers = share.companyMembers;
        }

        checkTeamLoaded();

        function checkTeamLoaded(){
            if(share.teamMembers && share.teamMembers.length>0){
                var otherUsers = _.differenceBy(share.teamMembers,_.flatten(_.map($scope.rolesList,"users")),"emailId");
                $scope.rolesList.push({
                    roleName:"Others",
                    users:otherUsers,
                    nonEditable:true
                });
            } else {
                setTimeOutCallback(200,function () {
                    checkTeamLoaded()
                })
            }
        }
    })

    $scope.searchForUser = function (query,role) {
        searchForUser(query,role)
    }

    $scope.openTaskModule = function () {
        $scope.createNewTask = !$scope.createNewTask;
        initTaskObj($scope);
    }

    $scope.openNotesModule = function () {
        $scope.showNotesModule = !$scope.showNotesModule
    }

    $scope.addNote = function (note) {
        addNote($scope,$http,note,$scope.opp.opportunityId)
    }

    $scope.searchForTeamMember = function (query) {

        checkTeamLoaded();

        function checkTeamLoaded(){
            if(share.teamMembers){
                $scope.searchList = findTeamMember(query,share.teamMembers)
            } else {
                setTimeOutCallback(200,function () {
                    checkTeamLoaded()
                })
            }
        }
    }

    $scope.searchForTeamMemberParticipants = function (query) {

        checkTeamLoaded();

        function checkTeamLoaded(){
            if(share.teamMembers){
                $scope.searchListParticipants = findTeamMember(query,share.teamMembers)
            } else {
                setTimeOutCallback(200,function () {
                    checkTeamLoaded()
                })
            }
        }
    }

    $scope.setTaskOwner = function (user) {

        if($scope.opp){

            if(!$scope.opp.usersWithAccess){
                $scope.opp.usersWithAccess = []
            }

            var userWithAccess = [{emailId:$scope.opp.userEmailId}];
            userWithAccess = $scope.opp.usersWithAccess.concat(userWithAccess);

            if(!checkUsersWithAccess(userWithAccess,user)){
                if (window.confirm(user.fullName+' is not part of this opportunity. Please add to the Internal Team')) {
                    $scope.assignTo = ""
                    $scope.searchListTaskOwner = []
                }
            } else {
                $scope.assignList.push(user)
                $scope.assignTo = ""
                $scope.searchListTaskOwner = []
            }
        } else {
            $scope.assignList.push(user)
            $scope.assignTo = ""
            $scope.searchListTaskOwner = []
        }
    }

    $scope.addTeamMembersToTask = function (user) {

        if($scope.opp){

            if(!$scope.opp.usersWithAccess){
                $scope.opp.usersWithAccess = []
            }

            var userWithAccess = [{emailId:$scope.opp.userEmailId}];
            userWithAccess = $scope.opp.usersWithAccess.concat(userWithAccess)

            if(!checkUsersWithAccess(userWithAccess,user)){
                if (window.confirm(user.fullName+' is not part of this opportunity. Please add to the Internal Team')) {
                    $scope.keyword = ""
                    $scope.searchListParticipants = []
                }
            } else {
                $scope.toList.push(user)
                $scope.keyword = ""
                $scope.searchListParticipants = []
            }
        } else {
            $scope.toList.push(user)
            $scope.keyword = ""
            $scope.searchListParticipants = []
        }
    }

    $scope.searchForTeamMemberTaskOwner = function (query) {

        checkTeamLoaded();

        function checkTeamLoaded(){

            if(share.teamMembers){
                $scope.searchListTaskOwner = findTeamMember(query,share.teamMembers);
            } else {
                setTimeOutCallback(200,function () {
                    checkTeamLoaded()
                })
            }
        }
    }

    $scope.saveTask = function () {

        $scope.task.startDate = $scope.startDate
        $scope.task.dueDate = $scope.dueDate
        $scope.task.assignedToEmailId = $scope.assignList && $scope.assignList[0]?$scope.assignList[0].emailId:null;
        $scope.task.participants = $scope.toList;
        $scope.task.refId = $scope.opp.opportunityId;

        createNew($scope,$http,share,$scope.task,function () {
            $scope.createNewTask = false;
            clearInputFields();
            getAllRelatedData($scope,$http,share,$scope.opp)
        });
    }

    $scope.setOppOwner = function (user) {

        if(share.team){
            $scope.oppOwner.value = share.team[user.emailId].fullName+" ("+share.team[user.emailId].emailId +")";
        } else if(share.usersDictionary){
            $scope.oppOwner.value = share.usersDictionary[user.emailId].fullName+" ("+share.usersDictionary[user.emailId].emailId +")";
        }

        var prevOwner = share.team[$scope.opp.userEmailId];
        $scope.opp.userEmailId = user.emailId;
        $scope.opp.userId = user.userId;
        $scope.oppOwner.value = share.team[user.emailId].fullName+" ("+share.team[user.emailId].emailId +")";
        $scope.searchList = [];

        var addUserToRole = null
            ,roleObj = {};

        _.each($scope.rolesList,function (rl) {
            _.each(rl.users,function (el) {
                if(el.emailId == prevOwner.emailId){
                    addUserToRole = rl.roleName;
                    roleObj = rl;
                }
            })
        })

        if(!addUserToRole){
            addUserToRole == "Others"
        }

        $scope.addUser(roleObj,prevOwner);
    }
    
    $scope.removeUser = function (role,opp,user) {

        if(user){
            opp.usersWithAccess = opp.usersWithAccess.filter(function (el) {
                return el.emailId != user.emailId
            })

            role.usersWithAccess = role.usersWithAccess.filter(function (el) {
                return el.emailId != user.emailId
            })
        }
    }

    $scope.addUser = function (role,user) {

        if(!role.usersWithAccess){
            role.usersWithAccess = [];
        }

        if(!$scope.opp.usersWithAccess){
            $scope.opp.usersWithAccess = [];
        }

        role.usersWithAccess.push(user);

        $scope.opp.usersWithAccess.push({
            emailId:user.emailId,
            fullName:user.fullName,
            accessGroup:role.roleName
        });

        $scope.opp.usersWithAccess = _.uniqBy($scope.opp.usersWithAccess,"emailId")
        role.usersWithAccess = _.uniqBy(role.usersWithAccess,"emailId")
        clearInputFields();
        role.searchList = [];
    }
}

function setDocumentsTableHeader($scope){
    $scope.hideSearchBar = true;
    // $scope.docHeaders = ["S.No.", "Latest Version", "Document Template Type", "Document Template Name", "Document Name", "Created By", "Created Date", "Updated Date",];

    $scope.docHeaders = [
        {
            name:"S.No.",
            styleWidth:"width:"+4+"px"+"; cursor:pointer",
            align:"text-right"

        },
        {
            name:"Version",
            styleWidth:"width:"+4+"px"+"; cursor:pointer",
            align:"text-right"
        },
        {
            name:"Type",
            styleWidth:"width:"+225+"px"+"; cursor:pointer"
        },
        {
            name:"Name",
            styleWidth:"width:"+225+"px"+"; cursor:pointer"
        },
        {
            name:"Created By",
            styleWidth:"cursor:pointer"
        },
        {
            name:"Created Date",
            styleWidth:"width:"+90+"px"+"; cursor:pointer"
        },
        {
            name:"Updated Date",
            styleWidth:"width:"+90+"px"+"; cursor:pointer"
        },
        {
            name:"Stage",
            styleWidth:"width:"+90+"px"+"; cursor:pointer"
        }]
}

function getLinkedDocuments($scope, $http) {
    $scope.loadingDocuments = true;
    var opportunityId = $scope.opp.opportunityId;
    $scope.filteredDocuments = [];

    $http.get('/documents/get/linked?refType="opportunity&refId='+opportunityId)
        .success(function (response) {
            $scope.loadingDocuments = false;

            var allDocuments = response.Data;
            allDocuments.forEach(function(el) {
                el.documentCreatedDate = moment(el.documentCreatedDate).format("DD MMM YYYY");
                el.documentUpdatedDate = moment(el.documentUpdatedDate).format("DD MMM YYYY");
                if(el.documentVersion == el.currentDocumentVersion){
                    $scope.filteredDocuments.push(el);
                }
            })
        });
}

function addUser(role,user) {

    //TODO add to opp
}

function searchForUser(query,role){

    if(query && query.length>0){
        query = query.toLowerCase();

        var results = role.users.map(function (el) {

            if(_.includes(el.emailId.toLowerCase(),query)){
                return el;
            } else if(el.fullName && _.includes(el.fullName.toLowerCase(),query)){
                return el;
            } else if(el.firstName && _.includes(el.firstName.toLowerCase(),query)){
                return el;
            } else if(el.lastName && _.includes(el.lastName.toLowerCase(),query)){
                return el;
            }
        });

        role.searchList = _.compact(results);
    } else {
        role.searchList = []
    }
}

function findTeamMember(query,userList){

    var searchList = [];

    if(query && query.length>0){
        query = query.toLowerCase();

        var results = userList.map(function (el) {

            if(_.includes(el.emailId.toLowerCase(),query) || (el.fullName && _.includes(el.fullName.toLowerCase(),query)) || (el.firstName && _.includes(el.firstName.toLowerCase(),query))){
                return el;
            }
        });

        searchList = _.compact(results);
    } else {
        searchList = []
    }

    return searchList;
}

function oppDetails(share){

    return [{
        name:"Basic",
        selected:"selected"
    },{
        name:"Customer",
        selected:""
    },{
        name:"Internal Team",
        selected:""
    },{
        name:"Tasks",
        isNewOpp:"de-active",
        selected:""
    },{
        name:"Notes",
        isNewOpp:"de-active",
        selected:""
    },{
        name:"Close Details",
        selected:"",
        closeOpp:"de-active"
    },{
        name:"Documents",
        selected:""
    },{
        name:"Activity Log",
        selected:""
    }]
}

function menuToggleSelection(select,menu) {

    _.each(menu,function (item) {
        if(item.name == select){
            item.selected = "selected"
        } else {
            item.selected = ""
        }
    })
}

function getRoles($scope,$http,callback) {
    $http.get('/organisation/get/roles')
        .success(function (response) {

            if(response && response.Data){
                callback(response.Data)
            } else {
                callback([])
            }
        });
}

function mapUsersWithAccessToOrgRoles($scope,share,opp){

    checkIfOrgRolesLoaded();

    function checkIfOrgRolesLoaded(){
        
        if(($scope.rolesList && $scope.rolesList.length>0) && (share.team || share.usersDictionary )){
            if(share.usersDictionary){
                _.each($scope.rolesList,function (role) {
                    role.usersWithAccess = [];
                    _.each(opp.usersWithAccess,function (user) {
                        if(role.roleName == user.accessGroup){
                            if(share.usersDictionary[user.emailId]){
                                role.usersWithAccess.push(share.usersDictionary[user.emailId])
                            } else {
                                role.usersWithAccess.push({
                                    fullName:user.emailId
                                })
                            }
                        }
                    });
                });
            } else if(share.team){

                _.each($scope.rolesList,function (role) {
                    role.usersWithAccess = [];
                    _.each(opp.usersWithAccess,function (user) {
                        if(role.roleName == user.accessGroup){
                            if(share.team[user.emailId]){
                                role.usersWithAccess.push(share.team[user.emailId])
                            } else {
                                role.usersWithAccess.push({
                                    fullName:user.emailId
                                })
                            }
                        }
                    });
                });
            }

        } else {
            setTimeOutCallback(100,function () {
                checkIfOrgRolesLoaded();
            })
        }
    }
}

function addNote($scope,$http,contactNote,oppId) {

    contactNote = nl2br(contactNote)

    var url = '/message/create/by/id';

    var obj = {
        messageReferenceType:"opportunity",
        text:contactNote,
        conversationId:oppId
    }

    $http.post(url,obj)
        .success(function (response) {
            if(response.SuccessCode){
                var a = response.Data;

                a.text = a.text.replace(/<br ?\/?>/g, "\n")
                a.dateFormatted = a.date ? moment(a.date).format("DD MMM YYYY") : null;
                $scope.notes.unshift(a)
            }
            else {
                toastr.error("Failed to add note, try again later");
            }

            $scope.showNotesModule = false;
            $scope.note = "";
            clearInputFields();
        });
}

function getNotes($scope,$http,conversationId) {

    var url = "/get/messages/by/conversation/id?conversationId="+conversationId;

    $http.get(url)
        .success(function (response) {
            if(response.SuccessCode){

                $scope.notes = response.Data

                $scope.notes.forEach(function(a) {
                    a.text = a.text.replace(/<br ?\/?>/g, "\n")
                    a.dateFormatted = a.date ? moment(a.date).format("DD MMM YYYY") : null;
                });

                $scope.notes.sort(function (o1, o2) {
                    return o1.date < o2.date ? 1 : o1.date > o2.date ? -1 : 0;
                });
            }
        })
}

function getTasks($scope,tasks,share) {

    $scope.taskStatusObj = {
        notStarted:"Not Started",
        complete:"Complete",
        inProgress:"In Progress"
    }

    $scope.tasks = tasks

    $scope.tasks.forEach(function(a) {
        if(share && share.usersDictionary){
            a.profile = share.usersDictionary[a.assignedToEmailId]
        }

        a.statusFormatted = $scope.taskStatusObj[a.status];

        if(a.status == "complete"){
            a.completed = true
        }
        a.dateFormatted = a.dueDate ? moment(a.dueDate).format("DD MMM YYYY") : null;
    });

    $scope.tasks.sort(function (o1, o2) {
        return o1.dueDate < o2.dueDate ? 1 : o1.dueDate > o2.dueDate ? -1 : 0;
    });
}

function pickDateTime($scope,minDate,maxDate,id,timezone,scopeSelector,timeRequired){

    var settings = {
        value:"",
        timepicker:timeRequired?true:false,
        validateOnBlur:false,
        onSelectDate: function (dp, $input){
            $scope.$apply(function () {
                $(".xdsoft_datetimepicker").hide();
                var timezone = timezone?timezone:"UTC";
                $scope[scopeSelector] = moment(dp).tz(timezone).format();
                $scope[scopeSelector+'Formatted'] = moment(dp).tz(timezone).format("DD MMM YYYY");
            });
        }
    }

    if(minDate){
        settings.minDate = minDate;
    }

    $(id).datetimepicker(settings);
}

function initTaskObj($scope){
    $scope.assignTo = ""
    $scope.toList = []
    $scope.searchListTaskOwner = []
    $scope.searchListParticipants = []

    $scope.task = {
        taskName:"",
        description:"",
        priority:null,
        startDate:null,
        dueDate:null,
        toList:[],
        participants:[],
        taskFor:"opportunities"
    }

}

function getAccountTypes($scope,$http,share,opp){

    if(!$scope.accountTypes || ($scope.accountTypes && $scope.accountTypes.length === 0)){
        $scope.accountTypes = [];
        $http.get("/account/master/types/by/group")
            .success(function (response) {
                if(response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.accountTypes = response.Data;
                    setAccounts($scope,$scope.accountTypes,opp)
                }
            })
    } else {
        setAccounts($scope,$scope.accountTypes,opp)
    }
}

function setAccounts($scope,accounts,opp){

    _.each(opp.accounts,function (opAcc) {
        _.each(accounts,function (coAcc) {
            if(opAcc.group === coAcc._id){
                coAcc.searchText = opAcc.name
            }
        })
    })
}

function getAllRelatedData($scope,$http,share,opp){
    $http.get("/opportunity/related/data/by/id?opportunityId="+opp.opportunityId)
        .success(function (response) {
            if(response.SuccessCode){
                if((/commits/.test(window.location.pathname))){
                } else {
                    formatLogs($scope,$http,share,opp,response.logs);

                }
                getTasks($scope,response.tasks,share);

                $scope.notes = response.notes

                $scope.notes.forEach(function(a) {
                    a.text = a.text.replace(/<br ?\/?>/g, "\n")
                    a.dateFormatted = a.date ? moment(a.date).format("DD MMM YYYY") : null;
                });

                $scope.notes.sort(function (o1, o2) {
                    return o1.date < o2.date ? 1 : o1.date > o2.date ? -1 : 0;
                });
            }
        })
}

function searchForAccountName($scope,$http,share,account){
    if(account.searchText && account.searchText.length>2){
        $http.post("/account/master/types/search",account)
            .success(function (response) {
                account.accountList = response.Data;
            })
    }
}

function selectThisAccount($scope,$http,share,account,group){

    group.searchText = account.name;

    if(!$scope.opp.accounts){
        $scope.opp.accounts = []
    }
    $scope.opp.accounts.push({name:account.name,group:group._id});

    _.uniqBy($scope.opp.accounts,"group");
    group.accountList = [];

}

function checkUsersWithAccess(usersWithAccess,user) {

    var exists = false;
    _.each(usersWithAccess,function (el) {
        if(el.emailId === user.emailId){
            exists = true;
            return false;
        }
    });

    return exists

}

function sendOpportunityCloseSummary($scope,$http,share,$rootScope, response) {
    var daysToClose = moment.duration(moment(new Date()).diff(moment($scope.opp.createdDate)));

    if(response.companyHead && response.reportingManager){

        if($scope.opp.mailRm || $scope.opp.mailOrgHead){

            var subject = "Deal worth "+share.primaryCurrency+" "+$scope.opp.amount+ " successfully closed."
            if($scope.opp.stage == "Close Lost"){
                subject = "Deal worth "+share.primaryCurrency+" "+$scope.opp.amount+ " Lost."
            }

            var intro = '';

            var contactEmailId = $scope.opp.contactEmailId ? $scope.opp.contactEmailId: $scope.newOppContact.emailId;

            if(response.companyHead){
                intro = intro+ "Hi "+response.companyHead.firstName;
            }

            if(response.companyHead && response.reportingManager){
                intro = intro+" & ";
            }

            if(response.reportingManager){
                intro = intro+ response.reportingManager.firstName
            }

            if(response.companyHead.emailId == response.reportingManager.emailId){
                intro = "Hi "+response.companyHead.firstName
            }

            if(!checkRequired($scope.opp.closeReasonDescription)){
                $scope.opp.closeReasonDescription = "No Comments"
            }

            var oppDetails = "\n\nOpportunity Name :"+$scope.opp.opportunityName+
                "\nDeal Size: "+$rootScope.currency+" "+$scope.opp.amountWithNgmCommas +"("+ $scope.opp.netGrossMargin +"% of "+$scope.opp.amount +")"+
                "\nClose date: "+moment($scope.opp.closeDate).format(standardDateFormat())+
                "\nDays to Closure: "+Math.ceil(daysToClose.asDays())+
                "\nStage: "+$scope.opp.stage+
                "\nContact: "+contactEmailId+
                "\nReason: "+$scope.opp.closeReasons.join(',')+
                "\nReason details: "+$scope.opp.closeReasonDescription
            // "\nView Opportunity: "+"http://"+$rootScope.companyDetails.url+"/opportunities/all"

            var renewalDetails = ""
            if(response.opportunity && response.opportunity.renewed && response.opportunity.renewed.amount){
                renewalDetails = "\n\n Renewal opportunity was created\n"
                    +"\nDeal size: "+$rootScope.currency+" "+response.opportunity.renewed.amount
                    +"\nClose Date: "+moment(response.opportunity.renewed.closeDate).format(standardDateFormat())
            }

            var signature = '\n\n\n'+getSignature(share.liuData.firstName+' '+share.liuData.lastName,share.liuData.designation,share.liuData.companyName,share.liuData.publicProfileUrl)
            // +'\n\n Powered by Relatas';
            var body = intro+",\n\n"+"Deal with "+contactEmailId+" has closed."+oppDetails+renewalDetails+signature;

            $scope.add_cc = [];
            if($scope.opp.mailOrgHead){
                $scope.add_cc = [response.companyHead.emailId];
            }

            sendEmail($scope,$http,subject,body,null,share,share.liuData._id,response.reportingManager.emailId,$scope.cPhone,share.liuData,null,response.reportingManager.firstName)
        }
    }
}

function autoInitGoogleLocationAPI(share,$scope){

    window.initAutocomplete = initAutocomplete;

    function initAutocomplete() {

        checkDOMLoaded();

        checkContactDOMLoaded();

        function checkContactDOMLoaded(){

            if(document.getElementById('p_location')){

                var autocomplete = new google.maps.places.Autocomplete(
                    (document.getElementById('p_location')),
                    {types:['(cities)']});
                autocomplete.addListener('place_changed', function(err,places){

                    $scope.coords = {
                        lat:autocomplete.getPlace().geometry.location.lat(),
                        lng:autocomplete.getPlace().geometry.location.lng(),
                        address:$("#p_location").val()
                    }
                    return false;
                });

                var autocomplete3 = new google.maps.places.Autocomplete(
                    (document.getElementById('p_location_2')),
                    {types:['(cities)']});
                autocomplete3.addListener('place_changed', function(err,places){

                    $scope.coords = {
                        lat:autocomplete3.getPlace().geometry.location.lat(),
                        lng:autocomplete3.getPlace().geometry.location.lng(),
                        address:$("#p_location_2").val()
                    }
                    return false;
                });
            } else {
                setTimeOutCallback(100,function () {
                    checkContactDOMLoaded();
                })
            }
        }

        function checkDOMLoaded(){
            if(document.getElementById('autocompleteCity')){

                var autocomplete2 = new google.maps.places.Autocomplete(
                    (document.getElementById('autocompleteCity')),
                    {types:['(cities)']});
                autocomplete2.addListener('place_changed', function(err,places){

                    if(!$scope.opp){
                        $scope.opp = {}
                    }

                    if(!$scope.opp.geoLocation){
                        $scope.opp.geoLocation = {}
                    }

                    $scope.opp.geoLocation.lat = autocomplete2.getPlace().geometry.location.lat()
                    $scope.opp.geoLocation.lng = autocomplete2.getPlace().geometry.location.lng()

                    share.setTown(null)
                    if($("#autocompleteCity").val() && $("#autocompleteCity").val() != "" && $("#autocompleteCity").val() != " "){
                        share.setTown($("#autocompleteCity").val())
                    }
                    return false;
                });
            } else {
                setTimeOutCallback(100,function () {
                    checkDOMLoaded();
                })
            }
        }
    }
}

function searchResults($scope,$http,keywords,share,searchService,type) {

    var selector = "showResults"+type;
    var typeSelector = type+"s"

    if(keywords && keywords.length > 3){

        searchService.search(keywords).success(function(response){

            if(response.SuccessCode){

                if(type == "contact"){
                    $scope.noContactsFound = false;
                }

                $scope[typeSelector+"NotFound"] = false

                $scope[selector] = true;
                processSearchResults($scope,$http,response.Data,type);
            } else {
                $scope[selector] = false;
                $scope[typeSelector] = [];

                if(type == "contact"){
                    $scope.noContactsFound = true;
                }

                $scope[typeSelector+"NotFound"] = true;

                var obj = {
                    noPicFlag:true,
                    fullName: '',
                    name: '',
                    image: '/getContactImage/' + null + '/' + null,
                    emailId:keywords
                }

                $scope[typeSelector].push(obj)

            }
        }).error(function(){
            console.log('error');
        });
    } else {
        $scope[selector] = false;
        $scope[typeSelector] = [];
    }
}

function removeRecipient($scope,$http,contact,type){

    var opportunityId = $scope.opp._id
    var selector = "showResults"+type;
    var typeSelector = type+"s";

    var hashtag = type;
    var relation = "decision_maker"

    if(type === 'partners'){
        hashtag = "partner"
    } else if(type === 'decisionMakers'){
        hashtag = "decisionMaker"
    } else if(type === 'influencers'){
        hashtag = "influencers"
        relation = "influencer"
    }

    $http.post('/opportunities/remove/people',{type:type,contact:contact,opportunityId:opportunityId,opportunityId2:$scope.opp.opportunityId})
        .success(function (response) {
            if(response.SuccessCode){
                // updateRelationship($http,contact,null,"decisionmaker_influencer",relation)
                // deleteHashtag($http,contact.contactId,hashtag,contact.emailId)
                var rmIndex = $scope.opp[typeSelector].indexOf(contact);
                $scope.opp[typeSelector].splice(rmIndex, 1);
            }
        });
}

function addRecipient($scope,$http,contact,type){

    var opportunityId = $scope.opp?$scope.opp._id:null;
    var selector = "showResults"+type;
    var typeSelector = type+"s";

    $scope.partner = '';
    var hashtag = type;
    var relation = "decision_maker"
    if(type === 'partners'){
        hashtag = "partner"
    } else if(type === 'decisionMakers'){
        hashtag = "decisionMaker"
    } else if(type === 'influencers'){
        hashtag = "influencers"
        relation = "influencer"
    }

    // addHashtag ($http,contact._id,hashtag)
    // updateRelationship($http,contact,relation,"decisionmaker_influencer");

    if(validateEmail(contact.emailId) && opportunityId){
        $http.post('/opportunities/add/people',{type:typeSelector,contact:contact,opportunityId:opportunityId,opportunityId2:$scope.opp.opportunityId})
            .success(function (response) {
                if(response.SuccessCode){
                    $scope.opp[typeSelector].push(contact)
                    $scope[selector] = false;
                } else {
                    $scope[selector] = false;
                }

                clearInputFields();
            });
    } else {

        if(!$scope.opp){
            $scope.opp = {}
        }

        if(!$scope.opp[typeSelector]){
            $scope.opp[typeSelector] = [];
        }

        $scope.opp[typeSelector].push(contact)

        $scope[selector] = false;
        $scope[hashtag] = "";
    }
}

function addHashtag ($http,p_id,hashtag){
    var str = hashtag.replace(/[^\w\s]/gi, '');
    var obj = { contactId:p_id, hashtag: str};
    $http.post("/api/hashtag/new",obj)
        .success(function (response) {

        });
}

function processSearchResults($scope,$http,response,type) {

    var typeSelector = type+"s";
    var contactsArray = response;
    $scope[typeSelector] = [];

    if(contactsArray.length>0){
        for(var i=0;i<contactsArray.length;i++){

            var obj = {};

            if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                var name = getTextLength(contactsArray[i].personName,20);
                var image = '/getImage/'+contactsArray[i].personId._id;

                obj = {
                    fullName:contactsArray[i].personName,
                    name:name,
                    image:image
                };

                obj.emailId = contactsArray[i].personEmailId;
                obj.twitterUserName = contactsArray[i].twitterUserName;
                obj.mobileNumber = contactsArray[i].mobileNumber;

            }
            else {
                var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                obj = {
                    fullName: contactsArray[i].personName,
                    name: getTextLength(contactsArray[i].personName, 20),
                    image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                    emailId:contactsArray[i].personEmailId,
                    twitterUserName: contactsArray[i].twitterUserName,
                    mobileNumber: contactsArray[i].mobileNumber
                    // noPicFlag:true
                };
            }

            if(obj.twitterUserName){
                obj.tweetAccExists = true;
            }

            obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
            obj._id = contactsArray[i]._id;
            obj.personId = contactsArray[i].personId && contactsArray[i].personId._id?contactsArray[i].personId._id:null;
            obj.type = type;

            obj.noPicFlag = true;
            obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();

            if(!userExists(obj.emailId) && validateEmail(obj.emailId)){
                $scope[typeSelector].push(obj)
            }

            function userExists(username) {
                return $scope[typeSelector].some(function(el) {
                    return el.emailId === username;
                });
            }
        }
    }
}

function updateRelationship($http,contact,relation,relationKey,relationType){

    if(contact.contactId){
        var id = contact.contactId
    } else {
        id = contact._id
    }

    var reqObj = {contactId:id,type:relation,relationKey:relationKey,relation:relationType};

    $http.post('/opportunities/contacts/update/reltionship/type',reqObj)
        .success(function(response){

        });
}

function resetErrorsField($scope){
    _.each($scope.accountTypes,function (accType) {
        accType.req = false;
    })

    for(var key in oppEmptyObj()){
        $scope.opp[key+"Req"] = false;
    }

    highlightTab($scope,0)

    $scope.errorsExist = false;
}

relatasApp.directive('searchResultsTeam', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="searchList.length>0">' +
            '<div ng-repeat="item in searchList track by $index"> ' +
            '<div class="clearfix cursor" ng-click="setOppOwner(item)">' +
            '<div class="pull-left relative">' +
            // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
            '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
            '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
            '<div class="pull-left"><p class="margin0">[[item.fullName]]</p></div>' +
            '</div></div></div>'
    };
});

relatasApp.directive('searchResultsTeamOne', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="searchListTaskOwner.length>0">' +
            '<div ng-repeat="item in searchListTaskOwner track by $index"> ' +
            '<div class="clearfix cursor" ng-click="setTaskOwner(item)">' +
            '<div class="pull-left relative">' +
            // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
            '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
            '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
            '<div class="pull-left"><p class="margin0">[[item.fullName]]</p></div>' +
            '</div></div></div>'
    };
});

relatasApp.directive('searchResultsTeamTwo', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="searchListParticipants.length>0">' +
            '<div ng-repeat="item in searchListParticipants track by $index"> ' +
            '<div class="clearfix cursor" ng-click="addTeamMembersToTask(item)">' +
            '<div class="pull-left relative">' +
            // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
            '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
            '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
            '<div class="pull-left"><p class="margin0">[[item.fullName]]</p></div>' +
            '</div></div></div>'
    };
});

relatasApp.directive('searchResults', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="role.searchList.length>0">' +
            '<div ng-repeat="teamMember in role.searchList track by $index"> ' +
            '<div class="clearfix cursor" style="margin: 10px 0;" title="[[teamMember.fullName]], [[teamMember.emailId]]" ng-click="addUser(role,teamMember)">' +
            '<div class="pull-left relative">' +
            // '<img ng-if="teamMember.noImage === 0" ng-src="[[teamMember.image]]" title="[[teamMember.fullName]], [[teamMember.emailId]]" class="contact-image">' +
            // '<span ng-if="teamMember.noImage === 1">' +
            // '<span class="contact-no-image">[[teamMember.firstName]]</span></span></div>' +
            // // '<div class="pull-left"><p class="margin0">[[teamMember.name]]</p><p class="margin0">[[teamMember.emailId]]</p></div>' +
            // '<div class="pull-left">' +
            '<p class="margin0">[[teamMember.fullName]]</p>' +
            '<p class="margin0">[[teamMember.emailId]]</p>' +
            '</div>' +
            '</div></div></div>'
    };
});

relatasApp.directive('searchResultsContacts', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" style="position: absolute;" ng-show="showResultscontact">' +
            '<div ng-repeat="item in contacts track by $index"> ' +
            '<div class="clearfix cursor" ng-click="selectContact(item)">' +
            '<div class="pull-left relative">' +
            // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
            '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
            '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
            '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
            '</div></div></div>'+
            '<div class="input-text-error" ng-show="noContactsFound && !opp.contactEmailIdReq"> ' +
            'We couldn"t find the contact. click .<span style="cursor:pointer; color:#267368" ng-click="createContactWindow()"> here</span> to create' +
            '</div>'
    };
});

function masterAccList($scope,$http,opp){

    $http.get("/corporate/admin/get/master/account/types")
        .success(function (response) {

            if (response && response[0]) {
                $scope.data = {
                    model: null,
                    availableOptions: response
                };

                $scope.defaultAccount = $scope.data.availableOptions[0].name;
                $scope.data.model = $scope.defaultAccount;
            }
        });

    $scope.removeMasterData = function(obj,list){

        if(window.confirm('This will delete the master data')) {
            var newList = [];
            if(list){
                _.each(list.lists,function (li) {
                    if(li._id !== obj._id){
                        newList.push(li)
                    }
                })
            }

            list.lists = newList;
            if(opp.masterData.length>0) {
                _.each(opp.masterData, function (ma) {
                    if (ma.type === list.name) {
                        ma.data = newList;
                    }
                })
            }

            if(list.lists.length === 0){
                list.typeHeaders = [];
            }

            if($scope.opp.masterData && $scope.opp.masterData.length>0){
                $scope.opp.masterData = $scope.opp.masterData.filter(function (ma) {
                    return ma.type != list.name;
                });
            }
        }
    }

    $scope.attachExistingData = function(obj){

        if(!opp.masterData){
            opp.masterData = [];
        }

        if(opp.masterData.length>0){
            _.each(opp.masterData,function (ma) {
                if(ma.type === obj.name){
                    obj.lists = ma.data;
                    obj.typeHeaders = [];

                    for(var key in ma.data[0]){
                        obj.typeHeaders.push({
                            name:key,
                            value:""
                        })
                    }
                }
            });
        }
    }

    $scope.selectMasterType = function(data,accType){

        if(opp){
            if(!opp.masterData){
                opp.masterData = [];
            }

            if(opp.masterData.length>0){
                _.each(opp.masterData,function (ma) {
                    if(ma.type === accType.name && ma._id){
                        ma.replaceThis = true
                    }
                });

                opp.masterData = opp.masterData.filter(function (ma) {
                   return !ma.replaceThis;
                });

                opp.masterData.push({
                    type:accType.name,
                    data:[data]
                })
            } else {
                opp.masterData = [{
                    type:accType.name,
                    data:[data]
                }]
            }
        }

        $scope.attachExistingData(accType);
    };

    $scope.closeMDPopup = function(){
        $scope.addMoreMasterData = false;
    }

    $scope.clearSearch = function(accType){
        $scope.ifMasterAccSearching = false;
        $scope.accName = "";
        $(".master-acc input").val("");

        fetchAccounts($scope,$http,null,accType);
    }

    $scope.getMasterDataFor = function(forType){
        $scope.addMoreMasterData = true;
        $scope.addList = forType;
        fetchAccounts($scope,$http,null,forType.name,forType);
    }

    $scope.searchForMasterAccount = function(accName,forType) {
        $scope.ifMasterAccSearching = true;
        if(accName && accName.length>1){
            fetchAccounts($scope,$http,accName,forType.name,forType)
        }
    }
}

function fetchAccounts($scope,$http,accountName,accountType) {

    var url = "/corporate/admin/get/master/account/list";

    if(accountType){
        url = fetchUrlWithParameter(url,"accountType",accountType);
    }

    if(accountName){
        url = fetchUrlWithParameter(url,"search",accountName);
    }

    $http.get(url)
        .success(function (response) {

            if(response && response[0] && response[0].data){
                $scope.typeHeaders = [];

                for(var key in response[0].data[0]){
                    $scope.typeHeaders.push({
                        name:key,
                        value:""
                    })
                };

                $scope.importantHeadersObj = {};
                if(response[0].importantHeaders){
                    _.each(response[0].importantHeaders,function (ih) {
                        $scope.importantHeadersObj[ih.name] = ih.isImportant
                    })
                }

                $scope.lists = response[0].data;
            }
        });
}
