
var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','angular-loading-bar','ngLodash','textAngular']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

var contactContextEmailId = getParams(window.location.href).emailId;
var createOppForEmailId = getParams(window.location.href).createOppForContact;
var opportunityIdState = getParams(window.location.href).opportunityId;
var notificationCategory = getParams(window.location.href).notifyCategory;
var notificationDate = getParams(window.location.href).notifyDate;

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        liuDetails:function(user){
            this.liuData = user;
        },
        getUserId:function(){
            return this.userId
        },
        setUserEmailId:function(emailId){
            this.userEmailId = emailId;
        },
        setTeamMembers:function(members){
            this.team = members;
        },
        setTargetForFy:function(value){
            this.targetForFy = value;
        },
        getTeamMembers:function(){
            return this.team;
        },
        getLiuUserEmailId:function(){
            return this.userEmailId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setTown:function(geoLocationTown){
            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        }
    };
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }

        else toastr.error("Please enter search content")
    };
});

var timezone;
relatasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        if(share.companyDetails.currency){
            share.companyDetails.currency.forEach(function (el) {
                share.currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    share.primaryCurrency = el.symbol;
                }
            });
        }
    });
    getErrorMessages($rootScope,$http,"opportunity");
});

relatasApp.controller("opportunities", function ($scope, $http,$rootScope,share,searchService,$sce){

    initServices($scope, $http,$rootScope,share,searchService,$sce);

    $scope.oppRemoveList = [];
    share.transferOpp = function () {
        share.resetFiltersToDefault();
        $scope.transferOpportunities = !$scope.transferOpportunities
        $scope.stages = JSON.parse(window.localStorage['relatasLocalDb']);
    }

    $scope.registerDatePickerIdChart = function(id){

        var minDate = new Date($scope.opp.createdDate);
        $('#'+id).datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: minDate,
            maxDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    if(!$scope['chart']){
                        $scope['chart'] = {};
                    }
                    $scope['chart'][id] = moment(dp).format(standardDateFormat());
                });
            }
        });
    }

    // $scope.addContactModal = true;

    $scope.viewOppTimeline = function (dates) {
        mixpanelTracker("Opps>view opp timeline");
        $scope.isOppModalOpen = true;
        if(!dates){
            $scope.showOppTimeline = !$scope.showOppTimeline;
        } else {
            $scope.showOppTimeline = true;
        }
        if($scope.showOppTimeline){
            $scope.getInteractionActivity($scope.opp,dates);
        }
    }

    $scope.transferSingleOpp = function () {
        $scope.oppRemoveList = []
        $scope.oppRemoveList.push($scope.opp)
        share.selectTeamMemberAndTransfer()
    }

    $scope.validateNetGrossMargin = function (ngm) {
        if(ngm && parseFloat(ngm) && parseFloat(ngm)>100){
            $scope.opp.netGrossMargin = 100;
        }
    }

    $scope.recalculateNgm = function (op) {
        op.amountWithNgm = parseFloat(op.amount);

        if(op.netGrossMargin || op.netGrossMargin == 0){
            op.amountWithNgm = (op.amount*op.netGrossMargin)/100
        }

        op.amountWithNgm = op.amountWithNgm.r_formatNumber(2);

        op.amountWithCommas = numberWithCommas(parseFloat(op.amount).r_formatNumber(2),share.primaryCurrency == "INR")
        op.amountWithNgmCommas = numberWithCommas(parseFloat(op.amountWithNgm).r_formatNumber(2),share.primaryCurrency == "INR")
    }

    autoInitGoogleLocationAPI(share,$scope);

    $scope.isNotOwner = false;

    $scope.sanitizeMe = function(text) {
        return $sce.trustAsHtml(text)
    };

    if(opportunityIdState){

        checkAllControllersLoaded()
        function checkAllControllersLoaded(){
            if(share.liuData && $scope.stages && share.fetchOpportunities){
                $scope.loadSourceOpp({sourceOpportunityId:opportunityIdState})
            } else {
                setTimeOutCallback(100,function () {
                    checkAllControllersLoaded();
                })
            }
        }
    }

    $scope.loadSourceOpp = function (opp) {

        $http.get("/opportunities/get/opportunity/by/id?opportunityId="+opp.sourceOpportunityId)
            .success(function (response) {
                if(response){
                    _.each(response,function (o) {
                        o.amountWithNgm = o.amount;

                        if(o.netGrossMargin || o.netGrossMargin == 0){
                            o.amountWithNgm = (o.amount*o.netGrossMargin)/100
                        }

                        o.amountWithNgm = parseFloat(o.amountWithNgm.r_formatNumber(2))

                        o.transferThis = true;
                        o.selected = "opp-selected"
                        o.closeReasons = _.uniq(o.closeReasons);

                        o.amount = parseFloat(o.amount);

                        o.netGrossMargin = parseFloat(o.netGrossMargin);

                        o.convertedAmt = o.amount;
                        o.convertedAmtWithNgm = o.amountWithNgm

                        if(o.currency && o.currency !== share.primaryCurrency){
                            if(share.currenciesObj[o.currency] && share.currenciesObj[o.currency].xr){
                                o.convertedAmt = o.amount/share.currenciesObj[o.currency].xr
                            }

                            if(o.netGrossMargin || o.netGrossMargin == 0){
                                o.convertedAmtWithNgm = (o.convertedAmt*o.netGrossMargin)/100
                            } else {
                                o.convertedAmtWithNgm = o.convertedAmt;
                            }

                            o.convertedAmt = parseFloat(o.convertedAmt)
                        }

                        if(o.contactEmailId){
                            $scope.contacts.push({
                                emailId:o.contactEmailId,
                                name:o.fullName?o.fullName:o.contactEmailId
                            });
                        }

                        if(o.contactEmailId){
                            o.account = fetchCompanyFromEmail(o.contactEmailId)
                        } else {
                            o.account = "Others"
                        }

                        if(o.createdByEmailId){
                            o["creator"] = $scope.teamDictionary[o.createdByEmailId]
                        } else {
                            o["creator"] = o.userEmailId?$scope.teamDictionary[o.userEmailId]:{};
                        }

                        o["owner"] = o.userEmailId && $scope.teamDictionary[o.userEmailId]?$scope.teamDictionary[o.userEmailId]:{fullName:o.userEmailId,emailId:o.userEmailId};

                        o.owner.noPicFlag = true;
                        o.owner.nameNoImg = o.owner.fullName.substr(0,2).toUpperCase();
                        if(o.partners && o.partners.length>0) {
                            _.each(o.partners,function (el) {
                                if(el){
                                    $scope.contacts.push({
                                        emailId:el.emailId,
                                        name:el.fullName?el.fullName:el.emailId
                                    });
                                }
                            })
                        }

                        if(o.influencers && o.influencers.length>0) {
                            _.each(o.influencers,function (el) {
                                if(el){
                                    $scope.contacts.push({
                                        emailId:el.emailId,
                                        name:el.fullName?el.fullName:el.emailId
                                    });
                                }
                            })
                        }

                        if(o.decisionMakers && o.decisionMakers.length>0) {
                            _.each(o.decisionMakers,function (el) {
                                if(el){
                                    $scope.contacts.push({
                                        emailId:el.emailId,
                                        name:el.fullName?el.fullName:el.emailId
                                    });
                                }
                            })
                        }

                        o.stage = o.stageName;
                        o.transferThis = true;
                        o.selected = "opp-selected"
                        o.amountWithNgmCommas = numberWithCommas(parseFloat(o.amountWithNgm).r_formatNumber(2),share.primaryCurrency == "INR")
                        $scope.showOpportunity(o)
                    })
                } else {
                    toastr.error("Source Opportunity not found. Please contact admin for help.")
                }
            })
    }

    getOppStages();

    function getOppStages(){

        if(share.opportunityStages){
            $scope.stagesSelection = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(500,function () {
                getOppStages()
            })
        }
    }

    var something = '<div class="row">'+
        '<div class="col-md-6">'+
        '<div ng-show="companyDetails.geoLocations.length>0">'+
        '<label>Region <span ng-show="isRegionRequired" class="required">*</span></label>'+
        '<div class="add-town clearfix">'+
        '<select ng-model="opp.geoLocation.zone" ng-disabled="opp.isNotOwner">'+
        '<option ng-repeat="item in companyDetails.geoLocations" ng-selected="opp.geoLocation.zone == item.region" value="[[item.region]]">[[item.region]]</option>'+
        '</select>'+
        '<input type="text" placeholder="Add Location" value="[[opp.geoLocation.town]]" ng-model="opp.geoLocation.town" id="autocompleteCity" ng-disabled="opp.isNotOwner">'+
    '</div>'+
    '   </div>'+
    '   </div>'+
        '<div class="col-md-6">'+
        '<div ng-show="companyDetails.productList.length>0">'+
    '<label>Products <span ng-show="isProductRequired" class="required">*</span></label>'+
        '<div class="product-list clearfix">'+
    '   <select ng-model="opp.productType" ng-disabled="opp.isNotOwner">'+
    '   <option ng-repeat="item in companyDetails.productList" ng-selected="opp.productType == item.name" value="[[item.name]]">[[item.name]]</option>'+
    '   </select></div></div></div></div>'

    $scope.mandatoryComponents = attachComponents(null,something)
    // $scope.mandatoryComponents = attachComponents(null,"<h1>Hello there</h1>")

    share.setCompanyDetails = function (companyDetails,liu) {

        share.loadPortfolios(companyDetails,liu);

        $scope.liu = liu;

        var accessControlProd = [],accessControlRegion = [],accessControlVertical = [];

        if(share.liuData){
            _.each(share.liuData.productTypeOwner,function (el) {
                accessControlProd.push({name:el})
            })
            _.each(share.liuData.verticalOwner,function (el) {
                accessControlVertical.push({name:el})
            })
            _.each(share.liuData.regionOwner,function (el) {
                accessControlRegion.push({region:el})
            })
        }

        if($rootScope.currency){

        } else {
            $rootScope.currency = 'USD';
        }

        if(!$scope.companyDetails){
            $scope.companyDetails = companyDetails;
        }

        if(companyDetails && companyDetails.currency){
            _.each(companyDetails.currency,function (el) {
                if(el.isPrimary){
                    $rootScope.currency = el.symbol
                }
            })
        }

        if(companyDetails && companyDetails.productList && companyDetails.productList.length>0){
            share.productList(companyDetails.productList)
        } else {
            share.productList(accessControlProd)
        }

        if(companyDetails && companyDetails.sourceList && companyDetails.sourceList.length>0){
            share.sourceList(companyDetails.sourceList)
        }

        if(companyDetails && companyDetails.geoLocations && companyDetails.geoLocations.length>0){
            share.geoLocations(companyDetails.geoLocations)

        } else {
            share.geoLocations(accessControlRegion)
        }

        if(companyDetails && companyDetails.verticalList && companyDetails.verticalList.length>0){
            share.verticalList(companyDetails.verticalList)
        } else {
            share.verticalList(accessControlVertical)
        }
    }

    share.fetchOpportunities = function (userIds,filter,inCache,fy,portfolios) {

        $scope.selectedUserIds = userIds;
        $scope.oppsLoading = true;

        if(inCache && filter.type == 'product'){
            handleProductTypesFilter($scope,filter.productType,share)
        } else if(inCache && filter.type == 'location'){
            handleRegionTypesFilter($scope,filter.location,share)
        } else if(inCache && filter.type == 'ownerEmailId'){
            handleOwnerEmailIdFilter($scope,filter.ownerEmailId,share)
        } else if(inCache && filter.type == 'vertical'){
            handleVerticalTypesFilter($scope,filter.vertical,share)
        } else if(inCache && filter.type == 'source'){
            handleSourceTypesFilter($scope,filter.source,share)
        } else if(inCache && filter.type == 'date'){
            handleEndDateFilter($scope,filter.date,share)
        } else if(inCache && filter.type == 'company'){
            handleCompanyFilter($scope,filter.company,share)
        }  else if(inCache && filter.type == 'contact' && !filter.resetContext){
            handleContactFilter($scope,filter.name,share)
        } else {
            if(userIds != share.userId){
                $scope.isNotOwner = true;
            } else {
                $scope.isNotOwner = false;
            }

            var url = "/opportunities/get/all/data";
            var users = share.getTeamMembers();
            var userIdList = [];

            for(var key in users){
                userIdList.push(users[key].userId)
            }

            if(userIds){
                url = fetchUrlWithParameter("/opportunities/get/all/data", "hierarchylist", userIds);
            }

            if(!filter && contactContextEmailId){
                url = fetchUrlWithParameter(url+"&emailId="+contactContextEmailId.replace(/[^a-zA-Z0-9_/-@.]/g,''));
            }

            url = fetchUrlWithParameter(url+"&userIdList="+userIdList)

            if(userIds && filter && filter == 'accessControl' || share.accessControl){
                url = fetchUrlWithParameter(url+"&accessControl="+true)
            }

            if(fy){
                url = fetchUrlWithParameter(url+"&fromDate="+fy)
            }

            if(portfolios){
                url = fetchUrlWithParameter("/opportunities/get/all/data", "portfolios", portfolios);
            }

            if(notificationCategory && notificationDate) {
                url = fetchUrlWithParameter(url+"&notifyCategory=" + notificationCategory + "&notifyDate=" + notificationDate);

                updateNotificationOpenDate($http, {"dayString":notificationDate, "category": notificationCategory}, function(response) {});
            }

            var ngmReq = share.companyDetails && share.companyDetails.netGrossMargin;

            $http.get(url)
                .success(function (response) {
                    $scope.oppsLoading = false;
                    $scope.contacts = response.contacts;
                    $scope.contactsDictionary = {};

                    if(response.SuccessCode && response.contacts[0] && response.contacts[0].contacts.length>0){
                        _.each(response.contacts[0].contacts,function (c) {
                            $scope.contactsDictionary[c.personEmailId] = c
                        });
                    }

                    if(response.fyList && response.fyList.length>0){
                        share.fyList(response.fyList,response.allQuarters);
                    }

                    share.allQuarters = response.allQuarters

                    if(response && response.data && response.data.length>0){
                        _.each(response.data,function (op) {
                            if(op && op.opportunities.length>0){

                                _.each(op.opportunities,function (op) {
                                    op.ngmReq = ngmReq;
                                    op.amount = parseFloat(op.amount);
                                    op.amountWithNgm = op.amount;

                                    if(op.netGrossMargin || op.netGrossMargin == 0){
                                        op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                                    }

                                    op.amountWithNgm = parseFloat(op.amountWithNgm.r_formatNumber(2))

                                    op.transferThis = true;
                                    op.selected = "opp-selected"
                                })

                                $scope.oppRemoveList = $scope.oppRemoveList.concat(op.opportunities);
                            }
                        })
                    }

                    stagesLoaded();

                    function stagesLoaded(){

                        if($scope.stagesSelection){
                            drawCompanyStages($scope,share,$scope.stagesSelection,response,share.getLiuUserEmailId())
                        } else {
                            setTimeOutCallback(500,function () {
                                stagesLoaded()
                            })
                        }
                    }

                });
        }
    }

    $scope.selectUser = function (user) {
        $scope.selectedUser = user;
        $scope.transferToThisUser = true;
        $scope.searchUsers = [];
        $scope.teamMate = '';
    }

    $scope.reportingManager = null;

    $scope.searchForTeamMate = function (user) {
        $scope.searchUsers = [];
        if(share.team){
            for(var key in share.team){
                if(user.length>1){
                    if(share.team[key].fullName.match(new RegExp(user, 'i')) || share.team[key].emailId.match(new RegExp(user, 'i'))){
                        $scope.searchUsers.push(share.team[key])
                    }
                }
            }
        }
    }

    share.selectTeamMemberAndTransfer = function () {

        $scope.oppRemoveList = _.uniqBy($scope.oppRemoveList,"opportunityId");

        if($scope.oppRemoveList.length>0){
            $scope.showTeamMembers = true;
        } else {
            $scope.showTeamMembers = false;
        }
    }

    $scope.startTransferring = function () {
        startOppTransfer($scope,$http,share.userEmailId,share.liuData.hierarchyParent,$scope.companyDetails._id,share,$rootScope)
    }

    $scope.selectOpp = function (opp) {

        opp.transferThis = !opp.transferThis;

        if(opp.transferThis){
            opp.selected = "opp-selected";
            $scope.oppRemoveList.push(opp);
        } else {
            opp.selected = "";
            removeOpp($scope,opp,$scope.oppRemoveList)
        }

    }
    
    $scope.getContactProfile = function (item) {
        var contactDetails = $scope.contactsDictionary[item.contactEmailId]
        getContactProfileInfo(contactDetails,item)
    }

    $scope.getContactProfileDetails = function (item) {
        item["userEmailId"] = $scope.opp.userEmailId;
        getContactProfileInfoFromDb($http,item)
    };

    $scope.registerDatePickerIdRenewal = function(minDate,maxDate){
        pickDatesForRenewal($scope,minDate,maxDate,'#opportunityCloseDateSelector4',1,"years")
    }

    $scope.setRenewalAmount = function(amount){

        if(!$scope.opp.renewed){
            $scope.opp.renewed = {
                amount:0,
                netGrossMargin:0,
                closeDate:null,
                createdDate:null
            }
        }
        $scope.opp.renewed.amount = checkRequired(amount)?parseFloat(amount):0;
    }

    $scope.registerDatePickerId = function(minDate,maxDate){

        if($scope.opp){
            if($scope.opp.stage == "Close Won" || $scope.opp.stage == "Close Lost"){
                maxDate = new Date();
            } else {
                maxDate = new Date(moment().add(4,"year"))
            }
        } else {
            $scope.opp = {}
        }

        $('#opportunityCloseDateSelector').datetimepicker({
            value:$scope.opp.closeDateFormatted,
            timepicker:false,
            validateOnBlur:false,
            // minDate: minDate,
            // minDate: new Date(moment().subtract(2,"week")),
            maxDate: maxDate,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    timezone = timezone?timezone:"UTC";
                    $scope.opp.closeDate = moment(dp).format();
                    $scope.opp.closeDateFormatted = moment(dp).format("DD MMM YYYY");
                });
            }
        });
    }

    $scope.getInteractionActivity = function (opp,dates) {
        if(!opp){
            opp = {}
        }
        var url = '/opportunity/activity/interactions';
        var userEmailIds = [opp.userEmailId];
        var contacts = [opp.contactEmailId];

        if(opp.usersWithAccess && opp.usersWithAccess.length>0){
            userEmailIds = userEmailIds.concat(_.map(opp.usersWithAccess,"emailId"));
        }

        if(opp.partners && opp.partners.length>0){
            contacts = contacts.concat(_.map(opp.partners,"emailId"));
        }

        if(opp.influencers && opp.influencers.length>0){
            contacts = contacts.concat(_.map(opp.influencers,"emailId"));
        }

        if(opp.decisionMakers && opp.decisionMakers.length>0){
            contacts = contacts.concat(_.map(opp.decisionMakers,"emailId"));
        }

        url = fetchUrlWithParameter(url,"userEmailIds", userEmailIds);
        url = fetchUrlWithParameter(url,"contacts", contacts);

        var from = new Date(opp.createdDate);
        var last90Days = new Date(moment().subtract(90,"days"));
        if(from< last90Days){
            from = last90Days
        }

        var to = new Date();

        if(dates){
            from = new Date(moment(dates.chartStartDate)).toISOString();
            to = new Date(moment(dates.chartEndDate)).toISOString();

            url = fetchUrlWithParameter(url,"from", from);
            url = fetchUrlWithParameter(url,"to", to);
        } else {
            url = fetchUrlWithParameter(url,"from", from.toISOString());

            if(_.includes(["Close Lost","Close Won"], opp.stage)){
                to = new Date(opp.closeDate);
                from = new Date(opp.createdDate);
                url = fetchUrlWithParameter(url,"to", to.toISOString());
            }
        }

        url = fetchUrlWithParameter(url,"opportunityId", opp.opportunityId);

        $scope.showOppTimeline = false;
        $scope.loadingOppTimeline = true;

        // url = "/opportunity/activity/interactions?userEmailIds=sureshhoel@gmail.com&contacts=robby@amazon.com,sudip@relatas.com&opportunityId=5ae02f24a4a60a390f440ded&from=2018-04-25T07:32:52.924Z"
        $http.get(url)
            .success(function (response) {
                $scope.showOppTimeline = true;
                $scope.loadingOppTimeline = false;
                drawHistory($scope,$http,share,response.logs,response.interactions,from,to);
            })
    };

    // $scope.viewOppTimeline();

    $scope.showOpportunity = function (opp) {

        $scope.isCorporateAdmin = share.liuData.corporateAdmin

        getAccountTypes($scope,$http,share,opp);
        getAllRelatedData($scope,$http,share,opp);
        ifOppExists(opp.opportunityId);
        masterAccList($scope,$http,opp)

        $scope.registerDatePickerId();

        $scope.renewalAmount = 0;
        $scope.renewalCloseDate = "";
        $scope.showResultscontact = false;

        if(!opp.currency){
            share.companyDetails.currency.forEach(function (el) {
                if(el.isPrimary){
                    opp.currency = el.symbol
                }
            });
        }

        checkEditAccessForClosedOpps($scope,share,opp);

        $scope.createdDateFormatted = opp.createdDate ? moment(opp.createdDate).format("DD MMM YYYY") : null;

        if(opp.closeReasons && opp.closeReasons.length>0){
            _.each(opp.closeReasons,function (op) {
                _.each($scope.companyDetails.closeReasons,function (cr) {

                    if(cr.name == op){
                        cr.selected = true;
                    }
                })
            })
        } else {
            _.each($scope.companyDetails.closeReasons,function (cr) {
                cr.selected = false;
            })
        }

        $scope.isExistingOpp = true;
        $scope.isOppModalOpen = true;

        if(opp.fullName){
            opp.searchContent = opp.fullName + " ("+opp.contactEmailId+")";
        } else {
            opp.searchContent = opp.contactEmailId;
        }

        if(opp.geoLocation && opp.geoLocation.town){
            share.setTown(opp.geoLocation.town)
        }

        $scope.opp = opp;
        $scope.opp.closeDateFormatted = opp.closeDate ? moment(opp.closeDate).format("DD MMM YYYY") : null;

        if(opp.usersWithAccess){
            mapUsersWithAccessToOrgRoles($scope,share,opp)
        }

        if(opp.renewed && opp.renewed.createdDate){
            opp.renewed.closeDate = moment(opp.renewed.closeDate).format("DD MMM YYYY")
            opp.renewed.createdDate = moment(opp.renewed.createdDate).format("DD MMM YYYY")
        }

        $scope.oppOwner = {};
        window.localStorage['oppsInCommitStage'] = "notSet";
        window.localStorage['teamMembers'] = "";
        window.localStorage['viewingOpp'] = JSON.stringify($scope.opp);

        checkTeamLoaded();

        function checkTeamLoaded(){
            if(share.team){
                $scope.oppOwner.value = share.team[opp.userEmailId].fullName+" ("+share.team[opp.userEmailId].emailId+")"
            } else {
                setTimeOutCallback(1000,function () {
                    checkTeamLoaded();
                })
            }
        }

        $scope.budget = opp.BANT?opp.BANT.budget:false,
            $scope.authority = opp.BANT?opp.BANT.authority:false,
            $scope.need = opp.BANT?opp.BANT.need:false,
            $scope.time = opp.BANT?opp.BANT.time:false
    }

    $scope.closeModal = function (opp) {

        $scope.showOppTimeline = false;
        if(opportunityIdState){
            window.location = "/opportunities/all"
        } else if(createOppForEmailId){
            window.location = "/opportunities/all"
        } else {
            $scope.isOppModalOpen = false
            $scope.opp = {};
            $scope.opp.closeDateFormatted = ""
            $scope.opp.closeDate = ""
            $scope.opp.searchContent = ""
            $scope.noContactsFound = false
            $scope.decisionMakersNotFound = false
            $scope.influencersNotFound = false
            $scope.partnersNotFound = false
            $scope.oppDetailsNav = oppDetails();
            $scope.selectedTab = $scope.oppDetailsNav[0];
            $scope.viewFor = $scope.oppDetailsNav[0].name.toLowerCase();
            $scope.mailOrgHead = false;
            $scope.mailRm = false;
            $scope.renewalCloseDateFormatted = "";
            $scope.renewalAmount = "";

            $("#opportunityCloseDateSelector4").val("")
            $("#renewalAmount").val(0)

            _.each($scope.companyDetails.closeReasons,function (el) {
                el.selected = false;
            })

            _.each($scope.rolesList,function (role) {
                role.usersWithAccess = [];
            });

            if(opp){

                try {
                    $scope.opp = JSON.parse(window.localStorage['viewingOpp']);
                    if($scope.opp){
                        for(var orgKey in opp){
                            for(var copyKey in $scope.opp){
                                if(orgKey == copyKey){
                                    opp[orgKey] = $scope.opp[copyKey]
                                }
                            }
                        }
                    }
                } catch (err){
                    console.log(err)
                }
            }

            _.each($scope.accountTypes,function (acc) {
                acc.searchText = "";
            })
        }
    }

    $(document).on('keyup',function(evt) {
        if (evt.keyCode == 27) {
            $scope.$apply(function (){
                $scope.closeModal();
            })
        }
    });

    $scope.setZone = function (opp,zone) {
        if(opp && opp.geoLocation){
            opp.geoLocation.zone = zone;
        }
    }

    $scope.saveOpportunity = function () {
        $scope.town = share.getTown();
        
        $scope.renewalAmount = $("#renewalAmount").val()
        
        if(!$scope.renewalAmount){
            if($("#renewalAmount").val()){
                $scope.renewalAmount = $("#renewalAmount").val()
            }
        }

        if(share.companyDetails.currency){
            share.companyDetails.currency.forEach(function (el) {
                if($scope.opp.currency === el.symbol){
                    $scope.opp.xr = el.xr
                }
            });
        }

        $scope.opp._id?saveExistingOpportunity($scope,$http,share,$rootScope):addNewOpportunity($scope,$http,share,$rootScope);
    }

    $scope.deleteOpp = function (opp) {
        deleteOpp($scope,$http,opp,function (result) {
            share.fetchOpportunities(share.getUserId());
            share.getTargets(share.getUserId());
            setTimeout($scope.closeModal(),function () {
            },1000)
        })
    }

    $scope.oppRenewalStatusSet = function (opp) {
        opp.renewalStatusSet = true
    }

    $scope.goBackToOpp = function () {

        if(!$scope.renewalAmount){
            if($("#renewalAmount").val()){
                $scope.renewalAmount = $("#renewalAmount").val()
            }
        }

        if($scope.opp.renewThisOpp && $scope.renewalAmount && String($scope.renewalAmount).match(/[a-z]/i)){
            toastr.error("Please enter only numbers for Renewal amount")
            $scope.reasonsRequired = true;
            $scope.mailOptions = true;
        } else if($scope.opp.renewThisOpp && !$scope.renewalCloseDate){
            toastr.error("Renewal close date is required")
            $scope.reasonsRequired = true;
            $scope.mailOptions = true;
        } else if($scope.opp.renewThisOpp && !$scope.renewalAmount){
            toastr.error("Please enter Renewal amount")
            $scope.reasonsRequired = true;
            $scope.mailOptions = true;
        } else if($scope.companyDetails && $scope.companyDetails.closeReasons && $scope.companyDetails.closeReasons.length>0){

            if(!$scope.opp.closeReasons || ($scope.opp.closeReasons && $scope.opp.closeReasons.length === 0)){
                toastr.error("Please select at least one reason")
            } else if(!$scope.opp.closeReasonDescription){
                toastr.error("Please give the reasons")
            } else {
                $scope.reasonsRequired = !$scope.reasonsRequired
            }
        } else {
            $scope.reasonsRequired = !$scope.reasonsRequired
        }
    }

    $scope.addToReasonList = function (reason) {

        if(!$scope.opp.closeReasons || $scope.opp.closeReasons.length == 0){
            $scope.opp.closeReasons = [];
        }

        if(reason.selected){
            $scope.opp.closeReasons.push(reason.name)
        } else {
            $scope.opp.closeReasons = $scope.opp.closeReasons.filter(function (el) {
                return el != reason.name
            })
        }

        $scope.opp.closeReasons = _.uniq($scope.opp.closeReasons)
    }

    $scope.ifOppClose = function (stage) {
        if(_.includes(["Close Lost","Close Won"], stage)){
            $scope.reasonsRequired = true;
            $scope.openViewFor({name:"Close Details",closeOpp:""})
            $scope.mailOptions = true;
            $scope.closingOpp = true;

            // if(new Date($scope.opp.closeDate)> new Date()){
            //     $scope.opp.closeDate = new Date();
            //     $scope.opp.closeDateFormatted = moment().format(standardDateFormat());
            // }

            $scope.opp.closeDate = new Date();
            $scope.opp.closeDateFormatted = moment().format(standardDateFormat());

            _.each($scope.oppDetailsNav,function (item) {
                if(item.name == "Close Details"){
                    item.closeOpp = ""
                }
            })
        } else {
            _.each($scope.oppDetailsNav,function (item) {
                if(item.name == "Close Details"){
                    item.closeOpp = "de-active"
                }
            })

            $scope.closingOpp = false;
            $scope.opp.renewalAmountReq = false;
            $scope.opp.renewalCloseDateReq = false;

            $scope.mailOptions = false;
        }
        $scope.registerDatePickerId()
    }

    function ifOppExists(opportunityId) {

        if (opportunityId) {

            _.each($scope.oppDetailsNav, function (item) {
                if (item.name == "Tasks" || item.name == "Notes") {
                    item.isNewOpp = ""
                }
            })
        } else {
            _.each($scope.oppDetailsNav, function (item) {
                if (item.name == "Tasks" || item.name == "Notes") {
                    item.isNewOpp = "de-active"
                }
            })
        }
    }

    $scope.changeSelection = function () {
        $scope.opp.BANT = {
            budget:$scope.budget,
            authority:$scope.authority,
            need:$scope.need,
            time:$scope.time
        }
    }

    share.openOppForm = function (contactEmailId,productType) {
        $scope.opp = {};

        getAccountTypes($scope,$http,share,$scope.opp);
        masterAccList($scope,$http,$scope.opp);
        $scope.opp.searchContent = '';
        $scope.isOppModalOpen = true;
        $scope.isExistingOpp = false;
        $scope.reasonsRequired = false;
        $scope.oppOwner = {};
        $scope.oppOwner.value = share.liuData.firstName +" "+share.liuData.lastName+" ("+share.liuData.emailId+")"
        $scope.createdDateFormatted = null;
        if(!$scope.opp.currency){

            if(share.companyDetails.currency){
                share.companyDetails.currency.forEach(function (el) {
                    if(el.isPrimary){
                        $scope.opp.currency = el.symbol
                    }
                });
            }
        }

        if(productType){
            $scope.opp.productType = productType;
        }

        if(contactEmailId){
            $scope.opp.contactEmailId = contactEmailId
            $scope.opp.searchContent = contactEmailId;
            $scope.newOppContact = {};
            $scope.newOppContact.emailId = contactEmailId
        }

        resetErrorsField($scope)
    };

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.removeRecipient = function(contact,type){
        if(!$scope.opp.isNotOwner){
            removeRecipient($scope,$http,contact,type)
        }
    }

    $scope.selectContact = function(contact){
        $scope.opp.searchContent  = contact.fullName + " ("+contact.emailId+")"
        $scope.searchContent = contact.fullName + " ("+contact.emailId+")";
        $scope.newOppContact = contact;
        $scope.showResultscontact = false;
        $scope.opp.contactEmailIdReq = false;

        if($scope.isExistingOpp){
            $scope.opp.contactEmailId = contact.emailId
            $scope.opp.mobileNumber = contact.mobileNumber
        }
    }

    $scope.showSelectedDocument = function(document) {
        var documentId = document._id;
        window.location = '/document/index?documentId='+documentId;
    }

    $scope.createDocument = function() {
        window.location = '/document/index?action=createDocument&opportunityId=' + $scope.opp.opportunityId;
    }

});

relatasApp.controller("topContextMenu", function ($scope, $http,$rootScope,share){

    $scope.transferOpp = function () {
        share.transferOpp(share.getUserId())
        $scope.hideExchange = !$scope.hideExchange
    }

    $scope.openPortfolios = function(){
        $scope.displayPortfolios = true;
    }

    share.loadPortfolios = function(company,liu){

        checkTeamLoaded();

        function checkTeamLoaded(){
            if($scope.team){
                buildPortfolioList(_.map($scope.team,"emailId"));
            } else {
                setTimeOutCallback(1000,function () {
                    checkTeamLoaded();
                })
            }
        }

        function buildPortfolioList(reportees){

            $scope.portfolios = [];
            if(company.productList && company.productList.length>0){
                _.each(company.productList,function (el) {
                    if(_.includes(reportees,el.headEmailId)){
                        $scope.portfolios.push({
                            name:el.name,
                            type:"productType",
                            type_format:"Products"
                        })
                    }
                });
            }

            if(company.verticalList && company.verticalList.length>0){
                _.each(company.verticalList,function (el) {
                    if(_.includes(reportees,el.headEmailId)){
                        $scope.portfolios.push({
                            name:el.name,
                            type:"vertical",
                            type_format:"Verticals"
                        })
                    }
                });
            }

            if(company.businessUnits && company.businessUnits.length>0){
                _.each(company.businessUnits,function (el) {
                    if(_.includes(reportees,el.headEmailId)){
                        $scope.portfolios.push({
                            name:el.name,
                            type:"businessUnit",
                            type_format:"Business Units"
                        })
                    }
                });
            }

            if(company.geoLocations && company.geoLocations.length>0){
                _.each(company.geoLocations,function (el) {
                    if(_.includes(reportees,el.headEmailId)){
                        $scope.portfolios.push({
                            name:el.region,
                            type:"region",
                            type_format:"Regions"
                        })
                    }
                });
            }

            $scope.groupedFilters = _
                .chain($scope.portfolios)
                .groupBy('type')
                .map(function(values, key) {
                    return {
                        type:key,
                        type_format:values[0]?values[0].type_format:"",
                        values:values.sort(function (a,b) {
                            if(a.name < b.name) return -1;
                            if(a.name > b.name) return 1;
                            return 0;
                        })
                    };

                })
                .value();

            $scope.widthStyle = "width:142px;"

            if($scope.groupedFilters.length == 2){
                $scope.widthStyle = "width:325px;"
            }

            if($scope.groupedFilters.length == 3){
                $scope.widthStyle = "width:425px;"
            }

            if($scope.groupedFilters.length == 4){
                $scope.widthStyle = "width:560px;"
            }

            if($scope.groupedFilters.length > 4){
                $scope.widthStyle = "min-width:560px;"
            }

        }
    }

    $scope.isAllSelected = function(portfolio){

        if(portfolio){
            var allselected = true;
            portfolio.values.forEach(function (va) {
                if(!va.selected){
                    allselected = false
                }
            })

            portfolio.selected = allselected;
        }
    }

    $scope.selectAll = function(type){
        if(type && type.selected){
            _.each(type.values,function (va) {
                va.selected = true;
            });
        } else {
            _.each(type.values,function (va) {
                va.selected = false;
            });
        }
    }

    $scope.getOppsBasedOnPortfolios = function(){

        var portfolios = [];
        _.each($scope.groupedFilters,function (el) {
            el.values.forEach(function (po) {
                if(po.selected){
                    portfolios.push(po.name+"_type_"+po.type)
                }
            })
        });

        if(portfolios.length>0){
            $scope.portfolioSelected = "portfolioSelected";
        } else {

            $scope.portfolioSelected = "";
        }

        $scope.getOpportunities($scope.selection,null,portfolios);
    }

    $scope.selectTeamMemberAndTransfer = function () {
        share.selectTeamMemberAndTransfer()
    }
    
    $scope.openTargets = function () {
        share.openTargets()
    }

    $scope.openRecommendations = function () {
        share.openRecommendations()
    }

    share.productList = function (productList) {
        $scope.disableProductList = 'disable';
        if(productList && productList.length>0){
            $scope.disableProductList = '';
        }

        share.displayProducts(productList)
    }

    $scope.filter = "current"

    share.setFyFilter = function (filter) {
        $scope.filter = filter
    }

    share.sourceList = function (sourceList) {
        $scope.disableProductList = 'disable';
        if(sourceList && sourceList.length>0){
            $scope.disableProductList = '';
        }

        share.displaySource(sourceList)
    }

    share.displaySource = function (sourceList) {
        $scope.sources = _.cloneDeep(sourceList);
        $scope.sources.push({
            name:"All sources"
        })
    }

    share.displayProducts = function (productList) {
        $scope.productTypes = _.cloneDeep(productList);

        $scope.productTypes.sort(function (a,b) {
            if(a.name < b.name) return -1;
            if(a.name > b.name) return 1;
            return 0;
        });

        $scope.productTypes.unshift({
            name:"All products"
        })
    }

    share.verticalList = function (verticalList) {
        $scope.disableVerticalList = 'disable';
        if(verticalList && verticalList.length>0){
            $scope.disableVerticalList = '';
        }

        share.displayVertical(verticalList)
    }

    share.displayVertical = function (verticalList) {
        $scope.verticals = _.cloneDeep(verticalList);
        $scope.verticals.push({
            name:"All verticals"
        })
    }

    share.geoLocations = function (geoLocations) {
        $scope.geoLocations = _.cloneDeep(geoLocations);

        $scope.geoLocations.push({
            region:$scope.locationSelected
        })
    }

    share.fyList = function (fyList,allQuarters){
        $scope.fyList = fyList;
        $scope.ifCurrentQtr = true;
        $scope.fySelected = !$scope.fySelected?'FY '+fyList[0].year:$scope.fySelected
        $scope.quarters = [];


        for(var key in allQuarters){
            if(allQuarters[key].start){
                $scope.quarters.push({
                    name:moment(allQuarters[key].start).format("MMM")+"-"+moment(allQuarters[key].end).format("MMM"),
                    start:allQuarters[key].start,
                    end:allQuarters[key].end,
                    year:moment(allQuarters["quarter1"].start).format("YYYY")+"-"+moment(allQuarters["quarter4"].end).format("YYYY"),
                    yearStart:allQuarters["quarter1"].start,
                    yearEnd:allQuarters["quarter4"].end
                });
            }

        }
    };

    $scope.openOpportunityForm = function (contactEmailId) {
        share.openOppForm(contactEmailId);
    }

    if(createOppForEmailId){

        checkOpportunitiesControllerLoaded();

        function checkOpportunitiesControllerLoaded(){
            if(share.openOppForm && share.liuData && share.companyDetails){
                $scope.openOpportunityForm(createOppForEmailId)
            } else {
                setTimeOutCallback(100,function () {
                    checkOpportunitiesControllerLoaded();
                })
            }
        }
    }
    
    $scope.getOpportunityWithAccess = function (liu) {
        mixpanelTracker("Opps>Excp Access");
        if(!liu){
            $scope.selection = {
                nameNoImg:"All",
                fullName: "All Team Members",
                noPicFlag:true,
                // userId:teamUserIds
            }

            share.accessControl = true;
            share.fetchOpportunities(share.getUserId(),'accessControl',null)
            share.getTargets(share.getUserId(),'accessControl')
            share.setCompanyDetails(share.companyDetails,'accessControl')
        } else {
            share.accessControl = false;
            share.fetchOpportunities(share.getUserId(),null,null)
            share.getTargets(share.getUserId(),null)
            share.setCompanyDetails(share.companyDetails)
        }
    }

    $scope.getTeam = function () {
        $scope.ifTeam = !$scope.ifTeam;
        $scope.ifProducts = false;
        $http.get('/company/user/hierarchy')
            .success(function (response) {
                $scope.team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.team = buildTeamProfiles(response.Data)
                    $scope.selection = $scope.team[0];
                    share.setUserEmailId($scope.selection.emailId)

                    var usersDictionary = {};
                    share.teamDictionaryByUserId = {};

                    if(response.companyMembers.length>0){
                        var companyMembers = buildTeamProfilesWithLiu(response.companyMembers)
                        share.teamMembers = companyMembers;

                        _.each(companyMembers,function (member) {

                            if(member.corporateAdmin){
                                $rootScope.corporateAdmin = member
                            }

                            usersDictionary[member.emailId] = member;
                            share.teamDictionaryByUserId[member.userId] = member;
                        })
                    }

                    share.selectedUserFromList = $scope.selection;
                    share.setTeamMembers(usersDictionary);
                    var defaultLoad = $scope.selection

                    if(opportunityIdState){
                        share.accessControl = true;
                        share.fetchOpportunities(share.getUserId(),'accessControl',null)
                    } else {
                        $scope.getOpportunities(defaultLoad)
                    }
                }
            });
    }

    share.loadOpps = function () {
        $scope.getTeam();
    }

    $scope.users = [];

    $scope.getOpportunities = function (user,filter,portfolios) {

        mixpanelTracker("Opps>view for user ");

        share.resetFiltersToDefault();

        share.selectedUserFromList = user;
        var userIds = [];

        if(user == "all"){
            userIds = _.map($scope.team,"userId")
        } else {
            userIds = user.userId
        }

        if(share.liuData && share.liuData.orgHead){
            share.getTargets(userIds,share.liuData.companyId);
        } else {
            share.getTargets(userIds);
        }

        $scope.selectFromList = false;
        $scope.selection = user;

        if(share.accessControl){
            share.fetchOpportunities(userIds,{type:"ownerEmailId",ownerEmailId:user.emailId},true,null,portfolios)
        } else {

            if(user == 'all'){

                var teamUserIds = _.map($scope.team,"userId");
                $scope.selection = {
                    nameNoImg:"All",
                    fullName: "All Team Members",
                    noPicFlag:true,
                    userId:teamUserIds
                }
                share.fetchOpportunities(teamUserIds,null,null,null,portfolios)
            } else {
                $scope.selection = user
                share.fetchOpportunities(userIds,null,null,null,portfolios)
            }
        }
    }
    
    share.resetFiltersToDefault = function () {
        $scope.productSelected = "All products";
        $scope.verticalSelected = "All verticals";
        $scope.sourceSelected = "All sources";
        $scope.locationSelected = "All regions";
        $scope.dateRange = "All close dates";
        $scope.companySelected = "All companies";
        $scope.contactSelected = "All contacts";
        $scope.cacheFilter = null
    }

    share.resetFiltersToDefault()
    share.filterOpportunitiesFy =function (item,filterType) {
        $scope.filterOpportunities(item,filterType)
    }

    share.selectedFilters = null;
    share.filterType = null;

    $scope.filterOpportunities = function (item,filterType) {

        mixpanelTracker("Opps>filter "+filterType);
        if(filterType == "fy"){

            $scope.fySelected = 'FY '+item.year;

            var userIds = $scope.selection.userId;

            if($scope.selection.fullName == "All Team Members"){
                userIds = share.liuData._id
            }
            share.fetchOpportunities(userIds,null,false,item)
            // share.getTargets($scope.selection.userId,null,item.start,item.end)
        }

        share.selectedFilters = item;
        share.filterType = filterType;

        if(filterType == "qtr"){
            $scope.fySelected = 'Qtr '+item.name
            share.fetchOpportunities($scope.selection.userId,null,false,item)
            share.getTargets($scope.selection.userId,null,item.yearStart,item.yearEnd,item)
        }

        if(filterType === "vertical"){
            $scope.verticalSelected = item.name;
            $scope.showVerticalList = false;
            $scope.productSelected = "All products";
            $scope.locationSelected = "All regions";
            $scope.dateRange = "All close dates";
            $scope.companySelected = "All companies"
            $scope.contactSelected = "All contacts";
            share.fetchOpportunities($scope.selection.userId,{type:filterType,vertical:$scope.verticalSelected},true)
        }

        if(filterType === "source"){
            $scope.sourceSelected = item.name;
            $scope.showSourceList = false;
            $scope.productSelected = "All products";
            $scope.locationSelected = "All regions";
            $scope.dateRange = "All close dates";
            $scope.companySelected = "All companies"
            $scope.contactSelected = "All contacts";
            share.fetchOpportunities($scope.selection.userId,{type:filterType,source:$scope.sourceSelected},true)
        }

        if(filterType === "product"){
            $scope.productSelected = item.name;
            $scope.showProductList = false;
            $scope.locationSelected = "All regions";
            $scope.dateRange = "All close dates";
            $scope.companySelected = "All companies"
            $scope.contactSelected = "All contacts";
            share.fetchOpportunities($scope.selection.userId,{type:filterType,productType:$scope.productSelected},true)
        }

        if(filterType === "location"){
            $scope.productSelected = "All products";
            $scope.dateRange = "All close dates";
            $scope.companySelected = "All companies"
            $scope.contactSelected = "All contacts";
            $scope.locationSelected = item.region;
            $scope.showLocationList = false;
            share.fetchOpportunities($scope.selection.userId,{type:filterType,location:$scope.locationSelected},true)
        }

        if(filterType === "company"){
            $scope.productSelected = "All products";
            $scope.dateRange = "All close dates";
            $scope.locationSelected = "All regions";
            $scope.contactSelected = "All contacts";
            $scope.companySelected = item.name;
            $scope.showCompanyList = false;

            share.fetchOpportunities($scope.selection.userId,{type:filterType,company:$scope.companySelected},true)
        }

        if(filterType === "contact"){
            $scope.productSelected = "All products";
            $scope.dateRange = "All close dates";
            $scope.locationSelected = "All regions";
            $scope.companySelected = "All companies";
            $scope.contactSelected = getTextLength(item.name,15);
            $scope.showContactList = false;
            $scope.emailIdSelected = item.emailId;  
            var resetContext = false;
            if(contactContextEmailId){
                resetContext = true;
            }

            contactContextEmailId = null;
            share.fetchOpportunities($scope.selection.userId,{type:filterType,name:item.emailId,resetContext:resetContext},true)
        }

    };

    closeAllDropDownsAndModals($scope,".list");
    closeAllDropDownsAndModals($scope,".list-unstyled");

    $scope.showAllDateRange = function () {
        $scope.dateRange = "All close dates";
        share.fetchOpportunities($scope.selection.userId,{type:'date',date:"All close dates"},true)
        $scope.showDateRangePicker = false;
    }
    
    $scope.registerDatePickerId = function(id){

        $('#'+id).datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            // minDate: new Date(moment().subtract(2,"week")),
            // minDate: $scope.startDt && id == "endDt"?new Date($scope.startDt):new Date(),
            onSelectDate: function (dp, $input){

                dp = new Date(dp)
                dp.setHours(23)
                dp.setMinutes(59)
                dp.setSeconds(59)
                
                $scope.$apply(function () {
                    $scope[id] = moment(dp).format("DD MMM YYYY");
                    $scope.dateRange = "Before "+$scope[id]
                    share.fetchOpportunities($scope.selection.userId,{type:'date',date:dp},true)
                    $scope.showDateRangePicker = false;
                    $scope.productSelected = "All products";
                    $scope.locationSelected = "All regions";
                });
            }
        });
    }
    
    share.displayCompanies = function (companies) {
        $scope.companies = _.unionBy(companies,'name')
        $scope.companies.sort(function (a,b) {
            if(a.name < b.name) return -1;
            if(a.name > b.name) return 1;
            return 0;
        });

        $scope.companies.unshift({name:"All companies"})
    }

    share.displayContacts = function (contacts) {
        $scope.contacts = _.unionBy(contacts,"emailId");

        $scope.contacts.sort(function (a,b) {

            if(a.name && b.name){
                if(a.name.toLowerCase() < b.name.toLowerCase()) return -1;
                if(a.name.toLowerCase() > b.name.toLowerCase()) return 1;
                return 0;
            } else {
                return 0;
            }

        });
        $scope.contacts.unshift({name:"All contacts",emailId:"All contacts"})
    }

});

relatasApp.controller("oppTargets", function ($scope,$http,$rootScope,share){

    share.openTargets = function () {
        $scope.loadTargets = !$scope.loadTargets;

        if($scope.loadTargets){
            getTargets()
        }
    }

    function getTargets(){
        $scope.loadingTargets = true;
        var url = fetchUrlWithParameter("/opportunities/by/month/year","userId", $scope.user)

        if($scope.filter && $scope.filter == 'accessControl'){
            url = fetchUrlWithParameter(url+"&accessControl="+true)
        }

        if($scope.fromDate && $scope.endDate){
            url = fetchUrlWithParameter(url+"&fromDate="+$scope.fromDate)
            url = fetchUrlWithParameter(url+"&toDate="+$scope.endDate)
        }

        setTimeOutCallback(10,function () {
            drawTargets($scope,$http,url,share,$scope.switchQtr)
        })
    }
    
    share.getTargets = function (user,filter,fromDate,endDate,switchQtr) {
        $scope.user = user;
        $scope.filter = filter;
        $scope.fromDate = fromDate;
        $scope.endDate = endDate;
        $scope.switchQtr = switchQtr;
        getTargets();
    }

});

relatasApp.controller("recommendations",function ($scope,$http,share) {

    share.openRecommendations = function () {
        $scope.recommendationsShow = !$scope.recommendationsShow
    }

    $scope.closeRecommendations = function () {
        $scope.recommendationsShow = false;
        share.recommendationConversion = false;
    }

    $scope.showActionItemRE = true;

    $scope.ignoreRecommendation = function (op) {

        $http.post("/recommendation/ignore",op)
            .success(function (response) {
                getRecommendations($scope,$http)
            });
    }

    // getRecommendations($scope,$http);

    $scope.convertRecommendation = function (op) {
        share.recommendationToConvert = op;
        share.recommendationConversion = true;
        share.openOppForm(null,op.recommendedProduct);
    }

    share.updateRecommendationsWithConvertedOpp = function(){
        $http.post("/recommendation/convert/to/opp",share.recommendationToConvert)
            .success(function (response) {
            });
    }

    // $scope.recommendationsShow = true;
})

function getRecommendations($scope,$http){

    // $scope.recommendationsShow = true;

    $http.get("/recommendations/getAccountOppsInsights")
        .success(function (response) {
            if(response && response.Data && response.Data.length>0){
                $scope.opps = response.Data;
            }
        });
}

function startOppTransfer($scope,$http,emailId,hierarchyParent,companyId,share,$rootScope){

    var contacts = [];
    var toTransfer = [];

    _.each($scope.oppRemoveList,function (op) {
        contacts.push(op.contactEmailId)

        if(op.partners && op.partners.length>0){
            contacts = contacts.concat(_.map(op.partners,"emailId"))
        }

        if(op.decisionMakers && op.decisionMakers.length>0){
            contacts = contacts.concat(_.map(op.decisionMakers,"emailId"))
        }

        if(op.influencers && op.influencers.length>0){
            contacts = contacts.concat(_.map(op.influencers,"emailId"))
        }
        
        toTransfer.push({
            transferredBy:share.liuData.emailId,
            from:op.userEmailId,
            fromUserId:op.userId,
            to:$scope.selectedUser.userId,
            opps:op.opportunityId,
            contacts:_.uniq(contacts),
            emailId:$scope.selectedUser.emailId,
            reportingManagerForUser1:share.team[op.userEmailId].userId,
            reportingManagerForUser2:$scope.selectedUser.userId,
            companyId:companyId,
            note:$scope.note
        })
    });

    var obj = {
        toTransfer:_.uniqBy(toTransfer,"opps")
    };
    
    transferOpportunities($scope,$rootScope,$http,share,obj)
}

function transferOpportunities($scope,$rootScope,$http,share,obj){
    
    $http.post("/opportunities/transfer",obj)
        .success(function (response) {

            toastr.success("Opportunity transferred successfully");

            $scope.showTeamMembers = false;
            $scope.isOppModalOpen = false;
            $scope.transferOpportunities = false;

            var opps = _.map($scope.oppRemoveList,"opportunityName")
            var subject = opps.length + " Opportunities transferred."

            var intro = ""

            $scope.add_cc = [];

            var reportingManagerOne = null
            var reportingManagerTwo = null

            if(response && response.reportingManager && response.reportingManager.length>0){
                reportingManagerOne = response.reportingManager[0];
                reportingManagerTwo = response.reportingManager[1];
            }

            if(reportingManagerOne && reportingManagerTwo){
                intro = reportingManagerOne.firstName +" & "+reportingManagerTwo.firstName

                $scope.add_cc.push(reportingManagerOne.emailId)
                $scope.add_cc.push(reportingManagerTwo.emailId)

                if(reportingManagerOne.emailId == reportingManagerTwo.emailId){
                    intro = reportingManagerOne.firstName
                }

            } else if(!reportingManagerOne && reportingManagerTwo){
                intro = reportingManagerTwo.firstName
                $scope.add_cc.push(reportingManagerTwo.emailId)
            } else if(reportingManagerOne && !reportingManagerTwo){
                intro = reportingManagerOne.firstName
                $scope.add_cc.push(reportingManagerOne.emailId)
            }

            var oppString = "";

            _.each(opps,function (op,index) {
                oppString = oppString+op+",\n"
            })

            $scope.add_cc.push($rootScope.corporateAdmin.emailId)
            $scope.add_cc.push(share.selectedUserFromList.emailId)

            var receiverName = $scope.selectedUser.firstName
            var oppDetails = "The Following opportunities have been transferred from " +share.selectedUserFromList.emailId+" to "+$scope.selectedUser.emailId
                +'\n'+ oppString

                +'\n\n\n'+getSignature(share.liuData.firstName+' '+share.liuData.lastName,share.liuData.designation,share.liuData.companyName,share.liuData.publicProfileUrl)
                // +'\n\n Powered by Relatas';

            intro = "Hi "+receiverName+", "+intro
            var body = intro+",\n\n"+oppDetails;

            $scope.add_cc = _.uniq($scope.add_cc);

            if(share.selectedUserFromList == "all"){
                var teamUserIds = []
                for(var key in share.team){
                    teamUserIds.push(share.team[key].userId)
                }

                share.fetchOpportunities(teamUserIds,null,null)
            }else {
                share.fetchOpportunities(share.selectedUserFromList.userId,null,null)
            }

            sendEmail($scope,$http,subject,body,null,share,share.liuData._id,$scope.selectedUser.emailId,$scope.cPhone,share.liuData,null,$scope.selectedUser.emailId)
        });
}

function drawTargets($scope,$http,url,share,switchQtr){

    $http.get(url)
        .success(function (response) {

            var quarters = [
                {
                    quarter:1,
                    startMonth:0,
                    endMonth:2
                },{
                    quarter:2,
                    startMonth:3,
                    endMonth:5
                },{
                    quarter:3,
                    startMonth:6,
                    endMonth:8
                },{
                    quarter:4,
                    startMonth:9,
                    endMonth:11
                }
            ];

            var targetForFy = 0;
            var total = _.map(response.Data,"total")
            var won = _.map(response.Data,"won")
            var lost = _.map(response.Data,"lost")
            var target = _.map(response.Data,"target")

            var values = _.flatten(_.concat(total,won,lost,target))

            var min = Math.min.apply( null, values );
            var max = Math.max.apply( null, values );

            var pipeline = 0;
            var currentQuarter = {};

            _.each(quarters,function (q) {
                if (q.startMonth <= new Date().getMonth() && q.endMonth >= new Date().getMonth()) {
                    currentQuarter = q;
                }
            });

            if(switchQtr){
                currentQuarter.startMonth = (moment(switchQtr.start).format("M"))-1,
                currentQuarter.endMonth = (moment(switchQtr.end).format("M"))-1
            }

            currentQuarter.values = [];
            currentQuarter.target = 0;
            currentQuarter.pipeline = 0;
            currentQuarter.won = 0;
            currentQuarter.lost = 0;

            var currentMonth = monthNames[new Date().getMonth()]

            $scope.cqHeader = monthNames[currentQuarter.startMonth].substr(0,3)+" - "+monthNames[currentQuarter.endMonth].substr(0,3) +" "+new Date().getFullYear();
            _.each(response.Data,function (value) {
                value.won = value.won?value.won:0;
                value.lost = value.lost?value.lost:0;
                value.total = value.total?value.total:0;
                value.openValue = value.openValue?value.openValue:0;

                targetForFy = targetForFy+value.target;

                var thisMonth = monthNames[new Date(moment(value.sortDate).subtract(1,"d")).getMonth()];

                if(thisMonth == currentMonth && new Date(value.sortDate).getFullYear() == new Date().getFullYear()){
                    value.highLightCurrentMonth = true
                }

                pipeline = pipeline+value.openValue
                
                if (currentQuarter.startMonth <= new Date(value.sortDate).getMonth() && currentQuarter.endMonth >= new Date(value.sortDate).getMonth()){
                    currentQuarter.target = currentQuarter.target+parseFloat(value.target)
                    currentQuarter.won = currentQuarter.won+value.won

                    currentQuarter.pipeline = currentQuarter.pipeline+value.openValue
                    currentQuarter.lost = currentQuarter.lost+value.lost
                }

                value.heightWon = {'height':scaleBetween(value.won,min,max)+'%'}
                value.heightLost = {'height':scaleBetween(value.lost,min,max)+'%'}
                value.heightTotal = {'height':scaleBetween(value.openValue,min,max)+'%'}
                value.heightTarget = {'height':scaleBetween(value.target,min,max)+'%'}

                value.won = numberWithCommas(value.won.r_formatNumber(2),share.primaryCurrency == "INR");
                value.lost = numberWithCommas(value.lost.r_formatNumber(2),share.primaryCurrency == "INR");
                value.openValue = numberWithCommas(value.openValue.r_formatNumber(2),share.primaryCurrency == "INR");
                value.target = numberWithCommas(value.target.r_formatNumber(2),share.primaryCurrency == "INR");
            });

            share.setTargetForFy(targetForFy)

            var allValues = [currentQuarter.pipeline,currentQuarter.lost,currentQuarter.won,currentQuarter.target]

            var cqMin = Math.min.apply( null, allValues );
            var cqMax = Math.max.apply( null, allValues );

            $scope.cqTarget = numberWithCommas(currentQuarter.target.r_formatNumber(2),share.primaryCurrency == "INR")
            // $scope.cqLost = {'width':scaleBetween(currentQuarter.lost,cqMin,cqMax)+'%',background: '#6dc3b8'}
            $scope.cqWonStyle = {'width':scaleBetween(currentQuarter.won,0,currentQuarter.target)+'%',background: '#8ECECB'}
            $scope.cqWon = numberWithCommas(currentQuarter.won.r_formatNumber(2),share.primaryCurrency == "INR")
            $scope.cqPipeline = numberWithCommas(currentQuarter.pipeline.r_formatNumber(2),share.primaryCurrency == "INR")

            if(currentQuarter.won && !currentQuarter.target){
                $scope.cqWonStyle = {'width':100+'%',background: '#8ECECB'}
            }

            if(currentQuarter.target){
                $scope.cqWonPercentage = ((currentQuarter.won/currentQuarter.target)*100).r_formatNumber(2)+'%'
            } else {
                $scope.cqWonPercentage = "-"
            }

            if(currentQuarter.won>currentQuarter.target){
                $scope.cqWonStyle = {'width':100+'%',background: '#8ECECB'}
                $scope.cqWonPercentage = 100+'%'
            }

            var t = _.sum(target);
            var w = _.sum(won);
            var g = t - w;

            var valArr = [];
            valArr.push(t,w,g,pipeline)

            var vmin = Math.min.apply( null, valArr );
            var vmax = Math.max.apply( null, valArr );

            $scope.fyWonStyle = {'width':scaleBetween(w,1,t)+'%',background: '#8ECECB'}

            if(!w || w ==0 ){
                $scope.fyWonStyle = {'width':0+'%',background: '#8ECECB'}
            }

            $scope.target = {'width':scaleBetween(t,vmin,vmax)+'%',background: '#6dc3b8'}
            $scope.pipeline = {'width':scaleBetween(pipeline,vmin,vmax)+'%',background: '#767777'}
            $scope.won = {'width':scaleBetween(w,vmin,vmax)+'%',background: '#8ECECB'}
            $scope.gap = {'width':scaleBetween(g,vmin,vmax)+'%',background: '#e74c3c'}

            if(t){
                $scope.wonPercentage = ((w/t)*100).r_formatNumber(2)+'%'
            } else {
                $scope.wonPercentage = "-"
            }

            if(w>t){
                $scope.fyWonStyle = {'width':100+'%',background: '#8ECECB'}
                $scope.won = {'width':100+'%',background: '#8ECECB'}
                $scope.wonPercentage = 100+'%'
            }

            $scope.targetCount = numberWithCommas(t.r_formatNumber(2),share.primaryCurrency == "INR");
            $scope.wonCount = numberWithCommas(w.r_formatNumber(2),share.primaryCurrency == "INR");
            $scope.gapCount = numberWithCommas(g.r_formatNumber(2),share.primaryCurrency == "INR");
            $scope.pipelineCount = numberWithCommas(pipeline.r_formatNumber(2),share.primaryCurrency == "INR");

            $scope.targetGraph = response.Data;

            $scope.targetGraph.sort(function (o1, o2) {
                return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
            });

            $scope.loadingTargets = false;
        })
}

function buildTeamProfiles(data) {
    var team = [];
    var liu = {}
    _.each(data,function (el) {

        if(!el.hierarchyParent){
            liu = {
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                imageUrl:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                firstName:el.firstName,
                lastName:el.lastName,
                noPicFlag:true,
                nameNoImg:el.firstName.substr(0,2).toUpperCase()
            };
        } else {
            team.push({
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                imageUrl:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                lastName:el.lastName,
                firstName:el.firstName,
                noPicFlag:true,
                nameNoImg:el.firstName.substr(0,2).toUpperCase()
            })
        }
    });

    team.unshift(liu);
    return team;
}

function buildTeamProfilesWithLiu(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            firstName:el.firstName,
            lastName:el.lastName,
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            hierarchyParent:el.hierarchyParent,
            corporateAdmin:el.corporateAdmin,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase()
        })
    });

    return team;
}

function drawCompanyStages($scope,share,stages,response,liuEmailId) {

    $scope.stages = [];
    $scope.companies = [];
    $scope.contacts = [];

    $scope.teamDictionary = share.getTeamMembers();
    var numOfCols = Math.floor(12/stages.length); // Mimic bootstrap's number of columns

    $scope.rowType = ""
    $scope.colType = ""

    if(numOfCols<=3){
        numOfCols = 3;
        numOfCols = numOfCols+" col-width-20"
        $scope.rowType = "no-wrap-row"
        $scope.colType = "inline-fix"
    }

    if(stages.length == 5){
        numOfCols = numOfCols+" col-width-20"
    }

    _.each(stages,function (stage,index) {

        $scope.stages.push({
            colClass:"col-md-"+numOfCols,
            name:stage,
            firstChild:index === 0?"first-child":"",
            lastChild:index === (stages.length -1)?"last-child":"",
            lastChildCol:index === (stages.length -1)?"last-child-col":"",
            values:[],
            sumOfAmount:0
        });
    });

    _.each($scope.stages,function (stage) {
        _.each(response.data,function (opp) {
            _.each(opp.opportunities,function (o) {

                o.closeReasons = _.uniq(o.closeReasons);

                if(o.createdByEmailId != o.userEmailId){
                    o.transferredOpp = "transferred-opp"
                };

                o.amount = parseFloat(o.amount);
                o.stageStyle2 = oppStageStyle(o.stageName,stages.indexOf(o.stageName),false,true);
                o.netGrossMargin = parseFloat(o.netGrossMargin);

                if(o.contactEmailId){
                    $scope.contacts.push({
                        emailId:o.contactEmailId,
                        name:o.fullName?o.fullName:o.contactEmailId
                    });
                }

                if(o.contactEmailId){
                    o.account = fetchCompanyFromEmail(o.contactEmailId)
                } else {
                    o.account = "Others"
                }

                if(o.stageName === stage.name){
                    o.isStale = o.closeDate && o.stageName !="Closed Lost" && o.stageName !="Closed Won" && o.stageName !="Close Lost" && o.stageName !="Close Won" && new Date(o.closeDate)<new Date(moment().startOf("day"))
                    stage['values'].push(o)
                }

                $scope.companies.push({
                    name:o.account
                });

                if(o.createdByEmailId){
                    o["creator"] = $scope.teamDictionary[o.createdByEmailId]
                } else {
                    o["creator"] = o.userEmailId?$scope.teamDictionary[o.userEmailId]:{};
                }

                o["owner"] = o.userEmailId && $scope.teamDictionary[o.userEmailId]?$scope.teamDictionary[o.userEmailId]:{fullName:o.userEmailId,emailId:o.userEmailId};

                o.owner.noPicFlag = true;
                o.owner.nameNoImg = o.owner.fullName.substr(0,2).toUpperCase();
                if(o.partners && o.partners.length>0) {
                    _.each(o.partners,function (el) {
                        if(el){
                            $scope.contacts.push({
                                emailId:el.emailId,
                                name:el.fullName?el.fullName:el.emailId
                            });
                        }
                    })
                }

                if(o.influencers && o.influencers.length>0) {
                    _.each(o.influencers,function (el) {
                        if(el){
                            $scope.contacts.push({
                                emailId:el.emailId,
                                name:el.fullName?el.fullName:el.emailId
                            });
                        }
                    })
                }

                if(o.decisionMakers && o.decisionMakers.length>0) {
                    _.each(o.decisionMakers,function (el) {
                        if(el){
                            $scope.contacts.push({
                                emailId:el.emailId,
                                name:el.fullName?el.fullName:el.emailId
                            });
                        }
                    })
                }

                o.stage = o.stageName;
                o.transferThis = true;
                o.selected = "opp-selected"
            })
        });

        _.each(stage.values,function (val) {

            val.convertedAmt = val.amount;
            val.convertedAmtWithNgm = val.amountWithNgm

            if(val.currency && val.currency !== share.primaryCurrency){
                if(share.currenciesObj[val.currency] && share.currenciesObj[val.currency].xr){
                    val.convertedAmt = val.amount/share.currenciesObj[val.currency].xr
                }

                if(val.netGrossMargin || val.netGrossMargin == 0){
                    val.convertedAmtWithNgm = (val.convertedAmt*val.netGrossMargin)/100
                } else {
                    val.convertedAmtWithNgm = val.convertedAmt;
                }

                val.convertedAmt = parseFloat(val.convertedAmt)
            }

            stage.sumOfAmount = stage.sumOfAmount+parseFloat(val.convertedAmtWithNgm);

            stage.sumOfAmountWithCommas = numberWithCommas(stage.sumOfAmount.r_formatNumber(2),share.primaryCurrency == "INR");
            // stage.sumOfAmount = stage.sumOfAmount+parseFloat();

            val.isNotOwner = false;
            val.canTransferOpp = false;
            if(val.userEmailId === liuEmailId || share.liuData.corporateAdmin){
                val.isNotOwner = false;
                val.canTransferOpp = true;
            }

            val.isOppOwner = false;
            if(val.userEmailId === liuEmailId){
                val.isOppOwner = true;
            } else {
                val.nonOwnerHighlight = "highlight"
            }

            val.closeDateFormatted = moment(val.closeDate).format(standardDateFormat());

            val.isOppClosed = _.includes(val.stage.toLowerCase(),"close");
            val.amountWithCommas = numberWithCommas(val.amount.r_formatNumber(2),share.primaryCurrency == "INR")

            val.amountWithNgmCommas = numberWithCommas(parseFloat(val.amountWithNgm).r_formatNumber(2),share.primaryCurrency == "INR")
        });

        stage.values.sort(function (o1, o2) {
            return o1.closeDate > o2.closeDate ? 1 : o1.closeDate < o2.closeDate ? -1 : 0;
        });

        var x = Math.floor(stage.sumOfAmount * 100) / 100;
        stage.sumOfAmount = numberWithCommas(x.r_formatNumber(2),share.primaryCurrency == "INR");
    });

    share.displayCompanies($scope.companies)
    share.displayContacts($scope.contacts)
    window.localStorage['oppsInCommitStage'] = "";
    window.localStorage['teamMembers'] = "";
    window.localStorage['relatasLocalDb'] = JSON.stringify($scope.stages);
}

function getContactProfileInfo(contactDetails,item){

    if(contactDetails){

        if(checkRequired(contactDetails.personId) && checkRequired(contactDetails.personName)){
            var name = getTextLength(contactDetails.personName,20);
            var image = '/getImage/'+contactDetails.personId;

            item["fullName"] = contactDetails.personName
            item["name"] = checkRequired(contactDetails.personName)?contactDetails.personName:contactDetails.personEmailId
            item["image"] = image
        }
        else {
            var contactImageLink = contactDetails.contactImageLink ? encodeURIComponent(contactDetails.contactImageLink) : null
            item["fullName"] = contactDetails.personName
            item["name"] = checkRequired(contactDetails.personName)?contactDetails.personName:contactDetails.personEmailId
            item["image"] = '/getContactImage/' + contactDetails.personEmailId + '/' + contactImageLink
        }

        item["noPicFlag"] = true

        item["nameNoImg"] = item["name"].substr(0,2).toUpperCase()

    } else {

        item["fullName"] = item.contactEmailId
        item["name"] = item.contactEmailId
        item["noPicFlag"] = true
        item["nameNoImg"] = item["name"].substr(0,2).toUpperCase()
    }
}

function getContactProfileInfoFromDb($http,item){
    item["name"] = getTextLength(item["fullName"], 13)
    item["noPicFlag"] = true;
    item["nameNoImg"] = item["fullName"].substr(0,2).toUpperCase()
}

function saveExistingOpportunity($scope,$http,share,$rootScope){

    if(!checkOppFieldsRequirement($scope,$rootScope,share)){
        $scope.town = $scope.town?$scope.town.replace(/[^a-zA-Z ]/g, ""):$scope.opp.geoLocation.town
        $scope.opp.name = $scope.opp.opportunityName;
        var daysToClose = moment.duration(moment(new Date()).diff(moment($scope.opp.createdDate)));

        if($scope.town){
            $scope.opp.geoLocation.town = $scope.town;
        }

        $http.post("/salesforce/edit/opportunity/for/contact", {
            opportunity: $scope.opp,
            contactEmailId: $scope.opp.contactEmailId,
            contactMobile: $scope.opp.mobileNumber,
            town:$scope.town,
            zone:$scope.opp.geoLocation?$scope.opp.geoLocation.zone:null,
            mailOrgHead:$scope.opp.mailOrgHead,
            mailRm:$scope.opp.mailRm,
            companyId:$rootScope.companyDetails._id,
            hierarchyParent:share.liuData.hierarchyParent,
            renewalCloseDate:$scope.renewalCloseDate,
            renewalAmount:$scope.renewalAmount
        })
            .success(function (response) {

                if(response.SuccessCode){
                    toastr.success("Opportunity saved successfully");

                    if(new Date($scope.opp.closeDate)>=new Date(share.allQuarters["quarter4"].end)){
                        share.filterOpportunitiesFy('future','fy')
                        share.setFyFilter("future")
                    } else if(new Date($scope.opp.closeDate)<=new Date(share.allQuarters["quarter1"].start)){
                        share.filterOpportunitiesFy('past','fy')
                        share.setFyFilter("past")
                    } else if(share.selectedFilters,share.filterType && share.filterType != 'contact' && share.filterType != 'company' && share.filterType != 'product'){
                        share.filterOpportunitiesFy('current','fy')
                        share.setFyFilter("current")
                    } else if(share.filterType && share.selectedFilters){
                        share.setFyFilter("current")
                    } else if(share.selectedUserFromList){
                        share.fetchOpportunities(share.selectedUserFromList.userId);
                        share.setFyFilter("current")
                    } else {
                        share.fetchOpportunities(share.getUserId());
                        share.setFyFilter("current")
                    }

                    $scope.renewalAmount = 0;
                    $scope.renewalCloseDate = "";

                    oppTargetMetNotifications($http,$scope);

                    sendOpportunityCloseSummary($scope,$http,share,$rootScope, response);
                    $scope.closeModal()
                    share.getTargets(share.getUserId());
                } else {
                    toastr.error("Opportunity save failed. Please try again later")
                }
            });
    }
}

function addNewOpportunity($scope,$http,share,$rootScope){

    if(!checkOppFieldsRequirement($scope,$rootScope,share)) {

        if($scope.town){
            $scope.town = $scope.town.replace(/[^a-zA-Z ]/g, "");
        } else if ($scope.opp.geoLocation && $scope.opp.geoLocation.town){
            $scope.town = $scope.opp.geoLocation.town
        }

        $scope.opp.name = $scope.opp.opportunityName;

        var data = {
            opportunity: $scope.opp,
            contactEmailId: $scope.newOppContact.emailId,
            contactMobile: $scope.newOppContact.mobileNumber?$scope.newOppContact.mobileNumber:null,
            town:$scope.town,
            zone:$scope.opp.geoLocation?$scope.opp.geoLocation.zone:null,
            renewalCloseDate:$scope.renewalCloseDate,
            renewalAmount:$scope.renewalAmount,
            companyId:$rootScope.companyDetails._id,
            hierarchyParent:share.liuData.hierarchyParent
        }

        createOpportunity($http,$scope,data,function (response) {

            if(response.SuccessCode){
                toastr.success("Opportunity added successfully");

                if(new Date($scope.opp.closeDate)>=new Date(share.allQuarters["quarter4"].end)){
                    share.filterOpportunitiesFy('future','fy')
                    share.setFyFilter("future")
                } else if(new Date($scope.opp.closeDate)<=new Date(share.allQuarters["quarter1"].start)){
                    share.filterOpportunitiesFy('past','fy')
                    share.setFyFilter("past")
                } else {
                    share.fetchOpportunities(share.getUserId());
                    share.setFyFilter("current")
                }

                share.getTargets(share.getUserId());
                if(share.recommendationConversion){
                    share.updateRecommendationsWithConvertedOpp();
                }

                if(createOppForEmailId){
                    createOppForEmailId = null;
                    window.location = "/opportunities/all"
                }

                sendOpportunityCloseSummary($scope,$http,share,$rootScope, response);

                $scope.closeModal()

            } else {
                toastr.error("Opportunity not created. Please try again later")
            }
        });
    }
}

function handleProductTypesFilter($scope,productType,share) {

    $scope.stages = JSON.parse(window.localStorage['relatasLocalDb']);

    if(productType != "All products"){
        _.each($scope.stages,function (stage) {
            stage.sumOfAmount = 0;
            _.remove(stage.values,function (el) {
                return el.productType != productType
            });
            //Recalculate the total opp value at each stage;
            recalculateOppValues(stage,$scope,share)
        });
    } else {
        $scope.oppsLoading = false;
    }
}

function handleRegionTypesFilter($scope,region,share) {

    $scope.stages = JSON.parse(window.localStorage['relatasLocalDb']);

    if(region != "All regions"){
        _.each($scope.stages,function (stage) {
            stage.sumOfAmount = 0;
            _.remove(stage.values,function (el) {
                if(el.geoLocation && el.geoLocation.zone){
                    return el.geoLocation.zone != region
                } else {
                    return true;
                }
            });

            // Recalculate the total opp value at each stage;
            recalculateOppValues(stage,$scope,share)
        });
    }
}

function handleOwnerEmailIdFilter($scope,ownerEmailId,share) {

    $scope.stages = JSON.parse(window.localStorage['relatasLocalDb']);

    _.each($scope.stages,function (stage) {
        stage.sumOfAmount = 0;
        _.remove(stage.values,function (el) {
            if(el.userEmailId){
                return el.userEmailId!= ownerEmailId
            } else {
                return true;
            }
        });

        // Recalculate the total opp value at each stage;
        recalculateOppValues(stage,$scope,share)
    });
}

function removeOpp($scope,opp,oppList,share) {
    _.remove(oppList,function (el) {
        return el.opportunityId == opp.opportunityId
    });
}

function handleVerticalTypesFilter($scope,verticalType,share) {

    $scope.stages = JSON.parse(window.localStorage['relatasLocalDb']);

    if(verticalType != "All verticals"){
        _.each($scope.stages,function (stage) {
            stage.sumOfAmount = 0;
            _.remove(stage.values,function (el) {
                return el.vertical != verticalType
            });

            //Recalculate the total opp value at each stage;
            recalculateOppValues(stage,$scope,share)
        });
    }
}

function handleSourceTypesFilter($scope,sourceType,share) {

    $scope.stages = JSON.parse(window.localStorage['relatasLocalDb']);

    if(sourceType != "All sources"){
        _.each($scope.stages,function (stage) {
            stage.sumOfAmount = 0;
            _.remove(stage.values,function (el) {
                return el.sourceType != sourceType
            });

            //Recalculate the total opp value at each stage;
            recalculateOppValues(stage,$scope,share)
        });
    }
}

function handleEndDateFilter($scope,date,share) {

    $scope.stages = JSON.parse(window.localStorage['relatasLocalDb']);

    if(date != "All close dates"){
        _.each($scope.stages,function (stage) {
            stage.sumOfAmount = 0;
            _.remove(stage.values,function (el) {
                if (el.closeDate && new Date(el.closeDate) <= new Date(date)) {
                    return false
                } else {
                    return true;
                }
            })

            // Recalculate the total opp value at each stage;
            recalculateOppValues(stage,$scope,share)
        });
    }
}

function handleCompanyFilter($scope,company,share) {

    $scope.stages = JSON.parse(window.localStorage['relatasLocalDb']);

    if(company != "All companies"){
        _.each($scope.stages,function (stage) {
            stage.sumOfAmount = 0;
            _.remove(stage.values,function (el) {
                return el.account != company
            });
            // Recalculate the total opp value at each stage;
            recalculateOppValues(stage,$scope,share)
        });
    } else {
        $scope.oppsLoading = false;
    }
}

function handleContactFilter($scope,contact,share) {

    $scope.stages = JSON.parse(window.localStorage['relatasLocalDb']);

    if(contact != "All contacts"){
        _.each($scope.stages,function (stage) {
            stage.sumOfAmount = 0;
            _.remove(stage.values,function (el) {

                if(el.contactEmailId == contact){
                    return false;
                } else if(el.partners && _.includes(_.map(el.partners,"emailId"), contact) ||
                    el.decisionMakers && _.includes(_.map(el.decisionMakers,"emailId"), contact) ||
                    el.influencers && _.includes(_.map(el.influencers,"emailId"), contact)){
                    return false;
                } else {
                    return true;
                }

            });
            // Recalculate the total opp value at each stage;
            recalculateOppValues(stage,$scope,share)
        });
    } else {
        $scope.oppsLoading = false;
    }
}

function recalculateOppValues(stage,$scope,share) {
    _.each(stage.values,function (val) {

        val.convertedAmt = val.amount;
        val.convertedAmtWithNgm = val.amountWithNgm

        if(val.currency && val.currency !== share.primaryCurrency){
            if(share.currenciesObj[val.currency] && share.currenciesObj[val.currency].xr){
                val.convertedAmt = val.amount/share.currenciesObj[val.currency].xr
            }

            if(val.netGrossMargin || val.netGrossMargin == 0){
                val.convertedAmtWithNgm = (val.convertedAmt*val.netGrossMargin)/100
            } else {
                val.convertedAmtWithNgm = val.convertedAmt;
            }

            val.convertedAmt = parseFloat(val.convertedAmt)
        }

        stage.sumOfAmount = stage.sumOfAmount+parseFloat(val.convertedAmtWithNgm);
    });

    if($scope){
        $scope.oppsLoading = false;
    }

    var x = Math.floor(stage.sumOfAmount * 100) / 100;
    stage.sumOfAmount = x;

    stage.sumOfAmountWithCommas = numberWithCommas(stage.sumOfAmount.r_formatNumber(2),share.primaryCurrency == "INR");
}

function closeAllDropDownsAndModals($scope,id,isModal) {
    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                $scope.showVerticalList = false;
                $scope.showProductList = false;
                $scope.selectFromList = false;
                $scope.showLocationList = false;
                $scope.showDateRangePicker = false;
                $scope.showCompanyList = false;
                $scope.showContactList = false;
                $scope.showSourceList = false;
                $scope.displayPortfolios = false;
                $scope.fy = false;
                if(isModal){
                    $scope.closeModal()
                }
            })
        }
    });
}

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){

                //To remove
                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else{
                    obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                scope.$apply(attrs.imageonload);
            });
        }
    };
});

relatasApp.directive('searchResultsD', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultsdecisionMaker">' +
        '<div ng-repeat="item in decisionMakers track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'+
        // '<div class="input-text-error" ng-show="decisionMakersNotFound">No contacts found. Please create a contact before adding <br> the decision maker.</div>'
        '<div class="input-text-error" ng-show="decisionMakersNotFound"> ' +
            'We couldn"t find the contact. click .<span style="cursor:pointer; color:#267368" ng-click="createContactWindow()"> here</span> to create' +
        '</div>'
    };
});

relatasApp.directive('searchResultsI', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultsinfluencer">' +
        '<div ng-repeat="item in influencers track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'+
        // '<div class="input-text-error" ng-show="influencersNotFound">No contacts found. Please create a contact before adding<br> the influencer.</div>'
        '<div class="input-text-error" ng-show="influencersNotFound"> ' +
        'We couldn"t find the contact. click .<span style="cursor:pointer; color:#267368" ng-click="createContactWindow()"> here</span> to create' +
        '</div>'
    };
});

relatasApp.directive('searchResultsP', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultspartner">' +
        '<div ng-repeat="item in partners track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'+
        // '<div class="input-text-error" ng-show="partnersNotFound">No contacts found. Please create a contact before adding<br> the partner.</div>'
        '<div class="input-text-error" ng-show="partnersNotFound"> ' +
                'We couldn"t find the contact. click .<span style="cursor:pointer; color:#267368" ng-click="createContactWindow()"> here</span> to create' +
            '</div>'
    };
});

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function deleteHashtag($http,p_id,hashtag,contactEmailId) {

    if(p_id){
        $http.get("/api/opportunities/hashtag/delete?contactId=" + p_id + "&hashtag=" + hashtag+"&contactEmailId="+contactEmailId+"&type="+hashtag)
            .success(function (response) {
                if (response.SuccessCode) {
                    // toastr.success("Hashtag Deleted");
                }
                else {
                    // toastr.error("Hashtag Delete Failed. Please try again later");
                }
            });
    }
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function attachComponents (existingComponent,newComponent){
    return existingComponent?existingComponent+newComponent:newComponent
}

function sendEmail($scope,$http,subject,body,prevMessage,share,userId,emailId,mobileNumber,liu,contactDetails,internalMailRecName){

    var respSentTimeISO = moment().format();

    if(!checkRequired(subject)){
        // toastr.error("Please enter the subject.")
    }
    else if(!checkRequired(body)){
        // toastr.error("Please enter the message.")
    }
    else{

        if(prevMessage && prevMessage.dataObj != null){
            body = body+'\n\n\n'+prevMessage.dataObj.bodyContent;
            $scope.reminder = prevMessage.dataObj.compose_email_remaind
        }

        var obj = {
            email_cc:$scope.add_cc.length>0?$scope.add_cc:null,
            receiverEmailId:internalMailRecName?emailId:$scope.cEmailId,
            receiverName:internalMailRecName,
            message:body,
            subject:subject,
            receiverId:"",
            docTrack:true,
            trackViewed:true,
            remind:$scope.reminder,
            respSentTime:respSentTimeISO,
            isLeadTrack:false,
            newMessage:prevMessage?false:true
        };

        if(internalMailRecName){
            obj.receiverEmailId = emailId
            obj.receiverName = internalMailRecName
            obj.receiverId = ''
        }

        if(prevMessage && prevMessage != null && prevMessage.updateReplied){
            obj.updateReplied = true;
            obj.refId = prevMessage.dataObj.interaction.refId
        }

        //Used for Outlook
        if(prevMessage && prevMessage.dataObj && prevMessage.dataObj.interaction && prevMessage.dataObj.interaction.refId){
            obj.id = prevMessage.dataObj.interaction.refId
        }

        $http.post("/messages/send/email/single/web",obj)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.selectedUser = {}
                }
                else{
                }
            })
    }
}

function checkEditAccessForClosedOpps($scope,share,opp){

    function liuLoaded() {
        if(share.liuData){
            if(share.liuData.corporateAdmin){
                opp.isNotOwner = false;
            } else {
                opp.isNotOwner = true;
            }
        } else {
            setTimeOutCallback(100,function () {
                liuLoaded()
            })
        }
    }

    if(_.includes(["Close Lost","Close Won"], opp.stage)){
        if($scope.oppDetailsNav){
            _.each($scope.oppDetailsNav,function (item) {
                if(item.name === "Close Details"){
                    item.closeOpp = ""
                }
            })
        }

        liuLoaded();
    }
}

function drawHistory($scope,$http,share,logs,interactions,startDate,endDate) {
    $scope.isOppModalOpen = true;

    if(!$scope.chart){
        $scope.chart = {
            chartStartDate: moment(startDate).format(standardDateFormat()),
            chartEndDate: moment(endDate).format(standardDateFormat())
        }
    }

    function checkCanvasLoaded(){
        var ctx = document.getElementById("myChart");
        if (myChart) {
            myChart.destroy();
        }
        // resetCanvas()

        if(ctx){
            var labels = getDateLabels(startDate,endDate);
            var allData = oppInteractionsLog_v2(logs,interactions,labels);

            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: getInteractionData(logs,interactions,labels,allData)
                },
                options: chartsPluginsOptions()
            });
        } else {
            setTimeOutCallback(200,function () {
                checkCanvasLoaded();
            })
        }
    }

    checkCanvasLoaded();
}

var resetCanvas = function(){
    $('#myChart').remove(); // this is my <canvas> element
    $('.chartjs-wrapper').append('<canvas id="myChart"><canvas>');
    canvas = document.querySelector('#myChart');
    ctx = canvas.getContext('2d');
    ctx.canvas.width = 400; // resize to parent width
    ctx.canvas.height = 200; // resize to parent height
};

function getInteractionData(logs,interactions,labels,allData){

    return [{
        borderWidth: 0,
        label: 'Opp Activity',
        // data: oppActivityLog(logs,labels),
        data: allData.log,
        type: 'bubble',
        backgroundColor: '#FF8360',
        datalabels: true,
        radius: function (context) {
            if(context.dataset.data[context.dataIndex].y>0){
                return 3;
            } else {
                return 0;
            }
        },
        hoverRadius: 0
    }, {
        borderWidth: 0,
        label: 'Us',
        backgroundColor: '#70dbf3',
        // data: oppInteractionLog(false,labels)
        data: allData.us
    }, {
        borderWidth: 0,
        label: 'Them',
        backgroundColor: '#f5c431',
        // data: oppInteractionLog(true,labels)
        data: allData.them
    }];
}

function getDateLabels(startDate,endDate) {
    var minDate = startDate;
    var maxDate = endDate?new Date(endDate):new Date();
    // var minDate = moment().subtract(2,"week");
    var dates = [];
    while (new Date(minDate)< new Date(maxDate)){
        dates.push(moment(minDate).format(standardDateFormat()));
        minDate = moment(minDate).add(1,"day");
    }

    return dates;
}

function oppInteractionsLog_v2(logs,interactions,labels) {
    var activity_logs = [],
    int_data_them = [],
    int_data_us = [],
    logs_dts = [],
    int_dts = [];

    _.each(logs,function (log) {
        var name = "";

        log.message = buildLogMessage(log);
        log.what = buildLogWhatText(log);

        if(log.action == "removed" || log.action == "added"){
            log.oldValue = log.oldValue.toString();
            log.newValue = log.newValue.toString();

        }

        if(log.type == "geoLocation") {
            var oldVal = "",newVal = ""
            if(log.oldValue.town){
                oldVal = log.oldValue.town +", "+log.oldValue.zone
            }
            if(log.newValue.town){
                newVal = log.newValue.town +", "+log.newValue.zone
            }

            log.oldValue = oldVal;
            log.newValue = newVal;

            if(log.action == "Renewal"){
                log.newValue = "Created";
            }
        } else if(log.what == "Close date "){
            log.oldValue = moment(log.oldValue).format(standardDateFormat());
            log.newValue = moment(log.newValue).format(standardDateFormat());
        }

        if(log.oldValue && log.newValue){
            name = log.what +" from " +log.oldValue +" to "+log.newValue;
        } else {
            name = log.what;
        }

        activity_logs.push({
            x:moment(log.date).format(standardDateFormat()),
            y: 20,
            name:name,
            type:"opp"
        });

        logs_dts.push(moment(log.date).format(standardDateFormat()))
    });

    _.each(interactions,function (el) {

        var date = moment().year(el._id.year).month(el._id.month-1).date(el._id.dayOfMonth).format(standardDateFormat());
        // var date = new Date(moment().year(el._id.year).month(el._id.month).date(el._id.dayOfMonth));

        int_dts.push(date);
        int_data_them.push({
            x:date,
            y: el.initiatedByThem,
            name:"Them"
        });

        int_data_us.push({
            x:date,
            y: el.initiatedByUs,
            name:"Us"
        });
    });

    var ne_logs = _.difference(labels,logs_dts)
    var ne_ints = _.difference(labels,int_dts);

    if(ne_logs.length>0){
        _.each(ne_logs,function (log) {
            activity_logs.push({
                x:log,
                y: 0,
                name:"",
                type:"opp"
            });
        })
    }

    if(ne_ints.length>0){
        _.each(ne_ints,function (dt) {
            int_data_them.push({
                x:dt,
                y: 0,
                name:"Them"
            });
            int_data_us.push({
                x:dt,
                y: 0,
                name:"Us"
            });
        })
    }

    activity_logs.sort(function (o1, o2) {
        return new Date(moment(o1.x)) > new Date(moment(o2.x)) ? 1 : new Date(moment(o1.x)) < new Date(moment(o2.x)) ? -1 : 0;
    });

    int_data_them.sort(function (o1, o2) {
        return new Date(moment(o1.x)) > new Date(moment(o2.x)) ? 1 : new Date(moment(o1.x)) < new Date(moment(o2.x)) ? -1 : 0;
    });

    int_data_us.sort(function (o1, o2) {
        return new Date(moment(o1.x)) > new Date(moment(o2.x)) ? 1 : new Date(moment(o1.x)) < new Date(moment(o2.x)) ? -1 : 0;
    });

    return {
        log:_.uniqBy(activity_logs,"x"),
        them:_.uniqBy(int_data_them,"x"),
        us:_.uniqBy(int_data_us,"x")
    }
}

function oppInteractionLog(them,labels) {

    var dates = labels;
    var data = [];
    var vals_us = [10, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2];
    var vals_them = [5, 7, 1, 5, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0];

    _.each(dates, function (dt, index) {
        data.push({
            x:dt,
            y: them?vals_them[index]:vals_us[index],
            name:them?"Them":"Us"
        })
    });

    return data;
}

function oppActivityLog(logs,labels){

    var dates = labels;
    var data = [];

    _.each(dates,function (dt,index) {
        if(index == 0){
            data.push({
                x:dt,
                y: 20,
                name:"Opp created"
            })
        } else if(index == 3){
            data.push({
                x:dt,
                y: 20,
                name:"Stage changed from Proposal to Evaluation"
            })
        } else if(index == 8){
            data.push({
                x:dt,
                y: 20,
                name:"Stage changed from Evaluation to Commit"
            })
        } else if(index == 8){
            data.push({
                x:dt,
                y: 20,
                name:"Amount changed from 100K to 90K"
            })
        } else {
            data.push({
                x:dt,
                y: 0,
                name:""
            })
        }
    })

    return data;
}

function chartsPluginsOptions(){
    return {
        responsive: true,
        maintainAspectRatio: false,
        tooltips: {
            caretSize:0,
            mode: 'label',
            backgroundColor:"#3f5466",
            cornerRadius: 2,
            callbacks: {
                label: function (tooltipItem, data) {
                    var type = data.datasets[tooltipItem.datasetIndex].label;
                    var value_obj = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    var value = value_obj.y;
                    var total = 0;

                    for (var i = 0; i < data.datasets.length; i++)
                        total += data.datasets[i].data[tooltipItem.index];
                    if (tooltipItem.datasetIndex !== data.datasets.length - 1) {
                        if(type !== "Opp Activity"){
                            return type + " : " + value.toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, '1,');
                        } else {
                            return value_obj.name
                        }
                    } else {
                        if(type !== "Opp Activity"){
                            return [type + " : " + value.toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, '1,')];
                        } else {
                            return value_obj.name
                        }
                    }
                }
            }
        },
        plugins: {
            datalabels: {
                formatter: function (value, ctx) {
                    let sum = 0;
                    let dataArr = ctx.chart.data.datasets[0].data;
                    dataArr.map(data => {
                        sum += data;
                    });
                    let percentage = (value * 100 / sum).toFixed(0) + "%";
                    return "";
                },
                font: {
                    weight: "bold"
                },
                color: "#fff",
                // display: false
            }
        },
        scales: {
            xAxes: [{
                stacked: true,
                maxBarThickness: 100,
                gridLines: {
                    display: false
                },
                ticks: {
                    fontSize: 10
                },
                // type: 'day',
            }],
            yAxes: [{
                stacked: true,
                ticks: {
                    beginAtZero: true,
                    callback: function (value) {
                        return value;
                    },
                    fontSize: 10
                }
            }]
        },
        legend: {
            position:'bottom',
            labels: {
                boxWidth:10,
                fontSize:11
            }
        }
    };
}
