/* Relatas : sign In */

$(document).ready(function () {

    checkIfAuthError();
    $('#login-with-linkedin').click(function(){
        mixpanelTrack("Signup LinkedIn");

        if(are_cookies_enabled()){
            window.location.replace('/linkedinAuthentication');
        }
        else{
            showMessagePopUp("Please enable your browser cookies for seamless system operation.",'tip');
        }
    });

    function jsonToQueryString(json) {
        return '?' +
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }

    $('#sign-up-with-linkedin').click(function(){
        mixpanelTrack("Signup LinkedIn");

        if(are_cookies_enabled()){
            window.location.replace('/linkedinAuthentication');
        }
        else{
            showMessagePopUp("Please enable your browser cookies for seamless system operation.",'tip');
        }
    });

     $('#login-with-google').on("click",function(){
         mixpanelTrack("Signup Google");
         if(are_cookies_enabled()){
             var reqObj = {
                 onSuccess:'/onboarding/new',
                 onFailure:'/',
                 action:'customSignup'
             };
             window.location.replace('/authenticate/google/signup/web'+jsonToQueryString(reqObj));
         }
         else{
             showMessagePopUp("Please enable your browser cookies for seamless system operation.",'tip');
         }
     });

     $('#loginGoogle').on("click",function(){
         mixpanelTrack("Signup Google");
         if(are_cookies_enabled()){
             var reqObj = {
                 onSuccess:'/onboarding/new',
                 onFailure:'/',
                 action:'customSignup'
             };
             window.location.replace('/authenticate/google/signup/web'+jsonToQueryString(reqObj));
         }
         else{
             showMessagePopUp("Please enable your browser cookies for seamless system operation.",'tip');
         }
     });

    $('#sign-up-with-google').on("click",function(){
        mixpanelTrack("Signup Google");
        if(are_cookies_enabled()){
            var reqObj = {
                onSuccess:'/onboarding/new',
                onFailure:'/',
                action:'customSignup'
            };
            window.location.replace('/authenticate/google/signup/web'+jsonToQueryString(reqObj));
        }
        else{
            showMessagePopUp("Please enable your browser cookies for seamless system operation.",'tip');
        }
    });

    function are_cookies_enabled()
    {
        var cookieEnabled = (navigator.cookieEnabled) ? true : false;

        if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled)
        {
            document.cookie="testcookie";
            cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
        }
        return (cookieEnabled);
    }

    function checkIfAuthError(){
        $.ajax({
            url:'/relatas/error/checkAuthError',
            type:'GET',
            success:function(response){
                if(response == 'linkedin'){
                    showMessagePopUp("Bummer. Looks like a LinkedIn server issue. Can you please try again. If the problem persists, please email us at hello@relatas.com.",'error');
                    clearLinkedInError();
                }else if(response == 'google'){
                    showMessagePopUp("Bummer. Looks like a Google server issue. Can you please try again. If the problem persists, please email us at hello@relatas.com.",'error');
                    clearLinkedInError();
                }else if(response == 'corporateUser'){
                    showMessagePopUp("Bummer. Looks like you are trying to login in wrong way. Can you please try again with your 'company.relatas.com'.",'error');
                }
            }
        })
    }

    function clearLinkedInError(){
        $.ajax({
            url:'/relatas/error/clearAuthError',
            type:'GET',
            success:function(response){

            }
        })
    }

    // Function to validate required feild
    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }


    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {
            return true;
        } else {

            return false;
        }
    }
    var popover = null;
    function showMessagePopUp(message,msgClass)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        popover = $("#user-name");
        $(".arrow").addClass("invisible");
      //  $(".popover").addClass('popupError')
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }
        $("#message").text(message);
    }

    $("body").on("click","#closePopup-message",function(){
       $(".popover").hide();
    });

    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }

    $("#unique-name-button").on("click",function(){
        var uniqueName = $("#unique-name").val();
        if(validateRequired(uniqueName)){
            checkUniqueNameExist(uniqueName)
        }

    });

    function checkUniqueNameExist(uniqueName){

                var details = {
                    publicProfileUrl:uniqueName.replace(/[^a-zA-Z0-9]/g,'')
                }
                $.ajax({
                    url:'/checkUrl',
                    type:'POST',
                    datatype:'JSON',
                    data:details,
                    traditional:true,
                    success:function(result){
                        if(result == true){
                            $("#unique-name-content").hide();
                            $("#try-free").show();
                            $("#unique-name-button").hide();
                            $("#unique-name-message").html("Congratulations! <span style='color:#0F8A40;'>"+uniqueName.replace(/[^a-zA-Z0-9]/g,'')+"</span> is available.")
                        }
                        else{
                            $("#unique-name-content").hide();
                            $("#try-free").hide();
                            $("#try-again").show();
                            $("#unique-name-button").hide();
                            $("#unique-name-message").text("Sorry !");
                            $("#unique-name-not-available").show();
                            $("#unique-name-not-available-image").show();
                            $("#unique-name-not-available").html("<span style='color:#FF7A72;'>"+uniqueName.replace(/[^a-zA-Z0-9]/g,'')+"</span> is not available")
                        }
                    },
                    error: function (event, request, settings) {

                    },
                    timeout: 30000
                })
    }
    $("#try-free").on("click",function(){
        $("#unique-name-content").show();
        $("#try-free").hide();
        $("#unique-name-button").show();
        $("#unique-name").val('');
        $("#unique-name-not-available-image").hide();
        $("#unique-name-message").text("Check if your name is available")
    });

    $("#try-again").on("click",function(){
        $("#unique-name-content").show();
        $("#try-free").hide();
        $("#unique-name-not-available-image").hide();
        $("#unique-name").val('');
        $("#try-again").hide();
        $("#unique-name-button").show();
        $("#unique-name-not-available").hide();
        $("#unique-name-message").text("Check if your name is available")
    });

    $("#enterprise-demo-button").on("click",function(){
        var firstName = $("#firstName").val();
        var lastName = $("#lastName").val();
        var emailId = $("#emailId").val();
        var companyName = $("#companyName").val();
        var phoneNumber = $("#phoneNumber").val();
        var skypeId = $("#skypeId").val();
        if(validateRequired(firstName) && validateRequired(lastName) && validateEmailField(emailId) && validateRequired(companyName) && validateRequired(phoneNumber) && validateRequired(skypeId)){
            var demoUser = {
                firstName:firstName,
                lastName:lastName,
                emailId:emailId,
                companyName:companyName,
                phoneNumber:phoneNumber,
                skypeId:skypeId
            };
            $.ajax({
                url:'/admin/request/enterpriseDemo',
                type:'POST',
                datatype:'JSON',
                data:demoUser,
                traditional:true,
                success:function(response){
                   if(response == false){

                   }
                   else if(response == true){
                        $(".after-sign-up").hide();
                        $("#enterprise-demo-button").hide();
                        $("#sign-up-success-msg").show();
                        $("#sign-up-success-msg").text("Thank you for your interest in Relatas. We will reach out to you with in 24 hours.");
                   }
                   else if(response == 'exist'){

                   }
                },
                error: function (event, request, settings) {

                },
                timeout: 30000
            })
        }
    });

    $("#mobile-launch-button").on("click",function(){
        var emailId = $("#emailId-mobile-launch").val();
        if(validateEmailField(emailId)){
            var userObj = {
                  emailId:emailId,
                  iosApp:$("#checkboxG4").prop("checked"),
                  androidApp:$("#checkboxG5").prop("checked"),
                  windowsApp:$("#checkboxG6").prop("checked")
            }

            $.ajax({
                url:'/admin/request/mobileAppLaunch',
                type:'POST',
                datatype:'JSON',
                data:userObj,
                traditional:true,
                success:function(response){
                    if(response == false){

                    }
                    else if(response == true){
                        $(".mobile-launch-content").hide();
                        $("#subscribe-success-msg").show();
                        $("#subscribe-success-msg").text("Thank you for your interest in Relatas. We will get back to you as we launch the app.");
                    }
                    else if(response == 'exist'){
                        $(".mobile-launch-content").hide();
                        $("#subscribe-success-msg").show();
                        $("#subscribe-success-msg").text("You have already subscribed.We will get back to you as we launch the app.");
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 30000
            })
        }
    });

   /* $.ajax({
        url:'/authenticate/google',
        type:"POST",
        datatype:"JSON",
        data:{firstName:'asd',emailId:'gg'},
        success:function(res){
            console.log(res)
        }
    })*/

});