
$(document).ready(function() {
    var table;

    getRsvpUserList();
    getUserProfile();


   function getRsvpUserList(){

       $.ajax({
           url:window.location+'/getList',
           type:'GET',
           datatype:'JSON',
           traditional:true,
           success:function(result){
               applyDataTable();
              if(result){
                  if(result.length > 0){
                      displayUser(result)
                  }
              }else{

              }
           }
       })
   }

    // Function to get user contacts info
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                    if(result && result.firstName){
                        if( result.profilePicUrl.charAt(0) == '/' || result.profilePicUrl.charAt(0) == 'h'){

                            $('#profilePic').attr("src",result.profilePicUrl);
                        }else $('#profilePic').attr("src","/"+result.profilePicUrl);
                    }

                    imagesLoaded("#profilePic",function(instance,img) {
                        if(instance.hasAnyBroken){
                            $('#profilePic').attr("src","/images/default.png");
                        }
                    });

            },
            error: function (event, request, settings) {


            },
            timeout: 20000
        })
    }


    function displayUser(list){
        for(var i=0; i<list.length; i++){
            var fName = list[i].firstName || '';
            var lName = list[i].lastName || '';
            var userUrl = '/'+getValidUniqueUrl(list[i].publicProfileUrl)
            if(table){
                table.row.add( [
                        '<a style="color: currentcolor;" href='+userUrl+'>'+fName+' '+lName+'</a>',
                        list[i].emailId
                ] ).draw();
            }
        }
    }

    function getValidUniqueUrl(uniqueName){
        var url = window.location+''.split('/');
        var patt = new RegExp(url[2]);
        if(patt.test(uniqueName)){

            url = uniqueName.split('/');
            for (var i = url.length - 1; i >= 0; i--) {
                if (url[i] == 'u') {
                    uniqueName = url[i+1];
                }
            }
            return uniqueName;

        }else {
            return uniqueName;
        }
    }

    function applyDataTable(){
        table = $('#RSVPListTable').DataTable({

            "dom": '<"top"iflp<"clear">>',
            "fnDrawCallback": function(oSettings) {
                if(table){
                    if (table.rows() < 11) {
                        $('.RSVPListTable_paginate').hide();
                    }
                    else  $('.RSVPListTable_paginate').show();
                }else
                if ($('#contactsTable tr').length < 11) {
                    $('.RSVPListTable_paginate').hide();
                }
                else  $('.RSVPListTable_paginate').show();
            },
            "order": [[ 0, "asc" ]]
        });

    }

});