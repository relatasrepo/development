
var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','ngLodash']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.controller("reports", function ($scope, $http,$rootScope){

    $rootScope.isCorporateAdmin = true;
    $scope.getReport = function (companyId) {
        
        if(companyId){
            var url = '/admin/getUserReport?companyId='+companyId;
            $scope.csvUrl = "/admin/getUserReport/csv?companyId="+companyId

            $http.get(url)
                .success(function(response) {
                    
                    if(response && response.length>0){
                        $scope.reports = buildTeamProfiles(response);
                    }
            });
        }
    }

    function checkIfCompanyObjExists() {
        if($('#company-object-hidden').text()){

            $scope.company = JSON.parse($('#company-object-hidden').text());
            $scope.getReport($scope.company._id);
        } else {
            checkIfCompanyObjExists();
        }
    };

    checkIfCompanyObjExists();

});

function buildTeamProfiles(data) {
    console.log("Inside buildteam profiles:");
    console.log("Report data:", data);
    var team = [];

    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            location:el.location,
            sortDate:el.registeredDate,
            totalOppsValueSort:el.totalOppsValue,
            registeredDate:el.registeredDate?moment(el.registeredDate).format("DD MMM YYYY"):"-",
            totalOpps:el.totalOpps,
            totalOppsValue:numberWithCommas(el.totalOppsValue.r_formatNumber(2)),
            lastMobileLoginDate: el.lastMobileLoginDate ? moment(el.lastMobileLoginDate).format("DD MMM YYYY") : "-",
            mobileAppVersion: el.mobileAppVersion
        })
    });

    return team;
}

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){

                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else if(obj.name){
                    obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    obj.noPPic = obj.name.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    scope.noPPic = obj.name.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                } else if(obj.fullName){

                    obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                //call the function that was passed
                scope.$apply(attrs.imageonload);
            });
        }
    };
});
