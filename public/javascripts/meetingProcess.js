$(document).ready(function () {

    $('#menu').tabify();
    $.ajaxSetup({cache: false});
    isSelfUserAjax();
    loadUserPublicProfile();
    getAWSCredentials();

    var showDetailsFlag = false, update = false, signInFlagNew = false, invite = false, partialProfile = false, inFormOrLink = false, isPageLoaded = false, toSendInvite = false;

    var uniqueId = '',popoverContent = null;
    var publicProfile, documents, updateEventId, date, authUserProfile, date1, date2, date3, lTitle, lLocation, lDescription, lLocationType, selfEmails, count, commonConnections,meetingPopup, calendar, timezone, timezone2, loggedInUserTimezone, start1Arr, end1Arr, start2Arr, end2Arr, s3bucket, AWScredentials, docName, docUrl;

    var monthNameFull = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    var day = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    var timeSlots = [];

    $("#selectedTimePanel-ul").on("click", function () {
        showMessagePopUp("Please click on the calendar to schedule time", 'tip');
    });

    Pace.on('start', function () {
        isPageLoaded = false;
    });

    Pace.on('done', function () {
        isPageLoaded = true;
    });
    var isSefCalendar = false;

    function isSelfUserAjax() {
        $.ajax({
            url: '/schedulePage/isSefCalendar',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if (result) {
                    isSefCalendar = true;
                } else {
                    isSefCalendar = false;
                }
            },
            timeout: 20000
        })
    }

    var signInFlag = true;
    $("#logOut").on("click", function () {

        $.ajax({
            url: '/isLoggedInUser',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if (result) {
                    window.location.replace('/logout');
                } else {
                    signInFlag = false;
                    $('#lightBoxPopup').css({display: 'block'});
                    $('#calendar').css({display: 'none'});
                    showHideTimeDisplay(false);
                }
            },
            timeout: 20000
        })
    })

    function getTimezone() {
        var tz = jstz.determine();
        timezone = tz.name();
        return timezone;
    }

    function getAWSCredentials() {
        $.ajax({
            url: '/getAwsCredentials',
            type: 'GET',
            datatye: 'JSON',
            success: function (credentials) {
                AWScredentials = credentials;
                AWS.config.update(credentials);
                AWS.config.region = credentials.region;
                s3bucket = new AWS.S3({params: {Bucket: credentials.bucket}});
                docUrl = 'https://' + AWScredentials.bucket + '.s3.amazonaws.com/';
            },
            timeout: 20000
        })
    }

    var fileChooser = document.getElementById('file-chooser');
    var button = document.getElementById('upload-button');
    var uploadFlag = true;
    button.addEventListener('click', function () {
        if (uploadFlag) {
            $('#file-chooser').show().trigger('click').addClass('invisible')
        }
        else {
            showMessagePopUp("Please wait document is uploading", 'error')
        }
    }, false);

    $("#file-chooser").on("change", function () {
        var file = fileChooser.files[0];
        $("#file-chooser").hide();
        if (file) {

            $("#selectedTimePanel").css({height: 'auto'})
            $('#docTitle').hide();
            $('#docTitleImg').hide();
            var dateNow = new Date();
            var d = dateNow.getDate();
            var m = dateNow.getMonth();
            var y = dateNow.getFullYear();
            var hours = dateNow.getHours();
            var min = dateNow.getMinutes();
            var sec = dateNow.getSeconds();
            var timeStamp = y + ':' + m + ':' + d + ':' + hours + ':' + min + ':' + sec;
            var dIdentity = timeStamp + '_' + file.name;
            var docNameTimestamp = dIdentity.replace(/\s/g, '');

            docName = file.name;
            var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
            uploadFlag = false;
            $("#docProgress").css({display: 'block'});
            var request = s3bucket.putObject(params);
            request.on('httpUploadProgress', function (progress) {

                $("#docProgress").val(progress.loaded / progress.total * 100)
            });
            request.send(function (err, data) {
                if (err) {
                    showMessagePopUp("Doc upload failed", 'error')
                }
                else {
                    $('#file-chooser').hide()
                    $('#upload-button').hide()
                    $('#docTitle').text(docName);
                    $('#docTitle').css({display: 'block'});
                    $('#docTitleImg').css({display: 'block'});
                    $("#docProgress").hide()
                    docUrl = docUrl + docNameTimestamp;
                    var docD = {
                        docName: docName,
                        docUrl: docUrl,
                        awsKey: docNameTimestamp
                    }
                    documents = {
                        documentName: docName,
                        documentUrl: docUrl,
                        awsKey: docNameTimestamp
                    }
                    $.ajax({
                        url: '/storeDocDetailsInSession',
                        type: 'POST',
                        datatype: 'JSON',
                        data: docD,
                        traditional: true,
                        success: function (result) {
                            mixpanelTrack("Share Document");
                            mixpanelIncrement("# Doc S");
                            uploadFlag = true;
                        },
                        timeout: 20000
                    })
                }
            });

        } else {
            showMessagePopUp("Please select file to upload.", 'error')
        }
    });

    function loadUserPublicProfile(signUp) {
        $.ajax({
            url: '/userPublicProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (profile) {
                if (!profile) {
                    inFormOrLink = false;
                    window.location.replace('/');
                } else {
                    publicProfile = profile;
                    getLoggedInUserProfile(signUp);
                    if (publicProfile.timezone) {
                        if (publicProfile.timezone.name) {
                            timezone2 = publicProfile.timezone.name
                        }
                    }

                    checkLinkedinSendInviteStatus();
                    getCommonConnections(publicProfile._id);
                    getTwitterInfo(publicProfile._id);
                    getFacebookInfo(publicProfile._id);
                    getLinkedinPofile(publicProfile._id);
                    $('#title').text(profile.firstName);
                    $('#user').text(profile.firstName + "'s Calendar");

                    $("#time-zone").text(' Time Zone (' + getTimezone() + ' )');
                    $("#user-name").text("with " + profile.firstName + "? ");
                    $('#designation').text(profile.designation);
                    $('#companyName').text(profile.companyName);

                    $('#profilePic').attr('src', profile.profilePicUrl);

                    if( profile.profilePicUrl.charAt(0) == '/' || profile.profilePicUrl.charAt(0) == 'h'){

                        $('#profilePic').attr("src",profile.profilePicUrl);
                    } else  $('#profilePic').attr("src","/"+profile.profilePicUrl);

                    $('#name').text("" + "  " + profile.firstName + " " + profile.lastName);
                    $('#firstName').text(profile.firstName);
                    $('#lastName').text(profile.lastName);
                    imagesLoaded("#profilePic", function (instance, img) {
                        if (instance.hasAnyBroken) {
                            $('#profilePic').attr("src", "/images/default.png");
                        }
                    });

                    var startTime1 = publicProfile.calendarAccess.timeOne.start;
                    var endTime1 = publicProfile.calendarAccess.timeOne.end;
                    var startTime2 = publicProfile.calendarAccess.timeTwo.start;
                    var endTime2 = publicProfile.calendarAccess.timeTwo.end;


                    if (checkRequredField(startTime1) && checkRequredField(endTime1)) {
                        start1Arr = startTime1.split(':');
                        end1Arr = endTime1.split(':');
                    }
                    if (checkRequredField(startTime2) && checkRequredField(endTime2)) {
                        start2Arr = startTime2.split(':');
                        end2Arr = endTime2.split(':');
                    }

                    if (!checkRequredField(profile.profilePrivatePassword)) {
                        if (!calendar) {
                            displayTime_zone_map_my_calendar()
                            displayCalendar();
                        }
                    }
                    else {
                        authoriseCalendarPassword(profile);
                    }
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        });
    };

    var calendarFlag = true;

    function authoriseCalendarPassword(user) {
        var htmlText = '<div id="privateCalMessage" style="margin-top: 10px; margin-left: 75px">';
        htmlText += '<span>' + user.firstName + ' has made his calendar as Private.</span>';
        htmlText += '<span>To schedule a meeting with ' + user.firstName + ', please enter his calendar access password below.</span></br>';
        htmlText += '<input class="form-control" type="password" id="calendarPassword" style="width: 100px; display: inline">';
        htmlText += '<input type="button" userId=' + user._id + ' id="calendarPasswordSubmit" value="Access Calendar" class="btn btn-sm blue-btn"><br>';
        htmlText += 'Else please request for password by logging in below</div>'

        $.ajax({
            url: '/isCalendarAuthorised',
            type: 'GET',
            datatype: 'JSON',
            success: function (isAuthorised) {
                if (isAuthorised) {
                    $("#selectedTimePanel").removeClass("invisible");
                    $("#calendar").css({display: 'block'});
                    showHideTimeDisplay(true);
                    displayLoginWindowMessage("Please Login to send the Meeting Request");
                    displayTime_zone_map_my_calendar()
                    if (!calendar) {
                        displayCalendar();
                        displayTime_zone_map_my_calendar()
                    }
                }
                else {
                    $.ajax({
                        url: '/isLoggedInUser',
                        type: 'GET',
                        datatype: 'JSON',
                        traditional: true,
                        success: function (result) {
                            if (result) {
                                $("#calendar").css({display: 'none'});
                                showHideTimeDisplay(false);
                                $("#calendarMessage").html(htmlText);
                                $("#selectedTimePanel").addClass("invisible");
                                $('#lightBoxPopup').css({display: 'none'});
                                $('#requestMail').css({display: 'block'});
                            }
                            else {
                                $("#calendar").css({display: 'none'});
                                showHideTimeDisplay(false);
                                $("#calendarMessage").html(htmlText);
                                $("#selectedTimePanel").addClass("invisible");
                                $('#lightBoxPopup').css({display: 'block'});
                                displayLoginWindowMessage("Please login to send the request for meeting password.");
                                $('#requestMail').css({display: 'none'});
                            }
                        },
                        error: function (event, request, settings) {

                        },
                        timeout: 20000
                    })

                }
            }
        })
    }

    $("body").on("click", "#calendarPasswordSubmit", function () {
        var userId = $(this).attr("userId");
        var calPassword = $("#calendarPassword").val();
        var calendarCredentials = {
            userId: userId,
            calPassword: calPassword
        }
        if (checkRequredField(calPassword)) {
            $.ajax({
                url: '/authoriseCalendar',
                type: 'POST',
                datatype: 'JSON',
                data: calendarCredentials,
                success: function (isValid) {
                    if (isValid) {
                        $('#lightBoxPopup').css({display: 'none'});
                        $("#calendarMessage").css({display: 'none'});
                        $("#calendar").css({display: 'block'});
                        showHideTimeDisplay(true);
                        displayTime_zone_map_my_calendar()
                        if (!calendar) {
                            displayCalendar();
                        }
                        $("#selectedTimePanel").removeClass("invisible");
                    }
                    else {
                        showMessagePopUp("Invalid password. Please try again", 'error')
                    }
                },
                timeout: 20000
            });
        }
        else {
            showMessagePopUp("Please enter password", 'error')
        }
    });

    function showHideTimeDisplay(display) {

        if (display) {
            $("#show-hide-time").css({display: 'block'});
        } else $("#show-hide-time").css({display: 'none'});
    }

    function displayTime_zone_map_my_calendar() {
        mapMyCalendarFlag = true;
    }

    $("#requestMailBut").on("click", function () {
        if (checkRequredField(authUserProfile) && checkRequredField(publicProfile)) {
            var reqObj = {
                fromUserId: getAuthUserId() == 'no user' ? '' : getAuthUserId(),
                fromName: authUserProfile.firstName + ' ' + authUserProfile.lastName,
                fromEmailId: authUserProfile.emailId,
                toUserId: publicProfile._id,
                toName: publicProfile.firstName + ' ' + publicProfile.lastName,
                toEmailId: publicProfile.emailId
            };

            $.ajax({
                url: '/calendarPasswordRequest',
                type: 'POST',
                datatype: 'JSON',
                data: reqObj,
                success: function (isOk) {
                    if (isOk) {
                        showMessagePopUp("Sending request success. We will let you know when " + publicProfile.firstName + " will accept the request.", 'success')
                        $("#requestMailBut").prop("disabled", true);
                    }
                    else {
                        showMessagePopUp("Sending request fail. Please try again", 'error')
                    }
                },
                timeout: 20000
            });

        }

    });

    function removeDuplicates_id(arr) {
        var end = arr.length;
        for (var i = 0; i < end; i++) {
            for (var j = i + 1; j < end; j++) {
                if (arr[i]._id == arr[j]._id) {
                    var shiftLeft = j;
                    for (var k = j + 1; k < end; k++, shiftLeft++) {
                        arr[shiftLeft] = arr[k];
                    }
                    end--;
                    j--;
                }
            }
        }

        var whitelist = [];
        for (var l = 0; l < end; l++) {
            whitelist[l] = arr[l];
        }

        return whitelist;
    }

    var isSelf = false;
    function loadUserPendingInvitations(mapMyCalendar, userId) {
        var eventSource = [];
        $.ajax({
            url: !mapMyCalendar ? '/newInvitations/' + publicProfile._id : '/newInvitations/authenticated',
            type: 'GET',
            datatype: 'JSON',
            success: function (invitations) {
                invitations = removeDuplicates_id(invitations);
                if (isSelfUser()) {
                    isSelf = true;
                }
                invitations.forEach(function (invitation) {
                    if (invitation.deleted || invitation.status == 'reScheduled') {

                    } else {
                        if (!mapMyCalendar) {
                            createEventForPendingMeetings(invitation, isSelf);
                        } else {
                            calendar.fullCalendar('removeEvents', 'busyTime' + invitation.invitationId);
                            createEventForPendingMeetings(invitation, true);
                        }
                    }
                });
                calendar.fullCalendar('addEventSource', eventSource);
            },
            timeout: 20000
        });


        function createEventForPendingMeetings(invitation, isSelfA) {

            var canceled = false;
            if(invitation.senderId == getAuthUserId()){
                if(invitation.selfCalendar){
                    if(invitation.toList.length == 1 && invitation.toList[0].canceled){
                        canceled = true;
                    }
                    else{
                        var totalParticipants = invitation.toList.length;
                        var numberOfParticipantsCanceled = 0;
                        for (var toList = 0; toList < totalParticipants; toList++) {
                            if (invitation.toList[toList].canceled) {
                                numberOfParticipantsCanceled++;
                            }
                        }
                        if(numberOfParticipantsCanceled == totalParticipants){
                            canceled = true;
                        }
                    }
                }
                else{
                    if(invitation.to.canceled == true){
                        canceled = true;
                    }
                }
            }
            else{
                if (invitation.selfCalendar) {
                    for (var k = 0; k < invitation.toList.length; k++) {
                        if (invitation.toList[k].receiverId == getAuthUserId()) {
                            if(invitation.toList[k].canceled){
                                canceled = true;
                            }
                        }
                    }
                }
                else {
                    if(invitation.to.canceled){
                        canceled = true;
                    }
                }
            }

            for(var slot=0; slot<invitation.scheduleTimeSlots.length; slot++){
                var obj = validateGoogleMeetings(new Date(invitation.scheduleTimeSlots[slot].start.date), new Date(invitation.scheduleTimeSlots[slot].end.date));

                if (obj.create && !canceled) {

                    var event = {
                        title: 'BOOKED',
                        meetingId:invitation.invitationId,
                        id: mapMyCalendar ? 'mapMyCalendar' : 'busyTime' + invitation.invitationId,
                        start: obj.start,
                        end: obj.end,
                        editable: false,
                        color: '#808080',
                        allDay: false,

                        selfCalendar: invitation.selfCalendar == true,
                        spanTitle:invitation.senderId == getAuthUserId() ? 'Delete Meeting?' : 'Cancel Meeting?',
                        meeting: !canceled,
                        marginTop:'-35%',
                        sender: invitation.senderId == getAuthUserId()
                    };

                    if (isSelfA) {
                        event.color = '#d09054';
                        event.id = mapMyCalendar ? 'not confirmmapMyCalendar' : 'not confirm';
                        event.mUrl = '/meeting/' + invitation.invitationId;
                        event.title = 'Not Conf. ( ' + invitation.scheduleTimeSlots[slot].title + ' )';
                        event.deleteBut=true;
                    }

                    eventSource.push(event)
                }
            }
        }
    }
    function getAuthUserId(){
       return checkRequredField(authUserProfile) ? authUserProfile._id : 'no user';
    }
    function loadUserPublicProfileCalendarEvents() {
        $.ajax({
            url: '/userPublicProfileCalendarEvents',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (events) {
                if (isSelfUser()) {
                    isSelf = true;
                }
                if (events == false) {

                }
                else {
                    var eventSource = [];
                    for (var i in events) {

                        var localStart = new Date(events[i].start.dateTime || events[i].start.date).toISOString();
                        var localEnd = new Date(events[i].end.dateTime || events[i].end.date).toISOString();

                        var obj = validateGoogleMeetings(new Date(localStart), new Date(localEnd));
                        if (obj.create) {
                            var event = {
                                title: 'BUSY',
                                id: 'busyTime',
                                start: obj.start,
                                end: obj.end,
                                editable: false,
                                color: '#808080',
                                allDay: events[i].start.dateTime ? false : true
                            }

                            if (isSelf) {
                                event.color = '#82CAFA';
                                event.textColor = '#000000';
                                event.title = events[i].summary;

                                if(events[i].id.substr(0, 12) == 'relatasevent'){

                                    event.deleteBut = true;
                                    event.spanTitle = 'Delete Event?';
                                    event.type = 'event';
                                    event.marginTop = event.title.length > 7 ? '-40%' : '-20%',
                                    event.meetingId = events[i].id.substr(12, events[i].id.length)
                                }
                            }
                            eventSource.push(event)
                        }
                    }
                    calendar.fullCalendar('addEventSource', eventSource);
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        });
    }

    function validateGoogleMeetings(startdate, endDate) {
        var now = new Date();
        var obj = {};
        if (now <= startdate) {
            obj.start = startdate;
            obj.end = endDate;
            obj.create = true;
        } else {
            if (now <= endDate) {
                obj.start = new Date();
                obj.end = endDate;
                obj.create = true;
            } else obj.create = false;
        }
        return obj;
    }

    var contactsFlag = true;

    function getLoggedInUserProfile(signUp) {

        contactsFlag = false;
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            success: function (profile) {
                if (profile) {
                    authUserProfile = profile;

                    identifyMixPanelUser(profile, signUp);
                    if (profile.timezone) {
                        if (profile.timezone.name) {
                            loggedInUserTimezone = profile.timezone.name;
                        }
                    }

                    isContactExist();
                    if (checkRequredField(publicProfile)) {
                        if (profile.registeredUser == false) {
                            $("#goToDashboard").attr('href', '#');
                            $("#goToCalendar").attr('href', '#');
                            $("#goToContacts").attr('href', '#');
                            $("#goToDocuments").attr('href', '#');
                            $("#goToDocuments").attr('href', '#');
                            $("#goHome").attr('href', '/editProfile');
                        }
                        else {
                            $("#goHome").attr('href', '/');
                        }
                    }
                    else {
                        show_hide_socialTabs(true, false);

                    }
                }
            },
            timeout: 20000
        })
    }

    function identifyMixPanelUser(profile, signUp) {

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login": new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page": window.location.href
        });

        if (signUp) {
            mixpanelIncrement("#Login");
        }

        if (signInFlagNew) {
            signInFlagNew = false;
            mixpanelTrack("Signup Relatas");
        }
    }

    function isContactExist() {

        $.ajax({
            url: '/getContactByUserId/' + publicProfile._id + '/' + getAuthUserId(),
            type: 'GET',
            datatype: 'JSON',
            success: function (contact) {
                if (contact) {

                    $('#emailId').text(getTextLength(publicProfile.emailId, 16));
                    $('#emailId').attr("title", publicProfile.emailId)
                    $('#skypeId').text(getTextLength(publicProfile.skypeId, 16));
                    $('#skypeId').attr("title", publicProfile.skypeId)
                    if (checkRequredField(publicProfile.skypeId)) {
                        $('#skypeId').attr("href", "skype:" + publicProfile.skypeId + "?call")
                        $('#skypeId').attr("target", "_blank")
                    }

                    $('#phoneNumber').text(publicProfile.mobileNumber);
                    $('#location').text(getTextLength(publicProfile.location, 16));
                    $('#location').attr("title", publicProfile.location)
                    show_hide_socialTabs(false, true);


                }
                else {
                    show_hide_socialTabs(true, false);

                }
            }
        })
    }

    function show_hide_socialTabs(relatasContactMsg, sideBarMenu) {

        if (relatasContactMsg)
            $("#relatasContactMessage").show()
        else $("#relatasContactMessage").hide()

        if (sideBarMenu)
            $("#sideBarMenu").show()
        else  $("#sideBarMenu").hide()
    }

    function getTextLength(text, maxLength) {
        var textLength = text.length;
        if (textLength > maxLength) {
            var formattedText = text.slice(0, maxLength)
            return formattedText + '..';
        }
        else return text;
    }

    // Function to generate string date format for html input type date
    function getDateFormat(date) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;

        var dateFormat = year + "-" + month + "-" + day;
        return dateFormat;
    };

    function getDayFormat(date) {
        var weekDay = day[date.getDay()];
        var toDay = date.getDate();
        var month = monthNameFull[date.getMonth()];
        return weekDay + " " + toDay + " " + month;
    }

    function getTimeFormatWithMeridian(date) {
        var hrs = date.getHours();
        var min = date.getMinutes();
        var meridian;
        if (hrs > 12) {
            hrs -= 12;
            meridian = 'PM';
        }
        else {
            if (hrs == 12) {
                meridian = 'PM'
            } else
                meridian = 'AM';
        }
        if (min < 10) {
            min = "0" + min;
        }
        ;
        var time = hrs + ":" + min + " " + meridian;
        return time;
    }

    // Function to generate string time format for html input type time
    function getTimeFormat(date) {
        var hrs = date.getHours();
        var min = date.getMinutes();
        if (hrs < 10) {
            hrs = "0" + hrs
        }
        ;
        if (min < 10) {
            min = "0" + min;
        }
        ;
        var time = hrs + ":" + min

        return time;
    }

    function onClickSend() {
        removePopover();
        $(".popover").remove();
        $.ajax({
            url: '/getTimeSlots',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                var size = result.length;
                if (result.length < 2 && !invite) {

                    invite = true;
                    showMessagePopUp("Our analysis shows that Relatas users prefer 3 selected time options. This has higher probability of getting a meeting accepted faster. Select additional time slots else click on Send invite again.", 'tip')

                }
                else {
                    invite = true
                    if (invite) {

                        lTitle = $('#MeetingTitleDetails').val();
                        lLocation = $('#locationDetails').val();
                        lLocationType = $('#locationType').val();
                        lDescription = $('#meetingDescriptionDetails').val();


                        if (isSelfUser()) {
                            selfEmails = $('#selfEmails').val();
                        }

                        if (validateMeetingDetails(isSelfUser())) {
                            if (!update) {
                                timeSlotsArray(date, size, lTitle, lLocation, lLocationType, lDescription, selfEmails);
                            }
                        }
                        else {
                            showMessagePopUp("Please enter all fields in meeting details.", 'error')
                        }
                    }
                }
            },
            error: function (event, request, settings) {

            }
        });
    }

    $("body").on("focusout", "#selfEmails", function () {
        if (isSelfUser()) {
            if (!contains($("#selfEmails").val(), '@'))
                showMessagePopUp("Please provide valid email id.", 'error');
        }
    });

    var participantFlag = false;

    function validateMeetingDetails(selfUser) {
        if (!selfUser) {
            if (lLocationType != 'Select Location Type*' && checkRequredField(lTitle) && checkRequredField(lLocation) && checkRequredField(lLocationType) && checkRequredField(lDescription)) {
                return true;
            } else return false;
        } else {
            if (lLocationType != 'Select Location Type*' && checkRequredField(lTitle) && checkRequredField(lLocation) && checkRequredField(lLocationType) && checkRequredField(lDescription) && checkRequredField(selfEmails)) {

                if (contains(selfEmails, '@')) {
                    return true;
                } else {
                    participantFlag = true;
                    return false;
                }
            } else return false;
        }
    }

    function isSelfUser() {
        return isSefCalendar;
    }

    function onClickSelectAnotherDate(send) {
        lTitle = $('#MeetingTitleDetails').val();
        lLocation = $('#locationDetails').val();
        lLocationType = $('#locationType').val();
        lDescription = $('#meetingDescriptionDetails').val();

        if (isSelfUser()) {
            selfEmails = $('#selfEmails').val();
        }

        if (validateMeetingDetails(isSelfUser())) {

            calendar.fullCalendar('removeEvents', '@@@')
            if (update) {

                var startDate = $("#startDate").val()
                var startTime = $("#startTime").val()
                var endDate = $("#endDate").val()
                var endTime = $("#endTime").val()
                var startDateArr = startDate.split('-')
                var startTimeArr = startTime.split(':')
                var endDateArr = endDate.split('-')
                var endTimeArr = endTime.split(':')
                var start = new Date(parseInt(startDateArr[0]), parseInt(startDateArr[1]) - 1, parseInt(startDateArr[2]), parseInt(startTimeArr[0]), parseInt(startTimeArr[1]));
                var end = new Date(parseInt(endDateArr[0]), parseInt(endDateArr[1]) - 1, parseInt(endDateArr[2]), parseInt(endTimeArr[0]), parseInt(endTimeArr[1]));
                if (start < end) {
                    var details = {
                        start: start,
                        end: end,
                        id: updateEventId,
                        title: lTitle,
                        location: lLocation,
                        locationType: lLocationType,
                        description: lDescription,
                        selfEmails: selfEmails
                    }
                    var now = new Date()

                    if (now >= details.start) {
                        showMessagePopUp("Please select future time to set the meeting.", 'error')
                    } else onSaveUpdateEvent(details, send);

                }
                else {
                    showMessagePopUp("Please select valid dates/ time", 'error')
                }
            }
            else {

                $.ajax({
                    url: '/getTimeSlots',
                    type: 'GET',
                    datatype: 'JSON',
                    traditional: true,
                    success: function (result) {
                        var size = result.length;
                        timeSlotsArray(date, size, lTitle, lLocation, lLocationType, lDescription, selfEmails, send)
                    },
                    error: function (event, request, settings) {

                    }
                });

                removePopover();
                $(".popover").remove();
            }
        }
        else {
            if (!participantFlag) {
                showMessagePopUp("Please enter all fields in meeting details.", 'error');
            }
            else {
                participantFlag = false;
                showMessagePopUp("Please provide valid email id for participant.", 'error');
            }

        }
    }

    function timeSlotsArray(date, size, title, location, locationType, description, emails, send) {
        var ids = ['_ 1 !', '_ 2 #', '_ 3 $']

        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var hours = date.getHours();
        var min = date.getMinutes();
        var hoursEnd = hours + 1;
        var minEnd = min + 30;
        var start = new Date(y, m, d, hours, min);
        var end = new Date(y, m, d, hours, minEnd);

        var details = {
            start: start,
            end: end,
            id: ids[size],
            title: title,
            location: location,
            locationType: locationType,
            description: description,
            selfEmails: emails
        }

        $.ajax({
            url: '/timeSlots',
            type: 'POST',
            datatype: 'JSON',
            data: details,
            success: function (result) {

                if (result) {

                    calendar.fullCalendar('removeEvents', '@@@')
                    showBottomMessage()
                    renderEvent(details)
                    display();
                    if (toSendInvite || send) {
                        $('#sendInvitation').trigger('click');
                    }
                }
                else {
                    showMessagePopUp("Maximum 3 time slots can be selected for a meeting.", 'error')
                }
            },
            timeout: 20000
        });
    }

    function getTimeSlots() {
        $.ajax({
            url: '/getTimeSlots',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if (checkRequredField(result)) {

                    if (checkRequredField(result[0])) {

                        isLoggedInUser()
                    }
                    else {
                        $('.MeetingProcessLogin').css({display: 'none'});
                    }
                }
                else {
                    $('.MeetingProcessLogin').css({display: 'none'});
                }
                for (var i = 0; i < result.length; i++) {
                    calendar.fullCalendar('renderEvent',
                        {
                            id: result[i].id,
                            start: result[i].start,
                            end: result[i].end,
                            title: result[i].title,
                            location: result[i].location,
                            locationType: result[i].locationType,
                            description: result[i].description,
                            selfEmails: result[i].selfEmails,
                            allDay: false,

                            deleteBut:true,
                            type:'timeSlot',
                            slot:true,
                            spanTitle:'Remove Time Slot',
                            selfCalendar:false,
                            sender:false,
                            marginTop:'-25%'
                        },
                        true // make the event "stick"
                    );
                }
            },
            error: function (event, request, settings) {

            }
        })
    }

    function getFileName() {
        $.ajax({
            url: '/fileName',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {

                if (result == false) {
                    $('#fileName').css({display: 'none'})
                }
                else {
                    documents = result;
                    $('#file-chooser').hide()
                    $('#upload-button').hide()

                    $('#fileName').css({'display': 'block', 'margin-top': '10px'})
                    $('#docTitle').text("Document Name : " + result.documentName)
                    $('#form').css({display: 'none'})

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function renderEvent(details) {

        calendar.fullCalendar('renderEvent',
            {
                id: details.id,
                start: details.start,
                end: details.end,
                title: details.title || 'Schedule',
                location: details.location,
                locationType: details.locationType,
                description: details.description,
                selfEmails: details.selfEmails,
                allDay: false,

                deleteBut:true,
                type:'timeSlot',
                slot:true,
                spanTitle:'Remove Time Slot',
                selfCalendar:false,
                sender:false,
                marginTop:'-25%'
            },
            true // make the event "stick"
        );
    }


    $("body").on("click", "#slot-delete", function () {
         var slotId = $(this).attr('slotId');
         if(checkRequredField(slotId)){
             $.ajax({
                 url:'/schedule/slot/remove',
                 type:'POST',
                 datatype:'JSON',
                 data:{slotId:slotId},
                 success:function(response){
                    if(response){

                        calendar.fullCalendar('removeEvents', '_ 1 !');
                        calendar.fullCalendar('removeEvents', '_ 2 #');
                        calendar.fullCalendar('removeEvents', '_ 3 $');
                        getTimeSlots()
                        display()
                        showMessagePopUp("Time slot has been successfully removed.",'success');
                    }
                    else{
                        showMessagePopUp("An error occurred. Please try again.",'error');
                    }
                 }
             })
         }
    });

    function updateEvent(event, delta, revertFunc) {
        if (event.id == '_ 1 !' || event.id == '_ 2 #' || event.id == '_ 3 $') {
            var details = {
                id: event.id,
                start: event.start,
                end: event.end,
                title: event.title,
                location: event.location,
                locationType: event.locationType,
                description: event.description,
                selfEmails: event.selfEmails

            }
            $.ajax({
                url: '/updateTimeSlots',
                type: 'POST',
                datatype: 'JSON',
                data: details,
                traditional: true,
                success: function (result) {
                    if (result == true) {
                        if (showDetailsFlag) {

                            display();
                        }
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            })
        }
    }

    // Function to update event
    function onSaveUpdateEvent(details, send) {
        removePopover();
        $(".popover").remove();
        $.ajax({
            url: '/updateTimeSlots',
            type: 'POST',
            datatype: 'JSON',
            data: details,
            traditional: true,
            success: function (result) {
                if (result == true) {
                    $('#box').hide();
                    calendar.fullCalendar('removeEvents', details.id)
                    renderEvent(details)

                    display()
                    if (send) {
                        $("#sendInvitationBut").trigger("click");
                    }
                }
                else {
                    showMessagePopUp("Error occurred. Please try again", 'error')
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function showSelectedTimes(result) {
        if (result[0]) {
            showBottomMessage();
            inFormOrLink = true;
            $('#selected-day-time1').removeClass('invisible');
            var time = getTimeFormatWithMeridian(new Date(result[0].start)) + " - " + getTimeFormatWithMeridian(new Date(result[0].end));
            var dayString = getDayFormat(new Date(result[0].start));
            $('#selected-day1').text(dayString);
            $('#selected-time1').text(time);
            $('#table-selected-date1').text(dayString);
            $('#table-selected-time1').text(time);
            $('#table-selected-location-type1').text(result[0].locationType);
            $('#table-selected-location-details1').text(result[0].location);

        }
        else {
            $('#selected-day-time1').addClass('invisible');
        }

        if (result[1]) {
            inFormOrLink = true;
            $('#selected-day-time2').removeClass('invisible');
            var time = getTimeFormatWithMeridian(new Date(result[1].start)) + " - " + getTimeFormatWithMeridian(new Date(result[1].end));
            var dayString = getDayFormat(new Date(result[1].start));
            $('#selected-day2').text(dayString);
            $('#selected-time2').text(time);
            $('#table-selected-date2').text(dayString);
            $('#table-selected-time2').text(time);
            $('#table-selected-location-type2').text(result[1].locationType);
            $('#table-selected-location-details2').text(result[1].location);
        }
        else {
            $('#selected-day-time2').addClass('invisible');
        }

        if (result[2]) {
            inFormOrLink = true;
            $('#selected-day-time3').removeClass('invisible');
            var time = getTimeFormatWithMeridian(new Date(result[2].start)) + " - " + getTimeFormatWithMeridian(new Date(result[2].end));
            var dayString = getDayFormat(new Date(result[2].start));
            $('#selected-day3').text(dayString);
            $('#selected-time3').text(time);
            $('#table-selected-date3').text(dayString);
            $('#table-selected-time3').text(time);
            $('#table-selected-location-type3').text(result[2].locationType);
            $('#table-selected-location-details3').text(result[2].location);
        }
        else {
            $('#selected-day-time3').addClass('invisible');
        }
    }

    function display() {
        isLoggedInUser()

        showDetailsFlag = true;

        $.ajax({
            url: '/getTimeSlots',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                timeSlots = result;
                showSelectedTimes(result)
            },
            error: function (event, request, settings) {

            }
        })
    };

    var analysisFlag = false;

    function construcInvitationDetails(loggedinUser, sName, sEmail) {
        var dName, dUrl, awsKey;
        if (documents == undefined) {
            dName = ''
            dUrl = ''
            awsKey = ''
        }
        else if (documents.documentName == undefined) {
            dName = ''
            dUrl = ''
            awsKey = ''
        }
        else {
            dName = documents.documentName
            dUrl = documents.documentUrl
            awsKey = documents.awsKey
        }

        var invitationDetails = {
            senderId: '',
            senderName: '',
            senderEmailId: '',
            receiverId: publicProfile._id,
            receiverName: publicProfile.firstName + " " + publicProfile.lastName,
            receiverEmailId: publicProfile.emailId,
            documentName: dName,
            documentUrl: dUrl,
            awsKey: awsKey,
            scheduledDate: new Date()
        }

        $.ajax({
            url: '/getTimeSlots',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                var timeSlots = result;
                if (result.length < 2) {
                    if (result.length == 0) {
                        if (loggedinUser == 'linkedin') {
                            $("#loginUsingLinkedin").trigger("click");
                        } else
                            showMessagePopUp("Please select time slots on the calendar before sending the invitation.", 'error')
                    } else if (!analysisFlag) {
                        showMessagePopUp("Our analysis shows that Relatas users prefer 3 selected time options. This has higher probability of getting a meeting accepted faster. Select additional time slots else click on Send invite again.", 'tip')

                        analysisFlag = true;
                    }
                    else {
                        constructTimeslotsJson(invitationDetails, timeSlots, loggedinUser, sName, sEmail)
                    }

                }
                else {
                    constructTimeslotsJson(invitationDetails, timeSlots, loggedinUser, sName, sEmail)
                }
            },
            error: function (event, request, settings) {

            }
        });
    }

    function contains(str, subStr) {
        return str.indexOf(subStr) != -1;
    }

    function constructTimeslotsJson(invitationDetails, timeSlots, loggedinUser, sName, sEmail) {
        if (timeSlots[0]) {
            invitationDetails.start1 = timeSlots[0].start
            invitationDetails.end1 = timeSlots[0].end
            invitationDetails.title1 = timeSlots[0].title
            invitationDetails.location1 = timeSlots[0].location
            invitationDetails.locationType1 = timeSlots[0].locationType
            invitationDetails.description1 = timeSlots[0].description
        }
        if (timeSlots[1]) {
            invitationDetails.start2 = timeSlots[1].start
            invitationDetails.end2 = timeSlots[1].end
            invitationDetails.title2 = timeSlots[1].title
            invitationDetails.location2 = timeSlots[1].location
            invitationDetails.locationType2 = timeSlots[1].locationType
            invitationDetails.description2 = timeSlots[1].description
        }
        if (timeSlots[2]) {
            invitationDetails.start3 = timeSlots[2].start
            invitationDetails.end3 = timeSlots[2].end
            invitationDetails.title3 = timeSlots[2].title
            invitationDetails.location3 = timeSlots[2].location
            invitationDetails.locationType3 = timeSlots[2].locationType
            invitationDetails.description3 = timeSlots[2].description
        }


        if (loggedinUser == 'yes') {
            sendInvitation(invitationDetails)
        }
        else if (loggedinUser == 'no') {
            invitationDetails.senderEmailId = sEmail;
            invitationDetails.senderName = sName;

            sendInvitationWithMail(invitationDetails)

        }
        else if (loggedinUser == 'linkedin') {
            sendInvitationUsingLinkedin(invitationDetails)
        }
        else if (loggedinUser == 'self') {
            sendInvitationSelf(invitationDetails, timeSlots);
        }
    }

    function displayLoginWindowMessage(text) {
        $("#login-window-message").text(text)
    }

    $("#lightBoxClose").on("click", function () {
        $('#lightBoxPopup').css({display: 'none'});
        $('#calendar').css({display: 'block'});
        showHideTimeDisplay(true);
        $("#btmMsg").text('Your message has NOT been sent yet. Please click on "Send Invite" below to send the invitation.');

    });

    $("#signInSignUpFB").on("click", function () {
        $('#calendar').css({display: 'none'});
        showHideTimeDisplay(false);
        $('#lightBoxPopup').css({display: 'block'});
    });
    $("#signInSignUpTwitter").on("click", function () {
        $('#calendar').css({display: 'none'});
        showHideTimeDisplay(false);
        $('#lightBoxPopup').css({display: 'block'});
    });
    $("#signInSignUpLinkedin").on("click", function () {
        $('#calendar').css({display: 'none'});
        showHideTimeDisplay(false);
        $('#lightBoxPopup').css({display: 'block'});
    });
    $("#signInSignUpRelatas").on("click", function () {
        $('#calendar').css({display: 'none'});
        showHideTimeDisplay(false);
        $('#lightBoxPopup').css({display: 'block'});
    });

    function sendInvitationSelf(invitationDetails, timeslots) {
        var toList = [];
        var popUp = true;
        invitationDetails.selfCalendar = true;

        if (timeslots[0]) {

            if (checkRequredField(timeslots[0].selfEmails)) {

                if (contains(timeslots[0].selfEmails, ',') || contains(timeslots[0].selfEmails, ';')) {

                    if (timeslots.length > 1) {
                        popUp = false;
                        showMessagePopUp("You selected two or more time slots. Please provide only one email id.", 'error')
                    } else {
                        var emails = extractEmails(timeslots[0].selfEmails);
                        for (var i = 0; i < emails.length; i++) {
                            if (validateEmailField(emails[i])) {
                                var toObj1 = {
                                    receiverEmailId: emails[i].trim()
                                }
                                toList.push(toObj1);
                            }
                        }
                    }
                } else {
                    if (validateEmailField(timeslots[0].selfEmails)) {
                        var toObj2 = {
                            receiverEmailId: timeslots[0].selfEmails.trim()
                        }
                        toList.push(toObj2);
                    }
                }

                if (toList[0]) {
                    invitationDetails.toList = JSON.stringify(toList);
                    invitationDetails.senderTimezone = timezone2 || timezone || getTimezone();
                    invitationDetails.receiverTimezone = timezone2 ||loggedInUserTimezone || timezone || getTimezone();
                    sendSelfMeetingAjax(invitationDetails)
                }
                else {
                    if (popUp)
                        showMessagePopUp("Please provide Participants details before sending the invitation.", 'error')
                }
            } else showMessagePopUp("Please provide Participants details before sending the invitation.", 'error')
        } else {
            showMessagePopUp("Please select time slots on the calendar before sending the invitation.", 'error')
        }
    }

    function extractEmails (text)
    {
        return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
    }

    function sendSelfMeetingAjax(invitationDetails) {
        if (checkRequredField(invitationDetails.start1) && checkRequredField(invitationDetails.toList)) {
            hideSendInviteButton(true);
            $.ajax({
                url: '/sendInvitationSelf',
                type: 'POST',
                datatype: 'JSON',
                data: invitationDetails,
                traditional: true,
                success: function (result) {

                    if (result == 'loginRequired') {
                        hideSendInviteButton(false);
                        $('#lightBoxPopup').css({display: 'block'});
                        $('#calendar').css({display: 'none'});
                        showHideTimeDisplay(false);
                        displayLoginWindowMessage("Please Login to send the Meeting Request");
                        $("#btmMsg").text("Your message has NOT been sent yet. Please login to send the meeting invitation.");

                    }
                    if (result == 'success') {
                        hideSendInviteButton(true);
                        mixpanelTrack("Create Meeting - SELF");
                        mixpanelIncrement("# Met");
                        aa = true;
                        newU = false;
                        showBottomMessage()
                        $("#btmMsg").text("Your Meeting invitation has been sent.");
                        if (createFlag) {
                            createFlag = false;
                            showMessagePopUp("Congratulations! Your meeting invitation has been sent. Confirmation mail has been sent to your email id. \n We also reserved your Unique Relatas Identity: " + uniqueId + ". Please click on CLOSE to reserve your Unique Relatas Identity and enjoy the benefits. It will not take more than 60 SECONDS to become a member of Relatas' growing community.", 'success')
                            newU = true;
                            aa = false;
                        } else
                            showMessagePopUp("Congratulations! Your meeting invitation has been sent. Confirmation mail has been sent to your email id.", 'success')

                    }
                    if (result == 'error') {
                        hideSendInviteButton(false);
                        showMessagePopUp("Error occurred while sending your invitation. Please try again", 'error')

                    }
                },
                error: function (event, request, settings) {
                    hideSendInviteButton(false);
                    var status = event.status;
                    switch (status) {
                        case 4010:
                            console.error("please specify all fields");
                            showMessagePopUp("Error occurred while sending your invitation. Please try again", 'error')

                            break;
                    }
                },
                timeout: 20000
            })
        }
        else {
            showMessagePopUp("'Please select time slots on the calendar before sending the invitation.", 'error')

        }

    }

    function sendInvitationUsingLinkedin(invitationDetails) {
        invitationDetails.senderTimezone = loggedInUserTimezone || timezone || getTimezone();
        invitationDetails.receiverTimezone = timezone2 ||loggedInUserTimezone || timezone || getTimezone();
        $.ajax({
            url: '/storeMeetingDetails',
            type: 'POST',
            datatype: 'JSON',
            data: invitationDetails,
            success: function (response) {
                if (response) {
                    inFormOrLink = false;
                    window.location.replace('/sendInviteUsingLinkedin');
                }
                else {
                    showMessagePopUp("Error occurred while sending your invitation. Please try again", 'error');
                }
            },
            timeout: 20000
        })
    }

    var newU = false;
    var oo = true;

    function showBottomMessage() {
        $("#btmMsg-show-hide").fadeIn();
    }

    function checkLinkedinSendInviteStatus() {
        $.ajax({
            url: '/sendInviteLinkedinStatus',
            type: 'GET',
            success: function (response) {

                if (response.sendInviteLinkedinStatus == 'userExist') {
                    showBottomMessage();
                    $('#sendInvitation').trigger("click");
                    analysisFlag = true;
                    partialProfile = true;
                    hideSendInviteButton(true);
                }
                else if (response.sendInviteLinkedinStatus == false) {
                    hideSendInviteButton(false);
                    showMessagePopUp("Error occurred while sending your invitation. Please try again", 'error');
                }
                else if (response.sendInviteLinkedinStatus == true) {
                    hideSendInviteButton(true);
                    mixpanelTrack("Create Meeting - OTHER");
                    mixpanelIncrement("# Met");
                    mixpanelIncrement("#Login");
                    showBottomMessage()
                    $("#btmMsg").text("Your Meeting invitation has been sent.");
                    showMessagePopUp("Congratulations! Your meeting invitation has been sent. Confirmation mail has been sent to your email id. \n We also reserved your Unique Relatas Identity: " + response.identity + ". Please click on CLOSE to reserve your Unique Relatas Identity and enjoy the benefits. It will not take more than 60 SECONDS to become a member of Relatas' growing community.", 'success')
                    newU = true;
                    aa = false;
                }
            },
            timeout: 20000
        })
    }

    function validateUserGoogleAccount(userInfo) {
        if (checkRequredField(userInfo.google)) {
            if (checkRequredField(userInfo.google[0])) {
                if (checkRequredField(userInfo.google[0].id)) {
                    return true;
                } else  return false;
            } else  return false;
        } else  return false;
    }

    var aa = false;
    var createFlag = false;

    function sendInvitation(invitationDetails) {
        if (checkRequredField(invitationDetails.start1)) {
            hideSendInviteButton(true);
            if (validateUserGoogleAccount(publicProfile)) {
                invitationDetails.receiverPrimaryEmailId = publicProfile.google[0].emailId;
            }

            invitationDetails.senderTimezone = loggedInUserTimezone || timezone || getTimezone();
            invitationDetails.receiverTimezone = timezone2 ||loggedInUserTimezone || timezone || getTimezone();
            $.ajax({
                url: '/sendInvitation',
                type: 'POST',
                datatype: 'JSON',
                data: invitationDetails,
                traditional: true,
                success: function (result) {

                    if (result == 'loginRequired') {
                        hideSendInviteButton(false);
                        $('#lightBoxPopup').css({display: 'block'});
                        $('#calendar').css({display: 'none'});
                        showHideTimeDisplay(false);
                        displayLoginWindowMessage("Please Login to send the Meeting Request");
                        $("#btmMsg").text("Your message has NOT been sent yet. Please login to send the meeting invitation.");

                    }
                    if (result == 'success') {
                        hideSendInviteButton(true);
                        aa = true;
                        newU = false;
                        showBottomMessage()
                        $("#btmMsg").text("Your Meeting invitation has been sent.");
                        mixpanelTrack("Create Meeting - OTHER");
                        mixpanelIncrement("# Met");
                        if (createFlag) {
                            createFlag = false;
                            showMessagePopUp("Congratulations! Your meeting invitation has been sent. Confirmation mail has been sent to your email id. \n We also reserved your Unique Relatas Identity: " + uniqueId + ". Please click on CLOSE to reserve your Unique Relatas Identity and enjoy the benefits. It will not take more than 60 SECONDS to become a member of Relatas' growing community.", 'success')
                            newU = true;
                            aa = false;
                        } else
                            showMessagePopUp("Congratulations! Your meeting invitation has been sent. Confirmation mail has been sent to your email id.", 'success')

                    }
                    if (result == 'error') {
                        hideSendInviteButton(false);
                        showMessagePopUp("Error occurred while sending your invitation. Please try again", 'error')

                    }
                },
                error: function (event, request, settings) {
                    hideSendInviteButton(false);
                    var status = event.status;
                    switch (status) {
                        case 4010:
                            console.error("please specify all fields");
                            showMessagePopUp("Error occurred while sending your invitation. Please try again", 'error')

                            break;
                    }
                },
                timeout: 20000
            })
        }
        else {
            showMessagePopUp("'Please select time slots on the calendar before sending the invitation.", 'error')

        }
    };

    function sendInvitationWithMail(invitationDetails) {
        invitationDetails.senderTimezone = timezone2 || timezone || getTimezone();
        invitationDetails.receiverTimezone = timezone2 ||loggedInUserTimezone || timezone || getTimezone();
        $.ajax({
            url: '/sendInvitationWithMail',
            type: 'POST',
            datatype: 'JSON',
            data: invitationDetails,
            traditional: true,
            success: function (result) {

                if (result == 'success') {
                    aa = true;
                    newU = false;
                    showBottomMessage()
                    $("#btmMsg").text("Your Meeting invitation has been sent.");
                    showMessagePopUp("Congratulations! Your meeting invitation has been sent. Confirmation mail has been sent to your email id.", 'success')
                }
                if (result == 'error') {
                    showMessagePopUp("Error occurred while sending your invitation. Please try again", 'error')

                }
            },
            error: function (event, request, settings) {
                var status = event.status;
                switch (status) {
                    case 4010:
                        console.error("Please specify all fields");
                        showMessagePopUp("Please specify all fields", 'error')
                        break;
                }
            },
            timeout: 20000
        })
    };
    isLoggedInUser();
    // Function to know user is loggedin or not
    function isLoggedInUser() {
        $.ajax({
            url: '/isLoggedInUser',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if (result) {
                    signInFlag = true;

                    $("#logOut").text('LOGOUT');
                }
                else {
                    signInFlag = false;
                    $("#logOut").text('Sign Up/ Login');
                    $('#mapMyCalendar').attr("checked", false);
                    show_hide_socialTabs(false, false);
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 40000
        })
    };
    var comConnectionsLength;
    var currentStart = 0;
    var currentEnd = 0;
    // Logic to get common connections
    function getCommonConnections(publicUserId) {
        var user = {
            id: publicUserId,
            requestFrom: true
        }

        $.ajax({
            url: '/getCommonConnections',
            type: 'POST',
            datatype: 'JSON',
            data: user,
            traditional: true,
            success: function (result) {

                if (result == false) {

                    $('.small-login-container').css({display: 'block'});
                    $('.linkedin-social-details').css({display: 'none'});
                }
                else if (result.message) {

                    $('.small-login-container').css({display: 'none'});
                    $('.social-connections-list').html('<span style="color: white"> &nbsp;&nbsp;' + result.message + '</span>')
                }
                else {

                    $('.small-login-container').css({display: 'none'});
                    $('.linkedin-social-details').css({display: 'block'});
                    if (checkRequredField(result.relationToViewer) && checkRequredField(result.relationToViewer.relatedConnections) && checkRequredField(result.relationToViewer.relatedConnections.values) && result.relationToViewer.relatedConnections.values.length > 0) {

                        var connections = result.relationToViewer.relatedConnections.values;
                        comConnectionsLength = connections.length >= 9 ? 9 : connections.length;

                        if (connections.length > 3) {
                            $("#connection-next").show()
                            //$("#connection-prev").show()
                        }

                        for (var i = 0; i < connections.length; i++) {
                            var linkedinUrl = connections[i].siteStandardProfileRequest.url;
                            var firstName = connections[i].firstName || '';
                            var lastName = connections[i].lastName || '';
                            var title = firstName + ' ' + lastName;
                            var iUrl = connections[i].pictureUrl || '/images/default.png';
                            var commonConnetionHtml = '';
                            var id = 'connection-' + i;
                            if (i > 2)
                                commonConnetionHtml = ' <td class="social-connection-item" style="display: none" id=' + id + '>';
                            else
                                commonConnetionHtml = ' <td class="social-connection-item" id=' + id + '>';

                            commonConnetionHtml += '<a target="_blank" href=' + linkedinUrl + '><div class="profile-picture-small social-connection-profile-picture">';
                            //commonConnetionHtml += '<img class="profile-picture-border" src="/images/white_circular_border_small.png" title='+title.replace(/\s/g, '&nbsp;')+'>';
                            commonConnetionHtml += '<img class="profile-pic-img-small" style="border: 2px solid white;" id="lImage1" src=' + iUrl + ' title=' + title.replace(/\s/g, '&nbsp;') + '><br></div></a>';
                            commonConnetionHtml += '</td>';
                            if (i < 9) {
                                // $("#connection-next").attr('next',i+1);
                                $('.social-connections-list').append(commonConnetionHtml);
                            }
                            if (i <= 2)
                                currentEnd = i;
                        }
                    } else {
                        $("#connection-next").hide()
                        $("#connection-prev").hide()
                        $('.small-login-container').css({display: 'none'});
                        $('.social-connections-list').html('<span style="color: white"> &nbsp;&nbsp;No common connections found</span>')
                    }

                    if (checkRequredField(result.currentShare)) {
                        var statusContent = '';
                        if (checkRequredField(result.currentShare.content)) {
                            statusContent = result.currentShare.content.description || result.currentShare.comment || '';

                            if (!checkRequredField(result.currentShare.content.description)) {
                                if (checkRequredField(result.currentShare.attribution) && checkRequredField(result.currentShare.attribution.share)) {
                                    statusContent = result.currentShare.attribution.share.comment || '';

                                } else statusContent = result.currentShare.comment;
                            }
                        } else if (checkRequredField(result.currentShare.attribution) && checkRequredField(result.currentShare.attribution.share)) {
                            statusContent = result.currentShare.attribution.share.comment || '';

                        } else {
                            statusContent = result.currentShare.comment || '';
                        }
                        if (checkRequredField(statusContent)) {
                            var linkedinStatusUpdaeHtml = '<div class="social-post-title">';

                            linkedinStatusUpdaeHtml += '<span>' + statusContent + '</span></div>';
                            $('#latestPost').html(linkedinStatusUpdaeHtml);
                        }
                    }

                    if (checkRequredField(result.positions) && checkRequredField(result.positions.values) && result.positions.values.length > 0) {
                        var positions = result.positions.values;
                        var linkedinCompaniesHtml = '';
                        linkedinCompaniesHtml += '';
                        for (var p = 0; p < positions.length; p++) {
                            if (p <= 2) {
                                linkedinCompaniesHtml += '<span style="color: white">' + positions[p].company.name + '</span></br>';
                            }
                        }
                        $('.social-latest-positions').html(linkedinCompaniesHtml);
                    }
                }
            },
            error: function (event, request, settings) {

            }
        })
    };

    $("#connection-next").on('click', function () {
        if (currentEnd < comConnectionsLength - 1) {
            var nowStart = currentEnd + 1;
            var nowEnd = (nowStart + 2) >= comConnectionsLength ? comConnectionsLength : nowStart + 2;
            displayPrevNext(nowStart, nowEnd)

        }
    });
    $("#connection-prev").on('click', function () {
        if (currentStart > 0) {
            var nowStart = currentStart - 3 <= 0 ? 0 : currentStart - 3;
            var nowEnd = (nowStart + 2) >= comConnectionsLength ? comConnectionsLength : nowStart + 2;
            displayPrevNext(nowStart, nowEnd)

        }
    });
    function displayPrevNext(nowStart, nowEnd) {
        for (var c = 0; c < comConnectionsLength; c++) {
            if (c < nowStart) {
                $("#connection-" + c).hide();
            } else if (c >= nowStart && c <= nowEnd) {
                $("#connection-" + c).show();
            } else $("#connection-" + c).hide();
        }
        if (nowStart == 0)
            $("#connection-prev").hide()
        else  $("#connection-prev").show()

        if (nowEnd == comConnectionsLength || nowEnd == comConnectionsLength - 1)
            $("#connection-next").hide()
        else  $("#connection-next").show()

        currentStart = nowStart;
        currentEnd = nowEnd;
    }

    // Function to get linkedin profile of public user
    function getLinkedinPofile(publicUserId) {
        var user = {
            id: publicUserId
        }
        $.ajax({
            url: '/linkedinPro',
            type: 'POST',
            datatype: 'JSON',
            data: user,
            traditional: true,
            success: function (result) {

                if (result == false) {

                }
                else if (result.message) {

                }
                else {
                    var linkedinSchoolHtml = '';
                    var schools = result.educations.values;
                    if (checkRequredField(schools)) {
                        if (checkRequredField(schools[0])) {
                            schools.forEach(function (school) {
                                linkedinSchoolHtml += '<span style="color: white">' + school.schoolName + '</span></br>';
                            })
                        }
                        $('.social-latest-school').html(linkedinSchoolHtml);
                    }
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    };

    function getTwitterInfo(publicUserId) {
        var user = {
            id: publicUserId
        }
        $.ajax({
            url: '/getTwitterInfo',
            type: 'POST',
            datatype: 'JSON',
            data: user,
            traditional: true,
            success: function (result) {

                if (result == false) {
                    $('.small-login-container').css({display: 'block'});

                }
                else if (result.message) {
                    $('#twitter-msg').html('<span style="color: white">&nbsp;&nbsp;No tweets found</span>');


                }
                else {
                    for (var i = 0; i < result.length; i++) {
                        if (i < 5) {
                            $('#twitter-msg').append('<span style="color: white">' + result[i].text + '</span></br><hr></br>');
                        }
                    }
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000

        })
    };

    function getFacebookInfo(publicUserId) {
        var user = {
            id: publicUserId
        }
        $.ajax({
            url: '/getFacebookInfo',
            type: 'POST',
            datatype: 'JSON',
            data: user,
            traditional: true,
            success: function (result) {

                if (result == false) {

                    $('.small-login-container').css({display: 'block'});
                }
                else if (result.message) {

                    $('.small-login-container').css({display: 'none'});
                    $('#facebook-msg').html('<span style="color: white"> &nbsp;&nbsp;No common facebook friends found.</span>');
                }
                else {

                    for (var i = 0; i < result.length; i++) {

                        $('#facebook-msg').append('<span style="color: white">' + result[i].name + '</span></br><hr></br>');

                    }
                    getFacebookStatus(user);
                }
            },
            error: function (event, request, settings) {

            }

        })
    };

    // Function to get latest linkedin update
    function getLinkedinStatusUpdate(userId, callback) {
        $.ajax({
            url: '/linkedinStatus/' + userId,
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if (!result) {

                }
                else if (result.message) {

                }
                else {

                    callback(result)

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    };

    // function to get facebook status
    function getFacebookStatus(user) {
        $.ajax({
            url: '/facebookStatus',
            type: 'POST',
            datatype: 'JSON',
            data: user,
            traditional: true,
            success: function (result) {
                if (!result) {

                }
                else if (result.message) {
                }
                else {

                    // handling facebook status

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        });
    }

    function removePopover() {
        $("#popover-source").popover('destroy');
    }

    $('body').on('click', '#Send_Invite', function (e) {
        onClickSend();
    });

    $('body').on('click', '#Select', function (e) {
        onClickSelectAnotherDate(false);
    });


    $('body').on('click', '#close', function (e) {
        calendar.fullCalendar('removeEvents', '@@@');
        removePopover();
    });


    $('#sendInvitation').on('click', function () {
        if (isSelfUser()) {
            construcInvitationDetails('self');
        } else construcInvitationDetails('yes');


    });

    $('#sendInviteWithEmail').on('click', function () {

        var sName = $('#sName').val()
        var sEmail = $('#sEmailId').val()

        if (validateEmailField(sEmail) && checkRequredField(sName)) {
            construcInvitationDetails('no', sName, sEmail)
        }
        else {
            showMessagePopUp("Please enter your details", 'error')

        }
    });


    $('#loginFormButton').on('click', function () {
        var emailId1 = $("#emailId2").val();
        var password1 = $("#password").val();

        if (checkRequredField(emailId1) && checkRequredField(password1)) {
            emailId1 = emailId1.toLowerCase();
            var credentials = {
                emailId: emailId1,
                password: password1
            }

            $.ajax({
                url: '/loginFromPublicProfile',
                type: 'POST',
                datatype: 'JSON',
                data: credentials,
                traditional: true,
                success: function (result) {
                    if (result) {
                        $('#lightBoxClose').trigger('click');
                        $("#btmMsg").text('Your message has NOT been sent yet. Please click on "Send Invite" below to send the invitation.');

                        loadUserPublicProfile()
                        isLoggedInUser()
                        exeFlag = true;
                        mapMyCalendarFlag = true;
                        if (signInFlag) {
                            signInFlag = true;
                            analysisFlag = true;
                            $("#sendInvitationBut").trigger("click");
                        }

                        onChangeMapMyCalendar()
                    }
                    else {
                        showMessagePopUp("Looks like either your email or password is incorrect. Please try again.", 'error')
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            })
        }
        else {
            showMessagePopUp("Please enter your credentials", 'error')

        }
    });

    $("#sendUsingLinkedin").on("click", function () {
        mixpanelTrack("Signup LinkedIn")

        construcInvitationDetails('linkedin');
    });

    $('#loginUsingGoogle').on('click', function () {
        mixpanelTrack("Signup Google")

        inFormOrLink = false;
        window.location.replace('/getGoogleTokenMeetingProcess');
    });
    $('#login-with-google').on('click', function () {
        mixpanelTrack("Signup Google")

        inFormOrLink = false;
        window.location.replace('/getGoogleTokenMeetingProcess');
    });
    $('#sign-up-with-google').on('click', function () {
        mixpanelTrack("Signup Google")

        inFormOrLink = false;
        window.location.replace('/getGoogleTokenMeetingProcess');
    });

    $('#loginUsingLinkedin').on('click', function () {
        mixpanelTrack("Signup LinkedIn")

        inFormOrLink = false;
        window.location.replace('/linkedinLoginFromMeetingPage');
    });
    $('#login-with-linkedin').on('click', function () {
        mixpanelTrack("Signup LinkedIn")

        inFormOrLink = false;
        window.location.replace('/linkedinLoginFromMeetingPage');
    });
    $('#sign-up-with-linkedin').on('click', function () {
        mixpanelTrack("Signup LinkedIn")

        inFormOrLink = false;
        window.location.replace('/linkedinLoginFromMeetingPage');
    });

    $('#signUp').on('click', function () {
        inFormOrLink = false;
        window.location.replace('/meetingProcessSignUp');
    });

    // Logic to get common connections
    $('.linkedin-icon-tab-item').on('click', function () {

        getCommonConnections(publicProfile.id);
        getLinkedinPofile(publicProfile.id);
    });

    $('#twitterImg').on('click', function () {
        getTwitterInfo(publicProfile.id);
    });

    $('#facebookImg').on('click', function () {
        getFacebookInfo(publicProfile.id);
    });

    $('#nextConnection').on('click', function () {

        if (count < commonConnections.length) {
            $('#userCommonConnectionPic').attr('src', commonConnections[count].pictureUrl)
            count++;
        }
        else count = 0
    });

    function loadUserBusyTimes(publicProfile,color,id) {

        if (publicProfile.calendarAccess.calendarType == 'selectPublic') {
            var dateBusy = new Date();
            var eventSource = [];


            var startTime1 = publicProfile.calendarAccess.timeOne.start;
            var endTime1 = publicProfile.calendarAccess.timeOne.end;
            var startTime2 = publicProfile.calendarAccess.timeTwo.start;
            var endTime2 = publicProfile.calendarAccess.timeTwo.end;


            if (checkRequredField(startTime1) && checkRequredField(endTime1)) {
                start1Arr = startTime1.split(':');
                end1Arr = endTime1.split(':');
            }
            if (checkRequredField(startTime2) && checkRequredField(endTime2)) {
                start2Arr = startTime2.split(':');
                end2Arr = endTime2.split(':');
            }


            for (var i = 0; i <= 46; i++) {
                if (i == 0) {

                } else dateBusy.setDate(dateBusy.getDate() + 1);

                var dn = dateBusy.getDate();
                var mn = dateBusy.getMonth();
                var yn = dateBusy.getFullYear();

                if (checkTime(publicProfile.calendarAccess.timeOne.end) && checkTime(publicProfile.calendarAccess.timeTwo.start)) {
                    var t1, t2, t3, t4, t5, t6;
                    var eventTop;
                    if (checkRequredField(timezone2)) {

                        var date = moment(dateBusy).tz(timezone2)

                        var d = date.date();
                        var m = date.month();
                        var y = date.year();

                        var datet1 = date.clone();
                        var datet2 = date.clone();
                        var datet3 = date.clone();
                        var datet4 = date.clone();
                        var datet5 = date.clone();
                        var datet6 = date.clone();

                        datet1.hours(0)
                        datet1.minutes(0)
                        datet1.seconds(0)
                        datet1.milliseconds(0)
                        datet1.year(y)
                        datet1.month(m)
                        datet1.date(d)
                        t1 = datet1.tz(timezone || getTimezone()).toDate();

                        datet2.hours(parseInt(start1Arr[0]))
                        datet2.minutes(0)
                        datet2.seconds(0)
                        datet2.milliseconds(0)
                        datet2.year(y)
                        datet2.month(m)
                        datet2.date(d)
                        t2 = datet2.tz(timezone || getTimezone()).toDate();

                        datet3.hours(parseInt(end1Arr[0]))
                        datet3.minutes(0)
                        datet3.seconds(0)
                        datet3.milliseconds(0)
                        datet3.year(y)
                        datet3.month(m)
                        datet3.date(d)
                        t3 = datet3.tz(timezone || getTimezone()).toDate();


                        datet4.hours(parseInt(start2Arr[0]))
                        datet4.minutes(0)
                        datet4.seconds(0)
                        datet4.milliseconds(0)
                        datet4.year(y)
                        datet4.month(m)
                        datet4.date(d)
                        t4 = datet4.tz(timezone || getTimezone()).toDate();


                        datet5.hours(parseInt(end2Arr[0]))
                        datet5.minutes(0)
                        datet5.seconds(0)
                        datet5.milliseconds(0)
                        datet5.year(y)
                        datet5.month(m)
                        datet5.date(d)
                        t5 = datet5.tz(timezone || getTimezone()).toDate();

                        datet6.hours(23)
                        datet6.minutes(59)
                        datet6.seconds(0)
                        datet6.milliseconds(0)
                        datet6.year(y)
                        datet6.month(m)
                        datet6.date(d)
                        t6 = datet6.tz(timezone || getTimezone()).toDate();


                    } else {
                        t1 = moment(new Date(yn, mn, dn, 0, 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate();
                        t2 = moment(new Date(yn, mn, dn, parseInt(start1Arr[0]), 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                        t3 = moment(new Date(yn, mn, dn, parseInt(end1Arr[0]), 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                        t4 = moment(new Date(yn, mn, dn, parseInt(start2Arr[0]), 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                        t5 = moment(new Date(yn, mn, dn, parseInt(end2Arr[0]), 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                        t6 = moment(new Date(yn, mn, dn, 24, 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                    }

                    eventTop = {
                        title: 'BUSY',
                        id: id,
                        start: t1,
                        end: t2,
                        color: color,
                        editable: false,
                        allDay: false
                    }

                    var eventMiddle;

                    eventMiddle = {
                        title: 'BUSY',
                        id: id,
                        start: t3,
                        end: t4,
                        color: color,
                        editable: false,
                        allDay: false
                    }

                    var eventBottom;

                    eventBottom = {
                        title: 'BUSY',
                        id: id,
                        start: t5,
                        end: t6,
                        color: color,
                        editable: false,
                        allDay: false
                    }


                    if (!moment(t1).isSame(t2)) {
                        if (checkTime(publicProfile.calendarAccess.timeOne.start) || checkTime(publicProfile.calendarAccess.timeOne.start)) {
                            eventSource.push(eventTop);
                        }
                    }
                    if (!moment(t3).isSame(t4)) {
                        if (checkTime(publicProfile.calendarAccess.timeOne.end) || checkTime(publicProfile.calendarAccess.timeTwo.start)) {
                            eventSource.push(eventMiddle);
                        }
                    }
                    if (!moment(t5).isSame(t6)) {
                        if (checkTime(publicProfile.calendarAccess.timeTwo.end) || checkTime(publicProfile.calendarAccess.timeTwo.start)) {
                            eventSource.push(eventBottom);
                        }
                    }

                } else if (checkTime(publicProfile.calendarAccess.timeOne.start) || checkTime(publicProfile.calendarAccess.timeOne.end)) {

                    var t7, t8, t9, t10;
                    if (checkRequredField(timezone2)) {

                        var date2 = moment(dateBusy).tz(timezone2)

                        var d2 = date2.date();
                        var m2 = date2.month();
                        var y2 = date2.year();

                        var datet7 = date2.clone();
                        var datet8 = date2.clone();
                        var datet9 = date2.clone();
                        var datet10 = date2.clone();

                        datet7.hours(0)
                        datet7.minutes(0)
                        datet7.seconds(0)
                        datet7.milliseconds(0)
                        datet7.year(y2)
                        datet7.month(m2)
                        datet7.date(d2)
                        t7 = datet7.tz(timezone || getTimezone()).toDate();


                        datet8.hours(parseInt(start1Arr[0]))
                        datet8.minutes(0)
                        datet8.seconds(0)
                        datet8.milliseconds(0)
                        datet8.year(y2)
                        datet8.month(m2)
                        datet8.date(d2)
                        t8 = datet8.tz(timezone || getTimezone()).toDate();

                        datet9.hours(parseInt(end1Arr[0]))
                        datet9.minutes(0)
                        datet9.seconds(0)
                        datet9.milliseconds(0)
                        datet9.year(y2)
                        datet9.month(m2)
                        datet9.date(d2)
                        t9 = datet9.tz(timezone || getTimezone()).toDate();

                        datet10.hours(23)
                        datet10.minutes(59)
                        datet10.seconds(0)
                        datet10.milliseconds(0)
                        datet10.year(y2)
                        datet10.month(m2)
                        datet10.date(d2)
                        t10 = datet10.tz(timezone || getTimezone()).toDate();


                    } else {
                        t7 = moment(new Date(yn, mn, dn, 0, 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                        t8 = moment(new Date(yn, mn, dn, parseInt(start1Arr[0]), 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                        t9 = moment(new Date(yn, mn, dn, parseInt(end1Arr[0]), 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                        t10 = moment(new Date(yn, mn, dn, 24, 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()

                    }


                    var eventTop2 = {
                        title: 'BUSY',
                        id: id,
                        start: t7,
                        end: t8,
                        color: color,
                        editable: false,
                        allDay: false
                    }
                    var eventMiddle2 = {
                        title: 'BUSY',
                        id: id,
                        start: t9,
                        end: t10,
                        color: color,
                        editable: false,
                        allDay: false
                    }
                    if (!moment(t7).isSame(t8)) {
                        eventSource.push(eventTop2);
                    }
                    if (!moment(t9).isSame(t10)) {
                        eventSource.push(eventMiddle2);
                    }
                }
            }
            calendar.fullCalendar('addEventSource', eventSource);
        }

    }

    function checkTime(time) {
        if (time == '00:00' || time == '' || time == null || time == undefined) {
            return false;
        } else {
            return true;
        }
    }

    function loadWorkHours(publicProfile,color,id) {
        if (checkRequredField(publicProfile.workHours)) {
            if (publicProfile.workHours.start == '00:00' && publicProfile.workHours.end == '00:00') {

            }
            else {
                var eventSource = [];
                var dateBusy = new Date();

                var workHoursStartArr = publicProfile.workHours.start.split(':');
                var workHoursEndArr = publicProfile.workHours.end.split(':');
                var date = new Date();
                var start = new Date(date.getFullYear(), date.getMonth(), date.getDate(), parseInt(workHoursStartArr[0]), parseInt(workHoursStartArr[1]), 0, 0);
                var end = new Date(date.getFullYear(), date.getMonth(), date.getDate(), parseInt(workHoursEndArr[0]), parseInt(workHoursEndArr[1]), 0, 0);
                if (start < end) {

                    for (var i = 0; i <= 45; i++) {
                        if (i == 0) {

                        } else dateBusy.setDate(dateBusy.getDate() + 1);

                        var d = dateBusy.getDate();
                        var m = dateBusy.getMonth();
                        var y = dateBusy.getFullYear();

                        var t1, t2, t3, t4;

                        if (checkRequredField(timezone2)) {

                            var date4 = moment(dateBusy).tz(timezone2)

                            var d3 = date4.date();
                            var m3 = date4.month();
                            var y3 = date4.year();

                            var datet1 = date4.clone();
                            var datet2 = date4.clone();
                            var datet3 = date4.clone();
                            var datet4 = date4.clone();

                            datet1.hours(0)
                            datet1.minutes(0)
                            datet1.seconds(0)
                            datet1.milliseconds(0)
                            datet1.year(y3)
                            datet1.month(m3)
                            datet1.date(d3)
                            t1 = datet1.tz(timezone || getTimezone()).toDate();

                            datet2.hours(parseInt(workHoursStartArr[0]))
                            datet2.minutes(0)
                            datet2.seconds(0)
                            datet2.milliseconds(0)
                            datet2.year(y3)
                            datet2.month(m3)
                            datet2.date(d3)
                            t2 = datet2.tz(timezone || getTimezone()).toDate();

                            datet3.hours(parseInt(workHoursEndArr[0]))
                            datet3.minutes(parseInt(workHoursEndArr[1]))
                            datet3.seconds(0)
                            datet3.milliseconds(0)
                            datet3.year(y3)
                            datet3.month(m3)
                            datet3.date(d3)
                            t3 = datet3.tz(timezone || getTimezone()).toDate();

                            datet4.hours(23)
                            datet4.minutes(59)
                            datet4.seconds(0)
                            datet4.milliseconds(0)
                            datet4.year(y3)
                            datet4.month(m3)
                            datet4.date(d3)
                            t4 = datet4.tz(timezone || getTimezone()).toDate();

                        } else {
                            t1 = moment(new Date(y, m, d, 0, 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate();
                            t2 = moment(new Date(y, m, d, parseInt(workHoursStartArr[0]), 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                            t3 = moment(new Date(y, m, d, parseInt(workHoursEndArr[0]), 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                            t4 = moment(new Date(y, m, d, 24, 0, 0, 0, 0)).tz(timezone || getTimezone()).toDate()
                        }

                        var eventTop = {
                            title: 'NON WORKING HOURS',
                            id: id,
                            start: t1,
                            end: t2,
                            color: color,
                            editable: false,
                            allDay: false
                        }
                        var eventBottom = {
                            title: 'NON WORKING HOURS',
                            id: id,
                            start: t3,
                            end: t4,
                            color: color,
                            editable: false,
                            allDay: false
                        }

                        if (publicProfile.workHours.start != '00:00' || publicProfile.workHours.end != '23:59') {
                            if (!moment(t1).isSame(t2)) {
                                eventSource.push(eventTop);
                            }
                            if (!moment(t3).isSame(t4)) {
                                eventSource.push(eventBottom);
                            }
                        }
                    }
                    calendar.fullCalendar('addEventSource', eventSource);
                }
            }
        }
    }

    $(document).on("click", ".fc-event-title", function (e) {
        if (checkRequredField($(this).attr('mUrl'))) {
            window.location.replace($(this).attr('mUrl'))
        }
    });

// Function will display Calendar
    function displayCalendar() {
        calendar = $('#calendar').fullCalendar({
            contentHeight: 400,

            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },

            defaultView: 'agendaWeek',

            selectable: true,
            selectHelper: true,
            editable: true,
            buttonIcons: true,
            handleWindowResize: true,
            viewDisplay: function getDate(date) {
                removePopover();
                $(".popover").remove();
                var currentDate = new Date();
                var minDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);
                var maxDate = new Date()
                maxDate.setDate(currentDate.getDate() + 42);

                if (date.start <= minDate) {
                    $(".fc-button-prev").css("display", "none");
                }
                else {
                    $(".fc-button-prev").css("display", "inline-block");
                }

                if (date.end >= maxDate) {
                    $(".fc-button-next").css("display", "none");
                }
                else {
                    $(".fc-button-next").css("display", "inline-block");
                }
            },
            eventAfterRender: function (event, element, view) {

                if (event.deleteBut) {
                    var html = '';
                    if(event.slot){
                        html = '<span type='+event.type+' title='+event.spanTitle.replace(/\s/g, '&nbsp;')+' self_calendar=' + event.selfCalendar + ' event_id=' + event.meetingId + ' sender=' + event.sender + ' id="slot-delete" style="z-index: 20;cursor:pointer;margin-top:' + event.marginTop + ';float:right;color: rgb(51, 51, 51);font-size: initial;">x</span>'
                        html = $(html).attr('slotId',event._id);
                    }else
                     html = '<span type='+event.type+' title='+event.spanTitle.replace(/\s/g, '&nbsp;')+' self_calendar=' + event.selfCalendar + ' event_id=' + event.meetingId + ' sender=' + event.sender + ' id="event-delete" style="z-index: 20;cursor:pointer;margin-top:' + event.marginTop + ';float:right;color: rgb(51, 51, 51);font-size: initial;">x</span>'

                    $($(element[0]).children()[0]).append(html);
                }

                if (!event.meeting) {
                    $($($(element[0]).children()[0]).children()[1]).addClass('no-cursor')
                }
                else {
                    $($($(element[0]).children()[0]).children()[1]).attr('mUrl', event.mUrl);
                }

            },
            eventClick: function (calEvent, jsEvent, view) {
               if(calEvent.deleteBut) return

                removePopover();
                $(".popover").remove();
                calendar.fullCalendar('removeEvents', '@@@');

                if (calEvent._id == "_ 1 !" || calEvent._id == "_ 2 #" || calEvent._id == "_ 3 $") {

                    var startDate = getDateFormat(calEvent.start);
                    var endDate = getDateFormat(calEvent.end);

                    var data = {
                        title: calEvent.title,
                        location: calEvent.location,
                        locType: calEvent.locationType || 'Select Location Type',
                        desc: calEvent.description,
                        startDate: startDate,
                        endDate: endDate,
                        startTime: getTimeFormat(calEvent.start),
                        endTime: getTimeFormat(calEvent.end)
                    }

                    var html = _.template($("#template-details").html())();
                    var id = "popover-source";
                    $(this).attr("id", id);

                    meetingPopup = $(this).popover({
                        title: "Request a meeting" + '<button type="button" id="close" class="close" onclick="$(\'#popover-source\').popover(&quot;destroy&quot;);">&times;</button>',
                        html: true,
                        content: html,
                        container: 'body',
                        placement: 'left',
                        id: "myPopover"
                    });
                    $(this).popover('show');

                    var tPosY = jsEvent.pageY - $('.popover').height() / 4;
                    tPosY = tPosY - 90;
                    var tPosX = jsEvent.pageX - $('.popover').height() / 4;
                    tPosX = tPosX - 250;


                    $(".popover").css("top", tPosY + "px");
                    $(".popover").css("left", tPosX + "px");
                    $(".popover-content").css({width: "320px"});
                    $(".popup-input").css({width: "320px"});
                    var delay = 100;
                    setTimeout(function () {
                        $('#MeetingTitleDetails').val(calEvent.title)
                        $('#locationDetails').val(calEvent.location)
                        $('#locationType').val(calEvent.locationType || 'Select Location Type*')
                        $('#meetingDescriptionDetails').val(calEvent.description)

                        $("#startDate").attr("value", startDate);
                        $("#endDate").attr("value", endDate);
                        $("#startTime").attr("value", getTimeFormat(calEvent.start));
                        $("#endTime").attr("value", getTimeFormat(calEvent.end));
                        if (isSelfUser()) {
                            $("#selfUser").show();
                            $('#selfEmails').val(calEvent.selfEmails || '');
                        }

                    }, delay);
                    update = true;
                    updateEventId = calEvent._id;

                    $('#Select').attr("value", 'Save');
                    $('#Select').focus();
                }
                else if (calEvent._id == 'busyTime' || contains(calEvent._id, 'busyTime')) {
                    showMessagePopUp("Please select from the Available time in White", 'tip')
                }
                else if (calEvent._id == 'mapMyCalendar') {
                    showMessagePopUp("Time is already blocked on your calendar. Please select from the Available time in White", 'tip')
                }
                else if (calEvent._id == 'past') {
                    showMessagePopUp("Please select future time to select the meeting.", 'error');
                }
            },
            eventResize: function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
                removePopover();
                $(".popover").remove();
                if (event.id == '_ 1 !' || event.id == '_ 2 #' || event.id == '_ 3 $') {
                    var now = new Date()
                    if (now >= event.start) {
                        revertFunc()
                    }
                    else if (validateBusyTime(event, event._id)) {
                        updateEvent(event, dayDelta, revertFunc);
                    } else  revertFunc()
                }

            },
            eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
                removePopover();
                $(".popover").remove();
                if (event.id == '_ 1 !' || event.id == '_ 2 #' || event.id == '_ 3 $') {
                    var now = new Date()

                    if (now >= event.start) {
                        revertFunc()

                    } else if (validateBusyTime(event, event._id)) {
                        updateEvent(event, dayDelta, revertFunc);
                    } else  revertFunc()


                }
            },
            dayClick: function (dates, allDay, jsEvent, view) {

                update = false;
                removePopover();

                $(".popover").remove();

                $('#Select').attr("value", 'Select Another Date');
                calendar.fullCalendar('removeEvents', '@@@');

                var d = dates.getDate();
                var m = dates.getMonth();
                var y = dates.getFullYear();
                var hours = dates.getHours();
                var min = dates.getMinutes();
                var hoursEnd = hours + 1;
                var minEnd = min + 30;

                var startEnd = {
                    start: new Date(y, m, d, hours, min),
                    end: new Date(y, m, d, hours, minEnd)
                }

                var check = $.fullCalendar.formatDate(dates, 'yyyy-MM-dd-HH-mm');
                var today = $.fullCalendar.formatDate(new Date(), 'yyyy-MM-dd-HH-mm');
                if (!isPageLoaded) {
                    showMessagePopUp("Bummer! Looks like the page has not yet loaded. Please wait till the complete page loads so that we can show you the correct open slots on this calendar.", 'tip');
                } else if (check < today) {
                    showMessagePopUp("Please select future time to select the meeting.", 'error');
                }
                else if (validateBusyTime(startEnd, 'o')) {

                    date = dates;

                    var html = _.template($("#template-details").html())();

                    var id = "popover-source";

                    $(this).attr("id", id);
                    var dayOfWeek = date.getDay()

                    meetingPopup = $(this).popover({
                        title: "Request a meeting" + '<button type="button" id="close" class="close" >&times;</button>',
                        html: true,
                        content: html,
                        container: 'body',
                        viewport: 'body',
                        placement: dayOfWeek == 0 ? 'right' : dayOfWeek == 1 ? 'right' : 'left',
                        id: "myPopover"
                    });

                    meetingPopup.on('hide.bs.popover', function () {
                        $("#popover-source").removeAttr("id");
                        calendar.fullCalendar('removeEvents', '@@@');
                    });

                    var delay = 100;
                    setTimeout(function () {
                        $("#popup-c-popup").show();
                        $('#MeetingTitleDetails').val(lTitle);
                        $('#locationDetails').val(lLocation);
                        $('#locationType').val(lLocationType || 'Select Location Type*');
                        $('#meetingDescriptionDetails').val(lDescription);

                        if (isSelfUser()) {
                            $("#selfUser").show();
                            $('#selfEmails').val(selfEmails || '');
                        }
                    }, delay);


                    $(this).popover('show');

                    var tPosY = jsEvent.pageY - $('.popover').height() / 4;
                    tPosY = tPosY - 50

                    $(".popover").css("top", tPosY + "px");
                    $(".popover").css({width: "276px"});

                    $('.date').css({'display': 'none'})

                    $('#Select').focus();
                    var d = date.getDate();
                    var m = date.getMonth();
                    var y = date.getFullYear();
                    var hours = date.getHours();
                    var min = date.getMinutes();
                    var hoursEnd = hours + 1;
                    var minEnd = min + 30;
                    var start = new Date(y, m, d, hours, min);
                    var end = new Date(y, m, d, hours, minEnd);

                    var details = {
                        start: start,
                        end: end
                    }

                    calendar.fullCalendar('renderEvent',
                        {
                            id: '@@@',
                            start: start,
                            end: end,
                            allDay: false

                        },
                        true // make the event "stick"
                    );

                    $.ajax({
                        url: '/getTimeSlots',
                        type: 'GET',
                        datatype: 'JSON',
                        success: function (result) {
                            if (result.length > 2) {

                                removePopover();
                                calendar.fullCalendar('removeEvents', '@@@');
                                showMessagePopUp("Maximum 3 time slots can be selected for a meeting.", 'error')

                            }
                            else if (result.length > 1) {
                                $('#Select').attr("value", "Send Invite");
                                toSendInvite = true;
                            }
                        },
                        error: function (event, request, settings) {

                        }
                    })
                }
                else {
                    showMessagePopUp("Looks like the time you selected is BUSY. Please select time from available times in WHITE", 'error')

                }
            }
        });
        $("#requestMail").css({display: 'none'});

        loadUserPendingInvitations(false, null);
        loadUserBusyTimes(publicProfile,'#808080','busyTime');
        loadWorkHours(publicProfile,'#808080','busyTime');
        loadUserPublicProfileCalendarEvents();
        onChangeMapMyCalendar();
        getTimeSlots();
        getFileName();
        display();
    }

    $("#mapMyCalendar").on("change", function () {
        if ($('#mapMyCalendar').prop('checked')) {
            exeFlag = true;
            onChangeMapMyCalendar();
        }
        else {
            calendar.fullCalendar('removeEvents', 'mapMyCalendar');
            calendar.fullCalendar('removeEvents', 'not confirmmapMyCalendar');
        }
    });

    $("body").on("click", "#popup-close-popup", function () {
        toSendInvite = false;
        analysisFlag = true;
        onClickSelectAnotherDate(true);
        calendar.fullCalendar('removeEvents', '@@@');
    });


    var mapMyCalendarFlag = true;
    var exeFlag = true;

    function onChangeMapMyCalendar() {
        if (exeFlag) {
            exeFlag = false;
            $.ajax({
                url: '/isLoggedInUser',
                type: 'GET',
                datatype: 'JSON',
                traditional: true,
                success: function (result) {
                    if (result) {
                        setTimeout(function () {
                            mapMyCalendarFlag = false;
                            if (checkRequredField(calendar))
                                calendar.fullCalendar('removeEvents', 'mapMyCalendar');

                            $('#mapMyCalendar').attr("checked", true);
                            mapMyCalendar()
                        }, 100)

                    }
                    else {
                        if (mapMyCalendarFlag) {
                            mapMyCalendarFlag = false;
                            $('#mapMyCalendar').attr("checked", false);

                            if (checkRequredField(calendar))
                                calendar.fullCalendar('removeEvents', 'mapMyCalendar');
                        }
                        else {
                            mapMyCalendarFlag = false;
                            showMessagePopUp("This feature is available to the Logged in Relatas user. Please login to Relatas to 'MapYourCalendar' and check your availability against the other user.", 'error')
                            $('#mapMyCalendar').attr("checked", false);

                            if (checkRequredField(calendar))
                                calendar.fullCalendar('removeEvents', 'mapMyCalendar');
                        }
                    }
                },
                timeout: 20000
            })
        }
    }

    function mapMyCalendar() {
        if (!isSelfUser()) {
            loadUserPendingInvitations(true, null);
            calendar.fullCalendar('removeEvents', 'mapMyCalendar');
            loadUserBusyTimes(authUserProfile,'#82CAFA','mapMyCalendar');
            loadWorkHours(authUserProfile,'#82CAFA','mapMyCalendar');
            $.ajax({
                url: '/calendarEvents/mapMyCalendar',
                type: 'GET',
                datatype: 'JSON',
                traditional: true,
                success: function (events) {
                    if (events == false) {

                    }
                    else {
                        var eventSource = [];
                        for (var i in events) {

                            var localStart = new Date(events[i].start.dateTime || events[i].start.date).toISOString();
                            var localEnd = new Date(events[i].end.dateTime || events[i].end.date).toISOString();

                            var obj = validateGoogleMeetings(new Date(localStart), new Date(localEnd));
                            if (obj.create) {
                                var event = {
                                    title: events[i].summary || 'BUSY',
                                    id: 'mapMyCalendar',
                                    start: obj.start,
                                    end: obj.end,
                                    editable: false,
                                    color: '#82CAFA',
                                    textColor: '#000000',
                                    allDay: events[i].start.dateTime ? false : true
                                }

                                if(events[i].id.substr(0, 12) == 'relatasevent'){

                                    event.deleteBut = true;
                                    event.spanTitle = 'Delete Event?';
                                    event.type = 'event';
                                    event.marginTop = event.title.length > 7 ? '-40%' : '-20%';
                                    event.meetingId = events[i].id.substr(12, events[i].id.length)
                                }
                                eventSource.push(event)
                            }

                        }

                        calendar.fullCalendar('addEventSource', eventSource);

                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            });
        }
        else {
            $("#mapCal").hide();
            calendar.fullCalendar('removeEvents', 'mapMyCalendar');
        }
    }


    function validateBusyTime(date, id) {
        var t = true;
        calendar.fullCalendar('clientEvents', function (event) {

            if (event._id != id) {
                if (date.start <= event.start) {
                    if (date.end <= event.start) {

                    } else t = false;
                }
                else {
                    if (date.start >= event.end) {

                    } else t = false;
                }
            }

        });
        return t;
    }

    function showMessagePopUp(message, msgClass) {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        // $(".popover").css({'margin-top':'25%'})

        if (msgClass == 'error') {
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        } else if (msgClass == 'success') {
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        } else if (msgClass == 'tip') {
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message)
        $("#closePopup-message").focus();

    }

    $("body").on("click", "#closePopup-message", function () {
        $("#user-name").popover('destroy');

        if (aa) {
            removeSession()
        } else if (newU) {
            aa = false;
            removeSession()
        }

    });

    function isRegisteredUser(){
        if(checkRequredField(authUserProfile)){
            return !authUserProfile.registeredUser
        }
        else return false;
    }

    function removeSession() {
        $.ajax({
            url: '/clearTimeSlots',
            type: 'GET',
            success: function (response) {
                console.log(isRegisteredUser())
                if(isRegisteredUser()){
                    aa = false;
                    newU = true;
                }
                if (aa) {
                    aa = false;
                    inFormOrLink = false;
                    window.location.reload();
                } else if (newU) {
                    inFormOrLink = false;
                    window.location.replace('/editProfile');
                }
            },
            timeout: 20000
        })
    }

    function validateEmailField(email) {
        if (checkRequredField(email)) {
            var emailText = email.trim();

            var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
            if (pattern.test(emailText)) {

                return true;
            }
            else {

                return false;
            }
        } else return false;

    }


    function checkRequredField(data) {
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else {
            return true;
        }
    };
    sidebarHeight()
    function sidebarHeight() {
        var sideHeight = $(document).height()
        $("#fb-tab-content").css({'min-height': sideHeight - 150})
        $("#twitter-tab-content").css({'min-height': sideHeight - 150})
        $("#linkedin-tab-content").css({'min-height': sideHeight - 150})
        $("#relatas-tab-content").css({'min-height': sideHeight - 150})

    }

    $("body").on("click", "#popup-c-popup", function () {
        removePopover();
    })

    $('a').on('click', function () {
        inFormOrLink = false;
    });
    $('form').bind('submit', function () {
        inFormOrLink = false;
    });

    $(window).on("beforeunload", function () {
        if (inFormOrLink)
            return "Your meeting invitation has not been sent. Please click on SEND INVITE button on bottom right to send your invitation.";

    })

    function hideSendInviteButton(flag) {
        if (flag) {
            $("#sendInvitationBut").hide();
        } else
            $("#sendInvitationBut").show();
    }

    function showHideSelectAnotherTimeButton(text, response) {
        if (checkRequredField(text) && (contains(text, ',') || contains(text, ';'))) {
            $("#Select").hide();
            $("#Select-OR").hide();
            if (response) return false
        } else {
            $("#Select").show();
            $("#Select-OR").show();
            if (response) return true
        }
    }

    $("body").bind("keyup", "#Select", function (event) {
        var emailsS = $("#selfEmails").val();

        showHideSelectAnotherTimeButton(emailsS, false);
    });

    function mixpanelTrack(eventName) {
        mixpanel.track(eventName, {page: window.location.href});
    }

    function mixpanelIncrement(eventName) {
        mixpanel.people.increment(eventName);
    }

    /*******************************************************************************/

    $('#userFirstName').change(function (event) {
        var text = $('#userFirstName').val().toLowerCase()
        var text2 = $('#userLastName').val().toLowerCase()
        $("#userPublicProfileUrl").val(getValidRelatasIdentity(text + '' + text2))
    })
    $('#userLastName').change(function (event) {
        var text = $('#userFirstName').val().toLowerCase()
        var text2 = $('#userLastName').val().toLowerCase()
        $("#userPublicProfileUrl").val(getValidRelatasIdentity(text + '' + text2))
    })

    $("#userPublicProfileUrl").focusout(function () {
        var text = $("#userPublicProfileUrl").val()
        checkIdentityAvailability(text)
    });
    var flagi = false;

    function checkIdentityAvailability(identity) {

        var details = {
            publicProfileUrl: getValidRelatasIdentity(identity)
        }

        $.ajax({
            url: '/checkUrl',
            type: 'POST',
            datatype: 'JSON',
            data: details,
            traditional: true,
            success: function (result) {
                if (result == true) {
                    $("#userPublicProfileUrl").val(getValidRelatasIdentity(identity))
                    identityFlag = 1;
                    if (flagi) {
                        flagi = false;
                        showMessagePopUp("Your selected name is not available. Next best name suggested is:  " + identity + "", 'tip')
                    }
                }
                else {

                    a(identity)

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }


    //On click event on saveDetailsButton
    $('#saveDetailsButton').on("click", function () {
        var data = formData()
        onSaveFormData(data)

    });

    function formData() {
        var mail = $('#userEmailId').val();

        var data = {
            'firstName': $('#userFirstName').val() || '',
            'lastName': $('#userLastName').val() || '',
            'publicProfileUrl': $('#userPublicProfileUrl').val() || '',
            'emailId': mail.toLowerCase() || '',
            'profilePicUrl': '/images/default.png',
            'password': $('#userPassword').val() || ''

        };

        return data;
    }


    function onSaveFormData(data) {
        var result = validate(data);
        if (result == true) {

            data.publicProfileUrl = getValidRelatasIdentity(data.publicProfileUrl)

            var details = {
                publicProfileUrl: data.publicProfileUrl
            }
            $.ajax({
                url: '/checkUrl',
                type: 'POST',
                datatype: 'JSON',
                data: details,
                traditional: true,
                success: function (result) {
                    if (result == true) {
                        checkEmailAddress(data)
                    }
                    else {
                        showMessagePopUp("Public profile url already exist in the Relatas, Please change it", 'error')

                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 30000
            })

        } else {
            result.focus();
        }
    }


    function checkEmailAddress(data) {

        $.ajax({
            url: '/checkEmail/' + data.emailId,
            type: 'GET',

            traditional: true,
            success: function (result) {
                if (result == true) {
                    data = createNewUser(data)

                    if ($('#acceptTerms').prop('checked')) {
                        saveFormData(data);

                    }
                    else {
                        $('#acceptTerms').focus();
                        showMessagePopUp("Please accept terms and conditions", 'error')
                    }
                }
                else {
                    showMessagePopUp("Email address already exist in the Relatas, Please change it", 'error')

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })

    }

    function createNewUser(data) {
        uniqueId = data.publicProfileUrl;
        var user = {
            'firstName': data.firstName,
            'lastName': data.lastName,
            'publicProfileUrl': data.publicProfileUrl,
            'screenName': data.publicProfileUrl,
            'emailId': data.emailId.toLowerCase(),
            'password': data.password,
            'registeredUser': false,
            'profilePicUrl': data.profilePicUrl || '/images/default.png',
            'serviceLogin': 'relatas',
            'public': true,
            'meetingProcess': true
        };

        user.google = googleString()
        user.linkedin = linkedinString()
        user.facebook = facebookString()
        user.twitter = twitterString()

        return user;

    }


// Function to store form data in session
    function saveFormData(data) {

        $.ajax({
            url: '/registerAccount',
            type: 'POST',
            datatype: 'JSON',
            traditional: true,
            data: data,
            success: function (msg) {
                if (msg == true) {
                    signInFlagNew = true;
                    $('#lightBoxClose').trigger('click');
                    $("#btmMsg").text('Your message has NOT been sent yet. Please click on "Send Invite" below to send the invitation.');
                    loadUserPublicProfile(true)
                    isLoggedInUser()
                    exeFlag = true;
                    mapMyCalendarFlag = true;
                    createFlag = true;
                    analysisFlag = true;
                    $("#sendInvitationBut").trigger("click");
                    onChangeMapMyCalendar()
                }
                else {
                    showMessagePopUp("Error occurred while saving, Please try again", 'error')

                }

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        });
    }

    var identityFlag = 1;

    function a(identity) {
        identity = getValidRelatasIdentity(identity) + '' + identityFlag
        identityFlag++;
        flagi = true;
        checkIdentityAvailability(identity)
    }

    function getValidRelatasIdentity(rIdentity) {
        rIdentity = rIdentity.toLowerCase();
        rIdentity = rIdentity.replace(/\s/g, '');
        return rIdentity;

    }


    function validateRelatasIdentity(rIdentity) {

        var regex = new RegExp("^[a-zA-Z0-9._ ]*$");
        if (regex.test(rIdentity) && rIdentity != '') {
            return true;

        }
        else {

            return false;
        }
    }

    //Functions to validate required feilds
    function validate(data) {
        if (!checkRequredField(data.firstName.trim())) {
            showMessagePopUp("Please provide First name", 'error')

            return $('#userFirstName');
        } else if (!checkRequredField(data.lastName.trim())) {
            showMessagePopUp("Please provide Last name", 'error')

            return $('#userLastName');
        } else if (!validateRelatasIdentity(data.publicProfileUrl.trim())) {
            showMessagePopUp("Please provide valid Unique Relatas identity", 'error')

            return $('#userPublicProfileUrl');
        } else if (!checkRequredField(data.emailId.trim())) {
            showMessagePopUp("Please provide Email id", 'error')

            return $('#userEmailId');

        } else if (!checkRequredField(data.password.trim())) {
            showMessagePopUp("Please provide password", 'error')

            return $('#userPassword');

        } else if (!validateEmailField(data.emailId)) {
            showMessagePopUp("Please provide valid email id", 'error')

            return $('#userEmailId');
        }
        else {
            return true;
        }
    }

    // Function to generate valid json string for goole account
    function googleString() {

        var google = '';

        google += '{ "id":"",';
        google += '"token":"",';
        google += '"refreshToken":"",';
        google += '"name":""}';

        var googleNew = "[]";
        return googleNew;
    }

    // Function to generate valid json string for linkedin account
    function linkedinString() {
        var linkedin = '{';

        linkedin += '"id":"",';
        linkedin += '"token":"",';
        linkedin += '"emailId":"",';
        linkedin += '"name":""';

        linkedin += '}';
        return linkedin;
    }

// Function to generate valid json string for facebook account
    function facebookString() {
        var facebook = '{';

        facebook += '"id":"",';
        facebook += '"token":"",';
        facebook += '"emailId":"",';
        facebook += '"name":""';

        facebook += '}';
        return facebook;
    }

    // Function to generate valid json string for twitter account
    function twitterString() {
        var twitter = '{';

        twitter += '"id":"",';
        twitter += '"token":"",';
        twitter += '"refreshToken":"",';
        twitter += '"displayName":"",';
        twitter += '"userName":""';

        twitter += '}';
        return twitter;
    }

    /***************************************************************************************/

    /* Cancel and delete meeting */
    function closePopOver() {
        if (popoverContent != null) {
            popoverContent.popover('destroy');
            popoverContent = null;
        }
    }

    $(document).on("click", "#event-delete", function (e) {

        var eventIdIn = $(this).attr('event_id');
        var sender = $(this).attr('sender');
        var selfCalendar = $(this).attr("self_calendar");
        closePopOver();
        if($(this).attr('type') == 'event'){
            deleteEvent($(this), eventIdIn)
        }else
        if (sender == true || sender == 'true') {
            deleteMeeting($(this), eventIdIn, sender, selfCalendar)
        }
        else {
            cancelMeeting($(this), eventIdIn, sender, selfCalendar)
        }
    });

    function deleteMeeting(element, eventIdIn, sender, selfCalendar) {
        var html = _.template($("#delete-meeting-popup").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });
        popoverContent = element;
        element.popover('show');
        $(".popover").css({'width': '20%'});
        $("#delete-meeting-but").attr("event_id_delete", eventIdIn);
    }

    function cancelMeeting(element, eventIdIn, sender, selfCalendar) {
        element = $(element[0]);
        var html = _.template($("#cancel-meeting-popup").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });

        popoverContent = element;
        element.popover('show');

        $(".popover").css({'width': '40%'})
        $("#cancel-meeting-but").attr("event_id_cancel", eventIdIn);
        $("#cancel-meeting-but").attr("self_calendar", selfCalendar);
    }

    function deleteEvent(element, eventIdIn) {
        var html = _.template($("#delete-event-popup").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });
        popoverContent = element;
        element.popover('show');
        $(".popover").css({'width': '20%'});
        $("#delete-event-but").attr("event_id_delete", eventIdIn);
    }

    $("body").on("click", "#delete-event-but", function (e) {
        var id = $(this).attr("event_id_delete");
        closePopOver();
        $.ajax({
            url:'/event/removeFromCalendar/'+id,
            type:'GET',
            datatype:'JSON',
            success:function(response){

                if(response){
                    calendar.fullCalendar('removeEvents', id);
                    showMessagePopUp("Event has been successfully deleted from your calendar.",'success');
                }
                else{
                    showMessagePopUp("An error occurred while removing event. Please try again.",'error');
                }
            }
        })
    });

    $("body").on("click", "#delete-meeting-but", function (e) {
        var id = $(this).attr("event_id_delete");
        closePopOver();
        $.ajax({
            url: "/meetings/action/delete?mId=" + id+"&timezone="+getUserTimezone(),
            type: "GET",
            success: function (response) {
                closePopOver();
                if (response) {
                    showMessagePopUp("Meeting successfully deleted. Message sent to all participants.", 'success');
                    calendar.fullCalendar('removeEvents', id);
                }
                else {
                    showMessagePopUp("An error occurred. Please try again", 'error');
                }
            }
        })
    });

    $("body").on("click", "#cancel-meeting-but", function (e) {
        var obj = {
            invitationId: $(this).attr("event_id_cancel"),
            message: $("#cancel-meeting-message").val(),
            selfCalendar: $(this).attr("self_calendar"),
            timezone:getUserTimezone()
        };

        closePopOver();
        $.ajax({
            url: "/meetings/action/cancel",
            type: "POST",
            datatype: "JSON",
            data: obj,
            success: function (response) {
                closePopOver();
                if (response) {
                    calendar.fullCalendar('removeEvents', obj.invitationId);
                    showMessagePopUp("Your cancellation of meeting success.", 'success');
                }
                else {
                    showMessagePopUp("An error occurred. Please try again", 'error');
                }
            }
        })
    });

    $("body").on("click", "#closePopup-delete-meeting", function (e) {
        closePopOver();
    });

    $("body").on("click", "#closePopup-delete-event", function (e) {
        closePopOver();
    });

    $("body").on("click", "#closePopup-cancel-meeting", function (e) {
        closePopOver();
    });

    function getUserTimezone(){
        return loggedInUserTimezone || timezone || 'UTC';
    }
});