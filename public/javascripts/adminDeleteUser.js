$(document).ready(function(){
     var userP;
    $("#getUserInfo").on('click',function(){
        var emailId = $("#emailId").val();
        if(!checkRequired(emailId)){
            showMessagePopUp("Please enter email id.",'error','Invalid Email Id');
        }
        else if(validateEmailField(emailId)){
            $.ajax({
                url:'/admin/user/'+emailId+'/get/admin',
                type:'GET',
                success:function(response){
                    userP = response.user;
                    if(response == 'no-user'){
                        showMessagePopUp("No user found",'error','No User')
                    }else
                    if(checkRequired(response) && checkRequired(response.user) && response != false && checkRequired(response.user.firstName)){
                        $("#deleteUser").attr('userId',response.user._id);
                        $("#deleteUser").attr('name',response.user.firstName);
                        $("#user-name").text(response.user.firstName+' '+response.user.lastName);
                        $("#user-email-id").text(response.user.emailId);
                        $("#user-unique-id").text(response.user.publicProfileUrl);
                        $("#user-created-on").text(moment(response.user.createdDate).format('DD-MMM-YYYY'));
                        $("#user-meetings").text(response.meetingsCount || 0);
                        $("#user-documents").text(response.documentsCount || 0);
                        $("#user-events").text(response.eventsCount || 0);
                        $("#user-info-table").show();
                    }
                    else showMessagePopUp('An error occurred please try again.','error','Failed To Get User');
                }
            })
        }
        else{
            showMessagePopUp("Please enter valid email id.",'error','Invalid Email Id');
        }
    });

    $("#deleteUser").on('click', function () {
        if (!userP.corporateUser) {

            var userId = $(this).attr('userId');
            if (checkRequired(userId)) {
                $("confirm").attr('userId', userId);
                var html = _.template($("#confirm-popup").html())();
                $(this).popover({
                    title: "Delete User",
                    html: true,
                    content: html,
                    container: 'body',
                    placement: 'bottom',
                    id: "myPopover"
                });

                $(this).popover('show');
                $(".arrow").addClass("invisible");
                // $(".popover").style({'max-width':'25%'});
                $("#deleteMsg").text('Do you want to delete user "' + $(this).attr('name') + '" ')
            }
            else showMessagePopUp("Please select user.", 'error', 'No User Info');

        }
        else showMessagePopUp(userP.firstName+" is Corporate User.", 'error', 'Deletion error');
    });

    function deleteUser(){
       $("#user-info-table").hide();
        $.ajax({
            url:'/admin/user/delete/permanently',
            type:'POST',
            datatype:'JSON',
            data:{
                userId:userP._id,
                emailId:userP.emailId,
                firstName:userP.firstName,
                uniqueName:userP.publicProfileUrl
            },
            success:function(status){

                if(checkRequired(status)){
                    if(status.status == 'access-denied'){
                        showMessagePopUp("Un authorised Access",'error','Authorization Error');
                    }
                    else if(status.status == 'no-id'){
                        showMessagePopUp("Please select user.", 'error', 'No User Info');
                    }
                    else if(status.status == true){
                        showMessagePopUp("User successfully deleted.",'success','Success Message');
                    }
                    else showMessagePopUp("An error occurred. Please try again.", 'error', 'Unknown Error');
                }
                else showMessagePopUp("An error occurred. Please try again.", 'error', 'Unknown Error');
            }
        })
    }

    /* MESSAGE POPUP SCRIPT */
    function showMessagePopUp(message, msgClass, popupTitle) {

        var html = _.template($("#message-popup").html())();
        $("#getUserInfo").popover({
            title: popupTitle+ '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });

        $("#getUserInfo").popover('show');
        $(".arrow").addClass("invisible");
        if (msgClass == 'error') {
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        } else if (msgClass == 'success') {
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        } else if (msgClass == 'tip') {
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message);
        $("#closePopup-message").focus();

    }

    $("body").on("click", "#close", function () {
        $("#getUserInfo").popover('destroy');
    });

    $("body").on("click", "#confirm", function () {
        $("#deleteUser").popover('destroy');
        deleteUser();
    });

    $("body").on("click", "#cancelDeletion", function () {
        $("#deleteUser").popover('destroy');
    });

    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {
            return true;
        } else {

            return false;
        }
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }
});