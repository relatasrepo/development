var relatasApp = angular.module('relatasApp', ['angular-loading-bar', 'ngSanitize']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.service('share', function() {
    return {
        setTargetForFy:function(value){
            this.targetForFy = value;
        }
    }
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }

        else toastr.error("Please enter search content")
    };
});

relatasApp.controller("logedinUser", function($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {

        $rootScope.liuEmailId = response.Data.emailId; 
        share.primaryCurrency = "USD";
        share.currenciesObj = {};

        $rootScope.opportunityStagesFilter = [{name:"All"}]
        $rootScope.opportunityStagesFilter = $rootScope.opportunityStagesFilter.concat(share.opportunityStages)
        share.companyDetails.currency.forEach(function (el) {
            share.currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                share.primaryCurrency = el.symbol;
            }
        });

        if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
            share.timezone = response.Data.timezone.name;
        }

        $rootScope.primaryCurrency = share.primaryCurrency;

        setTimeOutCallback(100,function () {
            share.populateFilters(share.companyDetails);
            share.currentFy = getCurrentFiscalYear("UTC",response.companyDetails.fyMonth);
            share.setCurrentQuarter(share.currentFy)
            share.initialiseAccFilters(share.currentFy)
        })
    })
});

relatasApp.controller("liu_hierarchy", function($scope, $http, share,$rootScope) {

    $rootScope.viewForSelected = "dashboard" //Default;
    $scope.openMenu = function(){
        $(".reports .left-panel").toggle();
    }

    $scope.dataAsOf = "Data as of "+moment().subtract(1,"day").format(standardDateFormat());

    $scope.isTop100 = true;
    share.toggleFySelection = function(by){
        $scope.isTop100 = by === 'opp'

        if($scope.isTop100){
            share.initialiseAccFilters(share.currentFy);
        }
    }

    $http.get('/company/user/all/hierarchy/types')
        .success(function (response) {
            if(response && response.SuccessCode){
                $scope.hierarchyList = [];

                _.each(response.Data,function (el) {
                    $scope.hierarchyList.push({
                        name:el
                    })
                });

                $scope.selectedHierarchy = $scope.hierarchyList[0];
            }
        })

    share.initialiseAccFilters = function(data){
        var startOfQuarter = data.quarter.obj[data.quarter.currentQuarter].start,
            endOfQuarter = data.quarter.obj[data.quarter.currentQuarter].end;

        $scope.start = {
            month: moment(startOfQuarter).format("MMM"),
            year: moment(startOfQuarter).format("YYYY")
        }
        $scope.end = {
            month: moment(endOfQuarter).format("MMM"),
            year: moment(endOfQuarter).format("YYYY")
        }

        $scope.oppRange = "Greater than";
        $scope.oppValue = 0;
        $scope.intRange = "Greater than";
        $scope.intValue = 0;
    }

    $scope.toggleQtrList = function(){
        $scope.ifOpenList = !$scope.ifOpenList
    }

    $scope.dataForQuarter = function(qtr){
        share.qtrFilterSelected = qtr
        $scope.qtrFilterSelected = qtr.display
        $scope.ifOpenList = !$scope.ifOpenList

        var emailIds = $scope.selection && $scope.selection.emailId?$scope.selection.emailId:null;
        if($scope.selection.emailId == "Show all team members"){
            emailIds = "all"
        }

        share.getDashBoardInsights(emailIds,qtr)
    }

    $scope.months = monthsAndYear().months;
    $scope.years = monthsAndYear().years;

    share.resetAccountFilters = function(){
        $scope.oppRange = null;
        $scope.oppValue = null;
        $scope.intRange = null;
        $scope.intValue = null;

        $scope.start = {
            year:null,
            month:null
        }

        $scope.end = {
            year:null,
            month:null
        }

        $rootScope.opportunityStagesFilter = [{name:"All"}]
        $rootScope.opportunityStagesFilter = $rootScope.opportunityStagesFilter.concat(share.opportunityStages)
        $scope.selectedStage = $rootScope.opportunityStagesFilter[0];
    }

    $scope.filterAccs = function(){

        var oppRange = {
            range: $scope.oppRange,
            value: $scope.oppValue
        }

        var intRange = {
            range: $scope.intRange,
            value: $scope.intValue
        }

        if($scope.start && $scope.start.month && $scope.start.year){
            var start = new Date(moment().year(parseInt($scope.start.year)).month(parseInt(moment().month($scope.start.month).format("M")-1)).startOf('month'))
        }

        if($scope.end && $scope.end.year && $scope.end.month){
            var end = new Date(moment().year(parseInt($scope.end.year)).month(parseInt(moment().month($scope.end.month).format("M")-1)));
        }

        share.filterAccounts($scope.selectedStage,oppRange,intRange,start,end)
    }

    getFilterDates(share,$scope);

    share.getDataForLiu = function() {
        var liuUser = _.filter($scope.team, function(user) {
            return (user.emailId === $rootScope.liuEmailId);
        })
        $scope.getDataFor(liuUser[0] ? liuUser[0] : "all");
    }

    share.getDataFor = function(member) {
        $scope.getDataFor(member);
    }

    $scope.getDataFor = function (member) {

        $scope.selection = member && member != "all"?member:{fullName:"Show all team members",emailId:"Show all team members",nameNoImg:"All",noPicFlag:true};
        share.selection = $scope.selection;

        if($rootScope.viewForSelected == "region"){
            share.regionChart($scope.selection.fullName == "Show all team members");
        }

        if($rootScope.viewForSelected == "dashboard" || !$rootScope.viewForSelected){
            if($scope.selection.fullName == "Show all team members"){
                share.getDashBoardInsights("all",share.qtrFilterSelected);
            } else {
                share.getDashBoardInsights(member.emailId?member.emailId:null,share.qtrFilterSelected);
            }
        }

        share.resetPrevfilters();
        share.rangeType = null // Reset range type
        $scope.selectFromList = !$scope.selectFromList
        share.setAllTeamMembers($scope.selection.fullName == "Show all team members");
        share.setLoaders();
        share.setLoaders2();

        if($rootScope.viewForSelected == "opportunity"){
            if($scope.selection.fullName !== "Show all team members"){
                share.drawOppsTable($scope.selection.emailId)
            } else {
                share.drawOppsTable()
            }
        };

        if($rootScope.viewForSelected == "accounts"){
            $scope.selectedStage = $rootScope.opportunityStagesFilter[0];
            if($scope.selection.fullName == "Show all team members"){
                share.getAccountInsights("all",share.qtrFilterSelected);
            } else {
                share.getAccountInsights(member.emailId?member.emailId:null,share.qtrFilterSelected);
            }

            $(document).ready(function() {
                function checkLoaded(){
                    if($('#spiderChart3').width()){
                        $("#spiderChart3 svg").attr("width",$('#spiderChart3').width());
                    } else {
                        setTimeOutCallback(1000,function () {
                            checkLoaded();
                        })
                    }
                }

                checkLoaded();
            });
        };
    }

    closeAllDropDownsAndModals($scope,".list");
    closeAllDropDownsAndModals($scope,".forClosing");

    $scope.getLiuHierarchy = function (hierarchy) {

        var url = '/company/user/hierarchy/insights';

        if(hierarchy && hierarchy !== "Org. Hierarchy"){
            url = '/company/users/for/hierarchy';
            url = fetchUrlWithParameter(url,"hierarchyType",hierarchy.replace(/[^A-Z0-9]+/ig, "_"))
        }

        share.selectedHierarchy = hierarchy;

        $http.get(url)
            .success(function (response) {
                $scope.team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.team = buildTeamProfiles(response.Data)

                    if($scope.team.length>1){
                        $scope.selection = {fullName:"Show all team members",emailId:"Show all team members",nameNoImg:"All",noPicFlag:true};
                    } else {
                        $scope.selection = $scope.team[0];
                    }

                    // $scope.selection = $scope.team[0];

                    share.selection = $scope.selection

                    share.team = $scope.team;
                    var usersDictionary = {};

                    share.teamChildren = {};
                    _.each(response.listOfMembers,function (el) {
                        share.teamChildren[el.userEmailId] = el.teamMatesEmailId
                    });

                    if(response.companyMembers.length>0){
                        var companyMembers = buildAllTeamProfiles(response.companyMembers)

                        _.each(companyMembers,function (member) {
                            usersDictionary[member.emailId] = member
                        });

                        share.teamMembers = companyMembers;
                    }

                    checkDashboardTemplateLoaded();

                    function checkDashboardTemplateLoaded(){
                        if(share.getDashBoardInsights){

                            if($rootScope.viewForSelected == "dashboard"){
                                var panel = $scope.team.length>1?1:0;
                                var all = $scope.team.length>1?'all':null;

                                share.loadRightPanel(panel)

                                if(!panel){
                                    share.openViewFor({
                                        name:"Today",
                                        selected:"selected"
                                    })
                                } else {
                                    share.getDashBoardInsights(all);
                                }

                            } else if($rootScope.viewForSelected == "opportunity") {
                                share.drawOppsTable();
                            }else if($rootScope.viewForSelected == "accounts") {
                                share.getAccountInsights("all",share.qtrFilterSelected);
                            }

                            // share.loadRightPanel(5)
                            // share.getDashBoardInsights();
                        } else {
                            setTimeOutCallback(500,function () {
                                checkDashboardTemplateLoaded();
                            });
                        }
                    }

                    share.usersDictionary = usersDictionary;
                }
            });
    }

    $scope.getLiuHierarchy("All Access");

    share.resetUserSelection = function(noReset){
        $scope.selection = $scope.team[0];
        share.teamData = $scope.team;
        $scope.getDataFor(noReset?"all":$scope.selection)
        $scope.selectFromList = false;
    }

});

relatasApp.controller("wrapper_controller", function($scope, $http, share,$rootScope) {

    share.loadRightPanel = function(index){
        $scope.menu = menuItems(true,index);
        $scope.selectedTab = $scope.menu[index];
        $scope.viewFor = $scope.menu[index].name.toLowerCase();
        $rootScope.viewForSelected = $scope.viewFor;

        $scope.showLiu = $rootScope.viewForSelected == "today" ? false : true;
    }

    check_right_data_panel_loaded();

    function check_right_data_panel_loaded(){
        if(share.viewFor){
            share.viewFor($scope.viewFor);
        } else {
            setTimeOutCallback(100,function () {
                share.viewFor($scope.viewFor);
            })
        }
    }

    $scope.openViewFor = function (viewFor,redirectFrom) {
        handleSideBarSelections($scope,$rootScope,share,viewFor,redirectFrom);
    };

    share.openViewFor = function(viewFor, redirectFrom){
        $scope.openViewFor(viewFor,redirectFrom)
    }

    share.viewFor = function (viewFor) {
        $scope.viewFor = viewFor;
    }

})

relatasApp.controller("exceptionalAccess", function($scope,$http,share,$rootScope){

});

relatasApp.controller("opportunities", function($scope,$http,share,$rootScope,searchService){

    $scope.loadingMetaData = true;

    share.setLoaders = function(){
        $scope.loadingMetaData = true;
    }

    $scope.getDetails = function (colType) {
        if(colType.colType == "Deals At Risk"){

            if(share.selection && share.selection.emailId == "Show all team members"){
                alert("Deals at risk insights not available for team. Please select individual team members to view deals at risk");
            } else {
                var userId = share.selection?share.selection.userId:share.liuData.userId;
                share.forDealsAtRisk(userId);
            }
        }
    }

    $scope.selectAll = function (colType) {
        _.each(colType.values,function (el) {
            el.selected = colType.selectingAll;
            $scope.selectFilter(colType.type,el)
        })
    }

    share.filterOpps = function (item,fromDashboard) {
        $scope.filterOpps(item,fromDashboard)
    }

    share.setCurrentQuarter = function (data) {

        var startOfQuarter = data.quarter.obj[data.quarter.currentQuarter].start,
            endOfQuarter = data.quarter.obj[data.quarter.currentQuarter].end;

        share.startOfQuarter = startOfQuarter;
        share.endOfQuarter = endOfQuarter;

        $scope.dateRange = {
            text:moment(startOfQuarter).format("MMM YYYY")+"-"+moment(endOfQuarter).format("MMM YYYY"),
            show:true
        };
    }

    $scope.goToOpp = function (op) {
        $rootScope.oppTabView = true;
        share.getInteractionHistory(op);
        // getInteractionHistory($scope,$rootScope,searchService,$http,share,op,null);
    }

    share.drawIntGrowth = function (data) {
        drawIntGrowth($scope,share,data)
    }

    $scope.openView = function(viewFor){
        setTabView($scope,viewFor);
        $scope.viewModeFor = viewFor;
    }

    $scope.closeOppInsightsModal = function(){
        $scope.showOppInsights = false;
        $scope.rolesList = [];
    };

    $scope.filterOpps = function (item,fromDashboard) {

        if(!$scope.filtersApplied || $scope.filtersApplied.length>0){
            $scope.filtersApplied = [];
        }

        if(item.colType == "Won"){
            $scope.filtersApplied.push({
                name:"Close Won",
                type:"stageName"
            });
        }

        if(item.colType == "Lost"){
            $scope.filtersApplied.push({
                name:"Close Lost",
                type:"stageName"
            });
        }

        if(item.colType == "Deals At Risk"){
            $scope.filtersApplied.push({
                name:"Close Won",
                type:"source"
            });
        }

        if(item.colType == "Renewal"){
            $scope.filtersApplied.push({
                name:"renewal",
                type:"source"
            });
        }

        var start = share.startOfQuarter
        var end = share.endOfQuarter

        if(item.colType == "Closing"){

            _.each(share.companyDetails.opportunityStages,function (el) {
                if(el.name !== "Close Won" && el.name !== "Close Lost"){
                    $scope.filtersApplied.push({
                        name:el.name,
                        type:"stageName"
                    });
                }
            });

            $scope.start = {
                month:moment(start).month,
                year:moment(start).year,
            }

            $scope.end = {
                month:moment(end).month,
                year:moment(end).year,
            }

            $scope.filtersApplied.push({
                name:moment(start).format("MMM YYYY")+"-"+moment(end).format("MMM YYYY"),
                type:"closeDate",
                start:start,
                end:end
            });

        } else {

            $scope.filtersApplied.push({
                name:moment(start).format("MMM YYYY")+"-"+moment(end).format("MMM YYYY"),
                type:"closeDate",
                start:start,
                end:end
            })
        }

        $scope.applyFilters($scope.filtersApplied[0],true);
        share.rangeType = "This Quarter"
    }

    $scope.dropDownSelection = null;
    $scope.openFilterDropDown = function (type) {
        type.open = true;
        resetOtherDropDowns($scope,type)
    }

    $scope.sortType = "closeDate";
    $scope.sortReverse = false;
    $scope.sortTable = function (item) {
        $scope.sortReverse = !$scope.sortReverse;
        $scope.sortType = item.type
    }

    $scope.sortTableByNumbers = function (item) {
        if(item.type == 'amount' || item.type == 'netGrossMargin' || item.type == 'convertedAmt' || item.type == 'convertedAmtWithNgm'){
            $scope.sortReverse = !$scope.sortReverse;
            $scope.sortType = item.type
        }
    }

    closeAllDropDownsAndModals($scope,".drop-down");

    share.resetPrevfilters = function () {
        $scope.start = {};
        $scope.end = {};

        $scope.filtersApplied = [];
        _.each($scope.headers,function (he) {
            if(he.values && he.values.length>0){
                _.each(he.values,function (va) {
                    va.selected = false;
                });
            }
        })
    }

    $scope.months = monthsAndYear().months
    $scope.years = monthsAndYear().years;

    $scope.start = {};
    $scope.end = {};

    $scope.selectFilter = function (type,filter,colType) {

        if(!$scope.filtersApplied){
            $scope.filtersApplied = [];
        }

        if(!filter.selected){
            $scope.filtersApplied = $scope.filtersApplied.filter(function (fl) {
                return fl.name !== filter.name
            })
        }

        if(filter.selected){

            var found = false;

            _.each($scope.filtersApplied,function (fl) {
                if(fl.name == filter.name && fl.type == type){
                    found = true;
                    return false;
                }
            });

            if(!found){

                if(type == 'userEmailId'){

                    share.selection = share.usersDictionary[filter.name]?share.usersDictionary[filter.name]:{fullName:filter.name,emailId:filter.name}

                    $scope.filtersApplied.push({
                        name:filter.name,
                        type:type,
                        displayName:share.usersDictionary[filter.name].fullName
                    });
                } else {
                    $scope.filtersApplied.push({
                        name:filter.name,
                        type:type,
                        displayName:filter.name
                    });
                }
            }
        }

        if(colType && colType.values){

            var selectingAll = true;
            _.each(colType.values,function (el) {
                if(!el.selected){
                    selectingAll = false;
                    return false;
                }
            })

            colType.selectingAll = selectingAll;
        }

        $scope.filtersApplied = _.uniqBy($scope.filtersApplied,"name");
    };

    share.applyFilters = function(colType) {
        $scope.applyFilters(colType);
    }

    $scope.applyFilters = function (colType,dontUpdateMetaData,index) {

        if(!$scope.filtersApplied){
            $scope.filtersApplied = [];
        }

        var closeDateExists = false;

        if(colType.type === "closeDate"){

            $scope.filtersApplied = $scope.filtersApplied.filter(function (fl) {
                return fl.type != "closeDate"
            });

            if($scope.start && typeof $scope.start.month == "string"){

                if($scope.start.date) {
                    var start = moment().year(parseInt($scope.start.year)).month(parseInt($scope.start.month)-1).date(parseInt($scope.start.date));
                } else {
                    var start = moment().year(parseInt($scope.start.year)).month(parseInt($scope.start.month)-1);
                }

                if($scope.end.date) {
                    var end = moment().year(parseInt($scope.end.year)).month(parseInt($scope.end.month)-1).date(parseInt($scope.end.date));                    
                } else {
                    var end = moment().year(parseInt($scope.end.year)).month(parseInt($scope.end.month)-1);
                }


                if(colType.includeDateRange) {
                    $scope.filtersApplied.push({
                        name:moment(start).format("MMM DD YYYY")+"-"+moment(end).format("MMM DD YYYY"),
                        type:colType.type,
                        start:start,
                        end:end,
                        includeDateRange: colType.includeDateRange
                    });

                } else {

                    $scope.filtersApplied.push({
                        name:moment(start).format("MMM YYYY")+"-"+moment(end).format("MMM YYYY"),
                        type:colType.type,
                        start:start,
                        end:end,
                        includeDateRange: colType.includeDateRange
                    });
                }

                share.rangeType = moment(start).format("MMM YYYY")+"-"+moment(end).format("MMM YYYY")
            }

        }

        $scope.dateRange.show = !$scope.dateRange.show;

        colType.open = !colType.open;

        _.each($scope.headers,function (he) {
            he.open = false;
        })

        var userEmailIdExists = false;
        _.each($scope.filtersApplied,function (el) {
            if(el.type == "userEmailId"){
                userEmailIdExists = true;
            }

            if(el.type == "closeDate"){
                closeDateExists = true;
            }
        });

        if(share.selection && share.selection.fullName !== "Show all team members"){
            $scope.filtersApplied.push({
                name:share.selection.emailId,
                displayName:share.selection.fullName,
                type:"userEmailId"
            })
        } else if(!userEmailIdExists && !share.selection){
            $scope.filtersApplied.push({
                name:share.liuData.emailId,
                displayName:share.liuData.fullName,
                type:"userEmailId"
            })
        }

        if(!closeDateExists){
            // share.rangeType = "All FYs"

            $scope.filtersApplied.push({
                name:moment(share.quarterRange.qStart).format("MMM YYYY")+"-"+moment(share.quarterRange.qEnd).format("MMM YYYY"),
                type:"closeDate",
                start:share.quarterRange.qStart,
                end:share.quarterRange.qEnd
            });
        }

        _.each($scope.stageMetaInfo,function (el) {
            el.rangeType = "This Quarter"
        })

        $scope.filtersApplied = _.uniqBy($scope.filtersApplied,"name");

        share.drawOppsTable(null,$scope.filtersApplied);
    }

    share.populateFilters = function(companyDetails){

        $scope.companyDetails = companyDetails;

        $scope.filterLists = [],$scope.filterListObj = {};
        for(var key in companyDetails){

            if(_.includes(["opportunityStages","accountTypes","businessUnits","geoLocations","productList","solutionList","sourceList","typeList","verticalList"], key)){

                var values = companyDetails[key];
                if(key === "geoLocations"){
                    values = [];
                    _.each(companyDetails[key],function (el) {
                        values.push({
                            name:el.region
                        })
                    });
                }

                values.forEach(function (el) {
                    if(key == "typeList"){
                        el.displayName = el.name; //this is needed for sorting.
                    }
                    el.selected = false;
                });

                var typeFormat = getTypeFormat(key);

                $scope.filterListObj[typeFormat] = {
                    type:key,
                    typeFormatted:typeFormat,
                    values:values
                }

                $scope.filterLists.push({
                    type:key,
                    typeFormatted:typeFormat,
                    values:values
                })
            }
        }
        setOppTableHeader($scope,share,$scope.filterListObj);
    }

    share.drawOppsTable = function (emailId,filters) {

        var filterObj = {};
        var url = "/reports/get/opportunities/v2"

        if(filters && filters.length>0){
            filterObj.filters = filters
        } else if(emailId && share.usersDictionary[emailId] && share.usersDictionary[emailId].userId) {
            filterObj.forUserEmailId = emailId
            filterObj.forUserId = share.usersDictionary[emailId].userId;
        } else if(!emailId && share.selection && share.selection.fullName !== "Show all team members") {
            filterObj.forUserEmailId = share.liuData.emailId
            filterObj.forUserId = share.liuData._id;
        }

        if(share.selection && share.selection.fullName === "Show all team members"){
            filterObj.allUserEmailId = [];
            filterObj.allUserId = [];

            _.each(share.team,function (tm) {
                filterObj.allUserEmailId.push(tm.emailId);
                filterObj.allUserId.push(tm.userId);
            });
        }

        filterObj["selectedHierarchy"] = share.selectedHierarchy;

        $http.post(url,filterObj)
            .success(function (response) {

                var importantHeaders = [],
                    importantHeadersObj = {};

                if(response && response.masterData && response.masterData.length>0){
                    _.each(response.masterData,function (ma) {
                        if(ma.importantHeaders){
                            _.each(ma.importantHeaders,function (ih) {
                                if(ih.isImportant){
                                    importantHeaders.push(ih.name);
                                }
                            });
                        }
                    })
                };

                importantHeaders = _.sortBy(importantHeaders,function (el) {
                    return el.toLowerCase()
                });

                if(!response.dealsAtRisk){
                    response.dealsAtRisk = {
                        "count": 0,
                        "amount": 0,
                        dealsRiskAsOfDate : new Date()
                    }
                }

                _.each(importantHeaders,function (el) {
                    importantHeadersObj[el] = true;
                });

                setOppTableHeader($scope,share,$scope.filterListObj,importantHeaders);

                $scope.rangeType = share.rangeType?share.rangeType:"This Quarter"

                var thisQuarterOpps = [],
                    thisQuarterOppsObj = {},
                    monthStartDate = moment().startOf("month");

                var date30End = moment().add(30,"days");

                var contacts = [],
                    owners = [],
                    productsAll = [],
                    accountsAll = [],
                    wonAmt = 0,
                    wonCount = 0,
                    lostCount = 0,
                    pipelineCount = 0,
                    renewalCount = 0,
                    staleCount = 0,
                    staleAmt = 0,
                    closing30DaysCount = 0,
                    lostAmt = 0,
                    closing30DaysAmt = 0,
                    pipelineAmt = 0,
                    renewalAmt = 0,
                    targetAmt = 0,
                    wonReasons = [],
                    accounts = [],
                    products = [],
                    filterLists = [],
                    locations = [],
                    lostReasons = [],
                    sourceTypes = [],
                    createdThisMonthOpps = [],
                    oppTypes = [];

                if(response.targets && response.targets.length>0){
                    targetAmt = _.sumBy(response.targets,"target")
                }

                var renewalTypes = [];

                _.each(share.companyDetails.typeList,function (tl) {
                    if(tl.isTypeRenewal){
                        renewalTypes.push(tl.name)
                    }
                })

                if(!$scope.filtersApplied || $scope.filtersApplied.length == 0){
                    $scope.filtersApplied = [];
                }

                $scope.oppsExists = false;
                if(response && response.opps){

                    _.each(response.opps,function (op) {

                        if(op.masterData && op.masterData.length>0){
                            _.each(op.masterData,function (ma) {
                                _.each(ma.data,function (da) {

                                    for(var key in da){
                                        if(importantHeadersObj[key]){

                                            if(!op.masterDataFormat){
                                                op.masterDataFormat = [];
                                            }

                                            if(da[key]){
                                                op.masterDataFormat.push({
                                                    key:key,
                                                    value:da[key]
                                                })
                                                op[key] = da[key];
                                            } else {
                                                op[key] = "";
                                                op.masterDataFormat.push({
                                                    key:key,
                                                    value:""
                                                })
                                            }
                                        }
                                    }
                                });
                            })
                        }

                        if(op.masterDataFormat && op.masterDataFormat.length>0){

                        } else if((!op.masterDataFormat || op.masterDataFormat.length === 0) && importantHeaders.length>0){
                            op.masterDataFormat = [];
                            _.each(importantHeaders,function (ih) {
                                op.masterDataFormat.push({
                                    key:ih,
                                    value:""
                                });
                            });
                        };

                        op.isNotOwner = false;
                        op.isOppClosed = _.includes(op.stageName.toLowerCase(),"close");

                        if(op.isOppClosed){
                            if(share.liuData.corporateAdmin){
                                op.isNotOwner = false;
                            } else {
                                op.isNotOwner = true;
                            }
                        }

                        var nonExistingImpHeaders = _.xor(importantHeaders,_.map(op.masterDataFormat,'key'));

                        if(nonExistingImpHeaders && nonExistingImpHeaders.length>0){
                            _.each(nonExistingImpHeaders,function (ih) {
                                op.masterDataFormat.push({
                                    key:ih,
                                    value:""
                                })
                            });

                        };

                        op.masterDataFormat = _.sortBy(op.masterDataFormat,function (el) {
                            return el.key.toLowerCase()
                        });

                        if(op.partners && op.partners.length>0){
                            op.partnersList = "";
                            _.each(op.partners,function (pr) {
                                op.partnersList = pr.emailId+','+op.partnersList
                            });
                        }

                        op.amount = parseFloat(op.amount);
                        op.amountWithNgm = op.amount;

                        if(op.netGrossMargin || op.netGrossMargin == 0){
                            op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                        }

                        op.convertedAmt = op.amount;
                        op.convertedAmtWithNgm = op.amountWithNgm

                        if(op.currency && op.currency !== share.primaryCurrency){

                            if(share.currenciesObj[op.currency] && share.currenciesObj[op.currency].xr){
                                op.convertedAmt = op.amount/share.currenciesObj[op.currency].xr
                            }

                            if(op.netGrossMargin || op.netGrossMargin == 0){
                                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                            }

                            op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

                        }

                        var acc = fetchCompanyFromEmail(op.contactEmailId);
                        op.account = acc?acc:"Others";

                        op.stageColor = "";

                        if(op.stageName == "Close Won") {
                            op.stageColor = "won"
                        }

                        if(op.stageName == "Close Lost") {
                            op.stageColor = "lost"
                        }

                        op.amount = parseFloat(op.amount);
                        op.amountWithNgm = op.amount;

                        if(op.netGrossMargin || op.netGrossMargin == 0){
                            op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                        }

                        op.convertedAmt = op.amount;
                        op.convertedAmtWithNgm = op.amountWithNgm

                        if(op.currency && op.currency !== share.primaryCurrency){

                            if(share.currenciesObj[op.currency] && share.currenciesObj[op.currency].xr){
                                op.convertedAmt = op.amount/share.currenciesObj[op.currency].xr
                            }

                            if(op.netGrossMargin || op.netGrossMargin == 0){
                                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                            }

                            op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))
                        }

                        if(op.closeReasons && op.closeReasons.length>0){
                            op.closeReasonsString = "";
                            _.each(op.closeReasons,function (cr,index) {
                                if(index == 0){
                                    op.closeReasonsString = cr;
                                } else {
                                    op.closeReasonsString = op.closeReasonsString+","+cr;
                                }
                            })
                        }

                        var monthYear =  moment(op.closeDate).format("MMM YYYY");
                        op.monthYear = monthYear;

                        if(_.includes(["Close Won"], op.stageName)){
                            wonAmt = wonAmt+op.convertedAmtWithNgm
                            wonCount++;
                            if(op.closeReasons && op.closeReasons.length>0){
                                _.each(op.closeReasons,function (cr) {
                                    var wonObj = {
                                        name:cr?cr:"Others",
                                        amount:op.convertedAmtWithNgm
                                    }
                                    wonReasons.push(wonObj)
                                })
                            }

                            if(op.geoLocation && op.geoLocation.town){
                                locations.push(op.geoLocation.town)
                            }

                            var obj = {}
                            obj[op.productType?op.productType:"Others"] = op.convertedAmtWithNgm;
                            var accObj = {}
                            accObj[acc] = op.convertedAmtWithNgm
                            accounts.push(accObj)
                            products.push(obj);

                            oppTypes.push({
                                name:op.type,
                                amount:op.convertedAmtWithNgm
                            })

                            sourceTypes.push({
                                name:op.sourceType,
                                amount:op.convertedAmtWithNgm
                            })

                        } else if(_.includes(["Close Lost"], op.stageName)){
                            lostAmt = lostAmt+op.convertedAmtWithNgm
                            lostCount++

                            if(op.closeReasons && op.closeReasons.length>0){
                                _.each(op.closeReasons,function (cr) {
                                    var lostObj = {
                                        name:cr?cr:"Others",
                                        amount:op.convertedAmtWithNgm
                                    }
                                    lostReasons.push(lostObj)
                                })
                            }
                        } else {

                            if(new Date(op.closeDate)>= new Date() && new Date(op.closeDate)<= new Date(date30End)){
                                closing30DaysCount++;
                                closing30DaysAmt = closing30DaysAmt+op.convertedAmtWithNgm;
                            }

                            pipelineAmt = pipelineAmt+op.convertedAmtWithNgm
                            pipelineCount++
                        }

                        if(op.relatasStage !== "Close Lost" && op.relatasStage !== "Close Won"){
                            if(new Date(op.closeDate)< new Date(moment().startOf("day"))){
                                staleCount++;
                                staleAmt = staleAmt+op.convertedAmtWithNgm
                            }
                        }

                        if(!op.currency){
                            op.currency = share.primaryCurrency
                        }

                        if(_.includes(renewalTypes,op.type)){
                            renewalAmt = renewalAmt+op.amountWithNgm
                            renewalCount++;
                        }

                        op.amountFormatted = op.amount.r_formatNumber(2)
                        op.amountWithNgm = parseFloat(op.amountWithNgm.r_formatNumber(2))
                        op.convertedAmtWithNgm = parseFloat(op.convertedAmtWithNgm.r_formatNumber(2))

                        op.account = acc?acc:"Others";

                        op.closeDateFormatted = moment(op.closeDate).format(standardDateFormat());
                        contacts.push({
                            name:op.contactEmailId,
                            displayName:op.contactEmailId
                        });

                        owners.push({
                            name:op.userEmailId,
                            displayName:op.userEmailId
                        })

                        if(acc && acc != "Others"){
                            accountsAll.push({
                                name:acc?acc:"Others",
                                displayName:acc?acc:"Others"
                            })
                        }

                        op.owner = share.usersDictionary[op.userEmailId]?share.usersDictionary[op.userEmailId]:{fullName:op.userEmailId};

                        productsAll.push({
                            name:op.productType,
                            displayName:op.productType
                        });

                        if(new Date(op.createdDate)>= new Date(monthStartDate) && new Date(op.createdDate)<= new Date()){
                            createdThisMonthOpps.push(op)
                        }
                    });
                    if(share.redirectFrom == "today_overdue") {
                        $scope.opps = _.filter(response.opps, function(opp) {
                                        return !(opp.stageName == 'Close Won' || opp.stageName == 'Close Lost')
                                    });
                        
                    } else {
                        $scope.opps = response.opps;
                    }

                    drawPipeline($scope,wonAmt,lostAmt,wonCount,
                        lostCount,pipelineAmt,pipelineCount,
                        closing30DaysAmt,closing30DaysCount,response.dealsAtRisk.count,
                        response.dealsAtRisk.amount,response.dealsAtRisk.dealsRiskAsOfDate,
                        renewalAmt,renewalCount,staleAmt,staleCount,share,targetAmt);
                }
                $scope.oppsExists = true;

                checkHeadersLoaded()
                function checkHeadersLoaded(){
                    if($scope.headers){

                        _.each($scope.headers,function (he) {

                            if(he.name == "Contact"){

                                if(!he.values || !he.values[0]){
                                    he.values = _.uniqBy(contacts,"name");
                                }
                            }

                            if(he.name == "Owner"){
                                he.values = _.uniqBy(owners,"name");
                                he.values.forEach(function (el) {
                                    el.displayName = share.usersDictionary[el.name]?share.usersDictionary[el.name].fullName:el.name
                                })
                            }

                            if(he.name == "Product" && productsAll && productsAll.length>0){
                                if(!he.values || !he.values[0]){
                                    he.values = _.uniqBy(productsAll,"name");
                                }
                            }

                            if(he.name == "Account"){
                                he.values = _.uniqBy(accountsAll,"name");
                            }
                        });

                        // Prepare Excel data:
                        $scope.fileName = 'Opps-'+moment().format("DDMMMMYY");
                        $scope.exportData = [];
                        // Headers:

                        $scope.exportData.push(getOppXLSHeaders(importantHeaders));
                        // Data:

                        angular.forEach($scope.opps, function(el, key) {
                            if(el.closeReasons && el.closeReasons.length>0){
                                el.closeReasonsString = "";
                                _.each(el.closeReasons,function (cr,index) {
                                    if(index == 0){
                                        el.closeReasonsString = cr;
                                    } else {
                                        el.closeReasonsString = el.closeReasonsString+","+cr;
                                    }
                                })
                            }

                            var arr = [
                                el.opportunityName,
                                el.userEmailId,
                                el.contactEmailId,
                                fetchCompanyFromEmail(el.contactEmailId),
                                el.currency,
                                parseFloat(el.amount),
                                parseFloat(el.netGrossMargin),
                                parseFloat(el.amountWithNgm.toFixed(2)),
                                share.currenciesObj[el.currency] && share.currenciesObj[el.currency].xr?share.currenciesObj[el.currency].xr:1,
                                parseFloat(el.convertedAmt),
                                parseFloat(el.convertedAmtWithNgm.toFixed(2)),
                                el.stageName,
                                new Date(el.closeDate),
                                el.productType,
                                el.businessUnit,
                                el.solution,
                                el.type,
                                el.geoLocation?el.geoLocation.zone:"",
                                el.geoLocation?el.geoLocation.town:"",
                                el.sourceType,
                                el.vertical,
                                _.map(el.partners,"emailId").join(","),
                                new Date(el.createdDate),
                                el.createdByEmailId,
                                el.opportunityId,
                                el.closeReasonsString,
                                el.closeReasonDescription
                            ];

                            if(el.masterDataFormat){
                               _.each(el.masterDataFormat,function (ma) {
                                   arr.push(ma.value);
                               });
                            }

                            $scope.exportData.push(arr);
                        });

                        $scope.loadingMetaData = false;
                    } else {
                        setTimeOutCallback(500,function () {
                            checkHeadersLoaded();
                        })
                    }
                }
            });
    }

    $scope.removeFilter = function (filter) {

        $scope.filtersApplied = $scope.filtersApplied.filter(function (fl) {
            return fl.name !== filter.name
        });

        if($scope.filtersApplied.length == 0){
            share.rangeType = "This Quarter";
        } else {
            share.rangeType = "All FYs";
            _.each($scope.filtersApplied,function (fl) {
                if(fl.type == "closeDate"){
                    share.rangeType = fl.name;
                }
            })
        }

        share.drawOppsTable(null,$scope.filtersApplied)
    }

});

relatasApp.controller("currentInsights", function($scope,$http,share,$rootScope){

    share.getCurrentInsights = function(response){
        currentInsights($scope,$http,share,response)
    }

    share.currentInsightsData = function(pipelinePercentage,achievementPercentage){
        $scope.pipelinePercentage = pipelinePercentage;
        $scope.achievementPercentage = achievementPercentage;
    }

    $scope.loadingMetaData = true;
})

function spiderChartInit(share,TranslateX,account,$scope,callback) {

    var RadarChart = {
        draw: function(id, d, options){
            var cfg = {
                radius: 2, //dot radii
                w: 300,
                h: 300,
                factor: 1,
                factorLegend: .85,
                levels: 5,
                maxValue: 100,
                radians: 2 * Math.PI,
                opacityArea: 0.5,
                ToRight: 5,
                TranslateX: TranslateX?TranslateX:80,
                TranslateY: 30,
                ExtraWidthX: 100,
                ExtraWidthY: 100,
                color: d3.scale.category10()
            };

            if('undefined' !== typeof options){
                for(var i in options){
                    if('undefined' !== typeof options[i]){
                        cfg[i] = options[i];
                    }
                }
            }
            cfg.maxValue = Math.max(cfg.maxValue, d3.max(d, function(i){return d3.max(i.map(function(o){return o.value;}))}));

            var tooltipClass = ".tooltip"
            var valueId = "#value"
            var cursorPosX = 100;
            var cursorPosY = 100;

            if(id == "#spiderChart1"){
                tooltipClass = ".tooltip2"
                valueId = "#value2"
            }

            if(id == "#spiderChart3"){
                tooltipClass = ".tooltip3"
                valueId = "#value3"
                cursorPosX = 100;
                cursorPosY = 200;
            }

            var allAxis = (d[0].map(function(i, j){return i}));
            var total = allAxis.length;
            var radius = cfg.factor*Math.min(cfg.w/2, cfg.h/2);
            var Format = d3.format('%');
            d3.select(id).select("svg").remove();

            var g = d3.select(id)
                .append("svg")
                .attr("width", cfg.w+cfg.ExtraWidthX)
                .attr("height", cfg.h+cfg.ExtraWidthY)
                .append("g")
                .attr("transform", "translate(" + cfg.TranslateX + "," + cfg.TranslateY + ")")
                .on('mousemove', function() {
                    if(d3.mouse(this) && d3.mouse(this)[0] && d3.mouse(this)[1]){
                        cursorPosX = d3.mouse(this)[0]
                        cursorPosY = d3.mouse(this)[1]
                    }
                });

            var tooltip;

            //Circular segments
            for(var j=0; j<cfg.levels-1; j++){
                var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
                g.selectAll(".levels")
                    .data(allAxis)
                    .enter()
                    .append("svg:line")
                    .attr("x1", function(d, i){return levelFactor*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
                    .attr("y1", function(d, i){return levelFactor*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
                    .attr("x2", function(d, i){return levelFactor*(1-cfg.factor*Math.sin((i+1)*cfg.radians/total));})
                    .attr("y2", function(d, i){return levelFactor*(1-cfg.factor*Math.cos((i+1)*cfg.radians/total));})
                    .attr("class", "line")
                    .style("stroke", "#ccc")
                    .style("stroke-opacity", "0.75")
                    .style("stroke-width", "0.3px")
                    .attr("transform", "translate(" + (cfg.w/2-levelFactor) + ", " + (cfg.h/2-levelFactor) + ")");
            }

            series = 0;

            var axis = g.selectAll(".axis")
                .data(allAxis)
                .enter()
                .append("g")
                .attr("class", "axis");

            axis.append("line")
                .attr("x1", cfg.w/2)
                .attr("y1", cfg.h/2)
                .attr("x2", function(d, i){return cfg.w/2*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
                .attr("y2", function(d, i){return cfg.h/2*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
                .attr("class", "line")
                .style("stroke", "grey")
                .style("stroke-width", "0.65px")
                .on('mouseover', function (d) {

                    var text = ""
                    var oppVal = 0;
                    var intsVal = 0;

                    if(account){

                        function checkAccObjLoaded(){
                            if(share.accObjForTooltip){
                                oppVal = parseFloat(share.accObjForTooltip[d.axis].oppsAmount.toFixed(2))
                                intsVal = parseFloat(share.accObjForTooltip[d.axis].interactionsCount.toFixed(2))
                            } else {
                                setTimeOutCallback(500,function () {
                                    checkAccObjLoaded()
                                })
                            }
                        }

                        checkAccObjLoaded();

                    } else {

                        function checkOppObjLoaded(){
                            if(share.oppObjForTooltip){
                                if(share.oppObjForTooltip[d.opportunityId]){
                                    oppVal = parseFloat(share.oppObjForTooltip[d.opportunityId].oppsAmount.toFixed(2))
                                    intsVal = parseFloat(share.oppObjForTooltip[d.opportunityId].interactionsCount.toFixed(2))
                                }
                            } else {
                                setTimeOutCallback(500,function () {
                                    checkOppObjLoaded()
                                })
                            }
                        }

                        checkOppObjLoaded();
                    }

                    oppVal = getAmountInThousands(oppVal,2,share.primaryCurrency == "INR")

                    var name = d.axis?d.axis.toUpperCase():""
                    if(account){
                        text = name+" | Value: "+oppVal+ " | Int: "+ intsVal
                    } else {
                        text = name+" | Value: "+oppVal+ " | Int: "+ intsVal + " | "+ share.oppObjForTooltip[d.opportunityId].contactEmailId
                    }

                    d3.select(tooltipClass)
                        .style("left", cursorPosX + "px")
                        .style("top", cursorPosY+35 + "px")
                        .select(valueId)
                        .text(text);

                    d3.select(tooltipClass).classed("hidden", false);

                })
                .on('mouseout', function (d) {
                    d3.select(this).style("stroke-width", ".65px");
                    d3.select(tooltipClass).classed("hidden", true);
                })
                .on("click", function(d) {

                    if(account){
                        window.location = "/accounts/all?accountName="+d.axis
                    } else {
                        window.location = "/opportunities/all?opportunityId="+d.opportunityId
                    }
                });

            axis.append("text")
                .attr("class", "legend")
                .on('mouseover', function (d){
                    var fullText = ""

                    $(this)
                        .attr("class", "no-pointer")
                        .text(fullText)
                        .css({'margin-top':'100px'})
                        .css({'font-size':'11px'})
                        .css({'pointer-events':'none'})
                })
                .on('mouseout', function(d){
                    $(this)
                        .attr("class", "legend")
                        .text(".")
                        .attr("fill", function (d) {
                        })
                        .css("font-size", "70px")
                        .css({'pointer-events':'auto'})

                })
                .attr("dy", "0.25em")
                .attr("transform", function(d, i){return "translate(0, -10)"})
                .attr("x", function(d, i){return cfg.w/2*(0.9-cfg.factorLegend*Math.sin(i*cfg.radians/total))-60*Math.sin(i*cfg.radians/total);})
                .attr("y", function(d, i){return cfg.h/2*(1-Math.cos(i*cfg.radians/total))-20*Math.cos(i*cfg.radians/total);})
                .text(function(d) {
                    if(!TranslateX){
                        return ".";
                    }
                })
                .attr("fill", function (d) {
                })
                .style("font-family", "Lato")
                .style("font-size", "70px")
                .attr("text-anchor", "middle")


            d.forEach(function(y, x){
                dataValues = [];
                var key3 = {
                    value:0
                };
                if(y && y[x] && y[x].value){
                    key3 = y[x]
                }

                g.selectAll(".nodes")
                    .data(y, function(j, i){
                        dataValues.push([
                            cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)),
                            cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total))
                        ]);
                    });
                dataValues.push(dataValues[0]);
                g.selectAll(".area")
                    .data([dataValues])
                    .enter()
                    .append("polygon")
                    .attr("class", "radar-chart-serie"+series)
                    .style("stroke-width", "2px")
                    .style("stroke", cfg.color(series))
                    .attr("data-id", function(j){return j.axis})
                    .attr("points",function(d) {
                        var str="";
                        for(var pti=0;pti<d.length;pti++){
                            str=str+d[pti][0]+","+d[pti][1]+" ";
                        }
                        return str;
                    })
                    .attr("cx", function(j, i){
                        return cfg.w/2*(1-(Math.max(key3.value, 0)/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total));
                    })
                    .attr("cy", function(j, i){
                        return cfg.h/2*(1-(Math.max(key3.value, 0)/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total));
                    })
                    .style("fill", function(j, i){return cfg.color(series)})
                    .style("fill-opacity", cfg.opacityArea)
                    .on('mouseover', function (d){

                        z = "polygon."+d3.select(this).attr("class");

                        d3.select(this).style("stroke-width", "3px");

                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", 0.1);
                        g.selectAll(z)
                            .transition(200)
                            .style("fill-opacity", .7);

                        var fullText = "";
                        var oppVal = 0;
                        var intsVal = 0;

                        var key = {
                            axis:null,
                            opportunityId:null
                        };

                        if(y && y[x] && y[x].axis){
                            key = y[x]
                        }

                        if(account){
                            oppVal = parseFloat(share.accObjForTooltip[key.axis].oppsAmount.toFixed(2))
                            intsVal = parseFloat(share.accObjForTooltip[key.axis].interactionsCount.toFixed(2))

                        } else {
                            oppVal = parseFloat(share.oppObjForTooltip[key.opportunityId].oppsAmount.toFixed(2))
                            intsVal = parseFloat(share.oppObjForTooltip[key.opportunityId].interactionsCount.toFixed(2))
                        }

                        oppVal = getAmountInThousands(oppVal,2,share.primaryCurrency == "INR")

                        var name = key.axis?key.axis.toUpperCase():"";

                        if(account){
                            fullText = name+" | Value: "+oppVal+ " | Int: "+ intsVal;
                        } else {
                            fullText = name+" | Value: "+oppVal+ " | Int: "+ intsVal + " | "+ share.oppObjForTooltip[key.opportunityId].contactEmailId
                        }

                        d3.select(tooltipClass)
                            .style("left", cursorPosX + "px")
                            .style("top", cursorPosY+35 + "px")
                            .select(valueId)
                            .text(fullText);

                        d3.select(tooltipClass).classed("hidden", false);

                        z = "polygon."+d3.select(this).attr("class");
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", 0.1);
                        g.selectAll(z)
                            .transition(200)
                            .style("fill-opacity", .7);
                    })
                    .on('mouseout', function(){
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", cfg.opacityArea);

                        d3.select(this).style("stroke-width", "2px");
                        d3.select(tooltipClass).classed("hidden", true);

                    })
                    .on("click", function(d,val) {

                        if(y && y[x] && y[x].axis){
                            if(account){
                                window.location = "/accounts/all?accountName="+y[x].axis
                            } else {
                                window.location = "/opportunities/all?opportunityId="+y[x].opportunityId
                            }
                        }
                    });
                series++;
            });
            series=0;

            d.forEach(function(y, x){
                g.selectAll(".nodes")
                    .data(y).enter()
                    .append("svg:circle")
                    .attr("class", "radar-chart-serie"+series)
                    .attr('r', cfg.radius)
                    .attr("alt", function(j){return Math.max(j.value, 0)})
                    .attr("cx", function(j, i){

                        dataValues.push([
                            cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)),
                            cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total))
                        ]);
                        return cfg.w/2*(1-(Math.max(j.value, 0)/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total));
                    })
                    .attr("cy", function(j, i){
                        return cfg.h/2*(1-(Math.max(j.value, 0)/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total));
                    })
                    .attr("data-id", function(j){return j.axis})
                    .style("fill", cfg.color(series)).style("fill-opacity", .9)
                    .on('mouseover', function (d){

                        d3.select(this).style("stroke-width", "3px");

                        var fullText = "";
                        var oppVal = 0;
                        var intsVal = 0;

                        if(account){
                            oppVal = parseFloat(share.accObjForTooltip[d.axis].oppsAmount.toFixed(2))
                            intsVal = parseFloat(share.accObjForTooltip[d.axis].interactionsCount.toFixed(2))

                        } else {
                            oppVal = parseFloat(share.oppObjForTooltip[d.opportunityId].oppsAmount.toFixed(2))
                            intsVal = parseFloat(share.oppObjForTooltip[d.opportunityId].interactionsCount.toFixed(2))
                        }

                        oppVal = getAmountInThousands(oppVal,2,share.primaryCurrency == "INR")

                        var name = d.axis?d.axis.toUpperCase():"";

                        if(account){
                            fullText = name+" | Value: "+oppVal+ " | Int: "+ intsVal;
                        } else {
                            fullText = name+" | Value: "+oppVal+ " | Int: "+ intsVal + " | "+ share.oppObjForTooltip[d.opportunityId].contactEmailId
                        }

                        d3.select(tooltipClass)
                            .style("left", cursorPosX + "px")
                            .style("top", cursorPosY+35 + "px")
                            .select(valueId)
                            .text(fullText);

                        d3.select(tooltipClass).classed("hidden", false);

                        z = "polygon."+d3.select(this).attr("class");
                        g.selectAll("polygon")
                            .transition(200)
                            .style("fill-opacity", 0.1);
                        g.selectAll(z)
                            .transition(200)
                            .style("fill-opacity", .7);
                    })
                    .on('mouseout', function(){

                        d3.select(tooltipClass).classed("hidden", true);
                        d3.select(this).style("stroke-width", "2px");
                    })
                    .append("svg:title")
                    .text(function(j){
                        // return Math.max(j.value, 0)
                    })
                    .on("click", function(d) {

                        if(y && y[x] && y[x].axis){
                            if(account){
                                window.location = "/accounts/all?accountName="+y[x].axis
                            } else {
                                window.location = "/opportunities/all?opportunityId="+y[x].opportunityId
                            }
                        }
                    });

                series++;
            });
            // Tooltip
            tooltip = g.append('text')
                .style('opacity', 0)
                .style('font-family', 'Lato')
                .style('font-size', '11px');

            var angleSlice = Math.PI * 2 / total;

            var radarLine = d3.svg.line.radial()
                .interpolate("basis")
                .radius(function(d) { return rScale(d.value); })
                .angle(function(d,i) {	return i*angleSlice; });

            var rScale = d3.scale.linear()
                .range([0, radius])
                .domain([0, cfg.maxValue]);

        }
    };

    callback(RadarChart)
}

function spiderDataInit(RadarChart,data,share,w,h,id,opps_original,interactions_original,forAccs,$scope,reverseSort,$http,verbose){
    w = w?w:200;
    h = h?h:200;
    id = id?id:"#spiderChart"

    var colorscale = d3.scale.category10();

    var interactions = [],
        opps = [];

    _.each(opps_original,function (op) {
        opps.push(op)
    })

    _.each(interactions_original,function (op) {
        interactions.push(op)
    })

    //Legend titles
    var LegendOptions = ['Accounts','Opportunities'];

    var nonExistingInOpps = [],
        nonExistingInInts = [];

    if(interactions.length>opps.length){
        nonExistingInOpps = _.differenceBy(interactions,opps,"axis")
    } else {
        nonExistingInOpps = _.differenceBy(interactions,opps,"axis")
    }

    if(opps.length>interactions.length){
        nonExistingInInts = _.differenceBy(opps,interactions,"axis")
    } else {
        nonExistingInInts = _.differenceBy(opps,interactions,"axis")
    }

    if(nonExistingInOpps.length>0){
        _.each(nonExistingInOpps,function (el) {
            opps.push({
                axis:el.axis,
                original_val:0,
                value: 0,
                opportunityId:null
            })
        })
    }

    if(nonExistingInInts.length>0){
        _.each(nonExistingInInts,function (el) {
            interactions.push({
                axis:el.axis,
                original_val:0,
                value: 0,
                opportunityId:null
            })
        })
    }

    var accsIntsInsights = {
        avgInts: [],
        belowAvgInts: [],
        lastIntsBelow30Days: []
    }
    var oppsExistForAcc = {};

    var minInt = _.minBy(interactions,"value");
    var maxInt = _.maxBy(interactions,"value");
    var avgInt = _.meanBy(interactions,"value");
    var minOpp = _.minBy(opps,"value");
    var maxOpp = _.maxBy(opps,"value");
    var minOppAllowed = minOpp.value>0?1:0;
    var minIntAllowed = minInt.value>0?1:0;
    var dataOpps = [],
        dataInts = [];
    share.intsObj = {}

    var daysAgo30 = new Date(moment().subtract(30,'days'))

    if(opps.length>0) {
        _.each(opps,function (el) {

            if(verbose || forAccs){
                if(el.interactionsCount < avgInt) {
                    accsIntsInsights.belowAvgInts.push(el.axis)
                }
            }

            oppsExistForAcc[el.axis] = el.oppsAmount?el.oppsAmount:el.amountWithNgm;
            dataOpps.push({
                axis:el.axis,
                original_val:el.amountWithNgm,
                value: scaleBetween(el.value,minOpp.value,maxOpp.value,minOppAllowed),
                opportunityId:el.opportunityId,
                opps:verbose?el.opps:[]
            })
        })
    };

    _.each(interactions,function (el) {
        if(verbose){
            share.intsObj[el.axis] = {
                count:el.interactionsCount,
                lastInteractedDate:el.lastInteractedDate
            };
        }

        if(verbose || forAccs){
            if(el.value>avgInt){
                if(!oppsExistForAcc[el.axis]){
                    accsIntsInsights.avgInts.push(el.axis)
                }
            }

            if(new Date(el.lastInteractedDate)<daysAgo30){
                accsIntsInsights.lastIntsBelow30Days.push(el.axis)
            }
        }

        dataInts.push({
            axis:el.axis,
            original_val:el.value,
            value:scaleBetween(el.value,minInt.value,maxInt.value,minIntAllowed),
            opportunityId:el.opportunityId
        });
    })

    if(verbose || forAccs){
        $scope.accsIntsInsights = [];


        if(accsIntsInsights.avgInts.length>0){
            $scope.accsIntsInsights.push(accsIntsInsights.avgInts.length+" accounts have interactions above average. Opp creation recommended with those accounts")
        }

        if(accsIntsInsights.belowAvgInts.length>0){
            $scope.accsIntsInsights.push(accsIntsInsights.belowAvgInts.length+" accounts with opps have below average interactions")
        }

        if(accsIntsInsights.lastIntsBelow30Days.length>0){
            $scope.accsIntsInsights.push(accsIntsInsights.lastIntsBelow30Days.length+" accounts with opps not interacted in the past 30 days")
        }
    }

    if(verbose){
        getOppForAccounts($scope,$http,dataOpps,share.intsObj,share.primaryCurrency)
    }

    if(forAccs){
        share.accObjForTooltip = {};
        _.each(opps,function (op) {
            share.accObjForTooltip[op.axis] = {
                oppsAmount: op.oppsAmount?op.oppsAmount:0
            }
        });

        _.each(interactions,function (ints) {
            if(share.accObjForTooltip[ints.axis]){
                share.accObjForTooltip[ints.axis].interactionsCount = ints.interactionsCount?ints.interactionsCount:0
            } else {
                share.accObjForTooltip[ints.axis] = {
                    interactionsCount: ints.interactionsCount?ints.interactionsCount:0
                }
            }
        });

    } else {
        share.oppObjForTooltip = {};
        _.each(opps,function (op) {

            share.oppObjForTooltip[op.opportunityId] = {
                oppsAmount: op.value?op.value:0,
                contactEmailId:op.contactEmailId
            }
        });

        _.each(interactions,function (ints) {

            ints.oppsAmount = ints.amountWithNgm;

            if(share.oppObjForTooltip[ints.opportunityId]){
                share.oppObjForTooltip[ints.opportunityId].interactionsCount = ints.interactionsCount?ints.interactionsCount:0;

            } else {
                share.oppObjForTooltip[ints.opportunityId] = ints;
            }
        })
    }

    if(reverseSort){

        dataOpps.sort(function(a, b) {
            return a.value - b.value;
        });

        var accIntIndexes = dataOpps.reduce(function(lookup, key, index) {
            lookup[key.axis] = index;
            return lookup;
        }, {});

        dataInts.sort(function(k1, k2) {
            return accIntIndexes[k1.axis] - accIntIndexes[k2.axis];
        });
    } else {

        dataInts.sort(function(a, b) {
            return a.value - b.value;
        });

        var accIntIndexes = dataInts.reduce(function(lookup, key, index) {
            lookup[key.axis] = index;
            return lookup;
        }, {});

        dataOpps.sort(function(k1, k2) {
            return accIntIndexes[k1.axis] - accIntIndexes[k2.axis];
        });
    }

//Data
    var d = [dataInts,dataOpps];

//Options for the Radar chart, other than default
    var mycfg = {
        w: w,
        h: h,
        maxValue: 100,
        levels: 5,
        ExtraWidthX: 300
    }

//Call function to draw the Radar chart
//Will expect that data is in %'s
    RadarChart.draw(id, d, mycfg);

    var svg = d3.select('#body')
        .selectAll('svg')
        .append('svg')
        .attr("width", w+300)
        .attr("height", h)
}

function getOppXLSHeaders(importantHeaders){
    var data = ["Opp Name" ,
        "Opp Owner" ,
        "Contact (selling to)" ,
        "Account",
        "Currency",
        "Amount",
        "Margin",
        "Bottomline",
        "Exchange Rate",
        "Top Line (Primary Currency)",
        "Bottom Line (Primary Currency)",
        "Stage",
        "Close Date",
        "Product",
        "BU ",
        "Solution",
        "Type",
        "Region",
        "City",
        "Source",
        "Vertical",
        "Partners",
        "Created Date",
        "Created By" ,
        "Opportunity Id",
        "Close reasons",
        "Close reasons description" ];
    if(importantHeaders && importantHeaders.length>0){
       data = data.concat(importantHeaders);
    }

    return data;
}

relatasApp.controller("account", function($scope,$http,share,$rootScope){

    window.localStorage.clear();

    $scope.allAcc = {
        name:"opp"
    };

    $scope.sortType = 'openCountSort';
    $scope.sortReverse = false;

    share.getAccountInsights = function(emailId,qtrFilterSelected){
        $scope.loadingMetaData = true;
        $scope.accounts = [];
        accountsInsightVerbose($http,$scope,share,qtrFilterSelected,null,emailId)
    }

    share.filterAccounts = function(stage,oppRange,intRange,start,end){
        if(stage && stage.name){
            $scope.getAccsByFilters($scope.allAcc.name,stage.name,oppRange,intRange,start,end)
        }
    }

    $scope.getAccsByFilters = function(by,stage,oppRange,intRange,start,end){
        accountsInsightVerbose($http,$scope,share,share.accountqtrFilterSelected,by,null,stage,oppRange,intRange,start,end)
    }

    $scope.getAccsBy = function(by){
        share.toggleFySelection(by);
        share.resetAccountFilters()
        accountsInsightVerbose($http,$scope,share,share.accountqtrFilterSelected,by)
    }

    $scope.goToAcc = function (acc) {
        window.location = "/accounts/all?accountName="+acc
    }

});

function accountsInsightVerbose($http,$scope,share,qtrFilterSelected,filter,userEmailId,stage,oppRange,intRange,start,end){

    $("#spiderChart3").empty();
    $scope.loadingMetaData = true;

    try {
        if(window.localStorage['relatasLocalDb']){
            share.responseAccApi = JSON.parse(window.localStorage['relatasLocalDb'])
        }
    } catch (e) {
        console.log("Local Storage Error")
        console.log(e)
    }

    if(filter && share.responseAccApi){

        if(filter === 'all') {
            draw(filterAccounts(share.responseAccApi,stage,oppRange,intRange,start,end))
        } else if(filter === 'ints'){

            share.responseAccApi.accountsInteractions.interactions.sort(function(b, a) {
                return a.interactionsCount - b.interactionsCount;
            });

            var topIntsAccs = share.responseAccApi.accountsInteractions.interactions.slice(0, 100);
            var corrOpps = [];
            var accName = _.map(topIntsAccs,"axis");

            corrOpps = share.responseAccApi.accountsInteractions.opportunities.filter(function (o2) {
                return _.includes(accName,o2.axis);
            });

            draw({
                accountsInteractions:{
                    opportunities:corrOpps,
                    interactions:topIntsAccs
                }
            })
        } else if(filter === 'opp'){
            if(share.responseAccApi.topAccByOppThisQtr){
                share.responseAccApi.topAccByOppThisQtr.opportunities.sort(function(b, a) {
                    return a.oppsCount - b.oppsCount;
                });

                var topOppsAccs = share.responseAccApi.topAccByOppThisQtr.opportunities.slice(0, 100);
                var accName = _.map(topOppsAccs,"axis");

                var corrInts = share.responseAccApi.accountsInteractions.interactions.filter(function (o2) {
                    return _.includes(accName,o2.axis);
                });

                draw(filterAccounts({
                    accountsInteractions:{
                        opportunities:topOppsAccs,
                        interactions:corrInts
                    }
                }, stage,oppRange,intRange,start,end),filter)

            } else {

                draw({
                    accountsInteractions:null
                })
            }
        }
    } else {

        var url = '/reports/dashboard/account/insights';

        var emailId = _.map(share.team,"emailId");
        if(userEmailId && userEmailId !== "all"){
            emailId = [userEmailId]
            if(share.teamChildren[userEmailId]){
                emailId = emailId.concat(share.teamChildren[userEmailId])
            }
        }

        url = fetchUrlWithParameter(url+"?emailId="+emailId)

        if(qtrFilterSelected){
            url = fetchUrlWithParameter(url+"&qStart="+moment(qtrFilterSelected.range.qStart).toISOString())
            url = fetchUrlWithParameter(url+"&qEnd="+moment(qtrFilterSelected.range.qEnd).toISOString())
        }

        $http.get(url)
            .success(function (response) {

                window.localStorage['relatasLocalDb'] = JSON.stringify(response);
                share.responseAccApi = response;
                filter = "opp"; //Default

                if(response && response.accountsInteractions){
                    if(response.accountsInteractions.interactions.length === 0 && response.accountsInteractions.opportunities.length === 0){
                        $scope.disableRadioButtons = true;
                    }
                }

                if(filter){
                    accountsInsightVerbose($http,$scope,share,qtrFilterSelected,filter);
                } else {
                    draw(response)
                }
            })
    }

    function draw(response,reverseSort) {

        $scope.loadingMetaData = false;
        $scope.noAccAndInts = false;

        if(response && response.accountsInteractions) {

            if(response && response.accountsInteractions){
                if(response.accountsInteractions.interactions.length === 0 && response.accountsInteractions.opportunities.length === 0){
                    $scope.noAccAndInts = true;
                }
            }

            spiderChartInit(share,235,true,$scope,function (RadarChart) {

                if(response.accountsInteractions.interactions.length>0 || response.accountsInteractions.opportunities.length>0){
                    spiderDataInit(RadarChart,[],share,300,300,"#spiderChart3"
                        ,response.accountsInteractions.opportunities
                        ,response.accountsInteractions.interactions,true,$scope,reverseSort,$http,true);
                }
            });
        } else {
            $scope.noAccAndInts = true;
        }
    }
}

function filterAccounts(response,stage,oppRange,intRange,start,end){
    var intsObj = {}

    _.each(response.accountsInteractions.interactions,function (el) {
        intsObj[el.axis] = el.interactionsCount;
    });

    if(response && response.accountsInteractions && response.accountsInteractions.opportunities){

        response.accountsInteractions.opportunities
            .forEach(function (el) {
                if(intsObj && intsObj[el.axis]){
                    el.interactionsCount = intsObj[el.axis]
                } else {
                    el.interactionsCount = 0;
                }

                var oppAmt = 0,
                    oppsCount = 0;

                if(el && el.opps){

                    if(stage && stage !== "All"){
                        el.opps = el.opps.filter(function (op) {
                            if(stage === op.stageName){
                                return op;
                            }
                        });
                    }

                    if(start && end && isValidDate(start) && isValidDate(end)){
                        el.opps = el.opps.filter(function (op) {
                            if(new Date(op.closeDate) >= new Date(start) && new Date(op.closeDate) <= new Date(end)){
                                return op;
                            }
                        });

                    } else {

                        if(start && isValidDate(start)){
                            el.opps = el.opps.filter(function (op) {
                                if(new Date(op.closeDate) >= new Date(start)){
                                    return op;
                                }
                            });
                        }

                        if(end && isValidDate(end)){
                            el.opps = el.opps.filter(function (op) {
                                if(new Date(op.closeDate) <= new Date(end)){
                                    return op;
                                }
                            });
                        }
                    }

                    el.opps.forEach(function (op) {
                        oppAmt = oppAmt +op.amount;
                        oppsCount++;
                    });

                    el.value = oppAmt;
                    el.oppsAmount = oppAmt;
                    el.oppsCount = oppsCount;
                }
            });
    }

    if(oppRange && oppRange.range && oppRange.value){
        oppRange.value = parseFloat(oppRange.value);
        response.accountsInteractions.opportunities = response.accountsInteractions.opportunities.filter(function (op) {
            if(oppRange.range == "Less than"){
                if(op.oppsAmount<oppRange.value) {
                    return op;
                }
            } else if(oppRange.range == "Greater than"){
                if(op.oppsAmount>oppRange.value) {
                    return op;
                }
            }
        });
    }

    if(intRange && intRange.range && intRange.value){
        intRange.value = parseFloat(intRange.value);
        response.accountsInteractions.opportunities = response.accountsInteractions.opportunities.filter(function (op) {

            if(intRange.range == "Less than"){
                if(op.interactionsCount<intRange.value) {
                    return op;
                }
            } else if(intRange.range == "Greater than"){
                if(op.interactionsCount>intRange.value) {
                    return op;
                }
            }
        });

        response.accountsInteractions.interactions = response.accountsInteractions.interactions.filter(function (op) {

            if(intRange.range == "Less than"){
                if(op.interactionsCount<intRange.value) {
                    return op;
                }
            } else if(intRange.range == "Greater than"){
                if(op.interactionsCount>intRange.value) {
                    return op;
                }
            }
        });
    }

    return response;
}

function getOppForAccounts($scope,$http,accounts,intsObj,primaryCurrency){

    $scope.accounts = [];
    if(accounts && accounts.length>0){
        _.each(accounts,function (ac) {
            var openCount = 0,
                wonCount = 0,
                lostCount = 0;

            if(ac.opps && ac.opps.length>0){
                _.each(ac.opps,function (op) {
                    if(op.stageName == "Close Won"){
                        wonCount = wonCount+op.amount
                    } else if(op.stageName == "Close Lost"){
                        lostCount = lostCount+op.amount
                    } else {
                        openCount = openCount+op.amount
                    }
                })
            }

            ac.openCount = getAmountInThousands(openCount,2,primaryCurrency == "INR");
            ac.wonCount = getAmountInThousands(wonCount,2,primaryCurrency == "INR");
            ac.lostCount = getAmountInThousands(lostCount,2,primaryCurrency == "INR");

            ac.openCountSort = openCount;
            ac.wonCountSort = wonCount;
            ac.lostCountSort = lostCount;

            if(intsObj && intsObj[ac.axis] && intsObj[ac.axis].count){
                ac.interactionsCount = intsObj[ac.axis].count
                ac.lastInteractedDateFormatted = moment(intsObj[ac.axis].lastInteractedDate).format(standardDateFormat());
                ac.lastInteractedDate = intsObj[ac.axis].lastInteractedDate
            } else {
                ac.interactionsCount = 0;
                ac.lastInteractedDateFormatted = "-"
            }
        })
    }

    $scope.accounts = accounts;
}

relatasApp.controller("regions", function($scope,$http,share,$rootScope) {

    share.regionChart = function (all) {

        var emailIds = [share.selection.emailId]
        if(share.selection.fullName == "Show all team members"){
            emailIds = _.map(share.team, "emailId");
        }

        if($scope.viewFor == 'opps'){
            getRegionChart($http, $scope, share, emailIds)
        } else {

            var url = '/reports/contacts';
            url = fetchUrlWithParameter(url+"?forUserEmailId="+emailIds);

            $http.get(url)
                .success(function (response) {
                    drawRegionContactsChart(response,[0, 0],$scope);
                })
        }
    }

    $scope.goToOpp = function (op) {
        $rootScope.regionTabView = true;
        share.getInteractionHistory(op);
    }

    $scope.loadNoLocData = function(){
        $scope.viewingLocation = "";
        $scope.tbRows = $scope.oppsWithNoLoc;
        $scope.tbRows.forEach(function (el) {
            if(el.personEmailId){
                el.account = fetchCompanyFromEmail(el.personEmailId);
            } else {
                el.account = fetchCompanyFromEmail(el.contactEmailId);
            }
        })
    }

    $scope.sortType = 'amount';
    $scope.sortReverse = false;
    $scope.viewFor = 'opps';

    $scope.getDataFor = function (item,contactType) {

        $scope.regEndDate = moment().format(standardDateFormat());
        $scope.regStartDate = moment().subtract(90,"days").format(standardDateFormat());

        $scope.tbRows = [];

        var emailIds = [share.selection.emailId]
        if(share.selection.fullName == "Show all team members"){
            emailIds = _.map(share.team, "emailId");
        }

        if(item == 'opps'){
            getRegionChart($http, $scope, share, emailIds)
        } else {

            var url = '/reports/contacts';
            url = fetchUrlWithParameter(url+"?forUserEmailId="+emailIds);
            if(contactType){
                url = fetchUrlWithParameter(url+"&contactType="+contactType)
            }

            $http.get(url)
                .success(function (response) {
                    drawRegionContactsChart(response,[0, 0],$scope);
                })
        }
    }

    $scope.sortTable = function (item) {

        var sorType = "amount";

        if(item == "Opportunity Name"){
            sorType = "opportunityName"
        }

        if(item == "Company"){
            sorType = "account"
        }
        if(item == "Stage"){
            sorType = "relatasStage"
        }
        if(item == "Sales Person"){
            sorType = "userEmailId"
        }

        if(item == "Selling To"){
            sorType = "contactEmailId"
        }

        if(item == "Contact Name"){
            sorType = "personEmailId"
        }

        if(item == "Type"){
            sorType = "prospect_customer"
        }

        if(item == "Contact Owner"){
            sorType = "ownerEmailId"
        }

        if(item == "Last Interacted"){
            sorType = "lastInteractedDate"
        }

        $scope.sortReverse = !$scope.sortReverse;
        $scope.sortType = sorType;
    }

    $scope.regEndDate = moment().format(standardDateFormat());
    $scope.regStartDate = moment().subtract(90,"days").format(standardDateFormat());

    $scope.applyFilter = function(){

        $scope.tbRows = [];
        if(share.selection && share.selection.emailId == "Show all team members"){
            getRegionChart($http, $scope, share, _.map(share.team, "emailId"),$scope.opp.stage,$scope.regStartDate,$scope.regEndDate)
        } else {
            getRegionChart($http, $scope, share, [share.selection.emailId],$scope.opp.stage,$scope.regStartDate,$scope.regEndDate)
        }
    }

    $scope.registerDatePickerId = function(id){
        $('#'+id).datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    $scope[id] = moment(dp).format(standardDateFormat());
                });
            }
        });
    }

    getOppStages();

    function getOppStages(){

        if(share.opportunityStages){
            $scope.opp = {};
            $scope.stagesSelection = ["Show All Opportunities"];
            $scope.stagesSelection = $scope.stagesSelection.concat(_.map(share.opportunityStages,"name"));
            $scope.opp.stage = $scope.stagesSelection[0];
        } else {
            setTimeOutCallback(500,function () {
                getOppStages()
            })
        }
    }

    $scope.contactTypes = ["All contacts","Contact","Lead","Prospect","Customer"];
    $scope.contact = {
        type:"All contacts"
    }

    $scope.getContactsForType = function (type) {
        $scope.getDataFor("contacts",type == 'All contacts'?null:type)
    }

    $scope.goToContact = function (contact) {
        window.location = '/contacts/all?contact='+contact.personEmailId+'&acc=true';
    }

    $scope.displayContacts = function (loc) {

        $scope.loadingOpps = true;
        if($scope.contactsObj[loc]){
            // $scope.viewingLocation = $scope.contactsObj[loc][0].location.replace(/ /g, ',');
            $scope.viewingLocation = $scope.contactsObj[loc][0].location;
        }

        if($scope.contactsObj[loc]){
            var url = "/reports/contact/details";
            url = fetchUrlWithParameter(url+"?emailIds="+_.map($scope.contactsObj[loc],"personEmailId"));
            var all = share.selection.fullName == "Show all team members";
            if(all){
                url = fetchUrlWithParameter(url+"&forUserEmailId="+_.map(share.team, "emailId"))
            }

            $scope.tbHeaders = ["Contact Name", "Company", "Type", "Amount", "Contact Owner", "Last Interacted"];

            $http.get(url)
                .success(function (response) {
                    $scope.tbRows = $scope.contactsObj[loc];

                    if(response){
                        $scope.tbRows.forEach(function (el) {
                            var d = response[el.ownerEmailId+el.personEmailId];

                            if(!el.contactRelation || (el.contactRelation && !el.contactRelation.prospect_customer)){
                                el.contactRelation = {
                                    prospect_customer: "Contact"
                                }
                            }

                            if(!el.amount){
                                el.amount = 0;
                            }

                            el.account = fetchCompanyFromEmail(el.personEmailId);
                            if(d){
                                el.amount = d.amount?d.amount:0;
                                el.lastInteractedDateFormatted = d.lastInteractedDate?moment(d.lastInteractedDate).format(standardDateFormat()):"-";
                                if(d.lastInteractedDate){
                                    el.lastInteractedDate = new Date(d.lastInteractedDate);
                                }

                                if(d.lastInteractedDate && (new Date(d.lastInteractedDate)< new Date(moment().subtract(60,"days")))){
                                    el.highlightContact = "lost";
                                }
                            } else {
                                el.highlightContact = "lost";
                                el.lastInteractedDateFormatted = "-"
                            }

                            if(!el.lastInteractedDateFormatted){
                                el.highlightContact = "lost";
                            }

                            if(el.lastInteractedDateFormatted == "-"){
                                el.highlightContact = "lost";
                                el.lastInteractedDate = new Date(moment().subtract(99,"years"));
                            }

                        });
                    }

                    $scope.loadingOpps = false;
                })
        }
    }

    $scope.displayOpps = function (loc) {
        $scope.tbRows = [];
        if($scope.regionOpps[loc]){

            $scope.tbRows = $scope.regionOpps[loc].opps;
            if($scope.tbRows[0] && $scope.tbRows[0].geoLocation && $scope.tbRows[0].geoLocation.town){
                $scope.viewingLocation = $scope.tbRows[0].geoLocation.town;
                // $scope.viewingLocation = $scope.viewingLocation.replace(/ /g, ',');
            }
        }

        $scope.tbRows.forEach(function (op) {
            op.isNotOwner = false;
            op.isOppClosed = _.includes(op.relatasStage.toLowerCase(),"close");

            if(op.isOppClosed){
                if(share.liuData.corporateAdmin){
                    op.isNotOwner = false;
                } else {
                    op.isNotOwner = true;
                }
            }

            op.account = fetchCompanyFromEmail(op.contactEmailId);
        });

        $scope.loadingOpps = false;
    }
});

relatasApp.controller("dashboard", function($scope,$http,share,$rootScope){

    $scope.togglePvHelp = function(){
        $scope.openPvHelp = !$scope.openPvHelp
    }

    $scope.toggleDRHelp = function(){
        $scope.openDRHelp = !$scope.openDRHelp
    }

    $scope.toggleAIHelp = function(){
        $scope.openAIHelp = !$scope.openAIHelp
    }

    $scope.toggleOIHelp = function(){
        $scope.openOIHelp = !$scope.openOIHelp
    }

    $scope.goToAccount = function(){
        share.openViewFor({
            name:"Accounts",
            selected:""
        })
    }

    share.setLoaders2 = function(){
        $scope.loadingMetaData = true;
    }

    share.getDashBoardInsights = function(emailId,qtrFilterSelected){
        $scope.loadingMetaData = true;
        checkLiuDataLoaded(emailId,qtrFilterSelected)
    }

    $scope.getAccsBy = function(by){
        accountsInsights($http,$scope,share,share.accountqtrFilterSelected,by)
    }

    $scope.getOppsBy = function(by){
        oppsInsights($http,$scope,share,share.accountqtrFilterSelected,by)
    }

    function checkLiuDataLoaded(emailId,qtrFilterSelected){

        $scope.allAcc = {
            name:"opp"
        };
        $scope.oppInt = {
            name: "toOpp"
        };

        if(share.liuData){

            var url = '/reports/dashboard/insights';

            if(emailId){
                url = fetchUrlWithParameter(url+"?emailId="+emailId)
            }

            if(qtrFilterSelected && qtrFilterSelected.range){
                url = fetchUrlWithParameter(url+"&qStart="+moment(qtrFilterSelected.range.qStart).toISOString())
                url = fetchUrlWithParameter(url+"&qEnd="+moment(qtrFilterSelected.range.qEnd).toISOString())
            }

            share.accountEmailId = emailId;
            share.accountqtrFilterSelected = qtrFilterSelected;

            accountsInsights($http,$scope,share,qtrFilterSelected,null,emailId);

            $http.get(url)
                .success(function (response) {

                    var target = 0;

                    $scope.noOppAndInts = false;
                    $scope.loadingMetaData = false;

                    share.apiResponse = response;

                    $(".accChart").empty();
                    $(".donutGraphWon").empty();
                    $(".donutGraphLost").empty();

                    if(response && response.pipelineFlow && response.pipelineFlow.oppStages){
                        $rootScope.stages = response.pipelineFlow.oppStages
                    }

                    $scope.allTeamMembers = response.forTeam;
                    $scope.targetGraph = [];

                    if(response && response.pipelineVelocity && response.pipelineVelocity.length>0){
                        $scope.targetGraph = response.pipelineVelocity
                    }

                    if($scope.targetGraph && $scope.targetGraph.length>0){
                        $scope.targetGraph.forEach(function (tr) {
                            target = target+parseFloat(tr.target.replace (/,/g, ""))
                        });
                    }

                    share.forAccsCreated(response?response.newCompaniesInteracted:null)
                    share.forOppGrowth(response && response.conversionRate && response.conversionRate[0]?response.conversionRate[0]:null);

                    share.forSnapshot(response && response.pipelineFunnel?response.pipelineFunnel:null)
                    if(response){
                        share.opp_props(response.typesWon,response.sourcesWon,response.productsWon)
                    } else {
                        share.opp_props([],[],[])
                    }

                    drawSankeyGraph(response && response.pipelineFlow?response.pipelineFlow:null,share,$scope);

                    var reasons = {
                        won:groupAndChainForTeamSummary(response.reasonsWon),
                        lost:groupAndChainForTeamSummary(response.reasonsLost)
                    };

                    $scope.noWon = reasons.won.length === 0;
                    $scope.noLost = reasons.lost.length === 0;

                    donutChart(reasons.won,".donutGraphWon",shadeGenerator(0,150,136,reasons.won.length,15),60,60)
                    donutChart(reasons.lost,".donutGraphLost",shadeGenerator(244,67,54,reasons.lost.length,15),60,60);

                    if(response){

                        if(!response.dealsAtRisk){
                            response.dealsAtRisk = {
                                "count": 0,
                                "amount": 0,
                                dealsRiskAsOfDate : new Date()
                            }
                        }
                        share.dealsAtRiskCount = response.dealsAtRisk.count
                        share.totalDealValueAtRisk = response.dealsAtRisk.amount
                        share.dealsRiskAsOfDate = response.dealsAtRisk.dealsRiskAsOfDate

                        drawPipeline($scope,response.oppWon.amount,response.oppLost.amount,response.oppWon.count,response.oppLost.count
                            ,0,0,0,0,response.dealsAtRisk.count,
                            response.dealsAtRisk.amount,response.dealsAtRisk.dealsRiskAsOfDate,response.renewalOpen.amount,response.renewalOpen.count,response.stale.amount,response.stale.count,share,target)

                        treemapChart(response.accountsWon,$scope,share);
                    } else {
                        treemapChart([],$scope,share);
                        drawPipeline($scope,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,share,target)
                    }

                    if(response && response.oppsInteractions){
                        if(response.oppsInteractions.interactions.length === 0 && response.oppsInteractions.opportunities.length === 0){
                            $scope.noOppAndInts = true;
                        }
                    } else {
                        $scope.noOppAndInts = true;
                    }

                    oppsInsights($http,$scope,share,qtrFilterSelected,$scope.oppInt.name);

                    if(response.currentInsights && response.currentInsights[0]){
                        share.getCurrentInsights({Data:response.currentInsights[0]})
                    } else {
                        share.getCurrentInsights({Data:null})
                    }

                })
        } else {
            setTimeOutCallback(200,function () {
                checkLiuDataLoaded(emailId,qtrFilterSelected)
            })
        }
    }

    $scope.loadingMetaData = true;

    share.newOppsCreated = function (data) {

        function checkQuarterRangeLoaded(){
            if(share.quarterRange){

                $scope.newOppsAdded = _.sumBy(data,function (el) {
                    if(new Date(el.sortDate) >= new Date(share.quarterRange.qStart) && new Date(el.sortDate) <= new Date(share.quarterRange.qEnd)){
                        return el.count
                    }
                })
            } else {
                setTimeOutCallback(1000,function () {
                    checkQuarterRangeLoaded()
                })
            }
        }

        checkQuarterRangeLoaded()
    }

    $scope.filterOpps = function (item) {
        share.filterOpps(item,true)
        share.openViewFor({
            name:"Opportunity",
            selected:""
        },true)
    }

    $rootScope.allTeamMembers = true;
    share.setAllTeamMembers = function (settings) {
        $rootScope.allTeamMembers = settings
    }

    $scope.showTable = function (table) {

        if(share.selection && share.selection.emailId == "Show all team members"){
            alert("Pipeline velocity insights not available for team. Please select individual team members to view pipeline velocity");
        } else {

            if(table == "pipelineVelocity"){
                $scope.pipelineVelocityCss = "insight-selection"
                var userId = share.selection?share.selection.userId:share.liuData.userId;
                share.showPipelineVelocity(true,userId);
            }
        }
    }

    $scope.getDetails = function (colType) {

        if(colType.colType == "Deals At Risk"){

            if(share.selection && share.selection.emailId == "Show all team members"){
                alert("Deals at risk insights not available for team. Please select individual team members to view deals at risk");
            } else {
                var userId = share.selection?share.selection.userId:share.liuData.userId;
                share.forDealsAtRisk(userId);
            }
        }
    }

});

function accountsInsights($http,$scope,share,qtrFilterSelected,filter,userEmailId){

    $("#spiderChart2").empty();
    if(filter && share.responseAccApi){

        if(filter === 'all') {
            draw(share.responseAccApi)
        } else if(filter === 'ints'){

            share.responseAccApi.accountsInteractions.interactions.sort(function(b, a) {
                return a.interactionsCount - b.interactionsCount;
            });

            var topIntsAccs = share.responseAccApi.accountsInteractions.interactions.slice(0, 100);

            var corrOpps = share.responseAccApi.accountsInteractions.opportunities;
            var accName = _.map(topIntsAccs,"axis");

            if(topIntsAccs.length>0){
                corrOpps = share.responseAccApi.accountsInteractions.opportunities.filter(function (o2) {
                    return _.includes(accName,o2.axis);
                });
            }

            draw({
                accountsInteractions:{
                    opportunities:corrOpps,
                    interactions:topIntsAccs
                }
            })
        } else if(filter === 'opp'){
            if(share.responseAccApi.topAccByOppThisQtr){
                share.responseAccApi.topAccByOppThisQtr.opportunities.sort(function(b, a) {
                    return a.oppsCount - b.oppsCount;
                });

                var topOppsAccs = share.responseAccApi.topAccByOppThisQtr.opportunities.slice(0, 100);

                var corrInts = share.responseAccApi.accountsInteractions.interactions;
                if(topOppsAccs.length>0){
                    var accName = _.map(topOppsAccs,"axis");
                    corrInts = share.responseAccApi.accountsInteractions.interactions.filter(function (o2) {
                        return _.includes(accName,o2.axis);
                    });
                }

                draw({
                    accountsInteractions:{
                        opportunities:topOppsAccs,
                        interactions:corrInts
                    }
                },filter)
            } else {

                draw({
                    accountsInteractions:null
                })
            }
        }
    } else {

        var url = '/reports/dashboard/account/insights';

        var emailId = _.map(share.team,"emailId");
        if(userEmailId && userEmailId !== "all"){
            emailId = [userEmailId]
            if(share.teamChildren[userEmailId]){
                emailId = emailId.concat(share.teamChildren[userEmailId])
            }
        }

        url = fetchUrlWithParameter(url+"?emailId="+emailId)

        if(qtrFilterSelected){
            url = fetchUrlWithParameter(url+"&qStart="+moment(qtrFilterSelected.range.qStart).toISOString())
            url = fetchUrlWithParameter(url+"&qEnd="+moment(qtrFilterSelected.range.qEnd).toISOString())
        }

        $http.get(url)
            .success(function (response) {

                share.responseAccApi = response;
                filter = "opp"; //Default

                if(response && response.accountsInteractions){
                    if(response.accountsInteractions.interactions.length === 0 && response.accountsInteractions.opportunities.length === 0){
                        $scope.disableRadioButtons = true;
                    }
                }

                if(filter){
                    accountsInsights($http,$scope,share,qtrFilterSelected,filter);
                } else {
                    draw(response)
                }
            })
    }

    function draw(response,reverseSort) {

        $scope.noAccAndInts = false;

        if(response && response.accountsInteractions) {

            if(response && response.accountsInteractions){
                if(response.accountsInteractions.interactions.length === 0 && response.accountsInteractions.opportunities.length === 0){
                    $scope.noAccAndInts = true;
                }
            }

            if(reverseSort == "opp" && response.accountsInteractions.opportunities.length === 0){
                $scope.noAccAndInts = true;
            }

            spiderChartInit(share,35,true,$scope,function (RadarChart) {

                if(response.accountsInteractions.interactions.length>0 || response.accountsInteractions.opportunities.length>0){
                    spiderDataInit(RadarChart,[],share,200,200,"#spiderChart2"
                        ,response.accountsInteractions.opportunities
                        ,response.accountsInteractions.interactions,true,$scope,reverseSort);
                }
            });
        } else {
            $scope.noAccAndInts = true;
        }

    }
}

function oppsInsights($http,$scope,share,qtrFilterSelected,filter){

    $("#spiderChart1").empty();

    if(filter){

        if(filter === 'all') {
            draw(share.apiResponse)
        } else if(filter === 'opp' || filter === 'toOpp'){

            if(share.apiResponse){
                share.apiResponse.topOppByOppThisQtr.opportunities.sort(function(b, a) {
                    return a.value - b.value;
                });

                var topOppsAccs = share.apiResponse.topOppByOppThisQtr.opportunities.slice(0, 100);
                var accName = _.map(topOppsAccs,"axis");

                var corrInts = share.apiResponse.topOppByOppThisQtr.interactions.filter(function (o2) {
                    return _.includes(accName,o2.axis);
                });
            }

            draw({
                oppsInteractions:{
                    opportunities:topOppsAccs,
                    interactions:corrInts
                }
            },filter)
        }
    } else {
        draw(share.apiResponse)
    }

    function draw(response,filter) {

        if(response && response.oppsInteractions) {
            spiderChartInit(share,135,false,$scope,function (RadarChart) {

                if(!response.oppsInteractions.interactions){
                    response.oppsInteractions.interactions = []
                }
                if(!response.oppsInteractions.opportunities){
                    response.oppsInteractions.opportunities = []
                }

                if(response.oppsInteractions.interactions.length>0 || response.oppsInteractions.opportunities.length>0){
                    spiderDataInit(RadarChart,[],share,200,200,"#spiderChart1"
                        ,response.oppsInteractions.opportunities
                        ,response.oppsInteractions.interactions,false,$scope,filter);
                } else {
                    $scope.noOppAndInts = true;
                }
            });
        }
    }
}

relatasApp.controller("pipeline_velocity",function ($scope,$http,share,$rootScope) {

    $scope.closePipelineVelocity = function () {
        $scope.openPipelineVelocity = false;
    }

    $scope.takeAction = function (opp) {

        if(!$rootScope.noAccess){
            $scope.opp = opp;
            $scope.showModal = true;
        }
    }

    $scope.closeModal = function () {
        $scope.showModal = false;
    }

    $scope.sortType = 'closeDate';
    $scope.sortReverse = false;

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.registerDatePickerId = function(){

        $('#opportunityCloseDateSelector4').datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    updateOpportunity($scope,$http,$scope.opp,"closeDate",dp,false,function (result) {
                        if(result){
                            share.refreshDealsAtRisk();
                            $scope.opp.formattedCloseDate = moment(dp).format("DD MMM YYYY");
                        } else {

                        }
                    })
                });
            }
        });
    }

    share.showPipelineVelocity = function (value,userId) {
        $scope.openPipelineVelocity = value;
        getPipelineVelocity(userId)
    }

    $scope.goTo = function () {
        window.location = "/opportunities/all"
    }

    share.forPipelineVelocity = function (userId,accessControl) {
        getPipelineVelocity(userId,accessControl)
    }

    function getPipelineVelocity (userId,accessControl){

        var url = '/insights/pipeline/velocity'
        if(userId){
            url = url+"?userId="+userId;
        }

        if(accessControl){

            if(share.liuData && share.liuData.orgHead){
                url = fetchUrlWithParameter(url+"&accessControl="+true)
                url = fetchUrlWithParameter(url+"&companyId="+share.liuData.companyId)
            } else {
                url = fetchUrlWithParameter(url+"&accessControl="+true)
            }
        }

        $http.get(url)
            .success(function (response) {
                if(response && response.SuccessCode){

                    var opportunityStages = {};

                    if(share.opportunityStages){
                        _.each(share.opportunityStages,function (op) {
                            opportunityStages[op.name] = op.order;
                        })
                    }

                    $scope.expectedPipeline = response.Data.expectedPipeline
                    $scope.deals = response.Data.oppNextQ && response.Data.oppNextQ[0] && response.Data.oppNextQ[0].opportunities?response.Data.oppNextQ[0].opportunities:[];
                    $scope.currentQuarter = response.Data.currentQuarter;

                    var allValues = [];
                    var target = response.Data.currentTargets[0]? response.Data.currentTargets[0].target:0

                    var pipeline = 0,won=0;
                    _.each(response.Data.currentOopPipeline,function (op) {

                        if(op._id == "Close Won"){
                            won = won+op.sumOfAmount
                        }

                        if(op._id != "Close Won" && op._id != "Close Lost"){
                            pipeline = pipeline+op.sumOfAmount
                        }
                    });

                    $scope.staleOppsExist = false;
                    $scope.nextQuarterOppsExist = false;

                    var gap = target - won;

                    allValues.push(target)
                    allValues.push(pipeline)
                    allValues.push(won)
                    allValues.push(gap)

                    $scope.targetCount = numberWithCommas(target.r_formatNumber(2),share.primaryCurrency == "INR");
                    $scope.pipelineCount = numberWithCommas(pipeline.r_formatNumber(2),share.primaryCurrency == "INR");
                    $scope.wonCount = numberWithCommas(won.r_formatNumber(2),share.primaryCurrency == "INR");
                    $scope.gapCount = numberWithCommas(gap.r_formatNumber(2),share.primaryCurrency == "INR");

                    var max = _.max(allValues);
                    var min = _.min(allValues);

                    $scope.target = {'width':scaleBetween(target,min,max)+'%',background: '#FE9E83'}
                    $scope.pipeline = {'width':scaleBetween(pipeline,min,max)+'%',background: '#767777'}
                    $scope.won = {'width':scaleBetween(won,min,max)+'%',background: '#8ECECB'}
                    $scope.gap = {'width':scaleBetween(gap,min,max)+'%',background: '#e74c3c'}

                    if(won>target){
                        $scope.expectationsExceed = true;
                    }

                    if($scope.deals.length>0){

                        $scope.nextQuarterOppsExist = true;

                        _.each($scope.deals,function (deal) {
                            deal.amountWithCommas = numberWithCommas(parseFloat(deal.amount.r_formatNumber(2)),share.primaryCurrency == "INR")
                            deal.formattedCloseDate = moment(deal.closeDate).format("DD MMM YYYY")
                            deal.company = fetchCompanyFromEmail(deal.contactEmailId)
                            deal.isStale = false
                            deal.suggestion = "Suggest moving this opportunity closing next quarter to current quarter."
                        })
                    }

                    if(response.Data.staleOpps && response.Data.staleOpps.length>0){

                        $scope.staleOppsExist = true;

                        _.each(response.Data.staleOpps,function (deal) {
                            deal.amountWithCommas = numberWithCommas(parseFloat(deal.amount).r_formatNumber(2),share.primaryCurrency == "INR")
                            deal.formattedCloseDate = moment(deal.closeDate).format("DD MMM YYYY")
                            deal.company = fetchCompanyFromEmail(deal.contactEmailId)
                            deal.isStale = true
                            deal.suggestion = "This is a stale opportunity. \n Move this deal to current quarter to meet your target or close the opportunity."

                            $scope.deals.push(deal)
                        })
                    }

                    if(response.Data.currentTargets && response.Data.currentTargets[0] && response.Data.currentTargets[0].target || accessControl){

                        $scope.actionRequired = true;
                        if(response.Data.expectedPipeline>response.Data.currentTargets[0].target || accessControl){
                            $scope.actionRequired = true;
                        }
                    }

                    if(!target && !pipeline){
                        $scope.targetPipelineNone = true;
                    }

                    if(pipeline>target){
                        $scope.targetPipelineNone = false;
                    }

                    if(won>target){
                        $scope.actionRequired = false;
                    }

                    if(target) {

                        if(!won){
                            $scope.actionRequired = true;
                            if(pipeline>=target){
                                $scope.actionRequired = false;
                            }

                        } else {
                            gap = target-won;
                            var wonPercentage = (won/target)*100;
                            var targetPipelineGap = pipeline-won;

                            //targetPipelineGap is the remaining pipeline after achievement, which still can be won
                            //Gap is the minimum won amount required to meet quarter target.

                            if(targetPipelineGap>gap && wonPercentage>=100){
                                $scope.actionRequired = false;
                                $scope.expectationsExceed = true;
                            }

                            if(target>pipeline && gap>0){
                                $scope.actionRequired = true;
                            }
                        }
                    }

                    _.each($scope.deals,function (deal) {
                        deal.stageStyle2 = oppStageStyle(deal.stageName,opportunityStages[deal.stageName]-1,true);
                    })

                } else {
                    $scope.deals = []
                    $scope.target = {}
                    $scope.pipeline = {}
                    $scope.won = {}
                    $scope.gap = {}

                    $scope.targetCount = 0;
                    $scope.pipelineCount = 0;
                    $scope.wonCount = 0;
                    $scope.gapCount = 0;
                }
            });
    }

});

relatasApp.controller("deals_at_risk",function ($scope,$http,share,searchService,$rootScope) {

    function getDealsAtRisk(userId,accessControl) {

        var url = "/insights/deals/at/risk"
        if(userId){
            url = url+"?userIds="+userId;
        }

        if(accessControl && !$rootScope.orgHead){
            url = fetchUrlWithParameter(url+"&accessControl="+true)
        }

        $scope.showDealsAtRisk = true;
        $scope.loadingDealsAtRisk = true;

        $http.get(url)
            .success(function (response) {
                if(response && response.deals){
                    dealsAtRiskGraph($scope,share,response.deals,response.averageRisk)
                }
            });
    }

    share.forDealsAtRisk = function (userId,accessControl) {
        getDealsAtRisk(userId,accessControl)
    }

    share.setTeamMembers = function (usersDictionary,usersArray) {

        var ids = _.map(usersArray,"_id");
        var url = "/insights/deals/at/risk/team/meta";
        url = fetchUrlWithParameter(url,'userIds',ids)

        $http.get(url)
            .success(function (response) {
                var team_atRisk = 0;
                $scope.teamDealsAtRisk = response;
                if(response && response.length>0) {
                    _.each(response,function (el) {
                        team_atRisk = team_atRisk+el.count;
                    })
                }
                share.teamRiskData(team_atRisk)
            });
    };

    share.refreshDealsAtRisk = function () {
        getDealsAtRisk();
    }

    $scope.closeDealsAtRisk = function () {
        $scope.showDealsAtRisk = false;
    }

    $scope.goToContact = function(emailId){
        window.location = '/contacts/all?contact='+emailId+'&acc=true'
    };

    $scope.sortType = 'closeDate';
    $scope.sortReverse = false;

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

    $scope.oppUpdateStage = function (stage) {

        updateOpportunity($scope,$http,$scope.opp,"stageName",stage,false,function (result) {
            toastr.success("Stage updated successfully");
            $scope.isStagnant = "fa-check-circle-o";
            $scope.stagnantStatus = true;
            $scope.stagnantDaysAgo = "Stage was last updated today";
            share.refreshDealsAtRisk();
            share.refreshPipelineSnaphot();
        });
    }

    $scope.getSuggestions = function (deal) {

        if(!$rootScope.noAccess){

            $scope.openDMs(); // Default open DMs

            $scope.dmActiveClass = "active"
            $scope.iActiveClass = "inactive"
            $scope.stagActiveClass = "inactive"
            $scope.staleActiveClass = "inactive"
            $scope.ciActiveClass = "inactive"
            $scope.mActiveClass = "inactive"
            $scope.compIActiveClass = "inactive"
            $scope.ltActiveClass = "inactive"

            $scope.totalValue = $scope.totalPipeLineValue?numberWithCommas($scope.totalPipeLineValue.r_formatNumber(2),share.primaryCurrency == "INR"):"";

            var percentageAtRisk = calculatePercentage(deal.amount,$scope.totalPipeLineValue);

            if(percentageAtRisk == 0){
                $scope.percentageOfTotal = "< 1% of "
            } else {
                $scope.percentageOfTotal = percentageAtRisk+"% of "
            }

            if(!$scope.totalPipeLineValue || !deal.amount){
                $scope.percentageOfTotal = false;
            }

            $scope.dmExist = "fa-exclamation-circle";
            $scope.InfExist = "fa-exclamation-circle";
            $scope.isStaleOpp = "fa-exclamation-circle";
            $scope.isStagnant = "fa-exclamation-circle";
            $scope.metDmInfl = "fa-exclamation-circle";
            $scope.IntScr = "fa-exclamation-circle";
            $scope.ltWithOwner = "fa-exclamation-circle";
            $scope.contatIntr = "fa-exclamation-circle";

            if(!deal.ltWithOwner){
                $scope.ltWithOwner = "fa-check-circle-o"
                $scope.ltTrue = false;
            } else {
                $scope.ltTrue = true;
            }

            if(!deal.skewedTwoWayInteractions){
                $scope.contatIntr = "fa-check-circle-o"
            }

            if(deal.daysSinceStageUpdated<45){
                $scope.isStagnant = "fa-check-circle-o"
                $scope.stagnantStatus = true;
            } else {
                $scope.stagnantStatus = false;
            }

            $scope.stagnantDaysAgo = "Stage was last updated "+deal.daysSinceStageUpdated + " days back";

            if(deal.daysSinceStageUpdated == 0){
                $scope.stagnantDaysAgo = "Stage was last updated today";
            }

            $scope.metDecisionMaker_infuencer = false;
            if(deal.metDecisionMaker_infuencer){
                $scope.metDecisionMaker_infuencer = true;
                $scope.metDmInfl = "fa-check-circle-o"
            }

            $scope.companyIntr = false;

            if(deal.averageInteractionsPerDeal){
                $scope.companyIntr = true;
                $scope.IntScr = "fa-check-circle-o"
            }

            if(share.usersDictionary[deal.userEmailId]){
                deal.owner = share.usersDictionary[deal.userEmailId]
            }

            $scope.opportunityName = deal.opportunityName;
            $scope.contactEmailId = deal.contactEmailId;

            $scope.closeDate = moment(deal.closeDate).format("DD MMM YYYY");
            $scope.oppCreatedDate = deal.createdDate?moment(deal.createdDate).format("DD MMM YYYY"):'';

            $scope.opp = deal;

            $scope.showModal = true;
            $scope.noDMs = !deal.decisionMakersExist;
            $scope.noInfl = !deal.influencersExist;

            if(deal.decisionMakersExist){
                $scope.dmExist = "fa-check-circle-o"
            }

            if(deal.influencersExist){
                $scope.InfExist = "fa-check-circle-o"
            }

            if(new Date(deal.closeDate)< new Date()){
                $scope.staleOpp = true;
            } else {
                $scope.staleOpp = false
                $scope.isStaleOpp = "fa-check-circle-o"
            }

            $scope.stageUpdated = deal.stageName;

            getLiu();
        }

    }

    $scope.openDMs = function () {
        $scope.showStagOpp = false;
        $scope.showDms = true;
        $scope.showContInt = false;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "active"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"

    }

    $scope.openInfl = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showInfl = true;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "active"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $scope.openStaleOpp = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showStaleOpp = true;

        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "active"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $scope.openDmOrInfMet = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showMetDmInf = true;

        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "active"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"

        if($scope.opp.dmsInfls[0]){
            getLiu($scope.opp.dmsInfls[0]);
        }
    }

    $scope.openIntScore = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showIntScr = true;

        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showLt = false;
        $scope.closeThis = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "active"
        $scope.ltActiveClass = "inactive"

    }

    $scope.openLt = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = false;
        $scope.showIntScr = false;
        $scope.showMetDmInf = false;
        $scope.showStaleOpp = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.closeThis = false;
        $scope.showLt = true;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "active"

    }

    $scope.openStagOpp = function () {

        $scope.showContInt = false;
        $scope.showStagOpp = true;
        $scope.showStaleOpp = false;
        $scope.closeThis = false;
        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "active"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "inactive"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    };

    $scope.openContactIntr = function () {
        $scope.showContInt = true;
        $scope.showStagOpp = false;
        $scope.showStaleOpp = false;
        $scope.closeThis = false;
        $scope.showMetDmInf = false;
        $scope.showInfl = false;
        $scope.showDms = false;
        $scope.showIntScr = false;
        $scope.showLt = false;

        $scope.dmActiveClass = "inactive"
        $scope.iActiveClass = "inactive"
        $scope.stagActiveClass = "inactive"
        $scope.staleActiveClass = "inactive"
        $scope.ciActiveClass = "active"
        $scope.mActiveClass = "inactive"
        $scope.compIActiveClass = "inactive"
        $scope.ltActiveClass = "inactive"
    }

    $(document).on('keyup',function(evt) {
        if (evt.keyCode == 27) {
            $scope.$apply(function (){
                $scope.showModal = false;
                // resetSuggestions($scope)
            })
        }
    });

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.removeRecipient = function(contact,type){
        removeRecipient($scope,$http,contact,type)
    }

    $scope.closeModal = function () {
        $scope.showModal = false;
    }

    $scope.oppCloseLost = function () {
        updateOpportunity($scope,$http,$scope.opp,"closeDate",new Date(),true,function (result) {
            if(result){
                share.refreshClosingSoonDeals()
                share.refreshDealsAtRisk()
                share.refreshPipelineSnaphot()
                share.refreshTarget()
                $scope.closeModal();

                toastr.success("Opportunity closed.")
            } else {
                toastr.error("An error occurred while closing this opportunity. Please try later")
            }
        })
    }

    $scope.registerDatePickerId = function(){

        $('#opportunityCloseDateSelector2').datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            minDate: new Date(),
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    updateOpportunity($scope,$http,$scope.opp,"closeDate",dp,false,function (result) {
                        if(result){
                            share.refreshDealsAtRisk();
                            $scope.closeDate = moment(dp).format("DD MMM YYYY");
                            $scope.isStaleOpp = "fa-check-circle-o";
                            $scope.staleOpp = false;
                        } else {

                        }
                    })
                });
            }
        });
    }

    function getLiu(emailId) {

        if(share.liuData){

            var signature = "\n\n"+getSignature(share.liuData.firstName + ' '+ share.liuData.lastName,
                share.liuData.designation,
                share.liuData.companyName,
                share.liuData.publicProfileUrl)

            var interactionsFor = emailId?emailId:$scope.contactEmailId;

            getLastInteractedDetailsMessage($scope,$http,interactionsFor,share.liuData.firstName,share.liuData.publicProfileUrl,function (message) {

                $scope.subject = message.subject;
                $scope.body = message.body+signature;

                $scope.subjectLt = message.subject;
                $scope.subjectCi = message.subject;
                $scope.bodyLt = "\n\n"+signature;

            });

        } else {
            setTimeOutCallback(100,function () {
                getLiu()
            });
        }
    }

    $scope.sendEmail = function (subject,body,reason,contactEmailId) {

        var contactDetails = {
            contactEmailId: contactEmailId?contactEmailId:$scope.opp.contactEmailId,
            personId:null,
            personName:null
        }

        sendEmail($scope,$http,subject,body,contactDetails,reason)
    }

});

relatasApp.controller("acc_growth",function ($scope,$http,share) {
    $scope.loadingMetaData = true;
    share.forAccsCreated = function (newCompaniesInteracted) {
        $scope.loadingMetaData = true;
        getAccGrowth(newCompaniesInteracted)
    }

    function getAccGrowth(response){
        var label = [], series = [];

        if(response) {
            response.sort(function (o1, o2) {
                return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
            });

            _.each(response, function (el) {
                var month = monthNames[moment(new Date(el.sortDate)).month()];
                var monthYear = monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year();
                label.push(month.substring(0, 3));
                series.push({meta:monthYear,value:el.accountDetails.length});
            });

            $scope.noAcc = _.sumBy(series,"value") === 0;
            drawLineChart($scope, share, series, label, ".acc-growth")
        } else {
            $scope.noAcc = true;
        }

        $scope.loadingMetaData = false;
    }
});

relatasApp.controller("opp_growth",function ($scope,$http,share) {

    $scope.loadingMetaData = true;
    share.forOppGrowth = function (response) {

        $scope.loadingMetaData = true;
        getOppGrowth(response)
    }

    function getOppGrowth(response) {
        if (response) {

            $scope.noOpp = true;
            var label = [], seriesOpen = [], seriesClose = [];

            function comparer(otherArray){
                return function(current){
                    return otherArray.filter(function(other){
                        return other.monthYear == current.monthYear
                    }).length == 0;
                }
            }

            var onlyInA = response.created.filter(comparer(response.closed));
            var onlyInB = response.closed.filter(comparer(response.created));

            if(onlyInA.length>0){
                var onlyInAObj = [];
                _.each(onlyInA,function (el) {
                    onlyInAObj.push(el.monthYear);
                })
            }

            if(onlyInB.length>0){
                var onlyInBObj = [];
                _.each(onlyInB,function (el) {
                    onlyInBObj.push(el.monthYear);
                })
            }

            if(onlyInAObj && onlyInAObj.length>0){
                response.created = response.created.filter(function (el) {
                    return !_.includes(onlyInAObj,el.monthYear)
                })
            }

            if(onlyInBObj && onlyInBObj.length>0){
                response.closed = response.closed.filter(function (el) {
                    return !_.includes(onlyInBObj,el.monthYear)
                })
            }

            response.created.sort(function (o1, o2) {
                return new Date(o1.sortDate) > new Date(o2.sortDate) ? 1 : new Date(o1.sortDate) < new Date(o2.sortDate) ? -1 : 0;
            });

            response.closed.sort(function (o1, o2) {
                return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
            });

            _.each(response.created, function (el) {
                if(el.count>0){
                    $scope.noOpp = false;
                }
                label.push(monthNames[moment(new Date(el.sortDate)).month()].substring(0,3))
                seriesOpen.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
            });

            _.each(response.closed, function (el) {
                seriesClose.push({meta:monthNames[moment(new Date(el.sortDate)).month()] +" "+moment(new Date(el.sortDate)).year(),value:el.count})
            });

            $scope.label2 = "Created";
            $scope.label1 = "Won";
            // $scope.loadingMetaData = false;
            drawLineChart($scope,share,seriesOpen,label,".opp-growth",seriesClose);
            share.newOppsCreated(response.Data)

        } else {
            $scope.noOpp = true;
        }

        $scope.loadingMetaData = false;
    }

});

relatasApp.controller("opportunitiesByStage",function ($scope,$http,$rootScope,share) {

    share.forSnapshot = function (response,fy) {
        $scope.loadingMetaData = true;
        getPipelineSnapshot(response,fy)
    }

    $scope.loadingMetaData = true;

    function getPipelineSnapshot(response,fy) {

        var oppExist = false;
        if(response && response.length>0){
            if(fy){
                $scope.fiscalYear = moment(fy.fromDate).format("MMM YY")+" - "+moment(fy.toDate).format("MMM YY")
            } else {
                $scope.fiscalYear = "Apr 18 - Mar 19"
            }

            oppExist = true;

            var maxCount = _.max(_.map(response,"count"))
            var minCount = _.min(_.map(response,"count"))
            var maxAmount = _.max(_.map(response,"totalAmount"))
            var minAmount = _.min(_.map(response,"totalAmount"))

            $scope.prospectColorLeft = "white";
            $scope.EvaluationColorLeft = "white";
            $scope.proposalColorLeft = "white";
            $scope.wonColorLeft = "white";
            $scope.lostColorLeft = "white";

            $scope.prospectColor = "white";
            $scope.EvaluationColor = "white";
            $scope.proposalColor = "white";
            $scope.wonColor = "white";
            $scope.lostColor = "white";

            $scope.funnels = [];
            var stagesWithData = [];

            function getPipelineFunnel(){

                if($rootScope.stages && $rootScope.stages.length>0){
                    _.each(response,function (el) {

                        _.each($rootScope.stages,function (st) {

                            if(el._id == st){
                                stagesWithData.push(st);
                                $scope.funnels.push({
                                    name:st,
                                    countLength:{'width':scaleBetween(el.count, minCount, maxCount)+'%'},
                                    amountLength:{'width':scaleBetween(el.totalAmount, minAmount, maxAmount)+'%'},
                                    amount:el.totalAmount.r_formatNumber(2),
                                    oppsCount:el.count
                                })
                            }
                        })
                    });

                    var noDataStages = _.difference($rootScope.stages,stagesWithData)

                    _.each(noDataStages,function (st) {
                        $scope.funnels.push({
                            name:st,
                            countLength:{'width':0+'%'},
                            amountLength:{'width':0+'%'},
                            amount:0,
                            oppsCount:0
                        })
                    })

                    _.each($scope.funnels,function (fl) {
                        _.each(share.opportunityStages,function (st) {
                            if(fl.name == st.name){
                                fl.order = st.order
                            }
                        })
                    });

                    $scope.funnels = _.sortBy($scope.funnels,function (o) {
                        return o.order
                    })


                } else {
                    setTimeOutCallback(1000,function () {
                        getPipelineFunnel()
                    });
                }
            }

            getPipelineFunnel()
        }

        $scope.loadingMetaData = false;
        $scope.noPipeline = oppExist;
    }

});

relatasApp.controller("opp_prop_viz",function ($scope,$http,$rootScope,share) {

    $scope.loadingMetaData = true;

    share.opp_props = function(oppTypes,sourceTypes,products) {
        $scope.loadingMetaData = true;

        $scope.noOppType = false;
        $scope.noSourceType = false;
        $scope.noProds = false;

        if(oppTypes.length == 0){
            $scope.noOppType = true;
        }

        if(sourceTypes.length == 0){
            $scope.noSourceType = true;
        }

        if(products.length == 0){
            $scope.noProds = true;
        }

        $(".typeGraph").empty()
        $(".sourceTypeGraph").empty()

        var oppType = groupAndChainForTeamSummary(oppTypes)
        donutChart(oppType,".typeGraph",shadeGenerator(63,81,181,oppType.length,15),185,185,185*.10);

        var sourceType = groupAndChainForTeamSummary(sourceTypes)
        donutChart(sourceType,".sourceTypeGraph",shadeGenerator(205,220,57,sourceType.length,15),185,185,185*.10);
        $scope.loadingMetaData = false;
        pieChart(_.flattenDeep(products),$scope);
    }
});

relatasApp.directive('excelExport', function () {
    return {
        restrict: 'A',
        scope: {
            fileName: "@",
            data: "&exportData"
        },
        replace: true,
        template: '<button class="btn btn-white margin0" ng-click="download()">Download <i class="fa fa-download"></i></button>',
        link: function (scope, element) {

            scope.download = function() {

                function datenum(v, date1904) {
                    if(date1904) v+=1462;
                    var epoch = Date.parse(v);
                    return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                };

                function getSheet(data, opts) {
                    var ws = {};
                    var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
                    for(var R = 0; R != data.length; ++R) {
                        for(var C = 0; C != data[R].length; ++C) {
                            if(range.s.r > R) range.s.r = R;
                            if(range.s.c > C) range.s.c = C;
                            if(range.e.r < R) range.e.r = R;
                            if(range.e.c < C) range.e.c = C;
                            var cell = {v: data[R][C] };
                            if(cell.v == null) continue;
                            var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

                            if(typeof cell.v === 'number') cell.t = 'n';
                            else if(typeof cell.v === 'boolean') cell.t = 'b';
                            else if(cell.v instanceof Date) {
                                cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                                cell.v = datenum(cell.v);
                            }
                            else cell.t = 's';

                            ws[cell_ref] = cell;
                        }
                    }
                    if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                    return ws;
                };

                function Workbook() {
                    if(!(this instanceof Workbook)) return new Workbook();
                    this.SheetNames = [];
                    this.Sheets = {};
                }

                var wb = new Workbook(), ws = getSheet(scope.data());
                /* add worksheet to workbook */
                wb.SheetNames.push(scope.fileName);
                wb.Sheets[scope.fileName] = ws;
                var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

                function s2ab(s) {
                    var buf = new ArrayBuffer(s.length);
                    var view = new Uint8Array(buf);
                    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                    return buf;
                }

                saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), scope.fileName+'.xlsx');

            };

        }
    };
});

function getTypeFormat(type){

    var typeFormat = "Account"
    if(type == "geoLocations"){
        typeFormat = "Region"
    }

    if(type == "businessUnits"){
        typeFormat = "BU"
    }
    if(type == "productList"){
        typeFormat = "Product"
    }

    if(type == "solutionList"){
        typeFormat = "Solution"
    }
    if(type == "sourceList"){
        typeFormat = "Source"
    }

    if(type == "typeList"){
        typeFormat = "Type"
    }

    if(type == "verticalList"){
        typeFormat = "Vertical"
    }
    if(type == "opportunityStages"){
        typeFormat = "Stage"
    }

    return typeFormat;
}

function currentInsights($scope,$http,share,response) {

    $scope.currentInsights = response.Data;

    if(response && response.Data){

        $scope.insightsExist = true;
        $scope.staleCount = response.Data.staleOpps

        $scope.todayDate = moment().format(standardDateFormat());

        for(var key in $scope.currentInsights){
            if(typeof $scope.currentInsights[key] == "number"){
                $scope.currentInsights[key+"Commas"] = getAmountInThousands($scope.currentInsights[key],2,share.primaryCurrency=="INR");
            }
        }

        $scope.currentInsights.avgDaysToCloseOppGraph = {width:'0%'};
        $scope.currentInsights.avgDealSizeGraph = {width:'0%'};
        $scope.currentInsights.avgInteractionsPerAmountWonGraph = {width:'0%'};

        if($scope.currentInsights.minDaysToCloseOpp && $scope.currentInsights.minDaysToCloseOpp == $scope.currentInsights.maxDaysToCloseOpp){
            $scope.currentInsights.avgDaysToCloseOppGraph = {width:100+'%'};
        } else if($scope.currentInsights.minDaysToCloseOpp && $scope.currentInsights.maxDaysToCloseOpp){
            $scope.currentInsights.avgDaysToCloseOppGraph = {width:scaleBetween($scope.currentInsights.avgDaysToCloseOpp,$scope.currentInsights.minDaysToCloseOpp,$scope.currentInsights.maxDaysToCloseOpp)+'%'};
        }

        if($scope.currentInsights.minDealSize && $scope.currentInsights.minDealSize == $scope.currentInsights.maxDealSize){
            $scope.currentInsights.avgDealSizeGraph = {width:scaleBetween($scope.currentInsights.avgDealSize,1,$scope.currentInsights.maxDealSize)+'%'};
        } else if($scope.currentInsights.minDealSize && $scope.currentInsights.maxDealSize){
            $scope.currentInsights.avgDealSizeGraph = {width:scaleBetween($scope.currentInsights.avgDealSize,$scope.currentInsights.minDealSize,$scope.currentInsights.maxDealSize)+'%'};
        }

        if($scope.currentInsights.maxDaysToCloseOpp && $scope.currentInsights.maxDaysToCloseOpp){
            $scope.currentInsights.avgInteractionsPerAmountWonGraph = {width:scaleBetween($scope.currentInsights.avgInteractionsPerAmountWon,$scope.currentInsights.minIntPerThousandAmountWon,$scope.currentInsights.maxIntPerThousandAmountWon)+'%'};
        }
    } else {
        $scope.currentInsights = {};
        $scope.insightsExist = false;
    }
    $scope.loadingMetaData = false;
}

function pipelineVelocity($scope,share,thisQuarterOpps,targets,thisQuarterOppsObj){
    $scope.targetGraph = targets
    var currentMonthYear = moment().format("MMM YYYY")

    _.each($scope.targetGraph,function (tr) {
        tr.monthYear = moment(tr.date).format("MMM YYYY")
        var thisMonthOpps = thisQuarterOppsObj[tr.monthYear];

        var won = 0,
            lost = 0,
            pipeline = 0,
            min = 0,
            max = 0;

        _.each(thisMonthOpps,function (op) {
            if(op.relatasStage == "Close Won"){
                won = won+op.convertedAmtWithNgm
            } else if(op.relatasStage == "Close Lost"){
                lost = lost+op.convertedAmtWithNgm
            } else {
                pipeline = pipeline+op.convertedAmtWithNgm
            }
        })

        max = won+lost+pipeline+tr.target;

        tr.won = won;
        tr.lost = lost;
        tr.pipeline = pipeline;

        if(tr.monthYear == currentMonthYear){
            tr.highLightCurrentMonth = true
        }

        tr.heightWon = {'height':scaleBetween(tr.won,min,max)+'%'}
        tr.heightLost = {'height':scaleBetween(tr.lost,min,max)+'%'}
        tr.heightTotal = {'height':scaleBetween(tr.pipeline,min,max)+'%'}
        tr.heightTarget = {'height':scaleBetween(tr.target,min,max)+'%'}

        tr.won = numberWithCommas(tr.won.r_formatNumber(2),share.primaryCurrency == "INR");
        tr.lost = numberWithCommas(tr.lost.r_formatNumber(2),share.primaryCurrency == "INR")
        tr.openValue = numberWithCommas(tr.pipeline.r_formatNumber(2),share.primaryCurrency == "INR")
        tr.target = numberWithCommas(tr.target?tr.target.r_formatNumber(2):0,share.primaryCurrency == "INR")

        tr.month = moment(tr.date).format("MMM")

    })

    $scope.targetGraph.sort(function (o1,o2) {
        return o1.date > o2.date ? 1 : o1.date < o2.date ? -1 : 0;
    })
}

function drawPipeline($scope,wonAmt,lostAmt,wonCount,lostCount,pipelineAmt,pipelineCount,closing30DaysAmt,closing30DaysCount,dealsAtRiskCount,totalDealValueAtRisk,dealsRiskAsOfDate,renewalAmt,renewalCount,staleAmt,staleCount,share,targetAmt) {

    $scope.rangeType = share.rangeType?share.rangeType:"This Quarter"

    var achievementPercentage = 0,
        pipelinePercentage = 0;
    $scope.stageMetaInfo = [];

    if(targetAmt){
        achievementPercentage = parseFloat(((wonAmt/targetAmt)*100).r_formatNumber(2))
        achievementPercentage = achievementPercentage>100?100:achievementPercentage

        pipelinePercentage = parseFloat(((pipelineAmt/targetAmt)*100).r_formatNumber(2))
        pipelinePercentage = pipelinePercentage>100?100:pipelinePercentage
    } else {
        $scope.noTargetsSet = true;
    }

    $scope.pipelinePercentage = pipelinePercentage;
    $scope.achievementPercentage = achievementPercentage;

    share.currentInsightsData(pipelinePercentage,achievementPercentage)

    $scope.stageMetaInfo = [
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Won",
            colIcon:"fas fa-trophy",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(wonAmt,2,share.primaryCurrency =="INR"),
            oppCount:wonCount,
            textColor:"green",
            currency:share.primaryCurrency,
            asOfDate:true,
            percentage:achievementPercentage,
            style:{width:achievementPercentage+"%"}
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Lost",
            colIcon:"fas fa-trophy",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(lostAmt,2,share.primaryCurrency =="INR"),
            oppCount:lostCount,
            textColor:"red",
            currency:share.primaryCurrency,
            asOfDate:true
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Closing",
            colIcon:"fas fa-chart-bar",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(pipelineAmt,2,share.primaryCurrency =="INR"),
            oppCount:pipelineCount,
            textColor:"blue",
            currency:share.primaryCurrency,
            asOfDate:true,
            percentage:pipelinePercentage,
            style:{width:pipelinePercentage+"%"}
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Deals At Risk",
            colIcon:"fas fa-dollar-sign",
            rangeType:dealsRiskAsOfDate?"As of "+moment(dealsRiskAsOfDate).format(standardDateFormat()):null,
            amountValue:getAmountInThousands(totalDealValueAtRisk,2,share.primaryCurrency =="INR"),
            oppCount:dealsAtRiskCount,
            textColor:"red",
            currency:share.primaryCurrency,
            asOfDate:dealsRiskAsOfDate?moment(dealsRiskAsOfDate).format(standardDateFormat()):null,
            cursor:"cursor",
            allTeamMembers:share.selection && share.selection.fullName == "Show all team members"
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Renewal",
            colIcon:"fas fa-dollar-sign",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(renewalAmt,2,share.primaryCurrency =="INR"),
            oppCount:renewalCount,
            textColor:"yellow",
            currency:share.primaryCurrency,
            asOfDate:true
        },
        {
            // tabType:renewalAmt && renewalAmt != 0?"col-style":"col-style-d",
            tabType:"col-style-d",
            colWidth:{width: "19.7%"},
            colType:"Stale",
            colIcon:"fas fa-dollar-sign",
            rangeType:share && share.rangeType?share.rangeType:"This Quarter",
            amountValue:getAmountInThousands(staleAmt,2,share.primaryCurrency =="INR"),
            oppCount:staleCount,
            textColor:"red",
            currency:share.primaryCurrency,
            asOfDate:true
        }
    ];

}

function drawSankeyGraph(response,share,$scope){

    $("#oppFlow").empty();

    if(!response){
        $scope.noOppFlowData = true;
        $scope.startAmount = 0;
        $scope.endAmount = 0;
    } else {

        var formattedData = formatDataForSankeyGraph(response,share);

        share.quarterRange = {
            qStart:response.qStart,
            qEnd:response.qEnd
        }

        // $scope.oppFlowHeader = moment(response.qStart).format("MMM") +"- "+moment(moment(response.qStart).add(1,"month")).format("MMM") +"- "+moment(moment(response.qEnd).subtract(1,"d")).format("MMM YYYY");
        $scope.oppFlowHeader = "This Quarter";

        share.totalCommitAmt = 0;
        share.totalPipelineAmt = 0;
        share.totalNewAmt = 0;

        $scope.startAmount = getAmountInThousands(share.startAmount,2,share.primaryCurrency == "INR");
        $scope.endAmount = getAmountInThousands(share.endAmount,2,share.primaryCurrency == "INR");

        var newExists = false,
            commitStartExists = false,
            pipelineStartExists = false,
            commitExists = false,
            pipelineExists = false,
            closeWonExists = false,
            closeLostExists = false;

        _.each(formattedData,function (el) {

            if(el.source == "New"){
                newExists = true;
            }

            if(el.source == "Pipeline"){
                pipelineStartExists = true;
            }

            if(el.source == "Commit"){
                commitStartExists = true;
            }

            if(el.target == "Pipeline "){
                pipelineExists = true;
            }

            if(el.target == "Commit "){
                commitExists = true;
            }

            if(el.target == "Close Won"){
                closeWonExists = true;
            }

            if(el.target == "Close Lost"){
                closeLostExists = true;
            }

            if(el.source == "New"){
                share.totalNewAmt = share.totalNewAmt+el.sourceAmt
            } else if(el.source == "Commit"){
                share.totalCommitAmt = share.totalCommitAmt+el.sourceAmt
            } else {
                share.totalPipelineAmt = share.totalPipelineAmt+el.sourceAmt
            }
        });

        var commitStage = "Commit" //response.commitStage;
        var commitStageEnd = "Commit " //commitStage+" ";

        var colorScheme = {
            "Pipeline":"#cbcfd2",
            "Pipeline ":"#cbcfd2",
            "Close Won": "#8ECECB", //"#47b758",
            "Close Lost":"#e74c3c",
            "New":"#2499c9"
        }

        colorScheme[commitStage] = "#f1c40f";
        colorScheme[commitStageEnd] = "#f1c40f";

        var allNodes = [];

        if(closeLostExists){
            allNodes.push({"name": "Close Lost"})
        }

        if(closeWonExists){
            allNodes.push({"name": "Close Won"})
        }

        if(newExists){
            allNodes.push({"name": "New"})
        }

        if(pipelineStartExists){
            allNodes.push({"name": "Pipeline"})
        }

        if(commitStartExists){
            allNodes.push({"name": commitStage})
        }

        if(pipelineExists){
            allNodes.push({"name": "Pipeline "})
        }

        if(commitExists){
            allNodes.push({"name": commitStageEnd})
        }

        share.sankeyData = {
            "nodes":allNodes,
            "links": formattedData
        };

        var data = {
            "nodes":allNodes,
            "links": formattedData
        }

        $scope.noOppFlowData = false;
        if(formattedData && formattedData.length>0){
            sankeyGraphSettings($scope,share,data,colorScheme)
        } else {
            $scope.noOppFlowData = true;
        }

        $scope.loadingMetaData = false;
    }

}

relatasApp.directive('resize', function ($window) {
    return function (scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function () {
            return {
                'h': w.height(),
                'w': w.width()
            };
        };
        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.windowHeight = newValue.h;
            scope.windowWidth = newValue.w;
        }, true);

        w.bind('resize', function () {
            scope.$apply();
        });
    }
})

function sankeyGraphSettings($scope,share,data,colorScheme) {

    var viewBox = "0 0 515 350";

    if($(window).width() && $(window).width()>1380){
        viewBox = "0 0 675 350";
    }

    if($(window).width() && $(window).width()>1900){
        viewBox = "0 0 855 350";
    }

    var nodeHash = {};
    data.nodes.forEach(function(d){
        nodeHash[d.name] = d;
    });
    // loop links and swap out string for object
    data.links.forEach(function(d){
        d.source = nodeHash[d.source];
        d.target = nodeHash[d.target];
    });

    var margin = {top: 1, right: 1, bottom: 6, left: 1},
        width = 515 - margin.left - margin.right,
        height = 295 - margin.top - margin.bottom;

    var formatNumber = d3.format(",.0f"),
        format = function(d) { return share.primaryCurrency +" "+formatNumber(d); },
        color = d3.scale.category20();

    var svg = d3.select("#oppFlow")
        .append("div")
        .classed("svg-container", true)
        .append("svg")
        .attr("preserveAspectRatio", "xMinYMin meet")
        .attr("viewBox", viewBox)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .classed("svg-content-responsive", true);

    var sankey = d3.sankey()
        .nodeWidth(5)
        .nodePadding(10)
        .size([width, height]);

    var path = sankey.link();

    sankey
        .nodes(data.nodes)
        .links(data.links)
        .layout(32);

    var tooltip = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    var css = '.stop-left { stop-color: #3f51b5; } .stop-right {stop-color: #009688;} .filled {fill: url(#mainGradient);}',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');

    style.type = 'text/css';
    style.appendChild(document.createTextNode(css));
    head.appendChild(style);

    var link = svg.append("g").selectAll(".link")
        .data(data.links)
        .enter().append("path")
        .attr("class", "link")
        .attr("d", path)
        .style("stroke-width", function(d) { return Math.max(1, d.dy); })
        .style("stroke", function(d) {
        })
        .style("stroke-opacity", function(d) {
            if(d.target.name === "Close Won" || d.target.name === "Close Lost"){
                // return 0.8;
            }
        })
        .sort(function(a, b) { return b.dy - a.dy; })
        .on("mouseover", function(d) {
            var html = getToolTip(d,share)
            tooltip.transition()
                .duration(200)
                .style("opacity", .99);
            tooltip	.html(html)
                .style("left", (d3.event.pageX - 50) + "px")
                .style("top", (d3.event.pageY - 55) + "px");
        })
        .on("mouseout", function(d) {
            tooltip.transition()
                .duration(500)
                .style("opacity", 0);
        });

    var node = svg.append("g").selectAll(".node")
        .data(data.nodes)
        .enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
        .call(d3.behavior.drag()
            .origin(function(d) { return d; })
            .on("dragstart", function() { this.parentNode.appendChild(this); })
            .on("drag", dragmove));

    node.append("rect")
        .attr("height", function(d) { return d.dy; })
        .attr("width", sankey.nodeWidth())
        .style("fill", function(d) {
            return d.color = colorScheme[d.name];
        })
        .style("stroke", function(d) {
            return colorScheme[d.name];
        })
        .append("title")
        .text(function(d) {return d.name + "\n" + format(d.value); })
    // .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

    node.append("text")
        .attr("x", -6)
        .attr("y", function(d) { return d.dy / 2; })
        .attr("dy", ".35em")
        .attr("text-anchor", "end")
        .attr("transform", null)
        .text(function(d) { return d.name; }) //Text in the middle
        .filter(function(d) { return d.x < width / 2; })
        .attr("x", 6 + sankey.nodeWidth())
        .attr("text-anchor", "start");

    function dragmove(d) {
        d3.select(this).attr("transform", "translate(" + d.x + "," + (d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))) + ")");
        sankey.relayout();
        link.attr("d", path);
    }
}

function getToolTip(data,share) {

    var movementCount = data.numberOfOpps + " opps";
    var amountDiff = 0;
    var amountDiffPercentage = 0;

    if(data.sourceAmt && data.targetAmt){
        if(data.source.name == "New"){
            amountDiff = data.targetAmt;
            amountDiffPercentage = (amountDiff/share.totalNewAmt)*100;
            amountDiffPercentage = parseFloat(amountDiffPercentage.toFixed(2))+"%";
        } else if(data.source.name == "Pipeline"){
            amountDiff = data.targetAmt;
            amountDiffPercentage = (amountDiff/share.totalPipelineAmt)*100;
            amountDiffPercentage = parseFloat(amountDiffPercentage.toFixed(2))+"%";
        } else {
            amountDiff = data.targetAmt;
            amountDiffPercentage = (amountDiff/share.totalCommitAmt)*100;
            amountDiffPercentage = parseFloat(amountDiffPercentage.toFixed(2))+"%";
        }
    }

    var directionElement = "<i class='grey fas fa-arrow-right'></i>";

    if(data.target.name == "Close Lost"){
        directionElement = "<i class='red fas fa-arrow-right'></i>"
    }

    if(data.target.name == "Close Won"){
        directionElement = "<i class='green fas fa-arrow-right'></i>"
    }

    var wrapperStart = "<div class='tooltip-wrapper'>";
    var wrapperEnd = "</div>";
    var source = "<div class='source'>"+data.source.name+"</div>"
    var target = "<div class='target'>"+data.target.name+"</div>"
    var direction = "<div class='direction'>"+directionElement+"</div>"
    var moreInfoStart = "<div class='more-info'>"
    var moreInfoEnd = "</div>"
    var spanNumber = "<span>"+amountDiffPercentage+" | "+share.primaryCurrency+" "+getAmountInThousands(amountDiff,2,share.primaryCurrency == "INR")+" | "+movementCount+"</span>"+"<span>"+"</span>"
    var moreInfo = moreInfoStart+spanNumber+moreInfoEnd;

    return wrapperStart+source+direction+target+moreInfo+wrapperEnd;
}

function formatDataForSankeyGraph(data,share){

    data.metaData.forEach(function (el,index) {
        if(!el){
            data.metaData[index] = {}
        }
    });

    data.metaData.sort(function (o1,o2) {
        return o1.month - o2.month;
    });

    var startOfMonth = {},
        endOfMonth = {};

    _.each(data.oppStages,function (el) {
        startOfMonth[el] = []
        endOfMonth[el] = []
    });

    var startMonth = [],
        oppsStart = {},
        oppsEnd = {},
        endMonth = [];

    startMonth = data.metaData[0];
    endMonth = data.metaData[1].oppMetaDataFormat;

    share.startAmount = 0;
    share.endAmount = 0;

    var monthStartDate = data.qStart;
    var monthEndDate = data.qEnd;
    var oppCreatedAndClosingThisQuarter= {}

    if(endMonth){
        _.each(endMonth.data,function (st) {
            _.each(st.oppIds,function (el) {
                el.amount = parseFloat(el.amount);
                el.stageName = st.stageName;

                oppsEnd[el.opportunityId] = el;

                if(st.stageName !== "Close Won" && st.stageName !== "Close Lost"){
                    oppCreatedAndClosingThisQuarter[el.opportunityId] = el.closeDate;
                } else if(st.stageName === "Close Won" || st.stageName === "Close Lost"){
                    if(new Date(el.closeDate)>= new Date(monthStartDate) && new Date(el.closeDate)<= new Date(monthEndDate)){
                        oppCreatedAndClosingThisQuarter[el.opportunityId] = el.closeDate;
                    }
                }
            })
        });
    }

    //Opps created in the selection will be added from data.metaData[1].createdThisSelection source. Not from the opp meta data snapshot.
    // This was to reduce computations when large number of users are involved. Until we move to the new meta data on top of the existing
    // oppMetaData, this will be the implementation...

    _.each(startMonth.data,function (st) {
        _.each(st.oppIds,function (el) {

            // if(oppCreatedAndClosingThisQuarter[el.opportunityId]){ // Considering opps closing this month.
            if(oppCreatedAndClosingThisQuarter[el.opportunityId]){ // Considering opps closing this selection and also removing opps created in the month.
                el.amount = parseFloat(el.amount);
                el.stageName = st.stageName;
                oppsStart[el.opportunityId] = el
                share.startAmount = share.startAmount+parseFloat(el.amount)
            }
        })
    });

    if(data.metaData[1].createdThisSelection && data.metaData[1].createdThisSelection.length>0){
        _.each(data.metaData[1].createdThisSelection,function (el) {
            oppsStart[el.opportunityId] = el;
            share.startAmount = share.startAmount+parseFloat(el.amount)
        })
    }

    var ending = [],
        totalCommitAmt = 0,
        totalPipelineAmt = 0,
        srcCommitAmt = 0,
        targetCommitAmt = 0,
        srcPipelineAmt = 0,
        targetPipelineAmt = 0;

    for(var key1 in oppsStart){

        var source = oppsStart[key1];
        var target = oppsEnd[key1];

        if(target && target.stageName){

            var src = source.stageName, // Start
                trgt = target.stageName;

            if(!source.fromSnapShot){
                src = "New"
                srcCommitAmt = srcCommitAmt+source.amount;
                totalCommitAmt = totalCommitAmt+source.amount;
            } else if(src == data.commitStage){
                src = "Commit"
                srcCommitAmt = srcCommitAmt+source.amount;
                totalCommitAmt = totalCommitAmt+source.amount;
            } else {
                src = "Pipeline"
                srcPipelineAmt = srcPipelineAmt+source.amount;
                totalPipelineAmt = totalPipelineAmt+source.amount;
            }

            if(trgt == data.commitStage){
                trgt = "Commit "
                targetCommitAmt = targetCommitAmt+target.amount;
                totalCommitAmt = totalCommitAmt+target.amount;
            } else if(trgt !== "Close Won" && trgt !== "Close Lost") {
                targetPipelineAmt = targetPipelineAmt+target.amount;
                totalPipelineAmt = totalPipelineAmt+target.amount;
                trgt = "Pipeline "
            }

            share.endAmount = share.endAmount+parseFloat(source.amount)

            ending.push({
                source:src,
                target:trgt,
                value:1,
                sourceAmt:source.amount,
                targetAmt:target.amount
            });
        }
    }

    share.targetPipelineAmt = targetPipelineAmt;
    share.srcCommitAmt = srcCommitAmt;
    share.targetCommitAmt = targetCommitAmt;
    share.srcPipelineAmt = srcPipelineAmt;

    var group = _
        .chain(ending)
        .groupBy('source')
        .map(function(value, key) {
            var obj = {};
            var endings = groupByTargets(key,value,share);
            obj[key] = key;
            obj.value = endings
            return obj;
        })
        .value();

    return _.flatten(_.map(group,"value"));
}

function groupByTargets(pKey,data,share){

    var group = _
        .chain(data)
        .groupBy('target')
        .map(function(value, key) {
            var obj = {};

            var targetAmt = 0,
                sourceAmt = 0;

            _.each(value,function (va) {
                targetAmt = targetAmt+va.targetAmt;
                sourceAmt = sourceAmt+va.sourceAmt;
            });

            obj.source = pKey;
            obj.target = key;
            obj.value = sourceAmt;
            obj.numberOfOpps = value.length;
            obj.sourceAmt = sourceAmt;
            obj.targetAmt = targetAmt;
            obj.diffAmt = sourceAmt-targetAmt;

            return obj;
        })
        .value();

    return group;

}

function menuItems(forRelatas,tabIndex){

    var list = [{
        name:"Today",
        selected:tabIndex == 0?"selected":""
    },{
        name:"Dashboard",
        selected:tabIndex == 1?"selected":""
    },
        //     {
        //     name:"Commit",
        //     selected:""
        // },{
        //     name:"Team",
        //     selected:""
        // },
        {
            name:"Opportunity",
            selected:""
        },{
            name:"Accounts",
            selected:""
        },{
            name:"Downloads",
            selected:""
        }];

    if(isTestingEnvironment()){
        list.push({
            name:"Region",
            selected:""
        }
        // ,{
        //     name:"Playground",
        //     selected:""
        // }
        )
    }

    return list;

}

function menuToggleSelection(select,menu) {

    _.each(menu,function (item) {
        if(item.name == select){
            item.selected = "selected"
        } else {
            item.selected = ""
        }
    })
}

function donutChart(data,className,pattern,height,width,thickness){

    var columns = _.map(data,function (el) {
        return [el.name,el.amount]
    });

    if(!height && !width){
        height = 155;
        width = 155;
    }

    if(!thickness) {
        thickness = 0.12*height
    }

    var chart = c3.generate({
        bindto: className,
        data: {
            columns: columns,
            type : 'donut'
        },
        donut: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            },
            width:thickness
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: height,
            width:width
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                return {top: top, left: parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))}
            }
        }
    });

}

function treemapChart(accounts,$scope,share) {

    var accountsData = {};
    var dataAcc = [];

    $scope.noAccounts = accounts.length === 0;

    if(accounts.length>0) {
        accounts.forEach(function (x) {
            for (var key in x) {
                accountsData[key] = (accountsData[key] || 0) + x[key];
            }
        });

        for (var key in accountsData) {
            dataAcc.push({
                name: key,
                total: parseFloat(accountsData[key].toFixed(2))
            })
        }

        var data = {
            "name": "cluster",
            "children": dataAcc
        };

        var range = shadeGenerator(96,125,139,dataAcc.length,10);
        var color = d3.scale.ordinal()
            .domain([0, dataAcc.length])
            .range(range);

        if(dataAcc.length>15){
            color = d3.scale.category20c();
        }
        // var color = d3.scale.category20c();

        var treemap =
            d3.layout.treemap()
                .size([100, 100])
                .sticky(true)
                .value(function(d) { return d.total; });

        var div = d3.select(".accChart");

        function position() {
            this
                .style("left", function(d) { return d.x + "%"; })
                .style("top", function(d) { return d.y + "%"; })
                .style("width", function(d) { return d.dx + "%"; })
                .style("height", function(d) { return d.dy + "%"; });
        }

        function getLabel(d) {
            return d.name;
        }

        var tooltip = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);

        var node =
            div.datum(data).selectAll(".node")
                .data(treemap.nodes)
                .enter().append("div")
                .attr("class", "node")
                .call(position)
                .style("background", function(d) {
                    return color(getLabel(d));
                })
                .text(getLabel)
                .on("mouseover", function(d) {
                    tooltip.transition()
                        .duration(200)
                        .style("opacity", .99);
                    tooltip	.html(treeMapTooltip(d,share))
                        .style("left", (d3.event.pageX - 0) + "px")
                        .style("top", (d3.event.pageY - 0) + "px");
                })
                .on("mouseout", function(d) {
                    tooltip.transition()
                        .duration(500)
                        .style("opacity", 0);
                })
    }

}

function pieChart(data,$scope){

    var productsData = {};
    var dataProd = [];
    var columns = [];

    $scope.noProds = data.length === 0;

    data.forEach(function(x) {
        for(var key in x){
            productsData[key] = (productsData[key] || 0)+x[key];
        }
    });

    for(var key in productsData){
        columns.push([key,productsData[key]])
        dataProd.push({
            name:key,
            total:productsData[key]
        })
    }

    // var pattern = ['#fadad0','#d1c0d1','#60a7b8','#c9deeb']
    var pattern = shadeGenerator(3,169,244,data.length,15)

    var chart = c3.generate({
        bindto: ".productChart",
        data: {
            columns: columns,
            type : 'pie'
        },
        pie: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            }
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: 200,
            width:200
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                var left = parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))
                return {top: top+50, left: 0}
            }
        }
    });

}

function buildTeamProfiles(data) {
    var team = [];

    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase()
        })
    });

    return team;
}

function buildAllTeamProfiles(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase()
        })
    });

    return team;
}

function closeAllDropDownsAndModals($scope,id,isModal) {
    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                $scope.selectFromList = false;
                $scope.ifOpenList = false;
                resetOtherDropDowns($scope,{name:"closeThis"})
            })
        }
    });
}

function setOppTableHeader($scope,share,filterListObj,importantHeaders){

    $scope.headers = [
        {
            filterReq:false,
            cursor:"",
            name:"Opp Name",
            type:"opportunityName"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Bottom Line("+share.primaryCurrency+")",
            type:"convertedAmtWithNgm",
            align:"text-right"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Stage",
            type:"stageName"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Close Date",
            type:"closeDate"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Account",
            type:"account"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Contact",
            type:"contactEmailId"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Product",
            type:"productType"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Type",
            type:"type"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Region",
            type:"geoLocation"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Currency",
            type:"currency"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Amount",
            type:"amount",
            align:"text-right"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Margin(%)",
            type:"netGrossMargin",
            align:"text-right"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Bottom Line",
            type:"convertedAmt",
            align:"text-right"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Owner",
            type:"userEmailId"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"BU",
            type:"businessUnit"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Vertical",
            type:"vertical"
        },
        {
            filterReq:true,
            cursor:"cursor",
            name:"Solution",
            type:"solution"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Partners",
            type:"partner"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Close Reasons",
            type:"closeReasons"
        },
        {
            filterReq:false,
            cursor:"",
            name:"Close Reasons Description",
            type:"closeReasonsDescription"
        }
    ];

    if(importantHeaders && importantHeaders.length>0){
        _.each(importantHeaders,function (ih) {
            $scope.headers.push({
                filterReq:false,
                cursor:"",
                name:ih,
                type:ih
            })
        })
    }

    $scope.cellWidth = getWidthOfCell();

    _.each($scope.headers,function (he) {
        var type = getWidthOfCell()[he.type]
        he.styleWidth = type?type:""

        if(filterListObj[he.name]){
            he.values = filterListObj[he.name].values
        }
    })
}

function getWidthOfCell(){
    return {
        currency:'min-width:'+65+'px;max-width:'+65+'px',
        account:'min-width:'+80+'px;max-width:'+80+'px',
        amount:'min-width:'+100+'px;max-width:'+100+'px',
        convertedAmt:'min-width:'+100+'px;max-width:'+100+'px',
        blc:'min-width:'+120+'px;max-width:'+120+'px',
        netGrossMargin:'min-width:'+75+'px;max-width:'+75+'px',
        margin:'min-width:'+75+'px;max-width:'+75+'px',
        closeDate:'min-width:'+100+'px;max-width:'+100+'px',
        type:'min-width:'+75+'px;max-width:'+75+'px'
    }
}

function monthsAndYear(){

    return {
        months:[{
            name:"Jan",
            val:1
        },{
            name:"Feb",
            val:2
        },{
            name:"Mar",
            val:3
        },{
            name:"Apr",
            val:4
        },{
            name:"May",
            val:5
        },{
            name:"Jun",
            val:6
        },{
            name:"Jul",
            val:7
        },{
            name:"Aug",
            val:8
        },{
            name:"Sep",
            val:9
        },{
            name:"Oct",
            val:10
        },{
            name:"Nov",
            val:11
        },{
            name:"Dec",
            val:12
        }],
        years:[{
            name:"2015",
            val:2015
        },{
            name:"2016",
            val:2016
        },{
            name:"2017",
            val:2017
        },{
            name:"2018",
            val:2018
        },{
            name:"2019",
            val:2019
        },{
            name:"2020",
            val:2020
        }]
    }
}

function resetOtherDropDowns($scope,type){

    _.each($scope.headers,function (he) {
        if(he.name !== type.name){
            he.open = false;
        }
    })
}

function treeMapTooltip(d,share){
    var wrapperStart = "<div class='tooltip-wrapper'>"
    var wrapperEnd = "</div>"

    return wrapperStart+"<div class='left'>" +d.name+
        "</div>"+
        "<div class='right'>" +share.primaryCurrency+" "+d.total+
        "</div>" +
        "</div>"+wrapperEnd
}

function dealsAtRiskGraph($scope,share,deals,averageRisk,accessControl,totalDeals) {

    var atRisk = 0,safe = 0;
    $scope.total = deals.length;
    $scope.deals = [];
    $scope.totalDealValue = 0;
    var allRisks = _.map(deals,'riskMeter');
    $scope.totalPipeLineValue = 0;

    var dealsWithRiskValue = {};
    var amountAtRisk = 0;

    var maxRisk = _.max(allRisks);
    var minRisk = _.min(allRisks);

    var ngmReq = share.companyDetails && share.companyDetails.netGrossMargin;

    processData()

    function processData(){

        var opportunityStages = {};

        if(share.opportunityStages){
            _.each(share.opportunityStages,function (op) {
                opportunityStages[op.name] = op.order;
            })

            _.each(deals,function (de) {

                var counter = 0;

                _.forIn(de, function(value, key) {
                    if(value === true){
                        counter++
                    }
                });

                de.ngmReq = ngmReq;
                de.amountWithNgm = de.amount;

                if(ngmReq){
                    de.amountWithNgm = (de.amount*de.netGrossMargin)/100
                }

                de.stageStyle2 = oppStageStyle(de.stageName,opportunityStages[de.stageName]-1,true);

                $scope.totalPipeLineValue = $scope.totalPipeLineValue+parseFloat(de.amount);

                $scope.totalDealValue = isNumber(de.amount)?$scope.totalDealValue+parseFloat(de.amount):$scope.totalDealValue+0;

                if((de.riskMeter >= averageRisk && counter<3) || de.ltWithOwner){
                    $scope.deals.push(de)
                    atRisk++
                    amountAtRisk = amountAtRisk+parseFloat(de.amountWithNgm)
                } else {
                    safe++
                }

                de.amountWithNgm = de.amountWithNgm.r_formatNumber(2)

                var riskPerc = scaleBetween(de.riskMeter,minRisk,maxRisk);

                if(minRisk == maxRisk){
                    riskPerc = 100
                }

                var suggestion = "Opportunity at highest risk";

                if(riskPerc > 70 && riskPerc < 90){
                    suggestion = "Opportunity at high risk";
                }

                if(riskPerc > 50 && riskPerc < 70){
                    suggestion = "Opportunity at medium risk";
                }

                if(riskPerc < 50){
                    suggestion = "Opportunity at low risk";
                }

                de.amountWithCommas = numberWithCommas(parseFloat(de.amount).r_formatNumber(2),share.primaryCurrency == "INR")
                de.riskSuggestion = suggestion;
                de.riskPercentage = riskPerc+'%';
                de.riskPercentageStyle = {
                    'width':riskPerc+'%'
                }

                if(de.riskMeter >= averageRisk || de.ltWithOwner){
                    de.riskClass = 'risk-high'
                } else {
                    de.riskClass = 'risk-low'
                }

                dealsWithRiskValue[de.opportunityId] = {
                    riskMeter:de.riskMeter,
                    riskPercentage:de.riskPercentage,
                    riskPercentageStyle:de.riskPercentageStyle,
                    riskSuggestion:de.riskSuggestion,
                    riskClass:de.riskClass,
                    averageRisk:averageRisk,
                    averageInteractionsPerDeal: de.averageInteractionsPerDeal,
                    metDecisionMaker_infuencer: de.metDecisionMaker_infuencer,
                    ltWithOwner: de.ltWithOwner,
                    skewedTwoWayInteractions: de.skewedTwoWayInteractions
                }

            });

            $scope.loadingDealsAtRisk = false;

        } else {
            setTimeOutCallback(1000,function () {
                processData()
            })
        }
    }

}

function drawLineChart($scope,share,series,label,className,series2) {

    var seriesArray = [series];

    if(series && series2){
        seriesArray = [ series, series2]
    }

    new Chartist.Line(className, {
        labels: label,
        series: seriesArray
    }, {
        low: 0,
        showArea: true,
        plugins: [
            tooltip
        ]
    });
}

function groupAndChainForTeamSummary(data) {

    var totalAmount = 0;
    var group = _
        .chain(_.flatten(data))
        .groupBy(function (el) {
            if(el && el.name){
                return el.name;
            } else if((el && el.name == null) || (el && !el.name && el.amount)) {
                return "Others"
            }
        })
        .map(function(values, key) {

            if(checkRequired(key)){
                var amount = _.sumBy(values, 'amount');
                totalAmount = amount;

                return {
                    nameTruncated:key,
                    name:key,
                    amount:amount,
                    amountWithCommas:getAmountInThousands(amount,3)
                }
            }
        })
        .value();

    var sortProperty = "amount";

    group.sort(function (o1, o2) {
        return o2[sortProperty] > o1[sortProperty] ? 1 : o2[sortProperty] < o1[sortProperty] ? -1 : 0;
    });

    return group;

}

function getFilterDates(share,$scope){

    function checkQuarterRangeLoaded(){
        if(share.quarterRange){

            var format = "MMM YYYY";
            var startDtObj = {'year': 2018, 'month': 3}
            var endDtObj = {'year': 2018, 'month': 5}
            $scope.quartersForFilter = [{
                range: {
                    qStart:new Date(moment(startDtObj).startOf("month")),
                    qEnd:new Date(moment(endDtObj).endOf("month"))
                },
                display:  moment(new Date(moment(startDtObj))).format(format)+"-"+moment(new Date(moment(endDtObj))).format(format)
            },{
                range: {
                    qStart:new Date(moment({'year': 2018, 'month': 6}).startOf("month")),
                    qEnd:new Date(moment({'year': 2018, 'month': 8}).endOf("month"))
                },
                display:  moment(new Date(moment({'year': 2018, 'month': 6}))).format(format)+"-"+moment(new Date(moment({'year': 2018, 'month': 8}))).format(format)
            },{
                range: {
                    qStart:new Date(moment({'year': 2018, 'month': 9}).startOf("month")),
                    qEnd:new Date(moment({'year': 2018, 'month': 11}).endOf("month"))
                },
                display:  moment(new Date(moment({'year': 2018, 'month': 9}))).format(format)+"-"+moment(new Date(moment({'year': 2018, 'month': 11}))).format(format)
            },{
                range: {
                    qStart:new Date(moment({'year': 2019, 'month': 0}).startOf("month")),
                    qEnd:new Date(moment({'year': 2019, 'month': 2}).endOf("month"))
                },
                display:  moment(new Date(moment({'year': 2019, 'month': 0}))).format(format)+"-"+moment(new Date(moment({'year': 2019, 'month': 2}))).format(format)
            }];

            $scope.qtrFilterSelected = $scope.quartersForFilter[$scope.quartersForFilter.length-1].display

        } else {
            setTimeOutCallback(500,function () {
                checkQuarterRangeLoaded()
            })
        }
    }

    checkQuarterRangeLoaded()
}

function composeCloseDateFilters(share, type) {
    var scope = angular.element($("#opportunity-insights")).scope();

    var colType = {
        cursor: "cursor",
        filterReq: true,
        name: "Close Date",
        open: true,
        type: "closeDate",
        includeDateRange: true
    }

    if(type == "today_deals_closing") {
        var startDate = moment()
        var endDate = moment().add(30,"days");
    
        scope.start.month = String(startDate.format('M'));
        scope.start.year = String(startDate.format('YYYY'));
    
        scope.end.month = String(endDate.format('M'));
        scope.end.year = String(endDate.format('YYYY'));

    } else if(type == "today_overdue") {
        var startDate = moment(new Date("01 Jan 2015"))
        var endDate = moment().subtract(1,"days");
    
        scope.start.month = String(startDate.format('M'));
        scope.start.year = String(startDate.format('YYYY'));
        scope.start.date = String(startDate.format('DD'));
    
        scope.end.month = String(endDate.format('M'));
        scope.end.year = String(endDate.format('YYYY'));
        scope.end.date = String(endDate.format('DD'));

    }
    

    return colType
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function handleSideBarSelections($scope,$rootScope,share,viewFor,redirectFrom) {

    $rootScope.viewForSelected = viewFor.name.toLowerCase()
    $scope.viewFor = viewFor.name.toLowerCase();
    share.viewFor($scope.viewFor);
    share.resetPrevfilters()
    share.redirectFrom = redirectFrom;
    menuToggleSelection(viewFor.name,$scope.menu);

    if(viewFor && viewFor.name.toLowerCase() == "opportunity"){
        if(share.redirectFrom == "today_deals_closing") {
            // share.getDataForLiu();
            // setTimeOutCallback(400, function() {
            var colType = composeCloseDateFilters(share, redirectFrom);
            share.applyFilters(colType);
            // })

        } else if(share.redirectFrom == "today_overdue") {
            // share.getDataForLiu();
            // setTimeOutCallback(400, function() {
            var colType = composeCloseDateFilters(share, redirectFrom);
            share.applyFilters(colType);
            // })

        } else if($rootScope.viewForSelected == "opportunity"){
            share.drawOppsTable()
        };
    } else if(viewFor && viewFor.name.toLowerCase() == "accounts") {
        share.resetUserSelection(true)
    } else if(viewFor && viewFor.name.toLowerCase() == "dashboard") {
        share.resetUserSelection(true)
    } else if(viewFor && viewFor.name.toLowerCase() == "region") {
        share.regionChart(true)
    }else {
        share.resetUserSelection(true)
    }

    if(viewFor && viewFor.name.toLowerCase() == "today") {
        $scope.showLiu = false;
        share.loadToday();
        share.loadTargets();
    } else {
        $scope.showLiu = true;
    }

    if(viewFor && viewFor.name.toLowerCase() == "allcrossfilters"){

    }
}

function getRegionChart($http,$scope,share,emailIds,stage,startDate,endDate) {

    var url = "/reports/region";
    if(emailIds && emailIds.length>0){
        url = fetchUrlWithParameter(url+"?forUserEmailId="+emailIds)
    }

    if(stage && stage != "Show All Opportunities"){
        url = fetchUrlWithParameter(url+"&stage="+stage)
    }

    if(startDate){
        url = fetchUrlWithParameter(url+"&startDate="+startDate)
    }

    if(endDate){
        url = fetchUrlWithParameter(url+"&endDate="+endDate)
    }

    $scope.tbRows = [];

    $scope.tbHeaders = ["Opportunity Name", "Company", "Stage","Currency", "Amount", "Sales Person", "Selling To"];

    $http.get(url)
        .success(function (response) {

            // var center = [20, 70];//india
            var center = [0, 0];
            $scope.regionOpps = {}

            var oppsWithLoc = [];$scope.oppsWithNoLoc = [];
            $scope.oppsWithLoc = [];

            if(response && response.length>0){
                _.each(response,function (el) {
                    if(!el.lat && !el.lon){
                        el.lat = center[0];
                        el.lon = center[1];
                    }

                    if(el.loc !== "0/0"){
                        oppsWithLoc.push(el)
                    } else {
                        $scope.oppsWithNoLoc = $scope.oppsWithNoLoc.concat(el.opps);
                    }

                    $scope.regionOpps[el.loc] = el;
                });

            };
            $scope.oppsWithLoc = oppsWithLoc;
            drawRegionChart(oppsWithLoc,center,$scope);
        });
}

function drawRegionChart(data,center,$scope){

    document.getElementById('mapWrapper').innerHTML = "<div id='regionMap' style='width: 99%; height: 400px;'></div>";

    var max = _.maxBy(data,"total")
    var min = _.minBy(data,"total")

    var mymap = L.map('regionMap', {scrollWheelZoom: false}).setView(center, 2)

    var tiles = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}');
    tiles.addTo(mymap);
    var charts = {};

    function onClick(e) {
        $scope.loadingOpps = true;
        if(e && e.target && e.target.options && e.target.options.orginal_city){
            $scope.displayOpps(e.target.options.orginal_city);
        }
    }

    for (var i = 0; i < data.length; i++) {
        var d = data[i];

        var scores = [
            d.Won,
            d.Lost,
            d.Pipeline
        ];

        charts[d.city] = L.minichart([d.lat, d.lon], {data: scores,
            maxValues: max.total,
            type:"pie",
            orginal_city:d.city,
            width:scaleBetween(d.total, min.total, max.total,10,95)
        });

        charts[d.city].on("click", function (e) {
            $scope.$apply(function () {
                onClick(e);
            });
        });

        mymap.addLayer(charts[d.city] ) ;
    }
}

function drawRegionContactsChart(data,center,$scope){
    $scope.oppsWithLoc = [];
    document.getElementById('mapWrapper').innerHTML = "<div id='regionMap' style='width: 99%; height: 400px;'></div>";

    var max = _.maxBy(data,"count")
    var min = _.minBy(data,"count")

    var mymap = L.map('regionMap', {scrollWheelZoom: false}).setView(center, 2)

    var tiles = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}');
    tiles.addTo(mymap);
    var charts = {};

    function onClickContacts(e) {
        $scope.loadingOpps = true;
        if(e && e.target && e.target.options && e.target.options.orginal_city){
            $scope.displayContacts(e.target.options.orginal_city);
        }
    }

    $scope.tbRows = [];

    var oppsWithLoc = [];$scope.oppsWithNoLoc = [];

    _.each(data,function (el) {
        if(el.loc !== "0/0"){
            oppsWithLoc.push(el)
        } else {
            $scope.oppsWithNoLoc = $scope.oppsWithNoLoc.concat(el.opps)
        }
    })

    $scope.contactsObj = {};
    $scope.oppsWithLoc = oppsWithLoc;

    for (var i = 0; i < oppsWithLoc.length; i++) {
        var d = data[i];

        var scores = [d.count];

        $scope.contactsObj[d.city] = d.contacts;

        charts[d.city] = L.minichart([d.lat, d.lon], {data: scores,
            maxValues: max.count,
            type:"pie",
            orginal_city:d.city,
            width:scaleBetween(d.count, min.count, max.count,25,75)
        });

        charts[d.city].on("click", function (e) {
            $scope.$apply(function () {
                onClickContacts(e);
            });
        });

        mymap.addLayer(charts[d.city] ) ;
    }
}

function cursorPos(canvas,evt) {
    return getMousePos(canvas, evt);
}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

var tooltip = Chartist.plugins.tooltip();

relatasApp.controller("opportunityTargets", function($scope, share, $http, $rootScope) {

    share.loadTargets = function(){
        getTargets($scope, $http, share);
    }

    share.getTargets = function (user,filter,fromDate,endDate,switchQtr) {
        $scope.user = user;
        $scope.filter = filter;
        $scope.fromDate = fromDate;
        $scope.endDate = endDate;
        $scope.switchQtr = switchQtr;
        getTargets();
    }
})

relatasApp.controller("today-primary-insight", function($scope, share, $http, $rootScope) {

    $scope.todayInsightNoDataMsg = {
        meetings: "No meetings scheduled today",
        tasks: "No tasks for today",
        dealsClosingSoon: "No deals owned by you closing in next 30 days",
        losingTouch: "Super networker. You are in touch with all your contacts",
        meetingFollowup: "You are all set. No follow ups pending"
    }

    $scope.selectedCommitRange = 1;
    
    $scope.commitRange = [{
        name: 'WEEKLY',
        selected: '',
        daysLeft:0,
        cuttOffDate:'',
        userCommitAmount: 0,
        inputComment: 'Enter your weekly commit',
        nextCommitMsg: 'Commit for next Week'
    },{
        name: 'MONTHLY',
        selected: 'selected',
        daysLeft:0,
        cuttOffDate:'',
        userCommitAmount: 0,
        inputComment: 'Enter your monthly commit',
        nextCommitMsg: 'Commit for next Month'
    },{
        name: 'QUARTERLY',
        selected: '',
        daysLeft:0,
        cuttOffDate:'',
        userCommitAmount: 0,
        inputComment: 'Enter your quarterly commit',
        nextCommitMsg: 'Commit for next Quarter'
    }]

    share.loadToday = function(){
        loadPage($scope, $http, share, $rootScope);
    }

    $scope.updateCommit = function() {
        var index = $scope.selectedCommitRange;
        var commitforNext = $scope.commitRange[index].commitforNext;

        $scope.saving = true;
        if($scope.commitRange[index].name){

            if($scope.commitRange[index].name == "WEEKLY"){
                
                saveWeeklyCommits($scope,$rootScope,$http,share,{selfCommitValue:$scope.selfCommitValue, commitforNext:commitforNext},function () {
                    $scope.saving = false;
                });
            } else if ($scope.commitRange[index].name == "QUARTERLY"){

                saveQuarterlyCommits($scope,$rootScope,$http,share,{selfCommitValue:$scope.selfCommitValue, commitforNext:commitforNext},function () {
                    $scope.saving = false;
                });
            } else {
                saveMonthlyCommit($scope,$rootScope,$http,share,{selfCommitValue:$scope.selfCommitValue, commitforNext:commitforNext},function () {
                    $scope.saving = false;
                });
            }
        } else {
            saveMonthlyCommit($scope,$rootScope,$http,share,function () {
                $scope.saving = false;
            });
        }
    }

    $scope.viewAllOpps = function() {
        window.open('team/commit/review', '_blank');
    }

    $scope.viewAllTasks = function(filter, redirectLink) {
        if(redirectLink) {
            var url = "tasks/all"
            url = fetchUrlWithParameter(url, "for", filter);
            window.open(url, '_blank');
        }
    }

    $scope.viewAllDealsClosingSoon = function() {
        var oppViewFor =  {
            name:"Opportunity",
            selected:""
        };

        share.openViewFor(oppViewFor, "today_deals_closing");
    }

    $scope.viewAllLosingTouch = function() {
        var url = "today/insights/all"
        url = fetchUrlWithParameter(url, "for", "losingTouch");
        // window.location = url;
        window.open(url, '_blank');
    }

    $scope.viewAllMeetingFollowUps = function() {
        var url = "today/insights/all";  
        url = fetchUrlWithParameter(url, "for", "meetingFollowUp");
        window.open(url, '_blank');

    }

    $scope.viewAllMeetings = function() {
        var url = "today/insights/all"
        url = fetchUrlWithParameter(url, "for", "todayMeeting");
        window.open(url, '_blank');

    }

    share.viewAllTasks = function(filter, redirectLink) {
        $scope.viewAllTasks(filter, redirectLink);
    }

    $scope.setTaskStatus = function(task) {

        if(task.checked) {
            task.status = "complete"
        } else {
            task.status = "inProgress"
        }
        updateTask($scope, $http, task);
    }

    $scope.commitFor = function(commit, index) {
        var url;
        var mode;

        menuToggleSelection(commit.name, $scope.commitRange);

        $scope.selectedCommitRange = index;
        if($scope.commitRange[index].name) {
            mode =  $scope.commitRange[index].name;
        }
        
        if(mode){
            if(mode == "WEEKLY"){
                url = '/review/commits/week';
                fetchCommits($scope,$http,url,mode)

            } else if (mode == "QUARTERLY"){
                url = '/review/commits/quarter';
                fetchCommits($scope,$http,url,mode)

            } else if(mode == "MONTHLY"){
                url = '/review/commits/month';
                fetchCommits($scope,$http,url,mode)
            }
        }
    }

    $scope.goToOpportunity = function(opp) {
        window.open("/opportunities/all?opportunityId="+opp.opportunityId, '_blank');
    }

    $scope.openLosingTouchContact = function(contact, index) {

        localStorage.setItem("losing-touch", JSON.stringify (contact));
        var url = "today/insights/all"

        url = fetchUrlWithParameter(url, "for", "losingTouch");
        url = fetchUrlWithParameter(url+"&index="+index);

        window.open(url, '_blank');
    }

    $scope.openMeetingDetails = function(meeting, index) {
        
        localStorage.setItem("today-meeting", JSON.stringify(meeting));
        var url = "today/insights/all"

        url = fetchUrlWithParameter(url, "for", "todayMeeting");
        url = url+"&index="+index
        window.open(url, '_blank');
    }

    $scope.openMeetingFollowUpDetails = function(contact, index) {
        localStorage.setItem("meeting-followUp", JSON.stringify(contact));
        var url = "today/insights/all"

        url = fetchUrlWithParameter(url, "for", "meetingFollowUp");
        url = url+"&index="+index
        window.open(url, '_blank');
    }

    $scope.openCommitModel = function() {
        $scope.commitModalOpen = true;
    }

    $scope.closeModal = function() {
        $scope.commitModalOpen = false;
    }

})

relatasApp.controller("today-secondary-insight", function($scope, share, $http, $rootScope) {

    $scope.loadTodayMetaInsights = function() {
        $scope.loadingInsights = true;

        $http.get("/today/insights/meta")
            .success(function (response) {
                $scope.loadingInsights = false;
                if(response.Data){

                    var insight = response.Data;

                    $scope.todayMetaInsight = {
                        "impMailCount": insight.mailInsights.important || 0,
                        "upComingMeetings": insight.upcomingMeetings || 0,
                        "upcomingTasks": insight.upcomingTasks || 0,
                        "staleOpportunityCount": insight.staleOpportunities || 0,
                        "dealsAtRiskCount": insight.dealsClosingSoon.count[0] ? insight.dealsClosingSoon.count[0].count : 0,
                        "tasksOverdue": insight.overdueTasks || 0,
                    }

                }
            })
    }

    share.loadTodayMetaInsights = function() {
        $scope.loadTodayMetaInsights();
    }

    $scope.showStaleOpportunities = function() {
        if($scope.todayMetaInsight.staleOpportunityCount) {
            var oppViewFor =  {
                name:"Opportunity",
                selected:""
            };
    
            share.openViewFor(oppViewFor, "today_overdue");
        }
    }

    $scope.showDealsAtRisk = function() {
        if($scope.todayMetaInsight.dealsAtRiskCount) {
            share.forDealsAtRisk(share.liuData._id);
        }
    }

    $scope.showMailSection = function() {
        if($scope.todayMetaInsight.impMailCount)
            window.open('insights/mails', "_blank");
    }

    $scope.viewAllUpcomingMeetings = function() {
        if($scope.todayMetaInsight.upComingMeetings) {
            var url = "today/insights/all"
            url = fetchUrlWithParameter(url, "for", "upcomingMeeting");
            window.open(url, '_blank');
        }
    }

    // $scope.viewAllRecommendedToMeet = function() {
    //     var url = "today/insights/all"
    //     url = fetchUrlWithParameter(url, "for", "recommendedToMeet");
    //     window.open(url, '_blank');
    // }

    $scope.showTasksOverdue = function() {
        window.open("tasks/all/for/overdue", '_blank');
    }

    $scope.showTasksUpcoming = function() {
        window.open("tasks/all/for/upcoming", '_blank');
    }

    $scope.viewAllTasks = function(filter) {
        var visibility;

        if(filter == 'upcoming') {
            visibility = $scope.todayMetaInsight.upcomingTasks ? true : false;
        } else if(filter == 'overdue') {
            visibility = $scope.todayMetaInsight.tasksOverdue ? true : false;
        }
        share.viewAllTasks(filter, visibility);
    }

})

function saveQuarterlyCommits($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){
        $http.post("/review/meeting/update/commit/value/quarterly",{commitValue:commits.selfCommitValue,
                                                                        committingForNext:commits.commitforNext})
            .success(function (response) {

                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // quarterlyCommitHistory($scope,$rootScope,$http,share,share.liuData._id)
                } else {
                    $scope.saving = false;
                    toastr.error("Commits not updated. Please try again later");
                }
                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only positive numbers")
    }
}

function saveWeeklyCommits($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){

        $http.post("/review/meeting/update/commit/value/weekly",{week:commits.selfCommitValue,
                                                                    committingForNext:commits.commitforNext})
            .success(function (response) {

                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // weeklyPastCommitHistory($scope,$rootScope,$http,share,share.liuData._id)
                } else {
                    $scope.saving = false;
                    toastr.error("Commits not updated. Please try again later")
                }
                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only positive numbers")
    }
}

function saveMonthlyCommit($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){
        $http.post("/review/meeting/update/commit/value/monthly",{commitValue:commits.selfCommitValue,
                                                                    committingForNext:commits.commitforNext})
            .success(function (response) {
                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // monthlyCommitHistory($scope,$rootScope,$http,share)
                } else {
                    $scope.saving = false;
                    toastr.error("Commits not updated. Please try again later")
                }

                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only numbers for opportunity amount")
    }
}

function getTargets($scope, $http, share){

    $scope.loadingTargets = true;
    var url = fetchUrlWithParameter("/opportunities/by/month/year","userId", $scope.user)

    if($scope.filter && $scope.filter == 'accessControl'){
        url = fetchUrlWithParameter(url+"&accessControl="+true)
    }

    if($scope.fromDate && $scope.endDate){
        url = fetchUrlWithParameter(url+"&fromDate="+$scope.fromDate)
        url = fetchUrlWithParameter(url+"&toDate="+$scope.endDate)
    }

    setTimeOutCallback(10,function () {
        drawTargets($scope,$http,url,share,$scope.switchQtr)
    })
}   

function menuToggleSelection(select,menu) {

    _.each(menu,function (item) {
        if(item.name == select){
            item.selected = "selected"
        } else {
            item.selected = ""
        }
    })
}

function loadPage($scope, $http, share, $rootScope) {

    $scope.loadingLosingTouch = true;
    $scope.loadingMeetings = true;
    $scope.loadingTasks = true;
    $scope.loadingLosingTouch = true;
    $scope.loadingDealsClosing = true;
    $scope.loadingMeetingFollowUp = true;
    
    getAllCommitCutOffDates($scope, $http);
    fetchCommits($scope, $http);
    getMeetings($scope, $http, share);
    getLosingTouch($scope, $http, $rootScope);

    setTimeOutCallback(100, function() {
        getAllTasksToday($scope, $http, $rootScope);
        getDealsClosingSoon($scope, $http, $rootScope);
        getPastMeetingFollowUps($scope, $http);
        share.loadTodayMetaInsights();
    });

    // setTimeOutCallback(200, function() {
    //     getOverdueInsights('staleOpportunity', $http, $scope, $rootScope, share);
    //     getOverdueInsights('dealsAtRisk', $http, $scope, $rootScope, share);
    //     getOverdueInsights('taskOverdue', $http, $scope, $rootScope, share);
    //     getOverdueInsights('mailResponsePending', $http, $scope, $rootScope, share);
    // });

}

function updateTask($scope, $http,task) {
    $http.post('/task/update/properties',task)
        .success(function (response) {
            if(response){
            }   
        });
}

function drawTargets($scope,$http,url,share,switchQtr){

    $http.get(url)
        .success(function (response) {
            
            var quarters = [
                {
                    quarter:1,
                    startMonth:0,
                    endMonth:2
                },{
                    quarter:2,
                    startMonth:3,
                    endMonth:5
                },{
                    quarter:3,
                    startMonth:6,
                    endMonth:8
                },{
                    quarter:4,
                    startMonth:9,
                    endMonth:11
                }
            ];

            var targetForFy = 0;
            var total = _.map(response.Data,"total")
            var won = _.map(response.Data,"won")
            var lost = _.map(response.Data,"lost")
            var target = _.map(response.Data,"target")

            var values = _.flatten(_.concat(total,won,lost,target))

            var min = Math.min.apply( null, values );
            var max = Math.max.apply( null, values );

            var pipeline = 0;
            var currentQuarter = {};

            _.each(quarters,function (q) {
                if (q.startMonth <= new Date().getMonth() && q.endMonth >= new Date().getMonth()) {
                    currentQuarter = q;
                }
            });

            if(switchQtr){
                currentQuarter.startMonth = (moment(switchQtr.start).format("M"))-1,
                currentQuarter.endMonth = (moment(switchQtr.end).format("M"))-1
            }

            currentQuarter.values = [];
            currentQuarter.target = 0;
            currentQuarter.pipeline = 0;
            currentQuarter.won = 0;
            currentQuarter.lost = 0;

            var currentMonth = monthNames[new Date().getMonth()]

            $scope.cqHeader = monthNames[currentQuarter.startMonth].substr(0,3)+" - "+monthNames[currentQuarter.endMonth].substr(0,3) +" "+new Date().getFullYear();
            _.each(response.Data,function (value) {
                value.won = value.won?value.won:0;
                value.lost = value.lost?value.lost:0;
                value.total = value.total?value.total:0;
                value.openValue = value.openValue?value.openValue:0;

                targetForFy = targetForFy+value.target;

                var thisMonth = monthNames[new Date(moment(value.sortDate).subtract(1,"d")).getMonth()];

                if(thisMonth == currentMonth && new Date(value.sortDate).getFullYear() == new Date().getFullYear()){
                    value.highLightCurrentMonth = true
                }

                pipeline = pipeline+value.openValue
                
                if (currentQuarter.startMonth <= new Date(value.sortDate).getMonth() && currentQuarter.endMonth >= new Date(value.sortDate).getMonth()){
                    currentQuarter.target = currentQuarter.target+parseFloat(value.target)
                    currentQuarter.won = currentQuarter.won+value.won

                    currentQuarter.pipeline = currentQuarter.pipeline+value.openValue
                    currentQuarter.lost = currentQuarter.lost+value.lost
                }

                value.heightWon = {'height':scaleBetween(value.won,min,max)+'%'}
                value.heightLost = {'height':scaleBetween(value.lost,min,max)+'%'}
                value.heightTotal = {'height':scaleBetween(value.openValue,min,max)+'%'}
                value.heightTarget = {'height':scaleBetween(value.target,min,max)+'%'}

                value.won = numberWithCommas(value.won.r_formatNumber(2),share.primaryCurrency == "INR");
                value.lost = numberWithCommas(value.lost.r_formatNumber(2),share.primaryCurrency == "INR");
                value.openValue = numberWithCommas(value.openValue.r_formatNumber(2),share.primaryCurrency == "INR");
                value.target = numberWithCommas(value.target.r_formatNumber(2),share.primaryCurrency == "INR");
            });

            share.setTargetForFy(targetForFy)

            var allValues = [currentQuarter.pipeline,currentQuarter.lost,currentQuarter.won,currentQuarter.target]

            var cqMin = Math.min.apply( null, allValues );
            var cqMax = Math.max.apply( null, allValues );

            $scope.cqTarget = numberWithCommas(currentQuarter.target.r_formatNumber(2),share.primaryCurrency == "INR")
            // $scope.cqLost = {'width':scaleBetween(currentQuarter.lost,cqMin,cqMax)+'%',background: '#6dc3b8'}
            $scope.cqWonStyle = {'width':scaleBetween(currentQuarter.won,0,currentQuarter.target)+'%',background: '#8ECECB'}
            $scope.cqWon = numberWithCommas(currentQuarter.won.r_formatNumber(2),share.primaryCurrency == "INR")
            $scope.cqPipeline = numberWithCommas(currentQuarter.pipeline.r_formatNumber(2),share.primaryCurrency == "INR")

            if(currentQuarter.won && !currentQuarter.target){
                $scope.cqWonStyle = {'width':100+'%',background: '#8ECECB'}
            }

            if(currentQuarter.target){
                $scope.cqWonPercentage = ((currentQuarter.won/currentQuarter.target)*100).r_formatNumber(2)+'%'
            } else {
                $scope.cqWonPercentage = "-"
            }

            if(currentQuarter.won>currentQuarter.target){
                $scope.cqWonStyle = {'width':100+'%',background: '#8ECECB'}
                $scope.cqWonPercentage = 100+'%'
            }

            var t = _.sum(target);
            var w = _.sum(won);
            var g = t - w;

            var valArr = [];
            valArr.push(t,w,g,pipeline)

            var vmin = Math.min.apply( null, valArr );
            var vmax = Math.max.apply( null, valArr );

            $scope.fyWonStyle = {'width':scaleBetween(w,1,t)+'%',background: '#8ECECB'}

            if(!w || w ==0 ){
                $scope.fyWonStyle = {'width':0+'%',background: '#8ECECB'}
            }

            $scope.target = {'width':scaleBetween(t,vmin,vmax)+'%',background: '#6dc3b8'}
            $scope.pipeline = {'width':scaleBetween(pipeline,vmin,vmax)+'%',background: '#767777'}
            $scope.won = {'width':scaleBetween(w,vmin,vmax)+'%',background: '#8ECECB'}
            $scope.gap = {'width':scaleBetween(g,vmin,vmax)+'%',background: '#e74c3c'}

            if(t){
                $scope.wonPercentage = ((w/t)*100).r_formatNumber(2)+'%'
            } else {
                $scope.wonPercentage = "-"
            }

            if(w>t){
                $scope.fyWonStyle = {'width':100+'%',background: '#8ECECB'}
                $scope.won = {'width':100+'%',background: '#8ECECB'}
                $scope.wonPercentage = 100+'%'
            }

            $scope.targetCount = numberWithCommas(t.r_formatNumber(2),share.primaryCurrency == "INR");
            $scope.wonCount = numberWithCommas(w.r_formatNumber(2),share.primaryCurrency == "INR");
            $scope.gapCount = numberWithCommas(g.r_formatNumber(2),share.primaryCurrency == "INR");
            $scope.pipelineCount = numberWithCommas(pipeline.r_formatNumber(2),share.primaryCurrency == "INR");

            $scope.targetGraph = response.Data;

            $scope.targetGraph.sort(function (o1, o2) {
                return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
            });

            _.map($scope.targetGraph, function(el) {
                return el.month =  el.monthYear.split(' ')[0]
            })

            $scope.loadingTargets = false;
        })
}

function getAllTasksToday($scope, $http, $rootScope) {

    var todayDate = moment(new Date()).format("YYYY-MM-DD");
    var tomorrowDate = new Date();
    tomorrowDate.setDate(tomorrowDate.getDate() + 1);

    tomorrowDate = moment(tomorrowDate).format("YYYY-MM-DD");

    $scope.taskInsights = {
        tasks:[],
    }

        $http.get('/tasks/get/all?filter=dueDate&start='+todayDate+'&end='+tomorrowDate)
        .success(function(response) {
            $scope.loadingTasks = false;
            var filteredTasks = [];
            if(response.Data.tasks) {

                filteredTasks = _.filter(response.Data.tasks, function(task) {
                    return ($rootScope.liuEmailId == task.assignedToEmailId);
                })
                
                filteredTasks.forEach(function(task) {
                    task.status = task.status == 'complete' ? true : false;
                    task.formattedTaskName = stripHtmltags(task.taskName);
                })
                
                $scope.taskInsights["totalCount"] = filteredTasks.length;
                $scope.taskInsights.tasks = filteredTasks.slice(0,5);
                $scope.taskInsights["displayedCount"] = $scope.taskInsights.tasks.length;
                $scope.taskInsights["widthDisplay"] = {'width': ($scope.taskInsights.displayedCount/$scope.taskInsights.totalCount)*100 +'%'}; 
                $scope.taskInsights.showNoDataMsg = $scope.taskInsights.tasks.length == 0 ? true : false;
            } else {
                $scope.taskInsights.showNoDataMsg = true;
            }
        })

}

function getLosingTouch($scope, $http, $rootScope) {
    var losingTouchContacts = [];
    
    $scope.losingTouchInsights = {
        losingTouchContacts: []
    };
    
    $http.get('/insights/losing/touch/info/by/relation')
        .success(function(response) {
            $scope.loadingLosingTouch = false;

            var contactsArray = _.uniqBy(response.Data, 'contactEmailId');

            _.each(contactsArray, function(contact) {
                var obj = formatLosingTouchContactDetails(contact);
                if(obj) {
                    losingTouchContacts.push(obj);
                }
            })

            losingTouchContacts.sort(function (o1, o2) {
                return new Date(o1.lastInteractionDate) < new Date(o2.lastInteractionDate) ? -1 : new Date(o1.lastInteractionDate) > new Date(o2.lastInteractionDate) ? 1 : 0;
            });

            $scope.losingTouchInsights["totalCount"] = losingTouchContacts.length;
            $scope.losingTouchInsights.losingTouchContacts = losingTouchContacts.slice(0,3);
            $scope.losingTouchInsights["displayedCount"] = $scope.losingTouchInsights.losingTouchContacts.length;
            $scope.losingTouchInsights["widthDisplay"] = {'width': ($scope.losingTouchInsights.displayedCount/$scope.losingTouchInsights.totalCount)*100 +'%'}; ;
            $scope.losingTouchInsights.showNoDataMsg = $scope.losingTouchInsights.losingTouchContacts.length == 0 ? true : false;

        })
}

function getDealsClosingSoon($scope, $http, $rootScope) {
    var contactList = [];

    $scope.dealsClosingSoonInsights = {
        dealsClosingSoon: []
    };

    $http.get('/insights/deals/closing/soon')
        .success(function(response) {
            $scope.loadingDealsClosing = false;

            _.each(response.Data, function(contact) {
                var companyName = fetchCompanyFromEmail(contact.contactEmailId);
                var companyLogo = "https://logo.clearbit.com/"+ getTextLength(companyName,25) +".com";

                var obj = {
                    companyName: fetchCompanyFromEmail(contact.contactEmailId),
                    amount: parseFloat(contact.amount.toFixed(2)),
                    closeDate: contact.closeDate,
                    opportunityName: getTextLength(contact.opportunityName,20),
                    opportunityId: contact.opportunityId,
                    noPicFlag: imageExists(companyLogo) ? false : true,
                    noPicText: companyName.slice(0,2),
                    accountImageUrl: companyLogo
                }
                contactList.push(obj);

            });

            contactList.sort(function (o1, o2) {
                return new Date(o1.closeDate) - new Date(o2.closeDate);
            });

            $scope.dealsClosingSoonInsights["totalCount"] = response.Data.length;
            $scope.dealsClosingSoonInsights.dealsClosingSoon = contactList.slice(0,3);
            // $scope.dealsClosingSoonInsights.dealsClosingSoon = contactList;
            $scope.dealsClosingSoonInsights["displayedCount"] = $scope.dealsClosingSoonInsights.dealsClosingSoon.length;
            $scope.dealsClosingSoonInsights["widthDisplay"] = {'width': ($scope.dealsClosingSoonInsights.displayedCount/$scope.dealsClosingSoonInsights.totalCount)*100 +'%'}; 
            $scope.dealsClosingSoonInsights.showNoDataMsg = $scope.dealsClosingSoonInsights.dealsClosingSoon.length == 0 ? true : false;
        })
}

function getMeetings($scope, $http, share) {

    $scope.meetingInsights = {
        todayMeetings:[]
    }

    $http.post('/get/all/meetings', {})
    .success(function(response) {
        $scope.loadingMeetings = false;
        var userId = response.userId;

        if(response.Data) {
            
            response.Data.forEach(function(meeting) {
                if(!meeting.actionItemSlotType) {
                    var meetingDetails = fetchMeetingDetails(meeting, share, userId);
                    meetingDetails['participants'] = meeting.toList;
                    meetingDetails['numberOfMeetingParticipants'] = '';
                    meetingDetails['liu'] = response.Data.userId;

                    if(meeting.toList.length>1) {
                        meetingDetails['numberOfMeetingParticipantsExists'] = true;
                        meetingDetails['numberOfMeetingParticipants'] = meeting.toList.length -1
                        for(var j=0;j<meeting.toList.length;j++){
                            // $rootScope.meetingsWithToday.push(meeting.toList[j].receiverEmailId)
                        }
                    }

                    // $rootScope.meetingsWithToday.push(meetingDetails.personEmailId)

                    // meetingDetails.meetingInitiatorIcon = setMeetingInitiatorIcon(meetingDetails);
                    meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/outgoingOrange.png';
                    if(meetingDetails.isSender && meetingDetails.isAccepted){
                        meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/Meeting_Outgoing_confirmed.png';
                    }

                    if(!meetingDetails.isSender && meetingDetails.isAccepted){
                        meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/IncomingGreen.png';
                    }

                    if(!meetingDetails.isSender && !meetingDetails.isAccepted){
                        meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/IncomingOrange.png';
                    }

                    meetingDetails.notSelfMeeting = true;

                    if(meetingDetails.participants.length === 0){
                        if((meetingDetails.personEmailId === meeting.to.receiverEmailId) || !meeting.to.receiverEmailId){
                            meetingDetails.notSelfMeeting = false;
                        }
                    }

                    if(meetingDetails.personEmailId === 'unknownorganizer@calendar.google.com'){
                        meetingDetails.nameTruncate = "Goal";
                        meetingDetails.picUrl = "/images/dashboard-icons/google-icon.png";
                        meetingDetails.meetingInitiatorIcon = "/images/dashboard-icons/flag.png";
                        meetingDetails.locationTypePic = "";
                        meetingDetails.isGoogleGoal = true;
                        meetingDetails.descriptionTruncate = getTextLength(meetingDetails.description,45);
                        meetingDetails.company = '';
                        meetingDetails.companyTruncate = '';
                    }

                    $scope.meetingInsights.todayMeetings.push(meetingDetails);
                }

            })

            $scope.meetingInsights["totalCount"] = $scope.meetingInsights.todayMeetings.length;
            $scope.meetingInsights.todayMeetings = $scope.meetingInsights.todayMeetings.slice(0,3);
            $scope.meetingInsights["displayedCount"] = $scope.meetingInsights.todayMeetings.length;
            $scope.meetingInsights["widthDisplay"] = {'width': ($scope.meetingInsights.displayedCount/$scope.meetingInsights.totalCount)*100 +'%'}; 
            $scope.meetingInsights.showNoDataMsg = $scope.meetingInsights.todayMeetings.length == 0 ? true : false;
            
        } else {
            $scope.meetingInsights.showNoDataMsg = true;
        }
    })
}

function getPastMeetingFollowUps($scope, $http) {

    $scope.meetingFollowupInsights = {
        meetingFollowup: []
    }
    var meeting = _.filter($scope.meetingInsights.todayMeetings,function (meeting) {
        return meeting.ifRelatasMailEventType === "meetingFollowUp"
    })

    var meetingId = meeting[0]?meeting[0].invitationId:null;

    var url ='/fetch/yesterdays/meetings?invitationId='+meetingId;

    $http.get(url)
        .success(function(response){
            $scope.loadingMeetingFollowUp = false;
            var meetingFollowUp = [];

            _.each(response.Data, function(contact) {
                var obj = formatMeetingFollowUpContactDetails(contact);
                meetingFollowUp.push(obj);
            })

            meetingFollowUp.sort(function (o1, o2) {
                return new Date(o2.sortDate) - new Date(o1.sortDate)
            });
            
            $scope.meetingFollowupInsights["totalCount"] = meetingFollowUp.length;
            $scope.meetingFollowupInsights.meetingFollowup = meetingFollowUp.slice(0,3);
            $scope.meetingFollowupInsights["displayedCount"] = $scope.meetingFollowupInsights.meetingFollowup.length
            $scope.meetingFollowupInsights["widthDisplay"] = {'width': ($scope.meetingFollowupInsights.displayedCount/$scope.meetingFollowupInsights.totalCount)*100 +'%'}; 
            $scope.meetingFollowupInsights.showNoDataMsg = $scope.meetingFollowupInsights.meetingFollowup.length == 0 ? true : false;
        
        })
}

function formatMeetingFollowUpContactDetails(contact) {
    var obj = {};
    
    if(checkRequired(contact.personId) && checkRequired(contact.personName)){

        var name = getTextLength(contact.personName,10);
        var image = '/getImage/'+contact.personId;

        obj = {
            fullName:contact.personName,
            name:name,
            image:image
        };

    }
    else {
        var contactImageLink = contact.contactImageLink ? encodeURIComponent(contact.contactImageLink) : null
        obj = {
            fullName: contact.personName,
            name: getTextLength(contact.personName, 10),
            image: '/getContactImage/' + contact.personEmailId + '/' + contactImageLink
            // noPicFlag:true
        };
    }

    obj.emailId = contact.personEmailId;
    obj.interactionDate = contact.interactionDate;
    obj.interactionId = contact.interactionId;
    obj.refId = contact.refId;
    obj.title = contact.title;
    obj.description = contact.description;
    obj.companyName = contact.company;
    obj.designation = contact.designation;
    // obj.filterToFetch = filter;
    obj.actionItemId = contact.actionItemId;
    obj.twitterUserName = contact.twitterUserName
    obj.personId = contact.personId;
    obj.participants = contact.participants;
    obj.meetingTimeFormat = "";
    // obj.meetingDateFormat = moment(contact.meetingDate).tz("IST").format("DD MMM");
    obj.meetingDateFormat = moment(contact.meetingDate).fromNow();
    obj.sortDate = contact.meetingDate;

    return obj;
}

function formatLosingTouchContactDetails(contact) {
    var obj = {};

    if(checkRequired(contact.personId) && checkRequired(contact.contactName)){

        var name = getTextLength(contact.contactName,12);
        var image = '/getImage/'+contact.personId;

        obj = {
            fullName:contact.contactName,
            name:name,
            image:image,
            companyName:contact.company,
            designation:contact.designation
        };

        obj.emailId = contact.contactEmailId;
        obj.recordId = contact.contactId;
        obj.filter = 'losingTouch';

    }
    else {
        var contactImageLink = contact.contactImageLink ? encodeURIComponent(contact.contactImageLink) : null
        obj = {
            fullName: contact.contactName,
            name: getTextLength(contact.contactName, 12),
            image: contactImageLink?'/getContactImage/' + contact.contactEmailId + '/' + contactImageLink:null,
            emailId:contact.contactEmailId,
            // noPicFlag:true
            recordId : contact.contactId,
            filter:'losingTouch',
            companyName:contact.company,
            designation:contact.designation
        };
    }

    obj.personId = contact.personId;
    obj.nameNoImg = contact.personName?contact.personName.substr(0,2).toUpperCase():''
    obj.actionItemId = contact.actionItemId;
    obj.filterToFetch = 'losingTouch';
    obj.twitterUserName = contact.twitterUserName;
    obj.publicProfileUrl = contact.publicProfileUrl;
    obj.lastInteractionDate = contact.lastInteractionDate;
    obj.lastInteractionDateFormatted = moment(contact.lastInteractionDate).format("DD MMM YYYY");
    obj.favoriteClass = contact.favorite?'contact-fav':false;
    obj.isImportant = contact.favorite;

    if(contact.favorite){
        atLeastOneImportantContact = true;
    }

    var contactCompany = fetchCompanyFromEmail(contact.contactEmailId);
    var liuCompanyName = fetchCompanyFromEmail(contact.ownerEmailId);

    if(liuCompanyName == "Others") {
        liuCompanyName = null;
    }

    // if(contact.contactEmailId && liuCompanyName != contactCompany && contact.lastInteractionDays>29){
    if(contact.contactEmailId && contact.lastInteractionDays>29){
        return obj;
    }
}

function fetchMeetingDetails(meeting, share, userId) {
    var counter = 1;

    var locationTruncate = '';
    if(meeting.scheduleTimeSlots[0].location){
        locationTruncate = getTextLength(meeting.scheduleTimeSlots[0].location, 50);
    }

    var titleTruncate = '';
    if(meeting.scheduleTimeSlots[0].title) {
        titleTruncate = getTextLength(meeting.scheduleTimeSlots[0].title, 20)
    }
    
    var date = checkRequired(share.timezone) ? moment(meeting.scheduleTimeSlots[0].start.date).tz(share.timezone) : moment(meeting.scheduleTimeSlots[0].start.date);
    var end = checkRequired(share.timezone) ? moment(meeting.scheduleTimeSlots[0].end.date).tz(share.timezone) : moment(meeting.scheduleTimeSlots[0].end.date);
    var obj = {
        start:new Date(date),
        end:new Date(end),
        sortDate:new Date(date.format()),
        meetingDuration: end.diff(date,'minutes'),
        startTime:date.format("hh:mm A"),
        endTime:end.format("hh:mm A"),
        title:meeting.scheduleTimeSlots[0].title || '',
        description:meeting.scheduleTimeSlots[0].description || '',
        location:meeting.scheduleTimeSlots[0].location || '',
        locationTruncate:locationTruncate,
        titleTruncate:titleTruncate,
        locationType:meeting.scheduleTimeSlots[0].locationType || '',
        date:date.format("DD MMM YYYY"),
        dateUpcoming:date.format("MMM DD YYYY"),
        invitationId:meeting.invitationId,
        url:'/today/details/'+ meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId,
        isPreparedMeeting:isPreparedMeeting(meeting,userId),
        isSender:false,
        isAccepted:meeting.scheduleTimeSlots[0].isAccepted || false,
        locationTypePic: getLocationTypePic(meeting.scheduleTimeSlots[0].locationType),
        ifRelatasMailEvent:meeting.actionItemSlot?meeting.actionItemSlot:false,
        ifRelatasMailEventType:meeting.actionItemSlotType,
        meetingIndex:'re-schedule-meeting'+counter,
        slotId:meeting.scheduleTimeSlots[0]._id,
        hoverClass:meeting.actionItemSlot?'non-meeting-row':'meeting-row',
        updateRelatasEvent:'update-relatas-event'+counter,
        displayPopOver:false
    };

    obj.title = getTextLength(obj.title, 25);

    obj.preparedMeetingUrl = obj.isPreparedMeeting ? null : '/today/details/'+ (meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId);

    if(meeting.suggested){
        obj.location = checkRequired(meeting.scheduleTimeSlots[0].suggestedLocation) ? meeting.scheduleTimeSlots[0].suggestedLocation : meeting.scheduleTimeSlots[0].location;
    }

    if(meeting.suggested){
        if(meeting.suggestedBy.userId == userId){
            obj.isSender = true;
            if(meeting.senderId == userId){
                var data1 = getDetailsIfSender(meeting);
                obj.picUrl = data1.picUrl;
                obj.name = data1.name;
                obj.userId = data1.userId
                obj.personEmailId = data1.personEmailId
            }
            else{
                var data3 = getDetailsIfReceiver(meeting);
                obj.picUrl = data3.picUrl;
                obj.name = data3.name;
                obj.userId = data3.userId
                obj.personEmailId = data3.personEmailId
            }
        }
        else{
            if(meeting.senderId == meeting.suggestedBy.userId){
                obj.isSender = true;
                var data5 = getDetailsIfReceiver(meeting);
                obj.picUrl = data5.picUrl;
                obj.name = data5.name;
                obj.userId = data5.userId
                obj.personEmailId = data5.personEmailId
            }
            else{
                if(meeting.selfCalendar){
                    for(var j=0; j<meeting.toList.length; j++){
                        if(meeting.suggestedBy.userId == meeting.toList[j].receiverId){
                            obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                            var fName = meeting.toList[j].receiverFirstName || '';
                            var lName = meeting.toList[j].receiverLastName || '';
                            obj.name = fName+' '+lName;
                            obj.personEmailId = meeting.toList[j].receiverEmailId;
                        }
                    }
                }
                else{
                    obj.picUrl = '/getImage/'+meeting.to.receiverId;
                    obj.name = meeting.to.receiverName;
                }
            }
        }
    }
    else{
        if(meeting.senderId == userId){
            obj.isSender = true;
            var data2 = getDetailsIfSender(meeting);

            obj.picUrl = data2.picUrl;
            obj.name = data2.name;
            obj.personEmailId = data2.personEmailId;
            obj.userId = data2.userId;
        }
        else{
            var data4 = getDetailsIfReceiver(meeting);
            obj.picUrl = data4.picUrl;
            obj.name = data4.name;
            obj.personEmailId = data4.personEmailId;
            obj.userId = data4.userId
        }
    }
    if(!checkRequired(obj.picUrl) && !checkRequired(obj.name)){
        obj.picUrl = '/getImage/'+meeting.senderId;
        obj.name = meeting.senderName;
    }
    if(!checkRequired(obj.picUrl)){
        obj.picUrl = '/images/default.png';
    }
    obj.prepareButtonText = isMeetingAccepted(meeting,userId) ? "Confirmed" : obj.isSender ? "Not Confirmed" : "Confirm";

    if (obj.prepareButtonText == "Confirmed" || obj.prepareButtonText == "Not Confirmed" && obj.isSender === true) {
        obj.buttonClass = "btn btn-transparent";
    }
    else {
        obj.buttonClass = "btn btn-green";
    }

    obj.nameTruncate = getTextLength(obj.name,10);

    var designation = '';
    var company = '';
    for(var i =0;i<meeting.toList.length;i++){

        if(meeting.toList[i].receiverEmailId == obj.personEmailId){
            company = meeting.toList[i].companyName?meeting.toList[i].companyName:'';
            designation = meeting.toList[i].designation?meeting.toList[i].designation:'';
        } else if(meeting.senderEmailId == obj.personEmailId){
            company = meeting.companyName
            designation = meeting.designation
        }

        var companyTruncate = getTextLength(company,10)
        var designationTruncate = getTextLength(designation,10)
    }

    obj.company = company;
    obj.designation = designation;
    obj.companyTruncate = companyTruncate
    obj.designationTruncate = designationTruncate

    return obj;
}

var isMeetingAccepted = function(meeting,userId){

    var isAccepted = false;
    if(meeting.scheduleTimeSlots && meeting.scheduleTimeSlots.length > 0){
        for(var i=0; i<meeting.scheduleTimeSlots.length; i++){
            if(meeting.scheduleTimeSlots[i].isAccepted){
                isAccepted = true;
            }
        }
    }

    if(isAccepted){
        if(meeting.selfCalendar && meeting.toList && meeting.toList.length > 1){
            if(meeting.senderId != userId){
                var exists = false;
                for(var j=0; j<meeting.toList.length > 0; j++){
                    if(meeting.toList[j].receiverId == userId){
                        exists = true;
                    }
                }
                if(!exists){
                    isAccepted = false;
                }
            }
        }
    }

    return isAccepted;
};

var isPreparedMeeting = function(meeting,userId){

    if(meeting.senderId == userId){
        return meeting.isSenderPrepared;
    }
    else if(meeting.selfCalendar){
        var isExists = false;
        var prepared = true;
        for(var j=0; j<meeting.toList.length; j++){
            if(meeting.toList[j].receiverId == userId){
                isExists = true;
                prepared = meeting.toList[j].isPrepared || false;
            }
        }
        return prepared;
    }
    else{
        if(meeting.to.receiverId == userId){
            return meeting.to.isPrepared || false;
        }else return true;
    }
};

var getLocationTypePic = function(meetingLocationType){

    switch (meetingLocationType) {
        case 'In-Person':
            return "fa fa-map-marker";
            break;
        case 'Phone':
            return "fa fa-mobile-phone";
            break;
        case 'Skype':
            return "fa fa-skype";
            break;
        case 'Conference Call':
            return "fa fa-mobile-phone";
            break;
        default:return "fa fa-map-marker";
            break;
    }
};

var getDetailsIfSender = function(meeting){

    var obj = {};
    if(meeting.selfCalendar == true){
        if(meeting.toList.length > 0){
            var isDetailsExists = false;
            for(var i=0; i<meeting.toList.length; i++){
                if(meeting.toList[i].isPrimary){
                    isDetailsExists = true;
                    obj.picUrl = '/getImage/'+meeting.toList[i].receiverId;
                    var fName = meeting.toList[i].receiverFirstName || '';
                    var lName = meeting.toList[i].receiverLastName || '';
                    obj.name = fName+' '+lName;
                    obj.userId = meeting.toList[i].receiverId;
                    obj.personEmailId = meeting.toList[i].receiverEmailId;
                }
            }
            if(!isDetailsExists){
                for(var j=0; j<meeting.toList.length; j++){
                    if(checkRequired(meeting.toList[j].receiverId)){
                        isDetailsExists = true;
                        obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                        var fName2 = meeting.toList[j].receiverFirstName || '';
                        var lName2 = meeting.toList[j].receiverLastName || '';
                        obj.name = fName2+' '+lName2;
                        obj.userId = meeting.toList[j].receiverId
                        obj.personEmailId = meeting.toList[j].receiverEmailId;
                    }
                }
            }
            if(!isDetailsExists){
                obj.picUrl = '/images/default.png';
                obj.name = meeting.toList[0].receiverEmailId;
                obj.personEmailId = meeting.toList[0].receiverEmailId;
            }
        }
    }
    else{
        if(meeting.to){
            if(meeting.to.receiverId){
                obj.picUrl = '/getImage/'+meeting.to.receiverId;
                obj.name = meeting.to.receiverName;
                obj.userId = meeting.to.receiverId
                obj.personEmailId = meeting.to.receiverEmailId;
            }
            else{
                obj.picUrl = '/images/default.png';
                obj.name = meeting.to.receiverEmailId;
                obj.personEmailId = meeting.to.receiverEmailId;
            }
        }
    }
    return obj;
};

var getDetailsIfReceiver = function(meeting){
    var obj = {};
    if(meeting.senderId){
        obj.picUrl = '/getImage/'+meeting.senderId;
        obj.name = meeting.senderName;
        obj.userId = meeting.senderId;
        obj.personEmailId = meeting.senderEmailId;
    }
    else{
        obj.picUrl = '/images/default.png';
        obj.name = meeting.senderEmailId;
        obj.personEmailId = meeting.senderEmailId;
    }
    return obj;
};

function getAllCommitCutOffDates($scope, $http) {
    $scope.commitCutOffLoaded = false;

    $http.get("/review/get/all/commit/cutoff")
        .success(function (response) {

            if(response){
                $scope.commitCutOffLoaded = true;
                $scope.commitRange[0].cuttOffDate = "Commit Close Date: "+moment(response.week).tz("UTC").format(standardDateFormat()); 
                $scope.commitRange[1].cuttOffDate = "Commit Close Date: "+moment(response.month).tz("UTC").format(standardDateFormat())
                $scope.commitRange[2].cuttOffDate = "Commit Close Date: "+moment(response.quarter.startOfQuarter).tz("UTC").format(standardDateFormat())
                
                $scope.commitRange[0].daysLeft = moment().diff(response.week, 'days')*-1;
                $scope.commitRange[1].daysLeft = moment().diff(response.month, 'days')*-1;
                $scope.commitRange[2].daysLeft = moment().diff(response.quarter.startOfQuarter, 'days')*-1;
            }
        })
}

function fetchCommits($scope,$http,url,mode) {
    $scope.loadingCommits = true; 

    if(!mode){
        mode = "MONTHLY"
        url = '/review/commits/month';
    }

    $http.get(url)
        .success(function (response) {

            $scope.commits = {
            selfCommitValue:0,
            selfCommitValueFormat:0,
            }

            if(response){

                var editAccess = false;
                var selfCommitValue = 0;
                var commitCutOffDate = null;
                var nextCommitCutOffDate = null;

                checkCommitCutOffLoaded();
                function checkCommitCutOffLoaded(){
                    if($scope.commitCutOffLoaded){
                        if(mode == "WEEKLY"){
                            commitCutOffDate = new Date($scope.commitRange[0].cuttOffDate);
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"week"));
                            selfCommitValue = response.commitsCurrentWeek.week.userCommitAmount; 

                        } else if(mode == "QUARTERLY"){
                            commitCutOffDate = new Date($scope.commitRange[2].cuttOffDate )
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"quarter"));
                            selfCommitValue = response.commitCurrentQuarter.quarter.userCommitAmount; 

                        } else {
                            commitCutOffDate = new Date($scope.commitRange[1].cuttOffDate );
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"month"));
                            selfCommitValue = response.commitCurrentMonth.month.userCommitAmount; 

                        }
                    } else {
                        setTimeOutCallback(500,function () {
                            checkCommitCutOffLoaded();
                        })
                    }
                }

                if(commitCutOffDate && moment(commitCutOffDate).endOf('day').toDate() >= new Date()){
                    editAccess = true;
                    $scope.canCommitForNext = false;
                } else {
                    $scope.canCommitForNext = true;
                    $scope.nextCommitCutOffDate = nextCommitCutOffDate;
                }

                $scope.commits = {
                    editAccess:editAccess,
                    selfCommitValue:selfCommitValue,
                }

                if(mode == 'WEEKLY')
                    $scope.commitRange[0].commitforNext = $scope.canCommitForNext;
                else if(mode == 'MONTHLY')
                    $scope.commitRange[1].commitforNext = $scope.canCommitForNext;
                else if(mode == 'QUARTERLY')
                    $scope.commitRange[2].commitforNext = $scope.canCommitForNext;

                $scope.selfCommitValue = $scope.commits.selfCommitValue;
                $scope.loadingCommits = false; 

            }
        });
}

function fetchCommitV2($scope,$http,url,mode) {
    if(users){
        if(!$scope.teamCommitsViewing) {
            url = fetchUrlWithParameter(url,"userId",users);
        } else {
            url = fetchUrlWithParameter(url,"hierarchylist",users)
        }
    }

    if(!mode){
        mode = "month"
    }

    $http.get(url)
        .success(function (response) {

            $scope.commits = {
                oppValue:0,
                selfCommitValue:0,
                opps:[],
                selfCommitValueFormat:0,
                response:null,
                graph:{}
            }

            if(response){

                $rootScope.commitStage = response.commitStage
                share.commitStage = response.commitStage

                var editAccess = false;
                var selfCommitValue = 0;
                var commitCutOffDate = null;
                var nextCommitCutOffDate = null;

                checkCommitCutOffLoaded();
                function checkCommitCutOffLoaded(){
                    if(share.commitCutOffObj){

                        if(mode == "week"){
                            commitCutOffDate = new Date(share.commitCutOffObj.week);
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"week"));
                        } else if(mode == "quarter"){
                            commitCutOffDate = new Date(share.commitCutOffObj.quarter)
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"quarter"));
                        } else {
                            commitCutOffDate = new Date(share.commitCutOffObj.month);
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"month"));
                        }
                    } else {
                        setTimeOutCallback(500,function () {
                            checkCommitCutOffLoaded();
                        })
                    }
                }

                if(commitCutOffDate && new Date(commitCutOffDate) >= new Date()){
                    editAccess = true;
                    $scope.canCommitForNext = false;
                } else {
                    $scope.canCommitForNext = true;
                    $scope.nextCommitCutOffDate = nextCommitCutOffDate;
                }

                var oppsToDisplay = response.opps;

                if(mode == "week"){
                    selfCommitValue = response.commitsCurrentWeek.week.userCommitAmount?response.commitsCurrentWeek.week.userCommitAmount:0
                    if(new Date()> new Date(response.commitWeek)){
                        oppsToDisplay = response.commitsCurrentWeek.opportunities
                    }

                } else if(mode == "quarter"){
                    if(response.commitCurrentQuarter.quarter.userCommitAmount){
                        selfCommitValue = response.commitCurrentQuarter.quarter.userCommitAmount;
                    }
                    if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                        oppsToDisplay = response.commitCurrentQuarter.opportunities
                    }

                } else {
                    if(response.commitCurrentMonth.month.userCommitAmount){
                        selfCommitValue = response.commitCurrentMonth.month.userCommitAmount;
                    }

                    if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                        oppsToDisplay = response.commitCurrentMonth.opportunities
                    }
                }

                var oppValue = getOppValInCommitStageForSelectedMonth(oppsToDisplay,response.commitMonth,share.primaryCurrency,share.currenciesObj);

                $scope.commits = {
                    oppValue:oppValue?getAmountInThousands(oppValue,2,share.primaryCurrency=="INR"):0,
                    editAccess:editAccess,
                    selfCommitValue:selfCommitValue,
                    opps:oppsToDisplay,
                    selfCommitValueFormat:getAmountInThousands(selfCommitValue,2,share.primaryCurrency=="INR"),
                    response:response
                }

                if(mode != 'week'){
                    getTargetsAndAchievements($http,users,startDate,null,function (insights) {
                        if(insights){
                            $scope.commits.graph = setGraphValues(insights,response,share,mode,oppValue,$scope);
                        }
                    });
                }

                if($scope.teamCommitsViewing){
                    teamCommitsList($scope,share,response,mode,users,$http)
                }
            }

            setTimeOutCallback(100,function () {
                $scope.actionloadingData = false;
                $scope.loadingData = false;
            })
        });
}

function stripHtmltags(taskName) {

    if ( !taskName || (taskName===''))
        return false;
    else
        taskName = taskName.toString();

    return taskName.replace(/<[^>]*>/g, '');
}

// function for controller today-secondary-insights

function getOverdueInsights(filter, $http, $scope, $rootScope, share) {
    switch(filter) {
        case 'staleOpportunity': getStaleOpportunity($scope, $rootScope, $http);
                                break;
        case 'dealsAtRisk': getDealsAtRisk($scope, $http, share);
                                break;
        case 'taskOverdue': getOverdueTasks($scope, $http);
                                break;
        case 'mailResponsePending': getMailResponsePending($scope, $http);
                            break;
        default: break;

    }
}

function getStaleOpportunity($scope, $rootScope, $http) {
    var url = 'reports/get/opportunities/v2';
    var filterObj = {};
    var filter = [];
    var startDate = moment(new Date("01 Jan 2015"));
    var endDate = moment().subtract(1, "days");

    filter.push({
        includeDateRange: true,
        type: "closeDate",
        start: startDate,
        end: endDate
    })

    filter.push({
        name: $rootScope.liuEmailId,
        type: "userEmailId"
    })

    filterObj.filters = filter;

    $http.post(url,filterObj)
        .success(function(response) {
            $scope.staleOpps = _.filter(response.opps, function(opp) {
                return !(opp.stageName == 'Close Won' || opp.stageName == 'Close Lost')
            });

        })
}

/**
 * Created by naveen on 10/8/15.
 */

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

var nl2br = function (str, isXhtml) {

    if (typeof str === 'undefined' || str === null) {
        return ''
    }

    var breakTag = (isXhtml || typeof isXhtml === 'undefined') ? '<br ' + '/>' : '<br>'

    return (str + '')
        .replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2')
}

function getBrowserLocation(callback) {

    if (navigator.geolocation && !_.includes(window.location.hostname, "localhost")) {
        navigator.geolocation.getCurrentPosition(function (location) {
            callback({
                lat:location.coords.latitude,
                lng:location.coords.longitude
            })
        });
    } else {
        callback("Geolocation is not supported.");
    }
}

function getPosition (position){
    return {
        lat:position.coords.latitude,
        lng:position.coords.longitude
    }
}

function isTestingEnvironment() {
    var domain = window.location.hostname;

    var localDomain = 'localhost';
    var showcaseDomain = 'showcase';
    var exampledevDomain = 'exampledev';
    var relatasDomain = 'relatas.relatas';

    var isTestingEnv= false;
    if((domain.indexOf(localDomain) > -1) || (domain.indexOf(showcaseDomain) > -1) || (domain.indexOf(exampledevDomain) > -1) || (domain.indexOf(relatasDomain) > -1)) {
        isTestingEnv= true;
    }

    return isTestingEnv;
}

var stringToColour = function(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}

function getTextLength(text,maxLength){
    if(!checkRequired(text))
        return "";

    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

function setTimeOutCallback(delay,callback){
    setTimeout(function () {
        callback()
    },delay)
}

function checkRequired(data){
    if (data === '' || data === null || data === undefined || data === "undefined") {
        return false;
    }
    else{
        return true;
    }
}

function get_connection_interactions_tweets($http,personId,twitterUserName,uName,name,designation,companyName,filter,location,meetingDate,meetingTitle,callback) {

    var url = "/contact/connections/interactions?id="+personId+"&skip=0&limit=30"+'&twitterUserName='+twitterUserName

    if(personId) {
        $http.get(url)
            .success(function (response) {

                var lastInteractionType = null;
                var subject = '';
                var over = '';
                var messageReConnect = '';
                var dateOnly = '';

                if(response && response.SuccessCode){

                    uName = uName?uName:response.Data.profile.publicProfileUrl;
                    designation = response.Data.profile.designation;
                    companyName = response.Data.profile.companyName;
                    var fullName = response.Data.profile.firstName +" "+ response.Data.profile.lastName
                    var calendarLink = " My calendar is readily available here: "+"www.relatas.com/"+uName

                    if(filter == "travellingToLocation"){
                        subject = "In "+location+" on "+meetingDate+". ";
                        messageReConnect = getMessageForTravellingTo(response.Data,meetingDate,location)
                    }

                    if(filter == "peopleNearMeetingLocation"){
                        subject = "In "+location+" today";
                        messageReConnect = getMessageForTravellingTo(response.Data,meetingDate,location)
                    }

                    if(!filter || filter == "losingTouch") {
                        subject = "Reconnecting "+response.Data.profile.firstName +" & "+name
                        messageReConnect = getMessageForLosingTouch(response.Data);

                        // messageReConnect = messageReConnect+calendarLink
                        messageReConnect = messageReConnect;
                    }

                    if(filter == "meetingFollowUp"){
                        subject = "Meeting Minutes : "+meetingTitle
                        var mfuMsg = ". \nHere are some of the meeting notes for your ready reference ";

                        if(lastInteractionType){
                            over = "over "+lastInteractionType
                        }

                        messageReConnect = "It was nice catching up with you "+over+dateOnly+" for our discussion on "+
                            meetingTitle+mfuMsg+". " +"\n<"+"please add meeting notes>"

                        if(meetingDate){
                            var endDate = moment();
                            var daysSinceMet = endDate.diff(new Date(meetingDate), 'days');

                            var formatMeetingDate = moment(meetingDate).format('DD MMM YYYY')
                            messageReConnect = "It was nice catching up with you "+over+ "on "+formatMeetingDate+" for our discussion on "+
                                meetingTitle+mfuMsg+". " +"\n<"+"please add meeting notes>"
                        }
                    }

                } else {
                    messageReConnect = "\nIt's been long since we last interacted. How have you been?" +
                        // " It would be great to find some time to catch up."+calendarLink;
                        " It would be great to find some time to catch up.";
                }

                messageReConnect = "Hi "+name+",\n"+messageReConnect+"\n\n"+getSignature(fullName,designation,companyName,uName)

                callback(response,{
                    messageReConnect:messageReConnect,
                    subject:subject,
                    disableSendBtn:false
                })
            });
    } else {
        callback(null,{
            messageReConnect:"Please add contact's email ID to send a mail through Relatas.",
            subject:'',
            disableSendBtn: true
        })
    }
}

function getSignature(fullName,designation,companyName,uName) {
    return "---\n"+fullName+"\n"+designation+", "+companyName+"\n"+"www.relatas.com/"+uName+'\n\n Powered by Relatas';
}

function getUserSignature(fullName,designation,companyName,domain,publicProfileUrl) {
    return "---\n"+fullName+"\n"+designation+", "+companyName+"\n"+domain+"/"+publicProfileUrl+'\n\n Powered by Relatas';
}

function numberWithCommas(x,ins) {

    if(ins){
        return numberWithCommas_ins(x);
    } else {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

function numberWithCommas_ins(x) {
    x=String(x).toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'),x.length);
    x = Math.floor(x);
    x=x.toString();
    var lastThree = x.substring(x.length-3);
    var otherNumbers = x.substring(0,x.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
}

function setQuarter(startMonth,timezone) {

    var qtrObj = {},
        array = [],
        months = [],
        currentYr = new Date().getFullYear(),
        currentMonth = new Date().getMonth(),
        fyStartDate = new Date(moment().startOf('month'));

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    var twelve_months_for_loop = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November"];

    fyStartDate.setMonth(twelve_months_for_loop.indexOf(startMonth))

    if(currentMonth<twelve_months_for_loop.indexOf(startMonth)){
        fyStartDate.setFullYear(currentYr-1)
    }

    months.push(fyStartDate)

    _.each(twelve_months_for_loop,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    qtrObj.quarter1 = {start:moment(months[0]).tz(timezone).startOf('month').format(),end:moment(months[2]).tz(timezone).endOf('month').format()}
    qtrObj.quarter2 = {start:moment(months[3]).tz(timezone).startOf('month').format(),end:moment(months[5]).tz(timezone).endOf('month').format()}
    qtrObj.quarter3 = {start:moment(months[6]).tz(timezone).startOf('month').format(),end:moment(months[8]).tz(timezone).endOf('month').format()}
    qtrObj.quarter4 = {start:moment(months[9]).tz(timezone).startOf('month').format(),end:moment(months[11]).tz(timezone).endOf('month').format()}

    var currentQuarter = "quarter4"

    if(startMonth =='January'){

        if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    if(startMonth =='April'){

        if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    array.push(qtrObj.quarter1,qtrObj.quarter2,qtrObj.quarter3,qtrObj.quarter4)
    return {array:array,obj:qtrObj,currentQuarter:currentQuarter};
}

function getMessageForLosingTouch(data) {
    var message = ''

    if(data.ccLastInteraction[0]){
        var fName = data.ccLastInteraction[0].firstName?data.ccLastInteraction[0].firstName:''
        var lName = data.ccLastInteraction[0].lastName?data.ccLastInteraction[0].lastName:''
        var ccLastInteractedDate = moment(data.ccLastInteraction[0].interactionDate).tz("UTC").format("DD MMM YYYY")

        if(data.commonConnections>100 && data.latestTweet) {
            message = 'Just saw your tweet " '+ stripHTMLTags(data.latestTweet.tweet) + ' " and remembered that we last '+
                getInteractionTypeResponse(data.lastInteraction)+
                ". We should catch-up, there's been much going on at my front. \n\n"+
                'I recently interacted with '+fName+' '+lName+' on '+ccLastInteractedDate+' and remembered we share a common connection. ' +
                'How about catching up sometime soon?'
        } else if(data.commonConnections>0 && !data.latestTweet){
            message = 'I recently interacted with '+fName+' '+lName+' on '+ccLastInteractedDate+' and remembered we share a common connection.\n\n'+
                'You and I last '+ getInteractionTypeResponse(data.lastInteraction)+" and it's time for us to catch up again. How about catching up sometime soon?"
        } else if(data.latestTweet){
            message = 'Just saw your tweet " '+ stripHTMLTags(data.latestTweet.tweet) + ' " and remembered that we last '+
                getInteractionTypeResponse(data.lastInteraction)+
                ". We should catch-up, there's been much going on at my front. \n\n"
        } else {
            message = "It's been sometime since we last "+getInteractionTypeResponse(data.lastInteraction)+
                " and then there's been silence from my end. Well, all good things must come to an end, so here's my email breaking the silence. :-)"
        }
    } if(data.lastInteraction[0]){
        message = "It's been sometime since we last "+getInteractionTypeResponse(data.lastInteraction)+
            " and then there's been silence from my end. Well, all good things must come to an end, so here's my email breaking the silence. :-)"
    }  else {
        message = "It's been a long time since we last interacted"+". How about catching up sometime soon?"
    }

    return message;
}

function getMessageForTravellingTo(data,meetingDate,location) {

    var message = '';
    var ccMessage ='';

    if(data.ccLastInteraction[0]){

        var fName = data.ccLastInteraction[0].firstName?data.ccLastInteraction[0].firstName:''
        var lName = data.ccLastInteraction[0].lastName?data.ccLastInteraction[0].lastName:''
        var ccLastInteractedDate = moment(data.ccLastInteraction[0].interactionDate).tz("UTC").format("DD MMM YYYY")

        message = "It's been sometime since we last "+getInteractionTypeResponse(data.lastInteraction)+
            ". How have things been at your end?\n\n"+
            "Are you around "+location+" on "+meetingDate+"? We could catch up over coffee.\n\n"

        if(data.commonConnections>0 && data.ccLastInteraction[0]){
            ccMessage = "Also, I noticed that we have "+data.commonConnections+" friends in common. Do you know "+fName+" "+lName+" well?\n\n"+
                "Let me know if we could catch up."
        }
    } else if(data.lastInteraction && data.lastInteraction[0]) {

        var lastIntDt = moment(data.lastInteraction[0].interactionDate).tz("UTC").format("DD MMM YYYY")
        message = "It's been sometime since we last interacted on "+lastIntDt+" and it's time for us to catch up again. How about catching up sometime soon?"
    } else {
        message = "It's been more than six months since we last interacted "+" and it's time for us to catch up again. How about catching up sometime soon?"
    }

    var subject = "In "+location+" on "+meetingDate+". ";

    return message+ccMessage;
}

function getInteractionTypeResponse(interaction) {

    if(interaction && interaction[0] && interaction[0].interactionDate){

        var interactionDate = moment(interaction[0].interactionDate).tz("UTC").format("DD MMM YYYY")

        if(interaction[0].type === 'email'){
            return 'exchanged emails on '+interactionDate+""
        } else if(interaction[0].type === 'call'){
            return 'spoke on '+interactionDate+""
        } else if(interaction[0].type === 'meeting'){
            return 'met on '+interactionDate+" "
        } else if(interaction[0].type === 'sms'){
            return 'exchanged SMS on '+interactionDate+""
        } else {
            return 'interacted '
        }
    } else {
        return 'interacted '
    }
}

function stripHTMLTags(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

function removeDuplicates(arr) {
    var end = arr.length;

    for (var i = 0; i < end; i++) {
        for (var j = i + 1; j < end; j++) {
            if (arr[i].pageNumber == arr[j].pageNumber) {
                var shiftLeft = j;
                for (var k = j + 1; k < end; k++, shiftLeft++) {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for (var i = 0; i < end; i++) {
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

var findOne = function (haystack, arr) {
    return arr.some(function (v) {
        return haystack.indexOf(v) >= 0;
    });
};

function getMaxOfArray(numArray) {
    return Math.max.apply(null, numArray);
}

function getMinOfArray(numArray) {
    return Math.min.apply(null, numArray);
}

function scaleBetween(unscaledNum, min, max,minAllowed,maxAllowed) {

    if(!minAllowed){
        minAllowed = 0;
    }

    if(!maxAllowed) {
        maxAllowed = 100;
    }

    if ((min == 0 && max == 0) || (min == max)) {
        return minAllowed
    } else {
        return (maxAllowed - minAllowed) * (unscaledNum - min) / (max - min) + minAllowed;
    }
}

function sessionTimeoutReload() {
    window.location = "/"
}

function getInteractionIconType(interactionType) {
    switch (interactionType){
        case 'meeting': return 'fa fa-calendar-check-o margin0';break;
        case 'email': return 'fa fa-envelope margin0';break;
        case 'call': return 'fa fa-phone margin0';break;
        case 'sms': return 'fa fa-reorder margin0';break;
        case 'twitter': return 'fa fa-twitter-square margin0';break;
    }
}

function fetchCompanyFromEmail(email){

    if(email){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            return removableTextList.indexOf(word.trim()) > -1
        })
        words = _.sortBy(words, function(word){return -(word.length);})

        return (personalMailDomains.indexOf(words[0]) > -1) ? "Others" : (words[0])
    } else {
        return null;
    }
}

function standardDateFormat(){
    return "DD MMM YYYY"
}

function oppTargetMetNotifications($http,$scope){

    // $http.get("/opportunities/by/month/year")
    //     .success(function (response) {
    //
    //         if(response.SuccessCode){
    //             var receivers = [];
    //             var salutation = "Hi "
    //             if(response.companyHead){
    //                 receivers.push(response.companyHead.emailId)
    //                 salutation = salutation+response.companyHead.firstName
    //             }
    //             if(response.reportingManager){
    //                 receivers.push(response.reportingManager.emailId)
    //                 salutation = salutation+response.reportingManager.firstName
    //             }
    //         }
    //
    //     });
}

function getErrorMessages($rootScope,$http,page,callback){

    $http.get('/error/messages/'+page)
        .success(function (response) {
            $rootScope.errorMessages = response.Data;
            if(callback){
                callback(response.Data)
            }
        })
}

function getLiuProfile($scope, $http, share,$rootScope,callback){

    $http.get('/profile/get/current/web')
        .success(function (response) {

            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                share.l_user = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.appConfig = response.appConfig;
                share.liuData = response.Data;

                $rootScope.currentUser = response.Data;

                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }

                share.loadAppConfig = response.appConfig;
                share.lUseEmailId = response.Data.emailId;

                if(response.companyDetails && response.companyDetails.opportunitySettings){
                    $rootScope.isProductRequired = response.companyDetails.opportunitySettings.productRequired;
                    $rootScope.isRegionRequired = response.companyDetails.opportunitySettings.regionRequired;
                    $rootScope.isVerticalRequired = response.companyDetails.opportunitySettings.verticalRequired;
                    $rootScope.isSourceRequired = response.companyDetails.opportunitySettings.sourceRequired;
                }

                $rootScope.dmSelectionRequired = false;
                $rootScope.inflSelectionRequired = false;
                $rootScope.partnerSelectionRequired = false;

                if(response.companyDetails && response.companyDetails.customerSetting && response.companyDetails.customerSetting.length>0){
                    _.each(response.companyDetails.customerSetting,function (el) {
                        if(el.name == "Decision Makers"){
                            $rootScope.dmSelectionRequired = el.mandatory;
                        } else if(el.name == "Influencers"){
                            $rootScope.inflSelectionRequired = el.mandatory;
                        } else if(el.name == "Partners/Resellers"){
                            $rootScope.partnerSelectionRequired = el.mandatory;
                        }
                    })
                }

                $rootScope.companyDetails = response.companyDetails
                share.companyDetails = response.companyDetails
                var regionOwner = response.Data.regionOwner
                var verticalOwner = response.Data.verticalOwner
                var productTypeOwner = response.Data.productTypeOwner

                $rootScope.hasExcpAccess = false;
                if((response.Data.regionOwner && response.Data.regionOwner.length>0) || (response.Data.productTypeOwner && response.Data.productTypeOwner.length>0) || (response.Data.verticalOwner && response.Data.verticalOwner.length>0)){
                    $rootScope.hasExcpAccess = true;
                }

                var regions = response.Data.regionOwner && response.Data.regionOwner.length>0?"Regions: "+regionOwner.join(',')+" \n": ''
                var products = response.Data.productTypeOwner && response.Data.productTypeOwner.length>0?"Products: "+productTypeOwner.join(',')+" \n": ''
                var verticals = response.Data.verticalOwner && response.Data.verticalOwner.length>0?"Verticals: "+verticalOwner.join(',')+" \n": ''

                $rootScope.orgHead = response.Data.orgHead;
                $rootScope.netGrossMargin = response.companyDetails.netGrossMargin;

                $rootScope.accessHelpTxt = "You have access to \n"+regions+products+verticals+"\n And/Or you are part of opportunity internal team."
                $rootScope.notificationForReportingManager = response.companyDetails.notificationForReportingManager;
                $rootScope.notificationForOrgHead = response.companyDetails.notificationForOrgHead;

                if($rootScope.notificationForOrgHead){
                    $rootScope.mailOrgHead = true;
                }

                if($rootScope.notificationForReportingManager){
                    $rootScope.mailRm = true;
                }

                if(share.liuDetails){
                    share.liuDetails(response.Data);
                }

                if(share.setUserId){
                    share.setUserId(response.Data._id);
                }

                if(share.setUserEmailId){
                    share.setUserEmailId(response.Data.emailId);
                }

                if(share.setCompanyDetails){
                    share.setCompanyDetails(response.companyDetails,response.Data)
                }

                if(response.companyDetails && response.companyDetails.opportunityStages && response.companyDetails.opportunityStages.length>0){
                    share.opportunityStages = response.companyDetails.opportunityStages;

                } else {
                    share.opportunityStages = [{order:1,name:'Prospecting'},
                        {order:2,name:'Evaluation'},
                        {order:3,name:'Proposal'},
                        {order:4,name:'Close Won'},
                        {order:5,name:'Close Lost'}];
                }

                share.opportunityStages = _.sortBy(share.opportunityStages, "order");

                if(share.loadOpps){
                    share.loadOpps();
                }

                if(share.setCompanyId){
                    share.setCompanyId(response.Data.companyId)
                }

                if(callback){
                    callback(response)
                }
            }
            else{

            }
        }).error(function (data) {
    })
}

function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
}

function setCookie(name,value,days) {
    var expires = "";
    var date = new Date();

    if (days) {
        date.setTime(date.getTime() + (days*24*60*60*1000));
    } else {
        date = new Date(moment().add(2,"h"))
    }

    expires = "; expires=" + date.toUTCString();

    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}

function oppStageStyle(stage,order,bg,border,colorCode){

    order = order+1;

    var colorAll = {
        1:{background:"#f1c40f","border-left": "3px solid #f1c40f"},
        2:{background:"#3498db","border-left": "3px solid #3498db"},
        3:{background:"#90C695","border-left": "3px solid #90C695"},
        4:{background:"#f791d7","border-left": "3px solid #f791d7"},
        5:{background:"#5b7fd2","border-left": "3px solid #5b7fd2"},
        6:{background:"#d2925b","border-left": "3px solid #d2925b"},
        7:{background:"#4286f4","border-left": "3px solid #4286f4"},
        "Close Lost":{background:"#e74c3c","border-left": "3px solid #e74c3c"},
        "Close Won":{background:"#8ECECB","border-left": "3px solid #8ECECB"}
    }

    var colorBorder = {
        1:{"border-left": "3px solid #f1c40f"},
        2:{"border-left": "3px solid #3498db"},
        3:{"border-left": "3px solid #90C695"},
        4:{"border-left": "3px solid #f791d7"},
        5:{"border-left": "3px solid #5b7fd2"},
        6:{"border-left": "3px solid #d2925b"},
        7:{"border-left": "3px solid #4286f4"},
        "Close Lost":{"border-left": "3px solid #e74c3c"},
        "Close Won":{"border-left": "3px solid #8ECECB"}
    }

    var colorBg = {
        1:{background:"#f1c40f"},
        2:{background:"#3498db"},
        3:{background:"#90C695"},
        4:{background:"#f791d7"},
        5:{background:"#5b7fd2"},
        6:{background:"#d2925b"},
        7:{background:"#4286f4"},
        "Close Lost":{background:"#e74c3c"},
        "Close Won":{background:"#8ECECB"}
    }

    var onlyColors = {
        1:"#f1c40f",
        2:"#3498db",
        3:"#90C695",
        4:"#f791d7",
        5:"#5b7fd2",
        6:"#d2925b",
        7:"#4286f4",
        "Close Lost":"#e74c3c",
        "Close Won":"#8ECECB"
    }

    if(stage == "Close Lost" || stage == "Close Won"){

        if(bg){
            return colorBg[stage]
        }

        if(border){
            return colorBorder[stage]
        }

        if(!bg && !border && !colorCode){
            return colorAll[order]
        }

        if(colorCode){
            return onlyColors[stage]
        }

    } else {

        if(isNaN(order)) {
            if(bg){
                return {background:"#aaa"}
            }

            if(border){
                return {"border-left": "3px solid #aaa"}
            }

            if(!bg && !border && !colorCode){
                return {background:"#aaa","border-left": "3px solid #aaa"}
            }

            if(colorCode){
                return onlyColors[order]
            }

        } else {

            if(bg){
                return colorBg[order]
            }

            if(border){
                return colorBorder[order]
            }

            if(!bg && !border && !colorCode){
                return colorAll[order]
            }
            
            if(colorCode){
                return onlyColors[order]
            }
        }

    }
}

function notifyRelevantPeople($http,subject,body,contactDetails){

    var respSentTimeISO = moment().format();

    var obj = {
        email_cc:contactDetails.cc && contactDetails.cc.length>0 && contactDetails.cc != contactDetails.contactEmailId?contactDetails.cc:null,
        receiverEmailId:contactDetails.contactEmailId,
        receiverName:contactDetails.contactEmailId,
        message:body,
        subject:subject,
        receiverId:contactDetails.personId,
        docTrack:true,
        trackViewed:true,
        remind:null,
        respSentTime:respSentTimeISO,
        isLeadTrack:false,
        newMessage:true
    };

    $http.post("/messages/send/email/single/web",obj, {
            ignoreLoadingBar: true
        }).success(function(response){
        });
}

function replyToEmail($scope,$http,subject,body,prevMessage,emailId,internalMailRecName,callback){

    var respSentTimeISO = moment().format();
    var forOutlookBody = body;

    if(!checkRequired(subject)){
        toastr.error("Please enter the subject.")
    }
    else if(!checkRequired(body)){
        toastr.error("Please enter the message.")
    }
    else{

        if(prevMessage && prevMessage.dataObj != null){
            body = body+'\n\n\n'+prevMessage.dataObj.bodyContent;
            $scope.reminder = prevMessage.dataObj.compose_email_remaind
        }

        var obj = {
            email_cc:$scope.add_cc && $scope.add_cc.length>0?$scope.add_cc:null,
            receiverEmailId:emailId,
            receiverName:emailId,
            message:body,
            subject:subject,
            receiverId:null,
            docTrack:true,
            trackViewed:true,
            remind:$scope.reminder,
            respSentTime:respSentTimeISO,
            isLeadTrack:false,
            newMessage:prevMessage?false:true,
            originalBody:forOutlookBody
        };

        if(internalMailRecName){
            obj.receiverEmailId = emailId
            obj.receiverName = internalMailRecName
            obj.receiverId = ''
        }

        if(prevMessage && prevMessage != null && prevMessage.updateReplied){
            obj.updateReplied = true;
            obj.refId = prevMessage.dataObj.interaction.refId
        }

        //Used for Outlook
        if(prevMessage && prevMessage.dataObj && prevMessage.dataObj.interaction && prevMessage.dataObj.interaction.refId){
            obj.id = prevMessage.dataObj.interaction.refId
        }

        $http.post("/messages/send/email/single/web",obj)
            .success(function(response){
                if(response.SuccessCode){
                    if(!internalMailRecName){
                        toastr.success("Email sent successfully");
                    }
                }
                else{
                    toastr.error("Email not sent. Please try again later");
                }
                
                if(callback){
                    callback(response)
                }
            })
    }
}

function getWebsiteByDomainTypeOrder(accountWebsites){
    var websiteToShow = null;
    for(var i = 0;i<accountWebsites.length;i++){
        var last = accountWebsites[i].substr(accountWebsites[i].length - 6);
        if(_.includes(last,".com")){
            websiteToShow = accountWebsites[i]
            if(websiteToShow){
                break;
            }
        }

        if(_.includes(last,".co.")){
            websiteToShow = accountWebsites[i]
            if(websiteToShow){
                break;
            }
        }
    }
    
    return websiteToShow;
}

function getCommitDayTimeEnd(companyDetails,timezone) {

    var dateTime = moment().day("Monday");
    if(companyDetails.commitDay){
        dateTime = moment().day(companyDetails.commitDay)
    }

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    dateTime = moment(dateTime).tz(timezone)

    if(companyDetails.commitHour){
        dateTime = dateTime.hour(parseInt(companyDetails.commitHour))
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    } else {
        dateTime = dateTime.hour(18)
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    }

    if(new Date(dateTime)< new Date()){
        dateTime = moment(dateTime).add(1,'week') //well past commit time
    }

    return new Date(dateTime);
}

function getCurrentDocumentNumberForCompany(companyDetails) {

    return companyDetails.documentNumber;
}

function getMonthCommitCutOff(companyDetails) {

    //TODO implement admin settings
    var monthCommitCutOff = null;
    if(!monthCommitCutOff){
        monthCommitCutOff = 10
    }

    return new Date(moment(moment().startOf('month')).add(monthCommitCutOff,"day"));
}

function getQuarterCommitCutOff(share) {
    //TODO implement admin settings
    return new Date(share.quarterCommitCutOff.startOfQuarter);
}

function checkForAlphanumericChars(value){
    var alphanumericFound = false;

    if(value && String(value).match(/[a-z]/i)) {
        alphanumericFound = true;
    }

    return alphanumericFound
}

function buildDateObj(weekYear) {
    var year = weekYear.substr(-4);
    var week = weekYear.split(year)[0]
    return new Date(moment().week(week).year(year));
}

var updateEmailOpen = function($http,emailId,trackId,userId){

    if(checkRequired(emailId) && checkRequired(trackId) && checkRequired(userId)){
        $http.get('/track/email/open/'+emailId+'/track/id/'+trackId+'/user/'+userId)
            .success(function(response){

            });
    }
};

function catchGlobalErrors(){
    window.onerror = function(msg, url, line, col, error) {

        var extra = !col ? '' : '\ncolumn: ' + col;
        extra += !error ? '' : '\nerror: ' + error;

        console.log("Error: " + msg + "\nurl: " + url + "\nline: " + line + extra);

        // TODO: Report this error via ajax so you can keep track
        //       of what pages have JS issues

        return true;
    };
}

// catchGlobalErrors();

function clearLocalStorage(key){
    localStorage.removeItem(key);
}

Number.prototype.r_formatNumber = formatNumber;

function formatNumber(num) {

    num = this.valueOf();
    if(num && num.toString().length>2){

        if(num % 1 != 0){
            return num.toFixed(2)
        } else {
            return num
        }
    } else {
        return num;
    }
}

function checkOppFieldsRequirement($scope,$rootScope,share){
    var checkStr = String($scope.opp.amount);
    var checkStrNGM = String($scope.opp.netGrossMargin)
    $scope.errorsExist = false;
    $scope.opp.isNotOwner = false;

    resetErrorsField($scope)

    if(!$scope.opp.opportunityName){
        $scope.opp.opportunityNameReq = true;
        $scope.errorsExist = true;
        highlightTab($scope,1)
    }

    if(!$scope.opp.closeDateFormatted){
        $scope.opp.closeDateReq = true;
        $scope.errorsExist = true;
        highlightTab($scope,1)
    }

    if(!$scope.isExistingOpp){
        if(!$scope.newOppContact || !$scope.newOppContact.emailId){
            $scope.opp.contactEmailIdReq = true;
            $scope.errorsExist = true;
            highlightTab($scope,1)
        }
    } else {
        if(!$scope.opp.contactEmailId){
            $scope.opp.contactEmailIdReq = true;
            $scope.errorsExist = true;
            highlightTab($scope,1)
        }
    }

    if($scope.opp.searchContent && !validateEmail($scope.opp.searchContent)){
        var string = $scope.opp.searchContent.substring($scope.opp.searchContent.lastIndexOf("(")+1,$scope.opp.searchContent.lastIndexOf(")"));
        if(!validateEmail(string)){
            $scope.opp.contactEmailIdReq = true;
            $scope.errorsExist = true;
            highlightTab($scope,1)
        }
    }

    if(!$scope.opp.amount){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.amountReq = true;
    }
    if(!$scope.opp.stage){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.stageReq = true;
    }
    if($scope.opp.amount && checkStr.match(/[a-z]/i)){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.amountReq = true;
    }

    if($rootScope.isRegionRequired && (!$scope.opp.geoLocation || !$scope.opp.geoLocation.zone || !$scope.opp.geoLocation.town)){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.geoLocationReq = true;
    }

    if($rootScope.isProductRequired && !$scope.opp.productType){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.productTypeReq = true;
    }
    if(!$scope.showOppInsights && $rootScope.dmSelectionRequired && (!$scope.opp.decisionMakers || ($scope.opp.decisionMakers && $scope.opp.decisionMakers.length == 0))){
        $scope.errorsExist = true;
        highlightTab($scope,2)
        $scope.opp.dmRequired = true;
    }
    if(!$scope.showOppInsights && $rootScope.inflSelectionRequired && (!$scope.opp.influencers || ($scope.opp.influencers && $scope.opp.influencers.length == 0))){
        $scope.errorsExist = true;
        highlightTab($scope,2)
        $scope.opp.inflRequired = true;
    }

    if(!$scope.showOppInsights && $rootScope.partnerSelectionRequired && (!$scope.opp.partners || ($scope.opp.partners && $scope.opp.partners.length == 0))){
        $scope.errorsExist = true;
        highlightTab($scope,2)
        $scope.opp.partnerRequired = true;
    }

    if($rootScope.isVerticalRequired && !$scope.opp.vertical){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.verticalReq = true;
    }

    if($scope.opp.netGrossMargin && !isNumber($scope.opp.netGrossMargin) && checkStrNGM.match(/[a-z]/i)){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.netGrossMarginReq = true;
    }
    if($rootScope.netGrossMargin && ($scope.opp.netGrossMargin<0 || !checkRequired($scope.opp.netGrossMargin))){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.netGrossMarginReq = true;
    }

    if(share.companyDetails && share.companyDetails.typeList.length>0 && share.companyDetails.opportunitySettings.typeRequired && !$scope.opp.type){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.typeReq = true;
    }
    if(share.companyDetails && share.companyDetails.solutionList.length>0 && share.companyDetails.opportunitySettings.solutionRequired && !$scope.opp.solution){
        $scope.errorsExist = true;
        highlightTab($scope,1)
        $scope.opp.solutionReq = true;
    }

    if(share.companyDetails && share.companyDetails.businessUnits.length>0 && share.companyDetails.opportunitySettings.businessUnitRequired && !$scope.opp.businessUnit){
        highlightTab($scope,1)
        $scope.errorsExist = true;
        $scope.opp.businessUnitReq = true;
    }
    if(share.companyDetails && share.companyDetails.sourceList.length>0 && share.companyDetails.opportunitySettings.sourceRequired && !$scope.opp.sourceType){
        highlightTab($scope,1)
        $scope.errorsExist = true;
        $scope.opp.sourceTypeReq = true;
    }
    if($scope.opp.netGrossMargin && ($scope.opp.netGrossMargin>100 || $scope.opp.netGrossMargin<0)){
        highlightTab($scope,1)
        $scope.errorsExist = true;
        $scope.opp.closeDateReq = true;
    }
    if($scope.opp.renewThisOpp && !$scope.renewalAmount){
        highlightTab($scope,5)
        $scope.errorsExist = true;
        $scope.opp.renewalAmountReq = true;
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    }

    if(_.includes($scope.opp.stage, 'Won') && $scope.opp.renewed && !$scope.opp.renewed.createdDate && share.companyDetails.opportunitySettings && share.companyDetails.opportunitySettings.renewalStatusRequired && !$scope.opp.renewalStatusSet){
        highlightTab($scope,5)
        $scope.opp.renewalStatusSelectionReq = true;
        $scope.errorsExist = true;
    }

    if($scope.opp.renewThisOpp && $scope.opp.renewed && !$scope.opp.renewed.createdDate && !$scope.renewalCloseDate){
        highlightTab($scope,5)
        $scope.errorsExist = true;
        $scope.opp.renewalCloseDateReq = true;
        $scope.reasonsRequired = true;
        $scope.mailOptions = true;
    }

    if((_.includes($scope.opp.stage, 'Won') || _.includes($scope.opp.stage, 'Lost')) && (!checkRequired($scope.opp.closeReasons) || (checkRequired($scope.opp.closeReasons) && $scope.opp.closeReasons.length == 0 ))){
        $scope.reasonsRequired = true;
        $scope.errorsExist = true;
        $scope.opp.closeReasonsReq = true;
        highlightTab($scope,5)
    }

    if($scope.data && $scope.data.availableOptions && $scope.data.availableOptions.length>0){

        var mandatoryObj = {};
        if($scope.opp.masterData && $scope.opp.masterData.length>0){
            _.each($scope.opp.masterData,function (ma) {
                mandatoryObj[ma.type] = true;
            })
        }

        _.each($scope.data.availableOptions,function (av) {
            if(av.oppLinkMandatory && !mandatoryObj[av.name]){
                av.isRequired = true;
            } else {
                av.isRequired = false;
            }

            if(av.isRequired){
                $scope.reasonsRequired = true;
                $scope.errorsExist = true;
                highlightTab($scope,2)
            }
        });

    }

    return $scope.errorsExist;

}

function highlightTab($scope,tab){
    $scope.oppDetailsNav.forEach(function (el) {

        if(tab == 0){
            el.errorExists = false;
        } else {

            if(tab == 1 && el.name == "Basic"){
                el.errorExists = true;
            }

            if(tab == 2 && el.name == "Customer"){
                el.errorExists = true;
            }

            if(tab == 5 && el.name == "Close Details"){
                el.errorExists = true;
            }
        }

    })
}

function getCurrentFiscalYear(timezone,fyMonth,forNextFys){
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];

    if (!fyMonth) {
        fyMonth = "April"; //Default
    }

    var currentYr = new Date().getFullYear()
    var currentMonth = new Date().getMonth()

    var toDate = null;
    var fromDate = new Date(moment().startOf('month'));
    fromDate.setMonth(monthNames.indexOf(fyMonth))

    if(currentMonth<monthNames.indexOf(fyMonth)){
        fromDate.setFullYear(currentYr-1)
    }

    toDate = moment(fromDate).add(11, 'month');
    toDate = moment(toDate).endOf('month');

    var obj = {
        start:moment(fromDate).tz(timezone).format(),
        end: moment(toDate).tz(timezone).format(),
        quarter:setQuarter(fyMonth,timezone),
        nextFysStartEnd:[]
    }

    if(forNextFys){
        for(var i = 1;i<=forNextFys;i++){
            obj.nextFysStartEnd.push({
                start:moment(obj.start).add(i,"years").format(),
                end:moment(obj.end).add(i,"years").format()
            })
        }
    }

    return obj
}

function validateNetGrossMargin(el,evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var number = el.value.split('.');

    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    if( el.value && (parseFloat(el.value)) > 100){
        return false;
    }
    return true;
}

function validateFloatKeyPress(el, evt) {

    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
        // console.log(number)
        return false;
    } else {
        // console.log("??")
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

function niceBytes(x){
    const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    let l = 0, n = parseInt(x, 10) || 0;
    while(n >= 1024 && ++l)
        n = n/1024;
    return(n.toFixed(n >= 10 || l < 1 ? 0 : 1) + ' ' + units[l]);
}

function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}

function getWatchers(root) {
    root = angular.element(root || document.documentElement);
    var watcherCount = 0;

    function getElemWatchers(element) {
        var isolateWatchers = getWatchersFromScope(element.data().$isolateScope);
        var scopeWatchers = getWatchersFromScope(element.data().$scope);
        var watchers = scopeWatchers.concat(isolateWatchers);
        angular.forEach(element.children(), function (childElement) {
            watchers = watchers.concat(getElemWatchers(angular.element(childElement)));
        });
        return watchers;
    }

    function getWatchersFromScope(scope) {
        if (scope) {
            return scope.$$watchers || [];
        } else {
            return [];
        }
    }

    return getElemWatchers(root);
}

function getAmountInThousands(num,digits,ins) {
    num = parseFloat(num);

    var si = [
        { value: 1E18, symbol: " E" },
        { value: 1E15, symbol: " P" },
        { value: 1E12, symbol: " T" },
        { value: 1E9,  symbol: " B" },
        { value: 1E6,  symbol: " M" },
        { value: 1E3,  symbol: " K" }
    ]

    // ins = true;

    if(ins){
        // digits = 2;
        si = [
            { value: 1E7,  symbol: " Cr" },
            { value: 1E5,  symbol: " L" },
            { value: 1E3,  symbol: " K" }
        ]
    }

    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;

    for (i = 0; i < si.length; i++) {
        if (num >= si[i].value) {
            return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
        }
    }

    if(num){
        return num.toFixed(digits).replace(rx, "$1");
    } else {
        return num;
    }

}

function updateAccountRelationship($scope,$http,data) {

    $http.post("/account/update/relationship",data)
        .success(function (response) {

        });
}

function shadeGenerator(r,g,b,numOfColors,intensity,ifHex){

    var saturationLevel = intensity;

    r = r%256;
    g = g%256;
    b = b%256;

    var colors = [];

    for(var i=0;i<numOfColors;i++) {
        r+=saturationLevel;
        g+=saturationLevel;
        b+=saturationLevel;

        var stringColor = "rgb("+r+","+g+","+b+")";

        if(ifHex){
            colors.push("#" + componentToHex(r) + componentToHex(g) + componentToHex(b))
        } else {
            colors.push(String(stringColor))
        }
    }

    return colors;
}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue){
    if(parameterValue instanceof Array)
        if(parameterValue.length > 0)
            parameterValue = parameterValue.join(",")
        else
            parameterValue = null

    if(parameterValue != undefined && parameterValue != null){
        if(baseUrl.indexOf("?") == -1)
            baseUrl += "?"
        else
            baseUrl += "&"
        baseUrl+= parameterName + "=" + parameterValue
    }

    return baseUrl
}

var interactionIconDetails = function(type,name,rName,title,action,_id,refId,emailId,ignore,interaction,timezone,remaindDays){

    remaindDays = remaindDays || 7;
    var actionType = "fa fa-arrow-right orange-color"

    switch(type){
        case 'google-meeting':
        case 'meeting':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-calendar-check-o green-color",
                iconTitle:"Meeting",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'document-share':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-file-pdf-o",
                iconTitle:"Document",
                title:action == 'sender' ? rName+' shared '+title+' with you' : 'You shared '+title+' with '+rName,
                but:action == 'sender' ? 'View':'View Stats',
                action:action == 'sender' ? 'viewDoc':'viewStats',
                showButton:true,
                actionType:actionType
            };
            break;
        case 'message':
        case 'meeting-comment':
        case 'email':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            var colorRed = false;
            var colorBlue = false;
            var titleN = null;
            var updateOpened = false;
            var updateReplied = false;
            var showButton = true;
            if(interaction.action == 'sender'){
                if(checkRequired(interaction.trackInfo)){
                    if(interaction.trackInfo.trackOpen){
                        updateOpened = true;
                    }
                    if(interaction.trackInfo.trackResponse){
                        updateReplied = true;
                    }
                    if(interaction.trackInfo.gotResponse){
                        showButton = false;
                    }
                }
            }
            if(interaction.action == 'receiver' && !interaction.ignore && interaction.interactionType == 'email' && interaction.source == 'relatas'){

                if(checkRequired(interaction.trackInfo)){
                    if(interaction.trackInfo.trackOpen){
                        // track info enabled
                        if(interaction.trackInfo.isRed){
                            // opened or no action
                            colorBlue = true;
                        }
                        else{
                            // not opened or no action
                            colorRed = true;
                        }
                    }

                    if(interaction.trackInfo.trackResponse){
                        // track response enabled
                        var interactionDate = moment(interaction.interactionDate).tz(timezone);
                        interactionDate.date(interactionDate.date() + remaindDays);
                        var now = moment().tz(timezone);

                        if(now.isAfter(interactionDate) || now.format("DD-MM-YYYY") == interactionDate.format("DD-MM-YYYY")){
                            if(interaction.trackInfo.gotResponse){
                                // got response
                                colorBlue = false;
                                colorRed = false;
                            }
                            else{
                                // not response
                                titleN = "Notification remainder, Subject: "+title;

                                if(!colorBlue){
                                    colorRed = true;
                                    colorBlue = false;
                                }
                            }
                        }
                        else{
                            if(interaction.trackInfo.gotResponse){
                                // got response
                                colorBlue = false;
                                colorRed = false;
                            }
                        }
                    }
                }
            }
            var subject = "Re: "+interaction.title;
            var body = "";
            if(checkRequired(interaction.description)){
                body = "\n\n"+interaction.description
            }

            if(interaction.title && interaction.title.substring(0,2).toLowerCase() == 're'){
                subject = interaction.title
            }
            if(checkRequired(interaction.trackInfo)){
                if(interaction.trackInfo.gotResponse){
                    showButton = false;
                }
            }
            return {
                iconClass:"fa-envelope green-color",
                iconTitle:"Email",
                title:titleN != null? titleN :title,
                but:'Reply',
                action:"viewEmail",
                showButton:showButton,
                colorRed:colorRed,
                colorBlue:colorBlue,
                subject:subject,
                subjectNormal:interaction.title,
                composeMailBox:false,
                emailBodyBox:false,
                compose_email_track_viewed:true,
                compose_email_remaind:true,
                compose_email_doc_tracking:false,
                compose_email_body:"",
                compose_email_subject:"",
                bodyContent:"",
                itemPointer:'cursor:pointer',
                updateOpened:updateOpened,
                emailId:null,
                userId:interaction.userId,
                trackId:interaction.trackId,
                updateReplied:updateReplied,
                actionType:actionType
            };
            break;
        case 'sms':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-reorder green-color",
                iconTitle:"SMS",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'call':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-phone green-color",
                iconTitle:"Call",
                title:action == 'sender' ? 'Received call from '+rName : 'You called '+rName,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'task':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            return {
                iconClass:"fa-check-square-o",
                iconTitle:"Tasks",
                title:action == 'sender' ? 'Task assigned to you by '+rName+'. <br>Task: '+title : 'You assigned a task to '+rName+'. <br>Task: '+title,
                but:'View Task',
                action:'viewTask',
                showButton:true,
                interaction:interaction,
                actionType:actionType
            };
            break;
        case 'twitter':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            return {
                iconClass:"fa-twitter-square twitter-color",
                iconTitle:"Twitter",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'facebook':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-facebook-official fb-color",
                iconTitle:"Facebook",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'linkedin':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-linkedin-square linkedin-color",
                iconTitle:"Linkedin",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'calendar-password':
            if(interaction.action == 'receiver') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            var calendarPasswordApprove = false;
            var calPassIgnoreBut = false;
            if(action == 'sender' && !ignore){
                calendarPasswordApprove = true;
                calPassIgnoreBut = true;
            }

            return {
                iconClass:"fa-calendar-check-o green-color",
                iconTitle:"Calendar Password Request",
                followUpBut:false,
                calendarPasswordApprove:calendarPasswordApprove,
                ignoreBut:calPassIgnoreBut,
                isCalPassReq:true,
                _id:_id,
                reqId:refId,
                name:name,
                emailId:emailId,
                actionType:actionType
            };
            break;
        default :return {image:'',title:'', showButton:false};
    }
};

var interactionIconDetails_new = function(type,name,rName,title,action,_id,refId,emailId,ignore,interaction,timezone,remaindDays){

    remaindDays = remaindDays || 7;
    var actionType = "fa fa-arrow-left orange-color"

    switch(type){
        case 'google-meeting':
        case 'meeting':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-calendar-check-o green-color",
                iconTitle:"Meeting",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'document-share':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-file-pdf-o",
                iconTitle:"Document",
                title:action == 'sender' ? rName+' shared '+title+' with you' : 'You shared '+title+' with '+rName,
                but:action == 'sender' ? 'View':'View Stats',
                action:action == 'sender' ? 'viewDoc':'viewStats',
                showButton:true,
                actionType:actionType
            };
            break;
        case 'message':
        case 'meeting-comment':
        case 'email':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            var colorRed = false;
            var colorBlue = false;
            var titleN = null;
            var updateOpened = false;
            var updateReplied = false;
            var showButton = true;
            if(interaction.action == 'sender'){
                if(checkRequired(interaction.trackInfo)){
                    if(interaction.trackInfo.trackOpen){
                        updateOpened = true;
                    }
                    if(interaction.trackInfo.trackResponse){
                        updateReplied = true;
                    }
                    if(interaction.trackInfo.gotResponse){
                        showButton = false;
                    }
                }
            }
            if(interaction.action == 'sender' && !interaction.ignore && interaction.interactionType == 'email' && interaction.source == 'relatas'){

                if(checkRequired(interaction.trackInfo)){
                    if(interaction.trackInfo.trackOpen){
                        // track info enabled
                        if(interaction.trackInfo.isRed){
                            // opened or no action
                            colorBlue = true;
                        }
                        else{
                            // not opened or no action
                            colorRed = true;
                        }
                    }

                    if(interaction.trackInfo.trackResponse){
                        // track response enabled
                        var interactionDate = moment(interaction.interactionDate).tz(timezone);
                        interactionDate.date(interactionDate.date() + remaindDays);
                        var now = moment().tz(timezone);

                        if(now.isAfter(interactionDate) || now.format("DD-MM-YYYY") == interactionDate.format("DD-MM-YYYY")){
                            if(interaction.trackInfo.gotResponse){
                                // got response
                                colorBlue = false;
                                colorRed = false;
                            }
                            else{
                                // not response
                                titleN = "Notification remainder, Subject: "+title;

                                if(!colorBlue){
                                    colorRed = true;
                                    colorBlue = false;
                                }
                            }
                        }
                        else{
                            if(interaction.trackInfo.gotResponse){
                                // got response
                                colorBlue = false;
                                colorRed = false;
                            }
                        }
                    }
                }
            }
            var subject = "Re: "+interaction.title;
            var body = "";
            if(checkRequired(interaction.description)){
                body = "\n\n"+interaction.description
            }

            if(interaction.title && interaction.title.substring(0,2).toLowerCase() == 're'){
                subject = interaction.title
            }
            if(checkRequired(interaction.trackInfo)){
                if(interaction.trackInfo.gotResponse){
                    showButton = false;
                }
            }
            return {
                iconClass:"fa-envelope green-color",
                iconTitle:"Email",
                title:titleN != null? titleN :title,
                but:'Reply',
                action:"viewEmail",
                showButton:showButton,
                colorRed:colorRed,
                colorBlue:colorBlue,
                subject:subject,
                subjectNormal:interaction.title,
                composeMailBox:false,
                emailBodyBox:false,
                compose_email_track_viewed:true,
                compose_email_remaind:true,
                compose_email_doc_tracking:false,
                compose_email_body:"",
                compose_email_subject:"",
                bodyContent:"",
                itemPointer:'cursor:pointer',
                updateOpened:updateOpened,
                emailId:null,
                userId:interaction.userId,
                trackId:interaction.trackId,
                updateReplied:updateReplied,
                actionType:actionType
            };
            break;
        case 'sms':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-reorder green-color",
                iconTitle:"SMS",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'call':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-phone green-color",
                iconTitle:"Call",
                title:action == 'sender' ? 'Received call from '+rName : 'You called '+rName,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'task':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            return {
                iconClass:"fa-check-square-o",
                iconTitle:"Tasks",
                title:action == 'sender' ? 'Task assigned to you by '+rName+'. <br>Task: '+title : 'You assigned a task to '+rName+'. <br>Task: '+title,
                but:'View Task',
                action:'viewTask',
                showButton:true,
                interaction:interaction,
                actionType:actionType
            };
            break;
        case 'twitter':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }

            return {
                iconClass:"fa-twitter-square twitter-color",
                iconTitle:"Twitter",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'facebook':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-facebook-official fb-color",
                iconTitle:"Facebook",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'linkedin':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            return {
                iconClass:"fa-linkedin-square linkedin-color",
                iconTitle:"Linkedin",
                title:title,
                showButton:false,
                actionType:actionType
            };
            break;
        case 'calendar-password':
            if(interaction.action == 'sender') {
                actionType = "fa fa-arrow-right orange-color"
            } else {
                actionType = "fa fa-arrow-left orange-off-color"
            }
            var calendarPasswordApprove = false;
            var calPassIgnoreBut = false;
            if(action == 'sender' && !ignore){
                calendarPasswordApprove = true;
                calPassIgnoreBut = true;
            }

            return {
                iconClass:"fa-calendar-check-o green-color",
                iconTitle:"Calendar Password Request",
                followUpBut:false,
                calendarPasswordApprove:calendarPasswordApprove,
                ignoreBut:calPassIgnoreBut,
                isCalPassReq:true,
                _id:_id,
                reqId:refId,
                name:name,
                emailId:emailId,
                actionType:actionType
            };
            break;
        default :return {image:'',title:'', showButton:false};
    }
};

function escapeHtml(unsafe) {
    return unsafe
    // .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
    // .replace(/"/g, "&quot;")
    // .replace(/'/g, "&#039;");
}

function oppEmptyObj(){
    return {
        "opportunityName": "",
        "closeDate": "",
        "contactEmailId": "",
        "relatasStage": "",
        "stageName": "",
        "stage": "",
        "geoLocation": "",
        "amount": "",
        "netGrossMargin": "",
        "type": "",
        "vertical": "",
        "closeReasons": "",
        "currency": "",
        "sourceType": "",
        "solution": "",
        "accounts": "",
        "businessUnit": ""
    }
}

function buildInteractionObjectForTimeline(interaction,userId,firstName,rName,timezone,index){
    var interactionIcon = interactionIconDetails(interaction.interactionType,
        firstName,
        rName,
        interaction.title,
        interaction.action,
        interaction._id,
        interaction.refId,
        interaction.emailId,
        interaction.ignore,
        interaction,
        timezone,
        null);

    var iDate = moment(interaction.interactionDate).tz(timezone);
    var now = moment().tz(timezone);
    var diff = now.diff(iDate);
    diff = moment.duration(diff).asMinutes();

    var colorClass = "";
    var  emailRead = false;
    var viewedOn,isEmailRead;

    if(interaction.action == 'receiver'
        && interaction.interactionType =='email'
        && interaction.trackInfo
        && interaction.trackInfo.lastOpenedOn
        && interaction.trackInfo.lastOpenedOn !== null){
        isEmailRead = true;
        viewedOn = moment(interaction.trackInfo.lastOpenedOn).format("DD MMM YYYY, h:mm a");
    } else {
        isEmailRead = false;
        viewedOn = '';
    }

    if(interactionIcon.colorRed){
        colorClass = 'color:#F86A52';
        emailRead = false;
    } else if(interactionIcon.colorBlue){
        colorClass = 'color:#2d3e48';
        emailRead = true;
    }

    var cursor = "no-action"

    if(interaction.interactionType == "email" || interaction.interactionType == "meeting"){
        cursor = "cursor"
    }

    var duration = moment.duration(diff,"minutes").humanize()+' ago'
    if(new Date(interaction.interactionDate)>new Date()){
        duration = "In "+moment.duration(diff,"minutes").humanize()
    }

    var interactionInitClass = "incoming";

    if(interaction.action == "receiver"){
        interactionInitClass = "outgoing";
    }

    return {
        emailId:interaction.emailId,
        interactionDate:interaction.interactionDate,
        cursor:cursor,
        duration:duration,
        dateText:iDate.format("DD MMM YYYY"),
        interactionIcon:interactionIcon.image,
        iconClass:interactionIcon.iconClass,
        iconTitle:interactionIcon.iconTitle,
        actionType:interactionIcon.actionType,
        subject:interactionIcon.title,
        title:interactionIcon.title?escapeHtml(interactionIcon.title.replace(/<br\s*[\/]?>/gi, "\n")):interactionIcon.title,
        but:interactionIcon.but,
        action:interactionIcon.action,
        refId:interaction.refId,
        showButton:interactionIcon.showButton ? 'display:block' : 'display:none',
        className: index==0 ? 'document-timeline' :'',
        viewitemId:interaction._id,
        colorClass:colorClass,
        emailRead:isEmailRead,
        interactionInitClass:interactionInitClass,
        viewedOn:viewedOn,
        emailOpens:interaction.trackInfo && interaction.trackInfo.emailOpens ? interaction.trackInfo.emailOpens : 0,
        callDuration:getDurationFormat(interaction),
        dataObj:{
            _id:interaction._id,
            body:interaction.description,
            bindHtmlKey:'content_'+interaction._id,
            interaction:interaction,
            subject:interactionIcon.subject,
            subjectNormal:interactionIcon.subjectNormal,
            composeMailBox:interactionIcon.composeMailBox,
            emailBodyBox:interactionIcon.emailBodyBox,
            compose_email_track_viewed:interactionIcon.compose_email_track_viewed,
            compose_email_remaind:interactionIcon.compose_email_remaind,
            compose_email_doc_tracking:interactionIcon.compose_email_doc_tracking,
            compose_email_body:interactionIcon.compose_email_body,
            compose_email_subject:interactionIcon.subject,
            bodyContent:interactionIcon.bodyContent,
            itemPointer:interactionIcon.itemPointer,
            updateOpened:interactionIcon.updateOpened,
            updateReplied:interactionIcon.updateReplied,
            emailId:interactionIcon.emailId,
            userId:userId?userId:null,
            trackId:interactionIcon.trackId,
            updateMailRead:true
        }
    }
}

function buildInteractionObjectForTimeline_new(interaction,userId,firstName,rName,timezone,index){
    var interactionIcon = interactionIconDetails_new(interaction.interactionType,
        firstName,
        rName,
        interaction.title,
        interaction.action,
        interaction._id,
        interaction.refId,
        interaction.emailId,
        interaction.ignore,
        interaction,
        timezone,
        null);

    var iDate = moment(interaction.interactionDate).tz(timezone);
    var now = moment().tz(timezone);
    var diff = now.diff(iDate);
    diff = moment.duration(diff).asMinutes();

    var colorClass = "";
    var  emailRead = false;
    var viewedOn,isEmailRead;

    if(interaction.action == 'receiver'
        && interaction.interactionType =='email'
        && interaction.trackInfo
        && interaction.trackInfo.lastOpenedOn
        && interaction.trackInfo.lastOpenedOn !== null){
        isEmailRead = true;
        viewedOn = moment(interaction.trackInfo.lastOpenedOn).format("DD MMM YYYY, h:mm a");
    } else {
        isEmailRead = false;
        viewedOn = '';
    }

    if(interactionIcon.colorRed){
        colorClass = 'color:#F86A52';
        emailRead = false;
    } else if(interactionIcon.colorBlue){
        colorClass = 'color:#2d3e48';
        emailRead = true;
    }

    var cursor = "no-action"

    if(interaction.interactionType == "email" || interaction.interactionType == "meeting"){
        cursor = "cursor"
    }

    var duration = moment.duration(diff,"minutes").humanize()+' ago'
    if(new Date(interaction.interactionDate)>new Date()){
        duration = "In "+moment.duration(diff,"minutes").humanize()
    }

    var interactionInitClass = "outgoing";

    if(interaction.action == "receiver"){
        interactionInitClass = "incoming";
    }

    return {
        emailId:interaction.emailId,
        interactionDate:interaction.interactionDate,
        cursor:cursor,
        duration:duration,
        dateText:iDate.format("DD MMM YYYY"),
        interactionIcon:interactionIcon.image,
        iconClass:interactionIcon.iconClass,
        iconTitle:interactionIcon.iconTitle,
        actionType:interactionIcon.actionType,
        subject:interactionIcon.title,
        title:interactionIcon.title?escapeHtml(interactionIcon.title.replace(/<br\s*[\/]?>/gi, "\n")):interactionIcon.title,
        but:interactionIcon.but,
        action:interactionIcon.action,
        refId:interaction.refId,
        showButton:interactionIcon.showButton ? 'display:block' : 'display:none',
        className: index==0 ? 'document-timeline' :'',
        viewitemId:interaction._id,
        colorClass:colorClass,
        emailRead:isEmailRead,
        interactionInitClass:interactionInitClass,
        viewedOn:viewedOn,
        emailOpens:interaction.trackInfo && interaction.trackInfo.emailOpens ? interaction.trackInfo.emailOpens : 0,
        callDuration:getDurationFormat(interaction),
        dataObj:{
            _id:interaction._id,
            body:interaction.description,
            bindHtmlKey:'content_'+interaction._id,
            interaction:interaction,
            subject:interactionIcon.subject,
            subjectNormal:interactionIcon.subjectNormal,
            composeMailBox:interactionIcon.composeMailBox,
            emailBodyBox:interactionIcon.emailBodyBox,
            compose_email_track_viewed:interactionIcon.compose_email_track_viewed,
            compose_email_remaind:interactionIcon.compose_email_remaind,
            compose_email_doc_tracking:interactionIcon.compose_email_doc_tracking,
            compose_email_body:interactionIcon.compose_email_body,
            compose_email_subject:interactionIcon.subject,
            bodyContent:interactionIcon.bodyContent,
            itemPointer:interactionIcon.itemPointer,
            updateOpened:interactionIcon.updateOpened,
            updateReplied:interactionIcon.updateReplied,
            emailId:interactionIcon.emailId,
            userId:userId?userId:null,
            trackId:interactionIcon.trackId,
            updateMailRead:true
        }
    }
}

var getInteractionTypeObj = function(obj,total){
    switch (obj._id){
        case 'google-meeting':
        case 'meeting':return {
            priority:0,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'call':return {
            priority:1,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'sms':return {
            priority:2,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'email':return {
            priority:3,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'facebook':return {
            priority:4,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'twitter':return {
            priority:5,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        case 'linkedin':return {
            priority:6,
            value:'height:'+calculatePercentage(obj.count,total)+'%',
            type:obj._id,
            count:obj.count,
            title:obj.count+' interactions'
        };
            break;

        default : return null;
    }
};

function getDurationFormat(interaction){
    var duration = "";
    if(interaction && interaction.duration != 0 && interaction.duration){
        duration = secondsToHms(interaction.duration)
    }

    if(interaction.duration == 0){
        duration = "Missed call"
    }

    return duration
}

function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hr " : " hrs ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " m " : " m ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " s" : " s") : "";
    return hDisplay + mDisplay + sDisplay;
}

var calculatePercentage = function(count,total){
    return Math.round((count*100)/total);
};

var calculatePercentageChange = function(current,prev){
    var num = String(Math.round((current-prev)/prev*100.0));
    num = num.replace(/\.00$/,'');
    return parseFloat(num);
};

var calculatePercentageChange_insights = function(current,prev){
    var num = String(Math.round((current-prev)/current*100.0));
    num = num.replace(/\.00$/,'');
    // return current>prev?-parseFloat(num):parseFloat(num);
    return parseFloat(num);
};

function clearInputFields() {
    $(".clearfield input").val("");
    $(".clearfield textarea").val("");
}

function imageExists(image_url){

    var http = new XMLHttpRequest();
    http.open('HEAD', image_url, false);

    try {
        http.send();
        return http.status != 404;
    } catch(err){
        return false;
    }

}

function loadDataOnlyForInternalTeam() {

    var loadAll = false;
    var domain = window.location.hostname;

    if((/relatas.relatas/.test(domain))){
        loadAll = true;
    } else if(/example/.test(domain)){
        loadAll = true;
    } else if(/sugar/.test(domain)){
        loadAll = true;
    } else if(/localhost/.test(domain)){
        loadAll = true;
    }

    return loadAll;
}

$(document).ready(function() {
    $('.body').hover( function() {
        $(this).siblings('.tooltip-xs').toggle();
    });
});

$(document).ready(function() {
        //Stop closing of dropdown in mobile when clicked inside

    $("body").on('click', '#mLogout', function (e) {
        clearLocalStorage("refreshData")
        clearLocalStorage("teamMembers")
        clearLocalStorage("teamMembersDictionary")
        clearLocalStorage("relatasLocalDb")
    });

    $("body").on('click', '.dropdown-menu', function (e) {
        $(this).hasClass('keep-open') && e.stopPropagation();
    });

    $("body").on('click', '#menu-toggle', function (e) {
        e.preventDefault();
        $(".overlay").show();
    });

    $(window).resize(function () {
        var scrollWidth = $(window).width();
        if (scrollWidth >= 768) {
            $(".overlay").hide();
        }
    });

    //Set sidebar height to main col's height

    $(window).scroll(function() {
        var windowHeight = $(".set-height").height();
        $(".sidebar-height").css("min-height", windowHeight);
    });

    $('#open-mapInteractions').popover({
        html : true,
        content: function() {
            return $('#popover_content_wrapper').html();
        }
    });

    $("#ignore-response").click(function(){
        $(".response-hide").hide(100);
    });

    $("body").on("click",".email-reply",function(){
        $("#email-response").toggle(100);
        var position = $(this).offset();
        $("#email-response").css({top:position.top})
    });

    $("body").on("click",".open-comment",function(){
        $("#comment-section").toggle(100);
        var position = $(this).offset();
        $("#comment-section").css({top:position.top})
    });

    $("body").on("click","#close-doc-analytics",function(){
        $("#analytics-box").toggle(100);
        var position = $(this).offset();
        $("#analytics-box").css({top:position.top})
    });

    $(".clickable-block").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    window.setTimeout(function() {
        $(".alert-message").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 5000);

    $("#toggle-left-menu").click(function(){
        $(".toggle-sidebar").toggle();
    });

    $("body").on("click",".fa-bars",function(){
        $(".contact-list-hidden-xs").toggle();
        $(".contact-list-hidden-xs #sidebar-wrapper").toggle();
    });

    var isMobile = window.matchMedia("only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) and (-webkit-min-device-pixel-ratio: 2)");

        if (isMobile.matches) {
            $("#menu-toggle").click(function(){
                $("#ipad-open").toggleClass("open");
            });
        }

    if (matchMedia('(min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) and (-webkit-min-device-pixel-ratio: 2').matches) {
        $("#menu-toggle").click(function(){
            $("#ipad-open").toggleClass("open");
        });

        $("#menu-toggle").click(function(){
            $(".navbar").addClass("texting");
        });
    }

    var colHeight = $(".this-height").height();
    $(".col-height").css("height", colHeight);

    $("body").on("click",".open-add-task",function(){
        $(".add-task-landing").toggle(100);
    });

    $("body").on("click",".show-networks",function(){
        $(".toggle-networks").toggle();
    });

    $("body").on("click",".fa-filter",function(){
        $(".filter-options").slideToggle(200);
        return false;
    });

    $("body").on("click","#emailFormOpen",function(){
        $("#emailFormShow").slideToggle(200);
        $("#compose-email-box-top").hide();
        return false;
    });

    $("body").on("click",".long-agenda",function(){
        $(".show-more-long-agenda").slideToggle(5);
    });

    $("body").on("click",".open-leftbar",function(){
        $("#sidebar-wrapper").toggle();
    });

    $("body").on("click", ".fa-plus-circle", function(){
        $(".display-hashtags form").slideToggle(200);
    });

    $("body").on("click",".move-enterprise",function(){
        $(".move-to-enterprise").slideToggle(200);
    });

    $("body").on("click",".close-help",function(){
        $("#contact").hide();
    });

    $("body").on("click",".shw-heirarchy",function(){
        $(".toggle-heirarchy").slideToggle(200);
    });

    $("body").on("click",".email-inline",function(){
        $(".compose-email-inline").slideToggle(200);
    });

    $("body").on("click", ".click-test", function(){
        $(".table-email").slideToggle(50);
        //$(".table-email-container").nextAll(".table-email").slideToggle(50);
    });

    $("body").on("click", "#youtubeVid", function(){
            var src = 'https://www.youtube.com/watch?v=Ax7oKZ6DYQY&feature=youtu.be;autoplay=1';
            $('#myModal').modal('show');
            $('#myModal iframe').attr('src', src);
    });

    $("body").on("click", "#myModal button", function(){
        $('#myModal iframe').removeAttr('src');
    });

    $("body").on("contextmenu", "#my-video", function(e){
       return false;
    });

    $("body").on("click", ".initiate-intro", function(){
        $(this).parents(".connect-sprite").nextAll(".toggle-initiate-intro").slideToggle(50);
    });

    $("body").on("click", ".forward-intro", function(){
        $(this).parents(".connect-sprite").nextAll(".toggle-forward-intro").slideToggle(50);
    });

    $("body").on("click", ".invite-contact", function(){
       $(".toggle-invite-contact").slideToggle(50);
    });

    function popUpCloseDefault(className, btnName) {

        $(document).on('mouseup', function (e){
            var container = $(className);
            if (!container.is(e.target) && container.has(e.target).length === 0 && !$(e.target).is(btnName))
            {
                container.hide(100);
            }
        });
    }

    // popUpCloseDefault(".filter-options",".fa-filter");
    popUpCloseDefault(".toggle-networks",".show-networks");
    //popUpCloseDefault(".toggle-initiate-intro",".initiate-intro");
    popUpCloseDefault(".toggle-forward-intro",".forward-intro");
    popUpCloseDefault("#contact",".close-help");

    // Onboarding wizard
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
        /*if ($target.parent().hasClass('disabled')) {
         return false;
         }*/
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }

    $(".hasEvent").parents(':eq(2)').css({"background-color": "#cb3c19"});
    $(".non-working").closest('td').css({"background-color": "#f5f3f0"});

    //Enterprise change value of btns on dropdown select

    $("#dropdown-landing li a").click(function(){
        $("#dropdown-value-3:first-child").html($(this).text()+' <span class="caret"></span>');
    });

    $("#dropdown-selected-1 li a").click(function(){
        $("#dropdown-value-1:first-child").html($(this).text()+' <span class="caret"></span>');
    });

    $("#dropdown-selected-2 li a").click(function(){
        $("#dropdown-value-2:first-child").html($(this).text()+' <span class="caret"></span>');
    });

    $("#dropdown-selected-5 li a").click(function(){
        $("#dropdown-value-5:first-child").html($(this).text()+' <span class="caret"></span>');
    });
});


/*!
 * Bootstrap-select v1.7.5 (http://silviomoreto.github.io/bootstrap-select)
 *
 * Copyright 2013-2015 bootstrap-select
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-select/blob/master/LICENSE)
 */
!function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(this,function(a){!function(a){"use strict";function b(b){var c=[{re:/[\xC0-\xC6]/g,ch:"A"},{re:/[\xE0-\xE6]/g,ch:"a"},{re:/[\xC8-\xCB]/g,ch:"E"},{re:/[\xE8-\xEB]/g,ch:"e"},{re:/[\xCC-\xCF]/g,ch:"I"},{re:/[\xEC-\xEF]/g,ch:"i"},{re:/[\xD2-\xD6]/g,ch:"O"},{re:/[\xF2-\xF6]/g,ch:"o"},{re:/[\xD9-\xDC]/g,ch:"U"},{re:/[\xF9-\xFC]/g,ch:"u"},{re:/[\xC7-\xE7]/g,ch:"c"},{re:/[\xD1]/g,ch:"N"},{re:/[\xF1]/g,ch:"n"}];return a.each(c,function(){b=b.replace(this.re,this.ch)}),b}function c(a){var b={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},c="(?:"+Object.keys(b).join("|")+")",d=new RegExp(c),e=new RegExp(c,"g"),f=null==a?"":""+a;return d.test(f)?f.replace(e,function(a){return b[a]}):f}function d(b,c){var d=arguments,f=b,g=c;[].shift.apply(d);var h,i=this.each(function(){var b=a(this);if(b.is("select")){var c=b.data("selectpicker"),i="object"==typeof f&&f;if(c){if(i)for(var j in i)i.hasOwnProperty(j)&&(c.options[j]=i[j])}else{var k=a.extend({},e.DEFAULTS,a.fn.selectpicker.defaults||{},b.data(),i);k.template=a.extend({},e.DEFAULTS.template,a.fn.selectpicker.defaults?a.fn.selectpicker.defaults.template:{},b.data().template,i.template),b.data("selectpicker",c=new e(this,k,g))}"string"==typeof f&&(h=c[f]instanceof Function?c[f].apply(c,d):c.options[f])}});return"undefined"!=typeof h?h:i}String.prototype.includes||!function(){var a={}.toString,b=function(){try{var a={},b=Object.defineProperty,c=b(a,a,a)&&b}catch(d){}return c}(),c="".indexOf,d=function(b){if(null==this)throw new TypeError;var d=String(this);if(b&&"[object RegExp]"==a.call(b))throw new TypeError;var e=d.length,f=String(b),g=f.length,h=arguments.length>1?arguments[1]:void 0,i=h?Number(h):0;i!=i&&(i=0);var j=Math.min(Math.max(i,0),e);return g+j>e?!1:-1!=c.call(d,f,i)};b?b(String.prototype,"includes",{value:d,configurable:!0,writable:!0}):String.prototype.includes=d}(),String.prototype.startsWith||!function(){var a=function(){try{var a={},b=Object.defineProperty,c=b(a,a,a)&&b}catch(d){}return c}(),b={}.toString,c=function(a){if(null==this)throw new TypeError;var c=String(this);if(a&&"[object RegExp]"==b.call(a))throw new TypeError;var d=c.length,e=String(a),f=e.length,g=arguments.length>1?arguments[1]:void 0,h=g?Number(g):0;h!=h&&(h=0);var i=Math.min(Math.max(h,0),d);if(f+i>d)return!1;for(var j=-1;++j<f;)if(c.charCodeAt(i+j)!=e.charCodeAt(j))return!1;return!0};a?a(String.prototype,"startsWith",{value:c,configurable:!0,writable:!0}):String.prototype.startsWith=c}(),Object.keys||(Object.keys=function(a,b,c){c=[];for(b in a)c.hasOwnProperty.call(a,b)&&c.push(b);return c}),a.fn.triggerNative=function(a){var b,c=this[0];c.dispatchEvent?("function"==typeof Event?b=new Event(a,{bubbles:!0}):(b=document.createEvent("Event"),b.initEvent(a,!0,!1)),c.dispatchEvent(b)):(c.fireEvent&&(b=document.createEventObject(),b.eventType=a,c.fireEvent("on"+a,b)),this.trigger(a))},a.expr[":"].icontains=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.text()).toUpperCase();return f.includes(d[3].toUpperCase())},a.expr[":"].ibegins=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.text()).toUpperCase();return f.startsWith(d[3].toUpperCase())},a.expr[":"].aicontains=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.data("normalizedText")||e.text()).toUpperCase();return f.includes(d[3].toUpperCase())},a.expr[":"].aibegins=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.data("normalizedText")||e.text()).toUpperCase();return f.startsWith(d[3].toUpperCase())};var e=function(b,c,d){d&&(d.stopPropagation(),d.preventDefault()),this.$element=a(b),this.$newElement=null,this.$button=null,this.$menu=null,this.$lis=null,this.options=c,null===this.options.title&&(this.options.title=this.$element.attr("title")),this.val=e.prototype.val,this.render=e.prototype.render,this.refresh=e.prototype.refresh,this.setStyle=e.prototype.setStyle,this.selectAll=e.prototype.selectAll,this.deselectAll=e.prototype.deselectAll,this.destroy=e.prototype.remove,this.remove=e.prototype.remove,this.show=e.prototype.show,this.hide=e.prototype.hide,this.init()};e.VERSION="1.7.5",e.DEFAULTS={noneSelectedText:"Nothing selected",noneResultsText:"No results matched {0}",countSelectedText:function(a,b){return 1==a?"{0} item selected":"{0} items selected"},maxOptionsText:function(a,b){return[1==a?"Limit reached ({n} item max)":"Limit reached ({n} items max)",1==b?"Group limit reached ({n} item max)":"Group limit reached ({n} items max)"]},selectAllText:"Select All",deselectAllText:"Deselect All",doneButton:!1,doneButtonText:"Close",multipleSeparator:", ",styleBase:"btn",style:"btn-default",size:"auto",title:null,selectedTextFormat:"values",width:!1,container:!1,hideDisabled:!1,showSubtext:!1,showIcon:!0,showContent:!0,dropupAuto:!0,header:!1,liveSearch:!1,liveSearchPlaceholder:null,liveSearchNormalize:!1,liveSearchStyle:"contains",actionsBox:!1,iconBase:"glyphicon",tickIcon:"glyphicon-ok",template:{caret:'<span class="caret"></span>'},maxOptions:!1,mobile:!1,selectOnTab:!1,dropdownAlignRight:!1},e.prototype={constructor:e,init:function(){var b=this,c=this.$element.attr("id");this.$element.addClass("bs-select-hidden"),this.liObj={},this.multiple=this.$element.prop("multiple"),this.autofocus=this.$element.prop("autofocus"),this.$newElement=this.createView(),this.$element.after(this.$newElement),this.$button=this.$newElement.children("button"),this.$menu=this.$newElement.children(".dropdown-menu"),this.$menuInner=this.$menu.children(".inner"),this.$searchbox=this.$menu.find("input"),this.options.dropdownAlignRight&&this.$menu.addClass("dropdown-menu-right"),"undefined"!=typeof c&&(this.$button.attr("data-id",c),a('label[for="'+c+'"]').click(function(a){a.preventDefault(),b.$button.focus()})),this.checkDisabled(),this.clickListener(),this.options.liveSearch&&this.liveSearchListener(),this.render(),this.setStyle(),this.setWidth(),this.options.container&&this.selectPosition(),this.$menu.data("this",this),this.$newElement.data("this",this),this.options.mobile&&this.mobile(),this.$newElement.on({"hide.bs.dropdown":function(a){b.$element.trigger("hide.bs.select",a)},"hidden.bs.dropdown":function(a){b.$element.trigger("hidden.bs.select",a)},"show.bs.dropdown":function(a){b.$element.trigger("show.bs.select",a)},"shown.bs.dropdown":function(a){b.$element.trigger("shown.bs.select",a)}}),setTimeout(function(){b.$element.trigger("loaded.bs.select")})},createDropdown:function(){var b=this.multiple?" show-tick":"",d=this.$element.parent().hasClass("input-group")?" input-group-btn":"",e=this.autofocus?" autofocus":"",f=this.options.header?'<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>'+this.options.header+"</div>":"",g=this.options.liveSearch?'<div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off"'+(null===this.options.liveSearchPlaceholder?"":' placeholder="'+c(this.options.liveSearchPlaceholder)+'"')+"></div>":"",h=this.multiple&&this.options.actionsBox?'<div class="bs-actionsbox"><div class="btn-group btn-group-sm btn-block"><button type="button" class="actions-btn bs-select-all btn btn-default">'+this.options.selectAllText+'</button><button type="button" class="actions-btn bs-deselect-all btn btn-default">'+this.options.deselectAllText+"</button></div></div>":"",i=this.multiple&&this.options.doneButton?'<div class="bs-donebutton"><div class="btn-group btn-block"><button type="button" class="btn btn-sm btn-default">'+this.options.doneButtonText+"</button></div></div>":"",j='<div class="btn-group bootstrap-select'+b+d+'"><button type="button" class="'+this.options.styleBase+' dropdown-toggle" data-toggle="dropdown"'+e+'><span class="filter-option pull-left"></span>&nbsp;<span class="bs-caret">'+this.options.template.caret+'</span></button><div class="dropdown-menu open">'+f+g+h+'<ul class="dropdown-menu inner" role="menu"></ul>'+i+"</div></div>";return a(j)},createView:function(){var a=this.createDropdown(),b=this.createLi();return a.find("ul")[0].innerHTML=b,a},reloadLi:function(){this.destroyLi();var a=this.createLi();this.$menuInner[0].innerHTML=a},destroyLi:function(){this.$menu.find("li").remove()},createLi:function(){var d=this,e=[],f=0,g=document.createElement("option"),h=-1,i=function(a,b,c,d){return"<li"+("undefined"!=typeof c&""!==c?' class="'+c+'"':"")+("undefined"!=typeof b&null!==b?' data-original-index="'+b+'"':"")+("undefined"!=typeof d&null!==d?'data-optgroup="'+d+'"':"")+">"+a+"</li>"},j=function(a,e,f,g){return'<a tabindex="0"'+("undefined"!=typeof e?' class="'+e+'"':"")+("undefined"!=typeof f?' style="'+f+'"':"")+(d.options.liveSearchNormalize?' data-normalized-text="'+b(c(a))+'"':"")+("undefined"!=typeof g||null!==g?' data-tokens="'+g+'"':"")+">"+a+'<span class="'+d.options.iconBase+" "+d.options.tickIcon+' check-mark"></span></a>'};if(this.options.title&&!this.multiple&&(h--,!this.$element.find(".bs-title-option").length)){var k=this.$element[0];g.className="bs-title-option",g.appendChild(document.createTextNode(this.options.title)),g.value="",k.insertBefore(g,k.firstChild),void 0===a(k.options[k.selectedIndex]).attr("selected")&&(g.selected=!0)}return this.$element.find("option").each(function(b){var c=a(this);if(h++,!c.hasClass("bs-title-option")){var g=this.className||"",k=this.style.cssText,l=c.data("content")?c.data("content"):c.html(),m=c.data("tokens")?c.data("tokens"):null,n="undefined"!=typeof c.data("subtext")?'<small class="text-muted">'+c.data("subtext")+"</small>":"",o="undefined"!=typeof c.data("icon")?'<span class="'+d.options.iconBase+" "+c.data("icon")+'"></span> ':"",p=this.disabled||"OPTGROUP"===this.parentNode.tagName&&this.parentNode.disabled;if(""!==o&&p&&(o="<span>"+o+"</span>"),d.options.hideDisabled&&p)return void h--;if(c.data("content")||(l=o+'<span class="text">'+l+n+"</span>"),"OPTGROUP"===this.parentNode.tagName&&c.data("divider")!==!0){var q=" "+this.parentNode.className||"";if(0===c.index()){f+=1;var r=this.parentNode.label,s="undefined"!=typeof c.parent().data("subtext")?'<small class="text-muted">'+c.parent().data("subtext")+"</small>":"",t=c.parent().data("icon")?'<span class="'+d.options.iconBase+" "+c.parent().data("icon")+'"></span> ':"";r=t+'<span class="text">'+r+s+"</span>",0!==b&&e.length>0&&(h++,e.push(i("",null,"divider",f+"div"))),h++,e.push(i(r,null,"dropdown-header"+q,f))}e.push(i(j(l,"opt "+g+q,k,m),b,"",f))}else c.data("divider")===!0?e.push(i("",b,"divider")):c.data("hidden")===!0?e.push(i(j(l,g,k,m),b,"hidden is-hidden")):(this.previousElementSibling&&"OPTGROUP"===this.previousElementSibling.tagName&&(h++,e.push(i("",null,"divider",f+"div"))),e.push(i(j(l,g,k,m),b)));d.liObj[b]=h}}),this.multiple||0!==this.$element.find("option:selected").length||this.options.title||this.$element.find("option").eq(0).prop("selected",!0).attr("selected","selected"),e.join("")},findLis:function(){return null==this.$lis&&(this.$lis=this.$menu.find("li")),this.$lis},render:function(b){var c,d=this;b!==!1&&this.$element.find("option").each(function(a){var b=d.findLis().eq(d.liObj[a]);d.setDisabled(a,this.disabled||"OPTGROUP"===this.parentNode.tagName&&this.parentNode.disabled,b),d.setSelected(a,this.selected,b)}),this.tabIndex();var e=this.$element.find("option").map(function(){if(this.selected){if(d.options.hideDisabled&&(this.disabled||"OPTGROUP"===this.parentNode.tagName&&this.parentNode.disabled))return;var b,c=a(this),e=c.data("icon")&&d.options.showIcon?'<i class="'+d.options.iconBase+" "+c.data("icon")+'"></i> ':"";return b=d.options.showSubtext&&c.data("subtext")&&!d.multiple?' <small class="text-muted">'+c.data("subtext")+"</small>":"","undefined"!=typeof c.attr("title")?c.attr("title"):c.data("content")&&d.options.showContent?c.data("content"):e+c.html()+b}}).toArray(),f=this.multiple?e.join(this.options.multipleSeparator):e[0];if(this.multiple&&this.options.selectedTextFormat.indexOf("count")>-1){var g=this.options.selectedTextFormat.split(">");if(g.length>1&&e.length>g[1]||1==g.length&&e.length>=2){c=this.options.hideDisabled?", [disabled]":"";var h=this.$element.find("option").not('[data-divider="true"], [data-hidden="true"]'+c).length,i="function"==typeof this.options.countSelectedText?this.options.countSelectedText(e.length,h):this.options.countSelectedText;f=i.replace("{0}",e.length.toString()).replace("{1}",h.toString())}}void 0==this.options.title&&(this.options.title=this.$element.attr("title")),"static"==this.options.selectedTextFormat&&(f=this.options.title),f||(f="undefined"!=typeof this.options.title?this.options.title:this.options.noneSelectedText),this.$button.attr("title",a.trim(f.replace(/<[^>]*>?/g,""))),this.$button.children(".filter-option").html(f),this.$element.trigger("rendered.bs.select")},setStyle:function(a,b){this.$element.attr("class")&&this.$newElement.addClass(this.$element.attr("class").replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi,""));var c=a?a:this.options.style;"add"==b?this.$button.addClass(c):"remove"==b?this.$button.removeClass(c):(this.$button.removeClass(this.options.style),this.$button.addClass(c))},liHeight:function(b){if(b||this.options.size!==!1&&!this.sizeInfo){var c=document.createElement("div"),d=document.createElement("div"),e=document.createElement("ul"),f=document.createElement("li"),g=document.createElement("li"),h=document.createElement("a"),i=document.createElement("span"),j=this.options.header?this.$menu.find(".popover-title")[0].cloneNode(!0):null,k=this.options.liveSearch?document.createElement("div"):null,l=this.options.actionsBox&&this.multiple?this.$menu.find(".bs-actionsbox")[0].cloneNode(!0):null,m=this.options.doneButton&&this.multiple?this.$menu.find(".bs-donebutton")[0].cloneNode(!0):null;if(i.className="text",c.className=this.$menu[0].parentNode.className+" open",d.className="dropdown-menu open",e.className="dropdown-menu inner",f.className="divider",i.appendChild(document.createTextNode("Inner text")),h.appendChild(i),g.appendChild(h),e.appendChild(g),e.appendChild(f),j&&d.appendChild(j),k){var n=document.createElement("span");k.className="bs-searchbox",n.className="form-control",k.appendChild(n),d.appendChild(k)}l&&d.appendChild(l),d.appendChild(e),m&&d.appendChild(m),c.appendChild(d),document.body.appendChild(c);var o=h.offsetHeight,p=j?j.offsetHeight:0,q=k?k.offsetHeight:0,r=l?l.offsetHeight:0,s=m?m.offsetHeight:0,t=a(f).outerHeight(!0),u="function"==typeof getComputedStyle?getComputedStyle(d):!1,v=u?null:a(d),w=parseInt(u?u.paddingTop:v.css("paddingTop"))+parseInt(u?u.paddingBottom:v.css("paddingBottom"))+parseInt(u?u.borderTopWidth:v.css("borderTopWidth"))+parseInt(u?u.borderBottomWidth:v.css("borderBottomWidth")),x=w+parseInt(u?u.marginTop:v.css("marginTop"))+parseInt(u?u.marginBottom:v.css("marginBottom"))+2;document.body.removeChild(c),this.sizeInfo={liHeight:o,headerHeight:p,searchHeight:q,actionsHeight:r,doneButtonHeight:s,dividerHeight:t,menuPadding:w,menuExtras:x}}},setSize:function(){if(this.findLis(),this.liHeight(),this.options.header&&this.$menu.css("padding-top",0),this.options.size!==!1){var b,c,d,e,f=this,g=this.$menu,h=this.$menuInner,i=a(window),j=this.$newElement[0].offsetHeight,k=this.sizeInfo.liHeight,l=this.sizeInfo.headerHeight,m=this.sizeInfo.searchHeight,n=this.sizeInfo.actionsHeight,o=this.sizeInfo.doneButtonHeight,p=this.sizeInfo.dividerHeight,q=this.sizeInfo.menuPadding,r=this.sizeInfo.menuExtras,s=this.options.hideDisabled?".disabled":"",t=function(){d=f.$newElement.offset().top-i.scrollTop(),e=i.height()-d-j};if(t(),"auto"===this.options.size){var u=function(){var i,j=function(b,c){return function(d){return c?d.classList?d.classList.contains(b):a(d).hasClass(b):!(d.classList?d.classList.contains(b):a(d).hasClass(b))}},p=f.$menuInner[0].getElementsByTagName("li"),s=Array.prototype.filter?Array.prototype.filter.call(p,j("hidden",!1)):f.$lis.not(".hidden"),u=Array.prototype.filter?Array.prototype.filter.call(s,j("dropdown-header",!0)):s.filter(".dropdown-header");t(),b=e-r,f.options.container?(g.data("height")||g.data("height",g.height()),c=g.data("height")):c=g.height(),f.options.dropupAuto&&f.$newElement.toggleClass("dropup",d>e&&c>b-r),f.$newElement.hasClass("dropup")&&(b=d-r),i=s.length+u.length>3?3*k+r-2:0,g.css({"max-height":b+"px",overflow:"hidden","min-height":i+l+m+n+o+"px"}),h.css({"max-height":b-l-m-n-o-q+"px","overflow-y":"auto","min-height":Math.max(i-q,0)+"px"})};u(),this.$searchbox.off("input.getSize propertychange.getSize").on("input.getSize propertychange.getSize",u),i.off("resize.getSize scroll.getSize").on("resize.getSize scroll.getSize",u)}else if(this.options.size&&"auto"!=this.options.size&&this.$lis.not(s).length>this.options.size){var v=this.$lis.not(".divider").not(s).children().slice(0,this.options.size).last().parent().index(),w=this.$lis.slice(0,v+1).filter(".divider").length;b=k*this.options.size+w*p+q,f.options.container?(g.data("height")||g.data("height",g.height()),c=g.data("height")):c=g.height(),f.options.dropupAuto&&this.$newElement.toggleClass("dropup",d>e&&c>b-r),g.css({"max-height":b+l+m+n+o+"px",overflow:"hidden","min-height":""}),h.css({"max-height":b-q+"px","overflow-y":"auto","min-height":""})}}},setWidth:function(){if("auto"===this.options.width){this.$menu.css("min-width","0");var a=this.$menu.parent().clone().appendTo("body"),b=this.options.container?this.$newElement.clone().appendTo("body"):a,c=a.children(".dropdown-menu").outerWidth(),d=b.css("width","auto").children("button").outerWidth();a.remove(),b.remove(),this.$newElement.css("width",Math.max(c,d)+"px")}else"fit"===this.options.width?(this.$menu.css("min-width",""),this.$newElement.css("width","").addClass("fit-width")):this.options.width?(this.$menu.css("min-width",""),this.$newElement.css("width",this.options.width)):(this.$menu.css("min-width",""),this.$newElement.css("width",""));this.$newElement.hasClass("fit-width")&&"fit"!==this.options.width&&this.$newElement.removeClass("fit-width")},selectPosition:function(){var b,c,d=this,e=a('<div class="bs-container" />'),f=function(a){e.addClass(a.attr("class").replace(/form-control|fit-width/gi,"")).toggleClass("dropup",a.hasClass("dropup")),b=a.offset(),c=a.hasClass("dropup")?0:a[0].offsetHeight,e.css({top:b.top+c,left:b.left,width:a[0].offsetWidth})};this.$newElement.on("click",function(){d.isDisabled()||(f(a(this)),e.appendTo(d.options.container),e.toggleClass("open",!a(this).hasClass("open")),e.append(d.$menu))}),a(window).on("resize scroll",function(){f(d.$newElement)}),this.$element.on("hide.bs.select",function(){d.$menu.data("height",d.$menu.height()),e.detach()})},setSelected:function(a,b,c){c||(c=this.findLis().eq(this.liObj[a])),c.toggleClass("selected",b)},setDisabled:function(a,b,c){c||(c=this.findLis().eq(this.liObj[a])),b?c.addClass("disabled").children("a").attr("href","#").attr("tabindex",-1):c.removeClass("disabled").children("a").removeAttr("href").attr("tabindex",0)},isDisabled:function(){return this.$element[0].disabled},checkDisabled:function(){var a=this;this.isDisabled()?(this.$newElement.addClass("disabled"),this.$button.addClass("disabled").attr("tabindex",-1)):(this.$button.hasClass("disabled")&&(this.$newElement.removeClass("disabled"),this.$button.removeClass("disabled")),-1!=this.$button.attr("tabindex")||this.$element.data("tabindex")||this.$button.removeAttr("tabindex")),this.$button.click(function(){return!a.isDisabled()})},tabIndex:function(){this.$element.is("[tabindex]")&&(this.$element.data("tabindex",this.$element.attr("tabindex")),this.$button.attr("tabindex",this.$element.data("tabindex")))},clickListener:function(){var b=this,c=a(document);this.$newElement.on("touchstart.dropdown",".dropdown-menu",function(a){a.stopPropagation()}),c.data("spaceSelect",!1),this.$button.on("keyup",function(a){/(32)/.test(a.keyCode.toString(10))&&c.data("spaceSelect")&&(a.preventDefault(),c.data("spaceSelect",!1))}),this.$newElement.on("click",function(){b.setSize(),b.$element.on("shown.bs.select",function(){if(b.options.liveSearch||b.multiple){if(!b.multiple){var a=b.liObj[b.$element[0].selectedIndex];if("number"!=typeof a||b.options.size===!1)return;var c=b.$lis.eq(a)[0].offsetTop-b.$menuInner[0].offsetTop;c=c-b.$menuInner[0].offsetHeight/2+b.sizeInfo.liHeight/2,b.$menuInner[0].scrollTop=c}}else b.$menuInner.find(".selected a").focus()})}),this.$menuInner.on("click","li a",function(c){var d=a(this),e=d.parent().data("originalIndex"),f=b.$element.val(),g=b.$element.prop("selectedIndex");if(b.multiple&&c.stopPropagation(),c.preventDefault(),!b.isDisabled()&&!d.parent().hasClass("disabled")){var h=b.$element.find("option"),i=h.eq(e),j=i.prop("selected"),k=i.parent("optgroup"),l=b.options.maxOptions,m=k.data("maxOptions")||!1;if(b.multiple){if(i.prop("selected",!j),b.setSelected(e,!j),d.blur(),l!==!1||m!==!1){var n=l<h.filter(":selected").length,o=m<k.find("option:selected").length;if(l&&n||m&&o)if(l&&1==l)h.prop("selected",!1),i.prop("selected",!0),b.$menuInner.find(".selected").removeClass("selected"),b.setSelected(e,!0);else if(m&&1==m){k.find("option:selected").prop("selected",!1),i.prop("selected",!0);var p=d.parent().data("optgroup");b.$menuInner.find('[data-optgroup="'+p+'"]').removeClass("selected"),b.setSelected(e,!0)}else{var q="function"==typeof b.options.maxOptionsText?b.options.maxOptionsText(l,m):b.options.maxOptionsText,r=q[0].replace("{n}",l),s=q[1].replace("{n}",m),t=a('<div class="notify"></div>');q[2]&&(r=r.replace("{var}",q[2][l>1?0:1]),s=s.replace("{var}",q[2][m>1?0:1])),i.prop("selected",!1),b.$menu.append(t),l&&n&&(t.append(a("<div>"+r+"</div>")),b.$element.trigger("maxReached.bs.select")),m&&o&&(t.append(a("<div>"+s+"</div>")),b.$element.trigger("maxReachedGrp.bs.select")),setTimeout(function(){b.setSelected(e,!1)},10),t.delay(750).fadeOut(300,function(){a(this).remove()})}}}else h.prop("selected",!1),i.prop("selected",!0),b.$menuInner.find(".selected").removeClass("selected"),b.setSelected(e,!0);b.multiple?b.options.liveSearch&&b.$searchbox.focus():b.$button.focus(),(f!=b.$element.val()&&b.multiple||g!=b.$element.prop("selectedIndex")&&!b.multiple)&&(b.$element.triggerNative("change"),b.$element.trigger("changed.bs.select",[e,i.prop("selected"),j]))}}),this.$menu.on("click","li.disabled a, .popover-title, .popover-title :not(.close)",function(c){c.currentTarget==this&&(c.preventDefault(),c.stopPropagation(),b.options.liveSearch&&!a(c.target).hasClass("close")?b.$searchbox.focus():b.$button.focus())}),this.$menuInner.on("click",".divider, .dropdown-header",function(a){a.preventDefault(),a.stopPropagation(),b.options.liveSearch?b.$searchbox.focus():b.$button.focus()}),this.$menu.on("click",".popover-title .close",function(){b.$button.click()}),this.$searchbox.on("click",function(a){a.stopPropagation()}),this.$menu.on("click",".actions-btn",function(c){b.options.liveSearch?b.$searchbox.focus():b.$button.focus(),c.preventDefault(),c.stopPropagation(),a(this).hasClass("bs-select-all")?b.selectAll():b.deselectAll(),b.$element.triggerNative("change")}),this.$element.change(function(){b.render(!1)})},liveSearchListener:function(){var d=this,e=a('<li class="no-results"></li>');this.$newElement.on("click.dropdown.data-api touchstart.dropdown.data-api",function(){d.$menuInner.find(".active").removeClass("active"),d.$searchbox.val()&&(d.$searchbox.val(""),d.$lis.not(".is-hidden").removeClass("hidden"),e.parent().length&&e.remove()),d.multiple||d.$menuInner.find(".selected").addClass("active"),setTimeout(function(){d.$searchbox.focus()},10)}),this.$searchbox.on("click.dropdown.data-api focus.dropdown.data-api touchend.dropdown.data-api",function(a){a.stopPropagation()}),this.$searchbox.on("input propertychange",function(){if(d.$searchbox.val()){var f=d.$lis.not(".is-hidden").removeClass("hidden").children("a");f=d.options.liveSearchNormalize?f.not(":a"+d._searchStyle()+'("'+b(d.$searchbox.val())+'")'):f.not(":"+d._searchStyle()+'("'+d.$searchbox.val()+'")'),f.parent().addClass("hidden"),d.$lis.filter(".dropdown-header").each(function(){var b=a(this),c=b.data("optgroup");0===d.$lis.filter("[data-optgroup="+c+"]").not(b).not(".hidden").length&&(b.addClass("hidden"),d.$lis.filter("[data-optgroup="+c+"div]").addClass("hidden"))});var g=d.$lis.not(".hidden");g.each(function(b){var c=a(this);c.hasClass("divider")&&(c.index()===g.first().index()||c.index()===g.last().index()||g.eq(b+1).hasClass("divider"))&&c.addClass("hidden")}),d.$lis.not(".hidden, .no-results").length?e.parent().length&&e.remove():(e.parent().length&&e.remove(),e.html(d.options.noneResultsText.replace("{0}",'"'+c(d.$searchbox.val())+'"')).show(),d.$menuInner.append(e))}else d.$lis.not(".is-hidden").removeClass("hidden"),e.parent().length&&e.remove();d.$lis.filter(".active").removeClass("active"),d.$searchbox.val()&&d.$lis.not(".hidden, .divider, .dropdown-header").eq(0).addClass("active").children("a").focus(),a(this).focus()})},_searchStyle:function(){var a={begins:"ibegins",startsWith:"ibegins"};return a[this.options.liveSearchStyle]||"icontains"},val:function(a){return"undefined"!=typeof a?(this.$element.val(a),this.render(),this.$element):this.$element.val()},changeAll:function(b){"undefined"==typeof b&&(b=!0),this.findLis();for(var c=this.$element.find("option"),d=this.$lis.not(".divider, .dropdown-header, .disabled, .hidden").toggleClass("selected",b),e=d.length,f=[],g=0;e>g;g++){var h=d[g].getAttribute("data-original-index");f[f.length]=c.eq(h)[0]}a(f).prop("selected",b),this.render(!1)},selectAll:function(){return this.changeAll(!0)},deselectAll:function(){return this.changeAll(!1)},keydown:function(c){var d,e,f,g,h,i,j,k,l,m=a(this),n=m.is("input")?m.parent().parent():m.parent(),o=n.data("this"),p=":not(.disabled, .hidden, .dropdown-header, .divider)",q={32:" ",48:"0",49:"1",50:"2",51:"3",52:"4",53:"5",54:"6",55:"7",56:"8",57:"9",59:";",65:"a",66:"b",67:"c",68:"d",69:"e",70:"f",71:"g",72:"h",73:"i",74:"j",75:"k",76:"l",77:"m",78:"n",79:"o",80:"p",81:"q",82:"r",83:"s",84:"t",85:"u",86:"v",87:"w",88:"x",89:"y",90:"z",96:"0",97:"1",98:"2",99:"3",100:"4",101:"5",102:"6",103:"7",104:"8",105:"9"};if(o.options.liveSearch&&(n=m.parent().parent()),o.options.container&&(n=o.$menu),d=a("[role=menu] li",n),l=o.$menu.parent().hasClass("open"),!l&&(c.keyCode>=48&&c.keyCode<=57||c.keyCode>=96&&c.keyCode<=105||c.keyCode>=65&&c.keyCode<=90)&&(o.options.container?o.$newElement.trigger("click"):(o.setSize(),o.$menu.parent().addClass("open"),l=!0),o.$searchbox.focus()),o.options.liveSearch&&(/(^9$|27)/.test(c.keyCode.toString(10))&&l&&0===o.$menu.find(".active").length&&(c.preventDefault(),o.$menu.parent().removeClass("open"),o.options.container&&o.$newElement.removeClass("open"),o.$button.focus()),d=a("[role=menu] li"+p,n),m.val()||/(38|40)/.test(c.keyCode.toString(10))||0===d.filter(".active").length&&(d=o.$menuInner.find("li"),d=o.options.liveSearchNormalize?d.filter(":a"+o._searchStyle()+"("+b(q[c.keyCode])+")"):d.filter(":"+o._searchStyle()+"("+q[c.keyCode]+")"))),d.length){if(/(38|40)/.test(c.keyCode.toString(10)))e=d.index(d.find("a").filter(":focus").parent()),g=d.filter(p).first().index(),h=d.filter(p).last().index(),f=d.eq(e).nextAll(p).eq(0).index(),i=d.eq(e).prevAll(p).eq(0).index(),j=d.eq(f).prevAll(p).eq(0).index(),o.options.liveSearch&&(d.each(function(b){a(this).hasClass("disabled")||a(this).data("index",b)}),e=d.index(d.filter(".active")),g=d.first().data("index"),h=d.last().data("index"),f=d.eq(e).nextAll().eq(0).data("index"),i=d.eq(e).prevAll().eq(0).data("index"),j=d.eq(f).prevAll().eq(0).data("index")),k=m.data("prevIndex"),38==c.keyCode?(o.options.liveSearch&&e--,e!=j&&e>i&&(e=i),g>e&&(e=g),e==k&&(e=h)):40==c.keyCode&&(o.options.liveSearch&&e++,-1==e&&(e=0),e!=j&&f>e&&(e=f),e>h&&(e=h),e==k&&(e=g)),m.data("prevIndex",e),o.options.liveSearch?(c.preventDefault(),m.hasClass("dropdown-toggle")||(d.removeClass("active").eq(e).addClass("active").children("a").focus(),m.focus())):d.eq(e).children("a").focus();else if(!m.is("input")){var r,s,t=[];d.each(function(){a(this).hasClass("disabled")||a.trim(a(this).children("a").text().toLowerCase()).substring(0,1)==q[c.keyCode]&&t.push(a(this).index())}),r=a(document).data("keycount"),r++,a(document).data("keycount",r),s=a.trim(a(":focus").text().toLowerCase()).substring(0,1),s!=q[c.keyCode]?(r=1,a(document).data("keycount",r)):r>=t.length&&(a(document).data("keycount",0),r>t.length&&(r=1)),d.eq(t[r-1]).children("a").focus()}if((/(13|32)/.test(c.keyCode.toString(10))||/(^9$)/.test(c.keyCode.toString(10))&&o.options.selectOnTab)&&l){if(/(32)/.test(c.keyCode.toString(10))||c.preventDefault(),o.options.liveSearch)/(32)/.test(c.keyCode.toString(10))||(o.$menuInner.find(".active a").click(),m.focus());else{var u=a(":focus");u.click(),u.focus(),c.preventDefault(),a(document).data("spaceSelect",!0)}a(document).data("keycount",0)}(/(^9$|27)/.test(c.keyCode.toString(10))&&l&&(o.multiple||o.options.liveSearch)||/(27)/.test(c.keyCode.toString(10))&&!l)&&(o.$menu.parent().removeClass("open"),o.options.container&&o.$newElement.removeClass("open"),o.$button.focus())}},mobile:function(){this.$element.addClass("mobile-device").appendTo(this.$newElement),this.options.container&&this.$menu.hide()},refresh:function(){this.$lis=null,this.liObj={},this.reloadLi(),this.render(),this.checkDisabled(),this.liHeight(!0),this.setStyle(),this.setWidth(),this.$lis&&this.$searchbox.trigger("propertychange"),this.$element.trigger("refreshed.bs.select")},hide:function(){this.$newElement.hide()},show:function(){this.$newElement.show()},remove:function(){this.$newElement.remove(),this.$element.remove()}};var f=a.fn.selectpicker;a.fn.selectpicker=d,a.fn.selectpicker.Constructor=e,a.fn.selectpicker.noConflict=function(){return a.fn.selectpicker=f,this},a(document).data("keycount",0).on("keydown",'.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input',e.prototype.keydown).on("focusin.modal",'.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input',function(a){a.stopPropagation()}),a(window).on("load.bs.select.data-api",function(){a(".selectpicker").each(function(){var b=a(this);d.call(b,b.data())})})}(a)});

/**
 * Created by naveen on 11/6/16.
 */

relatasApp.controller("left_menu_bar", function($scope){

    if((/action/.test(window.location.pathname))){
        $scope.sideNavLanding = 'sidebar-highlight';
        $scope.sideNavInsights = ''
    } else if((/today/.test(window.location.pathname))){
        $scope.sideNavLanding = 'sidebar-highlight';
    } else if((/insights/.test(window.location.pathname))){
        $scope.sideNavInsights = 'sidebar-highlight';
    }else if((/insights/.test(window.location.pathname))){
        $scope.sideNavAccounts = 'sidebar-highlight';
    }else if((/help/.test(window.location.pathname))){
        $scope.sideNavHelp = 'sidebar-highlight';
    }else if((/contacts/.test(window.location.pathname))){
        $scope.sideNavContacts = 'sidebar-highlight';
    }else if((/opportunities/.test(window.location.pathname))){
        $scope.sideNavOpp = 'sidebar-highlight';
    }else if((/recommendations/.test(window.location.pathname))) {
        $scope.sideNavRecommendation = 'sidebar-highlight';
    }else if((/accounts/.test(window.location.pathname))){
        $scope.sideNavAccounts = 'sidebar-highlight';
    }else if((/document/.test(window.location.pathname))){
        $scope.sideNavDocuments = 'sidebar-highlight';
    } else if((/commit/.test(window.location.pathname))){
        $scope.sideNavCommits = 'sidebar-highlight';
    }else if((/admin/.test(window.location.pathname))){
        $scope.sideNavAdmin = 'sidebar-highlight';
    }else if((/pulse/.test(window.location.pathname))){
        $scope.sideNavReports = 'sidebar-highlight';
    }
})

relatasApp.controller("refresh_data", function($scope,share,$http){

    $scope.refreshData = function () {

        $scope.dataNoSync = true
        $scope.message = "Syncing data... this may take few minutes..."

        setTimeOutCallback(40000,function () {
            $scope.message = "Data sync successful. Please refresh the page to see latest data."
            $scope.dataSynced = true;

            var data = {
                dataUpdatedAt: new Date()
            }

            window.localStorage['refreshData'] = JSON.stringify(data)
        });

        $http.get('/user/refresh/data')
            .success(function (response) {
            });
    }

    $scope.reloadPage = function () {
        $( ".refresh-data-widget").fadeOut( 1000, function() {
            $scope.dataRefreshed = true;
            window.location = "/contacts/all"
        });
    }

    dataNeedsToBeRefreshed($scope)
    clearLocalStorage("relatasLocalDb")

    $scope.message = ""

    function setMessage(){
        if(share.l_user){

            if(share.l_user.lastDataSyncDate){

                var lastSyncDt = new Date(share.l_user.lastDataSyncDate)

                if(share.l_user.lastMobileSyncDate && new Date(share.l_user.lastMobileSyncDate)>lastSyncDt){
                    lastSyncDt = new Date(share.l_user.lastMobileSyncDate)
                }

                $scope.message = "Data last synced on "+moment(lastSyncDt).format("dddd, MMMM Do YYYY, h:mm:ss A")
            } else {
                $scope.message = "Welcome aboard. Click here to update your interactions"
            }

        } else {
            setTimeOutCallback(100,function () {
                setMessage();
            })
        }
    }

    setMessage()

})

function dataNeedsToBeRefreshed($scope) {

    // clearLocalStorage("refreshData")
    try {
        var cachedData = JSON.parse(window.localStorage['refreshData']);
        if(cachedData && cachedData.dataUpdatedAt){
            var diff = moment().diff(moment(cachedData.dataUpdatedAt),"minutes")
            $scope.dataRefreshed = true;
            // if(diff>=0){
            // if(diff>180){
            if(diff>60){
                $scope.dataRefreshed = false;
                clearLocalStorage("refreshData")
            }
        }
    } catch (err){
        $scope.dataRefreshed = false;
    }
}
relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("commit", function($scope,$http,share,$rootScope,searchService){

    function checkCompanyLoaded(){
        if($rootScope.companyDetails){

        } else {
            setTimeOutCallback(1000,function () {
                checkCompanyLoaded()
            })
        }
    }
    checkCompanyLoaded();

    $scope.closing = {
        this:"all"
    }

    $scope.getDataForRange = function(range){
        $scope.selectedCommitRange = range;
        $scope.seeReview($scope.owner)
    }

    $scope.goToAccount = function(accountName){
        window.location = "/accounts/all?accountName="+accountName
    }

    $scope.closeOppInsightsModal = function(){
        $scope.showOppInsights = false;
        $rootScope.oppTabView = false;
        $rootScope.regionTabView = false;
        $scope.rolesList = [];
    };

    $scope.commitMovement = function(pastMonthCommit){
        commitMovement($scope,$http,share,$rootScope,pastMonthCommit)
    }

    $scope.goToTeamTab = function(op){
        $scope.getInteractionHistory(op,'internalTeam');
    }

    $scope.showOppsFor = function(user){
        var flip = false;
        if(!$scope.selectedUser){
            flip = true;
        }

        if(!$scope.selectedUser || ($scope.selectedUser && $scope.selectedUser.emailId != user.emailId)) {
            flip = true
        } else {
            flip = false
        }

        $scope.selectedUser = user;

        getOppsFromLocalStorage(function (oppsInCommitStage) {
            if(flip){
                if(oppsInCommitStage && oppsInCommitStage.length>0){
                    $scope.oppsInCommitStage = oppsInCommitStage.filter(function (op) {
                        return op.userEmailId == user.emailId;
                    })
                }
            } else {
                $scope.oppsInCommitStage = oppsInCommitStage;
            }
        });

        $('.action-board').animate({scrollTop: '+=450px'}, 500);
    }

    $scope.viewAllMyAccess = function(){
        $scope.allMyAccess = true;
        $scope.selfSelection = "";
        $scope.teamSelection = "";
        $scope.allSelection = "btn-selected";

        var url = "/review/all/my/access/opps";
        url = fetchUrlWithParameter(url,"userId",$scope.owner.userId);

        $http.get(url)
            .success(function (response) {
                window.localStorage['oppsInCommitStage'] = "notSet";

                if(response && response.SuccessCode){

                    $scope.oppsInCommitStage = response.Data.opps;
                    _.each($scope.oppsInCommitStage,function (op) {

                        op.closingThisWeek = {background:"inherit"};
                        op.riskMeter = response.Data.dealsAtRisk && response.Data.dealsAtRisk[op.opportunityId]?response.Data.dealsAtRisk[op.opportunityId]*100:0

                        if(new Date(op.closeDate)>= new Date(moment().startOf("month")) && new Date(op.closeDate) <= new Date(moment().endOf("month"))){
                            op.closingThisWeek = {
                                background:"#f2f2f2"
                            }
                        }
                        op = formatOpp(op,share,$scope)
                    });

                    window.localStorage['oppsInCommitStage'] = JSON.stringify($scope.oppsInCommitStage);
                    $scope.closing = {
                        this:"range"
                    }

                    $scope.filterOpps("range");
                }
            });
    }

    $scope.oppRenewalStatusSet = function (opp) {
        opp.renewalStatusSet = true
    }

    $scope.selectContact = function(contact){
        $scope.opp.searchContent  = contact.fullName + " ("+contact.emailId+")"
        $scope.searchContent = contact.fullName + " ("+contact.emailId+")";
        $scope.newOppContact = contact;
        $scope.showResultscontact = false;
        $scope.opp.contactEmailIdReq = false;

        if($scope.isExistingOpp){
            $scope.opp.contactEmailId = contact.emailId
            $scope.opp.mobileNumber = contact.mobileNumber
        }
    }

    $scope.filterOpps = function(filter,removeOppForUser){

        try {
            checkLocalStorageSet();

            function checkLocalStorageSet(){

                $scope.commitPipeline = 0;
                var oppsInCommitStage = JSON.parse(window.localStorage['oppsInCommitStage']);

                if(oppsInCommitStage !== "notSet"){

                    if(filter == 'range'){
                        $scope.oppsInCommitStage = oppsInCommitStage.filter(function (op) {
                            return op.closingThisWeek && op.closingThisWeek.background !== "inherit"
                        })
                    } else {
                        $scope.oppsInCommitStage = oppsInCommitStage;
                    }

                    if(removeOppForUser){
                        $scope.oppsInCommitStage = $scope.oppsInCommitStage.filter(function (op) {
                            return !removeOppForUser[op.userEmailId];
                        })
                    }

                    if($scope.oppsInCommitStage && $scope.oppsInCommitStage.length>0){
                        _.each($scope.oppsInCommitStage,function (op) {
                            $scope.commitPipeline = $scope.commitPipeline+op.amountWithNgm;
                        })
                    }

                    $scope.commitPipeline = getAmountInThousands($scope.commitPipeline,2,share.primaryCurrency=="INR")

                } else {
                    setTimeOutCallback(300,function () {
                        checkLocalStorageSet();
                    })
                }
            };
        } catch (e) {
            console.log("error", e);
        }

        $scope.closing.this = filter;
    }

    $scope.commitForNextRanges = function(){
        $scope.committingForNext = !$scope.committingForNext;
        if($scope.committingForNext){
            $rootScope.weeklyCommitCutOff = "Commit Close Date: "+moment(moment(share.commitCutOffObj.week).add(1,'week')).tz("UTC").format(standardDateFormat())
            $rootScope.monthlyCommitCutOff = "Commit Close Date: "+moment(moment(share.commitCutOffObj.month).add(1,'month')).tz("UTC").format(standardDateFormat())
            $rootScope.quarterlyCommitCutOff = "Commit Close Date: "+moment(moment(share.commitCutOffObj.quarter.startOfQuarter).add(2,"month")).tz("UTC").format(standardDateFormat());

            $scope.loadingData = true;

            $http.get("/review/next/commits?range="+$scope.selectedCommitRange.name)
                .success(function (response) {

                    $scope.next = {
                        selfCommitValue: 0
                    }
                    if(response && response.Data){
                        $scope.next = {
                            selfCommitValue: response.Data.month.userCommitAmount
                        }

                        if($scope.selectedCommitRange.name == "Week"){
                            $scope.next = {
                                selfCommitValue: response.Data.week.userCommitAmount
                            }
                        }

                        if($scope.selectedCommitRange.name == "Quarter"){
                            $scope.next = {
                                selfCommitValue: response.Data.quarter.userCommitAmount
                            }
                        }
                    }
                    $scope.loadingData = false;
                });

        } else {
            $rootScope.weeklyCommitCutOff = "Commit Close Date: "+moment(share.commitCutOffObj.week).tz("UTC").format(standardDateFormat())
            $rootScope.monthlyCommitCutOff = "Commit Close Date: "+moment(share.commitCutOffObj.month).tz("UTC").format(standardDateFormat())
            $rootScope.quarterlyCommitCutOff = "Commit Close Date: "+moment(share.commitCutOffObj.quarter.startOfQuarter).tz("UTC").format(standardDateFormat())
        }
    }

    $scope.sortType = "fullName"
    $scope.sortReverse = false;

    $scope.oppSortType = "closeDate"
    $scope.oppSortReverse = true;
    $scope.teamCommitsViewing = false;

    $scope.loadingData = true;
    $scope.actionloadingData = true;
    $scope.currentMonth = moment().format('MMM YYYY');

    $scope.initAutocomplete = function(){

        checkDOMLoaded();

        function checkDOMLoaded(){
            if(document.getElementById('autocompleteCity')){

                var autocomplete2 = new google.maps.places.Autocomplete(
                    (document.getElementById('autocompleteCity')),
                    {types:['(cities)']});
                autocomplete2.addListener('place_changed', function(err,places){

                    if(!$scope.opp){
                        $scope.opp = {}
                    }

                    if(!$scope.opp.geoLocation){
                        $scope.opp.geoLocation = {}
                    }

                    $scope.opp.geoLocation.lat = autocomplete2.getPlace().geometry.location.lat()
                    $scope.opp.geoLocation.lng = autocomplete2.getPlace().geometry.location.lng()

                    $scope.opp.geoLocation.town = $("#autocompleteCity").val();

                    if(share.setTown){
                        share.setTown(null)
                    }

                    if($("#autocompleteCity").val() && $("#autocompleteCity").val() != "" && $("#autocompleteCity").val() != " "){
                        if(share.setTown){
                            share.setTown($("#autocompleteCity").val())
                        }
                    }
                    return false;
                });
            } else {
                setTimeOutCallback(100,function () {
                    checkDOMLoaded();
                })
            }
        }
    }

    // autoInitGoogleLocationAPI(share,$scope);

    $scope.initAutocomplete();

    $scope.getAllCommitCutOffDates = function () {

        $http.get("/review/get/all/commit/cutoff")
            .success(function (response) {
                if(response){
                    share.commitCutOffObj = response;
                    $rootScope.weeklyCommitCutOff = "Commit Close Date: "+moment(response.week).tz("UTC").format(standardDateFormat())
                    $rootScope.monthlyCommitCutOff = "Commit Close Date: "+moment(response.month).tz("UTC").format(standardDateFormat())
                    $rootScope.quarterlyCommitCutOff = "Commit Close Date: "+moment(response.quarter.startOfQuarter).tz("UTC").format(standardDateFormat())
                    
                }
            })
    }

    getOppStages();

    function getOppStages(){

        if(share.opportunityStages){
            $scope.stagesSelection = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(500,function () {
                getOppStages()
            })
        }
    }

    $scope.searchContacts = function(keywords,type){
        searchResults($scope,$http,keywords,share,searchService,type)
    };

    $scope.addRecipient = function (contact) {
        addRecipient($scope,$http,contact,contact.type)
    }

    $scope.removeRecipient = function(contact,type){
        if(!$scope.opp.isNotOwner){
            removeRecipient($scope,$http,contact,type)
        }
    }

    $scope.reCalClosingThisSelection = function (user) {
    }

    $scope.selectedUserValues = function (user) {

        if(user.selected){
            if(isNumber(user.oppsValueUnderCommitStageSort)){
                $scope.oppValueSelected = $scope.oppValueSelected+user.oppsValueUnderCommitStageSort;
            }

            if(isNumber(user.valueSort)){
                $scope.commitValueSelected = $scope.commitValueSelected+user.valueSort;
            }

            if(isNumber(user.target)){
                $scope.target = $scope.target+user.target;
            }

            if(isNumber(user.won)){
                $scope.achievement = $scope.achievement+user.won;
            }

            if(user.oppsValClosingThisSelection){
                $scope.closingThisSelection.num = $scope.closingThisSelection.num+user.oppsValClosingThisSelection;
            }

        } else {
            $scope.target = $scope.target-user.target;
            $scope.achievement = $scope.achievement-user.won;
            $scope.commitValueSelected = $scope.commitValueSelected-user.valueSort;
            $scope.oppValueSelected = $scope.oppValueSelected-user.oppsValueUnderCommitStageSort;
            if(user.oppsValClosingThisSelection){
                $scope.closingThisSelection.num = $scope.closingThisSelection.num-user.oppsValClosingThisSelection;
            }
        }

        $scope.targetForDisplay = getAmountInThousands($scope.target,2,share.primaryCurrency=="INR")
        $scope.achievementForDisplay = getAmountInThousands($scope.achievement,2,share.primaryCurrency=="INR")
        $scope.commitValueSelectedForDisplay = getAmountInThousands($scope.commitValueSelected,2,share.primaryCurrency=="INR")
        $scope.oppValueSelectedForDisplay = getAmountInThousands($scope.oppValueSelected,2,share.primaryCurrency=="INR");
        $scope.closingThisSelection.formatted = getAmountInThousands($scope.closingThisSelection.num,2,share.primaryCurrency=="INR")
        $scope.oppsInCommitStage = [];

        var removeObj = {};
        _.each($scope.commitsByUsers,function (co) {
            if(co.selected === false){
                removeObj[co.profile.emailId] = true;
            }
        });

        var range = "range";
        if($scope.closing && $scope.closing.this){
            range = $scope.closing.this;
        }

        // $scope.filterOpps(range,removeObj);
    }

    $scope.getAllCommitCutOffDates();
    $scope.selfSelection = "btn-selected";
    $scope.allSelection = "";
    $scope.teamSelection = "";

    $scope.viewSelfCommits = function () {
        $scope.teamCommitsViewing = false;
        $scope.allMyAccess = false;
        $scope.selfSelection = "btn-selected";
        $scope.teamSelection = "";
        $scope.allSelection = "";
        $scope.oppSortType = "closeDateSort"
        $scope.oppSortReverse = true;
        $scope.seeReview($scope.owner);
    }

    $scope.assignTaskOpp = function(op){
        $scope.getInteractionHistory(op,'tasks');
    }

    $scope.closeTeamTask = function(user){
        $scope.teamTask = false;
    }

    $scope.assignTaskTeamMember = function(user){
        $scope.teamTask = true;
        var url = '/tasks/get/all'
        url = url+"?emailId="+user.emailId+"&filter="+"weeklyReview"
        initServices($scope, $http,$rootScope,share,searchService);

        $http.get(url)
            .success(function (response) {
                if (response && response.SuccessCode){
                    getTasks($scope,response.Data.tasks);
                }
            })
    }

    $scope.viewTeamCommits = function () {

        $scope.teamCommitsViewing = true;
        $scope.allMyAccess = false;
        $scope.selfSelection = "";
        $scope.allSelection = "";
        $scope.teamSelection = "btn-selected";
        $scope.oppsInCommitStage = [];
        var users = $scope.owner.children.teamMatesUserId;
        users.push($scope.owner.userId);
        users = _.uniq(users);
        var url = '/review/commits/month/team';
        url = fetchUrlWithParameter(url,"shType",$scope.selectedHierarchy.name.replace(/[^A-Z0-9]+/ig, "_"));
        $scope.loadingData = true;
        $scope.actionloadingData = true;

        if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
            if($scope.selectedCommitRange.name == "Week"){
                url = '/review/commits/week/team';
                url = fetchUrlWithParameter(url,"shType",$scope.selectedHierarchy.name.replace(/[^A-Z0-9]+/ig, "_"));
                fetchCommit($scope,$http,share,$rootScope,users,url,'week')
            } else if ($scope.selectedCommitRange.name == "Quarter"){
                url = '/review/commits/quarter/team';
                url = fetchUrlWithParameter(url,"shType",$scope.selectedHierarchy.name.replace(/[^A-Z0-9]+/ig, "_"));
                fetchCommit($scope,$http,share,$rootScope,users,url,"quarter")
            } else {
                fetchCommit($scope,$http,share,$rootScope,users,url)
            }
        } else {
            fetchCommit($scope,$http,share,$rootScope,users,url)
        }
    }

    $scope.getDatForRange = function(range){
        $scope.actionloadingData = true;
        $scope.selectedHierarchy = $scope.hierarchyList[0];
        share.displayCommits(range.name,$scope.selectedHierarchy.name);

        checkCommitCutOffLoaded();
        function checkCommitCutOffLoaded(){
            if(share.commitCutOffObj){

                if(range.name.toLowerCase() == "week"){
                    $scope.currentMonth = moment(share.commitCutOffObj.week).startOf("isoWeek").format('DD MMM')+" - "+moment(share.commitCutOffObj.week).endOf("isoWeek").format('DD MMM');
                } else if(range.name.toLowerCase() == "quarter"){
                    $scope.currentMonth = moment(share.commitCutOffObj.startOfQuarter).format('MMM YYYY')+" - "+moment(moment(share.commitCutOffObj.endOfQuarter).subtract(1,'day')).format('MMM YYYY')
                } else {
                    $scope.currentMonth = moment().format('MMM YYYY');
                }
            } else {
                setTimeOutCallback(500,function () {
                    checkCommitCutOffLoaded();
                })
            }
        }
    }

    share.setDateRange = function (start,end,mode) {

        if(start){

            if(mode == "qtr"){
                // $scope.dataForRange = moment(start).format('MMM') +"-"+moment(end).format('MMM YY')
            } else {
                $scope.currentMonthSelection = {
                    start:start,
                    end:end
                }

                $scope.dataForRange = moment(start).format('MMM')
            }

        } else {
            setDateRange();
        }
    }

    $scope.closeModal = function(){
        $scope.commitModalOpen = false;
    }

    $scope.openCommitModal = function(){
        $scope.commitModalOpen = true;
        $scope.committingForNext = false;

        var users = [share.liuData._id];
        var url = '/review/commits/month';
        $scope.loadingData = true;

        if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
            if($scope.selectedCommitRange.name == "Week"){
                url = '/review/commits/week';
                fetchCommit($scope,$http,share,$rootScope,users,url,'week')
            } else if ($scope.selectedCommitRange.name == "Quarter"){
                url = '/review/commits/quarter';
                fetchCommit($scope,$http,share,$rootScope,users,url,"quarter")
            } else {
                fetchCommit($scope,$http,share,$rootScope,users,url)
            }
        } else {
            fetchCommit($scope,$http,share,$rootScope,users,url)
        }
    }

    $scope.updateCommit = function(){

        $scope.saving = true;
        var selfCommitValue = $scope.commits.selfCommitValue;
        if($scope.committingForNext){
            selfCommitValue = $scope.next.selfCommitValue
        }

        selfCommitValue = parseFloat(selfCommitValue);

        if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
            if($scope.selectedCommitRange.name == "Week"){
                saveWeeklyCommits($scope,$rootScope,$http,share,{selfCommitValue:selfCommitValue},function () {
                    $scope.saving = false;
                    $scope.commitModalOpen = !$scope.commitModalOpen;
                    if(!$scope.committingForNext){
                        $scope.setNewCommit(selfCommitValue);
                    }
                });
            } else if ($scope.selectedCommitRange.name == "Quarter"){
                saveQuarterlyCommits($scope,$rootScope,$http,share,{selfCommitValue:selfCommitValue},function () {
                    $scope.saving = false;
                    $scope.commitModalOpen = !$scope.commitModalOpen;
                    if(!$scope.committingForNext){
                        $scope.setNewCommit(selfCommitValue);
                    }
                });
            } else {
                saveMonthlyCommit($scope,$rootScope,$http,share,{selfCommitValue:selfCommitValue},function () {
                    $scope.saving = false;
                    $scope.commitModalOpen = !$scope.commitModalOpen;
                    if(!$scope.committingForNext){
                        $scope.setNewCommit(selfCommitValue);
                    }
                });
            }
        } else {
            saveMonthlyCommit($scope,$rootScope,$http,share,function () {
                $scope.saving = false;
                $scope.commitModalOpen = !$scope.commitModalOpen;
                $scope.setNewCommit(selfCommitValue);
            });
        }
    }

    $scope.commitRanges = [{
        name: "Week"
    },{
        name: "Month"
    },{
        name: "Quarter"
    }]

    $scope.getDatForHierarchy = function(hierarchy){
        share.displayCommits($scope.selectedCommitRange.name,hierarchy.name)
    }

    $http.get('/company/user/all/hierarchy/types')
        .success(function (response) {
            if(response && response.SuccessCode){
                $scope.hierarchyList = [];

                _.each(response.Data,function (el) {
                    $scope.hierarchyList.push({
                        name:el
                    })
                });

                $scope.selectedHierarchy = $scope.hierarchyList[0];
                share.displayCommits(null,$scope.selectedHierarchy.name)
            }
        })

    $scope.stages = _.map(share.opportunityStages,"name");

    $scope.setNewCommit = function (selfCommitValue){
        if($scope.teamList && $scope.teamList.length>0){
            _.each($scope.teamList,function (tm) {
                if(tm.userId === share.liuData._id){
                    tm.commitSort = parseFloat(selfCommitValue);
                    tm.commit = getAmountInThousands(tm.commitSort,2,share.primaryCurrency=="INR");
                    if(tm.commitSort && tm.targetAndAchievement.targetOriginal){
                        var commitPercentage = scaleBetween(tm.commitSort,0,tm.targetAndAchievement.targetOriginal);
                        commitPercentage = Math.round(commitPercentage);

                        if(commitPercentage>100){
                            tm.commitPercentageStyle = {'width':'100%',background: '#638ca6',height:"inherit"}
                            tm.commitPercentage = commitPercentage.r_formatNumber(2)+'%';
                        } else {
                            tm.commitPercentageStyle = {'width':commitPercentage+'%',background: '#638ca6',height:"inherit"}
                        }
                        tm.commitPercentage = commitPercentage.r_formatNumber(2)+'%';
                    } else if(tm.commitSort){
                        tm.commitPercentageStyle = {'width':'100%',background: '#638ca6',height:"inherit"}
                    }
                }
            })
        }
    }

    share.displayCommits = function (range,hierarchy) {

        $scope.loadingData = true;
        var url = '/company/user/hierarchy';

        if(hierarchy && hierarchy !== "Org. Hierarchy"){
            url = '/company/users/for/hierarchy';
            url = fetchUrlWithParameter(url,"hierarchyType",hierarchy.replace(/[^A-Z0-9]+/ig, "_"))
        }

        url = fetchUrlWithParameter(url,"forCommits",true)

        if(range){
            url = fetchUrlWithParameter(url,"range",range.toLowerCase())
        }

        $http.get(url)
            .success(function (response) {

                var team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0) {
                    team = buildTeamProfilesWithCommits(response.Data, response.listOfMembers, share, $scope,range?range.toLowerCase():null);
                    $scope.commitPipeline = 0;

                    _.each(team,function (el) {
                        el.wonPercentage = 0+'%';
                        el.commitPercentage = 0+'%';
                        el.commitSort = parseFloat(el.commitSort);

                        if(el.targetAndAchievement){
                            $scope.commitPipeline = $scope.commitPipeline+parseFloat(el.targetAndAchievement.commitPipeline);

                            if(el.commitSort && el.targetAndAchievement.targetOriginal){
                                var commitPercentage = scaleBetween(el.commitSort,0,el.targetAndAchievement.targetOriginal);
                                commitPercentage = Math.round(commitPercentage);

                                if(commitPercentage>100){
                                    el.commitPercentageStyle = {'width':'100%',background: '#638ca6',height:"inherit"}
                                    el.commitPercentage = commitPercentage.r_formatNumber(2)+'%';
                                } else {
                                    el.commitPercentageStyle = {'width':commitPercentage+'%',background: '#638ca6',height:"inherit"}
                                }
                                el.commitPercentage = commitPercentage.r_formatNumber(2)+'%';
                            } else if(el.commitSort) {
                                el.commitPercentageStyle = {'width':'100%',background: '#638ca6',height:"inherit"}
                                el.commitPercentage = 100+'%';
                            }

                            if(el.targetAndAchievement.won && el.targetAndAchievement.targetOriginal){
                                el.wonPercentage = Math.round(scaleBetween(el.targetAndAchievement.won,0,el.targetAndAchievement.targetOriginal)).r_formatNumber(2)+'%';
                            }

                            if(el.targetAndAchievement.won && !el.targetAndAchievement.targetOriginal){
                                el.wonPercentage = '100%'
                            }

                            if(el.commit && !el.targetAndAchievement.targetOriginal){
                                el.commitPercentage = '100%'
                            }

                            el.wonPercentageStyle = {'width':el.wonPercentage,background: '#8ECECB',height:"inherit","max-width": "100%"}

                            el.targetAndAchievement.won = parseFloat(el.targetAndAchievement.won.r_formatNumber(2))
                            el.targetAndAchievement.wonWithCommas = getAmountInThousands(parseFloat(el.targetAndAchievement.won.r_formatNumber(2)),2,share.primaryCurrency=="INR");
                            el.targetAndAchievement.commitPipelineWithCommas = getAmountInThousands(parseFloat(el.targetAndAchievement.commitPipeline.r_formatNumber(2)),2,share.primaryCurrency=="INR");
                        }
                    });

                    $scope.commitPipeline = getAmountInThousands(parseFloat($scope.commitPipeline.r_formatNumber(2)),2,share.primaryCurrency=="INR")

                    $scope.teamList = team;
                    $scope.totalTeamMembers = team.length;
                    $scope.totalTarget = getAmountInThousands(_.sumBy(team,"targetSort"),2,share.primaryCurrency=="INR");
                    $scope.totalAchievement = getAmountInThousands(_.sumBy(team,"targetAndAchievement.won"),2,share.primaryCurrency=="INR");
                    $scope.totalCommit = getAmountInThousands(_.sumBy(team,"commitSort"),2,share.primaryCurrency=="INR");
                    $scope.totalDiff = getAmountInThousands(_.sumBy(team,"differenceSort"),2,share.primaryCurrency=="INR");
                }

                $scope.loadingData = false;
            });
    }

    $scope.seeReview = function (user) {
        $scope.owner = user;
        $scope.teamCommitsViewing = false;
        $scope.allMyAccess = false;
        $scope.actionloadingData = true;
        resetAllMemberSelection($scope);
        user.selected = "selected";
        $scope.selfSelection = "btn-selected";
        $scope.teamSelection = "";
        $scope.allSelection = "";
        $scope.oppsInCommitStage = [];

        window.localStorage['oppsInCommitStage'] = "notSet";

        if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
            if($scope.selectedCommitRange.name == "Week"){
                weeklyPastCommitHistory($scope,$rootScope,$http,share,user.userId);
            } else if ($scope.selectedCommitRange.name == "Quarter"){
                quarterlyCommitHistory($scope,$rootScope,$http,share,user.userId);
            } else {
                monthlyCommitHistory($scope,$rootScope,$http,share,user.userId);
            }
        } else {
            monthlyCommitHistory($scope,$rootScope,$http,share,user.userId);
        }
    }

    $scope.setRenewalAmount = function(amount){

        if(!$scope.opp.renewed){
            $scope.opp.renewed = {
                amount:0,
                netGrossMargin:0,
                closeDate:null,
                createdDate:null
            }
        }
        $scope.opp.renewed.amount = checkRequired(amount)?parseFloat(amount):0;
    }

    $scope.registerDatePickerId = function(minDate,maxDate){

        if($scope.opp){
            if($scope.opp.stage == "Close Won" || $scope.opp.stage == "Close Lost"){
                maxDate = new Date();
            } else {
                maxDate = new Date(moment().add(4,"year"))
            }
        } else {
            $scope.opp = {}
        }

        $('#opportunityCloseDateSelector').datetimepicker({
            value:$scope.opp.closeDateFormatted,
            timepicker:false,
            validateOnBlur:false,
            minDate: minDate,
            maxDate: maxDate,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    timezone = timezone?timezone:"UTC";
                    $scope.opp.closeDate = moment(dp).format();
                    $scope.opp.closeDateFormatted = moment(dp).format("DD MMM YYYY");
                });
            }
        });
    }

    $scope.registerDatePickerIdRenewal = function(minDate,maxDate){
        pickDatesForRenewal($scope,minDate,maxDate,'#opportunityCloseDateSelector4',1,"years")
    }

    $scope.addToReasonList = function (reason) {

        if(!$scope.opp.closeReasons || $scope.opp.closeReasons.length == 0){
            $scope.opp.closeReasons = [];
        }

        if(reason.selected){
            $scope.opp.closeReasons.push(reason.name)
        } else {
            $scope.opp.closeReasons = $scope.opp.closeReasons.filter(function (el) {
                return el != reason.name
            })
        }

        $scope.opp.closeReasons = _.uniq($scope.opp.closeReasons)
    }

    $scope.ifOppClose = function (stage) {


        if(_.includes(["Close Lost","Close Won"], stage)){
            $scope.reasonsRequired = true;
            $scope.viewModeFor = "close details";
            $scope.mailOptions = true;
            $scope.closingOpp = true;
            $scope.opp.closeDate = new Date();
            $scope.opp.closeDateFormatted = moment().format(standardDateFormat());

        } else {

            $scope.closingOpp = false;
            $scope.opp.renewalAmountReq = false;
            $scope.opp.renewalCloseDateReq = false;

            $scope.mailOptions = false;
        }
        $scope.registerDatePickerId()
    }

    $scope.toggleContactPopup = function() {
        $scope.showContactCreateModal = false;
    }

    $scope.createContactWindow = function() {
        $scope.showContactCreateModal = true;
    }

    $scope.saveContact = function() {

        if(!$scope.coords){
            $scope.coords = {}
        }

        var data = {
            personName:$("#fullName").val(),
            personEmailId:$("#p_emailId").val(),
            companyName:$("#companyName").val(),
            designation:$("#designation").val(),
            mobileNumber:$("#p_mobileNumber").val(),
            location:$scope.coords.address,
            lat:$scope.coords.lat,
            lng:$scope.coords.lng
        }

        var errExists = false;

        if(!data.personName){
            errExists = true
            toastr.error("Please enter a name for the contact.")
        } else if(!validateEmail(data.personEmailId)){
            toastr.error("Please enter a valid email ID.")
            errExists = true
        }

        if(!checkRequired(data.location)){
            errExists = true
            toastr.error("Please enter a city for this contact.")
        }

        if(!errExists){
            $http.post("/contacts/create",data)
                .success(function (response) {
                    toastr.success("Contact added successfully.")
                    $scope.showContactCreateModal = false;
                })
        }
    }

    $scope.saveOpp = function() {
        $scope.isExistingOpp = true;
        if (!checkOppFieldsRequirement($scope, $rootScope, share)) {
            $scope.town = $scope.town ? $scope.town.replace(/[^a-zA-Z ]/g, "") : $scope.opp.geoLocation.town
            $scope.opp.name = $scope.opp.opportunityName;

            if ($scope.town) {
                $scope.opp.geoLocation.town = $scope.town;
            }

            var obj = {
                opportunity: $scope.opp,
                contactEmailId: $scope.opp.contactEmailId,
                contactMobile: $scope.opp.mobileNumber,
                town: $scope.town,
                zone: $scope.opp.geoLocation ? $scope.opp.geoLocation.zone : null,
                mailOrgHead: $scope.mailOrgHead,
                mailRm: $scope.mailRm,
                companyId: $rootScope.companyDetails._id,
                hierarchyParent: share.liuData.hierarchyParent,
                renewalCloseDate: $scope.renewalCloseDate,
                renewalAmount: $scope.renewalAmount
            }

            $http.post("/salesforce/edit/opportunity/for/contact", obj).success(function (response) {
                toastr.success("Opportunity successfully updated")
            })
        }
    }

    share.getInteractionHistory = function (op,viewModeFor) {
        $scope.getInteractionHistory(op,viewModeFor)
    }

    $scope.getInteractionHistory = function (op,viewModeFor) {
        getInteractionHistory($scope,$rootScope,searchService,$http,share,op,viewModeFor)
    }

    $scope.getTeamCommits = function(){

    }

    $scope.openView = function(viewFor){
        setTabView($scope,viewFor);
        $scope.viewModeFor = viewFor;
    }

    share.drawIntGrowth = function (data) {
        drawIntGrowth($scope,share,data)
    }

});

function drawIntGrowth($scope,share,data) {
    var labels = [];
    var className = ".int-growth"

    var series = [],
        series2 = [];

    if(data.length>0){

        data.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(data,function (el) {
            labels.push(moment(el.date).format("MMM"))
            series.push({
                name: moment(el.date).format("MMM"),
                value:el.initiatedByUs
            })

            series2.push({
                name: moment(el.date).format("MMM"),
                value:el.initiatedByThem
            })
        });

        drawLineChart($scope,share,series,labels,className,series2)
    }
}

function getInteractionHistory($scope,$rootScope,searchService,$http,share,op,viewModeFor) {
    if(viewModeFor){
        $scope.viewModeFor = viewModeFor;
    } else {
        $scope.viewModeFor = "insights";
    }

    setTabView($scope,$scope.viewModeFor)

    $scope.oppOwner = {};
    $scope.teamTask = false;

    checkTeamLoaded();

    function checkTeamLoaded(){
        if(share.usersDictionary){
            if(share.usersDictionary[op.userEmailId]){
                $scope.oppOwner.value = share.usersDictionary[op.userEmailId].fullName+" ("+share.usersDictionary[op.userEmailId].emailId+")"
            } else {
                $scope.oppOwner.value = op.userEmailId
            }
        } else {
            setTimeOutCallback(1000,function () {
                checkTeamLoaded();
            })
        }
    }

    $scope.showOppInsights = true;
    $scope.interactionsCount = 0;
    var url = '/review/opp/insights';
    var users = [op.userId];
    var contacts = getContactsFromOpp(op,$scope);

    if(op.usersWithAccess && op.usersWithAccess[0]){
        users = users.concat(_.map(op.usersWithAccess,function (el) {
            if(share.usersDictionary && share.usersDictionary[el.emailId] && share.usersDictionary[el.emailId].userId){
                return share.usersDictionary[el.emailId].userId
            }
        }));
    }

    url = fetchUrlWithParameter(url,"hierarchylist",users);
    url = fetchUrlWithParameter(url,"contacts",contacts);
    url = fetchUrlWithParameter(url,"opportunityId",op.opportunityId);

    if(op.createdDate){
        url = fetchUrlWithParameter(url,"createdDate",String(op.createdDate));
    }

    if(_.includes(["Close Lost","Close Won"], op.stage)){
        if(op.closeDate){
            url = fetchUrlWithParameter(url,"closeDate",String(op.closeDate));
        }
    }

    $scope.insights = [];

    $http.get(url)
        .success(function (response) {
            if(response && response.SuccessCode){

                $scope.opp = formatOpp(response.Data.opp,share);

                console.log($scope.opp.usersWithAccess)
                if($scope.opp.usersWithAccess){
                    mapUsersWithAccessToOrgRoles($scope,share,$scope.opp)
                }

                $scope.opp.isNotOwner = op.isNotOwner;
                $scope.opp.isOppClosed = op.isOppClosed;

                $scope.newOppContact = {};
                $scope.newOppContact.emailId = $scope.opp.contactEmailId;
                initServices($scope, $http,$rootScope,share,searchService);
                getAllRelatedData($scope,$http,share,$scope.opp);
                $scope.interactionsCount = response.Data.interactionsCount
                $scope.interactions = response.Data.interactions.allInteractions;

                if(response.Data.insights){
                    _.each(response.Data.insights,function (el) {
                        $scope.insights.push({
                            text: el.text,
                            riskClass: el.risk == 'high'?'red':'green'
                        })
                    })
                }

                if($scope.interactions.length>0){
                    _.each($scope.interactions,function (el) {
                        el.interactionDateFormatted = moment(el.interactionDate).format(standardDateFormat());
                        el.member = share.usersDictionary[el.ownerEmailId]
                    });
                }

                if(response.Data.interactions && response.Data.interactions.interactionsFlow){
                    share.drawIntGrowth(response.Data.interactions.interactionsFlow);
                } else {
                    share.drawIntGrowth([])
                }
            } else {
                share.drawIntGrowth([]);
            }
        });
}

function commitMovement($scope,$http,share,$rootScope,pastMonthCommit) {

    $scope.initialCommitPipeline = {
        percentage:0,
        count:0,
        amount:0
    };
    $scope.selfCommit = 0;
    $scope.won = {
        percentage:0,
        count:0,
        amount:0
    };
    $scope.idle = {
        percentage:0,
        count:0,
        amount:0
    };
    $scope.lost = {
        percentage:0,
        count:0,
        amount:0
    };
    $scope.pipeline = {
        percentage:0,
        count:0,
        amount:0
    };
    $scope.commitMet = "not-met";

    var url = "/review/pipeline/movement";
    var opportunityIds = [];
    if(pastMonthCommit){

        $scope.commitMvMtLoading = true;

        _.each(pastMonthCommit.opportunities,function (op) {
            opportunityIds.push(op.opportunityId)
            $scope.initialCommitPipeline.amount = $scope.initialCommitPipeline.amount+op.amount;
        })

        $scope.initialCommitPipeline.count = opportunityIds.length;

        if($scope.selectedCommitRange && $scope.selectedCommitRange.name){
            if($scope.selectedCommitRange.name == "Week"){
                $scope.selfCommit = pastMonthCommit.week.userCommitAmount
            } else if ($scope.selectedCommitRange.name == "Quarter"){
                $scope.selfCommit = pastMonthCommit.quarter.userCommitAmount
            } else {
                $scope.selfCommit = pastMonthCommit.month.userCommitAmount
            }
        } else {
            $scope.selfCommit = pastMonthCommit.month.userCommitAmount
        }
        url = fetchUrlWithParameter(url,"range",$scope.selectedCommitRange.name);

        if(opportunityIds.length>0){
            url = fetchUrlWithParameter(url,"opportunityIds",opportunityIds);

            $http.get(url)
                .success(function (response) {
                    if(response && response.SuccessCode && response.Data && response.Data.length>0){
                        var endOfLastMonth = new Date(moment(moment().startOf('month')).subtract(1,'hr'));

                        _.each(response.Data,function (op) {
                            if(op.stageName == "Close Won" && (new Date(op.closeDate)<= endOfLastMonth)){
                                $scope.won.amount = $scope.won.amount+op.amount;
                                $scope.won.count++;
                            } else if(op.stageName == "Close Lost" && (new Date(op.closeDate)<= endOfLastMonth)){
                                $scope.lost.amount = $scope.lost.amount+op.amount;
                                $scope.lost.count++;
                            } else if(op.stageName == $rootScope.commitStage){
                                $scope.idle.amount = $scope.idle.amount+op.amount;
                                $scope.idle.count++;
                            } else {
                                $scope.pipeline.amount = $scope.pipeline.amount+op.amount;
                                $scope.pipeline.count++;
                            }
                        });
                    }

                    if($scope.won.amount>=$scope.selfCommit){
                        $scope.commitMet = "met"
                    }

                    $scope.won.percentage = (($scope.won.amount/$scope.initialCommitPipeline.amount)*100);
                    $scope.lost.percentage = (($scope.lost.amount/$scope.initialCommitPipeline.amount)*100);
                    $scope.idle.percentage = (($scope.idle.amount/$scope.initialCommitPipeline.amount)*100);
                    $scope.pipeline.percentage = (($scope.pipeline.amount/$scope.initialCommitPipeline.amount)*100);

                    $scope.won.percentage = $scope.won.percentage != 0?$scope.won.percentage.toFixed(2)+"%":$scope.won.percentage+"%"
                    $scope.lost.percentage = $scope.lost.percentage != 0?$scope.lost.percentage.toFixed(2)+"%":$scope.lost.percentage+"%"
                    $scope.idle.percentage = $scope.idle.percentage != 0?$scope.idle.percentage.toFixed(2)+"%":$scope.idle.percentage+"%"
                    $scope.pipeline.percentage = $scope.pipeline.percentage != 0?$scope.pipeline.percentage.toFixed(2)+"%":$scope.pipeline.percentage+"%";

                    $scope.initialCommitPipeline.amount = getAmountInThousands($scope.initialCommitPipeline.amount,2,share.primaryCurrency=="INR")
                    $scope.won.amount = getAmountInThousands($scope.won.amount,2,share.primaryCurrency=="INR")
                    $scope.lost.amount = getAmountInThousands($scope.lost.amount,2,share.primaryCurrency=="INR")
                    $scope.idle.amount = getAmountInThousands($scope.idle.amount,2,share.primaryCurrency=="INR")
                    $scope.pipeline.amount = getAmountInThousands($scope.pipeline.amount,2,share.primaryCurrency=="INR")
                    $scope.selfCommit = getAmountInThousands($scope.selfCommit,2,share.primaryCurrency=="INR");

                    $scope.commitMvMtLoading = false;

                });
        } else {
            $scope.commitMvMtLoading = false;
        }
    }
}

function getContactsFromOpp(o) {
    var contacts = [o.contactEmailId];

    if(o.partners && o.partners.length>0) {
        _.each(o.partners,function (el) {
            if(el){
                contacts.push(el.emailId)
            }
        })
    }

    if(o.influencers && o.influencers.length>0) {
        _.each(o.influencers,function (el) {
            if(el){
                contacts.push(el.emailId)
            }
        })
    }

    if(o.decisionMakers && o.decisionMakers.length>0) {
        _.each(o.decisionMakers,function (el) {
            if(el){
                contacts.push(el.emailId)
            }
        })
    }

    return contacts;
}

function saveQuarterlyCommits($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){
        $http.post("/review/meeting/update/commit/value/quarterly",{commitValue:commits.selfCommitValue,committingForNext:$scope.committingForNext})
            .success(function (response) {

                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // quarterlyCommitHistory($scope,$rootScope,$http,share,share.liuData._id)
                } else {
                    toastr.error("Commits not updated. Please try again later")
                }
                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only positive numbers")
    }
}

function saveWeeklyCommits($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){

        $http.post("/review/meeting/update/commit/value/weekly",{week:commits.selfCommitValue,committingForNext:$scope.committingForNext})
            .success(function (response) {

                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // weeklyPastCommitHistory($scope,$rootScope,$http,share,share.liuData._id)
                } else {
                    toastr.error("Commits not updated. Please try again later")
                }
                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only positive numbers")
    }
}

function saveMonthlyCommit($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){
        $http.post("/review/meeting/update/commit/value/monthly",{commitValue:commits.selfCommitValue,committingForNext:$scope.committingForNext})
            .success(function (response) {
                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // monthlyCommitHistory($scope,$rootScope,$http,share)
                } else {
                    toastr.error("Commits not updated. Please try again later")
                }

                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only numbers for opportunity amount")
    }
}

function resetAllMemberSelection($scope){
    _.each($scope.teamList,function (tm) {
        tm.selected = "";
    })
}

function monthlyCommitHistory($scope,$rootScope,$http,share,userId){

    var url = '/review/monthly/commits/history';

    if(userId){
        url = fetchUrlWithParameter(url,"userId",userId)
    }

    if($scope.selectedHierarchy && $scope.selectedHierarchy.name){
        url = fetchUrlWithParameter(url,"hierarchyType",$scope.selectedHierarchy.name.replace(/[^A-Z0-9]+/ig, "_"))
    }

    $http.get(url)
        .success(function (response) {
            var monthNames = [];
            var commitColName = response.commitStage;
            $rootScope.commitStage = response.commitStage;

            commitColName = commitColName+" stage ($)"
            var relatasCommitCol = [commitColName],
                selfCommitCol = ['Self commit'],
                oppCreatedCol = ['Opp created (count)'],
                oppWonAmountCol = ['Opp won ($)'],
                oppWonCol = ['Opp won (count)'],
                targetCol = ['Target'],
                commitsMadeObj = {},
                monthNamesObj = {};

            $scope.oppsInCommitStage = response && response.oppsInCommitStage?response.oppsInCommitStage:[];

            if($scope.oppsInCommitStage.length>0){
                _.each($scope.oppsInCommitStage,function (op) {

                    op.closingThisWeek = {background:"inherit"};
                    op.riskMeter = response.dealsAtRisk && response.dealsAtRisk[op.opportunityId]?response.dealsAtRisk[op.opportunityId]*100:0

                    if(new Date(op.closeDate)>= new Date(moment().startOf("month")) && new Date(op.closeDate) <= new Date(moment().endOf("month"))){
                        op.closingThisWeek = {
                            background:"#f2f2f2"
                        }
                    }
                    op = formatOpp(op,share,$scope)
                })
            }

            window.localStorage['oppsInCommitStage'] = JSON.stringify($scope.oppsInCommitStage);

            response.commits.sort(function (o1, o2) {
                return new Date(o1.commitForDate) > new Date(o2.commitForDate) ? 1 : new Date(o1.commitForDate) < new Date(o2.commitForDate) ? -1 : 0;
            });

            response.oppsConversion.created.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            response.oppsConversion.oppsWon.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            response.targets.sort(function (o1, o2) {
                return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
            });

            var pastMonthCommits = null,
                pastMonthYear = moment().subtract(1, 'month').month()+""+moment().subtract(1, 'month').year();

            _.each(response.commits,function (el,index) {
                if(el.monthYear == pastMonthYear){
                    pastMonthCommits = el;
                }
                monthNames.push(moment(el.date).format("MMM 'YY"));
                monthNamesObj[index] = el.date;
                commitsMadeObj[index] = el;
                selfCommitCol.push(el.month.userCommitAmount)
                relatasCommitCol.push(el.month.relatasCommitAmount)
            });

            _.each(response.oppsConversion.created,function (el) {
                oppCreatedCol.push(el.count)
            });

            _.each(response.oppsConversion.oppsWon,function (el) {
                oppWonAmountCol.push(el.amount?parseFloat(el.amount.r_formatNumber(2)):0)
                oppWonCol.push(el.count)
            });

            _.each(response.targets,function (tr,index) {
                targetCol.push(tr.target)
                if(new Date(moment(tr.date).endOf("month"))<=new Date(moment().endOf("month"))){
                } else {
                }
            })

            setTimeOutCallback(100,function () {
                $scope.graphLoading = false;
            })

            setTimeOutCallback(100,function () {
                $scope.commitMovement(pastMonthCommits)
            })

            var colors = {
                "Target": "#FE9E83",
                "Self commit": '#638ca6',
                "Opp created (count)": '#3498db',
                'Opp won (count)': '#8ECECB',
                'Opp won ($)':oppStageStyle("Close Won",$scope.stages.indexOf($rootScope.commitStage),false,false,true)
            }

            colors[commitColName] = oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)

            var axes = {
            }

            axes[commitColName] = 'y2'
            axes["Opp won ($)"] = 'y2'
            axes["Self commit"] = 'y2'
            axes["Target"] = 'y2';

            $scope.commitHistoryExists = checkCommitHistoryExists(oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol);

            var chart = c3.generate({
                bindto: ".pipeline-comparison",
                data: {
                    columns: [oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol,targetCol],
                    axes: axes,
                    type: 'bar',
                    types: {
                        'Opp created (count)': 'spline',
                        "Opp won (count)": 'spline'
                    },
                    colors:colors,
                    bar: {
                        width: {
                            ratio: 0.5
                        }
                    },
                    onclick: function(e) {
                        $scope.$apply(function () {
                            closeAllOppTables($scope);
                            $rootScope.commitStage = share.commitStage;
                            // displayOppsMonthly(e,$scope,share,response,commitsMadeObj)
                        });
                    }
                },
                axis: {
                    y2: {
                        label: {
                            text:'Opp amount',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    y: {
                        label: {
                            text:'No. of Opps',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    x : {
                        tick: {
                            fit: true,
                            format: function (x) { return monthNames[x];}
                        }
                    }
                },
                tooltip: {
                    format: {
                        title: function (d) {

                            if(commitsMadeObj[d]){
                                var startDate = commitsMadeObj[d].commitForDate
                                var dateRange = moment(startDate).format('MMM YY')
                                return dateRange;
                            }
                        }
                    }
                }
            });

            $scope.closing = {
                this:"range"
            }

            setTimeOutCallback(100,function () {
                $scope.actionloadingData = false;
            });
            $scope.filterOpps('range');
        })
}

function checkCommitHistoryExists(oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol) {
    var commitHistoryExists = false;
    _.each(oppWonCol,function (el) {
        if(!commitHistoryExists && isNumber(el) && el>0){
            commitHistoryExists = true;
        }
    })

    if(!commitHistoryExists){
        _.each(oppCreatedCol,function (el) {
            if(!commitHistoryExists && isNumber(el) && el>0){
                commitHistoryExists = true;
            }
        })
    }

    if(!commitHistoryExists){
        _.each(selfCommitCol,function (el) {
            if(!commitHistoryExists && isNumber(el) && el>0){
                commitHistoryExists = true;
            }
        })
    }
    if(!commitHistoryExists){
        _.each(oppWonAmountCol,function (el) {
            if(!commitHistoryExists && isNumber(el) && el>0){
                commitHistoryExists = true;
            }
        })
    }

    return commitHistoryExists;

}

function pipelineComparison($scope,$rootScope,$http,share,response){

    var weekNames = [];
    var commitColName = $rootScope.commitStage?$rootScope.commitStage:response.Data.commitStage;
    commitColName = commitColName+" stage ($)"
    var relatasCommitCol = [commitColName],
        selfCommitCol = ['Self commit ($)'],
        oppCreatedCol = ['Opp created (count)'],
        oppWonAmountCol = ['Opp won ($)'],
        oppWonCol = ['Opp won (count)']

    formatCommitData(relatasCommitCol,selfCommitCol,response,share)
    formatConversionData(oppCreatedCol,oppWonCol,oppWonAmountCol,response,weekNames,share)

    var relatasCommitColLength = relatasCommitCol.length,
        prefilledDataRelatasCommit = []

    _.each(relatasCommitCol,function (re,index) {
        if(index == relatasCommitColLength-1 && response.Data.oppsInStageClosingThisWeek && response.Data.oppsInStageClosingThisWeek.length>0){
            prefilledDataRelatasCommit.push(_.sumBy(response.Data.oppsInStageClosingThisWeek,"amount"))
        } else {
            prefilledDataRelatasCommit.push(re)
        }
    })

    var colors = {
        "Self commit ($)": '#638ca6',
        "Opp created (count)": '#3498db',
        'Opp won (count)': '#8ECECB',
        'Opp won ($)':oppStageStyle("Close Won",$scope.stages.indexOf($rootScope.commitStage),false,false,true)
    }

    colors[commitColName] = oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)

    var axes = {
    }

    axes[commitColName] = 'y2'
    axes["Opp won ($)"] = 'y2'
    axes["Self commit ($)"] = 'y2'

    $scope.commitHistoryExists = checkCommitHistoryExists(oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol);

    var chart = c3.generate({
        bindto: ".pipeline-comparison",
        data: {
            columns: [oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol],
            axes: axes,
            type: 'bar',
            types: {
                'Opp created (count)': 'spline',
                "Opp won (count)": 'spline'
            },
            colors:colors,
            bar: {
                width: {
                    ratio: 0.5
                }
            },
            onclick: function(e) {
                $scope.$apply(function () {

                });
            }
        },
        axis: {
            y2: {
                label: {
                    text:'Opp amount',
                    position: 'outer-center'
                },
                show: true
            },
            y: {
                label: {
                    text:'No. of Opps',
                    position: 'outer-center'
                },
                show: true
            },
            x : {
                tick: {
                    fit: true,
                    format: function (x) { return weekNames[x];}
                }
            }
        },
        tooltip: {
            format: {
                title: function (d) {

                    if(share.weekByIndexNumberForCommits[d]){
                        var date = moment(moment(buildDateObj(share.weekByIndexNumberForCommits[d].weekYear))).add(1,"d");
                        date = moment(date).startOf("isoWeek");
                        var endDate = moment(date).endOf("isoWeek")
                        return moment(date).format('DD MMM')+"-"+moment(endDate).format('DD MMM');
                    }
                }
            }
        }
    });
}

function formatCommitData(relatasCommitCol,selfCommitCol,response,share){

    var rangeType = 'week';
    share.oppsCommitedByWeek = {}
    share.weekByIndexNumberForCommits = {}

    if(response.Data && response.Data.commits){
        response.Data.commits.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(response.Data.commits,function (el,index) {

            share.weekByIndexNumberForCommits[index] = {
                weekYear:el.commitWeekYear,
                noCommits:el.noCommits,
                date:el.date
            };
            share.oppsCommitedByWeek[el.commitWeekYear] = el.opportunities;

            if(el[rangeType] && el[rangeType].relatasCommitAmount){
                relatasCommitCol.push(el[rangeType].relatasCommitAmount)
            } else {
                relatasCommitCol.push(0)
            }

            if(el[rangeType] && el[rangeType].userCommitAmount){
                selfCommitCol.push(el[rangeType].userCommitAmount)
            } else {
                selfCommitCol.push(0)
            }

        })
    }
}

function formatConversionData(oppCreatedCol,oppWonCol,oppWonAmountCol,response,weekNames,share){
    if(response.Data && response.Data.oppsConversion){

        response.Data.oppsConversion.created.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        response.Data.oppsConversion.oppsWon.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(response.Data.oppsConversion.oppsWon,function (el) {
            oppWonCol.push(el.count)
            oppWonAmountCol.push(el.amount?parseFloat(el.amount.r_formatNumber(2)):0)
        })

        _.each(response.Data.oppsConversion.created,function (el) {
            var date = moment(moment(buildDateObj(el.weekYear))).add(1,"d");
            date = moment(date).startOf("isoWeek");
            var endDate = moment(date).endOf("isoWeek");
            weekNames.push(moment(date).format('DD MMM')+"-"+moment(endDate).format('DD MMM'))
            oppCreatedCol.push(el.count)
        })
    }
}

function displayOppsMonthly(event,$scope,share,response,commitsMadeObj){

    $scope.opps = [];
    $scope.currentWeek = false;
    $scope.weekByWeek = true;
    $scope.commitsByUsers = [];
    $scope.loadOppsForWeek = true;
    $scope.weekByWeek = true;
    $scope.loadingOpps = true;

    var opps = commitsMadeObj[event.index].opportunities

    if(opps && opps.length>0){
        _.each(opps,function (op) {

            op.ngmReq = response.netGrossMarginReq;
            op.amountWithNgm = op.amountNonNGM?op.amountNonNGM:op.amount;

            if(response.netGrossMarginReq){
                op.amountWithNgm = (op.amountWithNgm*op.netGrossMargin)/100
            }

            op.closingThisWeek = {};

            if(new Date(op.closeDate)>= (new Date(moment(commitsMadeObj[event.index].commitForDate))) && new Date(op.closeDate) <= (new Date(commitsMadeObj[event.index].commitForDate))){
                op.closingThisWeek = {
                    background:"#f2f2f2"
                }
            }

            if(response.commitStage == op.stageName){
                op.amountWithCommas = getAmountInThousands(op.amountWithNgm,2);

                var age = moment(op.closeDate).diff(moment(op.createdDate), 'day');

                if(op.relatasStage != "Close Won" && op.relatasStage != "Close Lost"){
                    age = moment().diff(moment(op.createdDate), 'day');
                }

                op.age = age;
                op.closeDateSort = moment(op.closeDate).unix()
                op.closeDate = moment(op.closeDate).format(standardDateFormat());

                op.account = fetchCompanyFromEmail(op.contactEmailId)

                op.opportunityNameTruncated = getTextLength(op.opportunityName,20)

                $scope.opps.push(op)
            }
        })
    }
}

function buildTeamProfilesWithCommits(data,listOfMembers,share,$scope,range) {

    var childrenForTeamMember = {}

    if(listOfMembers && listOfMembers.length>0){
        _.each(listOfMembers,function (el) {
            childrenForTeamMember[el.userEmailId] = el;
        })
    }

    var team = [];

    _.each(data,function (el) {
        var children = {};

        if(childrenForTeamMember[el.emailId]){
            children = childrenForTeamMember[el.emailId];
            children.teamMatesEmailId.push(el.emailId)
            children.teamMatesUserId.push(el._id)
        } else {
            children = {
                userEmailId:el.emailId,
                teamMatesEmailId:[el.emailId],
                teamMatesUserId:[el._id]
            }
        }

        var commit = 0,
            difference = 0,
            target = 0;

        if(range && range == 'week'){

            if(el.commit && el.commit.week && el.commit.week.userCommitAmount){
                commit = el.commit.week.userCommitAmount
            }
        } else if(range && range == 'quarter'){

            if(el.commit && el.commit.quarter && el.commit.quarter.userCommitAmount){
                commit = el.commit.quarter.userCommitAmount
            }
        } else {

            if(el.commit && el.commit.month && el.commit.month.userCommitAmount){
                commit = el.commit.month.userCommitAmount
            }
        }

        if(el.targetAndAchievement && el.targetAndAchievement.target){
            target = el.targetAndAchievement.target
            el.targetAndAchievement.targetOriginal = el.targetAndAchievement.target;
            el.targetAndAchievement.target= getAmountInThousands(el.targetAndAchievement.target,2,share.primaryCurrency=="INR");
        }

        difference = target - commit;

        if(!el.targetAndAchievement){
            el.targetAndAchievement = {
                "userEmailId": el.emailId,
                "userId": el.userId,
                "pipeline": 0,
                "lost": 0,
                "won": 0,
                "commitPipeline": 0,
                "probability": 0,
                "target": 0
            }
        };

        team.push({
            fullName:el.firstName+' '+el.lastName,
            // image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            children:children,
            targetAndAchievement:el.targetAndAchievement,
            commit:getAmountInThousands(commit,2,share.primaryCurrency=="INR"),
            commitSort:commit,
            difference:getAmountInThousands(difference,2,share.primaryCurrency=="INR"),
            differenceSort:difference,
            targetSort:target,
            noPicFlag:true,
            nameNoImg:el.firstName?el.firstName.substr(0,2).toUpperCase():el.ownerEmailId.substr(0,2).toUpperCase()
        })

    });

    return team;
}

function weeklyPastCommitHistory($scope,$rootScope,$http,share,userId){

    var url = "/review/past/commits";

    if(userId){
        url = fetchUrlWithParameter(url,"userId",userId)
    }

    $http.get(url)
        .success(function (response) {
            if(response.SuccessCode){
                $rootScope.fySelection = "FY "+moment(response.Data.fyRange.start).year()+"-"+moment(response.Data.fyRange.end).year()
            }

            $scope.oppsInCommitStage = response.Data && response.Data.oppsInCommitStage?response.Data.oppsInCommitStage:[];

            if($scope.oppsInCommitStage.length>0){
                _.each($scope.oppsInCommitStage,function (op) {

                    op.closingThisWeek = {background:"inherit"};

                    if(new Date(op.closeDate)>= new Date(moment().startOf("isoweek")) && new Date(op.closeDate) <= new Date(moment().endOf("isoweek"))){
                        op.closingThisWeek = {
                            background:"#f2f2f2"
                        }
                    }

                    op = formatOpp(op,share,$scope);
                })
            }

            window.localStorage['oppsInCommitStage'] = JSON.stringify($scope.oppsInCommitStage);
            $scope.closing = {
                this:"range"
            }

            $scope.filterOpps('range');
            setTimeOutCallback(100,function () {
                $scope.actionloadingData = false;
            })
            pipelineComparison($scope,$rootScope,$http,share,response)
        });
}

function quarterlyCommitHistory($scope,$rootScope,$http,share,userId){

    var url = '/review/quarterly/commits/history';

    if(userId){
        url = fetchUrlWithParameter(url,"userId",userId)
    }

    $http.get(url)
        .success(function (response) {

            var qtrNames = [];
            var commitColName = $rootScope.commitStage?$rootScope.commitStage:response.commitStage;
            commitColName = commitColName+" stage ($)"
            var relatasCommitCol = [commitColName],
                selfCommitCol = ['Self commit'],
                oppCreatedCol = ['Opp created (count)'],
                oppWonAmountCol = ['Opp won ($)'],
                oppWonCol = ['Opp won (count)'],
                targetCol = ['Target'],
                commitsMadeObj = {}

            $scope.oppsInCommitStage = response && response.oppsInCommitStage?response.oppsInCommitStage:[];

            if($scope.oppsInCommitStage.length>0){
                _.each($scope.oppsInCommitStage,function (op) {

                    op.riskMeter = response && response.dealsAtRisk && response.dealsAtRisk[op.opportunityId]?response.dealsAtRisk[op.opportunityId]*100:0;

                    op.closingThisWeek = {background:"inherit"};

                    if(new Date(op.closeDate)>= new Date(response.qtrStart) && new Date(op.closeDate) <= new Date(response.qtrEnd)){
                        op.closingThisWeek = {
                            background:"#f2f2f2"
                        }
                    }
                    op = formatOpp(op,share,$scope)
                })
            }

            window.localStorage['oppsInCommitStage'] = JSON.stringify($scope.oppsInCommitStage);

            response.commits.sort(function (o1, o2) {
                return new Date(o1.commitForDate) > new Date(o2.commitForDate) ? 1 : new Date(o1.commitForDate) < new Date(o2.commitForDate) ? -1 : 0;
            });

            response.oppsConversion.created.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            response.oppsConversion.oppsWon.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            _.each(response.commits,function (el,index) {
                qtrNames.push(moment(el.date).format("MMM YY") +" - "+moment(moment(el.date).add(2,'month')).format("MMM YY"))
                commitsMadeObj[index] = el;
                selfCommitCol.push(el.quarter.userCommitAmount)
                relatasCommitCol.push(el.quarter.relatasCommitAmount)
                targetCol.push(el.target)
            });

            _.each(response.oppsConversion.created,function (el) {
                oppCreatedCol.push(el.count)
            });

            _.each(response.oppsConversion.oppsWon,function (el) {
                oppWonAmountCol.push(el.amount?parseFloat(el.amount.r_formatNumber(2)):0)
                oppWonCol.push(el.count)
            });

            setTimeOutCallback(100,function () {
                $scope.graphLoading = false;
            })

            setTimeOutCallback(100,function () {
                $scope.commitMovement(response.commits.slice(-2)[0])
            })

            var colors = {
                "Target": "#FE9E83",
                "Self commit": '#638ca6',
                "Opp created (count)": '#3498db',
                'Opp won (count)': '#8ECECB',
                'Opp won ($)':oppStageStyle("Close Won",$scope.stages.indexOf($rootScope.commitStage),false,false,true)
            }

            colors[commitColName] = oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)

            var axes = {
            }

            axes[commitColName] = 'y2'
            axes["Opp won ($)"] = 'y2'
            axes["Self commit"] = 'y2'
            axes["Target"] = 'y2'

            $scope.commitHistoryExists = checkCommitHistoryExists(oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol);

            var chart = c3.generate({
                bindto: ".pipeline-comparison",
                data: {
                    columns: [oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol,targetCol],
                    axes: axes,
                    type: 'bar',
                    types: {
                        'Opp created (count)': 'spline',
                        "Opp won (count)": 'spline'
                    },
                    colors:colors,
                    bar: {
                        width: {
                            ratio: 0.5
                        }
                    },
                    onclick: function(e) {
                        $scope.$apply(function () {
                            closeAllOppTables($scope);
                            $rootScope.commitStage = share.commitStage;
                            displayOppsQuarterly(e,$scope,share,response,commitsMadeObj)
                        });
                    }
                },
                axis: {
                    y2: {
                        label: {
                            text:'Opp amount',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    y: {
                        label: {
                            text:'No. of Opps',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    x : {
                        tick: {
                            fit: true,
                            format: function (x) { return qtrNames[x];}
                        }
                    }
                },
                tooltip: {
                    format: {
                        title: function (d) {
                            if(commitsMadeObj[d]){
                                var startDate = commitsMadeObj[d].commitForDate
                                var dateRange = moment(startDate).format('MMM')+"-"+moment(moment(startDate).add(1,"month")).format('MMM')+"-"+moment(moment(startDate).add(2,"month")).format('MMM YY')
                                return dateRange;
                            }
                        }
                    }
                }
            });
            $scope.closing = {
                this:"range"
            }

            setTimeOutCallback(100,function () {
                $scope.actionloadingData = false;
            })
            $scope.filterOpps('range');
        })
}

function formatOpp(op,share,$scope){

    op.isNotOwnerClass = '';

    if($scope){
        if(op.userEmailId !== $scope.owner.emailId){
            op.isNotOwnerClass = "background-color: #6EC7D5 !important"
        }
    }

    op.amount = parseFloat(op.amount);
    op.amountWithNgm = op.amount;

    if(!op.amountNonNGM && (op.netGrossMargin || op.netGrossMargin === 0)){
        op.amountWithNgm = parseFloat(((op.amount*op.netGrossMargin)/100).toFixed(2))
    }

    op.stage = op.stageName;

    op.topLine = getAmountInThousands(op.amount,2,share.primaryCurrency=="INR");
    if(op.amountNonNGM){
        op.topLine = getAmountInThousands(op.amountNonNGM,2,share.primaryCurrency=="INR");
    }
    op.amountWithCommas = getAmountInThousands(op.amountWithNgm,2,share.primaryCurrency=="INR");
    op.age = moment().diff(moment(op.createdDate), 'day');
    if(op.createdDate){
        op.createdDateFormatted = moment(op.createdDate).format(standardDateFormat());
    }

    if(_.includes(["Close Lost","Close Won"], op.stage)){
        op.daysToClosure = moment(op.closeDate).diff(moment(op.createdDate), 'day')
        op.daysClosureStyle = {color: '#4eb3a6'}
    } else {

        op.daysToClosure = moment(op.closeDate).diff(moment(), 'day')

        op.daysClosureStyle = {color: '#4eb3a6'}
        if(op.daysToClosure<0){
            op.daysClosureStyle = {color: '#cc4527'}
        }
    }

    op.closeDateSort = moment(op.closeDate).unix()
    // op.closeDate = moment(op.closeDate).format(standardDateFormat());
    op.closeDateFormatted = moment(op.closeDate).format(standardDateFormat())
    op.lastInteracted = moment(op.closeDate).format(standardDateFormat())

    if(op.geoLocation && op.geoLocation.zone){
        op.region = op.geoLocation.zone;
    }
    op.account = fetchCompanyFromEmail(op.contactEmailId)
    op.member = share.usersDictionary[op.userEmailId];

    op.opportunityNameTruncated = getTextLength(op.opportunityName,20);

    op.riskClass = "green";
    op.riskMeter = op.riskMeter?parseFloat(op.riskMeter.toFixed(2)):"";

    if(parseFloat(op.riskMeter)>50){
        op.riskClass = "red"
    }
    
    op.isStaleClass = new Date(moment(op.closeDate).endOf('day'))<new Date()?"red-color":"grey-color";
    op.isNotOwner = false;
    op.canTransferOpp = false;
    if(op.userEmailId === share.liuData.emailId || share.liuData.corporateAdmin){
        op.isNotOwner = false;
        op.canTransferOpp = true;
    }

    if(op.fullName){
        op.searchContent = op.fullName + " ("+op.contactEmailId+")";
    } else {
        op.searchContent = op.contactEmailId;
    }

    op.tasks = "Assign"

    return op;
}

function fetchCommit($scope,$http,share,$rootScope,users,url,mode,startDate) {

    if(users){
        if(!$scope.teamCommitsViewing) {
            url = fetchUrlWithParameter(url,"userId",users);
        } else {
            url = fetchUrlWithParameter(url,"hierarchylist",users)
        }
    }

    url = fetchUrlWithParameter(url,"userId",$scope.owner.userId);

    if(!mode){
        mode = "month"
    }

    $http.get(url)
        .success(function (response) {

            $scope.commits = {
                oppValue:0,
                selfCommitValue:0,
                opps:[],
                selfCommitValueFormat:0,
                response:null,
                graph:{}
            }

            if(response){

                $rootScope.commitStage = response.commitStage
                share.commitStage = response.commitStage;

                var editAccess = false;
                var selfCommitValue = 0;
                var commitCutOffDate = null;
                var nextCommitCutOffDate = null;

                checkCommitCutOffLoaded();
                function checkCommitCutOffLoaded(){
                    if(share.commitCutOffObj){

                        if(mode == "week"){
                            commitCutOffDate = new Date(share.commitCutOffObj.week);
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"week"));
                        } else if(mode == "quarter"){
                            commitCutOffDate = new Date(share.commitCutOffObj.quarter)
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"quarter"));
                        } else {
                            commitCutOffDate = new Date(share.commitCutOffObj.month);
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"month"));
                        }
                    } else {
                        setTimeOutCallback(500,function () {
                            checkCommitCutOffLoaded();
                        })
                    }
                }

                $scope.canCommitForNext = true;

                if(commitCutOffDate && new Date(commitCutOffDate) >= new Date()){
                    editAccess = true;
                    // $scope.canCommitForNext = false;
                } else {
                    // $scope.canCommitForNext = true;
                    $scope.nextCommitCutOffDate = nextCommitCutOffDate;
                }

                var oppsToDisplay = response.opps;

                if(mode == "week"){
                    selfCommitValue = response.commitsCurrentWeek.week.userCommitAmount?response.commitsCurrentWeek.week.userCommitAmount:0
                    if(new Date()> new Date(response.commitWeek)){
                        oppsToDisplay = response.commitsCurrentWeek.opportunities
                    }

                } else if(mode == "quarter"){
                    if(response.commitCurrentQuarter.quarter.userCommitAmount){
                        selfCommitValue = response.commitCurrentQuarter.quarter.userCommitAmount;
                    }
                    if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                        oppsToDisplay = response.commitCurrentQuarter.opportunities
                    }

                } else {
                    if(response.commitCurrentMonth.month.userCommitAmount){
                        selfCommitValue = response.commitCurrentMonth.month.userCommitAmount;
                    }

                    if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                        oppsToDisplay = response.commitCurrentMonth.opportunities
                    }
                }

                var oppValue = getOppValInCommitStageForSelectedMonth(oppsToDisplay,response.commitMonth,share.primaryCurrency,share.currenciesObj);

                $scope.commits = {
                    oppValue:oppValue?getAmountInThousands(oppValue,2,share.primaryCurrency=="INR"):0,
                    editAccess:editAccess,
                    selfCommitValue:selfCommitValue,
                    opps:oppsToDisplay,
                    selfCommitValueFormat:getAmountInThousands(selfCommitValue,2,share.primaryCurrency=="INR"),
                    response:response
                }

                if(mode != 'week'){
                    getTargetsAndAchievements($http,users,startDate,null,function (insights) {
                        if(insights){
                            $scope.commits.graph = setGraphValues(insights,response,share,mode,oppValue,$scope);
                        }
                    });
                }

                if(!$scope.commitModalOpen && $scope.teamCommitsViewing){
                    teamCommitsList($scope,share,response,mode,users,$http)
                }
            }

            setTimeOutCallback(100,function () {
                $scope.actionloadingData = false;
                $scope.loadingData = false;
            })
        });
}

function teamCommitsList($scope,share,data,mode,userIds,$http) {

    var insightsUrl = "/insights/current/fy";

    if(data.sh_users && data.sh_users.length>0){
        userIds = data.sh_users;
    }

    if(userIds){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"userIds",userIds)
    }

    $scope.sortTypeTeam = "fullName"
    $scope.sortReverseTeam = false;

    $http.get(insightsUrl)
        .success(function (insightsResponse) {

            $scope.commitsByUsers = [];
            $scope.closingThisSelection = {num:0,formatted:0};

            var targetsObj = {};

            if(insightsResponse && insightsResponse.Data && insightsResponse.Data.length>0){
                _.each(insightsResponse.Data,function (el) {
                    targetsObj[el._id] = el;
                })
            }

            var startDate = moment().startOf("month"),
                end = moment().endOf("month");

            var oppList = data.opps;
            $scope.oppsInCommitStage = [];
            var opportunitiesObj = {};
            var opportunitiesClosingThisSelectionObj = {};

            if(oppList && oppList.length>0){
                _.each(oppList,function (op) {
                    op.closingThisWeek = {background:"inherit"};

                    op.riskMeter = data && data.dealsAtRisk && data.dealsAtRisk[op.opportunityId]?data.dealsAtRisk[op.opportunityId]*100:0;

                    if(mode == "week"){
                        var weekStartDateTime = moment().startOf("isoWeek");
                        var weekEndDateTime = moment().endOf("isoWeek");
                        if(new Date(op.closeDate)>= new Date(weekStartDateTime) && new Date(op.closeDate) <= new Date(weekEndDateTime)){
                            op.closingThisWeek = {
                                background:"#f2f2f2"
                            }
                        }

                    } else if(mode == "quarter"){

                        if(new Date(op.closeDate)>= new Date(data.qStart) && new Date(op.closeDate) <= new Date(data.qEnd)){
                            op.closingThisWeek = {
                                background:"#f2f2f2"
                            }
                        }
                    } else {
                        if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate) <= new Date(end)){
                            op.closingThisWeek = {
                                background:"#f2f2f2"
                            }
                        }
                    }

                    if(opportunitiesObj[op.userEmailId]){
                        opportunitiesObj[op.userEmailId].push(op)
                    } else {
                        opportunitiesObj[op.userEmailId] = [];
                        opportunitiesObj[op.userEmailId].push(op)
                    }

                    var opp_f = formatOpp(op,share,$scope);

                    if(opp_f.closingThisWeek.background === "#f2f2f2"){
                        if(!opportunitiesClosingThisSelectionObj[opp_f.userEmailId]){
                            opportunitiesClosingThisSelectionObj[opp_f.userEmailId] = op.amountWithNgm
                        } else {
                            opportunitiesClosingThisSelectionObj[opp_f.userEmailId] = opportunitiesClosingThisSelectionObj[opp_f.userEmailId]+op.amountWithNgm;
                        }
                    }
                    $scope.oppsInCommitStage.push(opp_f);
                })
            }

            window.localStorage['oppsInCommitStage'] = JSON.stringify($scope.oppsInCommitStage);

            share.opportunitiesObj = opportunitiesObj;

            if(data.commitsCurrentRaw && data.commitsCurrentRaw.length>0) {

                _.each(data.commitsCurrentRaw, function (el) {

                    el.profile = share.usersDictionary[el.userEmailId];

                    if(mode == "week"){
                        el.value = el.week.userCommitAmount
                    } else if(mode == "quarter"){
                        el.value = el.quarter.userCommitAmount
                    } else {
                        el.value = el.month.userCommitAmount
                    }
                    if (isNumber(parseFloat(el.value))) {
                        el.valueSort = parseFloat(el.value)
                    }
                    el.value = getAmountInThousands(el.value, 2)

                    el.emailId = el.profile ? el.profile.emailId : el.userEmailId;
                    el.fullName = el.profile ? el.profile.fullName : el.userEmailId;
                    el.userId = el.profile ? el.profile.userId : el.userId;
                    el.commitDateTime = moment(el.date).format(standardDateFormat());
                    el.oppsValueUnderCommitStage = 0;
                    el.oppsValueUnderCommitStageSort = 0;
                    $scope.commitsByUsers.push(el)
                });

            }

            var selectedUsers = _.map(userIds,function (el) {
                return share.teamDictionaryByUserId[el];
            })
            var nonExistingUsers = _.differenceBy(_.map(selectedUsers,"emailId"),_.map($scope.commitsByUsers,"emailId"));

            if(nonExistingUsers && nonExistingUsers.length>0){
                _.each(nonExistingUsers,function (el) {
                    $scope.commitsByUsers.push({
                        value:0,
                        valueSort:0,
                        profile:share.usersDictionary[el],
                        commitDateTime:"-",
                        oppsValueUnderCommitStage:0,
                        oppsValueUnderCommitStageSort:0
                    })
                })
            }

            $scope.commitValueSelected = 0;
            $scope.oppValueSelected = 0;
            $scope.target = 0;
            $scope.achievement = 0;

            _.each($scope.commitsByUsers,function (el) {

                el.fullName = el.profile ? el.profile.fullName : el.userEmailId;
                if(el.profile && targetsObj[el.profile.userId]){
                    el.fyTarget = getAmountInThousands(targetsObj[el.profile.userId].target,2,share.primaryCurrency=="INR")
                    el.fyAchivement = getAmountInThousands(targetsObj[el.profile.userId].won,2,share.primaryCurrency=="INR")
                    el.won =targetsObj[el.profile.userId].won;
                    el.target =targetsObj[el.profile.userId].target;
                }

                el.wonPercentage = 0+'%';

                if(el.won && el.target){
                    el.wonPercentage = Math.round(scaleBetween(el.won,0,el.target)).r_formatNumber(2)+'%';
                }

                if(el.won && !el.target){
                    el.wonPercentage = '100%'
                }


                if(el.target<120000){
                    el.commitMet = "red"
                }

                el.wonPercentageStyle = {'width':el.wonPercentage,background: '#8ECECB',height:"inherit"}

                el.oppsValueUnderCommitStage = 0;
                el.oppsValueUnderCommitStageSort = 0
                el.oppsValClosingThisSelection = 0

                var oppsListForUser = [];
                if(el.profile && opportunitiesObj[el.profile.emailId]){
                    oppsListForUser = opportunitiesObj[el.profile.emailId]
                }

                if(oppsListForUser.length>0){
                    _.each(oppsListForUser,function (op) {
                        if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate) <= new Date(end)){
                            if(op.amount && isNumber(el.oppsValueUnderCommitStageSort)){
                                el.oppsValueUnderCommitStageSort = el.oppsValueUnderCommitStageSort+op.amountWithNgm
                            }
                        }

                        el.oppsValClosingThisSelection = el.oppsValClosingThisSelection+op.amountWithNgm
                    })
                }

                el.selected = true;
                $scope.selectedUserValues(el);
                el.oppsValueUnderCommitStage = getAmountInThousands(el.oppsValueUnderCommitStageSort,2,share.primaryCurrency=="INR");
            })

            window.localStorage['commitsByUsers'] = JSON.stringify($scope.commitsByUsers);


            $scope.closing = {
                this:"range"
            }

            $scope.filterOpps('range');

        });
}

function getTargetsAndAchievements($http,userIds,startDate,viewMode,callback){
    var insightsUrl = "/insights/current/month/stats";

    if(userIds){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"userIds",userIds)
    }

    if(startDate){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"startDate",startDate)
    }

    if(viewMode){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"viewMode",viewMode)
    }

    $http.get(insightsUrl)
        .success(function (insightsResponse) {
            callback(insightsResponse)
        });
}

function setGraphValues(insightsData,commitData,share,mode,oppValue,$scope){

    var stages = [];
    var obj = {};

    obj.idForTooltip = 0
    obj.idForHover = 0

    obj.targetVal = 0
    obj.targetValNum = 0
    obj.achievementVal = 0
    obj.achievementValNum = 0
    obj.selfCommitVal = 0
    obj.relatasCommitVal = 0

    function getOppStages(){
        if(share.opportunityStages){
            stages = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(1000,function () {
                getOppStages()
            })
        }
    }

    getOppStages();

    if(mode == "month"){
        if(insightsData && insightsData.Data && insightsData.Data.month){
            _.each(insightsData.Data.month,function (el) {
                if(el.target){
                    obj.targetValNum = obj.targetValNum+el.target
                }
                if(el.won){
                    obj.achievementValNum = obj.achievementValNum+el.won
                }
            })
        }

        if(commitData){
            if(commitData.commitCurrentMonth.month.userCommitAmount){
                obj.selfCommitVal = commitData.commitCurrentMonth.month.userCommitAmount;
            }

            if(commitData.commitCurrentMonth.month.relatasCommitAmount){
                obj.relatasCommitVal = commitData.commitCurrentMonth.month.relatasCommitAmount;
            }
        }
    } else {

        if(insightsData && insightsData.Data && insightsData.Data.qtr){
            _.each(insightsData.Data.qtr,function (el) {
                if(el.target){
                    obj.targetValNum = obj.targetValNum+el.target
                }

                obj.achievementValNum = obj.achievementValNum+el.won
            })
        }

        if(commitData){
            if(commitData.commitCurrentQuarter.quarter.userCommitAmount){
                obj.selfCommitVal = commitData.commitCurrentQuarter.quarter.userCommitAmount;
            }

            if(commitData.commitCurrentQuarter.quarter.relatasCommitAmount){
                obj.selfCommitVal = commitData.commitCurrentQuarter.quarter.relatasCommitAmount;
            }
        }
    }

    var allValues = [obj.targetValNum,obj.achievementValNum,obj.selfCommitVal,obj.relatasCommitVal]
    var max = _.max(allValues)

    var mon_targetStyle = {background:"#f4ada7",width:scaleBetween(obj.targetValNum,0,max)+"%"},
        mon_achievementStyle = {background:"#8ECECB",width:scaleBetween(obj.achievementValNum,0,max)+"%"},
        mon_relatasCommitStyle = {background:oppStageStyle(commitData.commitStage,stages.indexOf(commitData.commitStage),false,false,true),width:scaleBetween(obj.selfCommitVal,0,max)+"%"},
        mon_selfCommitStyle = {background:"#648ca6",width:scaleBetween(obj.selfCommitVal,0,max)+"%"},
        commitPipelineStyle = {background:"#638ca6",width:scaleBetween(oppValue,0,max)+"%"}

    $scope.shortfall = 0;

    if(obj.targetValNum){
        $scope.shortfall = obj.targetValNum
    }

    if(obj.achievementValNum && obj.targetValNum){
        $scope.shortfall = obj.targetValNum-obj.achievementValNum;
    }

    $scope.shortfall = getAmountInThousands($scope.shortfall,2,share.primaryCurrency=="INR")
    
    obj.target = mon_targetStyle
    obj.commitPipeline = commitPipelineStyle
    obj.commitPipelineVal = getAmountInThousands(oppValue,2,share.primaryCurrency=="INR")
    obj.achievement = mon_achievementStyle
    obj.selfCommit = mon_relatasCommitStyle
    obj.relatasCommit = mon_selfCommitStyle
    obj.displayToolTip = false;

    obj.targetVal = getAmountInThousands(obj.targetValNum,2,share.primaryCurrency=="INR")
    obj.achievementVal = getAmountInThousands(obj.achievementValNum,2,share.primaryCurrency=="INR")

    return obj;
}

function getOppValInCommitStageForSelectedMonth(opps,commitDt,primaryCurrency,currenciesObj) {

    var oppVal = 0,
        startDate = moment(commitDt).startOf("month"),
        end = moment(commitDt).endOf("month");

    _.each(opps,function (op) {

        op.convertedAmt = op.amountNonNGM;
        op.convertedAmtWithNgm = op.amount

        if(op.currency && op.currency !== primaryCurrency){
            if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                op.convertedAmt = op.amountNonNGM/currenciesObj[op.currency].xr
            }

            if(op.netGrossMargin || op.netGrossMargin == 0){
                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
            } else {
                op.convertedAmtWithNgm = op.convertedAmt;
            }

            op.convertedAmt = parseFloat(op.convertedAmt)
        }

        if(new Date(op.closeDate)>=new Date(startDate) && new Date(op.closeDate)<=new Date(end)){
            oppVal = oppVal+op.convertedAmtWithNgm;
        }
    })

    return oppVal

}

function setDateRange(){

    if(share.companyDetails){
        var weekStartDateTime = moment().startOf("isoWeek");
        var weekEndDateTime = moment().endOf("isoWeek");

        var startOfWeek = moment(weekStartDateTime).format('DD MMM')
        var endOfWeek = moment(weekEndDateTime).format('DD MMM')

        if(new Date(weekStartDateTime)>new Date()){
            weekStartDateTime = moment(weekStartDateTime).subtract(1,"week")
            weekEndDateTime = moment(weekEndDateTime).subtract(1,"week")
            startOfWeek = moment(weekStartDateTime).format('DD MMM')
            endOfWeek = moment(weekEndDateTime).format('DD MMM')
        }

        $scope.currentSelection = {
            weekStartDateTime:new Date(weekStartDateTime),
            weekEndDateTime:new Date(weekEndDateTime)
        }

        $scope.dataForRange = startOfWeek+"-"+endOfWeek;
    } else {
        setTimeOutCallback(1000,function () {
            setDateRange()
        })
    }
}

function pickDatesForRenewal($scope,minDate,maxDate,id,addTime,timeType,timezone){

    $(id).datetimepicker({
        value:"",
        timepicker:false,
        validateOnBlur:false,
        minDate: new Date(moment().subtract(2,"week")),
        maxDate: maxDate,
        onSelectDate: function (dp, $input){
            $scope.$apply(function () {
                var timezone = timezone?timezone:"UTC";
                $scope.renewalCloseDate = moment(dp).format();
                $scope.renewalCloseDateFormatted = moment(dp).format(standardDateFormat());
                $("#opportunityCloseDateSelector4").val($scope.renewalCloseDateFormatted)

                $scope.opp.renewed = {
                    closeDate:moment($scope.renewalCloseDate).format(standardDateFormat()),
                    amount:$("#renewalAmount").val()
                }
            });
        }
    });
}

function setTabView($scope,viewFor){
    resetAllTabsOppInsights($scope);
    $scope[viewFor+'Selected'] = "selected"
}

function resetAllTabsOppInsights($scope){
    $scope.basicSelected = ""
    $scope.insightsSelected = ""
    $scope.internalTeamSelected = ""
    $scope.tasksSelected = ""
    $scope.notesSelected = ""
}

function getOppsFromLocalStorage(callback) {
    try {
        var oppsInCommitStage = JSON.parse(window.localStorage['oppsInCommitStage']);
        callback(oppsInCommitStage);
    } catch (e) {
        console.log("error", e);
        callback([]);
    }
}

function initServices($scope, $http,$rootScope,share,searchService,$sce){

    $scope.oppDetailsNav = oppDetails();
    $scope.selectedTab = $scope.oppDetailsNav[0];
    $scope.viewFor = $scope.oppDetailsNav[0].name.toLowerCase();
    $scope.reasonsRequired = true;

    $scope.updateTaskName = function (task,taskName) {
        if($scope.task){
            $scope.task.taskName = taskName
        }
    }

    $scope.updateTaskPriority = function (priority) {
        if($scope.task){
            $scope.task.priority = priority
        }
    }

    $scope.toggleContactPopup = function() {
        $scope.showContactCreateModal = false;
    }

    $scope.createContactWindow = function() {
        $scope.showContactCreateModal = true;
    }

    $scope.saveContact = function() {

        if(!$scope.coords){
            $scope.coords = {}
        }

        var data = {
            personName:$("#fullName").val(),
            personEmailId:$("#p_emailId").val(),
            companyName:$("#companyName").val(),
            designation:$("#designation").val(),
            mobileNumber:$("#p_mobileNumber").val(),
            location:$scope.coords.address,
            lat:$scope.coords.lat,
            lng:$scope.coords.lng
        }

        var errExists = false;

        if(!data.personName){
            errExists = true
            toastr.error("Please enter a name for the contact.")
        } else if(!validateEmail(data.personEmailId)){
            toastr.error("Please enter a valid email ID.")
            errExists = true
        }

        if(!checkRequired(data.location)){
            errExists = true
            toastr.error("Please enter a city for this contact.")
        }

        if(!errExists){
            $http.post("/contacts/create",data)
                .success(function (response) {
                    toastr.success("Contact added successfully.")
                    $scope.showContactCreateModal = false;
                })
        }
    }

    $scope.openViewFor = function (viewFor) {
        if(viewFor.closeOpp == 'de-active'){
            alert("Close Details available when the the opportunity stage is 'Closed'")
        } else if(viewFor.isNewOpp == 'de-active'){
            alert("Please SAVE the opportunity before you add the first NOTE for this opportunity")
        } else if(viewFor.isNewOpp == 'de-active'){
            alert("Please SAVE the opportunity before you add the first TASK for this opportunity")
        } else {
            $scope.viewFor = viewFor.name.toLowerCase();
            menuToggleSelection(viewFor.name,$scope.oppDetailsNav)

            if($scope.viewFor == 'documents') {
                getLinkedDocuments($scope, $http);
            }
        }
    }

    $scope.checkErrorMessageStatus = function () {
        for(var key in $scope.opp){
            if($scope.opp[key] && $scope.opp[key].length>0){
                $scope.opp[key+"Req"]= false;
            }
        }
    }

    $scope.goToBasic = function () {

        _.each($scope.oppDetailsNav,function (el) {
           if(el.name == "Basic"){
               el.selected = "selected"
           } else {
               el.selected = ""
           }
        });
        $scope.selectedTab = $scope.oppDetailsNav[0];
        $scope.viewFor = $scope.oppDetailsNav[0].name.toLowerCase();
    }

    initTaskObj($scope)
    setDocumentsTableHeader($scope);

    $scope.startDateFormatted = moment().format("DD MMM YYYY")
    $scope.dueDateFormatted = "Select"

    $scope.assignList = [];
    $scope.removeFromToList = function (contact) {
        $scope.toList = $scope.toList.filter(function (item) {
            return item.emailId != contact.emailId;
        })
    }

    $scope.closeNotesModule = function (){
        $scope.showNotesModule = false;
        $scope.note = "";
        clearInputFields();
    }

    $scope.closeTaskCreator = function () {
        $scope.createNewTask = false
        $rootScope.createFormIsOpen = false;
        $("div[id^='taTextElement']").empty();
    }

    $scope.selectThisAccount = function (account,group) {
        selectThisAccount($scope,$http,share,account,group)
    }

    $scope.addItems = function () {
    }

    $scope.searchForAccountName = function (account) {
        searchForAccountName($scope,$http,share,account)
    }

    $scope.removeFromAssigneeList = function (contact) {
        $scope.assignList = $scope.assignList.filter(function (item) {
            return item.emailId != contact.emailId;
        })
    }

    $scope.pickDueDate = function () {
        pickDateTime($scope,new Date(),null,".selectDate",false,"dueDate",false)
    }

    $scope.setTaskCompletion = function (task) {

        if(task.status == "complete"){
            task.completed = false;
            task.status = "inProgress"
            task.statusFormatted = $scope.taskStatusObj[task.status]
        } else {
            task.status = "complete"
            task.completed = true
            task.statusFormatted = $scope.taskStatusObj[task.status]
        }

        $http.post('/task/update/properties',task)
            .success(function (response) {
            });
    }

    $rootScope.toolBarList = [
        ['h1','h2','h3'],
        ['bold','italics'],
        ['ul','ol'],
        ['indent','outdent'],
        // ['justifyLeft','justifyCenter','justifyRight','justifyFull']
    ]

    getRoles($scope,$http,function (roles) {
        $scope.rolesList = roles;

        if(!share.teamMembers){
            share.teamMembers = share.companyMembers;
        }

        checkTeamLoaded();

        function checkTeamLoaded(){
            if(share.teamMembers && share.teamMembers.length>0){
                var otherUsers = _.differenceBy(share.teamMembers,_.flatten(_.map($scope.rolesList,"users")),"emailId");
                $scope.rolesList.push({
                    roleName:"Others",
                    users:otherUsers,
                    nonEditable:true
                });
            } else {
                setTimeOutCallback(200,function () {
                    checkTeamLoaded()
                })
            }
        }
    })

    $scope.searchForUser = function (query,role) {
        searchForUser(query,role)
    }

    $scope.openTaskModule = function () {
        $scope.createNewTask = !$scope.createNewTask;
        initTaskObj($scope);
    }

    $scope.openNotesModule = function () {
        $scope.showNotesModule = !$scope.showNotesModule
    }

    $scope.addNote = function (note) {
        addNote($scope,$http,note,$scope.opp.opportunityId)
    }

    $scope.searchForTeamMember = function (query) {

        checkTeamLoaded();

        function checkTeamLoaded(){
            if(share.teamMembers){
                $scope.searchList = findTeamMember(query,share.teamMembers)
            } else {
                setTimeOutCallback(200,function () {
                    checkTeamLoaded()
                })
            }
        }
    }

    $scope.searchForTeamMemberParticipants = function (query) {

        checkTeamLoaded();

        function checkTeamLoaded(){
            if(share.teamMembers){
                $scope.searchListParticipants = findTeamMember(query,share.teamMembers)
            } else {
                setTimeOutCallback(200,function () {
                    checkTeamLoaded()
                })
            }
        }
    }

    $scope.setTaskOwner = function (user) {

        if($scope.opp){

            if(!$scope.opp.usersWithAccess){
                $scope.opp.usersWithAccess = []
            }

            var userWithAccess = [{emailId:$scope.opp.userEmailId}];
            userWithAccess = $scope.opp.usersWithAccess.concat(userWithAccess);

            if(!checkUsersWithAccess(userWithAccess,user)){
                if (window.confirm(user.fullName+' is not part of this opportunity. Please add to the Internal Team')) {
                    $scope.assignTo = ""
                    $scope.searchListTaskOwner = []
                }
            } else {
                $scope.assignList.push(user)
                $scope.assignTo = ""
                $scope.searchListTaskOwner = []
            }
        } else {
            $scope.assignList.push(user)
            $scope.assignTo = ""
            $scope.searchListTaskOwner = []
        }
    }

    $scope.addTeamMembersToTask = function (user) {

        if($scope.opp){

            if(!$scope.opp.usersWithAccess){
                $scope.opp.usersWithAccess = []
            }

            var userWithAccess = [{emailId:$scope.opp.userEmailId}];
            userWithAccess = $scope.opp.usersWithAccess.concat(userWithAccess)

            if(!checkUsersWithAccess(userWithAccess,user)){
                if (window.confirm(user.fullName+' is not part of this opportunity. Please add to the Internal Team')) {
                    $scope.keyword = ""
                    $scope.searchListParticipants = []
                }
            } else {
                $scope.toList.push(user)
                $scope.keyword = ""
                $scope.searchListParticipants = []
            }
        } else {
            $scope.toList.push(user)
            $scope.keyword = ""
            $scope.searchListParticipants = []
        }
    }

    $scope.searchForTeamMemberTaskOwner = function (query) {

        checkTeamLoaded();

        function checkTeamLoaded(){

            if(share.teamMembers){
                $scope.searchListTaskOwner = findTeamMember(query,share.teamMembers);
            } else {
                setTimeOutCallback(200,function () {
                    checkTeamLoaded()
                })
            }
        }
    }

    $scope.saveTask = function () {

        $scope.task.startDate = $scope.startDate
        $scope.task.dueDate = $scope.dueDate
        $scope.task.assignedToEmailId = $scope.assignList && $scope.assignList[0]?$scope.assignList[0].emailId:null;
        $scope.task.participants = $scope.toList;
        $scope.task.refId = $scope.opp.opportunityId;

        createNew($scope,$http,share,$scope.task,function () {
            $scope.createNewTask = false;
            clearInputFields();
            getAllRelatedData($scope,$http,share,$scope.opp)
        });
    }

    $scope.setOppOwner = function (user) {
        if(!share.team){
            share.team = share.usersDictionary;
        }

        var prevOwner = share.team[$scope.opp.userEmailId];
        $scope.opp.userEmailId = user.emailId;
        $scope.opp.userId = user.userId;
        $scope.oppOwner.value = share.team[user.emailId].fullName+" ("+share.team[user.emailId].emailId +")";
        $scope.searchList = [];

        var addUserToRole = null
            ,roleObj = {};

        _.each($scope.rolesList,function (rl) {
            _.each(rl.users,function (el) {
                if(el.emailId == prevOwner.emailId){
                    addUserToRole = rl.roleName;
                    roleObj = rl;
                }
            })
        })

        if(!addUserToRole){
            addUserToRole == "Others"
        }

        $scope.addUser(roleObj,prevOwner);
    }
    
    $scope.removeUser = function (role,opp,user) {

        if(user){
            opp.usersWithAccess = opp.usersWithAccess.filter(function (el) {
                return el.emailId != user.emailId
            })

            role.usersWithAccess = role.usersWithAccess.filter(function (el) {
                return el.emailId != user.emailId
            })
        }
    }

    $scope.addUser = function (role,user) {

        if(!role.usersWithAccess){
            role.usersWithAccess = [];
        }

        if(!$scope.opp.usersWithAccess){
            $scope.opp.usersWithAccess = [];
        }

        role.usersWithAccess.push(user);

        $scope.opp.usersWithAccess.push({
            emailId:user.emailId,
            fullName:user.fullName,
            accessGroup:role.roleName
        });

        $scope.opp.usersWithAccess = _.uniqBy($scope.opp.usersWithAccess,"emailId")
        role.usersWithAccess = _.uniqBy(role.usersWithAccess,"emailId")
        clearInputFields();
        role.searchList = [];
    }
}

function setDocumentsTableHeader($scope){
    $scope.hideSearchBar = true;
    // $scope.docHeaders = ["S.No.", "Latest Version", "Document Template Type", "Document Template Name", "Document Name", "Created By", "Created Date", "Updated Date",];

    $scope.docHeaders = [
        {
            name:"S.No.",
            styleWidth:"width:"+4+"px"+"; cursor:pointer",
            align:"text-right"

        },
        {
            name:"Version",
            styleWidth:"width:"+4+"px"+"; cursor:pointer",
            align:"text-right"
        },
        {
            name:"Type",
            styleWidth:"width:"+225+"px"+"; cursor:pointer"
        },
        {
            name:"Name",
            styleWidth:"width:"+225+"px"+"; cursor:pointer"
        },
        {
            name:"Created By",
            styleWidth:"cursor:pointer"
        },
        {
            name:"Created Date",
            styleWidth:"width:"+90+"px"+"; cursor:pointer"
        },
        {
            name:"Updated Date",
            styleWidth:"width:"+90+"px"+"; cursor:pointer"
        },
        {
            name:"Stage",
            styleWidth:"width:"+90+"px"+"; cursor:pointer"
        }]
}

function getLinkedDocuments($scope, $http) {
    $scope.loadingDocuments = true;
    var opportunityId = $scope.opp.opportunityId;
    $scope.filteredDocuments = [];

    $http.get('/documents/get/linked?refType="opportunity&refId='+opportunityId)
        .success(function (response) {
            $scope.loadingDocuments = false;

            var allDocuments = response.Data;
            allDocuments.forEach(function(el) {
                el.documentCreatedDate = moment(el.documentCreatedDate).format("DD MMM YYYY");
                el.documentUpdatedDate = moment(el.documentUpdatedDate).format("DD MMM YYYY");
                if(el.documentVersion == el.currentDocumentVersion){
                    $scope.filteredDocuments.push(el);
                }
            })
        });
}

function addUser(role,user) {

    //TODO add to opp
}

function searchForUser(query,role){

    if(query && query.length>0){
        query = query.toLowerCase();

        var results = role.users.map(function (el) {

            if(_.includes(el.emailId.toLowerCase(),query)){
                return el;
            } else if(el.fullName && _.includes(el.fullName.toLowerCase(),query)){
                return el;
            } else if(el.firstName && _.includes(el.firstName.toLowerCase(),query)){
                return el;
            } else if(el.lastName && _.includes(el.lastName.toLowerCase(),query)){
                return el;
            }
        });

        role.searchList = _.compact(results);
    } else {
        role.searchList = []
    }
}

function findTeamMember(query,userList){

    var searchList = [];

    if(query && query.length>0){
        query = query.toLowerCase();

        var results = userList.map(function (el) {

            if(_.includes(el.emailId.toLowerCase(),query) || (el.fullName && _.includes(el.fullName.toLowerCase(),query)) || (el.firstName && _.includes(el.firstName.toLowerCase(),query))){
                return el;
            }
        });

        searchList = _.compact(results);
    } else {
        searchList = []
    }

    return searchList;
}

function oppDetails(share){

    return [{
        name:"Basic",
        selected:"selected"
    },{
        name:"Customer",
        selected:""
    },{
        name:"Internal Team",
        selected:""
    },{
        name:"Tasks",
        isNewOpp:"de-active",
        selected:""
    },{
        name:"Notes",
        isNewOpp:"de-active",
        selected:""
    },{
        name:"Close Details",
        selected:"",
        closeOpp:"de-active"
    },{
        name:"Documents",
        selected:""
    },{
        name:"Activity Log",
        selected:""
    }]
}

function menuToggleSelection(select,menu) {

    _.each(menu,function (item) {
        if(item.name == select){
            item.selected = "selected"
        } else {
            item.selected = ""
        }
    })
}

function getRoles($scope,$http,callback) {
    $http.get('/organisation/get/roles')
        .success(function (response) {

            if(response && response.Data){
                callback(response.Data)
            } else {
                callback([])
            }
        });
}

function mapUsersWithAccessToOrgRoles($scope,share,opp){

    checkIfOrgRolesLoaded();

    function checkIfOrgRolesLoaded(){
        
        if(($scope.rolesList && $scope.rolesList.length>0) && (share.team || share.usersDictionary )){
            if(share.usersDictionary){
                share.team = share.usersDictionary;
            } else if(!share.team){
                share.team = share.usersDictionary;
            }

            _.each($scope.rolesList,function (role) {
                role.usersWithAccess = [];
                _.each(opp.usersWithAccess,function (user) {
                    if(role.roleName == user.accessGroup){
                        if(share.team[user.emailId]){
                            role.usersWithAccess.push(share.team[user.emailId])
                        } else {
                            role.usersWithAccess.push({
                                fullName:user.emailId
                            })
                        }
                    }
                });
            });

        } else {
            setTimeOutCallback(100,function () {
                checkIfOrgRolesLoaded();
            })
        }
    }
}

function addNote($scope,$http,contactNote,oppId) {

    contactNote = nl2br(contactNote)

    var url = '/message/create/by/id';

    var obj = {
        messageReferenceType:"opportunity",
        text:contactNote,
        conversationId:oppId
    }

    $http.post(url,obj)
        .success(function (response) {
            if(response.SuccessCode){
                var a = response.Data;

                a.text = a.text.replace(/<br ?\/?>/g, "\n")
                a.dateFormatted = a.date ? moment(a.date).format("DD MMM YYYY") : null;
                $scope.notes.unshift(a)
            }
            else {
                toastr.error("Failed to add note, try again later");
            }

            $scope.showNotesModule = false;
            $scope.note = "";
            clearInputFields();
        });
}

function getNotes($scope,$http,conversationId) {

    var url = "/get/messages/by/conversation/id?conversationId="+conversationId;

    $http.get(url)
        .success(function (response) {
            if(response.SuccessCode){

                $scope.notes = response.Data

                $scope.notes.forEach(function(a) {
                    a.text = a.text.replace(/<br ?\/?>/g, "\n")
                    a.dateFormatted = a.date ? moment(a.date).format("DD MMM YYYY") : null;
                });

                $scope.notes.sort(function (o1, o2) {
                    return o1.date < o2.date ? 1 : o1.date > o2.date ? -1 : 0;
                });
            }
        })
}

function getTasks($scope,tasks,share) {

    $scope.taskStatusObj = {
        notStarted:"Not Started",
        complete:"Complete",
        inProgress:"In Progress"
    }

    $scope.tasks = tasks

    $scope.tasks.forEach(function(a) {
        if(share && share.usersDictionary){
            a.profile = share.usersDictionary[a.assignedToEmailId]
        }

        a.statusFormatted = $scope.taskStatusObj[a.status];

        if(a.status == "complete"){
            a.completed = true
        }
        a.dateFormatted = a.dueDate ? moment(a.dueDate).format("DD MMM YYYY") : null;
    });

    $scope.tasks.sort(function (o1, o2) {
        return o1.dueDate < o2.dueDate ? 1 : o1.dueDate > o2.dueDate ? -1 : 0;
    });
}

function pickDateTime($scope,minDate,maxDate,id,timezone,scopeSelector,timeRequired){

    var settings = {
        value:"",
        timepicker:timeRequired?true:false,
        validateOnBlur:false,
        onSelectDate: function (dp, $input){
            $scope.$apply(function () {
                $(".xdsoft_datetimepicker").hide();
                var timezone = timezone?timezone:"UTC";
                $scope[scopeSelector] = moment(dp).tz(timezone).format();
                $scope[scopeSelector+'Formatted'] = moment(dp).tz(timezone).format("DD MMM YYYY");
            });
        }
    }

    if(minDate){
        settings.minDate = minDate;
    }

    $(id).datetimepicker(settings);
}

function initTaskObj($scope){
    $scope.assignTo = ""
    $scope.toList = []
    $scope.searchListTaskOwner = []
    $scope.searchListParticipants = []

    $scope.task = {
        taskName:"",
        description:"",
        priority:null,
        startDate:null,
        dueDate:null,
        toList:[],
        participants:[],
        taskFor:"opportunities"
    }

}

function getAccountTypes($scope,$http,share,opp){

    if(!$scope.accountTypes || ($scope.accountTypes && $scope.accountTypes.length === 0)){
        $scope.accountTypes = [];
        $http.get("/account/master/types/by/group")
            .success(function (response) {
                if(response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.accountTypes = response.Data;
                    setAccounts($scope,$scope.accountTypes,opp)
                }
            })
    } else {
        setAccounts($scope,$scope.accountTypes,opp)
    }
}

function setAccounts($scope,accounts,opp){

    _.each(opp.accounts,function (opAcc) {
        _.each(accounts,function (coAcc) {
            if(opAcc.group === coAcc._id){
                coAcc.searchText = opAcc.name
            }
        })
    })
}

function getAllRelatedData($scope,$http,share,opp){
    $http.get("/opportunity/related/data/by/id?opportunityId="+opp.opportunityId)
        .success(function (response) {
            if(response.SuccessCode){
                if((/commits/.test(window.location.pathname))){
                } else {
                    formatLogs($scope,$http,share,opp,response.logs);

                }
                getTasks($scope,response.tasks,share);

                $scope.notes = response.notes

                $scope.notes.forEach(function(a) {
                    a.text = a.text.replace(/<br ?\/?>/g, "\n")
                    a.dateFormatted = a.date ? moment(a.date).format("DD MMM YYYY") : null;
                });

                $scope.notes.sort(function (o1, o2) {
                    return o1.date < o2.date ? 1 : o1.date > o2.date ? -1 : 0;
                });
            }
        })
}

function searchForAccountName($scope,$http,share,account){
    if(account.searchText && account.searchText.length>2){
        $http.post("/account/master/types/search",account)
            .success(function (response) {
                account.accountList = response.Data;
            })
    }
}

function selectThisAccount($scope,$http,share,account,group){

    group.searchText = account.name;

    if(!$scope.opp.accounts){
        $scope.opp.accounts = []
    }
    $scope.opp.accounts.push({name:account.name,group:group._id});

    _.uniqBy($scope.opp.accounts,"group");
    group.accountList = [];

}

function checkUsersWithAccess(usersWithAccess,user) {

    var exists = false;
    _.each(usersWithAccess,function (el) {
        if(el.emailId === user.emailId){
            exists = true;
            return false;
        }
    });

    return exists

}

function sendOpportunityCloseSummary($scope,$http,share,$rootScope, response) {
    var daysToClose = moment.duration(moment(new Date()).diff(moment($scope.opp.createdDate)));

    if(response.companyHead && response.reportingManager){

        if($scope.opp.mailRm || $scope.opp.mailOrgHead){

            var subject = "Deal worth "+share.primaryCurrency+" "+$scope.opp.amount+ " successfully closed."
            if($scope.opp.stage == "Close Lost"){
                subject = "Deal worth "+share.primaryCurrency+" "+$scope.opp.amount+ " Lost."
            }

            var intro = '';

            var contactEmailId = $scope.opp.contactEmailId ? $scope.opp.contactEmailId: $scope.newOppContact.emailId;

            if(response.companyHead){
                intro = intro+ "Hi "+response.companyHead.firstName;
            }

            if(response.companyHead && response.reportingManager){
                intro = intro+" & ";
            }

            if(response.reportingManager){
                intro = intro+ response.reportingManager.firstName
            }

            if(response.companyHead.emailId == response.reportingManager.emailId){
                intro = "Hi "+response.companyHead.firstName
            }

            if(!checkRequired($scope.opp.closeReasonDescription)){
                $scope.opp.closeReasonDescription = "No Comments"
            }

            var oppDetails = "\n\nOpportunity Name :"+$scope.opp.opportunityName+
                "\nDeal Size: "+$rootScope.currency+" "+$scope.opp.amountWithNgmCommas +"("+ $scope.opp.netGrossMargin +"% of "+$scope.opp.amount +")"+
                "\nClose date: "+moment($scope.opp.closeDate).tz(timezone).format(standardDateFormat())+
                "\nDays to Closure: "+Math.ceil(daysToClose.asDays())+
                "\nStage: "+$scope.opp.stage+
                "\nContact: "+contactEmailId+
                "\nReason: "+$scope.opp.closeReasons.join(',')+
                "\nReason details: "+$scope.opp.closeReasonDescription
            // "\nView Opportunity: "+"http://"+$rootScope.companyDetails.url+"/opportunities/all"

            var renewalDetails = ""
            if(response.opportunity && response.opportunity.renewed && response.opportunity.renewed.amount){
                renewalDetails = "\n\n Renewal opportunity was created\n"
                    +"\nDeal size: "+$rootScope.currency+" "+response.opportunity.renewed.amount
                    +"\nClose Date: "+moment(response.opportunity.renewed.closeDate).format(standardDateFormat())
            }

            var signature = '\n\n\n'+getSignature(share.liuData.firstName+' '+share.liuData.lastName,share.liuData.designation,share.liuData.companyName,share.liuData.publicProfileUrl)
            // +'\n\n Powered by Relatas';
            var body = intro+",\n\n"+"Deal with "+contactEmailId+" has closed."+oppDetails+renewalDetails+signature;

            $scope.add_cc = [];
            if($scope.opp.mailOrgHead){
                $scope.add_cc = [response.companyHead.emailId];
            }

            sendEmail($scope,$http,subject,body,null,share,share.liuData._id,response.reportingManager.emailId,$scope.cPhone,share.liuData,null,response.reportingManager.firstName)
        }
    }
}

function autoInitGoogleLocationAPI(share,$scope){

    window.initAutocomplete = initAutocomplete;

    function initAutocomplete() {

        checkDOMLoaded();

        checkContactDOMLoaded();

        function checkContactDOMLoaded(){

            if(document.getElementById('p_location')){

                var autocomplete = new google.maps.places.Autocomplete(
                    (document.getElementById('p_location')),
                    {types:['(cities)']});
                autocomplete.addListener('place_changed', function(err,places){

                    $scope.coords = {
                        lat:autocomplete.getPlace().geometry.location.lat(),
                        lng:autocomplete.getPlace().geometry.location.lng(),
                        address:$("#p_location").val()
                    }
                    return false;
                });

                var autocomplete3 = new google.maps.places.Autocomplete(
                    (document.getElementById('p_location_2')),
                    {types:['(cities)']});
                autocomplete3.addListener('place_changed', function(err,places){

                    $scope.coords = {
                        lat:autocomplete3.getPlace().geometry.location.lat(),
                        lng:autocomplete3.getPlace().geometry.location.lng(),
                        address:$("#p_location_2").val()
                    }
                    return false;
                });
            } else {
                setTimeOutCallback(100,function () {
                    checkContactDOMLoaded();
                })
            }
        }

        function checkDOMLoaded(){
            if(document.getElementById('autocompleteCity')){

                var autocomplete2 = new google.maps.places.Autocomplete(
                    (document.getElementById('autocompleteCity')),
                    {types:['(cities)']});
                autocomplete2.addListener('place_changed', function(err,places){

                    if(!$scope.opp){
                        $scope.opp = {}
                    }

                    if(!$scope.opp.geoLocation){
                        $scope.opp.geoLocation = {}
                    }

                    $scope.opp.geoLocation.lat = autocomplete2.getPlace().geometry.location.lat()
                    $scope.opp.geoLocation.lng = autocomplete2.getPlace().geometry.location.lng()

                    share.setTown(null)
                    if($("#autocompleteCity").val() && $("#autocompleteCity").val() != "" && $("#autocompleteCity").val() != " "){
                        share.setTown($("#autocompleteCity").val())
                    }
                    return false;
                });
            } else {
                setTimeOutCallback(100,function () {
                    checkDOMLoaded();
                })
            }
        }
    }
}

function searchResults($scope,$http,keywords,share,searchService,type) {

    var selector = "showResults"+type;
    var typeSelector = type+"s"

    if(keywords && keywords.length > 2){

        searchService.search(keywords).success(function(response){

            if(response.SuccessCode){

                if(type == "contact"){
                    $scope.noContactsFound = false;
                }

                $scope[typeSelector+"NotFound"] = false

                $scope[selector] = true;
                processSearchResults($scope,$http,response.Data,type);
            } else {
                $scope[selector] = false;
                $scope[typeSelector] = [];

                if(type == "contact"){
                    $scope.noContactsFound = true;
                }

                $scope[typeSelector+"NotFound"] = true;

                var obj = {
                    noPicFlag:true,
                    fullName: '',
                    name: '',
                    image: '/getContactImage/' + null + '/' + null,
                    emailId:keywords
                }

                $scope[typeSelector].push(obj)

            }
        }).error(function(){
            console.log('error');
        });
    } else {
        $scope[selector] = false;
        $scope[typeSelector] = [];
    }
}

function removeRecipient($scope,$http,contact,type){

    var opportunityId = $scope.opp._id
    var selector = "showResults"+type;
    var typeSelector = type+"s";

    var hashtag = type;
    var relation = "decision_maker"

    if(type === 'partners'){
        hashtag = "partner"
    } else if(type === 'decisionMakers'){
        hashtag = "decisionMaker"
    } else if(type === 'influencers'){
        hashtag = "influencers"
        relation = "influencer"
    }

    $http.post('/opportunities/remove/people',{type:type,contact:contact,opportunityId:opportunityId,opportunityId2:$scope.opp.opportunityId})
        .success(function (response) {
            if(response.SuccessCode){
                updateRelationship($http,contact,null,"decisionmaker_influencer",relation)
                deleteHashtag($http,contact.contactId,hashtag,contact.emailId)
                var rmIndex = $scope.opp[typeSelector].indexOf(contact);
                $scope.opp[typeSelector].splice(rmIndex, 1);
            }
        });
}

function addRecipient($scope,$http,contact,type){

    var opportunityId = $scope.opp?$scope.opp._id:null;
    var selector = "showResults"+type;
    var typeSelector = type+"s";

    $scope.partner = '';
    var hashtag = type;
    var relation = "decision_maker"
    if(type === 'partners'){
        hashtag = "partner"
    } else if(type === 'decisionMakers'){
        hashtag = "decisionMaker"
    } else if(type === 'influencers'){
        hashtag = "influencers"
        relation = "influencer"
    }

    addHashtag ($http,contact._id,hashtag)
    updateRelationship($http,contact,relation,"decisionmaker_influencer");

    if(validateEmail(contact.emailId) && opportunityId){
        $http.post('/opportunities/add/people',{type:typeSelector,contact:contact,opportunityId:opportunityId,opportunityId2:$scope.opp.opportunityId})
            .success(function (response) {
                if(response.SuccessCode){
                    $scope.opp[typeSelector].push(contact)
                    $scope[selector] = false;
                } else {
                    $scope[selector] = false;
                }

                clearInputFields();
            });
    } else {

        if(!$scope.opp){
            $scope.opp = {}
        }

        if(!$scope.opp[typeSelector]){
            $scope.opp[typeSelector] = [];
        }

        $scope.opp[typeSelector].push(contact)

        $scope[selector] = false;
        $scope[hashtag] = "";
    }
}

function addHashtag ($http,p_id,hashtag){
    var str = hashtag.replace(/[^\w\s]/gi, '');
    var obj = { contactId:p_id, hashtag: str};
    $http.post("/api/hashtag/new",obj)
        .success(function (response) {

        });
}

function processSearchResults($scope,$http,response,type) {

    var typeSelector = type+"s";
    var contactsArray = response;
    $scope[typeSelector] = [];

    if(contactsArray.length>0){
        for(var i=0;i<contactsArray.length;i++){

            var obj = {};

            if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                var name = getTextLength(contactsArray[i].personName,20);
                var image = '/getImage/'+contactsArray[i].personId._id;

                obj = {
                    fullName:contactsArray[i].personName,
                    name:name,
                    image:image
                };

                obj.emailId = contactsArray[i].personEmailId;
                obj.twitterUserName = contactsArray[i].twitterUserName;
                obj.mobileNumber = contactsArray[i].mobileNumber;

            }
            else {
                var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                obj = {
                    fullName: contactsArray[i].personName,
                    name: getTextLength(contactsArray[i].personName, 20),
                    image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                    emailId:contactsArray[i].personEmailId,
                    twitterUserName: contactsArray[i].twitterUserName,
                    mobileNumber: contactsArray[i].mobileNumber
                    // noPicFlag:true
                };
            }

            if(obj.twitterUserName){
                obj.tweetAccExists = true;
            }

            obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
            obj._id = contactsArray[i]._id;
            obj.personId = contactsArray[i].personId && contactsArray[i].personId._id?contactsArray[i].personId._id:null;
            obj.type = type;

            obj.noPicFlag = true;
            obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();

            if(!userExists(obj.emailId) && validateEmail(obj.emailId)){
                $scope[typeSelector].push(obj)
            }

            function userExists(username) {
                return $scope[typeSelector].some(function(el) {
                    return el.emailId === username;
                });
            }
        }
    }
}

function updateRelationship($http,contact,relation,relationKey,relationType){

    if(contact.contactId){
        var id = contact.contactId
    } else {
        id = contact._id
    }

    var reqObj = {contactId:id,type:relation,relationKey:relationKey,relation:relationType};

    $http.post('/opportunities/contacts/update/reltionship/type',reqObj)
        .success(function(response){

        });
}

function resetErrorsField($scope){
    _.each($scope.accountTypes,function (accType) {
        accType.req = false;
    })

    for(var key in oppEmptyObj()){
        $scope.opp[key+"Req"] = false;
    }

    highlightTab($scope,0)

    $scope.errorsExist = false;
}

relatasApp.directive('searchResultsTeam', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="searchList.length>0">' +
            '<div ng-repeat="item in searchList track by $index"> ' +
            '<div class="clearfix cursor" ng-click="setOppOwner(item)">' +
            '<div class="pull-left relative">' +
            // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
            '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
            '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
            '<div class="pull-left"><p class="margin0">[[item.fullName]]</p></div>' +
            '</div></div></div>'
    };
});

relatasApp.directive('searchResultsTeamOne', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="searchListTaskOwner.length>0">' +
            '<div ng-repeat="item in searchListTaskOwner track by $index"> ' +
            '<div class="clearfix cursor" ng-click="setTaskOwner(item)">' +
            '<div class="pull-left relative">' +
            // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
            '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
            '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
            '<div class="pull-left"><p class="margin0">[[item.fullName]]</p></div>' +
            '</div></div></div>'
    };
});

relatasApp.directive('searchResultsTeamTwo', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="searchListParticipants.length>0">' +
            '<div ng-repeat="item in searchListParticipants track by $index"> ' +
            '<div class="clearfix cursor" ng-click="addTeamMembersToTask(item)">' +
            '<div class="pull-left relative">' +
            // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
            '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
            '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
            '<div class="pull-left"><p class="margin0">[[item.fullName]]</p></div>' +
            '</div></div></div>'
    };
});

relatasApp.directive('searchResults', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="role.searchList.length>0">' +
            '<div ng-repeat="teamMember in role.searchList track by $index"> ' +
            '<div class="clearfix cursor" style="margin: 10px 0;" title="[[teamMember.fullName]], [[teamMember.emailId]]" ng-click="addUser(role,teamMember)">' +
            '<div class="pull-left relative">' +
            // '<img ng-if="teamMember.noImage === 0" ng-src="[[teamMember.image]]" title="[[teamMember.fullName]], [[teamMember.emailId]]" class="contact-image">' +
            // '<span ng-if="teamMember.noImage === 1">' +
            // '<span class="contact-no-image">[[teamMember.firstName]]</span></span></div>' +
            // // '<div class="pull-left"><p class="margin0">[[teamMember.name]]</p><p class="margin0">[[teamMember.emailId]]</p></div>' +
            // '<div class="pull-left">' +
            '<p class="margin0">[[teamMember.fullName]]</p>' +
            '<p class="margin0">[[teamMember.emailId]]</p>' +
            '</div>' +
            '</div></div></div>'
    };
});

relatasApp.directive('searchResultsContacts', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" style="position: absolute;" ng-show="showResultscontact">' +
            '<div ng-repeat="item in contacts track by $index"> ' +
            '<div class="clearfix cursor" ng-click="selectContact(item)">' +
            '<div class="pull-left relative">' +
            // '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
            '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
            '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
            '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
            '</div></div></div>'+
            '<div class="input-text-error" ng-show="noContactsFound && !opp.contactEmailIdReq"> ' +
            'We couldn"t find the contact. click .<span style="cursor:pointer; color:#267368" ng-click="createContactWindow()"> here</span> to create' +
            '</div>'
    };
});

function masterAccList($scope,$http,opp){

    $http.get("/corporate/admin/get/master/account/types")
        .success(function (response) {

            if (response && response[0]) {
                $scope.data = {
                    model: null,
                    availableOptions: response
                };

                $scope.defaultAccount = $scope.data.availableOptions[0].name;
                $scope.data.model = $scope.defaultAccount;
            }
        });

    $scope.removeMasterData = function(obj,list){

        if(window.confirm('This will delete the master data')) {
            var newList = [];
            if(list){
                _.each(list.lists,function (li) {
                    if(li._id !== obj._id){
                        newList.push(li)
                    }
                })
            }

            list.lists = newList;
            if(opp.masterData.length>0) {
                _.each(opp.masterData, function (ma) {
                    if (ma.type === list.name) {
                        ma.data = newList;
                    }
                })
            }

            if(list.lists.length === 0){
                list.typeHeaders = [];
            }

            if($scope.opp.masterData && $scope.opp.masterData.length>0){
                $scope.opp.masterData = $scope.opp.masterData.filter(function (ma) {
                    return ma.type != list.name;
                });
            }
        }
    }

    $scope.attachExistingData = function(obj){

        if(!opp.masterData){
            opp.masterData = [];
        }

        if(opp.masterData.length>0){
            _.each(opp.masterData,function (ma) {
                if(ma.type === obj.name){
                    obj.lists = ma.data;
                    obj.typeHeaders = [];

                    for(var key in ma.data[0]){
                        obj.typeHeaders.push({
                            name:key,
                            value:""
                        })
                    }
                }
            });
        }
    }

    $scope.selectMasterType = function(data,accType){

        if(opp){
            if(!opp.masterData){
                opp.masterData = [];
            }

            if(opp.masterData.length>0){
                _.each(opp.masterData,function (ma) {
                    if(ma.type === accType.name && ma._id){
                        ma.replaceThis = true
                    }
                });

                opp.masterData = opp.masterData.filter(function (ma) {
                   return !ma.replaceThis;
                });

                opp.masterData.push({
                    type:accType.name,
                    data:[data]
                })
            } else {
                opp.masterData = [{
                    type:accType.name,
                    data:[data]
                }]
            }
        }

        $scope.attachExistingData(accType);
    };

    $scope.closeMDPopup = function(){
        $scope.addMoreMasterData = false;
    }

    $scope.clearSearch = function(accType){
        $scope.ifMasterAccSearching = false;
        $scope.accName = "";
        $(".master-acc input").val("");

        fetchAccounts($scope,$http,null,accType);
    }

    $scope.getMasterDataFor = function(forType){
        $scope.addMoreMasterData = true;
        $scope.addList = forType;
        fetchAccounts($scope,$http,null,forType.name,forType);
    }

    $scope.searchForMasterAccount = function(accName,forType) {
        $scope.ifMasterAccSearching = true;
        if(accName && accName.length>1){
            fetchAccounts($scope,$http,accName,forType.name,forType)
        }
    }
}

function fetchAccounts($scope,$http,accountName,accountType) {

    var url = "/corporate/admin/get/master/account/list";

    if(accountType){
        url = fetchUrlWithParameter(url,"accountType",accountType);
    }

    if(accountName){
        url = fetchUrlWithParameter(url,"search",accountName);
    }

    $http.get(url)
        .success(function (response) {

            if(response && response[0] && response[0].data){
                $scope.typeHeaders = [];

                for(var key in response[0].data[0]){
                    $scope.typeHeaders.push({
                        name:key,
                        value:""
                    })
                };

                $scope.importantHeadersObj = {};
                if(response[0].importantHeaders){
                    _.each(response[0].importantHeaders,function (ih) {
                        $scope.importantHeadersObj[ih.name] = ih.isImportant
                    })
                }

                $scope.lists = response[0].data;
            }
        });
}

function getLog($scope,$http,share,opp) {

    $http.get("/opportunities/get/log?opportunityId="+opp.opportunityId)
        .success(function (response) {
            if(response && response.SuccessCode){
                opp.logs = response.Data;

                checkTeamLoaded();

                function checkTeamLoaded(){
                    if(share.teamDictionaryByUserId){

                        _.each(opp.logs,function (log) {
                            log.dateFormatted = moment(log.date).format(standardDateFormat()) +" at "+moment(log.date).format("h:mm A")
                            log.noteExist = log.notes?true:false
                            log.message = buildLogMessage(log);
                            log.what = buildLogWhatText(log);

                            if(log.action == "removed" || log.action == "added"){
                                log.oldValue = log.oldValue.toString();
                                log.newValue = log.newValue.toString();

                            } else if(log.action == "transfer"){
                                log.oldValue = share.team[log.fromEmailId].emailId
                                log.newValue = share.team[log.toEmailId].emailId
                            }

                            if(log.type == "geoLocation") {
                                var oldVal = "",newVal = ""
                                if(log.oldValue.town){
                                    oldVal = log.oldValue.town +", "+log.oldValue.zone
                                }
                                if(log.newValue.town){
                                    newVal = log.newValue.town +", "+log.newValue.zone
                                }

                                log.oldValue = oldVal;
                                log.newValue = newVal;

                                if(log.action == "Renewal"){
                                    log.newValue = "Created";
                                }
                            }

                            log.by = share.teamDictionaryByUserId[log.fromUserId].fullName+"("+share.teamDictionaryByUserId[log.fromUserId].emailId+")"
                        });

                    } else {
                        setTimeOutCallback(200,function () {
                            checkTeamLoaded()
                        })
                    }
                }
            }
        });
}

function formatLogs($scope,$http,share,opp,logs) {

    opp.logs = logs;

    checkTeamLoaded();

    if(!share.team){
        share.team = share.usersDictionary;
    }

    function checkTeamLoaded(){
        if(share.teamDictionaryByUserId){

            _.each(opp.logs,function (log) {
                log.dateFormatted = moment(log.date).format(standardDateFormat()) +" at "+moment(log.date).format("h:mm A")
                log.noteExist = log.notes?true:false
                log.message = buildLogMessage(log);
                log.what = buildLogWhatText(log);


                if(log.action == "removed" || log.action == "added" || _.includes(log.action.toLowerCase(),"add") || _.includes(log.action.toLowerCase(),"remove")){

                    if(log.oldValue){
                        log.oldValue = log.oldValue.toString();
                    }
                    if(log.newValue){
                        log.newValue = log.newValue.toString();
                    }

                } else if(log.type == "decisionMakers" || log.type == "influencers" || log.type == "partners"){
                    log.oldValue = log.oldValue.length>0?_.map(log.oldValue,"emailId").join(","):""
                    log.newValue = log.newValue.length>0?_.map(log.newValue,"emailId").join(","):""
                } else if(log.action == "transfer"){
                    log.oldValue = share.team[log.fromEmailId].emailId
                    log.newValue = share.team[log.toEmailId].emailId
                }

                if(log.action == "close"){
                    log.oldValue = "Open";
                    log.newValue = "Close";
                }

                if(log.action == "created"){
                    log.oldValue = "";
                    log.newValue = "Created";
                }

                if(log.type == "closeDate"){
                    log.oldValue = moment(log.oldValue).format(standardDateFormat());
                    log.newValue = moment(log.newValue).format(standardDateFormat());
                }

                if(log.type == "geoLocation") {

                    var oldVal = "",newVal = ""

                    if(log.oldValue && log.oldValue.zone){
                        oldVal = log.oldValue.town +","+ log.oldValue.zone
                    } else if(log.oldValue && log.oldValue){
                        oldVal = log.oldValue
                    }

                    if(log.newValue && log.newValue.zone){
                        newVal = log.newValue.town +","+ log.newValue.zone
                    } else if(log.newValue && log.newValue){
                        newVal = log.newValue
                    }

                    log.oldValue = oldVal;
                    log.newValue = newVal;
                }

                if(log.action == "Renewal"){
                    log.newValue = "Created";
                }

                if(share.teamDictionaryByUserId[log.fromUserId]){
                    log.by = share.teamDictionaryByUserId[log.fromUserId].fullName+"("+share.teamDictionaryByUserId[log.fromUserId].emailId+")"
                }
            });

            opp.logs = opp.logs.filter(function (el) {
                if(checkRequired(el.oldValue) || checkRequired(el.newValue)){
                    return el;
                }
            })

        } else {
            setTimeOutCallback(200,function () {
                checkTeamLoaded()
            })
        }
    }
}

function buildLogMessage(log){
    var message = "";

    if(log.action == "created"){
        message = "Opp created by "+log.fromEmailId
    }

    if(log.action == "Renewal"){
        message = "Opp renewed by "+log.fromEmailId
    }

    if(log.action == "transfer"){

        var transferredBy = log.fromEmailId;

        if(log.transferredBy){
            transferredBy = log.transferredBy
        }

        message = "Opp transferred by "+transferredBy
    }

    if(log.action == "decisionMakersAdded" || log.action == "decisionMakerAdded"){
        message = "Decision maker added "
    }
    if(log.action == "partnersAdded" || log.action == "partnersAdded"){
        message = "Partner added "
    }

    if(log.action == "influencersAdded" || log.action == "influencerAdded"){
        message = "Influencer added "
    }

    if(log.action == "decisionMakerRemoved" || log.action == "decisionMakersRemoved"){
        message = "Decision maker removed "
    }
    if(log.action == "partnersRemoved" || log.action == "partnerRemoved"){
        message = "Partner removed "
    }

    if(log.action == "influencersRemoved" || log.action == "influencerRemoved"){
        message = "Influencer removed "
    }

    if(log.action == "propertyChange"){
        if(log.type == "stageName"){
            message = "Stage updated "
        }

        if(log.type == "closeDate"){
            message = "Close date updated "
        }

        if(log.type == "amount"){
            message = "Amount updated "
        }

        if(log.type == "contactEmailId"){
            message = "Contact updated "
        }

        if(log.type == "netGrossMargin"){
            message = "Net gross margin updated "
        }
    }

    if(message != ""){
        message = message+ " on "+log.dateFormatted;

        return message
    }
}

function buildLogWhatText(log){
    var message = "";

    if(log.action == "created"){
        message = "Opportunity"
    }

    if(log.action == "Renewal"){
        message = "Opportunity renewed"
    }

    if(log.action == "transfer"){
        message = "Opportunity transferred"
    }

    if(log.action == "decisionMakersAdded" || log.action == "decisionMakerAdded"){
        message = "Decision maker "
    }
    if(log.action == "partnersAdded" || log.action == "partnersAdded"){
        message = "Partner "
    }

    if(log.action == "influencersAdded" || log.action == "influencerAdded"){
        message = "Influencer "
    }

    if(log.action == "decisionMakerRemoved" || log.action == "decisionMakersRemoved"){
        message = "Decision maker "
    }
    if(log.action == "partnersRemoved" || log.action == "partnerRemoved"){
        message = "Partner "
    }

    if(log.action == "influencersRemoved" || log.action == "influencerRemoved"){
        message = "Influencer "
    }

    if(log.action == "propertyChange"){
        if(log.type == "stageName"){
            message = "Stage "
        }

        if(log.type == "closeDate"){
            message = "Close date "
        }

        if(log.type == "amount"){
            message = "Amount "
        }

        if(log.type == "contactEmailId"){
            message = "Contact "
        }

        if(log.type == "netGrossMargin"){
            message = "Net gross margin "
        }

        if(log.type == "userEmailId"){
            message = "Owner"
        }

        if(log.type == "type"){
            message = "Type"
        }

        if(log.type == "vertical"){
            message = "Vertical"
        }

        if(log.type == "solution"){
            message = "Solution"
        }

        if(log.type == "productType"){
            message = "Product"
        }

        if(log.type == "sourceType"){
            message = "Source"
        }

        if(log.type == "currency"){
            message = "Currency"
        }

        if(log.type == "businessUnit"){
            message = "Business Unit"
        }

        if(log.type == "geoLocation"){
            message = "Region"
        }

        if(log.type == "opportunityName"){
            message = "Opportunity Name"
        }

        if(log.type == "partners"){
            message = "Partner"
        }

        if(log.type == "influencers"){
            message = "Influencer"
        }

        if(log.type == "decisionMakers"){
            message = "Decision Maker"
        }


    }

    if(log.action == "removed"){
        message = log.type
    }

    if(log.action == "added"){
        message = log.type
    }

    if(log.action == "close"){
        message = "Opportunity"
    }

    return message
}
relatasApp.controller("taskMenu", function ($scope, $http, share, $rootScope) {

    closeAllDropDownsAndModals($scope,".task-filter")
    closeAllDropDownsAndModals($scope,".drop-down-list")
    // closeAllDropDownsAndModals($scope,".drop-down-list-2")

    $scope.filters = [{
        name:"All tasks",
        value:"allTasks"
    },{
        name:"Weekly sales call tasks",
        value:"weeklyReview"
    },{
        name:"Mobile tasks",
        value:"mobile"
    }]

    $scope.defaultMenu = "All tasks"
    if(/commit/.test(window.location.pathname)){
        $scope.defaultMenu = "Weekly sales call tasks"
    }

    $scope.createNew = function () {
        share.createNew()
        $rootScope.createFormIsOpen = true
    };

    share.unhideCreateTaskBtn = function () {
        $scope.createFormIsOpen = false;
    }

    $scope.displayFilter = function () {
        $scope.isDisplayFilter = !$scope.isDisplayFilter;
    }

    $scope.selectFilter = function (selection) {
        share.filterSelected = selection;
        $scope.defaultMenu = selection.name
        share.getAllTasks(share.selectedUser,selection.value,share.startDate,share.endDate)
    }

    $scope.tasksByDateRange = function(start,end) {
        $scope.displayDatePicker = false;
        if(start && end && start != "" && end != ""){
            $scope.dateRange = $scope.startFormatted +" - "+$scope.endFormatted
        }
        share.getAllTasks(share.selectedUser,share.filterSelected && share.filterSelected.value?share.filterSelected.value:"dueDate",start,end)
    }

    share.getTasksByDateFilter = function(start, end) {
        $scope.startFormatted = moment(start).format("DD MMM YYYY");
        $scope.endFormatted = moment(end).format("DD MMM YYYY");

        if(start && end && start != "" && end != ""){
            $scope.dateRange = $scope.startFormatted +" - "+$scope.endFormatted
        }
        share.getAllTasks(share.selectedUser,"dueDate",start,end)
    }

    $scope.dateRange = "Filter by due date";

    $scope.selectDateRange = function () {
        $scope.displayDatePicker = true;
    }

    $scope.pickDueDate = function () {

        pickDateTime($scope,null,null,"#start",false,"start",false,function (timeSelected) {

        })
        pickDateTime($scope,null,null,"#end",false,"end",false,function (timeSelected) {
        })
    }

    $scope.clearDateRange = function () {
        $scope.dateRange = "Filter by due date";
        $scope.start = null;
        $scope.end = null;
        $scope.endFormatted = "";
        $scope.startFormatted = "";
        $scope.displayDatePicker = false;
    }
})

relatasApp.controller("tasksList", function ($scope, $http, share, $rootScope) {
    $scope.headers = ["Assigned to","Task","Assigned on","Due date","Status","Category"]
    share.getAllTasks = function (user,filter,start,end) {
        share.selectedUser = user
        share.assignListPrePopulate(user);
        getAllTasks($scope, $http, share,$rootScope,user,filter,start,end)
    }

    $scope.openDiscussion = function (task) {
        share.openDiscussion(task)

        if((/commit/.test(window.location.pathname))){
            share.setCssForWeeklyReview()
        }
    }
})

relatasApp.controller("taskDiscussion", function ($scope, $http, share, $rootScope) {

    closeAllDropDownsAndModals($scope,".drop-down-list")
    closeAllDropDownsAndModals($scope,".close-div")

    $scope.moreSettingsOpen = function () {
        $scope.displayMoreOptions = true;
    }

    $scope.closeMoreOptions = function () {
        $scope.displayMoreOptions = false;
    }

    $scope.pickDueDate = function () {
        pickDateTime($scope,new Date(),null,"#changeDueDate",false,"dueDate",false,function (timeSelected) {
            $scope.task.dueDate = new Date(timeSelected)
            $scope.task.dueDateFormatted = moment(timeSelected).format(standardDateFormat())
            updateTask($scope, $http, share,$scope.task)
        })
    }

    share.openDiscussion = function (task) {
        $scope.taskName = "";
        $("div[id^='taTextElement']").empty();
        $scope.task = task;
        $scope.ifTaskDiscussion = true;
        $scope.taskName = $scope.task.taskName
        $("div[id^='taTextElement']").append($scope.taskName)
        getMessagesForTask($scope,$http,share,$rootScope,task._id)
    }

    $scope.selectNewPriority = function (task,toPriority) {
        $scope.openPriorityList = !$scope.openPriorityList;
        taskUpdatePriorities($scope,$http,share,task,toPriority)
    }

    $scope.selectNewStatus = function (task,toStatus) {
        $scope.openNewStatusList = false;
        taskUpdateStatus($scope,$http,share,task,toStatus)
    }

    $scope.changePriority = function () {
        $scope.openPriorityList = true;
    }

    $scope.changeStatus = function () {
        $scope.openNewStatusList = true;
    }

    $scope.taskPrioritiesList = ["low","medium","high"];

    $scope.closeDiscussion = function () {
        $scope.ifTaskDiscussion = false;

        if((/commit/.test(window.location.pathname))){
            share.removeCssForWeeklyReview()
        }
    }

    $scope.closeTaskName = function () {
        $scope.showTaskNameEditor = false;
    }

    $scope.sendMessage = function (message,task) {
        reply($scope,$http,share,message,task)
    }

    $scope.editTaskName = function () {
        $scope.showTaskNameEditor = true;
    }

    $scope.saveTaskName = function (taskName) {
        $scope.showTaskNameEditor = false;
        $scope.task.taskName = taskName;
        updateTask($scope, $http, share,$scope.task)
    }

})

relatasApp.controller("taskCreateNew", function ($scope, $http, share, $rootScope,searchContacts) {

    share.taskFor = "others";

    if((/commit/.test(window.location.pathname))){
        share.taskFor = "weeklyReview"
    }
    
    $rootScope.toolBarList = [
        ['h1','h2','h3'],
        ['bold','italics'],
        ['ul','ol'],
        ['indent','outdent'],
        // ['justifyLeft','justifyCenter','justifyRight','justifyFull']
    ]
    $scope.toList = []; // Only if multiple
    $scope.addRecipient = function (contact) {
        $scope.keyword = "";
        $scope.contactList = [];
        $scope.assigneeBucket = [];
        $scope.toList.push(contact);
        $(".addPeople").val("")
    };

    $scope.selectAssignee = function (contact) {
        $scope.assignList = [];
        $scope.keyword = "";
        $scope.contactList = [];
        $scope.assigneeBucket = [];
        $scope.assignList.push(contact);
        $(".addPeople").val("")
    };

    $scope.startDateFormatted = moment().format("DD MMM YYYY")
    $scope.dueDateFormatted = "Select"

    $scope.removeFromToList = function (contact) {
        $scope.toList = $scope.toList.filter(function (item) {
            return item.emailId != contact.emailId;
        })
    }
    
    $scope.closeTaskCreator = function () {
        $scope.createNewTask = false
        $rootScope.createFormIsOpen = false
    }

    $scope.removeFromAssigneeList = function (contact) {
        $scope.assignList = $scope.assignList.filter(function (item) {
            return item.emailId != contact.emailId;
        })
    }

    closeAllDropDownsAndModals($scope,".search-box-wrapper")

    $scope.pickStartDate = function () {
        pickDateTime($scope,new Date(),null,"#startDate",false,"startDate",false)
    }

    $scope.pickDueDate = function () {
        pickDateTime($scope,new Date(),null,"#dueDate",false,"dueDate",false)
    }

    $scope.searchContacts = function (keyword,onlyOne) {
        getContact($scope,$http,share,keyword,searchContacts,function (response) {
            var bucket = "contactList";
            if(onlyOne){
                bucket = "assigneeBucket"
            }
            
            populateList($scope,response,bucket);
        })
    }

    $scope.task = {
        taskName:"",
        description:"",
        priority:null,
        startDate:$scope.startDate,
        dueDate:$scope.dueDate,
        toList:$scope.assignList,
        participants:$scope.toList,
        taskFor:"other"
    }

    $scope.saveTask = function () {

        $scope.task.startDate = $scope.startDate
        $scope.task.dueDate = $scope.dueDate
        $scope.task.assignedToEmailId = $scope.assignList && $scope.assignList[0]?$scope.assignList[0].emailId:null;
        $scope.task.participants = $scope.toList;
        $scope.task.taskFor = (/commit/.test(window.location.pathname))?"weeklyReview":"other";

        if((/opportunities/.test(window.location.pathname))){
            $scope.task.taskFor = "opportunities"
        }

        share.unhideCreateTaskBtn()

        createNew($scope,$http,share,$scope.task,function () {
            $scope.createNewTask = false;
            $scope.task = {
                taskName:"",
                description:"",
                priority:null,
                startDate:"",
                dueDate:"",
                toList:[],
                participants:[],
                taskFor:(/commit/.test(window.location.pathname))?"weeklyReview":"other"
            }
        })
    }

    share.assignListPrePopulate = function(user){
        $scope.assignList = [];
        if(user){
            $scope.assignList.push(user)
        }

    }

    share.createNew = function () {
        $("div[id^='taTextElement']").empty();
        $scope.createNewTask = true;
    }
    
});

function populateList($scope,response,bucket) {
    if(response.SuccessCode && response.Data.length>0){

        var contactsArray = response.Data.filter(function (res) {
            return res.personEmailId != null
        });

        $scope[bucket] = [];

        if(contactsArray.length>0){
            for(var i=0;i<contactsArray.length;i++){

                var obj = {};

                if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                    var name = getTextLength(contactsArray[i].personName,20);
                    var image = '/getImage/'+contactsArray[i].personId._id;

                    obj = {
                        fullName:contactsArray[i].personName,
                        name:name,
                        image:image
                    };

                    obj.emailId = contactsArray[i].personEmailId;
                    obj.twitterUserName = contactsArray[i].twitterUserName;
                    obj.mobileNumber = contactsArray[i].mobileNumber;

                }
                else {
                    var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                    obj = {
                        fullName: contactsArray[i].personName,
                        name: getTextLength(contactsArray[i].personName, 20),
                        image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                        emailId:contactsArray[i].personEmailId,
                        twitterUserName: contactsArray[i].twitterUserName,
                        mobileNumber: contactsArray[i].mobileNumber
                        // noPicFlag:true
                    };
                }

                if(obj.twitterUserName){
                    obj.tweetAccExists = true;
                }

                obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
                obj._id = contactsArray[i]._id;
                obj.personId = contactsArray[i].personId && contactsArray[i].personId._id?contactsArray[i].personId._id:null;

                $scope[bucket].push(obj)

            }
        }
    }
}

function createNew($scope, $http, share,task,callback) {

    if(validateTaskFields(task)){
        $http.post('/task/create/new/v2',task)
            .success(function (response) {
                if(response){
                    sendEmails($http,share,task);
                    if(share.getAllTasks){
                        share.getAllTasks(share.selectedUser,share.filterSelected,share.startDate,share.endDate);
                    }
                    if(callback){
                        callback(response)
                    }
                }
            });
    }
}

function sendEmails($http,share,task){

    var subject = "New task assigned on Relatas",
        body = "Hi \n",
        contactDetails = {
            contactEmailId:task.assignedToEmailId
        }
    getAndCacheTeamProfiles($http,share.l_user._id,function (teamDictionary) {

        if(teamDictionary[task.assignedToEmailId]) {
            body = body+teamDictionary[task.assignedToEmailId].fullName+",\n\n";
        }

        if(teamDictionary[share.l_user.emailId]){
            body =body+teamDictionary[share.l_user.emailId].fullName
                +" has assigned a "
                +task.priority
                +" priority task to you:"
                +"\n\n"
                +task.taskName;
        }

        body = body+"\n\n"+getSignature(share.l_user.firstName+' '+share.l_user.lastName,share.l_user.designation,share.l_user.companyName,share.l_user.publicProfileUrl)

        notifyRelevantPeople($http,subject,body,contactDetails)
    })
}

function reply($scope,$http,share,message,task) {
    var url = '/message/reply';
    task.messageReferenceType = "task";
    task.conversationId = task._id;
    task.emailId = task.assignedToEmailId;
    task.text = message;


    if(message && message.length>0){
        $http.post(url,task)
            .success(function (response) {
                task.text = ""
                if(response.SuccessCode){
                    $(".reply textarea").val("")
                    share.openDiscussion(task)
                }
            });
    } else {
        toastr.error("Pl. enter a message")
    }
}

function taskUpdatePriorities($scope,$http,share,task,toPriority) {
    task.priority = toPriority
    updateTask($scope, $http, share,task)
}

function taskUpdateStatus($scope,$http,share,task,toStatus) {

    task.statusFormatted = toStatus;
    if(toStatus == "Not started"){
        task.status = "notStarted"
    }

    if(toStatus == "Completed"){
        task.status = "complete"
    }

    if(toStatus == "In progress"){
        task.status = "inProgress"
    }

    updateTask($scope, $http, share,task)
}

function updateTask($scope, $http, share,task) {

    $http.post('/task/update/properties',task)
        .success(function (response) {
            if(response){
                share.getAllTasks();
                toastr.success("Task updated")
            }
        });
}

function deleteReply($scope, $http, share) {

}

function getMessagesForTask($scope,$http,share,$rootScope,conversationId,callback) {

    $http.get('/get/messages/by/conversation/id?conversationId='+conversationId)
        .success(function (response) {
            if(response.SuccessCode){
                buildMessages($scope,$http,share,response.Data)
            }
        })
}

function buildMessages($scope,$http,share,messages) {
    $scope.pastMessages = [];

    getAndCacheTeamProfiles($http,share.l_user._id,function (teamDictionary) {
            _.each(messages,function (msg) {
                var obj = msg;
                var isSender = false;
                if(share.l_user.emailId == msg.messageOwner.emailId){
                    isSender = true
                }
                var sentDateTime = moment(msg.date).format("DD MMM YYYY, h:mm a")
                obj.isSender = isSender;
                obj.sentDateTime = sentDateTime;
                obj.profile = teamDictionary[msg.messageOwner.emailId];
                $scope.pastMessages.push(obj)
            });

        $scope.pastMessages.sort(function (o2, o1) {
            return o1.date < o2.date ? 1 : o1.date > o2.date ? -1 : 0;
        });
    })
}

function getAllTasks($scope, $http, share,$rootScope,user,filter,start,end) {

    var url = '/tasks/get/all'

    if(user && filter){
        url = url+"?emailId="+user.emailId
        url = fetchUrlWithParameter(url, "filter", filter);
    } else if(user && user.emailId){
        url = url+"?emailId="+user.emailId+"&filter="+"weeklyReview"
    } else if(filter && !user){
        url = fetchUrlWithParameter(url, "filter", filter);
    }

    share.startDate = start;
    share.endDate = end;

    if(start && end){
        url = fetchUrlWithParameter(url, "start", start);
        url = fetchUrlWithParameter(url, "end", end);
    }

    $scope.graphLoading = true;

    $http.get(url)
        .success(function (response) {
            if(response.SuccessCode){
                $rootScope.taskStatus = response.Data.taskStatus;

                getAndCacheTeamProfiles($http,share.l_user._id,function (teamDictionary) {
                    buildTaskObjList($scope,response,share.l_user.emailId,teamDictionary, share)
                })
            } else {

            }
        })
}

function buildTaskObjList($scope,response,liuEmailId,teamDictionary,share) {

    $scope.tasksList = [];
    $scope.notStartedList = [];
    $scope.inProgressList = [];
    $scope.completedList = [];

    var tasksFor = getParams(window.location.href).for;    

    if(response.Data.tasks && response.Data.tasks.length>0){
        _.each(response.Data.tasks,function (el) {
            var obj = el;
            var assignType = "fa-arrow-left"; // incoming
            var assignTypeBg = "incoming"; // incoming
            var status = "In progress";
            var priorityStyle = {background:""}

            if(liuEmailId == el.createdByEmailId){
                assignType = "fa-arrow-right"
                assignTypeBg = "outgoing";
                obj["profile"] = teamDictionary[el.assignedToEmailId]
            } else if(_.includes(_.map(el.participants,"emailId"),liuEmailId)) {
                obj["profile"] = teamDictionary[el.assignedToEmailId]
            } else {
                obj["profile"] = teamDictionary[el.createdByEmailId]
            }

            var toObj = {
                emailId:el.assignedToEmailId,
                fullName:el.assignedToEmailId,
                noPicFlag:true,
                nameNoImg:el.assignedToEmailId.substring(0,2).toUpperCase(),
                noPPic:el.assignedToEmailId.substring(0,2).toUpperCase(),
                userId:null
            }

            obj.to = teamDictionary[el.assignedToEmailId]?teamDictionary[el.assignedToEmailId]:toObj
            obj.by = teamDictionary[el.createdByEmailId]

            obj.assignTypeBg = assignTypeBg
            obj.dueDateFormatted = moment(el.dueDate).format(standardDateFormat());
            obj.createdDateFormatted = moment(el.createdDate).format(standardDateFormat());
            obj.assignType = assignType;

            obj.otherParticipants = [];

            if(obj.participants && obj.participants.length>0){
                _.each(obj.participants,function (pt) {
                    obj.otherParticipants.push(teamDictionary[pt.emailId])
                })
            }

            obj.otherParticipants = _.uniqBy(obj.otherParticipants,"emailId")

            if(el.status == "notStarted"){
                status = "Not started"
                obj.statusFormatted = status
                $scope.notStartedList.push(obj)
            } else if(el.status == "complete" && tasksFor !== 'overdue'){
                status = "Completed"
                obj.statusFormatted = status
                $scope.completedList.push(obj)
            } else {
                obj.statusFormatted = status
                $scope.inProgressList.push(obj)
            }

        })
    }

    if( (tasksFor == 'overdue' || tasksFor == 'upcoming' || tasksFor == 'today')) {
        var liuEmailId = share.l_user.emailId;

        $scope.notStartedList = _.filter($scope.notStartedList, function(el) {
            return (el.to.emailId == liuEmailId);
        }) 

        $scope.inProgressList = _.filter($scope.inProgressList, function(el) {
            return (el.to.emailId == liuEmailId);
        }) 

        if(liuEmailId == "sureshhoel@gmail.com")
            $scope.inProgressList.pop();

        $scope.completedList = _.filter($scope.completedList, function(el) {
            return (el.to.emailId == liuEmailId);
        }) 
    }
    $scope.notStartedList.length>0?sortByDateTasks($scope.notStartedList):""
    $scope.inProgressList.length>0?sortByDateTasks($scope.inProgressList):""
    $scope.completedList.length>0?sortByDateTasks($scope.completedList):""
    $scope.graphLoading = false;
}

function sortByDateTasks(tasks){

    tasks.sort(function (o2, o1) {
        return o1.dueDate < o2.dueDate ? 1 : o1.dueDate > o2.dueDate ? -1 : 0;
    });
}

function getContact($scope,$http,share,keyword,searchContacts,callback) {
    if(keyword && keyword.length > 2){
        searchContacts.search(keyword).success(function(response){
            callback(response)
        })
    }
}

function validateTaskFields(task){

    var noErrors = true

    if(!task.assignedToEmailId){
        noErrors = false;
        toastr.error("Task needs to be assigned to at least one person")
    } else if(!task.dueDate){
        noErrors = false;
        toastr.error("Please set a due date")
    } else if(!task.taskName || task.taskName == "") {
        noErrors = false;
        toastr.error("Please set a title")
    } else if(!task.priority || task.priority == "") {
        noErrors = false;
        toastr.error("Please set a priority")
    }

    return noErrors;
}

function getAndCacheTeamProfiles($http,userId,callback){

    var key = userId+'teamMembers';

    if(!_.isEmpty(window.localStorage[key])){
        // console.log("Data Exists")
        try{
            // console.log("Trying to parse...")
            callback(JSON.parse(window.localStorage[key]));
        } catch (err){
            getCompanyProfiles($http,key,callback)
        }
    } else {
        // console.log("No data. Building data...")
        getCompanyProfiles($http,key,callback)
    }
}

function getCompanyProfiles($http,key,callback) {

    $http.get('/company/user/hierarchy')
        .success(function (response) {
            if (response && response.SuccessCode && response.Data && response.Data.length > 0) {

                var usersDictionary = {};
                if (response.companyMembers.length > 0) {
                    var allProfiles = buildTeamProfilesWithLiu(response.companyMembers)
                    _.each(allProfiles, function (member) {
                        usersDictionary[member.emailId] = member
                    });
                    window.localStorage[key] = JSON.stringify(usersDictionary);
                    callback(usersDictionary)
                }
            }
        });
}

function buildTeamProfilesWithLiu(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            hierarchyParent:el.hierarchyParent,
            corporateAdmin:el.corporateAdmin,
            firstName:el.firstName
        })
    });

    return team;
}

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}
