
$(document).ready(function(){
 var resetFlag = false;
    $("#submit").on("click",function(){
        var password = $("#password").val();
        var confPassword = $("#confPassword").val();
        password = password.trim();
        confPassword = confPassword.trim();
        var data = {
            password:password
        }
        if(!checkRequredField(password) || !checkRequredField(confPassword)){
                showMessagePopUp("Please provide password");
        }
        if(password == confPassword){

            $.ajax({
                url:'/resetPasswordPost',
                type:'POST',
                datatype:'JSON',
                data:data,
                success:function(result){
                    if(result){
                        resetFlag = true;
                        showMessagePopUp("Password reset successful. You may now login to relatas.com with your new password.",'success')
                    }
                    else{
                        showMessagePopUp("Password reset failed, Please try again.",'error');
                    }
                }
            })
        }
        else{
            showMessagePopUp("Passwords mismatch. Please re-enter password.",'error');
        }
    });

    function checkRequredField(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    };

    function showMessagePopUp(message,msgClass)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':'9%'})
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        //setTimeout(function(){
        $("#message").text(message)
        //},1000);
        $("#message").focus();
    }

    $("body").on("click","#closePopup-message",function(){
        $("#user-name").popover('destroy');
        if(resetFlag){
            resetFlag = false;
            window.location.replace('/');
        }
    });

});
