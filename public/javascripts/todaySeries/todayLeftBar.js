
var todayLeftSectionApp = angular.module('todayLeftSectionApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);


todayLeftSectionApp.controller("left_bar_today", function ($scope, $http) {
    $scope.confirmedMeetings = [];
    $http.get('/today/left/section')
        .success(function (response) {
            if(response.SuccessCode){
                if(checkRequired(response.Data.confirmedMeetings != undefined && response.Data.confirmedMeetings.length > 0)){
                    for(var i=0; i<response.Data.confirmedMeetings.length; i++){
                        if(response.Data.confirmedMeetings[i].suggested){

                        }
                        else{

                        }
                    }
                }
            }
            else{

            }
        }).error(function (data) {

        });

    $scope.formatMeetingDetails = function(){

    }
});
