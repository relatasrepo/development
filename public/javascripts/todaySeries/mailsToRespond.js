
relatasApp.controller("mailsActionBoardController", function ($scope,$http,sharedServiceSelection,meetingObjService,searchService,$rootScope) {

    $scope.$on('handleEmailBroadcast', function() {

        $scope.dataLoaded = false;
        $scope.mailDetails = sharedServiceSelection.mailDetails;

        if($scope.mailDetails.openForm){
            $scope.isReplyFormOpen = true;
            $scope.replyStatus = "Cancel";
        } else {
            $scope.replyStatus = "Reply";
        }

        if($scope.mailDetails.designation && $scope.mailDetails.companyName){
            $scope.mailDetails.companyProfile = $scope.mailDetails.designation +', '+ $scope.mailDetails.companyName
        }

        if($scope.mailDetails.designation && !$scope.mailDetails.companyName){
            $scope.mailDetails.companyProfile = $scope.mailDetails.designation
        }

        if(!$scope.mailDetails.designation && $scope.mailDetails.companyName){
            $scope.mailDetails.companyProfile = $scope.mailDetails.companyName
        }

        var emailId = sharedServiceSelection.mailDetails.emailId;

        showEmail($scope,$http,sharedServiceSelection.mailDetails);

        nonSocialInteractions($scope,$http,emailId,$scope.timezone,function (nonSocialInteractions) {

            socialInteractions($scope,$http,emailId,$scope.timezone,function (socialInteractions) {

                $scope.timelineItems = nonSocialInteractions.concat(socialInteractions);
                $scope.timelineItems.sort(function (o1, o2) {
                    return new Date(o2.dataObj.interaction.interactionDate) - new Date(o1.dataObj.interaction.interactionDate)
                });

                $scope.showMoreInteractions = false;

                if($scope.timelineItems.length >= 5){
                    $scope.showMoreInteractions = true;
                }

                $scope.currentPageInteractions = 0;
                $scope.pageSizeInteractions = 5;
                $scope.numberOfInteractions = $scope.timelineItems.length;
                $scope.numberOfPages=function(){
                    return Math.ceil($scope.timelineItems.length/$scope.pageSizeInteractions);
                };

                $scope.dataLoaded = true;

            });
        });

        var userId = sharedServiceSelection.mailDetails.userId?sharedServiceSelection.mailDetails.userId:null;

        if(!userId){
            userId = emailId;
        }

        $rootScope.ifActionMail = false;

        if((/action/.test(window.location.pathname))){
            $rootScope.ifActionMail = true;
        }

        $scope.markAsSpam = function (contact) {
            var ask = window.confirm("Are you sure you want to mark this contact as spam?");
            if (ask) {
                console.log("Greate");
            } else {
                console.log('oopsie = ');

            }
        }
        
        function markSpam(emailId) {
            var list = [emailId];
            $http.post("/contact/add/to/invalid/list",{list:list})
                .success(function(response){
                    if(response){
                        toastr.success("Contact successfully added to spam list");
                    }
                    else{
                        toastr.error("Please try again later");
                    }
                });
            
        }

        $scope.searchContacts = function(keywords){

            if(keywords && keywords.length > 2){

                searchService.search(keywords).success(function(response){
                    if(response.SuccessCode){
                        $scope.showResults = true;
                        processSearchResults2($scope,$http,response.Data);
                    } else {
                        $scope.showResults = true;
                        $scope.contacts = [];
                        var obj = {
                            fullName: '',
                            name: '',
                            image: '',
                            emailId:keywords
                        }
                        $scope.contacts.push(obj)
                    }
                }).error(function(){
                    toastr.error("Something went wrong. Please try again");
                });
            } else {
                $scope.contacts = null;
            }
        };

        $scope.add_cc = [];

        $scope.addCCToList = function (contact) {
            var emailId = contact.emailId
            if(validateEmail(emailId)){
                $scope.add_cc.push(emailId)
                $scope.contacts = [];
                $scope.showResultscc = false;
                $scope.email_cc = ''
            } else {
                toastr.error("Please enter a valid email ID")
            }
        }

        $scope.removeCC = function(emailId){
            var rmIndex = $scope.add_cc.indexOf(emailId);
            $scope.add_cc.splice(rmIndex, 1);
        }

        var signature = ""
        if(meetingObjService.liuData){
            signature = "\n\n\n"+getSignature(meetingObjService.liuData.firstName + ' '+ meetingObjService.liuData.lastName,
                    meetingObjService.liuData.designation,
                    meetingObjService.liuData.companyName,
                    meetingObjService.liuData.publicProfileUrl)
        }
        
        $scope.replyContent = signature

        if(userId){
            $scope.numberOfContacts = 0;
            var url ='/contacts/filter/common/web?id='+userId+'&skip=0&limit=36';
            getCommonConnections($scope, $http, url, false);
        }

        $http.get('/profile/get/current/web')
            .success(function (response) {

                var liuEmailId = response.Data.emailId;

                var url ='/contacts/filter/common/companyName/web/v2?companyName='
                    +response.Data.companyName+'&emailId='
                    +liuEmailId+'&fetchWith='
                    +emailId+'&skip=0'
                    +'&limit=36';

                getCommonConnections_company($scope, $http, url, false);

            });

        var tempCounter = 99;

        $scope.donutChartId = $scope.mailDetails.actionItemId?$scope.mailDetails.actionItemId:tempCounter++;
        
        $scope.interaction_initiations_other_name = sharedServiceSelection.mailDetails?sharedServiceSelection.mailDetails.name:'';

        interactionsGraphs($scope,$http,emailId,$scope.donutChartId)

    });
    $scope.$emit('handleEmailBroadcast', null);

    /*
     *     $scope.$emit('handleEmailBroadcast', null) initializes the child controllers.
     *     This resolves a bug wherein the user has to click twice for any controller to reload
     *
     * */

    $scope.replyToMail = function (mailDetails,replyContent,bodyContent) {
        replyToMail($scope,$http,mailDetails,replyContent,bodyContent,sharedServiceSelection);
    };

    $scope.isReplyFormOpenFun =function (replyStatus) {
        if(replyStatus == "Cancel") {
            $scope.replyStatus = "Reply";
        } else {
            $scope.replyStatus = "Cancel";
        }
        $scope.isReplyFormOpen = !$scope.isReplyFormOpen;
    };

    $scope.goToContact = function (emailId,mobileNumber) {

        if(checkRequired(emailId) && checkRequired(mobileNumber) && mobileNumber !='undefined'){
            window.location = "/contacts/all?contact="+emailId+"&acc=true"
        } else {
            window.location = "/contacts/all?contact="+emailId+"&acc=true"
        }
    };


});

relatasApp.controller("mailsToRespondController", function ($scope,$http,$rootScope,sharedServiceSelection,teamMembers) {

    if(window.innerWidth>720 && window.innerWidth<1200){
        $rootScope.globalPageSize = 4;
    } else if(window.innerWidth>1200){
        $rootScope.globalPageSize = 5;
    } else if(window.innerWidth<480){
        $rootScope.globalPageSize = 2;
    } else {
        $rootScope.globalPageSize = 3;
    }

    $scope.goto = function (to) {
        window.location = '/insights/mails'
    }

    sharedServiceSelection.fetchModuleShared = function(filter){

        switch (filter) {
            case 'travellingToLocation':
                travellingToLocation($scope,$http,filter);
                break;
            case 'responsePendingAndImportant':
                mailsToRespond($scope,$http,filter);
                break;
            case 'reminderToConnect':
                reminderToConnect($scope,$http,filter,$rootScope);
                break;
            case 'meetingFollowUp':
                meetingFollowUp($scope,$http,filter,$rootScope);
                break;
        }
    };

    $scope.loadingMailsToRespond = true;
    $scope.loadingttl = true;

    $scope.fetchModule = function (filter) {

        switch (filter) {
            case 'travellingToLocation':
                setTimeOutCallback(4000,function () {
                    travellingToLocation($scope,$http,filter);
                });
                break;
            case 'responsePendingAndImportant':
                setTimeOutCallback(1,function () {
                    mailsToRespond($scope,$http,filter);
                });
                break;
            case 'reminderToConnect':
                reminderToConnect($scope,$http,filter,$rootScope);
                break;
            case 'meetingFollowUp':
                meetingFollowUp($scope,$http,filter,$rootScope,sharedServiceSelection);
                break;
            case 'losingTouch':
                losingTouch($scope,$http,filter,teamMembers,$rootScope);
                break;
        }
    };

    // $scope.multiAction = [];

    // showNumberOfMails($scope,$http,'followUp');

    $scope.openEmailForm = function (contact,index,type,isLastIndex) {

        // $scope.multiAction.push(contact.emailId)

        if(!contact.isMore) {
            closeAllActionBoard($rootScope,function (result) {

                $rootScope.sendEmailBoardToggle = true;

                sharedServiceSelection.emailForm(contact)
                openEmailForm($scope,$http,$rootScope,sharedServiceSelection,contact);
            });

        } else {
            window.location = '/insights/v3'
        }
    };

    $scope.actionBoardClose = function () {
        closeAllActionBoard($rootScope)
        $rootScope.mailSelected = null;
    };

    var timeOutCounter = 1;

    $scope.fetchContactsAtThisLocation = function (location,participants,item) {

        setTimeOutCallback(timeOutCounter*1000,function () {
            timeOutCounter++;
            fetchContactsAtThisLocation($scope,$http,location,participants,item,$rootScope)
        });
    };
    
    $scope.resetSelection = function () {
        $rootScope.mailSelected = null;
        $scope.contactSelected = null;
    };
    
    $scope.ignoreInsight = function (item) {
        ignoreInsight($scope,$http,sharedServiceSelection,item);
    }
    
    $scope.showDistances = function (item) {
        showDistances($scope,$http,$rootScope,item);
    }

    //Travelling to load all.
    $scope.loadAll = function () {
        $scope.notLoadAll = true;
        travellingToLocation($scope,$http,null,true)
    };
    
    $scope.loadAllResponsePending = function (filter) {
        window.location = '/insights/mail/action';
        mailsToRespond($scope,$http,filter,true)
    };
    
    $(window).resize(function(){

        if(window.innerWidth>720 && window.innerWidth<1200 && $rootScope.globalPageSize != 4){
            $rootScope.globalPageSize = 4;
            repaintAllModule($scope)
        } else if(window.innerWidth>1200 && $rootScope.globalPageSize != 5){
            $rootScope.globalPageSize = 5;
            repaintAllModule($scope)
        } else if(window.innerWidth<480 && $rootScope.globalPageSize != 2){
            $rootScope.globalPageSize = 2;
            repaintAllModule($scope)
        } else {
            $rootScope.globalPageSize = 3;
            repaintAllModule($scope)
        }
    });

});

function repaintAllModule($scope) {
    $scope.fetchModule("travellingToLocation")
    $scope.fetchModule("reminderToConnect")
    $scope.fetchModule("meetingFollowUp")
    $scope.fetchModule("losingTouch")
}

function ignoreInsight($scope,$http,sharedServiceSelection,item) {
    updateActionTakenDetails($scope,$http,sharedServiceSelection,item,'ignore')
}

var counter = 1;

relatasApp.controller("emailFormController",function ($scope,$http,$rootScope,sharedServiceSelection,searchService) {

    $scope.$on('handleEmailFormBroadcast', function() {

        $scope.donutChart = 'donut-chart'+counter++;
        $scope.dataLoaded = false;
        $scope.contactDetails = null;
        $scope.contactDetails = sharedServiceSelection.contactDetails;

        $scope.runScopes = function () {

            $scope.searchedContacts = [];

            if($scope.contactDetails.participants){
                for(var i = 0;i<$scope.contactDetails.participants.length>0;i++){
                    if($scope.contactDetails.emailId != $scope.contactDetails.participants[i].emailId){
                        $scope.searchedContacts.push($scope.contactDetails.participants[i].emailId);
                    }
                }
            }

            if($scope.searchedContacts.length>0){
                $scope.addCC = true;
            }
        }

        $scope.runScopes();

        if($scope.contactDetails.filter === 'losingTouch'){
            $scope.lastMeetingInfo = "You haven't met "+$scope.contactDetails.fullName+" in person before.";
            $scope.lastEmailInfo = "You last "
            processLosingTouch($scope,$http,$rootScope,$scope.contactDetails)
        }

        $scope.sendMeetingInvite = function () {

            $scope.compose_email_subject1 = $scope.contactDetails.fullName+ ", Let's Meet."
            var calendarId = 'https://relatas.com/'+$scope.contactDetails.publicProfileUrl;

            $scope.openEmailContainer = !$scope.openEmailContainer;
            $scope.compose_email_body1 = "Hi, \n"+"It's been long since we last interacted." +
                "It would be great to find some time to catch up. My Calendar is available here:\n"
            +calendarId
        };

        $scope.openEmailContainer = true;

        //TODO
        $scope.formLoading = true;

        var cName = $scope.contactDetails.fullName.split(" ");
        cName = cName[0];

        var fetchWith = $scope.contactDetails.emailId
        if($scope.contactDetails.personId){
            fetchWith = $scope.contactDetails.personId
        }

        get_connection_interactions_tweets($http,fetchWith,$scope.contactDetails.twitterUserName,null,cName,null,null,$scope.contactDetails.filterToFetch,$scope.contactDetails.location,$scope.contactDetails.meetingDate,$scope.contactDetails.title,function (response,message) {

            $scope.formLoading = true;

            $scope.compose_email_subject = message.subject;
            $scope.compose_email_subject1 = message.subject;
            $scope.compose_email_body = {msg:message.messageReConnect};
            $scope.compose_email_body1 = {msg:message.messageReConnect};
        });

        $scope.openEmailFormLt = function () {
            // $scope.compose_email_body1 = '';
            // $scope.compose_email_subject1 = ''
            $scope.openEmailContainer = !$scope.openEmailContainer;
        };

        $scope.reactToTweet = function (tweetId,status,action) {
            reactToTweet($http,{tweetId:tweetId},status,action)
        }

        if($scope.contactDetails.designation && $scope.contactDetails.companyName){
            $scope.contactDetails.companyProfile = $scope.contactDetails.designation +', '+ $scope.contactDetails.companyName
        }

        if($scope.contactDetails.designation && !$scope.contactDetails.companyName){
            $scope.contactDetails.companyProfile = $scope.contactDetails.designation
        }

        if(!$scope.contactDetails.designation && $scope.contactDetails.companyName){
            $scope.contactDetails.companyProfile = $scope.contactDetails.companyName
        }
        
        // if($scope.contactDetails.filter === 'reminderToConnect') {

            $scope.interaction_initiations_other_name2 = $scope.contactDetails.name

            var emailId = $scope.contactDetails.emailId;

            nonSocialInteractions($scope,$http,emailId,$scope.timezone,function (nonSocialInteractions) {

                socialInteractions($scope, $http, emailId, $scope.timezone, function (socialInteractions) {

                    $scope.dataLoaded = true;
                    $scope.pastInteractions = nonSocialInteractions.concat(socialInteractions)
                    $scope.pastInteractions.sort(function (o1, o2) {
                        return new Date(o2.dataObj.interaction.interactionDate) - new Date(o1.dataObj.interaction.interactionDate)
                    });

                    $scope.pastInteractions = $scope.pastInteractions.slice(0,5);
                })
            });

        if(emailId){
            var url ='/contacts/filter/common/web?id='+fetchWith+'&skip=0&limit=36';
            getCommonConnections($scope, $http, url, false);
            interactionsGraphs($scope,$http,emailId,$scope.donutChart)
        }

    });
    $scope.$emit('handleEmailFormBroadcast', null);

    $scope.sendEmail = function(subject,body,trackViewed,remind,newMessage,contact){
        var email_cc = $scope.searchedContacts.length>0?$scope.searchedContacts:null

        sendEmail($scope,$http,$rootScope,subject,body,trackViewed,remind,newMessage,contact,sharedServiceSelection,email_cc)

        if(contact.filter === "losingTouch") {
            insertLosingTouchActionTaken($scope,$http,sharedServiceSelection,contact)
        } else {
            updateActionTakenDetails($scope,$http,sharedServiceSelection,contact,"sendEmail")
        }

    };
    
    $scope.openEmilContainer = function () {
        closeAllActionBoard($rootScope)
    }

    $scope.goToContact = function (emailId,mobileNumber) {
        if(checkRequired(emailId) && checkRequired(mobileNumber) && mobileNumber !='undefined'){
            window.location = "/contacts/all?contact="+emailId+"&acc=true"
        } else {
            window.location = "/contacts/all?contact="+emailId+"&acc=true"
        }
    }

    $scope.searchContacts = function(keywords){

        if(keywords && keywords.length > 2){

            searchService.search(keywords).success(function(response){
                if(response.SuccessCode){
                    $scope.showResults = true;
                    processSearchResults2($scope,$http,response.Data);
                } else {
                    $scope.showResults = true;
                    $scope.contacts = [];
                    var obj = {
                        fullName: '',
                        name: '',
                        image: '',
                        emailId:keywords
                    }
                    $scope.contacts.push(obj)
                }
            }).error(function(){
                toastr.error("Something went wrong. Please try again");
            });
        } else {
            $scope.contacts = null;
        }
    };

    $scope.searchedContacts = [];

    $scope.addRecipient = function (contact) {
        var emailId = contact.emailId
        if(validateEmail(emailId)){
            $scope.searchedContacts.push(emailId)
            $scope.contacts = [];
            $scope.showResults = false;
            $scope.email_cc = ''
        } else {
            toastr.error("Please enter a valid email ID")
        }
    }

    $scope.removeRecipient = function(emailId){
        var rmIndex = $scope.searchedContacts.indexOf(emailId);
        $scope.searchedContacts.splice(rmIndex, 1);
    }

});

function processSearchResults2($scope,$http,response) {

    var contactsArray = response;
    $scope.contacts = [];
    $scope.ccs = [];

    if(contactsArray.length>0){
        for(var i=0;i<contactsArray.length;i++){

            var obj = {};

            if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                var name = getTextLength(contactsArray[i].personName,20);
                var image = '/getImage/'+contactsArray[i].personId._id;

                obj = {
                    fullName:contactsArray[i].personName,
                    name:name,
                    image:image
                };

                obj.emailId = contactsArray[i].personEmailId;
                obj.twitterUserName = contactsArray[i].twitterUserName;

            }
            else {
                var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                obj = {
                    fullName: contactsArray[i].personName,
                    name: getTextLength(contactsArray[i].personName, 20),
                    image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                    emailId:contactsArray[i].personEmailId,
                    twitterUserName: contactsArray[i].twitterUserName
                    // noPicFlag:true
                };
            }

            if(obj.twitterUserName){
                obj.tweetAccExists = true;
            }

            obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
            obj._id = contactsArray[i]._id;


            if(!userExists(obj.name) && validateEmail(obj.emailId)){
                $scope.contacts.push(obj)
                $scope.ccs.push(obj)
            }
            
            $scope.showResultscc = $scope.ccs.length>0;

            function userExists(username) {
                return $scope.contacts.some(function(el) {
                    return el.name === username;
                });
            }
        }
    }
}

function openEmailForm($scope,$http,$rootScope,sharedServiceSelection,contact) {

    $scope.title = contact.title;
    $scope.description = contact.description;

    $rootScope.actionBoardToggle = false;
    sharedServiceSelection.emailBroadcast(contact);

}

function fetchContactsAtThisLocation($scope,$http,location,participants,item,$rootScope) {
    var url = '/fetch/contacts/travelling/to?location='+location.replace(/[^\w\s]/gi, '')+'&invitationId='+item.invitationId;
    url = fetchUrlWithParameter(url, 'participants', participants);

    $http.get(url)
        .success(function(response){
            if(response.SuccessCode && response.Data.contacts[0] && response.Data.contacts[0].contacts.length>0) {

                var contactsArray = response.Data.contacts[0].contacts;

                item.location = response.Data.city;
                item.sublocality = response.Data.sublocality?' '+response.Data.sublocality+', ':'';

                var contactsAtLocation = [];
                for(var i=0;i<contactsArray.length;i++){

                    var obj = {};

                    if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                        var name = getTextLength(contactsArray[i].personName,10);
                        var image = '/getImage/'+contactsArray[i].personId;

                        obj = {
                            fullName:contactsArray[i].personName,
                            name:name,
                            image:image,
                            designation:contactsArray[i].designation,
                            companyName:contactsArray[i].companyName
                        };

                        obj.emailId = contactsArray[i].personEmailId;

                    }
                    else {
                        var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                        obj = {
                            fullName: contactsArray[i].personName,
                            name: getTextLength(contactsArray[i].personName, 10),
                            image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                            emailId:contactsArray[i].personEmailId,
                            designation:contactsArray[i].designation,
                            companyName:contactsArray[i].companyName
                            // noPicFlag:true
                        };
                    }

                    obj.isImportant = contactsArray[i].favorite;
                    obj.personId = contactsArray[i].personId;
                    obj.actionItemId = contactsArray[i].actionItemId;
                    obj.favoriteClass = contactsArray[i].favorite?'contact-fav':false;
                    obj.filterToFetch = 'travellingToLocation';
                    obj.location = item.sublocality + item.location
                    obj.meetingDate = item.meetingDate
                    obj.twitterUserName = contactsArray[i].twitterUserName
                    obj.distanceToPrevMeetingLocation = contactsArray[i].pastMeetingLocation?'/fetch/distance/matrix?from='+location+"&destination="+contactsArray[i].pastMeetingLocation:null

                    var contactCompany = fetchCompanyFromEmail(contactsArray[i].personEmailId);
                    var liuCompanyName = fetchCompanyFromEmail(contactsArray[i].ownerEmailId);

                    if(liuCompanyName == "Others") {
                        liuCompanyName = null;
                    }

                    if(liuCompanyName != contactCompany){
                        contactsAtLocation.push(obj);
                    }

                }

                $rootScope.peopleExist = contactsAtLocation.length>0

                item.currentPage = 0;
                item.pageSize = $rootScope.globalPageSize;
                item.numberOfContactsAtLocation = contactsAtLocation.length;
                item.numberOfPages = function(){
                    return Math.ceil(item.numberOfContactsAtLocation/item.pageSize);
                };

                contactsAtLocation.sort(function (o1, o2) {
                    return (o1.isImportant === o2.isImportant)? 0 : o1.isImportant? -1 : 1;
                });

                item.contactsAtLocation = _.uniqBy(contactsAtLocation,"emailId");

            } else {
                item.collapse = true;
                item.location = response.Data.city;
                item.numberOfContactsAtLocation = 0;
                item.sublocality = response.Data.sublocality?' '+response.Data.sublocality+', ':'';
            }

        });

}

function sendEmail($scope,$http,$rootScope,subject,body,trackViewed,remind,newMessage,contact,sharedServiceSelection,email_cc) {

    if(!checkRequired(subject)){
        toastr.error("Please enter an email subject.")
    } else if(!checkRequired(body)){
        toastr.error("Please enter an email body.")
    } else{

        var obj = {
            email_cc:email_cc,
            receiverEmailId:contact.emailId,
            receiverName:contact.name,
            message:body,
            subject:subject,
            docTrack:false,
            trackViewed:true,
            remind:remind,
            newMessage:newMessage?newMessage:false
        };

        $http.post("/messages/send/email/single/web",obj)
            .success(function(response){
                if(response.SuccessCode){
                    updateActionTakenDetails($scope,$http,sharedServiceSelection,contact,'sendEmail')
                    toastr.success(response.Message);
                }
                else{
                    toastr.error(response.Message);
                }
            })
    }
}

function mailsToRespond($scope,$http,filter,loadAll) {

    var url = '/insights/mail/actions/meta';
    
    $http.get(url)
        .success(function(response){

            $scope.loadingMailsToRespond = false;
            // response.Data.important,response.Data.onlyToMe,response.Data.followUp

            $scope.onlyToMe = response.Data.onlyToMe
            $scope.numberOfFollowUp = response.Data.followUp
            $scope.numberOfImportant = response.Data.important

        });

    // paintGraphResponsePending('today',$scope,$http);
}

function paintGraphResponsePending(chartFor,$scope,$http) {
    var days = 14;
    var chartId = '#chartResponsePending15Days';

    if(chartFor == 'today'){
        days = 0;
        chartId = '#chartResponsePendingToday';
    }

    $http.get('/insights/your/responses/pending?days=7&filter=internalAndExternal&getPage=1&infoFor=detailsPage').then(function(resp) {
        if (resp.data.SuccessCode) {
            $scope.totalMails = resp.data.Data.explain.length;
        }
    });
};

function travellingToLocation($scope,$http,filter,loadAll) {

    var url = '/fetch/upcoming/meeting/locations';

    //TODO don't show base location contacts to meet.
    $http.get(url)
        .success(function(response){

            $scope.loadingttl = false;
            $scope.meetingsLocations = []
            var timezone = 'UTC';
            if(response.timezone){
                timezone = response.timezone
            }

            if(response.SuccessCode){
                for(var i=0;i<response.Data.meetingsLocations.length;i++){

                    $scope.meetingsLocations.push({
                        meetingDate:moment(response.Data.meetingsLocations[i].date).tz(timezone).format("DD MMM YYYY"),
                        locationTrunc:getTextLength(response.Data.meetingsLocations[i].location,10),
                        fullLocation:response.Data.meetingsLocations[i].location,
                        participants:response.Data.meetingsLocations[i].participants,
                        invitationId:response.Data.meetingsLocations[i].invitationId,
                        sortDate:new Date(response.Data.meetingsLocations[i].date)
                    })
                }

                $scope.meetingsLocations.sort(function (o1, o2) {
                    return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                });

                $scope.meetingsLocations = loadAll?$scope.meetingsLocations:$scope.meetingsLocations.slice(0,3);

            }
        });
}

function reminderToConnect($scope,$http,filter,$rootScope) {

    var url = '/fetch/remind/to/connect';

    $http.get(url)
        .success(function(response){

            var timezone = 'UTC';
            if(response.timezone){
                timezone = response.timezone
            }

            var contactsArray = response.Data;

            var contactsWithRTC = [];
            for(var i=0;i<contactsArray.length;i++){

                var obj = {};

                if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                    var name = getTextLength(contactsArray[i].personName,12);
                    var image = '/getImage/'+contactsArray[i].personId;

                    obj = {
                        fullName:contactsArray[i].personName,
                        name:name,
                        image:image,
                        companyName:contactsArray[i].company,
                        designation:contactsArray[i].designation
                    };

                    obj.emailId = contactsArray[i].contactEmail;
                    obj.recordId = contactsArray[i].contactId;
                    obj.filter = 'reminderToConnect';

                }
                else {
                    var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                    obj = {
                        fullName: contactsArray[i].personName,
                        name: getTextLength(contactsArray[i].personName, 12),
                        image: '/getContactImage/' + contactsArray[i].contactEmail + '/' + contactImageLink,
                        emailId:contactsArray[i].contactEmail,
                        // noPicFlag:true
                        recordId : contactsArray[i].contactId,
                        filter:'reminderToConnect',
                        companyName:contactsArray[i].company,
                        designation:contactsArray[i].designation
                    };
                }

                obj.actionItemId = contactsArray[i].actionItemId;
                obj.filterToFetch = 'reminderToConnect';
                obj.personId = contactsArray[i].personId;
                obj.twitterUserName = contactsArray[i].twitterUserName

                contactsWithRTC.push(obj);
            }

            //Pagination

            $scope.currentPage = 0;
            $scope.pageSize = $rootScope.globalPageSize;
            $scope.numberOfRTC = contactsWithRTC.length;
            $scope.numberOfPages = function(){
                return Math.ceil($scope.numberOfRTC/$scope.pageSize);
            };

            $scope.contactsWithRTC = contactsWithRTC;

        });

}

function losingTouch($scope,$http,filter,teamMembers,$rootScope) {

    teamMembers.getTeamMembers()
        .then(function(companyData) {

            var url = '/insights/losing/touch/info/by/relation';
            var obj = companyData.Data.companyMembers;
            var companyArr = Object.keys(obj).map(function (k) {
                return obj[k]
            });

            url = fetchUrlWithParameter(url, "companyMembers", companyArr);
            url = fetchUrlWithParameter(url, 'limit', true);

            var hierarchyList = [];
            hierarchyList.push(companyData.Data.userId);
            url = fetchUrlWithParameter(url, 'hierarchyList', hierarchyList);
            url = fetchUrlWithParameter(url, 'getPage', 1);
            
            $http.get('/insights/losing/touch/info/by/relation')
                .success(function(response){
                    var contactsArray = response.Data;

                    var contactsLosingTouch = [];
                    var atLeastOneImportantContact = false;
                    for(var i=0;i<contactsArray.length;i++){

                        var obj = {};

                        if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].contactName)){

                            var name = getTextLength(contactsArray[i].contactName,12);
                            var image = '/getImage/'+contactsArray[i].personId;

                            obj = {
                                fullName:contactsArray[i].contactName,
                                name:name,
                                image:image,
                                companyName:contactsArray[i].company,
                                designation:contactsArray[i].designation
                            };

                            obj.emailId = contactsArray[i].contactEmailId;
                            obj.recordId = contactsArray[i].contactId;
                            obj.filter = 'losingTouch';

                        }
                        else {
                            var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                            obj = {
                                fullName: contactsArray[i].contactName,
                                name: getTextLength(contactsArray[i].contactName, 12),
                                image: contactImageLink?'/getContactImage/' + contactsArray[i].contactEmailId + '/' + contactImageLink:null,
                                emailId:contactsArray[i].contactEmailId,
                                // noPicFlag:true
                                recordId : contactsArray[i].contactId,
                                filter:'losingTouch',
                                companyName:contactsArray[i].company,
                                designation:contactsArray[i].designation
                            };
                        }

                        obj.personId = contactsArray[i].personId;
                        obj.nameNoImg = contactsArray[i].personName?contactsArray[i].personName.substr(0,2).toUpperCase():''
                        obj.actionItemId = contactsArray[i].actionItemId;
                        obj.filterToFetch = 'losingTouch';
                        obj.twitterUserName = contactsArray[i].twitterUserName;
                        obj.publicProfileUrl = contactsArray[i].publicProfileUrl;
                        obj.lastInteractionDate = contactsArray[i].lastInteractionDate;
                        obj.favoriteClass = contactsArray[i].favorite?'contact-fav':false;
                        obj.isImportant = contactsArray[i].favorite;

                        if(contactsArray[i].favorite){
                            atLeastOneImportantContact = true;
                        }

                        var contactCompany = fetchCompanyFromEmail(contactsArray[i].contactEmailId);
                        var liuCompanyName = fetchCompanyFromEmail(contactsArray[i].ownerEmailId);

                        if(liuCompanyName == "Others") {
                            liuCompanyName = null;
                        }

                        // if(contactsArray[i].contactEmailId && liuCompanyName != contactCompany && contactsArray[i].lastInteractionDays>29){
                        if(contactsArray[i].contactEmailId && contactsArray[i].lastInteractionDays>29){
                            contactsLosingTouch.push(obj);
                        }

                    }

                    if(atLeastOneImportantContact){
                        contactsLosingTouch.sort(function (o1, o2) {
                            return (o1.isImportant === o2.isImportant)? 0 : o1.isImportant? -1 : 1;
                        });
                    } else {
                        contactsLosingTouch.sort(function (o1, o2) {
                            return new Date(o1.lastInteractionDate) < new Date(o2.lastInteractionDate) ? -1 : new Date(o1.lastInteractionDate) > new Date(o2.lastInteractionDate) ? 1 : 0;
                        });
                    }

                    contactsLosingTouch.push({
                        isMore:true
                    })

                    //Pagination

                    $scope.currentPage = 0;
                    $scope.pageSize = $rootScope.globalPageSize;
                    $scope.numberOfContactsLosingTouch = contactsLosingTouch.length;
                    $scope.numberOfPages = function(){
                        return Math.ceil($scope.numberOfContactsLosingTouch/$scope.pageSize);
                    };

                    $scope.contactsLosingTouch = contactsLosingTouch;

                });
        });

};

function meetingFollowUp($scope,$http,filter,$rootScope,sharedServiceSelection) {
    
    var meeting = _.filter($scope.meetingsToday,function (meeting) {
        return meeting.ifRelatasMailEventType === "meetingFollowUp"
    })

    var meetingId = meeting[0]?meeting[0].invitationId:null;

    $scope.loadingFollowup = true;
    var url ='/fetch/yesterdays/meetings?invitationId='+meetingId;

    $http.get(url)
        .success(function(response){

            console.log(response);

            var contactsArray = response;
            var meetingFollowup = [];

            if(contactsArray.length){

                for(var i=0;i<contactsArray.length;i++){

                    var obj = {};
                    
                    if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                        var name = getTextLength(contactsArray[i].personName,10);
                        var image = '/getImage/'+contactsArray[i].personId;

                        obj = {
                            fullName:contactsArray[i].personName,
                            name:name,
                            image:image
                        };

                    }
                    else {
                        var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                        obj = {
                            fullName: contactsArray[i].personName,
                            name: getTextLength(contactsArray[i].personName, 10),
                            image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink
                            // noPicFlag:true
                        };
                    }

                    obj.emailId = contactsArray[i].personEmailId;
                    obj.interactionDate = contactsArray[i].interactionDate;
                    obj.interactionId = contactsArray[i].interactionId;
                    obj.refId = contactsArray[i].refId;
                    obj.title = contactsArray[i].title;
                    obj.description = contactsArray[i].description;
                    obj.companyName = contactsArray[i].company;
                    obj.designation = contactsArray[i].designation;
                    obj.filterToFetch = filter;
                    obj.actionItemId = contactsArray[i].actionItemId;
                    obj.twitterUserName = contactsArray[i].twitterUserName
                    obj.personId = contactsArray[i].personId;
                    obj.participants = contactsArray[i].participants;
                    obj.meetingTimeFormat = "";
                    // obj.meetingDateFormat = moment(contactsArray[i].meetingDate).tz("IST").format("DD MMM");
                    obj.meetingDateFormat = moment(contactsArray[i].meetingDate).fromNow();
                    obj.sortDate = contactsArray[i].meetingDate;

                    console.log("meeting duration:", obj.meetingDateFormat);

                    meetingFollowup.push(obj);
                }

                //Pagination

                $scope.currentPage = 0;
                $scope.pageSize = $rootScope.globalPageSize;
                $scope.numberOfmeetingFollowup = meetingFollowup.length;
                $scope.numberOfPages = function(){
                    return Math.ceil($scope.numberOfmeetingFollowup/$scope.pageSize);
                };

                meetingFollowup.sort(function (o1, o2) {
                    return new Date(o2.sortDate) - new Date(o1.sortDate)
                });
                
                $scope.meetingFollowup = meetingFollowup;
            } else {
                // if(sharedServiceSelection){
                //     sharedServiceSelection.refreshView();
                // }
            }

            $scope.loadingFollowup = false;

        });
};

function replyToMail($scope,$http,mailDetails,replyContent,bodyContent,sharedServiceSelection){

    if(!checkRequired(replyContent)){
        toastr.error("Please enter an email body.")
    }
    else{

        if(bodyContent != null){
            replyContent = replyContent+'\n\n\n'+bodyContent;
        }
        var obj = {
            email_cc:$scope.add_cc.length>0?$scope.add_cc:null,
            receiverEmailId:mailDetails.personEmailId?mailDetails.personEmailId:mailDetails.emailId,
            receiverName:mailDetails.fullName,
            message:replyContent,
            subject:"Re: "+mailDetails.subject,
            receiverId:mailDetails.receiverId,
            docTrack:mailDetails.docTrack,
            trackViewed:mailDetails.trackViewed,
            remind:mailDetails.remind,
            newMessage:false
        };

        if(bodyContent && mailDetails.refId){
            obj.updateReplied = true;
            obj.refId = mailDetails.refId
        };

        $http.post("/messages/send/email/single/web",obj)
            .success(function(response){
                if(response.SuccessCode) {
                    toastr.success(response.Message);
                    updateActionTakenDetails($scope,$http,sharedServiceSelection,mailDetails,'reply')
                }
                else {
                    toastr.error(response.Message);
                }
            });
    }
};

function insertLosingTouchActionTaken($scope,$http,sharedServiceSelection,contact) {

    $http.post('/update/lt/action/taken',contact).success(function (response) {
        sharedServiceSelection.fetchModuleShared(item.filterToFetch);
    });
}

function updateActionTakenDetails($scope,$http,sharedServiceSelection,item,actionTakenType) {

    var obj = {
        actionItemId:item.actionItemId,
        actionTakenSource:'dashboard',
        actionTakenType:actionTakenType
    };

    $http.post('/update/action/taken/info',obj).success(function (response) {

        if(response.SuccessCode){
            if(item.filterToFetch = "peopleNearMeetingLocation"){
                sharedServiceSelection.refreshView(item.filterToFetch)
            } else {
                sharedServiceSelection.fetchModuleShared(item.filterToFetch);
            }
        }
    });
}

function showEmail($scope,$http,mailDetails) {

    if(checkRequired(mailDetails.emailContentId)){
        $http.get('/message/get/email/single/web?emailContentId='+mailDetails.emailContentId+'&googleAccountEmailId='+mailDetails.googleAccountEmailId)
            .success(function(response){

                if(response.SuccessCode){
                    $scope.bodyContent = response.Data.data.replace(/\n/g, "<br />")

                    var temp = response.Data.data.replace(/\n/g, "<br />"); //to remove internal css
                    var start, end;
                    start = temp.indexOf("<style");
                    end = temp.indexOf("</style>");
                    end = end != -1 ? end + 8 : end;
                    var replace = temp.slice(start , end);

                    $scope.bodyContent = $scope.bodyContent.replace(replace, "");
                    temp = $scope.bodyContent;

                    var index = temp.indexOf('/track/email/open/')
                    if(index != -1){
                        $scope.bodyContent = removeRelatasTrackImg($scope.bodyContent, index)
                    }

                    // if($scope.updateOpened){
                    //     $scope.updateEmailOpen($scope.emailId,$scope.trackId,$scope.userId);
                    // }
                }
                else $scope.bodyContent = response.Message || '';
            })
    }
}

function updateEmailOpen($http,emailId,trackId,userId){

    if(!emailId){
        emailId = meetingObjService.lUseEmailId;
    }

    if(checkRequired(emailId) && checkRequired(trackId) && checkRequired(userId)){
        $http.get('/track/email/open/'+emailId+'/track/id/'+trackId+'/user/'+userId)
            .success(function(response){});
    }
}

function buildMailsToDisplay(mail,timezone,filter) {

    var interactionDate = timezone?moment(mail.interactionDate).tz(timezone):moment(mail.interactionDate).tz('UTC');
    var now = moment().tz(timezone);

    var difference = now.diff(interactionDate);
    difference = moment.duration(difference).asMinutes();

    var ago = moment.duration(difference,"minutes").humanize()+' ago'

    var personEmailId = mail.contactEmail?mail.contactEmail:mail.emailId;

    var subject = getTextLength(mail.title,30);
    var fullSubject = mail.title;

    var actionType = "fa fa-arrow-left"
    if(mail.action == 'receiver') {
        actionType = "fa fa-arrow-right"
    }
    var sortDate = new Date(interactionDate);

    // var futureEvents = "";
    // if(mail.futureEvents){
    // }

    if(checkRequired(mail.personId) && checkRequired(mail.personName)) {
        var name = getTextLength(mail.personName, 15);
        var image = '/getImage/' + mail.personId;

        return {
            fullName: toTitleCase(mail.personName),
            name: name,
            image: image,
            // cursor:'cursor:pointer',
            emailId: personEmailId,
            subject:subject,
            subjectFullLength:mail.title,
            refId:mail.refId,
            emailContentId:mail.emailContentId?mail.emailContentId:'',
            designation:mail.designation?mail.designation:'',
            company:mail.company?getTextLength(mail.company, 15):'',
            googleAccountEmailId:mail.ownerEmailId?mail.ownerEmailId:null,
            interactionDate:interactionDate.format("DD MMM YYYY"),
            ago:ago,
            fullSubject:fullSubject,
            recordId:mail.recordId ? mail.recordId : null,
            actionItemId:mail.actionItemId ? mail.actionItemId : null,
            filterToFetch:filter,
            actionType:actionType,
            sortDate:sortDate
        };
    } else {

        var noPicFlag = false;
        var contactImageLink = mail.contactImageLink?encodeURIComponent(mail.contactImageLink):null

        var noPicChar;
        if(personEmailId && !contactImageLink){
            noPicFlag = true;
            noPicChar = personEmailId.substr(0,2).toUpperCase()
        } else if(mail.personName && !contactImageLink){
            noPicChar = mail.personName?mail.personName.substr(0,2).toUpperCase():personEmailId.substr(0,2).toUpperCase()
            noPicFlag = true;
        }

        return {
            fullName:mail.personName?mail.personName:personEmailId,
            name:mail.personName?getTextLength(mail.personName,15):getTextLength(personEmailId,10),
            image:'/getContactImage/'+personEmailId+'/'+contactImageLink,
            cursor:'cursor:pointer',
            noPicFlag:noPicFlag,
            noPPic: noPicChar,
            subject:subject,
            subjectFullLength:mail.title,
            emailId: personEmailId,
            refId:mail.refId,
            emailContentId:mail.emailContentId?mail.emailContentId:'',
            designation:mail.designation?mail.designation:'',
            company:mail.company?getTextLength(mail.company, 15):'',
            url:null,
            className:'hide',
            personEmailId : personEmailId,
            nameNoImg: mail.personName?mail.personName.substr(0,2).toUpperCase():'',
            googleAccountEmailId:mail.ownerEmailId?mail.ownerEmailId:null,
            interactionDate:interactionDate.format("DD MMM YYYY"),
            ago:ago,
            fullSubject:fullSubject,
            recordId:mail.recordId ? mail.recordId : null,
            actionItemId:mail.actionItemId ? mail.actionItemId : null,
            filterToFetch:filter,
            actionType:actionType,
            sortDate:sortDate
        };

    }
}

function processLosingTouch($scope,$http,$rootScope,contactDetails) {

    if(contactDetails.twitterUserName){
        var twitterUrl = '/fetch/tweet/by/twitter/user/name?twitterUserName='+contactDetails.twitterUserName;

        $http.get(twitterUrl)
            .success(function (response) {
                if(checkRequired(response.Data)){
                    $scope.tweet = response.Data.tweet;
                    $scope.tweetId = response.Data.id;
                    // $scope.tweetExist = response.Data.tweet !== "<div>undefined</div>" ? true : false;
                    $scope.tweitterUserProfilUrl = response.Data.imageUrl;
                    // if(item.noPicFlag){
                    //     item.image = item.tweitterUserProfilUrl
                    //     item.noPicFlag = false;
                    // }
                }
            });
    }

}

function showNumberOfMails($scope,$http,filter){
    var url = '/fetch/email/by/filter?filter='+filter;
    $http.get(url)
        .success(function(response){

            if(filter == 'mailResponsePending'){
                $scope.numberOfMailResponsePending = response.Data.length;
            }

            if(filter == 'followUp'){
                $scope.numberOfFollowUp = response.Data.length;
            }

            if(filter == 'important'){
                $scope.numberOfImportant = response.Data.length;
            }
        })
}