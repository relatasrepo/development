var relatasApp = angular.module('relatasApp', ['ngRoute', 'ngCookies','ngSanitize','ngLodash']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.controller("header_controller", function($scope, $http, meetingObjService) {
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = false
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function() {
        return "/todaySeries/leftMeetingBar"
    };

    $scope.searchFromHeader = function(searchContent, yourNetwork, extendedNetwork, forCompanies) {

        //var str = searchContent.replace(/[^\w\s]/gi, '');

        var str = encodeURIComponent(searchContent);

        if (typeof str == 'string' && str.length > 0) {
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;

            window.location = '/contact/connect?searchContent=' + str + '&yourNetwork=' + yourNetwork + '&extendedNetwork=' + extendedNetwork + '&forCompanies=' + forCompanies;
        } else toastr.error("Please enter search content")
    };
    meetingObjService.searchFromHeader = $scope.searchFromHeader;

});

var timezone;
var showEmailGlobal = false;
var counterUpcoming = 1;
relatasApp.service('meetingObjService', function() {
    return {}
});

relatasApp.service('share', function () {
    return {}
});

relatasApp.factory('sharedServiceSelection', function($rootScope, $http) {
    var sharedService = {};
    sharedService.message = '';
    sharedService.prepForBroadcast = function(msg) {
        this.message = msg;
        this.broadcastItem();
    };

    sharedService.broadcastItem = function() {
        $rootScope.$broadcast('handleBroadcast');
    };

    sharedService.meetingBroadcast = function (meetingDetails) {
        this.meetingDetails = meetingDetails;
        this.broadcastMeetingObject();
    };

    sharedService.broadcastMeetingObject = function () {
        $rootScope.$broadcast('handleMeetingBroadcast')
    };

    sharedService.emailBroadcast = function (mailDetails) {
        this.mailDetails = mailDetails;
        this.broadcastEmailObject();
    };

    sharedService.broadcastEmailObject = function () {
        $rootScope.$broadcast('handleEmailBroadcast')
    };

    sharedService.actionBoardDisplay = function (status) {
        this.status = status;
        this.broadcastActionBoardDisplayObject();
    };

    sharedService.broadcastActionBoardDisplayObject = function () {
        $rootScope.$broadcast('handleActionBoardBroadcast')
    };

    sharedService.emailForm = function (status) {

        this.contactDetails = status;
        this.broadcastEmailFormObject();
    };

    sharedService.broadcastEmailFormObject = function () {
        $rootScope.$broadcast('handleEmailFormBroadcast')
    };

    sharedService.participantsBroadcast = function (status) {
        this.participant = status;
        this.broadcastParticipantsObject();
    };

    sharedService.broadcastParticipantsObject = function () {
        $rootScope.$broadcast('handleParticipantsBroadcast')
    };

    sharedService.updateActionTakenInfo = function(recordId,actionTakenType,$http,sharedServiceSelection,filter){
        updateActionTakenInfo(recordId,actionTakenType,$http,sharedServiceSelection,filter);
    }

    return sharedService;
});

relatasApp.factory('socket', function($rootScope) {
    // var socket = io.connect();
    return {
        on: function(eventName, callback) {
            socket.on(eventName, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function(eventName, data, callback) {
            socket.emit(eventName, data, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };
})

relatasApp.controller("logedinUser", function($scope, $http,searchService,share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function(response) {
            setTimeout(function() {
                $(".seeMoreIcon").addClass("disabled-see-more")
            }, 1000)

            if (response.SuccessCode) {
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.l_usr = response.Data;

                $scope.l_usr.initials = response.Data.firstName.substring(0,2);
                if(!imageExists($scope.l_usr.profilePicUrl)){
                    $scope.l_usr.imageExists = true;
                }
                share.l_user = response.Data;
                $scope.userName = response.Data.firstName;
                $scope.firstName = response.Data.firstName
                $scope.profilePicUrl = response.Data.profilePicUrl
                identifyMixPanelUser(response.Data);
                if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                    timezone = response.Data.timezone.name;
                }

                searchService.companyDetails = response.companyDetails;
                // searchService.setCompanyId(response.Data.companyId)
            } else {

            }
        }).error(function(data) {
    })
});

relatasApp.controller("background_image", function($scope,$http,$rootScope,meetingObjService,sharedServiceSelection,$cookies) {

    meetingObjService.getBackgroundImage = function (date) {
        $http.post('/today/top/section/info', { dateTime: date })
            .success(function(response) {
                if (response.SuccessCode) {
                    $scope.today_meeting_count_top = getValidStringNumber(response.Data.todayMeetings || 0);
                    $scope.today_tasks_count_top = response.Data.todayTasks || 0;
                    //$scope.image_top = '/images/app_images/drawable-hdpi/'+response.Data.image;
                    $scope.image_top = '/images/app_images/drawable-hdpi/' + response.Data.imageNew;

                } else {

                }
            }).error(function(data) {
        })
    }

})

relatasApp.controller("todayCounts", function($scope, $http) {

    var timer = 0;
    var interactionsUpdate = function() {
        $http.get('/check/onboarding/user')
            .success(function(response) {

                $scope.addContacts = '//contacts.google.com';
                if(response && response.profile && response.profile.serviceLogin == 'outlook'){
                    $scope.addContacts = '//outlook.live.com/owa/?path=/people';
                }

                if(response.fromOnBoarding && !response.interactionsWritten && timer<8){
                    $scope.showOverlay = true;
                    timer++;
                    setTimeout(interactionsUpdate,4000);
                } else {
                    $scope.showOverlay = false;
                    getAllValues();
                }
            });
    }

    interactionsUpdate();

    $scope.contactsInteractedForFirstTime = 0;
    $scope.remind = function(setFor) {
        var contactIds = [];
        if(setFor == 1) {
            $scope.contactsToRemind.forEach(function (a) {
                contactIds.push(a.contactId)
            })
        }
        else if(setFor == 7){
            $scope.contactsToRemindLast7Days.forEach(function (a) {
                contactIds.push(a.contactId)
            })
        }
        //if(contactIds.length > 0)
        $http.post("/dashboard/contacts/remind", { contactIds: contactIds })
            .success(function(response) {
                if (response.SuccessCode) {
                    toastr.success("Successfully updated")
                } else
                    toastr.error("Error! Please try again later")
            });
    }

    $scope.goToUrl = function (url) {
        window.location =url
    };

    //getAllValues();

    function getAllValues(){
        $http.get('/company/members')
            .success(function(response) {
                $scope.loader1 = true;
                $scope.loader2 = true;

                if (response.SuccessCode) {
                    var obj = response.Data.companyMembers;
                    var companyArr = Object.keys(obj).map(function(k) {
                        return obj[k]
                    });

                    $http.get('/insights/losing/touch/', { params: { companyMembers: companyArr.toString(), hierarchyList: response.Data.userId } })
                        .success(function(response) {

                            if (response.SuccessCode) {
                                $scope.loader1 = false;
                                $scope.losingTouch = response.Data.self.selfLosingTouch;
                                if (!isNaN($scope.losingTouch)) {
                                    $scope.losingTouch = Math.ceil($scope.losingTouch * 0.1);
                                }
                            } else {
                                $scope.loader1 = false;
                                $scope.losingTouch = 0;
                            }
                        });

                    $http.get('/insights/revenue/risk/graph', { params: { companyMembers: response.Data.userId,hierarchyList:response.Data.userId, getFor: 'self' } })
                        .success(function(res) {
                            if (response.SuccessCode) {
                                $scope.loader2 = false;
                                $scope.revenueAtRiskPercentage = Math.round((res.valueAtRisk / res.allContactsValue)*100) +'%';

                                if(isNaN(Math.round((res.valueAtRisk / res.allContactsValue)*100))){
                                    $scope.revenueAtRiskPercentage = 0 +'%'
                                }

                                var valueAtRisk10percent = res.valueAtRisk ? Math.round(res.valueAtRisk * 0.1): 0;
                                if(valueAtRisk10percent > 0){
                                    var data = res.Data;
                                    var companies = [];
                                    var company = null;
                                    var next = 0;
                                    for(var i=0; i< data.length - 1; i++){
                                        if(data[i].contactValue >= valueAtRisk10percent){
                                            if(data[i+1].contactValue >= valueAtRisk10percent){
                                                if(data[i].contactValue - valueAtRisk10percent >= data[i+1].contactValue - valueAtRisk10percent)
                                                    company =data[i+1].company;
                                                else
                                                    company = data[i].company;
                                            }
                                            else {
                                                if(!company)
                                                    company = data[i].company;
                                            }
                                        }
                                    }
                                    if(!company){
                                        for(var i=0; i< data.length - 1; i++){
                                            next += data[i].contactValue;
                                            companies.push(data[i].company);
                                            if(next > valueAtRisk10percent)
                                                break;
                                        }
                                    }
                                    else
                                        companies.push(company);

                                    $http.get('/insights/contacts/by/companies', { params: { companies:companies } })
                                        .success(function(respo) {
                                            $scope.valueAtRiskContacts = respo.Data.length ? respo.Data.length: 0;
                                        })
                                }
                                else
                                    $scope.valueAtRiskContacts = 0;

                            } else {
                                $scope.loader2 = false;
                                $scope.revenueAtRiskPercentage = 0 + '%';
                                $scope.valueAtRiskContacts = 0;
                            }
                        });

                    $http.get("/get/user/contacts/accounts")
                        .success(function(response) {
                            $scope.contactsWithAccountsCount = response.contactsWithAccountsCount;
                            var extendedNetworkUrl = '/dashboard/extended/network/from/all/companies';
                            if(response.contactsWithAccountsCount>100){
                                $scope.urlForExtendedNetwork = '/redirect/extended/network';
                                extendedNetworkUrl = '/dashboard/extended/network/from/all/companies';
                                getExtendedNetwork(extendedNetworkUrl)
                            } else {
                                getExtendedNetwork(extendedNetworkUrl)
                            }
                        });

                    function getExtendedNetwork(url) {
                        $http.get(url).then(function(respo) {
                            if (respo.data.SuccessCode) {
                                $scope.companiesForExtendedNetwork = [];
                                var extendedNetwork = 0;
                                for (var i = 0; i < respo.data.Data.length; i++) {
                                    extendedNetwork += respo.data.Data[i].extendedNetwork;
                                    if(respo.data.Data[i].extendedNetwork > 0) {
                                        $scope.companiesForExtendedNetwork.push(respo.data.Data[i].company);
                                    }
                                }
                                var companies = '';
                                for(var j=0;j<$scope.companiesForExtendedNetwork.length;j++) {
                                    if(j < $scope.companiesForExtendedNetwork.length - 1)
                                        companies += $scope.companiesForExtendedNetwork[j] + '+';
                                    else
                                        companies += $scope.companiesForExtendedNetwork[j];
                                }
                                $scope.urlForExtendedNetwork = "/contact/connect?searchContent=" + companies + "&yourNetwork=false&extendedNetwork=true&forCompanies=true&forInsights=false&forDashboard=true";
                                $scope.extendedNetwork = extendedNetwork > 0 ? extendedNetwork : undefined;
                                //$scope.extendedNetwork = undefined;
                                if(!$scope.extendedNetwork) {
                                    $http.get('/dashboard/extended/network/from/interacted/companies')
                                        .success(function (response) {
                                            var extendedContactsNumber = 0;
                                            if (response.SuccessCode) {
                                                var contacts = response.Data;
                                                var extendedNetcompanies = [];
                                                for(var i=0; i<contacts.length;i++){
                                                    if(i == 10 && extendedContactsNumber != 0)
                                                        break;
                                                    extendedContactsNumber += contacts[i].extendedNetwork;
                                                    if(contacts[i].extendedNetwork > 0)
                                                        extendedNetcompanies.push(contacts[i].company);
                                                }
                                                //extendedContactsNumber = 0;
                                            }
                                            if(extendedContactsNumber != 0){
                                                $scope.extendedNetwork = extendedContactsNumber > 10 ? 10 : extendedContactsNumber;
                                                var companies = '';
                                                for(var j=0;j<extendedNetcompanies.length;j++) {
                                                    if(j < extendedNetcompanies.length - 1)
                                                        companies += extendedNetcompanies[j] + '+';
                                                    else
                                                        companies += extendedNetcompanies[j];
                                                }
                                                $scope.urlForExtendedNetwork = "/contact/connect?searchContent=" + companies + "&yourNetwork=false&extendedNetwork=true&forCompanies=true&forInsights=false&forDashboard=true&forDashboardTopCompanies=true";
                                            }
                                            else{
                                                $scope.extendedNetwork = 0;
                                                $http.get('/dashboard/unique/contacts/interacted', { params: { days:7 } })
                                                    .success(function(response) {
                                                        if (response.SuccessCode) {
                                                            $scope.contactsToRemindLast7Days = response.Data.uniqueContacts;
                                                            $scope.sevenDaysInteraction = response.Data.uniqueContacts.length ? response.Data.uniqueContacts.length : 1; // if it is 0 make it as 1 to stop loading
                                                            //$scope.sevenDaysInteraction = response.Data.uniqueContacts.length
                                                        }
                                                    });
                                            }
                                        });
                                }
                            } else {

                            }
                        });
                    }
                } else {

                }
            }).error(function(data) {

        });

        $http.get('/dashboard/unique/contacts/interacted')
            .success(function(response) {
                if (response.SuccessCode) {
                    $scope.contactsToRemind = response.Data.uniqueContacts;
                    $scope.uniqueContacts = response.Data.uniqueContacts.length;
                    $scope.impactContacts = response.Data.impactContacts;
                } else {
                    $scope.uniqueContacts = 0;
                    $scope.impactContacts = 0;
                }
            });

        $http.get('/get/contacts/interactions/count')
            .success(function (responnse) {
                $scope.contactsCount = responnse.contactsCount
                $scope.interactionsCount = responnse.interactionsCount
                //$scope.interactionsCount = 0 ;
            });

        $http.get('/dashboard/contacts/interacted/for/first/time')
            .success(function(res) {
                if (res.SuccessCode) {

                    $scope.contactsInteractedForFirstTime = res.Data.contactsInteractedForFirstTime.length;
                } else {
                    $scope.contactsInteractedForFirstTime = 0;
                }
            });

        $http.get('/latest/interacted/contact')
            .success(function(response) {
                $scope.haveInteractionsPast24Hours = response;
            })

        $http.get('/check/pipeline/values')
            .success(function(response) {
                $scope.havePipelineValues = response;
            })

    }
});

relatasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

relatasApp.controller("actionItems", function($scope,$http,$rootScope,sharedServiceSelection,$window,searchService,lodash){

    $rootScope.currency = 'USD';

    searchService.setCompanyId = function (companyId) {
        if(companyId){
            $http.get('/corporate/company/' + companyId)
                .success(function (companyProfile) {
                    if(companyProfile.currency){
                        if(companyDetails && companyDetails.currency){
                            _.each(companyDetails.currency,function (el) {
                                if(el.isPrimary){
                                    $rootScope.currency = el.symbol
                                }
                            })
                        }
                    }
                });
        }
    }

    sharedServiceSelection.setUpcomingMeetingVisibility = function(visibility) {
        $scope.showUpcomingMeetings = visibility;
    }

    sharedServiceSelection.refreshViews = function(filter,watchlistType){
        if(filter === 'meetings'){
            fetchMeetings($scope,$http,$rootScope,searchService,sharedServiceSelection);
        }

        if(filter === 'watchlist'){
            fetchWatchList($scope,$http,watchlistType,searchService)
        }
    };

    $scope.publicProfileUrl = null;
    function checkInsightsBuiltStatus() {

        $http.get('/check/onboarding/user')
            .success(function (response) {

                if(!$scope.publicProfileUrl){
                    $scope.publicProfileUrl = 'https://relatas.com/'+response.publicProfileUrl;
                }

                kickOffDashboard();

                if(response.fromOnBoarding && !response.insightsBuiltStatus){
                    $rootScope.showOverlay2 = false;
                    setTimeout(checkInsightsBuiltStatus,4000);
                } if(!response.insightsBuiltStatus){
                    $rootScope.showOverlay3 = false;
                    setTimeout(checkInsightsBuiltStatus,4000);
                } else {
                    kickOffDashboard();
                    $rootScope.showOverlay2 = false;
                    $rootScope.showOverlay3 = false;
                }
            });
    }

    // checkInsightsBuiltStatus();
    kickOffDashboard();

    $scope.helpUrl = 'https://s3.amazonaws.com/relatas-live-storage/RelatasForBusinessOnboardWeb.pdf';

    // $scope.defSelected = "this-selected";
    $scope.selected = "this-selected";
    $scope.sortTweets = function (important) {
        if(important){
            $scope.selected = "this-selected";
            $scope.defSelected = "";

            fetchTweets($scope,$http,true);
        } else {
            $scope.selected = "";
            $scope.defSelected = "this-selected";
            fetchTweets($scope,$http,false);
        }
    }

    $scope.editOpportunity = function(opportunity){
        // $scope.isOppModalOpen = true;
        // editOpportunity($scope,$http,opportunity)

        window.location = "/opportunities/all?emailId="+opportunity.emailId
    }

    function kickOffDashboard() {
        $scope.showUpcomingEventsSection = false;
        $rootScope.actionBoardToggle = false;
        $scope.showMeeting = false;
        $scope.upcomingTotalItems = 0;
        $scope.upcomingEvents = [];
        $scope.loadingTweets = true;
        $scope.loadingUpcoming = true;
        fetchMeetings($scope,$http,$rootScope,searchService);
        fetchUpcomingMeetings($scope, $http, $rootScope);
    }

    sharedServiceSelection.refreshView = function (filter) {
        if(filter === "peopleNearMeetingLocation"){
            fetchMeetings($scope,$http,$rootScope,searchService);
        } else {
            fetchMeetings($scope,$http,$rootScope,searchService);
        }
    };

    $scope.showUpcomingEvents = function () {
        $scope.showUpcomingEventsSection = !$scope.showUpcomingEventsSection;
    };

    $scope.showTweet = function (item) {
        // showTweet($scope,$http,item);
    };

    $scope.addTwitterHandle = function (item) {
        item.openForm = !item.openForm;
    };

    $scope.updateTwitterHandle = function (contact,twitterHandle) {
        updateTwitterHandle($scope,$http,contact,twitterHandle,$rootScope.fetchWatchlistType,sharedServiceSelection)
    };

    $scope.actionBoardClose = function () {
        $rootScope.actionBoardToggle = false;
        closeAllActionBoard($rootScope);
        $rootScope.meetingSelected = null; //Reset previous selections.
        $rootScope.mailSelected = null;
    };

    $scope.addContactsToWatchlist = function (contact) {
        updateContactsToWatchlist($scope,$http,contact,$rootScope.fetchWatchlistType);
    };

    $scope.removeContactsFromWatchlist = function (contact) {
        updateContactsToWatchlist($scope,$http,contact);
    };

    $scope.actionBoard = function (meetingDetails,index) {
        $scope.showMeeting = true;
        $scope.showEmail = false;
        $rootScope.actionBoardToggle = true;

        if($scope.upcomingMeetings){
            $scope.upcomingMeetings.forEach(function (co) {
                co.selectedClass = "";
            })
        }
        if($scope.meetingsToday){
            $scope.meetingsToday.forEach(function (co) {
                co.selectedClass = "";
            })
        }

        meetingDetails.selectedClass = "highlight-clicked";
        $rootScope.meetingSelected = null; //Reset previous selections.
        $rootScope.meetingSelected = index;
        actionBoard($scope,$http,sharedServiceSelection,meetingDetails)

    };

    sharedServiceSelection.actionBoard = function(meetingDetails, index) {
        $scope.actionBoard(meetingDetails, index);
    }

    $scope.replyToThisMail = function (item,filter,index) {

        $rootScope.mailSelected = null; // reset previous selections
        $rootScope.mailSelected = index;
        $rootScope.actionBoardToggle = true;
        $scope.showMeeting = false;
        $scope.showEmail = true;
        item.filter = filter;
        replyToThisMail($scope,$http,sharedServiceSelection,item);
    };

    $scope.replyToThisMailOpenForm = function (item,filter,index) {

        $rootScope.mailSelected = null; // reset previous selections
        $rootScope.mailSelected = index;
        $rootScope.actionBoardToggle = true;
        $scope.showMeeting = false;
        $scope.showEmail = true;
        item.filter = filter;
        item.openForm = true;
        replyToThisMail($scope,$http,sharedServiceSelection,item);
    };

    $scope.meetingAction = function (type,meeting,index) {
        meetingAction($scope,$http,type,meeting,index,$window,$rootScope,sharedServiceSelection)
    };

    $scope.editRelatasMailEvent = function (type,item) {
        editRelatasMailEvent($scope,$http,type,item,sharedServiceSelection)
    };

    $scope.toggleSearchForm = function () {
        $scope.openSearchFormBox = !$scope.openSearchFormBox;
    };

    $scope.searchContacts = function(keywords){

        if(keywords && keywords.length > 2){

            searchService.search(keywords,$rootScope.fetchWatchlistType).success(function(response){
                if(response.SuccessCode){
                    processSearchResults($scope,$http,response.Data,$rootScope.fetchWatchlistType);
                }
            }).error(function(){
                toastr.error("Something went wrong. Please try again");
            });
        } else {
            $scope.contacts = null;
        }
    };

    $rootScope.fetchWatchlistType = "contacts";
    $scope.showWatchListItems = false;
    $scope.fetchWatchlist = function (type) {
        $rootScope.fetchWatchlistType = type;
        $scope.showWatchListItems = true;
        $scope.socialSettingsOpen = true;
        $scope.hideTweets = true;
        
        fetchWatchList($scope,$http,type)
    };

    $scope.updateActionTakenInfo = function(recordId,actionTakenType,filter) {
        updateActionTakenInfo(recordId, actionTakenType, $http, sharedServiceSelection, filter);
    };

    $scope.reactToTweet = function (item,status,action) {
        reactToTweet($http,item,status,action)
    };

    $scope.openReplyToTweetForm = function (item) {
        openReplyToTweetForm(item)
    }

    $scope.closeDialog = function () {
        $scope.showWatchListItems = !$scope.showWatchListItems;
        $scope.hideTweets = !$scope.hideTweets;

        fetchWatchList($scope,$http,'contacts')
    };

    $scope.hideTweets = false;
    $scope.$window = $window;
    $scope.toggleSocialSettings = function () {
        $scope.socialSettingsOpen = !$scope.socialSettingsOpen;

        if ($scope.socialSettingsOpen) {
            $scope.$window.onclick = function (event) {
                closeWhenClickingElsewhere(event, $scope.toggleSocialSettings);
            };
        } else {
            $scope.socialSettingsOpen = false;
            $scope.$window.onclick = null;
            $scope.$apply(); //--> trigger digest cycle and make angular aware.
        }
    }

    $scope.toggleVisibility = function(visibileSection) {
        if(visibileSection == 'upcomingMeeting') {
            $(".upcoming-meeting").toggleClass("hide");

            var classValue = $(".upcoming-meeting").attr('class');
            
            if(classValue.split(" ").indexOf("hide") !== -1)
                $scope.showUpcomingMeetings = false;
            else 
                $scope.showUpcomingMeetings = true;

            if($scope.showUpcomingMeetings) {
                if($scope.upcomingMeetings.length > 0) {
                    $scope.actionBoard($scope.upcomingMeetings[0])
                    $scope.showNoUpcomingMeetingText = false;
                } else {
                    $scope.actionBoardClose();
                    $scope.showNoUpcomingMeetingText = true;
                }
            }

        } else if(visibileSection == 'todayMeeting') {
            $(".today-meeting").toggleClass("hide");
            var classValue = $(".today-meeting").attr('class');
            
            if(classValue.split(" ").indexOf("hide") !== -1)
                $scope.showTodayMeetings = false;
            else 
                $scope.showTodayMeetings = true;

            if($scope.showTodayMeetings) {
                if($scope.meetingsToday && $scope.meetingsToday.length > 0) {
                    $scope.actionBoard($scope.meetingsToday[0])
                    $scope.showNoTodayMeetingText = false;
                } else {
                    $scope.showNoTodayMeetingText = true;
                }
            } else {
                $scope.actionBoardClose();
            }
        }
    }


});

function openReplyToTweetForm(item) {
    item.openForm = !item.openForm
}

function reactToTweet($http,item,status,action) {

    var url = "/social/feed/twitter/tweet/update/web?postId="+item.tweetId+"&action=reply&status="+status;

    if(action == "favorite"){
        url = "/social/feed/twitter/tweet/update/web?postId="+item.tweetId+"&action=favorite";
    } else if(action == "re-tweet"){
        url = "/social/feed/twitter/tweet/update/web?postId="+item.tweetId+"&action=re-tweet";
    }
    
    $http.get(url)
        .success(function (response) {
            // TODO?
        });

}

function updateTwitterHandle($scope,$http,contact,twitterHandle,fetchWatchlistType,sharedServiceSelection) {

    var obj = {
        emailId: contact.emailId,
        twitterUserName: twitterHandle,
        updateFor: 'person',
        companyName: null
    };

    if(fetchWatchlistType === 'companies'){

        obj = {
            emailId: contact.emailId,
            twitterUserName: twitterHandle,
            updateFor: 'company',
            companyName: contact.companyName
        };
    };

    $http.post('/update/twitter/username/for/company/or/person',obj)
        .success(function (response) {
            if(response.SuccessCode){
                toastr.success("Successfully updated Twitter handle");
                fetchTweets($scope,$http);
                // fetchWatchList($scope,$http,fetchWatchlistType)
                sharedServiceSelection.refreshViews('watchlist',fetchWatchlistType)
            } else {
                toastr.error("Error! Please try again later");
            }
        });
}

function processSearchResults($scope,$http,response,fetchWatchlistType) {

    var contactsArray = response;
    $scope.contacts = [];

    if(contactsArray.length>0){
        for(var i=0;i<contactsArray.length;i++){

            var obj = {};

            if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                var name = getTextLength(contactsArray[i].personName,10);
                var image = '/getImage/'+contactsArray[i].personId._id;

                obj = {
                    fullName:contactsArray[i].personName,
                    name:name,
                    image:image
                };

                obj.emailId = contactsArray[i].personEmailId;
                obj.twitterUserName = contactsArray[i].twitterUserName;

            }
            else {
                var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                obj = {
                    fullName: contactsArray[i].personName,
                    name: getTextLength(contactsArray[i].personName, 10),
                    image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                    emailId:contactsArray[i].personEmailId,
                    twitterUserName: contactsArray[i].twitterUserName
                    // noPicFlag:true
                };
            }

            if(obj.twitterUserName){
                obj.tweetAccExists = true;
            }

            obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
            obj._id = contactsArray[i]._id;

            if(fetchWatchlistType === 'companies'){

                obj = {};

                if(contactsArray[i].account && contactsArray[i].account.name){
                    obj.image = "https://logo.clearbit.com/"+ getTextLength(contactsArray[i].account.name,25) +".com";
                    obj.name = toTitleCase(getTextLength(contactsArray[i].account.name,10));
                    obj._id = contactsArray[i]._id;
                    obj.watchThisAccount = contactsArray[i].account.watchThisAccount?contactsArray[i].account.watchThisAccount:false;
                    obj.tweetAccExists = contactsArray[i].account.twitterUserName?true:false
                }
            }

            if(!userExists(obj.name)){
                $scope.contacts.push(obj)
            }

            function userExists(username) {
                return $scope.contacts.some(function(el) {
                    return el.name === username;
                });
            }
        }
    }
}

function updateContactsToWatchlist($scope,$http,contact,fetchWatchlistType) {

    var reqObj = {contactId:contact._id,watchThisContact:!contact.watchThisContact,type:fetchWatchlistType};

    $http.post('/update/watchlist/contact',reqObj)
        .success(function(response){
            if(response.SuccessCode){
                fetchTweets($scope,$http);
                fetchWatchList($scope,$http,fetchWatchlistType);
            }
        });
}

relatasApp.controller("actionBoardController",function ($scope,$http,$rootScope,sharedServiceSelection) {

    $scope.cancelMeeting = false;
    $scope.confirmMeeting = false;
    
    $scope.closeMessageBox = function (box) {
        if(box === 'cancelMeeting'){
            $scope.cancelMeeting = false;
        } else if(box === 'confirmMeeting'){
            $scope.confirmMeeting = false;
        }
    }

    $scope.openMessageBox = function (box) {
        if(box === 'cancelMeeting'){
            $scope.cancelMeeting = true;
            $scope.confirmMeeting = false;
        } else if(box === 'confirmMeeting'){
            $scope.cancelMeeting = false;
            $scope.confirmMeeting = true;
        }
    }

    $scope.$on('handleMeetingBroadcast', function() {

        $scope.meetingDetails = sharedServiceSelection.meetingDetails;
        $scope.isOpportunity = $scope.meetingDetails.salesForceEvent?$scope.meetingDetails.salesForceEvent:false;

        $scope.openContactOpportunity = '/contacts/all?contact='+$scope.meetingDetails.emailId+'&acc=true';

        // var fetchById = $scope.meetingDetails.userId?$scope.meetingDetails.userId:$scope.meetingDetails.liu;

        $scope.isAccepted = $scope.meetingDetails.isAccepted;

        $scope.showConfirmBtn = true;

        // $('.selectpicker').selectpicker();
        // $('.selectpicker').selectpicker('val', $scope.meetingDetails.locationType);

        if($scope.isAccepted || $scope.meetingDetails.isSender){
            $scope.showConfirmBtn = false;
        }

        // initializeDateTimePicker($scope,$http,'reschedule_start_date_picker','reschedule_start_date',getDateFormat($scope.meetingDetails.start,false),$scope.meetingDetails.meetingDuration)
        // initializeDateTimePicker($scope,$http,'reschedule_end_date_picker','reschedule_end_date',getDateFormat($scope.meetingDetails.end,false),$scope.meetingDetails.meetingDuration);
        // bindCalendarData($http, $scope, fetchById, timezone, $scope.meetingDetails.meetingDuration <= 60 ? $scope.meetingDetails.meetingDuration : 60, true, 14)
    });

    $scope.$emit('handleMeetingBroadcast', null);

    $scope.prepareICSFile = function (meeting) {
        $http.get("/meeting/get/"+meeting.invitationId+"/meeting")
            .success(function (response) {
                $scope.ics_url = response.Data.icsFile.url
            });
    };

    $scope.applyDateTimePicker = function(id,key,current,slotDuration){
        initializeDateTimePicker($scope,$http,id,key,current,slotDuration)
    };

    $scope.re_scheduleMeeting = function (suggestLocation,comment,meetingDetails) {
        re_scheduleMeeting($scope,$http,suggestLocation,comment,meetingDetails)
    };

    $scope.takeMeetingAction = function (type,meeting,message) {

        var fetchById = meeting.userId?meeting.userId:meeting.liu;

        $('.selectpicker').selectpicker();
        $('.selectpicker').selectpicker('val', meeting.locationType);

        if(type === 'reschedule'){
            $scope.rescheduleTrue = true;
            initializeDateTimePicker($scope,$http,'reschedule_start_date_picker','reschedule_start_date',getDateFormat(meeting.start,false),meeting.meetingDuration)
            initializeDateTimePicker($scope,$http,'reschedule_end_date_picker','reschedule_end_date',getDateFormat(meeting.end,false),meeting.meetingDuration);
            bindCalendarData($http, $scope, fetchById, timezone, meeting.meetingDuration <= 60 ? meeting.meetingDuration : 60, true, 14)
        }else if(type === 'confirm'){
            confirmMeeting($scope,$http,meeting,$rootScope,sharedServiceSelection,message)
        } else if(type === 'decline'){
            declineMeeting($scope,$http,meeting,$rootScope,sharedServiceSelection,message)
        }
    };

    $scope.cancelReschedule = function () {
        $scope.rescheduleTrue = false;
    }

});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords,fetchWatchlistType){
            return $http.post('/search/user/contacts', { "contactName" : keywords,"fetchWatchlistType":fetchWatchlistType });
        },
        setCompanyId:function(companyId){
            this.companyId = companyId;
        }
    }
}]);

function initializeDateTimePicker($scope,$http,id,key,current,slotDuration) {

    slotDuration = slotDuration <= 60 ? slotDuration : 60;

    $('#'+id).datetimepicker({
        dayOfWeekStart: 1,
        format:'d-m-Y h:i a',
        lang: 'en',
        value:current,
        minDate: new Date(),
        step:slotDuration,
        onClose: function (dp, $input){
            var selected = moment(dp);

            var isStartTime = key == "reschedule_start_date";
            var isFree = true;
            if(selected.isAfter(moment())){
                $scope[key] = selected.format();
                var end = selected.clone();
                end = end.minutes(selected.minutes() + slotDuration);
                $scope["reschedule_end_date"] = end.format();
                $("#reschedule_end_date_picker").val(getDateFormat($scope["reschedule_end_date"],false))
            }
            else{
                $input.val(getDateFormat($scope[key],false));
                toastr.error("Please select future time");
            }
        },

        onGenerate:function( ct,a,b ){

            $(".remove_disable_plugin").removeClass("disabled")
            var day = [moment(ct).tz(timezone).format("MM-DD-YYYY")];
            if($scope.allSlots && $scope.allSlots[day]) {
                var slots = $scope.allSlots[day];

                var allSlots = [];
                allSlots = allSlots.concat(slots.am.slots);
                allSlots = allSlots.concat(slots.pm.slots);
                if(allSlots.length > 0){
                    allSlots.forEach(function(item) {
                        if (item.isBlocked) {
                            $("#disable"+moment(item.start).tz(timezone).format("HH-mm")).addClass("disabled")
                        }
                    })
                }
            }
        }
    });
}

function re_scheduleMeeting($scope,$http,suggestLocation,comment,meetingDetails){

    if($scope.reschedule_start_date != undefined && checkRequired($scope.reschedule_start_date) && $scope.reschedule_end_date != undefined && checkRequired($scope.reschedule_end_date)){

        var start = $scope.reschedule_start_date;
        var end = $scope.reschedule_end_date;

        var ms = moment(end,"DD/MM/YYYY HH:mm:ss").diff(moment(start,"DD/MM/YYYY HH:mm:ss"));
        var d = moment.duration(ms);
        var slots = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");

        if(moment(start).isSame(end) || moment(start).isAfter(end)){
            toastr.error("Please select valid date and time");
        }
        else if(typeof suggestLocation == undefined || !checkRequired(suggestLocation)){
            toastr.error('Please suggest a new location');
        }
        else{
            $scope.locationType = $('.selectpicker').val()
            $('.re-schedule-popup').css({display:'none'});
            start = moment(start)
            end = moment(end)
            var reqObj = {
                "invitationId":meetingDetails.invitationId,
                "comment":comment,
                "newSlotDetails":{
                    "startDateTime":start.format(),
                    "endDateTime":end.format(),
                    "suggestedLocation":suggestLocation,
                    "locationType":$scope.locationType
                }
            };

            $http.post('/meetings/action/edit/web',reqObj)
                .success(function(response){

                    if(response.SuccessCode){
                        $scope.suggested_location = ""
                        $scope.suggest_comment = ""
                        toastr.success(response.Message);
                    }
                    else{
                        toastr.error(response.Message);
                    }
                })
        }
    }
    else toastr.error('Please select valid date and time.');
}

relatasApp.controller("peopleAtMeetingLocation", function ($scope,$http,$rootScope,sharedServiceSelection) {

    $scope.loadingpal = true;
    $scope.peopleAtLocation = function (location,locationType,personEmailId,invitationId,date) {
        setTimeOutCallback(6000,function () {
            paintContactsAtMyMeetingLocation($scope,$http,location,locationType,personEmailId,$rootScope,invitationId,date);
        });
    };

    $scope.openEmailForm = function(contact,index,location){

        if($scope.peopleAtThisLocation){
            $scope.peopleAtThisLocation.forEach(function (co) {
                co.selectedClass = ""
            });
        }

        if(contact){
            contact.selectedClass = "highlight-clicked";
        }

        $rootScope.actionBoardToggle = false;
        closeAllActionBoard($rootScope,function (result) {
            // $rootScope.sendEmailBoardPTM = true;
            $rootScope.sendEmailBoardToggle = true;
            sharedServiceSelection.emailForm(contact)
        });
    };

    $scope.showDistances = function (item) {
        showDistances($scope,$http,$rootScope,item);
    };

    $scope.actionBoardClose = function () {
        $rootScope.actionBoardToggle = false;
        $rootScope.meetingSelected = null; //Reset previous selections.
    };

});

var numberOfLocations = 1;
function showDistances($scope,$http,$rootScope,item) {

    var url = item.distanceToPrevMeetingLocation;

    if(url){
        $http.get(url)
            .success(function (response) {

                if(checkRequired(response && response.SuccessCode && response.Data)){
                    $scope.numberOfLocations = numberOfLocations++;
                    item.distance = response.Data.distance;
                    item.sortDistance = response.Data.distance;
                    item.time = response.Data.duration;
                } else {
                    item.distance = false;
                }
            });
    }
}

function editRelatasMailEvent($scope,$http,type,item,sharedServiceSelection) {

    if(type === 'remove'){
        declineMeeting($scope,$http,item)
    } else if(type === 'move'){

        bindCalendarData($http, $scope, item.liu, timezone, 30, false, 14,function (openSlotsFetched) {

            $('#'+item.updateRelatasEvent).datetimepicker({

                value: new Date(),
                format: 'd.m.Y H:i',
                minDate: new Date(),
                step:30,
                onClose: function (dp, $input) {

                    if(dp){

                        var end = moment(dp).add(30, "minutes");

                        var reqObj = {
                            "meeting":item,
                            "newSlotDetails":{
                                "startDateTime":new Date(dp),
                                "endDateTime":new Date(end)
                            }
                        };
                        updateEvent(reqObj)
                    }
                },
                onGenerate:function( ct,a,b ){
                    var day = [moment(ct).tz(timezone).format("MM-DD-YYYY")];
                    if($scope.allSlots && $scope.allSlots[day]) {
                        var slots = $scope.allSlots[day];

                        var allSlots = [];
                        allSlots = allSlots.concat(slots.am.slots);
                        allSlots = allSlots.concat(slots.pm.slots);
                        if(allSlots.length > 0){
                            allSlots.forEach(function(slot) {
                                if (slot.isBlocked) {
                                    $("#disable"+moment(slot.start).tz(timezone).format("HH-mm")).addClass("disabled")
                                }
                            })
                        }
                    }
                }
            });
        });

        function updateEvent(obj) {

            $http.post('/update/relatas/events',obj)
                .success(function (response) {
                    if(response.SuccessCode){
                        // sha
                        sharedServiceSelection.refreshView()
                    } else {
                        toastr.error(response.Message)
                    }
                });
        }
    }
}

function fetchWatchList($scope,$http,type) {

    if(!type){
        type = 'contacts'
    }

    var url = '/fetch/watchlist/contacts?type='+type;

    if(type === 'companies') {
        $scope.selectionContacts = 'btn-grey-bg';
        $scope.selectionCompanies = 'btn-primary';
    } else {
        $scope.selectionContacts = 'btn-primary';
        $scope.selectionCompanies = 'btn-grey-bg';
    }

    $http.get(url)
        .success(function (response) {
            var contactsArray = response;
            $scope.watchlist = [];

            if(contactsArray.length>0){
                for(var i=0;i<contactsArray.length;i++){

                    var obj = {};

                    if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                        var name = getTextLength(contactsArray[i].personName,10);
                        var image = '/getImage/'+contactsArray[i].personId;

                        obj = {
                            fullName:contactsArray[i].personName,
                            name:name,
                            image:image
                        };

                        obj.emailId = contactsArray[i].personEmailId;
                        obj.twitterUserName = contactsArray[i].twitterUserName;

                    }
                    else {
                        var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                        obj = {
                            fullName: contactsArray[i].personName,
                            name: getTextLength(contactsArray[i].personName, 10),
                            image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                            emailId:contactsArray[i].personEmailId,
                            twitterUserName: contactsArray[i].twitterUserName
                            // noPicFlag:true
                        };
                    }

                    if(obj.twitterUserName){
                        obj.tweetAccExists = true;
                    }

                    obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
                    obj._id = contactsArray[i]._id;

                    if(type === 'companies'){

                        obj = {};
                        obj.image = "https://logo.clearbit.com/"+ getTextLength(contactsArray[i].account.name,25) +".com";
                        obj.name = toTitleCase(getTextLength(contactsArray[i].account.name,10));
                        obj._id = contactsArray[i]._id;
                        obj.watchThisAccount = contactsArray[i].account.watchThisAccount?contactsArray[i].account.watchThisAccount:false;
                        obj.tweetAccExists = contactsArray[i].account.twitterUserName?true:false
                        obj.emailId = contactsArray[i].personEmailId;
                        obj.companyName = contactsArray[i].account.name;
                    }

                    $scope.watchlist.push(obj)
                }

                // $scope.watchlist = $scope.watchlist.slice(0,3);
            }
        })
}

function fetchTweets($scope,$http,sortByImportant) {

    var url = '/fetch/latest/tweets'

    $http.get(url)
        .success(function (response) {
            var contactsArray = response.contacts;

            $scope.loadingTweets = false;
            $scope.noTwitterAccSetup = response.noTwitterAccSetup;
            $scope.twitterFeeds = [];
            
            if(contactsArray && contactsArray.length>0){
                for(var i=0;i<contactsArray.length;i++){

                    var obj = {};

                    if(contactsArray[i].tweet){

                        if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                            var name = getTextLength(contactsArray[i].personName,20);
                            var image = '/getImage/'+contactsArray[i].personId;

                            obj = {
                                fullName:contactsArray[i].personName,
                                name:name,
                                image:image
                            };

                            obj.emailId = contactsArray[i].personEmailId;
                            obj.twitterUserName = contactsArray[i].twitterUserName;

                        }
                        else {
                            
                            var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null

                            obj = {
                                fullName: contactsArray[i].personName,
                                name: getTextLength(contactsArray[i].personName, 20),
                                image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                                emailId:contactsArray[i].personEmailId,
                                twitterUserName: contactsArray[i].twitterUserName,
                                noPicFlag: contactImageLink ? false : true
                            };
                        }

                        if(contactsArray[i].twitterUserName && contactsArray[i].personEmailId){
                            // obj.twitterUrl = '/fetch/tweet/by/twitter/user/name?twitterUserName='+obj.twitterUserName;
                            obj.tweet = contactsArray[i].tweet.tweet;
                            obj.tweetId = contactsArray[i].tweet.tweetId;
                            // obj.tweitterUserProfilUrl = contactsArray[i].tweet.imageUrl;
                            obj.tweitterUserProfilUrl = '/save/twitterContactImage?emailId='+contactsArray[i].personEmailId+'&imageUrl='+contactsArray[i].tweet.imageUrl;
                            if(obj.noPicFlag){
                                obj.image = obj.tweitterUserProfilUrl
                                obj.noPicFlag = false;
                            }

                            obj.tweetedDate = contactsArray[i].tweet.tweetedDate
                            obj.isImportant = contactsArray[i].favorite
                            obj.favoriteClass = contactsArray[i].favorite?'contact-fav':'';
                            $scope.twitterFeeds.push(obj)
                        }
                        else if(!contactsArray[i].tweet.emailId && contactsArray[i].tweet.company){

                            // obj.twitterUrl = '/fetch/tweet/by/twitter/user/name?twitterUserName='+contactsArray[i].account.twitterUserName;
                            obj.image = "https://logo.clearbit.com/"+ getTextLength(contactsArray[i].tweet.company,25) +".com";
                            obj.noPicFlag = false;
                            obj.name = toTitleCase(getTextLength(contactsArray[i].tweet.company,10));
                            obj.tweet = contactsArray[i].tweet.tweet;
                            obj.tweetId = contactsArray[i].tweet.tweetId;
                            // obj.tweitterUserProfilUrl = contactsArray[i].tweet.imageUrl;
                            obj.tweitterUserProfilUrl = '/save/twitterContactImage?emailId='+contactsArray[i].personEmailId+'imageUrl='+contactsArray[i].tweet.imageUrl
                            if(obj.noPicFlag){
                                obj.image = obj.tweitterUserProfilUrl
                                obj.noPicFlag = false;
                            }
                            obj.tweetedDate = contactsArray[i].tweet.tweetedDate;
                            obj.isImportant = contactsArray[i].favorite
                            obj.favoriteClass = contactsArray[i].favorite?'contact-fav':''
                            $scope.twitterFeeds.push(obj)
                        }
                    }
                }
                var tf = $scope.twitterFeeds;
                if(sortByImportant){
                    $scope.twitterFeeds = _.filter(tf,function (el) {
                        return el.isImportant == true;
                    })

                } else {
                    $scope.twitterFeeds.sort(function (o1, o2) {
                        return o1.tweetedDate > o2.tweetedDate ? -1 : o1.tweetedDate < o2.tweetedDate ? 1 : 0;
                    });
                }

            } else {
                $scope.updateWatchlist = true;
            }
        });

}

function meetingAction($scope,$http,type,meeting,index,$window,$rootScope,sharedServiceSelection) {

    switch (type) {
        case 'cancel':
            declineMeeting($scope,$http,meeting,$rootScope,sharedServiceSelection)
            break;
        case 'reschedule':
            // reschedule($scope,$http,meeting,index,$window);
            break;
        case 'confirm':
            confirmMeeting($scope,$http,meeting,$rootScope,sharedServiceSelection)
            break;
    }

}

function confirmMeeting($scope,$http,meeting,$rootScope,sharedServiceSelection,message) {

    var reqBody = {
        "invitationId": meeting.invitationId,
        "slotId": meeting.slotId,
        "message":message
    };

    $http.post('/meetings/action/accept/web',reqBody)
        .success(function(response){
            if(response.SuccessCode){
                /*
                 * Refresh meeting views
                 * */
                sharedServiceSelection.refreshViews('meetings')
                $scope.confirmMeeting = !$scope.confirmMeeting;
                $rootScope.actionBoardToggle = !$rootScope.actionBoardToggle;
                toastr.success(response.Message);
            }
            else{
                toastr.error(response.Message);
            }
        })

}

function declineMeeting($scope,$http,meeting,$rootScope,sharedServiceSelection,message) {

    var reqObj = {
        invitationId:meeting.invitationId,
        message:message
    };

    $http.post('/meetings/action/cancel/web',reqObj)
        .success(function(response){

            if(response.SuccessCode){
                toastr.success(response.Message);
                sharedServiceSelection.refreshView();
                $scope.cancelMeeting = !$scope.cancelMeeting;
                $rootScope.actionBoardToggle = !$rootScope.actionBoardToggle;
            }
            else{
                toastr.error(response.Message);
            }
        })
}

function bindCalendarData($http, $scope, personId, timezone, duration, mapMyCal, daysDuration,callback){

    var url = '/schedule/slots/available?id='+personId+'&slotDuration='+duration+'&daysDuration='+daysDuration+'&timezone='+timezone+'&withSlotDates=yes'
    if(mapMyCal){
        url = '/schedule/slots/available/mapcalendar?id='+personId+'&slotDuration='+duration+'&daysDuration='+daysDuration+'&timezone='+timezone+'&withSlotDates=yes'
    }

    $http.get(url)
        .success(function(oFreeSlots){
            if(oFreeSlots.SuccessCode){
                $scope.allSlots = oFreeSlots.Data;

                if(callback){
                    callback(true)
                }
            }
        });
}

function replyToThisMail($scope,$http,sharedServiceSelection,item) {
    sharedServiceSelection.emailBroadcast(item);
    // sharedServiceSelection.actionBoardDisplay("email");
}

function actionBoard($scope,$http,sharedServiceSelection,meetingDetails) {
    sharedServiceSelection.actionBoardDisplay("meeting");
    sharedServiceSelection.meetingBroadcast(meetingDetails);
}

function fetchMeetings($scope,$http,$rootScope,searchService, sharedServiceSelection) {

    var dateAgenda = checkRequired(timezone) ? moment().tz(timezone).format() : moment().format();
    var all_ms = 0;

    $scope.fetchAgenda = function(dateAgenda){
        getUniqueEvents($scope);
        $scope.eventsToday =[];

        $http.post('/today/agenda/section/web',{dateTime:dateAgenda})
            .success(function (response) {

                $rootScope.numberOfMeetingsToday = response.Data.meetings.length;

                if(response.SuccessCode){
                    if(checkRequired(response.Data.meetings != undefined && response.Data.meetings.length > 0)){

                        $rootScope.meetingsWithToday = [];
                        for(var i=0; i<response.Data.meetings.length; i++){

                            var meetingDetails = formatMeetingDetails(response.Data.meetings[i],response.Data.userId);
                            meetingDetails['participants'] = response.Data.meetings[i].toList;
                            meetingDetails['numberOfMeetingParticipants'] = '';
                            meetingDetails['liu'] = response.Data.userId;

                            if(response.Data.meetings[i].toList.length>1) {
                                meetingDetails['numberOfMeetingParticipantsExists'] = true;
                                meetingDetails['numberOfMeetingParticipants'] = response.Data.meetings[i].toList.length -1
                                for(var j=0;j<response.Data.meetings[i].toList.length;j++){
                                    $rootScope.meetingsWithToday.push(response.Data.meetings[i].toList[j].receiverEmailId)
                                }
                            }

                            $rootScope.meetingsWithToday.push(meetingDetails.personEmailId)

                            meetingDetails.meetingInitiatorIcon = setMeetingInitiatorIcon(meetingDetails);
                            meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/outgoingOrange.png';
                            if(meetingDetails.isSender && meetingDetails.isAccepted){
                                meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/Meeting_Outgoing_confirmed.png';
                            }

                            if(!meetingDetails.isSender && meetingDetails.isAccepted){
                                meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/IncomingGreen.png';
                            }

                            if(!meetingDetails.isSender && !meetingDetails.isAccepted){
                                meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/IncomingOrange.png';
                            }

                            meetingDetails.notSelfMeeting = true;

                            if(meetingDetails.participants.length === 0){
                                if((meetingDetails.personEmailId === response.Data.meetings[i].to.receiverEmailId) || !response.Data.meetings[i].to.receiverEmailId){
                                    meetingDetails.notSelfMeeting = false;
                                }
                            }

                            if(meetingDetails.personEmailId === 'unknownorganizer@calendar.google.com'){
                                meetingDetails.nameTruncate = "Goal";
                                meetingDetails.picUrl = "/images/dashboard-icons/google-icon.png";
                                meetingDetails.meetingInitiatorIcon = "/images/dashboard-icons/flag.png";
                                meetingDetails.locationTypePic = "";
                                meetingDetails.isGoogleGoal = true;
                                meetingDetails.descriptionTruncate = getTextLength(meetingDetails.description,45);
                                meetingDetails.company = '';
                                meetingDetails.companyTruncate = '';
                            }

                            var date1_ms = meetingDetails.start.getTime();
                            var date2_ms = meetingDetails.end.getTime();

                            // Calculate the difference in milliseconds
                            var difference_ms = date2_ms - date1_ms;

                            all_ms = all_ms+difference_ms;

                            $scope.eventsToday.push(meetingDetails);
                        }

                        for(var j=0; j<response.Data.events.length; j++){
                            var dateStart = checkRequired(timezone) ? moment(response.Data.events[j].startDateTime).tz(timezone) : moment(response.Data.events[j].startDateTime);
                            var dateEnd = checkRequired(timezone) ? moment(response.Data.events[j].endDateTime).tz(timezone) : moment(response.Data.events[j].endDateTime);
                            var obj = {
                                sortDate:new Date(dateStart.format()),
                                startTime:dateStart.format("hh:mm A"),
                                endTime:dateEnd.format("hh:mm A"),
                                title:response.Data.events[j].eventName || '',
                                location:response.Data.events[j].locationName || '',
                                eventId:response.Data.events[j]._id,
                                isAccepted:response.Data.events[j].accepted || false,
                                picUrl:'/getImage/'+response.Data.events[j].createdBy.userId,
                                url:'/event/'+response.Data.events[j]._id,
                                name:response.Data.events[j].createdBy.userName,
                                locationTypePic:"/images/icon_location_small.png",
                                preparedMeetingClass:'hide',
                                disableClass:''
                            };
                            $scope.eventsToday.push(obj);
                        }
                        getTodayMeetings($scope, sharedServiceSelection);
                    }

                }
                else{
                    $scope.noAgendaFoundFlag = true;
                    $scope.noAgendaFound = response.Message;
                }
                var dateMoment = checkRequired(timezone) ? moment(dateAgenda).tz(timezone) : moment(dateAgenda);
                var now = checkRequired(timezone) ? moment().tz(timezone) : moment();
                if(dateMoment.format("DD MMM YYYY") == now.format("DD MMM YYYY")){
                    $scope.agendaTitle = "Today's Agenda ("+dateMoment.format("DD MMM YYYY")+")";
                }
                else $scope.agendaTitle = "Agenda For ("+dateMoment.format("DD MMM YYYY")+")";
            }).error(function (data) {
        });
    };

    $scope.fetchAgenda(dateAgenda);

}

function getUniqueEvents($scope) {
    var eventsArr = [];
    $scope.eventsArr = [];

    eventsArr.push("meetingToday");
    eventsArr.push("meetingFollowUp");
    eventsArr.push("travellingToLocation");
    eventsArr.push("losingTouch");
    eventsArr.push("upcomingMeeting");

    $scope.eventsArr = eventsArr;
}

function getTodayMeetings($scope) {
    $scope.meetingsToday = [];

    $scope.meetingsToday =  _.filter($scope.eventsToday, function(event) {
        return !(event.ifRelatasMailEvent);
    })

    var openFor = getParams(window.location.href).for;
    var index = getParams(window.location.href).index;

    if(!index && openFor == 'todayMeeting' && $scope.meetingsToday.length > 0) {
        $scope.actionBoard($scope.meetingsToday[0]);
    }
}

function fetchUpcomingMeetings($scope,$http,$rootScope) {

    var all_ms = 0;

    $scope.upcomingMeetings =[];

    var fromDate = moment().add(1, "days").format();
    var toDate = moment().add(2, "days").format();

    $http.post('/get/all/meetings', {"fromDate": fromDate,
                                    "toDate": toDate})
        .success(function (response) {
            $rootScope.numberOfMeetingsToday = response.Data.length;

            if(response.SuccessCode){
                if(checkRequired(response.Data != undefined && response.Data.length > 0)){
                    $rootScope.meetingsWithToday = [];

                    _.each(response.Data, function(meeting) {

                        var meetingDetails = formatMeetingDetails(meeting,response.userId);
                        meetingDetails['participants'] = meeting.toList;
                        meetingDetails['numberOfMeetingParticipants'] = '';
                        meetingDetails['liu'] = response.Data.userId;
    
                        if(meeting.toList.length>1) {
                            meetingDetails['numberOfMeetingParticipantsExists'] = true;
                            meetingDetails['numberOfMeetingParticipants'] = meeting.toList.length -1
                            for(var j=0;j<meeting.toList.length;j++){
                                $rootScope.meetingsWithToday.push(meeting.toList[j].receiverEmailId)
                            }
                        }
    
                        $rootScope.meetingsWithToday.push(meetingDetails.personEmailId)
    
                        meetingDetails.meetingInitiatorIcon = setMeetingInitiatorIcon(meetingDetails);
                        meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/outgoingOrange.png';
                        if(meetingDetails.isSender && meetingDetails.isAccepted){
                            meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/Meeting_Outgoing_confirmed.png';
                        }
    
                        if(!meetingDetails.isSender && meetingDetails.isAccepted){
                            meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/IncomingGreen.png';
                        }
    
                        if(!meetingDetails.isSender && !meetingDetails.isAccepted){
                            meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/IncomingOrange.png';
                        }
    
                        meetingDetails.notSelfMeeting = true;
    
                        if(meetingDetails.participants.length === 0){
                            if((meetingDetails.personEmailId === meeting.to.receiverEmailId) || !meeting.to.receiverEmailId){
                                meetingDetails.notSelfMeeting = false;
                            }
                        }
    
                        if(meetingDetails.personEmailId === 'unknownorganizer@calendar.google.com'){
                            meetingDetails.nameTruncate = "Goal";
                            meetingDetails.picUrl = "/images/dashboard-icons/google-icon.png";
                            meetingDetails.meetingInitiatorIcon = "/images/dashboard-icons/flag.png";
                            meetingDetails.locationTypePic = "";
                            meetingDetails.isGoogleGoal = true;
                            meetingDetails.descriptionTruncate = getTextLength(meetingDetails.description,45);
                            meetingDetails.company = '';
                            meetingDetails.companyTruncate = '';
                        }
    
                        var date1_ms = meetingDetails.start.getTime();
                        var date2_ms = meetingDetails.end.getTime();
    
                        // Calculate the difference in milliseconds
                        var difference_ms = date2_ms - date1_ms;
    
                        all_ms = all_ms+difference_ms;

                        if(!meetingDetails.ifRelatasMailEventType) {
                            meetingDetails.formattedMeetingDate = moment(meetingDetails.start).format("DD MMM");
                            $scope.upcomingMeetings.push(meetingDetails);
                        }

                        $scope.upcomingMeetings.sort(function (o1, o2) {
                            return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                        });
                    })

                    var openFor = getParams(window.location.href).for;

                    if(openFor == 'upcomingMeeting' && $scope.upcomingMeetings.length > 0) {
                        $scope.actionBoard($scope.upcomingMeetings[0]);
    }

                }
            }
        })

}

function getParams(url) {
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

function paintContactsAtMyMeetingLocation($scope,$http,location,locationType,personEmailId,$rootScope,invitationId,date) {

    var locations = [], participants = [];

    location = location?location.replace(/[^\w\s]/gi, ''):false;

    if(location && locationType == "In-Person" && !isNumber(location)){
        // url = fetchUrlWithParameter(url, 'hierarchyList', companyData.Data.teamMembers);
        var url = '/fetch/contacts/at/my/meeting/location';
        url = url+"?locations="+location+"&meetingId="+invitationId;
        url = fetchUrlWithParameter(url, 'participants', personEmailId)
        
        $http.get(url)
            .success(function(response) {
                $scope.loadingpal = false;
                if(response && response.contacts && response.contacts.length>0){

                    var peopleAtThisLocation = [];
                    var numberOfContacts = response.contacts.length;
                    $scope.iCanConnect = true;

                    for(var i = 0;i<numberOfContacts;i++){
                        if(!include($rootScope.meetingsWithToday,response.contacts[i].personEmailId)){
                            peopleAtThisLocation.push(buildContactsToDisplay(response.contacts[i],location,response.city,date))
                        }
                    }

                    $scope.currentPage = 0;
                    $scope.pageSize = $rootScope.globalPageSize;
                    $scope.numberOfContactsToMeet = peopleAtThisLocation.length;
                    $scope.numberOfPages=function(){
                        return Math.ceil(peopleAtThisLocation.length/$scope.pageSize);
                    };

                    if(peopleAtThisLocation.length === 0){
                        $scope.iCanConnect = false;
                    }

                    $scope.peopleAtThisLocation = peopleAtThisLocation;

                    if(peopleAtThisLocation.length>1){
                        $scope.numberOfContacts = peopleAtThisLocation.length + ' contacts'
                    } else {
                        $scope.numberOfContacts = peopleAtThisLocation.length + ' contact'
                    }

                    if(peopleAtThisLocation.length>$scope.peopleAtThisLocation.length){
                        $scope.moreContacts = (peopleAtThisLocation.length - $scope.peopleAtThisLocation.length)
                    } else {
                        $scope.moreContacts = peopleAtThisLocation.length
                    }
                }
            });
    }
}

function buildContactsToDisplay(contact,location,city,date) {

    var distanceToPrevMeetingLocation = contact.pastMeetingLocation?'/fetch/distance/matrix?from='+location+"&destination="+contact.pastMeetingLocation:null
    var pastMeetingDate = contact.pastMeetingDate?contact.pastMeetingDate:null;

    if(checkRequired(contact.personId) && checkRequired(contact.personName)) {
        var name = getTextLength(contact.personName, 10);
        var image = '/getImage/' + contact.personId;

        return {
            fullName: toTitleCase(contact.personName),
            name: name,
            image: image,
            // cursor:'cursor:pointer',
            emailId: contact.personEmailId,
            mobileNumber: contact.mobileNumber ? contact.mobileNumber.replace(/[^a-zA-Z0-9]/g, '') : null,
            distanceToPrevMeetingLocation: distanceToPrevMeetingLocation,
            pastMeetingDate: pastMeetingDate,
            companyName: contact.companyName?contact.companyName:null,
            designation: contact.designation?contact.designation:null,
            actionItemId: contact.actionItemId,
            filterToFetch: 'peopleNearMeetingLocation',
            favoriteClass: contact.favorite?'contact-fav':false,
            location:city,
            meetingDate:date,
            twitterUserName:contact.twitterUserName,
            personId:contact.personId

        };
    } else {

        var noPicFlag = false;
        var contactImageLink = contact.contactImageLink?encodeURIComponent(contact.contactImageLink):null

        var noPicChar;
        if(contact.personEmailId && !contactImageLink){
            noPicFlag = true;
            noPicChar = contact.personEmailId.substr(0,2).toUpperCase()
        } else if(contact.personName && !contactImageLink){
            noPicChar = contact.personName.substr(0,2).toUpperCase()
            noPicFlag = true;
        }

        return {
            fullName:contact.personName,
            name:getTextLength(contact.personName,10),
            image:'/getContactImage/'+contact.personEmailId+'/'+contactImageLink,
            cursor:'cursor:pointer',
            noPicFlag:false,
            noPPic: noPicChar,
            url:null,
            className:'hide',
            emailId : contact.personEmailId?contact.personEmailId:contact.mobileNumber,
            mobileNumber : contact.mobileNumber?contact.mobileNumber.replace(/[^a-zA-Z0-9]/g,''):null,
            nameNoImg: contact.personName?contact.personName.substr(0,2).toUpperCase():'',
            distanceToPrevMeetingLocation: distanceToPrevMeetingLocation,
            pastMeetingDate: pastMeetingDate,
            companyName: contact.companyName?contact.companyName:null,
            designation: contact.designation?contact.designation:null,
            actionItemId: contact.actionItemId,
            filterToFetch: 'peopleNearMeetingLocation',
            favoriteClass: contact.favorite?'contact-fav':false,
            location:city,
            meetingDate:date,
            twitterUserName:contact.twitterUserName,
            personId:contact.personId
        };

    }
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
function salesForce($scope,$http,callback) {

    $http.get('/salesforce/get/upcoming/opportunities')
        .success(function (response) {
            if(response.SuccessCode){
                callback(response.Data,response.timezone)
            } else {
                callback([])
            }
        });
}

function opportunitiesToday($scope,$http,callback) {

    $http.get('/salesforce/get/today/opportunities')
        .success(function (response) {
            if(response.SuccessCode){
                callback(response.Data,response.timezone)
            } else {
                callback([])
            }
        });
}

function getTextLength(text,maxLength){

    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+' ...';
    }
    else return text;
}

function checkRequired(data) {
    if (data == '' || data == null || data == undefined) {
        return false;
    } else {
        return true;
    }
}

toastr.options.onHidden = function() {
    sessionStorage.setItem("is_reloaded", true);
    sessionStorage.setItem("persistent", "persistent");
};

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function getDateFormat(date,ISOFormat){
    date = checkRequired(timezone) ? moment(date).tz(timezone) : moment(date);
    if(ISOFormat)
        return date.format();
    else
        return date.format("DD-MM-YYYY hh:mm a");
}

function getTimeFormat(date) {
    date = checkRequired(timezone) ? moment(date).tz(timezone) : moment(date)
    var hrs = date.hours();
    var min = date.minutes();
    if (hrs < 10) {
        hrs = "0" + hrs
    }

    if (min < 10) {
        min = "0" + min;
    }

    return hrs +':'+ min
}

function include(arr,item) {
    return (arr.indexOf(item) != -1);
}

function updateActionTakenInfo(recordId,actionTakenType,$http,sharedServiceSelection,filter) {

    var obj = {
        actionItemId:recordId,
        actionTakenSource:"dashboard",
        actionTakenType:actionTakenType
    }

    $http.post("/update/action/taken/info",obj)
        .success(function(response){
            if(response.SuccessCode) {
                sharedServiceSelection.fetchModuleShared(filter);
                // toastr.success(response.Message);
            }
            else {
                // toastr.error(response.Message);
            }
        });
}

var formatMeetingDetails = function(meeting,userId){

    counter++;

    var locationTruncate = '';
    if(meeting.scheduleTimeSlots[0].location){
        locationTruncate = getTextLength(meeting.scheduleTimeSlots[0].location, 50);
    }

    var titleTruncate = '';
    if(meeting.scheduleTimeSlots[0].title) {
        titleTruncate = getTextLength(meeting.scheduleTimeSlots[0].title, 50)
    }
    
    var date = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].start.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].start.date);
    var end = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].end.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].end.date);
    var obj = {
        start:new Date(date),
        end:new Date(end),
        sortDate:new Date(date.format()),
        meetingDuration: end.diff(date,'minutes'),
        startTime:date.format("hh:mm A"),
        endTime:end.format("hh:mm A"),
        title:meeting.scheduleTimeSlots[0].title || '',
        description:meeting.scheduleTimeSlots[0].description || '',
        location:meeting.scheduleTimeSlots[0].location || '',
        locationTruncate:locationTruncate,
        titleTruncate:titleTruncate,
        locationType:meeting.scheduleTimeSlots[0].locationType || '',
        date:date.format("DD MMM YYYY"),
        dateUpcoming:date.format("MMM DD YYYY"),
        invitationId:meeting.invitationId,
        url:'/today/details/'+ meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId,
        isPreparedMeeting:isPreparedMeeting(meeting,userId),
        isSender:false,
        isAccepted:meeting.scheduleTimeSlots[0].isAccepted || false,
        locationTypePic: getLocationTypePic(meeting.scheduleTimeSlots[0].locationType),
        ifRelatasMailEvent:meeting.actionItemSlot?meeting.actionItemSlot:false,
        ifRelatasMailEventType:meeting.actionItemSlotType,
        meetingIndex:'re-schedule-meeting'+counter,
        slotId:meeting.scheduleTimeSlots[0]._id,
        hoverClass:meeting.actionItemSlot?'non-meeting-row':'meeting-row',
        updateRelatasEvent:'update-relatas-event'+counter,
        displayPopOver:false
    };

    obj.preparedMeetingUrl = obj.isPreparedMeeting ? null : '/today/details/'+ (meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId);

    if(meeting.suggested){
        obj.location = checkRequired(meeting.scheduleTimeSlots[0].suggestedLocation) ? meeting.scheduleTimeSlots[0].suggestedLocation : meeting.scheduleTimeSlots[0].location;
    }

    if(meeting.suggested){
        if(meeting.suggestedBy.userId == userId){
            obj.isSender = true;
            if(meeting.senderId == userId){
                var data1 = getDetailsIfSender(meeting);
                obj.picUrl = data1.picUrl;
                obj.name = data1.name;
                obj.userId = data1.userId
                obj.personEmailId = data1.personEmailId
            }
            else{
                var data3 = getDetailsIfReceiver(meeting);
                obj.picUrl = data3.picUrl;
                obj.name = data3.name;
                obj.userId = data3.userId
                obj.personEmailId = data3.personEmailId
            }
        }
        else{
            if(meeting.senderId == meeting.suggestedBy.userId){
                obj.isSender = true;
                var data5 = getDetailsIfReceiver(meeting);
                obj.picUrl = data5.picUrl;
                obj.name = data5.name;
                obj.userId = data5.userId
                obj.personEmailId = data5.personEmailId
            }
            else{
                if(meeting.selfCalendar){
                    for(var j=0; j<meeting.toList.length; j++){
                        if(meeting.suggestedBy.userId == meeting.toList[j].receiverId){
                            obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                            var fName = meeting.toList[j].receiverFirstName || '';
                            var lName = meeting.toList[j].receiverLastName || '';
                            obj.name = fName+' '+lName;
                            obj.personEmailId = meeting.toList[j].receiverEmailId;
                        }
                    }
                }
                else{
                    obj.picUrl = '/getImage/'+meeting.to.receiverId;
                    obj.name = meeting.to.receiverName;
                }
            }
        }
    }
    else{
        if(meeting.senderId == userId){
            obj.isSender = true;
            var data2 = getDetailsIfSender(meeting);

            obj.picUrl = data2.picUrl;
            obj.name = data2.name;
            obj.personEmailId = data2.personEmailId;
            obj.userId = data2.userId;
        }
        else{
            var data4 = getDetailsIfReceiver(meeting);
            obj.picUrl = data4.picUrl;
            obj.name = data4.name;
            obj.personEmailId = data4.personEmailId;
            obj.userId = data4.userId
        }
    }
    if(!checkRequired(obj.picUrl) && !checkRequired(obj.name)){
        obj.picUrl = '/getImage/'+meeting.senderId;
        obj.name = meeting.senderName;
    }
    if(!checkRequired(obj.picUrl)){
        obj.picUrl = '/images/default.png';
    }
    obj.prepareButtonText = isMeetingAccepted(meeting,userId) ? "Confirmed" : obj.isSender ? "Not Confirmed" : "Confirm";

    if (obj.prepareButtonText == "Confirmed" || obj.prepareButtonText == "Not Confirmed" && obj.isSender === true) {
        obj.buttonClass = "btn btn-transparent";
    }
    else {
        obj.buttonClass = "btn btn-green";
    }

    obj.nameTruncate = getTextLength(obj.name,10);

    var designation = '';
    var company = '';
    for(var i =0;i<meeting.toList.length;i++){

        if(meeting.toList[i].receiverEmailId == obj.personEmailId){
            company = meeting.toList[i].companyName?meeting.toList[i].companyName:'';
            designation = meeting.toList[i].designation?meeting.toList[i].designation:'';
        } else if(meeting.senderEmailId == obj.personEmailId){
            company = meeting.companyName
            designation = meeting.designation
        }

        var companyTruncate = getTextLength(company,10)
        var designationTruncate = getTextLength(designation,10)
    }

    obj.company = company;
    obj.designation = designation;
    obj.companyTruncate = companyTruncate
    obj.designationTruncate = designationTruncate

    return obj;
};

var isPreparedMeeting = function(meeting,userId){

    if(meeting.senderId == userId){
        return meeting.isSenderPrepared;
    }
    else if(meeting.selfCalendar){
        var isExists = false;
        var prepared = true;
        for(var j=0; j<meeting.toList.length; j++){
            if(meeting.toList[j].receiverId == userId){
                isExists = true;
                prepared = meeting.toList[j].isPrepared || false;
            }
        }
        return prepared;
    }
    else{
        if(meeting.to.receiverId == userId){
            return meeting.to.isPrepared || false;
        }else return true;
    }
};

var getLocationTypePic = function(meetingLocationType){

    switch (meetingLocationType) {
        case 'In-Person':
            return "fa fa-map-marker";
            break;
        case 'Phone':
            return "fa fa-mobile-phone";
            break;
        case 'Skype':
            return "fa fa-skype";
            break;
        case 'Conference Call':
            return "fa fa-mobile-phone";
            break;
        default:return "fa fa-map-marker";
            break;
    }
};

var getDetailsIfSender = function(meeting){

    var obj = {};
    if(meeting.selfCalendar == true){
        if(meeting.toList.length > 0){
            var isDetailsExists = false;
            for(var i=0; i<meeting.toList.length; i++){
                if(meeting.toList[i].isPrimary){
                    isDetailsExists = true;
                    obj.picUrl = '/getImage/'+meeting.toList[i].receiverId;
                    var fName = meeting.toList[i].receiverFirstName || '';
                    var lName = meeting.toList[i].receiverLastName || '';
                    obj.name = fName+' '+lName;
                    obj.userId = meeting.toList[i].receiverId;
                    obj.personEmailId = meeting.toList[i].receiverEmailId;
                }
            }
            if(!isDetailsExists){
                for(var j=0; j<meeting.toList.length; j++){
                    if(checkRequired(meeting.toList[j].receiverId)){
                        isDetailsExists = true;
                        obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                        var fName2 = meeting.toList[j].receiverFirstName || '';
                        var lName2 = meeting.toList[j].receiverLastName || '';
                        obj.name = fName2+' '+lName2;
                        obj.userId = meeting.toList[j].receiverId
                        obj.personEmailId = meeting.toList[j].receiverEmailId;
                    }
                }
            }
            if(!isDetailsExists){
                obj.picUrl = '/images/default.png';
                obj.name = meeting.toList[0].receiverEmailId;
                obj.personEmailId = meeting.toList[0].receiverEmailId;
            }
        }
    }
    else{
        if(meeting.to){
            if(meeting.to.receiverId){
                obj.picUrl = '/getImage/'+meeting.to.receiverId;
                obj.name = meeting.to.receiverName;
                obj.userId = meeting.to.receiverId
                obj.personEmailId = meeting.to.receiverEmailId;
            }
            else{
                obj.picUrl = '/images/default.png';
                obj.name = meeting.to.receiverEmailId;
                obj.personEmailId = meeting.to.receiverEmailId;
            }
        }
    }
    return obj;
};

var getDetailsIfReceiver = function(meeting){
    var obj = {};
    if(meeting.senderId){
        obj.picUrl = '/getImage/'+meeting.senderId;
        obj.name = meeting.senderName;
        obj.userId = meeting.senderId;
        obj.personEmailId = meeting.senderEmailId;
    }
    else{
        obj.picUrl = '/images/default.png';
        obj.name = meeting.senderEmailId;
        obj.personEmailId = meeting.senderEmailId;
    }
    return obj;
};

var isMeetingAccepted = function(meeting,userId){

    var isAccepted = false;
    if(meeting.scheduleTimeSlots && meeting.scheduleTimeSlots.length > 0){
        for(var i=0; i<meeting.scheduleTimeSlots.length; i++){
            if(meeting.scheduleTimeSlots[i].isAccepted){
                isAccepted = true;
            }
        }
    }

    if(isAccepted){
        if(meeting.selfCalendar && meeting.toList && meeting.toList.length > 1){
            if(meeting.senderId != userId){
                var exists = false;
                for(var j=0; j<meeting.toList.length > 0; j++){
                    if(meeting.toList[j].receiverId == userId){
                        exists = true;
                    }
                }
                if(!exists){
                    isAccepted = false;
                }
            }
        }
    }

    return isAccepted;
};

function setMeetingParticipants(obj,meeting,userId,$rootScope){

    obj['participants'] = meeting.toList;
    obj['numberOfMeetingParticipants'] = '';
    obj['liu'] = userId;

    obj['notSelfMeeting'] = true;

    if(obj.participants.length === 0){
        obj['notSelfMeeting'] = false;
    }

    if(meeting.toList.length>1) {
        obj['numberOfMeetingParticipantsExists'] = true;
        obj['numberOfMeetingParticipants'] = meeting.toList.length -1
        // for(var i=0;i<meeting.toList.length;i++){
        //     $rootScope.meetingsWithToday.push(meeting.toList[i].receiverEmailId)
        // }
    }

    return obj;
};

function setMeetingInitiatorIcon(meetingDetails){
    var meetingInitiatorIcon = '/images/dashboard-icons/outgoingOrange.png';
    if(meetingDetails.isSender && meetingDetails.isAccepted){
        meetingInitiatorIcon = '/images/dashboard-icons/Meeting_Outgoing_confirmed.png';
    }

    if(!meetingDetails.isSender && meetingDetails.isAccepted){
        meetingInitiatorIcon = '/images/dashboard-icons/IncomingGreen.png';
    }

    if(!meetingDetails.isSender && !meetingDetails.isAccepted){
        meetingInitiatorIcon = '/images/dashboard-icons/IncomingOrange.png';
    }

    return meetingInitiatorIcon;
};

var formatUpcomingMeetingDetails = function(meeting,userId){

    counter++;
    var designation = '';
    var company = '';
    for(var i =0;i<meeting.toList.length;i++){
        company = meeting.toList[i].companyName?meeting.toList[i].companyName:'';
        designation = meeting.toList[i].designation?meeting.toList[i].designation:'';

        var companyTruncate = getTextLength(company,10)
        var designationTruncate = getTextLength(designation,10)
    }

    var locationTruncate = '';
    if(meeting.scheduleTimeSlots[0].location){
        locationTruncate = getTextLength(meeting.scheduleTimeSlots[0].location, 50);
    }

    var titleTruncate = '';
    if(meeting.scheduleTimeSlots[0].title) {
        titleTruncate = getTextLength(meeting.scheduleTimeSlots[0].title, 50)
    }

    var date = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].start.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].start.date);
    var end = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].end.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].end.date);
    var obj = {
        start:new Date(date),
        end:new Date(end),
        sortDate:new Date(date.format()),
        meetingDuration: end.diff(date,'minutes'),
        startTime:date.format("hh:mm A"),
        endTime:end.format("hh:mm A"),
        title:meeting.scheduleTimeSlots[0].title || '',
        description:meeting.scheduleTimeSlots[0].description || '',
        location:meeting.scheduleTimeSlots[0].location || '',
        locationTruncate:locationTruncate,
        titleTruncate:titleTruncate,
        locationType:meeting.scheduleTimeSlots[0].locationType || '',
        date:date.format("DD MMM YYYY"),
        dateUpcoming:date.format("MMM DD YYYY"),
        invitationId:meeting.invitationId,
        url:'/today/details/'+ meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId,
        isPreparedMeeting:isPreparedMeeting(meeting,userId),
        isSender:false,
        isAccepted:meeting.scheduleTimeSlots[0].isAccepted || false,
        locationTypePic: getLocationTypePic(meeting.scheduleTimeSlots[0].locationType),
        company:company,
        designation:designation,
        ifRelatasMailEvent:meeting.actionItemSlot?meeting.actionItemSlot:false,
        ifRelatasMailEventType:meeting.actionItemSlotType,
        meetingIndex:'re-schedule-meeting'+counter,
        slotId:meeting.scheduleTimeSlots[0]._id,
        hoverClass:meeting.actionItemSlot?'non-meeting-row':'meeting-row',
        updateRelatasEvent:'update-relatas-event'+counter,
        displayPopOver:false,
        companyTruncate:companyTruncate,
        designationTruncate:designationTruncate
    };

    obj.preparedMeetingUrl = obj.isPreparedMeeting ? null : '/today/details/'+ (meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId);

    if(meeting.suggested){
        obj.location = checkRequired(meeting.scheduleTimeSlots[0].suggestedLocation) ? meeting.scheduleTimeSlots[0].suggestedLocation : meeting.scheduleTimeSlots[0].location;
    }

    if(meeting.suggested){
        if(meeting.suggestedBy.userId == userId){
            obj.isSender = true;
            if(meeting.senderId == userId){
                var data1 = getDetailsIfSender(meeting);
                obj.picUrl = data1.picUrl;
                obj.name = data1.name;
                obj.userId = data1.userId
                obj.personEmailId = data1.personEmailId
            }
            else{
                var data3 = getDetailsIfReceiver(meeting);
                obj.picUrl = data3.picUrl;
                obj.name = data3.name;
                obj.userId = data3.userId
                obj.personEmailId = data3.personEmailId
            }
        }
        else{
            if(meeting.senderId == meeting.suggestedBy.userId){
                obj.isSender = true;
                var data5 = getDetailsIfReceiver(meeting);
                obj.picUrl = data5.picUrl;
                obj.name = data5.name;
                obj.userId = data5.userId
                obj.personEmailId = data5.personEmailId
            }
            else{
                if(meeting.selfCalendar){
                    for(var j=0; j<meeting.toList.length; j++){
                        if(meeting.suggestedBy.userId == meeting.toList[j].receiverId){
                            obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                            var fName = meeting.toList[j].receiverFirstName || '';
                            var lName = meeting.toList[j].receiverLastName || '';
                            obj.name = fName+' '+lName;
                            obj.personEmailId = meeting.toList[j].receiverEmailId;
                        }
                    }
                }
                else{
                    obj.picUrl = '/getImage/'+meeting.to.receiverId;
                    obj.name = meeting.to.receiverName;
                }
            }
        }
    }
    else{
        if(meeting.senderId == userId){
            obj.isSender = true;
            var data2 = getDetailsIfSender(meeting);

            obj.picUrl = data2.picUrl;
            obj.name = data2.name;
            obj.personEmailId = data2.personEmailId;
            obj.userId = data2.userId;
        }
        else{
            var data4 = getDetailsIfReceiver(meeting);
            obj.picUrl = data4.picUrl;
            obj.name = data4.name;
            obj.personEmailId = data4.personEmailId;
            obj.userId = data4.userId
        }
    }
    if(!checkRequired(obj.picUrl) && !checkRequired(obj.name)){
        obj.picUrl = '/getImage/'+meeting.senderId;
        obj.name = meeting.senderName;
    }
    if(!checkRequired(obj.picUrl)){
        obj.picUrl = '/images/default.png';
    }
    obj.prepareButtonText = isMeetingAccepted(meeting,userId) ? "Confirmed" : obj.isSender ? "Not Confirmed" : "Confirm";

    if (obj.prepareButtonText == "Confirmed" || obj.prepareButtonText == "Not Confirmed" && obj.isSender === true) {
        obj.buttonClass = "btn btn-transparent";
    }
    else {
        obj.buttonClass = "btn btn-green";
    }

    obj.nameTruncate = getTextLength(obj.name,10);

    return obj;
};

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            scope.imageNotLoaded = function(obj){

                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else{
                    obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    obj.noPPic = obj.name.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                //call the function that was passed
                scope.$apply(attrs.imageonload);
            });
        }
    };
});

function millisToTime(ms){

    var x = ms / 1000;
    var seconds = Math.floor(x % 60);
    x /= 60;
    var minutes = Math.floor(x % 60);
    x /= 60;
    var hours = Math.floor(x % 24);
    x /= 24;
    var days = Math.floor(x);

    return {"Days" : days, "Hours" : hours, "Minutes" : minutes, "Seconds" : seconds};
}

function closeWhenClickingElsewhere(event, callbackOnClose) {

    var clickedElement = event.target;
    if (!clickedElement) return;

    var elementClasses = clickedElement.classList;
    var clickedOnSearchDrawer = elementClasses.contains('fa-gear')
        || elementClasses.contains('social-settings-dropdown')
        || (clickedElement.parentElement !== null
        && clickedElement.parentElement.classList.contains('social-settings-dropdown'));
    if (!clickedOnSearchDrawer) {
        callbackOnClose();
    }

}

// comment
function closeAllActionBoard($rootScope,callback){
    $rootScope.sendEmailBoardToggle = false;
    if(callback){callback(true)}
}

var getValidStringNumber = function(number){
    if(typeof number == 'number'){
        return number < 10 ? '0'+number : number
    }
    else return '00';
};