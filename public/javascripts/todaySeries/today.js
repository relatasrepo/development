var reletasApp = angular.module('reletasApp', ['ngRoute','ngCookies']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope,$http,meetingObjService){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = false
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/todaySeries/leftMeetingBar"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        //var str = searchContent.replace(/[^\w\s]/gi, '');

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;

            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
    meetingObjService.searchFromHeader = $scope.searchFromHeader;

});

var timezone ;

reletasApp.service('meetingObjService', function () {
    return {}
});

reletasApp.factory('sharedServiceSelection', function($rootScope,$http) {
    var sharedService = {};
    sharedService.message = '';
    sharedService.prepForBroadcast = function(msg) {
        this.message = msg;
        this.broadcastItem();
    };

    sharedService.broadcastItem = function() {
        $rootScope.$broadcast('handleBroadcast');
    };

    return sharedService;
});

reletasApp.factory('socket', function ($rootScope) {
// var socket = io.connect();
return {
    on: function (eventName, callback) {
        socket.on(eventName, function () {
            var args = arguments;
            $rootScope.$apply(function () {
                callback.apply(socket, args);
            });
        });
    },
    emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
            var args = arguments;
            $rootScope.$apply(function () {
                if (callback) {
                    callback.apply(socket, args);
                }
            });
        })
    }
};
})

reletasApp.controller("logedinUser", function ($scope, $http,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {
            setTimeout(function(){
                $(".seeMoreIcon").addClass("disabled-see-more")
            },1000)

            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
            }
            else{

            }
        }).error(function (data) {

        })
});

/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

//reletasApp.controller("linkedin_alert", function($scope, $http){
//    var myFunc=function() {
//        $http.get('/linkedinflag')
//            .success(function (response) {
//                if (response == "oops") {
//                    // setTimeout(myFunc,1000);
//                }
//                if (response === false) {
//                    $(".profile-banner-grid .alert").show();
//                }
//            });
//    };
//    myFunc();
//});
//
//reletasApp.controller("facebook_alert", function($scope, $http){
//    var myFunc=function() {
//        $http.get('/facebookflag')
//            .success(function (response) {
//                if (response == "oops") {
//                    // setTimeout(myFunc,1000);
//                }
//                if (response === false) {
//                    $(".profile-banner-grid .fb-alert").show();
//                }
//            });
//    }
//    myFunc();
//});

reletasApp.controller("facebook_linkedin_alert", function($scope, $http,socket,$cookies,sharedServiceSelection){

    $scope.alertAll = false;
    $scope.alertFacebook = false;
    $scope.alertLinkedin = false;

    // Alert hide until social is fixed.
    var myFunc =function() {
       $http.get('/socialflag')
           .success(function (response) {
               if (response == "oops") {
                   setTimeout(myFunc,1000);
               }
               else {
                   if(response.linkedInFlag === false && response.facebookFlag === false) {

                       $scope.alertAll = true;
                       $http.delete('/profile/update/remove/linkedin/web');
                       $http.delete('/profile/update/remove/facebook/web');

                   } else if (response.linkedInFlag === true && response.facebookFlag === false) {

                       $scope.alertFacebook = true;
                       $http.delete('/profile/update/remove/facebook/web');
                   } else if(response.linkedInFlag === false && response.facebookFlag === true){

                       $scope.alertLinkedin = true;
                       $http.delete('/profile/update/remove/linkedin/web');
                   }
               }
           });
    }
    //myFunc(); // Alert hide until social is fixed.

var setTimeoutCounter = 0;

    var msgShownExpire = moment().add(2, "hours").toDate()

    var msgShownCookie = $cookies.get('msgShown');

    var meetingsUpdate = function() {

      //For Meetings update
      $http.get('/meetingsUpdateFlash')
          .success(function(response){
            setTimeoutCounter++;
            var msgShownCookie = $cookies.get('msgShown');

              if(!response){
                if(setTimeoutCounter<15 && msgShownCookie || msgShownCookie != 'true'){

                  setTimeout(meetingsUpdate,5000);
                }
              } else {

                toastr.success("Your meeting details have been updated.");
                $http.post('/meetingsUpdateDone',{'meetingsUpdateShown':true})
                $cookies.put('msgShown', true,{expires:msgShownExpire});

                sharedServiceSelection.prepForBroadcast("refresh");
              }
          });
       }

       var msgShown = $cookies.get('msgShown');
       if(!msgShown){
          meetingsUpdate();
       }
});

reletasApp.controller("bottom_search", function ($scope, $http, meetingObjService) {
    $scope.mySearchFunction = function(searchContent){
        if(typeof searchContent == 'string' && checkRequired(searchContent.trim())){
            //window.location = '/contact/connect?searchContent='+searchContent;
            meetingObjService.searchFromHeader(searchContent,false,true,false);
            mixpanel.track('Contact Search 3A.1');

        }
    };
    $scope.goToContactsSearch = function(searchContent){
        if(typeof searchContent == 'string' && checkRequired(searchContent.trim())){
            //window.location = '/contact/connect?searchContent='+searchContent;
            meetingObjService.searchFromHeader(searchContent,false,true,false);
            mixpanel.track('Contact Search 3A.2');
        }
    }
});

var once = true;
reletasApp.controller("left_bar_today", function ($scope, $http,meetingObjService,sharedServiceSelection) {
    $scope.todayMeetings = [];
    $scope.newMeetings = [];
    $scope.upcomingMeetings = [];
    $scope.tasks = {};

    $scope.$on('handleBroadcast', function() {
      if(sharedServiceSelection.message = "refresh") {
        meetingObjService.fetchLeftBarTodayData();
      }
    });

    if(once){
        once = false;
        meetingObjService.fetchLeftBarTodayData = function() {
            $scope.todayMeetings = [];
            $scope.newMeetings = [];
            $scope.upcomingMeetings = [];
            $scope.tasks = {};

            $http.get('/today/left/section')
                .success(function (response) {
                    if(response.SuccessCode){

                        console.log("Reponse from today left section:", response);

                        if(checkRequired(response.Data.timezone)){
                            timezone = response.Data.timezone;
                        }
                        if(typeof response.Data.confirmedMeetings != 'string' && checkRequired(response.Data.confirmedMeetings != undefined && response.Data.confirmedMeetings.length > 0)){
                            response.Data.confirmedMeetings.sort(function (o1, o2) {
                                var date1 = new Date(o1.scheduleTimeSlots[0].start.date);
                                var date2 = new Date(o2.scheduleTimeSlots[0].start.date);
                                return date1 < date2 ? -1 : date1 > date2 ? 1 : 0;
                            });
                            for(var i=0; i<response.Data.confirmedMeetings.length; i++){
                                if($scope.todayMeetings.length < 3){
                                    var m = $scope.formatMeetingDetails(response.Data.confirmedMeetings[i],response.Data.userId)
                                    $scope.todayMeetings.push(m)
                                    $scope.todayMeetings.sort(function (o1, o2) {
                                        return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                    });
                                }
                            }
                        }
                        if(typeof response.Data.pendingMeetings != 'string' && checkRequired(response.Data.pendingMeetings != undefined && response.Data.pendingMeetings.length > 0)){
                            response.Data.pendingMeetings.sort(function (o1, o2) {
                                var date1 = new Date(o1.scheduleTimeSlots[0].start.date);
                                var date2 = new Date(o2.scheduleTimeSlots[0].start.date);
                                return date1 < date2 ? -1 : date1 > date2 ? 1 : 0;
                            });
                            var pendingMeetingsMeetingsSourceArr = [];
                            for(var j=0; j<response.Data.pendingMeetings.length; j++){
                                if($scope.newMeetings.length < 3){
                                    $scope.newMeetings.push($scope.formatMeetingDetails(response.Data.pendingMeetings[j],response.Data.userId));
                                    console.log($scope.formatMeetingDetails(response.Data.pendingMeetings[j]))
                                    $scope.newMeetings.sort(function (o1, o2) {
                                        return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                    });
                                }
                            }
                        }
                        if(typeof response.Data.upcomingMeetings != 'string' && checkRequired(response.Data.upcomingMeetings != undefined && response.Data.upcomingMeetings.length > 0)){
                            response.Data.upcomingMeetings.sort(function (o1, o2) {
                                var date1 = new Date(o1.scheduleTimeSlots[0].start.date);
                                var date2 = new Date(o2.scheduleTimeSlots[0].start.date);
                                return date1 < date2 ? -1 : date1 > date2 ? 1 : 0;
                            });
                            for(var k=0; k<response.Data.upcomingMeetings.length; k++){
                                //if($scope.upcomingMeetings.length < 5)
                                if(k < 3){
                                    var mObj = $scope.formatMeetingDetails(response.Data.upcomingMeetings[k],response.Data.userId);
                                    var a = {
                                        dateText:mObj.dateUpcoming,
                                        sortDate:mObj.sortDate,
                                        meetings:[mObj]
                                    };
                                    var exists = false;
                                    for(var z=0; z<$scope.upcomingMeetings.length; z++){
                                        if($scope.upcomingMeetings[z].dateText == mObj.dateUpcoming){
                                            exists = true;
                                            $scope.upcomingMeetings[z].meetings.push(mObj)
                                            $scope.upcomingMeetings[z].meetings.sort(function (o1, o2) {
                                                return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                            });
                                        }
                                    }
                                    if(!exists){
                                        $scope.upcomingMeetings.push(a);
                                    }
                                    $scope.upcomingMeetings.sort(function (o1, o2) {
                                        return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                    });

                                }else break;
                            }
                        }
                        if(typeof response.Data.pendingTasks != 'string' && checkRequired(response.Data.pendingTasks != undefined && response.Data.pendingTasks.length > 0)){
                            response.Data.pendingTasks.sort(function (o1, o2) {
                                var date1 = new Date(o1.dueDate);
                                var date2 = new Date(o2.dueDate);
                                return date1 < date2 ? -1 : date1 > date2 ? 1 : 0;
                            });
                            for(var l=0; l<response.Data.pendingTasks.length; l++){
                                if(l < 3){
                                    var obj = {
                                        url:'/task/'+response.Data.pendingTasks[l]._id,
                                        taskId:response.Data.pendingTasks[l]._id,
                                        sortDate:new Date(response.Data.pendingTasks[l].dueDate),
                                        status:response.Data.pendingTasks[l].status == 'complete',
                                        updateTaskStatus:response.Data.pendingTasks[l].assignedTo == response.Data.userId
                                    };
                                    obj.taskName = response.Data.pendingTasks[l].taskName || 'No Name'
                                    obj.taskName = getTextLength(obj.taskName,20);
                                    if(response.Data.pendingTasks[l].assignedTo == response.Data.userId){
                                        obj.id = response.Data.pendingTasks[l].createdBy;
                                        obj.separate = response.Data.pendingTasks[l].createdByEmailId
                                        obj.picUrl = '/getImage/'+response.Data.pendingTasks[l].createdBy || '/images/default.png';
                                    }
                                    else{
                                        obj.id = response.Data.pendingTasks[l].assignedTo;
                                        obj.separate = response.Data.pendingTasks[l].assignedToEmailId
                                        obj.picUrl = '/getImage/'+response.Data.pendingTasks[l].assignedTo || '/images/default.png';
                                    }
                                    if($scope.tasks[obj.separate]){
                                        $scope.tasks[obj.separate].items.push(obj);
                                        $scope.tasks[obj.separate].items.sort(function (o1, o2) {
                                            return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                        });
                                    }
                                    else{
                                        $scope.tasks[obj.separate] = {items:[],picUrl:obj.picUrl};
                                        $scope.tasks[obj.separate].items = [obj];
                                    }
                                }
                            }
                        }
                    }
                    if(typeof response.Data.pendingMeetings == 'string'){
                        $scope.noNewInvitationsMessage = response.Data.pendingMeetings;
                    }
                    if(typeof response.Data.pendingTasks == 'string'){
                        $scope.noTasksMessage = response.Data.pendingTasks;
                    }
                    if(typeof response.Data.confirmedMeetings == 'string'){
                        $scope.noConfirmedMeetingsMessage = response.Data.confirmedMeetings;
                    }
                    if(typeof response.Data.upcomingMeetings == 'string'){
                        $scope.noUpcomingMeetingsMessage = response.Data.upcomingMeetings;
                    }
                }).error(function (data) {

                });
        }
        meetingObjService.fetchLeftBarTodayData();
    }

    $scope.goToCalendar = function(){
        window.location = '/calendar'
    };

    $scope.goToTasks = function(){
        window.location = '/tasks'
    };

    $scope.goToUrl = function(url){
        if(url && url.length > 0 && url[0] && url[0][0] && url[0][0] != 'null' && url[0][0] != null ) {
            window.location = url[0][0];
        }
    };

    $scope.formatMeetingDetails = function(meeting,userId){
        var date = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].start.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].start.date);
        var obj = {
            sortDate:new Date(date.format()),
            start:date.format("hh:mm A"),
            end:checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].end.date).tz(timezone).format("hh:mm A") : moment(meeting.scheduleTimeSlots[0].end.date),
            title:meeting.scheduleTimeSlots[0].title,
            date:date.format("DD MMM YYYY"),
            dateUpcoming:date.format("MMM DD YYYY"),
            invitationId:meeting.invitationId,
            moreParticipatsShow:meeting.selfCalendar && meeting.toList && meeting.toList.length > 1 ? true : false,
            url:'/today/details/'+ (meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId),
            className:''
        };
        obj.title = getTextLength(obj.title,20);
        if(meeting.suggested){
            if(meeting.suggestedBy.userId == userId){
                if(meeting.senderId == userId){
                    var data1 = $scope.getDetailsIfSender(meeting);
                    obj.picUrl = data1.picUrl;
                    obj.name = data1.name;
                }
                else{
                    var data3 = $scope.getDetailsIfReceiver(meeting);
                    obj.picUrl = data3.picUrl;
                    obj.name = data3.name;
                }
            }
            else{
                if(meeting.senderId == meeting.suggestedBy.userId){
                    var data5 = $scope.getDetailsIfReceiver(meeting)
                    obj.picUrl = data5.picUrl;
                    obj.name = data5.name;
                }
                else{
                    if(meeting.selfCalendar){
                        for(var j=0; j<meeting.toList.length; j++){
                            if(meeting.suggestedBy.userId == meeting.toList[j].receiverId){
                                obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                                var fName = meeting.toList[j].receiverFirstName || '';
                                var lName = meeting.toList[j].receiverLastName || '';
                                obj.name = fName+' '+lName;
                            }
                        }
                    }
                    else{
                        obj.picUrl = '/getImage/'+meeting.to.receiverId;
                        obj.name = meeting.to.receiverName;
                    }
                }
            }
        }
        else{
            if(meeting.senderId == userId){
                var data2 = $scope.getDetailsIfSender(meeting);
                obj.picUrl = data2.picUrl;
                obj.name = data2.name;
            }
            else{
                var data4 = $scope.getDetailsIfReceiver(meeting);
                obj.picUrl = data4.picUrl;
                obj.name = data4.name;
            }
        }

        if(!checkRequired(obj.picUrl) && !checkRequired(obj.name)){
            obj.picUrl = '/getImage/'+meeting.senderId;
            obj.name = meeting.senderName;
        }

        if(!checkRequired(obj.picUrl)){
            obj.noPicFlag = true;
        }

        return obj;
    };

    $scope.getDetailsIfSender = function(meeting){

        var obj = {};
        if(meeting.selfCalendar){

            if(meeting.toList.length > 0){

                var isDetailsExists = false;
                for(var i=0; i<meeting.toList.length; i++){
                    if(meeting.toList[i].isPrimary){
                        isDetailsExists = true;
                        obj.picUrl = '/getImage/'+meeting.toList[i].receiverId;
                        var fName = meeting.toList[i].receiverFirstName || '';
                        var lName = meeting.toList[i].receiverLastName || '';
                        obj.name = fName+' '+lName;
                    }
                }
                if(!isDetailsExists){
                    for(var j=0; j<meeting.toList.length; j++){
                        if(checkRequired(meeting.toList[j].receiverId)){
                            isDetailsExists = true;
                            obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                            var fName2 = meeting.toList[j].receiverFirstName || '';
                            var lName2 = meeting.toList[j].receiverLastName || '';
                            obj.name = fName2+' '+lName2;
                        }
                    }
                }
                if(!isDetailsExists){

                    obj.picUrl = '/images/default.png';
                    obj.name = meeting.toList[0].receiverEmailId;
                }
            }
        }
        else{
            if(meeting.to){
                if(meeting.to.receiverId){
                    obj.picUrl = '/getImage/'+meeting.to.receiverId;
                    obj.name = meeting.to.receiverName;
                }
                else{
                    obj.picUrl = '/images/relatasLogo_Google.png';
                    obj.name = meeting.to.receiverEmailId;
                }
            }
        }

        return obj;
    };

    $scope.getDetailsIfReceiver = function(meeting){
        var obj = {};
        if(meeting.senderId){
            obj.picUrl = '/getImage/'+meeting.senderId;
            obj.name = meeting.senderName;
        }
        else{
            obj.picUrl = '/images/default.png';
            obj.name = meeting.senderEmailId;
        }
        return obj;
    };

    $scope.updateTaskStatus = function(taskId,status){
        var reqObj = {
            taskId:taskId,
            status:!status ? 'notStarted':'complete'
        };
        $http.post('/task/update/status/web',reqObj)
            .success(function(response){
                if(response.SuccessCode){
                    toastr.success(response.Message);
                }
                else toastr.error(response.Message);
            })
    };

});

reletasApp.controller("weather_and_today_coun", function ($scope, $http, meetingObjService,sharedServiceSelection) {
    $scope.weatherData = [];

    $scope.$on('handleBroadcast', function() {
      if(sharedServiceSelection.message = "refresh") {
        $scope.getTopSectionInfo(new Date());
      }
    });

    $scope.getDateCheck = function(list,dateCheck){

        var url = '/today/traveling/location?skip=0&limit=12&dateTime='+dateCheck;

        if(!checkRequired(dateCheck)){
            dateCheck = checkRequired(timezone) ? moment().tz(timezone).format('DD MMM YYYY') : moment().format('DD MMM YYYY');
        }

        for(var i=0; i<list.length; i++){
            var dateDT = checkRequired(timezone) ? moment.unix(list[i].dt).tz(timezone).format('DD MMM YYYY') : moment.unix(list[i].dt).format('DD MMM YYYY')

            if(dateDT == dateCheck){
                $scope.date_today_top = dateDT;
                $scope.today_temp_min = list[i].temp.min+'C';
                $scope.today_temp_max = list[i].temp.max+'C';
                break;
            }
        }
    };

    var cityUrl = '/today/traveling/location?skip=0&limit=12&dateTime=' + $scope.dateTime;
    $http.post(cityUrl, { dateTime: $scope.dateTime })
        .success(function(response) {
            var currentLocation = response.Data.location;
            $scope.city_country_top = currentLocation;
            var weatherUrl = '/today/weather/forecast/web/by/location?location=' + $scope.city_country_top;

            $http.get(weatherUrl)
                .success(function(response) {
                    if (response.SuccessCode) {

                        if (response.Data.list != undefined && response.Data.list.length > 0) {
                            $scope.weatherData = response.Data.list;
                            var list = response.Data.list;
                            for (var i = 0; i < list.length; i++) {
                                var dateDT = checkRequired(timezone) ? moment.unix(list[i].dt).tz(timezone).format('DD MMM YYYY') : moment.unix(list[i].dt).format('DD MMM YYYY')
                                var dateCheck = checkRequired(timezone) ? moment().tz(timezone).format('DD MMM YYYY') : moment().format('DD MMM YYYY');
                                if (dateDT == dateCheck) {
                                    $scope.date_today_top = dateDT;
                                    $scope.today_temp_min = list[i].temp.min + 'C';
                                    $scope.today_temp_max = list[i].temp.max + 'C';
                                    break;
                                }
                            }
                        }
                    }
                }).error(function(data) {

                });

        }).error(function(data) {

        });

    // $http.get('/today/weather/forecast/web')
    //     .success(function (response) {
    //         if(response.SuccessCode){
    //             if(checkRequired(response.Data.city)){

    //                 var url = '/today/traveling/location?skip=0&limit=12&dateTime='+$scope.dateTime;
    //                 $http.post(url,{dateTime:$scope.dateTime})
    //                     .success(function (response) {
    //                         var currentLocation = response.Data.location;
    //                         $scope.city_country_top = currentLocation;
    //                     });

    //             }
    //             if(response.Data.list != undefined && response.Data.list.length > 0){
    //                 $scope.weatherData = response.Data.list;
    //                 var list = response.Data.list;
    //                 for(var i=0; i<list.length; i++){
    //                     var dateDT = checkRequired(timezone) ? moment.unix(list[i].dt).tz(timezone).format('DD MMM YYYY') : moment.unix(list[i].dt).format('DD MMM YYYY')
    //                     var dateCheck = checkRequired(timezone) ? moment().tz(timezone).format('DD MMM YYYY') : moment().format('DD MMM YYYY');
    //                     if(dateDT == dateCheck){
    //                         $scope.date_today_top = dateDT;
    //                         $scope.today_temp_min = list[i].temp.min+'C';
    //                         $scope.today_temp_max = list[i].temp.max+'C';
    //                         break;
    //                     }
    //                 }
    //             }
    //         }
    //     }).error(function (data) {

    //     });

    meetingObjService.weather_and_today_next = function(momentDate){
        $scope.getDateCheck($scope.weatherData,momentDate.format('DD MMM YYYY'));
        $scope.getTopSectionInfo(momentDate.format())
    };

    $scope.getTopSectionInfo = function(date){

        $http.post('/today/top/section/info',{dateTime:date})
            .success(function (response) {
                if(response.SuccessCode){
                    $scope.today_meeting_count_top = response.Data.todayMeetings || 0;
                    $scope.today_tasks_count_top = response.Data.todayTasks || 0;
                    //$scope.image_top = '/images/app_images/drawable-hdpi/'+response.Data.image;
                    $scope.image_top = '/images/app_images/drawable-hdpi/'+response.Data.imageNew;

                    //if(response.Data.location){
                    //    $scope.city_country_top = response.Data.location;
                    //} else {
                        var url = '/today/traveling/location?skip=0&limit=12&dateTime='+date;

                        $http.post(url,{dateTime:date})
                            .success(function (response) {
                                var currentLocation = response.Data.location;
                                $scope.city_country_top = currentLocation;
                            });
                    //}

                }
                else{

                }
            }).error(function (data) {

            })
    };
    $scope.getTopSectionInfo(null);
});

reletasApp.controller("today_landing_top_counts", function ($scope, $http, meetingObjService,sharedServiceSelection) {
    $scope.$on('handleBroadcast', function() {
      if(sharedServiceSelection.message = "refresh"){
        $scope.today_landing_top_counts_next(null);;
      }
    });

    $scope.socialUpdatesSetupClass = 'hide';
    $scope.today_landing_top_counts_next = function(date){

        $http.post('/today/status/count',{dateTime:date})
            .success(function (response) {
                if(response.SuccessCode){
                    $scope.losingTouchCount = $scope.getValidStringNumber(response.Data.losingTouchCount || 0);
                    $scope.unpreparedMeetingsCount = $scope.getValidStringNumber(response.Data.unpreparedMeetingsCount || 0);
                    //$scope.emailResponsesPending = $scope.getValidStringNumber(response.Data.emailResponsesPending || 0);
                    $scope.emailAwaitingResponses = $scope.getValidStringNumber(response.Data.emailAwaitingResponses || 0);
                }
                else{
                    $scope.losingTouchCount = $scope.getValidStringNumber(0);
                    $scope.unpreparedMeetingsCount = $scope.getValidStringNumber(0);
                    //$scope.emailResponsesPending = $scope.getValidStringNumber(0);
                    $scope.emailAwaitingResponses = $scope.getValidStringNumber(0);
                }
            }).error(function (data) {

            });

        $http.get('/insights/your/responses/pending', { params: { days:0, infoFor:"graph" } }).then(function(resp) {
            if (resp.data.SuccessCode) {
                var responded = resp.data.Data.responded ? resp.data.Data.responded : 0;
                var internal = resp.data.Data.internal.length ? resp.data.Data.internal.length : 0;
                var external = resp.data.Data.external.length ? resp.data.Data.external.length : 0;
                var totalMails = internal + external;
                $scope.emailResponsesPending = $scope.getValidStringNumber(totalMails || 0);
            }
            else{
                $scope.emailResponsesPending = $scope.getValidStringNumber(0);
            }
        });

        $http.get('/people/to/connect/info', {params : {infoFor:'dashboard'}})
            .then(function(response) {
                if (response.data.SuccessCode)
                    $scope.peopleToConnect = $scope.getValidStringNumber(response.data.Data.peopleToConnect|| 0);
                else
                  $scope.peopleToConnect =  $scope.getValidStringNumber(0);
            });

        $http.get('/company/members')
            .success(function(response) {
                if (response.SuccessCode) {
                    var obj = response.Data.companyMembers;
                    var companyArr = Object.keys(obj).map(function(k) {
                        return obj[k]
                    });

                    $http.get('/insights/losing/touch/', { params: { companyMembers: companyArr.toString(), hierarchyList: response.Data.userId } })
                        .success(function(response) {
                            if (response.SuccessCode) {
                                $scope.losingTouchNew = $scope.getValidStringNumber(response.Data.self.selfLosingTouch|| 0)
                                
                            } else
                                $scope.losingTouchNew = $scope.getValidStringNumber(0);
                        });

                } else {

                }
            }).error(function(data) {

            })
    };
    meetingObjService.today_landing_top_counts_next = function(date){
        $scope.today_landing_top_counts_next(date)
    };
    $scope.today_landing_top_counts_next(null);
    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };

    $http.get('/today/social/updates/count')
        .success(function (response) {
            if(response.SuccessCode){
                $scope.socialUpdatesSetupClass = 'hide';
                $scope.socialUpdatesOpacity = '';
                if(response.Data.showSetup){
                    $scope.socialUpdatesOpacity = 'opacity: .3';
                    $scope.socialUpdatesSetupClass = '';
                }
                else{
                    $scope.socialUpdates = $scope.getValidStringNumber(response.Data.count || 0);
                }
            }
            else{
                $scope.socialUpdates = '00';
            }
        }).error(function (data) {

        })
});
var again = false;
reletasApp.controller("today_landing_agenda", function ($scope, $http, meetingObjService,sharedServiceSelection) {

    $scope.agendaItems =[];
    $scope.arrowDirection = 'right';
    var dateAgenda = checkRequired(timezone) ? moment().tz(timezone).format() : moment().format();
    $scope.noAgendaFoundFlag = false;

    $scope.$on('handleBroadcast', function() {
      if(sharedServiceSelection.message = "refresh"){
        $scope.fetchAgenda(dateAgenda);
      }
    });

    $scope.fetchAgenda = function(dateAgenda){
        $scope.agendaItems =[];
        $http.post('/today/agenda/section/web',{dateTime:dateAgenda})
            .success(function (response) {
                
                if(response.SuccessCode){
                    if(checkRequired(response.Data.meetings != undefined && response.Data.meetings.length > 0)){

                        for(var i=0; i<response.Data.meetings.length; i++){
                            $scope.agendaItems.push($scope.formatMeetingDetails(response.Data.meetings[i],response.Data.userId));
                        }

                        for(var j=0; j<response.Data.events.length; j++){
                            var dateStart = checkRequired(timezone) ? moment(response.Data.events[j].startDateTime).tz(timezone) : moment(response.Data.events[j].startDateTime);
                            var dateEnd = checkRequired(timezone) ? moment(response.Data.events[j].endDateTime).tz(timezone) : moment(response.Data.events[j].endDateTime);
                            var obj = {
                                sortDate:new Date(dateStart.format()),
                                time:dateStart.format("hh:mm A")+' - '+dateEnd.format("hh:mm A"),
                                title:response.Data.events[j].eventName || '',
                                location:response.Data.events[j].locationName || '',
                                eventId:response.Data.events[j]._id,
                                isAccepted:response.Data.events[j].accepted || false,
                                picUrl:'/getImage/'+response.Data.events[j].createdBy.userId,
                                url:'/event/'+response.Data.events[j]._id,
                                name:response.Data.events[j].createdBy.userName,
                                locationTypePic:"/images/icon_location_small.png",
                                preparedMeetingClass:'hide',
                                disableClass:''
                            };

                            $scope.agendaItems.push(obj);

                        }

                        for(var l=0; l<response.Data.tasks.length; l++){
                            var tDate = checkRequired(timezone) ? moment(response.Data.tasks[l].dueDate).tz(timezone): moment(response.Data.tasks[l].dueDate);
                            var objT = {
                                sortDate:new Date(tDate.format()),
                                title:response.Data.tasks[l].taskName || 'No Name',
                                time:tDate.format("hh:mm A"),
                                location:'Task',
                                url:'/task/'+response.Data.tasks[l]._id,
                                locationTypePic:"/images/icon_location_small.png",
                                preparedMeetingClass:'hide',
                                taskId:response.Data.tasks[l]._id,
                                disableClass:'disabled'
                            };

                            if(response.Data.tasks[l].assignedTo && response.Data.tasks[l].assignedTo._id == response.Data.userId){
                                objT.picUrl = '/getImage/'+response.Data.tasks[l].createdBy._id || '/images/default.png';
                                objT.name = response.Data.tasks[l].createdBy.firstName+' '+response.Data.tasks[l].createdBy.lastName;
                            }
                            else{
                                objT.picUrl = response.Data.tasks[l].assignedTo ?  '/getImage/'+response.Data.tasks[l].assignedTo._id : '/images/default.png';
                                objT.name = response.Data.tasks[l].assignedTo ? response.Data.tasks[l].assignedTo.firstName+' '+response.Data.tasks[l].assignedTo.lastName : "";
                            }
                            $scope.agendaItems.push(objT);

                        }
                    }
                    if($scope.agendaItems.length > 0){
                        $scope.agendaItems.sort(function (o1, o2) {
                            return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                        });
                    }
                }
                else{
                    $scope.noAgendaFoundFlag = true;
                    $scope.noAgendaFound = response.Message;
                }
                var dateMoment = checkRequired(timezone) ? moment(dateAgenda).tz(timezone) : moment(dateAgenda);
                var now = checkRequired(timezone) ? moment().tz(timezone) : moment();
                if(dateMoment.format("DD MMM YYYY") == now.format("DD MMM YYYY")){
                    $scope.agendaTitle = "Today's Agenda ("+dateMoment.format("DD MMM YYYY")+")";
                }
                else $scope.agendaTitle = "Agenda For ("+dateMoment.format("DD MMM YYYY")+")";
            }).error(function (data) {

            });

    };

    $scope.fetchAgenda(dateAgenda);
    
    $scope.agendaNext = function(){

        again = false;
        $scope.noAgendaFoundFlag = false;
        if($scope.arrowDirection == 'right'){
            $scope.arrowDirection = 'left';
            var dateAgenda = checkRequired(timezone) ? moment().tz(timezone) : moment();
            dateAgenda.date(dateAgenda.date() + 1);
            $scope.fetchAgenda(dateAgenda.format());
            meetingObjService.travelingLocationByDate(dateAgenda.format())
            //meetingObjService.today_landing_top_counts_next(dateAgenda.format());
            meetingObjService.weather_and_today_next(dateAgenda)
        }
        else{
            $scope.arrowDirection = 'right';
            var dateTime = checkRequired(timezone) ? moment().tz(timezone) : moment();
            $scope.fetchAgenda();
            meetingObjService.weather_and_today_next(dateTime);
            //meetingObjService.today_landing_top_counts_next(dateTime.format());
            meetingObjService.travelingLocationByDate(dateTime.format())
        }
    };

    $scope.formatMeetingDetails = function(meeting,userId){
        var date = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].start.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].start.date);
        var end = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].end.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].end.date);
        var obj = {
            sortDate:new Date(date.format()),
            time:date.format("hh:mm A")+' - '+end.format("hh:mm A"),
            title:meeting.scheduleTimeSlots[0].title || '',
            location:meeting.scheduleTimeSlots[0].location || '',
            locationType:meeting.scheduleTimeSlots[0].locationType || '',
            date:date.format("DD MMM YYYY"),
            dateUpcoming:date.format("MMM DD YYYY"),
            invitationId:meeting.invitationId,
            url:'/today/details/'+ meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId,
            isPreparedMeeting:$scope.isPreparedMeeting(meeting,userId),
            isSender:false,
            isAccepted:meeting.scheduleTimeSlots[0].isAccepted || false,
            locationTypePic:$scope.getLocationTypePic(meeting.scheduleTimeSlots[0].locationType)
        };
        
        obj.preparedMeetingUrl = obj.isPreparedMeeting ? null : '/today/details/'+ (meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId);

        if(meeting.suggested){
            obj.location = checkRequired(meeting.scheduleTimeSlots[0].suggestedLocation) ? meeting.scheduleTimeSlots[0].suggestedLocation : meeting.scheduleTimeSlots[0].location;
        }

        if(meeting.suggested){
            if(meeting.suggestedBy.userId == userId){
                obj.isSender = true;
                if(meeting.senderId == userId){
                    var data1 = $scope.getDetailsIfSender(meeting);
                    obj.picUrl = data1.picUrl;
                    obj.name = data1.name;
                }
                else{
                    var data3 = $scope.getDetailsIfReceiver(meeting);
                    obj.picUrl = data3.picUrl;
                    obj.name = data3.name;
                }
            }
            else{
                if(meeting.senderId == meeting.suggestedBy.userId){
                    obj.isSender = true;
                    var data5 = $scope.getDetailsIfReceiver(meeting)
                    obj.picUrl = data5.picUrl;
                    obj.name = data5.name;
                }
                else{
                    if(meeting.selfCalendar){
                        for(var j=0; j<meeting.toList.length; j++){
                            if(meeting.suggestedBy.userId == meeting.toList[j].receiverId){
                                obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                                var fName = meeting.toList[j].receiverFirstName || '';
                                var lName = meeting.toList[j].receiverLastName || '';
                                obj.name = fName+' '+lName;
                            }
                        }
                    }
                    else{
                        obj.picUrl = '/getImage/'+meeting.to.receiverId;
                        obj.name = meeting.to.receiverName;
                    }
                }
            }
        }
        else{
            if(meeting.senderId == userId){
                obj.isSender = true;
                var data2 = $scope.getDetailsIfSender(meeting);
                obj.picUrl = data2.picUrl;
                obj.name = data2.name;
            }
            else{
                var data4 = $scope.getDetailsIfReceiver(meeting);
                obj.picUrl = data4.picUrl;
                obj.name = data4.name;
            }
        }
        if(!checkRequired(obj.picUrl) && !checkRequired(obj.name)){
            obj.picUrl = '/getImage/'+meeting.senderId;
            obj.name = meeting.senderName;
        }
        if(!checkRequired(obj.picUrl)){
            obj.picUrl = '/images/default.png';
        }
        obj.prepareButtonText = $scope.isMeetingAccepted(meeting,userId) ? "Confirmed" : obj.isSender ? "Not Confirmed" : "Confirm";

        if (obj.prepareButtonText == "Confirmed" || obj.prepareButtonText == "Not Confirmed" && obj.isSender === true) {
            obj.buttonClass = "btn btn-transparent";
        }
        else {
            obj.buttonClass = "btn btn-green";
        }
        return obj;
    };

    $scope.getDetailsIfSender = function(meeting){

        var obj = {};
        if(meeting.selfCalendar == true){
            if(meeting.toList.length > 0){
                var isDetailsExists = false;
                for(var i=0; i<meeting.toList.length; i++){
                    if(meeting.toList[i].isPrimary){
                        isDetailsExists = true;
                        obj.picUrl = '/getImage/'+meeting.toList[i].receiverId;
                        var fName = meeting.toList[i].receiverFirstName || '';
                        var lName = meeting.toList[i].receiverLastName || '';
                        obj.name = fName+' '+lName;
                    }
                }
                if(!isDetailsExists){
                    for(var j=0; j<meeting.toList.length; j++){
                        if(checkRequired(meeting.toList[j].receiverId)){
                            isDetailsExists = true;
                            obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                            var fName2 = meeting.toList[j].receiverFirstName || '';
                            var lName2 = meeting.toList[j].receiverLastName || '';
                            obj.name = fName2+' '+lName2;
                        }
                    }
                }
                if(!isDetailsExists){
                    obj.picUrl = '/images/default.png';
                    obj.name = meeting.toList[0].receiverEmailId;
                }
            }
        }
        else{
            if(meeting.to){
                if(meeting.to.receiverId){
                    obj.picUrl = '/getImage/'+meeting.to.receiverId;
                    obj.name = meeting.to.receiverName;
                }
                else{
                    obj.picUrl = '/images/relatasLogo_Google.png';
                    obj.name = meeting.to.receiverEmailId;
                }
            }
        }
        return obj;
    };

    $scope.getDetailsIfReceiver = function(meeting){
        var obj = {};
        if(meeting.senderId){
            obj.picUrl = '/getImage/'+meeting.senderId;
            obj.name = meeting.senderName;
        }
        else{
            obj.picUrl = '/images/default.png';
            obj.name = meeting.senderEmailId;
        }
        return obj;
    };

    $scope.isPreparedMeeting = function(meeting,userId){

        if(meeting.senderId == userId){
            return meeting.isSenderPrepared;
        }
        else if(meeting.selfCalendar){
            var isExists = false;
            var prepared = true;
            for(var j=0; j<meeting.toList.length; j++){
                if(meeting.toList[j].receiverId == userId){
                    isExists = true;
                    prepared = meeting.toList[j].isPrepared || false;
                }
            }
            return prepared;
        }
        else{
            if(meeting.to.receiverId == userId){
                return meeting.to.isPrepared || false;
            }else return true;
        }
    };

    $scope.getLocationTypePic = function(meetingLocationType){
        switch (meetingLocationType) {
            case 'In-Person':
                return "/images/icon_location_small.png";
                break;
            case 'Phone':
                return "/images/icon_phone.png";
                break;
            case 'Skype':
                return "/images/mailer/icon_skype.png";
                break;
            case 'Conference Call':
                return "/images/conference_call.png";
                break;
            default:return "/images/icon_location_small.png"
                break;
        }
    };

    $scope.goToUrl = function(url){
        if(url && url.length > 0 && url[0] && url[0][0] && url[0][0] != 'null' && url[0][0] != null ) {
            window.location = url[0][0];
        }
    };

    $scope.prepareMeeting = function(url){
        if(checkRequired(url)){
            window.location = url;
        }
    };

    $scope.isMeetingAccepted = function(meeting,userId){

        var isAccepted = false;
        if(meeting.scheduleTimeSlots && meeting.scheduleTimeSlots.length > 0){
            for(var i=0; i<meeting.scheduleTimeSlots.length; i++){
                if(meeting.scheduleTimeSlots[i].isAccepted){
                    isAccepted = true;
                }
            }
        }

        if(isAccepted){
            if(meeting.selfCalendar && meeting.toList && meeting.toList.length > 1){
                if(meeting.senderId != userId){
                    var exists = false;
                    for(var j=0; j<meeting.toList.length > 0; j++){
                        if(meeting.toList[j].receiverId == userId){
                            exists = true;
                        }
                    }
                    if(!exists){
                        isAccepted = false;
                    }
                }
            }
        }

        return isAccepted;
    };
});

var start = 1;

var itemsScroll = 6;
var itemsToShow = 6;

if(window.innerWidth < 480){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 1
    itemsScroll = 1
}else if(window.innerWidth < 900){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 3
    itemsScroll = 3
}
var end = itemsToShow;

//var html = '';
reletasApp.controller("traveling_location", function ($scope, $http, meetingObjService) {
    $scope.cConnections = [];
    $scope.cConnectionsIds = [];
    $scope.currentLength = 0;
    $scope.isFirstTime = true;
    $scope.isConnectionOpen = false;
    $scope.start = start;
    $scope.end = end;
    $scope.isAllConnectionsLoaded = false;
    $scope.dateTime = moment().format();
    $scope.location_traveling_disable = true;

    $scope.isConnectionOpen = true;
    var url = '/today/traveling/location?skip=0&limit=12&dateTime='+$scope.dateTime;
    getConnections($scope, $http, url, true);

    meetingObjService.travelingLocationByDate = function(date){
        $scope.dateTime = date;
        $scope.cConnections = [];
        $scope.cConnectionsIds = [];
        $scope.currentLength = 0;
        $scope.isFirstTime = true;
        $scope.isConnectionOpen = false;
        $scope.start = start;
        $scope.end = end;
        $scope.isAllConnectionsLoaded = false;
        $scope.dateTime = date;

        $scope.isConnectionOpen = true;
        var url = '/today/traveling/location?skip=0&limit=12&dateTime='+$scope.dateTime;
        getConnections($scope, $http, url, true,null,null);
    };

    $scope.NextConn = function(){
        var statusNext =  $scope.statusNext;
        var statusPrev =  $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.end < total){
            var itemsToStayPrev = $scope.start+itemsScroll;
            var itemsToStayNext = $scope.end+itemsScroll;
            for(var i=0; i<$scope.cConnectionsIds.length; i++){
                /// if(i+1 > statusNext && i+1 <= (statusNext+itemsScroll)){
                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    $scope.cConnections[i].className = '';
                    //addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    $scope.cConnections[i].className = 'hide';
                    //addHide($scope.cConnectionsIds[i],true);
                }

            }

            $scope.end = $scope.end+itemsScroll;
            $scope.start = $scope.start+itemsScroll;

            $scope.statusNext = statusNext+itemsScroll;
            $scope.statusPrev = statusNext;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
        }
        $scope.showHideArrows();
        if($scope.fetchCommonConnectionsNext() && $scope.isAllConnectionsLoaded == false && $scope.isConnectionOpen == false){
            $scope.isConnectionOpen = true;
            if(checkRequired($scope.dataEntered)){
                var url1 = '/today/traveling/location?skip='+$scope.currentLength+'&limit=12&dateTime='+$scope.dateTime+'&location='+$scope.dataEntered.location+'&designation='+$scope.dataEntered.designation;
                getConnections($scope, $http, url1, false,$scope.dataEntered.location,$scope.dataEntered.designation);
            }
            else{
                var url2 = '/today/traveling/location?skip='+$scope.currentLength+'&limit=12&dateTime='+$scope.dateTime;
                getConnections($scope, $http, url2, false,null,null);
            }
        }
    };

    $scope.searchTraveling = function(){
        again = true;

        $scope.dataEntered = getLocationAndTitle();
        if(checkRequired($scope.dataEntered.location)){
            var url = '/today/traveling/location?skip=0&limit=12&dateTime='+$scope.dateTime+'location='+$scope.dataEntered.location+'&designation='+$scope.dataEntered.designation;
            getConnections($scope, $http, url, true,$scope.dataEntered.location,$scope.dataEntered.designation);

            mixpanel.track('Location Search', {
                'Location' : $scope.dataEntered.location,
                'Designation':  $scope.dataEntered.designation
            });
        }
    };

    $scope.fetchCommonConnectionsNext = function(){
        if($scope.end >= $scope.cConnectionsIds.length - 12){
            return true;
        }else return false;
    };

    $scope.PrevConn = function(){

        var statusPrev = $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.start >1){
            var itemsToStayPrev = $scope.start-itemsScroll;
            var itemsToStayNext = $scope.end-itemsScroll;

            for(var i=0; i<$scope.cConnectionsIds.length; i++){
                //if(i+1 <= statusPrev && i+1 > (statusPrev-itemsScroll)){
                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    $scope.cConnections[i].className = '';
                    //addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    $scope.cConnections[i].className = 'hide';
                    //addHide($scope.cConnectionsIds[i],true);
                }

            }

            $scope.end = $scope.end-itemsScroll;
            $scope.start = $scope.start-itemsScroll;

            $scope.statusNext = statusPrev;
            $scope.statusPrev = statusPrev-itemsScroll+1;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
            $scope.showHideArrows()
        }
    };

    $scope.showHideArrows = function(){
        if($scope.grandTotal >0){
            if($scope.start <=1 ){
                addHide("als-prev",true)
            }
            else addHide("als-prev",false)

            if($scope.end >= $scope.grandTotal){
                addHide("als-next",true)
            }
            else addHide("als-next",false)
        }
        else{
            addHide("als-prev",true)
            addHide("als-next",true)
        }
    };

    $scope.connClick = function(url){
        if(url && url.length > 0 && url[0] && url[0][0] && url[0][0] != 'null' && url[0][0] != null ) {
            // trackMixpanel("clicked on common connection",url[0][0],function(){
            window.location.replace(url[0][0]);
            // })
        }
    }

    $scope.interactionsContext = function(emailId){
        window.location.replace('/contact/selected?context='+emailId)
    }
});

function getLocationAndTitle(){
    return {location:$("#locationEntered").val(),designation:$("#designationEntered").val()}
}

function addHide(elementId,addHide){
    addHide ? $("#"+elementId).addClass('hide') : $("#"+elementId).removeClass('hide')
    //addHide ? $("#"+elementId).css({display:'none'}) : $("#"+elementId).css({display:'block'})
}

function getConnections($scope, $http, url, isHides,locationEntered,designation){
    $scope.location_traveling_disable = true;

    //   $scope.location_traveling = '';
    if(isHides){
        $scope.cConnections = [];
        $scope.cConnectionsIds = [];
    }

    $http.post(url,{dateTime:$scope.dateTime,location:locationEntered,designation:designation})
        .success(function (response) {
            if(response.SuccessCode == 0){
                $scope.location_traveling = response.Data.location || '';
                $scope.noConnectionsFoundFlag = true;
                $scope.noConnectionsFoundForSearch = response.Message;
                if(response.Data  && response.Data.Message && !checkRequired(response.Data.location)){
                    $("#no_meeting_location").text(response.Data.Message)
                    if(response.Data.notValid){
                        $scope.location_traveling_disable = false;
                    }
                }
                else{
                    if(!response.Data.ignore){

                        if(response.Data.meeting){
                            $("#no_meeting_location").html("Recommended people to meet while you are in <b>"+$scope.location_traveling+".&nbsp</b>")
                        }
                        else $("#no_meeting_location").html("Recommended people to meet while you are in <b>"+$scope.location_traveling+".&nbsp</b>")
                    }
                    if(!again){
                        //  $scope.searchTraveling();
                    }
                }
            }
            else{
                if(!response.Data.ignore){

                    if(response.Data.meeting){
                        $("#no_meeting_location").html("Recommended people to meet while you are in <b>"+response.Data.location+".&nbsp</b>")
                    }
                    else  $("#no_meeting_location").html("Recommended people to meet while you are in <b>"+response.Data.location+".&nbsp</b>")
                }

                $scope.noConnectionsFoundFlag = false;
                if(response.Data.notValid){
                    $scope.location_traveling_disable = false;
                }
            }

            $scope.isConnectionOpen = false;
            if(response.SuccessCode){
                $scope.grandTotal = response.Data.total;
                $scope.location_traveling = response.Data.location || '';
                response.Data = response.Data.contacts;

                if(response.Data && response.Data.length > 0){
                    var html = '';
                    hidePrevNext(true);
                    if(isHides){
                        $scope.currentLength = response.Data.length;
                        $scope.cConnections = [];
                        $scope.cConnectionsIds = [];
                    }
                    else $scope.currentLength += response.Data.length;

                    for(var i=0; i<response.Data.length; i++){
                        var li = '';
                        var obj = {name:''};
                        if(checkRequired(response.Data[i].firstName) && response.Data[i].firstName.length > 0){
                            response.Data[i].firstName.forEach(function(name){
                                if(checkRequired(name)){
                                    obj.name = name
                                }
                            })
                        }
                        if(checkRequired(response.Data[i].lastName) && response.Data[i].lastName.length > 0){
                            response.Data[i].lastName.forEach(function(name){
                                if(checkRequired(name)){
                                    obj.name += ' '+name
                                }
                            })
                        }
                        obj.emailId = response.Data[i]._id.emailId;
                        if(!checkRequired(obj.name) && response.Data[i]._id.emailId){
                            obj.name = getTextLength(response.Data[i]._id.emailId,13);
                        }
                        else if(!checkRequired(obj.name) && response.Data[i]._id.mobileNumber){
                            obj.name = getTextLength(response.Data[i]._id.mobileNumber,13)
                        }

                        if(checkRequired(obj.name)){
                            obj.fullName = obj.name
                            obj.name = getTextLength(obj.name,13);
                        }
                        if(checkRequired(response.Data[i].userId) && response.Data[i].userId.length > 0){
                            response.Data[i].userId.forEach(function(id){
                                if(checkRequired(id)){
                                    obj.image = '/getImage/'+id;
                                }
                            })
                        }
                        if(!checkRequired(obj.image)){
                            obj.cursor = 'cursor:default';
                            //obj.image = '/images/default.png';
                            obj.noPicFlag = true;
                        }else obj.cursor = 'cursor:pointer';

                        if(checkRequired(response.Data[i].publicProfileUrl) && response.Data[i].publicProfileUrl.length > 0){
                            response.Data[i].publicProfileUrl.forEach(function(id){
                                if(checkRequired(id)){
                                    if(id.charAt(0) != 'h'){
                                        obj.url = '/'+id;
                                    }
                                }
                            })
                        }
                        if(!checkRequired(obj.url)){
                            obj.url = null;
                        }
                        obj.interactionsCount = response.Data[i].count || 0;
                        obj.idName = 'com_con_item_'+response.Data[i].lastInteracted
                        obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                        obj.className = 'hide';
                        if(isHides && i<itemsToShow){
                            obj.className = ''
                        }

                        $scope.cConnections.push(obj);
                        $scope.cConnectionsIds.push('com_con_item_'+response.Data[i].lastInteracted);

                    }

                    if(isHides){
                        $scope.statusPrev = 1;
                        $scope.statusNext = itemsScroll;
                    }
                    $scope.showHideArrows()
                }
                else if(isHides){
                    hidePrevNext(true)
                    // commonConnectionsMessage(false)
                    $scope.commonConnectionsNotExist = response.Message;
                }else $scope.isAllConnectionsLoaded = true;
            }
            else if(isHides){
                hidePrevNext(true)
                //commonConnectionsMessage(false)
                $scope.commonConnectionsNotExist = response.Message;
            }else $scope.isAllConnectionsLoaded = true;
        })
}

function hidePrevNext(hide){
    if(hide){
        $(".als-prev").addClass('hide')
        $(".als-next").addClass('hide')
    }
    else{
        $(".als-prev").removeClass('hide')
        $(".als-next").removeClass('hide')
    }
}

function getTextLength(text,maxLength){

    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

function changeTextById(id,text){
    $("#"+id).html(text);
}

toastr.options.onHidden = function() {
  sessionStorage.setItem("is_reloaded", true);
  sessionStorage.setItem("persistent", "persistent");
};
