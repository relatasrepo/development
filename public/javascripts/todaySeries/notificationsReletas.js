
reletasApp.controller("notificationsController", function($scope, $http,$document,teamMembers,$window) {

    $scope.totalNotifications = 0;
    $scope.$window = $window;

    $scope.showNotifications = function () {
        checkNotificationsStatus($http,$scope,true)
        $scope.notificationsSeen = true;

        $scope.openNotifications = !$scope.openNotifications;

        if ($scope.openNotifications) {
            $scope.$window.onclick = function (event) {
                closeSearchWhenClickingElsewhere(event, $scope.showNotifications);
            };
        } else {
            $scope.openNotifications = false;
            $scope.$window.onclick = null;
            $scope.$apply(); //--> trigger digest cycle and make angular aware.
        }

    };

    checkNotificationsStatus($http,$scope)
    kickOffNotifications($scope,$http,teamMembers)
    reminderToConnectNotifications($scope,$http)
    losingTouchNotifications($scope,$http,teamMembers)

});

function closeSearchWhenClickingElsewhere(event, callbackOnClose) {

    var clickedElement = event.target;
    if (!clickedElement) return;

    var elementClasses = clickedElement.classList;
    var clickedOnSearchDrawer = elementClasses.contains('notifications-header-bell')
        || elementClasses.contains('notifications-dropdown')
        || (clickedElement.parentElement !== null
        && clickedElement.parentElement.classList.contains('notifications-dropdown'));
    if (!clickedOnSearchDrawer) {
        callbackOnClose();
    }

}

function checkNotificationsStatus($http,$scope,notificationsRead) {

    var url = '/v1/header/notifications/read/status';
    if(notificationsRead){
        url = '/v1/header/notifications/read/status?setNotificationsRead=true';
    }

    $http.get(url)
        .success(function (response) {

            if(!response.notifications && !response.notifications){
                $scope.notificationsSeen = false
            } else {
                $scope.notificationsSeen = true
            }
        });
}

function kickOffNotifications($scope,$http,teamMembers) {

    $http.get('/v1/header/notifications')
        .success(function (response) {

            if(response.SuccessCode){

                if(response.notifications.revenueAtRisk){
                    $scope.revenueAtRisk = 0;

                    if(isNaN(Math.round((response.notifications.revenueAtRisk.valueAtRisk / response.notifications.revenueAtRisk.allContactsValue)*100))){
                        $scope.revenueAtRisk = 0;
                    } else {
                        $scope.revenueAtRisk = Math.round((response.notifications.revenueAtRisk.valueAtRisk / response.notifications.revenueAtRisk.allContactsValue)*100);
                        $scope.totalNotifications = $scope.totalNotifications+1;
                    }
                }

            } else {
                revenueAtRiskNotificationsSet($scope,$http,teamMembers);
                losingTouchNotifications($scope,$http,teamMembers)
            }
        });
}

function losingTouchNotifications($scope,$http,teamMembers) {
    teamMembers.getTeamMembers()
        .then(function(companyData) {
            var obj = companyData.Data.companyMembers;
            var companyArr = Object.keys(obj).map(function (k) {
                return obj[k]
            });

            var url = fetchUrlWithParameter('/insights/losing/touch/', "companyMembers", companyArr);
            var hierarchyList = [];
            hierarchyList.push(companyData.Data.userId);
            url = fetchUrlWithParameter(url, 'hierarchyList', hierarchyList);

            $scope.losingTouch = 0;

            $http.get(url)
                .success(function (res) {
                    if (res.SuccessCode) {
                        $scope.losingTouch = res.Data.self.selfLosingTouch
                        $scope.totalNotifications = $scope.totalNotifications + 1
                    }
                });
        })
}

function reminderToConnectNotifications($scope,$http) {

    var url = '/fetch/remind/to/connect';
    $scope.rtcCount = 0;

    $http.get(url)
        .success(function(response) {
            if(response.Data && response.Data.length>0){
                $scope.rtcCount = response.Data.length;
            }
        });

}

function revenueAtRiskNotifications($scope,$http,teamMembers) {

}

function revenueAtRiskNotificationsSet($scope,$http,teamMembers) {
    teamMembers.getTeamMembers()
        .then(function (companyData) {

            var obj = companyData.Data.companyMembers;
            var companyArr = Object.keys(obj).map(function (k) {
                return obj[k]
            });

            var url = '/insights/revenue/risk/graph';
            var hierarchyList = [];
            hierarchyList.push(companyData.Data.userId);
            url = fetchUrlWithParameter(url, "companyMembers", companyArr);

            url = fetchUrlWithParameter(url, 'hierarchyList', hierarchyList);
            url = fetchUrlWithParameter(url, 'getFor', 'self');

            $http.get(url)
                .success(function(res) {
                    if (res.SuccessCode) {
                        if(isNaN(Math.round((res.valueAtRisk / res.allContactsValue)*100))){
                            $scope.revenueAtRisk = 0;
                        } else {
                            $scope.revenueAtRisk = Math.round((res.valueAtRisk / res.allContactsValue)*100)
                            $scope.totalNotifications = $scope.totalNotifications+1
                        }
                    }
                });
        })
}

function meetingsToday($scope,$http) {

    $http.post('/today/top/section/info', { dateTime: new Date() })
        .success(function(response) {
            if (response.SuccessCode) {
                $scope.meetingsCount = getValidStringNumber(response.Data.todayMeetings || 0);
            }
        });
}

function peopleToConnect($scope,$http) {

    $http.get('/people/to/connect/info', {params : {infoFor:'dashboard'}})
        .then(function(response) {
            if (response.data.SuccessCode)
                $scope.rtcCount = getValidStringNumber(response.data.Data.peopleToConnect|| 0);
            else
                $scope.rtcCount = getValidStringNumber(0);
        });
}

function responsesPending($scope,$http) {

    $http.get('/insights/your/responses/pending', { params: { days:0, infoFor:"graph" } }).then(function(resp) {
        if (resp.data.SuccessCode) {

            var internal = resp.data.Data.internal.length ? resp.data.Data.internal.length : 0;
            var external = resp.data.Data.external.length ? resp.data.Data.external.length : 0;
            var totalMails = internal + external;
            $scope.emailsToRespond = getValidStringNumber(totalMails || 0);
        }
        else{
            $scope.emailsToRespond = getValidStringNumber(0);
        }
    });
}

function getValidStringNumber(number){
    if(typeof number == 'number'){
        return number < 10 ? '0'+number : number
    }
    else return '00';
};

reletasApp.factory('teamMembers', function($http, $q) {
    var data = [],
        lastRequestFailed = true,
        promise;
    return {
        getTeamMembers: function() {
            if (!promise || lastRequestFailed) {
                // $http returns a promise, so we don't need to create one with $q
                promise = $http.get('/company/members')
                    .then(function(res) {
                        lastRequestFailed = false;
                        data = res.data;
                        return data;
                    }, function(res) {
                        return $q.reject(res);
                    });
            }
            return promise;
        }
    }
});

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue) {

    if (parameterValue instanceof Array)
        if (parameterValue.length > 0)
            parameterValue = parameterValue.join(",")
        else
            parameterValue = null

    if (parameterValue != undefined && parameterValue != null) {
        if (baseUrl.indexOf("?") == -1)
            baseUrl += "?"
        else
            baseUrl += "&"
        baseUrl += parameterName + "=" + parameterValue
    }

    return baseUrl
}