var reletasApp = angular.module('reletasApp', ['ngRoute','ngSanitize']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/todaySeries/leftMeetingBar"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){
        //var str = searchContent.replace(/[^\w\s]/gi, '');
        //var str = searchContent;

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

reletasApp.service('meetingObjService', function () {
    return {
       isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        setMeeting:function(m){
            this.meetingOb = m;
            this.isOk = true;
        },
        getMeeting:function(){
            return this.meetingOb;
        },
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        getUserId:function(){
           return this.userId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
           return this.selectedParticipantName;
        },
        updateTaskStatus:function($scope,$http,taskId,status,fetchtasks){
        var reqObj = {
            taskId:taskId,
            status:!status ? 'notStarted':'complete'
        };
        $http.post('/task/update/status/web',reqObj)
            .success(function(response){
                if(response.SuccessCode){
                    toastr.success(response.Message);
                    if(fetchtasks)
                        $scope.timelineTasks($scope.cuserId,$scope.cEmailId)
                }
                else toastr.error(response.Message);
            })
    }
    };
});

var timezone,l_userId;
reletasApp.controller("logedinUser", function ($scope, $http, meetingObjService,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {

            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                meetingObjService.l_user = response.Data;
                l_userId = $scope.l_usr._id;
                meetingObjService.isCorporateUser = response.Data.corporateUser

                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
                meetingObjService.lUseEmailId = response.Data.emailId
            }
            else{

            }
        }).error(function (data) {

        })
});
var once = true;
reletasApp.controller("left_bar_today", function ($scope, $http, meetingObjService) {
    if(once) {
        once = false;

        meetingObjService.fetchLeftBarTodayData = function(){
            $scope.todayMeetings = [];
            $scope.newMeetings = [];
            $scope.upcomingMeetings = [];
            $scope.tasks = {};
            $http.get('/today/left/section')
                .success(function (response) {
                    if(response.SuccessCode){
                        if(checkRequired(response.Data.timezone)){
                            timezone = response.Data.timezone;
                        }
                        var meetingIdSelected = $scope.getSelectedMeetingId();
                        if(typeof response.Data.confirmedMeetings != 'string' && checkRequired(response.Data.confirmedMeetings != undefined && response.Data.confirmedMeetings.length > 0)){
                            $scope.noConfirmedMeetingsMessage = '';
                            response.Data.confirmedMeetings.sort(function (o1, o2) {
                                var date1 = new Date(o1.scheduleTimeSlots[0].start.date);
                                var date2 = new Date(o2.scheduleTimeSlots[0].start.date);
                                return date1 < date2 ? -1 : date1 > date2 ? 1 : 0;
                            });
                            for(var i=0; i<response.Data.confirmedMeetings.length; i++){
                                if($scope.todayMeetings.length < 3){
                                    $scope.todayMeetings.push($scope.formatMeetingDetails(response.Data.confirmedMeetings[i],response.Data.userId,meetingIdSelected));
                                    $scope.todayMeetings.sort(function (o1, o2) {
                                        return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                    });
                                }
                            }
                        }
                        if(typeof response.Data.pendingMeetings != 'string' && checkRequired(response.Data.pendingMeetings != undefined && response.Data.pendingMeetings.length > 0)){
                            $scope.noNewInvitationsMessage = '';
                            response.Data.pendingMeetings.sort(function (o1, o2) {
                                var date1 = new Date(o1.scheduleTimeSlots[0].start.date);
                                var date2 = new Date(o2.scheduleTimeSlots[0].start.date);
                                return date1 < date2 ? -1 : date1 > date2 ? 1 : 0;
                            });
                            for(var j=0; j<response.Data.pendingMeetings.length; j++){
                                if($scope.newMeetings.length < 3){
                                    $scope.newMeetings.push($scope.formatMeetingDetails(response.Data.pendingMeetings[j],response.Data.userId,meetingIdSelected));
                                    $scope.newMeetings.sort(function (o1, o2) {
                                        return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                    });
                                }
                            }
                        }
                        if(typeof response.Data.upcomingMeetings != 'string' && checkRequired(response.Data.upcomingMeetings != undefined && response.Data.upcomingMeetings.length > 0)){
                            $scope.noUpcomingMeetingsMessage = '';
                            response.Data.upcomingMeetings.sort(function (o1, o2) {
                                var date1 = new Date(o1.scheduleTimeSlots[0].start.date);
                                var date2 = new Date(o2.scheduleTimeSlots[0].start.date);
                                return date1 < date2 ? -1 : date1 > date2 ? 1 : 0;
                            });
                            for(var k=0; k<response.Data.upcomingMeetings.length; k++){

                                if(k < 3){
                                    var mObj = $scope.formatMeetingDetails(response.Data.upcomingMeetings[k],response.Data.userId,meetingIdSelected);
                                    var a = {
                                        dateText:mObj.dateUpcoming,
                                        sortDate:mObj.sortDate,
                                        meetings:[mObj]
                                    };
                                    var exists = false;
                                    for(var z=0; z<$scope.upcomingMeetings.length; z++){
                                        if($scope.upcomingMeetings[z].dateText == mObj.dateUpcoming){
                                            exists = true;
                                            $scope.upcomingMeetings[z].meetings.push(mObj)
                                            $scope.upcomingMeetings[z].meetings.sort(function (o1, o2) {
                                                return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                            });
                                        }
                                    }
                                    if(!exists){
                                        $scope.upcomingMeetings.push(a);
                                    }
                                    $scope.upcomingMeetings.sort(function (o1, o2) {
                                        return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                    });

                                }else break;
                            }
                        }
                        if(typeof response.Data.pendingTasks != 'string' && checkRequired(response.Data.pendingTasks != undefined && response.Data.pendingTasks.length > 0)){
                            response.Data.pendingTasks.sort(function (o1, o2) {
                                var date1 = new Date(o1.dueDate);
                                var date2 = new Date(o2.dueDate);
                                return date1 < date2 ? -1 : date1 > date2 ? 1 : 0;
                            });
                            for(var l=0; l<response.Data.pendingTasks.length; l++){
                                if(l < 3){
                                    var obj = {
                                        url:'/task/'+response.Data.pendingTasks[l]._id,
                                        taskId:response.Data.pendingTasks[l]._id,
                                        sortDate:new Date(response.Data.pendingTasks[l].dueDate),
                                        status:response.Data.pendingTasks[l].status == 'complete',
                                        updateTaskStatus:response.Data.pendingTasks[l].assignedTo == response.Data.userId
                                    };
                                    obj.taskName = response.Data.pendingTasks[l].taskName || 'No Name'
                                    obj.taskName = getTextLength(obj.taskName,20);
                                    if(response.Data.pendingTasks[l].assignedTo == response.Data.userId){
                                        obj.id = response.Data.pendingTasks[l].createdBy;
                                        obj.separate = response.Data.pendingTasks[l].createdByEmailId
                                        obj.picUrl = '/getImage/'+response.Data.pendingTasks[l].createdBy || '/images/default.png';
                                    }
                                    else{
                                        obj.id = response.Data.pendingTasks[l].assignedTo;
                                        obj.separate = response.Data.pendingTasks[l].assignedToEmailId
                                        obj.picUrl = '/getImage/'+response.Data.pendingTasks[l].assignedTo || '/images/default.png';
                                    }
                                    if($scope.tasks[obj.separate]){
                                        $scope.tasks[obj.separate].items.push(obj);
                                        $scope.tasks[obj.separate].items.sort(function (o1, o2) {
                                            return o1.sortDate < o2.sortDate ? -1 : o1.sortDate > o2.sortDate ? 1 : 0;
                                        });
                                    }
                                    else{
                                        $scope.tasks[obj.separate] = {items:[],picUrl:obj.picUrl};
                                        $scope.tasks[obj.separate].items = [obj];
                                    }
                                }
                            }
                        }
                    }
                    if(typeof response.Data.pendingMeetings == 'string'){
                        $scope.noNewInvitationsMessage = response.Data.pendingMeetings;
                    }
                    if(typeof response.Data.pendingTasks == 'string'){
                        $scope.noTasksMessage = response.Data.pendingTasks;
                    }
                    if(typeof response.Data.confirmedMeetings == 'string'){
                        $scope.noConfirmedMeetingsMessage = response.Data.confirmedMeetings;
                    }
                    if(typeof response.Data.upcomingMeetings == 'string'){
                        $scope.noUpcomingMeetingsMessage = response.Data.upcomingMeetings;
                    }
                }).error(function (data) {

                });
        };
        meetingObjService.fetchLeftBarTodayData();
    }

    $scope.goToCalendar = function(){
        window.location = '/calendar'
    };

    $scope.goToTasks = function(){
        window.location = '/tasks'
    };

    $scope.goToUrl = function(url){
        if(url && url.length > 0 && url[0] && url[0][0] && url[0][0] != 'null' && url[0][0] != null ) {
            window.location = url[0][0];
        }
    };

    $scope.getSelectedMeetingId = function(){
        var url = window.location.href;
        url = url.split('/')
        url = url[url.length - 1];
        return url;
    };

    $scope.formatMeetingDetails = function(meeting,userId,meetingIdSelected){

        var date = checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].start.date).tz(timezone) : moment(meeting.scheduleTimeSlots[0].start.date);
        var obj = {
            sortDate:new Date(date.format()),
            start:date.format("hh:mm A"),
            end:checkRequired(timezone) ? moment(meeting.scheduleTimeSlots[0].end.date).tz(timezone).format("hh:mm A") : moment(meeting.scheduleTimeSlots[0].end.date),
            title:meeting.scheduleTimeSlots[0].title,
            date:date.format("DD MMM YYYY"),
            dateUpcoming:date.format("MMM DD YYYY"),
            invitationId:meeting.invitationId,
            url:'/today/details/'+meeting.invitationId,
            moreParticipatsShow:meeting.selfCalendar && meeting.toList && meeting.toList.length > 1 ? true : false,
            className:meeting.invitationId == meetingIdSelected ? 'highlight-active': ''
        };
        obj.title = getTextLength(obj.title,20);

        if(meeting.suggested){
            if(meeting.suggestedBy.userId == userId){
                if(meeting.senderId == userId){
                    var data1 = $scope.getDetailsIfSender(meeting);
                    obj.picUrl = data1.picUrl;
                    obj.name = data1.name;
                }
                else{
                    var data3 = $scope.getDetailsIfReceiver(meeting);
                    obj.picUrl = data3.picUrl;
                    obj.name = data3.name;
                }
            }
            else{
                if(meeting.senderId == meeting.suggestedBy.userId){
                    var data5 = $scope.getDetailsIfReceiver(meeting)
                    obj.picUrl = data5.picUrl;
                    obj.name = data5.name;
                }
                else{
                    if(meeting.selfCalendar){
                        for(var j=0; j<meeting.toList.length; j++){
                            if(meeting.suggestedBy.userId == meeting.toList[j].receiverId){
                                obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                                var fName = meeting.toList[j].receiverFirstName || '';
                                var lName = meeting.toList[j].receiverLastName || '';
                                obj.name = fName+' '+lName;
                            }
                        }
                    }
                    else{
                        obj.picUrl = '/getImage/'+meeting.to.receiverId;
                        obj.name = meeting.to.receiverName;
                    }
                }
            }
        }
        else{
            if(meeting.senderId == userId){
                var data2 = $scope.getDetailsIfSender(meeting);
                obj.picUrl = data2.picUrl;
                obj.name = data2.name;
            }
            else{
                var data4 = $scope.getDetailsIfReceiver(meeting);
                obj.picUrl = data4.picUrl;
                obj.name = data4.name;
            }
        }

        if(!checkRequired(obj.picUrl) && !checkRequired(obj.name)){
            obj.picUrl = '/getImage/'+meeting.senderId;
            obj.name = meeting.senderName;
        }

        if(!checkRequired(obj.picUrl)){
            obj.picUrl = '/images/default.png';
        }

        return obj;
    };

    $scope.getDetailsIfSender = function(meeting){
        var obj = {};
        if(meeting.selfCalendar){
            if(meeting.toList.length > 0){
                var isDetailsExists = false;

                for(var i=0; i<meeting.toList.length; i++){
                    if(meeting.toList[i].isPrimary){
                        isDetailsExists = true;
                        obj.picUrl = '/getImage/'+meeting.toList[i].receiverId;
                        var fName = meeting.toList[i].receiverFirstName || '';
                        var lName = meeting.toList[i].receiverLastName || '';
                        obj.name = fName+' '+lName;
                    }
                }
                if(!isDetailsExists){
                    for(var j=0; j<meeting.toList.length; j++){
                        if(checkRequired(meeting.toList[j].receiverId)){
                            isDetailsExists = true;
                            obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                            var fName2 = meeting.toList[j].receiverFirstName || '';
                            var lName2 = meeting.toList[j].receiverLastName || '';
                            obj.name = fName2+' '+lName2;
                        }
                    }
                }
                if(!isDetailsExists){
                    obj.picUrl = '/images/default.png';
                    obj.name = meeting.toList[0].receiverEmailId;
                }
            }
        }
        else{
            if(meeting.to){
                if(meeting.to.receiverId){
                    obj.picUrl = '/getImage/'+meeting.to.receiverId;
                    obj.name = meeting.to.receiverName;
                }
                else{
                    obj.picUrl = '/images/relatasLogo_Google.png';
                    obj.name = meeting.to.receiverEmailId;
                }
            }
        }
        return obj;
    };

    $scope.getDetailsIfReceiver = function(meeting){
        var obj = {};
        if(meeting.senderId){
            obj.picUrl = '/getImage/'+meeting.senderId;
            obj.name = meeting.senderName;
        }
        else{
            obj.picUrl = '/images/default.png';
            obj.name = meeting.senderEmailId;
        }
        return obj;
    };

    $scope.updateTaskStatus = function(taskId,status){
        meetingObjService.updateTaskStatus($scope,$http,taskId,status,false);
    }


});

function getDateFormat(date,ISOFormat){
    date = checkRequired(timezone) ? moment(date).tz(timezone) : moment(date);
    if(ISOFormat)
        return date.format();
    else
        return date.format("DD-MM-YYYY hh:mm a");
}

function getTimeFormat(date) {
    date = checkRequired(timezone) ? moment(date).tz(timezone) : moment(date)
    var hrs = date.hours();
    var min = date.minutes();
    if (hrs < 10) {
        hrs = "0" + hrs
    }

    if (min < 10) {
        min = "0" + min;
    }

   return hrs +':'+ min
}

reletasApp.controller("meetingDetails", function ($scope, $http, meetingObjService) {
    $scope.confirm_meeting_text = 'Confirm'
    $scope.meeting_download_ics = false;
    $scope.meeting_confirm = false;
    $scope.meeting_decline = false;
    $scope.meeting_re_schedule = false;

    $scope.meetingBeforeShow = false;
    $scope.meetingAfterShow = false;

    $scope.invitationId = null;
    $scope.slotId = null;
    $scope.meetingDuration = 60;
    $scope.allowTimes = [];

    var url = window.location.pathname.split('/');
    var invitationId = url[url.length - 1];
    var disable = moment();
    disable.date(disable.date() + 1);
    disable.hours(8);
    disable.minutes(0);
    disable.seconds(0);
    $scope.applyDateTimePicker = function(id,key,current,slotDuration){
        
        slotDuration = slotDuration <= 60 ? slotDuration : 60;
        $('#'+id).datetimepicker({
            dayOfWeekStart: 1,
            format:'d-m-Y h:i a',
            lang: 'en',
            value:current,
            minDate: new Date(),
            step:slotDuration,
            onClose: function (dp, $input){
                var selected = moment(dp);

                var isStartTime = key == "reschedule_start_date";
                var isFree = true;
                if(selected.isAfter(moment())){
                    $scope[key] = selected.format();
                    var end = selected.clone();
                    end = end.minutes(selected.minutes() + slotDuration);
                    $scope["reschedule_end_date"] = end.format();
                    $("#reschedule_end_date_picker").val(getDateFormat($scope["reschedule_end_date"],false))
                }
                else{
                    $input.val(getDateFormat($scope[key],false));
                    toastr.error("Please select future time");
                }
            },

            onGenerate:function( ct,a,b ){
                
                $(".remove_disable_plugin").removeClass("disabled")
                var day = [moment(ct).tz(timezone).format("MM-DD-YYYY")];
                if($scope.allSlots && $scope.allSlots[day]) {
                    var slots = $scope.allSlots[day];

                    var allSlots = [];
                    allSlots = allSlots.concat(slots.am.slots);
                    allSlots = allSlots.concat(slots.pm.slots);
                    if(allSlots.length > 0){
                        allSlots.forEach(function(item) {
                            if (item.isBlocked) {
                                $("#disable"+moment(item.start).tz(timezone).format("HH-mm")).addClass("disabled")
                            }
                        })
                    }
                }
            }
        });
    };

    $scope.validateSameMeeting = function(item,selected,isStartTime){
        if(isStartTime && selected.isSame($scope.start)){
            return true;
        }
        else if(!isStartTime && selected.isSame($scope.end)){
            return true;
        }
        else{
            return false;
        }
    }

    $scope.getMeeting = function(){
        $http.get("/meeting/get/"+invitationId+"/meeting")
            .success(function(response){

                if(response.SuccessCode){
                    if(checkRequired(response.Data.to) && checkRequired(response.Data.to.receiverEmailId)){
                        $("#meetingParticipants_content").show()
                        $(".seeMoreIcon").removeClass("disabled-see-more");
                    }
                    else if(response.Data.toList && response.Data.toList.length > 0){
                        $("#meetingParticipants_content").show()
                        $(".seeMoreIcon").removeClass("disabled-see-more");
                    }
                    else{
                        setTimeout(function(){
                            $(".seeMoreIcon").addClass("disabled-see-more");
                        },2000)
                        $("#meetingParticipants_content").hide()
                    }

                    if(checkRequired(response.timezone)){
                        timezone = response.timezone;
                    }
                    meetingObjService.setMeeting(response.Data);
                    meetingObjService.setUserId(response.userId);
                    var start = checkRequired(timezone) ? moment(response.Data.scheduleTimeSlots[0].start.date).tz(timezone) : moment(response.Data.scheduleTimeSlots[0].start.date);
                    var end = checkRequired(timezone) ? moment(response.Data.scheduleTimeSlots[0].end.date).tz(timezone) : moment(response.Data.scheduleTimeSlots[0].end.date);

                    $scope.start = start;
                    $scope.end = end;
                    $scope.hideOrRemoveMeetingActionButtons(response.Data,response.userId,start,response.Data.invitationId);
                    $scope.meetingTitle = response.Data.scheduleTimeSlots[0].title;
                    $scope.MeetingLocation = response.Data.scheduleTimeSlots[0].location;
                    $scope.meetingDate = start.format("DD MMM YYYY");
                    $scope.meetingTime = start.format("hh:mm A")+'-'+end.format("hh:mm A")+' ('+start.format("z")+')';

                    $scope.meetingDuration = end.diff(start,'minutes');
                    $scope.meetingAgenda = response.Data.scheduleTimeSlots[0].description || '';
                    $scope.invitationId = response.Data.invitationId;
                    $scope.slotId = response.Data.scheduleTimeSlots[0]._id;
                    $scope.locationType = response.Data.scheduleTimeSlots[0].locationType;
                    $('.selectpicker').selectpicker('val', $scope.locationType);
                     $scope.reschedule_start_date = getDateFormat(response.Data.scheduleTimeSlots[0].start.date,true);
                     $scope.reschedule_end_date = getDateFormat(response.Data.scheduleTimeSlots[0].end.date,true);
                    $scope.applyDateTimePicker('reschedule_start_date_picker','reschedule_start_date',getDateFormat(response.Data.scheduleTimeSlots[0].start.date,false),$scope.meetingDuration);
                    $scope.applyDateTimePicker('reschedule_end_date_picker','reschedule_end_date',getDateFormat(response.Data.scheduleTimeSlots[0].end.date,false),$scope.meetingDuration);

                }
                else{
                    $("#meetingParticipants_content").hide();
                    setTimeout(function(){
                        $(".seeMoreIcon").addClass("disabled-see-more");
                    },2000);

                    $scope.meetingTitle = response.Message
                    $scope.confirm_meeting_text = 'Confirm'
                    $scope.meeting_download_ics = false;
                    $scope.meeting_confirm = false;
                    $scope.meeting_decline = false;
                    $scope.meeting_re_schedule = false;

                    $scope.meetingBeforeShow = false;
                    $scope.meetingAfterShow = false;

                    $scope.invitationId = null;
                    $scope.slotId = null;

                    $scope.MeetingLocation = '';
                    $scope.meetingDate = '';
                    $scope.meetingTime = '';
                    $scope.meetingAgenda = '';
                    $scope.locationType = 'In-Person';
                }
            });
    };

    $scope.getPrimaryParticipantId = function(){
        var meeting = meetingObjService.getMeeting();
        if(meeting.selfCalendar){
            for(var i=0; i<meeting.toList.length; i++){
                if(meeting.toList[i].isPrimary && checkRequired(meeting.toList[i].receiverId)){
                    $scope.primaryParticipantId = meeting.toList[i].receiverId;
                }
            }
        }
        else if(checkRequired(meeting.to) && checkRequired(meeting.to.receiverId)){
            $scope.primaryParticipantId = meeting.to.receiverId;
        }
    };

    $scope.isMeetingAccepted = function(isSender,isSelfCalendar,userId,meeting){
        var isAccepted = false;
        if(isSender || !isSelfCalendar){
            for(var i=0; i<meeting.scheduleTimeSlots.length; i++){
                if(meeting.scheduleTimeSlots[i].isAccepted){
                    isAccepted = true;
                }
            }
            return isAccepted;
        }
        else{
            for(var j=0; j<meeting.toList.length; j++){
                if(meeting.toList[j].receiverId == userId && meeting.toList[j].isAccepted){
                    isAccepted = true;
                }
            }
        }
        return isAccepted;
    };

    $scope.hideOrRemoveMeetingActionButtons = function(meeting,userId,startDate,invitationId){

        var now = checkRequired(timezone) ? moment().tz(timezone) : moment()
        var isMeetingTimeOver = !now.isBefore(startDate);

        if(meeting.selfCalendar && meeting.toList.length > 1){
            var isAccepted = false;
            for(var s=0; s<meeting.scheduleTimeSlots.length; s++){
                if(meeting.scheduleTimeSlots[s].isAccepted){
                    isAccepted = true;
                }
            }
            $scope.meeting_decline = !isAccepted;
        }
        else isMeetingTimeOver ? $scope.meeting_decline = false : $scope.meeting_decline = true;

        if(meeting.suggested){
            if(meeting.suggestedBy.userId == userId){
                $scope.meeting_confirm = false;

                if(meeting.senderId == userId){
                    var isAccepted4 = $scope.isMeetingAccepted(true,meeting.selfCalendar,userId,meeting);
                    $scope.meeting_download_ics = isAccepted4;
                    if(isAccepted4){
                        $scope.addICSUrl(meeting.icsFile);
                    }else $scope.getMeetingBeforeAfter(startDate.format(),invitationId);

                    if(meeting.selfCalendar && meeting.toList.length > 1){
                        $scope.meeting_re_schedule = !isAccepted4;
                    }
                    else $scope.meeting_re_schedule = true;
                }
                else{
                    var isAccepted3 = $scope.isMeetingAccepted(false,meeting.selfCalendar,userId,meeting);
                    if(isAccepted3){
                        $scope.addICSUrl(meeting.icsFile);
                    }else $scope.getMeetingBeforeAfter(startDate.format(),invitationId);
                    $scope.meeting_download_ics = isAccepted3;

                    if(meeting.selfCalendar && meeting.toList.length > 1){
                        $scope.meeting_re_schedule = false;
                    }else $scope.meeting_re_schedule = true;
                }
            }
            else{
                if(!isMeetingTimeOver){
                    $scope.meeting_confirm = !$scope.isMeetingAccepted(false,meeting.selfCalendar,userId,meeting);
                }
                else $scope.meeting_confirm = false;

                if(meeting.senderId == userId){
                    var isAccepted5 = $scope.isMeetingAccepted(true,meeting.selfCalendar,userId,meeting);
                    $scope.meeting_download_ics = isAccepted5;
                    if(isAccepted5){
                        $scope.addICSUrl(meeting.icsFile);
                        $scope.meeting_confirm = false
                    }else $scope.getMeetingBeforeAfter(startDate.format(),invitationId);

                    if(meeting.selfCalendar && meeting.toList.length > 1){
                        $scope.meeting_re_schedule = !isAccepted5;
                    }
                    else $scope.meeting_re_schedule = true;
                }
                else{
                    var isAccepted6 = $scope.isMeetingAccepted(false,meeting.selfCalendar,userId,meeting);
                    if(isAccepted6){
                        $scope.addICSUrl(meeting.icsFile);
                    }else $scope.getMeetingBeforeAfter(startDate.format(),invitationId);
                    $scope.meeting_download_ics = isAccepted6;

                    if(meeting.selfCalendar && meeting.toList.length > 1){
                        $scope.meeting_re_schedule = false;
                    }else $scope.meeting_re_schedule = true;
                }
            }
        }
        else{
            if(meeting.senderId == userId){
                var isAccepted1 = $scope.isMeetingAccepted(true,meeting.selfCalendar,userId,meeting);
                if(meeting.selfCalendar && meeting.toList.length > 1){
                    $scope.meeting_re_schedule = !isAccepted1;
                }
                else $scope.meeting_re_schedule = true;

                $scope.meeting_confirm = false;
                $scope.meeting_download_ics = isAccepted1;
                if(isAccepted1){
                    $scope.addICSUrl(meeting.icsFile);
                }else $scope.getMeetingBeforeAfter(startDate.format(),invitationId);
            }
            else{
                var isAccepted2 = $scope.isMeetingAccepted(false,meeting.selfCalendar,userId,meeting);
                if(meeting.selfCalendar && meeting.toList.length > 1){
                    $scope.meeting_re_schedule = false;
                }else $scope.meeting_re_schedule = true;

                if(!isMeetingTimeOver){
                    $scope.meeting_confirm = !isAccepted2;
                }
                else $scope.meeting_confirm = false;

                $scope.meeting_download_ics = isAccepted2;
                if(isAccepted2){
                    $scope.addICSUrl(meeting.icsFile);
                }else $scope.getMeetingBeforeAfter(startDate.format(),invitationId);
            }
        }
    };

    $scope.addICSUrl = function(icsFile){
        $scope.ics_url = icsFile.url;
    };

    $scope.getLocationTypePic = function(meetingLocationType){
        switch (meetingLocationType) {
            case 'In-Person':
                return "/images/icon_location_small.png";
                break;
            case 'Phone':
                return "/images/icon_phone.png";
                break;
            case 'Skype':
                return "/images/icon_skype.png";
                break;
            case 'Conference Call':
                return "/images/conference_call.png";
                break;
            default:return "/images/icon_location_small.png"
                break;
        }
    };

    $scope.getMeetingBeforeAfter = function(date,invitationId){
        $http.post('/meeting/before/after/web',{dateTime:date,invitationId:invitationId})
            .success(function(response){

                if(response.SuccessCode){
                    if(response.Data.length > 0){
                        for(var i=0; i<response.Data.length; i++){
                            if(response.Data[i].position == 'before' && checkRequired(response.Data[i].endDate)){
                                $scope.meetingBeforeName = response.Data[i].firstName || '';
                                $scope.meetingBeforeShow = true;
                                if(checkRequired(response.Data[i].userId)){
                                    $scope.meetingBeforePic = '/getImage/'+response.Data[i].userId;
                                }
                                else $scope.meetingBeforePic = '/images/default.png';

                                var meetingBeforeTimeStart = checkRequired(timezone) ? moment(response.Data[i].startDate).tz(timezone) : moment(response.Data[i].startDate);
                                var meetingBeforeTimeStartEnd = checkRequired(timezone) ? moment(response.Data[i].endDate).tz(timezone): moment(response.Data[i].endDate);

                                $scope.meetingBeforeTime = meetingBeforeTimeStart.format("hh:mm A")+'-'+meetingBeforeTimeStartEnd.format("hh:mm A")+' ('+meetingBeforeTimeStart.format("z")+')';
                                $scope.meetingBeforeLocationTypePic = $scope.getLocationTypePic(response.Data[i].locationType);

                            }
                            if(response.Data[i].position == 'after' && checkRequired(response.Data[i].endDate)){
                                $scope.meetingAfterName = response.Data[i].firstName || '';
                                $scope.meetingAfterShow = true;
                                if(checkRequired(response.Data[i].userId)){
                                    $scope.meetingAfterPic = '/getImage/'+response.Data[i].userId;
                                }
                                else $scope.meetingAfterPic = '/images/default.png';
                                var start2 = checkRequired(timezone) ? moment(response.Data[i].startDate).tz(timezone) : moment(response.Data[i].startDate);
                                var end2 = checkRequired(timezone) ? moment(response.Data[i].endDate).tz(timezone) : moment(response.Data[i].endDate);

                                $scope.meetingAfterTime = start2.format("hh:mm A")+'-'+end2.format("hh:mm A")+' ('+start2.format("z")+')';
                                $scope.meetingAfterLocationTypePic = $scope.getLocationTypePic(response.Data[i].locationType);

                            }
                        }
                    }
                }
            })
    };

    $scope.confirmWithMessage = function(message){

        var reqBody = {
            "invitationId": $scope.invitationId,
            "slotId": $scope.slotId,
            "message":message
        };
        $scope.acceptMeetingRequest(reqBody);

    };

    $scope.confirmWithoutMessage = function(){
        var reqBody = {
            "invitationId": $scope.invitationId,
            "slotId": $scope.slotId
        };
        $scope.acceptMeetingRequest(reqBody);
    };

    $scope.confirmPopupClose = function(){
        $(".confirm-popup").toggle(100)
    };

    $scope.acceptMeetingRequest = function(reqBody){

        $scope.confirm_meeting_text = 'Please wait..';
        $('.confirm-popup').css({display:'none'});
        $http.post('/meetings/action/accept/web',reqBody)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.meeting_confirm = false;
                    $scope.getMeeting();
                    meetingObjService.fetchLeftBarTodayData();
                    meetingObjService.participantClickPast90Days_again();
                    meetingObjService.timelineTimeline_again();
                    toastr.success(response.Message);
                }
                else{
                    $scope.meeting_confirm = true;
                    $scope.getMeeting();
                    toastr.error(response.Message);
                }
            })
    };

    $scope.re_scheduleMeeting = function(suggestLocation,comment){
        if($scope.reschedule_start_date != undefined && checkRequired($scope.reschedule_start_date) && $scope.reschedule_end_date != undefined && checkRequired($scope.reschedule_end_date)){

            var start = $scope.reschedule_start_date;
            var end = $scope.reschedule_end_date;

            //if(moment(start).isSame(moment($scope.start)) && moment(end).isSame($scope.end)){
            //    toastr.error('Please select new date and time for rescheduling');
            //}
           // else
           //
           // if(moment(start)){
           //
           // }

            var ms = moment(end,"DD/MM/YYYY HH:mm:ss").diff(moment(start,"DD/MM/YYYY HH:mm:ss"));
            var d = moment.duration(ms);
            var slots = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");

            if(moment(start).isSame(end) || moment(start).isAfter(end)){
                toastr.error("Please select valid date and time");
            }
            else if(typeof suggestLocation == undefined || !checkRequired(suggestLocation)){
                toastr.error('Please suggest a new location');
            }
            else{
                $scope.locationType = $('.selectpicker').val()
                $('.re-schedule-popup').css({display:'none'});
                start = moment(start)
                end = moment(end)
                var reqObj = {
                        "invitationId":$scope.invitationId,
                        "comment":comment,
                        "newSlotDetails":{
                            "startDateTime":start.format(),
                            "endDateTime":end.format(),
                            "suggestedLocation":suggestLocation,
                            "locationType":$scope.locationType
                        }
                    };

                $http.post('/meetings/action/edit/web',reqObj)
                    .success(function(response){

                        if(response.SuccessCode){
                            $scope.suggested_location = ""
                            $scope.suggest_comment = ""
                            $scope.getMeeting();
                            meetingObjService.fetchLeftBarTodayData();
                            meetingObjService.participantClickPast90Days_again();
                            meetingObjService.timelineTimeline_again();
                            toastr.success(response.Message);
                        }
                        else{
                            $scope.getMeeting();
                            toastr.error(response.Message);
                        }
                    })
            }
        }
        else toastr.error('Please select valid date and time.');
    };

    $scope.declineMeeting = function(message){
        var reqObj = {
            invitationId:$scope.invitationId,
            message:message
        };
        $http.post('/meetings/action/cancel/web',reqObj)
            .success(function(response){
                $('.decline-popup').css({display:'none'});
                if(response.SuccessCode){
                    toastr.success(response.Message);
                    $scope.getMeeting();
                    meetingObjService.fetchLeftBarTodayData();

                }
                else{
                    $scope.getMeeting();
                    toastr.error(response.Message);
                }
            })
    };

    $scope.getMeeting();

    $("#re-schedule-meeting").on("click",function(){
        $scope.getPrimaryParticipantId();
       bindCalendarData($http, $scope, $scope.primaryParticipantId, timezone, $scope.meetingDuration <= 60 ? $scope.meetingDuration : 60, true, 14)
    });

    function bindCalendarData($http, $scope, userId, timezone, duration, mapMyCal, daysDuration){

        var url = '/schedule/slots/available?id='+userId+'&slotDuration='+duration+'&daysDuration='+daysDuration+'&timezone='+timezone+'&withSlotDates=yes'
        if(mapMyCal){
            url = '/schedule/slots/available/mapcalendar?id='+userId+'&slotDuration='+duration+'&daysDuration='+daysDuration+'&timezone='+timezone+'&withSlotDates=yes'
        }

        $http.get(url)
            .success(function(oFreeSlots){
                if(oFreeSlots.SuccessCode){
                    $scope.allSlots = oFreeSlots.Data;
                }
            })
    }
});

reletasApp.controller("meetingParticipants", function ($scope, $http, meetingObjService) {
    function getMeeting(callback){
        if(meetingObjService.getIsOk() == true){
            callback(meetingObjService.getMeeting());
        }
        else{
            setTimeout(function(){
                return getMeeting(callback);
            },100)
        }
    }

    function isCConnectionFunctionExists(callback){
        if(typeof meetingObjService.commonConnections == 'function'){
            callback();
        }
        else{
            setTimeout(function(){
                return isCConnectionFunctionExists(callback);
            },100)
        }
    }

    function isCConnectionFunctionExists_company(callback){
        if(typeof meetingObjService.commonConnections_company == 'function'){
            callback();
        }
        else{
            setTimeout(function(){
                return isCConnectionFunctionExists(callback);
            },100)
        }
    }

    function isUserObjExists(callback){
        if(typeof meetingObjService.l_user == 'object'){
            callback();
        }
        else{
            setTimeout(function(){
                return isUserObjExists(callback);
            },100)
        }
    }

    function isTimelineExists(callback){
        if(typeof meetingObjService.timelineTimeline == 'function'){
            callback();
        }
        else{
            setTimeout(function(){
                return isTimelineExists(callback);
            },100)
        }
    }

    function isTopTrendingPostExists(callback){
        if(typeof meetingObjService.topTrendingSocialPost == 'function'){
            callback();
        }
        else{
            setTimeout(function(){
                return isTopTrendingPostExists(callback);
            },100)
        }
    }

    getMeeting(function(meetingObj){
        showParticipantsStatus(meetingObj,meetingObjService.getUserId());
    });

    meetingObjService.updateUserInfo = function(relation,connectedVia){
        meetingObjService.p_name = relation.personName || '';
        if(!checkRequired(meetingObjService.selectedParticipantName)){
            meetingObjService.setSelectedParticipantName(relation.personName)
        }
        $scope.p_position = $scope.getPosition(relation.companyName,relation.designation);

        $scope.p_class_invisible = checkRequired($scope.p_position) ? '' : 'invisible'
        if(!checkRequired($scope.p_position)){
            $scope.p_position = 'No Designation'
        }

        $scope.p_location = relation.location || '';
        $scope.p_mobileNumber = relation.mobileNumber || '';
        $scope.p_emailId = relation.personEmailId || '';
        $scope.p_favorite = relation.favorite || false;
        $scope.p_favoriteColor = relation.favorite == true ? 'color: #f86b4f;' : 'color: #777777!important';
        $scope.p_favoriteColor_a = relation.favorite == true ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';

        $scope.l_userId = l_userId;
        $scope.p_id = relation._id;

        if(connectedVia && connectedVia.length > 0){

        }
        else connectedVia = [];

        $http.get('/interactions/connected/via/linkedin?emailId='+$scope.p_emailId)
            .success(function(response){
                $scope.p_connected_via_linkedin_color = response.Data.connectedVia.linkedin ? 'color: #f86b4f;background: transparent !important;border: none;' : '';
            });

        $http.get('/interactions/connected/via/twitter?emailId='+$scope.p_emailId+'&twitterUserName='+relation.twitterUserName)
            .success(function(response){
                $scope.p_connected_via_twitter_color = response.Data.connectedVia.twitter ? 'color: #f86b4f;background: transparent !important;border: none;' : '';
            });

        $http.get('/interactions/connected/via/facebook?emailId='+$scope.p_emailId)
            .success(function(response){
                $scope.p_connected_via_facebook_color = response.Data.connectedVia.facebook ? 'color: #f86b4f;background: transparent !important;border: none;' : '';
            });

        if(!relation.relation) {
            relation.relation = {};
        }

            $scope.p_customerColor = relation.relation.prospect_customer == "customer" ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
            $scope.p_prospectColor = relation.relation.prospect_customer == "prospect" ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
            $scope.p_influencerColor = relation.relation.decisionmaker_influencer == "influencer" ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
            $scope.p_decision_makerColor = relation.relation.decisionmaker_influencer == "decision_maker" ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
            $scope.p_relation_customer = "customer";
            $scope.p_relation_prospect = "prospect";
            $scope.p_relation_influencer = "influencer";
            $scope.p_relation_decision_maker = "decision_maker";
            $scope.p_relation_key_prospect_customer = 'prospect_customer'
            $scope.p_relation_key_decisionmaker_influencer = 'decisionmaker_influencer'

        $scope.p_relation_customer_value = relation.relation.prospect_customer == "customer" ? null :"customer";
        $scope.p_relation_prospect_value = relation.relation.prospect_customer == "prospect" ? null :"prospect";
        $scope.p_relation_influencer_value = relation.relation.decisionmaker_influencer == "influencer" ? null : "influencer";
        $scope.p_relation_decision_maker_value = relation.relation.decisionmaker_influencer == "decision_maker" ? null : "decision_maker";

        meetingObjService.bindPName(meetingObjService.getSelectedParticipantName())
        meetingObjService.interaction_initiations_name(meetingObjService.getSelectedParticipantName())
       // meetingObjService.bindPName_top_trending_post(meetingObjService.p_name)
       // meetingObjService.setSelectedParticipantName(meetingObjService.p_name);
    };

    $scope.getPosition = function(companyName,designation){
        var position = '';

        if(checkRequired(companyName) && checkRequired(designation)){
            position = designation+', '+companyName
        }
        else if(checkRequired(companyName) && !checkRequired(designation)){
            position = companyName
        }
        else if(!checkRequired(companyName) && checkRequired(designation)){
            position = designation
        }

        return position;
    };

    $scope.participantClick = function(userId,emailId,nameR){
        setTimeout(function(){
            $(".seeMoreIcon").attr("href",'/interactions?context='+emailId);
        },1000)

        meetingObjService.setSelectedParticipantName(nameR)
        for(var p=0; p<$scope.participantsList.length; p++){

            if($scope.participantsList[p].emailId == emailId){
                $scope.participantsList[p].className = 'tab-user-list';
            }else $scope.participantsList[p].className = '';
        }
        meetingObjService.participantClickPast90Days(userId,emailId);
        isCConnectionFunctionExists(function(){
            meetingObjService.commonConnections(userId,emailId,'/contacts/filter/common/web?id='+userId+'&skip=0&limit=12');
        });
        isCConnectionFunctionExists_company(function(){
            isUserObjExists(function(){
                meetingObjService.commonConnections_company(userId,emailId,'/contacts/filter/common/companyName/web?companyName='+meetingObjService.l_user.companyName+'&emailId='+meetingObjService.l_user.emailId+'&fetchWith='+emailId+'&skip=0&limit=12');
            })
        });
        isTimelineExists(function(){
            isUserObjExists(function(){
                meetingObjService.timelineTimeline(userId,emailId);
            })
        });
        isTopTrendingPostExists(function(){
            meetingObjService.topTrendingSocialPost(userId,emailId);
        })
    };
    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }
    function showParticipantsStatus(meeting,userId){
        $scope.participantsList = [];

        var pObj = {
            userId:meeting.senderId,
            emailId:meeting.senderEmailId,
            name:meeting.senderName,
            noPicFlag:!checkRequired(meeting.senderId),
            image:checkRequired(meeting.senderId) ? '/getImage/'+meeting.senderId : null
        };
        if(!checkRequired(meeting.senderName) && checkRequired(meeting.senderEmailId)){
            pObj.name = meeting.senderEmailId
        }

        if(pObj.userId != userId){
            $scope.participantsList.push(pObj);
        }

        if(meeting.selfCalendar){
            for(var i=0; i<meeting.toList.length; i++){
                if(!meeting.toList[i].isResource){
                    if(meeting.toList[i].receiverId != userId){
                        var name1 =  meeting.toList[i].receiverFirstName || '';
                        var name2 = meeting.toList[i].receiverLastName || '';
                        var fName = name1+' '+name2;
                        if(!checkRequired(fName.trim())){
                            fName = meeting.toList[i].receiverEmailId
                        }
                        var x= {
                            userId:meeting.toList[i].receiverId,
                            emailId:meeting.toList[i].receiverEmailId,
                            name:fName,
                            noPicFlag:!checkRequired(meeting.toList[i].receiverId),
                            image:checkRequired(meeting.toList[i].receiverId) ? '/getImage/'+meeting.toList[i].receiverId : '/images/default.png'
                        }
                        if(checkRequired(x.name)){
                            x.nameNoImg = (x.name.substr(0,2)).capitalizeFirstLetter()
                        }
                        else if(checkRequired(x.emailId)){
                            x.nameNoImg = (x.emailId.substr(0,2)).capitalizeFirstLetter()
                        }
                        $scope.participantsList.push(x);
                    }
                }
            }
        }
        else if(meeting.to.receiverId != userId){
            var rname = meeting.to.receiverName;
            if(!checkRequired(rname)){
                rname = meeting.to.receiverEmailId;
            }
            var y = {
                userId:meeting.to.receiverId,
                emailId:meeting.to.receiverEmailId,
                name:rname,
                noPicFlag:!checkRequired(meeting.to.receiverId),
                image:checkRequired(meeting.to.receiverId) ? '/getImage/'+meeting.to.receiverId : '/images/default.png'
            }
            if(checkRequired(y.name)){
                y.nameNoImg = (y.name.substr(0,2)).capitalizeFirstLetter()
            }
            else if(checkRequired(y.emailId)){
                y.nameNoImg = (y.emailId.substr(0,2)).capitalizeFirstLetter()
            }
            $scope.participantsList.push(y);
        }

        if($scope.participantsList.length > 0){
            $scope.selectedParticipantUserId = $scope.participantsList[0].userId
            $scope.selectedParticipantEmailId = $scope.participantsList[0].emailId
           // $scope.participantsList[0].className = 'tab-user-list';
            $scope.participantClick($scope.selectedParticipantUserId,$scope.selectedParticipantEmailId,$scope.participantsList[0].name)
        }
        else{
          setTimeout(function(){
              $(".seeMoreIcon").addClass("disabled-see-more")
          },1500)
        }
    }

    $scope.updateRelation = function(l_userId,p_id,relation,relationKey,value){
        var reqObj = {contactId:p_id,type:value,relationKey:relationKey};

        $http.post('/contacts/update/reltionship/type',reqObj)
            .success(function(response){

                if(response.SuccessCode){
                    if(relation == 'customer' || relation == 'prospect'){
                        $scope.p_customerColor = reqObj.type == "customer" ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
                        $scope.p_relation_customer_value = value == "customer" ? null :"customer";
                        $scope.p_prospectColor = reqObj.type == "prospect" ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
                        $scope.p_relation_prospect_value = value == "prospect" ? null :"prospect";
                    }

                    if(relation == 'influencer' || relation == 'decision_maker'){
                        $scope.p_influencerColor = reqObj.type == "influencer" ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
                        $scope.p_relation_influencer_value = value == "influencer" ? null : "influencer";
                        $scope.p_decision_makerColor = reqObj.type == "decision_maker" ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
                        $scope.p_relation_decision_maker_value = value == "decision_maker" ? null : "decision_maker";
                    }
                }
                else{
                    toastr.error(response.Message);
                }
            })
    };

    $scope.updateFavorite = function(p_id,favorite){
        var reqObj = {contactId:p_id,favorite:!favorite};
        $http.post('/contacts/update/favorite/web',reqObj)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.p_favorite = reqObj.favorite;
                    $scope.p_favoriteColor = reqObj.favorite == true ? 'color: #f86b4f;' : 'color: #777777!important';
                    $scope.p_favoriteColor_a = reqObj.favorite == true ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
                }
                else{
                    toastr.error(response.Message);
                }
            })
    }
});

reletasApp.controller("participantPast90Days", function ($scope, $http, meetingObjService) {
    meetingObjService.interaction_initiations_name = function(p_name){
        $scope.interaction_initiations_other_name = p_name;
    };

    meetingObjService.participantClickPast90Days_again = function(){
        meetingObjService.participantClickPast90Days($scope.userIdSelected,$scope.emailIdSelected);
    }
    
    meetingObjService.participantClickPast90Days = function(userId,emailId){

        $scope.userIdSelected = userId;
        $scope.emailIdSelected = emailId;
        $http.get('/meeting/participant/'+emailId+'/relationship')
            .success(function(response){

                $scope.dataFor = response.Data.dataFor;
                if(response.SuccessCode){
                    $scope.interaction_initiations_total = 0;
                    meetingObjService.updateUserInfo(response.Data.relation,response.Data.connectedVia);
                    if(response.Data.relation && response.Data.relation.relationshipStrength_updated){
                        $scope.relationshipStrength_updated = response.Data.relation.relationshipStrength_updated;
                    }
                    else $scope.relationshipStrength_updated = 0;

                    if(response.Data.interactionInitiations && response.Data.interactionInitiations.length){
                        $scope.interaction_initiations_total = 0;

                        for(var i=0; i<response.Data.interactionInitiations.length; i++){
                            $scope.interaction_initiations_total += response.Data.interactionInitiations[i].count
                            if(response.Data.interactionInitiations[i]._id == 'you'){
                                $scope.interaction_initiations_you = response.Data.interactionInitiations[i].count
                            }
                            if(response.Data.interactionInitiations[i]._id == 'others'){
                                $scope.interaction_initiations_other = response.Data.interactionInitiations[i].count
                            }
                        }

                        var a = ($scope.interaction_initiations_you*100)/$scope.interaction_initiations_total
                        var b = ($scope.interaction_initiations_other*100)/$scope.interaction_initiations_total

                        a = Math.round(a)
                        b = Math.round(b)
                        $scope.drawDonut(a,b)
                    }
                    else{
                        $scope.interaction_initiations_total = 0
                        $scope.interaction_initiations_you = 0
                        $scope.interaction_initiations_other = 0
                        $scope.relationshipStrength_updated = 0
                        $scope.drawDonut(0,0)
                    }

                    if(response.Data.relationshipStrength && response.Data.relationshipStrength.length > 0){
                        var obj = response.Data.relationshipStrength[0];
                        var doc = response.Data.doc;

                        var x = obj.maxCount - obj.minCount;
                        var ratio = x/(doc.maxRatio-doc.minRatio)
                        var s = Math.round(doc.minRatio+((obj.count - obj.minCount)/ratio))
                        $scope.relationshipStrength = s ? s : 0;

                    }
                    else $scope.relationshipStrength = 0;

                    if($scope.relationshipStrength_updated <= 0){
                        $scope.relationshipStrength_updated = $scope.relationshipStrength;
                    }
                    function updateRatio(value){
                        $scope.relationshipStrength_updated = value;
                        var reqObj = {contactId:response.Data.relation._id,relationshipStrength_updated:value};
                        $http.post('/contacts/update/relationship/strength/web',reqObj)
                            .success(function(response){
                                if(response.SuccessCode){
                                    $("#relationshipStrength_updated").text(value);
                                }
                                else toastr.error(response.Message);
                            });
                    }

                    if($scope.slider){
                        $scope.slider.destroy();
                    };

                    $scope.slider = new Slider('#ex1', {
                        formatter: function(value) {
                            return 'Current value: ' + value;
                        },
                        min:0,
                        max:100,
                        value:$scope.relationshipStrength_updated
                    });
                    $('#ex1').on('slideStop', function(ev){
                        updateRatio(ev.value)
                    });

                    $scope.getInteractionTypeObj = function(obj,total){
                        switch (obj._id){
                            case 'google-meeting':
                            case 'meeting':return {
                                priority:0,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'call':return {
                                priority:1,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'sms':return {
                                priority:2,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'email':return {
                                priority:3,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'facebook':return {
                                priority:4,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'twitter':return {
                                priority:5,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'linkedin':return {
                                priority:6,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            default : return null;
                        }
                    };

                    $scope.calculatePercentage = function(count,total){
                        return Math.round((count*100)/total);
                    };

                    if(response.Data.interactionTypes && response.Data.interactionTypes.length > 0){
                        $scope.interactionsByType = [];
                        var existingTypes = [];
                        var allTypes = ['meeting','call','sms','email','facebook','twitter','linkedin'];
                        for(var t=0; t<response.Data.interactionTypes[0].typeCounts.length; t++){
                            var interactionTypeObj = $scope.getInteractionTypeObj(response.Data.interactionTypes[0].typeCounts[t],response.Data.interactionTypes[0].maxCount);
                            if(interactionTypeObj != null){
                                existingTypes.push(interactionTypeObj.type);
                                $scope.interactionsByType.push(interactionTypeObj);
                            }
                        }
                        var mobileCountStatus = 0;
                        var mobileCountStatusLists = ['call','sms','email'];
                        for(var u=0; u<allTypes.length; u++){
                            if(existingTypes.indexOf(allTypes[u]) == -1){
                                var newObj = $scope.getInteractionTypeObj({_id:allTypes[u],count:0},response.Data.interactionTypes[0].maxCount)
                                if(newObj != null){
                                    if(mobileCountStatusLists.indexOf(allTypes[u]) != -1){
                                        mobileCountStatus ++;
                                    }
                                    $scope.interactionsByType.push(newObj);
                                }
                            }
                        }

                        if(mobileCountStatus > 0){
                            $scope.showDownloadMobileApp = true;
                        }else $scope.showDownloadMobileApp = false;
                        $scope.showSocialSetup = response.Data.showSocialSetup;

                        $scope.interactionsByType.sort(function (o1, o2) {
                            return o1.priority < o2.priority ? -1 : o1.priority > o2.priority ? 1 : 0;
                        });
                        $scope.interactionsByType = {
                            a:$scope.interactionsByType[0],
                            b:$scope.interactionsByType[1],
                            c:$scope.interactionsByType[2],
                            d:$scope.interactionsByType[3],
                            e:$scope.interactionsByType[4],
                            f:$scope.interactionsByType[5],
                            g:$scope.interactionsByType[6]
                        };

                    }
                    else{
                        $scope.interaction_initiations_total = 0;
                        $scope.interactionsByType = {"a":{"priority":0,"value":"height:0%","type":"meeting","count":0,"title":"0 interactions"},"b":{"priority":1,"value":"height:0%","type":"call","count":0,"title":"0 interactions"},"c":{"priority":2,"value":"height:0%","type":"sms","count":0,"title":"0 interactions"},"d":{"priority":3,"value":"height:0%","type":"email","count":0,"title":"0 interactions"},"e":{"priority":4,"value":"height:0%","type":"facebook","count":0,"title":"0 interactions"},"f":{"priority":5,"value":"height:0%","type":"twitter","count":0,"title":"0 interactions"},"g":{"priority":6,"value":"height:0%","type":"linkedin","count":0,"title":"0 interactions"}}
                    }
                }
            });
    };

    $scope.drawDonut = function(a,b){
        var canvas  = document.getElementById("donut-chart");
        var chart = canvas.getContext("2d");

        function drawdountChart(canvas)
        {

            this.x , this.y , this.radius , this.lineWidth , this.strockStyle , this.from , this.to = null;
            this.set = function( x, y, radius, from, to, lineWidth, strockStyle)
            {
                this.x = x;
                this.y = y;
                this.radius = radius;
                this.from=from;
                this.to= to;
                this.lineWidth = lineWidth;
                this.strockStyle = strockStyle;
            }

            this.draw = function(data)
            {
                canvas.beginPath();
                canvas.lineWidth = this.lineWidth;
                canvas.strokeStyle = this.strockStyle;
                canvas.arc(this.x , this.y , this.radius , this.from , this.to);
                canvas.stroke();
                var numberOfParts = data.numberOfParts;
                var parts = data.parts.pt;
                var colors = data.colors.cs;
                var df = 0;
                for(var i = 0; i<numberOfParts; i++)
                {
                    canvas.beginPath();
                    canvas.strokeStyle = colors[i];
                    canvas.arc(this.x, this.y, this.radius, df, df + (Math.PI * 2) * (parts[i] / 100));
                    canvas.stroke();
                    df += (Math.PI * 2) * (parts[i] / 100);
                }
            }
        }
        var data =
        {
            numberOfParts: 2,
            parts:{"pt": [a , b ]},//percentage of each parts
            colors:{"cs": ["#f86b4f", "rgba(248, 107, 79, 0.65)"]}//color of each part
        };

        var drawDount = new drawdountChart(chart);
        drawDount.set(100, 100, 65, 0, Math.PI*2, 15, "#fff");
        drawDount.draw(data);
    }

});

reletasApp.controller("top_trending_post", function ($scope, $http, meetingObjService) {
    $scope.setup_social_accounts = false;
    $scope.top_trending_post_show = false;

    meetingObjService.topTrendingSocialPost = function(userId,emailId){
        $scope.p_name = meetingObjService.getSelectedParticipantName();

        $http.get('/today/details/social/top/trending?id='+userId)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.latestFeedMessage = '';
                    var image = '';
                    if(response.Data.source == 'linkedin'){
                        image = "/images/linkedin_20x20.png"
                    }
                    else if(response.Data.source == 'twitter'){
                        $scope.socialTwitter = true
                    }
                    else if(response.Data.source == 'facebook'){
                        image = "/images/facebook_icon_24px.png"
                    }
                    $scope.latestFeedImage = image;
                    $scope.latestFeedImageShow = true;
                    $scope.top_trending_post_show = true;
                    $scope.latestFeedSource = response.Data.source;
                    $scope.latestFeedContent = response.Data.content
                    $scope.latestFeedDate = moment(response.Data.date).format("MMM DD YYYY")
                }
                else{
                    $scope.top_trending_post_show = false;
                    $scope.latestFeedImageShow = false;
                    $scope.latestFeedImage = "";
                    $scope.latestFeedSource = "";
                    $scope.latestFeedContent = ""
                    $scope.latestFeedDate = ""
                   // $scope.setup_social_accounts = response.Date.setup_social_accounts
                    $scope.latestFeedMessage = response.Message
                }
            });
    }
});

var start = 1;

var itemsScroll = 6;
var itemsToShow = 6;

if(window.innerWidth < 480){
    console.log("Hello")
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 1
    itemsScroll = 1
}else if(window.innerWidth < 900){
    console.log("No hello")
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 3
    itemsScroll = 3
}

var end = itemsToShow;

reletasApp.controller("commonConnections", function ($scope, $http, meetingObjService) {
    $scope.cConnections = [];
    $scope.cConnectionsIds = [];
    $scope.currentLength = 0;
    $scope.isFirstTime = true;
    $scope.isConnectionOpen = false;
    $scope.start = start;
    $scope.end = end;
    $scope.isAllConnectionsLoaded = false;

    meetingObjService.commonConnections = function(userId,emailId,url){

        $scope.cConnections = [];
        $scope.cConnectionsIds = [];
        $scope.currentLength = 0;
        $scope.isFirstTime = true;
        $scope.isConnectionOpen = false;
        $scope.start = start;
        $scope.end = end;
        $scope.isAllConnectionsLoaded = false;
        $scope.userId = userId;
        commonConnectionsMessage(true)
        getCommonConnections($scope, $http, url, true);
    };

    $scope.NextConn = function(){
        var statusNext =  $scope.statusNext;
        var statusPrev =  $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.end < total){
            var itemsToStayPrev = $scope.start+itemsScroll;
            var itemsToStayNext = $scope.end+itemsScroll;
            for(var i=0; i<$scope.cConnectionsIds.length; i++){

                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    addHide($scope.cConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end+itemsScroll;
            $scope.start = $scope.start+itemsScroll;

            $scope.statusNext = statusNext+itemsScroll;
            $scope.statusPrev = statusNext;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
        }
        $scope.showHideArrows();
        if($scope.fetchCommonConnectionsNext() && $scope.isAllConnectionsLoaded == false && $scope.isConnectionOpen == false){
            $scope.isConnectionOpen = true;
            var url = '/contacts/filter/common/web?id='+$scope.userId+'&skip='+$scope.currentLength+'&limit=12';
            getCommonConnections($scope, $http, url, false);
        }
    };

    $scope.fetchCommonConnectionsNext = function(){
        if($scope.end >= $scope.cConnectionsIds.length - 12){
            return true;
        }else return false;
    };

    $scope.PrevConn = function(){

        var statusPrev = $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.start >1){
            var itemsToStayPrev = $scope.start-itemsScroll;
            var itemsToStayNext = $scope.end-itemsScroll;

            for(var i=0; i<$scope.cConnectionsIds.length; i++){

                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    addHide($scope.cConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end-itemsScroll;
            $scope.start = $scope.start-itemsScroll;

            $scope.statusNext = statusPrev;
            $scope.statusPrev = statusPrev-itemsScroll+1;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
            $scope.showHideArrows()
        }
    };

    $scope.showHideArrows = function(){
        if($scope.grandTotal >0){
            if($scope.start <=1 ){
                addHide("als-prev",true)
            }
            else addHide("als-prev",false)

            if($scope.end >= $scope.grandTotal){
                addHide("als-next",true)
            }
            else addHide("als-next",false)
        }
        else{
            addHide("als-prev",true)
            addHide("als-next",true)
        }
    };

    $scope.connectionClick = function(url){
        if(checkRequired(url)){
            trackMixpanel("clicked on common connection",url,function(){
                window.location.replace(url);
            })
        }
    }

    $scope.interactionsContext = function(emailId,mobileNumber){
        if(mobileNumber && mobileNumber !='undefined'){
            window.location.replace('/contact/selected?context='+emailId+'&mobileNumber='+mobileNumber.replace(/[^a-zA-Z0-9]/g,''))
        } else {
            window.location.replace('/contact/selected?context='+emailId)
        }
    }

    //XLS Contacts Upload

    $scope.uploadContacts = function(){
        $("#selectedFile").trigger("click");
    };

    $scope.upload = function(file){
        var formData = new FormData();
        formData.append('contacts', file, file.name);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/contacts/upload/file', true);
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            var response = JSON.parse(xhr.response)
            if(response.SuccessCode){
                $("#uploadContactsAlert").addClass("hide");
                toastr.success(response.Message)
            }
            else{
                toastr.error(response.Message)
            }
        };
        // Send the Data.
        xhr.send(formData);
    };
    $("#selectedFile").on("change",function(){
        if($(this)[0] && $(this)[0].files && $(this)[0].files.length > 0){
            $scope.upload($(this)[0].files[0])
        }
        else toastr.error("Please select file to upload")
    });

});

function addHide(elementId,addHide){
    addHide ? $("#"+elementId).addClass('hide') : $("#"+elementId).removeClass('hide')
}

function getCommonConnections($scope, $http, url, isHides){
    
    $http.get(url)
        .success(function (response) {
            
            $scope.isConnectionOpen = false;
            if(response.SuccessCode){
                $scope.grandTotal = response.Data.total;
                if(response.Data.contacts && response.Data.contacts.length > 0){

                    var html = '';
                    hidePrevNext('als-prev','als-next',true);
                    $scope.currentLength += response.Data.returned;

                    for(var i=0; i<response.Data.contacts.length; i++){
                        var li = '';
                        var obj;
                        if(checkRequired(response.Data.contacts[i].personId) && checkRequired(response.Data.contacts[i].personName)){

                            var name = getTextLength(response.Data.contacts[i].personName,13);
                            var image = '/getImage/'+response.Data.contacts[i].personId;

                            obj = {
                                fullName:response.Data.contacts[i].personName,
                                name:name,
                                image:image,
                                url:url,
                                cursor:'cursor:pointer',
                                idName:'com_con_item_'+response.Data.contacts[i]._id,
                                className:'hide'
                            };
                            if(isHides && i<itemsToShow){
                                obj.className = ''
                            }
                            obj.emailId = response.Data.contacts[i].personEmailId;
                            obj.mobileNumber = response.Data.contacts[i].mobileNumber?response.Data.contacts[i].mobileNumber.replace(/[^a-zA-Z0-9]/g,''):null

                            $scope.cConnections.push(obj);
                            $scope.cConnectionsIds.push('com_con_item_'+response.Data.contacts[i]._id);
                        }
                        else{
                            var contactImageLink = response.Data.contacts[i].contactImageLink?encodeURIComponent(response.Data.contacts[i].contactImageLink):null
                            obj = {
                                fullName:response.Data.contacts[i].personName,
                                name:getTextLength(response.Data.contacts[i].personName,13),
                                image:'/getContactImage/'+response.Data.contacts[i].personEmailId+'/'+contactImageLink,
                                // noPicFlag:true,
                                cursor:'cursor:pointer',
                                url:null,
                                idName:'com_con_item_'+response.Data.contacts[i]._id,
                                className:'hide'
                            };
                            if(isHides && i<itemsToShow){
                                obj.className = ''
                            }
                            obj.emailId = response.Data.contacts[i].personEmailId;
                            obj.mobileNumber = response.Data.contacts[i].mobileNumber?response.Data.contacts[i].mobileNumber.replace(/[^a-zA-Z0-9]/g,''):null
                            obj.nameNoImg = response.Data.contacts[i].personName.substr(0,2).toUpperCase();

                            $scope.cConnections.push(obj);
                            $scope.cConnectionsIds.push('com_con_item_'+response.Data.contacts[i]._id);
                        }
                    }
                    if(isHides){
                        $scope.statusPrev = 1;
                        $scope.statusNext = itemsScroll;
                    }
                    $scope.showHideArrows()
                }
                else if(isHides){
                    hidePrevNext('als-prev','als-next',true)
                    commonConnectionsMessage(false)
                    $scope.commonConnectionsNotExist = response.Message;
                }else $scope.isAllConnectionsLoaded = true;
            }
            else if(isHides){
                hidePrevNext('als-prev','als-next',true)
                commonConnectionsMessage(false)
                $scope.commonConnectionsNotExist = response.Message;
            }else $scope.isAllConnectionsLoaded = true;
        })
}

reletasApp.controller("commonConnections_company", function ($scope, $http, meetingObjService) {
    $scope.cConnections = [];
    $scope.cConnectionsIds = [];
    $scope.currentLength = 0;
    $scope.isFirstTime = true;
    $scope.isConnectionOpen = false;
    $scope.start = start;
    $scope.end = end;
    $scope.isAllConnectionsLoaded = false;

    meetingObjService.bindPName = function(p_name){
        $scope.p_name = p_name;
    }

    meetingObjService.commonConnections_company = function(userId,emailId,url){

        $scope.cConnections = [];
        $scope.cConnectionsIds = [];
        $scope.currentLength = 0;
        $scope.isFirstTime = true;
        $scope.isConnectionOpen = false;
        $scope.start = start;
        $scope.end = end;
        $scope.isAllConnectionsLoaded = false;
        $scope.userId = userId;
        $scope.emailId = emailId;
        $scope.isCorporateUser = meetingObjService.isCorporateUser;

        getCommonConnections_company($scope, $http, url, true);
    };

    $scope.NextConn = function(){
        var statusNext =  $scope.statusNext;
        var statusPrev =  $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.end < total){
            var itemsToStayPrev = $scope.start+itemsScroll;
            var itemsToStayNext = $scope.end+itemsScroll;
            for(var i=0; i<$scope.cConnectionsIds.length; i++){

                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    addHide($scope.cConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end+itemsScroll;
            $scope.start = $scope.start+itemsScroll;

            $scope.statusNext = statusNext+itemsScroll;
            $scope.statusPrev = statusNext;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
        }
        $scope.showHideArrows();
        if($scope.fetchCommonConnectionsNext() && $scope.isAllConnectionsLoaded == false && $scope.isConnectionOpen == false){
            $scope.isConnectionOpen = true;
            var url ='/contacts/filter/common/companyName/web?companyName='+meetingObjService.l_user.companyName+'&emailId='+meetingObjService.l_user.emailId+'&fetchWith='+$scope.emailId+'&skip='+$scope.currentLength+'&limit=12'

            getCommonConnections_company($scope, $http, url, false);
        }
    };

    $scope.fetchCommonConnectionsNext = function(){
        if($scope.end >= $scope.cConnectionsIds.length - 12){
            return true;
        }else return false;
    };

    $scope.PrevConn = function(){

        var statusPrev = $scope.statusPrev;
        var total = $scope.cConnectionsIds.length;

        if($scope.start >1){
            var itemsToStayPrev = $scope.start-itemsScroll;
            var itemsToStayNext = $scope.end-itemsScroll;

            for(var i=0; i<$scope.cConnectionsIds.length; i++){

                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    addHide($scope.cConnectionsIds[i],false);
                }
                else{
                    addHide($scope.cConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end-itemsScroll;
            $scope.start = $scope.start-itemsScroll;

            $scope.statusNext = statusPrev;
            $scope.statusPrev = statusPrev-itemsScroll+1;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
            $scope.showHideArrows()
        }
    };

    $scope.showHideArrows = function(){
        if($scope.grandTotal >0){
            if($scope.start <=1 ){
                addHide("als-prev_c",true)
            }
            else addHide("als-prev_c",false)

            if($scope.end >= $scope.grandTotal){
                addHide("als-next_c",true)
            }
            else addHide("als-next_c",false)
        }
        else{
            addHide("als-prev_c",true)
            addHide("als-next_c",true)
        }
    };

    $scope.connectionClick = function(url){
        if(checkRequired(url)){
            trackMixpanel("clicked on company common connection ",url,function(){
                window.location.replace(url);
            })
        }
    }

    $scope.interactionsContext = function(emailId,mobileNumber){
        if(mobileNumber && mobileNumber !='undefined'){
            window.location.replace('/contact/selected?context='+emailId+'&mobileNumber='+mobileNumber.replace(/[^a-zA-Z0-9]/g,''))
        } else {
            window.location.replace('/contact/selected?context='+emailId)
        }
    }

});

function getCommonConnections_company($scope, $http, url, isHides){

    $http.get(url)
        .success(function (response) {

            $scope.isConnectionOpen = false;
            if(response.SuccessCode){
                $scope.grandTotal = response.Data.total;
                if(response.Data.contacts && response.Data.contacts.length > 0){
                    var html = '';
                    hidePrevNext('als-prev_c','als-next_c',true)
                    $scope.no_connections_by_company = false
                    $scope.currentLength += response.Data.returned;
                    for(var i=0; i<response.Data.contacts.length; i++){
                        var li = '';
                        var obj;
                        if(checkRequired(response.Data.contacts[i].personId) && checkRequired(response.Data.contacts[i].personId.firstName)){
                            var name = getTextLength(response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName,13);
                            var image = '/getImage/'+response.Data.contacts[i].personId._id;
                            var url = response.Data.contacts[i].personId.publicProfileUrl;
                            var emailId = response.Data.contacts[i]._id.emailId;
                            if(url.charAt(0) != 'h'){
                                url = '/'+url;
                            }
                            obj = {
                                fullName:response.Data.contacts[i].personId.firstName+' '+response.Data.contacts[i].personId.lastName,
                                name:name,
                                image:image,
                                url:url,
                                emailId:emailId,
                                cursor:'cursor:pointer',
                                idName:'com_con_item_c_'+response.Data.contacts[i]._id,
                                className:'hide',
                            };
                            if(isHides && i<itemsToShow){
                                obj.className = ''
                            }
                            obj.emailId = response.Data.contacts[i]._id.emailId;
                            $scope.cConnections.push(obj);
                            $scope.cConnectionsIds.push('com_con_item_c_'+response.Data.contacts[i]._id);
                        }
                        else{
                            obj = {
                                fullName:response.Data.contacts[i].personName,
                                name:getTextLength(response.Data.contacts[i].personName,13),
                                image:null,
                                noPicFlag:true,
                                cursor:'cursor:default',
                                url:null,
                                emailId:response.Data.contacts[i]._id.emailId,
                                idName:'com_con_item_c_'+response.Data.contacts[i]._id,
                                className:'hide'
                            };
                            if(isHides && i<itemsToShow){
                                obj.className = ''
                            }
                            obj.emailId = response.Data.contacts[i]._id.emailId;
                            obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                            $scope.cConnections.push(obj);
                            $scope.cConnectionsIds.push('com_con_item_c_'+response.Data.contacts[i]._id);
                        }
                    }
                    if(isHides){
                        $scope.statusPrev = 1;
                        $scope.statusNext = itemsScroll;
                    }
                    $scope.showHideArrows()
                }
                else if(isHides){
                    hidePrevNext('als-prev_c','als-next_c',true)
                    $scope.no_connections_by_company = true
                }else $scope.isAllConnectionsLoaded = true;
            }
            else if(isHides){
                $scope.no_connections_by_company = true
                hidePrevNext('als-prev_c','als-next_c',true)

            }else $scope.isAllConnectionsLoaded = true;
        })
}

function commonConnectionsMessage(hide){
    if(hide){
        $("#commonConnectionsName_msg").addClass('hide')
        $("#demo3").removeClass('hide')
    }
    else{
        $("#commonConnectionsName_msg").removeClass('hide')
        $("#demo3").addClass('hide')
    }
}

function hidePrevNext(idNameP,idNameN,hide){
    if(hide){
        $("#"+idNameP).addClass('hide')
        $("#"+idNameN).addClass('hide')
    }
    else{
        $("#"+idNameP).removeClass('hide')
        $("#"+idNameN).removeClass('hide')
    }
}

reletasApp.controller("interaction_timeline", function ($scope, $http, meetingObjService){
    $scope.send_email_container = false;
    $scope.show_show_more = false;
    $scope.compose_email_doc_tracking = true;
    $scope.compose_email_track_viewed = true;
    $scope.compose_email_remaind = true;

    function isrName(callback){
        if(typeof meetingObjService.p_name == 'string'){
            callback();
        }
        else{
            setTimeout(function(){
                return isrName(callback);
            },300)
        }
    }
    $scope.p_name = meetingObjService.getSelectedParticipantName();
    $scope.rName = meetingObjService.getSelectedParticipantName();
    if(!checkRequired($scope.p_name)){
        isrName(function(){
            $scope.p_name = meetingObjService.p_name;
        });
    }

    $scope.timelineTimeline = function(userId,emailId){
        $scope.emailId=emailId;
        $scope.userId = userId;
        $scope.timelineItems = [];
        $http.get('/interactions/timeline/'+emailId+'/interactions?timezone='+$scope.timezone)
            .success(function(response){
                if(response.SuccessCode){
                    if(response.Data.length > 0){

                        var interactions = response.Data[0].interactions;
                        $scope.show_show_more = interactions.length >= 5;

                        //$scope.rName = response.Data[0].firstName[0];
                        if(checkRequired($scope.rName)){

                        }
                        else if(checkRequired(response.Data[0].firstName) && response.Data[0].firstName.length > 0){
                            response.Data[0].firstName.forEach(function(name){
                                if(checkRequired(name)){
                                    $scope.rName = name;
                                }
                            })

                        }
                        else $scope.rName = emailId;

                        if(checkRequired(response.Data[0].publicProfileUrl) && response.Data[0].publicProfileUrl.length > 0){
                            $scope.scheduleMeeting = '/'+response.Data[0].publicProfileUrl[0];
                        }
                        if(interactions.length > 0){
                            $scope.lastInteraction = moment(interactions[0].interactionDate).tz($scope.timezone).format("DD MMM YYYY")
                           // $scope.scheduleMeeting = '';
                            for(var i=0; i<interactions.length; i++){
                                var iDate = moment(interactions[i].interactionDate).tz($scope.timezone);
                                var now = moment().tz($scope.timezone);
                                var diff = now.diff(iDate);
                                diff = moment.duration(diff).asMinutes();
                                var interactionIcon = $scope.getInteractionTypeImage(interactions[i].interactionType,$scope.firstName,$scope.rName,interactions[i].title,interactions[i].action,interactions[i]._id,interactions[i].refId,interactions[i].emailId,interactions[i].ignore,interactions[i],response.timezone,response.remaindDays);

                                var colorClass = "";
                                if(interactionIcon.colorRed){
                                    colorClass = 'color:#F86A52'
                                }
                                else if(interactionIcon.colorBlue){
                                    colorClass = 'color:#2d3e48'
                                }

                                var viewedOn,isEmailRead;

                                if(interactions[i].action == 'receiver' && interactions[i].interactionType =='email' && interactions[i].trackInfo && interactions[i].trackInfo.lastOpenedOn && interactions[i].trackInfo.lastOpenedOn !== null){
                                    isEmailRead = true;
                                    viewedOn = moment(interactions[i].trackInfo.lastOpenedOn).format("DD MMM YYYY, h:mm a");
                                } else {
                                    isEmailRead = false;
                                    viewedOn = '';
                                }

                                var interactionInitClass = "incoming";

                                if(interactions[i].action == "receiver"){
                                    interactionInitClass = "outgoing";
                                }

                                var obj = {
                                    duration:moment.duration(diff,"minutes").humanize()+' ago',
                                    dateText:iDate.format("DD MMM YYYY"),
                                    interactionIcon:interactionIcon.image,
                                    actionType:interactionIcon.actionType,
                                    iconClass:interactionIcon.iconClass,
                                    iconTitle:interactionIcon.iconTitle,
                                    title: interactionIcon.title ? escapeHtml(interactionIcon.title.replace(/<br\s*[\/]?>/gi, "\n")) : '',
                                    but:interactionIcon.but,
                                    action:interactionIcon.action,
                                    refId:interactions[i].refId,
                                    showButton:interactionIcon.showButton ? 'display:block' : 'display:none',
                                    className: i==0 ? 'document-timeline' :'',
                                    viewitemId:interactions[i]._id,
                                    colorClass:colorClass,
                                    emailRead:isEmailRead,
                                    //localtimezone: moment().tz(localtimezone.name()).zoneAbbr(),
                                    viewedOn:viewedOn,
                                    interactionInitClass:interactionInitClass,
                                    callDuration:getDurationFormat(interactions[i]),
                                    dataObj:{
                                       // subject:interactions[i].title,
                                        _id:interactions[i]._id,
                                        body:interactions[i].description,
                                        bindHtmlKey:'content_'+interactions[i]._id,
                                        interaction:interactions[i],
                                        subject:interactionIcon.subject,
                                        subjectNormal:interactionIcon.subjectNormal,
                                        composeMailBox:interactionIcon.composeMailBox,
                                        emailBodyBox:interactionIcon.emailBodyBox,
                                        compose_email_track_viewed:interactionIcon.compose_email_track_viewed,
                                        compose_email_remaind:interactionIcon.compose_email_remaind,
                                        compose_email_doc_tracking:interactionIcon.compose_email_doc_tracking,
                                        compose_email_body:interactionIcon.compose_email_body,
                                        compose_email_subject:interactionIcon.subject,
                                        bodyContent:interactionIcon.bodyContent,
                                        itemPointer:interactionIcon.itemPointer,
                                        updateOpened:interactionIcon.updateOpened,
                                        updateReplied:interactionIcon.updateReplied,
                                        emailId:interactionIcon.emailId,
                                        userId:userId,
                                        trackId:interactionIcon.trackId
                                    }
                                };

                                $scope.timelineItems.push(obj);
                            }
                        }
                    }
                    else{
                        $scope.show_show_more = false;
                        $scope.lastInteraction = 'None';
                    }
                }
                else $scope.show_show_more = false
            })
    };
    meetingObjService.timelineTimeline_again = function(){
        meetingObjService.timelineTimeline($scope.cuserId,$scope.cEmailId)
    }
    meetingObjService.timelineTimeline = function(userId,emailId){
        $scope.interactionUrl = '/interactions?context='+emailId;
        $scope.p_name = meetingObjService.getSelectedParticipantName();
        $scope.rName = meetingObjService.getSelectedParticipantName();
        if(!checkRequired($scope.p_name)){
            isrName(function(){
                $scope.p_name = meetingObjService.p_name;
            });
        }
        $scope.cEmailId = emailId
        $scope.cuserId = userId
        $scope.timezone = 'UTC';
        if(meetingObjService.l_user.timezone && checkRequired(meetingObjService.l_user.timezone.name)){
            $scope.timezone = meetingObjService.l_user.timezone.name;
        }
        else $scope.timezone = timezone || 'UTC';
        $scope.firstName = meetingObjService.l_user.firstName;
        $scope.lastName = meetingObjService.l_user.lastName;
        $scope.timelineTimeline(userId,emailId);
    };

    $scope.timelineSocial = function(userId,emailId){
        $scope.socialTimeline = [];
        $http.get('/interactions/timeline/'+emailId+'/social?timezone='+$scope.timezone)
            .success(function(response){
                if(response.SuccessCode){
                    //$scope.show_show_more = response.Data.length >= 5;
                    $scope.socialTimeline = $scope.parseInteractions(response);
                }
                else $scope.show_show_more = false
            })
    };

    $scope.timelineFiles = function(userId,emailId){
        $scope.docsTimeline = [];
        $http.get('/interactions/timeline/'+emailId+'/files?timezone='+$scope.timezone)
            .success(function(response){
                if(response.SuccessCode){
                    //$scope.show_show_more = response.Data.length >= 5;
                    $scope.docsTimeline = $scope.parseInteractions(response);
                }
                else $scope.show_show_more = false
            })
    };

    $scope.timelineTasks = function(userId,emailId){
        $scope.tasksTimeline = [];
        $http.get('/interactions/timeline/'+emailId+'/tasks?timezone='+$scope.timezone)
            .success(function(response){
                if(response.SuccessCode){
                    //$scope.show_show_more = response.Data.length >= 5;
                    $scope.tasksTimeline = [];
                    var interactions = response.Data[0].interactions;
                    $scope.show_show_more = interactions.length >= 5;
                    if(checkRequired(response.Data[0].firstName) && response.Data[0].firstName.length > 0){
                        $scope.rName = response.Data[0].firstName[0];
                    }

                    if(interactions.length > 0){
                        for(var i=0; i<interactions.length; i++){
                            var dueDate = moment(interactions[i].interactionDate).tz($scope.timezone);
                            var title = checkRequired(interactions[i].title) ? getTextLength(interactions[i].title,40) : ""
                            var description = checkRequired(interactions[i].description) ? getTextLength(interactions[i].description,40) : ""

                            var obj = {
                                showCheckBox:interactions[i].refId.status == 'complete' || interactions[i].action == 'sender',
                                status:interactions[i].refId.status == 'complete',
                                owner:interactions[i].action == 'sender' ? $scope.rName : $scope.firstName+' '+$scope.lastName,
                                ownerPic:interactions[i].action == 'sender' ? '/getImage/'+$scope.userId : '/getImage/'+meetingObjService.l_user._id,
                                updateTaskStatus:interactions[i].action == 'sender',
                                titleFull:interactions[i].title || '',
                                descriptionFull:interactions[i].description || '',
                                title:title,
                                description:description,
                                taskId:interactions[i].refId._id,
                                dueDate:dueDate.format("Do MMM"),
                                textColor:moment().isAfter(dueDate) ? 'text-color-red' : '',
                                viewitemId:interactions[i]._id
                            };

                            $scope.tasksTimeline.push(obj);
                        }
                    }
                }
                else $scope.show_show_more = false
            })
    };

    meetingObjService.timelineTasks = $scope.timelineTasks;

    $scope.parseInteractions = function(response){
        var arr = [];
        var interactions = response.Data[0].interactions;
        $scope.show_show_more = interactions.length >= 5;
        if(checkRequired(response.Data[0].firstName) && response.Data[0].firstName.length > 0){
            $scope.rName = response.Data[0].firstName[0];
        }

        if(interactions.length > 0){

            for(var i=0; i<interactions.length; i++){
                var iDate = moment(interactions[i].interactionDate).tz($scope.timezone);
                var now = moment().tz($scope.timezone);
                var diff = now.diff(iDate);
                diff = moment.duration(diff).asMinutes();
                var interactionIcon = $scope.getInteractionTypeImage(interactions[i].interactionType,$scope.firstName,$scope.rName,interactions[i].title,interactions[i].action,interactions[i]._id,interactions[i].refId,interactions[i].emailId,interactions[i].ignore,interactions[i]);

                var obj = {
                    duration:moment.duration(diff,"minutes").humanize()+' ago',
                    dateText:iDate.format("DD MMM YYYY"),
                    interactionIcon:interactionIcon.image,
                    actionType:interactionIcon.actionType,
                    iconClass:interactionIcon.iconClass,
                    iconTitle:interactionIcon.iconTitle,
                    title:interactionIcon.title,
                    but:interactionIcon.but,
                    action:interactionIcon.action,
                    refId:interactions[i].refId,
                    showButton:interactionIcon.showButton ? 'display:block' : 'display:none',
                    className: i==0 ? 'document-timeline' :'',
                    viewitemId:interactions[i]._id,
                    dataObj:{
                        subject:interactions[i].title,
                        body:interactions[i].description,
                        bindHtmlKey:'c_'+interactions[i]._id,
                        emailId:interactions[i].emailId
                    }
                };
                arr.push(obj);
            }
            return arr;
        }
    };

    $scope.getInteractionTypeImage = function(type,name,rName,title,action,_id,refId,emailId,ignore,interaction,timezone,remaindDays){
        timezone = timezone || $scope.timezone;
        remaindDays = remaindDays || 7;
        switch(type){
            case 'google-meeting':
            case 'meeting':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                return {
                    image:"/images/icon_calendar2_grey.png",
                    iconClass:"fa-calendar-check-o green-color",
                    iconTitle:"Meeting",
                    title:title,
                    showButton:false,
                    actionType:actionType
                };
                break;
            case 'document-share':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                return {
                    image:"/images/document_icon_new.png",
                    iconClass:"fa-file-pdf-o",
                    iconTitle:"Document",
                    title:action == 'sender' ? rName+' shared '+title+' with you' : 'You shared '+title+' with '+rName,
                    but:action == 'sender' ? 'View':'View Stats',
                    action:action == 'sender' ? 'viewDoc':'viewStats',
                    showButton:true,
                    actionType:actionType
                };
                break;
            case 'message':
            case 'meeting-comment':
            case 'email':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }

                var colorRed = false;
                var colorBlue = false;
                var titleN = null;
                var updateOpened = false;
                var updateReplied = false;
                var showButton = true;
                if(interaction.action == 'sender'){
                    if(checkRequired(interaction.trackInfo)){
                        if(interaction.trackInfo.trackOpen){
                            updateOpened = true;
                        }
                        if(interaction.trackInfo.trackResponse){
                            updateReplied = true;
                        }
                        if(interaction.trackInfo.gotResponse){
                            showButton = false;
                        }
                    }
                }
                if(interaction.action == 'receiver' && !interaction.ignore && interaction.interactionType == 'email' && interaction.source == 'relatas'){

                    if(checkRequired(interaction.trackInfo)){
                        if(interaction.trackInfo.trackOpen){
                            // track info enabled
                            if(interaction.trackInfo.isRed){
                                // opened or no action
                                colorBlue = true;
                            }
                            else{
                                // not opened or no action
                                colorRed = true;
                            }
                        }

                        if(interaction.trackInfo.trackResponse){
                            // track response enabled
                            var interactionDate = moment(interaction.interactionDate).tz(timezone);
                            interactionDate.date(interactionDate.date() + remaindDays);
                            var now = moment().tz(timezone);

                            if(now.isAfter(interactionDate) || now.format("DD-MM-YYYY") == interactionDate.format("DD-MM-YYYY")){
                                if(interaction.trackInfo.gotResponse){
                                    // got response
                                    colorBlue = false;
                                    colorRed = false;
                                }
                                else{
                                    // not response
                                    titleN = "Notification remainder, Subject: "+title;

                                    if(!colorBlue){
                                        colorRed = true;
                                        colorBlue = false;
                                    }
                                }
                            }
                            else{
                                if(interaction.trackInfo.gotResponse){
                                    // got response
                                    colorBlue = false;
                                    colorRed = false;
                                }
                            }
                        }
                    }
                }
                var subject = "Re: "+interaction.title;
                var body = "";
                if(checkRequired(interaction.description)){
                    body = "\n\n"+interaction.description
                }

                if(interaction.title && interaction.title.substring(0,2).toLowerCase() == 're'){
                    subject = interaction.title
                }
                if(checkRequired(interaction.trackInfo)){
                    if(interaction.trackInfo.gotResponse){
                        showButton = false;
                    }
                }
                return {
                    image:"/images/interaction/icon_gmail.png",
                    iconClass:"fa-envelope green-color",
                    iconTitle:"Email",
                    title:titleN != null? titleN : action == 'sender' ? 'Mail received from '+rName+'. <br>Subject: '+title : 'Mail sent To '+rName+'. <br>Subject: '+title,
                    but:'Reply',
                    action:"viewEmail",
                    showButton:showButton,
                    colorRed:colorRed,
                    colorBlue:colorBlue,
                    subject:subject,
                    subjectNormal:interaction.title,
                    composeMailBox:false,
                    emailBodyBox:false,
                    compose_email_track_viewed:true,
                    compose_email_remaind:true,
                    compose_email_doc_tracking:false,
                    compose_email_body:"",
                    compose_email_subject:"",
                    bodyContent:"",
                    itemPointer:'cursor:pointer',
                    updateOpened:updateOpened,
                    emailId:null,
                    userId:interaction.userId,
                    trackId:interaction.trackId,
                    updateReplied:updateReplied,
                    actionType:actionType
                };
                break;
            case 'sms':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                return {
                    image:"/images/interaction/icon_rel_mssg.png",
                    iconClass:"fa-reorder green-color",
                    iconTitle:"SMS",
                    title:action == 'sender' ? 'Received message from '+rName+'. <br>Message: '+title : 'Sent message to '+rName+'. <br>Message: '+title,
                    showButton:false,
                    actionType:actionType
                };
                break;
            case 'call':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                return {
                    image:"/images/mailer/icon_phone.png",
                    iconClass:"fa-phone green-color",
                    iconTitle:"Call",
                    title:action == 'sender' ? 'Received call from '+rName : 'You called '+rName,
                    showButton:false,
                    actionType:actionType
                };
                break;
            case 'task':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }

                return {
                    image:"/images/todo1.jpg",
                    iconClass:"fa-check-square-o",
                    iconTitle:"Tasks",
                    title:action == 'sender' ? 'Task assigned to you by '+rName+'. <br>Task: '+title : 'You assigned a task to '+rName+'. <br>Task: '+title,
                    but:'View Task',
                    action:'viewTask',
                    showButton:true,
                    interaction:interaction,
                    actionType:actionType
                };
                break;
            case 'twitter':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }

                return {
                    image:"/images/twitter_20.png",
                    iconClass:"fa-twitter-square twitter-color",
                    iconTitle:"Twitter",
                    title:title,
                    showButton:false,
                    actionType:actionType
                };
                break;
            case 'facebook':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                return {
                    image:"/images/facebook_icon_24px.png",
                    iconClass:"fa-facebook-official fb-color",
                    iconTitle:"Facebook",
                    title:title,
                    showButton:false,
                    actionType:actionType
                };
                break;
            case 'linkedin':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                return {
                    image:"/images/linkedin_20x20.png",
                    iconClass:"fa-linkedin-square linkedin-color",
                    iconTitle:"Linkedin",
                    title:title,
                    showButton:false,
                    actionType:actionType
                };
                break;
            case 'calendar-password':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                var calendarPasswordApprove = false;
                var calPassIgnoreBut = false;
                if(action == 'sender' && !ignore){
                    calendarPasswordApprove = true;
                    calPassIgnoreBut = true;
                }

                return {
                    iconClass:"fa-calendar-check-o green-color",
                    iconTitle:"Calendar Password Request",
                    followUpBut:false,
                    calendarPasswordApprove:calendarPasswordApprove,
                    ignoreBut:calPassIgnoreBut,
                    isCalPassReq:true,
                    _id:_id,
                    reqId:refId,
                    name:name,
                    emailId:emailId,
                    actionType:actionType
                };
                break;
            default :return {image:'',title:'', showButton:false};
        }
    };

    $scope.getTaskDetails = function(interaction){
        var obj = {
            title:interaction.title,
            description:interaction.description,
            dueDate:interaction.interactionDate
        };

     //   if(interaction.action)
    };

    $scope.openSendEmailContainer = function(){
        $scope.send_email_container = !$scope.send_email_container;
        $scope.compose_email_subject = "";
        $scope.compose_email_body = "";
    };

    $scope.ignoreEmail = function(id,isCalPassReq,reqId){
        if(checkRequired(id)){
            $http.post('/interactions/update/interaction/ignore',{interactionId:id,isCalPassReq:isCalPassReq,requestId:reqId})
                .success(function(response){

                })
        }
    };

    $scope.sendEmail = function(subject,body,docTrack,trackViewed,remind,rName,rEmailId,receiverId,bodyContent,newMessage){

        if(!checkRequired(subject)){
            toastr.error("Please enter an email subject.")
        }
        else if(!checkRequired(body)){
            toastr.error("Please enter an email body.")
        }
        else{
            if(bodyContent != null){
                body = body+'\n\n\n'+bodyContent.bodyContent;
            }
            var obj = {
                receiverEmailId:rEmailId,
                receiverName:rName,
                message:body,
                subject:subject,
                receiverId:receiverId,
                docTrack:docTrack,
                trackViewed:trackViewed,
                remind:remind,
                newMessage:newMessage?newMessage:false
            };
            if(!checkRequired(obj.receiverName)){
                obj.receiverName = meetingObjService.getSelectedParticipantName();
            }
            if(bodyContent != null && bodyContent.updateReplied){
                obj.updateReplied = true;
                obj.refId = bodyContent.interaction.refId
            }

            //Used for Outlook
            if(bodyContent && bodyContent.interaction && bodyContent.interaction.refId){
                obj.id = bodyContent.interaction.refId
            }

            $http.post("/messages/send/email/single/web",obj)
               .success(function(response){
                    if(response.SuccessCode){
                        if(bodyContent == null){
                            $scope.openSendEmailContainer();
                        }

                        toastr.success(response.Message);
                        setTimeout(function(){
                            $scope.timelineTimeline($scope.cuserId, $scope.cEmailId);
                            meetingObjService.participantClickPast90Days_again();
                        },2000)
                        if(bodyContent != null){
                            $scope.ignoreEmail(bodyContent._id,false,null);
                        }
                    }
                    else{
                        toastr.error(response.Message);
                    }
                })
        }
    };

    $scope.viewItem = function(action,refId,dataObj,_id,emailAction){
        switch(action){
            case 'viewDoc' : window.location = '/readDocument/'+refId;
                break;
            case 'viewStats' : $scope.showDocAnalytics(dataObj.emailId,refId,dataObj,_id)// view stats
                break;
            case 'viewEmail' :

                //if(!dataObj.composeMailBox && !dataObj.emailBodyBox){
                    if(emailAction == 'reply'){
                        dataObj.isView = false;
                        if(!dataObj.composeMailBox){
                            //dataObj.doIgnore = doIgnore;
                            dataObj.composeMailBox = true;
                            dataObj.emailBodyBox = true;
                        }
                        else{
                            //dataObj.doIgnore = doIgnore;
                            dataObj.composeMailBox = false;
                            dataObj.emailBodyBox = false;
                        }
                    }
                    else{
                        if(!dataObj.isView){
                            //dataObj.doIgnore = false;
                            dataObj.isView = true;
                            dataObj.composeMailBox = false;
                            dataObj.emailBodyBox = true;
                            dataObj.loading = true;
                        }
                        else{
                            dataObj.composeMailBox = false;
                            dataObj.emailBodyBox = false;
                            dataObj.isView = false;
                        }
                    }
                    if(checkRequired(dataObj.bodyContent)){
                        dataObj.bodyContent = dataObj.bodyContent.replace(/\n/g, "<br />");
                        dataObj.interaction.description = dataObj.bodyContent
                        dataObj.interaction.source = 'relatas'
                    }
                    if(checkRequired(dataObj.interaction.emailContentId) && dataObj.interaction.source != 'relatas'){
                        $http.get('/message/get/email/single/web?emailContentId='+dataObj.interaction.emailContentId+'&googleAccountEmailId='+dataObj.interaction.googleAccountEmailId)
                            .success(function(response){
                                dataObj.loading = false;
                                if(response.SuccessCode){
                                    dataObj.bodyContent = response.Data.data.replace(/\n/g, "<br />")
                                    //dataObj.bodyContent = response.Data.data;

                                    var temp = response.Data.data.replace(/\n/g, "<br />"); //to remove internal css
                                    var start, end;
                                    start = temp.indexOf("<style");
                                    end = temp.indexOf("</style>");
                                    end = end != -1 ? end + 8 : end;
                                    var replace = temp.slice(start , end);

                                    dataObj.bodyContent = dataObj.bodyContent.replace(replace, "");
                                    temp = dataObj.bodyContent;

                                    var index = temp.indexOf('/track/email/open/')
                                    if(index != -1){
                                        dataObj.bodyContent = removeRelatasTrackImg(dataObj.bodyContent, index)
                                    }

                                    if(dataObj.updateOpened){
                                        $scope.updateEmailOpen(dataObj.emailId,dataObj.trackId,dataObj.userId);
                                    }
                                }
                                else dataObj.bodyContent = response.Message || '';
                            })
                    }
                    else{
                        dataObj.loading = false;
                        dataObj.bodyContent = dataObj.interaction.description.replace(/\n/g, "<br />");
                        if(dataObj.updateOpened){
                            $scope.updateEmailOpen(dataObj.emailId,dataObj.trackId,dataObj.userId);
                        }
                    }

                break;
            case 'viewTask' :// view stats
                if(!$scope[dataObj.bindHtmlKey]){
                    var image = '/images/default.png';
                    var ownerName = '';
                    var dueDate = moment(dataObj.interaction.interactionDate).tz($scope.timezone);
                    dueDate = dueDate.format("Do MMM");
                    var title = dataObj.interaction.title || ''
                    var description = dataObj.interaction.description || ''
                    if(dataObj.interaction.action == 'sender'){
                        image = '/getImage/'+ $scope.cuserId;
                        ownerName = $scope.firstName;
                    }
                    else{
                        image = '/getImage/'+ meetingObjService.l_user._id;
                        ownerName = $scope.rName;
                    }

                    $scope[dataObj.bindHtmlKey] = true;

                    $("#"+_id).text("Close");
                    var html2 = '<br><br><div style="border:1px solid #eee;padding:10px 0 0 20px; margin-bottom: 10px;margin-top: 7px;">' +
                        '<div class="row">' +
                        '<div class="col-xs-12">' +
                        '<div style="padding:0 0 0 10px" class="timeline-text-trunc">' +
                        '<strong>Owner: </strong><img class="image-placeholder-small" src='+image+' title='+ownerName+'><br><br>' +
                        '<strong>Due Date: </strong><span>'+dueDate+'</span><br>'+
                        '<strong>Title: </strong><span>'+title+'</span><br>'+
                        '<strong>Description: </strong><span>'+description+'</span><br><br>'+
                        '</div>' +
                        '</div>' +
                            //'<button id='+id+' style="float:right;margin-right: 36px;margin-bottom: 5px;" class="btn-green btn">Close</button>' +
                        '</div>' +
                        '</div>';
                    $("#"+dataObj.bindHtmlKey).html(html2);
                    $("#"+dataObj.bindHtmlKey).show();
                }
                else{
                    $scope[dataObj.bindHtmlKey] = false;
                    $("#"+dataObj.bindHtmlKey).hide();
                    $("#"+_id).text(" View Task")
                }
                break;
            default:'';
                break;
        }
    };

    $scope.sendEmailTimeline = function(sub){

    };

    $scope.updateEmailOpen = function(emailId,trackId,userId){

        if(!emailId){
            emailId = meetingObjService.lUseEmailId;
        }

        if(checkRequired(emailId) && checkRequired(trackId) && checkRequired(userId)){
            $http.get('/track/email/open/'+emailId+'/track/id/'+trackId+'/user/'+userId)
                .success(function(response){

                });
        }
    };


    $scope.showDocAnalytics = function(emailId,refId,dataObj,_id){
        $http.get('/analytics/'+refId+'/byEmail/'+$scope.cEmailId)
            .success(function(response){
                if(response && response.accessInfo && response.accessInfo.pageReadTimings){
                    var prt = response.accessInfo.pageReadTimings;
                    var abc = prt.slice();
                    var prtNoDups = removeDuplicates(abc);

                    var separate = [];
                    for (var i in prtNoDups) {

                        var totalTime = 0;
                        var count = 0;
                        var pageNo = 0;
                        for (var j in prt) {
                            if (prt[j].pageNumber == prtNoDups[i].pageNumber) {

                                totalTime = totalTime + parseFloat(prt[j].Time);
                                count += 1;
                                pageNo = prtNoDups[i].pageNumber;
                            }
                        }
                        //  if(totalTime > 0){
                        var before = totalTime;

                        // totalTime = totalTime/count
                        totalTime = Math.round(totalTime / 60) // im minutes
                        if (totalTime == 0) {
                            if (before > 0) {
                                totalTime = 0.5
                            }
                        }
                        // }

                        var tootlTip = 'Page:' + pageNo + '\nMinutes:' + totalTime;
                        separate.push([
                            pageNo,
                            totalTime,
                            tootlTip,
                            'color:#03A2EA'
                        ])
                        var timeMax = 0;
                        jQuery.map(separate, function (obj) {
                            if (obj[1] > 0)
                                timeMax = obj[1];
                        });
                    }
                    var firstAccessed = "First Accessed: " + $scope.getDateFormat(response.firstAccessed) + ' ' + $scope.getTimeFormat(response.firstAccessed);
                    var lastAccessed = "Last Accessed : " + $scope.getDateFormat(response.lastAccessed) + ' ' + $scope.getTimeFormat(response.lastAccessed)
                    var numerOfTimesAccessed = "Number of times accessed: " + response.accessCount || 0

                    var chartId = 'analyticsGraph'+moment().format();
                    var close_analytics = 'close'+moment().format("hh-mm-ss");
                    if(!$scope[dataObj.bindHtmlKey]) {
                        $scope[dataObj.bindHtmlKey] = true;
                        var body = dataObj.body || '';
                        $("#" + _id).text("Close");
                        var html = '<div class="dialog" id="analytics-box" style="width:100%; background-color: #ffffff;text-align: -webkit-center;"><br><br>' +
                            '<div class="meeting-request-detail" id="analyticsDetails">' +
                            '<span id="firstAccessed">'+firstAccessed+'</span></br>' +
                            '<span id="lastAccessed">'+lastAccessed+'</span></br>' +
                            '<span id="accessCount">'+numerOfTimesAccessed+'</span></br>' +
                            '</div><br>' +
                            '<span id="analyticMsg"></span>' +
                            '<div id='+chartId+'>' +
                            '</div>' +

                                // '<button id='+close_analytics+' style="float:right;margin-right: 4%;" class="btn-primary">Close</button>' +

                            '</div>';
                        $("#"+dataObj.bindHtmlKey).html(html);
                        $("#"+dataObj.bindHtmlKey).show();
                    }
                    else{
                        $scope[dataObj.bindHtmlKey] = false;
                        $("#"+dataObj.bindHtmlKey).hide();
                        $("#"+_id).html("&nbsp; View Stats")
                    }


                    if (checkRequired(separate[0])) {
                        $scope.drawAnalytics(separate, prtNoDups.length, timeMax,chartId);
                    }
                    else {
                       // this doc not yet accessed
                        toastr.info("This document not accessed yet.");
                    }

                   // updateDocAccessed(docId,userId);
                }
                else toastr.info("Document not accessed yet.");
            })
    };

    $scope.drawAnalytics = function(a, maxPages, timeMax, elementId) {

        var data = new google.visualization.DataTable();

        data.addColumn('number', 'Pages');
        data.addColumn('number', 'Minutes');
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addColumn({type: 'string', role: 'style'});
        data.addRows(a);
        var options = {
            width: 250,
            hAxis: {title: 'Page', minValue: 0, maxValue: maxPages + 2},
            vAxis: {title: 'Total Page Open Time', minValue: 0, maxValue: timeMax + 2}
        };


        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById(elementId));
        chart.draw(data, options);

    };

    $scope.getDateFormat = function(date) {
        var dateObj = moment(date).tz($scope.timezone)
        return dateObj.format("DD MMM, YYYY")
        //return dateObj.getDate() + ' ' + monthNameSmall[dateObj.getMonth()] + ',' + dateObj.getFullYear()
    };

    $scope.getTimeFormat = function(date) {
        var dateObj = moment(date).tz($scope.timezone);
       return dateObj.format("hh:mm a")
    };

    $scope.updateTaskStatus = function(taskId,status){
        meetingObjService.updateTaskStatus($scope,$http,taskId,status,true);
    };

    $scope.assignTaskTimeline = function(event){
        meetingObjService.assignTask(meetingObjService.getSelectedParticipantName(),$scope.emailId,$scope.userId,event)
    };

    meetingObjService.assignTask = function(name,emailId,userId,event){

        $scope.dueDate = null;
        $scope.name = name;
        $scope.emailId = emailId;
        $scope.userId = userId;
        $("#add-task-box").toggle();
        var position = $("#add-task-timeline").offset()
        $('#dueDateTask').datetimepicker({
            dayOfWeekStart: 1,
            format:'d-m-Y h:i a',
            lang: 'en',
            minDate: new Date(),
            onClose: function (dp, $input){
                var selected = moment(dp);
                $scope.dueDate = selected.format();
            }
        });
    };

    $scope.assignTaskFinal = function(title,description){

        if(!checkRequired($scope.dueDate) || moment().isAfter(moment($scope.dueDate)) || moment().isSame(moment($scope.dueDate))){
            toastr.error("Please select valid date and time")
        }
        else if(!(checkRequired(title) && checkRequired(title.trim()))){
            toastr.error("Please provide Task title");
        }
        else if(!(checkRequired(description) && checkRequired(description.trim()))){
            toastr.error("Please provide Task description");
        }
        else if(!checkRequired($scope.dueDate)){
            toastr.error("Please select valid date")
        }
        else{
            var reqObj = {
                "taskName":title,
                "assignedTo":$scope.userId,
                "assignedToEmailI":$scope.emailId,
                "dueDate":$scope.dueDate,
                "taskFor":"other",
                "description":description
            };
            $http.post('/task/create/new/web',reqObj)
                .success(function(response){
                    if(response.SuccessCode){
                        toastr.success(response.Message);
                        $scope.closePopup()
                        meetingObjService.timelineTasks($scope.userId,$scope.emailId)
                    }
                    else toastr.error(response.Message);
                })
        }
    };

    $scope.closePopup = function(){
        $("#taskTitle").val("")
        $("#taskDescription").val("")
        $scope.taskTitle = ""
        $scope.taskDescription = ""
        $("#add-task-box").toggle(100);
    };

    $scope.goToUrl = function(url){
        window.location = url;
    }
});

reletasApp.controller("task-assign",function($scope, $http, meetingObjService){

});

function getTextLength(text,maxLength){
    if(!checkRequired(text)) return ""

    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

function escapeHtml(unsafe) {
    return unsafe
    // .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
    // .replace(/"/g, "&quot;")
    // .replace(/'/g, "&#039;");
}

function removeRelatasTrackImg(data, index){
    var temp = data;
    var f = temp.lastIndexOf('<img style=', index);
    var e = index + temp.substring(index).indexOf('\">');
    e = e != -1 ? e + 2 : e;
    var slice = temp.slice(f, e)

    return temp = temp.replace(slice, "")
    //return temp = temp;
}