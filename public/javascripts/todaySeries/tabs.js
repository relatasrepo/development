/**
 * Created by ramesh on 25/9/15.
 */

$(document).ready(function () {
    $('.tab-1').show()
    $('.tab-user-1').addClass('tab-user-list')
    $('.tab-user-1').click(function(){
        $('.tab-1').show()
        $('.tab-2').hide();
        $('.tab-user-1').addClass('tab-user-list')
        $('.tab-user-2').removeClass('tab-user-list')

    })
    $('.tab-user-2').click(function(){
        $('.tab-2').show();
        $('.tab-1').hide();
        $('.tab-user-1').removeClass('tab-user-list')
        $('.tab-user-2').addClass('tab-user-list')
    })


/*
    var tag="images/icon_twitter.png";
    var barData = {
        labels: ['', '', '', '', '', '',''],
        datasets: [
            {
                label: '2010 customers #',
                fillColor: '#f86b4f',
                data: [500, 1902, 1041, 610, 1245, 952,100]
            }
        ]
    };

    var context = document.getElementById('clients').getContext('2d');
    var clientsChart = new Chart(context).Bar(barData);




    var $ppc = $('.progress-pie-chart'),
        percent = parseInt($ppc.data('percent')),
        deg = 360*percent/100;
    if (percent > 50) {
        $ppc.addClass('gt-50');
    }
    $('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
    $('.ppc-percents span').html(percent+'');
*/


    $('#timeline1-sec').show();
    // $('#opportunity-sec').show();
    $('#timeline-button').addClass('active');
    $('#timeline-button').click(function(){
        $('#timeline-button').addClass('active');
        $('#task-button').removeClass('active');
        $('#social-button').removeClass('active');
        $('#files-button').removeClass('active');
        $('#opportunity-button').removeClass('active');
        $('#notes-button').removeClass('active');
        $('#opportunity-sec').hide()
        $('#timeline1-sec').show();
        $('#social-sec').hide();
        $('#files-sec').hide();
        $('.email-view-container').hide()
        $('#tasks-sec').hide();
        $('#send-email-container').hide();
        $('#notes-sec').hide();
    })
    $('#social-button').click(function(){
        $('#timeline-button').removeClass('active');
        $('#task-button').removeClass('active');
        $('#social-button').addClass('active');
        $('#files-button').removeClass('active');
        $('#opportunity-button').removeClass('active');
        $('#notes-button').removeClass('active');
        $('#opportunity-sec').hide();
        $('#social-sec').show();
        $('#timeline1-sec').hide();
        $('#files-sec').hide();
        $('.email-view-container').hide()
        $('#tasks-sec').hide();
        $('#send-email-container').hide();
        $('#notes-sec').hide();
    })
    $('#opportunity-button').click(function(){
        $('#timeline-button').removeClass('active');
        $('#task-button').removeClass('active');
        $('#social-button').removeClass('active');
        $('#files-button').removeClass('active');
        $('#opportunity-button').addClass('active');
        $('#notes-button').removeClass('active');
        $('#opportunity-sec').show();
        $('#social-sec').hide();
        $('#timeline1-sec').hide();
        $('#files-sec').hide();
        $('.email-view-container').hide()
        $('#tasks-sec').hide();
        $('#send-email-container').hide();
        $('#notes-sec').hide();
    })
    $('#notes-button').click(function(){
        $('#timeline-button').removeClass('active');
        $('#task-button').removeClass('active');
        $('#social-button').removeClass('active');
        $('#files-button').removeClass('active');
        $('#opportunity-button').removeClass('active');
        $('#notes-button').addClass('active');
        $('#notes-sec').show();
        $('#opportunity-sec').hide();
        $('#social-sec').hide();
        $('#timeline1-sec').hide();
        $('#files-sec').hide();
        $('.email-view-container').hide()
        $('#tasks-sec').hide();
        $('#send-email-container').hide();
    })
    $('#files-button').click(function(){
        $('#files-button').addClass('active');
        $('#timeline-button').removeClass('active');
        $('#task-button').removeClass('active');
        $('#social-button').removeClass('active');
        $('#opportunity-button').removeClass('active');
        $('#opportunity-sec').hide()
         $('#files-sec').show();
        $('#timeline1-sec').hide();
        $('#social-sec').hide();
        $('#tasks-sec').hide();
        $('.email-view-container').hide()
        $('#send-email-container').hide();

    })
    $('#task-button').click(function(){
        $('#timeline-button').removeClass('active');
        $('#task-button').addClass('active');
        $('#social-button').removeClass('active');
        $('#files-button').removeClass('active');
        $('#opportunity-button').removeClass('active');
        $('#opportunity-sec').hide()
        $('#tasks-sec').show();
        $('#timeline1-sec').hide();
        $('#social-sec').hide();
        $('#files-sec').hide();
        $('.email-view-container').hide();
        $('#send-email-container').hide();
    })

    /*$('#send-email').click(function(){
        $('#send-email-container').show();
        $('.email-view-container').hide()
    })*/
    $("#view-email-btn").click(function(){
        $('.email-view-container').show()
    })

    $('#re-schedule-meeting').click(function(){
        $('.re-schedule-popup').toggle();
        $('.decline-popup').hide();
        $('.confirm-popup').hide();
    })
    $('.close-btn').click(function(){
        $('.confirm-popup').hide();
        $('.re-schedule-popup').hide();
        $('.decline-popup').hide();
    });

    $('#confirm-button').click(function(){
        $('.confirm-popup').toggle()
        $('.re-schedule-popup').hide();
        $('.decline-popup').hide();
    })
    $('#meeting_decline-btn').click(function(){
        $('.decline-popup').toggle();
        $('.re-schedule-popup').hide();
        $('.confirm-popup').hide();

    })
})